<%--
*******************************************************************************
  Dashboard Preview -- this is essentially the home page
  with dummy data and no actions on it.
  
  The list of datagrids is taken from the request rather than the database.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="java.text.SimpleDateFormat, com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"    	 scope="session"></jsp:useBean>
<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>

<%

  //get the comma delimited set of sections to display
  String sectionsToDisplay = StringFunction.xssCharsToHtml(request.getParameter("sections"));

  List<String> sections = new ArrayList<String>();
  if ( sectionsToDisplay != null ) {
    StringTokenizer strTok = new StringTokenizer(sectionsToDisplay, ",");
    while (strTok.hasMoreTokens()) {
      String sectionId = strTok.nextToken();     
      sections.add(sectionId);
    }
  }

  Debug.debug("***START******** DASHBOARD Preview Page  ************START***");

  final String MY_MAIL_AND_UNASSIGNED = "Mail.MeAndUnassigned";
  final String ALL_MAIL = "Mail.AllMail";
  final String ALL_NOTIFS = "Notifications.All";
  final String ALL = "common.all";
  final String ALL_WORK = "common.allWork";
  final String MY_WORK = "common.myWork";

  String  userOid        = userSession.getUserOid();
  String  userOrgOid	 = userSession.getOwnerOrgOid();
  String  loginLocale    = userSession.getUserLocale(); 	
  String  loginRights    = userSession.getSecurityRights();
  String  defaultWIPView = userSession.getDefaultWipView();
  String  currentSecurityType = userSession.getSecurityType();
  boolean isCustomerSubsidiaryAccess = userSession.hasSavedUserSession();
  DocumentHandler hierarchyDoc = null;
  int totalOrganizations = 0;
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  

  //Variables needed for the sql statements and the returned number of rows.
  CorporateOrganizationWebBean org  = null;
  try {
    org = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
    org.getById(userOrgOid);
  }
  catch(Exception e){
    System.out.println("************** Unable to build the user object because" +
      " the userOid on the UserSession is null, which caused the" +
      " getDataFromAppserver to fail!  ***************");
  }
  String isDisplayPreDebitNotify = "";
  if(StringFunction.isNotBlank(userSession.getClientBankOid())){
  com.ams.tradeportal.common.cache.Cache reCertCache = (com.ams.tradeportal.common.cache.Cache)com.ams.tradeportal.common.cache.TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   	isDisplayPreDebitNotify = CBCResult.getAttribute("/ResultSetRecord(0)/PRE_DEBIT_FUNDING_OPTION_IND");
  }
  
  String preDebitEnabledAtCorpLevel = TradePortalConstants.INDICATOR_NO;
  if(TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)) {
     	preDebitEnabledAtCorpLevel = org.getAttribute("pre_debit_funding_option_ind");
  }
  
%>
<%-- *****************   HTML starts here   ****************** --%>

<div class="dialogContent fullwidth">

<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  String gridHtml = "";

%>

    <%--main content --%>
<%
  if (!TradePortalConstants.ADMIN.equals(currentSecurityType)) {
	  
	  if ( sections.size() <= 0 ) {
	    	if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MAIL_MESSAGES ) ) {
	   	 		sections.add(TradePortalConstants.DASHBOARD_SECTION__MAIL);
	    	}
		   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NOTIFICATIONS ) ) {
	    		sections.add(TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS);
		   }
		   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DEBIT_FUNDING_MAIL )
				   && TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)
				   &&  TradePortalConstants.INDICATOR_YES.equals(preDebitEnabledAtCorpLevel) ) {
		   		sections.add(TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL);
			}
		   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_OFFERED ) ) {
	    		sections.add(TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED);
		   }
		   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSACTIONS ) ) {
	    		sections.add(TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS);
	       }
		   
		   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PANEL_AUTHORISATION_TRANSACTIONS ) ) {
	   			sections.add(TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS); 
		   }
		   
		   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS ) ) {
	    		sections.add(TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES);
		   }
		   if ( sections.contains( ConditionalDisplay. DISPLAY_ACCOUNT_BALANCES) ) {
	    		sections.add(TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES);
		   }
	  } 
	  

    //execute common items if necessary

    //get the org hierarchy - this is used in most view so always do
    // This is the query used for populating the option drop down list; 
    // it retrieves all active child organizations that belong to the user's 
    // org in addition to the user's org itself.  It is used by both the mail
    // and notification.
    //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    String sqlQuery = "select organization_oid, name   from corporate_org "
   					 +" where activation_status = ? start with organization_oid = ? connect by prior organization_oid = p_parent_corp_org_oid"
  					 +" order by "+resMgr.localizeOrderBy("name");
    hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, TradePortalConstants.ACTIVE, userOrgOid);
    totalOrganizations = hierarchyDoc.getFragments("/ResultSetRecord").size();

  } //end if !ADMIN
  else { //if ADMIN

  }
 

  //now spin through the sections list.  
  // note that order of the below sections in the code doesn't matter
  // becuase the sections list specifies the order of display

  String sectionId = "";
  int gridShowCount = 5;
  for( int sectionIdx = 0; sectionIdx < sections.size(); sectionIdx++ ) {
    sectionId = sections.get(sectionIdx);
    /*MEer IR-34829 Update Preview page with NO sections display message.*/
    /************************************************************************/
    /*                           NO SECTIONS TO DISPLAY                     */
    /************************************************************************/
    if(TradePortalConstants.DASHBOARD_NO_SECTIONS_DISPLAY.equals(sectionId)){ 
%>    	
    
     <div id="infoSection" class="infos">
    <div id="infoTextMessage"  class="infoText"><%=resMgr.getText("Dashboard.Customization.Message", TradePortalConstants.TEXT_BUNDLE) %></div>
  </div>
   
<%     	
    }
    

    /************************************************************************/
    /*                           MAIL SECTION                               */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__MAIL.equals(sectionId)) {
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
          <%=resMgr.getText("Home.MailMessages", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%  
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) { 
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="mailShowCount5" class="<%= show5Class %>" >5</span>
            <span id="mailShowCount10" class="<%= show10Class %>" >10</span>
            <span id="mailShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeMailGrid_totalCount">0</span>
          </span>
          <span class="gridHeaderEdit"></span>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>
      <div>
<%    //build mail options
      String currentMailOption = EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey());

      StringBuffer mailOrgOptions = new StringBuffer();
      // If the user has rights to view child organization mail, retrieve the list of dropdown
      // options from the DatabaseQueryBean results doc (containing an option for each child org);
      // otherwise, simply use the user's org option to build the dropdown list (in addition 
      // to the default 'Me' or 'Me and Unassigned' option).
      mailOrgOptions.append("<option value=\"").
        append(EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey())).
        append("\" selected >");
      mailOrgOptions.append(resMgr.getText( MY_MAIL_AND_UNASSIGNED, TradePortalConstants.TEXT_BUNDLE));
      mailOrgOptions.append("</option>");
      if (SecurityAccess.hasRights(loginRights, SecurityAccess.VIEW_CHILD_ORG_MESSAGES)) {
         mailOrgOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME", 
                                                           currentMailOption, userSession.getSecretKey()));
         // Only display the 'All Mail' option if the user's org has child organizations
         if (totalOrganizations > 1) {
            mailOrgOptions.append("<option value=\"").
              append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_MAIL, userSession.getSecretKey())).
              append("\"");
            if (currentMailOption.equals(ALL_MAIL)) {
               mailOrgOptions.append(" selected");
            }
            mailOrgOptions.append(">");
            mailOrgOptions.append(resMgr.getText( ALL_MAIL, TradePortalConstants.TEXT_BUNDLE));
            mailOrgOptions.append("</option>");
         }
      }
      else {
         mailOrgOptions.append("<option value=\"").
           append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())).
           append("\"");
         if (currentMailOption.equals(userOrgOid)) {
            mailOrgOptions.append(" selected");
         }
         mailOrgOptions.append(">");
         mailOrgOptions.append(userSession.getOrganizationName());
         mailOrgOptions.append("</option>");
      }
%>
        <%= widgetFactory.createSearchSelectField( "MailOrg", "Home.Mail.Show", "", mailOrgOptions.toString()) %>
      </div>
<%
      gridHtml = dGridFactory.createDataGrid("homeMailGrid","HomeMailMessagesDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
    } //end if mail section
    
    
    
    
    
    //jgadela R92 IR T36000037116 - DASHBOARD_SECTION_DEBIT_FUNDING_MAIL - START
    /************************************************************************/
    /*                           PRE-DEBIT FUNDING NOTIFICATIONS            */
    /************************************************************************/
    
    if ( TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL.equals(sectionId) 
    		&& TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)
 		   && TradePortalConstants.INDICATOR_YES.equals(preDebitEnabledAtCorpLevel)
         && condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DEBIT_FUNDING_MAIL ) ) {
    	
  %>
      <div class="formContentNoSidebar">
        <div class="gridHeader">
        <span class="gridHeaderTitle">
           <%=resMgr.getText("Home.DebitFundingNotifications", TradePortalConstants.TEXT_BUNDLE)%>
         <%=widgetFactory.createHoverHelp("mailLink", "Home.MailMessagesLink") %>
       </span>

          <span class="gridHeader-right">
            <span class="gridHeaderShowCounts">
              <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
  <%
        String show5Class = "gridHeaderShowCountItem";
        String show10Class = "gridHeaderShowCountItem";
        String show20Class = "gridHeaderShowCountItem";
        if ( gridShowCount == 5 ) {
          show5Class += " selected";
        }
        else if ( gridShowCount == 10 ) {
          show10Class += " selected";
        }
        else if ( gridShowCount == 10 ) {
          show20Class += " selected";
        }
  %>
              <span id="debFundmailShowCount5" class="<%= show5Class %>" >5</span>
              <span id="debFundmailShowCount10" class="<%= show10Class %>" >10</span>
              <span id="debFundmailShowCount20" class="<%= show20Class %>" >20</span>
              <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
              <span id="homeDebitFundMailGrid_totalCount">0</span>
            </span>
            <span id="homeDebitFundMailGridEdit" class="gridHeaderEdit"></span>
            <%=widgetFactory.createHoverHelp("homeDebitFundMailGridEdit", "CustomizeListImageLinkHoverText") %>
          </span>
          <%--<div style="clear:both;"></div>--%>
        </div>
        <div>
  <%    //build mail options
        String currentDebFundMailOption = EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey());

        StringBuffer debFundMailOrgOptions = new StringBuffer();
        // If the user has rights to view child organization mail, retrieve the list of dropdown
        // options from the DatabaseQueryBean results doc (containing an option for each child org);
        // otherwise, simply use the user's org option to build the dropdown list (in addition
        // to the default 'Me' or 'Me and Unassigned' option).
        debFundMailOrgOptions.append("<option value=\"").
          append(EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey())).
          append("\" selected >");
        debFundMailOrgOptions.append(resMgr.getText( MY_MAIL_AND_UNASSIGNED, TradePortalConstants.TEXT_BUNDLE));
        debFundMailOrgOptions.append("</option>");
        if (SecurityAccess.hasRights(loginRights, SecurityAccess.VIEW_CHILD_ORG_MESSAGES)) {
           debFundMailOrgOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME",
                                                             currentDebFundMailOption, userSession.getSecretKey()));
           // Only display the 'All Mail' option if the user's org has child organizations
           if (totalOrganizations > 1) {
              debFundMailOrgOptions.append("<option value=\"").
                append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_MAIL, userSession.getSecretKey())).
                append("\"");
              if (currentDebFundMailOption.equals(ALL_MAIL)) {
                 debFundMailOrgOptions.append(" selected");
              }
              debFundMailOrgOptions.append(">");
              debFundMailOrgOptions.append(resMgr.getText( ALL_MAIL, TradePortalConstants.TEXT_BUNDLE));
              debFundMailOrgOptions.append("</option>");
           }
        }
        else {
           debFundMailOrgOptions.append("<option value=\"").
             append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())).
             append("\"");
           if (currentDebFundMailOption.equals(userOrgOid)) {
              debFundMailOrgOptions.append(" selected");
           }
           debFundMailOrgOptions.append(">");
           debFundMailOrgOptions.append(userSession.getOrganizationName());
           debFundMailOrgOptions.append("</option>");
        }
        
        String datePattern = userSession.getDatePattern();
        String dateWidgetOptions = "placeHolder:'" + datePattern + "'"; 
        String currency = "";
        String ccyOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
  %>
		<%= widgetFactory.createSearchSelectField( "DebitFundMailOrg", "Home.Mail.Show", "", debFundMailOrgOptions.toString()) %>
		<%=widgetFactory.createSubLabel("Home.DebitFundingNotifications.Section.PaymentDate")%>
		<%=widgetFactory.createSearchDateField("PaymentDateFrom","Home.DebitFundingNotifications.PaymentDateFrom","class='char10'",dateWidgetOptions) %>
		<%=widgetFactory.createSearchDateField("PaymentDateTo","Home.DebitFundingNotifications.PaymentDateTo","class='char10'",dateWidgetOptions) %>
		<%=widgetFactory.createSearchSelectField("DebFundCcy", "Home.DebitFundingNotifications.CCY", " ", ccyOptions,"class='char15'") %>
        </div>
  <%
  	gridHtml = dGridFactory.createDataGrid("homeDebitFundMailGrid","HomeDebitFundingMailMsgDataGrid",null);
  %>
        <%= gridHtml %>
      </div>
  <%
       
      }
    //jgadela R92 IR T36000037116 - DASHBOARD_SECTION_DEBIT_FUNDING_MAIL - End


    /************************************************************************/
    /*                           NOTIFICATIONS SECTION                      */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS.equals(sectionId) ) {
%>


    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
          <%=resMgr.getText("Home.Notifications", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%  
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) { 
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
          <span id="notifShowCount5" class="<%= show5Class %>" >5</span>
          <span id="notifShowCount10" class="<%= show10Class %>" >10</span>
          <span id="notifShowCount20" class="<%= show20Class %>" >20</span>

            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeNotifGrid_totalCount">0</span>
          </span>
          <span class="gridHeaderEdit"></span>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>
      <div>
<%    //build notification options
      String currentNotifOption = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());

      StringBuffer notifOrgOptions = new StringBuffer();
      // If the user has rights to view child organizations, retrieve the list of 
      // dropdown options from the DatabaseQueryBean results doc (containing an 
      // option for each child org), including an "All" option.  Otherwise,
      // simply use the user's org as the only option.
      if (SecurityAccess.hasRights(loginRights, 
          SecurityAccess.VIEW_CHILD_ORG_MESSAGES)) {
        notifOrgOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME", 
                                                           currentNotifOption, userSession.getSecretKey()));

        // Only display the 'All' option if the user's org has child orgs
        if (totalOrganizations > 1) {
          notifOrgOptions.append("<option value=\"");
          notifOrgOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_NOTIFS, userSession.getSecretKey()));
          notifOrgOptions.append("\"");

          if (currentNotifOption.equals(ALL_NOTIFS)) {
            notifOrgOptions.append(" selected");
          }

          notifOrgOptions.append(">");
          notifOrgOptions.append(resMgr.getText( ALL_NOTIFS, TradePortalConstants.TEXT_BUNDLE));
          notifOrgOptions.append("</option>");
        }
      } 
      else {
        notifOrgOptions.append("<option value=\"");
        notifOrgOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
        notifOrgOptions.append("\"");

        if (currentNotifOption.equals(userOrgOid)) {
          notifOrgOptions.append(" selected");
        }

        notifOrgOptions.append(">");
        notifOrgOptions.append(userSession.getOrganizationName());
        notifOrgOptions.append("</option>");
      }
%>
        <%= widgetFactory.createSearchSelectField( "NotifOrg", "Home.Notifications.Show", "", notifOrgOptions.toString()) %>
<%
      StringBuffer notifStatusOptions = new StringBuffer();
      notifStatusOptions.append("<option value='").append(ALL_NOTIFS).append("'").
        append(" selected >").
        append(resMgr.getText( ALL_NOTIFS, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZED, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_DELETED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_DELETED, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_STARTED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_STARTED, loginLocale)).
        append("</option>");
%>
        <%=widgetFactory.createSearchSelectField( "NotifStatus", "Home.Notifications.criteria.Status", "", notifStatusOptions.toString()) %>

      </div>
      
<%
      gridHtml = dGridFactory.createDataGrid("homeNotifGrid","HomeNotificationsDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
    } //end if notifications section

    //Ravindra - CR-708B - Start
    /************************************************************************/
    /*                           INVOICES OFFERED SECTION 					 */
    /************************************************************************/
    if ( TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED.equals(sectionId) &&
         condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_OFFERS ) ) {
   
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">

            <%=resMgr.getText("Home.InvoicesOffered", TradePortalConstants.TEXT_BUNDLE)%>
        
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 20 ) {
        show20Class += " selected";
      }
%>
  			<span id="invoicesOfferedShowCount5" class="<%= show5Class %>" >5</span>
            <span id="invoicesOfferedShowCount10" class="<%= show10Class %>" >10</span>
            <span id="invoicesOfferedShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeInvoicesOfferedGrid_totalCount">0</span>
          </span>
          <span class="gridHeaderEdit"></span>
         
        </span>
       
      </div>
      <div>
       <%
       String currency = "";
       String options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
       String datePattern = userSession.getDatePattern();
       String dateWidgetOptions = "placeHolder:'" + datePattern + "'"; 

       %>
      	<%=widgetFactory.createSearchSelectField("Currency", "InvoiceSearch.Currency", " ", options,"class='char5'") %>
      	<%=widgetFactory.createSubLabel("InvoicesOffered.Amount")%>
  		<%=widgetFactory.createSearchAmountField("AmountFrom", "InvoiceSearch.From","","style='width:8em' class='char5'", "") %>
		<%=widgetFactory.createSearchAmountField("AmountTo", "InvoiceSearch.To","","style='width:8em' class='char5'", "") %>
		<%=widgetFactory.createSubLabel("InvoicesOffered.DueOrPaymentDateFilter")%>
		<%=widgetFactory.createSearchDateField("DateFrom","InvoiceSearch.From","class='char10'",dateWidgetOptions) %>
		<%=widgetFactory.createSearchDateField("DateTo","InvoiceSearch.To","class='char10'",dateWidgetOptions) %>
      <div style="clear:both"></div>
      </div>
<%
	gridHtml = dGridFactory.createDataGrid("homeInvoicesOfferedGrid","HomeInvoicesOfferedDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
   
    } //end if invoices offered section
  
    /************************************************************************/
    /*                           ALL TRANSACTIONS SECTION                   */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS.equals(sectionId) ) {
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
          <%=resMgr.getText("Home.AllTransactions", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%  
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) { 
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="allTranShowCount5" class="<%= show5Class %>" >5</span>
            <span id="allTranShowCount10" class="<%= show10Class %>" >10</span>
            <span id="allTranShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeAllTranGrid_totalCount">0</span>
          </span>
          <span class="gridHeaderEdit"></span>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>

      <div>
<%    //build alltran options
      String currentAllTranWork = MY_WORK;
      String currentAllTranGroup = TradePortalConstants.INSTR_GROUP__ALL;
      String currentAllTranInstrType = ALL;
      String currentAllTranStatus = TradePortalConstants.STATUS_ALL;

      StringBuffer allTranWork = new StringBuffer();
      // If the user has rights to view child organization work, retrieve the 
      // list of dropdown options from the query listview results doc (containing
      // an option for each child org) otherwise, simply use the user's org 
      // option to the dropdown list (in addition to the default 'My Work' option).
      if(!userSession.hasSavedUserSession()) {
        allTranWork.append("<option value=\"").
          append(EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey())).
          append("\" selected >");
        allTranWork.append(resMgr.getText( MY_WORK, TradePortalConstants.TEXT_BUNDLE));
        allTranWork.append("</option>");
      }
      if (SecurityAccess.hasRights(loginRights, 
                                   SecurityAccess.VIEW_CHILD_ORG_WORK)) {
        allTranWork.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                        "ORGANIZATION_OID", "NAME", "",
                                                        currentAllTranWork, userSession.getSecretKey()));
        // Only display the 'All Work' option if the user's org has child organizations
        if (totalOrganizations > 1) {
          allTranWork.append("<option value=\"");
          allTranWork.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
          allTranWork.append("\"");
          if (ALL_WORK.equals(currentAllTranWork)) {
            allTranWork.append(" selected");
          }
          allTranWork.append(">");
          allTranWork.append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
          allTranWork.append("</option>");
        }
      }
      else {
        allTranWork.append("<option value=\"");
        allTranWork.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
        allTranWork.append("\"");
        if (userOrgOid.equals(currentAllTranWork)) {
          allTranWork.append(" selected");
        }
        allTranWork.append(">");
        allTranWork.append(userSession.getOrganizationName());
        allTranWork.append("</option>");
      }

%>
        <%= widgetFactory.createSearchSelectField("AllTranWork","Home.AllTransactions.Show", "", 
              allTranWork.toString(), " class=\"char15\"" )%>
 
<%
      StringBuffer allTranGroups = new StringBuffer();
      allTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__ALL).
        append("'>").
        append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__ALL, loginLocale)).
        append("</option>");
      allTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__TRADE).
        append("'>").
        append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__TRADE, loginLocale)).
        append("</option>");
      allTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__PAYMENT).
        append("'>").
        append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__PAYMENT, loginLocale)).
        append("</option>");
      allTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT).
        append("'>").
        append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT, loginLocale)).
        append("</option>");
      allTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__RECEIVABLES).
        append("'>").
        append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__RECEIVABLES, loginLocale)).
        append("</option>");

%>
        <%= widgetFactory.createSearchSelectField( "AllTranGroup", "Home.AllTransactions.criteria.InstrumentGroup", "", 
              allTranGroups.toString(), " class=\"char10\"" )%>

<%
      StringBuffer allTranInstrTypes = new StringBuffer();
      allTranInstrTypes.append("<option value='").append(ALL).append("'").
        append(" selected >").
        append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      //popupate the transaction type drop down with all instrument types based on what the user can see
      Vector viewableInstruments = Dropdown.getViewableInstrumentTypes(formMgr, 
        new Long(userOrgOid), loginRights, userSession.getOwnershipLevel(), new Long(userOid), "AllTranInstrType");
      String viewableInstrTypes = Dropdown.getInstrumentList("",  //selected instr type
        loginLocale, viewableInstruments );
      allTranInstrTypes.append(viewableInstrTypes);
      //todo: all add to top
      //this is labelled tran type, but it is the set of instrument types
%>
        <%= widgetFactory.createSearchSelectField( "AllTranInstrType", "Home.AllTransactions.criteria.InstrumentType", "", 
              allTranInstrTypes.toString()) %>

<%
      StringBuffer allTranStatus = new StringBuffer();
      allTranStatus.append("<option value='").append(ALL).append("'").
        append(" selected >").
        append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZED, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_DELETED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_DELETED, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_STARTED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_STARTED, loginLocale)).
        append("</option>");
%>
        <%=widgetFactory.createSearchSelectField( "AllTranStatus", "Home.AllTransactions.criteria.Status", "",
              allTranStatus.toString(), " class=\"char15\"" )%>
      </div>

<%
      gridHtml = dGridFactory.createDataGrid("homeAllTranGrid","HomeAllTransactionsDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
    } //end if notifications section


    /************************************************************************/
    /*                           PANEL AUTHORISATION TRANSACTIONS SECTION                   */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS.equals(sectionId)) {
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
         <%=resMgr.getText("Home.PanelAuthTransactions", TradePortalConstants.TEXT_BUNDLE)%>
          <%=widgetFactory.createHoverHelp("panelAuthTranLink", "Home.AllTranLink") %>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="panelAuthTranShowCount5" class="<%= show5Class %>" >5</span>
            <span id="panelAuthTranShowCount10" class="<%= show10Class %>" >10</span>
            <span id="panelAuthTranShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homePanelAuthTranGrid_totalCount">0</span>
          </span>
          
        <%--<div style="clear:both;"></div>--%>
      </div>

      <div>
       <%    //build panelAuthtran options
      String currentpanelAuthTranWork = MY_WORK;
      String currentpanelAuthTranGroup = TradePortalConstants.INSTR_GROUP__ALL;
      String currentpanelAuthTranInstrType = ALL;
      String currentpanelAuthTranStatus = TradePortalConstants.STATUS_ALL;

      StringBuffer panelAuthTranWork = new StringBuffer();
      // If the user has rights to view child organization work, retrieve the
      // list of dropdown options from the query listview results doc (containing
      // an option for each child org) otherwise, simply use the user's org
      // option to the dropdown list (in addition to the default 'My Work' option).
// IR NIUN012952035
	 StringBuffer panelAuthdropdownOptions = new StringBuffer();
	 if(!userSession.hasSavedUserSession())
	     { 
	       panelAuthdropdownOptions.append("<option value=\"");
	       panelAuthdropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey()));
	       panelAuthdropdownOptions.append("\"selected>");
	       panelAuthdropdownOptions.append(resMgr.getText( currentpanelAuthTranWork, TradePortalConstants.TEXT_BUNDLE));
	       panelAuthdropdownOptions.append("</option>");
	     }

        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(loginRights, 
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
        	panelAuthdropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                               "ORGANIZATION_OID", "NAME", 
                                                               currentpanelAuthTranWork,userSession.getSecretKey()));
           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
        	   panelAuthdropdownOptions.append("<option value=\"");
        	   panelAuthdropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
        	   panelAuthdropdownOptions.append("\"selected>");
              panelAuthdropdownOptions.append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
              panelAuthdropdownOptions.append("</option>");
           }
        }
        else
        {   panelAuthdropdownOptions.append("<option value=\"");
        	panelAuthdropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
        	panelAuthdropdownOptions.append("\"selected>");
           panelAuthdropdownOptions.append(userSession.getOrganizationName());
           panelAuthdropdownOptions.append("</option>");
        }
	 //IR NIUN012952035

%>
        <%= widgetFactory.createSearchSelectField("PanelAuthTranWork","Home.PanelAuthTransactions.Show", "",
        		panelAuthdropdownOptions.toString(), " class=\"char15\"" )%>

<%
      StringBuffer panelAuthTranGroups = new StringBuffer();
      panelAuthTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__ALL).
        append("'>").
        append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__ALL, loginLocale)).
        append("</option>");
      panelAuthTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__TRADE).
        append("'>").
        append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__TRADE, loginLocale)).
        append("</option>");
      panelAuthTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__PAYMENT).
        append("'>").
        append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__PAYMENT, loginLocale)).
        append("</option>");
      panelAuthTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT).
        append("'>").
        append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT, loginLocale)).
        append("</option>");
      panelAuthTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__RECEIVABLES).
        append("'>").
        append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__RECEIVABLES, loginLocale)).
        append("</option>");

%>
        <%= widgetFactory.createSearchSelectField( "PanelAuthAllTranGroup", "Home.PanelAuthTransactions.criteria.InstrumentGroup", "",
              panelAuthTranGroups.toString(), " class=\"char10\"" )%>

<%


      StringBuffer panelAuthTranInstrTypes = new StringBuffer();
      panelAuthTranInstrTypes.append("<option value='").append(ALL).append("'").
        append(" selected >").
        append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      //popupate the transaction type drop down with all instrument types based on what the user can see
      Vector viewableInstruments = Dropdown.getViewableInstrumentTypes(formMgr,
        new Long(userOrgOid), loginRights, userSession.getOwnershipLevel(), new Long(userOid));
      String viewableInstrTypes = Dropdown.getInstrumentList("",  //selected instr type
        loginLocale, viewableInstruments );
      panelAuthTranInstrTypes.append(viewableInstrTypes);
      //todo: all add to top
      //this is labelled tran type, but it is the set of instrument types
%>
        <%= widgetFactory.createSearchSelectField( "PanelAuthTranInstrType", "Home.PanelAuthTransactions.criteria.InstrumentType", "",
              panelAuthTranInstrTypes.toString()) %>

<%
      //cquinton 12/7/2012 per bank design call, the default for home all transactions should be 'Started'
      StringBuffer panelAuthTranStatus = new StringBuffer();
      panelAuthTranStatus.append("<option value='").append(ALL).append("'").
        append(">").
        append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED, loginLocale)).
        append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZED, loginLocale)).
        append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK, loginLocale)).
        append("</option>");
      
      //Sandeep 02/28/2013 Comment -Start
      //Removing Delete option from Status dropdown box. This Code change is Per Kim/Clay as it is a BMO Issue
      //panelAuthTranStatus.append("<option value='").
        //append(TradePortalConstants.TRANS_STATUS_DELETED).
        //append("'>").
        //append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_DELETED, loginLocale)).
        //append("</option>");
      //Sandeep 02/28/2013 Comment -End
      
      panelAuthTranStatus.append("<option value='").
      append(TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT).
      append("'>").
      append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT, loginLocale)).
      append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK, loginLocale)).
        append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED, loginLocale)).
        append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE, loginLocale)).
        append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK, loginLocale)).
        append("</option>");
     
     panelAuthTranStatus.append("<option value='").
       append(TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT).
       append("'>").
       append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT, loginLocale)).
       append("</option>");
     panelAuthTranStatus.append("<option value='").
     	append(TradePortalConstants.TRANS_STATUS_VERIFIED).
     	append("'>").
     	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_VERIFIED, loginLocale)).
     	append("</option>");
     panelAuthTranStatus.append("<option value='").
     	append(TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX).
     	append("'>").
     	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX, loginLocale)).
     	append("</option>");
     panelAuthTranStatus.append("<option value='").
     	append(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED).
     	append("'>").
     	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED, loginLocale)).
     	append("</option>");
     panelAuthTranStatus.append("<option value='").
     	append(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE).
     	append("'>").
     	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE, loginLocale)).
    	append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_STARTED).
        append("' selected >").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_STARTED, loginLocale)).
        append("</option>");
%>
        <%=widgetFactory.createSearchSelectField( "PanelAuthTranStatus", "Home.PanelAuthTransactions.criteria.Status", "",
              panelAuthTranStatus.toString(), " class=\"char15\"" )%>
      </div>

<%
      gridHtml = dGridFactory.createDataGrid("homePanelAuthTranGrid","HomePanelAuthTransactionsDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
      
    } //end if PanelAuth section
    /************************************************************************/
    /*                  RECEIVABLES MATCH NOTICES SECTION                   */
    /************************************************************************/
    if ( TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES.equals(sectionId) ) {
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
          <%=resMgr.getText("Home.ReceivablesMatching", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%  
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) { 
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="rcvTranShowCount5" class="<%= show5Class %>" >5</span>
            <span id="rcvTranShowCount10" class="<%= show10Class %>" >10</span>
            <span id="rcvTranShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeRcvTranGrid_totalCount">0</span>
          </span>
          <span class="gridHeaderEdit"></span>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>


      <div>
<%    //build receivables match options
      String currentRcvMatchWork = MY_WORK;
      String currentRcvMatchStatus = TradePortalConstants.STATUS_ALL;
      String currentRcvMatchPaySrc = ALL;

      StringBuffer rcvMatchWork = new StringBuffer();
      // If the user has rights to view child organization work, retrieve the 
      // list of dropdown options from the query listview results doc (containing
      // an option for each child org) otherwise, simply use the user's org 
      // option to the dropdown list (in addition to the default 'My Work' option).
      if (!userSession.hasSavedUserSession()) {
        rcvMatchWork.append("<option value=\"");
        rcvMatchWork.append(EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey()));
        rcvMatchWork.append("\"");
        rcvMatchWork.append(" selected");
        rcvMatchWork.append(">");
        rcvMatchWork.append(resMgr.getText(MY_WORK, TradePortalConstants.TEXT_BUNDLE));
        rcvMatchWork.append("</option>");
      }
      if (SecurityAccess.hasRights(loginRights, 
                                   SecurityAccess.VIEW_CHILD_ORG_RECEIVABLES)) {
        rcvMatchWork.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                   "ORGANIZATION_OID", "NAME", 
                                                   "", "", userSession.getSecretKey()));
        // Only display the 'All Work' option if the user's org has child organizations
        if (totalOrganizations > 1) {
          rcvMatchWork.append("<option value=\"");
          rcvMatchWork.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
          rcvMatchWork.append("\"");
          if (currentRcvMatchWork.equals(ALL_WORK)) {
            rcvMatchWork.append(" selected");
          }
          rcvMatchWork.append(">");
          rcvMatchWork.append(resMgr.getText(ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
          rcvMatchWork.append("</option>");
        }
      }
      else
      {
        rcvMatchWork.append("<option value=\"");
        rcvMatchWork.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
        rcvMatchWork.append("\"");
        if (currentRcvMatchWork.equals(userOrgOid)) {
          rcvMatchWork.append(" selected");
        }
        rcvMatchWork.append(">");
        rcvMatchWork.append(userSession.getOrganizationName());
        rcvMatchWork.append("</option>");
      }
%>
        <%=widgetFactory.createSearchSelectField( "RcvMatchWork", "Home.ReceivablesMatching.Show", "",
              rcvMatchWork.toString(), " class=\"char15\"" )%>

<%
      StringBuffer rcvMatchStatus = new StringBuffer();
      // Build the dropdown options (hardcoded in ALL, STARTED, READY, PARTIALLY_AUTH, AUTH_FAILED), re-selecting the
      // most recently selected option.
      rcvMatchStatus.append("<option value='");
      rcvMatchStatus.append(TradePortalConstants.STATUS_ALL);
      rcvMatchStatus.append("' ");
      if (TradePortalConstants.STATUS_ALL.equals(currentRcvMatchStatus)) {
        rcvMatchStatus.append("selected");
      }
      rcvMatchStatus.append(">");
      rcvMatchStatus.append(resMgr.getText("common.StatusAll", TradePortalConstants.TEXT_BUNDLE));
      rcvMatchStatus.append("</option>");
           
      rcvMatchStatus.append("<option value='");
      rcvMatchStatus.append(TradePortalConstants.STATUS_PAID);
      rcvMatchStatus.append("' ");
      if (TradePortalConstants.STATUS_PAID.equals(currentRcvMatchStatus)) {
        rcvMatchStatus.append("selected");
      }
      rcvMatchStatus.append(">");
      rcvMatchStatus.append(resMgr.getText("common.StatusPaid", TradePortalConstants.TEXT_BUNDLE));
      rcvMatchStatus.append("</option>");

      rcvMatchStatus.append("<option value='");
      rcvMatchStatus.append(TradePortalConstants.STATUS_PENDING);
      rcvMatchStatus.append("' ");
      if (TradePortalConstants.STATUS_PENDING.equals(currentRcvMatchStatus)) {
        rcvMatchStatus.append("selected");
      }
      rcvMatchStatus.append(">");
      rcvMatchStatus.append(resMgr.getText("common.StatusPending", TradePortalConstants.TEXT_BUNDLE));
      rcvMatchStatus.append("</option>");
%>
        <%=widgetFactory.createSearchSelectField( "RcvMatchStatus", "Home.ReceivablesMatching.criteria.Status", "",
              rcvMatchStatus.toString() )%>
 
<%
      StringBuffer rcvMatchPaySrc = new StringBuffer();
      rcvMatchPaySrc.append("<option value='");
      rcvMatchPaySrc.append(ALL);
      rcvMatchPaySrc.append("' ");
      if (ALL.equals(currentRcvMatchPaySrc)) {
        rcvMatchPaySrc.append("selected");
      }
      rcvMatchPaySrc.append(">");
      rcvMatchPaySrc.append(resMgr.getText(ALL, TradePortalConstants.TEXT_BUNDLE));
      rcvMatchPaySrc.append("</option>");
      String[] paymentSourceValues = {TradePortalConstants.PAYMENT_SOURCE_TYPE_BPY, 
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_CCD, 
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_DIR, 
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_HPA, 
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_LBX, 
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_RTG, 
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_SMT, 
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_SWF };
      for (int i = 0; i < paymentSourceValues.length; i++) {
        rcvMatchPaySrc.append("<option value='");
        rcvMatchPaySrc.append(paymentSourceValues[i]);
        rcvMatchPaySrc.append("' ");
        if (paymentSourceValues[i].equals(currentRcvMatchPaySrc)) {
          rcvMatchPaySrc.append("selected");
        }
        rcvMatchPaySrc.append(">");
        rcvMatchPaySrc.append(ReferenceDataManager.getRefDataMgr().getDescr(
                              TradePortalConstants.PAYMENT_SOURCE_TYPE, 
                              paymentSourceValues[i], loginLocale));
        rcvMatchPaySrc.append("</option>");
      }           
%>
        <%=widgetFactory.createSearchSelectField( "RcvMatchPaySrc", "Home.ReceivablesMatching.criteria.PaymentSource", "",
              rcvMatchPaySrc.toString() )%>
      </div> 
<%
      gridHtml = dGridFactory.createDataGrid("homeRcvTranGrid","HomeReceivablesMatchingDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
    } //end if receivables match notices section


    /************************************************************************/
    /*                           ACCOUNT BALANCE SECTION                    */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES.equals(sectionId) ) {
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
          <%=resMgr.getText("Home.AccountBalances", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%  
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) { 
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="acctShowCount5" class="<%= show5Class %>" >5</span>
            <span id="acctShowCount10" class="<%= show10Class %>" >10</span>
            <span id="acctShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeAcctGrid_totalCount">0</span>
          </span>
            <span id="homeAcctGridEdit" class="gridHeaderEdit"></span>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>

<%
      gridHtml = dGridFactory.createDataGrid("homeAcctGrid","HomeAccountDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
    } //end if account balances section


    /************************************************************************/
    /*                           ANNOUNCEMENTS SECTION                      */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__ANNOUNCEMENTS.equals(sectionId) &&
         ( TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) ||
           TradePortalConstants.OWNER_BOG.equals(userSession.getOwnershipLevel()) ) ) {
%>

    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
          <%=resMgr.getText("Home.Announcements", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) { 
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="announceShowCount5" class="<%= show5Class %>" >5</span>
            <span id="announceShowCount10" class="<%= show10Class %>" >10</span>
            <span id="announceShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeAnnounceGrid_totalCount">0</span>
          </span>
          <span class="gridHeaderEdit"></span>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>

<%
    gridHtml = dGridFactory.createDataGrid("homeAnnounceGrid","HomeAnnouncementsDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
    } //end if announcements section

    /************************************************************************/
    /*                          LOCKED OUT USERS SECTION                    */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__LOCKED_OUT_USERS.equals(sectionId) ) {
      boolean maintainAccess = SecurityAccess.hasRights(loginRights,
                                 SecurityAccess.MAINTAIN_LOCKED_OUT_USERS);
%>
    <%--do not include the form--%>

      <div class="formContentNoSidebar">
        <div class="gridHeader">
          <span class="gridHeaderTitle">
            <%=resMgr.getText("Home.LockedOutUsers", TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <span class="gridHeader-right">
            <span class="gridHeaderShowCounts">
              <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%  
        String show5Class = "gridHeaderShowCountItem";
        String show10Class = "gridHeaderShowCountItem";
        String show20Class = "gridHeaderShowCountItem";
        if ( gridShowCount == 5 ) { 
          show5Class += " selected";
        }
        else if ( gridShowCount == 10 ) {
          show10Class += " selected";
        }
        else if ( gridShowCount == 10 ) {
          show20Class += " selected";
        }
%>
              <span id="lockedUserShowCount5" class="<%= show5Class %>" >5</span>
              <span id="lockedUserShowCount10" class="<%= show10Class %>" >10</span>
              <span id="lockedUserShowCount20" class="<%= show20Class %>" >20</span>
              <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
              <span id="homeLockedUserGrid_totalCount">0</span>
            </span>
            <span class="gridHeaderEdit"></span>
          </span>
          <%--<div style="clear:both;"></div>--%>
        </div>

<%
        if ( maintainAccess ) {
          gridHtml = dGridFactory.createDataGrid("homeLockedUserGrid","HomeLockedUsersSelectDataGrid",null);
        }
        else {
          gridHtml = dGridFactory.createDataGrid("homeLockedUserGrid","HomeLockedUsersDataGrid",null);
        }
%>
      <%= gridHtml %>
      </div>
    </form>
<%
    } //end if locked out users section

  } //end section loop
%>

</div>

<%--ctq portal refresh end --%>


<script>

  <%--setup a named variable for placing shared callback functions--%>
  var thisPage = {};

  <%--create grids--%>
  require(["t360/OnDemandGrid", "dijit/registry", "t360/datagrid", "dojo/dom-construct", "dojo/domReady!"],
      function( onDemandGrid, registry, t360grid, domConstruct ) {
    var gridLayout;
    var initSearchParms;
    var selectedStatus;
    selectedStatus= '<%=TradePortalConstants.STATUS_ALL%>';
<%
  //loop through section again, generating necessary javascript
  sectionId = "";
  for( int sectionIdx = 0; sectionIdx < sections.size(); sectionIdx++ ) {
    sectionId = sections.get(sectionIdx);
    
    if ( TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED.equals(sectionId) &&
            condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_OFFERS ) ){
          String homeInvoicesOfferedGridLayout = dGridFactory.createGridLayout(
            "homeInvoicesOfferedGrid","HomeInvoicesOfferedDataGrid");
    %>
        gridLayout = <%= homeInvoicesOfferedGridLayout %>;
        initSearchParms = "selectedStatus="+selectedStatus;
        var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeInvoicesOfferedDataView",userSession.getSecretKey())%>"; 
        var homeInvoicesOfferedGrid = onDemandGrid.createOnDemandGrid("homeInvoicesOfferedGrid", viewName,
          gridLayout, initSearchParms, '-Currency');
        homeInvoicesOfferedGrid.set('autoHeight',5); 

          
    <%
    }
    if ( TradePortalConstants.DASHBOARD_SECTION__MAIL.equals(sectionId) &&
         SecurityAccess.hasRights(loginRights,SecurityAccess.ACCESS_MESSAGES_AREA) ){
      String mailGridLayout = dGridFactory.createGridLayout(
        "homeMailGrid","HomeMailMessagesDataGrid");
%>
    gridLayout = <%= mailGridLayout %>;
    initSearchParms = "org=<%= EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey()) %>"; <%-- hardcode it --%>
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeMailMessagesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var homeMailGrid = onDemandGrid.createOnDemandGrid("homeMailGrid", viewName,
      gridLayout, initSearchParms, '-DateAndTime');
    homeMailGrid.set('autoHeight',5); 

    <%-- add searchMail to thisPage --%>
    thisPage.searchMail = function() {
      var searchParms = "";
      var mailOrgSelect = registry.byId("MailOrg");
      var mailOrgOid = mailOrgSelect.get('value');
      if ( mailOrgOid && mailOrgOid.length > 0 ) {
        searchParms += "org="+mailOrgOid;
      }
      onDemandGrid.searchDataGrid("homeMailGrid", "HomeMailMessagesDataView", searchParms);
    }
<%
    } 

    if ( TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS.equals(sectionId) &&
         SecurityAccess.hasRights(loginRights,SecurityAccess.ACCESS_MESSAGES_AREA) ) {
      String notificationsGridLayout = dGridFactory.createGridLayout(
        "homeNotifGrid","HomeNotificationsDataGrid");
%>
    gridLayout = <%= notificationsGridLayout %>;
    initSearchParms = "org=<%= EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()) %>"; <%-- hardcode it --%>
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeNotificationsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var homeNotifGrid = onDemandGrid.createOnDemandGrid("homeNotifGrid", viewName,
      gridLayout, initSearchParms, '-DateAndTime');
    homeNotifGrid.set('autoHeight',5); <%-- default --%>

    <%-- add searchNotif to thisPage --%>
    thisPage.searchNotif = function() {
      var searchParms = "";
      var notifOrgSelect = registry.byId("NotifOrg");
      var notifOrgOid = notifOrgSelect.get('value');
      if ( notifOrgOid && notifOrgOid.length > 0 ) {
        searchParms += "org="+notifOrgOid;
      }
      var notifStatusSelect = registry.byId("NotifStatus");
      var notifStatus = notifStatusSelect.get('value');
      if ( notifStatus && notifStatus.length > 0 ) {
        if ( searchParms && searchParms.length > 0 ) {
          searchParms += "&";
        }
        searchParms += "status="+notifStatus;
      }
      onDemandGrid.searchDataGrid("homeNotifGrid", "HomeNotificationsDataView", searchParms);
    }
<%
    } 
    
 

    if ( TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS.equals(sectionId) &&
         ( (org.allowSomeTradeTransaction() &&  
            SecurityAccess.canViewTradeInstrument(loginRights)) ||
           (org.allowSomePaymentTransaction() && 
            SecurityAccess.canViewCashManagement(loginRights)) ) ) {
      String allTranGridLayout = dGridFactory.createGridLayout(
        "homeAllTranGrid", "HomeAllTransactionsDataGrid");
%>
    gridLayout = <%= allTranGridLayout %>;
    initSearchParms = "work=<%= EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()) %>"; <%-- hardcode it --%>
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("AllTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var homeAllTranGrid = onDemandGrid.createOnDemandGrid("homeAllTranGrid", viewName,
      gridLayout, initSearchParms, '0');
    homeAllTranGrid.set('autoHeight',5); <%-- default --%>

    <%-- add searchAllTran to thisPage --%>
    thisPage.searchAllTran = function() {
      var searchParms = "";
      var allTranWorkSelect = registry.byId("AllTranWork");
      var allTranWorkOid = allTranWorkSelect.get('value');
      if ( allTranWorkOid && allTranWorkOid.length > 0 ) {
        searchParms += "work="+allTranWorkOid;
      }
      var allTranGroupSelect = registry.byId("AllTranGroup");
      var allTranGroup = allTranGroupSelect.get('value');
      if ( allTranGroup && allTranGroup.length > 0 ) {
        if ( searchParms && searchParms.length > 0 ) {
          searchParms += "&";
        }
        searchParms += "grp="+allTranGroup;
      }
      var allTranInstrTypeSelect = registry.byId("AllTranInstrType");
      var allTranInstrType = allTranInstrTypeSelect.get('value');
      if ( allTranInstrType && allTranInstrType.length > 0 ) {
        if ( searchParms && searchParms.length > 0 ) {
          searchParms += "&";
        }
        searchParms += "iType="+allTranInstrType;
      }
      var allTranStatusSelect = registry.byId("AllTranStatus");
      var allTranStatus = allTranStatusSelect.get('value');
      if ( allTranStatus && allTranStatus.length > 0 ) {
        if ( searchParms && searchParms.length > 0 ) {
          searchParms += "&";
        }
        searchParms += "status="+allTranStatus;
      }
      onDemandGrid.searchDataGrid("homeAllTranGrid", "AllTransactionsDataView", searchParms);
    }
<%
    } 
  //CR 821 Rel 8.3 START
    if ( TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS.equals(sectionId) &&
            ( (org.allowSomeTradeTransaction() &&
               SecurityAccess.canViewTradeInstrument(loginRights)) ||
              (org.allowSomePaymentTransaction() &&
               SecurityAccess.canViewCashManagement(loginRights)) ) ) {
         String panelAuthTranGridLayout = dGridFactory.createGridLayout(
           "homePanelAuthTranGrid", "HomePanelAuthTransactionsDataGrid");
         //cquinton 12/7/2012 default all transactions search should be on started status
   %>
       gridLayout = <%= panelAuthTranGridLayout %>;
       <%-- BMO Issue#78 Begins.(Pavani) All_Work was hardcoded previously, which is removed and the workflow from the session is being set --%>
       initSearchParms = "work=<%=EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey())%>";
     	       
       <%-- reuse the alltransactions dataview --%>
       var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PanelAuthTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
       var homePanelAuthTranGrid = onDemandGrid.createOnDemandGrid("homePanelAuthTranGrid", viewName,
         gridLayout, initSearchParms, '0');
       homePanelAuthTranGrid.set('autoHeight',5); <%-- default --%>
       

       <%-- add searchPanelAuthTran to thisPage --%>
       thisPage.searchPanelAuthTran = function() {
         var searchParms = "";
         var panelAuthTranWorkSelect = registry.byId("PanelAuthTranWork");
         var panelAuthTranWorkOid = panelAuthTranWorkSelect.get('value');
         if ( panelAuthTranWorkOid && panelAuthTranWorkOid.length > 0 ) {
           searchParms += "work="+panelAuthTranWorkOid;
         }
         var panelAuthTranGroupSelect = registry.byId("PanelAuthTranGroup");
         var panelAuthTranGroup = panelAuthTranGroupSelect.get('value');
         if ( panelAuthTranGroup && panelAuthTranGroup.length > 0 ) {
           if ( searchParms && searchParms.length > 0 ) {
             searchParms += "&";
           }
           searchParms += "grp="+panelAuthTranGroup;
         }
         var panelAuthTranInstrTypeSelect = registry.byId("PanelAuthTranInstrType");
         var panelAuthTranInstrType = panelAuthTranInstrTypeSelect.get('value');
         if ( panelAuthTranInstrType && panelAuthTranInstrType.length > 0 ) {
           if ( searchParms && searchParms.length > 0 ) {
             searchParms += "&";
           }
           searchParms += "iType="+panelAuthTranInstrType;
         }
         var panelAuthTranStatusSelect = registry.byId("PanelAuthTranStatus");
         var panelAuthTranStatus = panelAuthTranStatusSelect.get('value');
         if ( panelAuthTranStatus && panelAuthTranStatus.length > 0 ) {
           if ( searchParms && searchParms.length > 0 ) {
             searchParms += "&";
           }
           searchParms += "status="+panelAuthTranStatus;
         }
         
         
         <%-- reuse the alltransactions dataview --%>
         onDemandGrid.searchDataGrid("homePanelAuthTranGrid", "PanelAuthTransactionsDataView", searchParms);
       }
   <%
       }
  //CR 821 Rel 8.3 END

    if ( TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES.equals(sectionId) &&
         ( org.allowSomeReceivablesTransaction() &&  
           SecurityAccess.hasRights(loginRights,SecurityAccess.ACCESS_RECEIVABLES_AREA)) ) {
      String rcvTranGridLayout = dGridFactory.createGridLayout(
        "homeRcvTranGrid", "HomeReceivablesMatchingDataGrid");
%>
    gridLayout = <%= rcvTranGridLayout %>;
    initSearchParms = "work=<%= EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()) %>"; <%-- hardcode it --%>
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeReceivablesMatchingDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var homeRcvTranGrid = onDemandGrid.createOnDemandGrid("homeRcvTranGrid", viewName,
      gridLayout, initSearchParms, '0');
    homeRcvTranGrid.set('autoHeight',5); <%-- default --%>

    <%-- add searchRcvMatch to thisPage --%>
    thisPage.searchRcvMatch = function() {
      var searchParms = "";
      var rcvMatchWorkSelect = registry.byId("RcvMatchWork");
      var rcvMatchWorkOid = rcvMatchWorkSelect.get('value');
      if ( rcvMatchWorkOid && rcvMatchWorkOid.length > 0 ) {
        searchParms += "work="+rcvMatchWorkOid;
      }
      var rcvMatchStatusSelect = registry.byId("RcvMatchStatus");
      var rcvMatchStatus = rcvMatchStatusSelect.get('value');
      if ( rcvMatchStatus && rcvMatchStatus.length > 0 ) {
        if ( searchParms && searchParms.length > 0 ) {
          searchParms += "&";
        }
        searchParms += "status="+rcvMatchStatus;
      }
      var rcvMatchPaySrcSelect = registry.byId("RcvMatchPaySrc");
      var rcvMatchPaySrc = rcvMatchPaySrcSelect.get('value');
      if ( rcvMatchPaySrc && rcvMatchPaySrc.length > 0 ) {
        if ( searchParms && searchParms.length > 0 ) {
          searchParms += "&";
        }
        searchParms += "pSrc="+rcvMatchPaySrc;
      }
      onDemandGrid.searchDataGrid("homeRcvTranGrid", "HomeReceivablesMatchingDataView", searchParms);
    }
<%
    } 

    if ( TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES.equals(sectionId) ) {
      String acctGridLayout = null;
      String acctGridDataView = null;
      if (userSession.hasSavedUserSession()) {
        acctGridLayout = dGridFactory.createGridLayout(
          "homeAcctGrid", "HomeCustAccountDataGrid");
        //reuse the account dataview
        acctGridDataView = "CustAccountDataView";
      }
      else {
    	  acctGridLayout = dGridFactory.createGridLayout(
          "homeAcctGrid", "HomeAccountDataGrid");
        //reuse the account dataview
        acctGridDataView = "AccountDataView";
      }

%>
    gridLayout = <%= acctGridLayout %>;

    initSearchParms = "";
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("CustAccountDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var homeAcctGrid = onDemandGrid.createOnDemandGrid("homeAcctGrid", viewName,
      gridLayout, initSearchParms, '0');
    homeAcctGrid.set('autoHeight',5); <%-- default --%>

    function refreshAccts() {
    	 require(["t360/OnDemandGrid"], function(onDemandGridSearch){
      var searchParms = "userOrgOid=<%=userOrgOid%>";
      onDemandGridSearch.searchDataGrid("homeAcctGrid", "CustAccountDataView", searchParms); 
    	 });
    }
<%
    } 
 %>
 <%//jgadela R92 IR T36000037116 - DASHBOARD_SECTION_DEBIT_FUNDING_MAIL - START11
 if ( TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL.equals(sectionId) ) {
 	String debFundMailGridLayout = dGridFactory.createGridLayout("homeDebitFundMailGrid","HomeDebitFundingMailMsgDataGrid");
%>   
  var debFundMailGridLayout = <%= debFundMailGridLayout %>;
  var debFundInitSearchParms = "";
 
  var org = ""; 
  if("" == org || null == org || "undefined" == org)
 	org='<%= EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey()) %>';
  else if(registry.byId("DebitFundMailOrg")){
 	org = registry.byId("DebitFundMailOrg").attr('value',org, false);
  }
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeDebitFundingMailMsgDataView",userSession.getSecretKey())%>"; <%--Rel9.2 IR T36000032596 --%>
  debFundInitSearchParms = "org="+org; 
  
  var homeDebFundMailGrid = onDemandGrid.createOnDemandGrid("homeDebitFundMailGrid", viewName,debFundMailGridLayout, debFundInitSearchParms, '-DateAndTime');
<%
}
//jgadela R92 IR T36000037116 - DASHBOARD_SECTION_DEBIT_FUNDING_MAIL - END
%>


 <%   if ( TradePortalConstants.DASHBOARD_SECTION__ANNOUNCEMENTS.equals(sectionId) ) {
      String announceGridLayout = dGridFactory.createGridLayout(
        "homeAnnounceGrid", "HomeAnnouncementsDataGrid");
%>
    gridLayout = <%= announceGridLayout %>;
    initSearchParms = "userOrgOid=<%=userOrgOid%>";
    <%-- cquinton 9/10/2012 Rel portal refresh - add default sort on start date descending --%>
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeAnnouncementsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var homeAnnounceGrid = onDemandGrid.createOnDemandGrid("homeAnnounceGrid", viewName,
      gridLayout, initSearchParms, '-StartDate',
      <%--cquinton 9/20/2012 ir#4158 pass in special not found text--%>
      '<%= resMgr.getText("Home.Announcements.NoActiveAnnouncements", TradePortalConstants.TEXT_BUNDLE) %>');
    homeAnnounceGrid.set('autoHeight',5); <%-- default --%>

    function refreshAnnouncements() {
    	 require(["t360/OnDemandGrid"], function(onDemandGridSearch){
		      var searchParms = "userOrgOid=<%=userOrgOid%>";
		      onDemandGridSearch.searchDataGrid("homeAnnounceGrid", "HomeAnnouncementsDataView", searchParms);
    	 });
    }
<%
    } 

    if ( TradePortalConstants.DASHBOARD_SECTION__LOCKED_OUT_USERS.equals(sectionId) ) {
      boolean viewAccess = SecurityAccess.hasRights(loginRights,
                             SecurityAccess.VIEW_OR_MAINTAIN_LOCKED_OUT_USERS);
      boolean maintainAccess = SecurityAccess.hasRights(loginRights,
                                 SecurityAccess.MAINTAIN_LOCKED_OUT_USERS);
      if ( viewAccess || maintainAccess ) {
        String lockedUserGridLayout = null;
        if (maintainAccess) {
          lockedUserGridLayout = dGridFactory.createGridLayout(
            "homeLockedUserGrid", "HomeLockedUsersSelectDataGrid");
        }
        else {
          lockedUserGridLayout = dGridFactory.createGridLayout(
            "homeLockedUserGrid", "HomeLockedUsersDataGrid");
        }
%>
    gridLayout = <%= lockedUserGridLayout %>;
    initSearchParms = "userOrgOid=<%=userOrgOid%>";
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeLockedUsersDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var lockGrid = onDemandGrid.createOnDemandGrid("homeLockedUserGrid", viewName,
      gridLayout, initSearchParms, '0');
    lockGrid.set('autoHeight',5); <%-- default --%>
    function refreshLockedUsers() {
    	require(["t360/onDemandGrid"], function(onDemandGridSearch){
    		var searchParms = "userOrgOid=<%=userOrgOid%>";
    		onDemandGridSearch.searchDataGrid("homeLockedUserGrid", "HomeLockedUsersDataView", searchParms);
    	});
      
    }
<%
      } 
    } 
  } //end for loop
%>

  
  });
  
  require(["t360/OnDemandGrid", "dijit/registry", "dojo/query", "dojo/on", "dojo/dom-class", "t360/dialog",
           "dojo/dom", "dijit/layout/ContentPane", "dijit/TooltipDialog", "dijit/popup",
           "t360/common", "t360/popup", "t360/datagrid","dgrid/extensions/ColumnResizer", 
           "dojo/aspect", "dojo/dom-style", "dojo/ready", "dojo/domReady!"],
      function(onDemandGrid, registry, query, on, domClass, dialog,
               dom, ContentPane, TooltipDialog, popup,
               common, t360popup, t360grid, ColumnResizer, aspect,domStyle,
               ready ) {
	  query('#homeAcctGridEdit').on("click", function() {
		    var columns = onDemandGrid.getColumnsForCustomization('homeAcctGrid');
		    var parmNames = [ "gridId", "gridName", "columns" ];
		    var parmVals = [ "homeAcctGrid", "HomeCustAccountDataGrid", columns ];
		    t360popup.open('homeAcctGridEdit', 'dGridCustomizationPopup.jsp',
		      parmNames, parmVals,
		      null, null);  <%-- callbacks --%>
		  });
	if (dom.byId('homeMailGrid')){
		    on(dom.byId('homeMailGrid'), 'dgrid-refresh-complete', function (event) {
		    	setDynamicGridHeitht(null, 'homeMailGrid');
		    });
	    }
	if (dom.byId('homeNotifGrid')){
	    on(dom.byId('homeNotifGrid'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'homeNotifGrid');
	    });
    }
	if (dom.byId('homeDebitFundMailGrid')){
	    on(dom.byId('homeDebitFundMailGrid'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'homeDebitFundMailGrid');
	    });
    }
	if (dom.byId('homeInvoicesOfferedGrid')){
	    on(dom.byId('homeInvoicesOfferedGrid'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'homeInvoicesOfferedGrid');
	    });
    }
	if (dom.byId('homeAllTranGrid')){
	    on(dom.byId('homeAllTranGrid'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'homeAllTranGrid');
	    });
    }
	if (dom.byId('homePanelAuthTranGrid')){
	    on(dom.byId('homePanelAuthTranGrid'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'homePanelAuthTranGrid');
	    });
    }
	if (dom.byId('homeRcvTranGrid')){
	    on(dom.byId('homeRcvTranGrid'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'homeRcvTranGrid');
	    });
    }
	if (dom.byId('homeAcctGrid')){
	    on(dom.byId('homeAcctGrid'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'homeAcctGrid');
	    });
    }
	if (dom.byId('homeAnnounceGrid')){
	    on(dom.byId('homeAnnounceGrid'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'homeAnnounceGrid');
	    });
    }
	if (dom.byId('homeLockedUserGrid')){
	    on(dom.byId('homeLockedUserGrid'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'homeLockedUserGrid');
	    });
    }
	  function setDynamicGridHeitht(event, gridId){
			var myGrid = registry.byId(gridId);
			var rowCount = parseInt("0", 10);
			if (event == null){
				rowCount = parseInt((dojo.query(".selected"))[0].innerHTML,10);
			}else{
			 	rowCount = parseInt(event.target.innerHTML, 10);
			}
			<%--  Calculate the height of the scrollbar --%>
			<%-- var dgridScrollerNode = query('.dgrid-scroller', myGrid.domNode)[0]; --%>
			  var dgridHeaderNode = myGrid.headerNode;
			  
		    var dgridScrollerNode = myGrid.bodyNode;
		    var scrollBarHeight = dgridScrollerNode.offsetHeight - dgridScrollerNode.clientHeight;
		    
			<%-- Include column header height with the grid height --%>
			var maxHeight = (myGrid.rowHeight * (rowCount)) + (myGrid.headerNode.offsetHeight+scrollBarHeight) + 'px';
			var scrollMaxHeight = (myGrid.rowHeight * (rowCount)) + scrollBarHeight + 'px';
			
			domStyle.set(myGrid.domNode, 'maxHeight', maxHeight);
			<%-- domStyle.set(dgridScrollerNode, 'maxHeight', scrollMaxHeight); --%>
			domStyle.set(dgridScrollerNode, 'height', scrollMaxHeight);
			var recCount = myGrid._total;
			
			<%--  below code ensures grid does not show blanks rows when records count is less  --%>
			<%--  then the selected row count. e.g. when row count is 10 and record count is 8 the we --%>
			<%--  show only 8 rows. --%>
			if (recCount < rowCount){
			    var dgridContentNode = myGrid.contentNode;

			    <%-- Adding this code to to prevent IE8 crash --%>
			    if(navigator.appVersion.indexOf("MSIE 8")==-1){
			    	dgridScrollerNode.style.height = '';
			    }
			    <%-- dgridScrollerNode.style.height = ''; commented as it cause horizontal scroll in IE8 to disappear. --%>
			    dgridContentNode.style.height = 'auto';
			    if (dgridContentNode.offsetHeight < dgridScrollerNode.offsetHeight) {
			    	var dgScrlNodeOffsetHght = dgridContentNode.offsetHeight;
					dgridScrollerNode.style.height = dgScrlNodeOffsetHght + 'px';
					if (recCount == 0){
						dgridScrollerNode.style.height = '';
						dgridScrollerNode.style.height = (dgScrlNodeOffsetHght+15) + 'px';
					}else if (recCount <= rowCount){
						if (dgridScrollerNode.scrollWidth > dgridScrollerNode.offsetWidth){
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
						} else{
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight) + 'px';
			    		}
			    	}
			    }else{
			    	if (recCount < rowCount){
						if (dgridScrollerNode.scrollWidth > dgridScrollerNode.offsetWidth){
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
						} else{
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight) + 'px';
			    		}

			    	}
			    }
			    dgridContentNode.style.height = '';
		    }
		} <%-- end of setgrid --%>
  });
  
  var viewAuthorisationsFormatter=function(columnValues) {
		var title = "<%=StringFunction.escapeQuotesforJS(resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE))%>"+columnValues[1];
		var gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"homePanelAuthTranGrid\",\"TradeCash\");'>"+"<%=StringFunction.escapeQuotesforJS(resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisations", TradePortalConstants.TEXT_BUNDLE))%>"+"</a>";
	    return gridLink;
	}
</script>
