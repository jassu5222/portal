<%--
*******************************************************************************
  Delete All confirmation dialog

  Description:
  	This PopUp will open when user clicks on Other Conditions/ Other Details/ Instructions to Bank
   	On Click of save the call JSP will be submitted
*******************************************************************************
--%>

<%--
 *		Dev Sandeep
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>


<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%


  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String notificationsCount = StringFunction.xssCharsToHtml(request.getParameter("notificationsCount"));
  // IR T36000048558 Rel9.5 05/09/2016
  String msg = StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("Mail.NotificationDeleteAll",TradePortalConstants.TEXT_BUNDLE));
%>

<div class="dialogContent">
  <%= widgetFactory.createLabel("DeleteText", msg, true, false, false, "" ) %>

  <span class="formItem">
    <button data-dojo-type="dijit.form.Button" name="DeleteAll" id="DeleteAll" type="button" data-dojo-props="">
      <%=resMgr.getText("Notifications.DeleteAll", TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        deleteAllDialog.deleteAll();
      </script>
    </button>
    <%=widgetFactory.createHoverHelp("DeleteAll","NotificationsDeleteAll") %>
    <button data-dojo-type="dijit.form.Button"  name="Cancel" id="Cancel" type="button" data-dojo-props="" >
      <%=resMgr.getText("common.cancel", TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        deleteAllDialog.deleteAllConfirmationDialogClose();
      </script>
    </button>
    <%=widgetFactory.createHoverHelp("Cancel","common.Cancel") %>
  </span>
  <br><br>
</div>

<script type="text/javascript">

  var deleteAllDialog = {};

  require(["t360/dialog", "dojo/domReady!"],function( dialog ) {

    <%-- on select perform execute the select callback function --%>
    deleteAllDialog.deleteAll = function() {
      dialog.hide('deleteAllConfirmationDialog');
      dialog.doCallback('deleteAllConfirmationDialog', 'deleteAll');
    };

    deleteAllDialog.deleteAllConfirmationDialogClose = function() {
      dialog.hide('deleteAllConfirmationDialog');
    }
  });

  var dialogName=dijit.byId("deleteAllConfirmationDialog");
  var dialogCloseButton=dialogName.closeButtonNode;
  dialogCloseButton.setAttribute("id","Threshold_Group_X_Button");
</script>
<%=widgetFactory.createHoverHelp("Threshold_Group_X_Button", "common.Cancel") %>

