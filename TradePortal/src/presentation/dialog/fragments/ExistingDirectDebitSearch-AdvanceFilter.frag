<%--
*****************************************************************************************************
                  Direct Debit Transaction History Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Direct Debit Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
 
  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

********************************************************************************************************
--%>
	<input type="hidden" name="NewSearch" value="Y">
	<div id="advanceDirectDebitFilter" style="padding-top: 5px;">
		<div>
			<div class="searchCriteria">
				<%= widgetFactory.createSearchTextField("DDI_DLG_InstrumentIdAdvance","ExistingDirectDebitSearch.InstrumentID", "20", "class='char15' onKeydown='filterDDsOnEnterDDI_DLG_(\"DDI_DLG_InstrumentIdAdvance\", \"Advanced\");'" ) %>
				<%=widgetFactory.createSearchSelectField( "DDI_DLG_CreditAcctAdvance", "ExistingDirectDebitSearch.CreditAccountNo", "", options.toString() ,"onKeydown='Javascript: filterDDsOnEnterDDI_DLG_(\"DDI_DLG_CreditAcctAdvance\", \"Advanced\");'") %>
				<%= widgetFactory.createSearchTextField("DDI_DLG_ReferenceAdvance","ExistingDirectDebitSearch.ReferenceNo", "20", "class='char20' onKeydown='filterDDsOnEnterDDI_DLG_(\"DDI_DLG_ReferenceAdvance\", \"Advanced\");'" ) %>
			</div>
			<div class="title-right inline">							
				<button data-dojo-type="dijit.form.Button" id="searchButtonAdvDDI" type="button">
					<%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	    			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					searchDirectDebitHistoryDDI_DLG_("Advanced");return false;
    			</script>
	    		</button>
				<%=widgetFactory.createHoverHelp("searchButtonAdvDDI","SearchHoverText") %>
	    		<br>
				&nbsp;&nbsp;&nbsp;&nbsp;<a id="searchLinkBasicDDI" href="javascript:shuffleFilterDDI_DLG_('Basic');"><%=resMgr.getText("ExistingDirectDebitSearch.Basic", TradePortalConstants.TEXT_BUNDLE)%></a>
				<%=widgetFactory.createHoverHelp("searchLinkBasicDDI","common.AdvancedSearchHypertextLink") %>
			</div>
			<div style="clear:both"></div>
		</div>

		<div >
			<%=widgetFactory.createSearchAmountField("DDI_DLG_AmountFrom", "ExistingDirectDebitSearch.AmountFrom","","onKeydown='Javascript: filterDDsOnEnterDDI_DLG_(\"DDI_DLG_AmountFrom\", \"Advanced\");' class='char6 inline'")%>
			<%=widgetFactory.createSearchAmountField("DDI_DLG_AmountTo", "ExistingDirectDebitSearch.To","","onKeydown='Javascript: filterDDsOnEnterDDI_DLG_(\"DDI_DLG_AmountTo\", \"Advanced\");' class='char6 inline'")%>
			<%=widgetFactory.createSearchDateField("DDI_DLG_FromDate", "ExistingDirectDebitSearch.ExecutionDateFrom", " class='char6 inline'")%>
			<%=widgetFactory.createSearchDateField("DDI_DLG_ToDate", "ExistingDirectDebitSearch.To", " class='char6 inline'")%>
			<div style="clear:both"></div>
		</div>
	</div>