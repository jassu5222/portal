<%--
*******************************************************************************
                    Direct Debit Transaction History Search Basic Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Basic Filter fields for the  
  Direct Debit Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>

	<input type="hidden" name="NewSearch" value="Y">
	
	<div id="basicDirectDebitFilter" style="padding-top: 5px;">
		<div class="searchCriteria">

			<%= widgetFactory.createSearchTextField("DDI_DLG_InstrumentIdBasic","ExistingDirectDebitSearch.InstrumentID", "20", "class='char15' onKeydown='filterDDsOnEnterDDI_DLG_(\"DDI_DLG_InstrumentIdBasic\", \"Basic\");'" ) %>
			<%=widgetFactory.createSearchSelectField( "DDI_DLG_CreditAcctBasic", "ExistingDirectDebitSearch.CreditAccountNo", "", options.toString(), "onKeydown='Javascript: filterDDsOnEnterDDI_DLG_(\"DDI_DLG_CreditAcctBasic\", \"Basic\");'") %>
			<%= widgetFactory.createSearchTextField("DDI_DLG_ReferenceBasic","ExistingDirectDebitSearch.ReferenceNo", "20", "class='char20' onKeydown='filterDDsOnEnterDDI_DLG_(\"DDI_DLG_ReferenceBasic\", \"Basic\");'" ) %>
		</div>						
		<div class="title-right inline" style="text-align: center">
			<button data-dojo-type="dijit.form.Button" id="searchButtonBasicDDI" type="button">
				<%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				searchDirectDebitHistoryDDI_DLG_("Basic");return false;
    		</script>
			</button>
			<%=widgetFactory.createHoverHelp("searchButtonBasicDDI","SearchHoverText") %>
			</br>
			<span id="searchLinkAdvDDI" ><a href="javascript:shuffleFilterDDI_DLG_('Advance');"><%=resMgr.getText("ExistingDirectDebitSearch.Advanced", TradePortalConstants.TEXT_BUNDLE)%></a>	</span>
			<%=widgetFactory.createHoverHelp("searchLinkAdvDDI","common.AdvancedSearchHypertextLink") %>
		</div>
		<div style="clear: both;"></div>
	</div>
