<%--
*****************************************************************************************************
                  Cash Management Transaction Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Cash Management Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
 
  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

********************************************************************************************************
--%>

	<input type="hidden" name="NewSearch" value="Y">

	<div id="advancdePaymentFilter" style="padding-top: 5px;">
	  <div>
		<div class="searchCriteria">
	  		<% 
	  			options = Dropdown.getInstrumentList(instrumentType, loginLocale, instrumentTypes );
	        %>
	    	<%=widgetFactory.createSearchSelectField("pmtD_InstrumentTypeAdvance","CashMgmtTransactionSearch.TransactionType"," ", options, "onKeydown='Javascript: filterPaymentsOnEnter(\"pmtD_InstrumentTypeAdvance\", \"Advanced\");'")%>
	    	
	    	<%
	    		options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
	    	%>
	    	<%=widgetFactory.createSearchSelectField("pmtD_Currency","InstSearch.Currency"," ", options, "class='char25' onKeydown='filterPaymentsOnEnter(\"pmtD_Currency\", \"Advanced\");'")%>
	
		</div>
		<div class="title-right inline">	
			<button data-dojo-type="dijit.form.Button" id="pmtD_searchButtonAdv"  type="button" >  <%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	        	filterPayments("Advanced");return false;
	      	</script>
	        </button>
			 <%=widgetFactory.createHoverHelp("pmtD_searchButtonAdv","SearchHoverText") %>
			<br>
	        &nbsp;&nbsp;&nbsp;&nbsp;<a id="pmtD_searchLinkBasic" href="javascript:shuffleFilter('Basic');"><%=resMgr.getText("ExistingDirectDebitSearch.Basic", TradePortalConstants.TEXT_BUNDLE)%></a>
			<%=widgetFactory.createHoverHelp("pmtD_searchLinkBasic","common.AdvancedSearchHypertextLink") %>
		  </div>
		   <div style="clear:both"></div>
		</div>
		
		<div>	
			<%=widgetFactory.createSearchAmountField("pmtD_AmountFrom", "ExistingDirectDebitSearch.AmountFrom", "", "onKeydown='Javascript: filterPaymentsOnEnter(\"pmtD_AmountFrom\", \"Advanced\");'")%>
			<%=widgetFactory.createSearchAmountField("pmtD_AmountTo", "CashMgmtTransactionSearch.To", "", "onKeydown='Javascript: filterPaymentsOnEnter(\"pmtD_AmountTo\", \"Advanced\");'")%>
			
			<%=widgetFactory.createSearchTextField("pmtD_OtherParty","CashMgmtTransactionSearch.OtherParty","30", "onKeydown='Javascript: filterPaymentsOnEnter(\"pmtD_OtherParty\", \"Advanced\");'")%>
                      <div style="clear:both"></div>
		</div>
	</div>