<%--
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Cash Management Transaction History Tab

  Description:
    Contains HTML to create the History tab for the Cash Management Transaction page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%> 

<%
   
   StringBuffer statusExtraTags = new StringBuffer();

      //	StringBuffer statusOptions  = Dropdown.createActiveInActiveStatusOptions(resMgr,selectedStatus);
    //StringBuffer statusOptions = Dropdown.createCashMgmtTransStatusOptions(resMgr,selectedStatus);
StringBuffer statusOptions = Dropdown.createActiveInActiveStatusOptions(resMgr,selectedStatus);

   // Upon changing the status selection, automatically go back to the Transactions Home
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)

   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToPaymentTransactionsHome", response));
   statusExtraTags.append("&amp;current2ndNav=");
   statusExtraTags.append(current2ndNav);
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;instrStatusType='+this.options[this.selectedIndex].value\"");

%>
	
		<div border-bottom: 1px solid grey; padding-top: 10px;">
			<%
			      // When the Instrument History tab is selected, display 
			      // Organizations dropdown or the single organization.
			    if ((SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK)) && (totalOrganizations > 1))
				{
					dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME", selectedOrg, userSession.getSecretKey()));
					dropdownOptions.append("<option value=\"");
					dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_ORGANIZATIONS, userSession.getSecretKey()));
					dropdownOptions.append("\"");

					if (selectedOrg.equals(ALL_ORGANIZATIONS)) {
   						dropdownOptions.append(" selected");
					}

					dropdownOptions.append(">");
					dropdownOptions.append(ALL_ORGANIZATIONS);
					dropdownOptions.append("</option>");
					
					extraTags = new StringBuffer("");
					extraTags.append("onchange=\"location='");
					extraTags.append(formMgr.getLinkAsUrl("goToPaymentTransactionsHome", response));
					extraTags.append("&amp;current2ndNav=");
					extraTags.append(current2ndNav);
					extraTags.append("&NewDropdownSearch=Y");
					extraTags.append("&historyOrg='+this.options[this.selectedIndex].value\"");
					pmtD_Organization = true;
			%>
				<%=widgetFactory.createSearchSelectField("pmtD_Organization", "InstrumentHistory.Show", "", dropdownOptions.toString(), "onChange='filterPaymentsChange()'")%>

			<%
			      }
			%> 
				<%=widgetFactory.createSearchSelectField("pmtD_instrStatusType", "InstrumentHistory.Status", "", statusOptions.toString(), "onChange='filterPaymentsChange()'")%>
			 
		</div>     
 <div class="subHeaderDivider"></div>  
		<%@ include file="/dialog/fragments/ExistingPaymentSearch-AdvanceFilter.frag" %>
		<%@ include file="/dialog/fragments/ExistingPaymentSearch-BasicFilter.frag" %>
		
		<input type=hidden name=SearchType value=<%=searchType%>>

		<%
			DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
			String gridHtml = dgFactory.createDataGrid("existingPaymentSearchDataGridId","ExistingPaymentSearchDataGrid",null);

		%>
		<%=gridHtml%>
