<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
     /***************************************************************************************************
      * Start of Documents Section 
      ************************************/  	       Debug.debug("****** Start of Documents Section ******");
 
  String urlParam;
%>


<%-- PHILC PUT IMAGES IN 3 COLUMN TABLE --%> 
<%
String doc_name="";
boolean displayedDocsGeneratedTable = false; //Vshah CR-452
try {
	query = new StringBuffer();
	//cquinton 8/25/2011 Rel 7.1.0 ppx240 - add hash
	query.append("select doc_name,form_type, image_id, hash ");
	query.append("from document_image ");
	query.append("where p_transaction_oid = ?");
	query.append(" and form_type <> '"+TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED+"'");
	query.append(" and form_type <> '"+TradePortalConstants.DOC_IMG_FORM_TYPE_OTL_ATTACHED+"'");
	Debug.debug("Query is : " + query);	 
	dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), true, new Object[]{transaction_oid});

	if((dbQuerydoc != null)
      && (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0)) 
    {		
		Debug.debug("**** dbQuerydoc --> " + dbQuerydoc.toString());
		//out.println(dbQuerydoc.toString());
		listviewVector = dbQuerydoc.getFragments("/ResultSetRecord/");
		displayedDocsGeneratedTable = true; //Vshah CR-452
%>
 

  
<%
		//This loop is designed to scroll through the ResultSetRecords and display the Links.
		//Note that the links need parameters passed with them.
		Debug.debug("Walk the tree to display the text in the listview.");
		int liTotalDocs = listviewVector.size();
		int liDocCounter = liTotalDocs;
		 String tran_oid = EncryptDecrypt.encryptStringUsingTripleDes(transaction_oid, userSession.getSecretKey()); 	//Transaction_Oid
			
		for(int y=0; y < ((liTotalDocs+2)/3); y++) {
%>
  
<%	
			for (int x=0; x < 3 ; x++) {
				if (liDocCounter==0) break;
%>
    
<%
					myDoc       = (DocumentHandler)listviewVector.elementAt((y*3)+x);
					attribute   = myDoc.getAttribute("/FORM_TYPE");
					doc_name   = myDoc.getAttribute("/DOC_NAME");
					displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.FORM_TYPE, attribute);
					attribute   = myDoc.getAttribute("/IMAGE_ID");
					encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( attribute, userSession.getSecretKey() );
					encryptVal3 = EncryptDecrypt.encryptStringUsingTripleDes( displayText, userSession.getSecretKey() );
                    // cquinton 7/21/2011 Rel 7.1.0 ppx240 - add hash parm
                    String imageHash = myDoc.getAttribute("/HASH");
                    String encryptedImageHash = "";
                    if ( imageHash != null && imageHash.length()>0 ) {
                        encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( imageHash, userSession.getSecretKey() );
                    } else {
                        //send something rather than nothing, which ensures we know we are not allowing
                        // attempts to view images when hash parm is just forgotten
                        encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( TradePortalConstants.NO_HASH, userSession.getSecretKey() );
                    }
					urlParam    = "&image_id=" + encryptVal2 + "&hash=" + encryptedImageHash + "&formType=" + encryptVal3 + "&tran_id=" + tran_oid ;
%>
            <%=doc_name %><a href="<%=formMgr.getLinkAsUrl("goToDocumentView", urlParam, response) %>">Image</a>
				
				 <% // [START] CR-529 - Tang - 3/1/2010
				 displayText = resMgr.getText("TransactionTerms.getImage", TradePortalConstants.TEXT_BUNDLE);
				 urlParam = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) +
				            "&hash=" + encryptedImageHash;
				 %>
				 <a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParam)%>' target='_blank'>PDF</a>
				 <% // [END  ] CR-529 %>
	
<%
				liDocCounter--;
			}
			if (liDocCounter==0) {
				for (int z=liTotalDocs % 3; z > 0; z--) {
%>
   
<%
				}			
			}
%>
	
<%
		}
%>

<%
	}
} catch(Exception e) {
	Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Document Image table *******");
	e.printStackTrace();
}
//Vshah CR-452 Add Begin
if (showPDFLinks)
	{
	if (!displayedDocsGeneratedTable) {
%>
	
<% 	
	}
%>	
	
<%
         				  linkArgs[0] = TradePortalConstants.PDF_EXPORT_COLLECTION;
				          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid, userSession.getSecretKey());
					      linkArgs[2] = "en_US";
				          linkArgs[3] = userSession.getBrandingDirectory();
%>
  	  					  <%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("ExportCollectionIssue.ExportCollection", TradePortalConstants.TEXT_BUNDLE), 
					            									 TradePortalConstants.PDF_EXPORT_COLLECTION,
                                                     				 linkArgs,
                                                    				 response) %>
			 	
<%
         				  linkArgs[0] = TradePortalConstants.PDF_BILL_OF_EXCHANGES;
				          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid, userSession.getSecretKey());
					      linkArgs[2] = "en_US";
				          linkArgs[3] = userSession.getBrandingDirectory();
%>
  	  					  <%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("ExportCollectionIssue.BillOfExchanges", TradePortalConstants.TEXT_BUNDLE), 
					            									 TradePortalConstants.PDF_BILL_OF_EXCHANGES,
                                                     				 linkArgs,
                                                    				 response) %>
				
<%
	}
%>		
<%-- Vshah CR-452 Add End --%> 

<%-- PHILC PUT IMAGES IN 3 COLUMN TABLE --%> 

<%-- PHILC PUT IMAGES IN 3 COLUMN TABLE --%> 
<%
try {
   query = new StringBuffer();

   //cquinton 8/25/2011 Rel 7.1.0 ppx240 - add image hash
   query.append("select doc_name,doc_image_oid, image_id, doc_name, hash ");
   query.append("from document_image ");
   query.append("where p_transaction_oid = ?");
   query.append(" and form_type = '"+TradePortalConstants.DOC_IMG_FORM_TYPE_OTL_ATTACHED+"'");
   query.append(" and mail_message_oid is null");
   query.append(" order by image_id");

   Debug.debug("Query is : " + query);
   dbQuerydoc = null;
   dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), false, new Object[] {transaction_oid});

   if((dbQuerydoc != null)
      && (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0)) 
   {
      Debug.debug("**** dbQuerydoc --> " + dbQuerydoc.toString());
      listviewVector = null;
      listviewVector = dbQuerydoc.getFragments("/ResultSetRecord/");
%>

  
  

<%
	int liTotalDocs = listviewVector.size();
	int liDocCounter = liTotalDocs;
		
	for(int y=0; y < ((liTotalDocs+2)/3); y++) {
%>
  
<%	
		for (int x=0; x < 3 ; x++) {
			if (liDocCounter==0) break;
%>
    
<%
            myDoc = (DocumentHandler)listviewVector.elementAt((y*3)+x);
            // cquinton 7/21/2011 Rel 7.1.0 ppx240 - add hash parm
            String imageHash = myDoc.getAttribute("/HASH");
            String encryptedImageHash = "";
            if ( imageHash != null && imageHash.length()>0 ) {
                encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( imageHash, userSession.getSecretKey() );
            } else {
                //send something rather than nothing, which ensures we know we are not allowing
                // attempts to view images when hash parm is just forgotten
                encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( TradePortalConstants.NO_HASH, userSession.getSecretKey() );
            }
            urlParam = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) +
                       "&hash=" + encryptedImageHash;
            displayText = resMgr.getText("TransactionTerms.getImage", TradePortalConstants.TEXT_BUNDLE);
%>
            <a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParam)%>' target='_blank'>
            	PDF
            </a>
		
<%
		liDocCounter--;
		}
	if (liDocCounter==0) {
		for (int z=liTotalDocs % 3; z > 0; z--) {
%>
    
<%
		}			
	}
%>
	
<%
    }
%>
 
<%
   } // if((dbQuerydoc != null) && (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0)) 
}catch(Exception e){
   Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Document Image table *******");
   e.printStackTrace();
}
%>
