<%--
 *  Instrument Search Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
--%>
<%@page import="java.util.Vector"%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 


//read in parameters
  String instrumentSearchDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
//This flag is set to 'Y', when the Trade Search dialog is called to Create Amendment.
//In this case, there is no need to open the Bank/Branch dialog
  String createAmendmentFlag = StringFunction.xssCharsToHtml(request.getParameter("createAmendmentFlag")); 
//jyoti added on 15-10-2012 IR6117 
	String createTracerFlag = StringFunction.xssCharsToHtml(request.getParameter("createTracerFlag"));
//This flag is set to 'Y', when the Trade Search Dialog is called to Copy an instrument from an existing instrument.
  String createNewInstrumentFlag = StringFunction.xssCharsToHtml(request.getParameter("createNewInstrumentFlag"));

//This flag is set to 'Y', when the Trade Search dialog is called to Create Assignment.
  String createAssignmentFlag = StringFunction.xssCharsToHtml(request.getParameter("createAssignmentFlag")); 

//This flag is set to 'Y', when the Trade Search Dialog is called to Create a Template from existing instrument.
  String createTemplFromInstrFlag = StringFunction.xssCharsToHtml(request.getParameter("createTemplFromInstrFlag"));

//This flag is set to 'Y', when the Trade Search Dialog is called to search for an Instrument.
  String searchInstrumentFlag = StringFunction.xssCharsToHtml(request.getParameter("searchInstrumentFlag"));
	//Srinivasu_D CR#269 Rel8.4 09/02/2013 - start
  String conversionCenter = StringFunction.xssCharsToHtml(request.getParameter("conversionCenter"));
  //Srinivasu_D CR#269 Rel8.4 09/02/2013 - end 
  String createSettlementInstructionsFlag = StringFunction.xssCharsToHtml(request.getParameter("createSettlementInstructionsFlag"));
  
  String requestRolloverFlag = StringFunction.xssCharsToHtml(request.getParameter("requestRolloverFlag"));
  
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  //think this is what is necessary to limit initially
   Debug.debug("***START********************INSTRUMENT SEARCH**************************START***");

  boolean newTemplate = true;     // indicates what page we came from (i.e.,
                                  // doing new template or new transaction)
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();
  String ownerLevel      = userSession.getOwnershipLevel();

  String options;
  String currencyOptions;
  String defaultText;

  // Parameters that drive logic for this page.
  String selectMode;
  String cancelAction;
  String prevStepAction;
  String nextStepForm;
  String searchCondition = "";
  String copyType;
  String titleKey;
  String bankBranch;
  String showNavBar;
  String showChildInstruments;

  StringBuffer dynamicWhere = new StringBuffer();
  StringBuffer newSearchCriteria = new StringBuffer();

  String       NewDropdownOptions = "";
  StringBuffer statusOptions   = new StringBuffer();
  StringBuffer statusExtraTags = new StringBuffer();
  String       selectedStatus  = "";
  String       gridHtml        = "";
  String       gridLayout      = "";
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);

  final String STATUS_ALL      = resMgr.getText("common.StatusAll",
                                                TradePortalConstants.TEXT_BUNDLE);
  final String STATUS_ACTIVE   = resMgr.getText("common.StatusActive",
                                                TradePortalConstants.TEXT_BUNDLE);
  final String STATUS_INACTIVE = resMgr.getText("common.StatusInactive",
                                                TradePortalConstants.TEXT_BUNDLE);
  // W Zhu 6/8/07 POUH060841116 Add STATUS_STRICTLY_ACTIVE
  // This status is passed in from invoking page directly and will not be part of
  // the visible Option field so we do not need to get from language-specific
  // text resource file.
  final String STATUS_STRICTLY_ACTIVE = "Strictly Active";

// PPRAKASH IR NGUJ013070808 Add Begin
  CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  corpOrg.getById(userSession.getOwnerOrgOid());
  //PPRAKASH IR NGUJ013070808 Add End
  
  // Based on the search type, read in the search parameters the user may have
  // previously used.  We use these to build the dynamic search criteria and
  // also to redisplay to the user

  String instrumentId = "";
  String refNum = "";
  String bankRefNum = "";
  String vendorId = "";
  String instrumentType = "";
  String currency = "";
  String amountFrom = "";
  String amountTo = "";
  String otherParty = "";
  String dayFrom = "";
  String monthFrom = "";
  String yearFrom = "";
  String dayTo = "";
  String monthTo = "";
  String yearTo = "";
  String helpSensitiveLink  = null;
  String bankInstrumentId = null;
  String allowImpCol      = corpOrg.getAttribute("allow_imp_col");
  String allowImportDLC   = corpOrg.getAttribute("allow_import_DLC");
  String allowLoanRequest = corpOrg.getAttribute("allow_loan_request");

  String searchType = "";
  Vector instrumentTypes = null;
  boolean showInstrumentDropDown = false;
  String mode = "";
  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'"; 
	    
  DocumentHandler doc = null;
  // First read in the parameters that drive how the page operates.

  cancelAction = StringFunction.xssCharsToHtml(request.getParameter("CancelAction"));
  if (cancelAction == null)
     Debug.debug("***Warning: CancelAction parameter not specified");

  prevStepAction = StringFunction.xssCharsToHtml(request.getParameter("PrevStepAction"));

  nextStepForm = StringFunction.xssCharsToHtml(request.getParameter("NextStepForm"));
  if (nextStepForm == null)
     Debug.debug("***Warning: NextStepForm parameter not specified");

  selectMode = StringFunction.xssCharsToHtml(request.getParameter("SelectMode"));
  if (selectMode == null) selectMode = TradePortalConstants.INDICATOR_NO;

  showNavBar = StringFunction.xssCharsToHtml(request.getParameter("ShowNavBar"));
  if (showNavBar == null)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  // Indicates whether or not instruments belonging to the child organization 
  // of the user's organization should be displayed in the list
  showChildInstruments = StringFunction.xssCharsToHtml(request.getParameter("ShowChildInstruments"));
  if(showChildInstruments == null)
   {
      showChildInstruments = TradePortalConstants.INDICATOR_NO;
   }

  // Check if this is a new Search (based on passed in NewSearch parameter)
  // If so, clear the listview information so it does not
  // retain any page/sort order/ sort column/ search type information 
  // resulting from previous searches
	
  // A new search can also result from the corp org or status type dropdown 
  // being reselected. In this case, NewDropdownSearch parameter indicates a 
  // pseudo new search -- the search type is retained but the rest of the 
  // search clause is not

  String newSearch = StringFunction.xssCharsToHtml(request.getParameter("NewSearch"));
  String newDropdownSearch = StringFunction.xssCharsToHtml(request.getParameter("NewDropdownSearch"));
  Debug.debug("New Search is " + newSearch);
  Debug.debug("New Dropdown Search is " + newDropdownSearch);

  if (newSearch != null && newSearch.equals(TradePortalConstants.INDICATOR_YES))
  {
       // Default search type for instrument status on the Instrument Search page is ALL.
      session.setAttribute("instrSearchStatusType", STATUS_ALL);
  }

  searchType = StringFunction.xssCharsToHtml(request.getParameter("SearchType"));

  if (searchType == null)
  {
	  searchType = TradePortalConstants.SIMPLE;

  }
  
  copyType = StringFunction.xssCharsToHtml(request.getParameter("CopyType"));
  if (copyType == null) copyType = "";


  bankBranch = StringFunction.xssCharsToHtml(request.getParameter("BankBranch"));
 
  titleKey = StringFunction.xssCharsToHtml(request.getParameter("TitleKey"));
  if (titleKey == null)
  {
     titleKey = "";
  }

  /************************
   * ERSKINE'S CODE START *
   ************************/

  //If we are in the transaction area the parameters will be passed in via this document
  HttpSession theSession = request.getSession(false);
  String inTransactionArea = (String) theSession.getAttribute("inNewTransactionArea");

  if (inTransactionArea != null && inTransactionArea.equals(TradePortalConstants.INDICATOR_YES))
  {
    doc = formMgr.getFromDocCache();
    Debug.debug("Getting data from doc cache ->\n" + doc.toString(true));

	mode    = doc.getAttribute("/In/mode");
    cancelAction   = TradePortalConstants.TRANSACTION_CANCEL_ACTION;
    selectMode     = TradePortalConstants.INDICATOR_NO;
    copyType       = doc.getAttribute("/In/copyType");
    bankBranch     = doc.getAttribute("/In/bankBranch");

    // Change the title of the page based on what we're in the process of creating
    if (mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
       titleKey = "InstSearch.HeadingNewTemplateStep2";
    else
       titleKey = "InstSearch.HeadingNewTransStep2"; 

    Debug.debug("Getting data from doc cache2 ->\n" + doc.toString(true));
	
   
    if (mode != null && mode.equals(TradePortalConstants.EXISTING_INSTR))
    {
        nextStepForm = "NewTransInstrSearchForm2";
        prevStepAction = "newTransactionStep1";
    }
    else
    {
        nextStepForm = "NewTransInstrSearchForm";
        if (mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
            prevStepAction = "newTemplate";
        else
            prevStepAction = "newTransactionStep1";
       
       //IAZ CR-586 08/16/10 Begin
        String isFixed = doc.getAttribute("/In/isFixed");
    	String isExpress = doc.getAttribute("/In/isExpress");
       if(TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isFixed))
  	 	dynamicWhere.append(" and i.instrument_type_code = '" + InstrumentType.DOM_PMT + "'");
  	   if(TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isExpress ))
  	 	dynamicWhere.append(" and i.instrument_type_code in ('" + TradePortalConstants.SIMPLE_SLC + "'"
  	 	 + ", '" + InstrumentType.APPROVAL_TO_PAY + "'"
  	 	 + ", '" + InstrumentType.REQUEST_ADVISE + "'"
  	 	 + ", '" + InstrumentType.IMPORT_DLC + "')");  	   
       //IAZ CR-586 08/16/10 End

    }

    theSession.setAttribute("prevStepAction", "goToInstrumentSearch");
    Debug.debug("Getting data from doc cache 3->\n" + doc.toString());

  }
  /************************
   *  ERSKINE'S CODE END  *
   ************************/
  else
  {
     doc = formMgr.getFromDocCache();

     if (doc.getDocumentNode("/In/InstrumentSearchInfo/SelectMode") != null)
     {
        selectMode   = doc.getAttribute("/In/InstrumentSearchInfo/SelectMode");
        cancelAction = doc.getAttribute("../CancelAction");
        nextStepForm = doc.getAttribute("../NextStepForm");
        showNavBar   = doc.getAttribute("../ShowNavBar");
        showChildInstruments = doc.getAttribute("../ShowChildInstruments");
        if(showChildInstruments == null)
          {
             showChildInstruments = TradePortalConstants.INDICATOR_NO;
          }
     }
  }


  String searchOnInstrumentType = doc.getAttribute("/In/InstrumentSearchInfo/SearchInstrumentType");

  if (searchOnInstrumentType == null)
  	searchOnInstrumentType = "";
	
  // determine what type of search is required - this is used by 
  // SearchParms and SearchType for sqlWhereClause and Instrument Type
  // dropdowns
  if (searchOnInstrumentType.equals(InstrumentType.EXPORT_DLC))
  	searchCondition = TradePortalConstants.SEARCH_EXP_DLC_ACTIVE;


  // TLE - 10/26/06 - IR#AYUG101742823 - Add Begin
  else if (searchOnInstrumentType.equals(TradePortalConstants.ALL_EXPORT_DLC)) {
  	searchCondition = TradePortalConstants.ALL_EXPORT_DLC;
  } 
  else if (searchOnInstrumentType.equals(InstrumentType.EXPORT_COL)) {
  	searchCondition = InstrumentType.EXPORT_COL;
  }
  //Vasavi CR 524 03/31/2010 Begin
  else if (searchOnInstrumentType.equals(InstrumentType.NEW_EXPORT_COL)) {
  	searchCondition = InstrumentType.NEW_EXPORT_COL;
  }
  //Vasavi CR 524 03/31/2010 End
  else if (searchOnInstrumentType.equals(TradePortalConstants.IMPORT)) {
  	searchCondition = TradePortalConstants.IMPORT;
  }
  // TLE - 10/26/06 - IR#AYUG101742823 - Add End



  else if(searchOnInstrumentType.equals(InstrumentType.LOAN_RQST)) {
  	searchCondition = InstrumentType.LOAN_RQST;
     
  }
  else if(searchOnInstrumentType.equals(TradePortalConstants.IMPORTS_ONLY))
	searchCondition = TradePortalConstants.IMPORTS_ONLY;	
  else if(mode != null && mode.equals(TradePortalConstants.EXISTING_INSTR))
  	searchCondition = TradePortalConstants.SEARCH_FOR_NEW_TRANSACTION;
  else if(mode != null && mode.equals(TradePortalConstants.NEW_INSTR))
  	searchCondition = TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT;
  else if(mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
  	searchCondition = TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT;
  else
      searchCondition = TradePortalConstants.SEARCH_ALL_INSTRUMENTS;

  Debug.debug("CancelAction is " + cancelAction);
  Debug.debug("PrevStepAction is " + prevStepAction);
  Debug.debug("NextStepForm is " + nextStepForm);
  Debug.debug("SelectMode is " + selectMode);
  Debug.debug("Mode is " + mode);
  Debug.debug("SearchType is " + searchType);
  Debug.debug("CopyType is " + copyType);
  Debug.debug("BankBranch is " + bankBranch);
  Debug.debug("ShowNavBar is " + showNavBar);
  Debug.debug("SearchCondition is " + searchCondition);
 
  // Create a link to the basic or advanced instrument search
  String linkParms;
  String linkText;
  String link;
    Debug.debug("Getting data from doc cache 5->\n");

  linkParms = "&SelectMode=" + selectMode
              + "&CancelAction=" + cancelAction
              + "&PrevStepAction=" + prevStepAction
              + "&NextStepForm=" + nextStepForm
              + "&CopyType=" + copyType
              + "&TitleKey=" + titleKey
              + "&BankBranch=" + bankBranch
              + "&ShowNavBar=" + showNavBar;
  linkText = "";

  if (searchType.equals(TradePortalConstants.ADVANCED)) {
    linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
    link = formMgr.getLinkAsUrl("goToInstrumentDialogSearch",
                                linkParms, response);
    linkText = resMgr.getText("InstSearch.BasicSearch",
                              TradePortalConstants.TEXT_BUNDLE);
    helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/instr_search.htm",
	   						"advanced",resMgr, userSession);
   } else {
    linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
    link = formMgr.getLinkAsUrl("goToInstrumentDialogSearch",
                                linkParms, response);
    linkText = resMgr.getText("InstSearch.AdvancedSearch",
                              TradePortalConstants.TEXT_BUNDLE);
    helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/instr_search.htm",
	   				"basic", resMgr, userSession);
   }

  // W Zhu 1/15/2013 T36000006937 remove userOrgOid for security.
  if(showChildInstruments.equals(TradePortalConstants.INDICATOR_YES))
    {
      // Append the dynamic where clause to view the instruments of the user's 
      // organizations and all of its children.
      dynamicWhere.append(" and i.a_corp_org_oid in ( ");      
      dynamicWhere.append(" select organization_oid ");
      dynamicWhere.append(" from corporate_org ");
      dynamicWhere.append(" start with organization_oid =  ");
      dynamicWhere.append(userSession.getOwnerOrgOid());
      dynamicWhere.append(" connect by prior organization_oid = p_parent_corp_org_oid ");
      dynamicWhere.append(" ) ");
    }
   else
    {
      // Append the dynamic where clause to view only the user
      // organization's instruments
      dynamicWhere.append(" and i.a_corp_org_oid = "+  userSession.getOwnerOrgOid());
    } 

    // Based on the statusType dropdown, build the where clause to include one or more
    // instrument statuses.
    Vector statuses = new Vector();
    int numStatuses = 0;

    selectedStatus = StringFunction.xssCharsToHtml(request.getParameter("instrSearchStatusType"));

    if (selectedStatus == null) 
    {
       selectedStatus = (String) session.getAttribute("instrSearchStatusType");
    }

    // W Zhu 6/8/07 POUH060841116 Add case Strictly_active
    if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
    }
    else if (STATUS_ACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
    }

    else if (STATUS_INACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
    }

    else // default is ALL (actually not all since DELeted instruments
         // never show up (handled by the SQL in the listview XML)
    {
       selectedStatus = STATUS_ALL;
    }

    session.setAttribute("instrSearchStatusType", selectedStatus);

    // If the desired status is ALL, there's no point in building the where 
    // clause for instrument status.  Otherwise, build it using the vector
    // of statuses we just set up.

    if (!STATUS_ALL.equals(selectedStatus))
    {
        if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) ) {
           dynamicWhere.append(" and (o.transaction_status = '");
           dynamicWhere.append(TradePortalConstants.TRANS_STATUS_AUTHORIZED);
           dynamicWhere.append("' or i.instrument_status in (");
       } else {
           dynamicWhere.append(" and i.instrument_status in (");
       }
     
       numStatuses = statuses.size();

       for (int i=0; i<numStatuses; i++) {
         dynamicWhere.append("'");
         dynamicWhere.append( (String) statuses.elementAt(i) );
         dynamicWhere.append("'");

         if (i < numStatuses - 1) {
            dynamicWhere.append(", ");
         }
       }
     if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) ) {
          dynamicWhere.append(")) ");
       } else {
          dynamicWhere.append(") ");
       }
     }

         //IAZ CR-586 IR- PRUK092452162 09/29/10 Begin
         UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
         thisUser.setAttribute("user_oid", userSession.getUserOid());
         thisUser.getDataFromAppServer();
         String confInd = "";
         if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))
         	confInd = TradePortalConstants.INDICATOR_YES;
         else if (userSession.getSavedUserSession() == null)
            confInd = thisUser.getAttribute("confidential_indicator");
         else   
         {
            if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
                confInd = TradePortalConstants.INDICATOR_YES;
            else	
                confInd = thisUser.getAttribute("subsid_confidential_indicator");  
         }       
          
         if (!TradePortalConstants.INDICATOR_YES.equals(confInd))
        	dynamicWhere.append(" and i.confidential_indicator = 'N'");
         //IAZ CR-586 IR-PRUK092452162 09/29/10 End
         

   // Now include a file which reads the search criteria and build the
   // dynamic where clause.
%>
   <%@ include file="/transactions/fragments/InstSearch-InstType.frag" %>
   <%@ include file="/transactions/fragments/InstSearch-SearchParms.frag" %>

<%
   // Build the status dropdown options (hardcoded in ALL (default), ACTIVE, INACTIVE order), 
   // re-selecting the most recently selected option.

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("' ");
   if (STATUS_ALL.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("' ");
   if (STATUS_ACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("' ");
   if (STATUS_INACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("</option>");

   // Upon changing the status selection, automatically go back to the Instrument Search
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)
   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToInstrumentSearch", response));
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;instrSearchStatusType='+this.options[this.selectedIndex].value\"");
%>



<%
  // Store values such as the userid, his security rights, and his org in a
  // secure hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);
%>

<%-- ********************** First form begins here ***********************  --%>
<%-- Supports processing of Filter button.                                  --%>

<form name="InstrumentFilter" method="post"
      action="<%=formMgr.getSubmitAction(response)%>">

  
      
<%     //W Zhu 6/8/07 POUH060841116 BEGIN
       // If the invoking page force searching for strictly active instrument,
       // do not display the Instrument Status option.
       if (!STATUS_STRICTLY_ACTIVE.equals(selectedStatus) )
       {
       //W Zhu 6/8/07 POUH060841116 END
%>

  <div id="instrumentSearchDialogContent234" class="dialogContent fullwidth">
<%
  out.println(widgetFactory.createLabel("","InstrumentHistory.Status",false, false, false, "inline"));                  
%>

<%=widgetFactory.createCheckboxField("tradeSearchActive", "common.StatusActive", true, false, false,"onClick='searchInstruments();'","","inline") %>
<%= widgetFactory.createCheckboxField("tradeSearchInactive", "common.StatusInactive", false, false,false,"onClick='searchInstruments();'","","inline") %>
<div style="clear:both"></div>
<%
        } //if (!TradePortalConstants.INSTR_SEARCH_STATUS_ACTIVE.equals(selectedStatus))
%>        

<input type="hidden" name="NewSearch" value="Y">
<%//Code modified by Sandeep 11-04-2012 - Begin%>
<div id="tradeAdvancedInstrumentFilter" style="padding-top: 5px;">
  <div class="formItem">
	<%
	Long organization_oid = new Long( userSession.getOwnerOrgOid() );
  	Long userOID = new Long(userSession.getUserOid());
  	Vector tradeInstrumentTypes;
  	if ( "Y".equals(createSettlementInstructionsFlag) || "Y".equals(requestRolloverFlag) ) { 
  		boolean isCreateSettlement =  "Y".equals(createSettlementInstructionsFlag);
  		 tradeInstrumentTypes = Dropdown.getCreateModifySettlementInstrumentTypes( formMgr, loginRights, 
  				 organization_oid.longValue(), userSession.getSecurityType(), userSession.getOwnershipLevel(), isCreateSettlement); 		 
  	} else {
  	   tradeInstrumentTypes = Dropdown.getCreateModifyInstrumentTypes( formMgr, loginRights, 
		 organization_oid.longValue(), userSession.getSecurityType(), userSession.getOwnershipLevel(), userOID.longValue());
  	   
  	   if(SecurityAccess.hasRights(userSession.getSecurityRights(),SecurityAccess.ACCESS_CONVERSION_CENTER_AREA)){
  		//first check if GUA is already added
  		if(!tradeInstrumentTypes.contains(InstrumentType.GUARANTEE)){
			tradeInstrumentTypes.addElement(  InstrumentType.GUARANTEE  );
  		}
	   }
       //if called from template creation then display payment instruments also, if not just trade instruments
  	   if(!"Y".equals(createTemplFromInstrFlag)){
  	      tradeInstrumentTypes.removeElement(  InstrumentType.FUNDS_XFER  );
  	      tradeInstrumentTypes.removeElement(  InstrumentType.XFER_BET_ACCTS  );
  	      tradeInstrumentTypes.removeElement(  InstrumentType.DOM_PMT  );
  	   }
  	}
  	StringBuffer tradeInstrTypes = new StringBuffer();
  	for(int i=0; i<tradeInstrumentTypes.size(); i++){
  		tradeInstrTypes.append((String)tradeInstrumentTypes.elementAt(i));
  		
  		if(i<(tradeInstrumentTypes.size()-1))
  			tradeInstrTypes.append(",");
  	}
  	
		
		if (showInstrumentDropDown){			
			//jyoti added on 15-10-2012 IR6117
  		    if (createTracerFlag != null && createTracerFlag.equalsIgnoreCase("Y")){
	 	  		instrumentTypes = new Vector();
	 	  		instrumentTypes.addElement(InstrumentType.NEW_EXPORT_COL);
	 	  		instrumentTypes.addElement(InstrumentType.EXPORT_COL);	  		
	 	  		options= Dropdown.getInstrumentList(instrumentType,loginLocale, instrumentTypes );
	%>
				<%=widgetFactory.createSearchSelectField("tradeSearchInstrumentTypeAdvance","InstSearch.InstrumentType"," ", options, "onChange='searchInstruments(\"Advanced\");' class='char15'")%>
	<%				
 	  		}else if (createAssignmentFlag != null && createAssignmentFlag.equalsIgnoreCase("Y")){
 	 %>
 	 			<%=widgetFactory.createSubLabel("InstSearch.InstrumentType")%>
   					&nbsp;&nbsp;
  				<%=widgetFactory.createSubLabel("CorpCust.ExportLC1")%>
		    	<input type ="hidden" name="tradeSearchInstrumentTypeAdvance" id="tradeSearchInstrumentTypeAdvance" value="<%=InstrumentType.EXPORT_DLC%>" >
 	 <%
 	  		}else{
 	  			options = Dropdown.getInstrumentList( instrumentType, loginLocale, tradeInstrumentTypes);
	%>
			<%=widgetFactory.createSearchSelectField("tradeSearchInstrumentTypeAdvance","InstSearch.InstrumentType"," ", options, "onChange='searchInstruments(\"Advanced\");' class='char15'")%>
				
	<%	
 	  		}
		}else{
	%>
			<%=widgetFactory.createSubLabel("InstSearch.InstrumentType")%>
			 <b> <%=resMgr.getText("CorpCust.ExportLC", TradePortalConstants.TEXT_BUNDLE)%> </b>
		    <input type ="hidden" name="tradeSearchInstrumentTypeAdvance" id="tradeSearchInstrumentTypeAdvance" value="<%=InstrumentType.EXPORT_DLC%>" >
	<%	}	%>
		
	    <% currencyOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale); %>
		<%=widgetFactory.createSearchSelectField("tradeSearchCurrencyAdvance","InstSearch.Currency"," ", currencyOptions, "onChange='searchInstruments(\"Advanced\");' class='char21'")%>
		
		<span class="formItemWithIndent4"><%=resMgr.getText("InstSearch.AmountFrom",TradePortalConstants.TEXT_BUNDLE)%></span>
		<%=widgetFactory.createAmountField("tradeSearchAmountFromAdvance","","","",false,false,false,"onKeydown='Javascript: filterInstrumentsOnEnter(\"tradeSearchAmountFromAdvance\", \"Advanced\");' class='char5'","","none")%>
		<span class="formItemWithIndent4"><%=resMgr.getText("InstSearch.To",TradePortalConstants.TEXT_BUNDLE)%></span>
		<%=widgetFactory.createAmountField("tradeSearchAmountToAdvance","","","",false,false,false,"onKeydown='Javascript: filterInstrumentsOnEnter(\"tradeSearchAmountToAdvance\", \"Advanced\");' class='char5'","","none")%>
		
		<div class="title-right inline">
		<button data-dojo-type="dijit.form.Button" type="button" id="advancedTradeSearchButton">
	            <%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  searchInstruments("Advanced");return false;
	      		</script>
	        </button><br>&nbsp;&nbsp;&nbsp;
			<span id="basicTradeSearchLink"><a href="javascript:shuffleFilter('Basic');"><%=resMgr.getText("ExistingDirectDebitSearch.Basic", TradePortalConstants.TEXT_BUNDLE)%></a></span>
			<%=widgetFactory.createHoverHelp("advancedTradeSearchButton","SearchHoverText") %>
			<%=widgetFactory.createHoverHelp("basicTradeSearchLink","common.BasicSearchHypertextLink") %>
		</div> 
	</div>
		<div class="formItem">		
		<%=widgetFactory.createSearchDateField("tradeSearchDateFromAdvance","InstSearch.ExpiryDateFrom","onChange='searchInstruments(\"Advanced\");' class='char10'","constraints:{datePattern:'"+datePattern+"'},"+dateWidgetOptions)%>
		<%=widgetFactory.createSearchDateField("tradeSearchDateToAdvance","InstSearch.To","onChange='searchInstruments(\"Advanced\");' class='char10'","constraints:{datePattern:'"+datePattern+"'},"+dateWidgetOptions)%>
		<%=widgetFactory.createSearchTextField("tradeSearchOtherPartyAdvance","InstSearch.Party","30","onKeydown='Javascript: filterInstrumentsOnEnter(\"tradeSearchOtherPartyAdvance\", \"Advanced\");' class='char10'")%>
		<%=widgetFactory.createSearchTextField("tradeSearchVendorIdAdvance","InstSearch.VendorId","15","onKeydown='Javascript: filterInstrumentsOnEnter(\"tradeSearchVendorIdAdvance\", \"Advanced\");' class='char10'")%>			  
 	
  </div> 
   <div style="clear: both;"></div>
    </div>
 
  <div id="tradeBasicInstrumentFilter" style="padding-top: 5px;"> 
  <div class="formItem">
  <%-- Jyoti commented the radio fields on 23-10-2012 for IR-6786 --%>
  	<%=widgetFactory.createSearchTextField("tradeSearchInstrumentIdBasic","InstSearch.InstrumentID","16", "onKeydown='Javascript: filterInstrumentsOnEnter(\"tradeSearchInstrumentIdBasic\", \"Basic\");' class='char8'")%>
  	<%if (showInstrumentDropDown){  	
  		   //jyoti added on 15-10-2012 IR6117
  		    if (createTracerFlag != null && createTracerFlag.equalsIgnoreCase("Y")){
	 	  		instrumentTypes = new Vector();
	 	  		instrumentTypes.addElement(InstrumentType.NEW_EXPORT_COL);
	 	  		instrumentTypes.addElement(InstrumentType.EXPORT_COL);	  		
	 	  		options= Dropdown.getInstrumentList(instrumentType,loginLocale, instrumentTypes );
	 %>
	 			<%=widgetFactory.createSearchSelectField("tradeSearchInstrumentTypeBasic","InstSearch.InstrumentType"," ", options, "onChange='searchInstruments(\"Basic\");' class='char16'")%>	  		
	<%
 	  		}else if (createAssignmentFlag != null && createAssignmentFlag.equalsIgnoreCase("Y")){
 	 %>
 	 			<%=widgetFactory.createSubLabel("InstSearch.InstrumentType")%>
   					&nbsp;&nbsp;
  				<%=widgetFactory.createSubLabel("CorpCust.ExportLC1")%>
		    	<input type ="hidden" name="tradeSearchInstrumentTypeBasic" id="tradeSearchInstrumentTypeBasic" value="<%=InstrumentType.EXPORT_DLC%>" >
		    	<span class="formItem"></span>
 	 <% 		
 	  		}else{
 	  			options = Dropdown.getInstrumentList( instrumentType, loginLocale, tradeInstrumentTypes);	
   		   				
  	%>
  				<%=widgetFactory.createSearchSelectField("tradeSearchInstrumentTypeBasic","InstSearch.InstrumentType"," ", options, "onChange='searchInstruments(\"Basic\");' class='char16'")%>
  	<% 
 	  		}
 		 }else{
      		// Based on search condition (for specific instrument type searches only),
      		// print the name of the instrument type.  Also store this value so it is 
     		// picked up on return to the page.
      		String instrTypeText = "";
      		if (searchCondition.equals(TradePortalConstants.SEARCH_EXP_DLC_ACTIVE))
         	instrTypeText = "CorpCust.ExportLC";
     		else if (searchCondition.equals(InstrumentType.LOAN_RQST))
         			instrTypeText = "CorpCust.LoanRequests";
      		else
         	instrTypeText = instrumentType;
   	%>
   			<%=widgetFactory.createSubLabel("InstSearch.InstrumentType")%>
   			<b> <%=resMgr.getText(StringFunction.xssCharsToHtml(instrTypeText), TradePortalConstants.TEXT_BUNDLE)%> </b>
   <%
      		out.println("<input type=hidden name=InstrumentType value='");
		 	//Validation to check Cross Site Scripting
			if(instrumentType != null){
				instrumentType = StringFunction.xssCharsToHtml(instrumentType);
			}
		 	//Rel 9.2 XSS - CID 11292
      		out.println(StringFunction.escapeQuotesforJS(instrumentType));
      		out.println("'>");
   		}
   %>
  <%=widgetFactory.createSearchTextField("tradeSearchRefNumBasic","InstSearch.ApplRefNumber","30","onKeydown='Javascript: filterInstrumentsOnEnter(\"tradeSearchRefNumBasic\", \"Basic\");' class='char10'")%>
  
  <div class="title-right inline">
	    	<button data-dojo-type="dijit.form.Button" type="button" id="basicTradeSearchButton">
	            <%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  searchInstruments("Basic");return false;
	      		</script>
	        </button><br>&nbsp;
			<span id="advanceTradeSearchLink"><a href="javascript:shuffleFilter('Advanced');"><%=resMgr.getText("ExistingDirectDebitSearch.Advanced", TradePortalConstants.TEXT_BUNDLE)%></a></span>
			<%=widgetFactory.createHoverHelp("basicTradeSearchButton","SearchHoverText") %>
			<%=widgetFactory.createHoverHelp("advanceTradeSearchLink","common.AdvancedSearchHypertextLink") %>
</div>
   <%=widgetFactory.createSearchTextField("tradeSearchBankRefNumBasic","InstSearch.OrigBanksRefNo","30","onKeydown='Javascript: filterInstrumentsOnEnter(\"tradeSearchBankRefNumBasic\", \"Basic\");' class='char10'")%>
  <%=widgetFactory.createSearchTextField("tradeSearchVendorIdBasic","InstSearch.VendorId","15","onKeydown='Javascript: filterInstrumentsOnEnter(\"tradeSearchVendorIdBasic\", \"Basic\");' class='char10'")%>
 <% if (userSession.isCustNotIntgTPS()) {%>
 		<%=widgetFactory.createSearchTextField("tradeSearchBankInstIdBasic","InstSearch.BankInstrumentID","16", "onKeydown='Javascript: filterInstrumentsOnEnter(\"tradeSearchBankInstIdBasic\", \"Basic\");' class='char8'")%>
 	<%} %>
</div>
<div style="clear: both;"></div>
</div> 


  <% 
  gridHtml = dgFactory.createDataGrid("tradeSearchPopupGrid","TradeSearchDataGrid", null);
  gridLayout = dgFactory.createGridLayout("TradeSearchDataGrid","TradeSearchDataGrid"); 
  %> 	
  <%=gridHtml%>

  <%-- on select perform execute the select callback function --%>
  <%-- hardcode the selected bank branch for now until data grid is working --%>
  

</form>
<form name="InstrumentSearchResults" method="post"
      action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>
<%--Rel 9.2 XSS - CID 11484, 11445, 11396, 11385, 11380, 11369, 11368, 11360, 11306 --%>
  <input type=hidden name=SelectMode value=<%=StringFunction.escapeQuotesforJS(selectMode)%>>
  <input type=hidden name=CancelAction value=<%=StringFunction.escapeQuotesforJS(cancelAction)%>>
  <input type=hidden name=PrevStepAction value=<%=StringFunction.escapeQuotesforJS(prevStepAction)%>>
  <input type=hidden name=NextStepForm value=<%=StringFunction.escapeQuotesforJS(nextStepForm)%>>
  <input type=hidden name=SearchType value=<%=StringFunction.escapeQuotesforJS(searchType)%>>  
  <input type=hidden name=CopyType value=<%=StringFunction.escapeQuotesforJS(copyType)%>>
  <input type=hidden name=TitleKey value=<%=StringFunction.escapeQuotesforJS(titleKey)%>>
  <input type=hidden name=BankBranch value=<%=StringFunction.escapeQuotesforJS(bankBranch)%>>
  <input type=hidden name=ShowNavBar value=<%=StringFunction.escapeQuotesforJS(showNavBar)%>>

</form>

<script LANGUAGE="JavaScript">

var custNotIntgWithTPS = false;
<% if(userSession.isCustNotIntgTPS()){ %>
 		custNotIntgWithTPS = true;
<% } %>
  function checkForSelection() {
    <%--
    // Before going to the next step, the user must have selected one of the
    // radio buttons.  If not, give an error.  Since the selection radio button
    // is dynamic, it is possible the field doesn't even exist.  First check
    // for the existence of the field and then check if a selection was made.
    --%>

    numElements = document.InstrumentSearchResults.elements.length;
    i = 0;
    foundField = false;

    <%-- First determine if the field even exists. --%>
    for (i=0; i < numElements; i++) {
       if (document.InstrumentSearchResults.elements[i].name == "selection") {
          foundField = true;
       }
    }

    if (foundField) {
        var size=document.InstrumentSearchResults.selection.length;
        <%-- Handle the case where there are two or more radio buttons --%>
        for (i=0; i<size; i++)
        {
            if (document.InstrumentSearchResults.selection[i].checked)
            {
                return true;
            }
        }
        <%-- Handle the case where there is only one radio button --%>
        if (document.InstrumentSearchResults.selection.checked)
        {
            return true;
        }
    }
    <%-- Handle the case where no button is checked --%>
    var msg = "<%=resMgr.getText("InstSearch.SelectOne",
                                  TradePortalConstants.TEXT_BUNDLE)%>"
    alert(msg);


   <%-- narayan IR# POUJ092282592 --- Add below one line to make variable formSubmitted to FALSE
   when user hit the button Next Step, system actually submits the page and making the value of Boolean variable formSubmitted to TRUE.
   Hence after clicking to Next Step whenever user clicks on search system won't do anything (means system won't search / submit the page again)
   because it thinks that this page is already submitted and Trade Portal don't allow to form submission twice.
   --%>
    formSubmitted = false;
    return false;
  }

   <%--Enable form submission by enter.  event.which works for Netscape and event.keyCode works for IE --%>  
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  


<%-- Code modified by Sandeep 11-04-2012 - Begin --%>
  require(["t360/datagrid", "dojo/domReady!"], function( t360grid ) {
    var gridLayout = <%=gridLayout%>;
  <%-- jyoti added on 16-10-2012 IR6117 --%>
  <%-- Rel 9.2 XSS CID 11262, 11281, 11256 --%>
    var createTracerFlag = '<%=StringFunction.escapeQuotesforJS(createTracerFlag)%>';
    var createAssignmentFlag = '<%=StringFunction.escapeQuotesforJS(createAssignmentFlag)%>';
  	var conversionCenter =  '<%=StringFunction.escapeQuotesforJS(conversionCenter)%>';
  	var createSettlementInstructionsFlag  =  '<%=StringFunction.escapeQuotesforJS(createSettlementInstructionsFlag)%>';
  	var requestRolloverFlag =  '<%=StringFunction.escapeQuotesforJS(requestRolloverFlag)%>';
  	  	
    if (createAssignmentFlag=="Y"){
      var instrumentType = "<%=InstrumentType.EXPORT_DLC%>";
        
      var initParms="instrumentType="+instrumentType;
      initParms+="&statusActive=Active";
    <%-- Rel 9.2 XSS CID 11475, 11463, 11388, 11425, 11303 --%>
      initParms+="&createTracerFlag=<%=StringFunction.escapeQuotesforJS(createTracerFlag)%>";    
      initParms+="&createAmendmentFlag=<%=StringFunction.escapeQuotesforJS(createAmendmentFlag)%>";      
      initParms+="&createAssignmentFlag=<%=StringFunction.escapeQuotesforJS(createAssignmentFlag)%>";
      initParms+="&conversionCenter=<%=StringFunction.escapeQuotesforJS(conversionCenter)%>";
      initParms+="&isFromTemplateCreation=<%=StringFunction.escapeQuotesforJS(createTemplFromInstrFlag)%>";
      
      var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("TradeSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
      var myGrid = t360grid.createDataGrid("tradeSearchPopupGrid",viewName,gridLayout,initParms);
    
      myGrid.set('autoHeight',10); 
    }
    else if (createTracerFlag=="Y"){
      var instrumentType = "<%=InstrumentType.EXPORT_COL%>";
        
      var initParms='';<%-- "instrumentType="+instrumentType; --%>
      initParms+="&statusActive=Active";
      initParms+="&createTracerFlag=<%=StringFunction.escapeQuotesforJS(createTracerFlag)%>";
      initParms+="&createAmendmentFlag=<%=StringFunction.escapeQuotesforJS(createAmendmentFlag)%>";
      initParms+="&createAssignmentFlag=<%=StringFunction.escapeQuotesforJS(createAssignmentFlag)%>";
      initParms+="&isFromTemplateCreation=<%=StringFunction.escapeQuotesforJS(createTemplFromInstrFlag)%>";
      
      var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("TradeSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
      var myGrid = t360grid.createDataGrid("tradeSearchPopupGrid",viewName,gridLayout,initParms);
    
      myGrid.set('autoHeight',10); 
    }
    else{
      <%-- Spenke T36000006422 10/15/2012 Begin  --%>
    
      var initParms="instrumentType=<%=searchCondition%>";
      initParms+="&tradeInstrTypes=<%=tradeInstrTypes.toString()%>";
      initParms+="&confInd=<%=confInd%>";
    <%-- Rel 9.2 XSS - CID 11317 --%>
      initParms+="&showChildInstruments=<%=StringFunction.escapeQuotesforJS(showChildInstruments)%>";
      initParms+="&statusActive=Active";
      initParms+="&createTracerFlag=<%=StringFunction.escapeQuotesforJS(createTracerFlag)%>";
      initParms+="&createAmendmentFlag=<%=StringFunction.escapeQuotesforJS(createAmendmentFlag)%>";
      initParms+="&createAssignmentFlag=<%=StringFunction.escapeQuotesforJS(createAssignmentFlag)%>";
	  initParms+="&conversionCenter=<%=StringFunction.escapeQuotesforJS(conversionCenter)%>";
	  initParms+="&isFromTemplateCreation=<%=StringFunction.escapeQuotesforJS(createTemplFromInstrFlag)%>";
	  initParms+="&createSettlementInstructionsFlag=<%= StringFunction.escapeQuotesforJS(createSettlementInstructionsFlag)%>";
	  initParms+="&requestRolloverFlag=<%= StringFunction.escapeQuotesforJS(requestRolloverFlag)%>";
	  initParms+="&allowImpCol=<%= StringFunction.escapeQuotesforJS(allowImpCol)%>";
	  initParms+="&allowImportDLC=<%= StringFunction.escapeQuotesforJS(allowImportDLC)%>";
	  initParms+="&allowLoanRequest=<%= StringFunction.escapeQuotesforJS(allowLoanRequest)%>";
      
	  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("TradeSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
      var myGrid = t360grid.createDataGrid("tradeSearchPopupGrid",viewName,gridLayout,initParms);
      <%-- cquinton 10/16/2012 ir#6448 --%>
      <%-- because its on a dialog explicitly set to 10 rows so it doesn't take over the browser --%>
      myGrid.set('autoHeight',10); 
      <%-- Spenke T36000006422 10/15/2012 End --%>
    }
    <%-- Jyoti IR6117 Ended --%>
  });

  var rowKeys = "";
  var createAmendmentFlag = '<%=StringFunction.escapeQuotesforJS(createAmendmentFlag)%>';
  var createTracerFlag = '<%=StringFunction.escapeQuotesforJS(createTracerFlag)%>';
  <%-- Rel 9.2 XSS - CID 11332, 11439, 11453 --%>
  var createNewInstrumentFlag = '<%=StringFunction.escapeQuotesforJS(createNewInstrumentFlag)%>';
  var createTemplFromInstrFlag = '<%=StringFunction.escapeQuotesforJS(createTemplFromInstrFlag)%>';
  var searchInstrumentFlag = '<%=StringFunction.escapeQuotesforJS(searchInstrumentFlag)%>';
  var createAssignmentFlag = '<%=StringFunction.escapeQuotesforJS(createAssignmentFlag)%>';
  var createSettlementInstructionsFlag  =  '<%=StringFunction.escapeQuotesforJS(createSettlementInstructionsFlag)%>';
  var requestRolloverFlag =  '<%=StringFunction.escapeQuotesforJS(requestRolloverFlag)%>';

  function filterInstrumentsOnEnter(fieldId, filterType){
    <%-- 
     * The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
      --%>
    require(["dojo/on","dijit/registry"],function(on, registry) {
      on(registry.byId(fieldId), "keypress", function(event) {
        if (event && event.keyCode == 13) {
          dojo.stopEvent(event);
          searchInstruments(filterType);
        }
      });
    });
  }
  
  var filterType = '';
  function searchInstruments(buttonId) {
    require(["dojo/dom","dojo/date/locale","dijit/registry", "dojo/domReady!"], function(dom,locale, registry){		
      if(typeof buttonId != 'undefined'){
        filterType=buttonId;
      }
	  
      var inactive = dom.byId("tradeSearchInactive").checked;
      var active = dom.byId("tradeSearchActive").checked;
      var statusActive = "";
      var statusInactive = "";
      if (inactive) 
        statusInactive = "Inactive";
      else
        statusInactive = "";
      if (active) 
        statusActive = "Active";
      else
        statusActive = "";			
					
      var searchParms="confInd=<%=confInd%>";
      <%-- Spenke T36000006422 10/15/2012 Begin --%>
      searchParms+="&showChildInstruments=<%=StringFunction.escapeQuotesforJS(showChildInstruments)%>";
      <%-- Spenke T36000006422 10/15/2012 End --%>
      searchParms+="&statusActive="+statusActive+"&statusInactive="+statusInactive;
        
      if(filterType=="Advanced"){
        var instrumentType='';
        	
        if(createAssignmentFlag=='Y')
          instrumentType='<%=InstrumentType.EXPORT_DLC%>';
        else
          instrumentType = registry.byId("tradeSearchInstrumentTypeAdvance").value;
        	
        <%-- Spenke T36000006422 10/15/2012 Begin --%>
        if(instrumentType == ""){
          instrumentType="<%=searchCondition%>";
        }
        <%-- Spenke T36000006422 10/15/2012 End --%>
        var currency = registry.byId("tradeSearchCurrencyAdvance").value;
        var amountFrom = registry.byId("tradeSearchAmountFromAdvance").get('value');
        var amountTo = registry.byId("tradeSearchAmountToAdvance").get('value');
       
        if (isNaN(amountFrom))
          amountFrom='';

        if (isNaN(amountTo))
          amountTo='';
        
        var dateFrom;
        var dateTo;		        
        if(dom.byId("tradeSearchDateFromAdvance").value != "")
          dateFrom = locale.format(registry.byId("tradeSearchDateFromAdvance").value, {selector: "date", datePattern: "MM/dd/yyyy"});
        else
          dateFrom = "";

        if(dom.byId("tradeSearchDateToAdvance").value != "")
          dateTo = locale.format(registry.byId("tradeSearchDateToAdvance").value, {selector: "date", datePattern: "MM/dd/yyyy"});
        else
          dateTo = "";

        var otherParty = dom.byId("tradeSearchOtherPartyAdvance").value;
        var vendorId = dom.byId("tradeSearchVendorIdAdvance").value;		        
				
        searchParms+="&instrumentType="+instrumentType;
        searchParms+="&currency="+currency;
        searchParms+="&amountFrom="+amountFrom;
        searchParms+="&amountTo="+amountTo;
        searchParms+="&dateFrom="+dateFrom;
        searchParms+="&dateTo="+dateTo;
        searchParms+="&otherParty="+otherParty;
        searchParms+="&vendorId="+vendorId;
        searchParms+="&searchType=Advanced";
		searchParms+="&conversionCenter=<%=StringFunction.escapeQuotesforJS(conversionCenter)%>";		
        registry.byId('tradeSearchAmountFromAdvance').set("currency", currency);
        registry.byId('tradeSearchAmountToAdvance').set("currency", currency);
      }
      else if(filterType=="Basic"){
        var instrumentType='';
	        	
        if(createAssignmentFlag=='Y')
          instrumentType='<%=InstrumentType.EXPORT_DLC%>';
        else
          instrumentType = registry.byId("tradeSearchInstrumentTypeBasic").value;
	        	
        <%-- Spenke T36000006422 10/15/2012 Begin --%>
        if(instrumentType == ""){
          instrumentType="<%=searchCondition%>";
        }
        <%-- Spenke T36000006422 10/15/2012 End --%>
        var instrumentId = dom.byId("tradeSearchInstrumentIdBasic").value;
        var refNum = dom.byId("tradeSearchRefNumBasic").value;
        var bankRefNum = dom.byId("tradeSearchBankRefNumBasic").value;
        var vendorId = dom.byId("tradeSearchVendorIdBasic").value;
            
        searchParms+="&instrumentType="+instrumentType;
        searchParms+="&instrumentId="+instrumentId;
        searchParms+="&refNum="+refNum;
        if(custNotIntgWithTPS){
        	 var bankInstrumentId = dom.byId("tradeSearchBankInstIdBasic").value;
        	 searchParms+="&bankInstrumentId="+bankInstrumentId;
        }
        searchParms+="&bankRefNum="+bankRefNum;
        searchParms+="&vendorId="+vendorId;
        searchParms+="&searchType=Basic";
      }
      <%-- cquinton 3/6/2013 ir#14655 remove unused vars --%>
      <%-- cquinton 3/6/2013 ir#14655 remove fromDocCache - it is unused --%>
      <%--  and sends xml which is restricted by UB sso --%>
      searchParms+="&createAmendmentFlag=<%= StringFunction.escapeQuotesforJS(createAmendmentFlag)%>";
      searchParms+="&createTracerFlag=<%= StringFunction.escapeQuotesforJS(createTracerFlag)%>";
      searchParms+="&createAssignmentFlag=<%= StringFunction.escapeQuotesforJS(createAssignmentFlag)%>";
      searchParms+="&tradeInstrTypes=<%=tradeInstrTypes.toString()%>";
	  searchParms+="&conversionCenter=<%= StringFunction.escapeQuotesforJS(conversionCenter)%>";	 
	  searchParms+="&isFromTemplateCreation=<%= StringFunction.escapeQuotesforJS(createTemplFromInstrFlag)%>";
	  searchParms+="&createSettlementInstructionsFlag=<%= StringFunction.escapeQuotesforJS(createSettlementInstructionsFlag)%>";
	  searchParms+="&requestRolloverFlag=<%= StringFunction.escapeQuotesforJS(requestRolloverFlag)%>";
	  searchParms+="&allowImpCol=<%= StringFunction.escapeQuotesforJS(allowImpCol)%>";
	  searchParms+="&allowImportDLC=<%= StringFunction.escapeQuotesforJS(allowImportDLC)%>";
	  searchParms+="&allowLoanRequest=<%= StringFunction.escapeQuotesforJS(allowLoanRequest)%>";
      
      searchDataGrid("tradeSearchPopupGrid", "TradeSearchDataView", searchParms);
    });
  }
<%-- Code modified by Sandeep 11-04-2012 - End --%>

  function selectInstrument(bankBranchArray) {
	    <%-- todo: add external event handler to datagrid footer items --%>
	    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
	        function(registry, query, on, dialog ) {
	      <%--get array of rowkeys from the grid--%>
	      rowKeys = getSelectedGridRowKeys("tradeSearchPopupGrid");
	      if(rowKeys == ""){
	  		 alert("<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("InstSearch.Select",TradePortalConstants.TEXT_BUNDLE)) %>");
	  		return;
	  	}
	   if(createAmendmentFlag == 'Y' || createTemplFromInstrFlag == 'Y' || searchInstrumentFlag == 'Y' || 
			   createTracerFlag =='Y' || createAssignmentFlag =='Y' || createSettlementInstructionsFlag == 'Y' || requestRolloverFlag == 'Y'){   
		   <%-- Rel 9.2 XSS - CID 11387 --%>
		   dialog.doCallback('<%=StringFunction.escapeQuotesforJS(instrumentSearchDialogId)%>', 'select',rowKeys[0]);
	      
	   }else if (typeof bankBranchArray != 'undefined' && bankBranchArray.length == 1 ) {
		   createNewInstrumentBankBranchSelectedValue(bankBranchArray[0]);
			}else{
			   dialog.open('bankBranchSelectorDialog', bankBranchSelectorDialogTitle,
	                   'bankBranchSelectorDialog.jsp',
	                   null, null, <%-- parameters --%>
	                   'select', this.createNewInstrumentBankBranchSelectedValue);
		   } 
	    });
	  }
    
  function createNewInstrumentBankBranchSelectedValue(bankBranchOid) {
				if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
			        var theForm = document.getElementsByName('NewInstrumentForm')[0];
			      	theForm.copyInstrumentOid.value=rowKeys[0];
			      	theForm.bankBranch.value=bankBranchOid;
			      	theForm.mode.value='CREATE_NEW_INSTRUMENT';
			        theForm.copyType.value='Instr';
			      	theForm.submit();
			      } else {
			        	alert('Problem in createNewInstrument: cannot find ExistingInstrumentsForm');
			      }
	}
  var basicSearchEnabled = false;
  if(basicSearchEnabled==true)
  	buttonId="Basic";
  else
  	buttonId="Advanced";
  function shuffleFilter(linkValue){
    require(["dojo/dom","dijit/registry","dojo/domReady!"],
        function(dom, registry){
      if(linkValue=='Advanced'){
        dom.byId("tradeAdvancedInstrumentFilter").style.display='block';
        dom.byId("tradeBasicInstrumentFilter").style.display='none';
	  				
        <%-- clearing Basic Filter --%>
        registry.byId("tradeSearchInstrumentTypeBasic").set('value',"");
        registry.byId("tradeSearchInstrumentIdBasic").value="";
        registry.byId("tradeSearchRefNumBasic").value="";
        registry.byId("tradeSearchBankRefNumBasic").value="";
        registry.byId("tradeSearchVendorIdBasic").value="";
        if(custNotIntgWithTPS){
       		 registry.byId("tradeSearchBankInstIdBasic").value="";
        }
    	basicSearchEnabled=false;
      }
	  			
      if(linkValue=='Basic'){
        dom.byId("tradeAdvancedInstrumentFilter").style.display='none';
        dom.byId("tradeBasicInstrumentFilter").style.display='block';
  	  				
        <%-- clearing Advance Filter --%>
        registry.byId("tradeSearchInstrumentTypeAdvance").set('value',"");
        registry.byId("tradeSearchCurrencyAdvance").set('value',"");
        registry.byId("tradeSearchAmountFromAdvance").value="";
        registry.byId("tradeSearchAmountToAdvance").value="";
        registry.byId("tradeSearchOtherPartyAdvance").value="";
        registry.byId("tradeSearchVendorIdAdvance").value="";	
        registry.byId("tradeSearchDateFromAdvance").reset();
        registry.byId("tradeSearchDateToAdvance").reset();
    	basicSearchEnabled=true;
      }
    });
  }
	
  require(["dojo/dom","dojo/domReady!" ], function(dom) {
 dom.byId("tradeAdvancedInstrumentFilter").style.display='none';
  });
	
	  function hideGrid() {
	    <%-- todo: add external event handler to datagrid footer items --%>
	    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
	        function(registry, query, on, dialog ) {
	      dialog.hide('instrumentSearchDialog');
	    });
	  }

  var dialogName=dijit.byId("instrumentSearchDialog");
  var dialogCloseButton=dialogName.closeButtonNode;
  dialogCloseButton.setAttribute("id","instrumentSearchDialogCloseLink");
  dialogCloseButton.setAttribute("title","");
</script>
<%=widgetFactory.createHoverHelp("instrumentSearchDialogCloseLink", "common.Cancel") %>
