<%--
 *  Instrument Search Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 


//read in parameters
  String instrumentSearchDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
//This flag is set to 'Y', when the Trade Search dialog is called to Create Amendment.
//In this case, there is no need to open the Bank/Branch dialog
  String createAmendmentFlag = StringFunction.xssCharsToHtml(request.getParameter("createAmendmentFlag")); 
  String createTracerFlag = StringFunction.xssCharsToHtml(request.getParameter("createTracerFlag")); 
//This flag is set to 'Y', when the Trade Search Dialog is called to Copy an instrument from an existing instrument.
  String createNewInstrumentFlag = StringFunction.xssCharsToHtml(request.getParameter("createNewInstrumentFlag"));

//This flag is set to 'Y', when the Trade Search Dialog is called to Create a Template from existing instrument.
  String createTemplFromInstrFlag = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(request.getParameter("createTemplFromInstrFlag")));

//This flag is set to 'Y', when the Trade Search Dialog is called to search for an Instrument.
  String searchInstrumentFlag = StringFunction.xssCharsToHtml(request.getParameter("searchInstrumentFlag"));
  
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  Debug.debug("***START********************INSTRUMENT SEARCH  DIALOG ID **************************START***");

  boolean newTemplate = true;     // indicates what page we came from (i.e.,
                                  // doing new template or new transaction)
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  String options;
  String defaultText;

  // Parameters that drive logic for this page.
  String selectMode;
  String cancelAction;
  String prevStepAction;
  String nextStepForm;
  String searchCondition = "";
  String copyType;
  String titleKey;
  String bankBranch;
  String showNavBar;
  String showChildInstruments;
 
  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'"; 

  StringBuffer dynamicWhere = new StringBuffer();
  StringBuffer newSearchCriteria = new StringBuffer();

  String       NewDropdownOptions = "";
  StringBuffer statusOptions   = new StringBuffer();
  StringBuffer statusExtraTags = new StringBuffer();
  String       selectedStatus  = "";
  String       gridHtml        = "";
  String       gridLayout      = "";
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);

  final String STATUS_ALL      = resMgr.getText("common.StatusAll",
                                                TradePortalConstants.TEXT_BUNDLE);
  final String STATUS_ACTIVE   = resMgr.getText("common.StatusActive",
                                                TradePortalConstants.TEXT_BUNDLE);
  final String STATUS_INACTIVE = resMgr.getText("common.StatusInactive",
                                                TradePortalConstants.TEXT_BUNDLE);
  // W Zhu 6/8/07 POUH060841116 Add STATUS_STRICTLY_ACTIVE
  // This status is passed in from invoking page directly and will not be part of
  // the visible Option field so we do not need to get from language-specific
  // text resource file.
  final String STATUS_STRICTLY_ACTIVE = "Strictly Active";


// PPRAKASH IR NGUJ013070808 Add Begin
  CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  corpOrg.getById(userSession.getOwnerOrgOid());
  //PPRAKASH IR NGUJ013070808 Add End
  
  // Based on the search type, read in the search parameters the user may have
  // previously used.  We use these to build the dynamic search criteria and
  // also to redisplay to the user

  String instrumentId = "";
  String refNum = "";
  String bankRefNum = "";
  String vendorId = "";
  String instrumentType = "";
  String currency = "";
  String amountFrom = "";
  String amountTo = "";
  String otherParty = "";
  String dayFrom = "";
  String monthFrom = "";
  String yearFrom = "";
  String dayTo = "";
  String monthTo = "";
  String yearTo = "";
  String helpSensitiveLink  = null;
  String itemid 			= StringFunction.xssCharsToHtml(request.getParameter("itemid"));
  String sectiontype = StringFunction.xssCharsToHtml(request.getParameter("section"));
  String custID = StringFunction.xssCharsToHtml(request.getParameter("custID"));
  String searchType = "";
  Vector instrumentTypes = null;
  boolean showInstrumentDropDown = false;
  String mode = "";
  String bankInstrumentId = "";
   if(StringFunction.isNotBlank(custID)){
	   custID = EncryptDecrypt.encryptStringUsingTripleDes(custID, userSession.getSecretKey());
   }
  
  DocumentHandler doc = null;
  // First read in the parameters that drive how the page operates.

  cancelAction = StringFunction.xssCharsToHtml(request.getParameter("CancelAction"));
  if (cancelAction == null)
     Debug.debug("***Warning: CancelAction parameter not specified");

  prevStepAction = StringFunction.xssCharsToHtml(request.getParameter("PrevStepAction"));

  nextStepForm = StringFunction.xssCharsToHtml(request.getParameter("NextStepForm"));
  if (nextStepForm == null)
     Debug.debug("***Warning: NextStepForm parameter not specified");

  selectMode = StringFunction.xssCharsToHtml(request.getParameter("SelectMode"));
  if (selectMode == null) selectMode = TradePortalConstants.INDICATOR_NO;

  showNavBar = StringFunction.xssCharsToHtml(request.getParameter("ShowNavBar"));
  if (showNavBar == null)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  // Indicates whether or not instruments belonging to the child organization 
  // of the user's organization should be displayed in the list
  showChildInstruments = StringFunction.xssCharsToHtml(request.getParameter("ShowChildInstruments"));
  if(showChildInstruments == null)
   {
      showChildInstruments = TradePortalConstants.INDICATOR_NO;
   }


  // Check if this is a new Search (based on passed in NewSearch parameter)
  // If so, clear the listview information so it does not
  // retain any page/sort order/ sort column/ search type information 
  // resulting from previous searches
	
  // A new search can also result from the corp org or status type dropdown 
  // being reselected. In this case, NewDropdownSearch parameter indicates a 
  // pseudo new search -- the search type is retained but the rest of the 
  // search clause is not

  String newSearch = StringFunction.xssCharsToHtml(request.getParameter("NewSearch"));
  String newDropdownSearch = StringFunction.xssCharsToHtml(request.getParameter("NewDropdownSearch"));
  Debug.debug("New Search is " + newSearch);
  Debug.debug("New Dropdown Search is " + newDropdownSearch);

  if (newSearch != null && newSearch.equals(TradePortalConstants.INDICATOR_YES))
  {
       // Default search type for instrument status on the Instrument Search page is ALL.
      session.setAttribute("instrSearchStatusType", STATUS_ALL);
  }

  searchType = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(request.getParameter("SearchType")));

  if (searchType == null)
  {
	searchType = TradePortalConstants.SIMPLE;

    }
  
  copyType = StringFunction.xssCharsToHtml(request.getParameter("CopyType"));
  if (copyType == null) copyType = "";


  bankBranch = StringFunction.xssCharsToHtml(request.getParameter("BankBranch"));


  
  titleKey = StringFunction.xssCharsToHtml(request.getParameter("TitleKey"));
  if (titleKey == null)
  {
     titleKey = "";
  }

  /************************
   * ERSKINE'S CODE START *
   ************************/

  //If we are in the transaction area the parameters will be passed in via this document
  HttpSession theSession = request.getSession(false);
  String inTransactionArea = (String) theSession.getAttribute("inNewTransactionArea");

  if (inTransactionArea != null && inTransactionArea.equals(TradePortalConstants.INDICATOR_YES))
  {
    doc = formMgr.getFromDocCache();
    Debug.debug("Getting data from doc cache ->\n" + doc.toString(true));

	mode    = doc.getAttribute("/In/mode");
    cancelAction   = TradePortalConstants.TRANSACTION_CANCEL_ACTION;
    selectMode     = TradePortalConstants.INDICATOR_NO;
    copyType       = doc.getAttribute("/In/copyType");
    bankBranch     = doc.getAttribute("/In/bankBranch");

    // Change the title of the page based on what we're in the process of creating
    if (mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
       titleKey = "InstSearch.HeadingNewTemplateStep2";
    else
       titleKey = "InstSearch.HeadingNewTransStep2"; 

    Debug.debug("Getting data from doc cache2 ->\n" + doc.toString(true));

    if (mode != null && mode.equals(TradePortalConstants.EXISTING_INSTR))
    {
        nextStepForm = "NewTransInstrSearchForm2";
        prevStepAction = "newTransactionStep1";
    }
    else
    {
        nextStepForm = "NewTransInstrSearchForm";
        if (mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
            prevStepAction = "newTemplate";
        else
            prevStepAction = "newTransactionStep1";
 
       String isFixed = doc.getAttribute("/In/isFixed");
       String isExpress = doc.getAttribute("/In/isExpress");
       if(TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isFixed))
  	 	dynamicWhere.append(" and i.instrument_type_code = '" + InstrumentType.DOM_PMT + "'");
  	   if(TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isExpress ))
  	 	dynamicWhere.append(" and i.instrument_type_code in ('" + TradePortalConstants.SIMPLE_SLC + "'"
  	 	 + ", '" + InstrumentType.APPROVAL_TO_PAY + "'"
  	 	 + ", '" + InstrumentType.REQUEST_ADVISE + "'"
  	 	 + ", '" + InstrumentType.IMPORT_DLC + "')");  	   
 
    }

    theSession.setAttribute("prevStepAction", "goToInstrumentSearch");
    Debug.debug("Getting data from doc cache 3->\n" + doc.toString());

  }
  /************************
   *  ERSKINE'S CODE END  *
   ************************/
  else
  {
     doc = formMgr.getFromDocCache();

     if (doc.getDocumentNode("/In/InstrumentSearchInfo/SelectMode") != null)
     {
        selectMode   = doc.getAttribute("/In/InstrumentSearchInfo/SelectMode");
        cancelAction = doc.getAttribute("../CancelAction");
        nextStepForm = doc.getAttribute("../NextStepForm");
        showNavBar   = doc.getAttribute("../ShowNavBar");
        showChildInstruments = doc.getAttribute("../ShowChildInstruments");
        if(showChildInstruments == null)
          {
             showChildInstruments = TradePortalConstants.INDICATOR_NO;
          }
     }
  }


  String searchOnInstrumentType = doc.getAttribute("/In/InstrumentSearchInfo/SearchInstrumentType");

  if (searchOnInstrumentType == null)
  	searchOnInstrumentType = "";
  
  if(request.getParameter("IntType") != null)
   searchOnInstrumentType = StringFunction.xssCharsToHtml(request.getParameter("IntType"));
  
  // determine what type of search is required - this is used by 
  // SearchParms and SearchType for sqlWhereClause and Instrument Type
  // dropdowns
  if (searchOnInstrumentType.equals(InstrumentType.EXPORT_DLC))
  	searchCondition = TradePortalConstants.SEARCH_EXP_DLC_ACTIVE;

  else if (searchOnInstrumentType.equals(TradePortalConstants.ALL_EXPORT_DLC)) {
  	searchCondition = TradePortalConstants.ALL_EXPORT_DLC;
  } 
  else if (searchOnInstrumentType.equals(InstrumentType.EXPORT_COL)) {
  	searchCondition = InstrumentType.EXPORT_COL;
  }
  else if (searchOnInstrumentType.equals(InstrumentType.NEW_EXPORT_COL)) {
  	searchCondition = InstrumentType.NEW_EXPORT_COL;
  }
  else if (searchOnInstrumentType.equals(TradePortalConstants.IMPORT)) {
  	searchCondition = TradePortalConstants.IMPORT;
  }
  else if(searchOnInstrumentType.equals(InstrumentType.LOAN_RQST)) {
  	searchCondition = InstrumentType.LOAN_RQST;
   
  }
  else if(searchOnInstrumentType.equals(TradePortalConstants.IMPORTS_ONLY))
	searchCondition = TradePortalConstants.IMPORTS_ONLY;	
  else if (searchOnInstrumentType.equals(TradePortalConstants.SEARCH_ALL_INSTRUMENTS_FOR_MESSAGES)) {//Added for Rel9.0 IR#T36000028569
	    searchCondition = TradePortalConstants.SEARCH_ALL_INSTRUMENTS_FOR_MESSAGES;
	  }
  else if(mode != null && mode.equals(TradePortalConstants.EXISTING_INSTR))
  	searchCondition = TradePortalConstants.SEARCH_FOR_NEW_TRANSACTION;
  else if(mode != null && mode.equals(TradePortalConstants.NEW_INSTR))
  	searchCondition = TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT;
  else if(mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
  	searchCondition = TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT;
  else
      searchCondition = TradePortalConstants.SEARCH_ALL_INSTRUMENTS;

  Debug.debug("CancelAction is " + cancelAction);
  Debug.debug("PrevStepAction is " + prevStepAction);
  Debug.debug("NextStepForm is " + nextStepForm);
  Debug.debug("SelectMode is " + selectMode);
  Debug.debug("Mode is " + mode);
  Debug.debug("SearchType is " + searchType);
  Debug.debug("CopyType is " + copyType);
  Debug.debug("BankBranch is " + bankBranch);
  Debug.debug("ShowNavBar is " + showNavBar);
  Debug.debug("SearchCondition is " + searchCondition);
 
  // Create a link to the basic or advanced instrument search
  String linkParms;
  String linkText;
  String link;
    Debug.debug("Getting data from doc cache 5->\n");

  linkParms = "&SelectMode=" + selectMode
              + "&CancelAction=" + cancelAction
              + "&PrevStepAction=" + prevStepAction
              + "&NextStepForm=" + nextStepForm
              + "&CopyType=" + copyType
              + "&TitleKey=" + titleKey
              + "&BankBranch=" + bankBranch
              + "&ShowNavBar=" + showNavBar;
  linkText = "";

  if (searchType.equals(TradePortalConstants.ADVANCED)) {
    linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
    link = formMgr.getLinkAsUrl("goToInstrumentDialogSearch",
                                linkParms, response);
    linkText = resMgr.getText("InstSearch.BasicSearch",
                              TradePortalConstants.TEXT_BUNDLE);
    helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/instr_search.htm",
	   						"advanced",resMgr, userSession);
   } else {
    linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
    link = formMgr.getLinkAsUrl("goToInstrumentDialogSearch",
                                linkParms, response);
    linkText = resMgr.getText("InstSearch.AdvancedSearch",
                              TradePortalConstants.TEXT_BUNDLE);
    helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/instr_search.htm",
	   				"basic", resMgr, userSession);
   }


   if(showChildInstruments.equals(TradePortalConstants.INDICATOR_YES))
    {
      // Append the dynamic where clause to view the instruments of the user's 
      // organizations and all of its children.
      dynamicWhere.append(" and i.a_corp_org_oid in ( ");      
      dynamicWhere.append(" select organization_oid ");
      dynamicWhere.append(" from corporate_org ");
      dynamicWhere.append(" start with organization_oid =  ");
      dynamicWhere.append(userSession.getOwnerOrgOid());
      dynamicWhere.append(" connect by prior organization_oid = p_parent_corp_org_oid ");
      dynamicWhere.append(" ) ");
    }
   else
    {
      // Append the dynamic where clause to view only the user
      // organization's instruments
      dynamicWhere.append(" and i.a_corp_org_oid = "+  userSession.getOwnerOrgOid());
    } 

    // Based on the statusType dropdown, build the where clause to include one or more
    // instrument statuses.
    Vector statuses = new Vector();
    int numStatuses = 0;

    selectedStatus = StringFunction.xssCharsToHtml(request.getParameter("instrSearchStatusType"));

    if (selectedStatus == null) 
    {
       selectedStatus = (String) session.getAttribute("instrSearchStatusType");
    }

    // W Zhu 6/8/07 POUH060841116 Add case Strictly_active
    if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
    }
    else if (STATUS_ACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
    }

    else if (STATUS_INACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
    }

    else // default is ALL (actually not all since DELeted instruments
         // never show up (handled by the SQL in the listview XML)
    {
       selectedStatus = STATUS_ALL;
    }

    session.setAttribute("instrSearchStatusType", selectedStatus);

    // If the desired status is ALL, there's no point in building the where 
    // clause for instrument status.  Otherwise, build it using the vector
    // of statuses we just set up.

    if (!STATUS_ALL.equals(selectedStatus))
    {
       //TLE - 07/10/07 - IR-POUH060841116 - Add Begin
       //dynamicWhere.append(" and i.instrument_status in (");
       if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) ) {
           dynamicWhere.append(" and (o.transaction_status = '");
           dynamicWhere.append(TradePortalConstants.TRANS_STATUS_AUTHORIZED);
           dynamicWhere.append("' or i.instrument_status in (");
       } else {
           dynamicWhere.append(" and i.instrument_status in (");
       }
       //TLE - 07/10/07 - IR-POUH060841116 - Add End

       numStatuses = statuses.size();

       for (int i=0; i<numStatuses; i++) {
         dynamicWhere.append("'");
         dynamicWhere.append( (String) statuses.elementAt(i) );
         dynamicWhere.append("'");

         if (i < numStatuses - 1) {
            dynamicWhere.append(", ");
         }
       }

      if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) ) {
          dynamicWhere.append(")) ");
       } else {
          dynamicWhere.append(") ");
       }
    }

         //IAZ CR-586 IR- PRUK092452162 09/29/10 Begin
         UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
         thisUser.setAttribute("user_oid", userSession.getUserOid());
         thisUser.getDataFromAppServer();
         String confInd = "";
         if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))
         	confInd = TradePortalConstants.INDICATOR_YES;
         else if (userSession.getSavedUserSession() == null)
            confInd = thisUser.getAttribute("confidential_indicator");
         else   
         {
            if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
                confInd = TradePortalConstants.INDICATOR_YES;
            else	
                confInd = thisUser.getAttribute("subsid_confidential_indicator");  
         }       
          
         if (!TradePortalConstants.INDICATOR_YES.equals(confInd))
        	dynamicWhere.append(" and i.confidential_indicator = 'N'");
         //IAZ CR-586 IR-PRUK092452162 09/29/10 End
         

   // Now include a file which reads the search criteria and build the
   // dynamic where clause.
%>
   <%@ include file="/transactions/fragments/InstSearch-InstType.frag" %>
   <%@ include file="/transactions/fragments/InstSearch-SearchParms.frag" %>

<%
   // Build the status dropdown options (hardcoded in ALL (default), ACTIVE, INACTIVE order), 
   // re-selecting the most recently selected option.

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("' ");
   if (STATUS_ALL.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("' ");
   if (STATUS_ACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("' ");
   if (STATUS_INACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("</option>");

   // Upon changing the status selection, automatically go back to the Instrument Search
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)
   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToInstrumentSearch", response));
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;instrSearchStatusType='+this.options[this.selectedIndex].value\"");
%>
<script LANGUAGE="JavaScript">

  function checkForSelection() {
    <%--
    // Before going to the next step, the user must have selected one of the
    // radio buttons.  If not, give an error.  Since the selection radio button
    // is dynamic, it is possible the field doesn't even exist.  First check
    // for the existence of the field and then check if a selection was made.
    --%>

    numElements = document.InstrumentSearchResults.elements.length;
    i = 0;
    foundField = false;

    <%-- First determine if the field even exists. --%>
    for (i=0; i < numElements; i++) {
       if (document.InstrumentSearchResults.elements[i].name == "selection") {
          foundField = true;
       }
    }

    if (foundField) {
        var size=document.InstrumentSearchResults.selection.length;
        <%-- Handle the case where there are two or more radio buttons --%>
        for (i=0; i<size; i++)
        {
            if (document.InstrumentSearchResults.selection[i].checked)
            {
                return true;
            }
        }
        <%-- Handle the case where there is only one radio button --%>
        if (document.InstrumentSearchResults.selection.checked)
        {
            return true;
        }
    }
    <%-- Handle the case where no button is checked --%>
    var msg = "<%=resMgr.getText("InstSearch.SelectOne",
                                  TradePortalConstants.TEXT_BUNDLE)%>"
    alert(msg);


   <%-- narayan IR# POUJ092282592 --- Add below one line to make variable formSubmitted to FALSE
   when user hit the button Next Step, system actually submits the page and making the value of Boolean variable formSubmitted to TRUE.
   Hence after clicking to Next Step whenever user clicks on search system won't do anything (means system won't search / submit the page again)
   because it thinks that this page is already submitted and Trade Portal don't allow to form submission twice.
   --%>
    formSubmitted = false;
    return false;
  }

   <%--Enable form submission by enter.  event.which works for Netscape and event.keyCode works for IE --%>  
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  

</script>


<%
  // Store values such as the userid, his security rights, and his org in a
  // secure hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);
%>

<div id="instrumentSearchDialogContent234" class="dialogContent fullwidth">

<% 
  //W Zhu 6/8/07 POUH060841116 BEGIN
  // If the invoking page force searching for strictly active instrument,
  // do not display the Instrument Status option.
  if (!STATUS_STRICTLY_ACTIVE.equals(selectedStatus) ) {
    //W Zhu 6/8/07 POUH060841116 END
%>

  	<%--cquinton 1/8/2013 use standard classes--%>
	<div class="searchHeader">
	    <span class="searchHeaderCriteria">
		<% 
	  		StringBuffer instrumentStatus = new StringBuffer();
	  		instrumentStatus.append(widgetFactory.createInlineLabel("", "InstrumentHistory.Status"));
	  		instrumentStatus.append(widgetFactory.createCheckboxField("Active", "common.StatusActive", true, false, false, "onClick='searchInstrumentIDs();'","","none"));
	  		instrumentStatus.append(widgetFactory.createCheckboxField("Inactive", "common.StatusInactive", false, false,false,"onClick='searchInstrumentIDs();'","","none"));
	  		widgetFactory.wrapSearchItem(instrumentStatus,"");
		%>	
	      <%= instrumentStatus %>
	    </span>
	    <div style="clear:both;"></div>
	</div>
	<div class="searchDivider"></div>  

<%
  } 
%>

	<% //IR-45289 11/02/2015 Rel-9.3.5
	if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()) && "BankMailMessageDetail".equals(formMgr.getCurrPage())) {%>
		<div id="bankInstrumentFilter">
    		<div class="searchDetail">
      			<span class="searchCriteria">
 					<%=widgetFactory.createSearchTextField("InstrumentIdBank","InstSearch.InstrumentID","16", "onKeydown='Javascript: filterInstrumentIDsOnEnter(\"InstrumentIdBank\", \"Bank\");' class='char15'")%>
	<% 
  				if (showInstrumentDropDown) {
    				options = Dropdown.getInstrumentList(instrumentType,loginLocale, instrumentTypes );
	%>
        			<%=widgetFactory.createSearchSelectField("InstrumentTypeBank","InstSearch.InstrumentType"," ", options, "onChange='searchInstrumentIDs(\"Bank\");' class='char15'")%>
	<% 
  				} else {
 					// Based on search condition (for specific instrument type searches only),
      				// print the name of the instrument type.  Also store this value so it is 
     				// picked up on return to the page.
      				String instrTypeText = "";
      				if (searchCondition.equals(TradePortalConstants.SEARCH_EXP_DLC_ACTIVE)){
         				instrTypeText = "CorpCust.ExportLC";
      				}else if (searchCondition.equals(InstrumentType.LOAN_RQST)){
         				instrTypeText = "CorpCust.LoanRequests";
      				}else{
         				instrTypeText = instrumentType;
      				}
   	%>
        			<%=widgetFactory.createSubLabel("InstSearch.InstrumentType")%>
        			<b> <%=resMgr.getText(StringFunction.xssCharsToHtml(instrTypeText), TradePortalConstants.TEXT_BUNDLE)%> </b>
	<%
	  				out.println("<input type=hidden name=InstrumentType value='");
      				out.println(StringFunction.xssCharsToHtml(instrumentType));
      				out.println("'>");
  				}
	%> 					
 					<%=widgetFactory.createSearchTextField("BankInstId","InstSearch.BankInstrumentID","16", "onKeydown='Javascript: filterInstrumentIDsOnEnter(\"BankInstId\", \"Bank\");' class='char15'")%>
 				</span>
 				<span class="searchActions">
			        <button data-dojo-type="dijit.form.Button" type="button" id="bankTradeSearchButton">
			          	<%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
			          	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            				searchInstrumentIDs("Bank");return false;
          				</script>
			        </button>
        			<%=widgetFactory.createHoverHelp("basicTradeSearchButton","SearchHoverText") %>
      			</span>
      			<div style="clear:both;"></div>
 			</div>
 		</div>
    <%}else{%>
		<div id="advancedInstrumentFilter" style="display:none;">
    		<div class="searchDetail">
      			<span class="searchCriteria">
	<%
				if (showInstrumentDropDown){
			// Build the option tags for the dropdown box.
				options = Dropdown.getInstrumentList( instrumentType, loginLocale, instrumentTypes );
	%>
        			<%=widgetFactory.createSearchSelectField("InstrumentTypeAdvance","InstSearch.InstrumentType"," ", options, "onChange='searchInstrumentIDs(\"Advanced\");' class='char15'")%>
	<%			
				}else{
	%>
	        		<%=widgetFactory.createSubLabel("InstSearch.InstrumentType")%>
	        		<b> <%=resMgr.getText("CorpCust.ExportLC", TradePortalConstants.TEXT_BUNDLE)%> </b>
	        		<input type ="hidden" name="instrumentType" id="instrumentType" value="<%=InstrumentType.EXPORT_DLC%>" >
	<%	
				}	
	%>
	    			<% options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale); %>
        			<%=widgetFactory.createSearchSelectField("Currency","InstSearch.Currency"," ", options, "onChange='searchInstrumentIDs(\"Advanced\");' class='char21'")%>
					<br/>
        			<div class="searchItem">
          				<span style="margin-left:13px"></span>
          				<%=widgetFactory.createInlineLabel("AmountFrom", "InstSearch.AmountFrom")%>
          				<%=widgetFactory.createAmountField("AmountFrom","","","",false,false,false,"onKeydown='Javascript: filterInstrumentIDsOnEnter(\"AmountFrom\", \"Advanced\");' class='char12'","","none")%>
        			</div>
        
        			<div class="searchItem">
                  		<span style="margin-left:35px"></span>
          				<%=widgetFactory.createInlineLabel("AmountTo", "InstSearch.To")%>
         				<%=widgetFactory.createAmountField("AmountTo","","","",false,false,false,"onKeydown='Javascript: filterInstrumentIDsOnEnter(\"AmountTo\", \"Advanced\");' class='char12'","","none")%>
        			</div>
      			</span>
      			<span class="searchActions">
        			<button data-dojo-type="dijit.form.Button" type="button" id="advancedTradeSearchButton">
          				<%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
          				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            				searchInstrumentIDs("Advanced");return false;
          				</script>
        			</button>
        			<%=widgetFactory.createHoverHelp("advancedTradeSearchButton","SearchHoverText") %>
        			<br>
        			<span id="basicTradeSearchLink" class="searchTypeToggle"><a href="javascript:shuffleFilter('Basic');">Basic</a></span>
        			<%=widgetFactory.createHoverHelp("basicTradeSearchLink","common.BasicSearchHypertextLink") %>
      			</span>
      			<div style="clear:both;"></div>
    		</div>

    		<div class="searchDetail lastSearchDetail">
      			<span class="searchCriteria">
        			<%=widgetFactory.createSearchDateField("DateFrom","InstSearch.ExpiryDateFrom","onChange='searchInstrumentIDs(\"Advanced\");' class='char10'","constraints:{datePattern:'"+datePattern+"'},"+dateWidgetOptions)%>
        			<%=widgetFactory.createSearchDateField("DateTo","InstSearch.To","onChange='searchInstrumentIDs(\"Advanced\");' class='char10'","constraints:{datePattern:'"+datePattern+"'},"+dateWidgetOptions)%>
        			<%=widgetFactory.createSearchTextField("OtherParty","InstSearch.Party","30","onKeydown='Javascript: filterInstrumentIDsOnEnter(\"OtherParty\", \"Advanced\");' class='char15'")%>
        			<%=widgetFactory.createSearchTextField("VendorIdAdvance","InstSearch.VendorId","15","onKeydown='Javascript: filterInstrumentIDsOnEnter(\"VendorIdAdvance\", \"Advanced\");' class='char15'")%>			  
      			</span>
      			<div style="clear:both;"></div>
    		</div> 
  		</div>
    
  		<input type="hidden" name="NewSearch" value="Y">

  		<div id="basicInstrumentFilter"> 
		    <div class="searchDetail">
      			<span class="searchCriteria">
        			<%=widgetFactory.createSearchTextField("InstrumentIdBasic","InstSearch.InstrumentID","16", "onKeydown='Javascript: filterInstrumentIDsOnEnter(\"InstrumentIdBasic\", \"Basic\");' class='char15'")%>
	<% 
  				if (showInstrumentDropDown) {
    				options = Dropdown.getInstrumentList(instrumentType,loginLocale, instrumentTypes );
	%>
        			<%=widgetFactory.createSearchSelectField("InstrumentTypeBasic","InstSearch.InstrumentType"," ", options, "onChange='searchInstrumentIDs(\"Basic\");' class='char15'")%>
	<% 
  				} else {
 					// Based on search condition (for specific instrument type searches only),
      				// print the name of the instrument type.  Also store this value so it is 
     				// picked up on return to the page.
      				String instrTypeText = "";
      				if (searchCondition.equals(TradePortalConstants.SEARCH_EXP_DLC_ACTIVE)){
         				instrTypeText = "CorpCust.ExportLC";
      				}else if (searchCondition.equals(InstrumentType.LOAN_RQST)){
         				instrTypeText = "CorpCust.LoanRequests";
      				}else{
         				instrTypeText = instrumentType;
      				}
   	%>
        			<%=widgetFactory.createSubLabel("InstSearch.InstrumentType")%>
        			<b> <%=resMgr.getText(StringFunction.xssCharsToHtml(instrTypeText), TradePortalConstants.TEXT_BUNDLE)%> </b>
	<%
	  				out.println("<input type=hidden name=InstrumentType value='");
      				out.println(StringFunction.xssCharsToHtml(instrumentType));
      				out.println("'>");
  				}
	%>
        			<%=widgetFactory.createSearchTextField("RefNum","InstSearch.ApplRefNumber","30","onKeydown='Javascript: filterInstrumentIDsOnEnter(\"RefNum\", \"Basic\");' class='char15'")%>
		      	</span>

				<span class="searchActions">
			        <button data-dojo-type="dijit.form.Button" type="button" id="basicTradeSearchButton">
			          	<%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
			          	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            				searchInstrumentIDs("Basic");return false;
          				</script>
			        </button>
        			<%=widgetFactory.createHoverHelp("basicTradeSearchButton","SearchHoverText") %>
        			<br>
       				<span id="advanceTradeSearchLink" class="searchTypeToggle"><a href="javascript:shuffleFilter('Advanced');"><%=resMgr.getText("ExistingDirectDebitSearch.Advanced", TradePortalConstants.TEXT_BUNDLE)%></a></span>
        			<%=widgetFactory.createHoverHelp("advanceTradeSearchLink","common.AdvancedSearchHypertextLink") %>
      			</span>
      			<div style="clear:both;"></div>
    		</div>
    		<div class="searchDetail lastSearchDetail">
     			<span class="searchCriteria">
        			<%=widgetFactory.createSearchTextField("BankRefNum","InstSearch.OrigBanksRefNo","30","onKeydown='Javascript: filterInstrumentIDsOnEnter(\"BankRefNum\", \"Basic\");' class='char16'")%>
        			<%=widgetFactory.createSearchTextField("VendorIdBasic","InstSearch.VendorId","15","onKeydown='Javascript: filterInstrumentIDsOnEnter(\"VendorIdBasic\", \"Basic\");' class='char16'")%>
        			<%if(userSession.isCustNotIntgTPS()){ %>
        				<%=widgetFactory.createSearchTextField("BankInstId","InstSearch.BankInstrumentID","16", "onKeydown='Javascript: filterInstrumentIDsOnEnter(\"BankInstId\", \"Bank\");' class='char15'")%>
        			<%} %>
      			</span>
      			<div style="clear:both;"></div>
    		</div>
  		</div>
	<%
    }//end of else -- if (userSession.isCustNotIntgTPS())
    	
  		StringBuffer tradeInstrTypes = new StringBuffer();
  		for(int i=0; i<instrumentTypes.size(); i++){
    		tradeInstrTypes.append((String)instrumentTypes.elementAt(i));
		
    		if(i<(instrumentTypes.size()-1)){
      			tradeInstrTypes.append(",");
    		}
  		}

  		gridHtml = dgFactory.createDataGrid("tradeSearchGrid","TradeSearchDataGrid", null);
  		gridLayout = dgFactory.createGridLayout("tradeSearchGrid", "TradeSearchDataGrid"); 
	%>
  	<%=gridHtml%>
</div>

	<form name="InstrumentSearchResults" method="post" action="<%=formMgr.getSubmitAction(response)%>">
  		<input type="hidden" value="" name=buttonName>
  		<input type="hidden" name="SelectMode" value="<%=StringFunction.xssCharsToHtml(selectMode)%>">
  		<input type="hidden" name="CancelAction" value="<%=StringFunction.xssCharsToHtml(cancelAction)%>">
  		<input type="hidden" name="PrevStepAction" value="<%=StringFunction.xssCharsToHtml(prevStepAction)%>">
  		<input type="hidden" name="NextStepForm" value="<%=StringFunction.xssCharsToHtml(nextStepForm)%>">
  		<input type="hidden" name="SearchType" value="<%=searchType%>">
  		<input type="hidden" name="CopyType" value="<%=StringFunction.xssCharsToHtml(copyType)%>">
  		<input type="hidden" name="TitleKey" value="<%=StringFunction.xssCharsToHtml(titleKey)%>">
  		<input type="hidden" name="BankBranch" value="<%=StringFunction.xssCharsToHtml(bankBranch)%>">
  		<input type="hidden" name="ShowNavBar" value="<%=StringFunction.xssCharsToHtml(showNavBar)%>">
	</form>

<script type="text/javascript">

  require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {
	var instrumentType = "<%=InstrumentType.EXPORT_COL%>";
    var gridLayout = <%=gridLayout%>;   
	var initParms="instrumentType=<%=searchCondition%>";	
	initParms+="&confInd=<%=StringFunction.escapeQuotesforJS(confInd)%>";
    initParms+="&showChildInstruments=<%=StringFunction.escapeQuotesforJS(showChildInstruments)%>";
    initParms+="&tradeInstrTypes=<%=tradeInstrTypes.toString()%>";
    initParms+="&statusActive=Active";
    initParms+="&instrumentIDSearchFlag=true";
    initParms+="&addEXPOCO=<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(addEXPOCO))%>";
    initParms+="&custID=<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(custID))%>";
    console.log("initParms: "+initParms);
	
    
    var myGrid = '';
	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("TradeSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
	if(typeof instrumentType != "undefined" ) {
		myGrid = t360grid.createDataGrid("tradeSearchGrid",viewName,gridLayout,initParms);
	}else{
		myGrid = t360grid.createDataGrid("tradeSearchGrid",viewName,gridLayout,"");	
	}
	
	myGrid.set('autoHeight',10); 
  });
  var rowKeys = "";
  var createAmendmentFlag = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(createAmendmentFlag))%>';
  var createTracerFlag =  '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(createTracerFlag))%>';
  var createNewInstrumentFlag = "<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(createNewInstrumentFlag))%>";
  var createTemplFromInstrFlag = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(createTemplFromInstrFlag))%>';
  var searchInstrumentFlag = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(searchInstrumentFlag))%>';
  
  function selectInstrument() {
	  
	
	  require(["dojo/ready","dijit/registry","dojo/_base/array","dojo/query","dojo/domReady!","dojo/on","t360/dialog"], 
				function(ready,registry,baseArray,query,dom, on, dialog) {
		
			myGrid = registry.byId("tradeSearchGrid");
			items = myGrid.selection.getSelected();
			var selection = getSelectedGridRowKeys('tradeSearchGrid');		
			var itemsObj = String("<%= StringFunction.escapeQuotesforJS(itemid)%>");
			
			var itemsArr = itemsObj.split(',');
			 if(selection == ""){
			  				alert("Please select a party or press 'Cancel'");
			  				return;
			  			}
			<%--  get the data of each column of selected grid and set the data to fields in the form --%>
			var arr = [];
						
			 arr[0] = items[0].i.rowKey;
			 arr[1] = items[0].i.InstrumentID;
			 
			<%
			if(((TradePortalConstants.OWNER_BOG.equals(userSession. getOwnershipLevel()) || TradePortalConstants. OWNER_BANK.equals(userSession. getOwnershipLevel())) && 
                    userSession. isEnableAdminUpdateInd()) ||  userSession.isCustNotIntgTPS())
 {%>
					arr[2] = items[0].i.BankInstrumentID;				
			<%} %>
			
			matchPartyPatternsByPageName(itemsArr, arr, section);
			dialog.hide('instrumentSearchDialog');
	
			});
	
	   
	  }
    
  function createNewInstrumentBankBranchSelectedValue(bankBranchOid) {
		console.log("inside createNewInstrumentBankBranchSelectedValue() - bankBranchOid: "+bankBranchOid);
		console.log("rowkeys: "+rowKeys+"==="+rowKeys[0])
		
		
				if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
			        var theForm = document.getElementsByName('NewInstrumentForm')[0];
			      	theForm.copyInstrumentOid.value=rowKeys[0];
			      	theForm.bankBranch.value=bankBranchOid;
			      	theForm.mode.value='CREATE_NEW_INSTRUMENT';
			        theForm.copyType.value='Instr';
		    	 	    	
			    	console.log("theForm.copyInstrumentOid.value: "+theForm.copyInstrumentOid.value);
			  		console.log("theForm.bankBranch.value: "+theForm.bankBranch.value);
			    		  
			      	theForm.submit();
			      } else {
			        	alert('Problem in createNewInstrument: cannot find ExistingInstrumentsForm');
			      }
		
		
	}
  
	function filterInstrumentIDsOnEnter(fieldId, filterType){
		<%-- 
		* The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
		 --%>
		require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldId), "keypress", function(event) {
		        if (event && event.keyCode == 13) {
		        	dojo.stopEvent(event);
		        	searchInstrumentIDs(filterType);
		        }
		    });
		});
	}
  
  var filterType = '';
  function searchInstrumentIDs(buttonId) {
    require(["dojo/dom","dojo/date/locale", "dijit/registry", "dojo/domReady!"], function(dom, locale, registry){	
			if(typeof buttonId != 'undefined'){
				filterType=buttonId;
			}
			
			console.log("buttonId: "+buttonId);
			console.log("filterType: "+filterType);
			
			var inactive = dom.byId("Inactive").checked;
			var active = dom.byId("Active").checked;
			var statusActive = "";
			var statusInactive = "";
			var instrumentType = '';
			
			if (inactive) 
				statusInactive = "Inactive";
			else
				statusInactive = "";
			if (active) 
				statusActive = "Active";
			else
				statusActive = "";			
				
	         var searchParms="confInd=<%=confInd%>";
		    	searchParms+="&showChildInstruments=<%=StringFunction.escapeQuotesforJS(showChildInstruments)%>";
		        searchParms+="&statusActive="+statusActive+"&statusInactive="+statusInactive;
		        
	        if(filterType=="Advanced"){
	        	instrumentType = registry.byId("InstrumentTypeAdvance").value;
		        var currency = registry.byId("Currency").value;
		        var amountFrom = registry.byId("AmountFrom").get('value');
		        var amountTo = registry.byId("AmountTo").get('value');
		        var addEXPOCO = '';
	 	        if (instrumentType == 'EXP_COL'){
	 	        	addEXPOCO ='N';
	 	        } 

		        if (isNaN(amountFrom))
		        	amountFrom='';
		        
		        if (isNaN(amountTo))
		        	amountTo='';
		        
		        var dateFrom;
		        var dateTo;		        
		        if(dom.byId("DateFrom").value != "")
		        	dateFrom = locale.format(registry.byId("DateFrom").value, {selector: "date", datePattern: "MM/dd/yyyy"});
		        else
		        	dateFrom = "";

		       	if(dom.byId("DateTo").value != "")
		        	dateTo = locale.format(registry.byId("DateTo").value, {selector: "date", datePattern: "MM/dd/yyyy"});
		       	else
		       		dateTo = "";
		        
		        var otherParty = dom.byId("OtherParty").value;
		        var vendorId = dom.byId("VendorIdAdvance").value;		        

		        searchParms+="&instrumentType="+instrumentType;
				searchParms+="&currency="+currency;
				searchParms+="&amountFrom="+amountFrom;
				searchParms+="&amountTo="+amountTo;
				searchParms+="&dateFrom="+dateFrom;
				searchParms+="&dateTo="+dateTo;
				searchParms+="&otherParty="+otherParty;
				searchParms+="&vendorId="+vendorId;
				searchParms+="&searchType=Advanced";
				searchParms+="&addEXPOCO="+addEXPOCO;
				
	        }else if(filterType=="Basic"){
	        	instrumentType = registry.byId("InstrumentTypeBasic").value;
	        	var instrumentId = dom.byId("InstrumentIdBasic").value;
	 	        var refNum = dom.byId("RefNum").value;	 	    
	 	        var bankRefNum = dom.byId("BankRefNum").value;
	 	        var vendorId = dom.byId("VendorIdBasic").value;
	 	        if (instrumentType == 'EXP_COL'){
	 	        	addEXPOCO ='N';
	 	        } 
	 	        searchParms+="&instrumentType="+instrumentType;
	 	        searchParms+="&instrumentId="+instrumentId;
	 	        searchParms+="&addEXPOCO="+addEXPOCO;
				searchParms+="&refNum="+refNum;
				searchParms+="&bankRefNum="+bankRefNum;
				searchParms+="&vendorId="+vendorId;
				
				searchParms+="&searchType=Basic";
				 <% if(userSession.isCustNotIntgTPS()){ //IR-45289 11/02/2015 %>
		      		var bankInstrumentId = dom.byId("BankInstId").value;
		      		searchParms += "&bankInstrumentId="+bankInstrumentId;	
		        <%}%>
	        }else if(filterType=="Bank"){        
	       		var instrumentTypeBank = registry.byId("InstrumentTypeBank").value;
	       		var instrumentIdBank = dom.byId("InstrumentIdBank").value;
	       		 <% if(userSession.isCustNotIntgTPS()){ %>
	       			var bankInstrumentId = dom.byId("BankInstId").value;
	        		searchParms+="&bankInstrumentId="+bankInstrumentId;
	        	 <%}%>
	        	searchParms+="&instrumentType="+instrumentTypeBank;
	        	searchParms+="&instrumentId="+instrumentIdBank;
	        	searchParms+="&searchType=Bank";
	        }
	        	searchParms+="&encrypted=true";
	        	searchParms+="&instrumentIDSearchFlag=true";
		        searchParms+="&loginLocale=<%=loginLocale%>";
		        searchParms+="&resourceLocale=<%=StringFunction.xssCharsToHtml(resMgr.getResourceLocale())%>";
		        searchParms+="&fromDocCache=<%=formMgr.getFromDocCache()%>";
		        searchParms+="&tradeInstrTypes=<%=tradeInstrTypes.toString()%>";
		        searchParms+="&custID=<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(custID))%>";
	        console.log("searchParms="+searchParms);
			searchDataGrid("tradeSearchGrid", "TradeSearchDataView", searchParms);
		});
	}
  
  function shuffleFilter(linkValue){
		console.log("inside shuffleFilter(): "+linkValue);
		require(["dojo/dom","dijit/registry"],
		      function(dom, registry){
	  			if(linkValue=='Advanced'){
	  				dom.byId("advancedInstrumentFilter").style.display='block';
	  				dom.byId("basicInstrumentFilter").style.display='none';
	  				
	  				<%-- clearing Basic Filter --%>
	  				registry.byId("InstrumentTypeBasic").value="";
	        		registry.byId("InstrumentIdBasic").set('value','');
	 	        	registry.byId("RefNum").value="";
	 	        	registry.byId("BankRefNum").value="";
	 	 	        registry.byId("VendorIdBasic").value="";
	 	 	      <% if(userSession.isCustNotIntgTPS()){ %>
	 	 	      		registry.byId("BankInstId").value="";
	 	 	      <%}%>
	  			}
	  			
	  			if(linkValue=='Basic'){
	  				dom.byId("advancedInstrumentFilter").style.display='none';
	  				dom.byId("basicInstrumentFilter").style.display='block';
	  				
	  				<%-- clearing Advance Filter --%>
	  				registry.byId("InstrumentTypeAdvance").value="";
		        	registry.byId("Currency").value="";
		        	registry.byId("AmountFrom").value="";
		        	registry.byId("AmountTo").value="";
		        	registry.byId("OtherParty").value="";
					registry.byId("VendorIdAdvance").value="";	
					registry.byId("DateFrom").reset();
					registry.byId("DateTo").reset();
	  			}
		});
	}
	
	function hideGrid(){		
		require(["t360/dialog"], 
			function(dialog) {
				dialog.hide('instrumentSearchDialog');	
		}); 
	}
	
  require(["dojo/dom","dijit/registry"],
      function(dom, registry){
    
    var dialogName=registry.byId("instrumentSearchDialog");
    var dialogCloseButton=dialogName.closeButtonNode;
    dialogCloseButton.setAttribute("id","instrumentSearchDialogCloseLink");
    dialogCloseButton.setAttribute("title","");
  });

</script>
<%=widgetFactory.createHoverHelp("instrumentSearchDialogCloseLink", "common.Cancel") %>