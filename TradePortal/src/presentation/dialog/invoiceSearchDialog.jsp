<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 
 *     All rights reserved
--%>

<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
				 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
		 		 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%
  Debug.debug("***START********************INVOICE*SEARCH**************************START***");


/******************************************************************************
 * InvoiceSearchDialog.jsp
 *
 * Filtering:
 * 
 * Selecting:
 * JavaScript is used to detect which radio button is selected, and the value
 * of that button is passed as a url parameter to page from which the search
 * page was called.
 ******************************************************************************/
WidgetFactory widgetFactory = new WidgetFactory(resMgr);
String returnAction     = null;
String onLoad           = "document.FilterForm.bankName.focus();";
String options = "";
String amountType = "";
String loginLocale = StringFunction.xssCharsToHtml(request.getParameter("loginLocale"));
String dateType = "";
String userOrgOid = StringFunction.xssCharsToHtml(request.getParameter("userOrgOid"));
String currency = StringFunction.xssCharsToHtml(request.getParameter("currency"));
//PMitnala Rel 8.3 IR T36000022445 START
boolean remitInd = Boolean.parseBoolean(StringFunction.xssCharsToHtml(request.getParameter("remitInd")));
String gridHtml="";
String gridLayout="";
//PMitnala Rel 8.3 IR T36000022445 END
//Rpasupulati IR T36000034280 start
ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
String datePattern = userSession.getDatePattern();
String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";
//Rpasupulati IR T36000034280 End

%>


  <div class="dialogContent fullwidth">
  
  <div id='tempInvoiceSearch' tabindex="0"></div>
  <div class="searchHeader">
    <span class="searchHeaderCriteria">
        <%=resMgr.getText("InvoiceSearch.FinanceStatus",TradePortalConstants.TEXT_BUNDLE)%> 
	<%=widgetFactory.createCheckboxField("Financed", "InvoiceSearch.Financed", false, false, false, "", "", "none") %>
			
		</span>
    
    <div style="clear:both;"></div>
  </div>
  
  <div class="searchDivider"></div>
			
	<div class="searchDetail">
	<span class="searchCriteria">
  			<%--<%= widgetFactory.createSearchTextField("InvoiceID", "InvoiceSearch.InvoiceID","20","class='char10'")%>--%>
  			<%=widgetFactory.createSearchTextField("InvoiceID", "InvoiceSearch.InvoiceID", "20", "class='char10' onKeydown='filterInvoiceOnEnter(\"InvoiceID\");'") %>
  			<%=widgetFactory.createSearchTextField("PONumber", "InvoiceSearch.PONumber", "20", "class='char10' onKeydown='filterInvoiceOnEnter(\"PONumber\");'") %>
  			<%=widgetFactory.createSearchTextField("BuyerIdentifier", "InvoiceSearch.BuyerIdentifier", "20", "class='char10' onKeydown='filterInvoiceOnEnter(\"BuyerIdentifier\");'") %>
		
  			<%--<%= widgetFactory.createSearchTextField("BuyerIdentifier", "InvoiceSearch.BuyerIdentifier","20","class='char10'")%>--%>
  			<%--<%= widgetFactory.createSearchTextField("PONumber", "InvoiceSearch.PONumber","20","class='char10'")--%>
  </span>
  			<span class="searchActions">
  			<button data-dojo-type="dijit.form.Button" type="button" id="searchParty">Search
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchMatchInvoices();</script>
		  	</button>
		  	</span>
		  	<%=widgetFactory.createHoverHelp("searchParty", "SearchHoverText") %>
		  	 <div style="clear:both;"></div>
		  	</div>
		  	
		  	<div class="searchDetail">
	<span class="searchCriteria">
  			<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AMOUNT_TYPE, amountType, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("AmountType", "InvoiceSearch.AmountType", " ", options,"onChange='searchMatchInvoices();' class='char10'"));
  			%>

  			
  			<%--<%=widgetFactory.createSearchAmountField("AmountFrom", "InvoiceSearch.From","","style='width:10em' class='char10'", "") %> --%>
  			<%=widgetFactory.createSearchAmountField("AmountFrom", "InvoiceSearch.From", "", "class='char10' onKeydown='filterInvoiceOnEnter(\"AmountFrom\");'","") %>
  			<%=widgetFactory.createSearchAmountField("AmountTo", "InvoiceSearch.To", "", "class='char10' onKeydown='filterInvoiceOnEnter(\"AmountTo\");'","") %>
		    <%--<%=widgetFactory.createSearchAmountField("AmountTo", "InvoiceSearch.To","","style='width:10em' class='char10'", "") %> --%>
  	
  	</span>
  	 <div style="clear:both;"></div>		
  	</div>		
  			
  		<div class="searchDetail">
	<span class="searchCriteria">	
  			<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.DATE_TYPE, dateType, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("DateType", "InvoiceSearch.DateType", " ", options, "onChange='searchMatchInvoices();' class='char10'"));
  			%>
  			<%--- R 9.3 T36000034280 Rpasupulati adding datePattern start--%>
  	    	<%=widgetFactory.createSearchDateField("DateFrom1","InvoiceSearch.From","onChange='searchMatchInvoices();' class='char10'","constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'") %>
			<%=widgetFactory.createSearchDateField("DateTo","InvoiceSearch.To","onChange='searchMatchInvoices();' class='char10'","constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'") %>
			<%--- R 9.3 T36000034280 Rpasupulati adding datePattern end--%>
  	</span>
  	<div style="clear:both"></div>
  	</div>
 

<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  //PMitnala Rel 8.3 IR T36000022445 START - Separate GridXMLs for single and multiple invoice select
  if(remitInd){
  	gridHtml = dgFactory.createDataGrid("MatchInvoicesDataGridId","MatchInvoicesSelectDataGrid",null);
  	gridLayout = dgFactory.createGridLayout("MatchInvoicesSelectDataGrid");
  }else{
	gridHtml = dgFactory.createDataGrid("MatchInvoicesDataGridId","MatchInvoicesDataGrid",null);
	gridLayout = dgFactory.createGridLayout("MatchInvoicesDataGrid"); 
  }
  //PMitnala Rel 8.3 IR T36000022445 END
%>

  <%=gridHtml%>  
</div>

<script type="text/javascript">

function filterInvoiceOnEnter(fieldID){
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	searchMatchInvoices();
	        }
	    });
	});	<%-- end require --%>
}<%-- end of filterInvoiceOnEnter() --%>

var initSearchParms="";
var loginLocale="";
var gridLayout = <%=gridLayout%>;
initSearchParms = "userOrgOid=<%=StringFunction.escapeQuotesforJS(userOrgOid)%>&currency=<%=StringFunction.escapeQuotesforJS(currency)%>";
console.log("initSearchParms: " + initSearchParms);
<%-- RPasupulati IR T36000034280 Start --%>
var tempDatePattern = encodeURI('<%=datePattern%>');
initSearchParms = initSearchParms+"&dPattern="+tempDatePattern;
 <%-- RPasupulati IR T36000034280 End --%>
require(["t360/datagrid"],  function(t360){
	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("MatchInvoicesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var myGrid = t360.createDataGrid("MatchInvoicesDataGridId", viewName,gridLayout,initSearchParms);
    myGrid.set('autoHeight',10); 
});


<%--provide a search function that collects the necessary search parameters--%>
function searchMatchInvoices() {
  require(["dojo/dom"],
    function(dom){
  	
 	 var finance="";
  		if(dom.byId("Financed").checked)
  			finance="Y";
  		else
  			finance="N";
  	
  	var invoiceID=(dom.byId("InvoiceID").value).toUpperCase();
  	var partyFilterText=(dom.byId("BuyerIdentifier").value).toUpperCase();
  	var poNumber=(dom.byId("PONumber").value).toUpperCase();
  	
  	
  	var amountType=(dijit.byId("AmountType").value).toUpperCase();
  	var amountFrom=(dom.byId("AmountFrom").value).toUpperCase();
  	var amountTo=(dom.byId("AmountTo").value).toUpperCase();
  	
  	var dateType=(dijit.byId("DateType").value).toUpperCase();
  	var fromDate=(dom.byId("DateFrom1").value).toUpperCase();
  	var toDate=(dom.byId("DateTo").value).toUpperCase();
  	
     
      <%-- var filterText=filterTextCS.toUpperCase(); --%>
	var searchParms = initSearchParms+"&finance="+finance+"&invoiceID="+invoiceID+"&partyFilterText="+partyFilterText+"&poNumber="
      +poNumber+"&amountType="+amountType+"&amountFrom="+amountFrom+"&amountTo="+amountTo+"&dateType="+dateType+"&fromDate="+fromDate
      +"&toDate="+toDate; 

    
      console.log("SearchParms : " + searchParms);
      searchDataGrid("MatchInvoicesDataGridId", "MatchInvoicesDataView",
                     searchParms);
    });
	}

 	  require(["dijit/registry", "dojo/on", "dojo/ready"], 
			function(registry,on,ready) {
 		  ready ( function () {
 			  var button = registry.byId("AddtoMatchInvoicesList");
 			  console.log('button1='+button);
 		   	 });
 		 });

  function chooseInvoices () {
	 require(["dojo/ready","dijit/registry","dojo/_base/array","t360/dialog"], 

				function(ready,registry,baseArray,dialog) {
			console.log('choose invoices');
			ready(function(){
				var items = registry.byId("MatchInvoicesDataGridId").selection.getSelected();
				if (items.length > 0){
					var retItems = new Array();
					for (j=0; j < items.length; j++){
						var invObject = new Array();
						invObject[0] = items[j].i.rowKey;
						invObject[1] = items[j].i.InvoiceID;
						invObject[2] = items[j].i.InvoiceID_linkUrl;
						invObject[3] = items[j].i.DueDate;
						invObject[4] = items[j].i.InvoiceAmount;
						invObject[5] = items[j].i.OutstandingAmount;
						invObject[6] = items[j].i.IssueDate;
						invObject[7] = items[j].i.FinanceAmount;
						invObject[8] = items[j].i.DisputedIndicator
						invObject[9] = items[j].i.FinanceStatus;
						<%-- IR T36000016617 Rel8.2 - Display Discount columns and DETAILS link --%>
						invObject[10] = items[j].i.PayAmount;
						invObject[11] = items[j].i.OverPayAmount;
						invObject[12] = items[j].i.DiscAmount;						
						invObject[13] = items[j].i.SellerDetails;
						invObject[14] = items[j].i.SellerDetails_linkUrl;
						invObject[15] = items[j].i.OTLInvoiceUoid;
						retItems[j] = invObject;
											}
					
					dialog.doCallback('invoiceSearchDialog','select', retItems);
					closePopup();
				} else {
					alert('Select Invoice(s) to be manually matched');
					return false;
				}
				
			});
			});
	 
  }

 	  
  function closePopup() {
	  require(["t360/dialog"], function(dialog) {
		  dialog.hide('invoiceSearchDialog');
	  });
	  
  }
  
  var dialogName=dijit.byId("invoiceSearchDialog");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","InvoiceSearchDialogCloseButton");
</script>

<%=widgetFactory.createHoverHelp("InvoiceSearchDialogCloseButton", "common.Cancel") %>

</body>
</html>
