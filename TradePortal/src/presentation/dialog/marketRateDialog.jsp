<%--
*******************************************************************************
                              Other Conditions PopUp

  Description: 
  	This PopUp will open when user clicks on Other Conditions/ Other Details/ Instructions to Bank
   	On Click of save the call JSP will be submitted
*******************************************************************************
--%>

<%--
 *		Dev Sandeep
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"     scope="session"> </jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"> </jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"> </jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"     scope="session"> </jsp:useBean>

<%


boolean isListView = true;

String selectedInstrument = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(request.getParameter("selectedInstrument")));
String formName = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(request.getParameter("form")));
if (InstrumentServices.isBlank(formName)) {
   formName = "0";
   isListView = false;
}

%>
   
<div id="container" style="display: none">
</div> 			
<div id="dialog" > 
      
      <div id="retrieving" >
      <table style="width: 100%; border: 0px;"  cellspacing="0">
         <tr height="50">
                     <td>&nbsp;</td>
            </tr>
            <tr>
			<td width="10">&nbsp;</td>       	
                     <td>
					 <span class="ListHeaderText"> <%= resMgr.getText("MarketRate.retrievingMsg",TradePortalConstants.TEXT_BUNDLE) %> </span> </td>
                     <td width="50">&nbsp;</td>
            </tr>
            <tr height="50">
                     <td>&nbsp;</td>
            </tr>
             </table>
      </div>
      
      <div id="gotRate" >
	  </div>
      
      <div id="confirming" >
      <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
            <tr height="50">
	                         <td>&nbsp;</td>
	                </tr>

	                <tr>
					 <td width="40">&nbsp;</td>       		
					 
	                         <td>
							 <span class="ListHeaderText"> <%= resMgr.getText("MarketRate.confirmingMsg",TradePortalConstants.TEXT_BUNDLE) %> </span> </td>
	                         <td>&nbsp;</td>
	                </tr>
	             <tr height="50">
                     <td>&nbsp;</td>
            </tr>
            </tr>
             </table>
      </div>
      
      
   </div> 
   
        
	   
   <script LANGUAGE="JavaScript">
       
       var xmlhttp;
	   var i=0;
       var timeout;
       
       function startCountDown(counter, delay, func) {
       
			<%--  make reference to div --%>
			var countDownObj = document.getElementById("countDown");
            if (countDownObj == null) return;
       
			countDownObj.count = function(counter) {
				countDownObj.innerHTML = counter;
				if (counter == 0) {
					func();  <%--  execute function --%>
					return;
				}
				timeout = setTimeout(function() {
					      countDownObj.count(counter - 1);}
					      , delay);
			};
       
			countDownObj.count(counter);
       }

  
      function changePopUp(layer) {
        document.getElementById("retrieving").style.display='none';
        document.getElementById("gotRate").style.display='none';
        document.getElementById("confirming").style.display='none';
        document.getElementById(layer).style.display='block';
        return true;
      }
	  
      function showPopUp(modal) {
           document.getElementById('dialog').style.display='block';
           changePopUp('retrieving');
           return true;
     }
	 
	 function parse(){
		require(["dojo/parser", "dojo/dom-class",
         "dojo/query", "dojo/NodeList-dom",
         "dijit/form/Form","dijit/form/Button",
         "dijit/form/TextBox","dijit/form/ValidationTextBox",
         "t360/widget/Tooltip"], function(parser, domClass, query){
	
	parser.parse(document.getElementById('dialog'));

	});
	}

     function dclose() {
	     require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"], function(registry, query, on, dialog ) {
		 console.log("closing....");
         if (timeout != null) {
            clearTimeout(timeout);
         }
		
		dialog.hide('MarketRate');
		dialog.close('MarketRate');
         return true;
		 });
     }
              
			  
			  
     function rateExpired() {
		 var errResp = document.getElementById('timedOutResp');
		 updateErr(errResp.innerHTML);
		 dclose();
     }
	 
	
	 
     function getSelected() {
	  		
		return '<%=selectedInstrument%>';

	 }
	 
     function makeRequest() {
	 
	 require(["dojo/_base/xhr"], function(xhr) {

	var remoteUrl  = "/portal/transactions/RequestMarketRate.jsp?"  + "selected="+getSelected()+ "&selectedCount=1&";
	xhr.get({
              url: remoteUrl,

			  load: function(data) {
			  	<%-- var fxOnlineAvailableDiv = document.getElementById("fxOnlineAvailableNonVis"); --%>
				<%-- fxOnlineAvailableDiv.innerHTML = data; --%>
				<%-- var fx = document.getElementById("hidden_fxInd").value; --%>
				var container = document.getElementById('container');
				container.innerHTML = data;
			   	
				var errorOccured = document.getElementById('errorSectionResp');
				
				if (errorOccured == null) {
				    var gotRate = document.getElementById('gotRate');
				    gotRate.innerHTML = data;
					container.innerHTML = "";
					parse();
	                changePopUp("gotRate");
	                startCountDown(<%=TradePortalConstants.getPropertyValue("TradePortal", "FX_ONLINE_QUOTE_TIMEOUT", "20")%>,1000,rateExpired);
			     } else {
				   updateErr(errorOccured.innerHTML);
				   container.innerHTML = "";
				   dclose();
                 }	
			
			},
			 error: function(error){
			     <%-- todo --%>
			}
		});
	});
	}
	 

		function updateErr(update) {
		var errSec = document.getElementById('_errorSection');
		
		 <%-- errSec = dojo.byId('errorSection'); --%>

		if (errSec == null) return;

		errSec.innerHTML = update;
		}
		
		function makeRequestPost() {
        if (!xmlhttp) xmlhttp = getXmlHttpRequest();
	    xmlhttp.open('POST', '/portal/Dispatcher.jsp', true);
	    xmlhttp.onreadystatechange = onGotRate;
        xmlhttp.send(null);
  	    }
		

		      var arr = document.getElementsByName('GetRateButton');
		      var getRateBtn = arr[0];
		      if (typeof(getRateBtn) != 'undefined')     getRateBtn.blur();
		     updateErr("");
            showPopUp(true);
             makeRequest();


		
	 function fxConfirm() {
	 require(["t360/common"], function(common){
		 clearTimeout(timeout);		
         changePopUp("confirming");

		 var _form=document.forms['<%=formName%>'];
		 common.addParmToForm(_form,"selected",getSelected());
		 <% if (isListView) { %>
          var rowKeys = getSelectedGridRowKeys("pmtPendGrid");
	       common.submitFormWithParms('<%=formName%>', "ConfirmFX", "checkbox", rowKeys);
		  <% }else  {%>
		 common.setButtonPressed("ConfirmFX", "<%=formName%>"); 
		  _form.submit();
		 <%}%>
		 
		 });
     }
	
   </script>
