<%--
 *  New Template Popup
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,
                 java.util.Vector" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 


//read in parameters
 String newTemplateDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
String  userOid        = userSession.getUserOid();
String  userOrgOid	 = userSession.getOwnerOrgOid();
String  loginLocale    = userSession.getUserLocale(); 	
String  loginRights    = userSession.getSecurityRights();
String ownerLevel      = userSession.getOwnershipLevel();
String options;
String defaultText;
String instrType       = "";
  boolean showExpress    = true;
  boolean showFixed    = SecurityAccess.hasRights(userSession.getSecurityRights(),
          SecurityAccess.CREATE_FIXED_PAYMENT_TEMPLATE);
  
  //out.println("rights: "+userSession.getSecurityRights());
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  StringBuffer allTranInstrTypes = new StringBuffer();

  Long organization_oid = new Long( userSession.getOwnerOrgOid() );
  String userSecurityType = userSession.getSecurityType();

  //Get the instrument type
  Vector instrumentTypes = Dropdown.getEditableInstrumentTypes(formMgr,
	                                       organization_oid.longValue(),
	                                       loginRights,
                                                   userSession.getSecurityType(),
                                                   ownerLevel);

      // Export LC should not appear in this dropdown since templates can
      // never be created for them
        if (instrumentTypes.contains(InstrumentType.EXPORT_DLC)) {
	      instrumentTypes.removeElement(InstrumentType.EXPORT_DLC);
        }

  //Build the option tags for the dropdown box.
  options = Dropdown.getInstrumentList( instrType, loginLocale, instrumentTypes);
  defaultText = resMgr.getText("NewInstTemplate1.selectInstType",
                                   TradePortalConstants.TEXT_BUNDLE);
  
//Corporate customers cannot create express templates
  // However, admin users using the customer access feature to act on behalf of a corporate 
  // customer are able to create express templates
  if ( userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE) )
   {
     if(!userSession.hasSavedUserSession() ||
        !userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN) )
     {
       showExpress = false;
     }
   }

//out.println("showExpress: "+showExpress);
//out.println("showFixed: "+showFixed);
//out.println("ownershipLevel: "+userSession.getOwnershipLevel());
%>

<div class="dialogContent">

<div class="formContent">
<div class="columnLeft">

<%= widgetFactory.createTextField( "TemplateName", "NewInstTemplate1.TemplateName", "", "35", false, true , false, "style='width: 16em;'", "", "") %>

<%
	  String clientBankOid = userSession.getClientBankOid();
      ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class,"ClientBank");
      clientBank.getById(clientBankOid);      

      if(TradePortalConstants.INDICATOR_YES.equals(clientBank.getAttribute("template_groups_ind"))){
   

		//Retrieve the data used to populate some of the dropdowns.
		// This data will be used in the default users dropdown
		StringBuffer sqlSet = new StringBuffer();
		QueryListView queryListView = null;
		DocumentHandler queryDoc = null;
		
		try {
		    queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView");
		
		    // Get a list of the users for the client bank
		    sqlSet.append("select PAYMENT_TEMPLATE_GROUP_OID, NAME");
		    sqlSet.append(" from PAYMENT_TEMPLATE_GROUP");
		    sqlSet.append(" where P_OWNER_ORG_OID in (?,?)");
		    Debug.debug(sqlSet.toString());
		    
		    queryListView.setSQL(sqlSet.toString(), new Object[]{userSession.getUserOid(),userSession.getOwnerOrgOid()});
		    queryListView.getRecords();
		    queryDoc = queryListView.getXmlResultSet();
		    Debug.debug("queryDoc --> " + queryDoc.toString());
		
		} catch (Exception e) {
		    e.printStackTrace();
		} 
		finally {
		  try {
		        if (queryListView != null) {
		            queryListView.remove();
		        }
		      } 
		      catch (Exception e) {
		           System.out.println("error removing querylistview in NewTemplate.jsp");
		      }
		}  // try/catch/finally block
		
		
		/********************************
		 * START Payment Template Group TYPE DROPDOWN
		 ********************************/
		 Debug.debug("Building Payment Template Group field");
		 	
		 String groupTmplateOptions = Dropdown.createSortedOptions(queryDoc,"PAYMENT_TEMPLATE_GROUP_OID", "NAME", 
		 										"", 
		 										resMgr.getResourceLocale());
		 Debug.debug(groupTmplateOptions);
		%> 
		<%= widgetFactory.createSelectField( "TemplateGroup", "NewInstTemplate1.TemplateGroup", " ", groupTmplateOptions, false, false, false, "style='width: 16em;'", "", "") %>
<% } %>

</div>

<div class="columnRight">
<br>
<%
  //for menu new transactions, we have a form that is submitted.
  // this is put in the footer so it is entirely after the other forms on the page
  //if ( TradePortalConstants.INDICATOR_YES.equals( includeNavBarFlag ) ) {
  //if ( !TradePortalConstants.INDICATOR_YES.equals( minimalHeaderFlag ) &&
  if ( !userSecurityType.equals(TradePortalConstants.ADMIN) ) {
%>
<%=widgetFactory.createRadioButtonField("copy", "CopyFromInstrument", "NewInstTemplate1.CopyFromInstrument",TradePortalConstants.FROM_INSTR, false, false,"onChange=\"clearInstrumentType();\"","") %>
<br>
<%
  }
%>
<%=widgetFactory.createRadioButtonField("copy", "CopyFromTemplate", "NewInstTemplate1.CopyFromTemplate",TradePortalConstants.FROM_TEMPL, false, false,"onChange=\"clearInstrumentType();\"","") %>
<br>
<%=widgetFactory.createRadioButtonField("copy", "UseBlankForm", "NewInstTemplate1.UseBlankForm",TradePortalConstants.FROM_BLANK, true, false) %>
<br>
<%= widgetFactory.createSelectField( "InstrumentType", "", defaultText, options, false, false, false, "class='char21' onChange=\"showFlags();\"", "", "") %>
<div id="divFlags" class="formItem">																									  
</div>
</div>

<div style="clear:both;"></div>
<div class="gridFooter" style="border-top: 1px solid #DBDBDB;">
	<button data-dojo-type="dijit.form.Button"  name="Continue" id="Continue" type="button">
		<%=resMgr.getText("CopySelected.Continue",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			continueCopy();     					
		</script>
	</button> 
	<%-- DK IR T36000015198  Rel8.2 03/25/2013 BEGINS --%>
	<%=widgetFactory.createHoverHelp("Continue","common.Continue") %>
	<%-- DK IR T36000015198  Rel8.2 03/25/2013 ENDS --%>
	<button data-dojo-type="dijit.form.Button"  name="Cancel" id="Cancel" type="button">
		<%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			closeBBDialog();     					
		</script>
	</button> 
	<%=widgetFactory.createHoverHelp("Cancel","common.Cancel") %>
</div>
</div>
</div>




<%--the following script loads the datagrid.  note that this dialog
    is a DialogSimple to allow scripts to execute after it loads on the page--%>
<script type='text/javascript'>
	function clearInstrumentType(){
		 require(["dijit/registry"],function(registry) {
			 registry.byId('InstrumentType').attr('displayedValue', "-- Select instrument type --");
		 });
	}

  function continueCopy() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
    	var copyType;
    	var templateName = registry.byId("TemplateName").value;
    	var templateGroup = "";
    	if(registry.byId("TemplateGroup")){
    		templateGroup = registry.byId("TemplateGroup").value;	
    	}
        
        var instrType = registry.byId("InstrumentType").value;
        var flagExpress;
        var flagFixed;
        
        if(registry.byId("CopyFromInstrument")!=undefined){
        	if(registry.byId("CopyFromInstrument").checked){
        		copyType= registry.byId("CopyFromInstrument").value;
        	}
    	}
        
        if (registry.byId("CopyFromTemplate").checked){
    		copyType = registry.byId("CopyFromTemplate").value;
    	}else if (registry.byId("UseBlankForm").checked){
    		if(instrType == ""){
    			alert('<%=StringFunction.asciiToUnicode(resMgr. getTextEscapedJS("NewTemplate.PromptMessage2",  TradePortalConstants.TEXT_BUNDLE))%>');
    			return false;
    		}
    		copyType = registry.byId("UseBlankForm").value;
    	}else if(!registry.byId("CopyFromInstrument").checked && !registry.byId("CopyFromTemplate").checked && !registry.byId("UseBlankForm").checked){
    		alert('<%=StringFunction.asciiToUnicode(resMgr. getTextEscapedJS("NewTemplate.PromptMessage1",  TradePortalConstants.TEXT_BUNDLE))%>');
    		return false;
    	}
    	
    	if(templateName == null || templateName ==""){
    		alert('<%=StringFunction.asciiToUnicode(resMgr. getTextEscapedJS("NewTemplate.PromptMessage2",  TradePortalConstants.TEXT_BUNDLE))%>');
			return false;
		}
      
    	if(registry.byId("Express")) { 
    	  if(registry.byId("Express").checked){
    		  flagExpress = "<%=TradePortalConstants.INDICATOR_ON%>";
    		  flagFixed = "";
    	  }else{
    		  flagExpress = "";
    		  flagFixed = "";
    	  }
    	}else{
    		flagExpress = "";
    	}
    	if(registry.byId("FixedPayment")) {
    	  if(registry.byId("FixedPayment").checked){
    		  flagFixed = "<%=TradePortalConstants.INDICATOR_ON%>";
    		  flagExpress = "";
    	  }else{
    		  flagFixed = "";
    		  flagExpress = "";
    	  }
    	}else{
    		flagFixed="";
    	}
      
      dialog.doCallback('<%=StringFunction.escapeQuotesforJS(newTemplateDialogId)%>', 'select',copyType, templateName, templateGroup, flagExpress+'/'+flagFixed,instrType );
    });
  }

  function closeBBDialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(newTemplateDialogId)%>');
    });
  }
	<%-- Code modified by Sandeep 12/05/2012 Start  	 --%>
  <%-- this function will called on change of InstrumentType list box. --%>
  <%-- The check boxes Express and Fixed will display based on the InstrumentType selected and the Logged in User security rights --%>
  	function showFlags(){
  		var showExpress = <%=showExpress%>;
  		var showFixed = <%=showFixed%>;
  		require(["dijit/registry","dojo/domReady!"], function(registry , dom) {
  			registry.byId("UseBlankForm").set('checked', true);
			
  			<%-- showExpress is true only when the logged in user is a Admin. --%>
			<%-- And only Approval to Pay, Import LC, Outgoing StandBy LC-Simple and Req to Advice is allowed to create the Express Template. --%>
			<%-- showExpress is flase if the logged in usear us Corporate. --%>
  			<%-- Only Payment Fixed Template is allowed to create. --%>
			if (registry.byId("InstrumentType").value == 'ATP' || registry.byId("InstrumentType").value == 'IMP_DLC'
					  || registry.byId("InstrumentType").value == 'SimpleSLC' || registry.byId("InstrumentType").value == 'RQA'){	
				if ( showExpress == true){
					document.getElementById("divFlags").innerHTML = "";
  					var w = registry.byId("Express"); if(w) { registry.byId("Express").destroy(); }
  					var widget = new dijit.form.CheckBox({
  						name : "Express",
  						id: "Express",
  						value: 'Y'
  					});
		  					
  					var widgetLabel = dojo.create("label", {'for': "Express", innerHTML: '<%=resMgr.getText("NewInstTemplate1.IsExpress", TradePortalConstants.TEXT_BUNDLE)%>' });
  					
  					document.getElementById("divFlags").appendChild(widget.domNode);
  					document.getElementById("divFlags").appendChild(widgetLabel);
				}else{
				  	document.getElementById("divFlags").innerHTML = "";
				  	var w = registry.byId("Express"); if(w) { registry.byId("Express").destroy(); }
			  	}
			}else if(registry.byId("InstrumentType").value == 'FTDP'){
				if ( showFixed == true){
					document.getElementById("divFlags").innerHTML = "";
					var w = registry.byId("FixedPayment"); if(w) { registry.byId("FixedPayment").destroy(); }
					var widget = new dijit.form.CheckBox({
 						name : "FixedPayment",
 						id: "FixedPayment",
 						value: 'Y'
 					});
		  					
					var widgetLabel = dojo.create("label", {'for': "FixedPayment", innerHTML: '<%=resMgr.getText("NewInstTemplate1.IsFixed", TradePortalConstants.TEXT_BUNDLE)%>' });
					
 					document.getElementById("divFlags").appendChild(widget.domNode);
 					document.getElementById("divFlags").appendChild(widgetLabel);
				}else{
				  	document.getElementById("divFlags").innerHTML = "";
				  	var y = registry.byId("FixedPayment"); if(y) { registry.byId("FixedPayment").destroy(); }
			  	}
			}else{
			  	document.getElementById("divFlags").innerHTML = "";
			  	var w = registry.byId("Express"); if(w) { registry.byId("Express").destroy(); }
			  	var y = registry.byId("FixedPayment"); if(y) { registry.byId("FixedPayment").destroy(); }
		  	}
	  	});		  
  	}
  <%-- Code modified by Sandeep 12/05/2012 End --%>

  	<%-- This code part is to remove default hover help (Cancel) and to set a required hover help for 'X' image on popup. --%>
	var dialogName=dijit.byId("<%=StringFunction.escapeQuotesforJS(newTemplateDialogId)%>");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","newTemplateDialogCloseLink");
	dialogCloseButton.setAttribute("title","");
</script>
	<%=widgetFactory.createHoverHelp("newTemplateDialogCloseLink", "common.Cancel") %>