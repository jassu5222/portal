<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%
if (TradePortalConstants.INDICATOR_YES.equals(SecurityRules.getAutoExtendSession())){
	HttpSession httpSession = request.getSession(false);
	 int maxInactiveTimeOut  = SecurityRules.getTimeOutPeriodInSeconds();
	 int sessionInActiveInterval = httpSession.getMaxInactiveInterval();
	
	 if (sessionInActiveInterval < maxInactiveTimeOut){
		 maxInactiveTimeOut = sessionInActiveInterval;
	 }
	
	 String savedSessionId = httpSession.getAttribute("currentSessionId") == null ? null : (String)httpSession.getAttribute("currentSessionId");
	 String thisSessionId = httpSession.getId() == null ? null : (String)httpSession.getId();
	 long lastAccessedTime = httpSession.getLastAccessedTime();
	 long currentTimeInMilis = System.currentTimeMillis();
	 long diff = ((currentTimeInMilis - lastAccessedTime ) / 1000);
	 int newMaxInactiveTimeout = maxInactiveTimeOut - (int)diff;
	 if (newMaxInactiveTimeout <= 0 ){
		 //do nothing
	 }else if (httpSession.isNew()||(savedSessionId == null )){
		 httpSession.invalidate();
	 } else {
		 httpSession.setMaxInactiveInterval(newMaxInactiveTimeout);
	 }

	 
}
%>

<div class="dialogContent padded">

  <div class="instruction">
    <%= resMgr.getText("TimeoutWarning.WarningPart1", TradePortalConstants.TEXT_BUNDLE) %>
    <%= SecurityRules.getTimeoutWarningPeriod() %>
    <%= resMgr.getText("TimeoutWarning.WarningPart2", TradePortalConstants.TEXT_BUNDLE) %>
  </div>

  <div>
    <button id="timeoutWarningCloseButton" data-dojo-type="dijit.form.Button"
            type="submit" >
      <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
    </button>
  </div>

</div>

<script>
  <%--event handler for close button--%>
  require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
      function(registry, query, on, dialog ) {
    query('#timeoutWarningCloseButton').on("click", function() {
      dialog.hide('timeoutWarningDialog');
    });
  });
</script>
