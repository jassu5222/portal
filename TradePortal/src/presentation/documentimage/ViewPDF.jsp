<%@ page import="weblogic.net.http.HttpURLConnection,java.net.URL,java.net.URLConnection,com.amsinc.ecsg.util.*,com.ams.tradeportal.common.cache.*,
				com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.TradePortalConstants,java.util.*,java.io.*" %>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%

if (request.getHeader("Referer") != null)
{
    String errorMessage = "An error occured while trying to retrieve PDF.  Please close this window and try again.";

    response.reset();
    response.setHeader("Cache-Control","no-store"); 
    response.setHeader("Pragma","no-cache");		
    response.setDateHeader("expires", 0);
    
    //cquinton 9/15/2011 Rel 7.1 ppx240 differentiate between encrypted/non encrypted parms
    String encryptedImageId = request.getParameter("image_id");
    		
    //IValavala Rel 8.2 CR 742.
    boolean isFromPayrem = false;
    isFromPayrem = !InstrumentServices.isBlank( request.getParameter("fromPayrem") );
    String payremParams = "";
    out.println(" isFromPayrem:" + isFromPayrem);
	System.out.println(" isFromPayrem:" + isFromPayrem);
    
    if(isFromPayrem){
    	String en_sourceType = request.getParameter("source_type");
    	String en_tranID = request.getParameter("tran_id");
    	//Validation to check Cross Site Scripting	  
    	if(en_sourceType != null){
    		en_sourceType = StringFunction.xssCharsToHtml(en_sourceType);
    	  }
    	//Rel 9.3 MEer XSS CID-11244, 11386

    	//out.println("source type:" + en_sourceType);
    	//Debug.debug(isFromPayrem + " source type:" + en_sourceType);
    	Cache cbCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    	DocumentHandler CBCResult = (DocumentHandler)cbCache.get(userSession.getClientBankOid());
    	String imageURL = CBCResult.getAttribute("/ResultSetRecord(0)/IMAGE_SVC_ENDPOINT");
    	String imageUser = CBCResult.getAttribute("/ResultSetRecord(0)/IMAGE_SVC_USERID");
    	String imagePasswd = CBCResult.getAttribute("/ResultSetRecord(0)/IMAGE_SVC_PWORD");
    	String customerID = request.getParameter("customer_id");
    	//Encrypt imageURL, user and passwd
    	imageURL = EncryptDecrypt.encryptStringUsingTripleDes( imageURL, userSession.getSecretKey() );
    	imageUser = EncryptDecrypt.encryptStringUsingTripleDes( imageUser, userSession.getSecretKey() );
    	imagePasswd = EncryptDecrypt.encryptStringUsingTripleDes( imagePasswd, userSession.getSecretKey() );
    	 
    	
        payremParams =  "&source_type=" + StringFunction.xssCharsToHtml(en_sourceType) + "&imgurl=" + imageURL + "&imgUsr=" + imageUser + "&imgPwd=" + imagePasswd + "&customer_id=" + StringFunction.xssCharsToHtml(customerID)
        				+ "&tran_id=" + StringFunction.xssCharsToHtml(en_tranID);
    	out.println(" urlParams:" + payremParams);
      }
    		
    if (encryptedImageId != null)
    {
        //cquinton 11/2/2011 Rel 7.1 start default to use ISRA, but if property is set use VeniceBridge
        PropertyResourceBundle tpProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
        boolean useISRA = true;
        // Commenting VeniceBridge Rpasupulati Rpasupulati #T36000013473 start.
//         try {
//             String imageImpl = tpProperties.getString("imageImpl");
//             if ( "VeniceBridge".equalsIgnoreCase( imageImpl ) ) {
//                 useISRA = false;
//             }
//         } catch ( Exception ex ) {
//             //do nothing, already default to useISRA=true
//         }
        // Commenting VeniceBridge Rpasupulati end.

        if ( useISRA ) {
            //cquinton 11/2/2011 Rel 7.1 end
    
            //cquinton 9/14/2011 Rel 7.1 ppx166 start
            //imageId = EncryptDecrypt.decryptStringUsingTripleDes( imageId, userSession.getSecretKey() );

            //cquinton 7/21/2011 Rel 7.1.0 ppx240 - pass through image hash
            String encryptedImageHash = request.getParameter("hash");
            if ( encryptedImageHash != null ) {

                ServletContext context = this.getServletConfig().getServletContext();
                RequestDispatcher requestDispatcher = 
                context.getRequestDispatcher(
                    "/GetImageContentServlet?doc_id="+encryptedImageId+
                    "&page_number=1&fromWhere=ViewPDF&hash=" + encryptedImageHash);
                //cquinton 11/9/2011 Rel 7.1
                //per j2ee spec, includes cannot modify response headers (including content type)
                //do a forward instead - we need this to be pdf if successful, text otherwise
                //requestDispatcher.include(request, response);
                requestDispatcher.forward(request, response);
                //cquinton 9/14/2011 Rel 7.1 ppx166 end
            } 
            else if(isFromPayrem){
            	ServletContext context = this.getServletConfig().getServletContext();
                RequestDispatcher requestDispatcher = 
                context.getRequestDispatcher(
                    "/GetImageContentServlet?doc_id="+encryptedImageId+
                    "&page_number=1&fromWhere=ViewPDF" + payremParams);
                //AiA - IR#T36000018569 - Add No cache to response - START
			    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.           
                //IR#T36000018569 - END
                requestDispatcher.forward(request, response);
            }
            else {
                ResourceManager resourceManager = (ResourceManager) request.getSession(true).getAttribute("resMgr");
                String errMsg = resourceManager.getText(
                    "DocumentView.ImageHashError", TradePortalConstants.TEXT_BUNDLE);
                out.println( errMsg );
            }
            //cquinton 11/2/2011 Rel 7.1 add back Venice Bridge capability if necessary
        } else {
            //use venice bridge
            String imageId = EncryptDecrypt.decryptStringUsingTripleDes( encryptedImageId, userSession.getSecretKey() );
            //PropertyResourceBundle tpProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
            URL imageSourceURL = new URL(tpProperties.getString("imageServerURL")
                +tpProperties.getString("imageWebDir")
                +"getPDFs.asp?ID="+imageId);
            HttpURLConnection pdfHttpURLConnection = (HttpURLConnection) imageSourceURL.openConnection();

            pdfHttpURLConnection.setRequestProperty("Content-Type", "application/octet-stream");
            //PHILC Switched call to GET from POST (more compatible) --> pdfHttpURLConnection.setRequestMethod("POST");
            pdfHttpURLConnection.setRequestMethod("GET");
            pdfHttpURLConnection.setUseCaches(false);
            pdfHttpURLConnection.setDefaultUseCaches(false);
   
            if(pdfHttpURLConnection.getResponseCode() == 200)
            {
                BufferedInputStream pdfInputStream = new BufferedInputStream(pdfHttpURLConnection.getInputStream());
                response.setHeader("Content-Type", "application/pdf");
                ServletOutputStream outputStream = response.getOutputStream();
    
                int oneByte;
                while((oneByte = pdfInputStream.read()) != -1)
                {
                    outputStream.write(oneByte);
                }
            }
            else
            {
%>
                <%= errorMessage %> 
<%
            }
        } // if !useIsra
    } // if (encryptedImageId != null)
    else
    {
%>
            <%= errorMessage %>
<%
    }
} // if (request.getHeader("Referer") != null)
else
{
%>
Invalid session.
<%
}
%>
