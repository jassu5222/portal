<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This JSP contains the logic to present a drop down of reference
 * caches maintained by the system, and the logic to flush those caches
 * upon selection.
 *
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*, com.ams.tradeportal.busobj.util.*,
	com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,
	java.text.*, java.security.*" %>
      
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>

<%--
 * uncomment this line to enable the password check on utilityLauncher.jsp
 * <%@ include file="fragments/internalPageAuth.frag" %>
--%>
<%
String CB = request.getParameter("clientBank");
Debug.debug("SLAMonitoringCOnfigUpdaterSave.jsp : SAVE CB ===> "+ CB);
if (StringFunction.isNotBlank(CB)){
	String deleteSQL = "Delete from perf_stat_config where client_bank = ?";
	DatabaseQueryBean.executeUpdate(deleteSQL, true, (Object)CB);
String insertSQl = "INSERT INTO PERF_STAT_CONFIG(PERF_STAT_CONFIG_OID,PAGE_NAME,ACTIVE,CLIENT_BANK) VALUES (?,?,?,?)";
ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);

Map<String, String[]> paramMap = request.getParameterMap();
Set keySet = paramMap.keySet();
Iterator itr = keySet.iterator();
itr.next();
for (int i = 1; itr.hasNext() ;i++){
	String key = (String)itr.next();
	//Debug.debug("SLAMonitoringCOnfigUpdaterSave.jsp : Key ====>> " +key);
	String[] val = paramMap.get(key);
	//Debug.debug("SLAMonitoringCOnfigUpdaterSave.jsp : VAL ====>> " +val[0]);
	if (!"clientBank".equals(key)){
		if ("Y".equals(val[0])){
			DatabaseQueryBean.executeUpdate(insertSQl, true, oid.generateObjectID(), key, val[0],CB);
		}
	}

}
PerfStatConfigManager.getPerfStatConfigMgr().refresh();
userSession.setIsSLAMonitoringOn(PerfStatConfigManager.getPerfStatConfigMgr(formMgr.getServerLocation()).getPerfStatConfig("SLAMonitoring", CB != null ? CB : "", "N"));
pageContext.forward( "/internal/SLAMonitorConfigUpdater.jsp");

}

%>