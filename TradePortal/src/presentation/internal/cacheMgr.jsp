<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This JSP contains the logic to present a drop down of reference
 * caches maintained by the system, and the logic to flush those caches
 * upon selection.
 *
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*, com.ams.tradeportal.busobj.util.*,
	com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.io.PrintWriter,
	java.text.*, java.security.*,java.util.*" %>
      
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>

<%--
 * uncomment this line to enable the password check on utilityLauncher.jsp
 * <%@ include file="fragments/internalPageAuth.frag" %>
--%>

<html>
   <head>
      <title>Cache Manager</title>
   </head>
   <body>
     <h1>
        Application Cache Management Functions
     </h1>
     <form name="CacheMgr" action="cacheMgr.jsp" method="POST">
     &nbsp;
     <td>
         Pick a cache from the drop down to flush the data.  Then, close the window.
     </td>
     <table height=80%>
       
       <tr valign="middle">
        <td>
           <select name="cacheToReset" id="cacheToReset" onChange="CacheMgr.submit()" >
              <option value="" selected></option>
              <option value="<%= TradePortalConstants.CLIENT_BANK_CACHE %>"><%= TradePortalConstants.CLIENT_BANK_CACHE %></option>
              <option value="<%= TradePortalConstants.FX_RATE_CACHE  %>"><%= TradePortalConstants.FX_RATE_CACHE %></option>
              <option value="<%= TradePortalConstants.BANK_BRANCH_RULE_CACHE  %>"><%= TradePortalConstants.BANK_BRANCH_RULE_CACHE %></option>
              <%-- DK IR T36000024082 Rel8.4  01/14/2014 --%>
              <option value="<%= TradePortalConstants.CONFIG_SETTING_CACHE  %>"><%= TradePortalConstants.CONFIG_SETTING_CACHE %></option>
           </select>
        </td>
       </tr>
     </table>
&nbsp;

     </form>
<%
  try { 
  String cacheName = StringFunction.xssCharsToHtml(request.getParameter("cacheToReset"));
  System.out.println("cacheName = " + cacheName);
  if (null != cacheName && !"".equals(cacheName)) {
       //call flush on appropriate cache
       Cache cacheToFlush = (Cache)TPCacheManager.getInstance().getCache(cacheName);
       if (cacheToFlush != null) {
          cacheToFlush.flush();
          out.print ("cache flushed, name = " + cacheName);
       }
   }
} catch (Exception e) {
  out.flush();
  PrintWriter pw = response.getWriter();
  e.printStackTrace(pw);
  if (e instanceof javax.servlet.ServletException) {
    pw.println("\nR O O T   C A U S E :\n\n");
    Throwable t = ((javax.servlet.ServletException) e).getRootCause();
    t.printStackTrace(pw);
  }	
}   
%>

</body>
</html>
