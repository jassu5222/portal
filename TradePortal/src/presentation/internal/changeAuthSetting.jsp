<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, com.ams.tradeportal.common.*, java.util.*" %>
<META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>

<%--
 *    This page allows the operations staff to change the verify_login_digital_sig setting
 *    for a client bank or global organization.   When this setting is set to 'Yes', certificate
 *    logins require a digital signature to be present in the URL.  This is the default behavior.
 *   
 *    The verify_login_digital_sig setting should only be set to 'No' when there is a problem
 *    with the bank's ability to create digital signatures.
 *
 *     Copyright  � 2003                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<html>
<body>
<font face="Arial" size="3">Change Digital Signature Requirement</font>
<hr>
<font face="Arial" size="2">This page allows operations staff to change an authentication requirement for organizations.
When logging in with certificates, a digital signature is typically required in the URL that connects the user to the portal.
In the event of a failure of the bank's ability to create this digital signature, the portal supports logging in 
without a digital signature in the link.   By setting the flag to 'No' below, the portal will bypass the digital
signature requirement for that organization.
<br>
<br>
This must only be done at the request of authorized personnel at the ASP bank.   
</font>
<hr>
<form method="post" action="<%=request.getContextPath()%>/internal/processAuthSettingChange.jsp">

<font face='Arial' size='2'><b>Internal Utility Password: </font><input type="password" name="password" />
<br><br>


<table cellspacing=1 border=0 cellpadding=0>
<tr bgcolor=eeeecc><td align=center><font face="Arial" size=2><b>Organization Type</td><td align=center><font face="Arial" size=2><b>Organization</td><td><font face="Arial" size=2><b>Require Digital Signatures?</td></tr>
<%
  StringBuffer sql = new StringBuffer();
  sql.append("select 'ASP' as org_type, 'GLOBAL_ORGANIZATION' as table_name, organization_oid, verify_logon_digital_sig,  name  from global_organization");
  sql.append(" union ");
  sql.append("select 'Client Bank' as orgType, 'CLIENT_BANK' as table_name, organization_oid, verify_logon_digital_sig, name from client_bank where activation_status = ? ");

  DocumentHandler resultsXml = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, new Object[]{TradePortalConstants.ACTIVE});

  for(int i=0; i< resultsXml.getFragments("/ResultSetRecord").size(); i++)
   {
     String orgType = resultsXml.getAttribute("/ResultSetRecord("+i+")/ORG_TYPE");
     String tableName = resultsXml.getAttribute("/ResultSetRecord("+i+")/TABLE_NAME");
     String orgName = resultsXml.getAttribute("/ResultSetRecord("+i+")/NAME");
     String orgSetting = resultsXml.getAttribute("/ResultSetRecord("+i+")/VERIFY_LOGON_DIGITAL_SIG");
     String orgOid = resultsXml.getAttribute("/ResultSetRecord("+i+")/ORGANIZATION_OID");

      boolean requireDigitalSignature = true;

      if(orgSetting.equals(TradePortalConstants.INDICATOR_NO))
              requireDigitalSignature = false;   
%>
     <tr bgcolor="<%= (i % 2 == 0) ? "E5E5E5" : "CCCCCC" %>">
     <td><font face="Arial" size=2><%=orgType %></td>
     <td><font face="Arial" size=2><%=StringFunction.xssCharsToHtml(orgName) %></td>
         <td align=center><font face="Arial" size=2>
            <select name="<%= orgOid+"/"+tableName %>">
               <option value="<%= TradePortalConstants.INDICATOR_YES %>" <%= requireDigitalSignature ? "selected" : "" %> >Yes</option>
               <option value="<%= TradePortalConstants.INDICATOR_NO %>" <%= !requireDigitalSignature ? "selected" : "" %> >No</option>
            </select></font>
         </td></tr>
<%
   }
%>
</table>
<br>
<input type=submit value="Submit Changes">
</form>

