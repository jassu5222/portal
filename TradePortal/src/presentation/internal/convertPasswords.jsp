<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.html.*, java.text.*,java.util.*" %>


<%

        long totalTime = 0;
        long updatedCount = 0;

	String passwordSql = "select user_oid, encryption_salt, password from users where converted_encryption is null or converted_encryption = ?";

	DocumentHandler result = DatabaseQueryBean.getXmlResultSet(passwordSql, true, true, new Object[]{"N"});
	
	if( result != null)
	 {
	    Vector userList = result.getFragments("/ResultSetRecord");
          
	    for (int i=0; i<userList.size(); i++)
             {
                    String userOid = null;

		    DocumentHandler passwordDoc = (DocumentHandler) userList.elementAt(i);
		    try
                     {
                            long startTime = new Date().getTime();

			           userOid  = passwordDoc.getAttribute("/USER_OID");
			    String salt     = passwordDoc.getAttribute("/ENCRYPTION_SALT");
			    String password = passwordDoc.getAttribute("/PASSWORD");
                            
                            String convertedSalt     = convert(salt);
                            String convertedPassword = convert(password);  
                          //jgadela R90 IR T36000026319 - SQL INJECTION FIX
			    			String updateSql = "update users set encryption_salt = ?, password = ?, converted_encryption='Y' where user_oid = ? ";
                            DatabaseQueryBean.executeUpdate(updateSql, true, convertedSalt, convertedPassword, userOid);

                            long elapsedTime = new Date().getTime() - startTime;
                            totalTime += elapsedTime;
                            updatedCount++;
 
                            System.out.println("Converted OID = "+userOid+" : Time = "+elapsedTime );

		     }
                    catch (Exception e)
                     {
                            System.out.println("Exception while converting user OID = "+userOid);
			    e.printStackTrace();
		     }
	    }

           if(updatedCount > 0)
             System.out.println("Average Time = "+((double)(totalTime / updatedCount)));

        }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

        totalTime = 0;
        updatedCount = 0;
      //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    String passwordHistorySql = 
             "select ph.password_history_oid, ph.password from users u, password_history ph where ph.p_user_oid = u.user_oid" +
             " and (ph.converted_encryption is null or ph.converted_encryption = ?)";

    result = DatabaseQueryBean.getXmlResultSet(passwordHistorySql, true, new Object[]{"N"});
	
	if( result != null)
	 {
	    Vector passwordHistoryList = result.getFragments("/ResultSetRecord");

	    for (int i=0; i<passwordHistoryList.size(); i++)
             {
                    String historyOid = null;
		    DocumentHandler passwordHistoryDoc = (DocumentHandler) passwordHistoryList.elementAt(i);
		    try
                     {
                            long startTime = new Date().getTime();

			    historyOid  = passwordHistoryDoc.getAttribute("/PASSWORD_HISTORY_OID");
			    String password    = passwordHistoryDoc.getAttribute("/PASSWORD");
                            
                String convertedPassword = convert(password);  
                //jgadela R91 IR T36000026319 - SQL INJECTION FIX
			    String updateSql = "update password_history set password = ?, converted_encryption='Y' where password_history_oid = ? ";
                DatabaseQueryBean.executeUpdate(updateSql.toString(), true, convertedPassword, historyOid);

                long elapsedTime = new Date().getTime() - startTime;
                totalTime += elapsedTime;
                updatedCount++;

                System.out.println("Converted Password History: OID = "+historyOid+" : Time = "+elapsedTime);
		    } 
                   catch (Exception e)
                    {
                            System.out.println("Exception while converting password history OID = "+historyOid);
			    e.printStackTrace();
		    }
	     }
 
           if(updatedCount > 0)
              System.out.println("Average Time = "+((double)(totalTime / updatedCount)));
        }      

%>

<%!
   // Various methods are declared here 

   public String convert(String source) throws Exception
    {
        // Handle null/blank situation
        if((source == null) || source.equals(""))
           return "";

        // Decode data passed in using base64
        byte[] sourceBytes = EncryptDecrypt.base64StringToBytes(source);

        // Decrypt using the blank key
        byte[] decryptedBytes = EncryptDecrypt.decryptUsingDes3(sourceBytes, true);  

        // Encrypt using real key
        byte[] encryptedBytes = EncryptDecrypt.encryptUsingDes3(decryptedBytes);

        // Encode value in base64 to be returned
        return EncryptDecrypt.bytesToBase64String(encryptedBytes);
    }
 

%> 


















