<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This fragment opens a popup to allow the user to clear cache.
 *
--%>
  

 <script language="JavaScript"> 
       function openCacheFlusher (url) 
       {

           winHeight=300;
           winWidth=600;  
           widgets = 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,';
           widgets = widgets + 'width='+winWidth+',height='+winHeight;

           reauthenticationWin = window.open(url, 'CacheManager', widgets);

           <%-- Center the window in the middle of the screen --%>
           horizLocation = (screen.width-winWidth)/2;
           vertLocation =  (screen.height-winHeight)/2;
           reauthenticationWin.moveTo(horizLocation,vertLocation);
   	   reauthenticationWin.focus();
             	
        }
   </script>      

