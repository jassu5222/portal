<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, java.util.*, com.ams.tradeportal.common.*" %>
<%--
 *   Authenticates the user to enable them to use the internal pages.
 *   The password and encryption salt are stored in TradePortal.properties
 *
 *
 *     Copyright  � 2002                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
    String encryptedPassword = SecurityRules.getInternalPassword();;
    String salt = SecurityRules.getInternalPasswordSalt();

    String userEnteredPassword = request.getParameter("password");

    if(!Password.encryptPassword(userEnteredPassword, salt).equals(encryptedPassword))
     {
        out.print("Access Denied.  Incorrect Password");
        return;  
     }
%>