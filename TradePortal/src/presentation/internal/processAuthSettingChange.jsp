<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, com.ams.tradeportal.common.*, java.util.*" %>

<%--
 *    This page allows the operations staff to change the verify_login_digital_sig setting
 *    for a client bank or global organization.   When this setting is set to 'Yes', certificate
 *    logins require a digital signature to be present in the URL.  This is the default behavior.
 *   
 *    The verify_login_digital_sig setting should only be set to 'No' when there is a problem
 *    with the bank's ability to create digital signatures.
 *
 *     Copyright   2003                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

  <%@ include file="fragments/internalPageAuth.frag" %>

<%
     
     // Get all fields that were submitted
     Enumeration enumer = request.getParameterNames();
 
     while(enumer.hasMoreElements())
      {
         String paramName = (String) enumer.nextElement();

         // Ignore the password field for purposes of doing updates
         if(paramName.equals("password"))
           continue;

         String authSetting = (String) request.getParameter(paramName);

         // Parse out the table name and OID
         int index = paramName.indexOf("/");

         String oid = paramName.substring(0, index);
         String tableName = paramName.substring(index+1, paramName.length());

         // Build SQL and update the table
         StringBuffer sql = new StringBuffer();
         sql.append("update ");
         sql.append(tableName);
         sql.append(" set verify_logon_digital_sig = ?");
         sql.append(" where organization_oid = ?");
	     Object sqlParams[] = new Object[2];
	     sqlParams[0] = authSetting; 
	     sqlParams[1] = oid;

         DatabaseQueryBean.executeUpdate(sql.toString(), false, sqlParams);
      }
%>

Update complete.
