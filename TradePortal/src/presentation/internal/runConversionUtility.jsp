<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, java.util.*, com.ams.tradeportal.devtool.*,
     com.ams.tradeportal.conversion.*" %>
<%--
 *
 *    Runs the corporate customer conversion utility based on the parameters passed in.
 *
 *
 *     Copyright  � 2002                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>

<font face="Arial" size="2">

  <%@ include file="fragments/internalPageAuth.frag" %>

<a href="<%=request.getContextPath()%>/internal/utilityLauncher.jsp">Return to Launcher Page</a>
<hr>

<%
     String clientBankId = request.getParameter("clientBankId");
     String inputCSVFileLocation = request.getParameter("inputCSVFileLocation");
     String mapperFileLocation = request.getParameter("mapperFileLocation");
     String fileType = request.getParameter("fileType");
     String userIdentifier = request.getParameter("userIdentifier");
     String maxErrors = request.getParameter("maxErrors");

     String params[] = {clientBankId, inputCSVFileLocation, fileType, userIdentifier, maxErrors, mapperFileLocation};
    
     // Start a new thread to kick off the utility
     // That way, it can run in the background.  The user must check the log
     // to see the results.
     Thread conversionThread = new Thread(new Conversion(params));
     conversionThread.start();
%>

The conversion utility is now executing.   Results are logged in the same directory as the CSV files.
