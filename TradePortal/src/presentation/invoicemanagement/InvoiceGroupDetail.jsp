<%--
*******************************************************************************
                        Invoice Group Detail

  Description:
     This page is used to display the invoices of an invoice group.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
StringBuffer      onLoad                = new StringBuffer();
String            helpSensitiveLink     = null;
String            currentTabLink        = null;
String            currentTab            = TradePortalConstants.INV_GROUP_TAB;//BSL IR NLUM040376572 04/06/2012 CHANGE
String            formName              = null;
String            tabOn                 = null;
String            userOrgOid            = null;
String            userOid               = null;
String            userSecurityRights    = null;
String            userSecurityType      = null;
StringBuffer      dynamicWhereClause           = new StringBuffer();
StringBuffer      dropdownOptions              = new StringBuffer();
int               totalOrganizations           = 0;
StringBuffer      extraTags                    = new StringBuffer();
StringBuffer      newLink                      = new StringBuffer();
StringBuffer      newUploadLink                = new StringBuffer();

String            userDefaultWipView           = null;
String            selectedWorkflow             = null;
String            selectedStatus               = "";
String            loginLocale                  = null;
Vector            codesToExclude               = null;
String            gridID                       = null;
String            gridName                     = null;
String            viewName                     = null;
String searchListViewName = "InvoiceGroupDetailListView.xml";
formName = "UploadInvoiceListForm";

   userSession.setCurrentPrimaryNavigation(resMgr.getText("NavigationBar.InvoiceManagement",
                                         TradePortalConstants.TEXT_BUNDLE));

   //cquinton 1/18/2013 set return page
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToInvoiceManagement");
   }

   //cquinton 1/18/2013 remove old close action stuff

   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();
   session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
   // getting the user security type to display authorization button or not.
   boolean isAdminUser;
   if (userSession.getSavedUserSession() == null) {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSecurityType());
   } else {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType());
   }

   CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.getById(userOid);

   boolean groupingEnabled = TradePortalConstants.INVOICES_GROUPED_TPR.equals(corpOrg.getAttribute("allow_grouped_invoice_upload"));

   if (groupingEnabled) {
	   session.removeAttribute("fromInvoiceGroupDetail");
   }

   formName = "UploadInvoiceListForm";
   helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm", resMgr, userSession);
   loginLocale = userSession.getUserLocale();
	//SHR RM-15 Invoice Groups- Invoice List Issues
   String invoiceGroupOid = request.getParameter("invoice_group_oid");
   if(InstrumentServices.isNotBlank(invoiceGroupOid)){
	   session.setAttribute("invGrpOid",invoiceGroupOid);
	}
   else{
	   invoiceGroupOid =(String) session.getAttribute("invGrpOid");
   }
   Hashtable secureParms = new Hashtable();
   secureParms.put("UserOid", userSession.getUserOid());
   secureParms.put("SecurityRights",   userSecurityRights);
   secureParms.put("ownerOrg",   userOrgOid);
   if(InstrumentServices.isNotBlank(invoiceGroupOid))
   secureParms.put("invoice_group_oid", invoiceGroupOid);
 	//SHR RM-15 Invoice Groups- Invoice List Issues
   if (invoiceGroupOid != null) {
      invoiceGroupOid = EncryptDecrypt.decryptStringUsingTripleDes(invoiceGroupOid, userSession.getSecretKey());
   }

   InvoiceGroupWebBean invoiceGroup = beanMgr.createBean(InvoiceGroupWebBean.class, "InvoiceGroup");
   invoiceGroup.getById(invoiceGroupOid);
   session.setAttribute("fromInvoiceGroupDetail", "GD");
   dynamicWhereClause.append(" and i.a_corp_org_oid = ").append(userOrgOid);
   if (groupingEnabled) {
      dynamicWhereClause.append(" and i.a_invoice_group_oid = ").append(invoiceGroupOid);
     // session.setAttribute("fromInvoiceGroupDetail", TradePortalConstants.INDICATOR_YES);
   }


   else if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
      dynamicWhereClause.append(" and (i.invoice_status = '");
      dynamicWhereClause.append(selectedStatus);
      dynamicWhereClause.append("'");
      if (TradePortalConstants.UPLOAD_INV_STATUS_AUTH.equals(selectedStatus)) {
          dynamicWhereClause.append(" or i.invoice_status = '");
          dynamicWhereClause.append(TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH);
          dynamicWhereClause.append("'");
      }
      else if (TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(selectedStatus)) {
          dynamicWhereClause.append(" or i.invoice_status = '");
          dynamicWhereClause.append(TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH);
          dynamicWhereClause.append("'");
      }
      else if (TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(selectedStatus)) {
          dynamicWhereClause.append(" or i.invoice_status = '");
          dynamicWhereClause.append(TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED);
          dynamicWhereClause.append("'");
      }
      dynamicWhereClause.append(")");
   }


%>
<%
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.
  //jgadela R92 - SQL INJECTION FIX
   String groupDueDatesql = "SELECT due_date FROM invoice_group_view WHERE invoice_group_oid = ? ";
   DocumentHandler invoiceDueDateDoc = DatabaseQueryBean.getXmlResultSet(groupDueDatesql, false, new Object[]{invoiceGroupOid});
   String dueDate = "";
   String paymentDate = "";
   if(invoiceDueDateDoc !=null){
	   dueDate = (invoiceDueDateDoc.getFragment("/ResultSetRecord")).getAttribute("DUE_DATE");
   }
   if(StringFunction.isBlank(dueDate)){
	   dueDate = invoiceGroup.getAttribute("payment_date");
   }

  StringBuilder invoicesForHeader = new StringBuilder(resMgr.getText("UploadInvoiceDetail.InvoicesFor", TradePortalConstants.TEXT_BUNDLE));
  invoicesForHeader.append(" ").append(" - ");
  invoicesForHeader.append(" ").append(invoiceGroup.getAttribute("trading_partner_name"));
  invoicesForHeader.append(" ").append(" - ");
  invoicesForHeader.append(" ").append(invoiceGroup.getAttribute("currency"));
  invoicesForHeader.append(" ").append(" - ");
  invoicesForHeader.append(" ").append(resMgr.getText("InvoiceDefinition.DueDate", TradePortalConstants.TEXT_BUNDLE)+":");
  invoicesForHeader.append(" ").append(TPDateTimeUtility.formatDate(dueDate, TPDateTimeUtility.SHORT, loginLocale));

  selectedStatus = invoiceGroup.getAttribute("invoice_status");
  String resourcePrefix ="";
  if(InvoiceUtility.isCreditGroup(selectedStatus)){

		 gridID = "InvoicePendingActionListID";
		 gridName = "InvoicePendingActionListDataGrid";
		 viewName = "InvoicePendingActionListDataView";
		 resourcePrefix = "UploadInvoices";

 }
 else{

		 gridID = "InvoiceGroupListID";
		 gridName = "InvoiceGroupListDataGrid";
		 viewName = "InvoiceGroupListDataView";
		 resourcePrefix = "UploadInvoices";

 }
%>

<%-- ********************* HTML for page begins here *********************  --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>


<%-- Body tag included as part of common header --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad.toString()%>" />
</jsp:include>

<div class="pageMainNoSidebar">
<%-- IR T36000030360: (gridInvoiceFooterDivHidden) Hide page content, untill widgets get parsed. --%>
  <div id="InvoiceMgtDivID" class="pageContent gridInvoiceFooterDivHidden">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="InvoiceMgmt.RecInvGroupInvList" />
      <jsp:param name="helpUrl" value="customer/receivables_grouped_invoice_list.htm" />
    </jsp:include>

    <%--cquinton 11/1/2012 ir#7015 replace return link with close button--%>
    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <%= invoicesForHeader %>
      </span>
      <span class="pageSubHeader-right">
        <%--cquinton 10/31/2012 ir#7015 change return link to close button when no close button present.
            note this does NOT include an icon as in the sidebar--%>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              '<%= formMgr.getLinkAsUrl("goToInvoiceManagement", response) %>';
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>

<% //AAlubala - 04/23/2012 - reauthenticate if necessary
    String certAuthURL = ""; //needed for openReauth frag
    Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());
    String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
    boolean InvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__INVOICE_MGNT_FIN_INVOICE);

    String authorizeLink = "";

    //CR711 - 04/18/2012
    String proxyAuthLink = "";
    /*  KMEhta Rel8400 Fix for IR T36000023304 on 31/12/2014 */
    String offlineAuthLink = "";
%>


<form name="<%=formName%>" method="POST" action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">
    <jsp:include page="/common/ErrorSection.jsp" />
    <div class="formContentNoSidebar">
   <input type=hidden value="" name=buttonName>
   <input type="hidden" name="payment_date" />
   <%-- Srinivasu_D IR#11199 Rel8.2 03/01/2013 start --%>
   <input type="hidden" name="instrumentType" />
   <input type="hidden" name="loanType" />
   <input type="hidden" value="yes" name="fromRecDetail">
<%--   <input type="hidden" value="yes" name="fromPayDetail">--%>
   <%-- Srinivasu_D IR#11199 Rel8.2 03/01/2013 End --%>
<%  //AAlubala VASCO reauthentication hidden fields
    if ( InvoiceRequireAuth) {
%>

      <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
      <input type=hidden name="reCertOK">
      <input type=hidden name="logonResponse">
      <input type=hidden name="logonCertificate">

<%
    }
%>

<%
    if (InvoiceRequireAuth) {
%>
  <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
  <%
  //IR T36000014931 REL ER 8.1.0.4 BEGIN
        //NOTE: although there are lists on the screen, this is specific to the pay remit
        authorizeLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_INV_AUTHORIZE_INVOICES + "'," +
            "'Invoice','"+gridID+"')";

        proxyAuthLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
	    "'" + formName + "','" + TradePortalConstants.BUTTON_PROXY_AUTHORIZE + "'," +
            "'Invoice','"+gridID+"')";
        /*  KMEhta Rel8400 Fix for IR T36000023304 on 31/12/2014 Star */
        offlineAuthLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
        	    "'" + formName + "','" + TradePortalConstants.BUTTON_INV_PROXYAUTHORIZE_INVOICES + "'," +
                    "'Invoice','"+gridID+"')";
        /*  KMEhta Rel8400 Fix for IR T36000023304 on 31/12/2014 END */
  //IR T36000014931 REL ER 8.1.0.4 END
    }
    //vasco end
  %>
  <%

  //SHR print button start
  String linkArgs[] = {"","","","","",""};
          String arg = "key~"+viewName+ "`key~" + resourcePrefix;
          /*Object obj = session.getAttribute(searchListViewName);
          String startRow = String.valueOf(lvHandler.getStartRow());
          String sortColumn = String.valueOf(lvHandler.getSortColumn());
          String sortOrder = lvHandler.getSortOrder();
          String rowCount = String.valueOf(lvHandler.getTotalRows());
          String hideColumn = " ";
          if (InstrumentServices.isNotBlank(request.getParameter("hideColumn"))){
              hideColumn =  request.getParameter("hideColumn");
          }*/


  linkArgs[0] = TradePortalConstants.PDF_LIST;
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(arg);//Use common key for encryption of parameter to doc producer
          linkArgs[2] = InstrumentServices.isNotBlank(resMgr.getResourceLocale())? resMgr.getResourceLocale():"en_US";
          //loginLocale;
          linkArgs[3] = userSession.getBrandingDirectory();
          String linkString =  formMgr.getDocumentLinkAsURLInFrame(resMgr.getText("UploadInvoiceDetail.PrintInvoiceDetails",
                  TradePortalConstants.TEXT_BUNDLE),
                  TradePortalConstants.PDF_LIST,
                  linkArgs,
                  response) ;

          //SHR end
 %>
 <%-- SHR --%>
 <div class="gridSearch">
  <div class="searchHeader">
   <span class="searchHeaderCriteria">
	<%=resMgr.getText("InvoiceList.Invoices",TradePortalConstants.TEXT_BUNDLE)%>

	</span>
 	<span class="searchHeaderActions">
 	<jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value='<%=gridID%>'/>
      </jsp:include>
    <button data-dojo-type="dijit.form.Button" name="print" id="print" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.PrintText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          window.open( "<%=linkString%>");
		  </script>
        </button>
         <%=widgetFactory.createHoverHelp("print", "PrintHoverText") %>
 	  <span id="invoiceGroupListRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("invoiceGroupListRefresh", "RefreshImageLinkHoverText") %>
      <span id="invoiceGroupListGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("invoiceGroupListGridEdit","CustomizeListImageLinkHoverText") %>
    </span>
    <div style="clear:both;"></div>

  <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  String gridHtml = dgFactory.createDataGridMultiFooter(gridID,gridName,"5");
 %>

 </div>
<%=gridHtml%>

<%-- <%@ include file="/invoicemanagement/fragments/InvoiceManagement_Invoices.frag"%>--%>

<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>

</div>
</div>
</form>
</div>
</div>
<%--cquinton Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script type="text/javascript" src="/portal/js/datagrid.js"></script>
<div id="InvoiceGroupListDialogID" ></div>
<%
  String gridLayout = dgFactory.createGridLayout(gridID,gridName);
%>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  var gridID = '<%= gridID %>';
  var gridName = '<%= gridName %>';
  var viewName = '<%=EncryptDecrypt.encryptStringUsingTripleDes(viewName,userSession.getSecretKey())%>';  <%--Rel9.2 IR T36000032596 --%>
  var formName = "UploadInvoiceListForm";
  <%--  KMehta Rel8400 @ 05 Apr 2014 commented code for IR T36000023217 Start  --%>
<%--   if(gridName != "InvoicePendingActionListDataGrid"){
  var gridLayout = [
{type: "dojox.grid._CheckBoxSelector"},[
{name:"rowKey", field:"rowKey", hidden:"true"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE)%>', field:"InvoiceID", fields:["InvoiceID","InvoiceID_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"80px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.TradingPartner",TradePortalConstants.TEXT_BUNDLE)%>', field:"TradingPartner", width:"75px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.Ccy",TradePortalConstants.TEXT_BUNDLE)%>', field:"Ccy", width:"50px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.DueDate",TradePortalConstants.TEXT_BUNDLE)%>', field:"DueDate", width:"65px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.PaymentDate",TradePortalConstants.TEXT_BUNDLE)%>', field:"PaymentDate", width:"65px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.InvoiceAmount",TradePortalConstants.TEXT_BUNDLE)%>', field:"InvoiceAmount", width:"70px", cellClasses:"gridColumnRight"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.FinancedAmount",TradePortalConstants.TEXT_BUNDLE)%>', field:"FinancedAmount", width:"70px", cellClasses:"gridColumnRight"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.EstimatedInterest",TradePortalConstants.TEXT_BUNDLE)%>', field:"EstimatedInterest", width:"80px", cellClasses:"gridColumnRight"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.NetFinanceAmount",TradePortalConstants.TEXT_BUNDLE)%>', field:"NetFinanceAmount", width:"85px", cellClasses:"gridColumnRight"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.Status",TradePortalConstants.TEXT_BUNDLE)%>', field:"Status", width:"75px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.DocName",TradePortalConstants.TEXT_BUNDLE)%>', field:"DocName", fields:["DocName","DocName_linkUrl"], formatter:t360gridFormatters.invPDFFormatter, width:"100px"} ]];
  }
else {
 var gridLayout = [
{type: "dojox.grid._CheckBoxSelector"},[
{name:"rowKey", field:"rowKey", hidden:"true"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE)%>', field:"InvoiceID", fields:["InvoiceID","InvoiceID_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"80px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.TradingPartner",TradePortalConstants.TEXT_BUNDLE)%>', field:"TradingPartner", width:"75px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.Ccy",TradePortalConstants.TEXT_BUNDLE)%>', field:"Ccy", width:"50px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.DueDate",TradePortalConstants.TEXT_BUNDLE)%>', field:"DueDate", width:"65px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.PaymentDate",TradePortalConstants.TEXT_BUNDLE)%>', field:"PaymentDate", width:"65px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.InvoiceAmount",TradePortalConstants.TEXT_BUNDLE)%>', field:"InvoiceAmount", width:"70px", cellClasses:"gridColumnRight"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.LinkedInstrType",TradePortalConstants.TEXT_BUNDLE)%>', field:"LinkedtoInstrType", width:"85px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.CreditNote",TradePortalConstants.TEXT_BUNDLE)%>', field:"CreditNote", width:"85px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.Status",TradePortalConstants.TEXT_BUNDLE)%>', field:"Status", width:"75px"} ,
{name:'<%=resMgr.getTextEscapedJS("UploadInvoices.DocName",TradePortalConstants.TEXT_BUNDLE)%>', field:"DocName", fields:["DocName","DocName_linkUrl"], formatter:t360gridFormatters.invPDFFormatter, width:"100px"} ]];

}
  --%>
  <%--  KMehta Rel8400 @ 05 Apr 2014 commented code for IR T36000023217 End  --%>
  <%--set the initial search parms--%>
<%
  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
  //search - note that the value in the dropdown is encrypted
  //String encryptedOrgOid = EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
%>
  var init='userOid=<%=StringFunction.xssCharsToHtml(userOid)%>&userOrgOid=<%=StringFunction.xssCharsToHtml(userOrgOid)%>&invoiceGroupOid=<%=StringFunction.xssCharsToHtml(invoiceGroupOid)%>';
  var InvoiceListId = createDataGrid(gridID, viewName, gridLayout, init);

 function performInvoiceListAction(pressedButton){
 <%--IR T36000014931 REL ER 8.1.0.4 Changes done in this function for Authorization using authentication --%>
	 if(pressedButton == 'AuthorizeDetailInvoices')
		 {
		 <% if(InvoiceRequireAuth && InstrumentServices.isNotBlank(certAuthURL)) { %>
		 		openURL("<%=authorizeLink%>");
		 <%} else { %>
		    <%--get array of rowkeys from the grid--%>
	 		var rowKeys = getSelectedGridRowKeys(gridID);

		    <%--submit the form with rowkeys
		        becuase rowKeys is an array, the parameter name for
		        each will be checkbox + an iteration number -
		        i.e. checkbox0, checkbox1, checkbox2, etc --%>
		 	submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
		 <%} %>
		 }else{
			 <%--get array of rowkeys from the grid--%>
		 		var rowKeys = getSelectedGridRowKeys(gridID);

			    <%--submit the form with rowkeys
			        becuase rowKeys is an array, the parameter name for
			        each will be checkbox + an iteration number -
			        i.e. checkbox0, checkbox1, checkbox2, etc --%>
			 	submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
		 }
}
<%-- KMEhta Rel8400 Fix for IR T36000023304 on 31/12/2014 Start --%>
 function performInvoiceGroupListOffAuthAction(){
	 <%-- //openURL("<%=offlineAuthLink%>"); --%>
	 openReauthenticationWindowForList(certAuthURL, formName, "ProxyAuthorizeInvoices", "InvoiceG","InvoiceGroupListID");
 }
 <%-- KMEhta Rel8400 Fix for IR T36000023304 on 31/12/2014 END --%>
<%--IR T36000014931 REL ER 8.1.0.4 BEGIN --%>
 function openURL(URL){
	    if (isActive =='Y') {
	    	if (URL != '' && URL.indexOf("javascript:") == -1) {
		        var cTime = (new Date()).getTime();
		        URL = URL + "&cTime=" + cTime;
		        URL = URL + "&prevPage=" + context;
	    	}
	    }
	    document.location.href  = URL;
		return false;
}
<%--IR T36000014931 REL ER 8.1.0.4 END--%>
 function applyPaymentDateToInvGroupList(){

 	require(["t360/dialog"], function(dialog) {
 		 var parmNames1 = [ "selectedCount", "newFormName","button"];
	     var parmVals1 = [ getSelectedGridRowKeys("InvoiceGroupListID").length, "ApplyPaymentDateToSelectionOnDetailForm","ApplyPaymentDate"];
	 	      dialog.open("InvoiceGroupListDialogID", '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignPaymentDate.jsp',
		                      parmNames1, parmVals1, <%-- parameters --%>
		                      'select', this.createNewApplyPaymentValue);
		  });
 }

 function createNewApplyPaymentValue(payDate) {
	 var theForm = document.getElementsByName(formName)[0];
   	theForm.payment_date.value=payDate;
   	var rowKeys = getSelectedGridRowKeys(gridID);
    submitFormWithParms(formName,"ApplyPaymentDate", "checkbox",rowKeys);
 }
 <%--  Srinivasu_D Fix of IR#11199 Re.8.2 02/25/2013 start --%>
 <%--  Invoice List's Common action method --%>
 function performCreditListAction(pressedButton){
	    <%--get array of rowkeys from the grid--%>
   var rowKeys = getSelectedGridRowKeys("InvoicePendingActionListID");

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	 submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
 }

 <%-- Instrument Type - for Detail invoices --%>
 function assignInstrumentTypeToInvList(){

	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignInstrumentType.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("InvoicePendingActionId").length], 
			                      'select', this.createNewInstrumentTypeValue);
			  });
 }
 <%-- Callback method for Instrument Type - for Detail invoices --%>
function createNewInstrumentTypeValue(instType,loanType) {

	 var theForm = document.getElementsByName(formName)[0];
   	theForm.instrumentType.value=instType;
	theForm.loanType.value=loanType;
   	var rowKeys = getSelectedGridRowKeys(gridID);
    submitFormWithParms("UploadInvoiceListForm","ListAssignInstrType", "checkbox",rowKeys);
 }
 <%--  Srinivasu_D Fix of IR#11199 Re.8.2 02/25/2013 end --%>
 function searchInvoiceGroupList() {
	    require(["dojo/dom"],
	      function(dom){
	    	var searchParms = "userOrgOid="+<%= StringFunction.xssCharsToHtml(userOrgOid)%>+"&invoiceGroupOid="+<%=StringFunction.xssCharsToHtml(invoiceGroupOid)%>;
	        console.log("SearchParms: " + searchParms);
       		searchDataGrid(gridID,viewName,
                searchParms);
	      });
	  }

 require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	      function(query, on, t360grid, t360popup){
	    query('#invoiceGroupListRefresh').on("click", function() {
	    	searchInvoiceGroupList();
	    });
   query('#invoiceGroupListGridEdit').on("click", function() {
     var columns = t360grid.getColumnsForCustomization(gridID);
     var parmNames = [ "gridId", "gridName", "columns" ];
     var parmVals = [ gridID, gridName, columns ];
     t360popup.open(
       'invoiceGroupListGridEdit', 'gridCustomizationPopup.jsp',
       parmNames, parmVals,
       null, null);  <%-- callbacks --%>
   });
  });


require(["dojo/dom-class", "dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dijit/registry", "dojo/ready", "dojo/domReady!"],
function(domClass, query, on, t360grid, t360popup, registry,ready) {
	 ready(function() {
		<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
			domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
			domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
		  if(registry.byId("InvoiceGroupListID")){
			  var invGrid = registry.byId("InvoiceGroupListID");
			  on(invGrid,"selectionChanged", function() {
				  if (registry.byId('UploadInvoices_Actions')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Actions",0);
				  }
				  if (registry.byId('UploadInvoices_Document')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Document",0);
				  }
				  if (registry.byId('UploadInvoices_Authorize')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Authorize",0);
				  }
			 });
		 }
		  <%-- pending action auth --%>
		  if(registry.byId("InvoicePendingActionListID")){
			  var invGrid = registry.byId("InvoicePendingActionListID");
			  on(invGrid,"selectionChanged", function() {
				  if (registry.byId('UploadInvoices_Document')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Document",0);
				  }
				  if (registry.byId('UploadInvoices_Actions')){
				  	  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Actions",0);
				  }
				  if (registry.byId('UploadInvoices_Authorize')){
				  	  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Authorize",0);
				  }
			 });
		 }
	 });
 });

</script>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value='<%=gridID%>'/>
</jsp:include>

</body>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</html>
