<%--
*******************************************************************************
                        Payable Invoice Group Detail

  Description:
     This page is used to display the Payable invoices of an invoice group.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated p
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
StringBuffer      onLoad                = new StringBuffer();
String            helpSensitiveLink     = null;
String            currentTabLink        = null;
String            currentTab            = TradePortalConstants.PAYABLE_INV_GROUP_TAB;
String            formName              = null;
String            tabOn                 = null;
String            userOrgOid            = null;
String            userOid               = null;
String            userSecurityRights    = null;
String            userSecurityType      = null;
StringBuffer      dynamicWhereClause           = new StringBuffer();
StringBuffer      dropdownOptions              = new StringBuffer();
int               totalOrganizations           = 0;
StringBuffer      extraTags                    = new StringBuffer();
StringBuffer      newLink                      = new StringBuffer();
StringBuffer      newUploadLink                = new StringBuffer();
String  timeZone						   		= null;
String            userDefaultWipView           = null; 
String            selectedWorkflow             = null;
String            selectedStatus               = "";
String            loginLocale                  = null;
Vector            codesToExclude               = null;
String searchListViewName = "PayablesPrgmInvoiceDetailListDataView.xml";

userSession.setCurrentPrimaryNavigation(resMgr.getText("NavigationBar.InvoiceManagement", TradePortalConstants.TEXT_BUNDLE));

if ( "true".equals( request.getParameter("returning") ) ) {
  userSession.pageBack(); //to keep it correct
}
else {
  userSession.addPage("goToPayableProgramInvoiceDetail");
}

   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgOid         = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
   userOid            = StringFunction.xssCharsToHtml(userSession.getUserOid());
   timeZone			  = StringFunction.xssCharsToHtml(userSession.getTimeZone());		
   session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
   // getting the user security type to display authorization button or not.
   boolean isAdminUser;
   if (userSession.getSavedUserSession() == null) {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSecurityType());
   } else {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType());
   }

   CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   String corporateOrgPayInvAllowH2HApprovalInd = corpOrg.getAttribute("allow_h2h_pay_inv_approval");
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.getById(userOid);
   
   boolean groupingEnabled = true;
   if (groupingEnabled) {
	   session.removeAttribute("fromInvoiceGroupDetail");
   }

   formName = "UploadInvoiceListForm";
   helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm", resMgr, userSession);
   loginLocale = userSession.getUserLocale();

   String invoiceGroupOid = StringFunction.xssCharsToHtml(request.getParameter("invoice_group_oid"));
   if(InstrumentServices.isNotBlank(invoiceGroupOid)){
	   session.setAttribute("invGrpOid",invoiceGroupOid);
	}
   else{
	   invoiceGroupOid =(String) session.getAttribute("invGrpOid");
   }
   Hashtable secureParms = new Hashtable();
   secureParms.put("UserOid", userSession.getUserOid());
   secureParms.put("SecurityRights",   userSecurityRights);
   secureParms.put("ownerOrg",   userOrgOid);
   secureParms.put("clientBankOid", userSession.getClientBankOid());
   if(InstrumentServices.isNotBlank(invoiceGroupOid))
   secureParms.put("invoice_group_oid", invoiceGroupOid);
   
   if (invoiceGroupOid != null) {
	  invoiceGroupOid = EncryptDecrypt.decryptStringUsingTripleDes(invoiceGroupOid, userSession.getSecretKey()); 
   }

   InvoiceGroupWebBean invoiceGroup = beanMgr.createBean(InvoiceGroupWebBean.class, "InvoiceGroup");
   invoiceGroup.getById(invoiceGroupOid);
   String groupDueDatesql = "SELECT due_date FROM invoice_group_view WHERE invoice_group_oid = ? ";
   DocumentHandler invoiceDueDateDoc = DatabaseQueryBean.getXmlResultSet(groupDueDatesql, false, new Object[]{invoiceGroupOid});
   String dueDate = "";
   String paymentDate = "";
   String endToEndID = "";
   if(invoiceDueDateDoc !=null){
	   dueDate = (invoiceDueDateDoc.getFragment("/ResultSetRecord")).getAttribute("DUE_DATE");
   }  
   if(StringFunction.isBlank(dueDate)){
	   dueDate = invoiceGroup.getAttribute("payment_date");
   }
   
   StringBuilder invoicesForHeader = new StringBuilder(resMgr.getText("UploadInvoiceDetail.InvoicesFor", TradePortalConstants.TEXT_BUNDLE));
   invoicesForHeader.append(" - ").append(invoiceGroup.getAttribute("trading_partner_name"));
   invoicesForHeader.append(" - ").append(invoiceGroup.getAttribute("currency"));
   invoicesForHeader.append(" - ").append(resMgr.getText("UploadInvoiceDetail.Date", TradePortalConstants.TEXT_BUNDLE));
   invoicesForHeader.append(": ").append(TPDateTimeUtility.formatDate(dueDate, TPDateTimeUtility.SHORT, loginLocale));
   if(StringFunction.isNotBlank(invoiceGroup.getAttribute("end_to_end_id"))){
	   endToEndID = invoiceGroup.getAttribute("end_to_end_id");
	   invoicesForHeader.append("  ").append(resMgr.getText("UploadInvoices.EndToEndId", TradePortalConstants.TEXT_BUNDLE));
	   invoicesForHeader.append(": ").append(invoiceGroup.getAttribute("end_to_end_id"));
   }
%>


<%  // reauthenticate if necessary
    String certAuthURL = ""; //needed for openReauth frag
    Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
    String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
    boolean InvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__PAYABLES_UPLOAD_INV_PROCESS);
    boolean offlineAuthoriseRequireAuth = SecurityAccess.hasRights(userSecurityRights,  SecurityAccess.UPLOAD_PAY_MGM_OFFLINE_AUTH);
    String authorizeLink = "";
    String proxyAuthLink = "";
%>
<%-- ********************* HTML for page begins here *********************  --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>


<%-- Body tag included as part of common header --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad.toString()%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <%-- IR T36000030360: (gridInvoiceFooterDivHidden) Hide page content, untill widgets get parsed. --%>
  <div id="InvoiceMgtDivID" class="pageContent gridInvoiceFooterDivHidden">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="InvoiceMgmt.PayInvGroupInvList" />
      <jsp:param name="helpUrl" value="customer/receivables_grouped_invoice_list.htm" />
    </jsp:include>

    <%--replace return link with close button--%>
    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <%= invoicesForHeader %>
      </span>
      <span class="pageSubHeader-right">
        <%-- change return link to close button when no close button present.
            note this does NOT include an icon as in the sidebar--%>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              '<%= formMgr.getLinkAsUrl("goToInvoiceManagement", response) %>';
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>

<form name="<%=formName%>" method="POST" action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">
  <jsp:include page="/common/ErrorSection.jsp" />
    <div class="formContentNoSidebar">
   <input type=hidden value="" name=buttonName>
	<input type=hidden name="payment_date">
	<input type="hidden" name="loanType" />
	<input type="hidden" name="instrumentType" /> 
	<input type="hidden" name="bankBranch" />
	<input type="hidden" value="yes" name="fromPayDetail">
	<input type=hidden  name="timeZone" value= "<%=timeZone%>">
	<input type=hidden  name="baseCurrencyCode" value = "<%=StringFunction.xssCharsToHtml(userSession.getBaseCurrencyCode())%>">
	<input type="hidden" name="supplierDate" />
    <input type="hidden" name="paymentAmount" />
<%  //VASCO reauthentication hidden fields
    if ( InvoiceRequireAuth || offlineAuthoriseRequireAuth) {
%> 

      <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
      <input type=hidden name="reCertOK">
      <input type=hidden name="logonResponse">
      <input type=hidden name="logonCertificate">

<%
    }
%>
   

<% 
    String gridID   = "PayablesPrgmInvoiceDetailListID";
	String gridName = "PayablesPrgmInvoiceDetailListDataGrid";
	String viewName = "PayablesPrgmInvoiceDetailListDataView";
	String resourcePrefix = "UploadInvoices";
	
	 String linkArgs[] = {"","","","","",""};
	 String arg = "key~"+viewName+ "`key~" + resourcePrefix;

	  linkArgs[0] = TradePortalConstants.PDF_LIST;
	          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(arg);//Use common key for encryption of parameter to doc producer
	          linkArgs[2] = StringFunction.isNotBlank(resMgr.getResourceLocale())? resMgr.getResourceLocale():"en_US";
	          linkArgs[3] = userSession.getBrandingDirectory();
	          String linkString =  formMgr.getDocumentLinkAsURLInFrame(resMgr.getText("UploadInvoiceDetail.PrintInvoiceDetails",
	                  TradePortalConstants.TEXT_BUNDLE),
	                  TradePortalConstants.PDF_LIST,
	                  linkArgs,
	                  response) ;
%>

<%
    if (InvoiceRequireAuth || offlineAuthoriseRequireAuth) {
%>   
  <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
  <%
      
  
  authorizeLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
          "'" + formName + "','PayPgmDetailListAuthorise'," +
          "'Invoice','"+gridID+"')";

    }
    //vasco end
  %> 

 <div class="gridSearch">
  <div class="searchHeader">
   <span class="searchHeaderCriteria">
	<%=resMgr.getText("InvoiceList.Invoices",TradePortalConstants.TEXT_BUNDLE)%>

	</span>
 	<span class="searchHeaderActions">
 	<jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value='<%=gridID%>'/>
      </jsp:include>
    <button data-dojo-type="dijit.form.Button" name="print" id="print" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.PrintText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          window.open( "<%=linkString%>");
		  </script>
        </button>
         <%=widgetFactory.createHoverHelp("print", "PrintHoverText") %>
 	  <span id="payInvoiceGroupListRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("payInvoiceGroupListRefresh", "RefreshImageLinkHoverText") %>
      <span id="payInvoiceGroupListGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("payInvoiceGroupListGridEdit","CustomizeListImageLinkHoverText") %>
    </span>
    <div style="clear:both;"></div>

  <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  String gridHtml = dgFactory.createDataGridMultiFooter(gridID,gridName,"5");
 %>
 
 </div>
<%=gridHtml%>

<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>


</div>
</div>

</form>
</div>
</div>
<form method="post" id="PayInvoiceSummary-NotifyPanelUserForm" name="PayInvoiceSummary-NotifyPanelUserForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden name="buttonName" value="">
<input type=hidden name="invoiceSummaryOid" value=""> 
<%= formMgr.getFormInstanceAsInputField("PayInvoiceSummary-NotifyPanelUserForm", secureParms) %>
 </form>
<%-- <%@ include file="/invoicemanagement/fragments/InvoiceManagement_PayableInvoices.frag"%>--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script type="text/javascript" src="/portal/js/datagrid.js"></script>
 <div id="PayablesPrgmInvoiceListDialogID" ></div>
<%
  String gridLayout = dgFactory.createGridLayout(gridID,gridName);
%>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
 
  var gridID = '<%= gridID %>';
  var gridName = '<%= gridName %>';
  var viewName = '<%=EncryptDecrypt.encryptStringUsingTripleDes(viewName,userSession.getSecretKey())%>';  <%--Rel9.2 IR T36000032596 --%>
  var formName = "UploadInvoiceListForm";
  <%--set the initial search parms--%>
<%
  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
  //search - note that the value in the dropdown is encrypted
%>
  var init='userOid=<%=userOid%>&userOrgOid=<%=userOrgOid%>&invoiceGroupOid=<%=invoiceGroupOid%>';
  var InvoiceListId = createDataGrid(gridID, viewName, gridLayout, init);

 function performInvPayPrgmAction(pressedButton){
	    <%--get array of rowkeys from the grid--%>
 var rowKeys = getSelectedGridRowKeys(gridID);
 if(rowKeys == ""){
		alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
			"common.selectMoreItems",
			TradePortalConstants.TEXT_BUNDLE))%>');
		return;
}
 	    submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
}
 function openURL(URL){
	 if (isActive =='Y') {
		 if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
		 }
	 }

	  document.location.href  = URL;
	  return false;
}

 function authoriseInvoices(){
	 require(["dojo/_base/array"], 
	        	function(array) {
	  var rowKeys = getSelectedGridRowKeys(gridID);
		 if(rowKeys == ""){
				alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
					"common.selectMoreItems",
					TradePortalConstants.TEXT_BUNDLE))%>');
				return;
		}
		 var items = getSelectedGridItems(gridID);
			var apprInd = "";
			var endToEndID = "";
		 	array.forEach(items, function(item){
	      		apprInd = item.i.ApprovalInd;
	      		endToEndID = item.i.EndToEndID;
	      	}); 
		 	var payCRAllowH2HApprovalInd = <%=TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAllowH2HApprovalInd)%>;
			if(apprInd!= "N" && payCRAllowH2HApprovalInd && endToEndID!= ""){
				if(confirm("<%= resMgr.getText("UploadInvoices.DeclineE2EInvoicesAlert", TradePortalConstants.TEXT_BUNDLE)%>")){
					 <%if (InvoiceRequireAuth && StringFunction.isNotBlank(certAuthURL) &&  StringFunction.isNotBlank(authorizeLink) ) {%>
				  		openURL("<%=authorizeLink%>");
				  <%} else {%>		
				 	submitFormWithParms(formName, "PayPgmDetailListAuthorise", "checkbox", rowKeys);
				    <%}%>
		 		}
		 		else{
		 			return false;
		 		}
			}
		 
	  <%if (InvoiceRequireAuth && StringFunction.isNotBlank(certAuthURL) &&  StringFunction.isNotBlank(authorizeLink) ) {%>
	  		openURL("<%=authorizeLink%>");
	  <%} else {%>		
	 	submitFormWithParms(formName, "PayPgmDetailListAuthorise", "checkbox", rowKeys);
	    <%}%>
	 });	 
 }
 
 function OfflineAuthInvoices(){
	  var rowKeys = getSelectedGridRowKeys(gridID);
		 if(rowKeys == ""){
				alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
					"common.selectMoreItems",
					TradePortalConstants.TEXT_BUNDLE))%>');
				return;
		}
	  <%if (StringFunction.isNotBlank(certAuthURL) && StringFunction.isNotBlank(authorizeLink)) {%>
	  		openURL("<%=authorizeLink%>");
	
	    <%}%>
 }
 
 <%--Added for CR 1006 --%>
 function performInvoiceDecline(pressedButton){
 var formName = '<%=formName%>';
 require(["dojo/_base/array"], 
    	function(array) {
		    <%--get array of rowkeys from the grid--%>
		    var rowKeys = getSelectedGridRowKeys(gridID);
			var items = getSelectedGridItems(gridID);
			var endToEndID = "<%=endToEndID%>";
		 	var payInvAllowH2HApprovalInd = <%=TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAllowH2HApprovalInd)%>;
		 	if(payInvAllowH2HApprovalInd && endToEndID!=""){
		 		if(confirm("<%= resMgr.getText("UploadInvoices.DeclineE2EInvoicesAlert", TradePortalConstants.TEXT_BUNDLE)%>")){
		 			submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
		 		}
		 		else{
		 			return false;
		 		}
		 	}else{
		 		submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
		 	}
	});	 	
 }
 
  function closePaymentDialog(){
			require(["t360/dialog"], function(dialog) {
				dialog.hide('copyPaymentsDialog');
			});
  }
		
 function applyPaymentDateToPayPrgmGroupList(){
	 var rowKeys = getSelectedGridRowKeys(gridID);
	 if(rowKeys == ""){
			alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
				"common.selectMoreItems",
				TradePortalConstants.TEXT_BUNDLE))%>');
			return;
	}
 	require(["t360/dialog"], function(dialog) {
 		
 		      dialog.open('PayablesPrgmInvoiceListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignPaymentDate.jsp',
		                      ['selectedCount'], [rowKeys.length], 
		                      'select', this.createNewApplyPaymentValue);
		  });
}
 function createNewApplyPaymentValue(payDate) {
	 var theForm = document.getElementsByName(formName)[0];
   	theForm.payment_date.value=payDate;
   	var rowKeys = getSelectedGridRowKeys(gridID);
    submitFormWithParms("UploadInvoiceListForm","PayPgmApplyPayDate", "checkbox",rowKeys);
 }
 
 function modifySuppDateToPayPrgmGroupList(){ 
	 var rowKeys = getSelectedGridRowKeys(gridID);
	 if(rowKeys == ""){
			alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
				"common.selectMoreItems",
				TradePortalConstants.TEXT_BUNDLE))%>');
			return;
	}
     require(["t360/dialog"], function(dialog) {
           dialog.open('PayablesPrgmInvoiceListDialogID', '<%=resMgr.getText("UploadInvoices.InvPayModifySuppDate", TradePortalConstants.TEXT_BUNDLE)%>',
                           'ModifySupplierDate.jsp',
                           ['selectedCount'], [rowKeys.length], 
                           'select', this.modifySuppDateValue);
       }); 

}
function modifySuppDateValue(suppDate) {
     var theForm = document.getElementsByName(formName)[0];
     theForm.supplierDate.value=suppDate;
     var rowKeys = getSelectedGridRowKeys(gridID);
   submitFormWithParms("UploadInvoiceListForm","ModifySupplierDate", "checkbox", rowKeys);
}

function ApplyPayAmtToPayPrgmGroupList(){  
	var rowKeys = getSelectedGridRowKeys(gridID);
	 if(rowKeys == ""){
			alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
				"common.selectMoreItems",
				TradePortalConstants.TEXT_BUNDLE))%>');
			return;
	}
    require(["t360/dialog"], function(dialog) {
          dialog.open('PayablesPrgmInvoiceListDialogID', '<%=resMgr.getText("UploadInvoices.ApplyEarlyPaymentAmount", TradePortalConstants.TEXT_BUNDLE)%>',
                          'ApplyEarlyPaymentAmt.jsp',
                          ['selectedCount'], [rowKeys.length], 
                          'select', this.applyPaymentAmtValue);
      }); 

}
function applyPaymentAmtValue(paymentAmt) {
    var theForm = document.getElementsByName(formName)[0];
    theForm.paymentAmount.value=paymentAmt;
    var rowKeys = getSelectedGridRowKeys(gridID);
  submitFormWithParms("UploadInvoiceListForm","ListApplyEarlyPayAmt", "checkbox", rowKeys);
}

function payablesPanelStatusFormatter(columnValues){
	  var gridLink = "";
	 	if(columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH%>' ||
	  		columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED%>'){
			 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
			 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"PayablesPrgmInvoiceDetailListID\",\"PAY_INV_SUMMARY\");'>"+columnValues[0]+"</a>";
	  		 return gridLink;
		}else{
			return columnValues[0];
		}
	}

 function searchInvoiceGroupList() {
	    require(["dojo/dom"],
	      function(dom){
	    	var searchParms = "userOrgOid="+<%=userOrgOid%>+"&invoiceGroupOid="+<%=invoiceGroupOid%>;

	        console.log("SearchParms: " + searchParms);
        searchDataGrid(gridID,viewName,
                searchParms);
	      });
	  }

 require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	      function(query, on, t360grid, t360popup){
	    query('#payInvoiceGroupListRefresh').on("click", function() {
	    	searchInvoiceGroupList();
	    });
   query('#payInvoiceGroupListGridEdit').on("click", function() {
     var columns = t360grid.getColumnsForCustomization(gridID);
     var parmNames = [ "gridId", "gridName", "columns" ];
     var parmVals = [ gridID, gridName, columns ];
     t360popup.open(
       'payInvoiceGroupListGridEdit', 'gridCustomizationPopup.jsp',
       parmNames, parmVals,
       null, null);  <%-- callbacks --%>
   });
  });

 <%-- KMehta - 13 Oct 2014 - Rel9.1 IR-T36000033383 - Added domClass  --%>
require(["dojo/query", "dojo/on", "dojo/dom-class", "t360/datagrid", "t360/popup", "dijit/registry", "dojo/ready", "dojo/domReady!"],
function(query, on, domClass, t360grid, t360popup, registry,ready) {
	 ready(function() {
		<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
			domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
			domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");

		  if(registry.byId("PayablesPrgmInvoiceDetailListID")){
			  var invGrid = registry.byId("PayablesPrgmInvoiceDetailListID");			   
			  on(invGrid,"selectionChanged", function() {
				  if (registry.byId('UploadInvoices_Actions')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Actions",0);
				  }
				  if (registry.byId('UploadInvoices_Document')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Document",0);
				  }
				  if (registry.byId('UploadInvoices_Authorize')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Authorize",0);
				  }				  
			 });
		 } 
	 });
 });

</script>

<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value='<%=gridID%>'/>
</jsp:include>

</body>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</html>