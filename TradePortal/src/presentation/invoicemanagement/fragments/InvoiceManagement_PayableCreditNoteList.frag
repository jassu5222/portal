<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                        Invoice Management Payable Credit Note List

  Description:
    Contains HTML to the list views that appear on the Payable Credit note list

  This is not a standalone frag.  It MUST be included using the following tag:
  <%@ include file="InvoiceManagement_PayableCreditNoteList.frag" %>
*******************************************************************************
--%> 
<%

      selectedStatus = request.getParameter(statusSearchParm);
      // Build the status dropdown options 
      // re-selecting the most recently selected option.
      dropdownOptions = new StringBuffer(Dropdown.createSortedRefDataIncludeOptions(statusRefData,
                                             selectedStatus,resMgr, loginLocale, codesToExclude,false));

%>


<div class="gridSearch">
  <div class="searchHeader">  
   <span class="searchHeaderCriteria">
 	
<%                 

out.print(widgetFactory.createSearchSelectField(searchID, "InvoiceSearch.Show",
		TradePortalConstants.STATUS_ALL,
        dropdownOptions.toString(),onSearch));     
 

%>
   </span>
	<span class="searchHeaderActions">
	<jsp:include page="/common/gridShowCount.jsp">
		<jsp:param name="gridId" value="PayablesCreditNoteListId" />
	</jsp:include>
 	  <span id="<%=dataRefreshID%>" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp(dataRefreshID, "RefreshImageLinkHoverText") %>
      <span id="<%=dataEditID%>" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp(dataEditID,"CustomizeListImageLinkHoverText") %> 
    </span>
   
    <div style="clear:both;"></div>
 
 <%
    gridHtmlSummary = dgFactory.createDataGridMultiFooter(gridID, gridName, buttonDisplay);
%>
</div>

<%=gridHtmlSummary%>
</div>
