
<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************

 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
 	String baseCurrency = userSession.getBaseCurrencyCode();
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  /* KMehta Rel8400 @ 02/05/2014 IR T36000023217 */
  String gridLayout = dgFactory.createGridLayout("InvoiceUploadListId","InvoicePayablePendingGroupListDataGrid");	
  String gridLRLayout ="";
  if(isLRQEnabled){
   gridLRLayout = dgFactory.createGridLayout("InvoiceLRListId","InvoicePayableLRGroupListDataGrid");
  }
  String gridPayPrgmLayout = "";
  if(isIntegratedPayPrgmEnabled){//913
	  gridPayPrgmLayout = dgFactory.createGridLayout("PayablesPrgmInvoiceGroupListId","PayablesPrgmInvoiceGroupListDataGrid");
  }
  String gridCreditLayout ="";
  if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){
   gridCreditLayout = dgFactory.createGridLayout("PayablesCreditNoteListId","PayablesCreditNoteListDataGrid");
  }
%>

<script type="text/javascript">

var formatGridLinkChild=function(columnValues, idx, level){

    var gridLink="";
  
    if(level == 0){
      gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
      return gridLink;
      
    }else if(level ==1){
      gridLink = "<a href='" + columnValues[2] + "'>" + columnValues[0] + "</a>";
      return gridLink;
    }
  }
var savedSearchQueryDataPen = {
				<% 
				if(savedSearchQueryDataPen !=null && !savedSearchQueryDataPen.isEmpty()) {
					Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataPen.entrySet().iterator();
				while (entries.hasNext()) {
					Map.Entry<String, String> entry = entries.next(); 
					%>
				'<%=entry.getKey()%>':'<%=entry.getValue()%>'
					<%if(entries.hasNext()) {%>
					<%=comma%>
					
				<%
					}
				}
				}%>
	}
var savedSearchQueryDataLR = {
		<% 
		if(savedSearchQueryDataLR !=null && !savedSearchQueryDataLR.isEmpty()) {
			Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataLR.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<String, String> entry = entries.next(); 
			%>
		'<%=entry.getKey()%>':'<%=entry.getValue()%>'
			<%if(entries.hasNext()) {%>
			<%=comma%>
			
		<%
			}
		}
		}%>
}
var savedSearchQueryDataCR = {
		<% 
		if(savedSearchQueryDataCR !=null && !savedSearchQueryDataCR.isEmpty()) {
			Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataCR.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<String, String> entry = entries.next(); 
			%>
		'<%=entry.getKey()%>':'<%=entry.getValue()%>'
			<%if(entries.hasNext()) {%>
			<%=comma%>
			
		<%
			}
		}
		}%>
}
var savedSearchQueryDataPayPrgm = {
		<% 
		if(savedSearchQueryDataPayPrgm !=null && !savedSearchQueryDataPayPrgm.isEmpty()) {
			Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataPayPrgm.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<String, String> entry = entries.next(); 
			%>
		'<%=entry.getKey()%>':'<%=entry.getValue()%>'
			<%if(entries.hasNext()) {%>
			<%=comma%>
			
		<%
			}
		}
		}%>
}
<%-- IR 16481 end --%>
  var gridPayPrgmLayout;
  <%if(isIntegratedPayPrgmEnabled){%>
     gridPayPrgmLayout = <%= gridPayPrgmLayout %>; <%-- CR913 --%>
  <%}%>
  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  var gridLRLayout;
  <%if(isLRQEnabled){%>
   gridLRLayout = <%= gridLRLayout %>;
  <%}%>
  
   var gridCreditLayout;
  <%if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){%>
  gridCreditLayout = <%=gridCreditLayout%>;
    <%}%>
  
  

  var init='userOid=<%=StringFunction.xssCharsToHtml(userOid)%>&userOrgOid=<%=StringFunction.xssCharsToHtml(userOrgOid)%>';
  var initSearchParms = "";
  var invoiceStatusType = savedSearchQueryDataPen["invoiceStatusType"];
  var invoiceLRStatusType = savedSearchQueryDataLR["invoiceLRStatusType"];
  var initLRSearchParms = "";
  var payablesPrgmInvStatusType = savedSearchQueryDataPayPrgm["payablesPrgmInvStatusType"];
  var initPayPrgmSearchParms = "";
  var formName = "PayableInvoiceGroupListForm";
  var rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");
  var savedSortLR = savedSearchQueryDataLR["sort"];
  var savedSortPen = savedSearchQueryDataPen["sort"];
  var savedSortPayPrgm = savedSearchQueryDataPayPrgm["sort"];
  var invoiceCRStatusType = savedSearchQueryDataCR["invoiceStatusType"]; <%-- IR 36527 --%>
  var savedSortCR = savedSearchQueryDataCR["sort"];
  var initCRSearchParms = "";
	var viewCRName ="";
 function initParms(){
	 require(["dojo/dom","dijit/registry"],
		      function(dom,registry){	

		 if("" == invoiceStatusType || null == invoiceStatusType || "undefined" == invoiceStatusType){
			 invoiceStatusType=registry.byId('invoiceStatusType').value;
		 }
		 else{
			if("All" !=invoiceStatusType)
				registry.byId("invoiceStatusType").set('value',invoiceStatusType);
		 }
		 if("" == invoiceStatusType){
			 invoiceStatusType = 'All';
			}

		 <%if(isLRQEnabled){%>
		 if("" == invoiceLRStatusType || null == invoiceLRStatusType || "undefined" == invoiceLRStatusType)
			 invoiceLRStatusType=dom.byId('invoiceLRStatusType').value;	
		 else{
			 if("All" !=invoiceLRStatusType)
			 registry.byId("invoiceLRStatusType").set('value',invoiceLRStatusType);
		 }
		
		 if("" == invoiceLRStatusType){
			 invoiceLRStatusType = 'All';
			}
		 if("" == savedSortLR || null == savedSortLR || "undefined" == savedSortLR){
			 savedSortLR = 'TradingPartner';
			 
    	 }
		 <%}%>
		 
		 if("" == savedSortPen || null == savedSortPen || "undefined" == savedSortPen){
			 savedSortPen = 'TradingPartner';
    	 }
	     <%--  Nar CR 913 Begin --%>
		 <%if(isIntegratedPayPrgmEnabled){%>
    	 if("" == savedSortPayPrgm || null == savedSortPayPrgm || "undefined" == savedSortPayPrgm){
    		 savedSortPayPrgm = 'TradingPartner';
    	 }

    	 if("" == payablesPrgmInvStatusType || null == payablesPrgmInvStatusType || "undefined" == payablesPrgmInvStatusType){
	    	 payablesPrgmInvStatusType=registry.byId('payablesPrgmInvStatusType').value;
	     } 
	    else{
		if("All" !=payablesPrgmInvStatusType)
			registry.byId("payablesPrgmInvStatusType").set('value',payablesPrgmInvStatusType);
	    }
	    if("" == payablesPrgmInvStatusType){
	    	payablesPrgmInvStatusType = 'All';
		}
	    
	    <%}%>
	    <%--  Nar CR 913 End --%>
	    
	     <%if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){%>
		 if("" == invoiceCRStatusType || null == invoiceCRStatusType || "undefined" == invoiceCRStatusType)
			 invoiceCRStatusType=dom.byId('invoiceCRStatusType').value;	
		 else{
			 if("All" !=invoiceCRStatusType)
			 registry.byId("invoiceCRStatusType").set('value',invoiceCRStatusType);
		 }
		
		 if("" == invoiceCRStatusType){
			 invoiceCRStatusType = 'All';
			}
		 if("" == savedSortCR || null == savedSortCR || "undefined" == savedSortCR){
			 savedSortCR = 'TradingPartner';
    	 }
		 <%}%>
		}); 
	    	     
	 initPayPrgmSearchParms = init+"&payablesPrgmInvStatusType="+payablesPrgmInvStatusType;	  <%--  Nar CR 913 --%>
	 initSearchParms = init+"&invoiceStatusType="+invoiceStatusType;
	 initLRSearchParms = init+"&invoiceLRStatusType="+invoiceLRStatusType;
	 initCRSearchParms = init+"&invoiceStatusType="+invoiceCRStatusType;
	 

  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoicePayablePendingGroupListDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var InvoiceUploadListId = createDataGrid("InvoiceUploadListId", viewName, gridLayout, initSearchParms,savedSortPen);
  <%if(isLRQEnabled){%>
  viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoicePayableLRGroupListDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var InvoiceLRId = createDataGrid("InvoiceLRListId", viewName, gridLRLayout, initLRSearchParms,savedSortLR);
 <%}%>
  <%-- Nar 913 --%>
  <%if(isIntegratedPayPrgmEnabled){%>
  viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PayablesPrgmInvoiceGroupListDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
     var PayablesPrgmInvoiceGroupListId = createDataGrid("PayablesPrgmInvoiceGroupListId", viewName, gridPayPrgmLayout, initPayPrgmSearchParms, savedSortPayPrgm);
  <%}%>
   <%if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){%>
  viewCRName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PayablesCreditNoteListDataView",userSession.getSecretKey())%>"; 
  <%-- T36000034958 - Rel-9.2 CR-914A 12/02/2014 Using createMultiSelectLazyTreeDataGrid() function. --%>
  <%-- var PayablesCreditNoteListId = createLazyTreeDataGrid("PayablesCreditNoteListId", viewCRName, gridCreditLayout, init,'TradingPartner'); --%>
  var PayablesCreditNoteListId = createMultiSelectLazyTreeDataGrid("PayablesCreditNoteListId", viewCRName, gridCreditLayout, init,'TradingPartner');
 <%}%>
 }
 
 var quickViewFormatter=function(columnValues, idx, level) {
	    var gridLink="";
	  
	    if(level == 0){
	  
	      if ( columnValues.length >= 2 ) {
	        gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
	      }
	      else if ( columnValues.length == 1 ) {
	        <%-- no url parameters, just return the display --%>
	        gridLink = columnValues[0];
	      }
	      return gridLink;
	  
	    }else if(level ==1){
	      if(columnValues[3]=='PROCESSED_BY_BANK'){
	        if(columnValues[2]){
	          return "<a href='javascript:dialogQuickView("+columnValues[2]+");'>Quick View</a> "+columnValues[0]+ " ";
	        }else{
	          return columnValues[0];   
	        }
	  
	      }else{
	        return columnValues[0];
	      }
	  
	    }else{
	    	return "<a href='javascript:dialogQuickView("+columnValues[2]+");'>Quick View</a> "+columnValues[0]+ " ";
	    }
	    }
  function searchPending(){
		 invoiceStatusType=dijit.byId("invoiceStatusType").value;
		 if("" == invoiceStatusType){
			 invoiceStatusType = 'All';
			}
		  var searchParms = init+"&invoiceStatusType="+invoiceStatusType;
	      searchDataGrid("InvoiceUploadListId", "InvoicePayablePendingGroupListDataView",
	                     searchParms);
	  }
 
 function searchLoanRequest(){
		 invoiceLRStatusType=dijit.byId("invoiceLRStatusType").value;
		 if("" == invoiceLRStatusType){
			 invoiceLRStatusType = 'All';
			}
		  var searchParms = init+"&invoiceLRStatusType="+invoiceLRStatusType;
	      searchDataGrid("InvoiceLRListId", "InvoicePayableLRGroupListDataView",
	                     searchParms);
	  }

  function performAction(pressedButton){
	    var formName = '<%=formName%>';

	    <%--get array of rowkeys from the grid--%>
	    var rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	   submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
  }
  
	function createApprovalToPay(bankBranchArray){

		<%--get array of rowkeys from the grid--%>
		rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");
		var items=getSelectedGridItems("InvoiceUploadListId");

		if(rowKeys == ""){
			alert("Please pick a record and click 'Select'");
			return;
		}
		if ( bankBranchArray.length == 1 ) {
	    	createPayableATP(bankBranchArray[0]);
		} else {
			require(["t360/dialog"], function(dialog ) {
		      dialog.open('bankBranchSelectorDialog', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'bankBranchSelectorDialog.jsp',
		                  null, null, <%-- parameters --%>
		                  'select', this.createPayableATP);
		    });
		 }
		}

		function createPayableATP(bankBranchOid) {
			submitInvFormWithParms(bankBranchOid);
			
		}

		function closePaymentDialog(){
			require(["t360/dialog"], function(dialog) {
				dialog.hide('copyPaymentsDialog');
			});
		}
		
		function submitInvFormWithParms(bankBranchOid) {
			buttonName='CreateApprovalToPay';
			parmName="checkbox";
			parmValue = rowKeys;
		      <%-- verify arguments at development time --%>
		      if ( formName == null ) {
		        alert('Problem submitting form: formName required');
		      }
		      if ( buttonName == null ) {
		        alert('Problem submitting form: buttonName required');
		      }
		      if ( parmName == null ) {
		        alert('Problem submitting form: parmName required');
		      }
		  
		      <%-- get the form and submit it --%>
		      if ( document.getElementsByName(formName).length > 0 ) {
		        var myForm = document.getElementsByName(formName)[0];
		        <%-- generate url parameters for the rowKeys and add to  --%>
		        <%-- form as hidden fields --%>
		        if (parmValue instanceof Array) {
		          for (var myParmIdx in parmValue) {
		            var myParmValue = parmValue[myParmIdx];
		            var myParmName = parmName + myParmIdx;
		            addParmToForm(myForm, myParmName, myParmValue);
		          }
		        }
		        else { <%-- assume a single --%>
		        	 addParmToForm(myForm, parmName, parmValue);
		          
		        }
				 var theForm = document.getElementsByName(formName)[0];
				 
   				theForm.bankBranch.value=bankBranchOid;
		        <%-- addParmToForm(myForm, 'bankBranch', bankBranchOid); --%>
		        var formNumber = getFormNumber(formName);
		        setButtonPressed(buttonName, formNumber); 
		        myForm.submit();
		      } else {
		        alert('Problem submitting form: form ' + formName + ' not found');
		      }
		    }
		 var certAuthURL = '<%=certAuthURL%>';
  function performAuthorization(){
	    var formName = '<%=formName%>';
	   
	    
	    <%if(InvoiceRequireAuth){%>    	      
	    	openReauthenticationWindowForList(certAuthURL, formName, "ListAuthorizeInvoices" , "InvoiceG");	     
	    <%}else{%>
	    
	    <%--get array of rowkeys from the grid--%>
	    var rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	    submitFormWithParms(formName, "ListAuthorizeInvoices", "checkbox", rowKeys);
       <%}%>
  }
  
  function applyPaymentDateToInvGroupList(){
	 
	    	require(["t360/dialog"], function(dialog) {
	    		dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignPaymentDate.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceUploadListId").length], 
			                      'select', this.createNewApplyPaymentValue);
			  }); 

 }
  
  function createNewApplyPaymentValue(payDate) {
		 var theForm = document.getElementsByName(formName)[0];
	   	theForm.paymentDate.value=payDate;
	   	var rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");
	    submitFormWithParms("PayableInvoiceGroupListForm","ApplyPaymentDate", "checkbox",rowKeys);
	 }
  function searchCreditNote(){
   require(["t360/datagrid", "dijit/registry","dojo/on", "dojo/ready", "dojo/domReady!"], function(t360grid, registry, on,ready){
		 invoiceStatusType=dijit.byId("invoiceCRStatusType").value;
		 if("" == invoiceStatusType){
			 invoiceStatusType = 'All';
			}
		  var searchParms = init+"&invoiceStatusType="+invoiceStatusType;
		  var newGridLayout = registry.byId('PayablesCreditNoteListId').get('structure');
		  t360grid.searchMultiSelectLazyTreeDataGrid("PayablesCreditNoteListId", viewCRName, newGridLayout,searchParms);
		  
		  ready(function() {
 			 <%-- SSikhakolli - Rel-9.2 CR-914A 02/17/2015 - Addinng below code to protect child rows from mouse selection. --%>
	          	var myGrid = registry.byId("PayablesCreditNoteListId");
	          myGrid.onCanSelect=function(idx){
					var selectedItem = this.getItem(idx).i;
					var isChild = (selectedItem.CHILDREN != "true");
				   	return (!isChild);
				};
 		});
	
		  <%--  Rel 9.2 - IR 36527 Start  --%>
		  if(registry.byId("PayablesCreditNoteListId")){
			  var crtPGrid = registry.byId("PayablesCreditNoteListId");			   
			  on(crtPGrid,"selectionChanged", function() {
				  if (registry.byId('UploadInvoices_CRActions')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRActions",0);
				  }
				  if (registry.byId('UploadInvoices_CRDocument')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRDocument",0);
				  }	
				  if (registry.byId('UploadInvoices_CRAuthorize')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRAuthorize",0);
				  }					  
			 });			  
		 }
		  <%--  Rel 9.2 - IR 36527 End  --%>
	    });
	  }
	  
  function searchInvoices() {
	    require(["dojo/dom"],
	      function(dom){

	    	var invoiceStatus = dijit.byId("invoiceStatusType").attr('value');
	 
	        <%-- var filterText=filterTextCS.toUpperCase(); --%>
	        var searchParms = init+"&invoiceStatusType="+invoiceStatusType;
	        searchDataGrid("InvoiceUploadListId", "InvoicePayablePendingGroupListDataView",
	                       searchParms);
	      });
	  }
  
<%-- SHR CR-709 Rel 8.2 start   --%>
	 function assignInstrumentTypeToInvList(){
	 
	    	require(["t360/dialog"], function(dialog) {
	    	      <%--  jgadela Rel 8.3  added escapeQuotesforJS to fix quotes error in the french text --%>
		 	      dialog.open('InvoiceGroupListDialogID', '<%=StringFunction.escapeQuotesforJS(resMgr.getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE))%>',
			                      'AssignInstrumentType.jsp',
			                      ['selectedCount','fromPage'], [getSelectedGridRowKeys("InvoiceUploadListId").length,'PG'], 
			                      'select', this.createNewInstrumentTypeValue);
			  }); 
			
}

function createNewInstrumentTypeValue(instType,loanType) {
 var theForm = document.getElementsByName(formName)[0];
	theForm.instrumentType.value=instType;
	theForm.loanType.value=loanType; 
	
	var rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");
submitFormWithParms("PayableInvoiceGroupListForm","ListAssignInstrType", "checkbox",rowKeys);
}

function assignLoanTypeToInv(){
	 
	    	require(["t360/dialog"], function(dialog) {
	    	    <%--  jgadela Rel 8.3  added escapeQuotesforJS to fix quotes error in the french text --%>
		 	      dialog.open('InvoiceGroupListDialogID', '<%=StringFunction.escapeQuotesforJS(resMgr.getText("UploadInvoiceAction.AssignLoanType", TradePortalConstants.TEXT_BUNDLE))%>',
			                      'AssignLoanType.jsp',
			                      ['selectedCount','fromPage'], [getSelectedGridRowKeys("InvoiceLRListId").length,"PLRG"], 
			                     'select', this.createNewLoanTypeValueToLoanReqInvGroup);
			  }); 
}  

<%-- Loan Req - LoanType action CallBack method --%>
 function createNewLoanTypeValueToLoanReqInvGroup(userLoanType) {	
	
	var theForm = document.getElementsByName(formName)[0];	
	theForm.loanType.value=userLoanType;	
	var rowKeys = getSelectedGridRowKeys("InvoiceLRListId");
	submitFormWithParms("PayableInvoiceGroupListForm","AssignLoanType", "checkbox",rowKeys);
	
 }

function createLoanReqByRulesInvList(){
	 
	    	require(["t360/dialog"], function(dialog) {
	    	      <%--  jgadela Rel 8.3  added escapeQuotesforJS to fix quotes error in the french text  --%>
		 	      dialog.open('InvoiceGroupListDialogID', '<%=StringFunction.escapeQuotesforJS(resMgr.getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE))%>',
			                      'CreateLoanRequestByRulesDialog.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceLRListId").length], 
			                      null, null);
			  }); 
} 
function performLRAction(pressedButton){
    var formName = '<%=formName%>';

    <%--get array of rowkeys from the grid--%>
    var rowKeys = getSelectedGridRowKeys("InvoiceLRListId");

    <%--submit the form with rowkeys
        becuase rowKeys is an array, the parameter name for
        each will be checkbox + an iteration number -
        i.e. checkbox0, checkbox1, checkbox2, etc --%>
   submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
}

function applyPaymentDateToLRGroupList(){
	 
	require(["t360/dialog"], function(dialog) {
		dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
	                      'AssignPaymentDate.jsp',
	                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceLRListId").length], 
	                      'select', this.createNewLRApplyPaymentValue);
	  }); 

}

function createNewLRApplyPaymentValue(payDate) {
 var theForm = document.getElementsByName(formName)[0];
	theForm.paymentDate.value=payDate;
	var rowKeys = getSelectedGridRowKeys("InvoiceLRListId");
submitFormWithParms("PayableInvoiceGroupListForm","ApplyPaymentDate", "checkbox",rowKeys);
}
<%-- SHR CR-709 Rel 8.2 End --%>
<%-- Srinivasu_D CR-709 Rel8.2 03/15/2013 start
function createInvLRCreateLRQ(){
	 var bankList=  '<%= bankOrgList%>';
	  if(bankList>1){
	
	require(["t360/dialog"], function(dialog) {
		dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoices.InvLRCreateLRQ", TradePortalConstants.TEXT_BUNDLE)%>',
	                      'CreateLoanRequestDialog.jsp',
	                      ['selectedCount','orglist'], [getSelectedGridRowKeys("InvoiceLRListId").length, "<%=bbGridData.toString()%>"], 
	                      'select', this.createNewInvLRCreateLRQ);
	  }); 
	   }
	   else{
		   createNewInvLRCreateLRQ('<%=orgOid%>');
	   }

}

function createNewInvLRCreateLRQ(userBankBranch) {
	var theForm = document.getElementsByName(formName)[0];
	theForm.bankBranch.value=userBankBranch;
	var rowKeys = getSelectedGridRowKeys("InvoiceLRListId");
	submitFormWithParms("PayableInvoiceGroupListForm","CreateLRQ", "checkbox",rowKeys);
}
<%-- Srinivasu_D CR-709 Rel8.2 03/15/2013 end --%> 
function createInvLRCreateLRQ(bankBranchArray){	
	  rowKeys = getSelectedGridRowKeys("InvoiceLRListId");
		var items=getSelectedGridItems("InvoiceLRListId");

		if(rowKeys == ""){
			alert("Please pick a record and click 'Select'");
			return;
		}
		if ( bankBranchArray.length == 1 ) {
			createNewLoanRequestInstValue(bankBranchArray[0]);
		} else {
			require(["t360/dialog"], function(dialog ) {
		      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'bankBranchSelectorDialog.jsp',
		                  ['branchGridId'], ['InvoiceBankBranchSelectorDataGrid'], <%-- parameters --%>
		                  'select', this.createNewLoanRequestInstValue);
		    });
		 }
}
 function submitLRInvFormWithParms(bankBranchOid) {
		buttonName='CreateLRQ';
		parmName="checkbox";
		parmValue = rowKeys;
	      <%-- verify arguments at development time --%>
	      if ( formName == null ) {
	        alert('Problem submitting form: formName required');
	      }
	      if ( buttonName == null ) {
	        alert('Problem submitting form: buttonName required');
	      }
	      if ( parmName == null ) {
	        alert('Problem submitting form: parmName required');
	      }
	  
	      <%-- get the form and submit it --%>
	      if ( document.getElementsByName(formName).length > 0 ) {
	        var myForm = document.getElementsByName(formName)[0];
	        <%-- generate url parameters for the rowKeys and add to  --%>
	        <%-- form as hidden fields --%>
	        if (parmValue instanceof Array) {
	          for (var myParmIdx in parmValue) {
	            var myParmValue = parmValue[myParmIdx];
	            var myParmName = parmName + myParmIdx;
	            addParmToForm(myForm, myParmName, myParmValue);
	          }
	        }
	        else { <%-- assume a single --%>
	        	 addParmToForm(myForm, parmName, parmValue);
	          
	        }
	        myForm.bankBranch.value=bankBranchOid; 
	        var formNumber = getFormNumber(formName);
	        setButtonPressed(buttonName, formNumber); 
	        myForm.submit();
	      } else {
	        alert('Problem submitting form: form ' + formName + ' not found');
	      }
	    }
  
function createNewLoanRequestInstValue(bankBranchOid) {	
	
	submitLRInvFormWithParms(bankBranchOid);

}

<%--  Nar 913 Begin --%>
 function searchPayablesPrgmInvoice(){
	 require(["dijit/registry"], function(registry){
		 payablesPrgmInvStatusType=registry.byId("payablesPrgmInvStatusType").value;
		 if("" == payablesPrgmInvStatusType){
			 payablesPrgmInvStatusType = 'All';
		  }
		  initPayPrgmSearchParms  = init+"&payablesPrgmInvStatusType="+payablesPrgmInvStatusType;
	      searchDataGrid("PayablesPrgmInvoiceGroupListId", "PayablesPrgmInvoiceGroupListDataView", initPayPrgmSearchParms );
	 });
 }
 
 function performInvPayPrgmAction(pressedButton){
	    var formName = '<%=formName%>';

	    <%--get array of rowkeys from the grid--%>
	    var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
		 
	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	   submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
 }
 
 function applyPaymentDateToPayPrgmGroupList(){
	 var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
	
		require(["t360/dialog"], function(dialog) {
			dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignPaymentDate.jsp',
		                      ['selectedCount'], [rowKeys.length], 
		                      'select', this.createNewApplyPaymentValueToPatPrgm);
		  }); 
 	}

 
 function createNewApplyPaymentValueToPatPrgm(payDate) {
	 var theForm = document.getElementsByName(formName)[0];
     theForm.paymentDate.value=payDate;
     var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
	 submitFormWithParms("PayableInvoiceGroupListForm","PayPgmApplyPayDate", "checkbox",rowKeys);
 }
 

	function modifySuppDateToPayPrgmGroupList(){	 
	 var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
	 
		require(["t360/dialog"], function(dialog) {
			dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoices.InvPayModifySuppDate", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'ModifySupplierDate.jsp',
		                      ['selectedCount'], [rowKeys.length], 
		                      'select', this.modifySuppToDateValueToPatPrgm);
		  }); 

	}
 	function modifySuppToDateValueToPatPrgm(suppDate) {
		var theForm = document.getElementsByName(formName)[0];
	   	theForm.supplierDate.value=suppDate;
	   	var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
	    submitFormWithParms("PayableInvoiceGroupListForm","ModifySupplierDate", "checkbox", rowKeys);
 	}
 	
 	function addSelectedInvoices(invLists){
 	var theForm = document.getElementsByName(formName)[0];
	   	var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
	   	var parmName1="checkbox";
			var parmValue1 = rowKeys;
	   	var parmName ="selectInv";
	   	var myForm = document.getElementsByName("PayableInvoiceGroupListForm")[0];
	   	if (invLists instanceof Array) {
	   	 
		          for (var myParmIdx in invLists) {
		            var myParmValue = invLists[myParmIdx];
		            var myParmName = parmName + myParmIdx;
		            addParmToForm(myForm, myParmName, myParmValue);
		          }
		        }
		        else { <%-- assume a single --%>
		        	 addParmToForm(myForm, parmName, invLists);
		          
		        }
		        
		        if (parmValue1 instanceof Array) {
		        
		          for (var myParmIdx in parmValue1) {
		            var myParmValue = parmValue1[myParmIdx];
		            var myParmName = parmName1 + myParmIdx;
		            addParmToForm(myForm, myParmName, myParmValue);
		          }
		        }
		        else { <%-- assume a single --%>
		        	 addParmToForm(myForm, parmName1, parmValue1);
		          
		        }
		        var formNumber = getFormNumber("PayableInvoiceGroupListForm");
		        setButtonPressed("ApplyCreditNote", formNumber); 
		        myForm.submit(); 
	   <%--  submitFormWithParms("PayableInvoiceGroupListForm","ApplyCreditNote", "checkbox", rowKeys); --%>
 	}
 	<%-- ToDo --%>
 	
 	 function applyCreditNote() {
	    require(["dijit/registry","t360/dialog"], function(registry,dialog ) {
	      	var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
	       if (rowKeys && rowKeys.length != 1 ) {
	       alert("Please select single credit Note");
	       return false;
	       }
	       if(rowKeys && rowKeys.length ==1){
	       		var items = getSelectedGridItems("PayablesCreditNoteListId");
	       		var endToEndID = items[0].i.EndToEndId;
	       		if(endToEndID !=""){
	       			alert('<%=resMgr.getText("UploadInvoices.ApplyCreditNoteErrMsg", TradePortalConstants.TEXT_BUNDLE)%>');
	       			return false;
	       		}
	       		
	       		<%-- SSikhakolli - Rel-9.2 CR-914A STRT IR# T36000035893 02/04/2015 - Adding alert message and return the acction if Applied CN Status is "Applied(FAP)".  --%>
	       		var appliedCNStatus = items[0].i.AppliedStatus;
	       		if(appliedCNStatus == "Applied"){
	       			alert('<%=resMgr.getText("UploadInvoices.FullyApplyCreditNoteErrMsg", TradePortalConstants.TEXT_BUNDLE)%>');
	       			return false;
	       		}
	       }
	       
		  dialog.open('creditNoteApplySearchDialog', '<%=resMgr.getText("InvoicesForCreditNoteApply.Apply", TradePortalConstants.TEXT_BUNDLE)%>',
	          'CreditNoteApplySearchDialog.jsp',
	          ['userOrgOid','loginLocale','creditNoteOid'],['<%=userOrgOid%>','<%=loginLocale%>',rowKeys[0]], 
	          'select', this.addSelectedInvoices);
	          
	          
	      
	    });
	  }
	   function UnApplyCreditNote() {
	    require(["dijit/registry","t360/dialog"], function(registry,dialog ) {
	      	var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
	       if (rowKeys && rowKeys.length != 1 ) {
	       alert("Please select single credit Note");
	       return false;
	       }
		  dialog.open('creditNoteUnApplySearchDialog', '<%=resMgr.getText("UploadInvoices.UnApplyCreditNote", TradePortalConstants.TEXT_BUNDLE)%>',
	          'creditNoteUnApplySearchDialog.jsp',
	          ['userOrgOid','loginLocale','creditNoteOid'],['<%=userOrgOid%>','<%=loginLocale%>',rowKeys[0]], 
	          'select', this.removeSelectedInvoices);
	          
	          
	      
	    });
	  }
	  
	  
	  function removeSelectedInvoices(invLists){
 	var theForm = document.getElementsByName(formName)[0];
	   	var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
	   	var parmName1="checkbox";
			var parmValue1 = rowKeys;
	  	   	var parmName ="selectInv";
	   	var myForm = document.getElementsByName("PayableInvoiceGroupListForm")[0];
	   	if (invLists instanceof Array) {
	   	
		          for (var myParmIdx in invLists) {
		            var myParmValue = invLists[myParmIdx];
		            var myParmName = parmName + myParmIdx;
		            addParmToForm(myForm, myParmName, myParmValue);
		          }
		        }
		        else { <%-- assume a single --%>
		        	 addParmToForm(myForm, parmName, invLists);
		          
		        }
		        
		        if (parmValue1 instanceof Array) {
		       
		          for (var myParmIdx in parmValue1) {
		            var myParmValue = parmValue1[myParmIdx];
		            var myParmName = parmName1 + myParmIdx;
		            addParmToForm(myForm, myParmName, myParmValue);
		          }
		        }
		        else { <%-- assume a single --%>
		        	 addParmToForm(myForm, parmName1, parmValue1);
		          
		        }
		        var formNumber = getFormNumber("PayableInvoiceGroupListForm");
		        setButtonPressed("UnApplyCreditNote", formNumber); 
		        myForm.submit(); 
	   <%--  submitFormWithParms("PayableInvoiceGroupListForm","ApplyCreditNote", "checkbox", rowKeys); --%>
 	}
 
 	function applyPaymentAmtToPayPrgmGroupList(){  
	 var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
	 
	    require(["t360/dialog"], function(dialog) {
	          dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoices.ApplyEarlyPaymentAmount", TradePortalConstants.TEXT_BUNDLE)%>',
	                          'ApplyEarlyPaymentAmt.jsp',
	                          ['selectedCount'], [rowKeys.length], 
	                          'select', this.applyPaymentAmtValue);
	      }); 

	}
	function applyPaymentAmtValue(paymentAmt) {
	    var theForm = document.getElementsByName(formName)[0];
	    theForm.paymentAmount.value=paymentAmt;
	    var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
	  submitFormWithParms("PayableInvoiceGroupListForm","ApplyEarlyPayAmt", "checkbox", rowKeys);
	}
	
	function performPayPrgmAuth(pressedButton){
		 
		 var formName = '<%=formName%>';
	 require(["dojo/_base/array"], 
        	function(array) {
			    <%--get array of rowkeys from the grid--%>
			    var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
				var items = getSelectedGridItems("PayablesPrgmInvoiceGroupListId");
				var endToEndID = "";
			 	array.forEach(items, function(item){
		      		endToEndID = endToEndID + item.i.EndToEndId;
		      	}); 
			 	var payCRAllowH2HApprovalInd = <%=TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAllowH2HApprovalInd)%>;
			 	if(endToEndID!= ""){
					if(!confirm("<%=resMgr.getText("UploadInvoices.AuthoriseEndToEndIDConfirmMsg",TradePortalConstants.TEXT_BUNDLE) %>")){
						return false;
					}else if(payCRAllowH2HApprovalInd){
					    if(confirm("<%= resMgr.getText("UploadInvoices.DeclineE2EInvoicesAlert", TradePortalConstants.TEXT_BUNDLE)%>")){
					   	performAuthorizeInvoices(pressedButton);
			 			}
			 			else{
			 				return false;
			 			}
					}
					else{
						performAuthorizeInvoices(pressedButton);
					} 
			    } 
			   else{
					performAuthorizeInvoices(pressedButton);
			   }  

		});	 
		 
		 
	}
	
	 function performAuthorizeInvoices(pressedButton){
 
 var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
 <% if(PayInvoiceRequireAuth && StringFunction.isNotBlank(certAuthURL)) { %>
	  openReauthenticationWindowForList(certAuthURL, "PayableInvoiceGroupListForm", pressedButton , "InvoiceG","PayablesPrgmInvoiceGroupListId");	 
	  <%} else {%>		
	 	submitFormWithParms("PayableInvoiceGroupListForm", pressedButton, "checkbox", rowKeys);
	    <%}%>
 }
	 
	 function performInvoiceListOfflineAuthorizationAction(){
		 var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
		 
		 openReauthenticationWindowForList(certAuthURL, "PayableInvoiceGroupListForm", "AuthorizeInvoices", "InvoiceG","PayablesPrgmInvoiceGroupListId");
	 }
	 
	 <%--Added for CR 1006 --%>
	 function performInvoiceDecline(pressedButton){
	 var formName = '<%=formName%>';
	 require(["dojo/_base/array"], 
        	function(array) {
			    <%--get array of rowkeys from the grid--%>
			    var rowKeys = getSelectedGridRowKeys("PayablesPrgmInvoiceGroupListId");
				var items = getSelectedGridItems("PayablesPrgmInvoiceGroupListId");
				var endToEndID = "";
			 	array.forEach(items, function(item){
		      		endToEndID = endToEndID + item.i.EndToEndId;
		      	}); 
			 	var payInvAllowH2HApprovalInd = <%=TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAllowH2HApprovalInd)%>;
			 	if(payInvAllowH2HApprovalInd && endToEndID!=""){
			 		if(confirm("<%= resMgr.getText("UploadInvoices.DeclineE2EInvoicesAlert", TradePortalConstants.TEXT_BUNDLE)%>")){
			 			submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
			 		}
			 		else{
			 			return false;
			 		}
			 	}else{
			 		submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
			 	}
		});	 	
	 }
	 
	 function payablesPanelStatusFormatter(columnValues){
		  var gridLink = "";
		 	if(columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH%>' ||
			  		columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED%>'){
				 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
				 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"PayablesPrgmInvoiceGroupListId\",\"PAY_INV_GROUP\");'>"+columnValues[0]+"</a>";
		  		 return gridLink;
			}else{
				return "";
			}
		}
		
		function creditNotesPanelStatusFormatter(columnValues){
  	  var gridLink = "";
		 	if(columnValues[2] == '<%=TradePortalConstants.CREDIT_NOTE_STATUS_PAUT%>' ||
			  		columnValues[2] == '<%=TradePortalConstants.CREDIT_NOTE_STATUS_FAUT%>'){
				 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
				 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"PayablesCreditNoteListId\",\"CREDIT_NOTES\");'>"+columnValues[0]+"</a>";
		  		 return gridLink;
			}else{
				return "";
			}
	}
<%--  Nar 913 End --%>

  <%--  Nar 914 Begin --%>
  
   function performPayPrgmCreditNoteAction(pressedButton){
	    var formName = '<%=formName%>';
		 require(["dojo/_base/array"], 
        	function(array) {
			    <%--get array of rowkeys from the grid--%>
			    var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
				var items = getSelectedGridItems("PayablesCreditNoteListId");
				var endToEndID = "";
				array.forEach(items, function(item){
		      		endToEndID = item.i.EndToEndId;
		      	});
				if(pressedButton == "AuthorizePayCreditNote"){
				
				
				var payCRAllowH2HApprovalInd = <%=TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)%>;
				
				if(endToEndID!= ""){
					if(!confirm("<%=resMgr.getText("UploadInvoices.AuthoriseEndToEndIDConfirmMsg",TradePortalConstants.TEXT_BUNDLE) %>")){
							return false;
					}
						
					else if(payCRAllowH2HApprovalInd){
				    	if(confirm("<%= resMgr.getText("UploadInvoices.DeclineE2EInvoicesAlert", TradePortalConstants.TEXT_BUNDLE)%>")){
			 				submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
			 			}
			 			else{
			 				return false;
			 			}
					}
					else{
						submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
					}
				
			    }
			   else
			   {
			     submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
			   }
			}
			else
			   {
			     submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
			   }
		 });
 }
 
 <%--Added for CR 1006 --%>
	 function performCreditNoteDecline(pressedButton){
	 var formName = '<%=formName%>';
	 require(["dojo/_base/array"], 
        	function(array) {
			    <%--get array of rowkeys from the grid--%>
			    var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
				var items = getSelectedGridItems("PayablesCreditNoteListId");
				var endToEndID = "";
			 	array.forEach(items, function(item){
		      		endToEndID = endToEndID + item.i.EndToEndId;
		      	}); 
			 	var payCRAllowH2HApprovalInd = <%=TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)%>;
			 	if(payCRAllowH2HApprovalInd && endToEndID!=""){
			 		if(confirm("<%= resMgr.getText("UploadInvoices.DeclineE2EInvoicesAlert", TradePortalConstants.TEXT_BUNDLE)%>")){
			 			submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
			 		}
			 		else{
			 			return false;
			 		}
			 	}else{
			 		submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
			 	}
		});	 	
	 }
  
  <%--  Nar 914 End --%>

	require(["dojo/query", "dojo/on", "t360/datagrid","dijit/registry", "t360/popup", "dojo/ready", "dojo/domReady!"],
		      function(query, on, t360grid,registry, t360popup, ready ) {
		 ready(function() {
		 initParms();

		if(registry.byId("PayablesCreditNoteListId") != undefined){
		 var myGrid = registry.byId("PayablesCreditNoteListId");
		
		 myGrid.onCanSelect=function(idx){
			var selectedItem = this.getItem(idx).i;
			var isChild = (selectedItem.CHILDREN == "false");
		
		   return (!isChild);
		};
		  } 
		    query('#invoiceUploadListRefresh').on("click", function() {
		    	searchInvoices();
		    });
		  <%-- RPasupulati IR#T36000018355 Start --%>
		    query('#invoiceLRListRefresh').on("click", function() {
		    	searchLoanRequest();
		    });
		    <%-- RPasupulati IR#T36000018355 Ends. --%>
		     query('#payablesPrgmInvListRefresh').on("click", function() {
		    	 searchPayablesPrgmInvoice();
		    });
		     query('#creditUploadListRefresh').on("click", function() {
		    	 searchCreditNote();
		    });
		    <%--  DK IR T36000018295 Rel8.2 06/15/2013 starts --%>
	  query('#InvoiceLRListEdit').on("click", function() {
	    var columns = t360grid.getColumnsForCustomization('InvoiceLRListId');
	    var parmNames = [ "gridId", "gridName", "columns" ];
	    var parmVals = [ "InvoiceLRListId", "InvoicePayableLRGroupListDataGrid", columns ];
	    t360popup.open(
	      'InvoiceLRListEdit', 'gridCustomizationPopup.jsp',
	      parmNames, parmVals,
	      null, null);  <%-- callbacks --%>
	  });
	  query('#InvoiceUploadListEdit').on("click", function() {
	    var columns = t360grid.getColumnsForCustomization('InvoiceUploadListId');
	    var parmNames = [ "gridId", "gridName", "columns" ];
	    var parmVals = [ "InvoiceUploadListId", "InvoicePayablePendingGroupListDataGrid", columns ];
	    t360popup.open(
	      'InvoiceUploadListEdit', 'gridCustomizationPopup.jsp',
	      parmNames, parmVals,
	      null, null);  <%-- callbacks --%>
	  });
	  <%--  DK IR T36000018295 Rel8.2 06/15/2013 ends --%>
	  query('#payablesPrgmInvListEdit').on("click", function() {
		    var columns = t360grid.getColumnsForCustomization('PayablesPrgmInvoiceGroupListId');
		    var parmNames = [ "gridId", "gridName", "columns" ];
		    var parmVals = [ "PayablesPrgmInvoiceGroupListId", "PayablesPrgmInvoiceGroupListDataGrid", columns ];
		    t360popup.open(
		      'payablesPrgmInvListEdit', 'gridCustomizationPopup.jsp',
		      parmNames, parmVals,
		      null, null);  <%-- callbacks --%>
	  });
	  query('#CreditUploadListEdit').on("click", function() {
		    var columns = t360grid.getColumnsForCustomization('PayablesCreditNoteListId');
		    var parmNames = [ "gridId", "gridName", "columns" ];
		    var parmVals = [ "PayablesCreditNoteListId", "PayablesCreditNoteListDataGrid", columns ];
		    t360popup.open(
		      'CreditUploadListEdit', 'gridCustomizationPopup.jsp',
		      parmNames, parmVals,
		      null, null);  <%-- callbacks --%>
		  });
	  
	  if(registry.byId("PayablesCreditNoteListId") != undefined){
	  var myGrid = registry.byId("PayablesCreditNoteListId");
      myGrid.onCanSelect=function(idx){
			var selectedItem = this.getItem(idx).i;
			var isChild = (selectedItem.CHILDREN != "true");
		   	return (!isChild);
		};
	  }
		<%-- MEer Rel 9.2 T36000035885 - Change font style to Italic for Child nodes in the CreditNotes Grid --%>
		dojo.connect(myGrid, 'onStyleRow', this, function (row) {
			  var trNode = row.node.childNodes[0].childNodes[0].childNodes[0];
			  if(trNode.className=="dojoxGridNoChildren"){
				  dojo.addClass(trNode, "treeGridChildren");
			  }			 
		});

	  
     });
	});
  
	require(["dojo/dom-class", "dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dijit/registry", "dojo/ready", "dojo/domReady!"],
			 function(domClass, query, on, t360grid, t360popup, registry,ready) {
		 ready(function() {	  
			 
				<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
				domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
				domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
			  
			  <%-- invoices for payables --%>
			  if(registry.byId("PayablesPrgmInvoiceGroupListId")){
				  var invGrid = registry.byId("PayablesPrgmInvoiceGroupListId");				   
				  on(invGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_Authorize')){
				          t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Authorize",0);
					  }
					  if (registry.byId('UploadInvoices_Actions')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Actions",0);
					  }
					  if (registry.byId('UploadInvoices_Document')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Document",0);
					  }
				 });
			 }
			  <%-- invoices for loan requests --%>
			  if(registry.byId("InvoiceLRListId")){
				  var invLRGrid = registry.byId("InvoiceLRListId");				   
				  on(invLRGrid,"selectionChanged", function() {	
					  if (registry.byId('UploadInvoices_LActions')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invLRGrid,"UploadInvoices_LActions",0);
					  }
					  if (registry.byId('UploadInvoices_LDocument')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invLRGrid,"UploadInvoices_LDocument",0);
					  }				  
				 });
			 }
			 
			  if(registry.byId("PayablesCreditNoteListId")){
				  var invCRGrid = registry.byId("PayablesCreditNoteListId");				   
				  on(invCRGrid,"selectionChanged", function() {	
					  if (registry.byId('UploadInvoices_CRActions')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invCRGrid,"UploadInvoices_CRActions",0);
					  }
					 <%--  if (registry.byId('UploadInvoices_CRDocument')){ --%>
					<%-- 	  t360grid.enableFooterItemOnSelectionGreaterThan(invCRGrid,"UploadInvoices_CRDocument",0); --%>
					<%--   }	 --%>
					<%--   if (registry.byId('UploadInvoices_CRAuthorize')){ --%>
				    <%--       t360grid.enableFooterItemOnSelectionGreaterThan(invCRGrid,"UploadInvoices_CRAuthorize",0); --%>
					 <%--  }			   --%>
				 });
			 }
			  <%-- invoices for pending action --%>
			  if(registry.byId("InvoiceUploadListId")){
				  invPGrid = registry.byId("InvoiceUploadListId");			   
				  on(invPGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_PActions')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invPGrid,"UploadInvoices_PActions",0);
					  }
					  if (registry.byId('UploadInvoices_PDocument')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invPGrid,"UploadInvoices_PDocument",0);
					  }		  
				 });			  
			 }
			 
			  <%--  Payable Credit Note --%>
			  if(registry.byId("PayablesCreditNoteListId")){
				  var crtPGrid = registry.byId("PayablesCreditNoteListId");			   
				  on(crtPGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_CRActions')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRActions",0);
					  }
					  if (registry.byId('UploadInvoices_CRDocument')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRDocument",0);
					  }	
					  if (registry.byId('UploadInvoices_CRAuthorize')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRAuthorize",0);
					  }					  
				 });			  
			 }
			 
		 });
	 });
	
</script>
