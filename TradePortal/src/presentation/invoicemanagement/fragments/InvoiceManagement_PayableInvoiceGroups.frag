<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Invoice Management List Invoice Tab

  Description:
    Contains HTML to display incoices which are uploaded through upload tab

  This is not a standalone frag.  It MUST be included using the following tag:
  <%@ include file="InvoiceManagement_PayableInvoiceGroups.frag" %>
*******************************************************************************
--%> 

<%
DataGridFactory dgFactory = null;
 	String searchType = StringFunction.xssCharsToHtml(request.getParameter("SearchType"));
 	String newDropdownSearch = request.getParameter("NewDropdownSearch");
 	String searchListViewName = null;
 	String statusSearchParm = null;
 	String defaultSearchStatus = null;
 	String statusSearchLabel = null;
 	String sectionHeader = null;
 	String buttonDisplay = "6";

 	StringBuffer statusExtraTags = new StringBuffer();
 	String linkArgs[] = { "", "", "", "", "", "" };
 	String gridHtmlSummary = null;
 	secureParms.put("UserOid", userOid);
 	secureParms.put("ownerOrg", userOrgOid);
 	secureParms.put("SecurityRights", userSecurityRights);
 	secureParms.put("clientBankOid", userSession.getClientBankOid());

 	boolean hasApproveRights = SecurityAccess.hasRights(
 			userSecurityRights,
 			SecurityAccess.RECEIVABLES_APPROVE_FINANCING);
 	boolean hasAuthRights = SecurityAccess.hasRights(
 			userSecurityRights,
 			SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE);
 	// boolean hasRemoveInvRights = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_REMOVE_INVOICE);     
 	boolean hasATPInvRights = SecurityAccess.hasRights(
 			userSecurityRights, SecurityAccess.PAYABLES_CREATE_ATP);
 	boolean hasRemoveInvRights = SecurityAccess.hasRights(
 			userSecurityRights, SecurityAccess.PAYABLES_DELETE_INVOICE);
 	boolean hasApplyPaymentInvRights = SecurityAccess.hasRights(
 			userSecurityRights, SecurityAccess.PAYABLES_APPLY_PAYDATE);
 	codesToExclude = new Vector();
 	%>
 	
 	<table width=100%> 	
 	<%
 	//Nar CR 913 Begin
 	if(isIntegratedPayPrgmEnabled){
 		
 		searchListViewName = "PayablesPrgmInvoiceGroupListView.xml";
 	 	statusSearchParm = "payablesPrgmInvStatusType";
 	 	defaultSearchStatus = resMgr.getText("common.StatusAll", TradePortalConstants.TEXT_BUNDLE);
 	 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
 	 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
 	 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
 	 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH);
 	 	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_DELETED);
 	 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DBC);
 	 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_RAA);

 	 	gridID = "PayablesPrgmInvoiceGroupListId";
 	 	gridName = "PayablesPrgmInvoiceGroupListDataGrid";
 	 	searchID = "payablesPrgmInvStatusType";
 	 	onSearch = "onChange='searchPayablesPrgmInvoice();'";
 	 	dataView = "PayablesPrgmInvoiceGroupListDataView";
 	 	dataRefreshID = "payablesPrgmInvListRefresh";
 	 	dataEditID = "payablesPrgmInvListEdit";
 	 	buttonDisplay = "6";
 	 	 dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response); 		
 	%>	
 	<tr>
 	  <td>
       <%=widgetFactory.createSectionHeader("1", "InvoiceGroups.SectionIPPHeader")%>
          <%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceGroupsList.frag"%>
       </div>
      </td>
    </tr>			
 	<%
 	}
 	// Nar CR 913 End
 	
 	if(isLRQEnabled){
 	searchListViewName = "InvoicePendingActionListView.xml";
 	statusSearchParm = "invoiceLRStatusType";
 	defaultSearchStatus = resMgr.getText("common.StatusAll",
 			TradePortalConstants.TEXT_BUNDLE);
	codesToExclude = new Vector();
 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
 	//codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING);
 	//codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
 	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_AUTHORISED);
 	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_DELETED);
 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION); 
	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED);
	//codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN);
	//codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PROCESSED_BY_BANK); //Srinivasu_D IR#T36000017756 Rel8.2 06/04/2013
	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_CANCELLED);
//	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED);
 	gridID = "InvoiceLRListId";
 	gridName = "InvoicePayableLRGroupListDataGrid";
 	searchID = "invoiceLRStatusType";
 	onSearch = "onChange='searchLoanRequest();'";
 	dataView = "InvoicePayableLRGroupListDataView";
 	dataRefreshID = "invoiceLRListRefresh";
 	dataEditID = "InvoiceLRListEdit"; // DK IR T36000018295 Rel8.2 06/15/2013
 	buttonDisplay = "6";
 	 dgFactory = new DataGridFactory(resMgr,
 			userSession, formMgr, beanMgr, response);
 %>
 
<%--Start T36000016280  Wrap 2 datagrids within a table --%>


<tr><td>
<% if(isIntegratedPayPrgmEnabled){ %>
<%=widgetFactory.createSectionHeader("2", "InvoiceGroups.Section2LRHeader")%>
<% } else {%>
<%=widgetFactory.createSectionHeader("1", "InvoiceGroups.SectionLRHeader")%>
<%} %>
<%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceGroupsList.frag"%>
</div>
</td></tr>

 <%
 	}
 	searchListViewName = "InvoicePendingActionListView.xml";
 	statusSearchParm = "invoiceStatusType";
 	defaultSearchStatus = resMgr.getText("common.StatusAll",
 			TradePortalConstants.TEXT_BUNDLE);
	codesToExclude = new Vector();
 	//codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED);
 	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION); 
 	//codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PROCESSED_BY_BANK); //Srinivasu_D IR#T36000017568 Rel8.2 05/29/2013
 	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_AUTHORISED);
 	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_DELETED);
 	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED);

 	gridID = "InvoiceUploadListId";
 	gridName = "InvoicePayablePendingGroupListDataGrid";
 	searchID = "invoiceStatusType";
 	onSearch = "onChange='searchPending();'";
 	dataView = "InvoicePayablePendingGroupListDataView";
 	dataRefreshID = "invoiceUploadListRefresh";
 	dataEditID = "InvoiceUploadListEdit";  // DK IR T36000018295 Rel8.2 06/15/2013
 	buttonDisplay = "6";
 	 dgFactory = new DataGridFactory(resMgr,
 			userSession, formMgr, beanMgr, response);
 %>
<tr><td>
<% if(isIntegratedPayPrgmEnabled && isLRQEnabled){ %>
<%=widgetFactory.createSectionHeader("3", "InvoiceGroups.Section3PAHeader")%>
<% } else if(isIntegratedPayPrgmEnabled || isLRQEnabled){%>
<%=widgetFactory.createSectionHeader("2", "InvoiceGroups.SectionPAHeader")%>
<%} else {%>
<%=widgetFactory.createSectionHeader("1", "InvoiceGroups.Section1PAHeader")%>
<%} %>

<%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceGroupsList.frag"%>
</div>

</td></tr>
<%
 	if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){
 // MEer  IR-35813 Retrieve based on Credit Note Status
 //	statusRefData="CREDIT_NOTE_APPLIED_STATUS";
 	 statusRefData = "CREDIT_NOTE_STATUS";
 	searchListViewName = "PayablesCreditNoteListView.xml";
 	statusSearchParm = "creditStatusType";
 	defaultSearchStatus = resMgr.getText("common.StatusAll",
 			TradePortalConstants.TEXT_BUNDLE);
	codesToExclude = new Vector();
 	
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_AVA);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_STB);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_AUT);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_PAUT);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_FAUT);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_CLO);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_PBB_BAL);
 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DBC);
 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_RAA);
 	

 	gridID = "PayablesCreditNoteListId";
 	gridName = "PayablesCreditNoteListDataGrid";
 	searchID = "invoiceCRStatusType";
 	onSearch = "onChange='searchCreditNote();'";
 	dataView = "PayablesCreditNoteListDataView";
 	dataRefreshID = "creditUploadListRefresh";
 	dataEditID = "CreditUploadListEdit"; 
 	buttonDisplay = "6";
 	 dgFactory = new DataGridFactory(resMgr,
 			userSession, formMgr, beanMgr, response);
 %>
<tr><td>
<% if(isIntegratedPayPrgmEnabled && isLRQEnabled){ %>
<%=widgetFactory.createSectionHeader("4", "InvoiceGroups.Section4CRHeader")%>
<% } else if(isIntegratedPayPrgmEnabled || isLRQEnabled){%>
<%=widgetFactory.createSectionHeader("3", "InvoiceGroups.Section3CRHeader")%>
<%} else {%>
<%=widgetFactory.createSectionHeader("2", "InvoiceGroups.Section2CRHeader")%>
<%} %>
<%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceGroupsList.frag"%>
</div>

</td></tr>
<%}%>
</table>
