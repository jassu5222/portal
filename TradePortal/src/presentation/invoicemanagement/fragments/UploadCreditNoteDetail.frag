<%
	String oid = "";
	String groupOID = "";
	boolean showDelete = true;
	boolean getDataFromDoc = false;
	boolean retrieveFromDB = false;
	DocumentHandler doc;
	String creditNoteStatus = null;
	String creditNoteAppliedStatus = null;
	String creditNoteUtilizedStatus = null;
	String addressLine1 = "";
	String addressLine2 = "";
	String addressCity = "";
	String addressCtry = "";
	String currency = null;
	String remainingAmount = "";
	String appliedAmount = "0.00";
	String useCurrency = null;
	String listViewToSort = null;
	DocumentHandler invoiceSummaryGoodsResult = null;
	DocumentHandler invoiceListDetailResult = null;
	DocumentHandler invoiceTradingPartnerDetailResult = null;
	String linkArgs[] = { "", "", "", "", "", "" };
	String LogGridHtml = "";
	String logGridLayout = "";
	String upload_invoice_oid = "";
	String linkString = "";
	ArMatchingRuleWebBean_Base rule = null;
	String userOrgOid = userSession.getOwnerOrgOid();
	boolean ignoreDefaultPanelValue = false;
	CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	corpOrg.getById(userOrgOid);

	CreditNotesWebBean_Base CreditNotes = beanMgr.createBean(CreditNotesWebBean_Base.class, "CreditNotes");
	
	CreditNotesWebBean CreditNotesWeb = beanMgr.createBean(CreditNotesWebBean.class, "CreditNotes");	


	DocumentHandler creditNoteAppliedInvoicesResult = null;
	Vector creditNoteAppliedInvoicesList = null;
	int numberOfAppliedInvoices = 0;
	
	String loginLocale = userSession.getUserLocale();
	String userSecurityType = userSession.getSecurityType();
	String userTimeZone = userSession.getTimeZone();
	String corpOrgOid = userSession.getOwnerOrgOid();
	// Get the listview to sort if the user clicked on a listview header link; we only want to sort the listview the
	// user clicked on, NOT both listviews on the page
	listViewToSort = request.getParameter("listView");
	// Get the document from the cache.  We'll use it to repopulate the screen 
	// if the user was entering data with errors.
	doc = formMgr.getFromDocCache();

	if (doc.getDocumentNode("/In/CreditNotes") == null) {
	
	
		groupOID = request.getParameter("a_invoice_group_oid");
		oid = request.getParameter("upload_invoice_oid");
		if (oid != null) {
			oid = EncryptDecrypt.decryptStringUsingTripleDes(oid,
					userSession.getSecretKey());
		}
		if (oid == null) {
			oid = "0";
		} else {
			// We have a good oid so we can retrieve.
			getDataFromDoc = false;
			retrieveFromDB = true;
		}
		upload_invoice_oid = oid;

	} else {
		// The doc does exist so check for errors.  If highest severity < WARNING,
		// its a good update.

		String maxError = doc.getAttribute("/Error/maxerrorseverity");

		if (maxError.compareTo(String
				.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
			// We've returned from a delete that was successful
			// We shouldn't be here.  Forward to the ReceivablesManagementHome page.
			formMgr.setCurrPage("InvoiceManagementHome");
			String physicalPage = NavigationManager.getNavMan()
					.getPhysicalPage("InvoiceManagementHome", request);
%>
<jsp:forward page='<%=physicalPage%>' />
<%
	return;
		} else {
			// We've returned from a save/update/delete but have errors.
			// We will display data from the cache.  Determine if we're
			// in insertMode by looking for a '0' oid in the input section
			// of the doc.

			retrieveFromDB = false;
			getDataFromDoc = true;

			// Use oid from input doc.
			oid = doc
					.getAttribute("/In/CreditNotes/upload_credit_note_oid");
		}
	}

	if (retrieveFromDB) {
		// Attempt to retrieve the item.  It should always work.  Still, we'll
		// check for a blank oid -- this indicates record not found.  Display error.

		getDataFromDoc = false;
		CreditNotes.setAttribute("upload_credit_note_oid", oid);
		CreditNotes.getDataFromAppServer();
		if (CreditNotes.getAttribute("upload_credit_note_oid").equals(
				"")) {
			oid = "0";
		}
		
		CreditNotesWeb.setAttribute("upload_credit_note_oid", oid);
		CreditNotesWeb.getDataFromAppServer();
		if (CreditNotesWeb.getAttribute("upload_credit_note_oid").equals(
				"")) {
			oid = "0";
		}		

		//jgadela R92 - SQL INJECTION FIX
		String invoiceSummaryGoodsSQL = "select * from credit_notes_goods where p_credit_note_oid = ? ";
		invoiceSummaryGoodsResult = DatabaseQueryBean.getXmlResultSet(invoiceSummaryGoodsSQL, false, new Object[]{oid});   
		CreditNotesGoodsWebBean inv = beanMgr.createBean(CreditNotesGoodsWebBean.class, "CreditNotesGoods");
		inv.setAttribute("upload_credit_goods_oid", oid);
		inv.getDataFromAppServer();
		//jgadela R92 - SQL INJECTION FIX		
	    StringBuilder creditNoteInvoicesSQL =  new StringBuilder();
		creditNoteInvoicesSQL.append(" SELECT i.invoice_reference_id as invoice_id, r.credit_note_applied_amount FROM invoice i, invoices_credit_notes_relation r ");
        creditNoteInvoicesSQL.append(" WHERE r.a_upload_invoice_oid = i.invoice_oid and r.a_upload_credit_note_oid = ? ");
		creditNoteInvoicesSQL.append(" UNION ");
		creditNoteInvoicesSQL.append(" SELECT i.invoice_id, r.credit_note_applied_amount FROM invoices_summary_data i, invoices_credit_notes_relation r ");
        creditNoteInvoicesSQL.append(" WHERE r.a_upload_invoice_oid = i.upload_invoice_oid and r.a_upload_credit_note_oid = ? ");
		creditNoteInvoicesSQL.append(" AND i.invoice_id not in ( SELECT invoice_reference_id FROM invoice WHERE a_corp_org_oid = ?)  ");
		creditNoteAppliedInvoicesResult = DatabaseQueryBean.getXmlResultSet(creditNoteInvoicesSQL.toString(), false, new Object[]{oid, oid, userOrgOid});

		if (creditNoteAppliedInvoicesResult != null) {
		creditNoteAppliedInvoicesList = creditNoteAppliedInvoicesResult.getFragments("/ResultSetRecord");
		numberOfAppliedInvoices = creditNoteAppliedInvoicesList.size();
		}
		
		String ruleOid = "0";
		ruleOid = CreditNotesWeb.getMatchingRule();
		rule = beanMgr.createBean(ArMatchingRuleWebBean_Base.class, "ArMatchingRule");
		rule.setAttribute("ar_matching_rule_oid", ruleOid);
		rule.getDataFromAppServer();
   

	}

	currency = CreditNotes.getAttribute("currency");
	if(StringFunction.isNotBlank(CreditNotes.getAttribute("credit_note_applied_amount"))){
		//jgadela 03/18/2015- R92 IR-T36000037839
		BigDecimal crAmount = new BigDecimal(CreditNotes.getAttribute("amount"));
	    BigDecimal crAppliedAmount = new BigDecimal(CreditNotes.getAttribute("credit_note_applied_amount"));
	    BigDecimal avalAmount = crAmount.subtract(crAppliedAmount);
		
		remainingAmount = avalAmount.toPlainString(); //String.format("%.2f", amount - appliedAmountDouble);
	} else{
		remainingAmount = CreditNotes.getAttribute("amount");
	}
	if (!InstrumentServices.isBlank(currency))
		useCurrency = currency;
	else
		useCurrency = "2";

	if ((!userSession.getSecurityType().equals(
			TradePortalConstants.ADMIN))) {
		linkArgs[0] = TradePortalConstants.PDF_CREDIT_NOTE_DETAILS;
		linkArgs[1] = EncryptDecrypt
				.encryptStringUsingTripleDes(CreditNotes
						.getAttribute("upload_credit_note_oid"));
		linkArgs[2] = loginLocale;
		linkArgs[3] = userSession.getBrandingDirectory();
		linkString = formMgr.getDocumentLinkAsURLInFrame(resMgr
				.getText("UploadInvoiceDetail.PrintInvoiceDetails",
						TradePortalConstants.TEXT_BUNDLE),
				TradePortalConstants.PDF_CREDIT_NOTE_DETAILS, linkArgs,
				response);
	}
%>


<%-- ********************* HTML for page begins here *********************  --%>
<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
%>

<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeErrorSectionFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMain">
	<div class="pageContent">
		<div class="secondaryNav">
			<div class="secondaryNavTitle"><%=resMgr.getText("CreditNoteDetails.DetailDesc",
					TradePortalConstants.TEXT_BUNDLE)%></div>

			<div id="secondaryNavHelp" class="secondaryNavHelp"><%=OnlineHelp.createContextSensitiveLink(
					"customer/invoice_details.htm", resMgr, userSession)%>
			</div>
			<%=widgetFactory.createHoverHelp("secondaryNavHelp",
					"HelpImageLinkHoverText")%>
			<div style="clear: both;"></div>
		</div>

		<div class="subHeaderDivider"></div>
		<div class="pageSubHeader">
			<span class="pageSubHeaderItem"><%=resMgr.getText("CreditNoteDetails.CreditNoteID",
					TradePortalConstants.TEXT_BUNDLE)%></span> <span class="pageSubHeaderItem">
				<%
					out.print(":");
				%> <%=CreditNotes.getAttribute("invoice_id")%></span> <span
				class="pageSubHeader-right">

				<button data-dojo-type="dijit.form.Button" name="print" id="print"
					type="button" class="pageSubHeaderButton">
					<%=resMgr.getText("common.PrintText",
					TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick"
						data-dojo-args="evt">
           window.open( "<%=linkString%>");
		  </script>
				</button> <%=widgetFactory.createHoverHelp("print", "PrintHoverText")%>
				<button data-dojo-type="dijit.form.Button" name="CloseButton"
					id="CloseButton" type="button" class="pageSubHeaderButton">
					<%=resMgr.getText("common.CloseText",
					TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick"
						data-dojo-args="evt"> 
			document.location.href = '<%=formMgr.getLinkAsUrl(closeLink, response)%>';
          </script>
				</button> <%=widgetFactory.createHoverHelp("CloseButton",
					"CloseHoverText")%>
			</span>
			<div style="clear: both;"></div>
		</div>


		<form name="UploadCreditNoteDetail" method="post"
			id="UploadCreditNoteDetail" data-dojo-type="dijit.form.Form"
			action="<%=formMgr.getSubmitAction(response)%>">
			<input type=hidden value="" name=buttonName>
			<%
				// Store values such as the userid, his security rights, and his org in a secure
				// hashtable for the form.  The mediator needs this information.
				Hashtable secureParms = new Hashtable();
				secureParms.put("UserOid", userSession.getUserOid());

				secureParms.put("upload_credit_note_oid", oid);
			%>
			<%=formMgr.getFormInstanceAsInputField("InvoicesForm",
					secureParms)%>

			<div class="formMainHeader">
				<%
					String link = formMgr.getLinkAsUrl("goToInvoiceManagement",
							response);
				%>
			</div>
			<div class="formContentNoSidebar">
				<%
					// UploadInvoiceDetail.jsp page is used in both group and list invoices. if request is coming from list, then go to management tab else
					// go to invoice group detail page.
					StringBuffer newLink = new StringBuffer();
					if (groupOID != null) {
						newLink.append(formMgr.getLinkAsUrl("goToInvoiceGroupDetail",
								response));
						newLink.append("&invoice_group_oid=" + groupOID);
					} else {
						newLink.append(formMgr.getLinkAsUrl("goToInvoiceManagement",
								response));
					}
				%>



				<table>
				  <tr valign="baseline">
						<td>
							<p>
								<%=widgetFactory
					.createSubLabel("CreditNoteDetails.IssueDate")%>
								<br> <b><%=TPDateTimeUtility.formatDate(
					CreditNotes.getAttribute("issue_date"),
					TPDateTimeUtility.SHORT, loginLocale)%></b>
							</p>
						</td>
<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							<p>
								<%=widgetFactory
					.createSubLabel("CreditNoteDetails.CreditNoteAmount")%>
								<br>
								<%
									if (!InstrumentServices.isBlank(CreditNotes.getAttribute("amount"))) {
								%>

								<b><%=currency%></b>&nbsp; <b><%=TPCurrencyUtility.getDisplayAmount(
						CreditNotes.getAttribute("amount"), useCurrency,
						loginLocale)%></b>
								<%
									} else {
								%>
								&nbsp;
								<%
									}
								%>
							</p>
						</td>

<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>
						<td>
							<p>
								<%=widgetFactory
					.createSubLabel("CreditNoteDetails.AppliedAmount")%>
								<br>
								<%
									if (!InstrumentServices.isBlank(CreditNotes
											.getAttribute("credit_note_applied_amount"))) {
											if(CreditNotes.getAttribute("credit_note_applied_amount").startsWith("-")) {
								%>
											<b><%=currency%></b>&nbsp; <b><%=TPCurrencyUtility.getDisplayAmount(
									CreditNotes.getAttribute("credit_note_applied_amount").substring(1),
									useCurrency, loginLocale)%></b>
									<%
									}else{
									%>
										<b><%=currency%></b>&nbsp; <b><%=TPCurrencyUtility.getDisplayAmount(
											CreditNotes.getAttribute("credit_note_applied_amount"),
											useCurrency, loginLocale)%></b>
									<%
									} 
								}else {
								%>
								<b><%=appliedAmount%></b>
								<%
									}
								%>
							</p>
						</td>
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>
						<td>
							<p>
								<%=widgetFactory
					.createSubLabel("CreditNoteDetails.RemainingAmount")%>
								<br>
								<b><%=currency%></b>&nbsp; <b><%=TPCurrencyUtility.getDisplayAmount(remainingAmount, useCurrency, loginLocale)%></b>
							</p>
						</td>

						<td>&nbsp;</td>
						<td>
							<p>
								<%=widgetFactory
					.createSubLabel("CreditNoteDetails.CreditNoteStatus")%>
								<br>
								<%
									creditNoteStatus = CreditNotes.getAttribute("credit_note_status");
									if (InstrumentServices.isBlank(creditNoteStatus)) {
										creditNoteStatus = "&nbsp;";
									} else {
										creditNoteStatus = creditNoteStatus.trim();
										creditNoteStatus = ReferenceDataManager.getRefDataMgr()
												.getDescr(TradePortalConstants.CREDIT_NOTE_STATUS,
														creditNoteStatus, loginLocale);
										if (InstrumentServices.isBlank(creditNoteStatus))
											creditNoteStatus = "&nbsp;";

									}
								%>
								<b><%=creditNoteStatus%></b>
							</p>
						</td>
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>
						<td>
							<p>
								<%=widgetFactory
					.createSubLabel("CreditNoteDetails.CreditNoteAppliedStatus")%>
								<br>
								<%
									creditNoteAppliedStatus = CreditNotes
											.getAttribute("credit_note_applied_status");
									if (InstrumentServices.isBlank(creditNoteAppliedStatus)) {
										creditNoteAppliedStatus = "&nbsp;";
									} else {
										creditNoteAppliedStatus = creditNoteAppliedStatus.trim();
										creditNoteAppliedStatus = ReferenceDataManager.getRefDataMgr()
												.getDescr(TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS,
														creditNoteAppliedStatus, loginLocale);

									}
								%>
								<b><%=creditNoteAppliedStatus%></b>
							</p>
						</td>

						<td>&nbsp;</td>




					</tr>
				</table>

				<table>
					<tr>
						<td>
							<p>
								<%=widgetFactory
					.createSubLabel("CreditNoteDetails.CreditNoteUtilizedStatus")%>
								<br>
								<%
									creditNoteUtilizedStatus = CreditNotes
											.getAttribute("credit_note_utilised_status");
									if (InstrumentServices.isBlank(creditNoteUtilizedStatus)) {
										creditNoteUtilizedStatus = "&nbsp;";
									} else {
										creditNoteUtilizedStatus = creditNoteUtilizedStatus.trim();
										creditNoteUtilizedStatus = ReferenceDataManager.getRefDataMgr()
												.getDescr(TradePortalConstants.CREDIT_NOTE_UTILISED_STATUS,
														creditNoteUtilizedStatus, loginLocale);

									}
								%>
								<b><%=creditNoteUtilizedStatus%></b>
							</p>
						</td>
						<td>&nbsp;</td>
						<td>
							<p>
								<%=widgetFactory
					.createSubLabel("CreditNoteDetails.EndToEndID")%>
								<br>
								<%
									if (StringFunction.isNotBlank(CreditNotes
											.getAttribute("end_to_end_id"))) {
								%>

								<b><%=CreditNotes.getAttribute("end_to_end_id")%></b>
								<%
									} else {
								%>
								&nbsp;
								<%
									}
								%>
							</p>
						</td>

						<td>&nbsp;</td>
						<td>
							<p>
								<%=widgetFactory
					.createSubLabel("CreditNoteDetails.LinkedToInstType")%>
								<br>
								<%
									String linkedToInsttype = ReferenceDataManager.getRefDataMgr()
											.getDescr(
													TradePortalConstants.UPLOAD_INV_LINKED_INSTR_TY,
													CreditNotes
															.getAttribute("linked_to_instrument_type"),
													loginLocale);
									linkedToInsttype = (linkedToInsttype == null) ? ""
											: linkedToInsttype;
								%>
								<b><%=linkedToInsttype%></b>
							</p>
						</td>
					</tr>
				</table>



				<div class="formContentNoBorder">
					<div class="columnLeftNoBorder">
						<%
							if (TradePortalConstants.PAYABLES_MGMT.equals(CreditNotes
									.getAttribute("linked_to_instrument_type"))) {
						%>
						<%=widgetFactory
						.createSubsectionHeader("UploadInvoiceDetail.BuyerInfo")%>
						<%
							} else {
						%>
						<%=widgetFactory
						.createSubsectionHeader("InvoicesOfferedDetail.Seller")%>
						<%
							}
						%>
						<div class="formItem">

							<%
								String buyerId = CreditNotes.getAttribute("buyer_name");
								buyerId = corpOrg.getAttribute("name");
								addressLine1 = corpOrg.getAttribute("address_line_1");
								addressLine2 = corpOrg.getAttribute("address_line_2");
								addressCity = corpOrg.getAttribute("address_city");
								addressCtry = corpOrg.getAttribute("address_country");
							%>
							<b><%=buyerId%></b> <br /> <b><%=addressLine1%></b>
							<%
								if (!InstrumentServices.isBlank(addressLine2)) {
							%>
							<br> <b><%=addressLine2%></b>
							<%
								}
							%>
							<br> <b><%=addressCity%></b>
							<div style="clear: both"></div>
						</div>
					</div>

					<div class="columnRightNoBorder"><%=widgetFactory
					.createSubsectionHeader("UploadInvoiceDetail.TradingPartnerInfo")%>
						<div class="formItem">

							<%
							String seller = StringFunction.isNotBlank(CreditNotes.getAttribute("seller_party_identifier"))?CreditNotes.getAttribute("seller_party_identifier"):CreditNotes.getAttribute("seller_id");
							addressLine1 = CreditNotes.getAttribute("seller_address_line_1");
							addressLine2 = CreditNotes.getAttribute("seller_address_line_2");
								if (rule != null) {
							seller=	StringFunction.isBlank(seller)?rule.getAttribute("buyer_name"):seller;
								addressLine1 = StringFunction.isBlank(addressLine1)?rule.getAttribute("address_line_1"):addressLine1;
									addressLine2 = StringFunction.isBlank(addressLine2)?rule.getAttribute("address_line_2"):addressLine2;
									addressCity = rule.getAttribute("address_city");
									addressCtry = rule.getAttribute("address_country");
								}	
							%>
							<b><%=seller%></b><br />
							
							<b><%=addressLine1%></b>
							<%
								if (!StringFunction.isBlank(addressLine2)) {
							%>
							<br> <b><%=addressLine2%></b>
							<%
								}
							%>
							<br> <b><%=addressCity%></b> <b><%=addressCtry%></b>
						</div>
					</div>
					<div style="clear: both"></div>
				</div>



				<div style="clear: both"></div>
				<div class="formContentNoBorder">
					<%=widgetFactory
					.createWideSubsectionHeader("CreditNoteDetails.InvoiceGoodsDesc")%>


					<%
						if (invoiceSummaryGoodsResult != null) {
							Vector summaryGoodsVector = invoiceSummaryGoodsResult
									.getFragments("/ResultSetRecord");
							int totalSummaryGoods = summaryGoodsVector.size();
							if (totalSummaryGoods > 0) {
								// Only display the first Goods Summary
								totalSummaryGoods = 1;
							}

							for (int goods = 0; goods < totalSummaryGoods; goods++) {
					%>
					<div class="columnLeftNoBorder">
						<div class="formItem">
							<table class="poContent">
								<%--DK IR T36000014551 Rel8.2 03/12/2013 --%>
								<%
									if (!InstrumentServices
													.isBlank(((DocumentHandler) summaryGoodsVector
															.get(goods))
															.getAttribute("/GOODS_DESCRIPTION"))
													|| !InstrumentServices
															.isBlank(((DocumentHandler) summaryGoodsVector
																	.get(goods))
																	.getAttribute("/INCOTERM"))
													|| !InstrumentServices
															.isBlank(((DocumentHandler) summaryGoodsVector
																	.get(goods))
																	.getAttribute("/COUNTRY_OF_LOADING"))
													|| !InstrumentServices
															.isBlank(((DocumentHandler) summaryGoodsVector
																	.get(goods))
																	.getAttribute("/COUNTRY_OF_DISCHARGE"))
													|| !InstrumentServices
															.isBlank(((DocumentHandler) summaryGoodsVector
																	.get(goods))
																	.getAttribute("/VESSEL"))
													|| !InstrumentServices
															.isBlank(((DocumentHandler) summaryGoodsVector
																	.get(goods))
																	.getAttribute("/CARRIER"))
													|| !InstrumentServices
															.isBlank(((DocumentHandler) summaryGoodsVector
																	.get(goods))
																	.getAttribute("/ACTUAL_SHIP_DATE"))
													|| !InstrumentServices
															.isBlank(((DocumentHandler) summaryGoodsVector
																	.get(goods))
																	.getAttribute("/PURCHASE_ORDER_ID"))) {
								%>
								<tr>
									<td width="2%" nowrap>&nbsp;</td>
									<td nowrap><%=widgetFactory
								.createSubLabel("UploadInvoiceDetail.GoodsDescr")%></td>
									<td><b><%=StringFunction
								.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
										.get(goods))
										.getAttribute("/GOODS_DESCRIPTION"))%></b></td>
								</tr>

								<tr>
									<td width="2%" nowrap>&nbsp;</td>
									<td nowrap><%=widgetFactory
								.createSubLabel("UploadInvoiceDetail.Incoterm")%></td>
									<td><b><%=StringFunction
								.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
										.get(goods)).getAttribute("/INCOTERM"))%></b></td>
								</tr>
								<tr>
									<td width="2%" nowrap>&nbsp;</td>
									<td nowrap><%=widgetFactory
								.createSubLabel("UploadInvoiceDetail.PortofLoading")%></td>
									<td><b><%=StringFunction
								.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
										.get(goods))
										.getAttribute("/COUNTRY_OF_LOADING"))%></b></td>
								</tr>
								<tr>
									<td width="2%" nowrap>&nbsp;</td>
									<td nowrap><%=widgetFactory
								.createSubLabel("UploadInvoiceDetail.PortofDischarge")%></td>
									<td><b><%=StringFunction
								.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
										.get(goods))
										.getAttribute("/COUNTRY_OF_DISCHARGE"))%></b></td>
								</tr>
								<tr>
									<td width="2%" nowrap>&nbsp;</td>
									<td nowrap><%=widgetFactory
								.createSubLabel("UploadInvoiceDetail.Vessel")%></td>
									<td><b><%=StringFunction
								.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
										.get(goods)).getAttribute("/VESSEL"))%></b></td>
								</tr>

								<tr>
									<td width="2%" nowrap>&nbsp;</td>
									<td nowrap><%=widgetFactory
								.createSubLabel("UploadInvoiceDetail.Carrier")%></td>
									<td><b><%=StringFunction
								.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
										.get(goods)).getAttribute("/CARRIER"))%></b></td>
								</tr>

								<tr>
									<td width="2%" nowrap>&nbsp;</td>
									<td nowrap><%=widgetFactory
								.createSubLabel("UploadInvoiceDetail.ActualShpDate")%></td>
									<td><b><%=TPDateTimeUtility.formatDate(
								StringFunction
										.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
												.get(goods))
												.getAttribute("/ACTUAL_SHIP_DATE")),
								TPDateTimeUtility.SHORT, loginLocale)%></b></td>
								</tr>
								<tr>
									<td width="2%" nowrap>&nbsp;</td>
									<td nowrap><%=widgetFactory
								.createSubLabel("UploadInvoiceDetail.PurchaseOrderId")%></td>
									<td><b><%=StringFunction
								.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
										.get(goods))
										.getAttribute("/PURCHASE_ORDER_ID"))%></b></td>
								</tr>
								<%
									}
								%>
							</table>
						</div>
					</div>



					<div class="columnRightNoBorder">
						<div class="formItem">
							<table>
								<%
									for (int i = 1; i <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i++) {
												if (!InstrumentServices
														.isBlank(((DocumentHandler) summaryGoodsVector
																.get(goods))
																.getAttribute("/BUYER_USERS_DEF" + i
																		+ "_LABEL"))) {
								%>
								<tr>
									<td nowrap><%=StringFunction
									.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
											.get(goods))
											.getAttribute("/BUYER_USERS_DEF"
													+ i + "_LABEL"))%></td>
									<td nowrap>&nbsp;</td>
									<td><b><%=StringFunction
									.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
											.get(goods))
											.getAttribute("/BUYER_USERS_DEF"
													+ i + "_VALUE"))%></b></td>
								</tr>
								<%
									}
											}
								%>
								<%
									for (int i = 1; i <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i++) {
												if (!InstrumentServices
														.isBlank(((DocumentHandler) summaryGoodsVector
																.get(goods))
																.getAttribute("/SELLER_USERS_DEF" + i
																		+ "_LABEL"))) {
								%>
								<tr>
									<td nowrap><%=StringFunction
									.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
											.get(goods))
											.getAttribute("/SELLER_USERS_DEF"
													+ i + "_LABEL"))%></td>
									<td nowrap>&nbsp;</td>
									<td><b><%=StringFunction
									.xssCharsToHtml(((DocumentHandler) summaryGoodsVector
											.get(goods))
											.getAttribute("/SELLER_USERS_DEF"
													+ i + "_VALUE"))%></b></td>
								</tr>
								<%
									}
											}
								%>
							</table>

						</div>
					</div>
					<%
						}
						}
					%>
				</div>


				<div style="clear: both"></div>
				<div class="formContentNoBorder">
					<%=widgetFactory
					.createWideSubsectionHeader("CreditNoteDetails.InvoicesAppliedDetails")%>
					<%--T36000036055 - appling scroll bar Rpasupulati--%>
				<%if(creditNoteAppliedInvoicesResult != null && numberOfAppliedInvoices>5){%>
				<div class="tableScroll5records">
				<%}%>
					<table>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><%=widgetFactory
					.createSubLabel("CreditNoteDetails.InvoiceID")%></td>

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><%=widgetFactory
					.createSubLabel("CreditNoteDetails.AppliedAmount")%></td>
						</tr>

						
					<% 
if (creditNoteAppliedInvoicesResult != null) {
for(int i=0; i<numberOfAppliedInvoices ; i++){
            creditNoteAppliedInvoicesResult = (DocumentHandler) creditNoteAppliedInvoicesList.elementAt(i);
       %>

						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><b><%=creditNoteAppliedInvoicesResult.getAttribute("/INVOICE_ID") %></b>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><b><%=currency%></b>&nbsp; <b><%=TPCurrencyUtility.getDisplayAmount(
										creditNoteAppliedInvoicesResult.getAttribute("/CREDIT_NOTE_APPLIED_AMOUNT").substring(1),
						useCurrency, loginLocale)%></b></td>
						</tr>

						<%
						}
						
						} %>
					</table>
					<%if(creditNoteAppliedInvoicesResult != null && numberOfAppliedInvoices>5){%>
					</div>
					<%}%>
</div>
  <%

   
  DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  LogGridHtml = dgFactory.createDataGrid("invoiceHistoryLogGrid","InvoiceHistoryLogDataGrid",null);
  logGridLayout = dgFactory.createGridLayout("InvoiceHistoryLogDataGrid");
  %>

				<div class="formItem">
					<div class="GridHeader">
						<span class="invGridHeaderTitle"
							style="border-top: 1px solid #DBDBDB;"> <%=resMgr.getText("UploadInvoiceSummary.TransactionLog",TradePortalConstants.TEXT_BUNDLE)%>
						</span>
					</div>
					<div style="clear: both"></div>
					<%=LogGridHtml %>
				</div>
			</div>
		</form>
	</div>

	<%@ include file="/transactions/fragments/LoadPanelLevelAliases.frag"%>

	<jsp:include page="/common/Footer.jsp">
		<jsp:param name="displayFooter"
			value="<%=TradePortalConstants.INDICATOR_YES%>" />
		<jsp:param name="includeNavigationBarFlag"
			value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>
	<script type="text/javascript" src="/portal/js/datagrid.js"></script>
	<script type="text/javascript">
require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
  <%--get grid layout from the data grid factory--%>
  var logGridLayout = <%= logGridLayout %>;
  <%--set the initial search parms--%>
  <%-- var init='upload_invoice_oid=<%=upload_invoice_oid%>'; --%> 
  var init='upload_invoice_oid=<%=EncryptDecrypt.encryptStringUsingTripleDes(upload_invoice_oid, userSession.getSecretKey())%>';

  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoiceHistoryLogDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var invoiceHistoryLogGrid =  onDemandGrid.createOnDemandGrid("invoiceHistoryLogGrid", viewName, logGridLayout, init,'0'); 
});
  

  function panelLevelAliasesFormatter(columnValues){
	  var panelLevel = columnValues[0];
	  var panelAlias="";
	  if(panelLevel != ""){
		  panelAlias = panelStore.get(panelLevel).name;
	  }
	  return panelAlias;
	  
  }
      
</script>
</div>

<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="invoiceHistoryLogGrid" />
</jsp:include>
</body>

<%
  // Finally, reset the cached document to eliminate carryover of 
  // information to the next visit of this page.
 if(!"goToViewInvoices".equals(closeLink) && !"goToViewTransInvoices".equals(closeLink))
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>

</html>
