<%--
Ravindra - Rel8200 CR-708B - Initial Version
*******************************************************************************
                        Invoice Group Detail

  Description:
     This page is used to display the invoices of an invoice group.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
StringBuffer      onLoad                = new StringBuffer();
String            helpSensitiveLink     = null;
String            formName              = null;
String            gridID                = null;
String            gridName              = null;
String            viewName              = null;
Vector 			  invoiceOidList 		= null;
String searchListViewName = "InvoicesOfferedGroupDetailDataView.xml";

  // Set up page navigation stack
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); 
  }
  else {
    userSession.addPage("goToAcceptInvoiceOffersGrouping");
  }

DocumentHandler doc;
//Get the document from the cache.  We'll may use it to repopulate the
// screen if returning from another page, a save, validation, or any other
// mediator called from this page.  Otherwise, we assume an instrument oid
// and transaction oid was passed in.

doc = formMgr.getFromDocCache();
invoiceOidList = doc.getFragments("/Out/InvoiceOfferList/InvoiceOffer");
StringBuffer invoiceOids=new StringBuffer();
for ( int iLoop=0; iLoop<invoiceOidList.size(); iLoop++)
{
	DocumentHandler invoiceOidDoc = (DocumentHandler) invoiceOidList.elementAt(iLoop);
	if(iLoop>0) {
		invoiceOids.append(",");
	}
	invoiceOids.append(invoiceOidDoc.getAttribute("/InvoiceData"));
	
}
   Debug.debug("invoiceOids  "+invoiceOids);
   formName = "InvoiceOfferesGroupingForm";
   helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm", resMgr, userSession);
   
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.
   Hashtable secureParms = new Hashtable();
   secureParms.put("UserOid", userSession.getUserOid());
   secureParms.put("SecurityRights",userSession.getSecurityRights()); 
   secureParms.put("ownerOrg",userSession.getOwnerOrgOid());
   String value;
   value = doc.getAttribute("/Out/group_name");
   if (value != null) secureParms.put("GroupName", value);
   value = doc.getAttribute("/Out/group_currency");
   if (value != null) secureParms.put("GroupCurrency", value);      
   value = doc.getAttribute("/Out/group_amount");
   if (value != null) secureParms.put("GroupAmount", value); //Nar Release8.3.0.0 09-09-2013 IR-T36000020527
   
   //secureParms.put("InvoiceOids",doc);
   String invoiceGroupOid = "";
%>

<%-- ********************* HTML for page begins here *********************  --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>


<%-- Body tag included as part of common header --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad.toString()%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">
    
    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <%= resMgr.getText("InvoicesOffered.GroupedInvoices", TradePortalConstants.TEXT_BUNDLE) %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <%= resMgr.getText("InvoicesOffered.TargetAmount", TradePortalConstants.TEXT_BUNDLE) + ": " %>
        <%= doc.getAttribute("/Out/target_amount") %>&nbsp;&nbsp;
        <%= resMgr.getText("InvoicesOffered.ActualAmount", TradePortalConstants.TEXT_BUNDLE) + ": " %>
        <%= StringFunction.xssCharsToHtml(doc.getAttribute("/Out/actual_amount")) %> 
      </span>
      <div style="clear:both;">
      </div>
    </div>

<form name="<%=formName%>" method="POST" action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">
    <jsp:include page="/common/ErrorSection.jsp" />
    <div class="formContentNoSidebar">
   <input type=hidden value="" name=buttonName />
<input type=hidden value='<%=StringFunction.xssCharsToHtml(doc.toString())%>' name="InvoiceOids" /> 
  <%
  String resourcePrefix ="";
  gridID = "InvoicesOfferesGroupingAcceptance";
 gridName = "InvoicesOfferedUserDefinedGroupingDataGrid";
 viewName = "InvoicesOfferedUserDefinedGroupingDataView";
 resourcePrefix = "InvoicesOffered";
  %>
  <div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderActions">
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="<%=gridID%>" />
      </jsp:include>
    </span>
    <div style="clear:both;"></div>
  </div>
  <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  String gridHtml = dgFactory.createDataGridMultiFooter(gridID,gridName,"5");
 %>
<%=gridHtml%>

<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
</div>
<div id="InvoiceGroupDetailDialogID"></div>
</form>
</div>
</div>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
</body>
<script type="text/javascript" src="/portal/js/datagrid.js"></script>

<%
  String gridLayout = dgFactory.createGridLayout(gridName);
%>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  var gridID = '<%= gridID %>';
  var gridName = '<%= gridName %>';
  var viewName = '<%= EncryptDecrypt.encryptStringUsingTripleDes(viewName,userSession.getSecretKey())%>';  <%--Rel9.2 IR T36000032596 --%>
  var formName = '<%=formName %>';
  <%--set the initial search parms--%>
<%
  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
  //search - note that the value in the dropdown is encrypted
  //String encryptedOrgOid = EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
%>
  var init='InvoiceOidList=<%=EncryptDecrypt.encryptStringUsingTripleDes(invoiceOids.toString(), userSession.getSecretKey())%>';
  var InvoiceListId = createDataGrid(gridID, viewName, gridLayout, init);

 function performInvoiceListAction(pressedButton){
	    <%--get array of rowkeys from the grid--%>
 var rowKeys = getSelectedGridRowKeys(gridID);

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	 submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
}
</script>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
</html>
