<%--
Ravindra - Rel8200 CR-708B - Initial Version
**********************************************************************************
  Receivables Transactions Home

  Description:
     This page is used as the main Receivables Management page. It displays one of
     three tabs: Receivable Match Notices, Invoices, Receivables Instruments.

**********************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);

%>

<%
   final String      STATUS_ALL = resMgr.getText("common.StatusAll",TradePortalConstants.TEXT_BUNDLE);
   StringBuffer      dynamicWhereClause     = new StringBuffer();
   StringBuffer      onLoad                 = new StringBuffer();
   Hashtable         secureParms            = new Hashtable();
   String            helpSensitiveLink      = null;
   String            userSecurityRights     = null;
   String            userSecurityType       = null;
   String            current2ndNav          = null;
   String            userOrgOid             = null;
   String            userOid                = null;
   String            formName               = "";
   StringBuffer      newLink                = new StringBuffer();
   
   //Added for all the options
   String initSearchParms="";
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
   String gridHtml = "";
   String gridLayout = "";
   String gridName = "";
   String gridId ="";
   String dataViewName ="";
   
   String 	     loginLocale 		  = null;

   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

   StringBuffer newSearchReceivableCriteria = new StringBuffer();//PPramey - CR-434 - 22th Nov 08

   
   userSecurityRights           = userSession.getSecurityRights();
   userSecurityType             = userSession.getSecurityType();
   userOrgOid                   = userSession.getOwnerOrgOid();
   userOid                      = userSession.getUserOid();
   session.setAttribute("fromInvoiceGroupDetail", "INVH");
   current2ndNav = request.getParameter("current2ndNav");
   if (current2ndNav == null)
   {
	   current2ndNav = (String) session.getAttribute("receivableTransactions2ndNav");

	   if (current2ndNav == null)
	   {
		   current2ndNav = TradePortalConstants.DISPLAY_INVOICE_OFFERED;
	   }
   }
   session.setAttribute("InvoiceGroupName","");
   
  // Set up page navigation stack
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); 
  }
  else {
    userSession.addPage("goToInvoiceOffersHistory", request);
  }

   // ***********************************************************************
   // Data setup for the Invoices tab
   // ***********************************************************************
   	  formName     		= "InvoicesOfferedHistoryForm";
      gridId 			= "invoicesOfferesHistoryGrid";
      dataViewName 		= "InvoicesOfferedHistoryView";
      gridName 			= "InvoicesOfferedHistoryGrid";
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/Invoice_Details.htm",  resMgr, userSession);
      loginLocale=userSession.getUserLocale();
   // End data setup for Invoices tab


%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>

<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<div class="pageMainNoSidebar">
<div class="pageContent">

<div class="secondaryNav">

  <div class="secondaryNavTitle">
    <%=resMgr.getText("TransactionsMenu.InvoiceOffers.History",TradePortalConstants.TEXT_BUNDLE)%>
  </div>
  
  <div id="secondaryNavHelp" class="secondaryNavHelp">
    <%=helpSensitiveLink%>
  </div>
  <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %>
  <div style="clear:both;"></div>

</div>




<form name="<%=formName%>" id="<%=formName%>" method="POST"
      action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">

 <jsp:include page="/common/ErrorSection.jsp" />

 <div class="formContentNoSidebar">
  <input type=hidden value="" name=buttonName>


<%@ include file="/invoiceoffers/fragments/InvoiceOffersHistory_Invoices.frag" %>

<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
</div>
</form>

</div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>


<script >
	
	var initSearchParms="";
	var loginLocale="";
	var instrument="";
	var gridId = '<%=gridId%>';
	var dataViewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes(dataViewName,userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
	var gridName = '<%=gridName%>';
	<%--get grid layout from the data grid factory--%>
	var gridLayout = <%=gridLayout%>;

	<%--set the initial search parms--%>
	initSearchParms = "<%=initSearchParms%>";
	loginLocale="<%=StringFunction.xssCharsToHtml(loginLocale)%>";
	console.log("initSearchParms: " + initSearchParms);
	var savedSort = savedSearchQueryData["sort"];  
   	if("" == savedSort || null == savedSort || "undefined" == savedSort){
        savedSort = '0';
     }
  function initializeTranDataGrid(){
	<%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
	 require(["t360/datagrid", "dijit/registry", "dojo/dom"],
		        function( t360grid,dom, registry) {
        	var currency = savedSearchQueryData["currency"]; 
         	var amountFrom = savedSearchQueryData["amountFrom"]; 
         	var amountTo = savedSearchQueryData["amountTo"]; 
         	var dateFrom = savedSearchQueryData["dateFrom"]; 
         	var dateTo = savedSearchQueryData["dateTo"]; 
         	var invoiceID = savedSearchQueryData["invoiceID"]; 
        	var relatedInstrumentID = savedSearchQueryData["relatedInstrumentID"]; 
        	var invoiceStatus = savedSearchQueryData["selectedStatus"]; 
           	if("" == currency || null == currency || "undefined" == currency)
           		currency=dom.byId("Currency").value;
      		 else
      			 registry.byId("Currency").set('value',currency);
         
           	if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
           		amountFrom=dom.byId("AmountFrom").value;
      		 else
      			dom.byId("AmountFrom").value = amountFrom;

           	if("" == amountTo || null == amountTo || "undefined" == amountTo)
           		amountTo=dom.byId("AmountTo").value;
      		 else
      			dom.byId("AmountTo").value = amountTo;
        	
        	if("" == dateFrom || null == dateFrom || "undefined" == dateFrom)
        		dateFrom=dom.byId("DateFrom").value;
      		 else
      			 registry.byId("DateFrom").set('value',dateFrom);
        	
	    	if("" == dateTo || null == dateTo || "undefined" == dateTo)
	    		dateTo=dom.byId("DateTo").value;
	  		 else
	  			 registry.byId("DateTo").set('value',dateTo);
	    	
	    	if("" == invoiceID || null == invoiceID || "undefined" == invoiceID)
	    		invoiceID=dom.byId("InvoiceID").value;
	  		 else
	  			dom.byId("InvoiceID").value = invoiceID;
	    	
	    	if("" == relatedInstrumentID || null == relatedInstrumentID || "undefined" == relatedInstrumentID)
	    		relatedInstrumentID=dom.byId("RelatedInstrumentID").value;
	  		 else
	  			dom.byId("RelatedInstrumentID").value = relatedInstrumentID;
	    	
	    	if("" == invoiceStatus || null == invoiceStatus || "undefined" == invoiceStatus)
	    		invoiceStatus=dom.byId("invoiceStatusType").value;
	  		 else
	  			 registry.byId("invoiceStatusType").set('value',invoiceStatus);
	    	
	    	initSearchParms = initSearchParms+"&selectedStatus="+invoiceStatus+"&invoiceID="+invoiceID+"&currency="+currency+"&amountFrom="+amountFrom
		        +"&amountTo="+amountTo+"&dateFrom="+dateFrom+"&dateTo="+dateTo+"&relatedInstrumentID="+relatedInstrumentID;
            t360grid.createDataGrid(gridId, dataViewName,gridLayout, initSearchParms,savedSort);

	});
  }

	function dialogQuickView(rowKey){
		 var dialogDiv = document.createElement("div");
		 dialogDiv.setAttribute('id',"quickviewdialogdivid");
		 document.forms[0].appendChild(dialogDiv);
		 
		 var title="";

		require(["t360/dialog"], function(dialog ) {
			
	        dialog.open('quickviewdialogdivid',title ,
	                    'TransactionQuickViewDialog.jsp',
	                    ['rowKey','DailogId'],[rowKey,'quickviewdialogdivid'], <%-- parameters --%>
	                    'select', quickViewCallBack);
	      });
	}

	var quickViewFormatter=function(columnValues, idx, level) {
		var gridLink="";

		if(level == 0){

			if ( columnValues.length >= 2 ) {
			      gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
			    }
			    else if ( columnValues.length == 1 ) {
			      <%-- no url parameters, just return the display --%>
			      gridLink = columnValues[0];
			    }
			    return gridLink;

	  }else if(level ==1){
		  	console.log(columnValues[3]);
		  	if(columnValues[3]=='PROCESSED_BY_BANK'){
		  		if(columnValues[2]){
		  			return "<a href='javascript:dialogQuickView("+columnValues[2]+");'>Quick View</a> "+columnValues[0]+ " ";
		  		}else{
		  			return columnValues[0];	
		  		}

		  	}else{
					return columnValues[0];
			  	}

	  }
	}
	var formatGridLinkChild=function(columnValues, idx, level){
	var gridLink="";

		if(level == 0){
			return columnValues[0];

	  }else if(level ==1){
		  if ( columnValues.length >= 2 ) {
		      gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
		    }
		    else if ( columnValues.length == 1 ) {
		      <%-- no url parameters, just return the display --%>
		      gridLink = columnValues[0];
		    }
		    return gridLink;
		    }
	}
	
	<%-- function to set the title after ajax call of dialog value[2]- dialogId, value[0]-complete instrument id, value[1]-transaction type --%>
	function quickViewCallBack(key,value){
		dijit.byId(value[2]).set('title','Transaction Quick View - '+value[0]+' - '+value[1]);
		dijit.byId(value[2]).set('style','width:600px');
		
	}
	
	function booleanFormat(item){
		if(item=='true')
			return true;
		else
			return false;
	}
	<%--provide a search function that collects the necessary search parameters--%>
	  function searchInvoices() {
	    require(["dojo/dom"],
	      function(dom){

	    	var invoiceStatus = dijit.byId("invoiceStatusType").attr('value');
	    	var invoiceID=(dijit.byId("InvoiceID").value).toUpperCase();
	    	var currency=(dijit.byId("Currency").value).toUpperCase();
	    	var amountFrom=(dom.byId("AmountFrom").value).toUpperCase();
	    	var amountTo=(dom.byId("AmountTo").value).toUpperCase();
	    	var dateFrom=(dom.byId("DateFrom").value).toUpperCase();
	    	var dateTo=(dom.byId("DateTo").value).toUpperCase();
	    	var relatedInstrumentID=(dijit.byId("RelatedInstrumentID").value).toUpperCase();

	        <%-- var filterText=filterTextCS.toUpperCase(); --%>
	        var searchParms = "selectedStatus="+invoiceStatus+"&invoiceID="+invoiceID+"&currency="+currency+"&amountFrom="+amountFrom
	        +"&amountTo="+amountTo+"&dateFrom="+dateFrom+"&dateTo="+dateTo+"&relatedInstrumentID="+relatedInstrumentID;

	        console.log("SearchParms: " + searchParms);
	        searchDataGrid(gridId, dataViewName, searchParms);
	      });
	  }
	  require(["dijit/registry", "dojo/query", "dojo/on", "dojo/dom-class", "t360/dialog", 
	           "dojo/dom", "dijit/layout/ContentPane", "dijit/TooltipDialog", "dijit/popup",
	           "t360/common", "t360/popup", "t360/datagrid",
	           "dojo/ready", "dojo/domReady!"],
	      function(registry, query, on, domClass, dialog,
	               dom, ContentPane, TooltipDialog, popup, 
	               common, t360popup, t360grid,
	               ready ) {
	

	    <%--register widget event handlers--%>
	    ready(function() {
	      <%--search event handlers--%>
	      initializeTranDataGrid();
	      var invoiceStatusType = registry.byId("invoiceStatusType");
	      if ( invoiceStatusType ) {
	    	  invoiceStatusType.on("change", function() {
	    		  searchInvoices();
	        });
	      }
	      
	
	    });
	  });
	  
	  <%-- Kiran  05/13/2013  Rel8.2 IR-T36000014861  Start --%>
	  <%-- Added this function in order for enter key to work in firefox --%>
	  function filterInvoiceOfferHistoryOnEnter(fieldId){
			<%-- 
		  	* The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
		  	 --%>
		  	require(["dojo/on","dijit/registry"],function(on, registry) {
			    on(registry.byId(fieldId), "keypress", function(event) {
			        if (event && event.keyCode == 13) {
			        	dojo.stopEvent(event);
			        	searchInvoices();
			        }
			    });
			});
		} 
	  <%-- Kiran  05/13/2013  Rel8.2 IR-T36000014861  End --%>
	  
  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
      function(query, on, t360grid, t360popup){

   require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	      function(query, on, t360grid, t360popup){
	    query('#invoicesOfferedRefresh').on("click", function() {
	    	searchInvoices();
	    });
    query('#invoicesOfferedGridEdit').on("click", function() {
      var columns = t360grid.getColumnsForCustomization(gridId);
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ gridId, gridName, columns ];
      t360popup.open(
        'invoicesOfferedGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
   });

  });

</script>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="<%=gridId%>" />
</jsp:include>

</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());

   session.setAttribute("receivableTransactions2ndNav", current2ndNav);
%>
