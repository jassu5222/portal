<%--
Ravindra - Rel8200 CR-708B - Initial Version
**********************************************************************************
  Receivables Transactions Home

  Description:
     This page is used as the main Receivables Management page. It displays one of
     three tabs: Receivable Match Notices, Invoices, Receivables Instruments.

**********************************************************************************
--%>

<%--
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);

%>

<%
   final String      STATUS_ALL = resMgr.getText("common.StatusAll",TradePortalConstants.TEXT_BUNDLE);
   StringBuffer      dynamicWhereClause     = new StringBuffer();
   StringBuffer      onLoad                 = new StringBuffer();
   Hashtable         secureParms            = new Hashtable();
   String            helpSensitiveLink      = null;
   String            userSecurityRights     = null;
   String            userSecurityType       = null;
   String            current2ndNav          = null;
   String            userOrgOid             = null;
   String            userOid                = null;
   String            formName               = "";
   StringBuffer      newLink                = new StringBuffer();
   
   //Added for all the options
   String initSearchParms="";
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
   String gridHtml = "";
   String gridLayout = "";
   String gridName = "";
   String gridId ="";
   String dataViewName ="";
   
   String 	     loginLocale 		  = null;

   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
  // Set up page navigation stack
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); 
  }
  else {
    userSession.addPage("goToInvoiceOffersTransactionsHome", request);
  }
  //IR T36000026794 Rel9.0 07/24/2014 starts
  session.setAttribute("startHomePage", "InvoiceOfferTransactionsHome");
  session.removeAttribute("fromTPHomePage");
  //IR T36000026794 Rel9.0 07/24/2014 End
  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryData =null;

   StringBuffer newSearchReceivableCriteria = new StringBuffer();//PPramey - CR-434 - 22th Nov 08

   
   userSecurityRights           = userSession.getSecurityRights();
   userSecurityType             = userSession.getSecurityType();
   userOrgOid                   = userSession.getOwnerOrgOid();
   userOid                      = userSession.getUserOid();
   secureParms.put("UserOid",userSession.getUserOid());
   secureParms.put("SecurityRights",userSecurityRights); 
   secureParms.put("ownerOrg",userSession.getOwnerOrgOid());

   CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   
   current2ndNav = request.getParameter("current2ndNav");
   if (current2ndNav == null)
   {
	   current2ndNav = (String) session.getAttribute("invoiceOfferTransactions2ndNav");

	   if (current2ndNav == null)
	   {
		   current2ndNav = TradePortalConstants.DISPLAY_INVOICE_OFFERED;
	   }
	   if( session.getAttribute("receivableTransactions2ndNav")!=null){

		   current2ndNav = (String) session.getAttribute("receivableTransactions2ndNav");
	   }
	  
   }
   
   // ***********************************************************************
   // Data setup for the Invoices tab
   // ***********************************************************************
   if (TradePortalConstants.DISPLAY_INVOICE_OFFERED.equals(current2ndNav))
   {
      formName     		= "InvoicesOfferedListForm";
      gridId 			= "invoicesOfferedGrid";
      dataViewName 		= "InvoicesOfferedDataView";
      gridName 			= "InvoicesOfferedDataGrid";
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/Invoice_Details.htm",  resMgr, userSession);
      loginLocale=userSession.getUserLocale();
   } // End data setup for Invoices tab

   // ***********************************************************************
   // Data setup for the Receivables Instruments tab
   // ***********************************************************************
   else if (TradePortalConstants.DISPLAY_FUTURE_VALUE_OFFERS.equals(current2ndNav))
   {
	  gridId 			= "invoicesFvdOfferedGrid";
	  dataViewName 		= "InvoicesFvdOfferedDataView";
      formName        	= "InvoicesFvdOfferedListForm";
      gridName 			= "InvoicesFvdOfferedDataGrid";
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/Receivables_Instruments_Listview.htm",  resMgr, userSession);
      loginLocale = userSession.getUserLocale();
    }  // End data setup for Receivables Instruments tab.
	 else if (TradePortalConstants.INDICATOR_YES.equals(current2ndNav))
	{
	  formName     		= "InvoicesOfferedHistoryForm";
      gridId 			= "invoicesOfferesHistoryGrid";
      dataViewName 		= "InvoicesOfferedHistoryView";
      gridName 			= "InvoicesOfferedHistoryGrid";
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/Invoice_Details.htm",  resMgr, userSession);
      loginLocale=userSession.getUserLocale();
	}
   String searchNav = "invOffTranHome."+current2ndNav;
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>

<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

<div class="pageMainNoSidebar">
<%-- IR T36000030360: (gridInvoiceFooterDivHidden) Hide page content, untill widgets get parsed. --%>
<div id="InvoiceMgtDivID" class="pageContent gridInvoiceFooterDivHidden">

<div class="secondaryNav">

  <div class="secondaryNavTitle">
    <%=resMgr.getText("SecondaryNavigation.InvoiceOffers",TradePortalConstants.TEXT_BUNDLE)%>
  </div>

  <%
    String invoicesSelected  = TradePortalConstants.INDICATOR_NO;
    String inquiriesSelected = TradePortalConstants.INDICATOR_NO;
	String invoicesHistorySelected = TradePortalConstants.INDICATOR_NO;
    String linkParms = "";
    
    if(TradePortalConstants.DISPLAY_INVOICE_OFFERED.equals(current2ndNav)) {
    	invoicesSelected = TradePortalConstants.INDICATOR_YES;
    }
	else if(TradePortalConstants.INDICATOR_YES.equals(current2ndNav)) {
    	invoicesHistorySelected = TradePortalConstants.INDICATOR_YES;
    }else{
    	inquiriesSelected = TradePortalConstants.DISPLAY_INVOICE_OFFERS_HISTORY;	
    }
      linkParms = "current2ndNav=" + TradePortalConstants.DISPLAY_INVOICE_OFFERED;
	  
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.InvoiceOffers.InvoicesOffered" />
    <jsp:param name="linkSelected" value="<%= invoicesSelected %>" />
    <jsp:param name="action"       value="goToInvoiceOffersTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= linkParms %>" />
    <jsp:param name="hoverText"    value="InvoiceOffersTransaction.invoicesList" />
  </jsp:include>
  <%
   	  linkParms = "current2ndNav=" + TradePortalConstants.DISPLAY_FUTURE_VALUE_OFFERS;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.InvoiceOffers.FutureValueDated" />
    <jsp:param name="linkSelected" value="<%= inquiriesSelected %>" />
    <jsp:param name="action"       value="goToInvoiceOffersTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= linkParms %>" />
    <jsp:param name="hoverText"    value="InvoiceOffersTransaction.fvdList" />
  </jsp:include>
  
  <%-------unCommenting below navigation link as per IR #T36000018131 RPasupulati Starts---%>
   <%
   	  linkParms = "current2ndNav=" + TradePortalConstants.DISPLAY_INVOICE_OFFERS_HISTORY;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.InvoiceOffers.History" />
    <jsp:param name="linkSelected" value="<%= invoicesHistorySelected %>" />
    <jsp:param name="action"       value="goToInvoiceOffersTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= linkParms %>" />
    <jsp:param name="hoverText"    value="InvoiceOffersTransaction.fvdList" />
  </jsp:include>
 <%--IR #T36000018131 RPasupulati Ends.--%>

  <div id="secondaryNavHelp" class="secondaryNavHelp">
    <%=helpSensitiveLink%>
  </div>
  <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %>
  <div style="clear:both;"></div>

</div>


<form name="<%=formName%>" id="<%=formName%>" method="POST"
      action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">

 <jsp:include page="/common/ErrorSection.jsp" />

 <div class="formContentNoSidebar">
  <input type=hidden value="" name=buttonName>
   <input type="hidden" name="FutureValueDate" />


<%
  // Based on the current link, display the appropriate HTML

 if (TradePortalConstants.DISPLAY_INVOICE_OFFERED.equals(current2ndNav))
  { if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("InvoicesOfferedDataView");
	}
%>
    <%@ include file="/invoiceoffers/fragments/InvoiceOffersManagement_Invoices.frag" %>
<%
  // jgadela 11/06/2014 R91 IR T36000034293	 - We need to set this to find out back navigation (Fixed the null pointer exp.) 
  session.setAttribute("documentImageFileUploadPageOriginator", "InvoiceOffersTransactionsHome");
  }
  else if ( TradePortalConstants.DISPLAY_FUTURE_VALUE_OFFERS.equals(current2ndNav))
  { if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("InvoicesFvdOfferedDataView");
  }
%>
    <%@ include file="/invoiceoffers/fragments/InvoiceOffersFvdManagement_Invoices.frag" %>
<%
	session.setAttribute("documentImageFileUploadPageOriginator", "InvoiceOffersTransactionsHome");
  }
   else if ( TradePortalConstants.INDICATOR_YES.equals(current2ndNav))
  { if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("InvoicesOfferedHistoryView");
	}
%>
    <%@ include file="/invoiceoffers/fragments/InvoiceOffersHistory_Invoices.frag" %>
<%
  }
%>
<%


%>
<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
</div>
</form>

</div>
</div>
<div id="InvoiceGroupListDialogID"></div>
<div id="InvoiceGroupingDialogID"></div>

<%--PMitnala Cr 821 Rel 8.3 START --%>
<form method="post" id="InvoiceOffersGroup-NotifyPanelUserForm" name="InvoiceOffersGroup-NotifyPanelUserForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden name="buttonName" value="">
<input type=hidden name="invoiceOfferGroupOid" value=""> 
<%= formMgr.getFormInstanceAsInputField("InvoiceOffersGroup-NotifyPanelUserForm", secureParms) %>
 </form>
 <%--PMitnala Cr 821 Rel 8.3 END --%>
 
<%--cquinton 3/16/2012 Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton 3/16/2012 Portal Refresh - end--%>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>

<script >
    var home={};
	var initSearchParms="<%=initSearchParms%>";
	var loginLocale="<%=StringFunction.xssCharsToHtml(loginLocale)%>";
	var gridId = '<%=gridId%>';
	var dataViewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes(dataViewName,userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
	var gridName = '<%=gridName%>';
	var gridLayout = <%=gridLayout%>;

   function viewInvoiceOfferFormatter (columnValues){
	    var gridLink = "";
	  <%-- IR T36000023210 RPasupulati Start --%>
		if(columnValues[1] == 'PARTIALLY_AUTHORIZED' ){
			 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+ "<%=resMgr.getText("TransactionsMenu.InvoiceOffers", TradePortalConstants.TEXT_BUNDLE)%>";
			 var insType = '<%= TradePortalConstants.SUPPLIER_PORTAL %>';
			 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"invoicesOfferedGrid\",\""+insType+"\");'>"+columnValues[0]+"</a>";
	    	 return gridLink;
		}else{
			columnValues[0]="";
			<%-- IR T36000023210 RPasupulati Ends --%>
			return columnValues[0];
		}
	}
	
	console.log("initSearchParms: " + initSearchParms);
	function initializeDataGrid(){
	
   	
	
	 
	function performInvoiceListAction(pressedButton){
	    <%--get array of rowkeys from the grid--%>
	var rowKeys = getSelectedGridRowKeys('<%=gridId%>');
	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	 submitFormWithParms('<%=formName%>', pressedButton, "checkbox", rowKeys);
	}
	function applyAssignFutureValueDateToInvGroupList(){
		require(["t360/dialog"], function(dialog) {
	 	      dialog.open("InvoiceGroupListDialogID", '<%=resMgr.getText("InvoicesOffered.AssignFutureValueDate", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignFutureValueDate.jsp',
		                      [], [], <%-- parameters --%>
		                      'selectFVD', this.submitAssignFutureValueDate);
		  });
	}

        <%-- Callback method for Assign Future Value Date --%>
        function submitAssignFutureValueDate(futureValueDate) {

                 var theForm = document.getElementsByName("<%=formName%>")[0];
                theForm.FutureValueDate.value=futureValueDate;
                var rowKeys = getSelectedGridRowKeys('<%=gridId%>');
            submitFormWithParms('<%=formName%>',"AssignFVD", "checkbox",rowKeys);
         }

	function invoiceGrouping(){
		require(["t360/dialog"], function(dialog) {
	 	      dialog.open("InvoiceGroupingDialogID", '<%=resMgr.getText("InvoicesOffered.GroupInvoices", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'InvoiceGrouping.jsp',
		                      ['selectedCount'], [getSelectedGridRowKeys('<%=gridId%>')], <%-- parameters --%>
		                      null, null);
		  });
	}
	}
	<%-- provide a search function that collects the necessary search parameters --%>
	  function searchInvoices() {
	    require(["dojo/dom"],
	      function(dom){
	    	alert("I am an alert box offers home");
         var invoiceStatusTypeDropdown = dijit.byId("invoiceStatusType");
         var invoiceStatus = "";
         if (dijit.byId("invoiceStatusType") != null) {
	    	   invoiceStatus = invoiceStatusTypeDropdown.attr('value');
         }
         
	    	var currency=(dijit.byId("Currency").value).toUpperCase();
	    	var amountFrom=(dom.byId("AmountFrom").value).toUpperCase();
	    	var amountTo=(dom.byId("AmountTo").value).toUpperCase();

	    	var dateFrom=(dom.byId("DateFrom").value).toUpperCase();
	    	var dateTo=(dom.byId("DateTo").value).toUpperCase();
	    	var fvDateFrom=(dom.byId("FvDateFrom").value).toUpperCase();
	    	var fvDateTo=(dom.byId("FvDateTo").value).toUpperCase();
	        
	    	<%-- var filterText=filterTextCS.toUpperCase(); --%>
	        var searchParms = "selectedStatus="+invoiceStatus+"&currency="+currency+"&amountFrom="+amountFrom
	        +"&amountTo="+amountTo+"&dateFrom="+dateFrom+"&dateTo="+dateTo+"&fvDateFrom="+fvDateFrom+"&fvDateTo="+fvDateTo;

	        console.log("SearchParms: " + searchParms);
	        searchDataGrid(gridId, dataViewName, searchParms);
	      });
	  }
	require(["dijit/registry", "dojo/query", "dojo/on", "dojo/dom-class", "t360/dialog", 
	           "dojo/dom", "dijit/layout/ContentPane", "dijit/TooltipDialog", "dijit/popup",
	           "t360/common", "t360/popup", "t360/datagrid",
	           "dojo/ready", "dojo/domReady!"],
	      function(registry, query, on, domClass, dialog,
	               dom, ContentPane, TooltipDialog, popup, 
	               common, t360popup, t360grid,
	               ready ) {
		

	    <%--register widget event handlers--%>
	    ready(function() {
	    	<%
			  if (TradePortalConstants.DISPLAY_INVOICE_OFFERED.equals(current2ndNav) || TradePortalConstants.DISPLAY_FUTURE_VALUE_OFFERS.equals(current2ndNav)){
			%>
	    	  initializeDataGrid();
	    	  <%}else {%>	
	    	initializeTranDataGrid();
	    	 <%}%>	
	      <%-- search event handlers --%>
	      var invoiceStatusType = registry.byId("invoiceStatusType");
	      if ( invoiceStatusType ) {
	    	  invoiceStatusType.on("change", function() {
	    		  searchInvoices();
	        });
	      }
	      
	
	    });
	  });
	 require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
		      function(query, on, t360grid, t360popup){

		<%
		  if (TradePortalConstants.DISPLAY_INVOICE_OFFERED.equals(current2ndNav)){
		%>


		require(["dojo/dom-class", "dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dijit/registry", "dojo/ready", "dojo/domReady!"],
				function(domClass, query, on, t360grid, t360popup, registry,ready) {
					 ready(function() {
						<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
							domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
							domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
						  if(registry.byId("invoicesOfferedGrid")){
							  var invGrid = registry.byId("invoicesOfferedGrid");			   
							  on(invGrid,"selectionChanged", function() {
								  if (registry.byId('InvoicesOffered_AcceptOffer')){
							          t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"InvoicesOffered_AcceptOffer",0);
								  }
								  if (registry.byId('InvoicesOffered_ValueDates')){
									  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"InvoicesOffered_ValueDates",0);
								  }
								  if (registry.byId('InvoicesOffered_Document')){
							          t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"InvoicesOffered_Document",0);
								  }
								  if (registry.byId('InvoicesOffered_Authorize')){
									  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"InvoicesOffered_Authorize",0);
								  }	
							 });
						 } 
					 });
				 });


		 require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
			      function(query, on, t360grid, t360popup){
			    query('#invoicesOfferedRefresh').on("click", function() {
			    	searchInvoices();
			    });
		    query('#invoicesOfferedGridEdit').on("click", function() {
		      var columns = t360grid.getColumnsForCustomization(gridId);
		      var parmNames = [ "gridId", "gridName", "columns" ];
		      var parmVals = [ gridId, gridName, columns ];
		      t360popup.open(
		        'invoicesOfferedGridEdit', 'gridCustomizationPopup.jsp',
		        parmNames, parmVals,
		        null, null); <%-- callbacks --%>
		    });
		   });


		 <%
		  }
		  if (TradePortalConstants.DISPLAY_FUTURE_VALUE_OFFERS.equals(current2ndNav)){
		%>

		  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/dom-class", "dojo/ready", "dojo/domReady!"],
			        function(query, on, t360grid, t360popup, domClass, ready){
				 <%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
				   ready(function() {	
						domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
						domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
						});
			      query('#invoicesFvdOfferedRefresh').on("click", function() {
			          searchInvoices();
			      });
			    query('#invoicesFvdOfferedGridEdit').on("click", function() {
			      var columns = t360grid.getColumnsForCustomization('invoicesFvdOfferedGrid');
			      var parmNames = [ "gridId", "gridName", "columns" ];
			      var parmVals = [ gridId, gridName, columns ];
			      t360popup.open(
			        'invoicesFvdOfferedGridEdit', 'gridCustomizationPopup.jsp',
			        parmNames, parmVals,
			        null, null);  <%-- callbacks --%>
			    });
			   });

		  <%
		  }
		%>

		  });
	  <%if (TradePortalConstants.INDICATOR_YES.equals(current2ndNav)){
		  %>
		  function searchInvoices() {
			    require(["dojo/dom"],
			      function(dom){
			    	alert("I am an alert box!");
			    	var invoiceStatus = dijit.byId("invoiceStatusType").attr('value');
			    	var invoiceID=(dom.byId("InvoiceID").value).toUpperCase();
			    	var currency=(dijit.byId("Currency").value).toUpperCase();
			    	var amountFrom=(dom.byId("AmountFrom").value).toUpperCase();
			    	var amountTo=(dom.byId("AmountTo").value).toUpperCase();
			    	var dateFrom=(dom.byId("DateFrom").value).toUpperCase();
			    	var dateTo=(dom.byId("DateTo").value).toUpperCase();
			    	var relatedInstrumentID=(dijit.byId("RelatedInstrumentID").value).toUpperCase();
			    	<%-- Rel 9.0 CR 913 START --%>
				var showInactive = "";
		            if(dom.byId("InActive")){
		                showInactive = (dom.byId("InActive").value).toUpperCase();
		            } 
		            var showInactive= dijit.byId("InActive");
		            if(showInactive){
		                if(dijit.byId("InActive").checked)
		                    showInactive="Y";
		                else
		                    showInactive="N";
		            }
				<%-- Rel 9.0 CR 913 Ends //
			        // var filterText=filterTextCS.toUpperCase(); --%>
			        var searchParms = "selectedStatus="+invoiceStatus+"&invoiceID="+invoiceID+"&currency="+currency+"&amountFrom="+amountFrom
			        +"&amountTo="+amountTo+"&dateFrom="+dateFrom+"&dateTo="+dateTo+"&relatedInstrumentID="+relatedInstrumentID+"&showInactive="+showInactive;

			        console.log("SearchParms: " + searchParms);
			        searchDataGrid(gridId, dataViewName, searchParms);
			      });
			  }
		  require(["dijit/registry", "dojo/query", "dojo/on", "dojo/dom-class", "t360/dialog", 
		           "dojo/dom", "dijit/layout/ContentPane", "dijit/TooltipDialog", "dijit/popup",
		           "t360/common", "t360/popup", "t360/datagrid",
		           "dojo/ready", "dojo/domReady!"],
		      function(registry, query, on, domClass, dialog,
		               dom, ContentPane, TooltipDialog, popup, 
		               common, t360popup, t360grid,
		               ready ) {
		

		    <%-- register widget event handlers --%>
		    ready(function() {
		    	
				<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
				domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
				domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
	    	
		      <%-- search event handlers --%>
		      var invoiceStatusType = registry.byId("invoiceStatusType");
		      if ( invoiceStatusType ) {
		    	  invoiceStatusType.on("change", function() {
		    		  searchInvoices();
		        });
		      }
		      <%-- Rel 9.0 CR 913 Starts  --%>
		      var showInactive = registry.byId("InActive");
	          if ( showInactive ) {
	              showInactive.on("change", function() {
	                     searchInvoices();
	            });
	          }
	    	    <%-- Rel 9.0 CR 913 Ends --%>
		
		    });
		  });
	  
		  <%-- Kiran  05/13/2013  Rel8.2 IR-T36000014861  Start //
		  // Added this function in order for enter key to work in firefox --%>
		  function filterInvoiceOfferHistoryOnEnter(fieldId){
				<%--
			  	// The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
			  	 --%>
			  	require(["dojo/on","dijit/registry"],function(on, registry) {
				    on(registry.byId(fieldId), "keypress", function(event) {
				        if (event && event.keyCode == 13) {
				        	dojo.stopEvent(event);
				        	searchInvoices();
				        }
				    });
				});
			} 
		  <%-- Kiran  05/13/2013  Rel8.2 IR-T36000014861  End --%>
		  
	require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	    function(query, on, t360grid, t360popup){

	 require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
		      function(query, on, t360grid, t360popup){
		    query('#invoicesOfferedRefresh').on("click", function() {
		    	searchInvoices();
		    });
	  query('#invoicesOfferedGridEdit').on("click", function() {
	    var columns = t360grid.getColumnsForCustomization(gridId);
	    var parmNames = [ "gridId", "gridName", "columns" ];
	    var parmVals = [ gridId, gridName, columns ];
	    t360popup.open(
	      'invoicesOfferedGridEdit', 'gridCustomizationPopup.jsp',
	      parmNames, parmVals,
	      null, null);  <%-- callbacks --%>
	  });
	 });

	});
	<%
	}
	%>
	
</script>
<script type="text/javascript" src="/portal/js/page/invoffershome.js"></script>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="<%=gridId%>" />
</jsp:include>

</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());

   session.setAttribute("invoiceOfferTransactions2ndNav", current2ndNav);
%>
