<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
Ravindra - Rel8200 CR-708B - Initial Version
**********************************************************************************
                        Transactions Home History Tab

  Description:
    Contains HTML to create the History tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%>

<%
      secureParms.put("UserOid",        userSession.getUserOid());
      secureParms.put("OrgOid",        userSession.getOwnerOrgOid());

      String newDropdownSearch = request.getParameter("NewDropdownSearch");
      Debug.debug("New Dropdown Search is " + newDropdownSearch);

      String searchType   = request.getParameter("SearchType");
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);

      String newSearchTrans = request.getParameter("NewSearch");

      loginLocale = userSession.getUserLocale();

    // Get buyer info from the invoice.  This seller belongs to only one buyer so all the invoices are for the same buyer.
    StringBuilder buyerSQL = new StringBuilder().append("select min(buyer_party_identifier) buyer_party_identifier from invoice")
        .append(" where a_corp_org_oid = ? and buyer_party_identifier is not null");
    DocumentHandler buyerDoc = DatabaseQueryBean.getXmlResultSet(buyerSQL.toString(), false, new Object[]{userOrgOid});
    String buyerID = "";
    if (buyerDoc!=null) {
        buyerID = buyerDoc.getAttribute("/ResultSetRecord(0)/BUYER_PARTY_IDENTIFIER");
    }


%>

<div class="gridSearch">

   <%--IR 23181 <span class="pageSubHeader"> --%>
    <span class="formItem readOnly">
       <%=resMgr.getText("InvoicesOffered.Buyer", TradePortalConstants.TEXT_BUNDLE)%>
      <%=buyerID%>
    </span>

  <div class="searchHeader">
    <span class="searchHeaderActions">
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="<%=gridId%>" />
      </jsp:include>
	  
      <span id="invoicesFvdOfferedRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("invoicesOfferedRefresh", "RefreshImageLinkHoverText") %>
      <span id="invoicesFvdOfferedGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("invoicesOfferedGridEdit","CustomizeListImageLinkHoverText") %>      
    </span>
    <div style="clear:both;"></div>
  </div>



<%
  gridHtml = dgFactory.createDataGrid("invoicesFvdOfferedGrid","InvoicesFvdOfferedDataGrid", null);
  gridLayout = dgFactory.createGridLayout("invoicesFvdOfferedGrid", "InvoicesFvdOfferedDataGrid");
  initSearchParms="";
%>
<div class="searchDivider"></div>

<%@ include file="/invoiceoffers/fragments/InvoiceOffersSearchFilter.frag" %>
</div>
<input type=hidden name=SearchType value="Y">
<%=gridHtml%>
