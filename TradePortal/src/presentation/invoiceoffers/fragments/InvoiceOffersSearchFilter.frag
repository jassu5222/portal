<%--
Ravindra - Rel8200 CR-708B - Initial Version
*******************************************************************************
                  Instrument Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
  
  IMPORTANT NOTE: This page may currently come from the following:
  
  Create Transaction Step 1 Page: for an existing instrument or copying an
  			instrument
  Export LC Issue Transfer: Search Instrument button
  MessageHome: Search Instrument
  
  Each originating page should contain the following code:
  
    <input type="hidden" name="NewSearch" value="Y">
  
  - this will allow a new search criteria to be created.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.
  
  
  
  <%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%

  String datePattern = userSession.getDatePattern();
  String dateWidgetOptions = "placeHolder:'" + datePattern + "'"; 

%>

<div class="searchDetail">
	<span class="searchCriteria">
		<%
			String options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, null, loginLocale);
  		%>
  		<%=widgetFactory.createSearchSelectField("Currency", "InvoiceSearch.Currency", " ", options,"class='char10'") %>
  		<%=widgetFactory.createSearchAmountField("AmountFrom", "InvoiceSearch.AmountFrom","","style='width:10em' class='char10'", "") %>
		<%=widgetFactory.createSearchAmountField("AmountTo", "InvoiceSearch.To","","style='width:10em' class='char10'", "") %>
	</span>
	<span class="searchActions">
		<button data-dojo-type="dijit.form.Button" type="button" id="searchParty">Search
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchInvoices();</script>
		  	</button>
		  	<%=widgetFactory.createHoverHelp("searchParty","SearchHoverText") %>
	</span>
	
	<div style="clear:both"></div>
</div>

<div class="searchDetail">
	<span class="searchCriteria">
		<%=widgetFactory.createSearchDateField("DateFrom","InvoiceSearch.DueOrPaymentDateFilterFrom","class='char10'", dateWidgetOptions) %>
		<%=widgetFactory.createSearchDateField("DateTo","InvoiceSearch.To","class='char10'", dateWidgetOptions) %>
	</span>
	<span class="searchCriteria">
		<%=widgetFactory.createSearchDateField("FvDateFrom","InvoiceSearch.FVDFrom","class='char10'", dateWidgetOptions) %>
		<%=widgetFactory.createSearchDateField("FvDateTo","InvoiceSearch.To","class='char10'", dateWidgetOptions) %>
	</span>
	<div style="clear:both"></div>
</div>
