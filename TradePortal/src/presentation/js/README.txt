For simplicity in maintenance, the /js/dojo, /js/dijit, and /js/dojox directories are not kept in source control.  We do not expect these directories to change, however when dojo is upgraded, having them exploded could potentially result in hundreds of changes.

Rather we keep the dojo-release-xxx.zip file directly downloaded from dojotoolkit.org in the code repository and during the build expand it.

If you would like to access the dojo files in src for local development, feel free to expand.  However, be sure not to check any of these files into source control.  And note that any modifications you may make will be overwritten on dist during the build.
