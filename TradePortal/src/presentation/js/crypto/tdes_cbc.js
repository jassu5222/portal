 /*----------------------------------------------------------------------------*/
 // Copyright (c) 
 // Permission to use, copy, modify, and/or distribute this software for any
 // purpose with or without fee is hereby granted, provided that the above
 // copyright notice and this permission notice appear in all copies.
 //
 // THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 // WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 // MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 // ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 // WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 // ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 // OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
/*----------------------------------------------------------------------------*/
/*
*  TDES CBC (Cipher Block Chaining) Mode for use in crypto Library
*  The TDES CBC mode is compatible with openssl TDES-xxx-cbc mode
*  using the same algorithms for key and iv creation and padding as openssl.
*
*  Depends on pidCrypt (pidcrypt.js, pidcrypt_util.js), TDES (tdes_core.js)
*  and MD5 (md5.js)
*
/*----------------------------------------------------------------------------*/

if(typeof(pidCrypt) != 'undefined' &&
   typeof(pidCrypt.TDES) != 'undefined' &&
   typeof(pidCrypt.MD5) != 'undefined')
{
  pidCrypt.TDES.CBC = function () {
    this.pidcrypt = new pidCrypt();
    this.tdes = new pidCrypt.TDES(this.pidcrypt);
    this.getOutput = function(){
      return this.pidcrypt.getOutput();
    }
    this.getAllMessages = function(lnbrk){
      return this.pidcrypt.getAllMessages(lnbrk);
    }
  }
/**
* Initialize CBC for encryption from password.
* @param  password: String
* @param  options {
*           nBits: tdes bit size (128, 192)
*         }
*/
  pidCrypt.TDES.CBC.prototype.init = function(password, options) {
    if(!options) options = {};
    var pidcrypt = this.pidcrypt;
    pidcrypt.setDefaults();
    var pObj = this.pidcrypt.getParams(); //loading defaults
    for(var o in options)
      pObj[o] = options[o];
    var k_iv = this.createKeyAndIv({password:password, salt: pObj.salt, bits: pObj.nBits});
    pObj.key = k_iv.key;
    pObj.iv = k_iv.iv;
    pObj.dataOut = '';
    pidcrypt.setParams(pObj)
    this.tdes.init();
  }

/**
* Initialize CBC for encryption from password.
* @param  dataIn: plain text
* @param  password: String
* @param  options {
*           nBits: tdes bit size (128, 192)
*         }
*/
  pidCrypt.TDES.CBC.prototype.initEncrypt = function(dataIn, password, options) {
    this.init(password,options);//call standard init
    this.pidcrypt.setParams({dataIn:dataIn, encryptIn: dataIn.toByteArray()})//setting input for encryption
  }
/**
* Initialize CBC for decryption from encrypted text (compatible with openssl).
* see thread http://thedotnet.com/nntp/300307/showpost.aspx
* @param  crypted: base64 encoded tdes encrypted text
* @param  passwd: String
* @param  options {
*           nBits: tdes bit size (128, 192),
*           UTF8: boolean, set to false when decrypting certificates,
*           A0_PAD: boolean, set to false when decrypting certificates
*         }
*/
  pidCrypt.TDES.CBC.prototype.initDecrypt = function(crypted, password, options){
    if(!options) options = {};
    var pidcrypt = this.pidcrypt;
    pidcrypt.setParams({dataIn:crypted})
    if(!password)
      pidcrypt.appendError('pidCrypt.TDES.CBC.initFromEncryption: Sorry, can not crypt or decrypt without password.\n');
    var ciphertext = crypted.decodeBase64();
    if(ciphertext.indexOf('Salted__') != 0)
      return pidcrypt.appendError('pidCrypt.TDES.CBC.initFromCrypt: Sorry, unknown encryption method.\n');
    var salt = ciphertext.substr(8,8);//extract salt from crypted text
    options.salt = salt.convertToHex();//salt is always hex string
    this.init(password,options);//call standard init
    ciphertext = ciphertext.substr(16);
    pidcrypt.setParams({decryptIn:ciphertext.toByteArray()})
  }
/**
* Init CBC En-/Decryption from given parameters.
* @param  input: plain text or base64 encrypted text
* @param  key: HEX String (16, 24 or 32 byte)
* @param  iv: HEX String (16 byte)
* @param  options {
*           salt: array of bytes (8 byte),
*           nBits: tdes bit size (64, 128, 192)
*         }
*/
  pidCrypt.TDES.CBC.prototype.initByValues = function(dataIn, key, iv, options){
    var pObj = {};
    this.init('',options);//empty password, we are setting key, iv manually
    pObj.dataIn = dataIn;
    pObj.key = key
    pObj.iv = iv
    this.pidcrypt.setParams(pObj)
  }

  pidCrypt.TDES.CBC.prototype.getAllMessages = function(lnbrk){
    return this.pidcrypt.getAllMessages(lnbrk);
  }
/**
* Creates key of length nBits and an iv form password+salt
* compatible to openssl.
* 
*
* @param  pObj {
*    password: password as String
*    [salt]: salt as String, default 8 byte random salt
*    [bits]: no of bits, default pidCrypt.params.nBits = 192
* }
*
* @return         {iv: HEX String, key: HEX String}
*/
  pidCrypt.TDES.CBC.prototype.createKeyAndIv = function(pObj){
    var pidcrypt = this.pidcrypt;
    var retObj = {};
    var count = 1;//openssl rounds
    var miter = "3";
    if(!pObj) pObj = {};
    if(!pObj.salt) {
      pObj.salt = pidcrypt.getRandomBytes(8);
      pObj.salt = byteArray2String(pObj.salt).convertToHex();
      pidcrypt.setParams({salt: pObj.salt});
    }
    var data00 = pObj.password + pObj.salt.convertFromHex();
    var hashtarget = '';
    var result = '';
    var keymaterial = [];
    var loop = 0;
    keymaterial[loop++] = data00;
    for(var j=0; j<miter; j++){
      if(j == 0)
        result = data00;   	//initialize
      else {
        hashtarget = result.convertFromHex();
        hashtarget += data00;
        result = hashtarget;
      }
      for(var c=0; c<count; c++){
        result = pidCrypt.MD5(result);
      }
      keymaterial[loop++] = result;
    }
    switch(pObj.bits){
      case 128://128 bit
        retObj.key = keymaterial[1];
        retObj.iv = keymaterial[3].substr(0,16);
        break;
      case 192://192 bit
        retObj.key = keymaterial[1] + keymaterial[1].substr(0,16);
        retObj.iv = keymaterial[3];
        break;
       default:
         pidcrypt.appendError('pidCrypt.TDES.CBC.createKeyAndIv: Sorry, only 128 and 192 bits are supported.\nBits('+typeof(pObj.bits)+') = '+pObj.bits);
    }
    return retObj;
  }
/**
* Encrypt a text using TDES encryption in CBC mode of operation
*
* one of the pidCrypt.TDES.CBC init funtions must be called before execution
*
* @param  byteArray: text to encrypt as array of bytes
*
* @return tdes-cbc encrypted text
*/
  pidCrypt.TDES.CBC.prototype.encryptRaw = function(byteArray) {
    var pidcrypt = this.pidcrypt;
    var tdes = this.tdes;
    var p = pidcrypt.getParams(); //get parameters for operation set by init
    if(!byteArray)
      byteArray = p.encryptIn;
    pidcrypt.setParams({encryptIn: byteArray});
    if(!p.dataIn) pidcrypt.setParams({dataIn:byteArray});
    var iv = p.iv.convertFromHex();
    //PKCS5 paddding
    var charDiv = p.blockSize - ((byteArray.length) % p.blockSize);
    // OpenSSL Hack
    if(p.A0_PAD)
    {
      charDiv = p.blockSize - ((byteArray.length+1) % p.blockSize);
      byteArray[byteArray.length] = 10
    }
    for(var c=0;c<charDiv;c++) byteArray[byteArray.length] = charDiv;
    var nBytes = Math.floor(p.nBits/8);  // nr of bytes in key
    var keyBytes = new Array(nBytes);
    var key = p.key.convertFromHex();
    for (var i=0; i<nBytes; i++) {
      keyBytes[i] = isNaN(key.charCodeAt(i)) ? 0 : key.charCodeAt(i);
    }
    // generate key schedule
    var keySchedule = tdes.expandKey(keyBytes);
    var blockCount = Math.ceil(byteArray.length/p.blockSize);
    var ciphertxt = new Array(blockCount);  // ciphertext as array of strings
    var textBlock = [];
    var state = iv.toByteArray();
    for (var b=0; b<blockCount; b++) {
      // XOR last block and next data block, then encrypt that
      textBlock = byteArray.slice(b*p.blockSize, b*p.blockSize+p.blockSize);
      state = tdes.xOr_Array(state, textBlock);
      state = tdes.encrypt(state.slice(), keySchedule);  // -- encrypt block --
      ciphertxt[b] = byteArray2String(state);
    }
    var ciphertext = ciphertxt.join('');
    pidcrypt.setParams({dataOut:ciphertext, encryptOut:ciphertext});

    //remove all parameters from enviroment for more security is debug off
    if(!pidcrypt.isDebug() && pidcrypt.clear) pidcrypt.clearParams();
   return ciphertext || '';
  }


/**
* Encrypt a text using TDES encryption in CBC mode of operation
*  - see http://csrc.nist.gov/publications/nistpubs/800-38a/sp800-38a.pdf
*
* Unicode multi-byte character safe
*
* one of the pidCrypt.TDES.CBC init funtions must be called before execution
*
* @param  plaintext: text to encrypt
*
* @return tdes-cbc encrypted text openssl compatible
*/
 pidCrypt.TDES.CBC.prototype.encrypt = function(plaintext) {
    var pidcrypt = this.pidcrypt;
    var salt = '';
    var p = pidcrypt.getParams(); //get parameters for operation set by init
    if(!plaintext)
      plaintext = p.dataIn;
    if(p.UTF8)
      plaintext = plaintext.encodeUTF8();
    pidcrypt.setParams({dataIn:plaintext, encryptIn: plaintext.toByteArray()});
    var ciphertext = this.encryptRaw()
    salt = 'Salted__' + p.salt.convertFromHex();
    ciphertext = salt  + ciphertext;
    ciphertext = ciphertext.encodeBase64();  // encode in base64
    pidcrypt.setParams({dataOut:ciphertext});
    //remove all parameters from enviroment for more security is debug off
    if(!pidcrypt.isDebug() && pidcrypt.clear) pidcrypt.clearParams();

    return ciphertext || '';
  }

/**
* Encrypt a text using TDES encryption in CBC mode of operation
*  - see http://csrc.nist.gov/publications/nistpubs/800-38a/sp800-38a.pdf
*
* Unicode multi-byte character safe
*
* @param  dataIn: plain text
* @param  password: String
* @param  options {
*           nBits: tdes bit size (128, 192)
*         }
*
* @param  plaintext: text to encrypt
*
* @return tdes-cbc encrypted text openssl compatible
*
*/
  pidCrypt.TDES.CBC.prototype.encryptText = function(dataIn,password,options) {
   this.initEncrypt(dataIn, password, options);
   return this.encrypt();
  }



/**
* Decrypt a text encrypted by TDES in CBC mode of operation
*
* one of the pidCrypt.TDES.CBC init funtions must be called before execution
*
* @param  byteArray: tdes-cbc encrypted text as array of bytes
* 
* @return           decrypted text as String
*/
pidCrypt.TDES.CBC.prototype.decryptRaw = function(byteArray) {
    var tdes = this.tdes;
    var pidcrypt = this.pidcrypt;
    var p = pidcrypt.getParams(); //get parameters for operation set by init
    if(!byteArray)
      byteArray = p.decryptIn;
    pidcrypt.setParams({decryptIn: byteArray});
    if(!p.dataIn) pidcrypt.setParams({dataIn:byteArray});
    if((p.iv.length/2)<p.blockSize)
      return pidcrypt.appendError('pidCrypt.TDES.CBC.decrypt: Sorry, can not decrypt without complete set of parameters.\n Length of key,iv:'+p.key.length+','+p.iv.length);
    var iv = p.iv.convertFromHex();
    if(byteArray.length%p.blockSize != 0)
      return pidcrypt.appendError('pidCrypt.TDES.CBC.decrypt: Sorry, the encrypted text has the wrong length for tdes-cbc mode\n Length of ciphertext:'+byteArray.length+byteArray.length%p.blockSize);
    var nBytes = Math.floor(p.nBits/8);  // nr of bytes in key
    var keyBytes = new Array(nBytes);
    var key = p.key.convertFromHex();
    for (var i=0; i<nBytes; i++) {
      keyBytes[i] = isNaN(key.charCodeAt(i)) ? 0 : key.charCodeAt(i);
    }
    // generate key schedule
    var keySchedule = tdes.expandKey(keyBytes);
    // separate byteArray into blocks
    var nBlocks = Math.ceil((byteArray.length) / p.blockSize);
    // plaintext will get generated block-by-block into array of block-length strings
    var plaintxt = new Array(nBlocks.length);
    var state = iv.toByteArray();
    var ciphertextBlock = [];
    var dec_state = [];
    for (var b=0; b<nBlocks; b++) {
      ciphertextBlock = byteArray.slice(b*p.blockSize, b*p.blockSize+p.blockSize);
      dec_state = tdes.decrypt(ciphertextBlock, keySchedule);  // decrypt ciphertext block
      plaintxt[b] = byteArray2String(tdes.xOr_Array(state, dec_state));
      state = ciphertextBlock.slice(); //save old ciphertext for next round
    }
    
    // join array of blocks into single plaintext string and return it
    var plaintext = plaintxt.join('');
    if(pidcrypt.isDebug()) pidcrypt.appendDebug('Padding after decryption:'+ plaintext.convertToHex() + ':' + plaintext.length + '\n');
    var endByte = plaintext.charCodeAt(plaintext.length-1);
    //remove OpenSSL A0 padding eg. 0A05050505
    if(p.A0_PAD){
        plaintext = plaintext.substr(0,plaintext.length-(endByte+1));
    }
    else {
      var div = plaintext.length - (plaintext.length-endByte);
      var firstPadByte = plaintext.charCodeAt(plaintext.length-endByte);
      if(endByte == firstPadByte && endByte == div)
        plaintext = plaintext.substr(0,plaintext.length-endByte);
    }
    pidcrypt.setParams({dataOut: plaintext,decryptOut: plaintext});

    //remove all parameters from enviroment for more security is debug off
    if(!pidcrypt.isDebug() && pidcrypt.clear) pidcrypt.clearParams();

   return plaintext || '';
  }

/**
* Decrypt a base64 encoded text encrypted by TDES in CBC mode of operation
* and removes padding from decrypted text
*
* one of the pidCrypt.TDES.CBC init funtions must be called before execution
*
* @param  ciphertext: base64 encoded and tdes-cbc encrypted text
*
* @return           decrypted text as String
*/
  pidCrypt.TDES.CBC.prototype.decrypt = function(ciphertext) {
    var pidcrypt = this.pidcrypt;
    var p = pidcrypt.getParams(); //get parameters for operation set by init
    if(ciphertext)
      pidcrypt.setParams({dataIn:ciphertext});
    if(!p.decryptIn) {
      var decryptIn = p.dataIn.decodeBase64();
      if(decryptIn.indexOf('Salted__') == 0) decryptIn = decryptIn.substr(16);
      pidcrypt.setParams({decryptIn: decryptIn.toByteArray()});
    }
    var plaintext = this.decryptRaw();
    if(p.UTF8)
      plaintext = plaintext.decodeUTF8();  // decode from UTF8 back to Unicode multi-byte chars
    if(pidcrypt.isDebug()) pidcrypt.appendDebug('Removed Padding after decryption:'+ plaintext.convertToHex() + ':' + plaintext.length + '\n');
    pidcrypt.setParams({dataOut:plaintext});

    //remove all parameters from enviroment for more security is debug off
    if(!pidcrypt.isDebug() && pidcrypt.clear) pidcrypt.clearParams();
    return plaintext || '';
  }

/**
* Decrypt a base64 encoded text encrypted by TDES in CBC mode of operation
* and removes padding from decrypted text
*
* one of the pidCrypt.TDES.CBC init funtions must be called before execution
*
* @param  dataIn: base64 encoded tdes encrypted text
* @param  password: String
* @param  options {
*           nBits: tdes bit size (128, 192),
*           UTF8: boolean, set to false when decrypting certificates,
*           A0_PAD: boolean, set to false when decrypting certificates
*         }
*
* @return           decrypted text as String
*/
   pidCrypt.TDES.CBC.prototype.decryptText = function(dataIn, password, options) {
     this.initDecrypt(dataIn, password, options);
     return this.decrypt();
   }

}

