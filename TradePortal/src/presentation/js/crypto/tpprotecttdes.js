 /*----------------------------------------------------------------------------*/
 // Copyright (c) 2009 Australia and New Zealand Banking Group Limited
 // 
 // THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 // WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 // MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 // ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 // WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 // ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 // OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
/*----------------------------------------------------------------------------*/
/*
*  TP Protect client encryption library that can be used to encrypt
*  any string to used with the server using an asymmetric algorithm.
*
*  Depends on pidCrypt (pidcrypt.js, pidcrypt_util.js), TDES (tdes_core.js, tdes_cbc.js)
*  MD5 (md5.js), ASN1 (asn1.js), BigInteger (jsbn.js), Random (rng.js, prng.js),
*  and RSA (rsa.js)
/*----------------------------------------------------------------------------*/

if(typeof(pidCrypt) != 'undefined' &&
   typeof(pidCrypt.TDES) != 'undefined' &&
   typeof(pidCrypt.MD5) != 'undefined' &&
   typeof(BigInteger) != 'undefined' &&
   typeof(SecureRandom) != 'undefined' &&
   typeof(Arcfour) != 'undefined' &&
   typeof(pidCrypt.RSA) != 'undefined')
{
    
    TPProtect = function() {
    }
    
    /**
    * protect the data provided using the key provided
    * @param  options {
    *           publicKey: base64 
    *           usePem: true | false indicates if public key is PEM encoded
    *           iv: initialiation Vector in Base64
    *           bits: bits of TDES key
    *           data: data to encode (must be a string)
    *         }
    */
    TPProtect.prototype.Encrypt = function (options) {
    
        // Load the RSA Public Key
        var publicKey = "MIGJAoGBALOqBQtpyyzLLs3bN43RZkAlPyPSWwkSy3rJsQbDKmfIxhrnrxxHCrG7TBeAMMhR+VyVqQrQB7uNw+hpjHRMRuBQNffyH5WRP/gf43VJcnk7zCG4R9hqkaNve46YPQkeX1Gm0wRqTOc4HstElz5UM+HSz6JNWvNP4LCcJTRSb0YhAgMBAAE=";
        if (options.publicKey)
            if (options.usePem) 
                publicKey = this.pemParser(options.publicKey).b64;
            else
                publicKey = options.publicKey;
        var key = publicKey.decodeBase64();
        var rsa = new pidCrypt.RSA();
        var asn = pidCrypt.ASN1.decode(key.toByteArray());
        var tree = asn.toHexTree();
        rsa.setPublicKeyFromASN(tree);

        // Generate a symmetric key
        var tdes = new pidCrypt.TDES.CBC();
        var rng = new SecureRandom();
        var bits = 192;
        if (options.bits)
            bits = options.bits;
        var keyBytes = new Array();
        keyBytes[(bits >> 3) - 1] = 0;
        rng.nextBytes(keyBytes);
        var key = byteArray2String(keyBytes).convertToHex();

        // Prepare the IV provided
        var iv = "";  // dummy sample
        if (options.iv)
            iv = options.iv;
        else {
            var ivBytes = new Array();
            ivBytes[16 - 1] = 0;
            rng.nextBytes(ivBytes);
            iv = byteArray2String(ivBytes).convertToHex();
        }
        // trim the IV
        iv = (iv.decodeBase64().convertToHex() + "00000000000000000000000000000000" ).substring(0,16);

        // Encrypt the data
        var utf8Data = options.data.encodeUTF8();
        tdes.initByValues('', key, iv, {nBits:bits,A0_PAD:false,UTF8:true});
        var encryptedData = tdes.encryptRaw(utf8Data.toByteArray());

        // Encrypt the key
        var encryptedKey = rsa.encryptRaw(key.convertFromHex());

        // Format the response data
        var encodedOutput = "ENC|" + iv.convertFromHex().encodeBase64() + "|" + encryptedKey.encodeBase64() + "|" + encryptedData.encodeBase64();
        
        return encodedOutput;
    }
    
    /** 
    * Load the Base64, PEM wrapped blob into a binary string
    *
    * @param pem: PEM encoded, unencrypted blob
    */
    TPProtect.prototype.pemParser = function (pem) {
        var lines = pem.split('\n');
        var read = false;
        var b64 = false;
        var end = false;
        var flag = '';
        var retObj = {};
        retObj.info = '';
        retObj.salt = '';
        retObj.iv;
        retObj.b64 = '';
        retObj.tdes = false;
        retObj.mode = '';
        retObj.bits = 0;
        for(var i=0; i < lines.length; i++) {
            flag = lines[i].substr(0,9);
            if(i==1 && flag.indexOf('M') == 0)  // ASN1 Sequence as first element
                b64 = true;
            switch(flag) {
                case '-----BEGI':
                    read = true;
                    break;
                case '':
                    if(read) b64 = true;
                    break;
                case '-----END ':
                    if(read) {
                        b64 = false;
                        read = false;
                    }
                    break;
                default:
                    if(read && b64)
                        retObj.b64 += lines[i].stripLineFeeds();
            }
        }
        return retObj;
    }
}

