
//library for all of the corporate customer detail page functions
// this is 2nd choice becuase we'd prefer having eventhandlers and use dojo asynch to load
// todo: make all event handlers

//load everything once
  require(["dojo/_base/array", "dojo/_base/xhr", "dojo/dom", "dojo/dom-class", "dojo/dom-construct","dojo/dom-style","dojo/query",
           "dijit/registry", "dojo/parser", "t360/common", "t360/dialog", "dojo/store/Memory","dojo/ready"], 
      function(baseArray, xhr, dom,  domClass, domConstruct, domStyle, query,
               registry, parser, common, dialog, Memory, ready ) {

    local.deleteAddressRow = function(){
      var myGrid = registry.byId("CorporateAddressDataGridId");
      if(selectedAddrRowKey!=null && selectedAddrRowKey!=""){

        myGrid.store.fetchItemByIdentity({ 'rowKey' : selectedAddrRowKey,  onItem :  function (item ) {
          if(item == null) {
            console.log(item+"not found");
          } else {
            myGrid.removeSelectedRows();
            //add new hidden fields section, but with just an address oid.
            //this signifies to the mediator that the address should be deleted
            local.addAddressHiddenFields("SecAddressOid",selectedAddrRowKey);
          }
        }});
      }
      else if(selectedAddrDivId!=null && selectedAddrDivId!=""){

        /* myGrid.store.fetchItemByIdentity({ 'divId' : selectedAddrDivId,  onItem :  function (item ) {
          if(item == null) {
            console.log(item+"not found");
          } else {
            myGrid.removeSelectedRows();
          }
        }}); */
        myGrid.removeSelectedRows();
        // Destroy node
        domConstruct.destroy(selectedAddrDivId);
      }
      myGrid.store.save();
    }; 

    //updateAddress callback
    local.updateAddress = function(Namearr,Valuearr) {
      var myGrid = registry.byId("CorporateAddressDataGridId");

      var myDivId = Valuearr[10];
      if ( myDivId!=null && myDivId!="" ) {
        myGrid.store.fetch({query: {divId: myDivId},
          onItem : function (item ) {
            myGrid.store.setValue(item, 'SeqNo', Valuearr[5]);
            myGrid.store.setValue(item, 'Name', Valuearr[7]);
            myGrid.store.setValue(item, 'AddressLine1', Valuearr[1]);
            myGrid.store.setValue(item, 'AddressLine2', Valuearr[2]);
            myGrid.store.setValue(item, 'City', Valuearr[8]);
            myGrid.store.setValue(item, 'StateProvince', Valuearr[6]);
            myGrid.store.setValue(item, 'Country', Valuearr[3]);
            myGrid.store.setValue(item, 'PostalCode', Valuearr[4]);
          }
        });
      }
      myGrid.store.save();

      var myDivIdIdx = myDivId.substring("addressHidden".length);
      var myInput = dom.byId("SecAddressSeqNum"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[5];
      myInput = dom.byId("SecName"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[7];
      myInput = dom.byId("SecAddressLine1"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[1];
      myInput = dom.byId("SecAddressLine2"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[2];
      myInput = dom.byId("SecCity"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[8];
      myInput = dom.byId("SecStateProvince"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[6];
      myInput = dom.byId("SecPostalCode"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[4];
      myInput = dom.byId("SecCountry"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[3];
      myInput = dom.byId("SecUserDefinedField1"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[11];
      myInput = dom.byId("SecUserDefinedField2"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[12];
      myInput = dom.byId("SecUserDefinedField3"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[13];
      myInput = dom.byId("SecUserDefinedField4"+myDivIdIdx);
      if ( myInput ) myInput.value = Valuearr[14];
    };

    //addAddress callback
    local.addAddress = function(Namearr,Valuearr) {
      var myGrid = registry.byId("CorporateAddressDataGridId");
      myGrid.store.newItem({rowKey:"", divId:"addressHidden"+addressCount, Name: Valuearr[7], SeqNo: Valuearr[5], AddressLine1: Valuearr[1],AddressLine2: Valuearr[2], City: Valuearr[8], StateProvince: Valuearr[6], Country: Valuearr[3], PostalCode: Valuearr[4]});
      myGrid.store.save();
      local.addAddressHiddenFields(Namearr,Valuearr);

      //we know we have an address now, make update address button visible
      query('#addressGridActions').forEach( function(entry, i) {
        domStyle.set(entry,'display','block');
      });

    };

    local.addAddressHiddenFields = function(parmName, parmValue) {
      var hiddenDiv = document.createElement("div");
      hiddenDiv.setAttribute('id',"addressHidden"+addressCount);
      if (parmName instanceof Array && parmValue instanceof Array) {
        for(var i=0;i<parmName.length;i++) {
          if (parmName[i]!="" ) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("id", parmName[i]+addressCount);
            hiddenField.setAttribute("name", parmName[i]+addressCount);
            hiddenField.setAttribute("value", parmValue[i]);
            hiddenDiv.appendChild(hiddenField);
          }
        }
      }
      else {
        if (parmName!="") {
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("id", parmName+addressCount);
          hiddenField.setAttribute("name", parmName+addressCount);
          hiddenField.setAttribute("value", parmValue);
          hiddenDiv.appendChild(hiddenField);
        }
      }
      var addrHiddenFields = dom.byId("addressHiddenFields");
      addrHiddenFields.appendChild(hiddenDiv);
      addressCount++;
      var noOfAddrRows = dom.byId("noOfAddrRows");
      noOfAddrRows.value = addressCount;
    };

    local.addNewAccount = function() {

      var acctGrid = registry.byId("acctGrid");
      var beforeRowCount = acctGrid.rowCount;

      var acctOidEdit = dom.byId("acctOidEdit").value;
      var acctNumEdit = registry.byId("acctNumEdit").value;
      var acctNameEdit = registry.byId("acctNameEdit").value;  
      var acctCurrEdit = registry.byId("acctCurrEdit").value;
      var acctTypeEdit = registry.byId("acctTypeEdit").value;
      var bankCountryCodeEdit = registry.byId("bankCountryCodeEdit").value;
      var regulatoryAccountTypeEdit = registry.byId("regulatoryAccountTypeEdit").value;
      var sourceSystemBranchEdit = registry.byId("sourceSystemBranchEdit").value;
      var fxRateGroupWidget = registry.byId("fxRateGroupFEdit"); // DK IR T36000017932 Rel8.2 06/05/2013
    //var pagIdSelectionEdit =  registry.byId("pagIdSelectionEdit").get('value');
      var pagIdSelectionEdit1 = (registry.byId("pagIdSelectionEdit1") != null)? (registry.byId("pagIdSelectionEdit1").get('value')):"";
      var pagIdSelectionEdit2 = (registry.byId("pagIdSelectionEdit2") != null)? (registry.byId("pagIdSelectionEdit2").get('value')):"";
      var pagIdSelectionEdit3 = (registry.byId("pagIdSelectionEdit3") != null)? (registry.byId("pagIdSelectionEdit3").get('value')):"";
      var pagIdSelectionEdit4 = (registry.byId("pagIdSelectionEdit4") != null)? (registry.byId("pagIdSelectionEdit4").get('value')):"";
      var pagIdSelectionEdit5 = (registry.byId("pagIdSelectionEdit5") != null)? (registry.byId("pagIdSelectionEdit5").get('value')):"";
      var pagIdSelectionEdit6 = (registry.byId("pagIdSelectionEdit6") != null)? (registry.byId("pagIdSelectionEdit6").get('value')):"";
      var pagIdSelectionEdit7 = (registry.byId("pagIdSelectionEdit7") != null)? (registry.byId("pagIdSelectionEdit7").get('value')):"";
      var pagIdSelectionEdit8 = (registry.byId("pagIdSelectionEdit8") != null)? (registry.byId("pagIdSelectionEdit8").get('value')):"";
      var pagIdSelectionEdit9 = (registry.byId("pagIdSelectionEdit9") != null)? (registry.byId("pagIdSelectionEdit9").get('value')):"";
      var pagIdSelectionEdit10 = (registry.byId("pagIdSelectionEdit10") != null)? (registry.byId("pagIdSelectionEdit10").get('value')):"";
      var pagIdSelectionEdit11 = (registry.byId("pagIdSelectionEdit11") != null)? (registry.byId("pagIdSelectionEdit11").get('value')):"";
      var pagIdSelectionEdit12 = (registry.byId("pagIdSelectionEdit12") != null)? (registry.byId("pagIdSelectionEdit12").get('value')):"";
      var pagIdSelectionEdit13 = (registry.byId("pagIdSelectionEdit13") != null)? (registry.byId("pagIdSelectionEdit13").get('value')):"";
      var pagIdSelectionEdit14 = (registry.byId("pagIdSelectionEdit14") != null)? (registry.byId("pagIdSelectionEdit14").get('value')):"";
      var pagIdSelectionEdit15 = (registry.byId("pagIdSelectionEdit15") != null)? (registry.byId("pagIdSelectionEdit15").get('value')):"";
      var pagIdSelectionEdit16 = (registry.byId("pagIdSelectionEdit16") != null)? (registry.byId("pagIdSelectionEdit16").get('value')):"";
      
      
      var PropCustomerWidget = registry.byId("PropCustomerIdEdit"); // DK IR T36000017932 Rel8.2 06/05/2013
      var opBankOrgIdWidget = registry.byId("opBankOrgIdEdit");
      var opBankOrgIdEdit = '';
      var fxRateGroupFEdit = ''; // DK IR T36000017932 Rel8.2 06/05/2013
      var PropCustomerIdEdit = ''; // DK IR T36000017932 Rel8.2 06/05/2013
      var opBankOrgIdEditDisplayedVal = '';
      if ( opBankOrgIdWidget ) {
        opBankOrgIdEdit = opBankOrgIdWidget.value;
        opBankOrgIdEditDisplayedVal = opBankOrgIdWidget.attr('displayedValue');
      }
      // DK IR T36000017932 Rel8.2 06/05/2013 starts
      if(fxRateGroupWidget){
    	  fxRateGroupFEdit = fxRateGroupWidget.value;
      }
      if(PropCustomerWidget){
    	  PropCustomerIdEdit = PropCustomerWidget.value;
      }
      //  DK IR T36000017932 Rel8.2 06/05/2013 ends
      var availableForDirectDebitEdit = registry.byId("availableForDirectDebitEdit").checked==true?'Y':'N';
      var availableForIntPmtEdit = registry.byId("availableForIntPmtEdit").checked==true?'Y':'N';
      var availableForTBAEdit = registry.byId("availableForTBAEdit").checked==true?'Y':'N';
      var availableForDomesticPymtEdit = registry.byId("availableForDomesticPymtEdit").checked==true?'Y':'N';
      var availableForSettlementInstrEdit = registry.byId("availableForSettlementInstrEdit").checked==true?'Y':'N';
      var availableForLoanReqEdit = registry.byId("availableForLoanReqEdit").checked==true?'Y':'N';
      var deactivateIndicatorEdit = registry.byId("deactivateIndicatorEdit").checked==true?'Y':'N';
                  	  
      acctGrid.store.newItem({rowKey:"",divId:"accountHidden"+accountCount, ACCOUNT_NUMBER: acctNumEdit, ACCOUNT_TYPE: acctTypeEdit, ACCOUNT_NAME: acctNameEdit, BANK_COUNTRY_CODE: bankCountryCodeEdit, CURRENCY: acctCurrEdit, SOURCE_SYSTEM_BRANCH: sourceSystemBranchEdit, OTHERCORP_CUSTOMER_INDICATOR: 'N', REGULATORY_ACCOUNT_TYPE: regulatoryAccountTypeEdit });
      acctGrid.store.save();

      var namesArray = new Array();
      var i = 1;
      namesArray[i++] = "acctOid";
      namesArray[i++] = "acctNum";
      namesArray[i++] = "acctName";
      namesArray[i++] = "acctType";
      namesArray[i++] = "fxRateGroup";
      namesArray[i++] = "sourceSystemBranch";
      namesArray[i++] = "PropCustomerId";
      namesArray[i++] = "availableForDirectDebit";
      namesArray[i++] = "availableForIntPmt";
      namesArray[i++] = "availableForTBA";
      namesArray[i++] = "availableForDomesticPymt";
      namesArray[i++] = "availableForSettlementInstr";
      namesArray[i++] = "availableForLoanReq";
      namesArray[i++] = "deactivateIndicator";
      namesArray[i++] = "opBankOrgId";
      namesArray[i++] = "bankCountryCode";
      namesArray[i++] = "acctCurr";
      //namesArray[i++] = "pagIdSelection";
      namesArray[i++] = "otherCorpsAccount";
      namesArray[i++] = "pagIdSelection1_";
      namesArray[i++] = "pagIdSelection2_";
      namesArray[i++] = "pagIdSelection3_";
      namesArray[i++] = "pagIdSelection4_";
      namesArray[i++] = "pagIdSelection5_";
      namesArray[i++] = "pagIdSelection6_";
      namesArray[i++] = "pagIdSelection7_";
      namesArray[i++] = "pagIdSelection8_";
      namesArray[i++] = "pagIdSelection9_";
      namesArray[i++] = "pagIdSelection10_";
      namesArray[i++] = "pagIdSelection11_";
      namesArray[i++] = "pagIdSelection12_";
      namesArray[i++] = "pagIdSelection13_";
      namesArray[i++] = "pagIdSelection14_";
      namesArray[i++] = "pagIdSelection15_";
      namesArray[i++] = "pagIdSelection16_";
      namesArray[i++] = "regulatoryAccountType";            	
      var valuesArray = new Array();
      var i = 1;
      valuesArray[i++] = "";
      valuesArray[i++] = acctNumEdit;
      valuesArray[i++] = acctNameEdit;
      valuesArray[i++] = acctTypeEdit;
      valuesArray[i++] = fxRateGroupFEdit;
      valuesArray[i++] = sourceSystemBranchEdit;
      valuesArray[i++] = PropCustomerIdEdit;
      valuesArray[i++] = availableForDirectDebitEdit;
      valuesArray[i++] = availableForIntPmtEdit;
      valuesArray[i++] = availableForTBAEdit;
      valuesArray[i++] = availableForDomesticPymtEdit;
      valuesArray[i++] = availableForSettlementInstrEdit;
      valuesArray[i++] = availableForLoanReqEdit;
      valuesArray[i++] = deactivateIndicatorEdit;
      valuesArray[i++] = opBankOrgIdEdit;
      valuesArray[i++] = bankCountryCodeEdit;
      valuesArray[i++] = acctCurrEdit;
      //valuesArray[i++] = pagIdSelectionEdit;
      valuesArray[i++] = "N";
      valuesArray[i++] = pagIdSelectionEdit1;
      valuesArray[i++] = pagIdSelectionEdit2;
      valuesArray[i++] = pagIdSelectionEdit3;
      valuesArray[i++] = pagIdSelectionEdit4;
      valuesArray[i++] = pagIdSelectionEdit5;
      valuesArray[i++] = pagIdSelectionEdit6;
      valuesArray[i++] = pagIdSelectionEdit7;
      valuesArray[i++] = pagIdSelectionEdit8;
      valuesArray[i++] = pagIdSelectionEdit9;
      valuesArray[i++] = pagIdSelectionEdit10;
      valuesArray[i++] = pagIdSelectionEdit11;
      valuesArray[i++] = pagIdSelectionEdit12;
      valuesArray[i++] = pagIdSelectionEdit13;
      valuesArray[i++] = pagIdSelectionEdit14;
      valuesArray[i++] = pagIdSelectionEdit15;
      valuesArray[i++] = pagIdSelectionEdit16;
      valuesArray[i++] = regulatoryAccountTypeEdit;                
      local.addAccountHiddenFields('accountHidden'+accountCount, namesArray, valuesArray, accountCount);
      accountCount++;
      var noOfRows = dom.byId("noOfRows");
      noOfRows.value = accountCount;
     
      //now select the added account so update becomes active
      //think grid adding is asynchronous so not sure how reliable this is...
      var lastAcctIdx = beforeRowCount;
      acctGrid.selection.setSelected(lastAcctIdx, true);
    };
                
    local.updateAccount = function() {

      var acctOidEdit      =   dom.byId("acctOidEdit").value;
      var acctNumEdit	= dom.byId("acctNumEdit").value;
      var acctNameEdit  =  dom.byId("acctNameEdit").value;  
      var acctTypeEdit =  dom.byId("acctTypeEdit").value;
      var fxRateGroupFWidget =  dom.byId("fxRateGroupFEdit");  // DK IR T36000017932 Rel8.2 06/05/2013
      var sourceSystemBranchEdit =  dom.byId("sourceSystemBranchEdit").value;
      var PropCustomerIdWidget =  dom.byId("PropCustomerIdEdit"); // DK IR T36000017932 Rel8.2 06/05/2013
      var availableForDirectDebitEdit =  registry.byId("availableForDirectDebitEdit").checked==true?'Y':'N';
      var availableForIntPmtEdit =  registry.byId("availableForIntPmtEdit").checked==true?'Y':'N';
      var availableForTBAEdit =  registry.byId("availableForTBAEdit").checked==true?'Y':'N';
      var availableForDomesticPymtEdit =  registry.byId("availableForDomesticPymtEdit").checked==true?'Y':'N';
      var availableForSettlementInstrEdit =  registry.byId("availableForSettlementInstrEdit").checked==true?'Y':'N';
      var availableForLoanReqEdit =  registry.byId("availableForLoanReqEdit").checked==true?'Y':'N';
      var deactivateIndicatorEdit =  registry.byId("deactivateIndicatorEdit").checked==true?'Y':'N';
      var otherCorpsAccountEdit =  dom.byId("ValOtherCorpsAccount").value == 'Y' ? 'Y' : 'N';
      var otherAccountOwnerOidEdit =  dom.byId("ValOtherAccountOwnerOid").value;
      
      //Drop downs
      var opBankOrgIdWidget = registry.byId("opBankOrgIdEdit");
      var opBankOrgIdEdit = '';
      var fxRateGroupFEdit =  '';  // DK IR T36000017932 Rel8.2 06/05/2013
      var PropCustomerIdEdit =  '';  // DK IR T36000017932 Rel8.2 06/05/2013
      if ( opBankOrgIdWidget ) {
        opBankOrgIdEdit =  opBankOrgIdWidget.value;
      }
      // DK IR T36000017932 Rel8.2 06/05/2013 starts
      if ( fxRateGroupFWidget ) {
    	  fxRateGroupFEdit =  fxRateGroupFWidget.value;
        }
      if ( PropCustomerIdWidget ) {
    	  PropCustomerIdEdit =  PropCustomerIdWidget.value;
        }
      // DK IR T36000017932 Rel8.2 06/05/2013 ends
      var bankCountryCodeEdit =  dom.byId("bankCountryCodeEdit").value;
      var regulatoryAccountTypeEdit =  dom.byId("regulatoryAccountTypeEdit").value;
      var acctCurrEdit =  dom.byId("acctCurrEdit").value;
      //var pagIdSelectionEdit =  registry.byId("pagIdSelectionEdit").get('value');
      var pagIdSelectionEdit1 = (registry.byId("pagIdSelectionEdit1") != null)? (registry.byId("pagIdSelectionEdit1").get('value')):"";
      var pagIdSelectionEdit2 = (registry.byId("pagIdSelectionEdit2") != null)? (registry.byId("pagIdSelectionEdit2").get('value')):"";
      var pagIdSelectionEdit3 = (registry.byId("pagIdSelectionEdit3") != null)? (registry.byId("pagIdSelectionEdit3").get('value')):"";
      var pagIdSelectionEdit4 = (registry.byId("pagIdSelectionEdit4") != null)? (registry.byId("pagIdSelectionEdit4").get('value')):"";
      var pagIdSelectionEdit5 = (registry.byId("pagIdSelectionEdit5") != null)? (registry.byId("pagIdSelectionEdit5").get('value')):"";
      var pagIdSelectionEdit6 = (registry.byId("pagIdSelectionEdit6") != null)? (registry.byId("pagIdSelectionEdit6").get('value')):"";
      var pagIdSelectionEdit7 = (registry.byId("pagIdSelectionEdit7") != null)? (registry.byId("pagIdSelectionEdit7").get('value')):"";
      var pagIdSelectionEdit8 = (registry.byId("pagIdSelectionEdit8") != null)? (registry.byId("pagIdSelectionEdit8").get('value')):"";
      var pagIdSelectionEdit9 = (registry.byId("pagIdSelectionEdit9") != null)? (registry.byId("pagIdSelectionEdit9").get('value')):"";
      var pagIdSelectionEdit10 = (registry.byId("pagIdSelectionEdit10") != null)? (registry.byId("pagIdSelectionEdit10").get('value')):"";
      var pagIdSelectionEdit11 = (registry.byId("pagIdSelectionEdit11") != null)? (registry.byId("pagIdSelectionEdit11").get('value')):"";
      var pagIdSelectionEdit12 = (registry.byId("pagIdSelectionEdit12") != null)? (registry.byId("pagIdSelectionEdit12").get('value')):"";
      var pagIdSelectionEdit13 = (registry.byId("pagIdSelectionEdit13") != null)? (registry.byId("pagIdSelectionEdit13").get('value')):"";
      var pagIdSelectionEdit14 = (registry.byId("pagIdSelectionEdit14") != null)? (registry.byId("pagIdSelectionEdit14").get('value')):"";
      var pagIdSelectionEdit15 = (registry.byId("pagIdSelectionEdit15") != null)? (registry.byId("pagIdSelectionEdit15").get('value')):"";
      var pagIdSelectionEdit16 = (registry.byId("pagIdSelectionEdit16") != null)? (registry.byId("pagIdSelectionEdit16").get('value')):"";
      

      console.log("opBankOrgIdEdit "+opBankOrgIdEdit);
     
      var acctGrid = registry.byId("acctGrid");
      if(selectedAcctRowKey !=null && selectedAcctRowKey.length>0){
        acctGrid.store.fetch({query : {rowKey : selectedAcctRowKey},
          onItem : function (item ) {
            acctGrid.store.setValue(item, 'ACCOUNT_NUMBER', acctNumEdit);
            acctGrid.store.setValue(item, 'ACCOUNT_TYPE', acctTypeEdit);
            acctGrid.store.setValue(item, 'ACCOUNT_NAME', acctNameEdit);
            acctGrid.store.setValue(item, 'BANK_COUNTRY_CODE', bankCountryCodeEdit);
            acctGrid.store.setValue(item, 'CURRENCY', acctCurrEdit);
            acctGrid.store.setValue(item, 'SOURCE_SYSTEM_BRANCH', sourceSystemBranchEdit);
            acctGrid.store.setValue(item, 'REGULATORY_ACCOUNT_TYPE', regulatoryAccountTypeEdit);
            //acctGrid._refresh();
          }
        });
      }
      else{
        acctGrid.store.fetch({query : {divId : selectedAcctDivId},
          onItem : function (item ) {
            acctGrid.store.setValue(item, 'ACCOUNT_NUMBER', acctNumEdit);
            acctGrid.store.setValue(item, 'ACCOUNT_TYPE', acctTypeEdit);
            acctGrid.store.setValue(item, 'ACCOUNT_NAME', acctNameEdit);
            acctGrid.store.setValue(item, 'BANK_COUNTRY_CODE', bankCountryCodeEdit);
            acctGrid.store.setValue(item, 'CURRENCY', acctCurrEdit);
            acctGrid.store.setValue(item, 'SOURCE_SYSTEM_BRANCH', sourceSystemBranchEdit);
            acctGrid.store.setValue(item, 'REGULATORY_ACCOUNT_TYPE', regulatoryAccountTypeEdit);
            // acctGrid._refresh();
          }
        });
      }

      //remove/readd the hidden node
      var hiddenAcctDiv = dom.byId(selectedAcctDivId);
      var hiddenAcctDivIdx ;
      if(hiddenAcctDiv != null){
        hiddenAcctDivIdx = selectedAcctDivId.substring("accountHidden".length);
        // Destroy node
        domConstruct.destroy(selectedAcctDivId);
        console.log("deleted div "+selectedAcctDivId);
      } 

      var namesArray = new Array();
      var i = 1;
      namesArray[i++] = "acctOid";
      namesArray[i++] = "acctNum";
      namesArray[i++] = "acctName";
      namesArray[i++] = "acctType";
      namesArray[i++] = "fxRateGroup";
      namesArray[i++] = "sourceSystemBranch";
      namesArray[i++] = "PropCustomerId";
      namesArray[i++] = "availableForDirectDebit";
      namesArray[i++] = "availableForIntPmt";
      namesArray[i++] = "availableForTBA";
      namesArray[i++] = "availableForDomesticPymt";
      namesArray[i++] = "availableForSettlementInstr";
      namesArray[i++] = "availableForLoanReq";
      namesArray[i++] = "deactivateIndicator";
      namesArray[i++] = "opBankOrgId";
      namesArray[i++] = "bankCountryCode";
      namesArray[i++] = "acctCurr";
      namesArray[i++] = "pagIdSelection1_";
      namesArray[i++] = "pagIdSelection2_";
      namesArray[i++] = "pagIdSelection3_";
      namesArray[i++] = "pagIdSelection4_";
      namesArray[i++] = "pagIdSelection5_";
      namesArray[i++] = "pagIdSelection6_";
      namesArray[i++] = "pagIdSelection7_";
      namesArray[i++] = "pagIdSelection8_";
      namesArray[i++] = "pagIdSelection9_";
      namesArray[i++] = "pagIdSelection10_";
      namesArray[i++] = "pagIdSelection11_";
      namesArray[i++] = "pagIdSelection12_";
      namesArray[i++] = "pagIdSelection13_";
      namesArray[i++] = "pagIdSelection14_";
      namesArray[i++] = "pagIdSelection15_";
      namesArray[i++] = "pagIdSelection16_";
      namesArray[i++] = "otherCorpsAccount";
      namesArray[i++] = "otherAccountOwnerOid";
      namesArray[i++] = "regulatoryAccountType";
                    	  
      i = 1;
      var valuesArray = new Array();
      valuesArray[i++] = acctOidEdit;
      valuesArray[i++] = acctNumEdit;
      valuesArray[i++] = acctNameEdit;
      valuesArray[i++] = acctTypeEdit;
      valuesArray[i++] = fxRateGroupFEdit;
      valuesArray[i++] = sourceSystemBranchEdit;
      valuesArray[i++] = PropCustomerIdEdit;
      valuesArray[i++] = availableForDirectDebitEdit;
      valuesArray[i++] = availableForIntPmtEdit;
      valuesArray[i++] = availableForTBAEdit;
      valuesArray[i++] = availableForDomesticPymtEdit;
      valuesArray[i++] = availableForSettlementInstrEdit;
      valuesArray[i++] = availableForLoanReqEdit;
      valuesArray[i++] = deactivateIndicatorEdit;
      valuesArray[i++] = opBankOrgIdEdit;
      valuesArray[i++] = bankCountryCodeEdit;
      valuesArray[i++] = acctCurrEdit;
      valuesArray[i++] = pagIdSelectionEdit1;
      valuesArray[i++] = pagIdSelectionEdit2;
      valuesArray[i++] = pagIdSelectionEdit3;
      valuesArray[i++] = pagIdSelectionEdit4;
      valuesArray[i++] = pagIdSelectionEdit5;
      valuesArray[i++] = pagIdSelectionEdit6;
      valuesArray[i++] = pagIdSelectionEdit7;
      valuesArray[i++] = pagIdSelectionEdit8;
      valuesArray[i++] = pagIdSelectionEdit9;
      valuesArray[i++] = pagIdSelectionEdit10;
      valuesArray[i++] = pagIdSelectionEdit11;
      valuesArray[i++] = pagIdSelectionEdit12;
      valuesArray[i++] = pagIdSelectionEdit13;
      valuesArray[i++] = pagIdSelectionEdit14;
      valuesArray[i++] = pagIdSelectionEdit15;
      valuesArray[i++] = pagIdSelectionEdit16;
      valuesArray[i++] = otherCorpsAccountEdit;
      valuesArray[i++] = otherAccountOwnerOidEdit;
      valuesArray[i++] = regulatoryAccountTypeEdit;
      local.addAccountHiddenFields(selectedAcctDivId, namesArray,valuesArray, hiddenAcctDivIdx);
    };
              
    local.addAccountHiddenFields = function(divId, parmName, parmValue, hiddenAcctDivIdx) {
      var hiddenDiv = document.createElement("div");
      hiddenDiv.setAttribute('id',divId);
      if (parmName instanceof Array ) {
        for(var i=1;i<parmName.length;i++){
          if (parmName[i]!=""){
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("id", parmName[i]+hiddenAcctDivIdx);
            hiddenField.setAttribute("name", parmName[i]+hiddenAcctDivIdx);
            hiddenField.setAttribute("value", parmValue[i]);
            hiddenDiv.appendChild(hiddenField);
          }
        }
      }
      else{
        if(parmName!=""){
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("id", parmName+hiddenAcctDivIdx);
          hiddenField.setAttribute("name", parmName+hiddenAcctDivIdx);
          hiddenField.setAttribute("value", parmValue);
          hiddenDiv.appendChild(hiddenField);
        }
      }
      console.log(hiddenDiv);
      var divEl = document.getElementById("acctFormHiddenElements");
      divEl.appendChild(hiddenDiv);
    }

    local.deleteAccountRow = function(myRowKey, myDivId){
      
      var acctGrid = registry.byId("acctGrid");
      if(myRowKey!=null && myRowKey!=""){
        console.log("deleting account by rowKey");
        acctGrid.removeSelectedRows();
        local.addAccountHiddenFields('accountHidden'+accountCount, 'acctOid', myRowKey, accountCount);
        accountCount++;
        var noOfRows = dom.byId("noOfRows");
        noOfRows.value = accountCount;
        /*acctGrid.store.fetchItemByIdentity({ 'rowKey' : myRowKey,  onItem :  function (item ) {
          if(item == null) {
            console.log(item+"not found");
          } else {
            acctGrid.removeSelectedRows();
            //add new hidden fields section, but with just an address oid.
            //this signifies to the mediator that the address should be deleted
            local.addAccountHiddenFields('accountHidden'+accountCount, 'acctOid', myRowKey, accountCount);
          }
        }});
        */
      }
      else if(myDivId!=null && myDivId!=""){
        console.log("deleting account by divId");
        acctGrid.removeSelectedRows();
        /*acctGrid.store.fetchItemByIdentity({ 'divId' : myDivId,  onItem :  function (item ) {
            if(item == null) {
              console.log(item+" divIdnot found");
            } 
            else {
              acctGrid.removeSelectedRows();
              //acctGrid._refresh();
            }
          }
        });
        */
        // Destroy node
        domConstruct.destroy(myDivId);
      }
      local.prepareForNewAccount();
    };

    local.prepareForNewAccount = function(){

      //first deselect any selected item in the grid
      var acctGrid = registry.byId("acctGrid");
      acctGrid.selection.clear();

      //now clear all of the account fields
      dom.byId("acctOidEdit").value = "";
      registry.byId("acctNumEdit").set("value", "");
      registry.byId("acctNameEdit").set("value", "");
      registry.byId("acctTypeEdit").set("value", "");

      //default fxRateGroup to corp org value
      var ccFxRateGrpWidget = registry.byId("FXRateGroup");
      var acctFxRateGrpWidget = registry.byId("fxRateGroupFEdit");
      if ( acctFxRateGrpWidget ) {
        if ( ccFxRateGrpWidget ) {
          acctFxRateGrpWidget.set("value",
            ccFxRateGrpWidget.get("value"));
        }
        else {
          acctFxRateGrpWidget.set("value", "");
        }
      }

      registry.byId("sourceSystemBranchEdit").set("value", "");
      if(dom.byId("acctCurrEdit")!=null)
        registry.byId("acctCurrEdit").attr('value', ""); // select
      if(dom.byId("acctCurr_D")!=null)
        registry.byId("acctCurr_D").attr('value', "");// select
      if(dom.byId("bankCountryCodeEdit")!=null)           // select
        registry.byId("bankCountryCodeEdit").attr('value', "");
      if(dom.byId("bankCountryCode_D")!=null)               // select
        registry.byId("bankCountryCode_D").attr('value', "");
      if(dom.byId("pagIdSelectionEdit1")!=null)  
        registry.byId("pagIdSelectionEdit1").attr('value', "");
      if(dom.byId("regulatoryAccountTypeEdit")!=null)           // select
          registry.byId("regulatoryAccountTypeEdit").attr('value', "");
      //default tpsCustId to corp org value
      var ccTpsCustIdWidget = registry.byId("ProponixCustomerId");
      var acctTpsCustIdWidget = registry.byId("PropCustomerIdEdit");
      if ( acctTpsCustIdWidget ) {
        if ( ccTpsCustIdWidget ) {
          acctTpsCustIdWidget.set("value",
            ccTpsCustIdWidget.get("value"));
        }
        else {
          acctTpsCustIdWidget.set("value", "");
        }
      }

      var deAct = registry.byId("deactivateIndicatorEdit")
      deAct.set('checked',false);

      var directDe = registry.byId("availableForDirectDebitEdit")
      directDe.set('checked',false);

      var intPmt = registry.byId("availableForIntPmtEdit")
      intPmt.set('checked',false);

      var tba = registry.byId("availableForTBAEdit")
      tba.set('checked',false);

      var pay = registry.byId("availableForDomesticPymtEdit")
      pay.set('checked',false);
      
      var settlInstr = registry.byId("availableForSettlementInstrEdit")
      settlInstr.set('checked',false);

      var loanReq = registry.byId("availableForLoanReqEdit")
      loanReq.set('checked',false);

      var corporateOrgOid = dom.byId("corporateOrgOid").value;
      var displayCOOBOid = dom.byId("displayCOOBOid").value;
      var isReadOnly = dom.byId("isReadOnly").value;

      if(registry.byId("acctNumEdit")!=null)
      	registry.byId("acctNumEdit").attr('disabled', false);
                            
      if(registry.byId("acctCurrEdit")!=null)
      	registry.byId("acctCurrEdit").attr('disabled', false);
                            
      if(registry.byId("acctTypeEdit")!=null)	
     	 registry.byId("acctTypeEdit").attr('disabled', false);
                            
      registry.byId("acctNumEdit").attr('disabled', false);
      registry.byId("acctCurrEdit").attr('disabled', false);
      registry.byId("acctTypeEdit").attr('disabled', false);
      registry.byId("bankCountryCodeEdit").attr('disabled', false);
      registry.byId("regulatoryAccountTypeEdit").attr('disabled', false);
      registry.byId("sourceSystemBranchEdit").attr('disabled', false);
      //registry.byId("pagIdSelectionEdit").attr('disabled', false);
   // DK IR T36000017932 Rel8.2 06/05/2013 starts
      if(registry.byId("opBankOrgIdEdit")){
    	  registry.byId("opBankOrgIdEdit").attr('disabled', false);  
      }
   // DK IR T36000017932 Rel8.2 06/05/2013 ends

      // Button management
      var myAdd = registry.byId("addAcc");
      if ( myAdd ) {
        myAdd.set('disabled',true);
      }
      var myUpdate = registry.byId('editAccount');
      if ( myUpdate ) {
        domStyle.set(myUpdate.domNode, 'display', 'none');
      }
      var myAddNew = registry.byId('addNewAccount');
      if ( myAddNew ) {
        domStyle.set(myAddNew.domNode, 'display', 'inline-block');
      }

      //reset the acct op bank org
      var acctOpBankOrgOid = '';
      var opBankOrg1Widget = registry.byId("OperationalBankOrg1");
      if ( opBankOrg1Widget ) {
        acctOpBankOrgOid = opBankOrg1Widget.get('value');
      }
      local.resetAcctOpBankEdit('N',acctOpBankOrgOid);
      
      var table = document.getElementById("panelTable");
      var rowCount = table.rows.length;
		var rowCountDup = rowCount;
          for(var i=1; i<rowCount; i++) {
          	table.deleteRow(i);
          	rowCount--;
              i--;
         }
         
          var k=0;
          for(var i=0;i<rowCountDup-1;i++){
          	dijit.byId("pagIdSelectionEdit"+(k+i+1)).destroy();
          	dijit.byId("pagIdSelectionEdit"+(k+i+2)).destroy();
          	k++;
          }
          var addButton = registry.byId("add2MorePanel");
          if ( addButton ) {
          	addButton.set('disabled',false);
          }     
          var k=0;
          var rowsToCreate = 2;
          for(var j=0;j<rowsToCreate;j++){
  	        var row = table.insertRow(j+1);
  		  	row.id = "panelIndex"+j;
  	   	 	var cell0 = row.insertCell(0);//first  cell in the row
  			var cell1 = row.insertCell(1);//second  cell in the row
  			var options = '<%=pagOptionsNew%>';
  		          cell0.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"pagIdSelectionEdit'+(j+k+1)+'\" id=\"pagIdSelectionEdit'+(j+k+1)+'\"  required=\"false\" class=\"char9\"> '+pagOptionsNew+' </select>';        
  		          cell1.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"pagIdSelectionEdit'+(j+k+2)+'\" id=\"pagIdSelectionEdit'+(j+k+2)+'\"  required=\"false\" class=\"char9\"> '+pagOptionsNew+' </select>';       
  				 require(["dojo/parser"], function(parser) {
  		         	parser.parse(row.id);
  		    	 });
  		        
  		        k++;
          }
    };

    local.resetAcctOpBankEdit = function(otherCorpInd, selectedOpBankOrgOid) {
      //get the fields
      var acctOpBankEdit = registry.byId("opBankOrgIdEdit");
      var otherAcctOpBank = registry.byId("opBankOrgIdOtherAcct");
      if ( acctOpBankEdit && otherAcctOpBank ) {
        var selectedDisplayVal = null; //if the input object is not found, will remain null
        if ( selectedOpBankOrgOid && selectedOpBankOrgOid.length>0 ) {
          var displayValInput = dom.byId(selectedOpBankOrgOid);
          if ( displayValInput ) {
            selectedDisplayVal = displayValInput.value;
          }
        }

        if ( otherCorpInd && otherCorpInd == 'Y' ) {
          //hide dropdown, make text box visible
          domStyle.set(acctOpBankEdit.domNode,'display','none');
          domStyle.set(otherAcctOpBank.domNode,'display','inline-block');
          var myData = [];
          if ( selectedDisplayVal != null ) { //it actually exists
            //put just the selected item in the store
            myData.push( {id:selectedOpBankOrgOid, name: selectedDisplayVal} );
          }
          //add the selected option
          var newStore = new Memory({ data: myData });
          acctOpBankEdit.set('store', newStore);
        }
        else { //not other account
          //hide text box, make drop down visible
          domStyle.set(otherAcctOpBank.domNode,'display','none');
          domStyle.set(acctOpBankEdit.domNode,'display','inline-block');
          //populate from op orgs associated with corp org
          var myData = [{id:'', name: ''}]; //ir #8895 always include a blank option
          var foundSelected = false;
          var opBankOrg1Widget = registry.byId("OperationalBankOrg1");
          if ( opBankOrg1Widget ) {
            var myId = opBankOrg1Widget.get('value');
            var myName = opBankOrg1Widget.get('displayedValue');
            if ( myId && myId.length>0 ) {
              myData.push( {id:myId, name: myName} );
              if ( myId == selectedOpBankOrgOid ) foundSelected = true;
            }
          }
          var opBankOrg2Widget = registry.byId("OperationalBankOrg2");
          if ( opBankOrg2Widget ) {
            var myId = opBankOrg2Widget.get('value');
            var myName = opBankOrg2Widget.get('displayedValue');
            if ( myId && myId.length>0 ) {
              myData.push( {id:myId, name: myName} );
              if ( myId == selectedOpBankOrgOid ) foundSelected = true;
            }
          }
          var opBankOrg3Widget = registry.byId("OperationalBankOrg3");
          if ( opBankOrg3Widget ) {
            var myId = opBankOrg3Widget.get('value');
            var myName = opBankOrg3Widget.get('displayedValue');
            if ( myId && myId.length>0 ) {
              myData.push( {id:myId, name: myName} );
              if ( myId == selectedOpBankOrgOid ) foundSelected = true;
            }
          }
          var opBankOrg4Widget = registry.byId("OperationalBankOrg4");
          if ( opBankOrg4Widget ) {
            var myId = opBankOrg4Widget.get('value');
            var myName = opBankOrg4Widget.get('displayedValue');
            if ( myId && myId.length>0 ) {
              myData.push( {id:myId, name: myName} );
              if ( myId == selectedOpBankOrgOid ) foundSelected = true;
            }
          }
          if ( foundSelected!=true ) {
            if ( selectedDisplayVal != null ) { //it actually exists
              //add the selected option
              myData.push( {id:selectedOpBankOrgOid, name: selectedDisplayVal} );
            }
          }
          var newStore = new Memory({ data: myData });
          acctOpBankEdit.set('store',newStore);
        }

        //this executes for both situations

        //put the displayable value in the text box
        otherAcctOpBank.set('value',selectedDisplayVal);

        //and select in the drop down
        acctOpBankEdit.set('value',selectedOpBankOrgOid);
      }
    };

    local.addSubCallBack = function(items){
      var acctGrid = registry.byId("acctGrid");
      var beforeRowCount = acctGrid.rowCount;
      var OperationalBankOrgHid1 = registry.byId("OperationalBankOrgHid1");
                               	
      baseArray.forEach(items, function(item) {

        var jOb = item.i;
        acctGrid.store.newItem({rowKey:"",divId:"accountHidden"+accountCount, ACCOUNT_NUMBER: jOb.ACCOUNT_NUMBER, ACCOUNT_NAME: jOb.ACCOUNT_NAME, CURRENCY: jOb.CURRENCY, ACCOUNT_TYPE: jOb.ACCOUNT_TYPE, BANK_COUNTRY_CODE: jOb.BANK_COUNTRY_CODE, SOURCE_SYSTEM_BRANCH: jOb.SOURCE_SYSTEM_BRANCH, OTHERCORP_CUSTOMER_INDICATOR: 'Y', REGULATORY_ACCOUNT_TYPE: regulatoryAccountTypeEdit });
        acctGrid.store.save();

        var namesArray = new Array();
        var i = 1;
        namesArray[i++] = "acctOid";
        namesArray[i++] = "acctNum";
        namesArray[i++] = "acctName";
        namesArray[i++] = "acctType";
        namesArray[i++] = "fxRateGroup";
        namesArray[i++] = "sourceSystemBranch";
        namesArray[i++] = "PropCustomerId";
        namesArray[i++] = "availableForDirectDebit";
        namesArray[i++] = "availableForIntPmt";
        namesArray[i++] = "availableForTBA";
        namesArray[i++] = "availableForDomesticPymt";
        namesArray[i++] = "availableForLoanReq";
        namesArray[i++] = "deactivateIndicator";
        namesArray[i++] = "opBankOrgId";
        namesArray[i++] = "bankCountryCode";
        namesArray[i++] = "acctCurr";
        //namesArray[i++] = "pagIdSelection";
        namesArray[i++] = "pendingTransactionBalance";
        namesArray[i++] = "pendingTransactionCreditBalance";
        namesArray[i++] = "otherCorpsAccount";
        namesArray[i++] = "otherAccountOwnerOid";
        namesArray[i++] = "pagIdSelection1_";
        namesArray[i++] = "pagIdSelection2_";
        namesArray[i++] = "pagIdSelection3_";
        namesArray[i++] = "pagIdSelection4_";
        namesArray[i++] = "pagIdSelection5_";
        namesArray[i++] = "pagIdSelection6_";
        namesArray[i++] = "pagIdSelection7_";
        namesArray[i++] = "pagIdSelection8_";
        namesArray[i++] = "pagIdSelection9_";
        namesArray[i++] = "pagIdSelection10_";
        namesArray[i++] = "pagIdSelection11_";
        namesArray[i++] = "pagIdSelection12_";
        namesArray[i++] = "pagIdSelection13_";
        namesArray[i++] = "pagIdSelection14_";
        namesArray[i++] = "pagIdSelection15_";
        namesArray[i++] = "pagIdSelection16_";
        namesArray[i++] = "regulatoryAccountType";                       	  
        i = 1;
        var valuesArray = new Array();
        valuesArray[i++] = "";
        valuesArray[i++] = jOb.ACCOUNT_NUMBER;
        valuesArray[i++] = jOb.ACCOUNT_NAME;
        valuesArray[i++] = jOb.ACCOUNT_TYPE;
        valuesArray[i++] = jOb.FX_RATE_GROUP;
        valuesArray[i++] = jOb.SOURCE_SYSTEM_BRANCH;
        valuesArray[i++] = jOb.PROPONIX_CUSTOMER_ID;
        valuesArray[i++] = jOb.AVAILABLE_FOR_DIRECT_DEBIT;
        valuesArray[i++] = jOb.AVAILABLE_FOR_INTERNATNL_PAY;
        valuesArray[i++] = jOb.AVAILABLE_FOR_XFER_BTWN_ACCTS;
        valuesArray[i++] = jOb.AVAILABLE_FOR_DOMESTIC_PYMTS;
        valuesArray[i++] = jOb.AVAILABLE_FOR_LOAN_REQUEST;
        valuesArray[i++] = jOb.DEACTIVATE_INDICATOR;
        valuesArray[i++] = (jOb.A_OP_BANK_ORG_OID!= null || jOb.A_OP_BANK_ORG_OID != "")?jOb.A_OP_BANK_ORG_OID:OperationalBankOrgHid1;
        valuesArray[i++] = jOb.BANK_COUNTRY_CODE;
        valuesArray[i++] = jOb.CURRENCY;
        //valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID;
        valuesArray[i++] = jOb.PENDING_TRANSACTION_BALANCE;
        valuesArray[i++] = jOb.PENDING_TRANSAC_CREDIT_BALANCE;
        valuesArray[i++] = "Y";
        valuesArray[i++] = jOb.P_OWNER_OID;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_1;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_2;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_3;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_4;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_5;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_6;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_7;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_8;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_9;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_10;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_11;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_12;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_13;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_14;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_15;
        valuesArray[i++] = jOb.A_PANEL_AUTH_GROUP_OID_16;
        valuesArray[i++] = regulatoryAccountTypeEdit;
  
        local.addAccountHiddenFields('accountHidden'+accountCount, namesArray,valuesArray, accountCount);
        accountCount++;
        var noOfRows = dom.byId("noOfRows");
        noOfRows.value = accountCount;
      });

      //now select the added account so update becomes active
      //think grid adding is asynchronous so not sure how reliable this is...
      var lastAcctIdx = beforeRowCount;
      acctGrid.selection.clear();
      acctGrid.selection.setSelected(lastAcctIdx, true);
    };

    local.setPaymentsCheckBox = function() {	
      registry.getEnclosingWidget(document.forms[0].DomesticPaymentIndicator).set('checked', true);
      //CR 988 Rel9.2 START
      if(document.forms[0].straightThroughAuthorizeIndicator.checked == true){
    	  registry.byId('allowPartialPayAuthorizeIndicator').set("disabled",false);
      }else{
    	  registry.getEnclosingWidget(document.forms[0].allowPartialPayAuthorizeIndicator).set('checked', false);
    	  registry.byId('allowPartialPayAuthorizeIndicator').set("disabled",true);
      }
      //CR 988 END
    };
    //CR 857 Start
    local.setPaymentBenAuthInDCheckBox = function() {	
        registry.getEnclosingWidget(document.forms[0].DomesticPaymentIndicator).set('checked', true);
	//IR T36000019854
        if(registry.getEnclosingWidget(document.forms[0].PaymentBenePanelAuthIndicator).checked == true){
        	registry.getEnclosingWidget(document.forms[0].DomesticPaymentDualAuthIndicator[3]).set('checked',true);
        }
      };
    //CR 857 End
    local.setILCCheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].ImportLettersOfCreditIndicator).set('checked', true);
      
      if(document.forms[0].ImportDLCDualAuthorizationIndicator[3].checked == false){
    	  registry.byId('ImportDLCPanelGroupOid').set("disabled",true);
      }else{
     	  registry.byId('ImportDLCPanelGroupOid').set("disabled",false);
      }	       
    };

    // This function checks the first Import Letters of Credit dual authorization radio
    // button if the checkbox associated with it is being checked; otherwise it unchecks
    // both radio buttons.
    local.setILCRadioButtons = function() {
      if (document.forms[0].ImportLettersOfCreditIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].ImportDLCDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ImportDLCDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ImportDLCDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ImportDLCDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ImportDLCProcessPOUploadIndicator).set('checked', false);
        registry.getEnclosingWidget(document.forms[0].ImportDLCProcessPOManualIndicator).set('checked', false);
        registry.getEnclosingWidget(document.forms[0].ImportDLCCheckerAuthIndicator).set('checked', false);
        registry.byId('ImportDLCPanelGroupOid').set("disabled",true);
      }
    };

    // This function sets the Standby Letters of Credit check box to checked whenever a
    // user clicks on one of the radio buttons associated with it.
    local.setSLCRadio = function(type) {
      registry.getEnclosingWidget(document.forms[0].StandbyLettersOfCreditIndicator).set('checked', true);
      console.log('type='+type);
      if(type == 'S') {
        document.forms[0].StandbyDLCUseOnlyGuaranteeIndicator.value = "N";//set default value as N not blank
        document.forms[0].StandbyLCUseBothFormsIndicator.value = "N";
      }else if(type == 'D') {
		document.forms[0].StandbyDLCUseOnlyGuaranteeIndicator.value = "Y";
		document.forms[0].StandbyLCUseBothFormsIndicator.value = "N";
      }else if(type == 'B'){
		document.forms[0].StandbyLCUseBothFormsIndicator.value = "Y";
		document.forms[0].StandbyDLCUseOnlyGuaranteeIndicator.value = "N";
      }
    };

    local.setSLCCheckBox = function() {	
      registry.getEnclosingWidget(document.forms[0].StandbyLettersOfCreditIndicator).set('checked', true);
      
      if(document.forms[0].StandbyDLCDualAuthorizationIndicator[3].checked == false){
      	  registry.byId('StandbyDLCPanelGroupOid').set("disabled",true);
      }else{
       	  registry.byId('StandbyDLCPanelGroupOid').set("disabled",false);
      }        
    }

    // This function checks the first Standby Letters of Credit dual authorization radio
    // button if the checkbox associated with it is being checked; otherwise it unchecks
    // both radio buttons.
    local.setSLCControls = function() {
      if (document.forms[0].StandbyLettersOfCreditIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].StandbyDLCDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].StandbyDLCDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].StandbyDLCDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].StandbyDLCDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].StandbyDLCCheckerAuthIndicator).set('checked',false);
        registry.byId("Simple").set('checked', false);
        registry.byId("Detail").set('checked', false);
        registry.byId("Both").set('checked', false);
        registry.byId('StandbyDLCPanelGroupOid').set("disabled",true);
      }
    };

    // This function sets the Export Letters of Credit check box to checked whenever a
    // user clicks on one of the radio buttons associated with it.
    local.setELCCheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].ExportLettersOfCreditIndicator).set('checked', true);
      
      if(document.forms[0].ExportLCDualAuthorizationIndicator[3].checked == false){
    	  registry.byId('ExportLCPanelGroupOid').set("disabled",true);
      }else{
     	  registry.byId('ExportLCPanelGroupOid').set("disabled",false);
      }	       
    };

    // This function checks the first Export Letters of Credit dual authorization radio
    // button if the checkbox associated with it is being checked; otherwise it unchecks
    // both radio buttons.
    local.setELCRadioButtons = function() {
      if (document.forms[0].ExportLettersOfCreditIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].ExportLCDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ExportLCDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ExportLCDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ExportLCDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ExportDLCCheckerAuthIndicator).set('checked',false);
        registry.byId('ExportLCPanelGroupOid').set("disabled",true);
      }
    };

    //Vasavi CR 524 03/31/2010 Add
    // This function sets the Export Collections check box to checked whenever a
    // user clicks on one of the radio buttons associated with it.
    local.setNECOLCheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].NewExportCollectionsIndicator).set('checked', true);
      
      if(document.forms[0].NewExportCollectionsDualAuthorizationIndicator[3].checked == false){
    	  registry.byId('NewExportCollectionsPanelGroupOid').set("disabled",true);
      }else{
     	  registry.byId('NewExportCollectionsPanelGroupOid').set("disabled",false);
      }	       
    };
    
    //SSikhakolli - Rel-9.4 CR-818
    // This function sets the Import Collections check box to checked whenever a
    // user clicks on one of the radio buttons associated with it.
    local.setImportCOLCheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].ImportCollectionsIndicator).set('checked', true);
      
      if(document.forms[0].ImportCollectionsDualAuthorizationIndicator[3].checked == false){
    	  registry.byId('ImportCollectionsPanelGroupOid').set("disabled",true);
      }else{
     	  registry.byId('ImportCollectionsPanelGroupOid').set("disabled",false);
      }	       
    };
    
    //SSikhakolli - Rel-9.4 CR-818
    // This function checks the first Import Collections dual authorization radio button
    // if the checkbox associated with it is being checked; otherwise it unchecks both
    // radio buttons.
    local.setImportCOLRadioButtons = function() {
      if (document.forms[0].ImportCollectionsIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].ImportCollectionsDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ImportCollectionsDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ImportCollectionsDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ImportCollectionsDualAuthorizationIndicator[3]).set('checked',false);
        registry.byId('ImportCollectionsCheckerAuthIndicator').set('checked',false);
        registry.byId('ImportCollectionsPanelGroupOid').set("disabled",true);
      }
    };

    // This function checks the first Export Collections dual authorization radio button
    // if the checkbox associated with it is being checked; otherwise it unchecks both
    // radio buttons.
    local.setNECOLRadioButtons = function() {
      if (document.forms[0].NewExportCollectionsIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].NewExportCollectionsDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].NewExportCollectionsDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].NewExportCollectionsDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].NewExportCollectionsDualAuthorizationIndicator[3]).set('checked',false);
        registry.byId('NewExportCollectionsCheckerAuthIndicator').set('checked',false);
        registry.byId('NewExportCollectionsPanelGroupOid').set("disabled",true);
      }
    };
    
    // This function sets the Direct Send Collections check box to checked whenever a
    // user clicks on one of the radio buttons associated with it.
    local.setECOLCheckBox = function() {
	   registry.getEnclosingWidget(document.forms[0].ExportCollectionsIndicator).set('checked', true);
	   
      if(document.forms[0].ExportCollectionsDualAuthorizationIndicator[3].checked == false){
    	  registry.byId('ExportCollectionsPanelGroupOid').set("disabled",true);
      }else{
     	  registry.byId('ExportCollectionsPanelGroupOid').set("disabled",false);
      }	   
    };

    // This function checks the first Direct Send Collections dual authorization radio button
    // if the checkbox associated with it is being checked; otherwise it unchecks both
    // radio buttons.
    local.setECOLRadioButtons = function() {
      if (document.forms[0].ExportCollectionsIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].ExportCollectionsDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ExportCollectionsDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ExportCollectionsDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ExportCollectionsDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ExportCollectionsCheckerAuthIndicator).set('checked',false);
        registry.byId('ExportCollectionsPanelGroupOid').set("disabled",true);
      }
    };

    // This function sets the Guarantees check box to checked whenever a user clicks on
    // one of the radio buttons associated with it.
    local.setGTEECheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].GuaranteesIndicator).set('checked', true);
      
      if(document.forms[0].GuaranteesDualAuthorizationIndicator[3].checked == false){
      	  registry.byId('GuaranteesPanelGroupOid').set("disabled",true);
        }else{
       	  registry.byId('GuaranteesPanelGroupOid').set("disabled",false);
        }      
    };

    // This function checks the first Guarantees dual authorization radio button if
    // the checkbox associated with it is being checked; otherwise it unchecks both
    // radio buttons.
    local.setGTEERadioButtons = function() {
      if (document.forms[0].GuaranteesIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].GuaranteesDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].GuaranteesDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].GuaranteesDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].GuaranteesDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].GuaranteesCheckerAuthIndicator).set('checked',false);
        registry.byId('GuaranteesPanelGroupOid').set("disabled",true);
      }
    };

    // This function sets the Air Waybills check box to checked whenever a
    // user clicks on one of the radio buttons associated with it.
    local.setAWBCheckBox = function() {	
      registry.getEnclosingWidget(document.forms[0].AirWaybillsIndicator).set('checked', true);
      
      if(document.forms[0].AirWaybillsDualAuthorizationIndicator[3].checked == false){
    	 registry.byId('AirWaybillsPanelGroupOid').set("disabled",true);
      }else{
    	  registry.byId('AirWaybillsPanelGroupOid').set("disabled",false);
      }
    };

    // This function checks the first Air Waybills dual authorization radio
    // button if the checkbox associated with it is being checked; otherwise it
    // unchecks both radio buttons.
    local.setAWBRadioButtons = function() {
      if (document.forms[0].AirWaybillsIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].AirWaybillsDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].AirWaybillsDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].AirWaybillsDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].AirWaybillsDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].AirWaybillsCheckerAuthIndicator).set('checked',false);
        registry.byId('AirWaybillsPanelGroupOid').set("disabled",true);
      }
    };

    // This function sets the Shipping Guarantees check box to checked whenever a user
    // clicks on one of the radio buttons associated with it.
    local.setSGTEECheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].ShippingGuaranteesIndicator).set('checked', true);
      
      if(document.forms[0].ShippingGuaranteesDualAuthorizationIndicator[3].checked == false){
      	  registry.byId('ShippingGuaranteesPanelGroupOid').set("disabled",true);
      }else{
       	  registry.byId('ShippingGuaranteesPanelGroupOid').set("disabled",false);
      }       
    };

    // This function checks the first Shipping Guarantees dual authorization radio button
    // if the checkbox associated with it is being checked; otherwise it unchecks both
    // radio buttons.
    local.setSGTEERadioButtons = function() {
      if (document.forms[0].ShippingGuaranteesIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].ShippingGuaranteesDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ShippingGuaranteesDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ShippingGuaranteesDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ShippingGuaranteesDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ShippingGuaranteeCheckerAuthIndicator).set('checked',false);
        registry.byId('ShippingGuaranteesPanelGroupOid').set("disabled",true);
      }
    };

    local.setPICCheckBox = function() {
      document.forms[0].PaymtInstrSettingsAuthIndicator.checked = true;
      document.forms[0].PaymtInstrSettingsAuthIndicator.value = 'Y';
      if(document.forms[0].AllowPanelAuthForPaymts[0].checked == true) {
        document.forms[0].TransferBtwAcctsIndicator.checked = false;
        document.forms[0].DomesticPaymentIndicator.checked = false;
        document.forms[0].FundsTransferIndicator.checked = false;
        document.forms[0].TransferBtwAcctsDualAuthIndicator[0].checked = false;
        document.forms[0].TransferBtwAcctsDualAuthIndicator[1].checked = false;
        document.forms[0].TransferBtwAcctsDualAuthIndicator[2].checked = false;
        document.forms[0].DomesticPaymentDualAuthIndicator[0].checked = false;
        document.forms[0].DomesticPaymentDualAuthIndicator[1].checked = false;
        document.forms[0].DomesticPaymentDualAuthIndicator[2].checked = false;
        document.forms[0].FundsTransferDualAuthorizationIndicator[0].checked = false;
        document.forms[0].FundsTransferDualAuthorizationIndicator[1].checked = false;
        document.forms[0].FundsTransferDualAuthorizationIndicator[2].checked = false;
      }
    };

    // This function sets the Funds Transfer Requests check box to checked whenever a user
    // clicks on one of the radio buttons associated with it.
    local.setFTRCheckBox = function(allowPanel) {
      registry.getEnclosingWidget(document.forms[0].FundsTransferIndicator).set('checked', true);
      document.forms[0].PaymtInstrSettingsAuthIndicator.value = 'Y';
    };

    // This function checks the first Funds Transfer dual authorization radio button
    // if the checkbox associated with it is being checked; otherwise it unchecks both
    // radio buttons.
    local.setFTRRadioButtons = function() {
      if (document.forms[0].FundsTransferIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].FundsTransferDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].FundsTransferDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].FundsTransferDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].FundsTransferDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].FundsTransferCheckerAuthIndicator).set('checked',false);
      } else {
        document.forms[0].PaymtInstrSettingsAuthIndicator.value = 'Y';
      }
    };

    //This function sets the Transfer Between Accounts check box to checked whenever a user
    //clicks on one of the radio buttons associated with it.
    local.setTBACheckBox = function(allowPanel) {
      registry.getEnclosingWidget(document.forms[0].TransferBtwAcctsIndicator).set('checked', true);
      document.forms[0].PaymtInstrSettingsAuthIndicator.value = 'Y';
    };

    //This function checks the first Transfer Between Accounts dual authorization radio button
    //if the checkbox associated with it is being checked; otherwise it unchecks both
    //radio buttons.
    local.setTBARadioButtons = function() {
      if (document.forms[0].TransferBtwAcctsIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].TransferBtwAcctsDualAuthIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].TransferBtwAcctsDualAuthIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].TransferBtwAcctsDualAuthIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].TransferBtwAcctsDualAuthIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].TransferBtxAcctsCheckerAuthIndicator).set('checked',false);
      } else {
        document.forms[0].PaymtInstrSettingsAuthIndicator.value = 'Y';
      }
    };

    //This function sets the Transfer Between Accounts check box to checked whenever a user
    //clicks on one of the radio buttons associated with it.
    local.setDPCheckBox = function(allowPanel) {
      registry.getEnclosingWidget(document.forms[0].DomesticPaymentIndicator).set('checked', true);
      document.forms[0].PaymtInstrSettingsAuthIndicator.value = 'Y';
      //IR T36000019854 Rel 8.3 START
      if(allowPanel == 'Y'){
    	  registry.getEnclosingWidget(document.forms[0].PaymentBenePanelAuthIndicator).set('disabled',false);
      }else{
    	  registry.getEnclosingWidget(document.forms[0].PaymentBenePanelAuthIndicator).set('checked',false);
    	  registry.getEnclosingWidget(document.forms[0].PaymentBenePanelAuthIndicator).set('disabled',true);
      }
      //IR T36000019854 Rel 8.3 END
    };

    //This function checks the first Transfer Between Accounts dual authorization radio button
    //if the checkbox associated with it is being checked; otherwise it unchecks both
    //radio buttons.
    local.setDPRadioButtons = function() {
      if (document.forms[0].DomesticPaymentIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].DomesticPaymentDualAuthIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].DomesticPaymentDualAuthIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].DomesticPaymentDualAuthIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].DomesticPaymentDualAuthIndicator[3]).set('checked',false);
        //Added below two lines as Subcheckboxes was not getting disabled once we uncheck the main checkbox.
        registry.getEnclosingWidget(document.forms[0].ProcessPaymentFileUploadIndicator).set('checked', false);
        registry.getEnclosingWidget(document.forms[0].straightThroughAuthorizeIndicator).set('checked', false);
        registry.getEnclosingWidget(document.forms[0].DomesticPaymentCheckerAuthIndicator).set('checked',false);
	//IR T36000019854 Rel 8.3 
        registry.getEnclosingWidget(document.forms[0].PaymentBenePanelAuthIndicator).set('checked', false);
        registry.getEnclosingWidget(document.forms[0].PaymentBenePanelAuthIndicator).set('disabled',false);
      } else {
        document.forms[0].PaymtInstrSettingsAuthIndicator.value = 'Y';
	//IR T36000019854 Rel 8.3 
        registry.getEnclosingWidget(document.forms[0].PaymentBenePanelAuthIndicator).set('disabled',false);
      }
    };

    // This function sets the Loan Requests check box to checked whenever a user
    // clicks on one of the radio buttons associated with it.
    local.setLRCheckBox = function() {	
    	registry.getEnclosingWidget(document.forms[0].LoanRequestIndicator).set('checked', true);
    	
        if(document.forms[0].LoanRequestDualAuthorizationIndicator[3].checked == false){
      	  registry.byId('LoanRequestPanelGroupOid').set("disabled",true);
        }else{
       	  registry.byId('LoanRequestPanelGroupOid').set("disabled",false);
        }	      	
    };

    local.setNPOIIndicator = function(indicatorVlaue) {
    	 registry.getEnclosingWidget(document.forms[0].NonPortalOriginatedInstrAllowedIndicator).set('checked', indicatorVlaue);
         registry.getEnclosingWidget(document.forms[0].NonPortalOriginatedInstrIndicator).set('checked', indicatorVlaue);
         
    };
    
  
    // This function checks the first Loan Request dual authorization radio button
    // if the checkbox associated with it is being checked; otherwise it unchecks both
    // radio buttons.
    local.setLRRadioButtons = function() {
      if (document.forms[0].LoanRequestIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].LoanRequestDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].LoanRequestDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].LoanRequestDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].LoanRequestDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].LoanRequestCheckerAuthIndicator).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ProcessInvoicesFileUploadIndicator).set('checked',false); // DK CR709 Rel8.2 10/31/2012
        registry.getEnclosingWidget(document.forms[0].TradeLoanIndicator).set('checked',false); // DK CR709 Rel8.2 10/31/2012
        registry.getEnclosingWidget(document.forms[0].ImportLoanIndicator).set('checked',false); // DK CR709 Rel8.2 10/31/2012
        registry.getEnclosingWidget(document.forms[0].ExportLoanIndicator).set('checked',false); // DK CR709 Rel8.2 10/31/2012
        registry.getEnclosingWidget(document.forms[0].InvoiceFinancingIndicator).set('checked',false); // DK CR709 Rel8.2 10/31/2012
        registry.byId('LoanRequestPanelGroupOid').set("disabled",true);
      }
    };

    // This function sets the Request Advise check box to checked whenever a user
    // clicks on one of the radio buttons associated with it.
    local.setRACheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].RequestAdviseIndicator).set('checked', true);
      
      if(document.forms[0].RequestAdviseDualAuthorizationIndicator[3].checked == false){
      	  registry.byId('RequestAdvisePanelGroupOid').set("disabled",true);
      }else{
       	  registry.byId('RequestAdvisePanelGroupOid').set("disabled",false);
      }        
    };

    // This function checks the first Request Advise dual authorization radio button
    // if the checkbox associated with it is being checked; otherwise it unchecks both
    // radio buttons.
    local.setRARadioButtons = function() {
      if (document.forms[0].RequestAdviseIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].RequestAdviseDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].RequestAdviseDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].RequestAdviseDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].RequestAdviseDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].RequestAdviseCheckerAuthIndicator).set('checked',false);
        registry.byId('RequestAdvisePanelGroupOid').set("disabled",true);
      }
    };
    
    //Ravindra - CR-708B - Start
    // This function sets the Supplier Portal check box to checked whenever a user
    // clicks on one of the radio buttons associated with it.
    local.setSPCheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].SupplierPortalIndicator).set('checked', true);
      
    //Sandeep CR708BC IR# T36000014646 03/15/2013 - Start
    	local.setSPMarginRadioButtons();
    	//Sandeep CR708BC IR# T36000014646 03/15/2013 - End
    	
        if(document.forms[0].SupplierPortalDualAuthorizationIndicator[3].checked == false){
        	  registry.byId('SupplierPortalPanelGroupOid').set("disabled",true);
        }else{
         	  registry.byId('SupplierPortalPanelGroupOid').set("disabled",false);
        }      	
    };
    // This function checks the first Supplier Portal dual authorization radio button
    // if the checkbox associated with it is being checked; otherwise it unchecks both
    // radio buttons.
    local.setSPRadioButtons = function() {
      if (document.forms[0].SupplierPortalIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].SupplierPortalDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].SupplierPortalDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].SupplierPortalDualAuthorizationIndicator[2]).set('checked',false);
	registry.getEnclosingWidget(document.forms[0].SupplierPortalDualAuthorizationIndicator[3]).set('checked',false);
	registry.byId('SupplierPortalPanelGroupOid').set("disabled",true);
	   // M Eerupula 04/17/2013 Rel8.3 CR776  
		var widget = registry.getEnclosingWidget(document.forms[0].SPPayProgName);
		var parentNode = widget.domNode.parentNode;
		widget.set('value','');
		widget.set('required',false);
		widget.set('readonly',true);
		domClass.remove(parentNode, "required");
	}else{
		var widget = registry.getEnclosingWidget(document.forms[0].SPPayProgName);
		var parentNode = widget.domNode.parentNode;
		widget.set('required',true);
		widget.set('readonly',false);
		domClass.add(parentNode, "required");           	 
	}

      
      	//Sandeep CR708BC IR# T36000014646 03/15/2013 - Start
    	local.setSPMarginRadioButtons();
    	//Sandeep CR708BC IR# T36000014646 03/15/2013 - End
    };
    //Ravindra - CR-708B - End

  //Sandeep CR708BC IR# T36000014646 03/15/2013 - Start
    //Disable/Enable "SPStraightDiscount, SPDiscountToYield" radio buttons
  	// based "Supplier Portal" check box selection
      local.setSPMarginRadioButtons = function() {
      	var spIndicatorFlag = registry.getEnclosingWidget(document.forms[0].SupplierPortalIndicator).checked;
        	if(!spIndicatorFlag){
        		registry.getEnclosingWidget(document.forms[0].SPStraightDiscount).set('checked', false);
        		registry.getEnclosingWidget(document.forms[0].SPDiscountToYield).set('checked', false);
    		
        		registry.getEnclosingWidget(document.forms[0].SPStraightDiscount).set('disabled', true);
        		registry.getEnclosingWidget(document.forms[0].SPDiscountToYield).set('disabled', true);
    	 	}
        	if(spIndicatorFlag){
    			//registry.byId("SPStraightDiscount").set('checked', false);
    			//registry.byId("SPDiscountToYield").set('checked', false);
    		
        		registry.getEnclosingWidget(document.forms[0].SPStraightDiscount).set('disabled', false);
        		registry.getEnclosingWidget(document.forms[0].SPDiscountToYield).set('disabled', false);
    	 	}
      };
    //Sandeep CR708BC IR# T36000014646 03/15/2013 - End
      
    // This function sets the Approval to Pay check box to checked whenever a
    // user clicks on one of the radio buttons associated with it.
    local.setATPCheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].ApprovalToPayIndicator).set('checked', true);
      
      if(document.forms[0].ApprovalToPayDualAuthorizationIndicator[3].checked == false){
    	  registry.byId('ApprovalToPayPanelGroupOid').set("disabled",true);
      }else{
     	  registry.byId('ApprovalToPayPanelGroupOid').set("disabled",false);
      }
    };

    // This function checks the first Approval to Pay dual authorization radio
    // button if the checkbox associated with it is being checked; otherwise it unchecks
    // both radio buttons.
    local.setATPRadioButtons = function() {
      if (document.forms[0].ApprovalToPayIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].ApprovalToPayDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ApprovalToPayDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ApprovalToPayDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ApprovalToPayDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ApprovalToPayCheckerAuthIndicator).set('checked',false);
        
        var chkBox = registry.byId('ApprovalToPayProcessPOUploadIndicator');
        if ( chkBox ) {
          chkBox.set('checked',false);
        }
        var chkBox = registry.byId('ApprovalToPayProcessPOManualIndicator');
        if ( chkBox ) {
          chkBox.set('checked',false);
        }
        var chkBox = registry.byId('ATPProcessInvoiceIndicator');
        if ( chkBox ) {
          chkBox.set('checked',false);
        }
        
       	 registry.byId('ApprovalToPayPanelGroupOid').set("disabled",true);
      }
    };

    // This function sets the Approval to Pay check box to checked whenever a
    // user clicks on one of the radio buttons associated with it.
    local.setARMCheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].ReceivablesManagementIndicator).set('checked', true);
      
      if(document.forms[0].ReceivablesManagementDualAuthorizationIndicator[3].checked == false){
    	  registry.byId('MatchingApprovalPanelGroupOid').set("disabled",true);
      }else{
     	  registry.byId('MatchingApprovalPanelGroupOid').set("disabled",false);
      }        
    };

    // This function checks the first Approval to Pay dual authorization radio
    // button if the checkbox associated with it is being checked; otherwise it unchecks
    // both radio buttons.
    local.setRMRadioButtons = function() {
      if (document.forms[0].ReceivablesManagementIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].ReceivablesManagementDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ReceivablesManagementDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ReceivablesManagementDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ReceivablesManagementDualAuthorizationIndicator[3]).set('checked',false);
        registry.byId('MatchingApprovalPanelGroupOid').set("disabled",true);
      }
    };

    //This function sets the Purchase Order Upload will be available for this customer check box to checked whenever a
    //user clicks on one of the radio buttons associated with it.
    /* KMehta - 10 Feb 2016 - Rel9.5 IR-T36000046039 Chaneg Start */
    local.setPOUploadCheckBox = function() {
      document.forms[0].PurchaseOrderManagementIndicator.checked = true;
      if(registry.byId('StructuredPO').checked == true) {
        document.forms[0].ApprovalToPayProcessPOManualIndicator.checked = false;
        registry.byId('StructuredPO').checked = true;
      }else  if(registry.byId('FreeTextPO').checked == true) {
          registry.byId('FreeTextPO').checked = true;
        }
      registry.getEnclosingWidget(document.forms[0].PurchaseOrderManagementIndicator).set('checked',true);
    };

    //This function checks the Purchase Order will be uploaded and stored in a Structured Format radio
    //button if the checkbox associated with it is being checked; otherwise it unchecks
    //both radio buttons.
    local.setPORadioButtons = function() {    	
      if (document.forms[0].PurchaseOrderManagementIndicator.checked == false) {
    	  	document.forms[0].POUploadDualIndicator[0].checked = false;
            document.forms[0].POUploadDualIndicator[1].checked = false;
	        var checkedFlag = false;
	        var checkedObj = registry.getEnclosingWidget(document.forms[0].POUploadDualIndicator[0]);
	        checkedObj.set('checked',checkedFlag);
	        checkedObj = registry.getEnclosingWidget(document.forms[0].POUploadDualIndicator[1]);
	        checkedObj.set('checked',checkedFlag);
	        
      }
      if(document.forms[0].PurchaseOrderManagementIndicator.checked == true) {
        if((registry.byId('StructuredPO').checked == false) && (registry.byId('StructuredPO').checked == false)) {
        	registry.byId('StructuredPO').checked = false;
      	    registry.byId('FreeTextPO').checked = false;
        }
      }	   
    };
    /* KMehta - 10 Feb 2016 - Rel9.5 IR-T36000046039 Chaneg End */
	
    //This function sets the Receivables Management Invoice authorisation check box to checked whenever a
    //user clicks on one of the radio buttons associated with it.
    local.setARMInvoiceCheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].ReceivablesManagementInvoiceIndicator).set('checked', true);
      
      if(document.forms[0].ReceivablesManagementInvoiceDualAuthorizationIndicator[3].checked == false){
    	  registry.byId('InvoiceAuthorizationPanelGroupOid').set("disabled",true);
      }else{
     	  registry.byId('InvoiceAuthorizationPanelGroupOid').set("disabled",false);
      }    
    };

    //This function checks the first Approval to Pay dual authorization radio
    //button if the checkbox associated with it is being checked; otherwise it unchecks
    //both radio buttons.
    local.setRMInvoiceRadioButtons = function() {
      if (document.forms[0].ReceivablesManagementInvoiceIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].ReceivablesManagementInvoiceDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ReceivablesManagementInvoiceDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ReceivablesManagementInvoiceDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ReceivablesManagementInvoiceDualAuthorizationIndicator[3]).set('checked',false);
        registry.byId('InvoiceAuthorizationPanelGroupOid').set("disabled",true);
      }
    };

    //This function sets the Credit Note authorisation check box to checked whenever a
    //user clicks on one of the radio buttons associated with it.
    local.setARMCreditNoteCheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].CreditNoteIndicator).set('checked', true);
      
      if(document.forms[0].CreditNoteDualAuthorizationIndicator[3].checked == false){
    	  registry.byId('CreditAuthorizationPanelGroupOid').set("disabled",true);
      }else{
     	  registry.byId('CreditAuthorizationPanelGroupOid').set("disabled",false);
      }       
    };

    //This function checks the first Approval to Pay dual authorization radio
    //button if the checkbox associated with it is being checked; otherwise it unchecks
    //both radio buttons.
    local.setRMCreditNoteRadioButtons = function() {
      if (document.forms[0].CreditNoteIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].CreditNoteDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].CreditNoteDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].CreditNoteDualAuthorizationIndicator[2]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].CreditNoteDualAuthorizationIndicator[3]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].CreditNoteDualAuthorizationIndicator[4]).set('checked',false);
        registry.byId('CreditAuthorizationPanelGroupOid').set("disabled",true);
      }
    };
    
  //Sandeep Rel-8.3 IR#T36000019765 08/08/2013 - Begin
    //This function disable/enable Panel Group Dropdown Box based on the Discrepancy Response / Approval to Pay Response Radiobuttons
    local.setDCRPanelDropDown = function(){
    	if(document.forms[0].DiscrepancyResponseDualAuthorizationIndicator[3].checked == false){
      	  registry.byId('DiscrepancyResponsePanelGroupOid').set("disabled",true);
        }else{
       	  registry.byId('DiscrepancyResponsePanelGroupOid').set("disabled",false);
        } 
    };
  //Sandeep Rel-8.3 IR#T36000019765 08/08/2013 - End
  
  //SSikhakolli - Rel-9.4 CR-818
   //This function disable/enable Panel Group Dropdown Box based on the Settlement Instructions Radiobuttons
    local.setSIPanelDropDown = function(){
    	if(document.forms[0].SettlementInstructionDualAuthorizationIndicator[3].checked == false){
      	  registry.byId('SettlementInstructionPanelGroupOid').set("disabled",true);
        }else{
       	  registry.byId('SettlementInstructionPanelGroupOid').set("disabled",false);
        } 
    };
    
    // This function sets the Approval to Pay check box to checked whenever a
    // user clicks on one of the radio buttons associated with it.
    local.setDDCheckBox = function() {
      registry.getEnclosingWidget(document.forms[0].DirectDebitsIndicator).set('checked', true);
    };

    // This function checks the first Approval to Pay dual authorization radio
    // button if the checkbox associated with it is being checked; otherwise it unchecks
    // both radio buttons.
    local.setDDRadioButtons = function() {
      if (document.forms[0].DirectDebitsIndicator.checked == false) {
        registry.getEnclosingWidget(document.forms[0].DirectDebitsDualAuthorizationIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].DirectDebitsDualAuthorizationIndicator[1]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].DirectDebitsDualAuthorizationIndicator[2]).set('checked',false);
      }
    };

    // This fuction unchecks the CheckForceCertsForUserMaint check box, which is not applicable
    // when authentication method is Certificate Only or SSO only or Password Only
    local.unCheckForceCertsForUserMaint = function() {
      document.forms[0].ForceCertsForUserMaint.checked = false;
    };

    local.setDailyLimitCheckBox = function() {
      if(registry.byId("TransferDailyLimit") != "") {
        if (document.forms[0].EnforceDailyLimitTransfer.checked == false) {
          document.forms[0].EnforceDailyLimitTransfer.checked = true;
          registry.byId("EnforceDailyLimitTransfer").set('checked', true);
        }
      }
      if(registry.byId("DomesticPymtDailyLimit") != "") {
        if (document.forms[0].EnforceDailyLimitDomesticPayment.checked == false) {
          document.forms[0].EnforceDailyLimitDomesticPayment.checked = true;
          registry.byId("EnforceDailyLimitDomesticPayment").set('checked', true);
        }
      }
      if(registry.byId("IntlPymtDailyLimit") != "") {
        if (document.forms[0].EnforceDailyLimitIntlPymt.checked == false) {
          document.forms[0].EnforceDailyLimitIntlPymt.checked = true;
          registry.byId("EnforceDailyLimitIntlPymt").set('checked', true);
        }
      }
    };

    local.setDailyLimitTextFields = function() {
      if(document.forms[0].EnforceDailyLimitTransfer.checked == false) {
        document.forms[0].TransferDailyLimit.value = "";
        registry.byId("TransferDailyLimit").set('value', "");
      }
      if(document.forms[0].EnforceDailyLimitDomesticPayment.checked == false) {
        document.forms[0].DomesticPymtDailyLimit.value = "";
        registry.byId("DomesticPymtDailyLimit").set('value', "");
      }
      if(document.forms[0].EnforceDailyLimitIntlPymt.checked == false) {
        document.forms[0].IntlPymtDailyLimit.value = "";
        registry.byId("IntlPymtDailyLimit").set('value', "");
      }
    };

    //	This function clears all the indicators when invoice upload is not allowed.
    //	In case with out saving user switches back to upload allowed then this method reverts
    //	all indicator states to as they were prior to select upload not allowed indicator.
    local.clearInvoiceFields = function() {

	if(document.forms[0].InvoiceManagementIndicator[0].checked) {
        InvoiceManagementIndicator1			=	document.forms[0].InvoiceManagementIndicator[0].checked;
        InvoiceManagementIndicator2			=	document.forms[0].InvoiceManagementIndicator[1].checked;
        GRInvoiceIndicator1					=	document.forms[0].GRInvoiceIndicator[0].checked;
        GRInvoiceIndicator2					=	document.forms[0].GRInvoiceIndicator[1].checked;
        RestrictedInvoiceUploadIndicator1	=	document.forms[0].RestrictedInvoiceUploadIndicator[0].checked;
        RestrictedInvoiceUploadIndicator2	=	document.forms[0].RestrictedInvoiceUploadIndicator[1].checked;
        SpecifyRestrictedInvoiceUploadDir	=	document.forms[0].SpecifyRestrictedInvoiceUploadDir.value;
        InterestFinanceAmountIndicator1		=	document.forms[0].InterestFinanceAmountIndicator[0].checked;
        InterestFinanceAmountIndicator2		=	document.forms[0].InterestFinanceAmountIndicator[1].checked;
        CalculateDiscountIndicator1			=	document.forms[0].CalculateDiscountIndicator[0].checked;
        CalculateDiscountIndicator2			=	document.forms[0].CalculateDiscountIndicator[1].checked;
        // DK IR - T36000011610 Rel8.2 02/14/2013 Starts
        FailedInvoiceIndicator1			    =	document.forms[0].FailedInvoiceIndicator[0].checked;
        FailedInvoiceIndicator2			    =	document.forms[0].FailedInvoiceIndicator[1].checked;
        // DK IR - T36000011610 Rel8.2 02/14/2013 Ends
        // DK IR - T36000014465 Rel8.2 03/01/2013 Starts
        LoanRequestsFromInvoiceUploadIndicator = document.forms[0].LoanRequestsFromInvoiceUploadIndicator.checked;
        ATPINVOnlyFromInvoiceUploadInd = document.forms[0].ATPINVOnlyFromInvoiceUploadInd.checked;
        // DK IR - T36000014465 Rel8.2 03/01/2013 Ends
        //Rel9.0 CR 913 Start
        PayablesInvoicesIndicator 			=	document.forms[0].PayablesInvoicesIndicator.checked;
        ReceivablesInvoicesIndicator		=	document.forms[0].ReceivablesInvoicesIndicator.checked;
        PayablesInvoicesGrouped				=	document.forms[0].PayablesInvoicesGrouped.checked;
        PayablesInvoicesIntegrated			=	document.forms[0].PayablesInvoicesIntegrated.checked;
        ReceivablesProgramUtilised			=	document.forms[0].ReceivablesProgramUtilised.checked;
        
        registry.getEnclosingWidget(document.forms[0].PayablesInvoicesIndicator).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ReceivablesInvoicesIndicator).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].PayablesInvoicesGrouped).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].PayablesInvoicesIntegrated).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ReceivablesProgramUtilised).set('checked',false);
        //Rel9.0 CR 913 End
        registry.getEnclosingWidget(document.forms[0].GRInvoiceIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].GRInvoiceIndicator[1]).set('checked',false);

        document.forms[0].SpecifyRestrictedInvoiceUploadDir.value		= '';

        registry.getEnclosingWidget(document.forms[0].RestrictedInvoiceUploadIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].RestrictedInvoiceUploadIndicator[1]).set('checked',false);

        registry.getEnclosingWidget(document.forms[0].InterestFinanceAmountIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].InterestFinanceAmountIndicator[1]).set('checked',false);

        registry.getEnclosingWidget(document.forms[0].CalculateDiscountIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].CalculateDiscountIndicator[1]).set('checked',false);
        
        // DK IR - T36000011610 Rel8.2 02/14/2013 Starts
        registry.getEnclosingWidget(document.forms[0].FailedInvoiceIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].FailedInvoiceIndicator[1]).set('checked',false);
        // DK IR - T36000011610 Rel8.2 02/14/2013 Ends
        
        // DK IR - T36000014465 Rel8.2 03/01/2013 Starts
        registry.getEnclosingWidget(document.forms[0].LoanRequestsFromInvoiceUploadIndicator).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].ATPINVOnlyFromInvoiceUploadInd).set('checked',false);
        // DK IR - T36000014465 Rel8.2 03/01/2013 Ends        
        }

      if(document.forms[0].InvoiceManagementIndicator[1].checked) {

        if (InvoiceManagementIndicator1) {
          document.forms[0].SpecifyRestrictedInvoiceUploadDir.value		= SpecifyRestrictedInvoiceUploadDir;

          registry.getEnclosingWidget(document.forms[0].GRInvoiceIndicator[0]).set('checked',GRInvoiceIndicator1);
          registry.getEnclosingWidget(document.forms[0].GRInvoiceIndicator[1]).set('checked',GRInvoiceIndicator2);
          registry.getEnclosingWidget(document.forms[0].RestrictedInvoiceUploadIndicator[0]).set('checked',RestrictedInvoiceUploadIndicator1);
          registry.getEnclosingWidget(document.forms[0].RestrictedInvoiceUploadIndicator[1]).set('checked',RestrictedInvoiceUploadIndicator2);
          registry.getEnclosingWidget(document.forms[0].InterestFinanceAmountIndicator[0]).set('checked',InterestFinanceAmountIndicator1);
          registry.getEnclosingWidget(document.forms[0].InterestFinanceAmountIndicator[1]).set('checked',InterestFinanceAmountIndicator2);
          registry.getEnclosingWidget(document.forms[0].CalculateDiscountIndicator[0]).set('checked',CalculateDiscountIndicator1);
          registry.getEnclosingWidget(document.forms[0].CalculateDiscountIndicator[1]).set('checked',CalculateDiscountIndicator2);
          // DK IR - T36000011610 Rel8.2 02/14/2013 Starts
          registry.getEnclosingWidget(document.forms[0].FailedInvoiceIndicator[0]).set('checked',FailedInvoiceIndicator1);
          registry.getEnclosingWidget(document.forms[0].FailedInvoiceIndicator[1]).set('checked',FailedInvoiceIndicator2);
          // DK IR - T36000011610 Rel8.2 02/14/2013 Ends
          // DK IR - T36000014465 Rel8.2 03/01/2013 Starts
          registry.getEnclosingWidget(document.forms[0].LoanRequestsFromInvoiceUploadIndicator).set('checked',LoanRequestsFromInvoiceUploadIndicator);
          registry.getEnclosingWidget(document.forms[0].ATPINVOnlyFromInvoiceUploadInd).set('checked',ATPINVOnlyFromInvoiceUploadInd);
          // DK IR - T36000014465 Rel8.2 03/01/2013 Ends
          //Rel9.0 CR 913 Start
          registry.getEnclosingWidget(document.forms[0].PayablesInvoicesIndicator).set('checked',PayablesInvoicesIndicator);
          registry.getEnclosingWidget(document.forms[0].ReceivablesInvoicesIndicator).set('checked',ReceivablesInvoicesIndicator);
          registry.getEnclosingWidget(document.forms[0].PayablesInvoicesGrouped).set('checked',PayablesInvoicesGrouped);
          registry.getEnclosingWidget(document.forms[0].PayablesInvoicesIntegrated).set('checked',PayablesInvoicesIntegrated);
          registry.getEnclosingWidget(document.forms[0].ReceivablesProgramUtilised).set('checked',ReceivablesProgramUtilised);
          //Rel9.0 CR 913 End
        }

        InvoiceManagementIndicator1			=	document.forms[0].InvoiceManagementIndicator[0].checked;
        InvoiceManagementIndicator2			=	document.forms[0].InvoiceManagementIndicator[1].checked;
        GRInvoiceIndicator1					=	document.forms[0].GRInvoiceIndicator[0].checked;
        GRInvoiceIndicator2					=	document.forms[0].GRInvoiceIndicator[1].checked;
        RestrictedInvoiceUploadIndicator1	=	document.forms[0].RestrictedInvoiceUploadIndicator[0].checked;
        RestrictedInvoiceUploadIndicator2	=	document.forms[0].RestrictedInvoiceUploadIndicator[1].checked;
        SpecifyRestrictedInvoiceUploadDir	=	document.forms[0].SpecifyRestrictedInvoiceUploadDir.value;
        InterestFinanceAmountIndicator1		=	document.forms[0].InterestFinanceAmountIndicator[0].checked;
        InterestFinanceAmountIndicator2		=	document.forms[0].InterestFinanceAmountIndicator[1].checked;
        CalculateDiscountIndicator1			=	document.forms[0].CalculateDiscountIndicator[0].checked;
        CalculateDiscountIndicator2			=	document.forms[0].CalculateDiscountIndicator[1].checked;
        // DK IR - T36000011610 Rel8.2 02/14/2013 Starts
        FailedInvoiceIndicator1			    =	document.forms[0].FailedInvoiceIndicator[0].checked;
        FailedInvoiceIndicator2			    =	document.forms[0].FailedInvoiceIndicator[1].checked;
        // DK IR - T36000011610 Rel8.2 02/14/2013 Ends
        // DK IR - T36000014465 Rel8.2 03/01/2013 Starts
        LoanRequestsFromInvoiceUploadIndicator = document.forms[0].LoanRequestsFromInvoiceUploadIndicator.checked;
        ATPINVOnlyFromInvoiceUploadInd = document.forms[0].ATPINVOnlyFromInvoiceUploadInd.checked;
        // DK IR - T36000014465 Rel8.2 03/01/2013 Ends
        //Rel9.0 CR 913 Start
        PayablesInvoicesIndicator 			=	document.forms[0].PayablesInvoicesIndicator.checked;
        ReceivablesInvoicesIndicator		=	document.forms[0].ReceivablesInvoicesIndicator.checked;
        PayablesInvoicesGrouped				=	document.forms[0].PayablesInvoicesGrouped.checked;
        PayablesInvoicesIntegrated			=	document.forms[0].PayablesInvoicesIntegrated.checked;
        ReceivablesProgramUtilised			=	document.forms[0].ReceivablesProgramUtilised.checked;
        //Rel9.0 CR 913 End
	  }

    };

    local.clearInvoiceDiscountFields = function() {
      if(document.forms[0].InterestFinanceAmountIndicator[0].checked) {
        InterestFinanceAmountIndicator1 = document.forms[0].InterestFinanceAmountIndicator[0].checked;
        InterestFinanceAmountIndicator2 = document.forms[0].InterestFinanceAmountIndicator[1].checked;
        CalculateDiscountIndicator1 = document.forms[0].CalculateDiscountIndicator[0].checked;
        CalculateDiscountIndicator2 = document.forms[0].CalculateDiscountIndicator[1].checked;

        registry.getEnclosingWidget(document.forms[0].CalculateDiscountIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].CalculateDiscountIndicator[1]).set('checked',false);
      }
      if(document.forms[0].InterestFinanceAmountIndicator[1].checked) {
        if (InterestFinanceAmountIndicator1) {
          registry.getEnclosingWidget(document.forms[0].CalculateDiscountIndicator[0]).set('checked',CalculateDiscountIndicator1);
          registry.getEnclosingWidget(document.forms[0].CalculateDiscountIndicator[1]).set('checked',CalculateDiscountIndicator2);
        }

        InterestFinanceAmountIndicator1 = document.forms[0].InterestFinanceAmountIndicator[0].checked;
        InterestFinanceAmountIndicator2 = document.forms[0].InterestFinanceAmountIndicator[1].checked;
        CalculateDiscountIndicator1 = document.forms[0].CalculateDiscountIndicator[0].checked;
        CalculateDiscountIndicator2 = document.forms[0].CalculateDiscountIndicator[1].checked;
      }
    };

    local.setNettedDiscountFiled = function() {
      if(document.forms[0].CalculateDiscountIndicator[0].checked || document.forms[0].CalculateDiscountIndicator[1].checked) {
        registry.getEnclosingWidget(document.forms[0].InterestFinanceAmountIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].InterestFinanceAmountIndicator[1]).set('checked',true);
      }
    };

    local.clearInvoiceUploadDir = function() {

      if(document.forms[0].RestrictedInvoiceUploadIndicator[0].checked) {
        invoiceUploadDir	=	document.forms[0].SpecifyRestrictedInvoiceUploadDir.value;
        document.forms[0].SpecifyRestrictedInvoiceUploadDir.value = '';
      }

      if(document.forms[0].RestrictedInvoiceUploadIndicator[1].checked) {
        if (document.forms[0].SpecifyRestrictedInvoiceUploadDir.value != '' & invoiceUploadDir=='') {
          invoiceUploadDir	=	document.forms[0].SpecifyRestrictedInvoiceUploadDir.value;
        }
        document.forms[0].SpecifyRestrictedInvoiceUploadDir.value = invoiceUploadDir;
      }
    };

    local.setRestrictedInvoiceUploadIndicator = function() {
      if (document.forms[0].SpecifyRestrictedInvoiceUploadDir.value != "") {
        registry.getEnclosingWidget(document.forms[0].RestrictedInvoiceUploadIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].RestrictedInvoiceUploadIndicator[1]).set('checked',true);
      }
      else {
        registry.getEnclosingWidget(document.forms[0].RestrictedInvoiceUploadIndicator[0]).set('checked',true);
        registry.getEnclosingWidget(document.forms[0].RestrictedInvoiceUploadIndicator[1]).set('checked',false);
      }
    };
	
	//Srinivasu_D CR-599 Rel8.4 11/21/2013 - start
	 local.clearPayMgmtUploadDir = function() {

      if(document.forms[0].PayMgmtDirectoryIndicator[0].checked) {
      
        document.forms[0].SpecifyRestrictedPayMgmtUploadDir.value = '';
      }
	
    };

	 local.setRestrictedPayMgmtUploadIndicator = function() {
      if (document.forms[0].SpecifyRestrictedPayMgmtUploadDir.value != "") {
        registry.getEnclosingWidget(document.forms[0].PayMgmtDirectoryIndicator[0]).set('checked',false);
        registry.getEnclosingWidget(document.forms[0].PayMgmtDirectoryIndicator[1]).set('checked',true);
      }
      else {
        registry.getEnclosingWidget(document.forms[0].PayMgmtDirectoryIndicator[0]).set('checked',true);
        registry.getEnclosingWidget(document.forms[0].PayMgmtDirectoryIndicator[1]).set('checked',false);
      }
    };

	//Srinivasu_D CR-599 Rel8.4 11/21/2013 - End

    local.setAccountFxRateGroup = function(corporateOrgOid) {

      var tbl = document.getElementById('customer');//to identify the table in which the row will get insert
      var lastElement = tbl.rows.length;
      var index = lastElement -1;

      if(corporateOrgOid == "0"){
        for(var i=0; i<index; i++){
          
          document.getElementById("fxRateGroup"+i).value = document.CorporateCustomerDetailForm.FXRateGroup.value;
          document.getElementById("fxRateGroupF"+i).value = document.CorporateCustomerDetailForm.FXRateGroup.value;
        }
      }
      else {
        for(var i=0; i<index ;i++){
          var acctFxRateGrp = document.getElementById("fxRateGroup"+i).value;
          var ValOtherCorpsAccount = document.getElementById("ValOtherCorpsAccount"+i).value;
          
          //if (!(acctFxRateGrp !=  "" && acctFxRateGrp != document.CorporateCustomerDetailForm.FXRateGroup.value))
          if (ValOtherCorpsAccount != 'Y') {
            document.getElementById("fxRateGroup"+i).value = document.CorporateCustomerDetailForm.FXRateGroup.value;
            document.getElementById("fxRateGroupF"+i).value = document.CorporateCustomerDetailForm.FXRateGroup.value;
          }
        }
      }
    }

    local.add4MoreAlias = function() {
      var myTable = dom.byId('aliasTable');
      common.appendAjaxTableRows(myTable,"aliasIndex",
        "/portal/refdata/CorporateCustomerAddAliasRows.jsp?aliasIndex="+
          (myTable.rows.length-1)); //table length includes header, so subtract 1
    }

    local.add4MoreRules = function(){
      var myTable = dom.byId('marginTable'); 
      common.appendAjaxTableRows(myTable,"marginIndex",
        "/portal/refdata/CorporateCustomerAddMarginRows.jsp?marginIndex="+
          (myTable.rows.length-1)); //table length includes header, so subtract 1

    }
    
    local.add4MoreSPMarginRules = function(){
        var myTable = dom.byId('SpMarginTable');
        document.getElementById('noOfSPMarginRules' ).value = myTable.rows.length + 4;
        common.appendAjaxTableRows(myTable,"spMarginIndex",
          "/portal/refdata/CorporateCustomerAddSPMarginRows.jsp?spMarginIndex="+(myTable.rows.length-1)); //table length includes header, so subtract 1
      }

    local.disableDaysBeforeAfter = function(disable) {
      if (disable) {
        var dBefore = registry.byId('DaysBefore');
        if ( dBefore ) {
          dBefore.set("disabled",true);
          dBefore.set("value","");
        }
        var dAfter = registry.byId('DaysAfter');
        if ( dAfter ) {
          dAfter.set("disabled",true);
          dAfter.set("value","");
        }
      }
      else {
        var dBefore = registry.byId('DaysBefore');
        if ( dBefore ) {
          dBefore.set("disabled",false);
        }
        var dAfter = registry.byId('DaysAfter');
        if ( dAfter ) {
          dAfter.set("disabled",false);
        }
      }
    }

    local.enableMarginThresholdAmt = function(marginIdx) {
      var thrCurWidget = registry.byId("marginCurr"+marginIdx);
      var thrAmtWidget = registry.byId("thresholdAmount"+marginIdx);
      if ( thrCurWidget && thrAmtWidget ) {
        var thrCur = thrCurWidget.get('value');
        if ( thrCur && thrCur.length>0 ) {
          thrAmtWidget.set('disabled',false);
        }
        else {
          thrAmtWidget.set('disabled',true);
        }
      }
    }
   
    //DK - CR709 Rel8.2 10/31/2012 Begin
    ready(function() {   	
  	  
    var processInvoicesFileUploadInd = registry.byId("ProcessInvoicesFileUploadIndicator"); 
    var tradeLoanInd = registry.byId("TradeLoanIndicator");
    var importLoanInd = registry.byId("ImportLoanIndicator");
    var exportLoanInd = registry.byId("ExportLoanIndicator");
    var invoiceFinancingInd = registry.byId("InvoiceFinancingIndicator");
   //KMehta 9 Sep 2014 - Rel9.1 IR-T36000031738 - Change  - Begin
    var loanRequestCheckerAuthInd = registry.byId("LoanRequestCheckerAuthIndicator");     
	// DK IR T36000015044 Rel8.2 04/08/2013 Starts
  	  if(processInvoicesFileUploadInd){
  		processInvoicesFileUploadInd.on("change", function(checkValue) {
  				if(this.checked){
  					registry.getEnclosingWidget(document.getElementById('LoanRequestIndicator')).set('checked', true);
  				}   
  				else if(document.forms[0].LoanRequestDualAuthorizationIndicator[0].checked==false &&
  		    		   document.forms[0].LoanRequestDualAuthorizationIndicator[1].checked==false &&
  		    		   document.forms[0].LoanRequestDualAuthorizationIndicator[2].checked==false &&
  		    		   document.forms[0].LoanRequestDualAuthorizationIndicator[3].checked==false &&
  		    		   tradeLoanInd.checked==false &&
  		    		   importLoanInd.checked==false &&
  		    		   exportLoanInd.checked==false &&
  		    		   invoiceFinancingInd.checked==false &&
  		    		   document.getElementById('LoanRequestCheckerAuthIndicator').checked==false){  					
  		    	   registry.getEnclosingWidget(document.forms[0].LoanRequestIndicator).set('checked', false);  					
  				}
  			});
  			}  
  	if(loanRequestCheckerAuthInd){
  		loanRequestCheckerAuthInd.on("change", function(checkValue) {
  				if(this.checked){
  					registry.getEnclosingWidget(document.getElementById('LoanRequestIndicator')).set('checked', true);
  				}   
  				else if(document.forms[0].LoanRequestDualAuthorizationIndicator[0].checked==false &&
  		    		   document.forms[0].LoanRequestDualAuthorizationIndicator[1].checked==false &&
  		    		   document.forms[0].LoanRequestDualAuthorizationIndicator[2].checked==false &&
  		    		   document.forms[0].LoanRequestDualAuthorizationIndicator[3].checked==false &&
 		    		   document.getElementById('ProcessInvoicesFileUploadIndicator').checked==false &&
  		    		   tradeLoanInd.checked==false &&
  		    		   importLoanInd.checked==false &&
  		    		   exportLoanInd.checked==false &&
  		    		   invoiceFinancingInd.checked==false){  					
  		    	   registry.getEnclosingWidget(document.forms[0].LoanRequestIndicator).set('checked', false);  					
  				}
  			});
  			}  
  	
  	 if(tradeLoanInd){
  		tradeLoanInd.on("change", function(checkValue) {
   				if(this.checked){
   					registry.getEnclosingWidget(document.getElementById('LoanRequestIndicator')).set('checked', true);
   				}   
   				else if(document.forms[0].LoanRequestDualAuthorizationIndicator[0].checked==false &&
   		    		   document.forms[0].LoanRequestDualAuthorizationIndicator[1].checked==false &&
   		    		   document.forms[0].LoanRequestDualAuthorizationIndicator[2].checked==false &&
   		    		   document.forms[0].LoanRequestDualAuthorizationIndicator[3].checked==false &&
   		    		   document.getElementById('ProcessInvoicesFileUploadIndicator').checked==false &&
		    		   document.getElementById('ImportLoanIndicator').checked==false &&
		    		   document.getElementById('ExportLoanIndicator').checked==false &&
		    		   document.getElementById('InvoiceFinancingIndicator').checked==false &&
  		    		   document.getElementById('LoanRequestCheckerAuthIndicator').checked==false){
   		    	   registry.getEnclosingWidget(document.forms[0].LoanRequestIndicator).set('checked', false);  					
   				}
   			});
   			} 
  	
  	if(importLoanInd){
  		importLoanInd.on("change", function(checkValue) {
			if(this.checked){
				registry.getEnclosingWidget(document.getElementById('LoanRequestIndicator')).set('checked', true);
			}   
			else if(document.forms[0].LoanRequestDualAuthorizationIndicator[0].checked==false &&
	    		   document.forms[0].LoanRequestDualAuthorizationIndicator[1].checked==false &&
	    		   document.forms[0].LoanRequestDualAuthorizationIndicator[2].checked==false &&
	    		   document.forms[0].LoanRequestDualAuthorizationIndicator[3].checked==false &&
	    		   document.getElementById('ProcessInvoicesFileUploadIndicator').checked==false &&
	    		   document.getElementById('TradeLoanIndicator').checked==false &&
	    		   document.getElementById('ExportLoanIndicator').checked==false &&
	    		   document.getElementById('InvoiceFinancingIndicator').checked==false &&
		    	   document.getElementById('LoanRequestCheckerAuthIndicator').checked==false){
	    	   registry.getEnclosingWidget(document.forms[0].LoanRequestIndicator).set('checked', false);  					
			}
		});
		} 
  	
  	if(exportLoanInd){
  		exportLoanInd.on("change", function(checkValue) {
			if(this.checked){
				registry.getEnclosingWidget(document.getElementById('LoanRequestIndicator')).set('checked', true);
			}   
			else if(document.forms[0].LoanRequestDualAuthorizationIndicator[0].checked==false &&
	    		   document.forms[0].LoanRequestDualAuthorizationIndicator[1].checked==false &&
	    		   document.forms[0].LoanRequestDualAuthorizationIndicator[2].checked==false &&
	    		   document.forms[0].LoanRequestDualAuthorizationIndicator[3].checked==false &&
	    		   document.getElementById('ProcessInvoicesFileUploadIndicator').checked==false &&
	    		   document.getElementById('TradeLoanIndicator').checked==false &&
	    		   document.getElementById('ImportLoanIndicator').checked==false &&
	    		   document.getElementById('InvoiceFinancingIndicator').checked==false &&
	    		   document.getElementById('LoanRequestCheckerAuthIndicator').checked==false){
	    	   registry.getEnclosingWidget(document.forms[0].LoanRequestIndicator).set('checked', false);  					
			}
		});
		} 
  	
  	if(invoiceFinancingInd){
  		invoiceFinancingInd.on("change", function(checkValue) {
			if(this.checked){
				registry.getEnclosingWidget(document.getElementById('LoanRequestIndicator')).set('checked', true);
			}   
			else if(document.forms[0].LoanRequestDualAuthorizationIndicator[0].checked==false &&
	    		   document.forms[0].LoanRequestDualAuthorizationIndicator[1].checked==false &&
	    		   document.forms[0].LoanRequestDualAuthorizationIndicator[2].checked==false &&
	    		   document.forms[0].LoanRequestDualAuthorizationIndicator[3].checked==false &&
	    		   document.getElementById('ProcessInvoicesFileUploadIndicator').checked==false &&
	    		   document.getElementById('TradeLoanIndicator').checked==false &&
	    		   document.getElementById('ImportLoanIndicator').checked==false &&
	    		   document.getElementById('ExportLoanIndicator').checked==false &&
	    		   document.getElementById('LoanRequestCheckerAuthIndicator').checked==false){
	    	   registry.getEnclosingWidget(document.forms[0].LoanRequestIndicator).set('checked', false);  					
			}
		});
		} 
  		
  		//Sandeep Rel-8.3 IR#T36000019765 08/08/2013 - Begin
  		if(document.forms[0].AirWaybillsDualAuthorizationIndicator[3].checked == false)
  			registry.byId('AirWaybillsPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].ApprovalToPayDualAuthorizationIndicator[3].checked == false)
  			registry.byId('ApprovalToPayPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].ExportCollectionsDualAuthorizationIndicator[3].checked == false)
  			registry.byId('ExportCollectionsPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].ExportLCDualAuthorizationIndicator[3].checked == false)
  			registry.byId('ExportLCPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].NewExportCollectionsDualAuthorizationIndicator[3].checked == false)
  			registry.byId('NewExportCollectionsPanelGroupOid').set("disabled",true);  
  		//SSikhakolli - Rel-9.4 CR-818
  		if(document.forms[0].ImportCollectionsDualAuthorizationIndicator[3].checked == false)
  			registry.byId('ImportCollectionsPanelGroupOid').set("disabled",true);  
  		if(document.forms[0].ImportDLCDualAuthorizationIndicator[3].checked == false)
  			registry.byId('ImportDLCPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].LoanRequestDualAuthorizationIndicator[3].checked == false)
  			registry.byId('LoanRequestPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].GuaranteesDualAuthorizationIndicator[3].checked == false)
  			registry.byId('GuaranteesPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].StandbyDLCDualAuthorizationIndicator[3].checked == false)
  			registry.byId('StandbyDLCPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].ShippingGuaranteesDualAuthorizationIndicator[3].checked == false)
  			registry.byId('ShippingGuaranteesPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].RequestAdviseDualAuthorizationIndicator[3].checked == false)
  			registry.byId('RequestAdvisePanelGroupOid').set("disabled",true); 		
  		if(document.forms[0].SupplierPortalDualAuthorizationIndicator[3].checked == false)
  			registry.byId('SupplierPortalPanelGroupOid').set("disabled",true);
  		//SSikhakolli - Rel-9.4 CR-818
  		if(document.forms[0].DiscrepancyResponseDualAuthorizationIndicator[3].checked == false)
  			registry.byId('DiscrepancyResponsePanelGroupOid').set("disabled",true);  
  		if(document.forms[0].SettlementInstructionDualAuthorizationIndicator[3].checked == false)
  			registry.byId('SettlementInstructionPanelGroupOid').set("disabled",true);  
  		if(document.forms[0].ReceivablesManagementDualAuthorizationIndicator[3].checked == false)
  			registry.byId('MatchingApprovalPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].ReceivablesManagementInvoiceDualAuthorizationIndicator[3].checked == false)
  			registry.byId('InvoiceAuthorizationPanelGroupOid').set("disabled",true);  		
  		if(document.forms[0].CreditNoteDualAuthorizationIndicator[3].checked == false)
  			registry.byId('CreditAuthorizationPanelGroupOid').set("disabled",true); 
  		//Sandeep Rel-8.3 IR#T36000019765 08/08/2013 - End
  		//Rel 9.0 CR 913 Start
  		if(document.forms[0].PayablesUploadInvDualAuthorizationIndicator[3].checked == false)
  			registry.byId('UploadedPayablesAuthPanelGroupOid').set("disabled",true);
  		if(document.forms[0].PayablesInvAuthDualAuthorizationIndicator[3].checked == false)
  			registry.byId('PayablesAuthPanelGroupOid').set("disabled",true);
  		//Rel 9.0 CR 913 End
  		//IR T36000019854 Rel 8.3 START
  		if(registry.getEnclosingWidget(document.forms[0].DomesticPaymentDualAuthIndicator[3]).checked == true){
            registry.getEnclosingWidget(document.forms[0].PaymentBenePanelAuthIndicator).set('disabled',false);
  		}else{
  			registry.getEnclosingWidget(document.forms[0].PaymentBenePanelAuthIndicator).set('checked',false);
  			registry.getEnclosingWidget(document.forms[0].PaymentBenePanelAuthIndicator).set('disabled',true);
  		}
  		//IR T36000019854 Rel 8.3 END
  		//CR 988 Rel9.2 START
  		if(document.forms[0].straightThroughAuthorizeIndicator.checked == true){
      	  registry.byId('allowPartialPayAuthorizeIndicator').set("disabled",false);
        }else{
      	  registry.getEnclosingWidget(document.forms[0].allowPartialPayAuthorizeIndicator).set('checked', false);
      	  registry.byId('allowPartialPayAuthorizeIndicator').set("disabled",true);
        }
  		//CR 988 END
  		//Rel9.2 CR 914 START
  		if(document.forms[0].DualAuthPayCrNote[3].checked == false)
  			registry.byId('PayCrNoteAuthPanelGroupOid').set("disabled",true); 
  		
  		if(!document.forms[0].AllowPayCrNoteUpload[0].checked && !document.forms[0].AllowPayCrNoteUpload[1].checked)  {
  			registry.getEnclosingWidget(document.forms[0].AllowPayCrNoteUpload[0]).set('checked',true);
  		}
  		
  		if(document.forms[0].AllowPayCrNoteUpload[0].checked) {
  			registry.getEnclosingWidget(document.forms[0].PayCrNoteAvailableDays).set('disabled',true);
  			registry.getEnclosingWidget(document.forms[0].AllowEndToEndProcessing).set('disabled',true);
  			registry.getEnclosingWidget(document.forms[0].AllowManualCrNoteApplication).set('disabled',true);
  			
  			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('disabled',true);
  			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('disabled',true);
  			
  			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('disabled',true);
  			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('disabled',true);
  			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('disabled',true);
  			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('disabled',true);
  			
  		}
  		
  		
  		if(document.forms[0].AllowEndToEndProcessing.checked) {
  			local.handleManualCrNoteOptions(true);
  		}else{
  			local.handleManualCrNoteOptions(false);
  		}
  		//Rel9.2 CR 914 END
  	//SSikhakolli - Rel-9.3.5 CR-1029 IR#T36000042510 - Code changes per Design Change - Begin
  		if(registry.byId('BankOrgGroupOid')){
	  		var showCheckBoxFlag = false;
	  		var oidDocArray = new Array();
	  		var textDocArray = new Array();
	  		oidDocArray = oidDocFinal.split(',');
	  		textDocArray = textDocFinal.split(',');
	  		
	  		var bankOrgGroupOid = registry.byId('BankOrgGroupOid').getValue();
	  		for (var i = 0; i < oidDocArray.length; i++){
	  			if(bankOrgGroupOid == oidDocArray[i]){
	  				if(textDocArray[i] == 'Y'){
	  					showCheckBoxFlag = true;
	  				}
	  			}
	  		}
	  		
	  		if(showCheckBoxFlag){
	  			document.getElementById('custIsNotIntgTpsDiv').style.display = "block";
	  		}else{
	  			document.getElementById('custIsNotIntgTpsDiv').style.display = "none";
	  		}
  		}
  		
  		var bankOrgGroupOid = registry.byId('BankOrgGroupOid');     
  	  	if(bankOrgGroupOid){
  	  		bankOrgGroupOid.on("change", function(value) {
	  	  		var showCheckBoxFlag = false;
		  		var oidDocArray = new Array();
		  		var textDocArray = new Array();
		  		oidDocArray = oidDocFinal.split(',');
		  		textDocArray = textDocFinal.split(',');
		  		
		  		for (var i = 0; i < oidDocArray.length; i++){
		  			if(value == oidDocArray[i]){
		  				if(textDocArray[i] == 'Y'){
		  					showCheckBoxFlag = true;
		  				}
		  			}
		  		}
		  		
		  		if(showCheckBoxFlag){
		  			document.getElementById('custIsNotIntgTpsDiv').style.display = "block";
		  		}else{
		  			document.getElementById('custIsNotIntgTpsDiv').style.display = "none";
		  		}
  	  		});
  	  	} 
  	//SSikhakolli - Rel-9.3.5 CR-1029 IR#T36000042510 - Code changes per Design Change - End
    });//End of Ready
    
    local.handleManualCrNoteOptions = function(disable){
    	
     		if (disable){
     			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('disabled',true);
    	  		registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('disabled',true);
       			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('disabled',true);
      			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('disabled',true);
      			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('disabled',true);
      			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('disabled',true);
    		}else{
    			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('disabled',false);
    	  		registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('disabled',false);
       			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('disabled',false);
      			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('disabled',false);
      			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('disabled',false);
      			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('disabled',false);
    		}
    }
    
    
    
    
  //DK - CR709 Rel8.2 10/31/2012 End    
    local.enableSPMarginThresholdAmt = function(marginIdx) {
      var thrCurWidget = registry.byId("SpMarginCurr"+marginIdx);
      var thrAmtWidget = registry.byId("SpThresholdAmount"+marginIdx);
      if ( thrCurWidget && thrAmtWidget ) {
        var thrCur = thrCurWidget.get('value');
        if ( thrCur && thrCur.length>0 ) {
          thrAmtWidget.set('readonly',false);
        }
        else {
        	thrAmtWidget.set('value','');
        	thrAmtWidget.set('readonly',true);
        }
      }
    }
    //Sandeep CR708BC IR# T36000015147 03/25/2013 - Start
    local.enableDisableSPMargin = function(marginIdx) {
    	var spRateTypeWidget = registry.byId("SpRateType"+marginIdx);
        var spMarginWidget = registry.byId("SpMargin"+marginIdx);
        
        if(spRateTypeWidget.value == "FXD"){
        	spMarginWidget.set('disabled', true);
        	spMarginWidget.set('value', '0.00');
        }else{
        	spMarginWidget.set('disabled', false);
        	spMarginWidget.set('value', '');
        }
    }
  //Sandeep CR708BC IR# T36000015147 03/25/2013 - End
  //Added for CR 821 - Rel 8.3 START
    local.setAWBPanelRadioButton = function(){
    	if(dijit.byId(AirWaybillsPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].AirWaybillsDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].AirWaybillsIndicator).set('checked',true);
    	}
    }
    local.setATPPanelRadioButton = function(){
    	if(dijit.byId(ApprovalToPayPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].ApprovalToPayDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].ApprovalToPayIndicator).set('checked',true);
    	}
    }
    local.setECOLPanelRadioButton = function(){
    	if(dijit.byId(ExportCollectionsPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].ExportCollectionsDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].ExportCollectionsIndicator).set('checked',true);
    	}
    }
    local.setELCPanelRadioButton = function(){
    	if(dijit.byId(ExportLCPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].ExportLCDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].ExportLettersOfCreditIndicator).set('checked',true);
    	}
    }
    local.setNECOLPanelRadioButton = function(){
    	if(dijit.byId(NewExportCollectionsPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].NewExportCollectionsDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].NewExportCollectionsIndicator).set('checked',true);
    	}
    }
  //SSikhakolli - Rel-9.4 CR-818
    local.setImportCOLPanelRadioButton = function(){
    	if(dijit.byId(ImportCollectionsPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].ImportCollectionsDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].ImportCollectionsIndicator).set('checked',true);
    	}
    }
    local.setILCPanelRadioButton = function(){
    	if(dijit.byId(ImportDLCPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].ImportDLCDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].ImportLettersOfCreditIndicator).set('checked',true);
    	}
    }
    local.setLRPanelRadioButton = function(){
    	if(dijit.byId(LoanRequestPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].LoanRequestDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].LoanRequestIndicator).set('checked',true);
    	}
    }
    local.setGTEEPanelRadioButton = function(){
    	if(dijit.byId(GuaranteesPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].GuaranteesDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].GuaranteesIndicator).set('checked',true);
    	}
    }
    local.setSLCPanelRadioButton = function(){
    	if(dijit.byId(StandbyDLCPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].StandbyDLCDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].StandbyLettersOfCreditIndicator).set('checked',true);
    	}
    }
    local.setSGTEEPanelRadioButton = function(){
    	if(dijit.byId(ShippingGuaranteesPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].ShippingGuaranteesDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].ShippingGuaranteesIndicator).set('checked',true);
    	}
    }
    local.setRAPanelRadioButton = function(){
    	if(dijit.byId(RequestAdvisePanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].RequestAdviseDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].RequestAdviseIndicator).set('checked',true);
    	}
    }
    local.setSPPanelRadioButton = function(){
    	if(dijit.byId(SupplierPortalPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].SupplierPortalDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].SupplierPortalIndicator).set('checked',true);
    	}
    }
  //SSikhakolli - Rel-9.4 CR-818
    local.setDCRPanelRadioButton = function(){
    	if(dijit.byId(DiscrepancyResponsePanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].DiscrepancyResponseDualAuthorizationIndicator[3]).set('checked',true);
    	}
    }
    local.setSIPanelRadioButton = function(){
    	if(dijit.byId(SettlementInstructionPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].SettlementInstructionDualAuthorizationIndicator[3]).set('checked',true);
    	}
    }
    local.setRMPanelRadioButton = function(){
    	if(dijit.byId(MatchingApprovalPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].ReceivablesManagementDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].ReceivablesManagementIndicator).set('checked',true);
    	}
    }
    local.setRMInvoicePanelRadioButton = function(){
    	if(dijit.byId(InvoiceAuthorizationPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].ReceivablesManagementInvoiceDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].ReceivablesManagementInvoiceIndicator).set('checked',true);
    	}
    }
    local.setRMCreditNotePanelRadioButton = function(){
    	if(dijit.byId(CreditAuthorizationPanelGroupOid).value!=""){
    	registry.getEnclosingWidget(document.forms[0].CreditNoteDualAuthorizationIndicator[3]).set('checked',true);
    	registry.getEnclosingWidget(document.forms[0].CreditNoteIndicator).set('checked',true);
    	}
    }
    
    local.add2MorePanels = function() {
    	 var acctGrid = registry.byId("acctGrid");
    	 var items = acctGrid.selection.getSelected();
         if (items.length<=0) return; 
          baseArray.forEach(items, function(item){
           selectedAcctDivId = this.store.getValue(item, "divId");
         }, acctGrid);
         var myDivIdIdx = selectedAcctDivId.substring("accountHidden".length);
    	var corporateOrgOid = dom.byId("corporateOrgOid").value;
        var isReadOnly = dom.byId("isReadOnly").value;
        var myTable = dom.byId('panelTable');
        if((myTable.rows.length-1) == 7){
        	var addButton = registry.byId("add2MorePanel");
            if ( addButton ) {
            	addButton.set('disabled',true);
            }
        }
        common.appendAjaxTableRows(myTable,"panelIndex",
          "/portal/refdata/CorporateCustomerAddPanelRows.jsp?rowIndex="+(myTable.rows.length-1)+"&panelIndex="+
            (myTable.rows.length-1)*2+"&corporateOrgOid="+corporateOrgOid+"&isReadOnly="+isReadOnly+"&myDivIdIdx="+myDivIdIdx); //table length includes header, so subtract 1
      }
    local.add2MorePanelsDefault = function() {
   	   var corporateOrgOid = dom.byId("corporateOrgOid").value;
       var isReadOnly = dom.byId("isReadOnly").value;
       var myTable = dom.byId('panelTable');
       if((myTable.rows.length-1) == 7){
       	var addButton = registry.byId("add2MorePanel");
           if ( addButton ) {
           	addButton.set('disabled',true);
           }
       }
       common.appendAjaxTableRows(myTable,"panelIndex",
         "/portal/refdata/CorporateCustomerAddPanelRows.jsp?rowIndex="+(myTable.rows.length-1)+"&panelIndex="+
           (myTable.rows.length-1)*2+"&corporateOrgOid="+corporateOrgOid+"&isReadOnly="+isReadOnly); //table length includes header, so subtract 1
     }
      //CR 821 - Rel 8.3 END

    //Rel9.0 CR 913 Starts    
    local.setPayAuthRadioButtons = function() {
        if (document.forms[0].PayablesInvoiceUploadedAuthIndicator.checked == false) {
          registry.getEnclosingWidget(document.forms[0].PayablesUploadInvDualAuthorizationIndicator[0]).set('checked',false);
          registry.getEnclosingWidget(document.forms[0].PayablesUploadInvDualAuthorizationIndicator[1]).set('checked',false);
          registry.getEnclosingWidget(document.forms[0].PayablesUploadInvDualAuthorizationIndicator[2]).set('checked',false);
          registry.getEnclosingWidget(document.forms[0].PayablesUploadInvDualAuthorizationIndicator[3]).set('checked',false);
          registry.byId('UploadedPayablesAuthPanelGroupOid').set("disabled",true);
        }
      }
    local.setPayAuthCheckBox = function() {
        registry.getEnclosingWidget(document.forms[0].PayablesInvoiceUploadedAuthIndicator).set('checked', true);
        
        if(document.forms[0].PayablesUploadInvDualAuthorizationIndicator[3].checked == false){
      	  registry.byId('UploadedPayablesAuthPanelGroupOid').set("disabled",true);
        }else{
       	  registry.byId('UploadedPayablesAuthPanelGroupOid').set("disabled",false);
        }        
      };
      
      local.setPayMgmtRadioButtons = function() {
          if (document.forms[0].PayablesInvoiceAuthIndicator.checked == false) {
            registry.getEnclosingWidget(document.forms[0].PayablesInvAuthDualAuthorizationIndicator[0]).set('checked',false);
            registry.getEnclosingWidget(document.forms[0].PayablesInvAuthDualAuthorizationIndicator[1]).set('checked',false);
            registry.getEnclosingWidget(document.forms[0].PayablesInvAuthDualAuthorizationIndicator[2]).set('checked',false);
            registry.getEnclosingWidget(document.forms[0].PayablesInvAuthDualAuthorizationIndicator[3]).set('checked',false);
            registry.byId('PayablesAuthPanelGroupOid').set("disabled",true);
          }
        }
      local.setPayMgmtCheckBox = function() {
          registry.getEnclosingWidget(document.forms[0].PayablesInvoiceAuthIndicator).set('checked', true);
          
          if(document.forms[0].PayablesInvAuthDualAuthorizationIndicator[3].checked == false){
        	  registry.byId('PayablesAuthPanelGroupOid').set("disabled",true);
          }else{
         	  registry.byId('PayablesAuthPanelGroupOid').set("disabled",false);
          }        
        }
        
     local.setInvoiceUploadIndicator = function(){
    	 if(document.forms[0].PayablesInvoicesIndicator.checked || document.forms[0].ReceivablesInvoicesIndicator.checked){
    		 registry.getEnclosingWidget(document.forms[0].InvoiceManagementIndicator[1]).set('checked',true);
    		 registry.getEnclosingWidget(document.forms[0].InvoiceManagementIndicator[0]).set('checked',false);
    	 }
     };   
     //Rel9.0 CR 913 Ends      
     
     //Rel9.2 CR 914 START
     local.clearPayCrNoteFields = function(){
    	 if(document.forms[0].AllowPayCrNoteUpload[0].checked) {
   			
    		 registry.getEnclosingWidget(document.forms[0].PayCrNoteAvailableDays).set('disabled',true);
   			registry.getEnclosingWidget(document.forms[0].AllowEndToEndProcessing).set('disabled',true);
   			registry.getEnclosingWidget(document.forms[0].AllowManualCrNoteApplication).set('disabled',true);
   			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('disabled',true);
   			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('disabled',true);
   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('disabled',true);
   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('disabled',true);
   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('disabled',true);
   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('disabled',true);
   			
   			registry.getEnclosingWidget(document.forms[0].PayCrNoteAvailableDays).set('checked',false);
   			registry.getEnclosingWidget(document.forms[0].AllowEndToEndProcessing).set('checked',false);
   			registry.getEnclosingWidget(document.forms[0].AllowManualCrNoteApplication).set('checked',false);
   			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('checked',false);
   			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('checked',false);
   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('checked',false);
   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('checked',false);
   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('checked',false);
   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('checked',false);
   			
   			registry.getEnclosingWidget(document.forms[0].PayCrNoteAvailableDays).set('value','');
   			
   		}else{
   			registry.getEnclosingWidget(document.forms[0].PayCrNoteAvailableDays).set('disabled',false);
   			registry.getEnclosingWidget(document.forms[0].AllowEndToEndProcessing).set('disabled',false);
   			registry.getEnclosingWidget(document.forms[0].AllowManualCrNoteApplication).set('disabled',false);
   			if((document.forms[0].AllowEndToEndProcessing.checked == false && document.forms[0].AllowManualCrNoteApplication.checked == false)
   					|| (document.forms[0].AllowEndToEndProcessing.checked == true && document.forms[0].AllowManualCrNoteApplication.checked == false)){
	   			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('disabled',true);
	   			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('disabled',true);
	   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('disabled',true);
	   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('disabled',true);
	   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('disabled',true);
	   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('disabled',true);
   			}else if(document.forms[0].AllowManualCrNoteApplication.checked){
   				registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('disabled',false);
	   			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('disabled',false);
	   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('disabled',false);
	   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('disabled',false);
	   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('disabled',false);
	   			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('disabled',false);
   			}
   		}
     };
     
     local.disableApplyCrNotesButtons = function(){
    	 	 	
    	 		registry.getEnclosingWidget(document.forms[0].AllowManualCrNoteApplication).set('checked',false);
    		 	registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('disabled',true);
    			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('disabled',true);
    			
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('disabled',true);
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('disabled',true);
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('disabled',true);
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('disabled',true);
    			
    		 	registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('checked',false);
    			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('checked',false);
    			
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('checked',false);
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('checked',false);
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('checked',false);
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('checked',false);
    			
     };
     
     local.enableApplyCrNotesButtons = function(){
    	
    	 if(document.forms[0].AllowManualCrNoteApplication.checked){
    		 	
    		 	registry.getEnclosingWidget(document.forms[0].AllowEndToEndProcessing).set('checked',false);
    		 	registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('disabled',false);
    			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('disabled',false);
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('disabled',false);
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('disabled',false);
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('disabled',false);
    			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('disabled',false);
    			
    	 }else{
	    		registry.getEnclosingWidget(document.forms[0].ApplicationMethod[0]).set('disabled',true);
	 			registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('disabled',true);
	 			registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('disabled',true);
	 			registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('disabled',true);
	 			registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('disabled',true);
	 			registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('disabled',true); 
    	 }
     };
     
     local.setSequentialButton = function(){
    	 if(document.forms[0].SequentialMethod[0].checked || document.forms[0].SequentialMethod[1].checked || document.forms[0].SequentialMethod[2].checked || document.forms[0].SequentialMethod[3].checked){
    		 registry.getEnclosingWidget(document.forms[0].ApplicationMethod[1]).set('checked',true);
    	 }
     };
     local.clearSequentialButtons = function(){
    	 if(document.forms[0].ApplicationMethod[0].checked ){
    		 	registry.getEnclosingWidget(document.forms[0].SequentialMethod[0]).set('checked',false);
 				registry.getEnclosingWidget(document.forms[0].SequentialMethod[1]).set('checked',false);
 				registry.getEnclosingWidget(document.forms[0].SequentialMethod[2]).set('checked',false);
 				registry.getEnclosingWidget(document.forms[0].SequentialMethod[3]).set('checked',false);
    	 }
     };
     local.setPMCreditNoteRadioButtons = function() {
         if (document.forms[0].AllowPayCrNote.checked == false) {
           registry.getEnclosingWidget(document.forms[0].DualAuthPayCrNote[0]).set('checked',false);
           registry.getEnclosingWidget(document.forms[0].DualAuthPayCrNote[1]).set('checked',false);
           registry.getEnclosingWidget(document.forms[0].DualAuthPayCrNote[2]).set('checked',false);
           registry.getEnclosingWidget(document.forms[0].DualAuthPayCrNote[3]).set('checked',false);
           registry.getEnclosingWidget(document.forms[0].DualAuthPayCrNote[4]).set('checked',false);
           registry.byId('PayCrNoteAuthPanelGroupOid').set("disabled",true);
         }
       };
       local.setPMCreditNoteCheckBox = function() {
    	      registry.getEnclosingWidget(document.forms[0].AllowPayCrNote).set('checked', true);
    	      
    	      if(document.forms[0].DualAuthPayCrNote[3].checked == false){
    	    	  registry.byId('PayCrNoteAuthPanelGroupOid').set("disabled",true);
    	      }else{
    	     	  registry.byId('PayCrNoteAuthPanelGroupOid').set("disabled",false);
    	      }       
       };
       
       local.setPMCreditNotePanelRadioButton = function(){
       	if(dijit.byId(PayCrNoteAuthPanelGroupOid).value!=""){
       	registry.getEnclosingWidget(document.forms[0].DualAuthPayCrNote[3]).set('checked',true);
       	registry.getEnclosingWidget(document.forms[0].AllowPayCrNote).set('checked',true);
       	}
       };
     //Rel9.2 CR 914 END
	 //#RKAZI CR1006 04-24-2015  - BEGIN
          local.setRMDeclineInvoiceRadioButtons = function() {
              if (document.forms[0].ReceivablesManagementDeclineInvoiceIndicator.checked == false) {
                  registry.getEnclosingWidget(document.forms[0].ReceivablesManagementDeclineInvoiceDualIndicator[0]).set('checked',false);
                  registry.getEnclosingWidget(document.forms[0].ReceivablesManagementDeclineInvoiceDualIndicator[1]).set('checked',false);
                  registry.getEnclosingWidget(document.forms[0].ReceivablesManagementDeclineInvoiceDualIndicator[2]).set('checked',false);
              }
          };
          //This function sets the Receivables Management Invoice authorisation check box to checked whenever a
          //user clicks on one of the radio buttons associated with it.
          local.setDeclineRMInvoiceCheckBox = function() {
              registry.getEnclosingWidget(document.forms[0].ReceivablesManagementDeclineInvoiceIndicator).set('checked', true);


          };
          local.setPayMgmtDeclineRadioButtons = function() {
              if (document.forms[0].PayablesInvoiceDeclineIndicator.checked == false) {
                  registry.getEnclosingWidget(document.forms[0].PayablesInvDeclineDualIndicator[0]).set('checked',false);
                  registry.getEnclosingWidget(document.forms[0].PayablesInvDeclineDualIndicator[1]).set('checked',false);
                  registry.getEnclosingWidget(document.forms[0].PayablesInvDeclineDualIndicator[2]).set('checked',false);
              }
          };
          local.setPayMgmtDeclineCheckBox = function() {
              registry.getEnclosingWidget(document.forms[0].PayablesInvoiceDeclineIndicator).set('checked', true);
          };
          local.setPMDeclineCreditNoteRadioButtons = function() {
              if (document.forms[0].DeclinePayCrNote.checked == false) {
                  registry.getEnclosingWidget(document.forms[0].DualDeclinePayCrNote[0]).set('checked',false);
                  registry.getEnclosingWidget(document.forms[0].DualDeclinePayCrNote[1]).set('checked',false);
                  registry.getEnclosingWidget(document.forms[0].DualDeclinePayCrNote[2]).set('checked',false);
              }
          };
          local.setPMDeclineCreditNoteCheckBox = function() {
              registry.getEnclosingWidget(document.forms[0].DeclinePayCrNote).set('checked', true);

          };
		//#RKAZI CR1006 04-24-2015  - END
          
          
        //SURREWSH -Rel-9.4 CR-932 Start
          local.setBIndicator = function(indicatorVlaue) {
         	 
              registry.getEnclosingWidget(document.forms[0].BillingInstrumentsIndicator).set('checked', indicatorVlaue);
              registry.getEnclosingWidget(document.forms[0].BillingInstrumentsAllowedIndicator).set('checked', indicatorVlaue);
         };
       //SURREWSH -Rel-9.4 CR-932 End

      });