require(["dojo/_base/array", "dojo/dom",
         "dojo/dom-construct", "dojo/dom-class", 
         "dojo/dom-style", "dojo/query",
         "dijit/registry", "dojo/on", 
         "t360/common",
         "dojo/ready", "dojo/NodeList-traverse"],
    function(arrayUtil, dom,
             domConstruct, domClass,
             domStyle, query,
             registry, on, 
             common, 
             ready ) {
	 
		
	 
	 local.add4MoreDefined = function(tableToIncrease) {
	 
	// function add4MoreDefined(tableToIncrease){
			require(["dojo/dom","dojo/domReady!" ], function(dom) {
			
			var bType = "seller_users_def";
			var toalUserDef = 10;
			if (tableToIncrease == "InvBuyerDefinedTableId")
				bType = "buyer_users_def";
			if (tableToIncrease =="InvUserDefinitionLineTableId"){
				bType = "prod_chars_ud";
				toalUserDef = 7;
			}
			
			var table = dom.byId(tableToIncrease);
				
			//get index# for field naming
			var insertRowIdx = table.rows.length;
			var cnt = 0;	  
			  
			for(cnt = 0; cnt<4; cnt++){
			  	
			  	//k is used to check the total data item rows. Maximum = 10.
			  	//we need to subtract 2 to remove header lines
			  	k=insertRowIdx+1-2;
			  	if(k > toalUserDef){
					  break;
			    }
				//add the table row
			  	var row = table.insertRow(insertRowIdx);
			  	row.id = bType+(insertRowIdx-1);
		   	 	var cell0 = row.insertCell(0);//first  cell in the row
				var cell1 = row.insertCell(1);//second  cell in the row
				var cell2 = row.insertCell(2);//third  cell in the row
				var cell3 = row.insertCell(3);//third  cell in the row
				cell0.setAttribute("align","center");
				cell2.setAttribute("align","right");
			    cell2.setAttribute("class","formItem");
			    cell3.setAttribute("align","center");
				if(bType == "prod_chars_ud"){
		          cell0.innerHTML ='<input data-dojo-type=\"dijit.form.CheckBox\" name=\"'+bType+k+'_type_req\" id=\"'+bType+k+'_type_req\" value=\"Y\">'        
		          cell1.innerHTML ='<input data-dojo-type="dijit.form.TextBox" name=\"'+bType+k+'_type\" id=\"'+bType+k+'_type\"   maxLength=\"35\" width=\'10\' value=\"\">'        
		          cell2.innerHTML ='<label>35</label>'
		          cell3.innerHTML ='<input  data-dojo-type=\"dijit.form.CheckBox\" name=\"'+bType+k+'_type_data_req\" id=\"'+bType+k+'_type_data_req\" value=\"Y\">'
		      	    
				}else{
			      cell0.innerHTML ='<input data-dojo-type=\"dijit.form.CheckBox\" name=\"'+bType+k+'_req\" id=\"'+bType+k+'_req\" value=\"Y\">'        
			      cell1.innerHTML ='<input data-dojo-type="dijit.form.TextBox" name=\"'+bType+k+'_label\" id=\"'+bType+k+'_label\"   maxLength=\"35\" width=\'10\' required=\"true\" value=\"\">'        
			      cell2.innerHTML ='<label >140</label>'	
			      cell3.innerHTML ='<input  data-dojo-type=\"dijit.form.CheckBox\" name=\"'+bType+k+'_data_req\" id=\"'+bType+k+'_data_req\" value=\"Y\">'
			     }
		    
		        require(["dojo/parser"], function(parser) {
		         	parser.parse(row.id);
		    	 });
			   	insertRowIdx = insertRowIdx+1;
			  }  
			});
		} 
	 
	 local.updateDefOrder = function(gIndex, filedName) {
	// function updateDefOrder(gIndex, filedName)
	 //{

	 	var temp = "";
	 	var index = 0;
	 	var temp_upload_order_value = "";
	 	gIndex = gIndex-1;	
	 	var isSwaped = false;
	 	//Validate text box
	 	for( i=1; i < gIndex; i++ ){
	 		textValue = document.getElementById('text'+i).value;
	 		//Check for in-correct value
	 		if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < 1) || (textValue >= gIndex) ) {
	 				document.getElementById('text'+i).focus();
	 				return false;
	 		}else{
	 			document.getElementById('text'+i).value = textValue ;
	 		}

	 	}

	 	for(var i=1; i < gIndex; i++){
	 		//check the entered text box value with the i value 
	 		if( i != document.getElementById('text'+(i)).value ){
	 			// check if textbox value's are repeated or not
	 			
	 			if(document.getElementById('text'+i).value == document.getElementById('text'+(document.getElementById('text'+i).value)).value){
	 				document.getElementById('text'+i).focus();
	 				return false;
	 			}
	 			
	 			//Swap fieldname data
	 			temp = document.getElementById('fileFieldName'+i).innerHTML;
	 			document.getElementById('fileFieldName'+ i).innerHTML = document.getElementById('fileFieldName'+document.getElementById('text'+(i)).value).innerHTML;
	 			document.getElementById("fileFieldName"+document.getElementById('text'+(i)).value).innerHTML = temp;
	 			
	 			//swap even the ids
	             temp_upload_order_value = document.getElementById("upload_order_"+i).value;
	 			document.getElementById("upload_order_"+i).value = document.getElementById("upload_order_"+(document.getElementById('text'+i).value) ).value;
	 			document.getElementById("upload_order_"+(document.getElementById('text'+i).value) ).value = temp_upload_order_value;

	 			//Swap the index value ie, textbox value
	 			index = document.getElementById('text'+(i)).value
	 			document.getElementById('text'+(i)).value =  document.getElementById('text'+index).value;
	 			document.getElementById('text'+index).value = index;
	 			isSwaped = true;
	 			
	 		}
	 		
	 	}	
	 if(isSwaped){
	 		
	 		var totalPOSummGoodOrderDara = (document.getElementById("InvDefFileOrderTableId").rows.length) - (2); // two row for header
	 		var rowToModify = "";
	 		var spanId="";
	 		var idx;
	 		for(var row=0; row<totalPOSummGoodOrderDara; row++){
	 			rowToModify = document.getElementById("InvDefFileOrderTableId").rows[row+2]; // two for header
	 			if(rowToModify.cells[1].getElementsByTagName("span")[0]){
	 				
	 			spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
	 			//idx = spanId.indexOf("_label");
	 			if(spanId){
	 				//spanId = spanId.substring(0,idx);
	 			//	rowToModify.id = spanId;
	 			}
	 			//rowToModify.id = spanId;
	 			rowToModify.id = spanId + "_req_order";
	 		}}
	 	}
	 	
	 }
	 
	local.updateSumDefOrder = function (gIndex, sIndex, filedName)
	 {

	 	var temp = "";
	 	var index = 0;
	 	var temp_upload_order_value = "";
	 	
	 	sIndex = sIndex - 1;
	 	gIndex = gIndex -1;
	 	totalOrder = sIndex + gIndex -2;
	 	var isSwaped = false;
	 	
	 	for( var j = 1; j < gIndex; j++ ){

	 		textValue = document.getElementById('userDefText'+(j)).value;
	 		//Check for in-correct value
	 		if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < sIndex) || (textValue > totalOrder) ) {
	 				document.getElementById('userDefText'+j).focus();
	 				return false;
	 		}else{
	 			document.getElementById('userDefText'+j).value = textValue ;
	 		}

	 	}

	 	for(var i=1; i < gIndex; i++){
	 		//check the entered text box value with the i value 
	 		var userDefTextVal = document.getElementById('userDefText'+(i)).value;
	 		var invSummGoodsStart = sIndex + i -1;
	 		if( invSummGoodsStart != userDefTextVal ){
	 			// check if textbox value's are repeated or not

	 		   var swapFieldPosition = userDefTextVal - sIndex +1;
	 			if(document.getElementById('userDefText'+(i)).value == document.getElementById('userDefText'+swapFieldPosition).value){
	 				document.getElementById('userDefText'+i).focus();
	 				return false;
	 			}
	 			
	 			//Swap fieldname data
	 			temp = document.getElementById('userDefFileFieldName'+(i)).innerHTML;					
	 			document.getElementById('userDefFileFieldName'+ i).innerHTML = document.getElementById('userDefFileFieldName'+ swapFieldPosition).innerHTML;
	 			document.getElementById("userDefFileFieldName"+ swapFieldPosition).innerHTML = temp;
	 			
	 			//swap even the ids
	             temp_upload_order_value = document.getElementById("upload_userdef_order_"+i).value;
	 			document.getElementById("upload_userdef_order_"+i).value = document.getElementById("upload_userdef_order_"+ swapFieldPosition).value;
	 			document.getElementById("upload_userdef_order_"+ swapFieldPosition).value = temp_upload_order_value;

	 			//Swap the index value ie, textbox value
	 			index = (document.getElementById('userDefText'+(i)).value);
	 			document.getElementById('userDefText'+(i)).value =  document.getElementById('userDefText'+ swapFieldPosition).value;
	 			document.getElementById('userDefText'+ swapFieldPosition).value = index;
	 			isSwaped = true;
	 		}
	 	}	
	 	// update row is according to data after swapping
	 	if(isSwaped){
	 		
	 		var totalPOSummGoodOrderDara = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2); // two row for header
	 		var rowToModify = "";
	 		var spanId="";
	 		var idx;
	 		for(var row=0; row<totalPOSummGoodOrderDara; row++){
	 			rowToModify = document.getElementById("poStructSumDataOrderTableId").rows[row+2]; // two for header
	 			spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
	 			idx = spanId.indexOf("_label");
	 			if( idx > -1){
	 				spanId = spanId.substring(0,idx);
	 			}
	 			rowToModify.id = spanId + "_req_order";
	 		}
	 	}
	 }
	
	//Rel8.2 CR-741 AiA Discount codes update - start
	local.updateCreditNoteDefOrder=function (gIndex, sIndex, filedName)
	{

		var temp = "";
		var index = 0;
		var temp_upload_order_value = "";
		
		sIndex = sIndex - 1;
		gIndex = gIndex -1;
		totalOrder = sIndex + gIndex -2;
		var isSwaped = false;
		
		for( var j = 1; j < gIndex; j++ ){

			textValue = document.getElementById('1userDefText'+(j)).value;

			if( textValue == "" || textValue.length == 0 || isNaN(textValue)){
					document.getElementById('1userDefText'+j).focus();
					return false;
			}else{
				document.getElementById('1userDefText'+j).value = textValue ;
			}

		}

		for(var i=1; i < gIndex; i++){
			//check the entered text box value with the i value 
			var userDefTextVal = document.getElementById('1userDefText'+(i)).value;
			var invSummGoodsStart = sIndex + i -1;
			if( invSummGoodsStart != userDefTextVal ){
				// check if textbox value's are repeated or not

			   var swapFieldPosition = userDefTextVal - sIndex +1;
				if(document.getElementById('1userDefText'+(i)).value == document.getElementById('1userDefText'+swapFieldPosition).value){
					document.getElementById('1userDefText'+i).focus();
					return false;
				}
				
				//Swap fieldname data
				temp = document.getElementById('1userDefFileFieldName'+(i)).innerHTML;					
				document.getElementById('1userDefFileFieldName'+ i).innerHTML = document.getElementById('1userDefFileFieldName'+ swapFieldPosition).innerHTML;
				document.getElementById("1userDefFileFieldName"+ swapFieldPosition).innerHTML = temp;
				
				//swap even the ids
	            temp_upload_order_value = document.getElementById("1upload_userdef_order_"+i).value;
				document.getElementById("1upload_userdef_order_"+i).value = document.getElementById("1upload_userdef_order_"+ swapFieldPosition).value;
				document.getElementById("1upload_userdef_order_"+ swapFieldPosition).value = temp_upload_order_value;

				//Swap the index value ie, textbox value
				index = (document.getElementById('1userDefText'+(i)).value);
				document.getElementById('1userDefText'+(i)).value =  document.getElementById('1userDefText'+ swapFieldPosition).value;
				document.getElementById('1userDefText'+ swapFieldPosition).value = index;
				isSwaped = true;
			}
		}	
		// update row is according to data after swapping
		if(isSwaped){
			
			var totalPOSummGoodOrderDara = (document.getElementById("discountCodeDataOrderTableId").rows.length) - (2); // two row for header
			var rowToModify = "";
			var spanId="";
			var idx;
			for(var row=0; row<totalPOSummGoodOrderDara; row++){
				rowToModify = document.getElementById("discountCodeDataOrderTableId").rows[row+2]; // two for header
				spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
				idx = spanId.indexOf("_label");
				if( idx > -1){
					spanId = spanId.substring(0,idx);
				}
				rowToModify.id = spanId + "_req_order";
			}
		}
	}


	local.updateSumLineItemOrder = function (gIndex, sIndex, filedName)
	{

		var temp = "";
		var index = 0;
		var temp_upload_order_value = "";
		//var j = 1;	
		
		sIndex = sIndex + 1;
		gIndex = gIndex -1;
		totalOrder = sIndex + gIndex -1;
		
		for( var j = 1; j < gIndex; j++ ){        
			textValue = document.getElementById('userDefLineText'+j).value;
			//Check for in-correct value
			if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < sIndex) || (textValue > totalOrder) ) {
					document.getElementById('userDefLineText'+j).focus();
					return false;
			}else{
				document.getElementById('userDefLineText'+j).value = textValue ;
			}

		}

		for(var i=1; i < gIndex; i++){
			//check the entered text box value with the i value 
			var userDefTextVal = document.getElementById('userDefLineText'+(i)).value;
			var invSummLineItemStart = sIndex + i -1;
			if( invSummLineItemStart != userDefTextVal ){
				// check if textbox value's are repeated or not
	            var swapFieldPosition = userDefTextVal - sIndex +1;
				if(document.getElementById('userDefLineText'+(i)).value == document.getElementById('userDefLineText'+swapFieldPosition).value){
					document.getElementById('userDefLineText'+i).focus();
					return false;
				}
				
				//Swap fieldname data
				temp = document.getElementById('userDefLineFileFieldName'+(i)).innerHTML;					
				document.getElementById('userDefLineFileFieldName'+ i).innerHTML = document.getElementById('userDefLineFileFieldName'+swapFieldPosition).innerHTML;
				document.getElementById("userDefLineFileFieldName"+swapFieldPosition).innerHTML = temp;
				
				//swap even the ids
	            temp_upload_order_value = document.getElementById("upload_userDefLine_order_"+i).value;
				document.getElementById("upload_userDefLine_order_"+i).value = document.getElementById("upload_userDefLine_order_"+swapFieldPosition).value;
				document.getElementById("upload_userDefLine_order_"+swapFieldPosition ).value = temp_upload_order_value;

				//Swap the index value ie, textbox value
				index = (document.getElementById('userDefLineText'+(i)).value);
				document.getElementById('userDefLineText'+(i)).value =  document.getElementById('userDefLineText'+swapFieldPosition).value;
				document.getElementById('userDefLineText'+swapFieldPosition).value = index;
				
			}
		}	
		
	}

	local.reorderInvDefFileOrder =function (){

		//	var invSummDataOrderCount = (document.getElementById("InvDefFileOrderTableId").rows.length) - (2);
			var invSummDataOrderCount = 0;	
			for(var i=1; i <= invSummDataOrderCount; i++){
				document.getElementById("text" + i).value = (invSummDataOrderCount) + (i);
			}		
	}

	local.reorderInvSummGoodsOrder =function (){

		var invSummGoodsOrderCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
		if(invSummGoodsOrderCount > 0){
			var invSummDataOrderCount = (document.getElementById("InvDefFileOrderTableId").rows.length) - (2);
			for(var i=1; i <= invSummGoodsOrderCount; i++){
				document.getElementById("userDefText" + i).value = (invSummDataOrderCount) + (i);
			}		
		}	
	}

	local.reorderInvSummLineItemOrder = function (){

		var invSummLineItemOrderCount = (document.getElementById("poStructSumLineItemOrderTableId").rows.length) - (2);
		if(invSummLineItemOrderCount > 0){
			var invSummDataOrderCount = (document.getElementById("InvDefFileOrderTableId").rows.length) - (2);
			var invSummGoodsOrderCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
			var invCreditOrderItemOrderCount = (document.getElementById("discountCodeDataOrderTableId").rows.length) - (2);
			for(var i=1; i <= invSummLineItemOrderCount; i++){
				
				document.getElementById("userDefLineText" + i).value = (invSummDataOrderCount) + (invSummGoodsOrderCount) + (invCreditOrderItemOrderCount) + (i);
			}		
		}	
	}


	//AAlubala - Reorder Discount codes section -  Rel8.2 CR-741  - start
	local.reorderInvCreditNoteItemOrder =function (){

		var invSummLineItemOrderCount = (document.getElementById("discountCodeDataOrderTableId").rows.length) - (2);
		if(invSummLineItemOrderCount > 0){
			var invSummDataOrderCount = (document.getElementById("InvDefFileOrderTableId").rows.length) - (2);
			var invSummGoodsOrderCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
			for(var i=1; i <= invSummLineItemOrderCount; i++){
				
				document.getElementById("1userDefText" + i).value = (invSummDataOrderCount) + (invSummGoodsOrderCount) + (i);
			}		
		}	
	}
	
	
	function resetPaymentFields(resetForm){
		var thisForm = resetForm;
		//RKAZI Rel 8.2 T36000011727 - START
		resetField(thisForm.ben_acct_num_req,thisForm.ben_acct_num_data_req,"ben_act_num", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_add1_req,thisForm.ben_add1_data_req,"ben_add1", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_add2_req,thisForm.ben_add2_data_req,"ben_add2", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_add3_req,thisForm.ben_add3_data_req,"ben_add3", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_email_addr_req,thisForm.ben_email_addr_data_req,"ben_email_addr", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_country_req,thisForm.ben_country_data_req,"ben_country", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_bank_name_req,thisForm.ben_bank_name_data_req,"ben_bank_name", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_bank_sort_code_req,thisForm.ben_bank_sort_code_data_req,"ben_bank_sort_code", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_branch_code_req,thisForm.ben_branch_code_data_req,"ben_branch_code", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_branch_add1_req,thisForm.ben_branch_add1_data_req,"ben_branch_add1", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_branch_add2_req,thisForm.ben_branch_add2_data_req,"ben_branch_add2", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_bank_city_req,thisForm.ben_bank_city_data_req,"ben_bank_city", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_bank_province_req,thisForm.ben_bank_province_data_req,"ben_bank_province", "InvDefFileOrderTableId", "text");
		resetField(thisForm.ben_branch_country_req,thisForm.ben_branch_country_data_req,"ben_branch_country", "InvDefFileOrderTableId", "text");
		//RKAZI Rel 8.2 T36000011727 - END
	}
	function resetProCharType(prodCharType){
		var prodCharTypeLocal = registry.getEnclosingWidget(prodCharType);
		if (prodCharTypeLocal){
			prodCharTypeLocal.set('value', '');
		}
	}
	function resetField(reqField, reqDataField, fieldName, tableId, fieldType){
		var checkedObj = dijit.getEnclosingWidget(reqField);
		if (checkedObj){
        	checkedObj.set('checked',false);
		}
        var checkedDataObj = dijit.getEnclosingWidget(reqDataField);
        if (checkedDataObj){
        	checkedDataObj.set('checked',false);
        }
  	  var row = document.getElementById(fieldName+"_req_order");
	  deleteOrderField(row, document.getElementById(tableId), fieldType);	

	}
	
	function unsetDataReq(reqField){
		var checkedObj = dijit.getEnclosingWidget(reqField);
		if (checkedObj){
	        checkedObj.set('checked',false);
	        return false;
		}
	}
	
	  function deleteOrderField(deleteRow, table, idName){
		  if(deleteRow){
		var tabSize = (table.rows.length)- (2); // two row for header
		var replaceRow = deleteRow.rowIndex;
		var uploadOrder = "upload_order_";
		  if ("userDefText" == idName){
			 uploadOrder = "upload_userdef_order_";
		 } else if("1userDefText" == idName){
			 uploadOrder = "1upload_userdef_order_";
		 } else if("userDefLineText" == idName){
			 uploadOrder = "upload_userDefLine_order_";
		 }
		
		for(var row= replaceRow; row <= tabSize; row++){
			table.rows[ row ].id = table.rows[ row + 1 ].id;
			table.rows[ row ].cells[0].childNodes[0].value = table.rows[ row+1 ].cells[0].childNodes[0].value;
			table.rows[ row ].cells[0].childNodes[1].value = table.rows[ row+1 ].cells[0].childNodes[1].value;
			document.getElementById((uploadOrder+(row-1))).value=document.getElementById((uploadOrder+(row))).value;
			table.rows[ row ].cells[1].innerHTML = table.rows[ row+1 ].cells[1].innerHTML;
		}
		
		table.deleteRow(tabSize+1);	
		var idNumberToDestroy = (table.rows.length) - (1);
		registry.byId(idName + idNumberToDestroy).value="";
		registry.byId(idName + idNumberToDestroy).destroy(true);

		 if("text" == idName){
			 local.reorderInvSummGoodsOrder();
	    	 local.reorderInvSummLineItemOrder();
	    	 local.reorderInvCreditNoteItemOrder();
		 } else if ("userDefText" == idName){
			 local.reorderInvSummLineItemOrder();
			 reorderInvCreditNoteItemOrder();
		 } else if("1userDefText" == idName){
			 local.reorderInvSummLineItemOrder();
		 }
		  }
	  }
	  
	  //add most functions to thisPage for convenience of access
      var thisPage = {
	    		 
     		 selectInvSummItem: function(checkboxWidget) {
     			
     			 var invSummDataTable = document.getElementById("InvDefFileOrderTableId");
     			 var rowCount = invSummDataTable.rows.length;
     			 var row = invSummDataTable.insertRow(rowCount);
     			 
     			 var cell1 = row.insertCell(0);
     			 var cell2 = row.insertCell(1);
     			
     			 var summOrderCount = rowCount-1;
     			 var DefCharCheckbox = new dijit.form.TextBox({
     			        name: "",
     			        id: "text"+ summOrderCount,
     			        style:"width: 25px;",
     			        value: summOrderCount
     			 	});

     			    cell1.appendChild(DefCharCheckbox.domNode);
     			    cell1.align="center";
     			    var val1 = document.createElement("input"); 
     			    val1.type = 'hidden';
     			    val1.id = 'upload_order_'+summOrderCount;
     			    val1.name = 'inv_summary_order'+summOrderCount;
     			           			            			            			   
     			    cell2.setAttribute("id","fileFieldName"+summOrderCount);
     			    cell2.setAttribute("align","left");
     			    
     			    var checkboxId = checkboxWidget.id;
     			    row.id = checkboxId + "_order";
     			    var idx = checkboxId.indexOf("_req");
     			   
     			   var val2 = document.createElement("span"); 
     			   val2.className = "deleteSelectedItem";  
     			   val2.style.display = "";
      			   val2.style.verticalAlign="top";
     			    if( idx != -1){   
     			    	
     			    	 var addSectionId = checkboxId.substring(0,idx);
     			    	 val1.value = addSectionId;
     			    	 cell1.appendChild(val1);
     			    	 cell2.innerHTML = '<label class=\"pageSubHeaderItem\" style=\"width: 50% ;padding: 3px 0.1em 0px 1px;\">' + document.getElementById(addSectionId).value + '</label>'+'<span id=\"'+addSectionId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';  
      			    	 
     			    	 // reorder invoice summary goods anf line item order
     			    	 local.reorderInvDefFileOrder();
     			    	 local.reorderInvSummGoodsOrder();
     			    	 local.reorderInvSummLineItemOrder();
     			    	 //AiA
     			    	 local.reorderInvCreditNoteItemOrder();
     			    }	
     			   
     		 },
     		 
     		 selectInvSummGoodsItem: function(checkboxWidget) {
      			
     			 var invSummGoodsDataTable = document.getElementById("poStructSumDataOrderTableId");
     			 var rowCount = invSummGoodsDataTable.rows.length;
     			 var row = invSummGoodsDataTable.insertRow(rowCount);
     			 
                  var invSummOrderDataCount = (document.getElementById("InvDefFileOrderTableId").rows.length) - (2);
     			 
     			 var cell1 = row.insertCell(0);
     			 var cell2 = row.insertCell(1);
     			
     			 var invSummGoodsOrderCount = rowCount-1;
     			 
     			 var totalOrder = invSummOrderDataCount + invSummGoodsOrderCount;        			 
     			 var DefCharCheckbox = new dijit.form.TextBox({
     			        name: "",
     			        id: "userDefText"+ invSummGoodsOrderCount,
     			        style:"width: 25px;",
     			        value: totalOrder
     			 	});
     			 
     			    cell1.appendChild(DefCharCheckbox.domNode);
     			    cell1.align="center";
     			    var val1 = document.createElement("input"); 
     			    val1.type = 'hidden';
     			    val1.id = 'upload_userdef_order_'+invSummGoodsOrderCount;
     			    val1.name = 'inv_goods_order'+invSummGoodsOrderCount;
     			           			            			            			   
     			    cell2.setAttribute("id","userDefFileFieldName"+invSummGoodsOrderCount);
     			    cell2.setAttribute("align","left");
     			    cell2.setAttribute("class","formItem");
     			    
     			    var checkboxId = checkboxWidget.id;
     			    row.id = checkboxId + "_order";
     		        var idx = checkboxId.indexOf("_req");
     		        var val2 = document.createElement("span"); 
     			    val2.className = "deleteSelectedItem";
     			    val2.style.display = "";
     			    val2.style.top= "-2px";

     		        
     			    if( idx != -1){   
     			    	 var addSectionId = checkboxId.substring(0,idx);
     			    	 var userdef = checkboxId.indexOf("_users_def");
     			    	 if(userdef != -1){
     			    	   val1.value = addSectionId + "_label";
     			    	   val2.setAttribute("id", addSectionId + "_label");
       			    	   cell1.appendChild(val1);
       			    	   var id1 = addSectionId + "_label";          			    	   
       			    	   cell2.innerHTML = '<label class=\"pageSubHeaderItem\" style=\"width: 50% ;padding: 3px 0.1em 0px 1px;\">' + document.getElementById(id1).value + '</label>'+'<span class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\" id='+id1+'>';           			    	 
     			    	 }else{
      			    	   val1.value = addSectionId;
       			    	   val2.setAttribute("id", addSectionId);
     			    	   cell1.appendChild(val1);       			    	   
     			    	   cell2.innerHTML = '<label class=\"pageSubHeaderItem\" style=\"width: 50% ;padding: 3px 0.1em 0px 1px;\">' + document.getElementById(addSectionId).value + '</label>'+'<span class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\" id='+addSectionId+'>';         			    		 
     			    	 }
     			    	 reorderInvSummLineItemOrder();
     			    	 //AiA
     			    	 reorderInvCreditNoteItemOrder();        			    	 
     			    }
     		    
     		 },
     		 
     		 selectInvSummLineItem: function(checkboxWidget) {
       			
     			 var invSummLineDataTable = document.getElementById("poStructSumLineItemOrderTableId");
     			 var rowCount = invSummLineDataTable.rows.length;
     			 var row = invSummLineDataTable.insertRow(rowCount);
     			 
                  var invSummOrderDataCount = (document.getElementById("InvDefFileOrderTableId").rows.length) - (2);
                  var invSummGoodsrderDataCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
     			 
     			 var cell1 = row.insertCell(0);
     			 var cell2 = row.insertCell(1);
     			
     			 var invSummLineOrderCount = rowCount-1;
     			 
     			 var totalOrder = invSummOrderDataCount + invSummGoodsrderDataCount + invSummLineOrderCount;        			 
     			 var DefCharCheckbox = new dijit.form.TextBox({
     			        name: "",
     			        id: "userDefLineText"+ invSummLineOrderCount,
     			        style:"width: 25px;align:center",
     			        value: totalOrder
     			 	});
     			 
     			    cell1.appendChild(DefCharCheckbox.domNode);
     			    cell1.align="center";
     			    var val1 = document.createElement("input"); 
     			    val1.type = 'hidden';
     			    val1.id = 'upload_userDefLine_order_'+invSummLineOrderCount;
     			    val1.name = 'inv_line_item_order'+invSummLineOrderCount;
     			             			            			   
     			    cell2.setAttribute("id","userDefLineFileFieldName"+invSummLineOrderCount);
     			    cell2.setAttribute("align","left");
     			    cell2.setAttribute("class","formItem");
     			    
     			    var checkboxId = checkboxWidget.id;
     			    row.id = checkboxId + "_order";
     		        var idx = checkboxId.indexOf("_req");
     		        var val2 = document.createElement("span"); 
     			    val2.className = "deleteSelectedItem";
     			    val2.style.display = "";
     			    val2.style.top= "-2px";
     		        
     			    if( idx != -1){  
     			    	
     			    	 var addSectionId = checkboxId.substring(0,idx);
     			    	   val1.value = addSectionId;
       			    	   val2.setAttribute("id", addSectionId);
     			    	   cell1.appendChild(val1);        			    	    
     			    	   cell2.innerHTML = '<label class=\"pageSubHeaderItem\" style=\"width: 60% ;padding: 3px 0.1em 0px 1px;\">' + document.getElementById(addSectionId).value + '</label>'+'<span class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\" id='+addSectionId+'>';  
     			    	 }
     		    
     		 },
     		 
     		 deleteEventHandler: function(deleteButton, tableID, orderId) {
     		        //uncheck the available item
     		        //this automatically does the onchange mechanism there
     		        //including removing this item and decreasing the total count
     		       // var deleteSelectItem = deleteButton.parentNode;
     		        var deleteSelectItemId = deleteButton.id;
			    	    var userdef = deleteSelectItemId.indexOf("_label");
			    	    if(userdef != -1){
			    	    	deleteSelectItemId = deleteSelectItemId.substring(0, userdef);
			    	    }   
     		        var checkbox = registry.byId(deleteSelectItemId+'_req');  
     		        if ( checkbox ) {
     		          checkbox.set('checked',false);
     		        }
     		        var row = document.getElementById(deleteSelectItemId + '_req_order');
     		        deleteOrderField(row, tableID, orderId);	
     		      }
     		 //Rel8.2 CR-741 AAlubala Discount codes order code 02/27/2013 - start
,
     		 
selectCreditDiscountItem: function(checkboxWidget) {
      			
     			 var invCreditNoteOrderCount = document.getElementById("discountCodeDataOrderTableId");
     			 var rowCount = invCreditNoteOrderCount.rows.length;
     			 var row = invCreditNoteOrderCount.insertRow(rowCount);

                  var invSummOrderDataCount = (document.getElementById("InvDefFileOrderTableId").rows.length) - (2);
                  var invSummGoodsDataCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
     			 
     			 var cell1 = row.insertCell(0);
     			 var cell2 = row.insertCell(1);
     			
     			 var invSummGoodsOrderCount = rowCount-1;
     			 
     			 var totalOrder = invSummOrderDataCount + invSummGoodsOrderCount + invSummGoodsDataCount;  
     			 var DefCharCheckbox = new dijit.form.TextBox({
     			        name: "",
     			        id: "1userDefText"+ invSummGoodsOrderCount,
     			        style:"width: 25px;",
     			        value: totalOrder
     			 	});
     			 
     			    cell1.appendChild(DefCharCheckbox.domNode);
     			    cell1.align="center";
     			    var val1 = document.createElement("input"); 
     			    val1.type = 'hidden';
     			    val1.id = '1upload_userdef_order_'+invSummGoodsOrderCount;
     			    val1.name = 'inv_credit_order'+invSummGoodsOrderCount;
     			           			            			            			   
     			    cell2.setAttribute("id","1userDefFileFieldName"+invSummGoodsOrderCount);
     			    cell2.setAttribute("align","left");
     			    cell2.setAttribute("class","formItem");
     			    
     			    var checkboxId = checkboxWidget.id;
     			    row.id = checkboxId + "_order";
     		        var idx = checkboxId.indexOf("_req");
     		        var val2 = document.createElement("span"); 
     			    val2.className = "deleteSelectedItem";
     			    val2.style.display = "";
     			    val2.style.top= "-2px";
     		        
     			    if( idx != -1){   
     			    	 var addSectionId = checkboxId.substring(0,idx);
      			    	   val1.value = addSectionId;
       			    	   val2.setAttribute("id", addSectionId);
     			    	   cell1.appendChild(val1);
     			    	   cell2.innerHTML = '<label class=\"pageSubHeaderItem\" style=\"width: 50% ;padding: 3px 0.1em 0px 1px;\">' + document.getElementById(addSectionId).value + '</label>'+'<span class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\" id='+addSectionId+'>';  

     			    	 reorderInvSummLineItemOrder();
     			    }
     		    
     		 }        		 
     	  }
	  
	  
	  ready(function() { 
		  var invTypeIndicator = registry.byId("TradePortalConstants.INVOICE_TYPE_REC_MGMT");
 		   if (invTypeIndicator) {
 			   invTypeIndicator.on("change", function(checkValue) {
 				   if ( checkValue ) {
							dom.byId('buyerId').style.display  = '';
							dom.byId('buyerName').style.display  = '';
							dom.byId('sellerId').style.display  = 'none';
							dom.byId('sellerName').style.display  = 'none';
							dom.byId('supplierToSendDate').style.display  = 'none';
							dom.byId('buyerAcctNum').style.display  = 'none';
							dom.byId('buyerAcctCurr').style.display  = 'none';
							dom.byId('endToEndID').style.display  = 'none';
							dom.byId('repoatingCode1').style.display  = 'none';
							dom.byId('repoatingCode2').style.display  = 'none';
							dom.byId('discountCodeDataOrderTableId').style.display  = '';
							dom.byId('InvCreditNoteTableId').style.display  = '';
							dom.byId('updateCreditNoteOrderButton').style.display  = '';
							registry.getEnclosingWidget(dom.byId('seller_id_req')).set('checked', false);
							registry.getEnclosingWidget(dom.byId('seller_name_req')).set('checked', false);
							registry.getEnclosingWidget(dom.byId('send_to_supplier_date_req')).set('checked', false);
							registry.getEnclosingWidget(dom.byId('buyer_acct_num_req')).set('checked', false);
							registry.getEnclosingWidget(dom.byId('buyer_acct_currency_req')).set('checked', false);
							registry.getEnclosingWidget(dom.byId('reporting_code_1_req')).set('checked', false);
							registry.getEnclosingWidget(dom.byId('reporting_code_2_req')).set('checked', false);
							registry.getEnclosingWidget(dom.byId('end_to_end_id_req')).set('checked', false);							
 				   }
 		        });
 		   }
            var invTypeIndicator = registry.byId("TradePortalConstants.INVOICE_TYPE_PAY_MGMT");
 		   if (invTypeIndicator) {
 			   invTypeIndicator.on("change", function(checkValue) {
 				   if ( checkValue ) {    						
 					   dom.byId('sellerId').style.display  = '';
 					   dom.byId('sellerName').style.display  = '';
 					   dom.byId('supplierToSendDate').style.display  = '';
 					   dom.byId('buyerAcctNum').style.display  = '';
 					   dom.byId('buyerAcctCurr').style.display  = '';
 					   dom.byId('endToEndID').style.display  = '';
 					   dom.byId('buyerId').style.display  = 'none';
 					   dom.byId('buyerName').style.display  = 'none';
 					   dom.byId('repoatingCode1').style.display  = '';
 					   dom.byId('repoatingCode2').style.display  = '';
 					   dom.byId('discountCodeDataOrderTableId').style.display  = 'none';
 					   dom.byId('InvCreditNoteTableId').style.display  = 'none';
 					   dom.byId('updateCreditNoteOrderButton').style.display  = 'none';
 					   registry.getEnclosingWidget(dom.byId('buyer_id_req')).set('checked', false);
 					   registry.getEnclosingWidget(dom.byId('buyer_name_req')).set('checked', false);
 					   registry.getEnclosingWidget(dom.byId('credit_discount_code_id_req')).set('checked', false);
 					   registry.getEnclosingWidget(dom.byId('credit_discount_gl_code_id_req')).set('checked', false);
 					   registry.getEnclosingWidget(dom.byId('credit_discount_comments_id_req')).set('checked', false);
    				   }
 		        });
 			   
 			   if('<%=TradePortalConstants.INVOICE_TYPE_PAY_MGMT%>' == invTypeIndicator.get("value")){
   				 dom.byId('updateCreditNoteOrderButton').style.display  = 'none'; 
   			 }
 		   }
     	 
        var buyerName = registry.byId("buyer_name_req");
		   if (buyerName) {
			 buyerName.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_name_data_req);
		        	  var row = document.getElementById("buyer_name_req_order");
		        	   deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		 var sellerName = registry.byId("seller_name_req");
		   if (sellerName) {
			   sellerName.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_name_data_req);
		        	  var row = document.getElementById("seller_name_req_order");
		        	   deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var buyerId = registry.byId("buyer_id_req");
		   if (buyerId) {
			   buyerId.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  var row = document.getElementById("buyer_id_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var sellerId = registry.byId("seller_id_req");
		   if (sellerId) {
			   sellerId.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  var row = document.getElementById("seller_id_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
        
        var paymentDate = registry.byId("payment_date_req");
		   if ( paymentDate ) {
			    paymentDate.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	unsetDataReq(document.InvoiceDefinitionDetailForm.payment_date_data_req);
		        	  var row = document.getElementById("payment_date_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		 var paymentDateData = registry.byId("payment_date_data_req");
		   if (paymentDateData) {
			   paymentDateData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("payment_date_req").checked, document.InvoiceDefinitionDetailForm.payment_date_data_req);
		          	}
				  
		        });
		   } 
		   //Narayan CR913 Rel9.0 30-Jan-2014 Begin
		   var sendSupplierDate = registry.byId("send_to_supplier_date_req");
		   if ( sendSupplierDate ) {
			 sendSupplierDate.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.send_to_supplier_date_data_req);
		        	  var row = dom.byId("send_to_supplier_date_req_order");
		        	  deleteOrderField(row, dom.byId("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   
		 var sendSupplierDateData = registry.byId("send_to_supplier_date_data_req");
		   if (sendSupplierDateData) {
			   sendSupplierDateData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(dom.byId("send_to_supplier_date_req").checked, document.InvoiceDefinitionDetailForm.send_to_supplier_date_data_req);
		          	}
				  
		        });
		   }
		   //Narayan CR913 Rel9.0 30-Jan-2014 End
		   		   
		   var instrumentType = registry.byId("linked_to_instrument_type_req");
		   if (instrumentType) {
			   instrumentType.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.linked_to_instrument_type_data_req);
		        	  var row = document.getElementById("linked_to_instrument_type_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var instrumentTypeData = registry.byId("linked_to_instrument_type_data_req");
		   if (instrumentTypeData) {
			   instrumentTypeData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("linked_to_instrument_type_req").checked, document.InvoiceDefinitionDetailForm.linked_to_instrument_type_data_req);
		          	}
				  
		        });
		   } 
		   
		   var invType = registry.byId("invoice_type_req");
		   if (invType) {
			   invType.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.invoice_type_data_req);
		        	  var row = document.getElementById("invoice_type_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var invTypeData = registry.byId("invoice_type_data_req");
		   if (invTypeData) {
			   invTypeData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("invoice_type_req").checked, document.InvoiceDefinitionDetailForm.invoice_type_data_req);
		          	}
				  
		        });
		   } 
		   
		   //DK CR709 Rel8.2 Starts
		   
		   var creditNote = registry.byId("credit_note_req");
		   
		   if (creditNote) {
			   creditNote.on("change", function(checkValue) {
				  
		          if ( checkValue ) {
		        	  if(local.checkInvLabelCreditLabelVal(document.getElementById("CreditNoteIndText").value, document.InvoiceDefinitionDetailForm.credit_note_req,
     			  "InvoiceDefinition.Label")){
		        	  	thisPage.selectInvSummItem(this);
		        	  }
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.credit_note_data_req);
		        	  var row = document.getElementById("credit_note_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		        	  registry.getEnclosingWidget(document.getElementById('CreditNoteIndText')).set('value', '');
		        	  // set all related credit note item as unselected.
		        	  var credDiscCodeID = registry.byId("credit_discount_code_id_req");
		        	  if(credDiscCodeID && credDiscCodeID.checked){
		        		  credDiscCodeID.set('checked',false);
		        	  }
		        	  
		        	  var credDiscGLCodeID = registry.byId("credit_discount_gl_code_id_req");
		        	  if(credDiscGLCodeID && credDiscGLCodeID.checked){
		        		  credDiscGLCodeID.set('checked',false);
		        	  }
		        	  
		        	  var credDiscCommentD = registry.byId("credit_discount_comments_id_req");
		        	  if(credDiscCommentD && credDiscCommentD.checked){
		        		  credDiscCommentD.set('checked',false);
		        	  }
		          }
		        });
		   }
		   var creditNoteData = registry.byId("credit_note_data_req");
		   if (creditNoteData) {
			   creditNoteData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("credit_note_req").checked, document.InvoiceDefinitionDetailForm.credit_note_data_req);
		          	}
				
		        });
		   } 

		var benAcctNum = registry.byId("ben_acct_num_req");
		var payMethod = registry.byId("pay_method_req");
		if(payMethod){
			payMethod.on("change", function(checkValue) {
			if(this.checked){
				registry.getEnclosingWidget(document.getElementById('ben_acct_num_req')).set('checked', true);
				//registry.getEnclosingWidget(document.getElementById('ben_acct_num_data_req')).set('checked', true);
			}else{
				registry.getEnclosingWidget(document.getElementById('pay_method_data_req')).set('checked', false);
			}  
	          if ( checkValue ) {
	        	  thisPage.selectInvSummItem(this);
	          }
	          else {
	        	  unsetDataReq(document.InvoiceDefinitionDetailForm.pay_method_data_req);
	        	  var row = document.getElementById("pay_method_req_order");
	        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
	        	  resetPaymentFields(document.InvoiceDefinitionDetailForm);
	          }

		});
		}  
		   var payMethodData = registry.byId("pay_method_data_req");
		   if (payMethodData) {
			   payMethodData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.pay_method_data_req);
		          	}
		        });
		   }    		
		if(benAcctNum){
			benAcctNum.on("change", function(checkValue) {
			if(this.checked){
				registry.getEnclosingWidget(document.getElementById('pay_method_req')).set('checked', true);
				//registry.getEnclosingWidget(document.getElementById('pay_method_data_req')).set('checked', true);
			}else{
				registry.getEnclosingWidget(document.getElementById('ben_acct_num_data_req')).set('checked', false);
			//RKAZI Rel 8.2 T36000011727 - Changed
				resetField(document.InvoiceDefinitionDetailForm.pay_method_req, document.InvoiceDefinitionDetailForm.pay_method_data_req,"pay_method", "InvDefFileOrderTableId", "text");
			}  
	          if ( checkValue ) {
	        	  thisPage.selectInvSummItem(this);
	          }
	          else {
	        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_acct_num_data_req);
	        	  var row = document.getElementById("ben_acct_num_req_order");
	        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
	          }
			
			
		});
		}  
		   var benAcctNumData = registry.byId("ben_acct_num_data_req");
		   if (benAcctNumData) {
			   benAcctNumData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_acct_num_req").checked, document.InvoiceDefinitionDetailForm.ben_acct_num_data_req);
		          	}
		        });
		   } 
		// Narayan CR-913 Rel9.0 15-Feb-2014 ADD -Begin		
		    var buyerAcctNum  = registry.byId("buyer_acct_num_req");
	   		if(buyerAcctNum){
	   			buyerAcctNum.on("change", function(checkValue) {
	   			if(this.checked){
	   				registry.getEnclosingWidget(dom.byId('buyer_acct_currency_req')).set('checked', true);
	   			    //Commenting below line as per CR 913 requirement-RPasupulati
	   				//registry.getEnclosingWidget(dom.byId('buyer_acct_currency_data_req')).set('checked', true);
	   				thisPage.selectInvSummItem(this);
	   			}else{
	   				registry.getEnclosingWidget(dom.byId('buyer_acct_num_data_req')).set('checked', false);
	   				resetField(document.InvoiceDefinitionDetailForm.buyer_acct_currency_req, document.InvoiceDefinitionDetailForm.buyer_acct_currency_data_req,"buyer_acct_currency", "InvDefFileOrderTableId", "text");
	   			    unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_acct_num_data_req);
	        	    var row = dom.byId("buyer_acct_num_req_order");
	        	    deleteOrderField(row, dom.byId("InvDefFileOrderTableId"), "text");	
	   			}  
	   		});
	   		} 
	   		
	   	   var buyerAcctNumData = registry.byId("buyer_acct_num_data_req");
		   if (buyerAcctNumData) {
			   buyerAcctNumData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(dom.byId("buyer_acct_num_req").checked, document.InvoiceDefinitionDetailForm.buyer_acct_num_data_req);
		          	}
				  
		        });
		   } 
		   
	   	   var buyerAcctCurr = registry.byId("buyer_acct_currency_req");
		   if(buyerAcctCurr){
	   			buyerAcctCurr.on("change", function(checkValue) {
	   			if(this.checked){
	   				registry.getEnclosingWidget(dom.byId('buyer_acct_num_req')).set('checked', true);
	   				//Commenting below line as per CR 913 requirement-RPasupulati
	   				//registry.getEnclosingWidget(dom.byId('buyer_acct_num_data_req')).set('checked', true);
	   				thisPage.selectInvSummItem(this);
	   			}else{
	   				registry.getEnclosingWidget(dom.byId('buyer_acct_currency_data_req')).set('checked', false);
	   				resetField(document.InvoiceDefinitionDetailForm.buyer_acct_num_req, document.InvoiceDefinitionDetailForm.buyer_acct_num_data_req,"buyer_acct_num", "InvDefFileOrderTableId", "text");
	   				unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_acct_currency_data_req);
		        	var row = dom.byId("buyer_acct_currency_req_order");
		        	deleteOrderField(row, dom.byId("InvDefFileOrderTableId"), "text");	
	   			}  	   				   			
	   		});
	   		} 
		  
		   var buyerAcctCurrData = registry.byId("buyer_acct_currency_data_req");
		   if (buyerAcctCurrData) {
			   buyerAcctCurrData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	local.checkRequiredVal(dom.byId("buyer_acct_currency_req").checked, document.InvoiceDefinitionDetailForm.buyer_acct_currency_data_req);
		          	}
				
		        });
		   }		
		// Narayan CR-913 Rel9.0 15-Feb-2014 Add -End
		   
		   var benAdd1 = registry.byId("ben_add1_req");
		   
		   if (benAdd1) {
			   benAdd1.on("change", function(checkValue) {
				  if (this.checked){
					  local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_add1_req);
				  }
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_add1_data_req);
		        	  var row = document.getElementById("ben_add1_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benAdd1Data = registry.byId("ben_add1_data_req");
		   if (benAdd1Data) {
			   benAdd1Data.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_add1_req").checked, document.InvoiceDefinitionDetailForm.ben_add1_data_req);
		          	}
				  
		        });
		   } 

		   var benAdd2 = registry.byId("ben_add2_req");
		   
		   if (benAdd2) {
			   benAdd2.on("change", function(checkValue) {
					  if (this.checked){
						  local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_add2_req);
					  }				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_add2_data_req);
		        	  var row = document.getElementById("ben_add2_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benAdd2Data = registry.byId("ben_add2_data_req");
		   if (benAdd2Data) {
			   benAdd2Data.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_add2_req").checked, document.InvoiceDefinitionDetailForm.ben_add2_data_req);
		          	}
				  
		        });
		   } 

		   var benAdd3 = registry.byId("ben_add3_req");
		   
		   if (benAdd3) {
			   benAdd3.on("change", function(checkValue) {
					  if (this.checked){
						  local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_add3_req);
					  }					  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_add3_data_req);
		        	  var row = document.getElementById("ben_add3_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benAdd3Data = registry.byId("ben_add3_data_req");
		   if (benAdd3Data) {
			   benAdd3Data.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_add3_req").checked, document.InvoiceDefinitionDetailForm.ben_add3_data_req);
		          	}
				  
		        });
		   } 
		   
		   var benEmailAddr = registry.byId("ben_email_addr_req");
		   
		   if (benEmailAddr) {
			   benEmailAddr.on("change", function(checkValue) {
					  if (this.checked){
						  local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_email_addr_req);
					  }					  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_email_addr_data_req);
		        	  var row = document.getElementById("ben_email_addr_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benEmailAddrData = registry.byId("ben_email_addr_data_req");
		   if (benEmailAddrData) {
			   benEmailAddrData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_email_addr_req").checked, document.InvoiceDefinitionDetailForm.ben_email_addr_data_req);
		          	}
				  
		        });
		   } 
		   
		   var benCountry = registry.byId("ben_country_req");
		   
		   if (benCountry) {
			   benCountry.on("change", function(checkValue) {
					  if (this.checked){
						  local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_country_req);
					  }					  
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_country_data_req);
		        	  var row = document.getElementById("ben_country_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benCountryData = registry.byId("ben_country_data_req");
		   if (benCountryData) {
			   benCountryData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_country_req").checked, document.InvoiceDefinitionDetailForm.ben_country_data_req);
		          	}
				  
		        });
		   } 
		   
		   var benSortCode = registry.byId("ben_bank_sort_code_req");
		   
		   if (benSortCode) {
			   benSortCode.on("change", function(checkValue) {
					  if (this.checked){
						  local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_bank_sort_code_req);
					  }					  
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_bank_sort_code_data_req);
		        	  var row = document.getElementById("ben_bank_sort_code_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benSortCodeData = registry.byId("ben_bank_sort_code_data_req");
		   if (benSortCodeData) {
			   benSortCodeData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_bank_sort_code_req").checked, document.InvoiceDefinitionDetailForm.ben_bank_sort_code_data_req);
		          	}
				  
		        });
		   } 

		   var benBankName = registry.byId("ben_bank_name_req");
		   
		   if (benBankName) {
			   benBankName.on("change", function(checkValue) {
					  if (this.checked){
						  local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_bank_name_req);
						  //registry.getEnclosingWidget(document.getElementById('ben_bank_name_data_req')).set('checked', true);
						  registry.getEnclosingWidget(document.getElementById('ben_branch_add1_req')).set('checked', true);
						  //registry.getEnclosingWidget(document.getElementById('ben_branch_add1_data_req')).set('checked', true);
					  }	else{
						  registry.getEnclosingWidget(document.getElementById('ben_bank_name_data_req')).set('checked', false);
						//RKAZI Rel 8.2 T36000011727 - Changed
						  resetField(document.InvoiceDefinitionDetailForm.ben_branch_add1_req, document.InvoiceDefinitionDetailForm.ben_branch_add1_data_req, "ben_branch_add1", "InvDefFileOrderTableId", "text");
					  }				  
				  
		          if ( checkValue ) {
		        	  	thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_bank_name_data_req);
		        	  var row = document.getElementById("ben_bank_name_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benBankNameData = registry.byId("ben_bank_name_data_req");
		   if (benBankNameData) {
			   benBankNameData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_bank_name_req").checked, document.InvoiceDefinitionDetailForm.ben_bank_name_data_req);
		          	}
		        });
		   } 
		   
		   var benBranchCode = registry.byId("ben_branch_code_req");
		   
		   if (benBranchCode) {
			   benBranchCode.on("change", function(checkValue) {
					  if (this.checked){
						  local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_branch_code_req);
						  //registry.getEnclosingWidget(document.getElementById('ben_branch_code_data_req')).set('checked', true);
					  }	else{
						  registry.getEnclosingWidget(document.getElementById('ben_branch_code_data_req')).set('checked', false);
						//RKAZI Rel 8.2 T36000011727 - Changed
						  resetField(document.InvoiceDefinitionDetailForm.ben_branch_add1_req, document.InvoiceDefinitionDetailForm.ben_branch_add1_data_req, "ben_branch_add1", "InvDefFileOrderTableId", "text");						  
					  }				  
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_branch_code_data_req);
		        	  var row = document.getElementById("ben_branch_code_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benBranchCodeData = registry.byId("ben_branch_code_data_req");
		   if (benBranchCodeData) {
			   benBranchCodeData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_branch_code_req").checked, document.InvoiceDefinitionDetailForm.ben_branch_code_data_req);
		          	}
		        });
		   } 
		   
		   var benBranchAdd1 = registry.byId("ben_branch_add1_req");
		   
		   if (benBranchAdd1) {
			   benBranchAdd1.on("change", function(checkValue) {
					  if (this.checked){
						  if (!local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_branch_add1_req)){
							  return false;
						  }
						  if (!local.checkBankNameOrBranchCodeRequiredVal(document.getElementById("ben_bank_name_req").checked, document.getElementById("ben_branch_code_req").checked,
								  document.InvoiceDefinitionDetailForm.ben_branch_add1_req)){
							  return false;
						  }  
						  //registry.getEnclosingWidget(document.getElementById('ben_branch_add1_data_req')).set('checked', true);
					  }	else{
						  registry.getEnclosingWidget(document.getElementById('ben_branch_add1_data_req')).set('checked', false);
						  var thisForm = document.InvoiceDefinitionDetailForm;
						//RKAZI Rel 8.2 T36000011727 - Changed
							resetField(thisForm.ben_branch_add2_req,thisForm.ben_branch_add2_data_req,"ben_branch_add2", "InvDefFileOrderTableId", "text");
							resetField(thisForm.ben_bank_city_req,thisForm.ben_bank_city_data_req,"ben_bank_city", "InvDefFileOrderTableId", "text");
							resetField(thisForm.ben_bank_province_req,thisForm.ben_bank_province_data_req,"ben_bank_province", "InvDefFileOrderTableId", "text");
							resetField(thisForm.ben_branch_country_req,thisForm.ben_branch_country_data_req,"ben_branch_country", "InvDefFileOrderTableId", "text");

					  }							  
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_branch_add1_data_req);
		        	  var row = document.getElementById("ben_branch_add1_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benBranchAdd1Data = registry.byId("ben_branch_add1_data_req");
		   if (benBranchAdd1Data) {
			   benBranchAdd1Data.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_branch_add1_req").checked, document.InvoiceDefinitionDetailForm.ben_branch_add1_data_req);
		          	}
		        });
		   } 
		   
		   var benBranchAdd2 = registry.byId("ben_branch_add2_req");
		   
		   if (benBranchAdd2) {
			   benBranchAdd2.on("change", function(checkValue) {
					  if (this.checked){
						  if (!local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_branch_add2_req)){
							  return false;
						  }
						  local.checkBankAdd1RequiredVal(document.getElementById("ben_branch_add1_req").checked, document.InvoiceDefinitionDetailForm.ben_branch_add2_req);
					  }					  
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_branch_add2_data_req);
		        	  var row = document.getElementById("ben_branch_add2_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benBranchAdd2Data = registry.byId("ben_branch_add2_data_req");
		   if (benBranchAdd2Data) {
			   benBranchAdd2Data.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_branch_add2_req").checked, document.InvoiceDefinitionDetailForm.ben_branch_add2_data_req);
		          	}
				  
		        });
		   } 

		   var benBankCity = registry.byId("ben_bank_city_req");
		   
		   if (benBankCity) {
			   benBankCity.on("change", function(checkValue) {
					  if (this.checked){
						  if (!local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_bank_city_req)){
							  return false;
						  }
						  local.checkBankAdd1RequiredVal(document.getElementById("ben_branch_add1_req").checked, document.InvoiceDefinitionDetailForm.ben_bank_city_req);
					  }					  
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_bank_city_data_req);
		        	  var row = document.getElementById("ben_bank_city_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benBankCityData = registry.byId("ben_bank_city_data_req");
		   if (benBankCityData) {
			   benBankCityData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_bank_city_req").checked, document.InvoiceDefinitionDetailForm.ben_bank_city_data_req);
		          	}
				  
		        });
		   } 

		   var benBankProvince = registry.byId("ben_bank_province_req");
		   
		   if (benBankProvince) {
			   benBankProvince.on("change", function(checkValue) {
					  if (this.checked){
						  if (!local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_bank_province_req)){
							  return false;
						  }
						  local.checkBankAdd1RequiredVal(document.getElementById("ben_branch_add1_req").checked, document.InvoiceDefinitionDetailForm.ben_bank_province_req);
					  }					  
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_bank_province_data_req);
		        	  var row = document.getElementById("ben_bank_province_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benBankProvinceData = registry.byId("ben_bank_province_data_req");
		   if (benBankProvinceData) {
			   benBankProvinceData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_bank_province_req").checked, document.InvoiceDefinitionDetailForm.ben_bank_province_data_req);
		          	}
				  
		        });
		   } 

		   var benBranchCountry = registry.byId("ben_branch_country_req");
		   
		   if (benBranchCountry) {
			   benBranchCountry.on("change", function(checkValue) {
					  if (this.checked){
						  if (!local.checkPayMethodRequiredVal(document.getElementById("pay_method_req").checked, document.InvoiceDefinitionDetailForm.ben_branch_country_req)){
							  return false;
						  }
						  local.checkBankAdd1RequiredVal(document.getElementById("ben_branch_add1_req").checked, document.InvoiceDefinitionDetailForm.ben_branch_country_req);
					  }					  
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.ben_branch_country_data_req);
		        	  var row = document.getElementById("ben_branch_country_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var benBranchCountryData = registry.byId("ben_branch_country_data_req");
		   if (benBranchCountryData) {
			   benBranchCountryData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("ben_branch_country_req").checked, document.InvoiceDefinitionDetailForm.ben_branch_country_data_req);
		          	}
				  
		        });
		   } 

		   var charges = registry.byId("charges_req");
		   
		   if (charges) {
			   charges.on("change", function(checkValue) {
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.charges_data_req);
		        	  var row = document.getElementById("charges_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var chargesData = registry.byId("charges_data_req");
		   if (chargesData) {
			   chargesData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("charges_req").checked, document.InvoiceDefinitionDetailForm.charges_data_req);
		          	}
				  
		        });
		   } 
//CR 1001 Rel 9.4 Rpasupulati Start
		var reportingCode1 = registry.byId("reporting_code_1_req");
		   
		   if (reportingCode1) {
			   reportingCode1.on("change", function(checkValue) {
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.reporting_code_1_data_req);
		        	  resetField(document.InvoiceDefinitionDetailForm.repoatingCode1,document.InvoiceDefinitionDetailForm.reporting_code_1_data_req,"reporting_code_1", "InvDefFileOrderTableId", "text");
		        	  var row = document.getElementById("reporting_code_1_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var reportingCode1Data = registry.byId("reporting_code_1_data_req");
		   if (reportingCode1Data) {
			   reportingCode1Data.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("reporting_code_1_req").checked, document.InvoiceDefinitionDetailForm.reporting_code_1_data_req);
		          	}
				  
		        });
		   } 
		var reportingCode2 = registry.byId("reporting_code_2_req");
		   
		   if (reportingCode2) {
			   reportingCode2.on("change", function(checkValue) {
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.reporting_code_2_data_req);
		        	  resetField(document.InvoiceDefinitionDetailForm.repoatingCode2,document.InvoiceDefinitionDetailForm.reporting_code_2_data_req,"reporting_code_2", "InvDefFileOrderTableId", "text");
		        	  var row = document.getElementById("reporting_code_2_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var reportingCode2Data = registry.byId("reporting_code_2_data_req");
		   if (reportingCode2Data) {
			   reportingCode2Data.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("reporting_code_2_req").checked, document.InvoiceDefinitionDetailForm.reporting_code_2_data_req);
		          	}
				  
		        });
		   } 
		   // CR 1001 Rel 9.4 Rpasupulati End
		   var centralBankRep1 = registry.byId("central_bank_rep1_req");
		   
		   if (centralBankRep1) {
			   centralBankRep1.on("change", function(checkValue) {
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.central_bank_rep1_data_req);
		        	  var row = document.getElementById("central_bank_rep1_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var centralBankRep1Data = registry.byId("central_bank_rep1_data_req");
		   if (centralBankRep1Data) {
			   centralBankRep1Data.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("central_bank_rep1_req").checked, document.InvoiceDefinitionDetailForm.central_bank_rep1_data_req);
		          	}
				  
		        });
		   } 

		   var centralBankRep2 = registry.byId("central_bank_rep2_req");
		   
		   if (centralBankRep2) {
			   centralBankRep2.on("change", function(checkValue) {
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.central_bank_rep2_data_req);
		        	  var row = document.getElementById("central_bank_rep2_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var centralBankRep2Data = registry.byId("central_bank_rep2_data_req");
		   if (centralBankRep2Data) {
			   centralBankRep2Data.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("central_bank_rep2_req").checked, document.InvoiceDefinitionDetailForm.central_bank_rep2_data_req);
		          	}
				  
		        });
		   } 

		   var centralBankRep3 = registry.byId("central_bank_rep3_req");
		   
		   if (centralBankRep3) {
			   centralBankRep3.on("change", function(checkValue) {
				  
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.central_bank_rep3_data_req);
		        	  var row = document.getElementById("central_bank_rep3_req_order");
		        	  deleteOrderField(row, document.getElementById("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   var centralBankRep3Data = registry.byId("central_bank_rep3_data_req");
		   if (centralBankRep3Data) {
			   centralBankRep3Data.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("central_bank_rep3_req").checked, document.InvoiceDefinitionDetailForm.central_bank_rep3_data_req);
		          	}
				  
		        });
		   } 

		   // DK CR709 Rel8.2 Ends
		   
		   var goodsDesc = registry.byId("goods_description_req");
		   if (goodsDesc) {
			   goodsDesc.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummGoodsItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.goods_description_data_req);
		        	  var row = document.getElementById("goods_description_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
		          }
		        });
		   }
		   var goodsDescData = registry.byId("goods_description_data_req");
		   if (goodsDescData) {
			   goodsDescData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("goods_description_req").checked, document.InvoiceDefinitionDetailForm.goods_description_data_req);
		          	}
				  
		        });
		   } 
		   
		   var incoterm = registry.byId("incoterm_req");
		   if (incoterm) {
			   incoterm.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummGoodsItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.incoterm_data_req);
		        	  var row = document.getElementById("incoterm_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
		          }
		        });
		   }
		   var incotermData = registry.byId("incoterm_data_req");
		   if (incotermData) {
			   incotermData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("incoterm_req").checked, document.InvoiceDefinitionDetailForm.incoterm_data_req);
		          	}
				  
		        });
		   } 
		   
		   var contryOfLoading = registry.byId("country_of_loading_req");
		   if (contryOfLoading) {
			   contryOfLoading.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummGoodsItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.country_of_loading_data_req);
		        	  var row = document.getElementById("country_of_loading_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
		          }
		        });
		   } 
		   var contryOfLoadingData = registry.byId("country_of_loading_data_req");
		   if (contryOfLoadingData) {
			   contryOfLoadingData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("country_of_loading_req").checked, document.InvoiceDefinitionDetailForm.country_of_loading_data_req);
		          	}
				  
		        });
		   } 
		   
		   var countryOfDischarge = registry.byId("country_of_discharge_req");
		   if (countryOfDischarge) {
			   countryOfDischarge.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummGoodsItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.country_of_discharge_data_req);
		        	  var row = document.getElementById("country_of_discharge_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
		          }
		        });
		   } 
		   var countryOfDischargeData = registry.byId("country_of_discharge_data_req");
		   if (countryOfDischargeData) {
			   countryOfDischargeData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("country_of_discharge_req").checked, document.InvoiceDefinitionDetailForm.country_of_discharge_data_req);
		          	}
				  
		        });
		   } 
		   
		   var vessel = registry.byId("vessel_req");
		   if (vessel) {
			   vessel.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummGoodsItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.vessel_data_req);
		        	  var row = document.getElementById("vessel_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
		          }
		        });
		   } 
		   var vesselData = registry.byId("vessel_data_req");
		   if (vesselData) {
			   vesselData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("vessel_req").checked, document.InvoiceDefinitionDetailForm.vessel_data_req);
		          	}
				  
		        });
		   } 
		   
		   var carrier = registry.byId("carrier_req");
		   if (carrier) {
			   carrier.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummGoodsItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.carrier_data_req);
		        	  var row = document.getElementById("carrier_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
		          }
		        });
		   } 
		   var carrierData = registry.byId("carrier_data_req");
		   if (carrierData) {
			   carrierData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("carrier_req").checked, document.InvoiceDefinitionDetailForm.carrier_data_req);
		          	}
				  
		        });
		   } 
		   
		   var actualShipDate = registry.byId("actual_ship_date_req");
		   if (actualShipDate) {
			   actualShipDate.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummGoodsItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.actual_ship_date_data_req);
		        	  var row = document.getElementById("actual_ship_date_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
		          }
		        });
		   } 
		   var actualShipDateData = registry.byId("actual_ship_date_data_req");
		   if (actualShipDateData) {
			   actualShipDateData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("actual_ship_date_req").checked, document.InvoiceDefinitionDetailForm.actual_ship_date_data_req);
		          	}
				  
		        });
		   } 
		   
		   var purchaseOrdId = registry.byId("purchase_ord_id_req");
		   if (purchaseOrdId) {
			   purchaseOrdId.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummGoodsItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.purchase_ord_id_data_req);
		        	  var row = document.getElementById("purchase_ord_id_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
		          }
		        });
		   } 
		   
		   var purchaseOrdIdData = registry.byId("purchase_ord_id_data_req");
		   if (purchaseOrdIdData) {
			   purchaseOrdIdData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("purchase_ord_id_req").checked, document.InvoiceDefinitionDetailForm.purchase_ord_id_data_req);
		          	}
				  
		        });
		   } 
		   
		   // Narayan CR914 Rel9.2 03-Nov-2014 Add- Begin
		   var endToEndID = registry.byId("end_to_end_id_req");
		   if ( endToEndID ) {
			 endToEndID.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  thisPage.selectInvSummItem(this);
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.end_to_end_id_data_req);
		        	  var row = dom.byId("end_to_end_id_req_order");
		        	  deleteOrderField(row, dom.byId("InvDefFileOrderTableId"), "text");	
		          }
		        });
		   }
		   
		 var endToEndIDData = registry.byId("end_to_end_id_data_req");
		   if (endToEndIDData) {
			   endToEndIDData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(dom.byId("end_to_end_id_req").checked, document.InvoiceDefinitionDetailForm.end_to_end_id_data_req);
		          	}
				  
		        });
		   }
		   // Narayan CR914 Rel9.2 03-Nov-2014 Add- End
		  		   
		   //Rel8.2 CR-74
		   //AiA - start
		   var creditDiscOrdId = registry.byId("credit_discount_code_id_req");
		   if (creditDiscOrdId) {
			   creditDiscOrdId.on("change", function(checkValue) {
		          if ( checkValue ) {
		        	  if(local.checkInvLabelCreditLabelVal(dom.byId("CreditNoteIndText").value, document.InvoiceDefinitionDetailForm.credit_discount_code_id_req,
     			    "InvoiceDefinition.Label"))
     			  {
		        	    thisPage.selectCreditDiscountItem(this);
		        	  }
		          }
		          else {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.credit_discount_code_id_data_req);
		        	  var row = document.getElementById("credit_discount_code_id_req_order");
		        	  deleteOrderField(row, document.getElementById("discountCodeDataOrderTableId"), "1userDefText");	
		          }
		        });
		   } 
		   
		   var creditDiscOrdIdData = registry.byId("credit_discount_code_id_data_req");
		   if (creditDiscOrdIdData) {
			   creditDiscOrdIdData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	 local.checkRequiredVal(document.getElementById("credit_discount_code_id_req").checked, document.InvoiceDefinitionDetailForm.credit_discount_code_id_data_req);
		          	}
				  
		        });
		   }
		   
		   //discount gl codes
		   var creditDiscOrdId = registry.byId("credit_discount_gl_code_id_req");
		   if (creditDiscOrdId) {
			   creditDiscOrdId.on("change", function(checkValue) {
		          if ( checkValue ) {
			        	if(local.checkInvLabelCreditLabelVal(dom.byId("CreditNoteIndText").value, document.InvoiceDefinitionDetailForm.credit_discount_gl_code_id_req,
	        			  "InvoiceDefinition.Label"))
	        			{
			        	    thisPage.selectCreditDiscountItem(this);
	        			}
			      }
		          else 
		          {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.credit_discount_gl_code_id_data_req);
		        	  var row = document.getElementById("credit_discount_gl_code_id_req_order");
		        	  
		        	  deleteOrderField(row, document.getElementById("discountCodeDataOrderTableId"), "1userDefText");	
		          }
		        });
		   } 
		   
		   var creditDiscOrdIdData = registry.byId("credit_discount_gl_code_id_data_req");
		   if (creditDiscOrdIdData) {
			   creditDiscOrdIdData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	 local.checkRequiredVal(document.getElementById("credit_discount_gl_code_id_req").checked, document.InvoiceDefinitionDetailForm.credit_discount_gl_code_id_data_req);
		          	}
				  
		        });
		   }		   
		   
		   //
		   
		   //discount comments
		   var creditDiscOrdId = registry.byId("credit_discount_comments_id_req");
		   if (creditDiscOrdId) {
			   creditDiscOrdId.on("change", function(checkValue) {
		          if ( checkValue ) {
			        	  if(local.checkInvLabelCreditLabelVal(dom.byId("CreditNoteIndText").value, document.InvoiceDefinitionDetailForm.credit_discount_comments_id_req,
	        			  "InvoiceDefinition.Label"))
	        			  {
			        	    thisPage.selectCreditDiscountItem(this);
			        	  }
			       }
		          else 
		          {
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.credit_discount_comments_id_data_req);
		        	  var row = document.getElementById("credit_discount_comments_id_req_order");
		        	  
		        	  deleteOrderField(row, document.getElementById("discountCodeDataOrderTableId"), "1userDefText");	
		          }
		        });
		   } 
		   
		   var creditDiscOrdIdData = registry.byId("credit_discount_comments_id_data_req");
		   if (creditDiscOrdIdData) {
			   creditDiscOrdIdData.on("change", function(checkValue) {
				   if ( this.checked ) {
			        	 local.checkRequiredVal(document.getElementById("credit_discount_comments_id_req").checked, document.InvoiceDefinitionDetailForm.credit_discount_comments_id_data_req);
		          	}
				  
		        });
		   }
	
		   //CR 913 issues not checking the check box automatically.Rpasupulati END
		   //CR741 - AiA - End
		   
			   var buyerUserDef1 = registry.byId("buyer_users_def1_req");
			   if (buyerUserDef1) {
				   buyerUserDef1.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  if(local.checkInvLabelLabelVal(document.getElementById("buyer_users_def1_label").value, document.InvoiceDefinitionDetailForm.buyer_users_def1_req,
				        			  "InvoiceDefinition.Label")){
			        	   		thisPage.selectInvSummGoodsItem(this);
			          		  }
					   }
			          else {
			        	  var row = document.getElementById("buyer_users_def1_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_users_def1_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('buyer_users_def1_label')).set('value', '');
			          }
			        });
			   } 
			   var buyerUserDef1Label = registry.byId("buyer_users_def1_label");
			   if (buyerUserDef1Label) {

				   buyerUserDef1Label.on("change", function(checkValue) {
					   
					   if ( this.value == '' ) {
						   resetField(document.InvoiceDefinitionDetailForm.buyer_users_def1_req,document.InvoiceDefinitionDetailForm.buyer_users_def1_data_req, "buyer_users_def1", "poStructSumDataOrderTableId", "userDefText");
			          }}
			        );
			   }		
			   
			   var buyerUserDataDef1 = registry.byId("buyer_users_def1_data_req");
			   if (buyerUserDataDef1) {
				   buyerUserDataDef1.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("buyer_users_def1_req").checked, document.InvoiceDefinitionDetailForm.buyer_users_def1_data_req);
			          	}
					  
			        });
			   } 
			   
			   var buyerUserDef2 = registry.byId("buyer_users_def2_req");
			   if (buyerUserDef2) {
				   buyerUserDef2.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  if(local.checkInvLabelLabelVal(document.getElementById("buyer_users_def2_label").value, document.InvoiceDefinitionDetailForm.buyer_users_def2_req,
				        			  "InvoiceDefinition.Label")){
				        	  			        	   thisPage.selectInvSummGoodsItem(this);
			          }
					   }
			          else {
			        	  var row = document.getElementById("buyer_users_def2_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_users_def2_data_req);	
			        	  registry.getEnclosingWidget(document.getElementById('buyer_users_def2_label')).set('value', '');
			          }
			        });
			   } 
			   var buyerUserDef2Label = registry.byId("buyer_users_def2_label");
			   if (buyerUserDef2Label) {

				   buyerUserDef2Label.on("change", function(checkValue) {
					   
					   if ( this.value == '' ) {
						   resetField(document.InvoiceDefinitionDetailForm.buyer_users_def2_req,document.InvoiceDefinitionDetailForm.buyer_users_def2_data_req, "buyer_users_def2", "poStructSumDataOrderTableId", "userDefText");
			          }}
			        );
			   }		
			   
			   var buyerUserDataDef2 = registry.byId("buyer_users_def2_data_req");
			   if (buyerUserDataDef2) {
				   buyerUserDataDef2.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("buyer_users_def2_req").checked, document.InvoiceDefinitionDetailForm.buyer_users_def2_data_req);
			          	}
					  
			        });
			   } 
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def3_req:click", function(checkValue) {
			          if ( this.checked ) {
			        	 
				        	  if(local.checkInvLabelLabelVal(document.getElementById("buyer_users_def3_label").value, document.InvoiceDefinitionDetailForm.buyer_users_def3_req,
				        			  "InvoiceDefinition.Label")){
			        	  thisPage.selectInvSummGoodsItem(this);
			          }
					   }
			          else {
			        	  var row = document.getElementById("buyer_users_def3_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_users_def3_data_req);		
			        	  registry.getEnclosingWidget(document.getElementById('buyer_users_def3_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#buyer_users_def3_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.buyer_users_def3_req,document.InvoiceDefinitionDetailForm.buyer_users_def3_data_req, "buyer_users_def3", "poStructSumDataOrderTableId", "userDefText");
		          }

		        });

			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def3_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("buyer_users_def3_req").checked, document.InvoiceDefinitionDetailForm.buyer_users_def3_data_req);
		          	}

			        });
			   
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def4_req:click", function(checkValue) {
			          if ( this.checked ) {
				        if(local.checkInvLabelLabelVal(document.getElementById("buyer_users_def4_label").value, document.InvoiceDefinitionDetailForm.buyer_users_def4_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}else {
		        	  var row = document.getElementById("buyer_users_def4_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
		        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_users_def4_data_req);
		        	  registry.getEnclosingWidget(document.getElementById('buyer_users_def4_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#buyer_users_def4_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.buyer_users_def4_req,document.InvoiceDefinitionDetailForm.buyer_users_def4_data_req, "buyer_users_def4", "poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def4_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("buyer_users_def4_req").checked, document.InvoiceDefinitionDetailForm.buyer_users_def4_data_req);
		          	}

			        });
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def5_req:click", function(checkValue) {
			          if ( this.checked ) {
				        	  if(local.checkInvLabelLabelVal(document.getElementById("buyer_users_def5_label").value, document.InvoiceDefinitionDetailForm.buyer_users_def5_req,
				        			 "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("buyer_users_def5_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_users_def5_data_req);	
			        	  registry.getEnclosingWidget(document.getElementById('buyer_users_def5_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#buyer_users_def5_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.buyer_users_def5_req,document.InvoiceDefinitionDetailForm.buyer_users_def5_data_req, "buyer_users_def5", "poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			    
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def5_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("buyer_users_def5_req").checked, document.InvoiceDefinitionDetailForm.buyer_users_def5_data_req);
		          	}

			        });
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def6_req:click", function(checkValue) {
			          if ( this.checked ) {
				        	  if(local.checkInvLabelLabelVal(document.getElementById("buyer_users_def6_label").value, document.InvoiceDefinitionDetailForm.buyer_users_def6_req,
				        			  "InvoiceDefinition.Label")){
			        	  thisPage.selectInvSummGoodsItem(this);			        	  
			          }}
			          else {
			        	  var row = document.getElementById("buyer_users_def6_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_users_def6_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('buyer_users_def6_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#buyer_users_def6_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.buyer_users_def6_req,document.InvoiceDefinitionDetailForm.buyer_users_def6_data_req, "buyer_users_def6", "poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def6_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("buyer_users_def6_req").checked, document.InvoiceDefinitionDetailForm.buyer_users_def6_data_req);
		          	}

			        });
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def7_req:click", function(checkValue) {
			          if ( this.checked ) {
				        	  if(local.checkInvLabelLabelVal(document.getElementById("buyer_users_def7_label").value, document.InvoiceDefinitionDetailForm.buyer_users_def7_req,
				        			  "InvoiceDefinition.Label")){
				        		  thisPage.selectInvSummGoodsItem(this);
				        	  }
			          }
			          else {
			        	  var row = document.getElementById("buyer_users_def7_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_users_def7_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('buyer_users_def7_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#buyer_users_def7_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.buyer_users_def7_req,document.InvoiceDefinitionDetailForm.buyer_users_def7_data_req, "buyer_users_def7", "poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def7_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("buyer_users_def7_req").checked, document.InvoiceDefinitionDetailForm.buyer_users_def7_data_req);
		          	}

			        });
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def8_req:click", function(checkValue) {
			          if ( this.checked ) {
				        	  if(local.checkInvLabelLabelVal(document.getElementById("buyer_users_def8_label").value, document.InvoiceDefinitionDetailForm.buyer_users_def8_req,
				        			  "InvoiceDefinition.Label")){
			        	  thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("buyer_users_def8_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_users_def8_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('buyer_users_def8_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#buyer_users_def8_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.buyer_users_def8_req,document.InvoiceDefinitionDetailForm.buyer_users_def8_data_req, "buyer_users_def8", "poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			   
			  
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def8_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("buyer_users_def8_req").checked, document.InvoiceDefinitionDetailForm.buyer_users_def8_data_req);
		          	}

			        });
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def9_req:click", function(checkValue) {
			          if ( this.checked ) {
				        	  if(local.checkInvLabelLabelVal(document.getElementById("buyer_users_def9_label").value, document.InvoiceDefinitionDetailForm.buyer_users_def9_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("buyer_users_def9_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_users_def9_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('buyer_users_def9_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#buyer_users_def9_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.buyer_users_def9_req,document.InvoiceDefinitionDetailForm.buyer_users_def9_data_req, "buyer_users_def9", "poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def9_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("buyer_users_def9_req").checked, document.InvoiceDefinitionDetailForm.buyer_users_def9_data_req);
		          	}

			        });
			   			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def10_req:click", function(checkValue) {
			          if ( this.checked ) {
				        	  if(local.checkInvLabelLabelVal(document.getElementById("buyer_users_def10_label").value, document.InvoiceDefinitionDetailForm.buyer_users_def10_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("buyer_users_def10_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.buyer_users_def10_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('buyer_users_def10_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#buyer_users_def10_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.buyer_users_def10_req,document.InvoiceDefinitionDetailForm.buyer_users_def10_data_req, "buyer_users_def10", "poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			   
			   query('#InvBuyerDefinedTableId').on("#buyer_users_def10_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("buyer_users_def10_req").checked, document.InvoiceDefinitionDetailForm.buyer_users_def10_data_req);
		          	}

			        });
			   
			   var sellerUserDef1 = registry.byId("seller_users_def1_req");
			   if (sellerUserDef1) {
				   sellerUserDef1.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  if(local.checkInvSellerLabelVal(document.getElementById("seller_users_def1_label").value, document.InvoiceDefinitionDetailForm.seller_users_def1_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("seller_users_def1_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_users_def1_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('seller_users_def1_label')).set('value', '');
			          }
			        });
			   }
			   var sellerUserDef1Label = registry.byId("seller_users_def1_label");
			   if (sellerUserDef1Label) {

				   sellerUserDef1Label.on("change", function(checkValue) {
					   
					   if ( this.value == '' ) {
						   resetField(document.InvoiceDefinitionDetailForm.seller_users_def1_req,document.InvoiceDefinitionDetailForm.seller_users_def1_data_req, "seller_users_def1", "poStructSumDataOrderTableId", "userDefText");
			          }}
			        );
			   }		
      
			   var sellerUserDataDef1 = registry.byId("seller_users_def1_data_req");
			   if (sellerUserDataDef1) {
				   sellerUserDataDef1.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("seller_users_def1_req").checked, document.InvoiceDefinitionDetailForm.seller_users_def1_data_req);
			          	}
					  
			        });
			   } 
			   
			   var sellerUserDef2 = registry.byId("seller_users_def2_req");
			   if (sellerUserDef2) {
				   sellerUserDef2.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  if(local.checkInvSellerLabelVal(document.getElementById("seller_users_def2_label").value, document.InvoiceDefinitionDetailForm.seller_users_def2_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("seller_users_def2_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_users_def2_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('seller_users_def2_label')).set('value', '');
			          }
			        });
			   }

			   var sellerUserDef2Label = registry.byId("seller_users_def2_label");
			   if (sellerUserDef2Label) {

				   sellerUserDef2Label.on("change", function(checkValue) {
					   
					   if ( this.value == '' ) {
						   resetField(document.InvoiceDefinitionDetailForm.seller_users_def2_req,document.InvoiceDefinitionDetailForm.seller_users_def2_data_req, "seller_users_def2","poStructSumDataOrderTableId", "userDefText");
			          }}
			        );
			   }		
			   
			   var sellerUserDataDef2 = registry.byId("seller_users_def2_data_req");
			   if (sellerUserDataDef2) {
				   sellerUserDataDef2.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("seller_users_def2_req").checked, document.InvoiceDefinitionDetailForm.seller_users_def2_data_req);
			          	}
					  
			        });
			   } 
			   
			   query('#InvSellerDefinedTableId').on("#seller_users_def3_req:click", function(checkValue) {
	 		          if ( this.checked ) {
				        	  if(local.checkInvSellerLabelVal(document.getElementById("seller_users_def3_label").value, document.InvoiceDefinitionDetailForm.seller_users_def3_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("seller_users_def3_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_users_def3_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('seller_users_def3_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def3_label:change", function(checkValue) {
					   if ( this.value == '' ) {
						   resetField(document.InvoiceDefinitionDetailForm.seller_users_def3_req,document.InvoiceDefinitionDetailForm.seller_users_def3_data_req, "seller_users_def3","poStructSumDataOrderTableId", "userDefText");
			          }

			        });

			   query('#InvSellerDefinedTableId').on("#seller_users_def3_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("seller_users_def3_req").checked, document.InvoiceDefinitionDetailForm.seller_users_def3_data_req);
		          	}

			        });
			   
			   query('#InvSellerDefinedTableId').on("#seller_users_def4_req:click", function(checkValue) {
	 		          if ( this.checked ) {
				        	  if(local.checkInvSellerLabelVal(document.getElementById("seller_users_def4_label").value, document.InvoiceDefinitionDetailForm.seller_users_def4_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("seller_users_def4_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_users_def4_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('seller_users_def4_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def4_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.seller_users_def4_req,document.InvoiceDefinitionDetailForm.seller_users_def4_data_req, "seller_users_def4","poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			   
			   query('#InvSellerDefinedTableId').on("#seller_users_def4_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("seller_users_def4_req").checked, document.InvoiceDefinitionDetailForm.seller_users_def4_data_req);
		          	}

			        });
			   
			   query('#InvSellerDefinedTableId').on("#seller_users_def5_req:click", function(checkValue) {
	 		          if ( this.checked ) {
				        	  if(local.checkInvSellerLabelVal(document.getElementById("seller_users_def5_label").value, document.InvoiceDefinitionDetailForm.seller_users_def5_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("seller_users_def5_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_users_def5_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('seller_users_def5_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def5_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.seller_users_def5_req,document.InvoiceDefinitionDetailForm.seller_users_def5_data_req, "seller_users_def5","poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			 
			   query('#InvSellerDefinedTableId').on("#seller_users_def5_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("seller_users_def5_req").checked, document.InvoiceDefinitionDetailForm.seller_users_def5_data_req);
		          	}

			        });
			   
			   query('#InvSellerDefinedTableId').on("#seller_users_def6_req:click", function(checkValue) {
	 		          if ( this.checked ) {
				        	  if(local.checkInvSellerLabelVal(document.getElementById("seller_users_def6_label").value, document.InvoiceDefinitionDetailForm.seller_users_def6_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("seller_users_def6_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_users_def6_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('seller_users_def6_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def6_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.seller_users_def6_req,document.InvoiceDefinitionDetailForm.seller_users_def6_data_req, "seller_users_def6","poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			
			   query('#InvSellerDefinedTableId').on("#seller_users_def6_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("seller_users_def6_req").checked, document.InvoiceDefinitionDetailForm.seller_users_def6_data_req);
		          	}

			        });
			   
			   query('#InvSellerDefinedTableId').on("#seller_users_def7_req:click", function(checkValue) {
	 		          if ( this.checked ) {
				        	  if(local.checkInvSellerLabelVal(document.getElementById("seller_users_def7_label").value, document.InvoiceDefinitionDetailForm.seller_users_def7_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("seller_users_def7_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_users_def7_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('seller_users_def7_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def7_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.seller_users_def7_req,document.InvoiceDefinitionDetailForm.seller_users_def7_data_req, "seller_users_def7","poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			  
			   query('#InvSellerDefinedTableId').on("#seller_users_def7_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("seller_users_def7_req").checked, document.InvoiceDefinitionDetailForm.seller_users_def7_data_req);
		          	}

			        });
			   
			   query('#InvSellerDefinedTableId').on("#seller_users_def8_req:click", function(checkValue) {
	 		          if ( this.checked ) {
				        	  if(local.checkInvSellerLabelVal(document.getElementById("seller_users_def8_label").value, document.InvoiceDefinitionDetailForm.seller_users_def8_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("seller_users_def8_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_users_def8_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('seller_users_def8_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def8_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.seller_users_def8_req,document.InvoiceDefinitionDetailForm.seller_users_def8_data_req, "seller_users_def8","poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			 
			   query('#InvSellerDefinedTableId').on("#seller_users_def8_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("seller_users_def8_req").checked, document.InvoiceDefinitionDetailForm.seller_users_def8_data_req);
		          	}

			        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def9_req:click", function(checkValue) {
	 		          if ( this.checked ) {
				        	  if(local.checkInvSellerLabelVal(document.getElementById("seller_users_def9_label").value, document.InvoiceDefinitionDetailForm.seller_users_def9_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("seller_users_def9_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_users_def9_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('seller_users_def9_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def9_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.seller_users_def9_req,document.InvoiceDefinitionDetailForm.seller_users_def9_data_req, "seller_users_def9","poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def9_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("seller_users_def9_req").checked, document.InvoiceDefinitionDetailForm.seller_users_def9_data_req);
		          	}

			        });
			   
			   query('#InvSellerDefinedTableId').on("#seller_users_def10_req:click", function(checkValue) {
	 		          if ( this.checked ) {
				        	  if(local.checkInvSellerLabelVal(document.getElementById("seller_users_def10_label").value, document.InvoiceDefinitionDetailForm.seller_users_def10_req,
				        			  "InvoiceDefinition.Label")){
			        	   thisPage.selectInvSummGoodsItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("seller_users_def10_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.seller_users_def10_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('seller_users_def10_label')).set('value', '');
			          }
			        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def10_label:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.seller_users_def10_req,document.InvoiceDefinitionDetailForm.seller_users_def10_data_req, "seller_users_def10","poStructSumDataOrderTableId", "userDefText");
		          }

		        });
			   query('#InvSellerDefinedTableId').on("#seller_users_def10_data_req:click", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("seller_users_def10_req").checked, document.InvoiceDefinitionDetailForm.seller_users_def10_data_req);
		          	}

			        });

			   var lineItemDetaiProvided = registry.byId("line_item_detail_provided");
			   if (lineItemDetaiProvided) {
				   lineItemDetaiProvided.on("change", function(checkValue) {
			          if ( !checkValue ) {
			        	  resetField(document.InvoiceDefinitionDetailForm.line_item_id_req, document.InvoiceDefinitionDetailForm.line_item_id_data_req,"line_item_id","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.unit_price_req, document.InvoiceDefinitionDetailForm.unit_price_data_req,"unit_price","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.unit_of_measure_price_req, document.InvoiceDefinitionDetailForm.unit_of_measure_price_data_req,"unit_of_measure_price","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.inv_quantity_req, document.InvoiceDefinitionDetailForm.inv_quantity_data_req,"inv_quantity","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.unit_of_measure_quantity_req, document.InvoiceDefinitionDetailForm.unit_of_measure_quantity_data_req,"unit_of_measure_quantity","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud1_type_req, document.InvoiceDefinitionDetailForm.prod_chars_ud1_type_data_req,"prod_chars_ud1_type","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud2_type_req, document.InvoiceDefinitionDetailForm.prod_chars_ud2_type_data_req,"prod_chars_ud2_type","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud3_type_req, document.InvoiceDefinitionDetailForm.prod_chars_ud3_type_data_req,"prod_chars_ud3_type","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud4_type_req, document.InvoiceDefinitionDetailForm.prod_chars_ud4_type_data_req,"prod_chars_ud4_type","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud5_type_req, document.InvoiceDefinitionDetailForm.prod_chars_ud5_type_data_req,"prod_chars_ud5_type","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud6_type_req, document.InvoiceDefinitionDetailForm.prod_chars_ud6_type_data_req,"prod_chars_ud6_type","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud7_type_req, document.InvoiceDefinitionDetailForm.prod_chars_ud7_type_data_req,"prod_chars_ud7_type","poStructSumLineItemOrderTableId", "userDefLineText");
			        	  resetProCharType(document.getElementById('prod_chars_ud1_type'));
			        	  resetProCharType(document.getElementById('prod_chars_ud2_type'));
			        	  resetProCharType(document.getElementById('prod_chars_ud3_type'));
			        	  resetProCharType(document.getElementById('prod_chars_ud4_type'));
			        	  resetProCharType(document.getElementById('prod_chars_ud5_type'));
			        	  resetProCharType(document.getElementById('prod_chars_ud6_type'));
			        	  resetProCharType(document.getElementById('prod_chars_ud7_type'));
		
			          }
			        });
			   }
						   
			   var lineItemId = registry.byId("line_item_id_req");
			   if (lineItemId) {
				   lineItemId.on("change", function(checkValue) {
			          if ( checkValue ) {
			        	   thisPage.selectInvSummLineItem(this);
			          }
			          else {
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.line_item_id_data_req);
			        	  var row = document.getElementById("line_item_id_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			          }
			        });
			   }
			   
			   var lineItemIdData = registry.byId("line_item_id_data_req");
			   if (lineItemIdData) {
				   lineItemIdData.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("line_item_id_req").checked, document.InvoiceDefinitionDetailForm.line_item_id_data_req);
			          	}
					  
			        });
			   } 

			   var unitPrice = registry.byId("unit_price_req");
			   if (unitPrice) {
				   unitPrice.on("change", function(checkValue) {
			          if ( checkValue ) {
			        	   thisPage.selectInvSummLineItem(this);
			          }
			          else {
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.unit_price_data_req);
			        	  var row = document.getElementById("unit_price_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			          }
			        });
			   }
			   
			   var unitPriceData = registry.byId("unit_price_data_req");
			   if (unitPriceData) {
				   unitPriceData.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("unit_price_req").checked, document.InvoiceDefinitionDetailForm.unit_price_data_req);
			          	}
					  
			        });
			   } 

			   var unitOfMeasurePrice = registry.byId("unit_of_measure_price_req");
			   if (unitOfMeasurePrice) {
				   unitOfMeasurePrice.on("change", function(checkValue) {
			          if ( checkValue ) {
			        	   thisPage.selectInvSummLineItem(this);
			          }
			          else {
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.unit_of_measure_price_data_req);
			        	  var row = document.getElementById("unit_of_measure_price_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			          }
			        });
			   }
			   
			   var unitOfMeasurePriceData = registry.byId("unit_of_measure_price_data_req");
			   if (unitOfMeasurePriceData) {
				   unitOfMeasurePriceData.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("unit_of_measure_price_req").checked, document.InvoiceDefinitionDetailForm.unit_of_measure_price_data_req);
			          	}
					  
			        });
			   } 

			   var invQuantity = registry.byId("inv_quantity_req");
			   if (invQuantity) {
				   invQuantity.on("change", function(checkValue) {
			          if ( checkValue ) {
			        	   thisPage.selectInvSummLineItem(this);
			          }
			          else {
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.inv_quantity_data_req);
			        	  var row = document.getElementById("inv_quantity_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			          }
			        });
			   }
			   
			   var invQuantityData = registry.byId("inv_quantity_data_req");
			   if (invQuantityData) {
				   invQuantityData.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("inv_quantity_req").checked, document.InvoiceDefinitionDetailForm.inv_quantity_data_req);
			          	}
					  
			        });
			   } 

			   var unitOfMeasureqty = registry.byId("unit_of_measure_quantity_req");
			   if (unitOfMeasureqty) {
				   unitOfMeasureqty.on("change", function(checkValue) {
			          if ( checkValue ) {
			        	   thisPage.selectInvSummLineItem(this);
			          }
			          else {
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.unit_of_measure_quantity_data_req);
			        	  var row = document.getElementById("unit_of_measure_quantity_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			          }
			        });
			   }
			   
			   var unitOfMeasureqtyData = registry.byId("unit_of_measure_quantity_data_req");
			   if (unitOfMeasureqtyData) {
				   unitOfMeasureqtyData.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("unit_of_measure_quantity_req").checked, document.InvoiceDefinitionDetailForm.unit_of_measure_quantity_data_req);
			          	}
					  
			        });
			   } 

			   var prodCharType1 = registry.byId("prod_chars_ud1_type_req");
			   if (prodCharType1) {
				   prodCharType1.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  if(local.checkProdLabelLabelVal(document.getElementById("prod_chars_ud1_type").value, document.InvoiceDefinitionDetailForm.prod_chars_ud1_type_req,
				        			  "InvoiceDefinition.FieldName")){
			        	   thisPage.selectInvSummLineItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("prod_chars_ud1_type_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.prod_chars_ud1_type_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('prod_chars_ud1_type')).set('value', '');
			          }
			        });
			   }
			   var prodCharType1Label = registry.byId("prod_chars_ud1_type");
			   if (prodCharType1Label) {
				   prodCharType1Label.on("change", function(checkValue) {
					   if ( this.value == '' ) {
						   resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud1_type_req,document.InvoiceDefinitionDetailForm.prod_chars_ud1_type_data_req, "prod_chars_ud1_type", "poStructSumLineItemOrderTableId", "userDefLineText");
			          }
			          
			        });
			   }			   
			   var prodCharDataDef1 = registry.byId("prod_chars_ud1_type_data_req");
			   if (prodCharDataDef1) {
				   prodCharDataDef1.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("prod_chars_ud1_type_req").checked, document.InvoiceDefinitionDetailForm.prod_chars_ud1_type_data_req);
			          	}
					  
			        });
			   } 
			   
			   var prodCharType2 = registry.byId("prod_chars_ud2_type_req");
			   if (prodCharType2) {
				   prodCharType2.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  if(local.checkProdLabelLabelVal(document.getElementById("prod_chars_ud2_type").value, document.InvoiceDefinitionDetailForm.prod_chars_ud2_type_req,
				        			  "InvoiceDefinition.FieldName")){
			        	   thisPage.selectInvSummLineItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("prod_chars_ud2_type_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.prod_chars_ud2_type_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('prod_chars_ud2_type')).set('value', '');
			          }
			        });
			   }
			   var prodCharType2Label = registry.byId("prod_chars_ud2_type");
			   if (prodCharType2Label) {
				   prodCharType2Label.on("change", function(checkValue) {
					   if ( this.value == '' ) {
						   resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud2_type_req,document.InvoiceDefinitionDetailForm.prod_chars_ud2_type_data_req, "prod_chars_ud2_type", "poStructSumLineItemOrderTableId", "userDefLineText");
			          }
			          
			        });
			   }			   
			   var prodCharDataDef2 = registry.byId("prod_chars_ud2_type_data_req");
			   if (prodCharDataDef2) {
				   prodCharDataDef2.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("prod_chars_ud2_type_req").checked, document.InvoiceDefinitionDetailForm.prod_chars_ud2_type_data_req);
			          	}
					  
			        });
			   } 
			   
			   query('#InvUserDefinitionLineTableId').on("#prod_chars_ud3_type_req:click", function(checkValue) {
	 		          if ( this.checked ) {
				        	  if(local.checkProdLabelLabelVal(document.getElementById("prod_chars_ud3_type").value, document.InvoiceDefinitionDetailForm.prod_chars_ud3_type_req,
				        			  "InvoiceDefinition.FieldName")){
			        	   thisPage.selectInvSummLineItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("prod_chars_ud3_type_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.prod_chars_ud3_type_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('prod_chars_ud3_type')).set('value', '');
			          }
			        });
			   query('#InvUserDefinitionLineTableId').on("#prod_chars_ud3_type:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud3_type_req,document.InvoiceDefinitionDetailForm.prod_chars_ud3_type_data_req, "prod_chars_ud3_type", "poStructSumLineItemOrderTableId", "userDefLineText");
		          }

		        });
			   
			  
			   var prodCharDataDef3 = registry.byId("prod_chars_ud3_type_data_req");
			   if (prodCharDataDef3) {
				   prodCharDataDef3.on("change", function(checkValue) {
					   if ( this.checked ) {
				        	  local.checkRequiredVal(document.getElementById("prod_chars_ud3_type_req").checked, document.InvoiceDefinitionDetailForm.prod_chars_ud3_type_data_req);
			          	}
					  
			        });
			   } 
      	query('#InvUserDefinitionLineTableId').on("#prod_chars_ud4_type_req:click", function(checkValue) {
	          if ( this.checked ) {
				        	  if(local.checkProdLabelLabelVal(document.getElementById("prod_chars_ud4_type").value, document.InvoiceDefinitionDetailForm.prod_chars_ud4_type_req,
				        			  "InvoiceDefinition.FieldName")){
			        	   thisPage.selectInvSummLineItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("prod_chars_ud4_type_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.prod_chars_ud4_type_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('prod_chars_ud4_type')).set('value', '');
			          }
			        });
			   query('#InvUserDefinitionLineTableId').on("#prod_chars_ud4_type:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud4_type_req,document.InvoiceDefinitionDetailForm.prod_chars_ud4_type_data_req, "prod_chars_ud4_type", "poStructSumLineItemOrderTableId", "userDefLineText");
		          }

		        });
			   
      	 query('#InvUserDefinitionLineTableId').on("#prod_chars_ud4_type_data_req:change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("prod_chars_ud4_type_req").checked, document.InvoiceDefinitionDetailForm.prod_chars_ud4_type_data_req);
		          	}

			        });
			query('#InvUserDefinitionLineTableId').on("#prod_chars_ud5_type_req:click", function(checkValue) {
			  if ( this.checked ) {
				        	  if(local.checkProdLabelLabelVal(document.getElementById("prod_chars_ud5_type").value, document.InvoiceDefinitionDetailForm.prod_chars_ud5_type_req,
				        			  "InvoiceDefinition.FieldName")){
			        	   thisPage.selectInvSummLineItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("prod_chars_ud5_type_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.prod_chars_ud5_type_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('prod_chars_ud5_type')).set('value', '');
			          }
			        });
			   query('#InvUserDefinitionLineTableId').on("#prod_chars_ud5_type:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud5_type_req,document.InvoiceDefinitionDetailForm.prod_chars_ud5_type_data_req, "prod_chars_ud5_type", "poStructSumLineItemOrderTableId", "userDefLineText");
		          }

		        });
			   
			  
			 query('#InvUserDefinitionLineTableId').on("#prod_chars_ud5_type_data_req:change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("prod_chars_ud5_type_req").checked, document.InvoiceDefinitionDetailForm.prod_chars_ud5_type_data_req);
		          	}

			        });
			 query('#InvUserDefinitionLineTableId').on("#prod_chars_ud6_type_req:click", function(checkValue) {
		          if ( this.checked ) {
				        	  if(local.checkProdLabelLabelVal(document.getElementById("prod_chars_ud6_type").value, document.InvoiceDefinitionDetailForm.prod_chars_ud6_type_req,
				        			  "InvoiceDefinition.FieldName")){
			        	   thisPage.selectInvSummLineItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("prod_chars_ud6_type_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.prod_chars_ud6_type_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('prod_chars_ud7_type')).set('value', '');
			          }
			        });
			   query('#InvUserDefinitionLineTableId').on("#prod_chars_ud6_type:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud6_type_req,document.InvoiceDefinitionDetailForm.prod_chars_ud6_type_data_req, "prod_chars_ud6_type", "poStructSumLineItemOrderTableId", "userDefLineText");
		          }

		        });
			   
			 query('#InvUserDefinitionLineTableId').on("#prod_chars_ud6_type_data_req:change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("prod_chars_ud6_type_req").checked, document.InvoiceDefinitionDetailForm.prod_chars_ud6_type_data_req);
		          	}

			        });
			   
			 query('#InvUserDefinitionLineTableId').on("#prod_chars_ud7_type_req:click", function(checkValue) {
		          if ( this.checked ) {
				        	  if(local.checkProdLabelLabelVal(document.getElementById("prod_chars_ud7_type").value, document.InvoiceDefinitionDetailForm.prod_chars_ud7_type_req,
				        			  "InvoiceDefinition.FieldName")){
			        	   thisPage.selectInvSummLineItem(this);
			          }}
			          else {
			        	  var row = document.getElementById("prod_chars_ud7_type_req_order");
			        	  deleteOrderField(row, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");	
			        	  unsetDataReq(document.InvoiceDefinitionDetailForm.prod_chars_ud7_type_data_req);
			        	  registry.getEnclosingWidget(document.getElementById('prod_chars_ud7_type')).set('value', '');
			          }
			        });
			   query('#InvUserDefinitionLineTableId').on("#prod_chars_ud7_type:change", function(checkValue) {
				   if ( this.value == '' ) {
					   resetField(document.InvoiceDefinitionDetailForm.prod_chars_ud7_type_req,document.InvoiceDefinitionDetailForm.prod_chars_ud7_type_data_req, "prod_chars_ud7_type", "poStructSumLineItemOrderTableId", "userDefLineText");
		          }

		        });
			   
			 query('#InvUserDefinitionLineTableId').on("#prod_chars_ud7_type_data_req:change", function(checkValue) {
				   if ( this.checked ) {
			        	  local.checkRequiredVal(document.getElementById("prod_chars_ud7_type_req").checked, document.InvoiceDefinitionDetailForm.prod_chars_ud7_type_data_req);
		          	}

			        });

			    query('#InvDefFileOrderTableId').on(".deleteSelectedItem:click", function(evt) {
				      thisPage.deleteEventHandler(this, document.getElementById("InvDefFileOrderTableId"), "text");
				     
				    });
			    query('#poStructSumDataOrderTableId').on(".deleteSelectedItem:click", function(evt) {
				      thisPage.deleteEventHandler(this, document.getElementById("poStructSumDataOrderTableId"), "userDefText");
				     
				    });
			    query('#poStructSumLineItemOrderTableId').on(".deleteSelectedItem:click", function(evt) {
				      thisPage.deleteEventHandler(this, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");
				      
				    });
			    //Rel8.2 CR-741 - Delete function for discount codes
			    query('#discountCodeDataOrderTableId').on(".deleteSelectedItem:click", function(evt) {
			    	
				      thisPage.deleteEventHandler(this, document.getElementById("discountCodeDataOrderTableId"), "1userDefText");
				      
				    });			    
     })
	  
	 
 });