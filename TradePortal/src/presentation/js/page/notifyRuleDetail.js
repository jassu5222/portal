
//library for all of the Notification Rule Template detail page functions
// this is 2nd choice becuase we'd prefer having eventhandlers and use dojo asynch to load
// todo: make all event handlers

//load everything once
	require(["dojo/_base/array", "dojo/_base/xhr", "dojo/dom", "dojo/dom-class", 
	         "dojo/dom-construct","dojo/dom-style","dojo/query", "dijit/registry",
	         "dojo/parser", "t360/common", "t360/dialog", "dojo/store/Memory","dojo/ready"], 
	         function(baseArray, xhr, dom,  domClass, domConstruct, domStyle, query, registry, parser, common, dialog, Memory, ready ) {
		var secNum;
		var uiType;
		var instrumentType;
		var transArrayStr;
		var transCriteriaStartCount;
		
	    local.applyToAllGroups = function(dynaInstrTypesStr, dynaInstrTransTypesStr) {
	    	var dynaInstrArray = new Array();
	    	var dynaInstrTransArray = new Array();
	    	var instrTransArray = new Array();
	    	var tempTransStr;
	    	
	    	dynaInstrArray = dynaInstrTypesStr.split(',');
	    	if(dynaInstrArray.indexOf('LRQ_INV') > -1){
	    		dynaInstrArray.splice(dynaInstrArray.indexOf('LRQ_INV'), 1);
	    	}
	    	if(dynaInstrArray.indexOf('H2H_INV_CRN') > -1){
	    		dynaInstrArray.splice(dynaInstrArray.indexOf('H2H_INV_CRN'), 1);
	    	}
	    	 
	    	instrTransArray = dynaInstrTransTypesStr.split('||');
	    	
	    	for(var i = 0; i < instrTransArray.length; i++){
	    		tempTransStr = instrTransArray[i];
	    		dynaInstrTransArray[i] = tempTransStr.split(',');
	    	}
	    	
	    	if(registry.byId('defaultNotifySettingAlways').checked){
				document.getElementById("defaultApplyToAllGroup").value="Y";
	    		local.checkAllRadioGroups('notifySettingAlways', dynaInstrArray.length);
	    	}else if(registry.byId('defaultNotifySettingNone').checked){
				document.getElementById("defaultApplyToAllGroup").value="Y";
	    		local.checkAllRadioGroups('notifySettingNone', dynaInstrArray.length);
	    	}else if(registry.byId('defaultNotifySettingChrgsDocsOnly').checked){
				document.getElementById("defaultApplyToAllGroup").value="Y";
	    		local.checkAllRadioGroups('notifySettingChrgsDocsOnly', dynaInstrArray.length);
	    	}else{
	    		local.unCheckAllRadioGroups(['notifySettingAlways', 'notifySettingNone', 'notifySettingChrgsDocsOnly'], dynaInstrArray.length);
	    	}
	    	
	    	if(registry.byId('defaultEmailSettingAlways').checked){
				document.getElementById("defaultApplyToAllGroup").value="Y";
	    		local.checkAllRadioGroups('emailSettingAlways', dynaInstrArray.length);
	    	}else if(registry.byId('defaultEmailSettingNone').checked){
				document.getElementById("defaultApplyToAllGroup").value="Y";
	    		local.checkAllRadioGroups('emailSettingNone', dynaInstrArray.length);
	    	}else if(registry.byId('defaultEmailSettingChrgsDocsOnly').checked){
				document.getElementById("defaultApplyToAllGroup").value="Y";
	    		local.checkAllRadioGroups('emailSettingChrgsDocsOnly', dynaInstrArray.length);
	    	}else{
	    		local.unCheckAllRadioGroups(['emailSettingAlways', 'emailSettingNone','emailSettingChrgsDocsOnly'], dynaInstrArray.length);
	    	}
	    	
	    	var defAddlEmailAddress = registry.byId('defaultAdditionalEmailAddr').getValue();
	    	for(i=1; i<=dynaInstrArray.length; i++){
    			registry.byId("additionalEmailAddr"+i).setValue(defAddlEmailAddress);
				
			//	if(defAddlEmailAddress !=''){
				document.getElementById("applyInst"+i).value="Y";
    		//}
    		}
	    	

	    	var defaultTable = dojo.byId('NotifUsersTableDefault');
	    	var instrTable;
	    	var rowsToCreate;
	    	var instrEmailIdCounter = 0;
	    	var emailIdCount;
	    
	    	for(i=1; i<=dynaInstrArray.length; i++){
	    		instrTable = dojo.byId("NotifUsersTable"+i);
	    		if(instrTable.rows.length < defaultTable.rows.length){
	    			rowsToCreate = (defaultTable.rows.length) - (instrTable.rows.length);
	    			
		    		emailIdCount = (i-1) * 24;
		    		
	    			for(k=1; k<=rowsToCreate; k++){
	    				local.add2MoreUserEmails(i, emailIdCount);
	    			}
	    		}
	    		var defUserIds='';
	    		for(j=0; j<24; j++){
	    			if(dijit.byId("emailId"+(instrEmailIdCounter)) === undefined){
	    				//dijit.byId("emailId"+(instrEmailIdCounter)).setValue('');
	    			}else{
	    				dijit.byId("emailId"+(instrEmailIdCounter)).setValue(dijit.byId("defaultEmail"+(j)).getValue());
						defUserIds = defUserIds + dijit.byId("defaultEmail"+(j)).getValue();
	    				defUserIds = defUserIds + ',';	    				
	    			}
	    			instrEmailIdCounter++;
	    		}
				dojo.byId('defaultNotificationUserIds').value = defUserIds;
	    	}
	    	
	    	local.updateAllCriterias(defaultTable, dynaInstrArray.length, dynaInstrTransArray);
	    };//end applyToAllGroups()

	    //Update hidden variables for all instr-trans Criteria's one-by-one
	    //First read all Default settings and then update each
	    local.updateAllCriterias = function(defaultTable, noOfInstr, dynaInstrTransArray) {
	    	var transArray = [];
	    	var notifSettingId;
	    	var emailSetingId;
	    	var addlEmailAdderId;
	    	var emailId;
	    	var userNames = '';
	    	var defaultNotifSetting;
	    	var defaultEmailSetting;
	    	var defaultAdditionalEmailAddr;
	    	var defaultEmailIdsArray = new Array(24);
	    	var defaultEmailNamesArray = new Array(24);
	    	var criteriaStartCount = 0;
			var userEmailStartCount;
	    	
	    	if (registry.byId('defaultNotifySettingAlways').checked){
	    		defaultNotifSetting = registry.byId('defaultNotifySettingAlways').value;
	    	}else if (registry.byId('defaultNotifySettingNone').checked){
	    		defaultNotifSetting = registry.byId('defaultNotifySettingNone').value;
	    	}else if (registry.byId('defaultNotifySettingChrgsDocsOnly').checked){
	    		defaultNotifSetting = registry.byId('defaultNotifySettingChrgsDocsOnly').value;
	    	}else{
				defaultNotifSetting = "";
	    	}
	    	
	    	if (registry.byId('defaultEmailSettingAlways').checked){
	    		defaultEmailSetting = registry.byId('defaultEmailSettingAlways').value;
	    	}else if (registry.byId('defaultEmailSettingNone').checked){
	    		defaultEmailSetting = registry.byId('defaultEmailSettingNone').value;
	    	}else if (registry.byId('defaultEmailSettingChrgsDocsOnly').checked){
	    		defaultEmailSetting = registry.byId('defaultEmailSettingChrgsDocsOnly').value;
	    	}else{
				defaultEmailSetting = "";
	    	}
	    	
	    	defaultAdditionalEmailAddr = registry.byId('defaultAdditionalEmailAddr').value;
	    	
	    	for(e=1; e<=(defaultTable.rows.length)*2; e++){
    			defaultEmailIdsArray[e-1] = dijit.byId("defaultEmail"+(e-1)).getValue();
    			userNames = userNames + dijit.byId("defaultEmail"+(e-1)).getDisplayedValue();
    			
    			if(e!=(defaultTable.rows.length)*2){
    				userNames = userNames + ',';
    			}

    		}
	    	
	    	for(i=0; i<noOfInstr; i++){
	    		transArray = dynaInstrTransArray[i];
	    		userEmailStartCount = criteriaStartCount * 24;
	    		for(t=0; t<transArray.length; t++){
	    			notifSettingId = "send_notif_setting"+criteriaStartCount;
	    			emailSettingId = "send_email_setting"+criteriaStartCount;
	    			addlEmailAdderId = "additional_email_addr"+criteriaStartCount;
	    			emailId = "email_id";
	    			
	    			if(dojo.byId(notifSettingId)){
	    				dojo.byId(notifSettingId).value = defaultNotifSetting;
	    			}
	    			if(dojo.byId(emailSettingId)){
	    				dojo.byId(emailSettingId).value = defaultEmailSetting;
	    			}
	    			if(dojo.byId(addlEmailAdderId)){
	    				dojo.byId(addlEmailAdderId).value = defaultAdditionalEmailAddr;
	    			}
	    			
	    			var userIds = '';
	    			for(e=0; e<defaultEmailIdsArray.length; e++){
	    				if(defaultEmailIdsArray[e] === undefined){
	    					defaultEmailIdsArray[e] = 0;
	    				}
	    				
	    				dojo.byId(emailId+userEmailStartCount).value = defaultEmailIdsArray[e];
	    				userIds = userIds + defaultEmailIdsArray[e];
	    				
	    				if(e != defaultEmailIdsArray.length-1){
	    					userIds = userIds + ',';
	    				}
	    				userEmailStartCount++;
	    			}
	    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
	    			
	    			criteriaStartCount++;
	    		}
	    		dojo.byId('instNotificationUserIds'+(i+1)).value = dojo.byId('defaultNotificationUserIds').value;
	    		
	    		/*userNames = '';
	    		for(e=0; e<defaultEmailNamesArray.length; e++){
	    			userNames =  userNames + defaultEmailIdsArray[e];
	    			if(e != defaultEmailNamesArray.length-1){
	    				userNames = userNames + ',';
    				}
	    		}*/
	    		dojo.byId('instNotificationUserNames'+(i+1)).value = userNames;
	    	}
	    };//end updateAllCriterias()
	    
	    local.checkAllRadioGroups = function(radioButtonId, noOfInstr) {
	    	for(i=1; i<=noOfInstr; i++){
	    		if(registry.byId(radioButtonId+i)){
					registry.byId(radioButtonId+i).setChecked(true);
				}
	    	}
	    };//end of checkAllRadioGroups()
	    
	    local.unCheckAllRadioGroups = function(radioButtonIds, noOfInstr) {
	    	for(i=1; i<=noOfInstr; i++){
	    		for(var j = 0; j < radioButtonIds.length; j++) {
	    			if(registry.byId(radioButtonIds[j]+i)){
	    				registry.byId(radioButtonIds[j]+i).setChecked(false);
	    			}
	    		}
    		}
	    };//end of unCheckAllRadioGroups()
	    
	    local.applyToAllTransactions = function(sectionNum, criteriaStartCount, instrTransArrayStr, instrType, reqType) {
	    	var transArray = instrTransArrayStr.split(',');
	    	var emailTable = dojo.byId("NotifUsersTable"+sectionNum);
			var notifSetting;
			var emailSetting;
			var additionalEmailAddr;
			var emailIdsArray = new Array(24);
			var transNotifSetting;
			var transEmailSetting;
			var transAdditionalEmailAddr;
			var transEmails;
			//var criteriaStartCount = criteriaStartCounter - 1;
			var userEmailStartCount = criteriaStartCount * 24;
			var userEmailIdx;
			var notifyEmailFrequency;
	    	var transNotifEmailFrequency;
	    	var userIds = '';
	    	var userNames = '';
	    	

			
			if(reqType == 'ALL'){
				document.getElementById("applyInst"+sectionNum).value='Y';
				if (registry.byId('notifySettingAlways'+sectionNum).checked){					
		    		notifSetting = registry.byId('notifySettingAlways'+sectionNum).value;
		    	}else if (registry.byId('notifySettingNone'+sectionNum).checked){				
		    		notifSetting = registry.byId('notifySettingNone'+sectionNum).value;
		    	}else if (registry.byId('notifySettingChrgsDocsOnly'+sectionNum).checked){				
		    		notifSetting = registry.byId('notifySettingChrgsDocsOnly'+sectionNum).value;
		    	}else{
					notifSetting="";
				//	document.getElementById("applyInst"+sectionNum).value='';
				}
		    	
		    	if (registry.byId('emailSettingAlways'+sectionNum).checked){				
		    		emailSetting = registry.byId('emailSettingAlways'+sectionNum).value;
		    	}else if (registry.byId('emailSettingNone'+sectionNum).checked){				
		    		emailSetting = registry.byId('emailSettingNone'+sectionNum).value;
		    	}else if (registry.byId('emailSettingChrgsDocsOnly'+sectionNum).checked){				
		    		emailSetting = registry.byId('emailSettingChrgsDocsOnly'+sectionNum).value;
		    	}else{
					emailSetting="";					
				}
		    	
		    	additionalEmailAddr = registry.byId('additionalEmailAddr'+sectionNum).value;
		    	userEmailIdx = (sectionNum-1) * 24;
		    	for(e=1; e<=(emailTable.rows.length)*2; e++){
	    			emailIdsArray[e-1] = dijit.byId("emailId"+userEmailIdx).getValue();
	    			userNames = userNames + dijit.byId("emailId"+userEmailIdx).getDisplayedValue();
	    			
	    			if(e!=(emailTable.rows.length)*2){
	    				userNames = userNames + ',';
	    			}
	    			userEmailIdx++;
	    		}
		    	
	    		for(t=0; t<transArray.length; t++){
	    			transNotifSetting = "send_notif_setting"+criteriaStartCount;
	    			transEmailSetting = "send_email_setting"+criteriaStartCount;
	    			transAdditionalEmailAddr = "additional_email_addr"+criteriaStartCount;
	    			transEmails = "email_id";

	    			dojo.byId(transNotifSetting).value = notifSetting;
	    			dojo.byId(transEmailSetting).value = emailSetting;
	    			dojo.byId(transAdditionalEmailAddr).value = additionalEmailAddr;
	    			
	    			userIds = '';
	    			for(e=0; e<emailIdsArray.length; e++){
	    				if(emailIdsArray[e] === undefined){
	    					emailIdsArray[e] = 0;
	    				}
	    				
	    				dojo.byId(transEmails+userEmailStartCount).value = emailIdsArray[e];
	    				
	    				userIds = userIds + emailIdsArray[e];
	    				if(e != emailIdsArray.length-1){
	    					userIds = userIds + ',';
	    				}
	    				userEmailStartCount++;
	    			}
	    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
	    			
	    			criteriaStartCount++;
	    		}
	    		dojo.byId('instNotificationUserIds'+(sectionNum)).value = userIds;
	    		dojo.byId('instNotificationUserNames'+(sectionNum)).value = userNames;
			}//end of if ALL
			if(reqType == 'MESSAGES'){
				document.getElementById("applyInst"+sectionNum).value='Y';
				if (registry.byId('emailSettingAlways'+sectionNum).checked){
		    		emailSetting = registry.byId('emailSettingAlways'+sectionNum).value;
		    	}else if (registry.byId('emailSettingNone'+sectionNum).checked){
		    		emailSetting = registry.byId('emailSettingNone'+sectionNum).value;
		    	}else{
					emailSetting="";
				}
		    	
		    	additionalEmailAddr = registry.byId('additionalEmailAddr'+sectionNum).value;
		    	
		    	userEmailIdx = (sectionNum-1) * 24;
		    	for(e=1; e<=(emailTable.rows.length)*2; e++){
	    			emailIdsArray[e-1] = dijit.byId("emailId"+userEmailIdx).getValue();
	    			userNames = userNames + dijit.byId("emailId"+userEmailIdx).getDisplayedValue();
	    			
	    			if(e!=(emailTable.rows.length)*2){
	    				userNames = userNames + ',';
	    			}
	    			userEmailIdx++;
	    		}
		    	
	    		for(t=0; t<transArray.length; t++){
	    			transEmailSetting = "send_email_setting"+criteriaStartCount;
	    			transAdditionalEmailAddr = "additional_email_addr"+criteriaStartCount;
	    			transEmails = "email_id";
	    			
	    			dojo.byId(transEmailSetting).value = emailSetting;
	    			dojo.byId(transAdditionalEmailAddr).value = additionalEmailAddr;
	    			
	    			userIds = '';
	    			for(e=0; e<emailIdsArray.length; e++){
	    				if(emailIdsArray[e] === undefined){
	    					emailIdsArray[e] = 0;
	    				}
	    				
	    				dojo.byId(transEmails+userEmailStartCount).value = emailIdsArray[e];
	    				
	    				userIds = userIds + emailIdsArray[e];
	    				if(e != emailIdsArray.length-1){
	    					userIds = userIds + ',';
	    				}
	    				userEmailStartCount++;
	    			}
	    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
	    			criteriaStartCount++;
	    		}
	    		dojo.byId('instNotificationUserIds'+(sectionNum)).value = userIds;
	    		dojo.byId('instNotificationUserNames'+(sectionNum)).value = userNames;
			}//end of if MESSAGES
			if(reqType == 'OTHERS'){
				document.getElementById("applyInst"+sectionNum).value='Y';
				if (registry.byId('emailSettingYes'+sectionNum).checked){
		    		emailSetting = registry.byId('emailSettingYes'+sectionNum).value;
		    	}else if (registry.byId('emailSettingNo'+sectionNum).checked){
		    		emailSetting = registry.byId('emailSettingNo'+sectionNum).value;
		    	}else{
					emailSetting="";
				}
		    	
		    	additionalEmailAddr = registry.byId('additionalEmailAddr'+sectionNum).value;
		    	notifyEmailFrequency = registry.byId('notifyEmailInterval'+sectionNum).value;
		    	
		    	userEmailIdx = (sectionNum-1) * 24;
		    	for(e=1; e<=(emailTable.rows.length)*2; e++){
	    			emailIdsArray[e-1] = dijit.byId("emailId"+userEmailIdx).getValue();
	    			userNames = userNames + dijit.byId("emailId"+userEmailIdx).getDisplayedValue();
	    			
	    			if(e!=(emailTable.rows.length)*2){
	    				userNames = userNames + ',';
	    			}
	    			userEmailIdx++;
	    		}
		    	
	    		for(t=0; t<transArray.length; t++){
	    			transEmailSetting = "send_email_setting"+criteriaStartCount;
	    			transAdditionalEmailAddr = "additional_email_addr"+criteriaStartCount;
	    			transNotifEmailFrequency = "notify_email_freq"+criteriaStartCount;
	    			transEmails = "email_id";
	    			
	    			dojo.byId(transEmailSetting).value = emailSetting;
	    			dojo.byId(transAdditionalEmailAddr).value = additionalEmailAddr;
	    			dojo.byId(transNotifEmailFrequency).value = notifyEmailFrequency;
	    			
	    			userIds = '';
	    			for(e=0; e<emailIdsArray.length; e++){
	    				if(emailIdsArray[e] === undefined){
	    					emailIdsArray[e] = 0;
	    				}
	    				
	    				dojo.byId(transEmails+userEmailStartCount).value = emailIdsArray[e];
	    				
	    				userIds = userIds + emailIdsArray[e];
	    				if(e != emailIdsArray.length-1){
	    					userIds = userIds + ',';
	    				}
	    				userEmailStartCount++;
	    			}
	    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
	    			criteriaStartCount++;
	    		}
	    		dojo.byId('instNotificationUserIds'+(sectionNum)).value = userIds;
	    		dojo.byId('instNotificationUserNames'+(sectionNum)).value = userNames;
			}//end of if 'OTHERS'
	    };//end applyToAllTransactions()
	    
	    local.clearAllDefaultSection = function() {
	    	if (!confirm(" 'Clear All' will Remove All Selected Users and Entered Email Addresses. Do you want to continue?")){
	    		return false;
	    	}else{
	    		registry.byId('defaultNotifySettingAlways').setChecked(false);
	   			registry.byId('defaultNotifySettingNone').setChecked(false);
	   			registry.byId('defaultNotifySettingChrgsDocsOnly').setChecked(false);
	   			registry.byId('defaultEmailSettingAlways').setChecked(false);
	   			registry.byId('defaultEmailSettingNone').setChecked(false);
	   			registry.byId('defaultEmailSettingChrgsDocsOnly').setChecked(false);
	   			registry.byId('defaultAdditionalEmailAddr').setValue('');
   			
	   			var myTable = dojo.byId('NotifUsersTableDefault');
	   			for(j=1; j<=(myTable.rows.length)*2; j++){
	   				dijit.byId("defaultEmail"+(j-1)).setValue('');
	   			}
       		
	   			document.getElementById("defaultClearToAllGroup").value="Y";
           }
	    };//end clearAllDefaultSection()
	    
	    local.clearInstrCategorySection = function(sectionNum, criteriaStartCount, instrTransArrayStr, instrType, reqType) {
	    	if (!confirm(" 'Clear All' will Remove All Selected Users and Entered Email Addresses. Do you want to continue?")){
	    		return false;
	    	}else{
	    		var myTable = dojo.byId("NotifUsersTable"+sectionNum);
		    	var transArray = instrTransArrayStr.split(',');
				var transNotifSetting;
				var transEmailSetting;
				var transAdditionalEmailAddr;
				var transNotifEmailFrequency;
				var transEmails;
				var userEmailStartCount = criteriaStartCount * 24;
				var userEmailIdx;
				var userIds;
				
				document.getElementById("clearAll"+sectionNum).value="Y";	
				
				if(reqType == 'ALL'){
					registry.byId('notifySettingAlways'+sectionNum).setChecked(false);
					registry.byId('notifySettingNone'+sectionNum).setChecked(false);
					registry.byId('notifySettingChrgsDocsOnly'+sectionNum).setChecked(false);
					registry.byId('emailSettingAlways'+sectionNum).setChecked(false);
					registry.byId('emailSettingNone'+sectionNum).setChecked(false);
					registry.byId('emailSettingChrgsDocsOnly'+sectionNum).setChecked(false);
					registry.byId("additionalEmailAddr"+sectionNum).setValue('');
					
					userEmailIdx = (sectionNum-1) * 24;
			    	for(e=1; e<=(myTable.rows.length)*2; e++){
			    		dijit.byId("emailId"+userEmailIdx).setValue('');
			    		userEmailIdx++;
		    			//emailIdsArray[e-1] = dijit.byId(instrType+"_EMAIL"+(e-1)).getValue();
		    		}
			    	
		    		for(t=0; t<transArray.length; t++){
		    			transNotifSetting = "send_notif_setting"+criteriaStartCount;
		    			transEmailSetting = "send_email_setting"+criteriaStartCount;
		    			transAdditionalEmailAddr = "additional_email_addr"+criteriaStartCount;
		    			transEmails = "email_id";
		    			
		    			dojo.byId(transNotifSetting).value = '';
		    			dojo.byId(transEmailSetting).value = '';
		    			dojo.byId(transAdditionalEmailAddr).value = '';
		    			
		    			for(e=0; e<24; e++){
		    				dojo.byId(transEmails+userEmailStartCount).value = '';
		    				userEmailStartCount++;
		    				
		    				userIds = '';
		    				userIds = userIds + '0';
		    				if(e != 23){
		    					userIds = userIds + ',';
		    				}
		    			}
		    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
						dojo.byId("clear_tran_ind"+criteriaStartCount).value = "Y";
		    			criteriaStartCount++;
		    		}
				}//end of if ALL
				if(reqType == 'MESSAGES'){
					registry.byId('emailSettingAlways'+sectionNum).setChecked(false);
					registry.byId('emailSettingNone'+sectionNum).setChecked(false);
					registry.byId("additionalEmailAddr"+sectionNum).setValue('');
			    	
					userEmailIdx = (sectionNum-1) * 24;
			    	for(e=1; e<=(myTable.rows.length)*2; e++){
			    		dijit.byId("emailId"+userEmailIdx).setValue('');
			    		userEmailIdx++;
		    		}
			    	
			    	for(t=0; t<transArray.length; t++){
		    			transEmailSetting = "send_email_setting"+criteriaStartCount;
		    			transAdditionalEmailAddr = "additional_email_addr"+criteriaStartCount;
		    			transEmails = "email_id";
		    			
		    			dojo.byId(transEmailSetting).value = '';
		    			dojo.byId(transAdditionalEmailAddr).value = '';
		    			
		    			for(e=0; e<24; e++){
		    				dojo.byId(transEmails+userEmailStartCount).value = '';
		    				userEmailStartCount++;
		    				
		    				userIds = '';
		    				userIds = userIds + '0';
		    				if(e != 23){
		    					userIds = userIds + ',';
		    				}
		    			}
		    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
						dojo.byId("clear_tran_ind"+criteriaStartCount).value = "Y";
		    			criteriaStartCount++;
		    		}
				}//end of if MESSAGES
				if(reqType == 'OTHERS'){
			    	registry.byId('emailSettingYes'+sectionNum).setChecked(false);
					registry.byId('emailSettingNo'+sectionNum).setChecked(false);
					registry.byId("additionalEmailAddr"+sectionNum).setValue('');
					registry.byId("notifyEmailInterval"+sectionNum).setValue('');
			    	
					userEmailIdx = (sectionNum-1) * 24;
			    	for(e=1; e<=(myTable.rows.length)*2; e++){
			    		dijit.byId("emailId"+userEmailIdx).setValue('');
			    		userEmailIdx++;
		    		}
			    	
			    	for(t=0; t<transArray.length; t++){
		    			transEmailSetting = "send_email_setting"+criteriaStartCount;
		    			transAdditionalEmailAddr = "additional_email_addr"+criteriaStartCount;
		    			transNotifEmailFrequency = "notify_email_freq"+criteriaStartCount;
		    			transEmails = "email_id";
		    			
		    			dojo.byId(transEmailSetting).value = '';
		    			dojo.byId(transAdditionalEmailAddr).value = '';
		    			dojo.byId(transNotifEmailFrequency).value = '';
		    			
		    			for(e=0; e<24; e++){
		    				dojo.byId(transEmails+userEmailStartCount).value = '';
		    				userEmailStartCount++;
		    				
		    				userIds = '';
		    				userIds = userIds + '0';
		    				if(e != 23){
		    					userIds = userIds + ',';
		    				}
		    			}
		    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
						dojo.byId("clear_tran_ind"+criteriaStartCount).value = "Y";
		    			criteriaStartCount++;
		    		}
				}//end of if OTHERS
	    	}
	    };//end clearInstrCategorySection()
	    
	    local.add2MoreUserEmailsDefault = function() {
	    	var isReadOnly = dom.byId("isReadOnly").value;// If require use isReadOnly while creating widgets
	        var tbl = document.getElementById('NotifUsersTableDefault');// To identify the table in which the row will get insert
	    	var tblLength = tbl.rows.length;// Table length
	    	var emailIdCount = (tblLength)*2;//Current total email count

	    	for(i=tblLength;i<tblLength+1;i++){
	    		if(tblLength < 12){//Only add new row if the tblLenth less than 12. 
		       	 	var newRow = tbl.insertRow(i);//Creation of new row
		       	 	newRow.id = 'NotifUsersTableDefault_'+i;//Provide id for newly created row
		       	 	for(j=0;j<2;j++){//Creates two cells in a row and attaches two FilteringSelects to those
		    			var cell=newRow.insertCell(j);//Creates new cell in a row
		    			cell.innerHTML='<select data-dojo-type=\"dijit.form.FilteringSelect\" class=\"char9\" name=\"defaultEmail'+emailIdCount+'\" id=\"defaultEmail'+emailIdCount+'\"></select>';
		    			emailIdCount++;
		    		}
		       	 	parser.parse(newRow.id);//Parse the new row
		       	 	var firstEmailUserSelect = registry.byId("defaultEmail0");
		       	 	if(firstEmailUserSelect){//If the widget is available then only get the store from it.
		       	 		var userStore = firstEmailUserSelect.store;//Get first email user store
			       	 	for(j=0;j<2;j++){//Set store for newly created widgets
			       	 		emailIdCount--;
				       	 	registry.byId('defaultEmail'+emailIdCount).set("store",userStore);
							registry.byId('defaultEmail'+emailIdCount).attr('displayedValue', "");
	    				}
		       	 		
		       	 	}
		       	 	
	       	 	}else{//Disable the button
		       	 	var addButton = registry.byId("add2MoreUsersDefault");
		            if ( addButton ) {
		            	addButton.set('disabled',true);
		            }
	       	 	}
	    	}
	    };
	    
	    local.add2MoreUserEmails = function(sectionNum, emailIdCount) {
	    	var isReadOnly = dom.byId("isReadOnly").value;// If require use isReadOnly while creating widgets
	        var tbl = document.getElementById('NotifUsersTable'+sectionNum);// To identify the table in which the row will get insert
	    	var tblLength = tbl.rows.length;// Table length
	    	emailIdCount = emailIdCount + (tblLength)*2;//Current total email count

	    	for(i=tblLength;i<tblLength+1;i++){
	    		if(tblLength < 12){//Only add new row if the tblLenth less than 12. 
		       	 	var newRow = tbl.insertRow(i);//Creation of new row
		       	 	newRow.id = 'NotifUsersTable'+sectionNum+"_"+i;//Provide id for newly created row
		       	 	for(j=0;j<2;j++){//Creates two cells in a row and attaches two FilteringSelects to those
		    			var cell=newRow.insertCell(j);//Creates new cell in a row
		    			cell.innerHTML='<select data-dojo-type=\"dijit.form.FilteringSelect\" class=\"char9\" name=\"emailId'+emailIdCount+'\" id=\"emailId'+emailIdCount+'\"></select>';
		    			emailIdCount++;
		    		}
		       	 	parser.parse(newRow.id);//Parse the new row
		       	 	var firstEmailUserSelect = registry.byId("emailId0");
		       	 	if(firstEmailUserSelect){//If the widget is available then only get the store from it.
		       	 		var userStore = firstEmailUserSelect.store;//Get first email user store
			       	 	for(j=0;j<2;j++){//Set store for newly created widgets
			       	 		emailIdCount--;
				       	 	registry.byId('emailId'+emailIdCount).set("store",userStore);
							registry.byId('emailId'+emailIdCount).attr('displayedValue', "");
	    				}
		       	 		
		       	 	}
		       	 	
	       	 	}else{//Disable the button
		       	 	var addButton = registry.byId("add2MoreUsers"+sectionNum);
		            if ( addButton ) {
		            	addButton.set('disabled',true);
		            }
	       	 	}
	    	}
	        
	    };
	    
	    local.ocerrideTransactionsSection = function(sectionNum, criteriaStartCounter, instrTransArrayStr, instrType, reqFrom, sectionDialogTitle) {
	    	secNum = sectionNum;
	    	transCriteriaStartCount = criteriaStartCounter;
	    	transArrayStr = instrTransArrayStr;
	    	instrumentType = instrType;
	    	uiType = reqFrom;
	    	
    		dialog.open('notificationRuleDetailDialogId', sectionDialogTitle, 'NotificationRuleInstrTransDialog.jsp',
    	                ['sectionNum', 'criteriaStartCount', 'transArrayStr', 'instrType', 'uiType'], [sectionNum, transCriteriaStartCount, transArrayStr, instrumentType, uiType], //parameters
    	                'select', this.ocerrideTransactionsCallback, null, this.customTransactionsCallback);
	    };//end ocerrideTransactionsSection()
	    
	    local.customTransactionsCallback = function(){
	    	var instrType = instrumentType;
	    	var transArray = transArrayStr.split(',');
	    	var criteriaStartCount = transCriteriaStartCount
			var userEmailStartCount = criteriaStartCount * 24;
			var notifySetting;
			var emailSetting;
			var transUserTable;
			var instrUsersTable = dojo.byId('NotifUsersTable'+secNum);
			var rowsToCreate;
			
			if(uiType == 'ALL'){
				
				for(i=0; i<transArray.length; i++){
					notifySetting = dojo.byId("send_notif_setting"+criteriaStartCount).value;
					if(notifySetting == 'A'){
						dijit.byId('transNotifySettingAlways_'+instrType+'_'+transArray[i]).setChecked(true);
					}else if(notifySetting == 'N'){
						dijit.byId('transNotifySettingNone_'+instrType+'_'+transArray[i]).setChecked(true);
					}else if(notifySetting == 'C'){
						dijit.byId('transNotifySettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).setChecked(true);
					}
					
					emailSetting = dojo.byId("send_email_setting"+criteriaStartCount).value;
					if(emailSetting == 'A'){
						dijit.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).setChecked(true);
					}else if(emailSetting == 'N'){
						dijit.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).setChecked(true);
					}else if(emailSetting == 'C'){
						dijit.byId('transEmailSettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).setChecked(true);
					}
					
					dijit.byId('transAdditionalEmailAddr_'+instrType+'_'+transArray[i]).setValue(dojo.byId("additional_email_addr"+criteriaStartCount).value);
					criteriaStartCount++; 
				}
				criteriaStartCount= criteriaStartCount-transArray.length;
				baseArray.forEach(transArray, function(transType){
					transUserTable = dojo.byId('NotifUsersTableDialog_'+instrType+'_'+transType);
					var notificationUserIdsArray= dojo.byId("notificationUserIds"+criteriaStartCount).value.split(',');
					notificationUserIdsArray = baseArray.filter(notificationUserIdsArray, function(item){//Removes empty elements from array
							if(item.trim().length !== 0 && item.trim()!=0){
								return item;
							}
					    });
					
					var totalRows = Math.round(notificationUserIdsArray.length/2);
					if(transUserTable.rows.length < totalRows){
		    			rowsToCreate = totalRows - (transUserTable.rows.length);
		    			for(k=1; k<=rowsToCreate; k++){
		    				local.add2MoreDialogUserEmails(instrType, transType);
		    			}
		    		}
					criteriaStartCount++; 
			    });
				
				for(i=0; i<transArray.length; i++){
					for(j = 0; j < 24; j++){
		  				var userIdValue = dojo.byId('email_id'+userEmailStartCount).value;
		  				if(registry.byId('trans'+instrType+transArray[i]+'_EMAIL'+j)){
	  						registry.byId('trans'+instrType+transArray[i]+'_EMAIL'+j).set('value',userIdValue);
	  					}
		  				userEmailStartCount++;
	  				}
				}
			}//end of if ALL
			if(uiType == 'MESSAGES'){
				for(i=0; i<transArray.length; i++){
					emailSetting = dojo.byId("send_email_setting"+criteriaStartCount).value;
					if(emailSetting == 'A'){
						dijit.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).setChecked(true);
					}else if(emailSetting == 'N'){
						dijit.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).setChecked(true);
					}
					dijit.byId('transAdditionalEmailAddr_'+instrType+'_'+transArray[i]).setValue(dojo.byId("additional_email_addr"+criteriaStartCount).value);
					criteriaStartCount++; 
				}
				criteriaStartCount= criteriaStartCount-transArray.length;
				baseArray.forEach(transArray, function(transType){
					transUserTable = dojo.byId('NotifUsersTableDialog_'+instrType+'_'+transType);
					var notificationUserIdsArray= dojo.byId("notificationUserIds"+criteriaStartCount).value.split(',');
					notificationUserIdsArray = baseArray.filter(notificationUserIdsArray, function(item){//Removes empty elements from array
							if(item.trim().length !== 0 && item.trim()!=0){
								return item;
							}
					    });
					
					var totalRows = Math.round(notificationUserIdsArray.length/2);
					if(transUserTable.rows.length < totalRows){
		    			rowsToCreate = totalRows - (transUserTable.rows.length);
		    			for(k=1; k<=rowsToCreate; k++){
		    				local.add2MoreDialogUserEmails(instrType, transType);
		    			}
		    		}
					criteriaStartCount++; 
			    });
				
				for(i=0; i<transArray.length; i++){
					for(j = 0; j < 24; j++){
		  				var userIdValue = dojo.byId('email_id'+userEmailStartCount).value;
		  				if(registry.byId('trans'+instrType+transArray[i]+'_EMAIL'+j)){
	  						registry.byId('trans'+instrType+transArray[i]+'_EMAIL'+j).set('value',userIdValue);
	  					}
		  				userEmailStartCount++;
	  				}
				}
			}//end of if MESSAGES
			if(uiType == 'OTHERS'){
				for(i=0; i<transArray.length; i++){
					emailSetting = dojo.byId("send_email_setting"+criteriaStartCount).value;
					if(emailSetting == 'Y'){
						dijit.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).setChecked(true);
					}else if(emailSetting == 'N'){
						dijit.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).setChecked(true);
					}
					
					dijit.byId('transNotifyEmailInterval_'+instrType+'_'+transArray[i]).setValue(dojo.byId("notify_email_freq"+criteriaStartCount).value);
					dijit.byId('transAdditionalEmailAddr_'+instrType+'_'+transArray[i]).setValue(dojo.byId("additional_email_addr"+criteriaStartCount).value);
					criteriaStartCount++; 
				}
				
				criteriaStartCount= criteriaStartCount-transArray.length;
				baseArray.forEach(transArray, function(transType){
					transUserTable = dojo.byId('NotifUsersTableDialog_'+instrType+'_'+transType);
					var notificationUserIdsArray= dojo.byId("notificationUserIds"+criteriaStartCount).value.split(',');
					notificationUserIdsArray = baseArray.filter(notificationUserIdsArray, function(item){//Removes empty elements from array
							if(item.trim().length !== 0 && item.trim()!=0){
								return item;
							}
					    });
					
					var totalRows = Math.round(notificationUserIdsArray.length/2);
					if(transUserTable.rows.length < totalRows){
		    			rowsToCreate = totalRows - (transUserTable.rows.length);
		    			for(k=1; k<=rowsToCreate; k++){
		    				local.add2MoreDialogUserEmails(instrType, transType);
		    			}
		    		}
					criteriaStartCount++; 
			    });
				
				for(i=0; i<transArray.length; i++){
					for(j = 0; j < 24; j++){
		  				var userIdValue = dojo.byId('email_id'+userEmailStartCount).value;
		  				if(registry.byId('trans'+instrType+transArray[i]+'_EMAIL'+j)){
	  						registry.byId('trans'+instrType+transArray[i]+'_EMAIL'+j).set('value',userIdValue);
	  					}
		  				userEmailStartCount++;
	  				}
				}
			}//end of if OTHERS
		};
		
		local.checkSPValidation = function(sectionCounter) {
		//console.log("sectionCounter:"+sectionCounter);
		document.getElementById("applyInst"+sectionCounter).value="";
    };

		local.add2MoreDialogUserEmails = function(instrType, transType) {
			var isReadOnly = dom.byId("isReadOnly").value;// If require use isReadOnly while creating widgets
	        var tbl = document.getElementById('NotifUsersTableDialog_'+instrType+'_'+transType);// To identify the table in which the row will get insert
	    	var tblLength = tbl.rows.length;// Table length
	    	var emailIdCount = (tblLength)*2;//Current total email count

	    	for(i=tblLength;i<tblLength+1;i++){
	    		if(tblLength < 12){//Only add new row if the tblLenth less than 12. 
		       	 	var newRow = tbl.insertRow(i);//Creation of new row
		       	 	newRow.id = 'NotifUsersTableDialog_'+instrType+'_'+transType+"_"+i;//Provide id for newly created row 
		       	 	for(j=0;j<2;j++){//Creates two cells in a row and attaches two FilteringSelects to those
		    			var cell=newRow.insertCell(j);//Creates new cell in a row
		    			cell.innerHTML='<select data-dojo-type=\"dijit.form.FilteringSelect\" class=\"char9\" name=\"trans'+instrType+transType+"_EMAIL"+emailIdCount+'\" id=\"trans'+instrType+transType+"_EMAIL"+emailIdCount+'\"></select>';
		    			emailIdCount++;
		    		}
		       	 	parser.parse(newRow.id);//Parse the new row
		       	 	var firstEmailUserSelect = registry.byId('trans'+instrType+transType+'_EMAIL0');
		       	 	if(firstEmailUserSelect){//If the widget is available then only get the store from it.
		       	 		var userStore = firstEmailUserSelect.store;//Get first email user store
			       	 	for(j=0;j<2;j++){//Set store for newly created widgets
			       	 		emailIdCount--;
				       	 	registry.byId('trans'+instrType+transType+'_EMAIL'+emailIdCount).set("store",userStore);
							registry.byId('trans'+instrType+transType+'_EMAIL'+emailIdCount).attr('displayedValue', "");
	    				}
		       	 		
		       	 	}
		       	 	
	       	 	}else{//Disable the button
		       	 	var addButton = registry.byId('add2MoreTransUsers_'+instrType+'_'+transType);
		            if ( addButton ) {
		            	addButton.set('disabled',true);
		            }
	       	 	}
	    	}
	    };//end add2MoreDialogUserEmails()
	    
	    local.ocerrideTransactionsCallback = function() {
    		//alert("in callback");
	    };//end ocerrideTransactionsCallback()
	    
	    local.updateUserRecipientsDefault = function(dynaInstrTypesStr, dynaInstrTransTypesStr) {
		    	var dynaInstrArray = new Array();
		    	var dynaInstrTransArray = new Array();
		    	var instrTransArray = new Array();
		    	var tempTransStr;
		    	var userNames = '';
		    	
		    	dynaInstrArray = dynaInstrTypesStr.split(',');
		    	if(dynaInstrArray.indexOf('LRQ_INV') > -1){
		    		dynaInstrArray.splice(dynaInstrArray.indexOf('LRQ_INV'), 1);
		    	}
		    	if(dynaInstrArray.indexOf('H2H_INV_CRN') > -1){
		    		dynaInstrArray.splice(dynaInstrArray.indexOf('H2H_INV_CRN'), 1);
		    	}
		    	 
		    	instrTransArray = dynaInstrTransTypesStr.split('||');
		    	
		    	for(var i = 0; i < instrTransArray.length; i++){
		    		tempTransStr = instrTransArray[i];
		    		dynaInstrTransArray[i] = tempTransStr.split(',');
		    	}
		    	
    	    	var defaultTable = dojo.byId('NotifUsersTableDefault');
		    	var instrTable;
		    	var rowsToCreate;
		    	var instrEmailIdCounter = 0;
		    	var emailIdCount;
		    	document.getElementById("defaultApplyToAllGroup").value="Y";
		    	document.getElementById("defaultUpdateRecipientsOnly").value="Y";
		    	for(i=1; i<=dynaInstrArray.length; i++){
		    		var defUserIds='';
		    		for(j=0; j<24; j++){
		    			if(dijit.byId("emailId"+(instrEmailIdCounter)) === undefined){
		    				//dijit.byId("emailId"+(instrEmailIdCounter)).setValue('');
		    			}else{
		    				dijit.byId("emailId"+(instrEmailIdCounter)).setValue(dijit.byId("defaultEmail"+(j)).getValue());
							defUserIds = defUserIds + dijit.byId("defaultEmail"+(j)).getValue();
		    				defUserIds = defUserIds + ',';	    				
		    			}
		    			instrEmailIdCounter++;
		    		}
					dojo.byId('defaultNotificationUserIds').value = defUserIds;
		    	}
		   	local.updateUsersOnlyAllCriterias(defaultTable, dynaInstrArray.length, dynaInstrTransArray);
				  
		    };//end updateUserEmailsDefault()
		    
		  //Update hidden variables for all instr-trans Criteria's one-by-one
		    //First read all Default settings and then update each
		    local.updateUsersOnlyAllCriterias = function(defaultTable, noOfInstr, dynaInstrTransArray) {
		    	var transArray = [];
		    	var emailId = "email_id";
		    	var userNames = '';
		    	var defaultEmailIdsArray = new Array(24);
		    	var defaultEmailNamesArray = new Array(24);
		    	var criteriaStartCount = 0;
				var userEmailStartCount;
		    	var updateValues = false;
		    	
		    	for(e=1; e<=(defaultTable.rows.length)*2; e++){
	    			defaultEmailIdsArray[e-1] = dijit.byId("defaultEmail"+(e-1)).getValue();
	    			userNames = userNames + dijit.byId("defaultEmail"+(e-1)).getDisplayedValue();
	    			
	    			if(e!=(defaultTable.rows.length)*2){
	    				userNames = userNames + ',';
	    			}
	    		}
		    	
		    	for(i=0; i<noOfInstr; i++){
		    		document.getElementById("updateRecipientsOnlyInst"+(i+1)).value="Y";
		    		transArray = dynaInstrTransArray[i];
		    		userEmailStartCount = criteriaStartCount * 24;
		    		for(t=0; t<transArray.length; t++){
		    			var emailSettingId = "send_email_setting"+criteriaStartCount;
		    			emailId = "email_id";
		    			var userIds = '';
		    			
		    			if(dojo.byId(emailSettingId)){
		    				if(dojo.byId(emailSettingId).value == 'A' || dojo.byId(emailSettingId).value == 'C' )
		    					updateValues = true;
		    			}
		    			
		    			if(updateValues){
			    			var y=0;
			    			for(f=userEmailStartCount; f<(userEmailStartCount+24); f++){
			    				y++;
			    				if(dojo.byId(emailId+f).value == "null" || dojo.byId(emailId+f).value == "0" || dojo.byId(emailId+f).value == ""){
			    					break;
			    				}else{
			    				userIds = userIds + dojo.byId(emailId+f).value+',';	
			    				}
			    				
			    			}
			    			var x= defaultEmailIdsArray.length-y;
			    			for(e=0; e<x; e++){
			    				if(defaultEmailIdsArray[e] === undefined){
			    					defaultEmailIdsArray[e] = 0;
			    				}
			    				dojo.byId(emailId+f).value = defaultEmailIdsArray[e];
			    				userIds = userIds + defaultEmailIdsArray[e];
			    				
			    				if(e != defaultEmailIdsArray.length-1){
			    					userIds = userIds + ',';
			    				}
			    				f++;
			    			}
	    				}
		    			userEmailStartCount = userEmailStartCount+24;
		    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
		    			criteriaStartCount++;
		    		}
		    		dojo.byId('instNotificationUserIds'+(i+1)).value = dojo.byId('defaultNotificationUserIds').value;
		    		dojo.byId('instNotificationUserNames'+(i+1)).value = userNames;
		    	}
		    };//end updateAllCriterias()
		    
		    local.updateAddnEmailsDefault = function(dynaInstrTypesStr, dynaInstrTransTypesStr) {
		    	var dynaInstrArray = new Array();
		    	var dynaInstrTransArray = new Array();
		    	var instrTransArray = new Array();
		    	var tempTransStr;
		    	var userNames = '';
		    	
		    	dynaInstrArray = dynaInstrTypesStr.split(',');
		    	if(dynaInstrArray.indexOf('LRQ_INV') > -1){
		    		dynaInstrArray.splice(dynaInstrArray.indexOf('LRQ_INV'), 1);
		    	}
		    	if(dynaInstrArray.indexOf('H2H_INV_CRN') > -1){
		    		dynaInstrArray.splice(dynaInstrArray.indexOf('H2H_INV_CRN'), 1);
		    	}
		    	 
		    	instrTransArray = dynaInstrTransTypesStr.split('||');
		    	
		    	for(var i = 0; i < instrTransArray.length; i++){
		    		tempTransStr = instrTransArray[i];
		    		dynaInstrTransArray[i] = tempTransStr.split(',');
		    	}
		    	
		    	document.getElementById("defaultApplyToAllGroup").value="Y";
	    		document.getElementById("defaultUpdateEmailsOnly").value="Y";
	    		var defAddlEmailAddress = registry.byId('defaultAdditionalEmailAddr').getValue();
		    	for(i=1; i<=dynaInstrArray.length; i++){
	    			registry.byId("additionalEmailAddr"+i).setValue(defAddlEmailAddress);
					//document.getElementById("applyInst"+i).value="Y";
	    		}
			   	local.updateAddnEmailOnlyAllCriterias(dynaInstrArray.length, dynaInstrTransArray);
				   
		    };//end updateAddnEmailsDefault()
		    
		  //Update hidden variables for all instr-trans Criteria's one-by-one
		    //First read all Default settings and then update each
		    local.updateAddnEmailOnlyAllCriterias = function(noOfInstr, dynaInstrTransArray) {
		    	var transArray = [];
		    	var criteriaStartCount = 0;
				var userEmailStartCount;
				var defaultAdditionalEmailAddr;
		    	var updateValues = false;
				defaultAdditionalEmailAddr = registry.byId('defaultAdditionalEmailAddr').value;
		    	
				for(i=0; i<noOfInstr; i++){
					document.getElementById("updateEmailsOnlyInst"+(i+1)).value="Y";
		    		transArray = dynaInstrTransArray[i];
		    		userEmailStartCount = criteriaStartCount * 24;
		    		for(t=0; t<transArray.length; t++){
		    			var addlEmailAdderId = "additional_email_addr"+criteriaStartCount;
		    			var emailSettingId = "send_email_setting"+criteriaStartCount;
		    			
		    			if(dojo.byId(emailSettingId)){
		    				if(dojo.byId(emailSettingId).value == 'A' || dojo.byId(emailSettingId).value == 'C' )
		    					updateValues = true;
		    				else
		    					updateValues = false;
		    			}
		    			
		    			if(updateValues){
		    				if(dojo.byId(addlEmailAdderId).value == ""){
	    						dojo.byId(addlEmailAdderId).value = defaultAdditionalEmailAddr;
	    					}else{
	    						dojo.byId(addlEmailAdderId).value = dojo.byId(addlEmailAdderId).value+","+defaultAdditionalEmailAddr;
	    					}
		    			}
		    			
		    			criteriaStartCount++;
		    		}
		    	}
		    };//end updateAllCriterias()
		    
		    local.updateUsersOnly = function(sectionNum, criteriaStartCount, instrTransArrayStr, instrType, reqType) {
		    	var transArray = instrTransArrayStr.split(',');
		    	var emailTable = dojo.byId("NotifUsersTable"+sectionNum);
				var emailSetting;
				var emailIdsArray = new Array(24);
				var transEmailSetting;
				var transEmails;
				var userEmailStartCount = criteriaStartCount * 24;
				var userEmailIdx;
				var userIds = '';
		    	var userNames = '';
		    	var updateValues = false;
		    					
				if(reqType == 'ALL'){
					document.getElementById("updateRecipientsOnlyInst"+sectionNum).value='Y';
					userEmailIdx = (sectionNum-1) * 24;
			    	for(e=1; e<=(emailTable.rows.length)*2; e++){
		    			emailIdsArray[e-1] = dijit.byId("emailId"+userEmailIdx).getValue();
		    			userNames = userNames + dijit.byId("emailId"+userEmailIdx).getDisplayedValue();
		    			
		    			if(e!=(emailTable.rows.length)*2){
		    				userNames = userNames + ',';
		    			}
		    			userEmailIdx++;
		    		}
			    	
		    		for(t=0; t<transArray.length; t++){
		    			transEmailSetting = "send_email_setting"+criteriaStartCount;
		    			transEmails = "email_id";
		    			
		    			if(dojo.byId(transEmailSetting)){
		    				if(dojo.byId(transEmailSetting).value == 'A' || dojo.byId(transEmailSetting).value == 'C' )
		    					updateValues = true;
		    				else
		    					updateValues = false;
		    			}
		    			
		    			userIds = '';
		    			if(updateValues){
			    			var y=0;
			    			for(f=userEmailStartCount; f<(userEmailStartCount+24); f++){
			    				if(dojo.byId(transEmails+f).value == "null" || dojo.byId(transEmails+f).value == "0" || dojo.byId(transEmails+f).value == ""){
			    					break;
			    				}else{
			    				userIds = userIds + dojo.byId(transEmails+f).value+',';	
			    				}
			    				y++;
			    			}
			    			var x= emailIdsArray.length-y;
			    			for(e=0; e<x; e++){
			    				if(emailIdsArray[e] === undefined){
			    					emailIdsArray[e] = 0;
			    				}
			    				dojo.byId(transEmails+f).value = emailIdsArray[e];
			    				userIds = userIds + emailIdsArray[e];
			    				
			    				if(e != emailIdsArray.length-1){
			    					userIds = userIds + ',';
			    				}
			    				f++;
			    			}
	    				}
		    			userEmailStartCount = userEmailStartCount+24;
		    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
		    			criteriaStartCount++;
		    		}
		    		dojo.byId('instNotificationUserIds'+(sectionNum)).value = userIds;
		    		dojo.byId('instNotificationUserNames'+(sectionNum)).value = userNames;
				}//end of if ALL
				if(reqType == 'MESSAGES'){
					document.getElementById("updateRecipientsOnlyInst"+sectionNum).value='Y';
					userEmailIdx = (sectionNum-1) * 24;
					for(e=1; e<=(emailTable.rows.length)*2; e++){
		    			emailIdsArray[e-1] = dijit.byId("emailId"+userEmailIdx).getValue();
		    			userNames = userNames + dijit.byId("emailId"+userEmailIdx).getDisplayedValue();
		    			
		    			if(e!=(emailTable.rows.length)*2){
		    				userNames = userNames + ',';
		    			}
		    			userEmailIdx++;
		    		}
			    	for(t=0; t<transArray.length; t++){
		    			transEmailSetting = "send_email_setting"+criteriaStartCount;
		    			transEmails = "email_id";
		    			
		    			if(dojo.byId(transEmailSetting)){
		    				if(dojo.byId(transEmailSetting).value == 'A' )
		    					updateValues = true;
		    				else
		    					updateValues = false;
		    			}
		    			
		    			userIds = '';
		    			if(updateValues){
		    				var y=0;
			    			for(f=userEmailStartCount; f<(userEmailStartCount+24); f++){
			    				if(dojo.byId(transEmails+f).value == "null" || dojo.byId(transEmails+f).value == "0" || dojo.byId(transEmails+f).value == ""){
			    					break;
			    				}else{
			    				userIds = userIds + dojo.byId(transEmails+f).value+',';	
			    				}
			    				y++;
			    			}
			    			var x= emailIdsArray.length-y;
			    			for(e=0; e<x; e++){
			    				if(emailIdsArray[e] === undefined){
			    					emailIdsArray[e] = 0;
			    				}
			    				dojo.byId(transEmails+f).value = emailIdsArray[e];
			    				userIds = userIds + emailIdsArray[e];
			    				
			    				if(e != emailIdsArray.length-1){
			    					userIds = userIds + ',';
			    				}
			    				f++;
			    			}
	    				}
		    			userEmailStartCount = userEmailStartCount+24;
		    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
		    			criteriaStartCount++;
		    		}
		    		dojo.byId('instNotificationUserIds'+(sectionNum)).value = userIds;
		    		dojo.byId('instNotificationUserNames'+(sectionNum)).value = userNames;
				}//end of if MESSAGES
				if(reqType == 'OTHERS'){
					document.getElementById("updateRecipientsOnlyInst"+sectionNum).value='Y';
					userEmailIdx = (sectionNum-1) * 24;
			    	for(e=1; e<=(emailTable.rows.length)*2; e++){
		    			emailIdsArray[e-1] = dijit.byId("emailId"+userEmailIdx).getValue();
		    			userNames = userNames + dijit.byId("emailId"+userEmailIdx).getDisplayedValue();
		    			
		    			if(e!=(emailTable.rows.length)*2){
		    				userNames = userNames + ',';
		    			}
		    			userEmailIdx++;
		    		}
			    	
		    		for(t=0; t<transArray.length; t++){
		    			transEmailSetting = "send_email_setting"+criteriaStartCount;
		    			transEmails = "email_id";
		    			
		    			if(dojo.byId(transEmailSetting)){
		    				if(dojo.byId(transEmailSetting).value == 'Y' )
		    					updateValues = true;
		    				else
		    					updateValues = false;
		    			}
		    			
		    			userIds = '';
		    			if(updateValues){
			    			var y=0;
			    			for(f=userEmailStartCount; f<(userEmailStartCount+24); f++){
			    				if(dojo.byId(transEmails+f).value == "null" || dojo.byId(transEmails+f).value == "0" || dojo.byId(transEmails+f).value == ""){
			    					break;
			    				}else{
			    				userIds = userIds + dojo.byId(transEmails+f).value+',';	
			    				}
			    				y++;
			    			}
			    			var x= emailIdsArray.length-y;
			    			for(e=0; e<x; e++){
			    				if(emailIdsArray[e] === undefined){
			    					emailIdsArray[e] = 0;
			    				}
			    				dojo.byId(transEmails+f).value = emailIdsArray[e];
			    				userIds = userIds + emailIdsArray[e];
			    				
			    				if(e != emailIdsArray.length-1){
			    					userIds = userIds + ',';
			    				}
			    				f++;
			    			}
	    				}
		    			userEmailStartCount = userEmailStartCount+24;
		    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
		    			criteriaStartCount++;
		    		}
		    		dojo.byId('instNotificationUserIds'+(sectionNum)).value = userIds;
		    		dojo.byId('instNotificationUserNames'+(sectionNum)).value = userNames;
				}//end of if 'OTHERS'
		    };//end applyToAllTransactions()
		    
		    local.updateEmailsOnly = function(sectionNum, criteriaStartCount, instrTransArrayStr, instrType, reqType) {
		    	var transArray = instrTransArrayStr.split(',');
		    	var emailTable = dojo.byId("NotifUsersTable"+sectionNum);
				var emailSetting;
				var additionalEmailAddr;
				var transEmailSetting;
				var transAdditionalEmailAddr;
				var updateValues = false;
								
				if(reqType == 'ALL'){
					document.getElementById("updateEmailsOnlyInst"+sectionNum).value='Y';
					additionalEmailAddr = registry.byId('additionalEmailAddr'+sectionNum).value;
			    	for(t=0; t<transArray.length; t++){
		    			transEmailSetting = "send_email_setting"+criteriaStartCount;
		    			transAdditionalEmailAddr = "additional_email_addr"+criteriaStartCount;
		    			
		    			if(dojo.byId(transEmailSetting)){
		    				if(dojo.byId(transEmailSetting).value == 'A' || dojo.byId(transEmailSetting).value == 'C' )
		    					updateValues = true;
		    				else
		    					updateValues = false;
		    			}
		    			
		    			if(updateValues){
		    				if(dojo.byId(transAdditionalEmailAddr).value == ""){
	    						dojo.byId(transAdditionalEmailAddr).value = additionalEmailAddr;
	    					}else{
	    						dojo.byId(transAdditionalEmailAddr).value = dojo.byId(transAdditionalEmailAddr).value+","+additionalEmailAddr;
	    					}
		    			}
		    			criteriaStartCount++;
		    		}
		    	}//end of if ALL
				if(reqType == 'MESSAGES'){
					document.getElementById("updateEmailsOnlyInst"+sectionNum).value='Y';
					additionalEmailAddr = registry.byId('additionalEmailAddr'+sectionNum).value;
			    	
			    	for(t=0; t<transArray.length; t++){
			    		transEmailSetting = "send_email_setting"+criteriaStartCount;
		    			transAdditionalEmailAddr = "additional_email_addr"+criteriaStartCount;
		    			
		    			if(dojo.byId(transEmailSetting)){
		    				if(dojo.byId(transEmailSetting).value == 'A' )
		    					updateValues = true;
		    				else
		    					updateValues = false;
		    			}
		    			
		    			if(updateValues){
		    				if(dojo.byId(transAdditionalEmailAddr).value == ""){
	    						dojo.byId(transAdditionalEmailAddr).value = additionalEmailAddr;
	    					}else{
	    						dojo.byId(transAdditionalEmailAddr).value = dojo.byId(transAdditionalEmailAddr).value+","+additionalEmailAddr;
	    					}
		    			}
		    			criteriaStartCount++;
		    		}
		    	}//end of if MESSAGES
				if(reqType == 'OTHERS'){
					document.getElementById("updateEmailsOnlyInst"+sectionNum).value='Y';
					additionalEmailAddr = registry.byId('additionalEmailAddr'+sectionNum).value;
			    	for(t=0; t<transArray.length; t++){
			    		transEmailSetting = "send_email_setting"+criteriaStartCount;
		    			transAdditionalEmailAddr = "additional_email_addr"+criteriaStartCount;
		    			
		    			if(dojo.byId(transEmailSetting)){
		    				if(dojo.byId(transEmailSetting).value == 'Y' )
		    					updateValues = true;
		    				else
		    					updateValues = false;
		    			}
		    			
		    			if(updateValues){
		    				if(dojo.byId(transAdditionalEmailAddr)){
		    					if(dojo.byId(transAdditionalEmailAddr).value == ""){
		    						dojo.byId(transAdditionalEmailAddr).value = additionalEmailAddr;
		    					}else{
		    						dojo.byId(transAdditionalEmailAddr).value = dojo.byId(transAdditionalEmailAddr).value+","+additionalEmailAddr;
		    					}
			    			}
		    			}
		    			criteriaStartCount++;
		    		}
		    		
		    	}//end of if 'OTHERS'
		    };//end applyToAllTransactions()
		    
	    local.prepareUserIds = function(sectionNum, sectionCategory) {
	    	var emailTable;
	    	var emailIdsArray = new Array(24);
			var userIds = '';
	    	var userNames = '';
	    	var userEmailIdx;
	    	var emailID;
	    	
	    	if(sectionNum == 0){
	    		emailTable = dojo.byId('NotifUsersTableDefault');
	    		emailID = "defaultEmail";
	    		userEmailIdx = "0";
	    	}else{
	    		emailTable = dojo.byId("NotifUsersTable"+sectionNum);
	    		emailID = "emailId";
	    		userEmailIdx = (sectionNum-1) * 24;
	    	}
	    	
			
	    	for(e=1; e<=(emailTable.rows.length)*2; e++){
    			userIds = userIds + dijit.byId(emailID+userEmailIdx).getValue();
    			userNames = userNames + dijit.byId(emailID+userEmailIdx).getDisplayedValue();
    			
    			if(e!=(emailTable.rows.length)*2){
    				userNames = userNames + ',';
    				userIds = userIds +',';
    			}
    			userEmailIdx++;
    		}
	    	if(sectionNum == 0){
	    		dojo.byId('defaultNotificationUserIds').value = userIds;
	    	}else{
	    		dojo.byId('instNotificationUserIds'+(sectionNum)).value = userIds;
	    		dojo.byId('instNotificationUserNames'+(sectionNum)).value = userNames;
	    	}
	    	if(sectionCategory == 'LRQ_INV'){
	    		document.getElementById("applyInst"+sectionNum).value="";
	    	}
		};//end prepareUserIds()	
		
	});//End of AMD loading 'require
