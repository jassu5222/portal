
//library for all of the Notification Rule Template detail page functions
// this is 2nd choice becuase we'd prefer having eventhandlers and use dojo asynch to load
// todo: make all event handlers

//load everything once
	require(["dojo/_base/array", "dojo/_base/xhr", "dojo/dom", "dojo/dom-class", 
	         "dojo/dom-construct","dojo/dom-style","dojo/query", "dijit/registry",
	         "dojo/parser", "t360/common", "t360/dialog", "dojo/store/Memory","dojo/ready"], 
	         function(baseArray, xhr, dom,  domClass, domConstruct, domStyle, query, registry, parser, common, dialog, Memory, ready ) {
		var secNum;
		var uiType;
		var criteriaStartCounter;
		
		var instrArray = ['AIR', 'ATP', 'BIL', 'CBA', 'EXP_OCO', 'EXP_DBA', 'EXP_COL', 'EXP_DFP', 'EXP_DLC', 
		                  'LOI', 'EXP_TAC', 'IMP_DBA', 'IMC', 'IMP_DFP', 'IMP_DLC', 'IMP_TAC', 'INC_GUA', 'INC_SLC', 
		                  'FTRQ', 'LRQ', 'GUA', 'SLC', 'PYB', 'FTDP', 'RFN', 'REC', 'RBA', 
		                  'RQA', 'SHP', 'SUPPLIER_PORTAL', 'FTBA', 'MESSAGES', 'LRQ_INV', 'H2H_INV_CRN'];
		
		var AIR_TransArray = ["ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXP", "PAY", "REA", "REL"];
		  var ATP_TransArray = ["ARU", "ADJ", "AMD", "CHG", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA"];//1 EXT
		  var BIL_TransArray = ["ARU", "ADJ", "BCH", "BCA", "PCS", "PCE", "DEA", "BNW","REA", "RED", "RVV"];
		  var CBA_TransArray = ["ARU", "ADJ", "BUY", "CHG", "PCS", "PCE", "CUS", "LIQ"];//3 BUY PCS PCE
		  
		  var EXP_OCO_TransArray = ["ARU", "ADJ", "AMD", "CHG", "NAC", "ISS", "PAY", "REA"];
		  var EXP_DBA_TransArray = [ "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"];
		  var EXP_COL_TransArray = ["ARU", "ADJ", "AMD", "CHG", "NAC", "ISS", "PAY", "REA"];
		  var EXP_DFP_TransArray = [ "ARU", "ADJ", "BUY",  "CHG", "CUS", "LIQ"];
		  
		  var EXP_DLC_TransArray = ["ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"];
		  var LOI_TransArray = ["ARU", "ADJ", "AMD", "CHG", "CRE", "DEA", "EXP", "PAY", "REA"];//2 DEA EXT
		  var EXP_TAC_TransArray = [ "ARU", "ADJ",  "BUY",  "CHG", "CUS", "LIQ"];
		  var IMP_DBA_TransArray = [ "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"]; 
		  
		  var IMC_TransArray = ["ARU", "ADJ", "AMD", "CHG", "COL", "NAC", "PAY", "REA"];//DEA
		  var IMP_DFP_TransArray = ["ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"];
		  var IMP_DLC_TransArray = ["ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"];
		  var IMP_TAC_TransArray = [ "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"];
		  
		  var INC_GUA_TransArray = ["ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"];
		  var INC_SLC_TransArray = ["ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"];
		  var FTRQ_TransArray = ["ARU", "ADJ", "ISS"];
		  var LRQ_TransArray = ["ARU", "ADJ", "CHG", "PCS", "PCE", "ISS", "LIQ"];
		  
		  var GUA_TransArray = ["ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"];
		  var SLC_TransArray = ["ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"];
		  var PYB_TransArray = ["CHP", "APM", "PPY"];
		  var FTDP_TransArray = ["ARU", "ADJ", "ISS"];
		  
		  var RFN_TransArray = ["ARU", "ADJ", "CHG", "PCS", "PCE", "ISS", "LIQ"];
		  var REC_TransArray = ["PAR", "CHM", "ARM"];
		  var RBA_TransArray = ["ARU", "ADJ", "CHG", "PCS", "PCE", "CUS", "LIQ"];
		  var RQA_TransArray = ["ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV", "TRN", "PYT"];
		  
		  var SHP_TransArray = ["ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXP", "ISS", "PAY", "REA"];
		  var SUPPLIER_PORTAL_TransArray = ["ISS", "LIQ"];
		  var FTB_ATransArray = ["ARU", "ADJ", "ISS"];
		  var MESSAGES_TransArray = ["DISCR", "MAIL", "MATCH", "SETTLEMENT"];
		  
		  var LRQ_INV_TransArray = ["LRQ_INV"];
		  var HTOH_TransArray = ["PIN", "PCN", "RIN"];

		  var instrTransArrays = [
				  	 AIR_TransArray, ATP_TransArray, BIL_TransArray, 
				  	 CBA_TransArray, EXP_OCO_TransArray, EXP_DBA_TransArray, 
				  	 EXP_COL_TransArray, EXP_DFP_TransArray, EXP_DLC_TransArray, 
				  	 LOI_TransArray, EXP_TAC_TransArray, IMP_DBA_TransArray, 
		             IMC_TransArray, IMP_DFP_TransArray, IMP_DLC_TransArray, 
		             IMP_TAC_TransArray, INC_GUA_TransArray, INC_SLC_TransArray, 
		             FTRQ_TransArray, LRQ_TransArray, GUA_TransArray, 
		             SLC_TransArray, PYB_TransArray, FTDP_TransArray, 
		             RFN_TransArray, REC_TransArray, RBA_TransArray, 
		             RQA_TransArray, SHP_TransArray, SUPPLIER_PORTAL_TransArray, 
		             FTB_ATransArray, MESSAGES_TransArray, LRQ_INV_TransArray, 
		             HTOH_TransArray];
		
		//var instrTransCriteriaFieldStartCount = [0, 11, 21, 27, 38, 43, 49, 55, 63, 71, 90, 99, 107, 122, 141, 160, 163, 170, 185, 200, 203, 206, 213, 216, 223, 242, 253, 255, 258, 262, 263];
		  
		
	 local.setSupValue = function() {       
    	 document.getElementById("supplierReadOnly").value="N";      
    };

	    local.applyToAllGroups = function() {
	    	document.getElementById("defaultApplyToAllGroup").value="Y";
	    	
	    	if(registry.byId('defaultNotifySettingAlways').checked){
	    		local.checkAllRadioGroups('notifySettingAlways');
	    	}else if(registry.byId('defaultNotifySettingNone').checked){
	    		local.checkAllRadioGroups('notifySettingNone');
	    	}else if(registry.byId('defaultNotifySettingChrgsDocsOnly').checked){
	    		local.checkAllRadioGroups('notifySettingChrgsDocsOnly');
	    	}else{
	    		local.unCheckAllRadioGroups(['notifySettingAlways', 'notifySettingNone', 'notifySettingChrgsDocsOnly']);
	    	}
	    	
	    	if(registry.byId('defaultEmailSettingAlways').checked){
	    		local.checkAllRadioGroups('emailSettingAlways');
	    	}else if(registry.byId('defaultEmailSettingNone').checked){
	    		local.checkAllRadioGroups('emailSettingNone');
	    	}else if(registry.byId('defaultEmailSettingChrgsDocsOnly').checked){
	    		local.checkAllRadioGroups('emailSettingChrgsDocsOnly');
	    	}else{
	    		local.unCheckAllRadioGroups(['emailSettingAlways', 'emailSettingNone','emailSettingChrgsDocsOnly']);
	    	}
			for(i=1; i<=31; i++){
				document.getElementById("applyInst"+i).value="Y";
    		}
	    	local.updateAllCriterias();
	    };//end applyToAllGroups()

	    //Update hidden variables for all instr-trans Criteria's one-by-one
	    //First read all Default settings and then update each
	    local.updateAllCriterias = function() {
	    	var transArray = [];
	    	var notifSettingId;
	    	var emailSetingId;
	    	var emailId;
	    	var defaultNotifSetting;
	    	var defaultEmailSetting;
	    	var criteriaStartCount = 0;
			
	    	if (registry.byId('defaultNotifySettingAlways').checked){
	    		defaultNotifSetting = registry.byId('defaultNotifySettingAlways').value;
	    	}else if (registry.byId('defaultNotifySettingNone').checked){
	    		defaultNotifSetting = registry.byId('defaultNotifySettingNone').value;
	    	}else if (registry.byId('defaultNotifySettingChrgsDocsOnly').checked){
	    		defaultNotifSetting = registry.byId('defaultNotifySettingChrgsDocsOnly').value;
	    	}else{
				defaultNotifSetting="";
			}
	    	
	    	if (registry.byId('defaultEmailSettingAlways').checked){
	    		defaultEmailSetting = registry.byId('defaultEmailSettingAlways').value;
	    	}else if (registry.byId('defaultEmailSettingNone').checked){
	    		defaultEmailSetting = registry.byId('defaultEmailSettingNone').value;
	    	}else if (registry.byId('defaultEmailSettingChrgsDocsOnly').checked){
	    		defaultEmailSetting = registry.byId('defaultEmailSettingChrgsDocsOnly').value;
	    	}else{
				defaultEmailSetting = "";
			}
	    	for(i=0; i<31; i++){
	    		transArray = instrTransArrays[i];
	    		//criteriaStartCount = instrTransCriteriaFieldStartCount[i];
	    		for(t=0; t<transArray.length; t++){
	    			notifSettingId = "send_notif_setting"+criteriaStartCount;
	    			emailSettingId = "send_email_setting"+criteriaStartCount;
	    			if(dojo.byId(notifSettingId)){
	    				dojo.byId(notifSettingId).value = defaultNotifSetting;
	    			}
	    			if(dojo.byId(emailSettingId)){
	    				dojo.byId(emailSettingId).value = defaultEmailSetting;
	    			}
					criteriaStartCount++;
	    		}
	    	}
	    };//end updateAllCriterias()
	    
	    local.checkAllRadioGroups = function(radioButtonId) {
	    	for(i=1; i<=31; i++){
    			registry.byId(radioButtonId+i).setChecked(true);
    		}
	    };//end of checkAllRadioGroups()
	    
	    local.unCheckAllRadioGroups = function(radioButtonIds) {
	    	for(i=1; i<=31; i++){
	    		for(var j = 0; j < radioButtonIds.length; j++) {
	    			registry.byId(radioButtonIds[j]+i).setChecked(false);
	    		}
    		}
	    };//end of unCheckAllRadioGroups()
	    
	    local.applyToAllTransactions = function(sectionNum, criteriaStartCount) {
	    	var instrType = instrArray[sectionNum-1];
	    	var transArray = instrTransArrays[sectionNum-1];
	    	var emailTable = dojo.byId("NotifUsersTable"+sectionNum);
			var notifSetting;
			var emailSetting;
			var transNotifSetting;
			var transEmailSetting;
			//var criteriaStartCount = instrTransCriteriaFieldStartCount[sectionNum-1];
			
			document.getElementById("applyInst"+sectionNum).value='Y';
			
			if (registry.byId('notifySettingAlways'+sectionNum).checked){
	    		notifSetting = registry.byId('notifySettingAlways'+sectionNum).value;
	    	}else if (registry.byId('notifySettingNone'+sectionNum).checked){
	    		notifSetting = registry.byId('notifySettingNone'+sectionNum).value;
	    	}else if (registry.byId('notifySettingChrgsDocsOnly'+sectionNum).checked){
	    		notifSetting = registry.byId('notifySettingChrgsDocsOnly'+sectionNum).value;
	    	}else{
				notifSetting="";
			}
	    	
	    	if (registry.byId('emailSettingAlways'+sectionNum).checked){
	    		emailSetting = registry.byId('emailSettingAlways'+sectionNum).value;
	    	}else if (registry.byId('emailSettingNone'+sectionNum).checked){
	    		emailSetting = registry.byId('emailSettingNone'+sectionNum).value;
	    	}else if (registry.byId('emailSettingChrgsDocsOnly'+sectionNum).checked){
	    		emailSetting = registry.byId('emailSettingChrgsDocsOnly'+sectionNum).value;
	    	}else{
				emailSetting="";
			}

	    	for(t=0; t<transArray.length; t++){
    			transNotifSetting = "send_notif_setting"+criteriaStartCount;
    			transEmailSetting = "send_email_setting"+criteriaStartCount;
    			
    			dojo.byId(transNotifSetting).value = notifSetting;
    			dojo.byId(transEmailSetting).value = emailSetting;
    			
				criteriaStartCount++;
    		}
	    };//end applyToAllTransactions()
	    
	    local.applyToAllMessageTransactions = function(sectionNum, criteriaStartCount) {
	    	var emailSetting;
	    	var transEmailSetting;
	    	var transAdditionalEmailAddr;
	    	//var criteriaStartCount = instrTransCriteriaFieldStartCount[sectionNum-1];
	    	var transArray = instrTransArrays[sectionNum-1];
	    	
	    	document.getElementById("applyInst"+sectionNum).value='Y';
	    	if (registry.byId('emailSettingAlways'+sectionNum).checked){
	    		emailSetting = registry.byId('emailSettingAlways'+sectionNum).value;
	    	}else if (registry.byId('emailSettingNone'+sectionNum).checked){
	    		emailSetting = registry.byId('emailSettingNone'+sectionNum).value;
	    	}else{
				emailSetting="";
			}
	    	for(t=0; t<transArray.length; t++){
    			transEmailSetting = "send_email_setting"+criteriaStartCount;
    			dojo.byId(transEmailSetting).value = emailSetting;
    			criteriaStartCount++;
    		}
	    };//end applyToAllMessageTransactions()
	    
	    local.applyToAllOtherTransactions = function(sectionNum, criteriaStartCount) {
	    	var emailSetting;
	    	var notifyEmailFrequency;
	    	var transEmailSetting;
	    	var transNotifEmailFrequency;
	    	//var criteriaStartCount = instrTransCriteriaFieldStartCount[sectionNum-1];
	    	var transArray = instrTransArrays[sectionNum-1];
	    	
	    	document.getElementById("applyInst"+sectionNum).value='Y';
			document.getElementById("supplierReadOnly").value='Y';
	    	if (registry.byId('emailSettingYes'+sectionNum).checked){
	    		emailSetting = registry.byId('emailSettingYes'+sectionNum).value;
	    	}else if (registry.byId('emailSettingNo'+sectionNum).checked){
	    		emailSetting = registry.byId('emailSettingNo'+sectionNum).value;
	    	}else{
				emailSetting="";
			}
	    	
	    	notifyEmailFrequency = registry.byId('notifyEmailInterval'+sectionNum).value;
			console.log("notifyEmailFrequency:"+notifyEmailFrequency);
    		for(t=0; t<transArray.length; t++){
    			transEmailSetting = "send_email_setting"+criteriaStartCount;
    			transNotifEmailFrequency = "notify_email_freq"+criteriaStartCount;
    			//if(sectionNum !=31 || sectionNum !=30 || sectionNum !=32){
    			dojo.byId(transEmailSetting).value = emailSetting;
				//}
    			dojo.byId(transNotifEmailFrequency).value = notifyEmailFrequency;
    			criteriaStartCount++;
    		}
	    };//end applyToAllOtherTransactions()
	    
		local.clearAllDefaultSection = function() {
			registry.byId('defaultNotifySettingAlways').setChecked(false);
			registry.byId('defaultNotifySettingNone').setChecked(false);
			registry.byId('defaultNotifySettingChrgsDocsOnly').setChecked(false);
			registry.byId('defaultEmailSettingAlways').setChecked(false);
			registry.byId('defaultEmailSettingNone').setChecked(false);
			registry.byId('defaultEmailSettingChrgsDocsOnly').setChecked(false);
    		document.getElementById("defaultClearToAllGroup").value="Y";
	    };//end clearAllDefaultSection()
	    
	    local.clearInstrCategorySection = function(sectionNum, criteriaStartCount) {
	    	var instrType = instrArray[sectionNum-1];
	    	var transArray = instrTransArrays[sectionNum-1];
			var transNotifSetting;
			var transEmailSetting;
			//var criteriaStartCount = instrTransCriteriaFieldStartCount[sectionNum-1];
			
			document.getElementById("clearAll"+sectionNum).value="Y";	
			
			registry.byId('notifySettingAlways'+sectionNum).setChecked(false);
			registry.byId('notifySettingNone'+sectionNum).setChecked(false);
			registry.byId('notifySettingChrgsDocsOnly'+sectionNum).setChecked(false);
			registry.byId('emailSettingAlways'+sectionNum).setChecked(false);
			registry.byId('emailSettingNone'+sectionNum).setChecked(false);
			registry.byId('emailSettingChrgsDocsOnly'+sectionNum).setChecked(false);
    		for(t=0; t<transArray.length; t++){
    			transNotifSetting = "send_notif_setting"+criteriaStartCount;
    			transEmailSetting = "send_email_setting"+criteriaStartCount;
    			dojo.byId(transNotifSetting).value = '';
    			dojo.byId(transEmailSetting).value = '';
    			criteriaStartCount++;
    		}
	    };//end clearInstrCategorySection()
	    
	    local.clearAllMessagesSection = function(sectionNum, criteriaStartCount) {
	    	var transArray = instrTransArrays[sectionNum-1];
			var transEmailSetting;
			var transAdditionalEmailAddr;
			//var criteriaStartCount = instrTransCriteriaFieldStartCount[sectionNum-1];
	    	document.getElementById("clearAll"+sectionNum).value="Y";
	    	
	    	registry.byId('emailSettingAlways'+sectionNum).setChecked(false);
			registry.byId('emailSettingNone'+sectionNum).setChecked(false);
	    	for(t=0; t<transArray.length; t++){
    			transEmailSetting = "send_email_setting"+criteriaStartCount;
				dojo.byId(transEmailSetting).value = '';
    			criteriaStartCount++;
    		}
	    };//end clearAllMailMessagesSection()
	    
	    local.clearAllOtherSection = function(sectionNum, criteriaStartCount) {
	    	var transArray = instrTransArrays[sectionNum-1];
			var transEmailSetting;
			var transNotifEmailFrequency;
			//var criteriaStartCount = instrTransCriteriaFieldStartCount[sectionNum-1];
	    	
	    	document.getElementById("clearAll"+sectionNum).value="Y";
 			document.getElementById("supplierReadOnly").value='Y';
	    	registry.byId('emailSettingYes'+sectionNum).setChecked(false);
			registry.byId('emailSettingNo'+sectionNum).setChecked(false);
			registry.byId("notifyEmailInterval"+sectionNum).setValue('');
	    	for(t=0; t<transArray.length; t++){
    			transEmailSetting = "send_email_setting"+criteriaStartCount;
    			transNotifEmailFrequency = "notify_email_freq"+criteriaStartCount;
    			dojo.byId(transEmailSetting).value = '';
    			dojo.byId(transNotifEmailFrequency).value = '';
    			criteriaStartCount++;
    		}
	    };//end clearAllMailMessagesSection()
	    
	    local.overrideTransactionsSection = function(sectionNum, criteriaStartCount, sectionDialogTitle, uiTypeStr) {
	    	secNum = sectionNum;
			uiType = uiTypeStr;
	    	criteriaStartCounter = criteriaStartCount;
	    	//var criteriaStartCount = instrTransCriteriaFieldStartCount[sectionNum-1];
    		dialog.open('notificationRuleDetailDialogId', 
    					sectionDialogTitle,
    	                'NotificationRuleInstrTransTemplateDialog.jsp',
    	                ['sectionNum', 'criteriaStartCount', 'uiType'], [sectionNum, criteriaStartCount, uiTypeStr], //parameters
    	                'select', this.overrideTransactionsCallback, null, this.customTransactionsCallback);
	    };
	    /*
	    local.overrideMessagesTransactionsSection = function(sectionNum, criteriaStartCount, sectionDialogTitle) {
	    	secNum = sectionNum;
	    	criteriaStartCounter = criteriaStartCount;
	    	//var criteriaStartCount = instrTransCriteriaFieldStartCount[sectionNum-1];
    		dialog.open('notificationRuleDetailDialogId', 
    				sectionDialogTitle,
    	                'NotificationRuleInstrTransTemplateDialog.jsp',
    	                ['sectionNum', 'criteriaStartCount', 'uiType'], [sectionNum, criteriaStartCount, 'MESSAGES'], //parameters
    	                'select', this.overrideMsgTransactionsCallback, null, this.customTransactionsCallback);
	    };
	    
	    local.overrideOthersTransactionsSection = function(sectionNum, criteriaStartCount,sectionDialogTitle) {
	    	secNum = sectionNum;
	    	criteriaStartCounter = criteriaStartCount;	
	    	//var criteriaStartCount = instrTransCriteriaFieldStartCount[sectionNum-1];
    		dialog.open('notificationRuleDetailDialogId', 
    					sectionDialogTitle,
    	                'NotificationRuleInstrTransTemplateDialog.jsp',
    	                ['sectionNum', 'criteriaStartCount', 'uiType'], [sectionNum, criteriaStartCount, 'OTHERS'], //parameters
    	                'select', this.overrideOthersTransactionsCallback, null, this.customTransactionsCallback);
	    };
	    */
	    local.customTransactionsCallback = function(){		console.log("customTransactionsCallback().."+uiType);
	    	var instrType = instrArray[secNum-1];
	    	var transArray = instrTransArrays[secNum-1];
	    	//var criteriaStartCount = instrTransCriteriaFieldStartCount[secNum-1];
	    	var criteriaStartCount = criteriaStartCounter; 
			var notifySetting;
			var rowsToCreate;
			if(uiType == 'ALL'){				
				for(i=0; i<transArray.length; i++){
					notifySetting = dojo.byId("send_notif_setting"+criteriaStartCount).value;
					if(notifySetting == 'A'){
						dijit.byId('transNotifySettingAlways_'+instrType+'_'+transArray[i]).setChecked(true);
					}else if(notifySetting == 'N'){
						dijit.byId('transNotifySettingNone_'+instrType+'_'+transArray[i]).setChecked(true);
					}else if(notifySetting == 'C'){
						dijit.byId('transNotifySettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).setChecked(true);
					}
					
					emailSetting = dojo.byId("send_email_setting"+criteriaStartCount).value;
					if(emailSetting == 'A'){
						dijit.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).setChecked(true);
					}else if(emailSetting == 'N'){
						dijit.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).setChecked(true);
					}else if(emailSetting == 'C'){
						dijit.byId('transEmailSettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).setChecked(true);
					}
					
					criteriaStartCount++; 
				}
			}
			if(uiType == 'MESSAGES'){		
				for(i=0; i<transArray.length; i++){
				emailSetting = dojo.byId("send_email_setting"+criteriaStartCount).value;
				if(emailSetting == 'A'){
					dijit.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).setChecked(true);
				}else if(emailSetting == 'N'){
					dijit.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).setChecked(true);
				}else if(emailSetting == 'Y'){	
					registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).set("checked",true);
				}
				
				criteriaStartCount++; 
				}
			}
			if(uiType == 'OTHERS'){				
				for(i=0; i<transArray.length; i++){
				emailSetting = dojo.byId("send_email_setting"+criteriaStartCount).value;
				if(emailSetting == 'Y'){	
					registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).set("checked",true);
				}else if(emailSetting == 'N'){
					registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).set("checked",true);
				}else if(emailSetting == 'A'){
					registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).set("checked",true);
				}else if(emailSetting == 'C'){
					registry.byId('transEmailSettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).set("checked",true);
				}
			
				dijit.byId('transNotifyEmailInterval_'+instrType+'_'+transArray[i]).setValue(dojo.byId("notify_email_freq"+criteriaStartCount).value);
				criteriaStartCount++; 
			}
			}
		};
		
		local.customMessagesTransactionsCallback = function(){	
	    	var instrType = instrArray[secNum-1];
	    	var transArray = instrTransArrays[secNum-1];
	    	//var criteriaStartCount = instrTransCriteriaFieldStartCount[secNum-1];
	    	var criteriaStartCount = criteriaStartCounter;
			var notifySetting;
			var emailSetting;
			var rowsToCreate;
			for(i=0; i<transArray.length; i++){
				emailSetting = dojo.byId("send_email_setting"+criteriaStartCount).value;
				if(emailSetting == 'A'){
					dijit.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).setChecked(true);
				}else if(emailSetting == 'N'){
					dijit.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).setChecked(true);
				}else if(emailSetting == 'Y'){	
					registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).set("checked",true);
				}
				
			criteriaStartCount++; 
			}
		};
		
		local.customOthersTransactionsCallback = function(){
	    	var instrType = instrArray[secNum-1];
	    	var transArray = instrTransArrays[secNum-1];
	    	//var criteriaStartCount = instrTransCriteriaFieldStartCount[secNum-1];
	    	var criteriaStartCount = criteriaStartCounter;
			var notifySetting;
			var emailSetting;
			for(i=0; i<transArray.length; i++){
				emailSetting = dojo.byId("send_email_setting"+criteriaStartCount).value;
				if(emailSetting == 'Y'){	
					registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).set("checked",true);
				}else if(emailSetting == 'N'){
					registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).set("checked",true);
				}else if(emailSetting == 'A'){
					registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).set("checked",true);
				}else if(emailSetting == 'C'){
					registry.byId('transEmailSettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).set("checked",true);
				}
			
				dijit.byId('transNotifyEmailInterval_'+instrType+'_'+transArray[i]).setValue(dojo.byId("notify_email_freq"+criteriaStartCount).value);
				criteriaStartCount++; 
			}
		};
	    
		local.overrideTransactionsCallback = function() {
    	};
	    
		local.overrideMsgTransactionsCallback = function() {
    	};
		
		local.overrideOthersTransactionsCallback = function() {
    	};
		
	});//End of AMD loading 'require
	