/*Prateep Gedupudi
Writing page specific code here. This is not completely AMD specific as some code is deppendent on page.
TODO: Write AMD modules for this page after internationalisation in js side.*/
var panelauthgroup;
require(["dojo/data/ItemFileWriteStore",
         "t360/dialog",
         "dojo/data/ObjectStore",
         "dojo/store/Memory",
         "dojox/grid/DataGrid",
         "dojo/dom-construct",
         "dojo/query",
         "dijit/form/CheckBox",
         "dijit/registry",
         "dojo/dom",
         "dojo/dom-style",
         "dojo/_base/array",
         "dojo/ready",
         "dojo/_base/lang",
         "dojo/domReady!"],
function(itemfilewritestore,dialog,ObjectStore,Memory,DataGrid,domconstruct,query,checkbox,registry,dom,domstyle,array,ready,lang) {
	
	panelauthgroup={
    config:{},			
	openDialog:function(title,callinggrid,hiddenfieldsdiv,range){
		this.config.callinggrid=callinggrid;
		this.config.hiddenfieldsdiv=hiddenfieldsdiv;
		this.config.range=range;
		
		var editabledialog=registry.byId("editabledailog");
		editabledialog.set("title",title);
		editabledialog.show();
		
		var callingdatagrid=registry.byId(callinggrid);
		var editabledatagrid=registry.byId("editabledatagrid");
		var objStore = new ObjectStore({ objectStore: lang.clone(callingdatagrid.store.objectStore) });
		editabledatagrid.setStore(objStore);
		editabledatagrid.setStructure(editabledatagrid.get("structure"));
		
		//IR#T36000020701#Issue#4 Starts
		editabledialog.set('onCancel',function(){
			panelauthgroup.editValidate(editabledatagrid,editabledialog);
		});
		//IR#T36000020701#Issue#4 Ends
		
	},		 
    createDataGrid:function(layout,storedata,node){
    	var myStore = new Memory({data: storedata});
        var objStore = new ObjectStore({ objectStore: myStore });
    	var grid = new DataGrid({
	        store: objStore,
	        canSort:false,//IR#T36000020558
	        structure: layout
	        },
	        dom.byId(node));
    	
    	grid.set('autoHeight',5);
    	grid.startup();	
    	
    },
    createEditableDataGrid:function(layout,storedata,node){
    	var myStore = new Memory({data: storedata});
        var objStore = new ObjectStore({ objectStore: myStore });
    	var grid = new DataGrid({
	        store: objStore,
	        structure: layout,
	        canSort:false,//IR#T36000020558
	        doApplyCellEdit:function(inValue,inRowIndex,inAttrName){
	        	var item = grid.getItem(inRowIndex);
	        	grid.store.setValue(item, inAttrName, inValue);
	        	},
	        singleClickEdit:true
	        },
	        dom.byId(node));
    	
    	grid.set('autoHeight',10);
    	grid.startup();	
    },
    createhiddenfieldsfromstore:function(objectstore,hiddendiv,range){
    	var hiddendiv=dom.byId(hiddendiv);
    	if(hiddendiv.hasChildNodes()){
    		while (hiddendiv.hasChildNodes()) {
    		    hiddendiv.removeChild(hiddendiv.lastChild);
    		}
    	}
    	var list=0;
    	objectstore.query().forEach(function(rule){
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_rule_oid"+list , rule.oid ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_sequence"+list , rule.approver_sequence ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_one"+list , rule.approver_1 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_two"+list , rule.approver_2 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_three"+list , rule.approver_3 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_four"+list , rule.approver_4 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_five"+list , rule.approver_5 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_six"+list , rule.approver_6));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_seven"+list , rule.approver_7 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_eight"+list , rule.approver_8 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_nine"+list , rule.approver_9 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_ten"+list , rule.approver_10 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_eleven"+list , rule.approver_11));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_twelve"+list , rule.approver_12 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_thirteen"+list , rule.approver_13));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_fourteen"+list , rule.approver_14 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_fifteen"+list , rule.approver_15 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_sixteen"+list , rule.approver_16 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_seventeen"+list , rule.approver_17 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_eightteen"+list , rule.approver_18 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_nineteen"+list , rule.approver_19 ));
    		hiddendiv.appendChild(panelauthgroup.createhiddenvariable("panel_range"+range+"_approver_twenty"+list , rule.approver_20 ));
    		list++;
    	});
    }, 
    createhiddenvariable:function(name,value){
    	var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("id", name);
        hiddenField.setAttribute("name", name);
        hiddenField.setAttribute("value", value);
        return hiddenField;
    },
    deleteallClick:function(){
    		var grid=registry.byId("editabledatagrid");
    		grid.store.objectStore.query().forEach(function(rule){
    		grid.store.setValue(rule, "approver_sequence", "");
    		grid.store.setValue(rule, "approver_1", "");
    		grid.store.setValue(rule, "approver_2", "");
    		grid.store.setValue(rule, "approver_3", "");
    		grid.store.setValue(rule, "approver_4", "");
    		grid.store.setValue(rule, "approver_5", "");
    		grid.store.setValue(rule, "approver_6", "");
    		grid.store.setValue(rule, "approver_7", "");
    		grid.store.setValue(rule, "approver_8", "");
    		grid.store.setValue(rule, "approver_9", "");
    		grid.store.setValue(rule, "approver_10", "");
    		grid.store.setValue(rule, "approver_11", "");
    		grid.store.setValue(rule, "approver_12", "");
    		grid.store.setValue(rule, "approver_13", "");
    		grid.store.setValue(rule, "approver_14", "");
    		grid.store.setValue(rule, "approver_15", "");
    		grid.store.setValue(rule, "approver_16", "");
    		grid.store.setValue(rule, "approver_17", "");
    		grid.store.setValue(rule, "approver_18", "");
    		grid.store.setValue(rule, "approver_19", "");
    		grid.store.setValue(rule, "approver_20", "");
    		
    	});
    },
    saveandcloseClick:function(){
    	var callinggrid=registry.byId(this.config.callinggrid);
    	var editabledatagrid=registry.byId("editabledatagrid");
    	//IR#T36000020701#Issue#4 Starts
    	if (editabledatagrid.edit.isEditing()){
			if(editabledatagrid.edit._isValidInput()){
				editabledatagrid.edit.apply();
				callinggrid.setStore(editabledatagrid.store);
		    	panelauthgroup.createhiddenfieldsfromstore(editabledatagrid.store.objectStore,this.config.hiddenfieldsdiv,this.config.range);
    		}
			
		}else{
			callinggrid.setStore(editabledatagrid.store);
	    	panelauthgroup.createhiddenfieldsfromstore(editabledatagrid.store.objectStore,this.config.hiddenfieldsdiv,this.config.range);
		}
    	panelauthgroup.editValidate(editabledatagrid,registry.byId("editabledailog"));
    	//IR#T36000020701#Issue#4 End
    },
    cancelClick:function(){
    	var editabledialog=registry.byId("editabledailog");
    	var editabledatagrid=registry.byId("editabledatagrid");
    	panelauthgroup.editValidate(editabledatagrid,editabledialog);//IR#T36000020701#Issue#4
    },
    //IR#T36000020701#Issue#4 Starts
    editValidate:function(grid,dialog){
    	if(grid.edit.isEditing()){
    		if(grid.edit._isValidInput()){
    			dialog.hide();
    		}
    	}else{
    		dialog.hide();
    	}
    },//IR#T36000020701#Issue#4 Ends
    sequenceformatter:function(value, index){
    	 var item =  seqStore.get(value);
          return item.name; 
    },
    approvformatter:function(value, index){
    	var item =  panelStore.get(value);
        return item.name; 
    },
    getsequenceValue:function(){
    	return this.widget.get('value');
    },
    deleteformatter:function(inValue, index){
    	return "<img src='/portal/themes/"+brandingDirectory+"/images/delete_icon.png' alt='Delete' width='16' height='16' style='cursor: pointer;' onclick='panelauthgroup.deleterow("+index+");'/>";
    },
    deleterow:function(index){
    	var grid=registry.byId("editabledatagrid");
    	var item=grid.getItem(index);
    	
    	grid.store.setValue(item, "approver_sequence", "");
		grid.store.setValue(item, "approver_1", "");
		grid.store.setValue(item, "approver_2", "");
		grid.store.setValue(item, "approver_3", "");
		grid.store.setValue(item, "approver_4", "");
		grid.store.setValue(item, "approver_5", "");
		grid.store.setValue(item, "approver_6", "");
		grid.store.setValue(item, "approver_7", "");
		grid.store.setValue(item, "approver_8", "");
		grid.store.setValue(item, "approver_9", "");
		grid.store.setValue(item, "approver_10", "");
		grid.store.setValue(item, "approver_11", "");
		grid.store.setValue(item, "approver_12", "");
		grid.store.setValue(item, "approver_13", "");
		grid.store.setValue(item, "approver_14", "");
		grid.store.setValue(item, "approver_15", "");
		grid.store.setValue(item, "approver_16", "");
		grid.store.setValue(item, "approver_17", "");
		grid.store.setValue(item, "approver_18", "");
		grid.store.setValue(item, "approver_19", "");
		grid.store.setValue(item, "approver_20", "");
		grid.render();
    },
    totalauthformatter:function(formatters){
    	var noofformatters=0;
    	for(i=0;i<formatters.length;i++){
    		if(formatters[i]!="")
    			noofformatters++;
    	}
    	return noofformatters;
    },
   
	showdiv:function(divid){
		this.disableenableallwidgetsindiv(divid,false);
		domstyle.set(dom.byId(divid),"display","block");
		
	},
	hidediv:function(divid){
		this.disableenableallwidgetsindiv(divid,true);
		domstyle.set(dom.byId(divid),"display","none");
	},
	disableenableallwidgetsindiv:function(divid,action){
		var Widgets = registry.findWidgets(dom.byId(divid));
		this.disableenableprocess(Widgets,action);
		
	},
	disableenableprocess:function(widgets,action){
		 array.forEach(widgets,function(widget){
			 if(action)   
			 widget.set('checked',false);
			  });
	},
	findMatchingElements:function(match){
		return query(match);
	},
	setValuesforMatched:function(match,value){
		var elements=this.findMatchingElements(match);
		array.forEach(elements,function(element){
			 if(element)
				 element.value=value;
			  });
	},
	//Adding following method is only for IE7 selector issue with dynamically added hidden elements. Get more info from attachment of IR#T36000019990_Dev
	setEmtyValues:function(value){
		for(i=0;i<6;i++){
		 for(j=0;j<10;j++){//We have only 10 rules per range. -Pradeep
			if(document.getElementsByName('panel_range'+i+'_rule_oid'+j)[0])
			{
				document.getElementsByName('panel_range'+i+'_rule_oid'+j)[0].value=value;
			}
		 }
		}

	}
	
	}
	ready(function(){
	    
	  
	registry.byId("payments").on("change", function(isChecked){
		if(isChecked){
			//Hide receivables and trade sections
			panelauthgroup.hidediv("receivables_section");
			panelauthgroup.hidediv("trade_section");
			panelauthgroup.hidediv("payables_section");
			
			panelauthgroup.showdiv("payments_section");
			
			domstyle.set(dom.byId('panelgroupconfpaymentinstsection'),"display","none");//Sandeep - Rel 8.3 SysTest IR# T36000020742 09/16/2013
			
			if(registry.byId("payment").checked){
				panelauthgroup.showdiv("payment_type_section");
				panelauthgroup.showdiv("panelgroupconfpaymentinstsection");//Sandeep - Rel 8.3 SysTest IR# T36000020411 09/05/201
			}
			
			if(registry.byId("ccy_basis")){
				registry.byId("ccy_basis").query={id:/.*/};
			}
		}
	}, true);
	//Rel 9.0 CR 913 Start
	registry.byId("payables").on("change", function(isChecked){
		if(isChecked){
			//Hide payments, receivables and trade sections
			panelauthgroup.hidediv("receivables_section");
			panelauthgroup.hidediv("trade_section");
			panelauthgroup.hidediv("payments_section");
			panelauthgroup.hidediv("payment_type_section");
			panelauthgroup.hidediv("panelgroupconfpaymentinstsection");
			
			panelauthgroup.showdiv("payables_section");
			
			if(registry.byId("ccy_basis")){
				registry.byId("ccy_basis").query={id:'BASE_CCY'};
				//Currency Basis dropdown should only display Base Currency option when Instrument type Payables selected.
				registry.byId("ccy_basis").set('value', 'BASE_CCY');
			}
		}
	}, true);
	//Rel 9.0 CR 913 End
	
	registry.byId("int_payment").on("change", function(isChecked){
		if(isChecked){
		panelauthgroup.hidediv("payment_type_section");
		domconstruct.place(dom.byId("panelgroupconfpaymentinstsection"),dom.byId("intpaymentconfind"));
		
		panelauthgroup.showdiv("panelgroupconfpaymentinstsection");//Sandeep - Rel 8.3 SysTest IR# T36000020411 09/05/2013
		//dom.byId("intpaymentconfind").innerHTML=dom.byId("panelgroupconfpaymentinstsection");
		}
	}, true);
	
	registry.byId("payment").on("change", function(isChecked){
		if(isChecked){
		panelauthgroup.showdiv("payment_type_section");
		domconstruct.place(dom.byId("panelgroupconfpaymentinstsection"),dom.byId("paymentconfind"));
		
		panelauthgroup.showdiv("panelgroupconfpaymentinstsection");//Sandeep - Rel 8.3 SysTest IR# T36000020411 09/05/2013
		//dom.byId("paymentconfind").innerHTML=dom.byId("panelgroupconfpaymentinstsection");
		}
	}, true);


	registry.byId("receivables").on("change", function(isChecked){
		if(isChecked){
			//Hide payments and trade sections
			panelauthgroup.hidediv("payments_section");
			panelauthgroup.hidediv("payment_type_section");
			panelauthgroup.hidediv("trade_section");
			panelauthgroup.hidediv("panelgroupconfpaymentinstsection");
			panelauthgroup.hidediv("payables_section");
			
			panelauthgroup.showdiv("receivables_section");
			
			if(registry.byId("ccy_basis")){
				registry.byId("ccy_basis").query={id:'BASE_CCY'};
				// jgadela  09/03/2013 Rel 8.3 IR - 20413 [START]  - Currency Basis dropdown should only display Base Currency option when Instrument type Receivableis selected.
				registry.byId("ccy_basis").set('value', 'BASE_CCY');
				// jgadela  09/03/2013 Rel 8.3 IR - 20413 [END]  
			}
		}
	}, true);

	registry.byId("trade").on("change", function(isChecked){
		if(isChecked){
		//Hide payments and receivables sections
		panelauthgroup.hidediv("payments_section");
		panelauthgroup.hidediv("payment_type_section");
		panelauthgroup.hidediv("receivables_section");
		panelauthgroup.hidediv("panelgroupconfpaymentinstsection");
		panelauthgroup.hidediv("payables_section");
		
		panelauthgroup.showdiv("trade_section");
		
		if(registry.byId("ccy_basis")){
			registry.byId("ccy_basis").query={id:'BASE_CCY'};
			// jgadela  09/03/2013 Rel 8.3 IR - 20413 [START]  - Currency Basis dropdown should only display Base Currency option when Instrument type Receivableis selected.
			registry.byId("ccy_basis").set('value', 'BASE_CCY');
			// jgadela  09/03/2013 Rel 8.3 IR - 20413 [END]
		}
		
		}
	}, true);

	if(registry.byId("payments").checked){
		panelauthgroup.hidediv("receivables_section");
		panelauthgroup.hidediv("trade_section");
		panelauthgroup.hidediv("payables_section");
		
		panelauthgroup.showdiv("payments_section");
		
		if(registry.byId("payment").checked){
			panelauthgroup.showdiv("payment_type_section");
			panelauthgroup.showdiv("panelgroupconfpaymentinstsection");//Sandeep - Rel 8.3 SysTest IR# T36000020411 09/05/2013
		}
		
		if(registry.byId("ccy_basis")){
			registry.byId("ccy_basis").query={id:/.*/};
		}
	}
	//Rel9.0 CR 913 Start
	if(registry.byId("payables").checked){
		panelauthgroup.hidediv("payments_section");
		panelauthgroup.hidediv("payment_type_section");
		panelauthgroup.hidediv("trade_section");
		panelauthgroup.hidediv("panelgroupconfpaymentinstsection");
		panelauthgroup.hidediv("receivables_section");
		
		panelauthgroup.showdiv("payables_section");
		
		if(registry.byId("ccy_basis")){
			registry.byId("ccy_basis").query={id:'BASE_CCY'};
		}
	}
	//Rel9.0 CR 913 End
	if(registry.byId("receivables").checked){
		panelauthgroup.hidediv("payments_section");
		panelauthgroup.hidediv("payment_type_section");
		panelauthgroup.hidediv("trade_section");
		panelauthgroup.hidediv("panelgroupconfpaymentinstsection");
		panelauthgroup.hidediv("payables_section");
		
		panelauthgroup.showdiv("receivables_section");
		
		if(registry.byId("ccy_basis")){
			registry.byId("ccy_basis").query={id:'BASE_CCY'};
		}
	}
	if(registry.byId("trade").checked){
		panelauthgroup.hidediv("payments_section");
		panelauthgroup.hidediv("payment_type_section");
		panelauthgroup.hidediv("receivables_section");
		panelauthgroup.hidediv("panelgroupconfpaymentinstsection");
		panelauthgroup.showdiv("trade_section");
		panelauthgroup.hidediv("payables_section");
		
		if(registry.byId("ccy_basis")){
			registry.byId("ccy_basis").query={id:'BASE_CCY'};
		}
	}
	if(registry.byId("int_payment").checked){
		panelauthgroup.hidediv("payment_type_section");
		domconstruct.place(dom.byId("panelgroupconfpaymentinstsection"),dom.byId("intpaymentconfind"));
	}
	if(registry.byId("payment").checked){
		panelauthgroup.showdiv("payment_type_section");
		domconstruct.place(dom.byId("panelgroupconfpaymentinstsection"),dom.byId("paymentconfind"));
	}
	});	
});