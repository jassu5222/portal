dependencies = {
    stripConsole    : "normal",
    selectorEngine  : "acme",
    optimize        : "closure",
    layerOptimize   : "closure",
    cssOptimize     : "comments.keepLines",
    mini            : true,
    internStrings   : true,
    releaseName     : "t360-release",
    action          : "release",
    optimize        : "shrinksafe",
    layerOptimize   : "shrinksafe",
    packages: [ 
               
               { 
                 name: 'images', 
                 location: '../../**/images' 
               } 
         	] ,
    layers : [
        {
	            name         : "dojo.js",
	            dependencies : 
	            	[
						"dijit.Dialog",
						"dijit.Tooltip",
						"dijit.form.Button",
						"dijit.Tooltip",
						"dijit.PopupMenuBarItem",
						"dijit.MenuBarItem",
						"dijit.layout.ContentPane",
						"dijit.form.ValidationTextBox",
						"dijit.form.TextBox",
						"dijit.form.NumberTextBox",
						"dijit.form.Textarea",
						"dijit.form.SimpleTextarea",
						"dijit.form.DateTextBox",
						"dijit.form.CurrencyTextBox",
						"dijit.form.CheckBox",
						"dijit.form.RadioButton",
						"dijit.TitlePane",
						"dijit.form.Form",
						"dijit.form.NumberSpinner",
						"dijit.form.FilteringSelect",
						"dijit.MenuBar",
						"dojo.data.ItemFileReadStore",
						"dojo.data.ItemFileWriteStore",
						"dojo.store.Memory",
						"dojo.data.ObjectStore",
						"dijit.form.nls.ComboBox",
						"dijit.TooltipDialog"
					]
        },
		//Custom layer mydojo.js which
		// includes our custom Dojo artifacts
		{
		//place the file under dojoRootDir
			name: "dojox.js",
			layerDependencies:
				[
					"dojo.js"
				],
			dependencies:
			[
				"dojox.grid.LazyTreeGrid",
				"dojox.grid._CheckBoxSelector",
				"dojox.grid._RadioSelector",
				"dojox.data.QueryReadStore",
				"dojox.layout.FloatingPane",
				"dojox.layout.ResizeHandle",
				"dojox.layout.Dock"
			]
		},

		//This layer is used to discard modules
		//from the dijit package.

		//Custom layer mydojo.js which
		// includes our custom Dojo artifacts
		{
		//place the file under dojoRootDir
		name: "../t360/t360.js",
		layerDependencies:
			[
				"dojo.js",
				"dojox.js"
			],
		dependencies:
			[
				//modules to be included from the
				//custom project. You can probably
				//list a single module here which in
				//turn declares all the required
				//modules as opposed to listing all the
				//modules individually. Please take a
				//look at dojoRootDir/dojo/_base.js
				//file for an example.
				// "dojo._base"
				
				
				"t360.common",
				"t360.datagrid",
				"t360.dialog",
				"t360.itemselect",
				"t360.menu",
				"t360.OnDemandGrid",
				"t360.partySearch",
				"t360.phraseLookup",
				"t360.popup",
				"t360.textFormatter",
				"t360.test",	
				"t360.checkSessionTimeOut",
				"widget.FormSidebar"
			]
		},

		//This layer is used to discard modules
		//from the dijit package.

		//Custom layer mydojo.js which
		// includes our custom Dojo artifacts
		{
		//place the file under dojoRootDir
		name: "../t360/t360-widgets.js",
		layerDependencies:
			[
			 	"../t360/t360.js",
			],
		dependencies:
			[
				//modules to be included from the
				//custom project. You can probably
				//list a single module here which in
				//turn declares all the required
				//modules as opposed to listing all the
				//modules individually. Please take a
				//look at dojoRootDir/dojo/_base.js
				//file for an example.
				// "dojo._base"
				
				"t360.widget.OnDemandGrid",
				"t360.widget.DataGrid",
				"t360.widget.FormSidebar",
				"t360.widget.Dialog",
				"t360.widget.FilteringSelect",
				"t360.widget.MenuBar",
				"t360.widget.Tooltip",
				"dojo-smore.QueryRead"
			]
		}


    ],
    prefixes: 
    		[ 
    		  	[ "dijit", "../dijit" ], 
	    		[ "dojox", "../dojox" ], 
	    		["dgrid", "../dgrid"], 
	    		["put-selector", "../put-selector"], 
	    		["xstyle","../xstyle"], 
	    		["dojo-smore", "../dojo-smore"], 
	    		["t360","../t360"], 
	    		["widget","../widget"], 
	    		["page", "../page"],
	    		["crypto", "../crypto"],
	    		["dialog", "../dialog"],
	    		["themes", "../themes"]
    		]
}