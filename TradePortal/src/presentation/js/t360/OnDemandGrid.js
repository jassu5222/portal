define([
        "dojo/_base/xhr", "dojo/_base/array", "dojo/dom", "dijit/registry", "dojo/_base/declare", "dojo/aspect", 
        "dgrid/Keyboard", "dgrid/Selection", "dgrid/selector", "dgrid/extensions/DijitRegistry",
        "dgrid/extensions/ColumnHider", "dgrid/extensions/ColumnReorder", "dgrid/extensions/ColumnResizer", 
        "t360/widget/OnDemandGrid", "dgrid/ColumnSet", "dojo-smore/QueryRead", "dojo/_base/lang", "t360/common", "dojo/dom-construct", "dojo/domReady!"
        ],
        function ( 	
    		xhr, array, dom, registry, declare, aspect, 
    		Keyboard, Selection, selector, DijitRegistry, 
    		ColumnHider, ColumnReorder, ColumnResizer,
    		OnDemandGrid, ColumnSet, QueryRead, lang, common, domConstruct
    			) {
        var ondemandgrid = {
        	
            createQueryObj: function (searchParms) {
                var queryObj = {};
                var arrURLParams = searchParms.split("&");
                var arrParamNames = new Array(arrURLParams.length);
                var arrParamValues = new Array(arrURLParams.length);
                for (var i = 0; i < arrURLParams.length; i++) {
                    if (arrURLParams[i] && arrURLParams[i].length > 0) {
                        var opIndex = arrURLParams[i].indexOf("=");
                        var key = arrURLParams[i].substring(0, opIndex);
                        var value = arrURLParams[i].substring(opIndex + 1);
                        if (value && value.length > 0) {
                            queryObj[key] = value;
                        }
                    }
                }
                return queryObj;
            }, 
            createOnDemandGrid: function (dataGridId, dataViewName, gridLayout, initSearchParms, sortColumn, notFoundMessage, gridLoadingMessage) {
            	if (isActive =='Y'){
            		gridsToLoad++;
            		startTime(context,dataGridId,"LOAD");
            	}
            	var queryObj = this.createQueryObj(initSearchParms);
            	
            	//globalLoadingMessage is an Global constant defined in footer.jsp
            	var myLoadingMessage = globalLoadingMessage;
            	
                if ( typeof gridLoadingMessage != 'undefined' ) {
                	myLoadingMessage = gridLoadingMessage;
                }
            	var gridDataStore = new QueryRead({
            		url: "/portal/getViewData?vName=" + dataViewName,
            		totalProperty: 'numRows',
            		idProperty: 'rowKey',
            		processQuery: function () {
	            		var query = this.inherited('processQuery', arguments);
	            		query.method = 'POST';
	            		// rename the 'query' property to 'data', which will cause it to be POSTed
	            		// rather than processed as a set of GET parameters
	            		query.data = query.query;
	            		query.query = '';
	            		return query;
            		},
            		processRequest: function(promise) {
    					var resultsPromise = promise.then(function(data) {
    							// Add an errors property to items so errors will be available with the QueryResults
    							data.items.errors = data.errors;
    							return data.items;
    						}),
    						// Need to delegate to build results object, since promises are frozen.
    						results = lang.delegate(resultsPromise, {
    							// Provide a total promise with the data
    							total: promise.then(function(data) {
    								if(myGrid && myGrid.id){
    									//Set total count for every grid
    									var myTotalCountNode = dom.byId(myGrid.id + "_totalCount");
    				                	if (myTotalCountNode) {
    				                		myTotalCountNode.innerHTML = data.numRows;
    				                	}
    				                	//Set no data message, if count=0
    				                	if(data.numRows==0){
    				                		myGrid.set('noDataMessage', nodatafoundglobal);
    				                	}
    				                	//get all server side error from returned json object, show errors, only for account grid.
    				                	var errors = data.items.errors;   
										//Srinivasu_D IR#T36000033578 Rel 9.1 10/22/2014 - commented as msgs are being cleared.
										/*
    			    					if( myGrid && ( myGrid.id=="homeAcctGrid" || myGrid.id=="accountDataGrid" ) ){
    										//Clear errors before loading new errors
    										common.clearErrorSection('_errorSection');
    										//populates new error on errorsection
    										common.addErrorsToErrorSection('_errorSection', errors, false);
    			    					}
										*/
    								}
    								return data.numRows || data.items.length;
    							})
    						});
    				
    					return results;
    				},    				
    				query: function (query, options) {
    				    var results;

    				    // Generate a string key that identifies this query
    				    var queryJSON = JSON.stringify({ query: query, options: options });

    				    // If a cache exists, and contains a cached value for the current query,
    				    // return the cached value - this will prevent server refreshes on ColumnReorder
    				    if (this.cache && this.cache[queryJSON]) {
    				        results = this.cache[queryJSON];
    				    }
    				    else {
    				        // Create a new cache - necessary if none exists
    				        // If a cache did exist, it was for a different query, so replace it,
    				        // freeing up the previous cache for garbage collection
    				        this.cache = {};

    				        // Call QueryRead's 'query' method to fetch results from the server
    				        results = this.inherited('query',arguments);

    				        // Cache the QueryResults
    				        this.cache[queryJSON] = results;
    				    }

    				    return results;
    				}
            	});
            	var myGrid = new declare([OnDemandGrid, Keyboard, Selection, ColumnResizer, ColumnReorder, ColumnHider, DijitRegistry])({
            		columns: gridLayout,
            		store: gridDataStore,
            		minRowsPerPage: 30,
            		maxRowsPerPage: 30,
            		query : queryObj,
            		loadingMessage: "<span class='dojoxGridLoading'>"+myLoadingMessage+"</span>",
            		//noDataMessage: nodatafoundglobal,
            		//showLoadingMessage: true,
            		//pagingMethod : "throttleDelayed",
                    //pagingDelay : 500,
            		sort:[{ attribute: this.sortGridColumn(gridLayout, sortColumn), descending: this.getSortDirection(sortColumn) }] ,
                    keepScrollPosition : true,
                    bufferRows : 10, 
                    queryRowsOverlap : 0,
                    farOffRemoval : 2000                    
            	}, dataGridId);

                myGrid.startup();
                
              //Make particular column non-sortable
                myGrid.on('dgrid-sort', function (event) {    
                	/*IR T36000034285 making Party column sotable
                	 if("homeAllTranGrid" == event.grid.id){
                		if (event.sort[0].attribute === 'Party') {
                              event.preventDefault();
                          }
                	}
                	if("homeNotifGrid" == event.grid.id){
                		if (event.sort[0].attribute === 'Party') {
                              event.preventDefault();
                          }
                	}*/
                	if("homePanelAuthTranGrid" == event.grid.id){
                		if (event.sort[0].attribute === 'Action') {
                              event.preventDefault();
                          }
                	}
                	if (isActive =='Y'){
                		startTime(context, event.grid.id,"SORT");
                	}
                });
                
                myGrid.on('dgrid-refresh-complete',function(event){
                	if (isActive =='Y'){
	                	gridsLoaded++;

	    	    	    var gridStopTime = (new Date()).getTime();
	//    	    	    var context = '<%=formMgr.getNavigateToPage()%>';
	    	    	    var pageTimes = {};
	    	    	    if (context in timer){
	    	    	    	pageTimes = timer[context];
	    	    	    	pageTimes["gridsToLoad"]=gridsToLoad;
	    	    	    	pageTimes["gridsLoaded"]=gridsLoaded;
	    	    	    	timer[context] = pageTimes;
	    	    	    }
	    	  		  var contextName = context;
	//    	  		  //var navId = <%=request.getParameter("ni")%>;
	    	  		  var times = {};
	    	  		  contextName = context + "." + event.grid.id;
	    	  			if (contextName in timer){
	    	  				times = timer[contextName];
	    	  			}
	    	  			//times["startTime"] = gridTimes[event.grid.id];
	    	  			times["creationTimeStamp"] = creationTimeStamp;
	    	  			times["navId"] = navId;
	    		        if (transOid !=''  ){
	    		        	times["transOid"] = transOid;
	    		        }
	    	  			timer[contextName] = times;
	
	
	    	  			
	    	  	   		endTime(context, event.grid.id);
                	}
                });
                
                return myGrid;
            },
            createTextBox: function (this_col, col_node){
        		col_field=col_node.field; //column name
        		
        		domConstruct.create("div", {innerHTML: this_col.label}, col_node);
         		
         		return col_node.domNode;
        	}, 
            //column sort
            sortGridColumn: function(gridLayout, sortColumn){
            	
            	 //Sort Grid column as per the sortColumn param or sort it default
                if ( typeof sortColumn == 'undefined' ||
                        sortColumn == null ||
                        sortColumn == ''  ) {
                     //when no sort info is specific, 
                     // do the default, which is to 
                     // sort on the first non-hidden column ascending
                    
                     //identify the first non-hidden column
                     var colFieldName = "";
                     var colHiddenValue = "";
                     var gridColumns =this.getGridColumns(gridLayout);
                     
                    	 for (var i in gridColumns) {
                    		 colFieldName = gridColumns[i].field;
                    		 colHiddenValue = gridColumns[i].hidden;
                    		 
                    		 if(colHiddenValue){
                    			 // do nothing
                    		 }else{
                    			 break;
                    		 }
                    	 }                    	 
                    	 return colFieldName;
                   }
                   else {                	  
                     //if sortColumn is specified attempt it
                     // if sortColumn is invalid for any reason
                     // we just won't sort
                     if ( isNaN(sortColumn) ) {
                       //assume its a column id
                       //translate into numeric sortColumn
                       var sortDirection = '';
                       var colFieldName = sortColumn;
                       if ( sortColumn.indexOf('-')==0 ) {
                    	   colFieldName = sortColumn.substring(1);
                         sortDirection = '-';
                       }                       		
                       return colFieldName;                     
                     }
                     else {
                    	 //Sort the column of given index.
                    	 var gridColumns =this.getGridColumns(gridLayout);
                    	 if( gridColumns[sortColumn] ){   
	                    	 var colFieldName = gridColumns[sortColumn].field;     
	                    	 return colFieldName;   
                    	 }
                     }
                   }
            },
            //Get sort direction asce/desc
            getSortDirection: function(sortColumn){
            	 if ( typeof sortColumn != 'undefined' || sortColumn != null ||
                         sortColumn != ''  ) {            		 
            		 if ( isNaN(sortColumn) ) {
            			 
            			 var sortDirection = '';
                         var colFieldName = sortColumn;
                         if ( sortColumn.indexOf('-')==0 ) {                      	   
                        	 return true;                          
                         }else{
                        	 return false;
                         }     
            		 }
            	 }
            	
            },
            //used to get records based upon searchParms
            searchDataGrid: function(dataGridId, dataViewName, searchParms, gridLoadingMessage){    
                var grid = registry.byId(dataGridId);
                if (grid) {
                    if(gridLoadingMessage){
                    	grid.set("loadingMessage", "<span class='dojoxGridLoading'>"+gridLoadingMessage+"</span>");
                    }
                
                  var queryObj = this.createQueryObj(searchParms);
                  grid.setQuery(queryObj);                  
                }
              },  
            //to-do :- while implementing grid with radio/checkbox component  
            getSelectedGridRowKeys: function(dataGridId){
                var rowKeys = [];
                var grid =registry.byId(dataGridId);
                if (grid) {

                        var rowKeyIdx = 0;
                        // Iterate through all currently-selected items
                        for(var id in grid.selection){
                            if(grid.selection[id]){
                                rowKeys[rowKeyIdx]  = id;
                                rowKeyIdx++;
                            }
                        }


                }
                return rowKeys;
            },

          //get grid columns for the customization popup
            // this is in a specific simplified format becuase it is
            // passed to an ajax call, so we want it to be small
            //format is <columnId>,<columnId>:off, where
            // off means the columns is not visible
            getColumnsForCustomization: function(gridId) {
              var columns = "";
              var theGrid = registry.byId(gridId);
              //structure is column layout, but order could be different due to reordering
              var theStructure = theGrid.get('subRows');
              theStructure = theStructure[0];
              var gridColumns;
              //see if have fields or more advanced structure
              if ( theStructure[0].field ) {
                gridColumns = theStructure;
              }
              else {
                //assumes first item is selection object, 2nd is columns
                gridColumns = theStructure[1];
              }
              //loop through cell names
              var columnIdx=0;
              for (var i = 0; i<gridColumns.length; i++) {
                //var theCell = theGrid.getCell(i);
                //var cellName = theGrid.getCellName(theCell);
            	  
            	var cellName = gridColumns[i].field;
                //find the column in the structure
                for (var j in gridColumns) {
                  var columnId = gridColumns[j].field;
                  if (columnId == cellName) {
                    if ( columnIdx > 0 ) {
                      columns += ",";
                    }
                    columns += columnId;
                    //if hidden, include just set to :off
                    //we want the hidden fields because they are still in the grid ordered
                    // this allows user to check/uncheck fields that they want and 
                    // it staying in place on the popup rather than moving around
                    var myHidden = gridColumns[j].hidden;
                    if (myHidden) {
                      columns += ":off";
                    }
                    columnIdx++;
                    break;
                  }
                }
              }
              return columns;
            },

            //hide a grid column
            //this modifies the grid layout and
            // then resets the layout on the grid
            // so the grid repaints
            hideColumn: function(gridId, columnId) {
              var theGrid = registry.byId(gridId);
              //this is the grids internal layout structure
              // not the layout we set
              //we create a new layout becuase trying to 
              // modify the existing layout causes issues
              var theStructure = theGrid.get('columns');
              var gridColumns = theStructure;
              for (var i = 0; i<gridColumns.length; i++) {
            	  if ( columnId == gridColumns[i].field ) {
            		  theGrid.toggleColumnHiddenState(gridColumns[i].id, true);
            		  break;
                  }
              }
            },

            //show a grid column
            // of course assumes the grid column is hidden
            //this modifies the grid layout and
            // then resets the layout on the grid
            // so the grid repaints
            showColumn: function(gridId, columnId) {
              var theGrid = registry.byId(gridId);
              //this is the grids internal layout structure
              // not the layout we set
              //we create a new layout becuase trying to 
              // modify the existing layout causes issues
              var theStructure = theGrid.get('columns');
              var gridColumns = theStructure;
              for (var i = 0; i<gridColumns.length; i++) {
            	  if ( columnId == gridColumns[i].field ) {
            		  theGrid.toggleColumnHiddenState(gridColumns[i].id, false);
            		  break;
                  }
              }
            },

            copyColumnLayout: function(fromColumn, toColumn) {
              var layoutName = fromColumn.name;
              if ( layoutName ) {
                toColumn.name = layoutName;
              }
              var layoutField = fromColumn.field;
              if ( layoutField ) {
                toColumn.field = layoutField;
              }
              var layoutFields = fromColumn.fields;
              if ( layoutFields ) {
                toColumn.fields = layoutFields;
              }
              var layoutFormatter = fromColumn.formatter;
              if ( layoutFormatter ) {
                toColumn.formatter = layoutFormatter;
              }
              var layoutWidth = fromColumn.width;
              if ( layoutWidth ) {
                toColumn.width = layoutWidth;
              }
              var layoutCellClasses = fromColumn.cellClasses;
              if ( layoutCellClasses ) {
                toColumn.cellClasses = layoutCellClasses;
              }
              var layoutHidden = fromColumn.hidden;
              if ( layoutHidden ) {
                toColumn.hidden = true;
              }
            },

            saveGridLayout: function(gridId,gridName) {
              var urlParms = "action=save&gridId="+gridId+"&gridName="+gridName;
              //spin through the grid creating appropriate url
              // parms for each visible column
              var theGrid = registry.byId(gridId);
        	  	var colIdx = 0;
        	  	var layoutHidden;
        	  	var gridColumns = theGrid.get('subRows');
        	  	gridColumns = gridColumns[0];
        	  	
        	  	for(col = 0; col<gridColumns.length; col++){
        	  		layoutHidden = gridColumns[col].hidden;

        	  		if(!layoutHidden){
        	  			urlParms+="&columnId"+colIdx+"="+gridColumns[col].field;
      	  	          	urlParms+="&width"+colIdx+"="+gridColumns[col].width;
      	  	          	colIdx++;
        	  		}
        	  	}
              var remoteUrl = "/portal/customizeGrid?" + urlParms;
              //ajax call to save grid columns
              //include grid id and all columns that are checked
              xhr.get({
                url: remoteUrl,
                load: function(data) {
                  if ( data.indexOf("SUCCESS")>=0 ) {
                    //do nothing
                  }
                  else {
                    var errMsg = "Unknown error saving grid layout.";
                    if ( data.indexOf("<errorMessage>")>=0 ) {
                      errMsg = data.substring( data.indexOf('<errorMessage>')+14 );
                      errMsg = errMsg.substring( 0, errMsg.indexOf('</errorMessage>') );
                    }
                    alert(errMsg);
                  }
                },
                error: function(err) {
                  //todo: make a custom dialog for errors
                  //would be nice to display this in a nice fashion, 
                  // probably also allow the user to avoid the error in future
                  // an alternative would be to have some kind of error display at bottom of the page???
                  alert('Error retrieving remote content:'+err);
                }
              });
            },

            resetGridLayout: function(gridId,gridName) {
              //ajax call to reset grid columns
              //only grid id is needed
              var urlParms = "action=reset&gridId="+gridId+"&gridName="+gridName;
              var remoteUrl = "/portal/customizeGrid?" + urlParms;
              xhr.get({
                url: remoteUrl,
                load: function(data) {
                  if ( data.indexOf("SUCCESS")>=0 ) {
                    //pull gridLayout out of the response
                    var gridLayoutStr = data.substring( data.indexOf('<gridLayout>')+12 );
                    gridLayoutStr = gridLayoutStr.substring( 0, gridLayoutStr.indexOf('</gridLayout>') );
                    //need to eval the string to make it an array
                    //todo: eval is slow, can this be optimized?
                    var gridLayout = eval(gridLayoutStr);
                    
                    //refresh the grid with the new details
                    var theGrid = registry.byId(gridId);
                    //theGrid.setColumns(gridLayout);
                    //theGrid.refresh();
                    /*KMehta IR-T36000037880 Rel 9300 on 22-Apr-2015 ADD - Start */
            	  	var gridColumns = theGrid.get('subRows');
            	  	gridColumns = gridColumns[0];
                    
            	  	for(col = 0; col<gridColumns.length; col++){

            	  		if(gridColumns[col].hidden)
            	  			gridColumns[col].hidden = false;
            	  	}
                    theGrid.set("columns", gridColumns); 
                    /*KMehta IR-T36000037880 Rel 9300 on 22-Apr-2015 ADD - End */
                  }
                  else {
                    var errMsg = "Unknown error resetting grid layout.";
                    if ( data.indexOf("<errorMessage>")>=0 ) {
                      errMsg = data.substring( data.indexOf('<errorMessage>')+14 );
                      errMsg = errMsg.substring( 0, errMsg.indexOf('</errorMessage>') );
                    }
                    alert(errMsg);
                  }
                },
                error: function(err) {
                  //todo: make a custom dialog for errors
                  //would be nice to display this in a nice fashion, 
                  // probably also allow the user to avoid the error in future
                  // an alternative would be to have some kind of error display at bottom of the page???
                  alert('Error retrieving remote content:'+err);
                }
              });
            },

            //this helper function returns the grid columns array
            // for a given layout
            getGridColumns: function(gridLayout) {
              var gridColumns;
              //see if have fields or more advanced structure
              if ( gridLayout[0].field ) {
                gridColumns = gridLayout;
              }
              else {
                //assumes first item is selection object, 2nd is columns
                gridColumns = gridLayout[1];
              }
              return gridColumns;
            },
            
            //cquinton 9/11/2012 Rel portal refresh ir#t36000004008 start
            //return the column index for a given layout and column id
            // return -1 if not found
            getColumnIndex: function(gridLayout,columnId) {
              var colIdx = -1;
              var gridColumns = this.getGridColumns(gridLayout);
              for (var i = 0; i<gridColumns.length; i++) {
                if ( columnId == gridColumns[i].field ) {
                  colIdx = parseInt(i);
                  break;
                }
              }
              return colIdx;
            },
            //cquinton 9/11/2012 Rel portal refresh ir#t36000004008 end

          
            
        //MEerupula Rel 8.3 IR-T36000021433/20624 utility method to get Column Index with given Column Name- starts 
            
            getColumnIndexWithName: function(dataGridId, columnName){    	
                var index = -1;  
                var grid = registry.byId(dataGridId);
                  if (grid) {
                	  array.forEach(grid.layout.cells, function(cell,idx) {    	
                      	if (cell.field == columnName) {
                      	 	index = idx +1;  
                      	 	return index;
                      	}
                  	});
                  }
                 return index;
            }
            
         //MEerupula Rel 8.3 IR-T36000021433/20624 utility method to get Column Index with given Column Name- ends
            
        };
        return ondemandgrid;
    });
