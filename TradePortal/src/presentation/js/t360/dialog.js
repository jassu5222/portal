define("t360/dialog", [
  "dojo/_base/array",
  "dojo/dom",
  "dojo/dom-construct",
  "dijit/registry",
  "dojo/dom-class",
  "t360/widget/Dialog"
], function(arrayUtil, dom, domConstruct,registry, domClass, T360Dialog) {

  var dialog = {
    //dialogRefs is an object that holds references to dialog instances
    //this allows us to deal with multiple dialogs on a page
    dialogRefs: {},

    open: function(dialogId, dialogTitle, dialogJsp, parmName, parmValue, callbackName, callbackFunction, dialogType, customCallBackFunction) {
      
      var dialogUrl = "/portal/dialog/" + dialogJsp;
      
      //always include the dom id as a url parameter
      dialogUrl += "?dialogId=" + dialogId;
      //add other parms if they were passed in
      if ( parmName!=null ) { //something exists

        //determine if a single parmValue or multiple
        var multipleParms = false;
        if (parmName instanceof Array ) {
          multipleParms = true;
        }

        //generate url parameters
        if (multipleParms == true) {
          for (var myParmIdx in parmName) {
            var myParmName = parmName[myParmIdx];
            var myParmValue = parmValue[myParmIdx];
         // <START IR T36000010804- komal - 29th Jan 2013> - Encoded the parameter value so that all the special characters contained within the parameters are
        	// sent to the dialog as request parameter. The catch here is this parameter should be Decoded on the serverside (JSP / Servlet)
        	// NEED TO ANALYZE THIS FURTHER BEFORE MAKING ANY CONCRETE DECISION. THE FOLLOWING ENCODING OF THE PARAMETER VALUE SOLVES THE
        	// ISSUE ASSOCIATED WITH THE 'NEWLINE' GETTING RIPPED OFF FROM THE TEXT AREA ATTRIBUTE ON THE FORM. - Announcement page
            dialogUrl += "&" + myParmName + "=" + encodeURIComponent(myParmValue);
            //<END -komal - 29th Jan 2013>
          }
        } 
        else { //assume a single
          //dialogUrl += "&" + parmName + "=" + parmValue;
        	// <START - rajendra - Dec-11-2012> - Encoded the parameter value so that all the special characters contained within the parameters are
        	// sent to the dialog as request parameter. The catch here is this parameter should be Decoded on the serverside (JSP / Servlet)
        	// NEED TO ANALYZE THIS FURTHER BEFORE MAKING ANY CONCRETE DECISION. THE FOLLOWING ENCODING OF THE PARAMETER VALUE SOLVES THE
        	// ISSUE ASSOCIATED WITH THE 'NEWLINE' GETTING RIPPED OFF FROM THE TEXT AREA ATTRIBUTE ON THE FORM.
        	dialogUrl += "&" + parmName + "=" + encodeURIComponent(parmValue);
        	//</END - rajendra - Dec-11-2012>
        }
      }

      //only instantiate if it doesn't exist
      if (!registry.byId( dialogId )) {
        //create a new instance mapped by dialogId
        this.dialogRefs[dialogId] = new T360Dialog({
            title: dialogTitle,
            //Added to identify if the dialog is warningDialog or criticalDialog or normalDialog
            dialogType: dialogType,
            //Added for re request the content when dialog coming from hide to show
            refreshOnShow:true,
            //the array of callbacks
            callbacks: callbackName,
            callbackFunctions: callbackFunction,
            onDownloadEnd: customCallBackFunction,
            //load the dialog from url
            href: dialogUrl
  	}, dialogId );
      } else { 
        //just reset dialog data on re-entry, including href
    	this.dialogRefs[dialogId].set("title", dialogTitle);
        this.dialogRefs[dialogId].set("href", dialogUrl);
        this.dialogRefs[dialogId].set("callbacks", callbackName);
        this.dialogRefs[dialogId].set("callbackFunctions", callbackFunction);
      }

      //open it
      this.show( dialogId );
    },

    //todo: replace this with deferreds/promises
    doCallback: function(dialogId, callbackName, arg1, arg2, arg3, arg4, arg5) {
      if (this.dialogRefs[dialogId].callbacks instanceof Array ) {
        for (var myIdx in this.dialogRefs[dialogId].callbacks) {
          var myName = this.dialogRefs[dialogId].callbacks[myIdx];
          if ( callbackName == myName ) {
            //cquinton 1/8/2013 defensively avoid call if callback function does not exist
            var myFunction = this.dialogRefs[dialogId].callbackFunctions[myIdx];
            if ( myFunction && myFunction!=null ) {
              myFunction(arg1, arg2, arg3, arg4, arg5);
            }
          }
        }
      }
      else {
        if ( callbackName == this.dialogRefs[dialogId].callbacks ) {
          //cquinton 1/8/2013 defensively avoid call if callback function does not exist
          var myFunction = this.dialogRefs[dialogId].callbackFunctions;
          if ( myFunction && myFunction!=null ) {
            this.dialogRefs[dialogId].callbackFunctions(arg1, arg2, arg3, arg4, arg5);
          }
        }
      }
    },

    show: function(dialogId) {
    	var dialogToShow = registry.byId(dialogId);
    	//RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - Start - Add check if dialog is initialized in registry if not then use the array ref
    	if (dialogToShow){
    		registry.byId(dialogId).show();
    	}else{
    		if (this.dialogRefs[dialogId]){
    			this.dialogRefs[dialogId].show();
    		}else{
    			console.log('Dialog not defined with ID: '+ dialogId);
    		}
    	}
    	//RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - End
    },

    hide: function(dialogId) {
    	var dialogToHide = registry.byId(dialogId);
    	//RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - Start - Add check if dialog is initialized in registry if not then use the array ref
    	if (dialogToHide){
    		registry.byId(dialogId).hide();
    	}else{
    		if (this.dialogRefs[dialogId]){
    			this.dialogRefs[dialogId].hide();
    		}else{
    			console.log('Dialog not defined with ID: '+ dialogId);
    		}
    	}
    	//RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - End 
    },

    //Prateep added for destroy all the widgets under dialog, this is use full when we recursively calling same dialog in same page
    close: function(dialogId) {
      //cquinton 12/10/2012 close also hides the dialog. also include removing dialog widget itself and associated dom nodes
      var myDialog = registry.byId(dialogId);
      if ( myDialog ) {
        myDialog.hide();
        var myDomNode = myDialog.domNode;
        myDialog.destroyRecursive(true);
        domConstruct.empty(myDomNode);
        //var myDomNode = myDialog.domNode;
        //remove widgets and close
        //var popupWidgets = registry.findWidgets(myDomNode);
        //arrayUtil.forEach(popupWidgets, function(entry, i) {
        //  entry.destroyRecursive(true);
        //});
      }
    }

  };

  return dialog;
});

