define(["dojo/_base/array", "dojo/dom",
         "dojo/dom-construct", "dojo/dom-class", 
         "dojo/dom-style", "dojo/query",
         "dijit/registry", "dojo/on", 
         "dijit/focus",
         "dojo/NodeList-traverse"],
    function(arrayUtil, dom,
             domConstruct, domClass,
             domStyle, query,
             registry, on, 
             focusUtil ) {

  //this package assumes a domnode of selected item rows
  // each row will contain the set of inputs necessary to save the data in a form submit
  //structure is assumed as follows:
  // <div id="selectedItems">
  //   <div class="selectedItem">
  //     <span class="selectedItemOrder">
  //       <input id="display_orderXXX" name="display_orderXXX" class="selectedItemOrderTextBox">
  //     </span>
  //     <span id="selectedDescXXX" class="selectedItemDesc">display text</span>
  //     <input id="selectedItemOidXXX" name="selectedItemOidXXX" type="hidden">
  //     <input id="selectedItemIdXXX" name="selectedItemIdXXX" type="hidden">
  //     <input id="selectedItemOptLockXXX" name="selectedItemOptLockXXX" type="hidden">
  //   </div>
  // </div>

  //to select an item, an available item is selected/unselected
  //structure of each chckbox is assumed as follows:
  // <div id="<itemId>_avail" class="availItem">
  //   <span class="availItemSelect">
  //     <input id="<itemId>_checkbox
  //   </span>
  //   <span class="availItemDesc">display text</span>
  // </div>

  var itemSelect = {
    getSelectedItemCount: function(selectedItemsId) {
      return dom.byId(selectedItemsId).children.length;
    },

    getMaxOrderValue: function(selectedItemsId) {
      var maxOrder = 0;
      var selectItems = dom.byId(selectedItemsId);
      var orderWidgets = registry.findWidgets(selectItems);
      arrayUtil.forEach(orderWidgets, function(entry) {
        var orderValue = entry.get('value');
        if (orderValue > maxOrder ) {
          maxOrder = parseInt(orderValue);
        }
      });
      return maxOrder;
    },

    //note: selectItem is so specific to each page we just do locally on page
    unselectItem: function(checkboxWidget, selectedItemsId, selectedItemFieldIds) {

      //cquinton 12/14/2012 remove calculations on total count, just set
      // the count at the end
      //this.decreaseTotalCount(selectedItemsId);

      //remove the selected item
      var checkboxId = checkboxWidget.id;
      var idx = checkboxId.indexOf("_checkbox");
      var deleteSelectItemId = checkboxId.substring(0,idx);

      //cquinton 9/13/2012 ir#t36000004200
      //look through the list of selected items and find
      // the one that has the correct selected Item id
      //also get the index of it
      var deleteSelectItem;
      var deleteIdx = -1;
      query("#"+selectedItemsId).children(".selectedItem").
        children(".selectedItemId").forEach( function(entry) {
          if ( entry.value == deleteSelectItemId ) {
            deleteSelectItem = entry.parentNode;
            var mySelectItemIdPrefix = selectedItemFieldIds[1];
            var deleteIdxStr = entry.id.substring(mySelectItemIdPrefix.length);
            deleteIdx = parseInt(deleteIdxStr);
          }
        }
      );

      if ( deleteSelectItem && deleteIdx >= 0 ) {
        var lastIdx = this.getSelectedItemCount(selectedItemsId)-1;

        //if this is not the last item, iterate through moving data
        while (deleteIdx < lastIdx) {
          var copyIdx = deleteIdx+1;

          for (var i=0; i< selectedItemFieldIds.length; i++ ) {
            var copyTo = dom.byId(selectedItemFieldIds[i]+deleteIdx);
            var copyFrom = dom.byId(selectedItemFieldIds[i]+copyIdx);
            if ( typeof copyFrom.value == 'undefined' ) {
              copyTo.innerHTML = copyFrom.innerHTML;
            }
            else {
              copyTo.value = copyFrom.value;
            }
          }

          deleteIdx = copyIdx;
        }

        //get the last selected item
        var deleteDesc = dom.byId(selectedItemFieldIds[1]+lastIdx);
        deleteSelectItem = deleteDesc.parentNode;
        //remove the widgets
        //this is necessary so same id can be registered again if added back
        var orderWidgets = registry.findWidgets(deleteSelectItem);
        arrayUtil.forEach(orderWidgets, function(entry, i) {
          entry.destroyRecursive(true);
        });
        //remove the domNode
        domConstruct.destroy(deleteSelectItem);
      }

      //after all is done, synch up the actual count
      this.setTotalCount(selectedItemsId);
    },
 
    increaseTotalCount: function(selectedItemsId) {
      //increase totalCount
      var totalCountNode = dom.byId(selectedItemsId+"_totalCount");
      var count = parseInt(totalCountNode.innerHTML);
      count = count + 1;
      totalCountNode.innerHTML = count;
    },

    decreaseTotalCount: function(selectedItemsId) {
      //increase totalCount
      var totalCountNode = dom.byId(selectedItemsId+"_totalCount");
      var count = parseInt(totalCountNode.innerHTML);
      count = count - 1;
      totalCountNode.innerHTML = count;
    },

    //cquinton 12/14/2012 new function that just sets the total count
    // to the number of current items - this is more accurate than
    // trying to keep track of how many there are
    //increaseTotalCount and decreaseTotalCount nodes have been removed
    setTotalCount: function(selectedItemsId) {
      var count = this.getSelectedItemCount(selectedItemsId);
      var totalCountNode = dom.byId(selectedItemsId+"_totalCount");
      totalCountNode.innerHTML = count;
    },

    deleteEventHandler: function(deleteNode) {
      //uncheck the available item
      //this automatically does the onchange mechanism there
      //including removing this item and decreasing the total count
      var deleteSelectItem = deleteNode.parentNode;
      var deleteSelectItemId;
      //cquinton 9/13/2012 ir#t36000004200
      //create a nodelist so we can use children()
      // to get the id
      var nl = new query.NodeList();
      nl.push(deleteSelectItem);
      nl.children(".selectedItemId").forEach( function(entry) {
        deleteSelectItemId = entry.value;
      });
      var checkbox = registry.byId(deleteSelectItemId+'_checkbox');
      if ( checkbox ) {
        checkbox.set('checked',false);
      }
    },

    updateOrder: function(selectedItemsId, selectedItemFieldIds, lastChangedId){
      var count = this.getSelectedItemCount(selectedItemsId);

      //Validate order - todo: popup field validation messages
      for( var i=0; i<count; i++ ){
        //ie7 does not like trim so we just skip it
        //textValue = dom.byId(selectedItemFieldIds[2]+i).value.trim();
        var textValue = dom.byId(selectedItemFieldIds[2]+i).value;
        //Check for in-correct value
        if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < 1) ) {
          dom.byId(selectedItemFieldIds[2]+i).focus();
          return;
        }
      }

      //if lastChangedId is present, this is the id of the display order
      // item that was last touched.
      //get the currently focused item
      if ( lastChangedId ) {
        var lastChangedValue = parseInt(dom.byId(lastChangedId).value);
        //spin through and see if there is another above this that
        // has the same value, if so swap it
        for (var i=0; i<count; i++ ) {
          //if ids match, drop out we don't care anymore
          if ( lastChangedId == selectedItemFieldIds[2]+i ) {
            break;
          }
          var otherValue = parseInt(dom.byId(selectedItemFieldIds[2]+i).value);
          if ( otherValue == lastChangedValue ) {
            var displayOrderPrefix = selectedItemFieldIds[2];
            var lastChangedIdxStr = lastChangedId.substring(displayOrderPrefix.length);
            this.swapSelectedItems(selectedItemFieldIds, i, lastChangedIdxStr);
            break;
          }
        }
      }

      //use a simple bubble sort to ensure ordered
      var swapped = false;
      var item1Order;
      var item2Order;
      do {
        swapped = false;
        for (var i=1; i<count; i++) {
          item1Order = parseInt(dom.byId(selectedItemFieldIds[2]+(i-1)).value);
          item2Order = parseInt(dom.byId(selectedItemFieldIds[2]+i).value);
          if ( item1Order > item2Order ) {
            this.swapSelectedItems(selectedItemFieldIds, i-1, i);
            swapped = true;
          }
        }
      } while ( swapped ); //continue until no swapping occurs

      //cquinton 11/28/2012 add renumbering
      //now update the order numbers
      for( var i=0; i<count; i++ ){
        var orderField = dom.byId(selectedItemFieldIds[2]+i);
        if ( orderField.value != i+1 ) {
          orderField.value = i+1;
        }
      }
    },

    swapSelectedItems: function(selectedItemFieldIds, index1, index2) {
      var tempValue;
      //swap data for all of the fields
      for (var i=0; i< selectedItemFieldIds.length; i++ ) {
        var swap1 = dom.byId(selectedItemFieldIds[i]+index1);
        var swap2 = dom.byId(selectedItemFieldIds[i]+index2);
        if ( typeof swap1.value == 'undefined' ) {
          tempValue = swap1.innerHTML;
          swap1.innerHTML = swap2.innerHTML;
          swap2.innerHTML = tempValue;
        }
        else {
          tempValue = swap1.value;
          swap1.value = swap2.value;
          swap2.value = tempValue;
        }
      }
    }

  };

  return itemSelect;

});

  
