define([
  "dojo/_base/array",
  "dojo/dom",
  "dojo/dom-construct",
  "dijit/registry",
  "dijit/popup",
  "dojox/layout/ContentPane"
], function(arrayUtil, dom, domConstruct, registry, popup, ContentPane) {

  var popupLib = {
    open: function(popupAnchor, popupJsp, parmName, parmValue, callbackName, callbackFunction) {

      //first determine if there is already a widget
      // for the popup widget.  if so, just drop out
      //this fixes issues with trying to bring up the popup
      // twice
      var popupId = popupAnchor + "_popup";
      var popupRef = registry.byId(popupId);
      if (popupRef) {
        return popupRef;
      }
      
      var popupUrl = "/portal/dialog/" + popupJsp;
      
      //add parms if they were passed in
      if ( parmName!=null ) { //something exists

        popupUrl += "?";

        //determine if a single parmValue or multiple
        var multipleParms = false;
        if (parmName instanceof Array ) {
          multipleParms = true;
        }

        //generate url parameters
        if (multipleParms == true) {
          for (var myParmIdx in parmName) {
            var myParmName = parmName[myParmIdx];
            var myParmValue = parmValue[myParmIdx];
            if ( myParmIdx > 0 ) {
              popupUrl += "&";
            }
            popupUrl += myParmName + "=" + myParmValue;
          }
        } 
        else { //assume a single
          popupUrl += parmName + "=" + parmValue;
        }
      }

      popupRef = new ContentPane({
        //name the popup popup widget
        //the dom anchor id + _popup
        id: popupId,
        onMouseLeave: function() {
          popupLib.close(popupRef);
        }
      });
      popup.moveOffScreen(popupRef);
      if(popupRef.startup && !popupRef._started) {
        popupRef.startup();
      }
      popupRef.set('href',popupUrl);
      popup.open({
        popup: popupRef,
        around: dom.byId(popupAnchor),
        //this is not quite accurate, but for quick delivery adding it
        //problem is that the calculation is pre-ajax population of the popup
        // probably need to dynamically set this
        orient: ["below-alt"]
      });

      return popupRef;
    },

    close: function(popupRef) {
      var myDomNode = popupRef.domNode;
      //remove widgets and close
      var popupWidgets = registry.findWidgets(popupRef.domNode);
      arrayUtil.forEach(popupWidgets, function(entry, i) {
        entry.destroyRecursive(true);
      });
      popup.close(popupRef);
      popupRef.destroy();
      //remove the parent node - which is a dijitPopup
      domConstruct.destroy(myDomNode.parentNode);
    }

    //todo: replace this with deferreds/promises
    //doCallback: function(dialogId, callbackName, arg1, arg2, arg3, arg4, arg5) {
    //  if (this.dialogRef.callbacks instanceof Array ) {
    //    for (var myIdx in this.dialogRef.callbacks) {
    //      var myName = this.dialogRef.callbacks[myIdx];
    //      if ( callbackName == myName ) {
    //        var myFunction = this.dialogRef.callbackFunctions[myIdx];
    //        myFunction(arg1, arg2, arg3, arg4, arg5);
    //      }
    //    }
    //  }
    //  else {
    //    if ( callbackName == this.dialogRef.callbacks ) {
    //      this.dialogRef.callbackFunctions(arg1, arg2, arg3, arg4, arg5);
    //    }
    //  }
    //}
  };

  return popupLib;
});

