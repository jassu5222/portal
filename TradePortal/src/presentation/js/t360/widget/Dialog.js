define(["dojo/_base/declare", "dijit/registry","dojo/dom-class", "dojox/widget/DialogSimple"], 
  function(declare, registry, domClass, Dialog){
    return declare("t360.widget.Dialog", Dialog, {
      critical: false,
      postCreate: function(){
        // perform any logic from superclasses
        this.inherited(arguments);
        
        if (this.dialogType == 'warningDialog' || this.dialogType =='criticalDialog'){
        	domClass.add(this.domNode, this.dialogType);
        } else{
        	domClass.add(this.domNode, "normalDialog");
        }
      }
    });
  }
);

