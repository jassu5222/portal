define(["dojo/_base/declare", "dgrid/Grid", "dgrid/OnDemandList"], function(declare, Grid, OnDemandList){
    return declare("t360.widget.OnDemandGrid",[Grid, OnDemandList], {});
});