//Acts like On and Off Tooltip. More precisly it avoids the initial connection and registration of events
//if the tips are disabled by a global variable globalUIPref.showToolTips
//Please use this versus the dijit.Tooltip when you want this behaviour (most of the times).
define([
        "dojo/_base/declare",
        "dijit/Tooltip",
        "dojo/_base/lang"
  ], 
  function(declare, Tooltip, lang){
    return declare("t360.widget.Tooltip", Tooltip,  {
        //saves the connectid what you would keep for a dom element - id of a dom element        
        saveConnId: null, 
        
        _setSaveConnIdAttr: function(anId) {
                this.saveConnId=anId;
                //globalUIPref is defined in Footer.jsp as a global variable
                if ( typeof globalUIPref == 'undefined' )   {
                    this.connectId=lang.isArrayLike(anId) ? anId : [anId];
                } else {
                     if(globalUIPref.showToolTips == true)
                        this.connectId=lang.isArrayLike(anId) ? anId : [anId];
                }
        }

    });
  }
);

