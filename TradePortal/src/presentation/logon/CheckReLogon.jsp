<%--
 *   After relogon, determine next behavior.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, 
    com.ams.tradeportal.mediator.util.*, com.ams.tradeportal.busobj.webbean.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%

   DocumentHandler xmlDoc = formMgr.getFromDocCache();

   // Set the current page we're about to be forwarded to in form manager
   formMgr.setCurrPage("TradePortalHome");

   //set active session on the listener and on userSession
   String activeUserSessionOid = xmlDoc.getAttribute("/Out/activeUserSessionOid");
   TradePortalSessionListener listener = (TradePortalSessionListener)
       session.getAttribute("TimeoutListener");
   listener.setActiveUserSessionOid(activeUserSessionOid);

   DocumentHandler logoutDoc = listener.getLogoutDoc();
   logoutDoc.setAttribute("/In/activeUserSessionOid", activeUserSessionOid);
   listener.setLogoutDoc(logoutDoc);

   userSession.setActiveUserSessionOid(activeUserSessionOid);
%>
<%@ include file="fragments/forwardOnLogonSuccess.frag" %>
