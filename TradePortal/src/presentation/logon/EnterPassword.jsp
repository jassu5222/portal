<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
	  java.net.URL,java.util.*" %><%-- CR-817 Rel8.2.0.0 JKOK --%>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session" />
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session" />
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session" />


<%

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String version = resMgr.getText("version", "TradePortalVersion");
  if (version.equals("version")) {
    version = resMgr.getText("Footer.UnknownVersion", 
                             TradePortalConstants.TEXT_BUNDLE);
  }
 //M Eerupula 03/21/2013 Rel 8.2 T3600001495 version is encrypted consistently to fix the URL difference for cache busting i.e.,the version is same on all included pages 
version = EncryptDecrypt.bytesToBase64String(version.getBytes());
 //RPasupulati geting first 4 char's from encripted version String IR no T36000017311 Starts.
 //version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey());
 version = version.substring(0, 4);
 //RPasupulati geting first 4 char's from encripted version String IR no T36000017311 ENDs.
  // [START] CR-482  
  // Check if this TradePortal instance is configured for HSM encryption / decryption
  boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
  // [END] CR-482

  formMgr.setCurrPage("EnterPassword");

  DocumentHandler doc = formMgr.getFromDocCache();
  Hashtable addlParms = new Hashtable();
  String brandingDirectory;
  String organizationId;
  String loginId = "";
  DocumentHandler brandedButtonDoc;
  String brandedButtonInd = null;
  String brandedButtonDirectory = null;
  
  if ((doc != null) && (doc.getAttribute("/In/User/loginId") != null)) {
    // Get page parameters from the XML that was returned from the mediator
    brandingDirectory = doc.getAttribute("/In/brandingDirectory");
    organizationId = doc.getAttribute("/In/organizationId");
    loginId = doc.getAttribute("/In/User/loginId");
      
    // [START] CR-482
    if(instanceHSMEnabled && (doc.getAttribute("/Out/User/loginId") != null)) {
    	loginId = doc.getAttribute("/Out/User/loginId");
    }
    // [END] CR-482
 
    // NShrestha - 07/09/2009 - IR#RHUJ061654844 - Begin --
    loginId = StringFunction.xssCharsToHtml(loginId);
    // NShrestha - 07/09/2009 - IR#RHUJ061654844 - End --

    // Determine which login attempt this is (the first, the second, the third, etc)
    int loginAttemptNumber;

    try {
      loginAttemptNumber = Integer.parseInt(doc.getAttribute("/In/loginAttemptNumber"));
    } 
    catch(Exception e) {
      loginAttemptNumber = 1;
    }

    // If the logon failed, and they have already tried to log in the maximum
    // number of times, redirect them to another page
    if ( loginAttemptNumber == SecurityRules.getLoginAttemptsBeforeBeingRedirected() ) {
      StringBuffer parmsFromLogon = new StringBuffer();
      parmsFromLogon.append("?branding=");
      parmsFromLogon.append(brandingDirectory);
      parmsFromLogon.append("&organization=");
      parmsFromLogon.append(organizationId);
      parmsFromLogon.append("&locale=");
      parmsFromLogon.append(resMgr.getResourceLocale());

      String tooManyLogonsPage = NavigationManager.getNavMan().getPhysicalPage("TooManyInvalidLogonAttempts", request);
%>
  <jsp:forward page='<%= tooManyLogonsPage + parmsFromLogon %>' />
<%
    }

    // Increment the login attempt number
    addlParms.put("loginAttemptNumber",String.valueOf(loginAttemptNumber+1));
  }
  else {
    // Get page parameters from the URL parameters
	brandingDirectory = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
    organizationId = request.getParameter("organization");

    // This is the first time in a row that they are logging in, so set count to 1
    addlParms.put("loginAttemptNumber","1");

    formMgr.storeInDocCache("default.doc",new DocumentHandler());
  }

  //cquinton 2/27/2013 remove unused branded button code

  // This flag indicates that the password must be validated.
  addlParms.put("auth", TradePortalConstants.AUTH_PASSWORD);
  addlParms.put("brandingDirectory", brandingDirectory);
  addlParms.put("organizationId", organizationId);
  addlParms.put("locale", resMgr.getResourceLocale());
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div id="mainPageContent" class="pageContent">

  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="Login.Title" />
    <jsp:param name="helpUrl"  value="/customer/login.htm" />
  </jsp:include>


<form id="LogonForm" name="LogonForm" method="POST" 
      data-dojo-type="dijit.form.Form"
      action="<%= formMgr.getSubmitAction(response) %>">

  <jsp:include page="/common/ErrorSection.jsp" />
<input type="hidden" name="cTime" id="cTime" />
<input type="hidden" name="prevPage" id="prevPage" />
  <div class="formContentNoSidebar">

    <div class="padded">

      <input type=hidden value="" name=buttonName>
<%
  // [START] CR-482
  if (instanceHSMEnabled) {
%>
      <input type=hidden name="encryptedLoginId" value="">
      <input type=hidden name="encryptedPassword" value="">
<%-- [START] CR-543 --%>
      <input type=hidden name="protectedPassword" value="">
<%-- [END] CR-543 --%>
<%
  } // if (instanceHSMEnabled)
  // [END] CR-482
%>
      <%= formMgr.getFormInstanceAsInputField("LogonForm", addlParms) %>

      <div class="instruction formItem">
        <%= resMgr.getText("Login.Instructions",TradePortalConstants.TEXT_BUNDLE) %>
      </div>

      <%= widgetFactory.createTextField( "loginId", "Login.LoginId",
                                         loginId, "32", false, false, false, "AutoComplete=\"off\"", "", "" ) %>

      <%--note: password width is overridden to be same as login width--%>
      
      <%= widgetFactory.createTextField( "password", "Login.Password",
                                         "", "30", false, false, false, "type=\"password\" AutoComplete=\"off\" width=\"32\"", "", "" ) %>
      
      <%--cquinton 2/27/2013 make of type button rather than submit to avoid incorrect/duplicate submits--%>
      <button id="loginButton" data-dojo-type="dijit.form.Button"
              type="button" class="formItem" >
        <%=resMgr.getText("common.LoginText",TradePortalConstants.TEXT_BUNDLE)%>
      </button>

    </div>

  </div>

</form>

<%
   formMgr.storeInDocCache("default.doc",new DocumentHandler());
%>

  </div>
</div> 

<%	// [BEGIN] CR-817 Rel8.2.0.0 JKOK
	String footerHtmlUri = "/themes/"+brandingDirectory+"/html/EnterPassword_"+resMgr.getResourceLocale()+".html";
	URL fileURL = pageContext.getServletContext().getResource(footerHtmlUri);
	if(fileURL != null)
	{
%>
<jsp:include page="<%=footerHtmlUri%>" flush="true" />
<%
	}
%>
<%-- // [END] CR-817 Rel8.2.0.0 JKOK --%>
<script src="<%=userSession.getdojoJsPath()%>/dojoconfig.js?<%= version %>"></script>
<script src='<%=userSession.getdojoJsPath()%>/dojo/dojo.js?<%= version %>' data-dojo-config="async: 1" ></script>
<script>
require(["dojo/parser", "dojo/dom-class",
         "dojo/query", "dojo/dom","dojo/NodeList-dom",
         "dijit/form/Form","dijit/form/Button",
         "dijit/form/TextBox","dijit/form/ValidationTextBox",
	     "t360/widget/Tooltip"], function(parser, domClass, query,dom){
	
	parser.parse(dom.byId("mainPageContent"));
       domClass.add(document.body, 'loaded');
});

  
</script>
<%-- pmitnala - Changes Begin-Help link is not working without include of common.js. Hence this change --%>
<script src="<%=userSession.getdojoJsPath()%>/common.js"></script>
<%-- pmitnala - Changes End --%>
<%
  // Include javascript functions for encryption
  if (instanceHSMEnabled) {
%>
<script src="<%=userSession.getdojoJsPath()%>/crypto/pidcrypt.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/crypto/pidcrypt_util.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/crypto/asn1.js?<%= version %>"></script><%-- needed for parsing decrypted rsa certificate --%>
<script src="<%=userSession.getdojoJsPath()%>/crypto/jsbn.js?<%= version %>"></script><%-- needed for rsa math operations --%>
<script src="<%=userSession.getdojoJsPath()%>/crypto/rng.js?<%= version %>"></script><%-- needed for rsa key generation --%>
<script src="<%=userSession.getdojoJsPath()%>/crypto/prng4.js?<%= version %>"></script><%-- needed for rsa key generation --%>
<script src="<%=userSession.getdojoJsPath()%>/crypto/rsa.js?<%= version %>"></script><%-- needed for rsa en-/decryption --%>
<script src="<%=userSession.getdojoJsPath()%>/crypto/md5.js?<%= version %>"></script><%-- needed for key and iv generation --%>
<script src="<%=userSession.getdojoJsPath()%>/crypto/aes_core.js?<%= version %>"></script><%-- needed block en-/decryption --%>
<script src="<%=userSession.getdojoJsPath()%>/crypto/aes_cbc.js?<%= version %>"></script><%-- needed for cbc mode --%>
<script src="<%=userSession.getdojoJsPath()%>/crypto/sha256.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/crypto/tpprotect.js?<%= version %>"></script><%-- needed for cbc mode --%> 
<%
  } // if (instanceHSMEnabled)
%>

<script>

  var local = {};
<%--  
//AiA IR#T36000016170 - eventState will be used to prevent double submission within same request session (was happening in ie9)
--%>
var eventState=true;
  <%--todo: this function seems like something that should be on the server instead of in jsp--%>
<%
  if (instanceHSMEnabled) {
%>
  function encryptHsmFields() {
    var public_key = '-----BEGIN PUBLIC KEY-----\n<%= HSMEncryptDecrypt.getPublicKey() %>\n-----END PUBLIC KEY-----';
    var iv = '<%= HSMEncryptDecrypt.getNewIV() %>';
    var protector = new TPProtect();
    document.LogonForm.encryptedLoginId.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.LogonForm.loginId.value});
    document.LogonForm.encryptedPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.LogonForm.password.value});
    var passwordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password: document.LogonForm.password.value, challenge: iv});
    document.LogonForm.protectedPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: passwordMac});
    document.LogonForm.loginId.value = "";
    document.LogonForm.password.value = "";
  }
<%
  } // if (instanceHSMEnabled)
%>

  require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
      function(registry, on, common, ready) {

    <%--cquinton 2/27/2013 refactor common login logic--%>
    local.login = function() {
        if (isActive =='Y') {
        	var prevPage = '<%=formMgr.getNavigateToPage()%>';
        	if (prevPage == undefined || prevPage == null){
        		prevPage ='';
        	}
            var cTime = (new Date()).getTime();
            var field = document.getElementById("cTime");
			if (field){
				document.getElementById("cTime").value= (new Date(cTime)).getTime();
				document.getElementById("cTime").text= (new Date(cTime)).getTime();
			}
			var field1 = document.getElementById("prevPage");
			if (field1){
				document.getElementById("prevPage").value= prevPage;
				document.getElementById("prevPage").text= prevPage;
			}
        }
        var branding = '<%=((StringFunction.escapeQuotesforJS(brandingDirectory) == null) ? TradePortalConstants.BLANK : StringFunction.escapeQuotesforJS(brandingDirectory))%>';
        if (branding.toUpperCase().indexOf('ANZ') != -1){
	        var prevPage = '<%=formMgr.getNavigateToPage()%>';
	    	if (prevPage == undefined || prevPage == null){
	    		prevPage ='';
	    	}
	    	var field1 = document.getElementById("prevPage");
			if (field1){
				document.getElementById("prevPage").value= prevPage;
				document.getElementById("prevPage").text= prevPage;
			}
	        var cTime = (new Date()).getTime();
	        var field = document.getElementById("cTime");
			if (field){
				document.getElementById("cTime").value= (new Date()).getTime();
				document.getElementById("cTime").text= (new Date(cTime)).getTime();
			}
        }
    	common.setButtonPressed('<%= TradePortalConstants.BUTTON_LOGIN %>','0');
<%
  if (instanceHSMEnabled) {
%>
      encryptHsmFields();
<%
  } // if (instanceHSMEnabled)
%>
  registry.byId("LogonForm").submit(); 
  <%-- AiA IR#T36000016170 - Return false after submit --%>
  return false;
    };

    local.loginOnKeyPress = function(event) {

    	<%-- IR#T36000016170 - eventState assisting in preventing double submission  (was happening esp in ie9)  --%>
    	if (eventState && event && event.keyCode == 13) {

    		eventState = false;       
    		local.login();
      }
    };

    ready(function() {

      <%--set focus--%>
<%
  if ( loginId!=null && loginId.length()>0 ) {
%>
      var focusField = registry.byId("password");
<%
  }
  else {
%>
      var focusField = registry.byId("loginId");
<%
  }
%>
      focusField.focus();

      <%--register event handlers--%>
      registry.byId("loginButton").on("click", function() {
        local.login();
      });
      <%--for ie, we need an event handler for enter--%>
      <%--cquinton 2/27/2013 move handler to text boxes rather than
          the whole form due to manual button click issues in IE--%>
      registry.byId("loginId").on("keyPress", function(event) {
        local.loginOnKeyPress(event);
      });
      registry.byId("password").on("keyPress", function(event) {
        local.loginOnKeyPress(event);
      });

    });
  });
</script>

</body>
</html>
