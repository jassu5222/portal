<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.frame.*,com.ams.tradeportal.mediator.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session" />
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session" />
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session" />


<%

	
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String version = resMgr.getText("version", "TradePortalVersion");
  if (version.equals("version")) {
    version = resMgr.getText("Footer.UnknownVersion", 
                             TradePortalConstants.TEXT_BUNDLE);
  }
//M Eerupula 03/21/2013 Rel 8.2 T3600001495 version is encrypted consistently to fix the URL difference for cache busting i.e.,the version is same on all included pages 
   // version = EncryptDecrypt.bytesToBase64String(version.getBytes()); 
 	 //RPasupulati geting first 4 char's from encripted version String IR no T36000017311 Starts.
 	 version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey());
    version = version.substring(0, 4);
    //RPasupulati geting first 4 char's from encripted version String IR no T36000017311 ENDs.
	// [START] CR-482  
	// Check if this TradePortal instance is configured for HSM encryption / decryption
	boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();

	// [END] CR-482

	formMgr.setCurrPage("EnterPassword2FA");

	DocumentHandler doc = formMgr.getFromDocCache();
	Hashtable addlParms = new Hashtable();
	String brandingDirectory;
	String organizationId;
	String loginId = "";
	DocumentHandler brandedButtonDoc;
	String brandedButtonInd = null;
	String brandedButtonDirectory = null;



	// [START] CR-482
	if (instanceHSMEnabled
			&& (doc.getAttribute("/Out/User/loginId") != null)) {
		doc.setAttribute("/In/User/loginId",
				doc.getAttribute("/Out/User/loginId"));
	}
	// [END] CR-482

	if ((doc != null) && (doc.getAttribute("/In/User/loginId") != null)) {


		// Get page parameters from the XML that was returned from the mediator
		brandingDirectory = doc.getAttribute("/In/brandingDirectory");
		organizationId = doc.getAttribute("/In/organizationId");
		loginId = doc.getAttribute("/In/User/loginId");
		brandedButtonInd = doc.getAttribute("/In/brandbutton");

		// Determine which login attempt this is (the first, the second, the third, etc)
		int loginAttemptNumber;

		try {
			loginAttemptNumber = Integer.parseInt(doc.getAttribute("/In/loginAttemptNumber2FA"));

		} catch (Exception e) {
			loginAttemptNumber = 0;
		}

		// If the logon failed, and they have already tried to log in the maximum
		// number of times, redirect them to another page
		// DK IR T36000027938 Rel8.4 BTM 05/15/2014 starts - user should not be locked outafter 3 attempts for RSA validation
		/*if (loginAttemptNumber == SecurityRules.getLoginAttemptsBeforeBeingRedirected() 
				&& !"EnterPassword2FA".equals(doc.getAttribute("/Out/LogonStatus"))) {

			StringBuffer parmsFromLogon = new StringBuffer();
			parmsFromLogon.append("?branding=");
			parmsFromLogon.append(brandingDirectory);
			parmsFromLogon.append("&organization=");
			parmsFromLogon.append(organizationId);
			parmsFromLogon.append("&locale=");
			parmsFromLogon.append(resMgr.getResourceLocale());

			String tooManyLogonsPage = NavigationManager.getNavMan().getPhysicalPage("TooManyInvalidLogonAttempts", request);*/


%>
	<%-- 	<jsp:forward page='<%=tooManyLogonsPage + parmsFromLogon%>' />--%>
	   <%
	  // 	}

	   		// Increment the login attempt number
	   		addlParms.put("loginAttemptNumber2FA",
	   				String.valueOf(loginAttemptNumber + 1));
	   		if (doc.getAttribute("/Out/2FAserverName") != null) {
	   			addlParms.put("2FAserverName",
	   					doc.getAttribute("/Out/2FAserverName"));
	   		}
	   		if (doc.getAttribute("/Out/2FAsharedSecret") != null) {
	   			addlParms.put("2FAsharedSecret",
	   					doc.getAttribute("/Out/2FAsharedSecret"));
	   		}
	   	} else {
	   		// Get page parameters from the URL parameters
	   		brandingDirectory = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
	   		organizationId = request.getParameter("organization");
	   		brandedButtonInd = request.getParameter("brandbutton");

	   		// This is the first time in a row that they are logging in, so set count to 1
	   		addlParms.put("loginAttemptNumber2FA", "1");

	   		formMgr.storeInDocCache("default.doc", new DocumentHandler());
	   	}

	   	// Retrieve brand_button_ind from the request parameter.  
	   	// If it is null, get from client bank
	   	if (brandedButtonInd == null) {
	   		
	   		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
	   		String sqlQuery = "select brand_button_ind from client_bank where otl_id = ?";
	   		brandedButtonDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{organizationId});

	   		if (brandedButtonDoc != null) {
	   			brandedButtonDoc = (DocumentHandler) brandedButtonDoc.getFragments("/ResultSetRecord").elementAt(0);
	   			brandedButtonInd = brandedButtonDoc.getAttribute("/BRAND_BUTTON_IND");
	   		}
	   	}
	   	if (brandedButtonInd != null && brandedButtonInd.equals(TradePortalConstants.INDICATOR_YES)) {




	   		brandedButtonDirectory = brandingDirectory;
	   	}

	   	// This flag indicates that the password must be validated.
	   	addlParms.put("auth", TradePortalConstants.AUTH_2FA);
	   	addlParms.put("brandingDirectory", brandingDirectory);
	   	addlParms.put("organizationId", organizationId);
	   	addlParms.put("locale", resMgr.getResourceLocale());
	   	addlParms.put("loginId", loginId);
	   	addlParms.put("brandbutton", brandedButtonInd);
	   	if(StringFunction.isBlank(doc.getAttribute("/Out/LogonStatus")))
	   		addlParms.put("LogonStatus", "EnterPassword2FA"); // DK CR-804 Rel8.4 01/03/2014
	   	else
	   		addlParms.put("LogonStatus",doc.getAttribute("/Out/LogonStatus"));

	   %>


<%-- Body tag included as part of common header --%>





















<jsp:include page="/common/Header.jsp">
   <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />

   <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
</jsp:include>







































<div class="pageMainNoSidebar">
  <div id="mainPageContent" class="pageContent">

		<jsp:include page="/common/PageHeader.jsp">
			<jsp:param name="titleKey" value="Login.2FA" />
			<jsp:param name="helpUrl" value="/customer/login.htm" />
		</jsp:include>

		<form id="LogonForm" name="LogonForm" method="POST" data-dojo-type="dijit.form.Form"
			action="<%=formMgr.getSubmitAction(response)%>">

  <jsp:include page="/common/ErrorSection.jsp" />
			<div class="formContentNoSidebar">
				<div class="padded">
					<input type=hidden value="" name=buttonName>
					<%
						// [START] CR-482
						if (instanceHSMEnabled) {
					%>
					<input type=hidden name="encryptedPassword2FA" value="">
					<%
						} // if (instanceHSMEnabled)
							// [END] CR-482
					%>
					<%=formMgr.getFormInstanceAsInputField("LogonForm",
					addlParms)%>

					<%=OnlineHelp.createContextSensitiveLink(
					"/customer/2fa_auth.htm", resMgr, brandingDirectory)%>

					<div class="instruction formItem">
						<%=resMgr.getText("Login.2FAInstructions",
					TradePortalConstants.TEXT_BUNDLE)%>
					</div>
				<%--  MEerupula Rel 8.3 IR-T36000020748 2FA token Passcode field length increased from 6 to 30  --%>
					<%=widgetFactory
					.createTextField(
							"password2FA",
							"",
							"",
							"30",
							false,
							false,
							false,
							"type=\"password\" AutoComplete=\"off\" width=\"26\"",
							"", "")%>
							
					<button id="loginButton" data-dojo-type="dijit.form.Button"
						type="submit" class="formItem" onclick="return clickSubmit()">
						<%=resMgr.getText("common.LoginText",
					TradePortalConstants.TEXT_BUNDLE)%>
					</button>
				</div>
			</div>
		</form>

		<%
			formMgr.storeInDocCache("default.doc", new DocumentHandler());
		%>
	</div>
</div>
<script src="<%=userSession.getdojoJsPath()%>/dojoconfig.js?<%= version %>"></script>
<script src='<%=userSession.getdojoJsPath()%>/dojo/dojo.js?<%= version %>' data-dojo-config="async: 1" ></script>
<script>
require(["dojo/parser", "dojo/dom-class",
         "dojo/query", "dojo/dom","dojo/NodeList-dom",
         "dijit/form/Form","dijit/form/Button",
         "dijit/form/TextBox","dijit/form/ValidationTextBox",
	     "t360/widget/Tooltip"], function(parser, domClass, query,dom){
	
	parser.parse(dom.byId("mainPageContent"));
       domClass.add(document.body, 'loaded');
});
</script>
<%-- pmitnala - Changes Begin-Help link is not working without include of common.js. Hence this change --%>
<script src="<%=userSession.getdojoJsPath()%>/common.js"></script>
<%-- pmitnala - Changes End --%>
<%
	// [START] CR-482
	// Include javascript functions for encryption
	if (instanceHSMEnabled) {
%>
<script src="js/crypto/pidcrypt.js?<%= version %>"></script>
<script src="js/crypto/pidcrypt_util.js?<%= version %>"></script>
<script src="js/crypto/asn1.js?<%= version %>"></script><%-- needed for parsing decrypted rsa certificate --%>
<script src="js/crypto/jsbn.js?<%= version %>"></script><%-- needed for rsa math operations --%>
<script src="js/crypto/rng.js?<%= version %>"></script><%-- needed for rsa key generation --%>
<script src="js/crypto/prng4.js?<%= version %>"></script><%-- needed for rsa key generation --%>
<script src="js/crypto/rsa.js?<%= version %>"></script><%-- needed for rsa en-/decryption --%>
<script src="js/crypto/md5.js?<%= version %>"></script><%-- needed for key and iv generation --%>
<script src="js/crypto/aes_core.js?<%= version %>"></script><%-- needed block en-/decryption --%>
<script src="js/crypto/aes_cbc.js?<%= version %>"></script><%-- needed for cbc mode --%>
<script src="js/crypto/sha256.js?<%= version %>"></script>
<script src="js/crypto/tpprotect.js?<%= version %>"></script><%-- needed for cbc mode --%> 

<%
	} // if (instanceHSMEnabled)
%>


<script>
				function clickSubmit() {	
<%if (instanceHSMEnabled) {%>
         	var public_key = '-----BEGIN PUBLIC KEY-----\n<%=HSMEncryptDecrypt.getPublicKey()%>\n-----END PUBLIC KEY-----';
					var iv = '<%=HSMEncryptDecrypt.getNewIV()%>';
			    var protector = new TPProtect();
					document.LogonForm.encryptedPassword2FA.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.LogonForm.password2FA.value});
					document.LogonForm.password2FA.value = "";
<%} // if (instanceHSMEnabled)%>
				}
<%// [END] CR-482%>
   
		 
	require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
      function(registry, on, common, ready) {
    ready(function() {

      var focusField = registry.byId("password2FA");
      focusField.focus();
	 
  
   <%--for ie, we need an event handler for enter--%>
      registry.byId("LogonForm").on("keyPress", function(event) {
        if (event && event.keyCode == 13) {
<%
  if (instanceHSMEnabled) {
%>
        var public_key = '-----BEGIN PUBLIC KEY-----\n<%=HSMEncryptDecrypt.getPublicKey()%>\n-----END PUBLIC KEY-----';
		var iv = '<%=HSMEncryptDecrypt.getNewIV()%>';
		var protector = new TPProtect();
		document.LogonForm.encryptedPassword2FA.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.LogonForm.password2FA.value});
		document.LogonForm.password2FA.value = "";
<%
  } // if (instanceHSMEnabled)
%>
          registry.byId("LogonForm").submit();
        }
      });
    });
  });
		 
</script>
<%
	// [END] CR-482
%>

</body>
</html>
