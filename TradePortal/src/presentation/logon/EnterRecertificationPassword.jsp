<%--
 *   This page will present the user with certificate fields to reauthenticate against the previously provided
 *   certificate.
 *
 *     Copyright   2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
com.ams.tradeportal.common.*,com.ams.tradeportal.mediator.util.*,com.ams.tradeportal.busobj.util.*,
com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.html.*,java.util.Hashtable,java.util.StringTokenizer" %>
                 
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<%-- Naveen- For close button --%>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
   <%

	
    InstrumentWebBean instrument = (InstrumentWebBean) beanMgr.getBean("Instrument");   //Naveen- For close button
   	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   	StringBuffer cssFilename = new StringBuffer(
   			TradePortalConstants.IMAGES_PATH);
   	cssFilename.append(userSession.getBrandingDirectory());
   	cssFilename.append("/");
   	cssFilename.append(resMgr.getText("brandedImage.stylesheet",
   			TradePortalConstants.TEXT_BUNDLE));

   	String orgId = request.getParameter("organization");
   	String brandingDirectory = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
   	brandingDirectory = "";// RKAZI REL 8.3 T3600021568 making barnding  varaible blank to avoid XSS attack and use branding from userSession.
   	String locale = request.getParameter("locale");
   	String bankDate = request.getParameter("date");
   	String certData = request.getParameter("info");
   	String requestedAuthMethod = request.getParameter("auth");
   	String submitButtonName = request.getParameter("submitButtonName");
   	String physicalPage = "";
   	String loginId = "";
   	Hashtable addlParms = new Hashtable();

   	addlParms.put("brandingDirectory", brandingDirectory);
   	addlParms.put("organizationId", orgId);
   	addlParms.put("locale", locale);
   	if (!(InstrumentServices.isBlank(bankDate))) {
   		addlParms.put("date", bankDate);
   	}
   	if (!(InstrumentServices.isBlank(certData))) {
   		addlParms.put("info", certData);
   	}
   	if (!(InstrumentServices.isBlank(requestedAuthMethod))) {
   		addlParms.put("auth", requestedAuthMethod);
   	}

   	// Narayan IR-RMUK122050315 Begin 
   	session.setAttribute("submitButtonName", submitButtonName);
   	//put reAuthObjectType/Oid as parms for use in LogonMediator
   	String reAuthObjectType = request.getParameter("reAuthObjectType");
   	if (reAuthObjectType != null) {
   		addlParms.put("reAuthObjectType", reAuthObjectType);
   	}
   	//attempt to get the reAuthObjectOid.
   	//ir cnuk113043991 start - get multiple reAuthObjectOid parms
   	String allReAuthObjectOids = "";
   	int reAuthObjectIdx = 0;
   	String reAuthObjectOid = request.getParameter("reAuthObjectOid"
   			+ reAuthObjectIdx);
   	if (reAuthObjectOid != null) {
   		//if found, parent is a list
   		do {
   			//the reAuthObjectOid here is the checkbox value, which is encrypted and may have other data than
   			//what we are looking for
   			//for pending transactions, we want the 1st value = transaction_oid
   			//for receivables payment matches, we want the 1st value = pay_remit_oid
   			//for invoices, we want the 1st value = invoice_oid
   			String checkBoxValue = EncryptDecrypt
   					.encryptStringUsingTripleDes(reAuthObjectOid, userSession.getSecretKey());
   			StringTokenizer parser = new StringTokenizer(checkBoxValue,
   					"/");
   			if (parser.hasMoreTokens()) {
   				//add to the list of reAuthObjectOids - separate by !
   				if (reAuthObjectIdx == 0) {
   					allReAuthObjectOids = parser.nextToken();
   				} else {
   					allReAuthObjectOids += "!" + parser.nextToken();
   				}
   			}
   			reAuthObjectIdx++;
   		} while ((reAuthObjectOid = request
   				.getParameter("reAuthObjectOid" + reAuthObjectIdx)) != null);
   	} else {
   		//ir CAUK113043267 start
   		//parent is a detail
   		//use the parentNi parm to get the oid from secure parms
   		String parentNi = request.getParameter("parentNi");
   		//out.println("parentNi:"+parentNi);
   		if (parentNi != null) {
   			//try to get from transaction detail page
   			reAuthObjectOid = formMgr.getSecurePageParm(parentNi,
   					"transaction_oid");
   			if (reAuthObjectOid == null) {
   				//try to get from pay remit detail page
   				reAuthObjectOid = formMgr.getSecurePageParm(parentNi,
   						"pay_remit_oid");
   			}
   		}
   		allReAuthObjectOids = reAuthObjectOid;
   	}
   	if (allReAuthObjectOids != null && allReAuthObjectOids.length() > 0) {
   		addlParms.put("reAuthObjectOids", allReAuthObjectOids);
   	}
   	//ir cnuk113043991 end

   	//Narayan IR-RMUK122050315 End
   	// Forward the user to the CompleteRecertification page to close the window, if a reponse is not received.
   	// Subtract 15 seconds from the timeout period.
   	// This way, the browser will forward to the timeout page before the session
   	// is actually killed by the server
   	int timeoutInSeconds = SecurityRules.getTimeOutPeriodInSeconds() - 15;

   	session.setAttribute("recertificationSuccessful",
   			TradePortalConstants.INDICATOR_NO); //IAZ CM-451 03/12/09 Add
   %>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="displayHeader" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
   <jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>

<script>
    setTimeout("document.location.href='<%=formMgr.getLinkAsUrl("goToCompleteRecertification",
					response)%>'", <%=timeoutInSeconds%>000);
         
<%--cquinton 5/1/2013 Rel 8.1.0.6 ir#16256 make behavior like login page so submit works--%>
</script>

<div class="dialogContent midwidth">

  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="Recertification.Title" />
  </jsp:include>

  <form id="RecertificationForm" name="RecertificationForm" method="POST"
	action="<%=formMgr.getSubmitAction(response)%>"
	data-dojo-type="dijit.form.Form">

	<%=formMgr.getFormInstanceAsInputField(
		"RecertificationForm", addlParms)%>

			
    <div class="formContentNoSidebar">
      <div class="padded">
					<div class="instruction formItem">
						<%=resMgr.getText("Recertification.Instructions",
					TradePortalConstants.TEXT_BUNDLE)%>
					</div>
					<%=widgetFactory
					.createTextField(
							"loginId",
							"Recertification.LoginId",
							loginId,
							"32",
							false,
							false,
							false,
							"AutoComplete=\"off\"",
							"", "")%>

					<%=widgetFactory
					.createTextField(
							"password",
							"Recertification.Password",
							"",
							"30",
							false,
							false,
							false,
							"type=\"password\" AutoComplete=\"off\" width=\"32\"",
							"", "")%>

        <button id="loginButton" name="loginButton" data-dojo-type="dijit.form.Button" type="button" class="formItem">
          <%=resMgr.getText("GuaranteeIssue.SubmitText",TradePortalConstants.TEXT_BUNDLE)%>
        </button>
        <button id="closeButton" name="closeButton" data-dojo-type="dijit.form.Button" type="button" class="formItem">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
        </button>	
      </div>
    </div>
  </form>
		<%
			formMgr.storeInDocCache("default.doc", new DocumentHandler());
		%>
	</div>
<jsp:include page="/common/Footer.jsp">
	<jsp:param name="displayFooter"
		value="<%=TradePortalConstants.INDICATOR_NO%>" />
	<jsp:param name="minimalHeaderFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>
<script>
  document.title = '<%=resMgr.getText("Recertification.Title",TradePortalConstants.TEXT_BUNDLE)%>';

  var local = {};

  require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
      function(registry, on, common, ready) {

    <%--cquinton 2/27/2013 refactor common login logic--%>
    local.authenticate = function() {
      common.setButtonPressed('<%= TradePortalConstants.BUTTON_LOGIN %>','0');
      registry.byId("RecertificationForm").submit();
    };

    local.authenticateOnKeyPress = function(event) {
      if (event && event.keyCode == 13) {
        local.authenticate();
      }
    };

    ready(function() {

      <%--set focus--%>
      var focusField = registry.byId("loginId");
      focusField.focus();

      <%--register event handlers--%>
      registry.byId("loginButton").on("click", function() {
        local.authenticate();
      });
      registry.byId("closeButton").on("click", function() {
        window.close();
      });
      <%--for ie, we need an event handler for enter--%>
      <%--cquinton 2/27/2013 move handler to text boxes rather than
          the whole form due to manual button click issues in IE--%>
      registry.byId("loginId").on("keyPress", function(event) {
        local.authenticateOnKeyPress(event);
      });
      registry.byId("password").on("keyPress", function(event) {
        local.authenticateOnKeyPress(event);
      });

    });
  });
</script>
</body>
</html>
