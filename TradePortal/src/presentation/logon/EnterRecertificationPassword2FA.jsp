<%--
 *   This page will present the user with 2FA Device Token Code fields to reauthenticate.
 *
 *     Copyright   2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.mediator.util.*, com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.busobj.webbean.*,
		 com.ams.tradeportal.html.*,java.util.*" %>
                 
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>

<%

	
   // [START] CR-482  
   // Check if this TradePortal instance is configured for HSM encryption / decryption
   boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
   // [END] CR-482

   StringBuffer cssFilename = new StringBuffer(TradePortalConstants.IMAGES_PATH);
   cssFilename.append(userSession.getBrandingDirectory());
   cssFilename.append("/");
   cssFilename.append(resMgr.getText("brandedImage.stylesheet", TradePortalConstants.TEXT_BUNDLE));
  
   String orgId               = request.getParameter("organization");
   String brandingDirectory   = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
   String locale              = request.getParameter("locale");
   String bankDate            = request.getParameter("date");
   String certData            = request.getParameter("info");
   //cr498 - add submitButtonName for dynamic form submission
   String submitButtonName    = request.getParameter("submitButtonName");
   //cr498 end
   String physicalPage        = "";
   Hashtable addlParms = new Hashtable();

   addlParms.put("brandingDirectory", brandingDirectory);
   addlParms.put("organizationId", orgId);
   addlParms.put("locale", locale); 
   addlParms.put("LogonStatus", "EnterPassword2FA"); // DK IR T36000024234 Rel8.4 02/07/2014
   if (!(InstrumentServices.isBlank(bankDate))) {
       addlParms.put("date", bankDate);
   }   
   if (!(InstrumentServices.isBlank(certData))) {
       addlParms.put("info", certData);
   } 
   addlParms.put("auth", TradePortalConstants.AUTH_2FA);
   addlParms.put("loginId", userSession.getUserId());
   //ir cnuk113043991 recertificationInd set to multiple below for list pages
   //addlParms.put("RecertificationIndicator", TradePortalConstants.INDICATOR_YES);
   String recertificationInd = TradePortalConstants.INDICATOR_YES; // default detail

   //cr498 - set submitButtonName in session for CompleteRecertification.jsp
   session.setAttribute("submitButtonName", submitButtonName);
   //put reAuthObjectType/Oid as parms for use in LogonMediator
   String reAuthObjectType    = request.getParameter("reAuthObjectType");
   if ( reAuthObjectType != null ) {
      addlParms.put("reAuthObjectType", reAuthObjectType);
   }
   //attempt to get the reAuthObjectOid.
   //ir cnuk113043991 start - get multiple reAuthObjectOid parms
   String allReAuthObjectOids = "";
   int reAuthObjectIdx = 0;
  // String reAuthObjectOid = request.getParameter("reAuthObjectOid"+reAuthObjectIdx);
   String reAuthObjectOid = request.getParameter("checkbox"+reAuthObjectIdx);
   if ( reAuthObjectOid != null ) {
      //if found, parent is a list
      recertificationInd = TradePortalConstants.INDICATOR_MULTIPLE;
      do {
         //the reAuthObjectOid here is the checkbox value, which is encrypted and may have other data than
         //what we are looking for
         //for pending transactions, we want the 1st value = transaction_oid
         //for receivables payment matches, we want the 1st value = pay_remit_oid
         //for invoices, we want the 1st value = invoice_oid
         String checkBoxValue = EncryptDecrypt.decryptStringUsingTripleDes(reAuthObjectOid, userSession.getSecretKey());
         StringTokenizer parser = new StringTokenizer(checkBoxValue, "/");
         if (parser.hasMoreTokens()){
            //add to the list of reAuthObjectOids - separate by !
	    if ( reAuthObjectIdx == 0 ) {
               allReAuthObjectOids = parser.nextToken();
	    } else {
	       allReAuthObjectOids += "!" + parser.nextToken();
            }
         }
         reAuthObjectIdx++;
      } while ( ( reAuthObjectOid = request.getParameter("checkbox"+reAuthObjectIdx) ) != null );
   } else {
      //ir CAUK113043267 start
      //parent is a detail
      //use the parentNi parm to get the oid from secure parms
      String parentNi = request.getParameter("parentNi");
      //out.println("parentNi:"+parentNi);
      if ( parentNi != null ) {
         //try to get from transaction detail page
         reAuthObjectOid = formMgr.getSecurePageParm(parentNi,"transaction_oid");
         if (reAuthObjectOid==null) {
            //try to get from pay remit detail page
            reAuthObjectOid = formMgr.getSecurePageParm(parentNi,"pay_remit_oid");
         }
      }
      allReAuthObjectOids = reAuthObjectOid;
      //ir CAUK113043267 end
   }
   if ( allReAuthObjectOids != null && allReAuthObjectOids.length()>0 ) {
      addlParms.put("reAuthObjectOids", allReAuthObjectOids);
   }
   addlParms.put("RecertificationIndicator", recertificationInd);
   //ir cnuk113043991 end
   //out.println("submitButton:"+submitButtonName);
   //out.println("reauthtype/objs:"+reAuthObjectType+allReAuthObjectOids);
   //cr498 end
   
   // Forward the user to the CompleteRecertification page to close the window, if a reponse is not received.
  // Subtract 15 seconds from the timeout period.
  // This way, the browser will forward to the timeout page before the session
  // is actually killed by the server
   int timeoutInSeconds = SecurityRules.getTimeOutPeriodInSeconds() - 15;
   
   session.setAttribute("recertificationSuccessful", TradePortalConstants.INDICATOR_NO); //IAZ CM-451 03/12/09 Add
	       
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="displayHeader" value="<%=TradePortalConstants.INDICATOR_NO%>" />
	<jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
   <jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>
<%
  if (instanceHSMEnabled)
  {
    //cquinton 5/15/2013 ir#17092 update url so its actually pulled in
%>
<script src="../js/crypto/pidcrypt.js"></script>
<script src="../js/crypto/pidcrypt_util.js"></script>
<script src="../js/crypto/asn1.js"></script><%--needed for parsing decrypted rsa certificate--%>
<script src="../js/crypto/jsbn.js"></script><%--needed for rsa math operations--%>
<script src="../js/crypto/rng.js"></script><%--needed for rsa key generation--%>
<script src="../js/crypto/prng4.js"></script><%--needed for rsa key generation--%>
<script src="../js/crypto/rsa.js"></script><%--needed for rsa en-/decryption--%>
<script src="../js/crypto/md5.js"></script><%--needed for key and iv generation--%>
<script src="../js/crypto/aes_core.js"></script><%--needed block en-/decryption--%>
<script src="../js/crypto/aes_cbc.js"></script><%--needed for cbc mode--%>
<script src="../js/crypto/sha256.js"></script>
<script src="../js/crypto/tpprotect.js"></script><%--needed for cbc mode--%> 
<%
  } // if (instanceHSMEnabled)
%>
<%
	// [END] CR-482
  //cquinton 5/1/2013 Rel 8.1.0.6 ir#16256
  //also populate the window title
  String pageHeaderKey = "Login.2FA";
%>

<div class="dialogContent midwidth">

<jsp:include page="/common/PageHeader.jsp">
  <jsp:param name="titleKey" value="<%= pageHeaderKey %>"/>
</jsp:include>

  <form id="RecertificationForm" name="RecertificationForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>"
        data-dojo-type="dijit.form.Form">
   
    <%= formMgr.getFormInstanceAsInputField("RecertificationForm", addlParms) %>
 
    <div class="formContentNoSidebar">

<%
  if (instanceHSMEnabled) {
%>
      <input type=hidden name="encryptedPassword2FA" value="">
<%
  } // if (instanceHSMEnabled)
%>

      <br/>
      
      <div class="formItem">
        <%= resMgr.getText("Login.2FAInstructions",TradePortalConstants.TEXT_BUNDLE) %> 
      </div>
      <br/>

      <%--this dummy field resolves an issue with internet explorer submitting the form
          on enter.  DO NOT REMOVE.  by including a 2nd text field the submission does not occur
          and the page correctly uses the dojo onkeypress event handler instead!--%>
      <%= widgetFactory.createTextField( "dummy", "", "", "1", false, false, false,
            "style=\"display:none;\"", "", "" ) %>

      <%= widgetFactory.createTextField( "password2FA", "", "", "30", false, false, false,
            "type=\"password\" AutoComplete=\"off\" width=\"32\"", "", "" ) %>

      <button id="LoginButton" name="LoginButton" data-dojo-type="dijit.form.Button"
              type="button" class="formItem" >
        <%=resMgr.getText("GuaranteeIssue.SubmitText",TradePortalConstants.TEXT_BUNDLE)%>
      </button>

    </div>
  </form>

<%
   formMgr.storeInDocCache("default.doc",new DocumentHandler());
%>
</div>
<%--cquinton 5/1/2013 Rel 8.1.0.6 ir#16256 start
    remove footer display--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
</jsp:include>			

<script>
  document.title = '<%=resMgr.getText(pageHeaderKey,TradePortalConstants.TEXT_BUNDLE)%>';

  setTimeout("document.location.href='<%=formMgr.getLinkAsUrl("goToCompleteRecertification", response)%>'", <%=timeoutInSeconds%>000);

  var local = {};

<%
  if (instanceHSMEnabled) {
%>
  function encryptHsmFields() {
    if (typeof(TPProtect) != 'undefined') {
      var public_key = '-----BEGIN PUBLIC KEY-----\n<%= HSMEncryptDecrypt.getPublicKey() %>\n-----END PUBLIC KEY-----';
      var iv = '<%= HSMEncryptDecrypt.getNewIV() %>';
      var protector = new TPProtect();
      RecertificationForm.encryptedPassword2FA.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: RecertificationForm.password2FA.value});
      RecertificationForm.password2FA.value = "";
    }
  }
<%
  } // if (instanceHSMEnabled)
%>

  require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
      function(registry, on, common, ready) {

    <%--cquinton 2/27/2013 refactor common login logic--%>
    local.authenticate = function() {
      common.setButtonPressed('<%= TradePortalConstants.BUTTON_LOGIN %>','0');
<%
  if (instanceHSMEnabled) {
%>
      encryptHsmFields();
<%
  } // if (instanceHSMEnabled)
%>
      registry.byId("RecertificationForm").submit();
    };

    local.authenticateOnKeyPress = function(event) {
      if (event && event.keyCode == 13) {
        local.authenticate();
      }
    };

    ready(function() {

      <%--set focus--%>
      var focusField = registry.byId("password2FA");
      focusField.focus();

      <%--register event handlers--%>
      registry.byId("LoginButton").on("click", function() {
        local.authenticate();
      });
      <%--for ie, we need an event handler for enter--%>
      <%--cquinton 2/27/2013 move handler to text boxes rather than
          the whole form due to manual button click issues in IE--%>
      registry.byId("password2FA").on("keyPress", function(event) {
        local.authenticateOnKeyPress(event);
      });

    });
  });
</script>
<%--cquinton 5/1/2013 Rel 8.1.0.6 ir#16256 end --%>
 </body>
</html>
