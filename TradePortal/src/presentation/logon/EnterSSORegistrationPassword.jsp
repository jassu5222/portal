<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.frame.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"                   scope="session"/>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"                   scope="session" />
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"              scope="session" />
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session" />

<%

	
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	 String version = resMgr.getText("version", "TradePortalVersion");
    if (version.equals("version")) {
      version = resMgr.getText("Footer.UnknownVersion", 
                             TradePortalConstants.TEXT_BUNDLE);
    }
  //M Eerupula 03/21/2013 Rel 8.2 T3600001495  version is encrypted consistently using Base64 encoding to fix the URL difference for cache busting, means the version is same on all included pages 
    //version = EncryptDecrypt.bytesToBase64String(version.getBytes()); 
  	//RPasupulati geting first 4 char's from encripted version String IR no T36000017311 Starts.
    version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey());
  	version = version.substring(0, 4);
    //RPasupulati geting first 4 char's from encripted version String IR no T36000017311 ENDs.
	formMgr.setCurrPage("EnterSSORegistrationPassword");

	DocumentHandler doc = formMgr.getFromDocCache();
	Hashtable addlParms = new Hashtable();
	String brandingDirectory;
	String organizationId;
	String loginId = "";
	String brandedButtonInd = null;
	String brandedButtonDirectory = null;
	String registrationHeader = "";
	String registrationText = "";


	if ((doc != null) && (doc.getAttribute("/In/User/loginId") != null)) {

		// Get page parameters from the XML that was returned from the mediator
		brandingDirectory = doc.getAttribute("/In/brandingDirectory");
		organizationId = doc.getAttribute("/In/organizationId");
		loginId = doc.getAttribute("/In/User/loginId");

		if (doc.getAttribute("/Out/User/loginId") != null) {
			loginId = doc.getAttribute("/Out/User/loginId");
		}

		loginId = StringFunction.xssCharsToHtml(loginId);

		// Determine which login attempt this is (the first, the second, the third, etc)
		int loginAttemptNumber;
		try {
			loginAttemptNumber = Integer.parseInt(doc
					.getAttribute("/In/loginAttemptNumber"));
		} catch (Exception e) {
			loginAttemptNumber = 1;
		}

		// If the logon failed, and they have already tried to log in the maximum
		// number of times, redirect them to another page
		if (loginAttemptNumber == SecurityRules
				.getLoginAttemptsBeforeBeingRedirected()) {
			StringBuffer parmsFromLogon = new StringBuffer();
			parmsFromLogon.append("?branding=");
			parmsFromLogon.append(brandingDirectory);
			parmsFromLogon.append("&organization=");
			parmsFromLogon.append(organizationId);
			parmsFromLogon.append("&locale=");
			parmsFromLogon.append(resMgr.getResourceLocale());

			String tooManyLogonsPage = NavigationManager.getNavMan()
					.getPhysicalPage("TooManyInvalidLogonAttempts",
							request);
%>
    	  <jsp:forward page='<%=tooManyLogonsPage + parmsFromLogon%>' />
    	  <%
    	  	}

    	  		// Increment the login attempt number
    	  		addlParms.put("loginAttemptNumber",
    	  				String.valueOf(loginAttemptNumber + 1));
    	  	} else {
    	  		// Get page parameters from the URL parameters
    	  		brandingDirectory = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
    	  		organizationId = request.getParameter("organization");

    	  		// This is the first time in a row that they are logging in, so set count to 1
    	  		addlParms.put("loginAttemptNumber", "1");
    	  		formMgr.storeInDocCache("default.doc", new DocumentHandler());
    	  	}

    	  	// Retrieve brand_button_ind from the request parameter
    	  	brandedButtonInd = request.getParameter("brandbutton");
    	  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    	  	// Retrieve registration header and text from client bank
    	  	String sqlQuery = "select ORGANIZATION_OID from client_bank where otl_id = ?  and activation_status = '"
    	  	+TradePortalConstants.ACTIVE+"'";

    	  	DocumentHandler clientBankDoc = DatabaseQueryBean
    	  			.getXmlResultSet(sqlQuery.toString(), false, new Object[]{organizationId});
    	  	if (clientBankDoc != null) {
    	  		clientBankDoc = (DocumentHandler) clientBankDoc.getFragments(
    	  				"/ResultSetRecord").elementAt(0);
    	  		String clientBankOid = clientBankDoc
    	  				.getAttribute("/ORGANIZATION_OID");

    	  		beanMgr.registerBean(
    	  				"com.ams.tradeportal.busobj.webbean.ClientBankWebBean",
    	  				"ClientBank");
    	  		ClientBankWebBean clientBank = (ClientBankWebBean) beanMgr
    	  				.getBean("ClientBank");
    	  		clientBank.getById(clientBankOid);

    	  		registrationHeader = clientBank.getAttribute(
    	  				"registration_header").replace("\n", "<br/>");
    	  		registrationText = clientBank.getAttribute("registration_text")
    	  				.replace("\n", "<br/>");

    	  		// If request parm is null, get brandedButtonInd from client bank
    	  		if (brandedButtonInd == null) {
    	  			brandedButtonInd = clientBank
    	  					.getAttribute("brand_button_ind");
    	  		}
    	  	}

    	  	if (TradePortalConstants.INDICATOR_YES.equals(brandedButtonInd)) {
    	  		brandedButtonDirectory = brandingDirectory;
    	  	}

    	  	// This flag indicates that the password must be validated.
    	  	addlParms.put("auth", TradePortalConstants.AUTH_SSO_REGISTRATION);
    	  	addlParms.put("brandingDirectory", brandingDirectory);
    	  	addlParms.put("organizationId", organizationId);
    	  	addlParms.put("locale", resMgr.getResourceLocale());
    	  %>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
</jsp:include>

<div class="pageMainNoSidebar">
	<div id="mainPageContent"  class="pageContent">

		<jsp:include page="/common/PageHeader.jsp">
			<jsp:param name="titleKey" value="Login.Registration" />
			<jsp:param name="helpUrl" value="/customer/login.htm" />
		</jsp:include>

		<form id="LogonForm" name="LogonForm" method="POST"
		data-dojo-type="dijit.form.Form"
			action="<%=formMgr.getSubmitAction(response)%>">
			
			<jsp:include page="/common/ErrorSection.jsp" />
			
			<div class="formContentNoSidebar">
				<div class="padded">
					<input name=buttonName type=hidden value="">
					<%=formMgr.getFormInstanceAsInputField("LogonForm",
					addlParms)%>
					<%=OnlineHelp.createContextSensitiveLink(
					"/customer/login.htm", resMgr, brandingDirectory)%>
					<div align="center">
						<%=registrationHeader%>
						<%=registrationText%>
					</div>
					<div class="instruction formItem">
						<%=resMgr.getText("Login.RegistrationInstructions",
					TradePortalConstants.TEXT_BUNDLE)%>
					</div>
					<%=widgetFactory
					.createTextField(
							"loginId",
							"Login.RegistrationLoginId",
							loginId,
							"32",
							false,
							false,
							false,
							"AutoComplete=\"off\" width=\"32\"",
							"", "")%>
					<%=widgetFactory
					.createTextField(
							"password",
							"Login.RegistrationPassword",
							"",
							"32",
							false,
							false,
							false,
							"type=\"password\" AutoComplete=\"off\" width=\"32\"",
							"", "")%>

					<button id="loginButton" data-dojo-type="dijit.form.Button"
						type="submit" class="formItem">
						<%=resMgr.getText("common.LoginText",
					TradePortalConstants.TEXT_BUNDLE)%>
					</button>			        
				</div>
			</div>
		</form>
		<%
			formMgr.storeInDocCache("default.doc", new DocumentHandler());
		%>
	</div>
</div>


<script src="<%=userSession.getdojoJsPath()%>/dojoconfig.js?<%= version %>"></script>
<script src='<%=userSession.getdojoJsPath()%>/dojo/dojo.js?<%= version %>' data-dojo-config="async: 1" ></script>
<script>
require(["dojo/parser", "dojo/dom-class",
         "dojo/query", "dojo/dom","dojo/NodeList-dom",
         "dijit/form/Form","dijit/form/Button",
         "dijit/form/TextBox","dijit/form/ValidationTextBox",
	     "t360/widget/Tooltip"], function(parser, domClass, query,dom){
	
	parser.parse(dom.byId("mainPageContent"));
       domClass.add(document.body, 'loaded');
});
</script>

<script>

require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
      function(registry, on, common, ready) {
    ready(function() {

<%
  if ( loginId!=null && loginId.length()>0 ) {
%>
      var focusField = registry.byId("password");
<%
  }
  else {
%>
      var focusField = registry.byId("loginId");
<%
  }
%>
      focusField.focus();
  
        <%--for ie, we need an event handler for enter--%>
      registry.byId("LogonForm").on("keyPress", function(event) {
        if (event && event.keyCode == 13) {

          registry.byId("LogonForm").submit();
       }
      });
    });
  });
</script>

</body>
</html>
