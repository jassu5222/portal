<%--
 *   ReLogon prompts the user to confirm relogon attempt.
 *   The page is displayed when the system detects that the user
 *   has an already open session.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
   com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
   com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,java.util.Hashtable" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session" />
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session" />
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session" />
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session" />


<%


	
   Hashtable addlParms = new Hashtable();
   addlParms.put("userOid",userSession.getUserOid());
   addlParms.put("auth",request.getParameter("auth"));

   UserWebBean user = beanMgr.createBean(UserWebBean.class, "User");
   user.setAttribute("user_oid", userSession.getUserOid());
   user.getDataFromAppServer();
   String userIdentifier = user.getAttribute("user_identifier");
%>
<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="Login.Title" />
      <jsp:param name="helpUrl"  value="/customer/login.htm" />
    </jsp:include>
    <%	WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>

<form name="ReLogonForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
   <input type="hidden" value="" name="buttonName">

  <%= formMgr.getFormInstanceAsInputField("ReLogonForm", addlParms) %>

  <jsp:include page="/common/ErrorSection.jsp" />

  <div class="formContentNoSidebar">

    <div class="padded">

      <div class="instruction formItem">
        <span><%= resMgr.getText("ReLogon.Warning1",TradePortalConstants.TEXT_BUNDLE) %>&nbsp;<%=userIdentifier%>&nbsp;<%= resMgr.getText("ReLogon.Warning2",TradePortalConstants.TEXT_BUNDLE) %></span>
      </div>
      <div class="instruction formItem">
        <span><%= resMgr.getText("ReLogon.Prompt",TradePortalConstants.TEXT_BUNDLE) %></span>
      </div>
      <table><tr> <td>
 	   <div class="formItem">
      
      <button id="continueButton" data-dojo-type="dijit.form.Button"
              type="submit">
        <%=resMgr.getText("common.continueText",TradePortalConstants.TEXT_BUNDLE)%>
      </button>
     
      </div>
     		</td>
 	   <td>
 	    <div class="formItem">

<%
        String link = formMgr.getLinkAsUrl("logout", response);
%>
        <button data-dojo-type="dijit.form.Button" type="button" id="cancelButton">
        <%= resMgr.getText("common.CancelText",TradePortalConstants.TEXT_BUNDLE) %>
       
      </button>	
      
      <%--   <a href="<%= link%>">
          <%= resMgr.getText("common.CancelText",TradePortalConstants.TEXT_BUNDLE) %>
        </a> --%>
       
 	   </div>
 	   </td></tr> </table>

    </div>

  </div>
 <%=widgetFactory.createHoverHelp("continueButton","ContinueReloginHoverText") %>
  <%=widgetFactory.createHoverHelp("cancelButton","common.cancel") %>
</form>
<%
   formMgr.storeInDocCache("default.doc",new DocumentHandler());
%>

  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<script type="text/javascript">
		require(["dijit/registry", "dojo/on", "dojo/ready"],
          function(registry, on, ready ) {
			ready(function() {
			console.log('page ready');
            console.log('NewLink='+'<%=link%>');
			var newButton = registry.byId("cancelButton");
			if ( newButton ) {
				newButton.on("click", function() {
					var newLink = '<%=link%>';
		        	
		        	if (isActive =='Y') {
		        		if (newLink != '' && newLink.indexOf("javascript:") == -1) {
					        var cTime = (new Date()).getTime();
					        newLink = newLink + "&cTime=" + cTime;
					        newLink = newLink + "&prevPage=" + context;
		        		}
				    }
					window.location = newLink;
				});
			}
			});
         });
	</script>
</body>
</html>
