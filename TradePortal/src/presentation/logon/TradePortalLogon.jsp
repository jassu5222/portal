<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This page serves as the starting point for the login and authentication process.
 * Based on a parameter ('auth') passed to it by calling pages, it determines the
 * authentication method being used.
 *
 * If the user must enter passwords, they are forwarded to a page where they can do so.
 * Otherwise, the identity of the user is determined from the certificate and then the 
 * LogonMediator is called.
 *
 * Certificate expiration is checked in this page.  We also need to check that the user
 * came from a link on their bank's page.   This must be done to ensure that the user's
 * certificate has not been revoked.  We are trusting that the bank's site will check
 * for revocation.
 *
 * Several parameters must be passed:
 *     auth         - the authentication method code for this logon attempt
 *     organization - the three letter code for the organization to which the user belongs
 *     branding     - the branding directory
 *     locale       - the locale to use (default to en_US if not passed)
 *     date         - the date and time at which the link to the Trade Portal was
 *                    created on the bank's web site.   The date and time are in the 
 *                    following format: YYYY-MM-DDThh:mm:ssZ (T and Z are hard coded characters)
 *                    A digital signature of the date and time (in base64 format) is
 *                    appended to the end of the date and time.   
 *                    are then encrypted using the bank's private key and then formatted in base64.
 *     certData     - the certificate ID of the user.  A digital signature of the certificate ID
 *                    (in base64 format) is appended to the end of the date and time.   The appended values 
 *                    are then encrypted using the bank's private key and then formatted in base64.
 *
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.html.*, java.text.*, java.security.*, java.util.*"%>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session" />
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session" />
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session" />
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session" />
<%
  // Should never be checked in as 'false'
  // When set to 'true' this JSP does not actually look for a certificate
  // Instead, it bypasses pulling data from the certificate and assumes that
  // the certificate ID is the value passed in the URL

  // This should only be set to 'false' during testing.
  boolean checkingForRealCertificate = true;

  //////////////////////////////////////////////////////////
  // GET AND CHECK FOR THE PRESENCE OF PAGE PARAMETERS    //
  //////////////////////////////////////////////////////////     
  

  // Get page parameters 
  String orgId               = StringFunction.xssCharsToHtml(request.getParameter("organization"));
  String brandingDirectory   = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
  String locale              = request.getParameter("locale");
  String bankDate            = request.getParameter("date");
  String certData            = request.getParameter("info");
  String requestedAuthMethod = request.getParameter("auth");
 
  //Ravindra - 10/01/2011 - CR-541 - Start
  /*
  *   this ensures Encrypted user and time (identification) from CMA is assigned to certData variable
  *
  */

  if(certData == null){
     certData = request.getParameter("identification");  
  }

  //Ravindra - 10/01/2011 - CR-541 - End

  // Check for an authentication method - this is mandatory
  if( (requestedAuthMethod == null) || (requestedAuthMethod.equals("")))
   {
     out.print("Authentication method must be specified.");
     return;
   }

  // Check for an organization ID and branding directory - these are mandatory
  if( (orgId == null) || (brandingDirectory == null) || (orgId.equals("")) || (brandingDirectory.equals("")))
   {
     out.print("A branding directory and organiztion ID must be specified");
     return;
   }
  else
   {
     // Make it so that the organization ID is not case sensitive
     orgId = orgId.toUpperCase();
   }

  // Check for optional locale
  if( (locale == null) || (locale.equals("")))
   {
     // Default to en_US if no locale is received
     locale = "en_US";
     Debug.debug("No locale specified in TradePortalLogon... defaulting to en_US");
   }


  //////////////////////////////////////////////////////////
  // INITIALIZE CRITICAL BEANS ON THE SESSION             //
  //////////////////////////////////////////////////////////     

  // Force initialization of the session's key beans
  // These beans must be in the session for all users 

  // jPylon Bean Manager
  beanMgr.init(request.getSession(true), "AmsServlet");

  // jPylon Form Manager
  formMgr.init(request.getSession(true), "AmsServlet", request.getContextPath());

  // We always use form based navigation in the Trade Portal
  formMgr.setNavigationType(FormManager.ACTION_BASED_NAVIGATION);

  // Set resource manager's locale to the locale passed in for now.
  // CheckLogon.jsp will reset this to the locale stored for the user
  resMgr.setResourceLocale(locale);

  // Set the session timeout
  request.getSession(false).setMaxInactiveInterval(SecurityRules.getTimeOutPeriodInSeconds());

  if (requestedAuthMethod.equals(TradePortalConstants.AUTH_PASSWORD))
   {
     //////////////////////////////////////////////////////////
     // FORWARD USER TO ENTER PASSWORD PAGE                  //
     //////////////////////////////////////////////////////////     
     String url = NavigationManager.getNavMan().getPhysicalPage("EnterPassword", request);
%>

    <jsp:forward page='<%= url %>'>
       <jsp:param name="organization" value="<%= orgId %>" />
       <jsp:param name="branding"     value="<%= brandingDirectory %>" />
       <jsp:param name="locale"       value="<%= locale %>" />
    </jsp:forward>

<%
   }
   //BSL 08/19/11 CR-663 Begin
   else if (TradePortalConstants.AUTH_SSO_REGISTRATION.equals(requestedAuthMethod)) {
	     // FORWARD USER TO ENTER REGISTRATION PASSWORD PAGE
	     String url = NavigationManager.getNavMan().getPhysicalPage("EnterSSORegistrationPassword", request);
	%>
	    <jsp:forward page='<%= url %>'>
	       <jsp:param name="organization" value="<%= orgId %>" />
	       <jsp:param name="branding"     value="<%= brandingDirectory %>" />
	       <jsp:param name="locale"       value="<%= locale %>" />
	    </jsp:forward>
	<%
   }
   else if (TradePortalConstants.AUTH_SSO.equals(requestedAuthMethod) ||
		   TradePortalConstants.AUTH_REGISTERED_SSO.equals(requestedAuthMethod)) {
       // If we use SSO, Affiliate Agent has already validated the SSOId.
       // If we use REGISTERED_SSO, WebLogic Server has received an
       // assertion for a user that may already be registered for SSO.
       // In either case, call LogonMediator to log the user in.
   //BSL 08/19/11 CR-663 End
       DocumentHandler inputDoc = new DocumentHandler();
       DocumentHandler outputDoc = null;

       // Populate the XML doc to pass to the mediator
       inputDoc.setAttribute("/In/User/ssoId", certData);
       inputDoc.setAttribute("/In/brandingDirectory", brandingDirectory);
       inputDoc.setAttribute("/In/organizationId", orgId);
       inputDoc.setAttribute("/In/locale", request.getParameter("locale"));
       // This makes sure that the password will not be checked
       //inputDoc.setAttribute("/In/auth", TradePortalConstants.AUTH_SSO);//BSL 08/19/11 CR-663 Change constant to requestAuthMethod
       inputDoc.setAttribute("/In/auth", requestedAuthMethod);
       inputDoc.setAttribute("/In/HTTP_CAAS_AUTHLEVEL", request.getParameter("HTTP_CAAS_AUTHLEVEL"));

       try {
          // Call the mediator
    	  LogonMediator mediator = (LogonMediator) 
			         EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "LogonMediator");
          outputDoc =  mediator.performService(inputDoc, resMgr.getCSDB());
          mediator.remove();
       }
       catch(Exception e) {
 	      out.print("Error calling LogonMediator for authentication.  System cannot proceed");
	      return;
       }

       // Put the output of the mediator (includes /In, /Out, and /Error sections) 
       // into default.doc so that the next page (CheckLogon) will have access.
       formMgr.storeInDocCache("default.doc", outputDoc);

       // After calling the mediator, always forward to CheckLogon.jsp.
       // Any errors will be handled there.
       String checkLogon = NavigationManager.getNavMan().getPhysicalPage("CheckLogon", request);
   %>
       <jsp:forward page='<%=checkLogon%>' />
   <%
   }
   //Added for SiteMinder12 Upgrade
   else if (TradePortalConstants.AUTH_CERTIFICATE_SSO.equals(requestedAuthMethod)) {
      // Certificate was valid by AMS, so LogonMediator must be called to log them in
      DocumentHandler inputDoc = new DocumentHandler();
      DocumentHandler outputDoc = null;

      // Populate the XML doc to pass to the mediator
      inputDoc.setAttribute("/In/User/certificateId", certData);
      inputDoc.setAttribute("/In/brandingDirectory", brandingDirectory);
      inputDoc.setAttribute("/In/organizationId", orgId);
      inputDoc.setAttribute("/In/locale", request.getParameter("locale"));
      // This makes sure that the password will not be checked
      inputDoc.setAttribute("/In/auth", TradePortalConstants.AUTH_CERTIFICATE);

      try
      {
         // Call the mediator
         LogonMediator mediator = (LogonMediator) 
            EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "LogonMediator");

         outputDoc =  mediator.performService(inputDoc, resMgr.getCSDB());

         mediator.remove();
      }
      catch(Exception e)
      {
         out.print("Error calling LogonMediator for authentication AUTH_CERTIFICATE_SSO.  System cannot proceed");
         return;
      }

      // Put the output of the mediator (includes /In, /Out, and /Error sections) 
      // into default.doc so that the next page (CheckLogon) will have access.
      formMgr.storeInDocCache("default.doc", outputDoc);

      // After calling the mediator, always forward to CheckLogon.jsp.  If there are 
      // any errors, CheckLogon.jsp will handle that.
      String checkLogon = NavigationManager.getNavMan().getPhysicalPage("CheckLogon", request);
%>
           <jsp:forward page='<%=checkLogon%>' />
<%
   }
   else if (requestedAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE))
   {
      ////////////////////////////////////////////////////////////////
      // VERIFY USER'S CERTIFICATE
      //   1. Check that the user has presented a certificate
      //   2. Check that the certificate has not expired
      //   3. Decrypt, check the signature, and check the date parameter
      //      to ensure that the user has come from a link on the bank's
      //      site created recently
      //   4. Decrypt, check the signature, and get the certificate ID
      //      from the URL 
      //   5. Determine the certificate ID using fields from the certificate
      //      (this is a bank specific process)
      //   6. Log in or send to Authentication Errors page
      ////////////////////////////////////////////////////////////////     

      // This must be set during the certificate validation and identity determination
      // logic.  Start with the certificate being valid and then try to disprove that
      boolean isCertificateValid = true;

      // The user's certificate ID.  If other tests pass, this is set to the 
      // a field(s) from the certificate
      String certificateId = null;


      ////////////////////////////////////////////////////////////////
      // GET CERTIFICATE DATA AND VERIFY THAT CERT HAS NOT EXPIRED  //
      ////////////////////////////////////////////////////////////////     

      // Create a CertificateInfo object based on the certificate in the
      // http request.
      CertificateInfo ci = null;
      if(checkingForRealCertificate)
           ci = new CertificateInfo(request);

      // Check that certificate information could be obtained from the
      // HTTP request.  If everything is set up properly, this should
      // never be null.  A possible cause if it is null is that the 
      // web server is not set up to ask for a client certificate.
      if(checkingForRealCertificate && ci.getCertificate() == null)
       {
          out.print("A certificate must be presented while in a secure connection...");
          return;
       }

      // Check that certificate has not expired
      if (checkingForRealCertificate && ci.isCertExpired())
       {
          System.out.println("Certificate for " + ci.getCertName() + " is expired.");
          isCertificateValid = false;
        }


      ///////////////////////////////////////////////////////////////
      // DETERMINE IF THE DIGITAL SIGNATURE NEEDS TO BE VERIFIED   //
      ///////////////////////////////////////////////////////////////

      // In most cases, it is proper to validate the digital signature to
      // ensure that the certificate has not been revoked by the bank
      // However, in the event of a failure of the bank's ability to 
      // create digital signatures, the verify_logon_digital_sig can be
      // set to 'N' to bypass the digital signature check.


      //jgadela R90 IR T36000026319 - SQL INJECTION FIX
      List<Object> sqlParams = new ArrayList();
      String sql;
      if(orgId.equals(TradePortalConstants.GLOBAL_ORG_ID))
       {
           // User belongs to the global organization - look there for the flag
           sql = "select verify_logon_digital_sig from global_organization";
       }
      else
       {
           // Otherwise, look on the client bank
           sql = "select verify_logon_digital_sig from client_bank where otl_id = ?";
           sqlParams.add(orgId);
       }

      DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sql, false, sqlParams);
      String verifySignedDataFlag = results.getAttribute("/ResultSetRecord(0)/VERIFY_LOGON_DIGITAL_SIG");

      boolean verifySignedData = verifySignedDataFlag.equals(TradePortalConstants.INDICATOR_YES);      


      //////////////////////////////////////////////////////
      // VERIFY THE CERTIFICATE DATA THAT WAS PASSED IN   //
      //////////////////////////////////////////////////////

      String decryptedCertData = null;  

      if(verifySignedData)
       {


	      PrivateKey portalPrivateKey = null;
	      PublicKey orgPublicKey = null;
    
	      // If no problems so far, proceed with verifying the cert data passed in
	      if(isCertificateValid)
	       {
	         // Look up the portal's private key and the public key of the bank to which the 
	         // user belongs
	         portalPrivateKey = StoredCertificateData.getPortalPrivateKeyFromFile();
	         orgPublicKey     = StoredCertificateData.getPublicKeyFromFile(orgId);



	         // The date and the signature must be parsed out
	         // The signature is always the same length (172 base64 characters) if 1024 bit keys are used
	         String encryptedCertIdData  = certData.substring(0, certData.length() - 172);
	         String certDataSignature    = certData.substring(certData.length() - 172, certData.length());

	         // Decrypt the certData URL parameter using the portal's private key
	         // The result of this decryption will be the certificate ID appended with a 
	         // digital signature in base64 format
	         decryptedCertData = EncryptDecrypt.decryptUsingPrivateKey(encryptedCertIdData, portalPrivateKey);

	         // Verify that digital signature for the certificate ID is valid using the bank's public key
	         if (!EncryptDecrypt.verifyDigitalSignature(decryptedCertData, certDataSignature, orgPublicKey)) 
	          {
	              System.out.println("Certificate data from link does not have a valid signature: certificate ID = 	"+decryptedCertData);
	              isCertificateValid = false;
	          }
	       }

	      //////////////////////////////////////////
	      // VERIFY THE DATE THAT WAS PASSED IN   //
	      //////////////////////////////////////////
	
	      // If no problems so far, proceed with verifying the date passed in
	      if(isCertificateValid)
	       {
	         // The date and the signature must be parsed out
	         // The signature is always the same length (172 base64 characters) if 1024 bit keys are used
	         String encryptedDate      = bankDate.substring(0, bankDate.length() - 172);
	         String dateSignature      = bankDate.substring(bankDate.length() - 172, bankDate.length());  
	
	         // Decrypt the date URL parameter using the portal's private key
	         // The result of this decryption will be the date appended with a digital signature in base64 format
	         String decryptedBankDate = EncryptDecrypt.decryptUsingPrivateKey(encryptedDate, portalPrivateKey);
	
	         // Verify that digital signature is valid using the bank's public key
	         if(!EncryptDecrypt.verifyDigitalSignature(decryptedBankDate, dateSignature, orgPublicKey))
	          {
	            System.out.println("Bank date from link does not have a valid signature: certificate ID = "+decryptedCertData);
	            isCertificateValid = false;
	          }
	         else
	          {
	            // Remove hard coded characters from the date string.
	            // The string is sent in as YYYY-MM-DDThh:mm:ssZ
	            //    For example, 2002-01-15T02:13:22Z
	            // Removed the T and the Z
	            char[] decryptedDateChars = decryptedBankDate.toCharArray();
	            decryptedDateChars[10] = ' ';
	            decryptedBankDate = new String(decryptedDateChars, 0, 19); 
	            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	            Date formattedBankDate = null;
	            Date currentTime = new java.util.Date();
            
	            try {
	                 formattedBankDate = formatter.parse(decryptedBankDate);
        	    } catch (ParseException e) {
	                 isCertificateValid = false;
	            }
            
	            int toleranceAmt = 5;
	            GregorianCalendar upperBound = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
	            upperBound.setTime(currentTime);
	            upperBound.add(Calendar.MINUTE, toleranceAmt);

	            toleranceAmt *= -1;

	            GregorianCalendar lowerBound = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
	            lowerBound.setTime(currentTime);
	            lowerBound.add(Calendar.MINUTE, toleranceAmt);

	            if (formattedBankDate.after(lowerBound.getTime()) && formattedBankDate.before(upperBound.getTime())) {
	                 isCertificateValid = true;
	            } else {
	                 isCertificateValid = false;
	                 System.out.println("Certificate logon failed because of invalid date stamp");
	                 System.out.println("   Cert ID: "+decryptedCertData);
	                 System.out.println("   Bank Date: "+formattedBankDate);
	                 System.out.println("   Portal Lower Bound: "+lowerBound.getTime());
	                 System.out.println("   Portal Upper Bound: "+upperBound.getTime()); 
        	    }
	         }
	       }
       } 

      //////////////////////////////////////////
      // PERFORM BANK SPECIFIC LOGIC TO       //
      // GET IDENTIFYING INFO FROM THE CERT   //
      //////////////////////////////////////////

      // Each client bank can choose a different set of fields to comprise
      // the identifying information for a user.   One client bank may
      // choose e-mail address, for example.  Another may choose to append
      // the phone number and last name.

      // Bank specific logic is run to pull field(s) off of the certificate
      // and build them into identifying information.  The 'certificateId'
      // variable is used to hold the identifying information.

      // The value set for 'certificateId' is used to look up the user by
      // the certificate_id field.

      // If there are no problems so far, proceed with determining the certificate ID
      if (isCertificateValid)
       {
          if(checkingForRealCertificate)
           {
            // Pull certificate ID off of certificate
            if(orgId.equals(TradePortalConstants.GLOBAL_ORG_ID))
             {
	       String[] emailAddresses = ci.getEmailAddressesFromSubjectAltName();
               if (emailAddresses.length != 0)
                  certificateId = emailAddresses[0];  
             }
            else if(orgId.equals("BAR"))
             {
               certificateId = ci.getEmailAddress();  
             }
            else if(orgId.equals("BMG"))
             {
	       String[] emailAddresses = ci.getEmailAddressesFromSubjectAltName();
               if (emailAddresses.length != 0)
                  certificateId = emailAddresses[0];  
             }
            else if(orgId.equals("ANZ"))
             {
               certificateId = ci.getCertName();  
             }
           }
          else
           {
             // We're in testing mode, there is no real certificate, so
             // just assume that the certifcate ID passed in the URL is the actual one
             certificateId = decryptedCertData;
           }


           if(verifySignedData)
            {

	         // Verify that the certificate ID determined by looking at the certificate
	         // matches the decrypted certificate ID sent in the URL
	         if( (certificateId == null) || !certificateId.equals(decryptedCertData) )
	          {
	               System.out.println("Encrypted certificate data does not match data on certificate. Data on cert: "+certificateId+" : Data in URL: "+decryptedCertData);
	               isCertificateValid = false;
	          }
             }
       }

      if(isCertificateValid)
       {
          ///////////////////////////////////////////
          // EVERYTHING IS OK TO LOG IN            //
          // CALL LOGON MEDIATOR AND PASS IN DATA  //
          ///////////////////////////////////////////

           // Certificate was valid, so LogonMediator must be called to log them in
           DocumentHandler inputDoc = new DocumentHandler();
           DocumentHandler outputDoc = null;

           // Populate the XML doc to pass to the mediator
           inputDoc.setAttribute("/In/User/certificateId", certificateId);
           inputDoc.setAttribute("/In/brandingDirectory", brandingDirectory);
           inputDoc.setAttribute("/In/organizationId", orgId);
           inputDoc.setAttribute("/In/locale", request.getParameter("locale"));
           // This makes sure that the password will not be checked
           inputDoc.setAttribute("/In/auth", TradePortalConstants.AUTH_CERTIFICATE);

	     try
            {
              // Call the mediator
	    	  LogonMediator mediator = (LogonMediator) 
				         EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "LogonMediator");

		  outputDoc =  mediator.performService(inputDoc, resMgr.getCSDB());

		  mediator.remove();
 	      }
           catch(Exception e)
            {
	 	   out.print("Error calling LogonMediator for authentication.  System cannot proceed");
		   return;
	      }

           // Put the output of the mediator (includes /In, /Out, and /Error sections) 
           // into default.doc so that the next page (CheckLogon) will have access.
	     formMgr.storeInDocCache("default.doc", outputDoc);

           // After calling the mediator, always forward to CheckLogon.jsp.  If there are 
           // any errors, CheckLogon.jsp will handle that.
          String checkLogon = NavigationManager.getNavMan().getPhysicalPage("CheckLogon", request);
       %>
           <jsp:forward page='<%=checkLogon%>' />
       <%
       }
      else
       {
           //////////////////////////////////////////////
           // THERE WAS A PROBLEM WITH THE CERTIFICATE //
           // OR WITH THE DATA IN THE URL PARAMETERS   //
           //////////////////////////////////////////////

           // Issue an error indicating that the certificate is not valid

           // Use mediatorServices to accumulate errors
           MediatorServices mediatorServices = new MediatorServices();
           mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
           mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                   TradePortalConstants.CERTIFICATE_NOT_VALID);
           mediatorServices.addErrorInfo();

           // Add these errors to the default.doc XML so that it can be accessed by the 
           // error page
           DocumentHandler defaultDoc = new DocumentHandler();
           defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
           formMgr.storeInDocCache("default.doc", defaultDoc);

           // Forward the user to the error page, passing along parameter
           String url = NavigationManager.getNavMan().getPhysicalPage("AuthenticationErrors", request);
        %>
          <jsp:forward page='<%= url %>'>
             <jsp:param name="organization" value="<%= orgId %>" />
             <jsp:param name="branding"     value="<%= brandingDirectory %>" />
             <jsp:param name="locale"       value="<%= locale %>" />
          </jsp:forward>
        <%
       }     
   }
   //Ravindra - 10/01/2011 - CR-541 - Start
   /*
    * AES-256 authentication method TradePortalConstants.AUTH_AES256
    * This logic is specific to CMA, to accommodate an addition of new AES-256 authentication method
    */
   else if (requestedAuthMethod.equals(TradePortalConstants.AUTH_AES256))
   {
	 //jgadela R90 IR T36000026319 - SQL INJECTION FIX
      String sql = "SELECT ENCRYPTION_SALT, BANK_LOGON_TOLERANCE_MINUTES, CORP_LOGON_TOLERANCE_MINUTES FROM CLIENT_BANK WHERE OTL_ID = ?";
      DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{orgId});
      String decryptedUserId=null;

      if(results != null){
              
         //Ravindra - 03/29/2011 - CLUL032349513 - Start 
         String aesEncryptKey=null;    
         String aesEncryptionSalt = results.getAttribute("/ResultSetRecord(0)/ENCRYPTION_SALT");
         if ( aesEncryptionSalt != null && aesEncryptionSalt.length()>0 ){
            try {
               // Convert the salt into bytes 
               //byte[] saltBytes = EncryptDecrypt.base64StringToBytes(aesEncryptionSalt);
               // Decrypt the salt using TripleDES
               //byte[] decryptedSalt = EncryptDecrypt.decryptUsingDes3(saltBytes);
               // STang [Begin] Rel8.0cma - 8/29/2012 - SEUM082950101 - Decrypt using bytesToHexString
               //aesEncryptKey = EncryptDecrypt.bytesToBase64String(decryptedSalt);
               //aesEncryptKey = EncryptDecrypt.bytesToHexString(decryptedSalt);
               // STang [End] Rel8.0cma - 8/29/2012 - SEUM082950101
               
               //AAlubala - IR SNUM020738044 - decrypt the key using this method which handles conversion ot bytes etc
               aesEncryptKey = EncryptDecrypt.decryptStringUsingTripleDes(aesEncryptionSalt);
            } catch ( Exception ex ) {
             	// response.setStatus(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.ACCESS_FAILURE ));
                //todo: forward to AuthenticationError page?
 	            // T36000013446- use custom http header to communicate CMA specific status codes - start
            	String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.ACCESS_FAILURE  ));
 	 		    response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
  	 		    System.err.println("Cannot decrypt AES encryption salt. " + "[" + aesEncryptKey + "]" + "System cannot proceed");
                out.println("Cannot decrypt AES encryption salt. System cannot proceed");
                return;
          	  // T36000013446- use custom http header to communicate CMA specific status codes - end
            }
         }
         //Ravindra - 03/29/2011 - CLUL032349513 - End 
         //Ravindra - 03/29/2011 - CAUL031766015 - Start 
         if(aesEncryptKey ==null || aesEncryptKey.length() %16 !=0){
             //Ravindra - 03/31/2011 - CUUL032965287 - Start
             // 603 is CMA specific status code to notate "The UserID is not a valid user on the system."
             //response.setStatus(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.ACCESS_FAILURE ));
             
             	  // T36000013446- use custom http header to communicate CMA specific status codes - start
         	   String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.ACCESS_FAILURE  ));
 	 		   response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
 	 		  // T36000013446- use custom http header to communicate CMA specific status codes - end
             //todo: forward to AuthenticationError page?
             System.err.println("Invalid AES encryption salt [" + aesEncryptKey + "] length of key = [" + (null==aesEncryptKey?0:aesEncryptKey.length()) + "] System cannot proceed");
             
 	 		   out.println("Invalid AES encryption salt. System cannot proceed");
             //Ravindra - 03/31/2011 - CUUL032965287 - End
             return;
         }
         //Ravindra - 03/29/2011 - CAUL031766015 - End 
                            
         String decryptedUserIdAndTime = null;
         String decryptedTime = null;
  	 // STang [Begin] Rel8.0cma - 8/29/2012 - SEUM082950101 - The identification/certData is sent URLEncoded.  This will need to be URLDecoded prior to use.
  	 //AAlubala - IR SNUM020738044 - URLDecoding not neeeded since it is done by request.getParameter() method
  	 //certData = java.net.URLDecoder.decode(certData);
         // STang [End] Rel8.0cma - 8/29/2012 - SEUM082950101 
         String encryptedUserIdAndTime = certData;
         //System.out.println("encryptedUserIdAndTime:"+encryptedUserIdAndTime);
         //System.out.println("aesEncryptKey:"+aesEncryptKey);
         /*
          * Call EncryptDecrypt to decrypt the identification and time parameter
          */
         try {
            decryptedUserIdAndTime = EncryptDecrypt.decryptUsingAESKey(encryptedUserIdAndTime, aesEncryptKey);
            decryptedUserId = decryptedUserIdAndTime.substring(0, decryptedUserIdAndTime.indexOf(";"));
            decryptedTime = decryptedUserIdAndTime.substring(decryptedUserIdAndTime.indexOf(";")+1);
         } catch (Exception ex ) {
        	 
              //response.setStatus(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.ACCESS_FAILURE ));
 	 			 // T36000013446- use custom http header to communicate CMA specific status codes - start
	      	   	String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.ACCESS_FAILURE  ));
	 		   	response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);

	 			  // T36000013446- use custom http header to communicate CMA specific status codes - end
             //todo: forward to AuthenticationError page?
            System.err.println("Cannot decrypt identification info. Length of aesKey [" + aesEncryptKey.length() + "] Length of UserID and Time [" + encryptedUserIdAndTime.length() + "] System cannot proceed");
            out.println("Cannot decrypt identification info. System cannot proceed");
            return;
         }

         //cquinton 4/4/2011 - login_id is only required for passwords
         //lookup must be by sso_id
         //Ravindra - 03/29/2011 - CDUL031849759 - Start 
         //cquinton 4/15/2011 Rel 7.0.0 ir#clul041136190 start
         //add additional criteria - sso_id is unique per client bank
         //note: this does not take into account global org becuase per above
         // this is not even possible (have to have a client bank)
         sql = "SELECT u.OWNERSHIP_LEVEL FROM USERS u, client_bank cb WHERE " +
            "u.a_client_bank_oid = cb.organization_oid " +
            "and cb.otl_id =? " +
            "and u.activation_status = ? " +
            "and u.SSO_ID= ?";
         List<Object> sqlParamsSso = new ArrayList(); 
         sqlParamsSso.add(orgId);
         sqlParamsSso.add(TradePortalConstants.ACTIVE) ;
         sqlParamsSso.add(decryptedUserId);
         
         System.err.println("TradePortalLogon: decryptedUserId [" + decryptedUserId + "] Org [" + orgId + "]");   
         //cquinton 4/15/2011 Rel 7.0.0 ir#clul041136190 end
         //Ravindra - 03/29/2011 - CDUL031849759 - End 
         DocumentHandler usersResults = DatabaseQueryBean.getXmlResultSet(sql, false,sqlParamsSso);
         if(usersResults == null){
            //this is a invalid user.  just to be doubly sure, what to return, lookup by login id.
            //if login id is found its a 602 (invalid provisioning) - if not its a 603
            //cquinton 4/15/2011 Rel 7.0.0 ir#clul041136190 start
            sql = "SELECT u.OWNERSHIP_LEVEL FROM USERS u, client_bank cb WHERE " +
               "u.a_client_bank_oid = cb.organization_oid " +
               "and cb.otl_id = ? " +
               "and u.activation_status = ? " +
               "and u.LOGIN_ID= ?";
            //cquinton 4/15/2011 Rel 7.0.0 ir#clul041136190 end
             List<Object> sqlParams = new ArrayList();
            sqlParams.add(SQLParamFilter.filter(orgId));
            sqlParams.add(TradePortalConstants.ACTIVE);
            sqlParams.add(SQLParamFilter.filter(decryptedUserId));
            
            
            
            usersResults = DatabaseQueryBean.getXmlResultSet(sql, false,sqlParams);
            if(usersResults != null){
            
                // 602 is CMA specific status code to notate "When a UserID is not setup as a single sign-on user"
                //Ravindra - 03/31/2011 - CUUL032965287 - Start
                // response.setStatus(HttpResponseUtility.getResponseCode(orgId, decryptedUserId, HttpResponseUtility.INCOMPLETE_USER ));
                
          	  // T36000013446- use custom http header to communicate CMA specific status codes - start
            	String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.INCOMPLETE_USER  ));
 	 		   	response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
 	 		  // T36000013446- use custom http header to communicate CMA specific status codes - end
                //todo: forward to AuthenticationError page?
                System.err.println("TradePortalLogon:Incomplete User Provisioning for [" + decryptedUserId + "] in " + orgId + " org . System cannot proceed");
                out.println("Incomplete User Provisioning. User Provisioning for [" + decryptedUserId + "] in " + orgId + " org . System cannot proceed");
            } else {
                //response.setStatus(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.ACCESS_FAILURE ) );
                
                // T36000013446- use custom http header to communicate CMA specific status codes - start
                String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.ACCESS_FAILURE  ));
 	 		   response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
 	 		   
 	 		  // T36000013446- use custom http header to communicate CMA specific status codes - end
                
                System.err.println("Inappropriate Access. System cannot proceed");
                out.println("Inappropriate Access. System cannot proceed");
                //todo: forward to AuthenticationError page?
            }
            //Ravindra - 03/29/2011 - CLUL032349513 - Start 
            return;
            //Ravindra - 03/29/2011 - CLUL032349513 - End 
         }

         //get tolerance - value pulled from above id lookup
         int toleranceAmt=0 ;
         String ownershipLevel = usersResults.getAttribute("/ResultSetRecord(0)/OWNERSHIP_LEVEL");

         /*
          * Value of toleranceAmt is CORP_LOGON_TOLERANCE_MINUTES when ownershipLevel is CORPORATE
          * Otherwise make it as BANK_LOGON_TOLERANCE_MINUTES
          */
         //Ravindra - 03/29/2011 - CYUL032844053 - Start
         String toleranceMinutes=null ;
         if(TradePortalConstants.OWNER_CORPORATE.equals(ownershipLevel)){
            toleranceMinutes = results.getAttribute("/ResultSetRecord(0)/CORP_LOGON_TOLERANCE_MINUTES");
         }else{
            toleranceMinutes = results.getAttribute("/ResultSetRecord(0)/BANK_LOGON_TOLERANCE_MINUTES");
         }

         if ( toleranceMinutes == null || toleranceMinutes.trim().length()<=0 ) {
            toleranceMinutes = "0"; //for display below if necessary
         }
         try {
            toleranceAmt=Integer.parseInt(toleranceMinutes);
         } catch (Exception ex) {
            toleranceAmt=0;
         }
         //Ravindra - 03/29/2011 - CYUL032844053 - End
               
         //char[] decryptedDateChars = decryptedTime.toCharArray();
         //decryptedDateChars[10] = ' ';
            
         // Change decrypted time to YYYY-MM-DDThh:mm:ssZ format
         SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         Date formattedDecryptedTime = null;
         //cquinton 4/18/2011 - use GMTUtility for consistency. don't use override
         Date currentTime = GMTUtility.getGMTDateTime(true,false);
            
         try {
            decryptedTime= decryptedTime.replace('T', ' ');
            decryptedTime=decryptedTime.replace('Z', ' ').trim();
            formattedDecryptedTime = formatter.parse(decryptedTime);
                 
         } catch (ParseException e) {
            e.printStackTrace();
         }catch (Exception e){
            e.printStackTrace();
         }
            
         GregorianCalendar upperBound = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
         upperBound.setTime(currentTime);
         upperBound.add(Calendar.MINUTE, toleranceAmt);
            
         toleranceAmt *= -1;
         
         GregorianCalendar lowerBound = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
         lowerBound.setTime(currentTime);
         lowerBound.add(Calendar.MINUTE, toleranceAmt);
            
         System.out.println("TradePortalLogon:            currentTime >> " + currentTime);
         System.out.println("TradePortalLogon:   lowerBound.getTime() >> " + lowerBound.getTime());
         System.out.println("TradePortalLogon: formattedDecryptedTime >> " + formattedDecryptedTime);
         System.out.println("TradePortalLogon:   upperBound.getTime() >> " + upperBound.getTime());
         System.out.println("TradePortalLogon:         /In/User/ssoId >> " + decryptedUserId);
         System.out.println("TradePortalLogon:         /In/           >> " + brandingDirectory);
         System.out.println("TradePortalLogon:         /In/locale     >> " + request.getParameter("locale"));
         System.out.println("TradePortalLogon:         /In/auth       >> " + TradePortalConstants.AUTH_AES256);
         /*
          * Checking   If the date and time is within the specified time allocated
          */
         //Ravindra - 03/29/2011 - CYUL032844053 - Start
         if (formattedDecryptedTime.after(lowerBound.getTime()) && formattedDecryptedTime.before(upperBound.getTime())) {
         //if ( formattedDecryptedTime.before(upperBound.getTime())) {
            //Ravindra - 03/29/2011 - CYUL032844053 - End
            // So just call LogonMediator log the user in
            DocumentHandler inputDoc = new DocumentHandler();
            DocumentHandler outputDoc = new DocumentHandler();
         
            // Populate the XML doc to pass to the mediator
            inputDoc.setAttribute("/In/User/ssoId", decryptedUserId);
            inputDoc.setAttribute("/In/brandingDirectory", brandingDirectory);
            inputDoc.setAttribute("/In/organizationId", orgId);
            inputDoc.setAttribute("/In/locale", request.getParameter("locale"));
            // This makes sure that the password will not be checked
            inputDoc.setAttribute("/In/auth", TradePortalConstants.AUTH_AES256);
         
            try {
               // Call the mediator
               LogonMediator mediator = (LogonMediator) 
                  EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "LogonMediator");
            
               outputDoc =  mediator.performService(inputDoc, resMgr.getCSDB());
                     
               mediator.remove();
            } catch(Exception e) {
               out.print("Error calling LogonMediator for authentication.  System cannot proceed");
               return;
            }
            
            // Put the output of the mediator (includes /In, /Out, and /Error sections) 
            // into default.doc so that the next page (CheckLogon) will have access.
            formMgr.storeInDocCache("default.doc",outputDoc);
            userSession.setAuthMethod(TradePortalConstants.AUTH_AES256);
            userSession.setSsoId(decryptedUserId);                       
            
            // After calling the mediator, always forward to CheckLogon.jsp.  If there are 
            // any errors, CheckLogon.jsp will handle that.
            String checkLogon = NavigationManager.getNavMan().getPhysicalPage("CheckLogon", request);
            %>
               <jsp:forward page='<%=checkLogon%>' />
            <%
            //Ravindra - 03/31/2011 - CUUL032965287 - Start
            // 200 is CMA specific status code to notate "When a user successfully logs in"
            //response.setStatus(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.LOGON_SUCCESS ) );
            //Ravindra - 03/31/2011 - CUUL032965287 - End
                 
         }//End of if (formattedDecryptedTime.after(lowerBound.getTime()) && formattedDecryptedTime.before(upperBound.getTime()))
         else{
        	 //Ravindra - 03/31/2011 - CUUL032965287 - Start
 		    // 605 is CMA specific status code to notate "The date and time request parameter is not within tolerance minutes"
 		    // response.setStatus(HttpResponseUtility.getResponseCode(orgId, decryptedUserId, HttpResponseUtility.LOGON_TOLERANCE_TIMEOUT ));
 		    //todo: forward to AuthenticationError page?
 		
 		   	// T36000013446- use custom http header to communicate CMA specific status codes -start
 		   	String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.LOGON_TOLERANCE_TIMEOUT ));
 			response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
 			 // T36000013446- use custom http header to communicate CMA specific status codes - end
 		    System.err.println("Login request not within the " + toleranceMinutes + " minute window. System cannot proceed");
 		    out.println("Login request not within the " + toleranceMinutes + " minute window. System cannot proceed");
     		//Ravindra - 03/31/2011 - CUUL032965287 - End         
     		}
         
      }//End of if(results != null)
      else {
          //org not found - still lookup response code just in case
          //response.setStatus(HttpResponseUtility.getResponseCode(orgId, null, HttpResponseUtility.ACCESS_FAILURE ));
          //todo: forward to AuthenticationError page?
          System.err.println("Inappropriate Access. System cannot proceed");
          out.println("Inappropriate Access. System cannot proceed");
      		 // T36000013446- use custom http header to communicate CMA specific status codes - start
 		   String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, decryptedUserId, HttpResponseUtility.ACCESS_FAILURE ));
  		   response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
  		  // T36000013446- use custom http header to communicate CMA specific status codes - end
      }//End of if(results != null)
   }
   //Ravindra - 10/01/2011 - CR-541 - End
   else {// no proper authentication method specified.  This really should not happen. 
         // Forward the user to the error page, passing along parameter
      String url = NavigationManager.getNavMan().getPhysicalPage("AuthenticationErrors", request);
      %>
         <jsp:forward page='<%= url %>'>
            <jsp:param name="organization" value="<%= orgId %>" />
            <jsp:param name="branding"     value="<%= brandingDirectory %>" />
            <jsp:param name="locale"       value="<%= locale %>" />
         </jsp:forward>
      <%
   }
%>
