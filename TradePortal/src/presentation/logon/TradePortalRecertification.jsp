<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This page performs validation of the user's certificate during the recertification process
 * in the TradePortal.   During a few user activities, such as Cash Management authorization, 
 * the user will be required to re-enter certificate information, and this JSP will perform the 
 * validation.
 *
 * Certificate expiration is checked in this page.  We also need to check that the user
 * came from a link on their bank's page.   This must be done to ensure that the user's
 * certificate has not been revoked.  We are trusting that the bank's site will check
 * for revocation.
 *
 * Several parameters must be passed:
 *     auth         - the authentication method code for this logon attempt
 *     organization - the three letter code for the organization to which the user belongs
 *     branding     - the branding directory
 *     locale       - the locale to use (default to en_US if not passed)
 *     date         - the date and time at which the link to the Trade Portal was
 *                    created on the bank's web site.   The date and time are in the 
 *                    following format: YYYY-MM-DDThh:mm:ssZ (T and Z are hard coded characters)
 *                    A digital signature of the date and time (in base64 format) is
 *                    appended to the end of the date and time.   
 *                    are then encrypted using the bank's private key and then formatted in base64.
 *     certData     - the certificate ID of the user.  A digital signature of the certificate ID
 *                    (in base64 format) is appended to the end of the date and time.   The appended values 
 *                    are then encrypted using the bank's private key and then formatted in base64.
 *
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*, com.ams.tradeportal.busobj.util.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.html.*, java.text.*, java.security.*" %>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session" />
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session" />
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session" />

<%


  // Should never be checked in as 'false'
  // When set to 'true' this JSP does not actually look for a certificate
  // Instead, it bypasses pulling data from the certificate and assumes that
  // the certificate ID is the value passed in the URL

  // This should only be set to 'false' during testing.
  boolean checkingForRealCertificate = true;

  //////////////////////////////////////////////////////////
  // GET AND CHECK FOR THE PRESENCE OF PAGE PARAMETERS    //
  //////////////////////////////////////////////////////////     
  
  DocumentHandler              xmlDoc                   = formMgr.getFromDocCache();
  
  // Get page parameters 
  String orgId               = xmlDoc.getAttribute("/In/organizationId");
  String brandingDirectory   = xmlDoc.getAttribute("/In/brandingDirectory");
  String locale              = xmlDoc.getAttribute("/In/locale");
  String bankDate            = xmlDoc.getAttribute("/In/date");
  String certData            = xmlDoc.getAttribute("/In/info");
  String requestedAuthMethod = xmlDoc.getAttribute("/In/auth");
  String certificateType     = xmlDoc.getAttribute("/In/certificate_type");
  String loginId             = xmlDoc.getAttribute("/In/User/loginId");
  String password            = xmlDoc.getAttribute("/In/User/password");
  String password2FA         = xmlDoc.getAttribute("/In/User/password2FA");
  
  // CR-923 If this page is invoked via link (EnterRecertificationGemalto.jsp), instead of form submission, the 
  // parameters are added to the XML manually.
  String navId = request.getParameter("ni");
  if (navId != null && !navId.equals("")) {
      orgId = formMgr.getSecurePageParm(navId, "organizationId");
      xmlDoc.setAttribute("/In/organizationId", orgId);
      brandingDirectory   = formMgr.getSecurePageParm(navId, "brandingDirectory");
      xmlDoc.setAttribute("/In/brandingDirectory", brandingDirectory);
      locale              = formMgr.getSecurePageParm(navId, "locale");
      xmlDoc.setAttribute("/In/locale", locale);
      bankDate            = formMgr.getSecurePageParm(navId, "date");
      xmlDoc.setAttribute("/In/date", bankDate);
      certData            = formMgr.getSecurePageParm(navId, "info");
      xmlDoc.setAttribute("/In/info", certData);
      requestedAuthMethod = formMgr.getSecurePageParm(navId, "auth");
      xmlDoc.setAttribute("/In/auth", requestedAuthMethod);
      certificateType = formMgr.getSecurePageParm(navId, "CertificateType");
      xmlDoc.setAttribute("/In/certificate_type", certificateType);
      loginId             = formMgr.getSecurePageParm(navId, "loginId");
      xmlDoc.setAttribute("/In/User/loginId", loginId);
      String reAuthObjectType = formMgr.getSecurePageParm(navId, "reAuthObjectType");
      xmlDoc.setAttribute("/In/reAuthObjectType", reAuthObjectType);
      String signatureData = request.getParameter("Signature_Data");
      xmlDoc.setAttribute("/In/Signature_Data", signatureData);
      String contentData = formMgr.getSecurePageParm(navId, "VascoKeyInfo");
      xmlDoc.setAttribute("/In/VascoKeyInfo", contentData);
      
  }
  
  // Check for an authentication method - this is mandatory
  if( InstrumentServices.isBlank(requestedAuthMethod))
   {
     System.out.println("Authentication method must be specified.");
       %>
         <%@ include file="fragments/recertificationErrors.frag" %>
       <%
   }

  // Check for an organization ID and branding directory - these are mandatory
 if(InstrumentServices.isBlank(orgId) || InstrumentServices.isBlank(brandingDirectory))
   {
     System.out.println("A branding directory and organiztion ID must be specified");
       %>
         <%@ include file="fragments/recertificationErrors.frag" %>
       <%
   }
  else
   {
     // Make it so that the organization ID is not case sensitive
     orgId = orgId.toUpperCase();
   }

  // Check for optional locale
   if(InstrumentServices.isBlank(locale))
   {
     // Default to en_US if no locale is received
     locale = "en_US";
     Debug.debug("No locale specified in TradePortalLogon... defaulting to en_US");
   }
   

   // This must be set during the certificate validation and identity determination
   // logic.  Start with the certificate being valid and then try to disprove that
   boolean isReauthSuccessful = true;
   
   if (!(InstrumentServices.isBlank(requestedAuthMethod)) && requestedAuthMethod.equalsIgnoreCase("test")) {
       //only supported for testing purposes
       System.out.println("WARNING: Bypassing certificate reauthentication");          
       Debug.debug ("loginId received = " + loginId);
       Debug.debug ("password received = " + password);    
       // Forward to the CompleteRecertification page, which will return control to the Portal page to complete
       // Authorization in testing mode
	    
       //TradePortalRecertification will set this to YES, when recertification is successful
       session.setAttribute("recertificationSuccessful", TradePortalConstants.INDICATOR_YES);
	   
       String physicalPage = NavigationManager.getNavMan().getPhysicalPage("CompleteRecertification", request);

         %>
            <jsp:forward page='<%= physicalPage %>'/>
            <%       

  }
  if (requestedAuthMethod.equals(TradePortalConstants.AUTH_PASSWORD))
   {
       System.out.println("Recertification only supports certificate authentication");
       %>
         <%@ include file="fragments/recertificationErrors.frag" %>
       <%
   }
   else if (requestedAuthMethod.equals(TradePortalConstants.AUTH_SSO))//TradePortalConstants.AUTH_SSO
   {
           // If we use SSO, Affiliate Agent has already validated the SSOId
       System.out.println("Recertification only supports certificate authentication");
        %>
          <%@ include file="fragments/recertificationErrors.frag" %>
       <%
     
   }
   // W Zhu 12/21/2015 CR-923 IR T36000045889 add GEMALTO
   else if (requestedAuthMethod.equals(TradePortalConstants.AUTH_2FA) 
           || requestedAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)
              && TradePortalConstants.GEMALTO.equals(certificateType)) {
      try
      {
         // Call the mediator
         //ir cnuk113043991 - refactor reauthentication logic to ReauthenticateMediator
         ReauthenticateMediator mediator = (ReauthenticateMediator) 
            EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "ReauthenticateMediator");

		  mediator.performService(xmlDoc, resMgr.getCSDB());

         mediator.remove();
      }
      catch(Exception e)
      {
         out.print("Error calling ReauthenticateMediator for authentication.  System cannot proceed");
         System.out.println("TradePortalRecertification.jsp: Error calling ReauthenticateMediator for authentication.  System cannot proceed");
         e.printStackTrace();
         return;
      }
       
      System.out.println("TradePortalRecertification.jsp: ReauthenticateMediator returns logonSuccesful = " + xmlDoc.getAttribute("/Out/logonSuccesful"));

      if ( xmlDoc != null && "true".equals(xmlDoc.getAttribute("/Out/logonSuccesful")) ) {
         isReauthSuccessful = true;
      } 	  
      else {
         isReauthSuccessful = false;
      }
      //AAlubala - CR711 - Rel8.0 - 01/14/2012
      //Obtain the proxy user
       if ( (xmlDoc != null )){    	   
    	   String proxyUserOid = "";    	  
    	   proxyUserOid = xmlDoc.getAttribute("/Out/proxyUserOid");
		   //
		   //ok, if we have a proxy user, set the value in session to be used downstream 
		   //The proxy user may be used for obtaining their panel authority code
		   //to be used during instrument authorization steps
		   //
    	   session.removeAttribute("proxyUserOid");
    	   if(InstrumentServices.isNotBlank(proxyUserOid)){    		   
    		   session.setAttribute("proxyUserOid",proxyUserOid);    		   
    	   }
       }      
   }
  else if (requestedAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE))
   {
      ////////////////////////////////////////////////////////////////
      // VERIFY USER'S CERTIFICATE
      //   1. Check that the user has presented a certificate
      //   2. Check that the certificate has not expired
      //   3. Decrypt, check the signature, and check the date parameter
      //      to ensure that the user has come from a link on the bank's
      //      site created recently
      //   4. Decrypt, check the signature, and get the certificate ID
      //      from the URL 
      //   5. Determine the certificate ID using fields from the certificate
      //      (this is a bank specific process)
      //   6. Log in or send to Authentication Errors page
      ////////////////////////////////////////////////////////////////     


      // The user's certificate ID.  If other tests pass, this is set to the 
      // a field(s) from the certificate
      String certificateId = null;
      
     
      if( InstrumentServices.isBlank(certData) ) {
           System.out.println("Certificate data not provided, cert = " + certData);
          isReauthSuccessful = false;
          %>
          <%@ include file="fragments/recertificationErrors.frag" %>
          <%
     }

      ////////////////////////////////////////////////////////////////
      // GET CERTIFICATE DATA AND VERIFY THAT CERT HAS NOT EXPIRED  //
      ////////////////////////////////////////////////////////////////     

      // Create a CertificateInfo object based on the certificate in the
      // http request.
      CertificateInfo ci = null;
      if(checkingForRealCertificate)
           ci = new CertificateInfo(request);

      // Check that certificate information could be obtained from the
      // HTTP request.  If everything is set up properly, this should
      // never be null.  A possible cause if it is null is that the 
      // web server is not set up to ask for a client certificate.
      if(checkingForRealCertificate && ci.getCertificate() == null)
       {
          System.out.println("A certificate must be presented while in a secure connection...");
          isReauthSuccessful = false;
       }

      // Check that certificate has not expired
      if (checkingForRealCertificate && ci.isCertExpired())
       {
          System.out.println("Certificate for " + ci.getCertName() + " is expired.");
          isReauthSuccessful = false;
        }


      ///////////////////////////////////////////////////////////////
      // DETERMINE IF THE DIGITAL SIGNATURE NEEDS TO BE VERIFIED   //
      ///////////////////////////////////////////////////////////////

      // In most cases, it is proper to validate the digital signature to
      // ensure that the certificate has not been revoked by the bank
      // However, in the event of a failure of the bank's ability to 
      // create digital signatures, the verify_logon_digital_sig can be
      // set to 'N' to bypass the digital signature check.

      String sql;
    //jgadela R91 IR T36000026319 - SQL INJECTION FIX
      List<Object> sqlParams = new ArrayList();
      if(orgId.equals(TradePortalConstants.GLOBAL_ORG_ID))
       {
           // User belongs to the global organization - look there for the flag
           sql = "select verify_logon_digital_sig from global_organization";
       }
      else
       {
           // Otherwise, look on the client bank
           sql = "select verify_logon_digital_sig from client_bank where otl_id = ?";
           sqlParams.add(orgId);
       }

      DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sql, false, sqlParams);
      String verifySignedDataFlag = results.getAttribute("/ResultSetRecord(0)/VERIFY_LOGON_DIGITAL_SIG");

      boolean verifySignedData = verifySignedDataFlag.equals(TradePortalConstants.INDICATOR_YES);      


      //////////////////////////////////////////////////////
      // VERIFY THE CERTIFICATE DATA THAT WAS PASSED IN   //
      //////////////////////////////////////////////////////

      String decryptedCertData = null;  

      if(verifySignedData)
       {


	      PrivateKey portalPrivateKey = null;
	      PublicKey orgPublicKey = null;
    
	      // If no problems so far, proceed with verifying the cert data passed in
	      if(isReauthSuccessful)
	       {
	         // Look up the portal's private key and the public key of the bank to which the 
	         // user belongs
	         portalPrivateKey = StoredCertificateData.getPortalPrivateKeyFromFile();
	         orgPublicKey     = StoredCertificateData.getPublicKeyFromFile(orgId);



	         // The date and the signature must be parsed out
	         // The signature is always the same length (172 base64 characters) if 1024 bit keys are used
	         String encryptedCertIdData  = certData.substring(0, certData.length() - 172);
	         String certDataSignature    = certData.substring(certData.length() - 172, certData.length());

	         // Decrypt the certData URL parameter using the portal's private key
	         // The result of this decryption will be the certificate ID appended with a 
	         // digital signature in base64 format
	         decryptedCertData = EncryptDecrypt.decryptUsingPrivateKey(encryptedCertIdData, portalPrivateKey);

	         // Verify that digital signature for the certificate ID is valid using the bank's public key
	         if (!EncryptDecrypt.verifyDigitalSignature(decryptedCertData, certDataSignature, orgPublicKey)) 
	          {
	              System.out.println("Certificate data from link does not have a valid signature: certificate ID = 	"+decryptedCertData);
	              isReauthSuccessful = false;
	          }
	       }

	      //////////////////////////////////////////
	      // VERIFY THE DATE THAT WAS PASSED IN   //
	      //////////////////////////////////////////
	
	      // If no problems so far, proceed with verifying the date passed in
	      if(isReauthSuccessful)
	       {
	         // The date and the signature must be parsed out
	         // The signature is always the same length (172 base64 characters) if 1024 bit keys are used
	         String encryptedDate      = bankDate.substring(0, bankDate.length() - 172);
	         String dateSignature      = bankDate.substring(bankDate.length() - 172, bankDate.length());  
	
	         // Decrypt the date URL parameter using the portal's private key
	         // The result of this decryption will be the date appended with a digital signature in base64 format
	         String decryptedBankDate = EncryptDecrypt.decryptUsingPrivateKey(encryptedDate, portalPrivateKey);
	
	         // Verify that digital signature is valid using the bank's public key
	         if(!EncryptDecrypt.verifyDigitalSignature(decryptedBankDate, dateSignature, orgPublicKey))
	          {
	            System.out.println("Bank date from link does not have a valid signature: certificate ID = "+decryptedCertData);
	            isReauthSuccessful = false;
	          }
	         else
	          {
	            // Remove hard coded characters from the date string.
	            // The string is sent in as YYYY-MM-DDThh:mm:ssZ
	            //    For example, 2002-01-15T02:13:22Z
	            // Removed the T and the Z
	            char[] decryptedDateChars = decryptedBankDate.toCharArray();
	            decryptedDateChars[10] = ' ';
	            decryptedBankDate = new String(decryptedDateChars, 0, 19); 
	            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	            Date formattedBankDate = null;
	            Date currentTime = new java.util.Date();
            
	            try {
	                 formattedBankDate = formatter.parse(decryptedBankDate);
        	    } catch (ParseException e) {
	                 isReauthSuccessful = false;
	            }
            
	            int toleranceAmt = 5;
	            GregorianCalendar upperBound = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
	            upperBound.setTime(currentTime);
	            upperBound.add(Calendar.MINUTE, toleranceAmt);

	            toleranceAmt *= -1;

	            GregorianCalendar lowerBound = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
	            lowerBound.setTime(currentTime);
	            lowerBound.add(Calendar.MINUTE, toleranceAmt);

	            if (formattedBankDate.after(lowerBound.getTime()) && formattedBankDate.before(upperBound.getTime())) {
	                 isReauthSuccessful = true;
	            } else {
	                 isReauthSuccessful = false;
	                 System.out.println("Certificate logon failed because of invalid date stamp");
	                 System.out.println("   Cert ID: "+decryptedCertData);
	                 System.out.println("   Bank Date: "+formattedBankDate);
	                 System.out.println("   Portal Lower Bound: "+lowerBound.getTime());
	                 System.out.println("   Portal Upper Bound: "+upperBound.getTime()); 
        	    }
	         }
	       }
       } 

      //////////////////////////////////////////
      // PERFORM BANK SPECIFIC LOGIC TO       //
      // GET IDENTIFYING INFO FROM THE CERT   //
      //////////////////////////////////////////

      // Each client bank can choose a different set of fields to comprise
      // the identifying information for a user.   One client bank may
      // choose e-mail address, for example.  Another may choose to append
      // the phone number and last name.

      // Bank specific logic is run to pull field(s) off of the certificate
      // and build them into identifying information.  The 'certificateId'
      // variable is used to hold the identifying information.

      // The value set for 'certificateId' is used to look up the user by
      // the certificate_id field.

      // If there are no problems so far, proceed with determining the certificate ID
      if (isReauthSuccessful)
       {
          if(checkingForRealCertificate)
           {
            // Pull certificate ID off of certificate
            if(orgId.equals(TradePortalConstants.GLOBAL_ORG_ID))
             {
	       String[] emailAddresses = ci.getEmailAddressesFromSubjectAltName();
               if (emailAddresses.length != 0)
                  certificateId = emailAddresses[0];  
             }
            else if(orgId.equals("BAR"))
             {
               certificateId = ci.getEmailAddress();  
             }
            else if(orgId.equals("BMG"))
             {
	       String[] emailAddresses = ci.getEmailAddressesFromSubjectAltName();
               if (emailAddresses.length != 0)
                  certificateId = emailAddresses[0];  
             }
            else if(orgId.equals("ANZ"))
             {
               certificateId = ci.getCertName();  
             }
           }
          else
           {
             // We're in testing mode, there is no real certificate, so
             // just assume that the certifcate ID passed in the URL is the actual one
             certificateId = decryptedCertData;
           }


           if(verifySignedData)
            {

	         // Verify that the certificate ID determined by looking at the certificate
	         // matches the decrypted certificate ID sent in the URL
	         if( (certificateId == null) || !certificateId.equals(decryptedCertData) )
	          {
	               System.out.println("Encrypted certificate data does not match data on certificate. Data on cert: "+certificateId+" : Data in URL: "+decryptedCertData);
	               isReauthSuccessful = false;
	          }
             }
       }

     }
     else {// no proper authentication method specified.  This really should not happen. 
     
            System.out.println("Authentication method not specified");
           
            %>
             <%@ include file="fragments/recertificationErrors.frag" %>
          <%
  }
  
 if(isReauthSuccessful)
  {
   ///////////////////////////////////////////
   // RECERTIFICAION WAS SUCCESSFUL         //
   ///////////////////////////////////////////

   // Forward to the CompleteRecertification page, which will return control to the Portal page to complete
   // Authorization
   String url = NavigationManager.getNavMan().getPhysicalPage("CompleteRecertification", request);
 
   //TradePortalRecertification will set this to YES, when recertification is successful
   session.setAttribute("recertificationSuccessful", TradePortalConstants.INDICATOR_YES);
   Debug.debug("session attribute recertificationSuccessful = " + session.getAttribute("recertificationSuccessful"));   
         %>
           <jsp:forward page='<%= url %>'/>
       <%
   }
   else
   {
          //////////////////////////////////////////////
          // THERE WAS A PROBLEM WITH THE CERTIFICATE //
          // OR WITH THE DATA IN THE URL PARAMETERS   //
          //////////////////////////////////////////////

          // Issue an error indicating that the certificate is not valid

         %>
         <%@ include file="fragments/recertificationErrors.frag" %>
         <%
   }
 %>
