<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.common.*, com.ams.tradeportal.html.*, com.amsinc.ecsg.util.DocumentHandler, java.util.*,com.ams.tradeportal.busobj.webbean.*" %>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"      scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"  class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%--cquinton 9/19/2012 ir#5507 removed expires meta tag - header.jsp should always be first so doctype is first.
    dispatcher.jsp ensures the expires header is sent regardless--%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%   // Determine if there is a SiteMinder Web Agent SSO session that should be terminated
  boolean isThereASiteMinderWebAgentSession = userSession.isUsingSiteMinderWebAgentSSO();
  //BSL CR 749 05/15/2012 Rel 8.0 END
  
  //cquinton 4/30/2013 Rel 8.1.0.6 ir#16255 start
  PropertyResourceBundle tpProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");	
  //read ssowebagent logout url, if it exists, null means use hardcoded value
  String ssoWebAgentLogoutUrl = null;
  if ( isThereASiteMinderWebAgentSession ) {
    try {
      ssoWebAgentLogoutUrl = tpProperties.getString("WebAgent_SSO_Logout_URL");
    } catch ( Exception ex ) {
      // Report there was a problem and and set to null so default is used below
      System.out.println("TradePortalTimeout.jsp: problem reading WebAgent_SSO_Logout_URL. Defaulting");
      ssoWebAgentLogoutUrl = null;
    }
  }
  //cquinton 4/30/2013 Rel 8.1.0.6 ir#16255 start
  if (isThereASiteMinderWebAgentSession) {
    if ( ssoWebAgentLogoutUrl!=null && ssoWebAgentLogoutUrl.length()>0 ) {
      response.sendRedirect(ssoWebAgentLogoutUrl);
    }
    else {
      //go to the default web agent logout url
      //note: this really should use sendRedirect as well rather than refresh
      // as that flashes content and is deprecated, but to avoid risk
      // with ER deploy am leaving code as is...
%>
<meta http-equiv="refresh" content="0;url=/saam/SAAMLogin/logout.fcc" />
<%
    }
  }
  //cquinton 4/30/2013 Rel 8.1.0.6 ir#16255 end
%>



<div class="pageMainNoSidebar">
  <div class="pageContent">

  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="Timeout.Title" />
    <jsp:param name="helpUrl"  value="/customer/timed_out.htm" />
  </jsp:include>

<form>
      
  <div class="formContentNoSidebar padded">

    <div class="instruction">
<%-- BSL CR 749 05/16/2012 Rel 8.0 BEGIN --%>
<%
	if (userSession.isUsingMaxSessionTimeout()) {
%>
		<%= resMgr.getText("Timeout.MaxSessionTimeoutMessagePart1", TradePortalConstants.TEXT_BUNDLE) %>
		<%= SecurityRules.getMaxSessionTimeOutPeriodInSeconds() / 3600 %>
		<%= resMgr.getText("Timeout.MaxSessionTimeoutMessagePart2", TradePortalConstants.TEXT_BUNDLE) %>
<%
	}
	else {
%>
      <%= resMgr.getText("Timeout.TimeoutMessagePart1", TradePortalConstants.TEXT_BUNDLE) %>
      <%= SecurityRules.getTimeOutPeriodInSeconds() / 60 %>
      <%= resMgr.getText("Timeout.TimeoutMessagePart2", TradePortalConstants.TEXT_BUNDLE) %>
<%
	}
%>
<%-- BSL CR 749 05/16/2012 Rel 8.0 END --%>
<%
  String autoSaved = request.getParameter("AutoSaved");
  if (autoSaved != null ) {
    if (autoSaved.equals("no")) { // Auto Save / Template Auto Save with errors
%>
      <%=resMgr.getText("Timeout.TimeoutMessageAutoSaveFailed", TradePortalConstants.TEXT_BUNDLE) %>
<%
    } 
    else if (autoSaved.equals("yes")) { // Auto save without errors
%>
      <%=resMgr.getText("Timeout.TimeoutMessageAutoSave", TradePortalConstants.TEXT_BUNDLE) %>
<%
    }
    else if (autoSaved.equals("template")){ // Template auto save without errors
%>
      <%=resMgr.getText("Timeout.TimeoutMessageAutoSaveTemplate", TradePortalConstants.TEXT_BUNDLE) %>
<%
    }
  }
  else {
%>
      <%=resMgr.getText("Timeout.TimeoutMessageNoAutoSave", TradePortalConstants.TEXT_BUNDLE) %>
<%
  }
%>
    </div>

    <jsp:include page="/common/ErrorSection.jsp" />

  </div>

</form>


  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>

<%
	//Ravindra - 21/01/2011 - CR-541 - Start
	//Ravindra - 03/31/2011 - CUUL032965287 - Start
   // T36000013446- use custom http header to communicate CMA specific status codes -start
   try
		{
	   		UserWebBean userWebBean = beanMgr.createBean(UserWebBean.class, "User");
	   	  	userWebBean.getById(userSession.getUserOid());
	   	  
			if (null != userSession.getAuthMethod() &&  userSession.getAuthMethod().equals(TradePortalConstants.AUTH_AES256))
			{

 				String user = userWebBean.getAttribute("sso_id");
 				// 601 is CMA specific status code to notate "Trade Portal times out before the client bank portal"
			    // response.setStatus(HttpResponseUtility.getResponseCode( orgId, user, HttpResponseUtility.SESSION_TIMEOUT ));

 				String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( userSession.getClientBankCode(), userSession.getSsoId(), HttpResponseUtility.SESSION_TIMEOUT ));
	  		   	response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
			}
		}
		catch (Exception any)
		{
			any.printStackTrace();
		}   
	// T36000013446- use custom http header to communicate CMA specific status codes -end
		   
	//Ravindra - 03/31/2011 - CUUL032965287 - End
   //Ravindra - 21/01/2011 - CR-541 - End
   HttpSession theSession = request.getSession(false);

   theSession.invalidate();
%>
</body>
</html>
