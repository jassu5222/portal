<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This fragment contains the logic to read the ClientBank table to determine the 
 * URL to use in opening a Recertification window.
 *
 * This process is expecting the parameter auth=CERTIFICATE on the URL.
--%>

<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*, com.ams.tradeportal.busobj.util.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*, java.text.*, java.security.*" %>

<%-- jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session" / --%>
<%-- jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session" / --%>
<%-- jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session" / --%>
<%-- jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session" --%><%-- /jsp:useBean --%>

<%
     
     QueryListView                  queryListViewCC                   = null;
     StringBuffer                   sqlQueryCC                        = null;
  //////////////////////////////////////////////////////////
  // GET AND CHECK FOR THE PRESENCE OF PAGE PARAMETERS    //
  //////////////////////////////////////////////////////////     

  // Get page parameters 
  String orgId               = userSession.getLoginOrganizationId();
  String brandingDirectory   = userSession.getBrandingDirectory();
  String locale              = userSession.getUserLocale();
  String clientBankOid       = userSession.getClientBankOid();
  String certData            = request.getParameter("info");
  //String newLink             = "";

  // Check for an organization ID and branding directory - these are mandatory
  if(InstrumentServices.isBlank(orgId) || InstrumentServices.isBlank(brandingDirectory))
   {
     Debug.debug("A branding directory and organiztion ID must be specified");
     return;
   }
  else
   {
     // Make it so that the organization ID is not case sensitive
     orgId = orgId.toUpperCase();
   }

  // Check for optional locale
  if(InstrumentServices.isBlank(locale))
   {
     // Default to en_US if no locale is received
     locale = "en_US";
     Debug.debug("No locale specified in TradePortalLogon... defaulting to en_US");
   }


   //retrieve ClientBank from cache
   Cache cache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler result = (DocumentHandler)cache.get(clientBankOid);

   String requireCert = result.getAttribute("/ResultSetRecord(0)/REQUIRE_CERTS_FUND_FINAL_AUTH");
   
   if (InstrumentServices.isBlank(requireCert) || requireCert.equals(TradePortalConstants.INDICATOR_NO)) {
       Debug.debug("recertification not required");
       return;   
   }
   
   String certAuthURL = result.getAttribute("/ResultSetRecord(0)/AUTHORIZATION_CERT_AUTH_URL");
   if (InstrumentServices.isBlank(certAuthURL)) {
         Debug.debug("unable to display reauthentication page due to URL problem");
        return;  
   } else {
        // This process is expecting the parameter auth=CERTIFICATE on the URL, and will append the other parameters.
        certAuthURL = certAuthURL + "&organization=" + orgId + "&branding=" + brandingDirectory + "&locale=" + locale;
        if (!(InstrumentServices.isBlank(certData))) {
            System.out.println("Certificate data provided, cert = " + certData);
            certAuthURL = certAuthURL + "&info=" + certData;
        }
        newLink = "javascript:openReauthenticationWindow('" + certAuthURL + "')";
        Debug.debug("open window " + newLink);
   }
   
   //onLoad+="openReauthenticationWindow('" + certAuthURL + "')";
       
%>
 
   
 <script language="JavaScript"> 
       function openReauthenticationWindow (url) 
       {

           winHeight=300;
           winWidth=600;  
           widgets = 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,';
           widgets = widgets + 'width='+winWidth+',height='+winHeight;

           reauthenticationWin = window.open(url, 'Reauthentication', widgets);

           <%-- Center the window in the middle of the screen --%>
           horizLocation = (screen.width-winWidth)/2;
           vertLocation =  (screen.height-winHeight)/2;
           reauthenticationWin.moveTo(horizLocation,vertLocation);
   	   reauthenticationWin.focus();
             	
        }
   </script>          
