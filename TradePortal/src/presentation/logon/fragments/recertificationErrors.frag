<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, java.util.*, com.ams.tradeportal.common.*,com.ams.tradeportal.mediator.*" %>
<%--
 *   This fragment is responsible for handling authentication errors during the recertification process.
 *   When recertification fails, this indicator tells the authorization process to hold transactions
 *   requiring re-certification.  It will then forward to the JSP that will close the recertification popup.
 *
 *     Copyright  � 2002                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
    //When recertification fails, this indicator tells the authorization process to hold transactions
    //requiring re-certification.
    session.setAttribute("recertificationSuccessful", TradePortalConstants.INDICATOR_NO);

 
     // Forward the user to the error page, passing along parameter
     String url = NavigationManager.getNavMan().getPhysicalPage("CompleteRecertification", request);
         %>
           <jsp:forward page='<%= url %>'/>
          <%
%>