<%--
*******************************************************************************
                              Mail Message Page

  Description:
    This page is designed to display the dynamic Mail Message page.  The page
  is dynamic because it can be displayed in one of 9 different views.  In some
  views the buttons (like 'Reply to Bank'), the button is a submit button,
  in other views the button is a link.  Pay close attention to that, because
  it will affect whether or not a mediator is called.  For example, the Route
  Button sometimes will call the RouteDeleteMediator and other times it will
  call the MailMessage Mediator.  Generally there are 2 modes the Mail Message
  Mediator is called in =>
  Message Mode:  'C' = 'Create' a new message.
             'U' = 'Update' an existing message.
  This informs the mediator whether or not we are updating a preExisting message.

    This Particular jsp is creating an ejb instead of creating a webBean.
  This was intentional because the un_read flag needs to be updated prior to
  leaving the page when a message is read for the first time.  The only way to
  accomplish this is to use an ejb rather than a webBean.

  The basic views this page be displayed in are as follows:

  View                        Condition                     Read Only Status (in general)
  ---------------------------------------------------------------------------------------------------
  New Mail Message            Message oid passed in is null       False
                        isReply flag == false
                        isEditFlage == false

  Draft Mail Message          Message oid was passed in           False
                        MessageStatus = 'DRAFT'
                        isEdit = 'true' OR Message is assigned to you
                              OR Message is assgined to no-One && is assigned to your corporate Org

  Mail Message Rec From Bank  Message oid was passed in           True
                        Message was assigned to someone else (&& you have viewing rights)

  Mail Message Sent To Bank   Message oid was passed in           True
                        Message Status is 'SENT_TO_BANK'
                        isReply flag is not true

  MailMessage Rec From Bank   Message oid was passed in           True
                        Message Status is 'REC'
                        Discrepancy Flag is set to 'N' or ''

  Discrepancy Notice Rec/Bank Message oid was passed in           True
  'Not yet Started'           Message Status is 'REC'
                        Discrepancy Flag is set to 'Y'
                        Response Transaction oid is null

  Discrepancy Notice Rec/Bank Message oid was passed in           True
  'Started'             Message Status is 'REC'
                        Discrepancy Flag is set to 'Y'.
                        Response Transaction oid is defined with a value

  New Response from Mail Msg  Message oid passed in is null       False
                        isReply(url parm) is = 'true'
                        Related Inst was passed in via url Parm
                        subject was passed in via url Parm

  Response to Mail Message    Message oid passed in is null       False
                        message isReply is defined as a reply (on object)
                        Related Inst was passed in via url Parm
                        subject was passed in via url Parm

  Draft Response to Message   Message oid passed in is null       False
                        message status is 'DRAFT'
                        isReply(url parm) is = 'true'
                        Related Inst was passed in via url Parm
                        subject was passed in via url Parm
*******************************************************************************
--%>

<%--
 *          Dev Owner: Sandeep
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@page import="java.util.regex.Pattern"%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.busobj.*,
                 com.ams.tradeportal.html.*, java.text.*,java.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"          scope="session"></jsp:useBean>

<%
      WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   Debug.debug("***START******************** Mail Message Page *********************START***");
	//Variables to ensure Subject and InstrumentId is displayed as desired.DPatra 01-DEC-2012
	boolean isSubjOrInstrIdReadOnly=true;
	boolean isSubjOrInstrIdReq=false;

  //URL Parameter variables
  String    reply                   = request.getParameter("isReply");        //Reply button pressed
  String    edit                    = request.getParameter("isEdit");         //Edit button pressed
  String    relatedInstID           = request.getParameter("relatedInstID");
  String    subject                 = request.getParameter("subject");
  String    mailMessageOid          = request.getParameter("mailMessageOid");
  String    sequenceNumber          = request.getParameter("sequenceNumber");
  String    instrumentId          = request.getParameter("instrumentOid");
  
  String instrumentSearchDialogTitle=resMgr.getText("InstSearch.HeadingGeneric",TradePortalConstants.TEXT_BUNDLE);
  //cquinton 1/18/2013 remove old close action logic


  String    loginLocale             = userSession.getUserLocale();
  String    loginTimezone           = userSession.getTimeZone();
  String    loginRights             = userSession.getSecurityRights();
  String    userOid                 = userSession.getUserOid();
  String    corpOrgOid        = userSession.getOwnerOrgOid();
  String    clientBankOid           = userSession.getClientBankOid();
  String    goToMailMessageDetail   = "goToMailMessageDetail";
  Long            messageOid        = null;
  String    messageMode       = TradePortalConstants.MESSAGE_CREATE;
  //CR 375-D Krishna ATP-Response
  //String  instrumentOid;
  //String instrumentType;
  String    instrumentOid="";
  String    instrumentType="";
  String    instrumentTypeCode="";
  //CR 375-D Krishna ATP-Response
  String    transactionStatus = "";
  String    completeInstId          = "";
  String    displayInstrumentID     = "";
  int       stringLength            = 0;
  boolean       displayDiscrepancyReplyToBankButton = false;
  //CR-419 Krishna Begin
  TermsPartyWebBean    counterParty  = null;
  String    counterPartyOid          = null;
  String    counterPartyName         = null;
  //CR-419 Krishna End
  DocumentHandler defaultDoc        = formMgr.getFromDocCache();
  Hashtable       secureParms       = new Hashtable();
  ClientServerDataBridge csdb             = resMgr.getCSDB();

  String    serverLocation          = JPylonProperties.getInstance().getString("serverLocation");

  MailMessage     message;          //Business object - needed to do this since we needed to be able
                              //to update the un_read flag in the db with-in the jsp.
  InstrumentWebBean instrument            = beanMgr.createBean(InstrumentWebBean.class, "Instrument");
  UserWebBean     routedByUser            = beanMgr.createBean(UserWebBean.class, "User");
  TransactionWebBean transaction    = beanMgr.createBean(TransactionWebBean.class, "Transaction");

  //Message attribute related variables

  String    messageStatus           = "";
  String    messageText       = "";
  //CR-419 Krishna Begin
  String    discrepancyText              = "";
  String    presentationDate       = "";
  String    issueDate = "";
  String    maturityDate = "";
  String    poInvoiceDiscrepancyText     = "";
  //CR-419 Krishna End
  String    discrepancyFlag         = "";
  String    responseTransOid  = "";
  String    date;
  String    amount;                       //used on discrepancy pages
  String    currencyCode;                 //used on discrepancy pages

  DocumentHandler docImageList = null;          //used to display a list of document Image
                                    //links if the handler is non null.
  DocumentHandler myDoc;
  Vector listviewVector = null;                 //list of returned records formatted for easy looping.
  String bankInstrumentId = "";
  //Protect against null values passed in from calling jsp(s) regarding Page Parameters

  if( relatedInstID == null )       relatedInstID     = "";
  if( subject == null )       subject           = "";
  if( mailMessageOid == null )  mailMessageOid  = "";
  //Naveen IR-T36000011007(ANZ- 721) 02/02/2013- Added for Route button navigation- Start
  String    fromPage          = (String) session.getAttribute("fromPage");
  if(fromPage == null) {
	  session.setAttribute("fromPage", "MessagesHome");
  }
  //Naveen IR-T36000011007(ANZ- 721) 02/02/2013- Added for Route button navigation- End

  //If the Message Oid was not passed in through the the URL - look for it
  //in the session since the create Transaction process may have put it there
  //during a reply to bank.  If it's there, we dont need to decrypt it - but
  //we'll want to remove it from the session.

  if( InstrumentServices.isBlank(mailMessageOid) ){
      mailMessageOid = (String) session.getAttribute("mailMessageOid");

      if( InstrumentServices.isBlank( mailMessageOid ) ) {
            mailMessageOid    = defaultDoc.getAttribute("/In/MailMessage/message_oid");

            if( InstrumentServices.isBlank( mailMessageOid ) ){
                  mailMessageOid    = defaultDoc.getAttribute("/In/mailMessageOid");

                  if( InstrumentServices.isBlank( mailMessageOid ) )
                                    mailMessageOid = "";
            }
      }else{
            HttpSession theSession    = request.getSession(false);
            theSession.removeAttribute("mailMessageOid");
      }

  }else{
      mailMessageOid = EncryptDecrypt.decryptStringUsingTripleDes( mailMessageOid, userSession.getSecretKey() );
  }

  //Build the Message object...
  if( InstrumentServices.isNotBlank( mailMessageOid ) )
  {
      messageOid  = new Long(mailMessageOid);

      message      = (MailMessage)EJBObjectFactory.createClientEJB( serverLocation,
                                                      "MailMessage", messageOid.longValue(), csdb);

      // W Zhu Rel 8.1 T36000006807 #331 filter xss characters.
      responseTransOid = StringFunction.xssCharsToHtml(message.getAttribute("response_transaction_oid"));
      discrepancyFlag  = StringFunction.xssCharsToHtml(message.getAttribute("discrepancy_flag"));
      messageStatus      = StringFunction.xssCharsToHtml(message.getAttribute("message_status"));
      subject      = message.getAttribute("message_subject");
      messageText = message.getAttribute("message_text");
    //CR-419 Krishna Begin
    discrepancyText              = StringFunction.xssCharsToHtml(message.getAttribute("discrepancy_text"));
    presentationDate         = StringFunction.xssCharsToHtml(message.getAttribute("presentation_date"));
    poInvoiceDiscrepancyText = StringFunction.xssCharsToHtml(message.getAttribute("po_invoice_discrepancy_text"));
    //CR-419 Krishna End
      sequenceNumber    = StringFunction.xssCharsToHtml(message.getAttribute("sequence_number"));
      messageMode = TradePortalConstants.MESSAGE_UPDATE;

  }else{
      message      = (MailMessage)EJBObjectFactory.createClientEJB( serverLocation,
                                                                    "MailMessage", csdb);
      message.newObject();
      messageOid  = new Long( message.getAttribute("message_oid") );

  }

  //Need to populate the amount and CurrencyCode and set up for display.
  amount = StringFunction.xssCharsToHtml(message.getAttribute("discrepancy_amount"));
  currencyCode = StringFunction.xssCharsToHtml(message.getAttribute("discrepancy_currency_code"));

  if (InstrumentServices.isNotBlank(amount) && InstrumentServices.isNotBlank(currencyCode) )
      amount = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);

  //Build the Sql to get a list of Document Images from the Database.

  try{
      if( InstrumentServices.isNotBlank( mailMessageOid ) ) {
    	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
          // W Zhu 7/26/07 EEUH0725555228 add image_format
          // [START] IR-EEUH072555228 - jkok - Added doc_name
          // cquinton 7/21/2011 Rel 7.1.0 ppx240 - add hash
          String sql = "select doc_image_oid, form_type, image_id, image_format, doc_name, hash "+
          // [END] IR-EEUH072555228 - jkok
          "from document_image where mail_message_oid = ? "+
// [START] CR-186 - jkok -
// [BEGIN] IR-YRUH032953136 - jkok
          " and form_type <> ?";
// [END] IR-YRUH032953136 - jkok
// [END] CR-186 - jkok

          Debug.debug("*** Sql == " + sql.toString() );

          docImageList = DatabaseQueryBean.getXmlResultSet(sql, false, mailMessageOid, TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED);

         if( docImageList != null ) {
            Debug.debug("*** Returned Resultset is: " + docImageList.toString() );

            listviewVector = docImageList.getFragments("/ResultSetRecord/");
         }
      }
  }catch(Exception e){
      Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Document Image table *******");
     e.printStackTrace();
  }



  //boolean flags defined to help determine what should be displayed on the page (for each page)
  boolean   isMessageOidNull  = InstrumentServices.isBlank(mailMessageOid);

  boolean   isNewReply;

  // Set the edit flag based on either a variable in the URL or
  // a value that was submitted last time in a hidden field
  // Having the edit flag set overrides some other read-only rules
  if(reply == null)
   {
      if(defaultDoc.getAttribute("/In/isNewReply") != null)
            isNewReply=true;
      else
            isNewReply=false;
   }
  else
   {
      isNewReply = reply.equals("true");
   }


  boolean   isEdit;

  // Set the edit flag based on either a variable in the URL or
  // a value that was submitted last time in a hidden field
  // Having the edit flag set overrides some other read-only rules
  if(edit == null)
   {
      if(defaultDoc.getAttribute("/In/isEdit") != null)
            isEdit=true;
      else
            isEdit=false;
   }
  else
   {
      isEdit = edit.equals("true");
   }

  boolean   isCurrentUser           = message.getAttribute("assigned_to_user_oid").equals(userOid);
  boolean   hasEditRights           = SecurityAccess.hasRights(loginRights,SecurityAccess.CREATE_MESSAGE );
  boolean   hasSendRights           = SecurityAccess.hasRights(loginRights,SecurityAccess.SEND_MESSAGE );
  boolean   hasRouteRights          = SecurityAccess.hasRights(loginRights,SecurityAccess.ROUTE_MESSAGE);
  boolean   hasDelDocMsg            = SecurityAccess.hasRights(loginRights, SecurityAccess.DELETE_DOC_MESSAGE);
  
  boolean   hasDeleteRights         = SecurityAccess.hasRights(loginRights,SecurityAccess.DELETE_MESSAGE);
  
  boolean   hasEditDiscrepRights    = SecurityAccess.hasRights(loginRights,SecurityAccess.DISCREPANCY_CREATE_MODIFY);
  boolean   hasRouteDiscrepRights   = SecurityAccess.hasRights(loginRights,SecurityAccess.DISCREPANCY_ROUTE);
  boolean   hasDeleteDiscrepRights  = SecurityAccess.hasRights(loginRights,SecurityAccess.DISCREPANCY_DELETE);
  boolean   hasDeleteSITRights = false;
  boolean   hasEditSITRights = false;
  boolean   hasRouteSITRights = false;
  boolean   isUnAssigned            = InstrumentServices.isBlank(message.getAttribute("assigned_to_user_oid"));
  boolean   isRouted          = InstrumentServices.isNotBlank(message.getAttribute("last_routed_by_user_oid"));
  boolean   isUsersOrgOid;
  boolean   isDiscrepancy;
  boolean   isSettlementReq = false;
  boolean   isStatus          = InstrumentServices.isBlank( messageStatus );
  boolean   messageIsReply;
  boolean   isMsgFromBank;                                                  //message.Message_Source_Type
  boolean   isResponse        = InstrumentServices.isNotBlank( responseTransOid );  //Response from -Bank-
  boolean   isRelatedInst           = InstrumentServices.isBlank( relatedInstID );      //is : Related Inst passed in
  boolean   isReadOnly        = true;
  boolean       messageCanSendToBank    = false;
  //Commented  by dillip Dt 22-Feb-2013
  //boolean showDeleteAttachDocButton =false;
  MediatorServices mediatorServices = null;                                       //Populate Error section
  String    messageSourceType = message.getAttribute("message_source_type");
  String    headerPrefix            = "";
  String    headerSuffix            = "";
  String    headerTitle             = "";
  String    headerSuffixDate        = "";
                              //Either equals: "Close", "Cancel New Repsonse", or "Cancel New Message"
  String    cancelFlag        = "common.CloseText";
  String    cancelImg         = "common.CloseImg";
  String    cancelWidth       = "";

  //Since these variables will be used repetitively, do a check for null values.
  // if the variables are null, we default them here to a value.
  secureParms.put("last_entry_user_oid", message.getAttribute("last_entry_user_oid") );
  userSession.setCurrentPrimaryNavigation("NavigationBar.Messages");
  //cquinton 1/18/2013
  boolean returning = false;
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); //to keep it correct
    returning = true;
  }
  else {
    //cquinton 2/11/2013 - include request parameters
    userSession.addPage("goToMailMessageDetail", request);
  }

  //If the message object has been flagged as deleted, then don't allow the user to edit
  // the message.  Rather populate an error in the xml doc and store in the doc cache so
  // that when the header.jsp is included, it will be able to get the data from the cache
  // to populate the screen with the info.
  // This could happen if the message was updated by another user prior to this user pressing save.

  if( message.isDeleted() ) {
      Debug.debug("***** Message is deleted - Can't Edit it ****** ");
      isEdit = false;
      mediatorServices = new MediatorServices();
        mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
      mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.DELETED_BY_ANOTHER_USER);
      mediatorServices.addErrorInfo();

      defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
      formMgr.storeInDocCache("default.doc", defaultDoc);
      }

  //Default value-
  instrumentOid = message.getAttribute("related_instrument_oid");
  if (InstrumentServices.isBlank( instrumentOid ) ){
	  SessionWebBean.PageFlowRef previousPage = userSession.peekPageBack();
	  String linkAction = previousPage.getLinkAction();
	  if ("goToInstrumentSummary".equals(linkAction))
		  instrumentOid = (String)session.getAttribute(TradePortalConstants.INSTRUMENT_SUMMARY_OID);
  }

  //If we are coming from the instrument Summary page, then the instrumentOid
  //was placed onto the session there.

  //cquinton 1/18/2013 remove old close action logic

  if (defaultDoc.getDocumentNode("/In/InstrumentSearchInfo/InstrumentData") != null) {

     // We have returned from the instrument search page.  Based on the returned
     // data, retrieve the complete instrument ID of the selected instrument to
     // populate the Related Instrument ID in the General section
     String instrumentData = defaultDoc.getAttribute("/In/InstrumentSearchInfo/InstrumentData");

     instrumentOid  = InstrumentServices.parseForOid(instrumentData);
     completeInstId = InstrumentServices.parseForInstrumentId(instrumentData);
  }

  if( InstrumentServices.isNotBlank( instrumentOid ) ) {

      instrument.setAttribute("instrument_oid", instrumentOid );
      instrument.getDataFromAppServer();
      instrumentType = instrument.getAttribute("instrument_type_code");
      instrumentTypeCode=instrumentType; //rkrishna CR 375-D ATP 08/22/2007

      //instrumentType = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE, instrumentType);

      completeInstId = instrument.getAttribute("complete_instrument_id");
      bankInstrumentId = instrument.getAttribute("bank_instrument_id");

      if( InstrumentServices.isNotBlank( message.getAttribute("sequence_number") ) ) {

            sequenceNumber = message.getAttribute("sequence_number");
            displayInstrumentID = completeInstId + "-" + sequenceNumber;

      }else{
            displayInstrumentID = completeInstId;
      }
  }else if ( isNewReply && InstrumentServices.isNotBlank(relatedInstID) ) {
      if( InstrumentServices.isNotBlank(sequenceNumber)) {
            displayInstrumentID = relatedInstID + "-" + sequenceNumber;
      }else{
            displayInstrumentID = relatedInstID;
      }
      completeInstId = relatedInstID;
     
  }
  // CR 818 rel 9400 07/22/2015 Begin
  String settlementInstrFlag = message.getAttribute("settlement_instr_flag"); 
  if ( StringFunction.isNotBlank(settlementInstrFlag) &&  !TradePortalConstants.INDICATOR_NO.equals(settlementInstrFlag) ) {
	  isSettlementReq = true;
	  //hasDeleteSITRights   = SecurityAccess.hasRights(loginRights,SecurityAccess.SIT_DELETE);
	  hasDeleteSITRights   = SecurityAccess.hasRights(loginRights,SecurityAccess.DELETE_PREDEBIT_FUNDING_NOTIFICATION);
	  hasEditSITRights     = SecurityAccess.hasRights(loginRights,SecurityAccess.SIT_CREATE_MODIFY);
	  //hasRouteSITRights    = SecurityAccess.hasRights(loginRights,SecurityAccess.SIT_ROUTE);
	  hasRouteSITRights    = SecurityAccess.hasRights(loginRights,SecurityAccess.ROUTE_PREDEBIT_FUNDING_NOTIFICATION);
	  
	  amount = StringFunction.xssCharsToHtml(message.getAttribute("funding_amount"));
	  currencyCode = StringFunction.xssCharsToHtml(message.getAttribute("funding_currency"));

	  if (StringFunction.isNotBlank(amount) && StringFunction.isNotBlank(currencyCode) ) {
	      amount = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);
	  }
  }
  // CR 818 rel 9400 07/22/2015 End
  //CR 913 Start
  String fundingAmt = StringFunction.xssCharsToHtml(message.getAttribute("funding_amount"));
  String fundingDate = StringFunction.xssCharsToHtml(message.getAttribute("funding_date"));
  String fundingCurrency = StringFunction.xssCharsToHtml(message.getAttribute("funding_currency"));
  boolean isPreDebitFun = false;
  if((TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrumentTypeCode)
		  || StringFunction.isNotBlank(fundingAmt) || StringFunction.isNotBlank(fundingCurrency)
		  || StringFunction.isNotBlank(fundingDate) ) && !isSettlementReq ){
	  isPreDebitFun = true;
	  hasRouteRights          = SecurityAccess.hasRights(loginRights,SecurityAccess.ROUTE_PREDEBIT_FUNDING_NOTIFICATION);
	  hasDeleteRights          = SecurityAccess.hasRights(loginRights,SecurityAccess.DELETE_PREDEBIT_FUNDING_NOTIFICATION);
  }
  //CR 913 end 
  
   //CR-419 Krishna Begin
   // Retrieve the name of the counter party associated with the instrument
   counterPartyOid = instrument.getAttribute("a_counter_party_oid");
   if (!InstrumentServices.isBlank(counterPartyOid))
   {
      counterParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

      counterParty.getById(counterPartyOid);

      counterPartyName = counterParty.getAttribute("name");
   }
   else
   {
      counterPartyName = "";
   }
   //CR-419 Krishna End

//If the subject is not defined from a EJB nor the Url Parameter - Check the Xml Doc
//Maybe it was returned from a page call that came back to this page from earlier.

  if( defaultDoc.getAttribute("/In/MailMessage/message_subject") != null )
  {
      subject = defaultDoc.getAttribute("/In/MailMessage/message_subject");
  }

   //CR-419 Krishna Begin
   //If the  Presentation Date is not defined from a EJB - Check the Xml Doc
   //Maybe it was returned from a page call that came back to this page from earlier.
    if( defaultDoc.getAttribute("/In/MailMessage/presentation_date") != null )
   {
      presentationDate = defaultDoc.getAttribute("/In/MailMessage/presentation_date");
   }

 //If the  DiscrepancyText is not defined from a EJB - Check the Xml Doc
 //Maybe it was returned from a page call that came back to this page from earlier.

  if( defaultDoc.getAttribute("/In/MailMessage/discrepancy_text") != null )
   {
      discrepancyText = defaultDoc.getAttribute("/In/MailMessage/discrepancy_text");
   }
  //If the  POInvoiceDiscrepancyText is not defined from a EJB - Check the Xml Doc
  //Maybe it was returned from a page call that came back to this page from earlier.
  if( defaultDoc.getAttribute("/In/MailMessage/po_invoice_discrepancy_text") != null )
   {
      poInvoiceDiscrepancyText = defaultDoc.getAttribute("/In/MailMessage/po_invoice_discrepancy_text");
   }
  //CR-419 Krishna End

//If the Message Text is not defined from a EJB - Check the Xml Doc
//Maybe it was returned from a page call that came back to this page from earlier.

  if( defaultDoc.getAttribute("/In/MailMessage/message_text") != null )
   {
      messageText = defaultDoc.getAttribute("/In/MailMessage/message_text");
   }
  
  if( defaultDoc.getAttribute("/In/MailMessage/bank_instrument_id") != null )
  {
	  bankInstrumentId = defaultDoc.getAttribute("/In/MailMessage/bank_instrument_id");
  }

//If the Instrument ID is not defined from a EJB nor the Url Parameter - Check the Xml Doc
//Maybe it was returned from a page call that came back to this page from earlier.

  if( InstrumentServices.isBlank(completeInstId) &&
      (defaultDoc.getAttribute("/In/Instrument/complete_instrument_id") != null)) {
            Debug.debug("************** Made it HERE 5 *************");
      completeInstId = defaultDoc.getAttribute("/In/Instrument/complete_instrument_id");
      displayInstrumentID = completeInstId;
  }

  if( InstrumentServices.isBlank(bankInstrumentId) &&
	      (defaultDoc.getAttribute("/In/Instrument/bank_instrument_id") != null)) {
	            Debug.debug("************** Made it HERE bankInstrument Id *************");
	     bankInstrumentId = defaultDoc.getAttribute("/In/Instrument/bank_instrument_id");
	  }


  if( InstrumentServices.isBlank(message.getAttribute("is_reply")) ||
      message.getAttribute("is_reply").equals(TradePortalConstants.INDICATOR_NO ) ) {
                        messageIsReply = false;
  }else{                messageIsReply = true;
                        messageMode = TradePortalConstants.MESSAGE_UPDATE;
  }

  if( InstrumentServices.isBlank(discrepancyFlag) ||
      discrepancyFlag.equals(TradePortalConstants.INDICATOR_NO) )
                        isDiscrepancy = false;
  else                        isDiscrepancy = true;

  if( InstrumentServices.isBlank(messageSourceType) ||
      messageSourceType.equals(TradePortalConstants.PORTAL) )
                        isMsgFromBank = false;
  else                        isMsgFromBank = true;

  if( InstrumentServices.isBlank(message.getAttribute("assigned_to_corp_org_oid")) ||
      !corpOrgOid.equals(message.getAttribute("assigned_to_corp_org_oid")) )
                        isUsersOrgOid = false;
  else                        isUsersOrgOid = true;


  //We need to determine if the page needs to display the text as readOnly or as editable data.
  //Consequently there are a set of rules with which to determine this...

  //If the Message originated in OTL -OR- The Message is a Discrepancy -OR-
  //User doesn't have right to edit/create messages set Page into ReadOnly mode
  //*** Then the read only mode is already defaulted to true ***

                                                      //If the message is assigned to the
                                                      //current user -OR- is unassigned and
                                                      //is owned by the users corporation.
  if(messageSourceType.equals( TradePortalConstants.BANK ) ||
     !SecurityAccess.hasRights(loginRights,SecurityAccess.CREATE_MESSAGE) ||
     messageStatus.equals(TradePortalConstants.REC) ||
     messageStatus.equals(TradePortalConstants.SENT_TO_BANK) ||
     messageStatus.equals(TradePortalConstants.SENT_TO_BANK_DELETED) ||
     messageStatus.equals(TradePortalConstants.REC_DELETED) ||
     messageStatus.equals(TradePortalConstants.REC_ASSIGNED)) {

       isReadOnly = true;                                   //If the message is assigned to the
                                                      //current user -OR- is unassigned and
                                                      //is owned by the users corporation.
  }else if( isCurrentUser || (isUnAssigned && isUsersOrgOid) ||
          isEdit || isMessageOidNull) {

       isReadOnly = false;
  }

  //IR - LRUH011547224 - BEGIN
  //Below conditions will determine whether to display "Send To Bank" button or not
  if(messageSourceType.equals( TradePortalConstants.BANK ) ||
     messageStatus.equals(TradePortalConstants.REC) ||
     messageStatus.equals(TradePortalConstants.SENT_TO_BANK) ||
     messageStatus.equals(TradePortalConstants.SENT_TO_BANK_DELETED) ||
     messageStatus.equals(TradePortalConstants.REC_DELETED) ||
     messageStatus.equals(TradePortalConstants.REC_ASSIGNED)) {

            messageCanSendToBank = false;

  }else if( isCurrentUser || (isUnAssigned && isUsersOrgOid) ||
          isEdit || isMessageOidNull) {

            messageCanSendToBank = true;
  }
  //IR - LRUH011547224 - END


  if( isRouted ) {
      routedByUser.setAttribute("user_oid", message.getAttribute("last_routed_by_user_oid"));
      routedByUser.getDataFromAppServer();
  }


  if( isResponse ) {
      transaction.setAttribute("transaction_oid", responseTransOid);
      transaction.getDataFromAppServer();
      transactionStatus = transaction.getAttribute("transaction_status");
  }

  // PLUI070959610 begin
  if ( isDiscrepancy || isSettlementReq) {
      if( !isResponse || (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_DELETED) )) {
               displayDiscrepancyReplyToBankButton = true;
      }
  }
  // PLUI070959610 end

  //Check to see if errors were returned in the document, if so -
  //Populate the variables with values from the xml document and NOT from the EJB.


  if( InstrumentServices.isBlank( sequenceNumber ) )
      sequenceNumber = "";

  //Set up the secure parameters....
      //This set of parameters is set up for the Transaction creation (Reply to Bank)
      //The textfields have a higher order of precedence than the secureParms does in
      //the formMgr.getFormInstanceAsInputField call.  So if a field is updated - or
      // NOT in read only mode than the value from that field will be what is passed
      //to the mediator if both the input field and the secureParm share the same
      //mapping in the Mapfile...
  secureParms.put( "instrumentOid", instrumentOid + "/");
  secureParms.put( "mode", TradePortalConstants.EXISTING_INSTR );
  secureParms.put( "clientBankOid", clientBankOid );
  secureParms.put( "userOid", userOid );
  secureParms.put( "ownerOrg", corpOrgOid );
  secureParms.put( "securityRights", loginRights );
  secureParms.put( "mailMessageOid", mailMessageOid );
  secureParms.put( "startPage", "MailMessageDetail" );
  if ( isSettlementReq ) { 
	  secureParms.put( "transactionType", TransactionType.SIR );
  } else {
    //rkrishna CR 375-D ATP 07/26/2007 Begin
    if(instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY))
       secureParms.put( "transactionType", TransactionType.APPROVAL_TO_PAY_RESPONSE );
    else
    //rkrishna CR 375-D ATP 07/26/2007 End
       secureParms.put( "transactionType", TransactionType.DISCREPANCY );
  }
  secureParms.put( "validationState", TradePortalConstants.VALIDATE_NOTHING );
  secureParms.put( "MessageMode", messageMode);
  secureParms.put( "timeZone", loginTimezone);
  secureParms.put( "user_oid", userOid);
  secureParms.put( "message_oid", mailMessageOid);
  secureParms.put( "complete_instrument_id", completeInstId);
  secureParms.put( "sequence_number", sequenceNumber);
  secureParms.put( "instrument_type_code", instrumentTypeCode);
  secureParms.put( "fromListView", TradePortalConstants.INDICATOR_NO);

  //Document Image link related Variables.

  String    urlParm;
  String    attribute;
  String    displayText;
  String    encryptedMailMessageOid;
  String    encryptVal2;
  String    encryptVal3;

  //cquinton 9/15/2011 Rel 7.1 ppx240
  encryptedMailMessageOid = EncryptDecrypt.encryptStringUsingTripleDes( mailMessageOid, userSession.getSecretKey() );
  secureParms.put("MessageOid", encryptedMailMessageOid);
  secureParms.put("fromMessages", TradePortalConstants.INDICATOR_YES);
  secureParms.put("SecurityRights", loginRights);
  secureParms.put("UserOid", userOid);



  if( isNewReply ) {
       secureParms.put( "is_reply", TradePortalConstants.INDICATOR_YES);
       secureParms.put( "assigned_to_corp_org_oid", message.getAttribute("assigned_to_corp_org_oid"));
  }else{
       secureParms.put( "is_reply", TradePortalConstants.INDICATOR_NO);
       secureParms.put( "assigned_to_corp_org_oid", corpOrgOid);
  }
	//Naveen- Added for Message Details Subject issue.
	String newSubject="";
	int lastIndex=0;
  	lastIndex= subject.indexOf(":");
  	String translatedTitle = "";
 	if(lastIndex != -1) {
 		//PPX-48784 use locale and convert
		newSubject = subject.substring(0, lastIndex);
		translatedTitle = ReferenceDataManager.getRefDataMgr().getDescr("MESSAGE_SUBJECT",
				newSubject,loginLocale);
  	}
  	else {
  		newSubject = subject;
  	}



%>

<%-- ********************************** HTML STARTS HERE **************************************** --%>



<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/ButtonPrep.jsp" />
<script language="JavaScript">

   <%--
   // This function is used to display a popup message confirming
   // that the user wishes to delete the selected items in the
   // listview.
   --%>
   function confirmDocumentDelete()
   {
      var   confirmMessage = "<%=resMgr.getText("PendingTransactions.PopupMessage",TradePortalConstants.TEXT_BUNDLE) %>";

      <%-- Find any checked checkbox and flip a flag if one is found --%>
      var isAtLeastOneDocumentChecked = Boolean.FALSE;
      if (document.forms[0].AttachedDocument != null) {
         if (document.forms[0].AttachedDocument.length != null) {
            for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
               if (document.forms[0].AttachedDocument[i].checked==true) {
                  isAtLeastOneDocumentChecked = true;
               }
            }
         }
         else if (document.forms[0].AttachedDocument.checked==true) {
            isAtLeastOneDocumentChecked = true;
         }
      }

      if (isAtLeastOneDocumentChecked==true) {
         if (confirm(confirmMessage)==false)
         {
            <%-- Uncheck any checked checkboxes --%>
            if (document.forms[0].AttachedDocument != null) {
               if (document.forms[0].AttachedDocument.length != null) {
                  for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
                     if (document.forms[0].AttachedDocument[i].checked==true) {
                     document.forms[0].AttachedDocument[i].checked = false;
                     }
                  }
               }
               else if (document.forms[0].AttachedDocument.checked==true) {
                  document.forms[0].AttachedDocument.checked = false;
               }
            }
            <%-- [BEGIN] IR-YRUH032953136 - jkok --%>
            formSubmitted = false;
            <%-- [END] IR-YRUH032953136 - jkok --%>
            return false;
         }
         else
         {
            <%-- [BEGIN] IR-YRUH032953136 - jkok --%>
            formSubmitted = true;
            <%-- [END] IR-YRUH032953136 - jkok --%>
            return true;
         }
      }
      else {
         <%-- [BEGIN] IR-YVUH032339608 - jkok --%>
         alert("<%=resMgr.getText("common.DeleteAttachedDocumentNoneSelected", TradePortalConstants.TEXT_BUNDLE) %>");
         <%-- [END] IR-YVUH032339608 - jkok --%>
         <%-- [BEGIN] IR-YRUH032953136 - jkok --%>
         formSubmitted = false;
         <%-- [END] IR-YRUH032953136 - jkok --%>
         return false;
      }
  }


</script>

      <div class="pageMain">
            <div class="pageContent">
                  <form id="MessagesDetail" name="MessagesDetail" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
                        <input type="hidden" name="buttonName" value="">

    <%
     /***************************
      * Title Bar
      **************************/
      Debug.debug("*** Title Bar ***");
    %>

      <%-- The Text Display that needs to go here has alot of required logic, since this jsp               --%>
      <%-- could have upto 9 -or more- different views in this particular jsp.                    --%>
      <%-- Because there is no easy way to determine which look this jsp requires, each control for the --%>
      <%-- jsp will require a certain amount of logic.  Most of the logic will must call boolean variables. --%>
      <%-- The Page header is broken up into the Prefix and Suffix.  The Suffix text will be in      --%>
      <%-- parenthesis and look like: (Assigned: 02 Janruary 2001 8:51PM).  After defining the prefix   --%>
      <%-- and suffix, the two are added in a final out.print statement.                          --%>
		<%
            String helpUrl = "/customer/mail_message.htm";
	    String tracer = resMgr.getText("Message.CreateTracers", TradePortalConstants.TEXT_BUNDLE);
    	    String correspondence = resMgr.getText("Message.Correspondence", TradePortalConstants.TEXT_BUNDLE);
      %>
      <%
            if( isMessageOidNull && !isNewReply ) {
            	  helpUrl = "/customer/new_mail_message.htm";
                  headerPrefix = resMgr.getText("Message.NewMailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
                  headerTitle = resMgr.getText("Message.NewMailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
                  cancelFlag = "common.CancelNewMessageText";
                  cancelImg  = "common.CancelNewMessageImg";
                  cancelWidth = "140";
				  isSubjOrInstrIdReadOnly=false;
				  isSubjOrInstrIdReq=true;

            }else if( !isMessageOidNull && (messageStatus.equals(TradePortalConstants.DRAFT)) && !messageIsReply )
            {
                 headerPrefix = resMgr.getText("Message.DraftMailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
                 headerTitle = resMgr.getText("Message.DraftMailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
				  isSubjOrInstrIdReadOnly=false;
				  isSubjOrInstrIdReq=true;

            }else if( isDiscrepancy ) {
                  //headerPrefix = resMgr.getText("Message.DiscrepancyNoticeHeader", TradePortalConstants.TEXT_BUNDLE);
                  if (instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY)) {
                	  	helpUrl = "/customer/approval_to_pay_response.htm";
                        headerPrefix = resMgr.getText("Message.ATPNoticeHeader", TradePortalConstants.TEXT_BUNDLE);
                        headerTitle = resMgr.getText("Message.ATPNoticeHeader", TradePortalConstants.TEXT_BUNDLE);
                        } else {
                        //headerPrefix = resMgr.getText("Message.MailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
                        //headerPrefix = newSubject;
                        // IR T36000048415 Rel9.5 05.03/2016
                        headerPrefix = resMgr.getText("Message.DiscrepancyNoticeHeader", TradePortalConstants.TEXT_BUNDLE);
                        headerTitle = resMgr.getText("Message.MailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
                        }
            }else if( isMessageOidNull && isNewReply ) {
                        headerPrefix = resMgr.getText("Message.ResponseToMailMessage", TradePortalConstants.TEXT_BUNDLE);
                        headerTitle = resMgr.getText("Message.ResponseToMailMessage", TradePortalConstants.TEXT_BUNDLE);
                        cancelFlag = "common.CancelNewResponseText";
                        cancelImg  = "common.CancelNewResponseImg";
                        cancelWidth = "140";

                        date = DateTimeUtility.getCurrentDateTime();
                        SimpleDateFormat isoFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                        Date convertedDate = TPDateTimeUtility.getLocalDateTime( date, loginTimezone );
                  date = TPDateTimeUtility.formatDateTime( isoFormat.format(convertedDate), "1", loginLocale);
                  headerSuffixDate = date;
            }else if( (messageStatus.equals(TradePortalConstants.DRAFT)) && !isMessageOidNull && messageIsReply) {
                        headerPrefix = resMgr.getText("Message.DraftResponseHeader", TradePortalConstants.TEXT_BUNDLE);
                        headerTitle = resMgr.getText("Message.DraftResponseHeader", TradePortalConstants.TEXT_BUNDLE);
            }else if( messageIsReply ) {
                        headerPrefix = resMgr.getText("Message.ResponseToMailMessage", TradePortalConstants.TEXT_BUNDLE);
                        headerTitle = resMgr.getText("Message.ResponseToMailMessage", TradePortalConstants.TEXT_BUNDLE);
            }else if( isSettlementReq ) {
            			helpUrl = "/customer/Settlement_Instruction_Request.htm";
            	        headerPrefix = resMgr.getText("MailMessage.SettlementInstrReq", TradePortalConstants.TEXT_BUNDLE);
                        headerTitle = resMgr.getText("Message.MailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
            }else{
                        headerPrefix = newSubject;
                        headerTitle = resMgr.getText("Message.MailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
            }

            if( isMsgFromBank ) {
            	headerSuffix = resMgr.getText("Message.ReceivedFromBank", TradePortalConstants.TEXT_BUNDLE);

            	if(newSubject.equals(tracer))
                  	helpUrl = "/customer/Tracer_Mail_Message.htm";
            	if(newSubject.equals(correspondence))
                    helpUrl = "/customer/mail_message.htm";
            }else if( messageStatus.equals(TradePortalConstants.SENT_TO_BANK) ) {
            			helpUrl = "/customer/sent_to_bank_detail.htm";
                        headerSuffix = resMgr.getText("Message.SentToBank", TradePortalConstants.TEXT_BUNDLE);
                        headerPrefix = "";
            }else if( isRouted ) {
            			helpUrl = "/customer/routed_to_you_by.htm";
                        headerSuffix = resMgr.getText("Message.Assigned", TradePortalConstants.TEXT_BUNDLE);
            }else{
                  headerSuffix = "";
            }
            if(isMsgFromBank && isRouted && (!(newSubject.equals(tracer)) && !(newSubject.equals(correspondence)))){
            	helpUrl = "/customer/routed_to_you_by.htm";
            }

            //If the header Suffix is not an empty string - meaning a date needs to be displayed in Parens...
            //Need to format the date into the iso format first (Surprised jPylon does'nt offer a method to do this.)
            // the retreived data from the Db looks like: '05/05/2001 00:00:00'
            // therefore seperating the year and the time will be a little cumbersome.

            // You should never need to display the date if we're NOT in ReadOnly mode. (except for if the message
              // has been routed to the user

            if( isReadOnly || (isRouted && !isPreDebitFun) ) {
                  date = message.getAttribute("last_update_date") + ".0";
                  SimpleDateFormat isoFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                  Date convertedDate = TPDateTimeUtility.getLocalDateTime( date, loginTimezone );

                  date = TPDateTimeUtility.formatDateTime( isoFormat.format(convertedDate), "1", loginLocale);
                  headerSuffixDate = date;
            }
            String tempHeaderPrefix = headerPrefix;
      %>


                        <jsp:include page="/common/PageHeader.jsp">
                           <jsp:param name="titleKey" value="<%=headerTitle%>" />
                           <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
                        </jsp:include>

                        <div class="subHeaderDivider"></div>

                        <div class="pageSubHeader">
                          <span class="pageSubHeaderItem">
                           <%  //PPX-48784 -
                          		if(StringFunction.isNotBlank(translatedTitle) && !headerPrefix.equals("")) {
                          			headerPrefix=translatedTitle;
                          		}
                          %>
                              <% if(!headerPrefix.equals("")){ %>
                                    <span><%=StringFunction.xssCharsToHtml(headerPrefix)%></span>
                              <% } if(!headerSuffix.equals("")){ %>
                                    <% if(!headerPrefix.equals("")){ %>
                                          <span>&nbsp;-&nbsp;</span>
                                    <% } %>
                                    <span><%=StringFunction.xssCharsToHtml(headerSuffix)%></span>
                              <% } if(!headerSuffixDate.equals("")){ %>
                                    <span>&nbsp;-&nbsp;</span>
                                    <span><%=headerSuffixDate%></span>
                              <% } %>
                          </span>
<%--                           <span class="pageSubHeader-right">
         --%>
         <div style="clear:both;"></div>
                        </div>

                  <div class="formArea">
                        <jsp:include page="/common/ErrorSection.jsp" />

                        <%--cquinton 2/11/2013 add border--%>
                        <div class="formContent formBorder">
                              <br/>

                              <%
                                    if( messageStatus.equals(TradePortalConstants.SENT_TO_BANK) || (isMessageOidNull && !isNewReply)){
                                          if( isNewReply ){
                                          String re = resMgr.getText("Message.Re", TradePortalConstants.TEXT_BUNDLE);
                                          if(!subject.startsWith(re)){
                                                subject = re + "  " + subject;
                                          }
                                          if(subject.length() > 60){
                                          subject = subject.substring(0, 56) + "...";
                                          }

                                                // Add to secure parms since the field will be read only
                                                secureParms.put("message_subject", subject);
                                          }
                                          if( messageIsReply || isReadOnly ){
                                                // Add to secure parms since the field will be read only
                                                secureParms.put("message_subject", subject);
                                          }

                                    }
                            //PPX-48784 - use locale to onvert subject
                              String convertedSubj = subject;
                              if(StringFunction.isNotBlank(translatedTitle)){
                                	convertedSubj = convertedSubj.replace(tempHeaderPrefix,translatedTitle);
                              }
                              final Pattern srcPattern = Pattern.compile("[-]\\s\\d+\\s");
                              String [] values=  srcPattern.split(subject);
                             
                              if(values.length==2){
                              String prodTypenEnglish = values[1] ;
                              String translatedProdType = ReferenceDataManager.getRefDataMgr().getDescr("MESSAGE_PRODUCT",
                            		  prodTypenEnglish,loginLocale);
                              	if(StringFunction.isNotBlank(translatedProdType)){
                             	 	convertedSubj =  convertedSubj.replace(prodTypenEnglish, translatedProdType)  ;
                              	}
                              }
                            //PPX-48784 end

               					 %>

               					<%=widgetFactory.createTextField("message_subject",
               					"Message.Subject", StringFunction.xssCharsToHtml(convertedSubj), "60", isSubjOrInstrIdReadOnly,
               					isSubjOrInstrIdReq, false, "style='width:420px;'", "", "")%>

                              <%-- This control is set up to display the complete Instrument ID WITH the sequence number. --%>
                              <%-- But since the seqeunce number is a seperate piece of data, it is NOT included in the   --%>
                              <%-- complete instrument id which we send in secureParms that is also mapped to the same    --%>
                              <%-- mapfile entry as this control.                                                                               --%>
                              <div>
                                    <%
                                          String ItemID1 = "complete_instrument_id,bank_instrument_id";
                                          String section1="InstrumentId";
                                          if(userSession.isCustNotIntgTPS()){
                          					section1="BankInstrumentId";
                                          }
                                          String instrId = "";
                                          String instruButton = "";
					  //Rel9.0 Adding instrument type 'PYB' to the instrument search dialog
                                          instruButton = widgetFactory.createInstrumentSearchButton(ItemID1,section1,isReadOnly,TradePortalConstants.SEARCH_ALL_INSTRUMENTS_FOR_MESSAGES, false);
                                          if( isReadOnly ){                                        	 
                                                StringBuffer addlParms = new StringBuffer();
                                                addlParms.append("&instrument_oid=");
                                                addlParms.append(EncryptDecrypt.encryptStringUsingTripleDes(instrumentOid, userSession.getSecretKey()));
                                                //addlParms.append("&fromPage="+fromPage);
                                                //Added by dillip 08/Feb/2013
                                                addlParms.append("&mailMessageOid=");
												if(encryptedMailMessageOid!=null){
													addlParms.append(encryptedMailMessageOid);
												}
                                                String instrLink = formMgr.getLinkAsHref("goToInstrumentSummary", displayInstrumentID, addlParms.toString(), response);

                                                instrId = "Message.RelatedToInstrumentID";

                                    %>

                                         <span id="instrIdHover">
                                         	<%=widgetFactory.createTextField("complete_instrument_id", instrId, instrLink, "19",true, false, false, "", "", "inline", instruButton)%>
                                          </span>
											<%=widgetFactory.createHoverHelp("instrIdHover","InstrumentHoverText")%>
									  <%-- CR-1026 06/30/2015 --%>
									  <% if(userSession.isCustNotIntgTPS() && StringFunction.isNotBlank(bankInstrumentId)){  %>
                                    	   <%=widgetFactory.createTextField("bank_instrument_id", "Message.BankInstrumentID", bankInstrumentId, "19", true, false, false, "",	"", "inline", "")%>							
                                    <%
									  }
                                          }
                                          if( !isReadOnly ){                                        	 
                                                instrId = "Message.ReferenceAnInstrumentID";
                                                StringBuffer addlParms = new StringBuffer();
                                                String instrLink = "";
	                                                addlParms.append("&instrument_oid=");
	                                                if(instrumentOid!=null ||instrumentOid!="" ){
	                                                	addlParms.append(EncryptDecrypt.encryptStringUsingTripleDes(instrumentOid, userSession.getSecretKey()));
	                                                }else{
	                                                	addlParms.append(EncryptDecrypt.encryptStringUsingTripleDes(instrumentId, userSession.getSecretKey()));
	                                                }
	                                                //Added by dillip 08/Feb/2013
	                                                addlParms.append("&mailMessageOid=");
													if(encryptedMailMessageOid!=null){
														addlParms.append(encryptedMailMessageOid);
													}
	                                                //addlParms.append("&fromPage="+fromPage);
	                                                instrLink = formMgr.getLinkAsHref("goToInstrumentSummary", completeInstId, addlParms.toString(), response);
	                                                if(isSubjOrInstrIdReadOnly==true)
	                    							{
	                                                	
	                    						%>

                                                <span id="instrIdHover1">
                                                	 <%=widgetFactory.createTextField("complete_instrument_id", instrId, instrLink, "19",isSubjOrInstrIdReadOnly, isSubjOrInstrIdReq, false, "", "", "inline", instruButton)%>
												</span>
												<%=widgetFactory.createHoverHelp("instrIdHover1","InstrumentHoverText")%>
												
												<%
													}
	                                                else if (isSubjOrInstrIdReadOnly==false)
													{
	                                                	
												%>
												<%--  To display InstrumentId text Box for new mail Message and Draft Message created by New Mail Message DPATRA 01-Dec-2012--%>

												 <%=widgetFactory.createTextField("complete_instrument_id", instrId,completeInstId, "19", isSubjOrInstrIdReadOnly, false, false, "",	"", "inline", instruButton)%>

												<%-- <%=widgetFactory.createHoverHelp("InstrumentId","InstrumentHoverText")%> --%>
												<%
													}
												%>
                                  
                                    <%-- Search Instruments button --%>
                                    <%
                                                if( !isReadOnly && !isNewReply && !messageIsReply ) {
                                                	
                                    %>
                                                <input type="hidden" name="NewSearch" value="Y">

                                    <%
                                                      // Send flag indicating that user is able to view messages of their children
                                                if(SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_CHILD_ORG_MESSAGES )){
                                    %>
                                                      <input type="hidden" name="ShowChildInstruments" value="<%=TradePortalConstants.INDICATOR_YES%>">
                                    <%
                                                      }
                                                }
                                    
                                   				 if(userSession.isCustNotIntgTPS() ){ 
                                    %>
    	 							 <%-- CR-1026 06/30/2015 --%>
    	 							 			<div class="formItem inline readOnly" >												
												
													<label for="bank_instrument_id"><%=resMgr.getText("Message.BankInstrumentID", TradePortalConstants.TEXT_BUNDLE) %></label>
													<br><div class="fieldValue" id="bank_instrument_id_display"><%=bankInstrumentId%></div>

													<input type="hidden" name="bank_instrument_id" id='bank_instrument_id' value=<%=bankInstrumentId%>>
												</div>
												 
												   <%--=widgetFactory.createTextField("bank_instrument_id_display", "Message.BankInstrumentID", bankInstrumentId, "19", true, false, false, "",	"", "inline", "")--%>
                                    		
                                    <% 
                                   				 }
                                          }
                                    %>

						


						<%------- Payment Date And Payment Amount Rpasupulati Start --%>
						<div style="clear:both"></div>
							<% if ( isSettlementReq ) {%>
								<%@ include file="fragments/MessageSettlementDetail.frag" %>
							<%} else {
								String paymentamount = StringFunction.xssCharsToHtml(message.getAttribute("funding_amount"));
 								 String paymentCurrencyCode = StringFunction.xssCharsToHtml(message.getAttribute("funding_currency"));

  							if (StringFunction.isNotBlank(paymentamount) && StringFunction.isNotBlank(paymentCurrencyCode) )
      												paymentamount = TPCurrencyUtility.getDisplayAmount(paymentamount, paymentCurrencyCode, loginLocale);
												%>
							<%	if (StringFunction.isNotBlank(paymentamount) || StringFunction.isNotBlank(message.getAttribute("funding_date")) ){ %>
												
												
							<%= widgetFactory.createTextField("Payment_Amount", "MailMessage.paymentamount", paymentCurrencyCode + "   " + paymentamount,
                                                      "35", true, false, false, "", "", "inline") %>
                                                     
				
                        				<%		
                        				
                        				String paymentdate="";
                        				paymentdate=StringFunction.xssCharsToHtml(message.getAttribute("funding_date"));
                        				if(StringFunction.isNotBlank(paymentdate)){ 
                        						/* Rel 9.3 IR#T36000038781 */
                        						Date presentationDateObj = new Date(StringFunction.xssHtmlToChars(paymentdate));
                                                paymentdate = TPDateTimeUtility.formatMessageDate(presentationDateObj , loginLocale);
                        				} 
                                   		 %>
                                   
                                          <%= widgetFactory.createTextField("paymentDate", "MailMessage.PaymentDate", paymentdate,"35", true, false, false, "", "", "inline") %>
                         <br/>
					
						<%	} 
						   }
						%>
						<%------- Payment Date And Payment Amount Rpasupulati End --%>


                                    <%
                                          //Discrepancy Amount
                                          if( isDiscrepancy ) {
                                    %>
                                                <%= widgetFactory.createTextField("PresentationAmount", "MailMessage.PresentationAmount", currencyCode + "   " + amount,
                                                      "35", isReadOnly, false, false, "", "", "inline") %>
                                    <%
                                          }

                                          //Routed by name
                                          //Naveen- Added "!isDiscrepancy" clause to below if condition as "Routed To You By" was displaying twice for Discrepancy Responses which are routed
                                          if( isReadOnly && isRouted && !isDiscrepancy) {
                                    %>
                                                <%= widgetFactory.createTextField("RoutedToYouBy", "Message.RoutedToYouBy", routedByUser.getAttribute("first_name") + "  " + routedByUser.getAttribute("last_name"),
                                                      "35", true, false, false, "", "", "inline") %>
                                    <%
                                          }
                                          if( (isDiscrepancy && isRouted) || (!isReadOnly && isRouted ) ) {
                                    %>
                                                <%= widgetFactory.createTextField("RoutedToYouBy", "Message.RoutedToYouBy", routedByUser.getAttribute("first_name") + "  " + routedByUser.getAttribute("last_name"),
                                                      "35", true, false, false, "", "", "inline") %>
                                    <%
                                          }
                                          if( isDiscrepancy ) {
                                    %>
                                                <%= widgetFactory.createTextField("OtherParty", "MailMessage.OtherParty", counterPartyName,
                                                      "35", isReadOnly, false, false, "", "", "inline") %>
                                    <%    } %>
                                    <div style="clear:both;"></div>
                              </div>

                              <div>
                                    <%
                                          if( isDiscrepancy ) {
                                    %>
                                          <%= widgetFactory.createTextField("PresentationNumber", "MailMessage.PresentationNumber", sequenceNumber,
                                                "35", isReadOnly, false, false, "", "", "inline") %>
                                    <%
                                                 //As "PresentationDate" is a newly added attribute empty check is being done here to avoid exceptions
                                                 //for the existing messages in the system.
                                                 if(InstrumentServices.isNotBlank(presentationDate)){
                                                 /* Rel 9.3 IR#T36000038781 */
                                                  Date presentationDateObj = new Date(StringFunction.xssHtmlToChars(presentationDate));
                                                  presentationDate = TPDateTimeUtility.formatMessageDate(presentationDateObj , loginLocale);
                                                 }
                                    %>
                                          <%= widgetFactory.createTextField("PresentationDate", "MailMessage.PresentationDate", presentationDate,
                                                "35", isReadOnly, false, false, "", "", "inline") %>
                                    <%    } %>
                                    <div style="clear: both;"></div>
                              </div>

                              <%
                                   /***************************
                                    * Document Image Links
                                    **************************/
                                    Debug.debug("*** Document Image Links ***");

                                    if( (docImageList != null) && (listviewVector != null) ) {
                              %>
                                          <%= widgetFactory.createLabel("", "Message.RelatedDocuments", false, false, false, "") %>
                              <%
                                          int liTotalDocs = listviewVector.size();
                                          int liDocCounter = liTotalDocs;
                                          String  displayTextPDF = "";
                                          encryptedMailMessageOid = EncryptDecrypt.encryptStringUsingTripleDes( mailMessageOid, userSession.getSecretKey() );
                                          for(int y=0; y < ((liTotalDocs+2)/3); y++) {
                                          for (int x=0; x < 3 ; x++) {
                                                      if (liDocCounter==0) break;

                                                      myDoc       = ((DocumentHandler)listviewVector.elementAt((y*3)+x));
                                                  String imageHash = myDoc.getAttribute("/HASH");
                                                  String encryptedImageHash = "";

                                                  if ( imageHash != null && imageHash.length()>0 ) {
                                                      encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( imageHash, userSession.getSecretKey() );
                                                } else {
                                                      //send something rather than nothing, which ensures we know we are not allowing
                                                        // attempts to view images when hash parm is just forgotten
                                                        encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( TradePortalConstants.NO_HASH, userSession.getSecretKey() );
                                                    }

                                                  // Display doc conditional on the format
                                                  String imageFormat = myDoc.getAttribute("/IMAGE_FORMAT");

                                                  if (imageFormat.equals(TradePortalConstants.PDF_IMAGE)) {
                                                            //Use correct display text depending on IMAGE_FORMAT
                                                            displayText = myDoc.getAttribute("/DOC_NAME");

                                                            //add hash parm
                                                            urlParm = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) + "&hash=" + encryptedImageHash;
                                                            
                                                            //T36000018206 --smanohar-start--changed the display text from 'Image' to 'PDF' and removed '|' seperator since there is no second viewing option avilable to user
                                                            displayTextPDF = resMgr.getText("Message.getImage", TradePortalConstants.TEXT_BUNDLE);
                              %>
                                                            <div class="formItem">
                                                                  <%= widgetFactory.createSubLabel(displayText+" ") %>
                                                                  <a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParm)%>' target='_blank'><%= displayTextPDF %></a>
															</div>	  
                              <%
                                                }else {
                                                            //Use correct display text depending on IMAGE_FORMAT
                                                            displayText = getDisplayText( myDoc );

                                                            //add hash parm
                                                            urlParm = buildURLParameters( listviewVector, myDoc, encryptedMailMessageOid, subject, displayText, encryptedImageHash, userSession );
                              %>
                                                            <div class="formItem">
                                                                  <%= widgetFactory.createSubLabel(displayText+" ") %>
                                                            <a href="<%= formMgr.getLinkAsUrl("goToDocumentView", urlParm, response)   %>">Image</a>
                                                            <%= widgetFactory.createSubLabel(" | ") %>
                              <%
                                                            displayTextPDF = resMgr.getText("Message.getImage", TradePortalConstants.TEXT_BUNDLE);
                                                            //add imageHash parm
                                                            urlParm = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) + "&hash=" + encryptedImageHash;
                              %>
                                                            <a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParm)%>' target='_blank'><%= displayTextPDF %></a>
                                                      </div>
                              <%
                                                      }//end else
                                                      liDocCounter--;
                                          }//end inner for "x"

                                          if (liDocCounter==0) {
                                                      for (int z=liTotalDocs % 3; z > 0; z--) {}
                                                }
                                    }//end outer for "y"
                                    } //Closes off If we have associated documents
                              %>

<%
                 /***************************
                  * Subject
                  **************************/
                  Debug.debug("*** Subject ***");
                  /* If this message is a reply to an earlier message, then we need to append a 'Re : ' to
                     the front the subject.  And if in doing this the subject exceeds 60 characters in length
                     then we need to truncate the excess off and add an ellipsis
                  */

                  if( isNewReply ){
                  String re = resMgr.getText("Message.Re", TradePortalConstants.TEXT_BUNDLE);
                  if(!subject.startsWith(re)){
                        subject = re + "  " + subject;
                  }
                  if(subject.length() > 60){
                  subject = subject.substring(0, 56) + "...";
                  }

                        // Add to secure parms since the field will be read only
                        secureParms.put("message_subject", subject);
                  }
                  if( messageIsReply || isReadOnly ){
                        // Add to secure parms since the field will be read only
                        secureParms.put("message_subject", subject);
                  }

                  if( !messageStatus.equals(TradePortalConstants.SENT_TO_BANK) && !(isMessageOidNull && !isNewReply)){
                  // Display subject in read only if page is in read only mode, reply is being created, or message has been saved and is a reply
            %>
                  <%-- <%= widgetFactory.createTextField("message_subject", "Message.Subject", subject,
                        "60", isReadOnly || isNewReply || messageIsReply, !isReadOnly, false, "style='width:420px;'", "", "") %> --%>
            <%
                  }
                 /***************************************
                  * Message Text : Multi Line Text Area
                  **************************************/
                  Debug.debug("*** Message Text : Multi Line Text Area ***");

                  // Sandeep IR -T36000003310  07/17/2012 Start
            %>
				 <%@ include file="fragments/Messages-Documents.frag" %>
				 
				 <%if(StringFunction.isNotBlank(messageText) && messageText.startsWith("Settlement")){
					 String translatedMessageText = ReferenceDataManager.getRefDataMgr().getDescr("MESSAGE",
								"SETTLE",loginLocale);
					 if(StringFunction.isNotBlank(translatedMessageText)){
						 messageText = translatedMessageText;
					 }
				 }%>
                  <%= widgetFactory.createTextArea("message_text", "Message.Message", StringFunction.xssCharsToHtml(messageText), isReadOnly, false, false, "maxlength=4000 rows=3", "", "") %>
                  <%if(!isReadOnly) {	%>
				  		<%=widgetFactory.createHoverHelp("message_text","MessageTextAreaHoverText") %>
				  <% } %>


            <%
                  //Sandeep IR -T36000003310  07/17/2012 End

                 /***************************************
                  * Message Text : Multi Line DiscrepancyText Area
                  **************************************/
                  Debug.debug("*** Message Text : Multi Line DiscrepancyText Area Area ***");

            if( isDiscrepancy ) {
            %>
                        <%= widgetFactory.createTextArea("discrepancy_text", "MailMessage.ExamDiscrepancyText", discrepancyText, isReadOnly, false, false, "rows=10", "", "") %>
            <%
                 /***************************************
                  * Message Text : Multi Line POInvoiceDiscrepancyText Area
                  **************************************/
                  Debug.debug("*** Message Text :Multi Line POInvoiceDiscrepancyText Area ***");
            %>
                        <%= widgetFactory.createTextArea("po_invoice_discrepancy_text", "MailMessage.POInvoiceDiscrepancyText", poInvoiceDiscrepancyText, isReadOnly) %>
<%                }//end of discrepancy

                  /***************************************
                  * Discrepancy Response Status
                  **************************************/
                  Debug.debug("*** Discrepancy Response Status ***");

                  String respText = "";
                  if( isDiscrepancy || isSettlementReq) {
                  if (instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY)) {
                        respText = "Message.ATPResponseStatus";
                  } else if (isSettlementReq) {
                	  respText = "Message.SettlementResponseStatus";
                  }else {
                        respText = "Message.DiscrepancyResponseStatus";
                        }

                        //If this discrepancy does not have a related Response Transaction Oid -OR-
                        //the message is flagged as deeted then display the fact that the message is
                        //not in a started state, otherwise we display the message status.

                        //PLUI070959610 Move the setting of displayDiscrepancyReplyToBankButton to the top
                  if (displayDiscrepancyReplyToBankButton) {
                        String respTextStatus = resMgr.getText("Message.NotYetStarted", TradePortalConstants.TEXT_BUNDLE);
%>
                              <%= widgetFactory.createTextField(respText, respText, respTextStatus, "100", true) %>
<%                      }else{ %>
                              <%= widgetFactory.createTextField(respText, respText,
                                    ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_STATUS,
                                    transactionStatus,loginLocale), "100", true) %>
<%                      }
                  }  // closes if isDiscrepancy
%>                <br>
                  </div><%--closes formContent area--%>
                  </div><%--closes formArea area--%>

                  <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'MessagesDetail'">
                        <%@ include file="fragments/MessagesSidebar.frag" %>
                  </div> <%--closes sidebar area--%>
<%
                  secureParms.put("opt_lock", message.getAttribute("opt_lock"));

                // Put the isEdit flag into the input document so that after
                // the edit button is pressed subsequent pages (until final submit)
                // will also be in edit mode
                if(isEdit)
                        secureParms.put("isEdit", "true");
                  if(isNewReply)
                  secureParms.put("isNewReply", "true");
%>
                  <%= formMgr.getFormInstanceAsInputField("MessagesDetailForm", secureParms) %>
                  <div id="instrumentSearchDialog"></div>
            </form>
      </div> <%--closes pageContent area--%>
</div> <%--closes pageMain area--%>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>


<%
   Debug.debug("Default Doc data is: " + defaultDoc.toString() );

   if( (isCurrentUser || (isUnAssigned && isUsersOrgOid )) &&
       !(message.getAttribute("unread_flag")).equals(TradePortalConstants.INDICATOR_NO) ) {

      message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_NO);
      message.save();                                     //This may increment opt_lock
  }                                                 //This will ensure the mediator
                                                    //has the right numeric value.
  message.remove();

   Debug.debug("***END******************** Mail Message Page *********************END***");
%>

<%--cquinton 2/7/2013 added for debugging if necessary--%>
<%--
  out.println("returning="+returning);
  out.println(" request mailMessageOid="+request.getParameter("mailMessageOid"));
  out.println(" mailMessageOid="+mailMessageOid);
  out.println(" xmlDoc="+StringFunction.xssCharsToHtml(defaultDoc.toString()));
--%>

</body>
</html>
<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   defaultDoc.removeAllChildren("/");
   formMgr.storeInDocCache("default.doc", defaultDoc);
%>


<div id="routeDialog" style="display:none;"></div>
<%!
   // Various methods are declared here (in alphabetical order).

   // This method helps build the url parameters when creating the document
   // image links in the 'for' loop.  This just localizes code that is used
   // twice in that same loop (for multiple table column reasons).

   // Parameter List :
   // Vector            listviewVector          -List of document image(s) from the Db.
   // DocumentHandler   myDoc             -handle to the actual data for each image.
   // int         x                 -Counter from loop.
   // String            encryptedMailMessageOid -Contains previously encrypted MailMessageOid.

   //cquinton 7/21/2011 Rel 7.1.0 ppx240 - add imageHash
   public String buildURLParameters (Vector listviewVector, DocumentHandler myDoc, String encryptedMailMessageOid,
                                     String subject, String displayText, String encryptedImageHash, SessionWebBean userSession ) {

      String urlParm = "";

      String imageId = myDoc.getAttribute("/IMAGE_ID");
      String encryptedImageId = EncryptDecrypt.encryptStringUsingTripleDes( imageId, userSession.getSecretKey() );

      String encryptedDisplayText = EncryptDecrypt.encryptStringUsingTripleDes( displayText , userSession.getSecretKey());

      urlParm    = "&image_id=" + encryptedImageId +
                   "&hash=" + encryptedImageHash +
                   "&formType=" + encryptedDisplayText +
                   "&fromMessages=Y" + "&messageSubject=" + subject +
                   "&mailMessageOid=" + encryptedMailMessageOid;

      return urlParm;
   }


   //This method only focuses on building a display value of the formtype.  The returned
   //result will be used as the link text.

   public String getDisplayText (DocumentHandler myDoc) {
      String attribute;
      String displayText = "";

      try {
         attribute   = myDoc.getAttribute("/FORM_TYPE");
         displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.FORM_TYPE,
                                                     attribute);
      }catch(AmsException e) {            //This should never happen
            Debug.debug("*********** Call to Refdata Mgr failed to get description for a documemt image. **********");
            Debug.debug("***** The problematic FormType is : " + myDoc.getAttribute("/FORM_TYPE") );
      }

      return displayText;
   }
%>
<script type=text/javascript>
function SearchInstrument(identifierStr, sectionName , InstrumentType) {
    <%--todo: add dialog require here, but need to walk back to event handler from the menus!!! --%>
    itemid = String(identifierStr);
      section = String(sectionName);
      IntType=String(InstrumentType);


    require(["t360/dialog"], function(dialog ) {
      dialog.open('instrumentSearchDialog', '<%=instrumentSearchDialogTitle%>',
                  'instrumentSearchDialogID.jsp',
                  ['itemid','section','IntType'], [itemid,section,IntType], //parameters
                  'select', null);

    });
  }


var xhReq;
function routeMail(){
	  require(["dijit/registry"],
			    function(registry ) {
		  		//XHR request will give a call to the Dummy JSP and perform the save of the message
	  				if (!xhReq) xhReq = getXmlHttpRequest();
	  				 	var instrumentID = registry.byId("complete_instrument_id").value;
	  				 	var messageSubject = registry.byId("message_subject").value;
	  				 	var messageText = registry.byId("message_text").value;
	  				 	if(messageSubject == "" || messageSubject == null){
	  				 		alert("<%=resMgr.getText("Message.Warning1", TradePortalConstants.TEXT_BUNDLE)%>");
	  				 	}else{
			  				var req = "subject=" + messageSubject + "&text="+messageText+"&complete_instrument_id="+instrumentID;
			  				
			  				xhReq.open("GET", '/portal/transactions/PerformSave.jsp?'+req, true);
			  				
			  				xhReq.onreadystatechange = checkStatus;
			  				xhReq.send(null);
	  				 	}
	  			  });}

//Fuction used to create XHR request
function getXmlHttpRequest()
{
  var xmlHttpObj;
  if (window.XMLHttpRequest) {
    xmlHttpObj = new XMLHttpRequest();
  }
  else {
    try {
      xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e) {
        xmlHttpObj = false;
      }
    }
  }
  console.log(xmlHttpObj);
  return xmlHttpObj;
}

//Based on the result of the XHR request, if its success- open the Route dialog, or submit the form.
function checkStatus(){
	require(["dijit/registry"], function(registry){
	 if (xhReq.readyState == 4 && xhReq.status == 200) {
		var response_Value = JSON.parse(xhReq.responseText);
		
  	var message_oid = response_Value.oid;
  	var ifInsert = response_Value.flag;
  	
  	

  	if(ifInsert == 0) {
  		
  		<%String dialogName = resMgr.getText("routeDialog.title", TradePortalConstants.TEXT_BUNDLE);%>
  		var dialogName = '<%=dialogName%>';
  	 	var buttonName = '<%=TradePortalConstants.BUTTON_ROUTE%>';
  	 	var messageSubject = registry.byId("message_subject").value;

	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('routeDialog', dialogName,
			                      'routeDialog.jsp',
			                      ['fromListView','fromWhere','rowKey','gridDisplayValue1'], ['<%=TradePortalConstants.INDICATOR_NO%>','<%=TradePortalConstants.FROM_MESSAGES%>',message_oid,messageSubject], //parameters
			                      'select', this.routeSelectedItems);
			  });
	 	}
  	else {
  		require(["t360/common"], function(common) {
  			if ( document.getElementsByName('MessagesDetailPopupForm').length > 0 ) {
  				
  		 		var instrumentID = registry.byId("complete_instrument_id").value;
  				var messageSubject = registry.byId("message_subject").value;
  				var messageText = registry.byId("message_text").value;
  				var theForm = document.getElementsByName('MessagesDetailPopupForm')[0];
  				theForm.message_subject.value = messageSubject;
  				theForm.message_text.value = messageText;
  				theForm.complete_instrument_id.value = instrumentID;
  				theForm.buttonName.value = buttonName;
  				theForm.is_reply.value='N';
  				common.setButtonPressed(buttonName,0);
  				
  				theForm.submit();
  			}
  		});
  	}
    }
	});
}


function routeSelectedItems(routeToUser, routeUserOid, routeCorporateOrgOid, multipleOrgs, rowKey){
	
	
		 if ( document.getElementsByName('RouteMessagesForm').length > 0 ) {
		        var theForm = document.getElementsByName('RouteMessagesForm')[0];
		        theForm.RouteUserOid.value=routeUserOid;
		        if(routeCorporateOrgOid != ""){
		        	theForm.RouteCorporateOrgOid.value=routeCorporateOrgOid;
		        }
		        theForm.RouteToUser.value=routeToUser;
		        theForm.multipleOrgs.value=multipleOrgs;
		        theForm.routeAction.value='<%=TradePortalConstants.INDICATOR_YES%>';
		        theForm.fromListView.value='<%=TradePortalConstants.INDICATOR_YES%>';

		        var buttonName = 'RouteAndSave';
		   	    submitFormWithParms("RouteMessagesForm", buttonName, "checkbox", rowKey);

		 }
	}
</script>
