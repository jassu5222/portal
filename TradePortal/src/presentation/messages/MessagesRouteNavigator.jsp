<%--
*******************************************************************************
                         Messages Route Navigator Page

  Description:
	Directs the routing to the necessary page.
	1. Determine if the routing originates from listview or detail page
	2. If RouteSelection errors exist, display the route selection page
	3. Otherwise, if from listview, display listview page
	4. If from detail page, if other errors exist, display the detail page.
	5. 	  otherwise, return to the detail page's originating page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

  DocumentHandler doc = null;
  String newCurrPage = null;
  String forwardPage = null;
  String forwardLink = null;
  String fromPage= (String) session.getAttribute("fromPage");

  // Get the document from the cache to determine what page
  // to forward to:
  // route selection related errors - return to Route Selection page
  // from listview - return to transactions listview
  // from detail page - if there are errors, return to detail page
  // otherwise, return to InstrumentCloseNavigator page
  doc = formMgr.getFromDocCache();

  Debug.debug("doc from cache is " + doc.toString());

  // determine if originating page is from listview or from detail

  boolean fromListView = doc.getAttribute("/In/Route/fromListView") != null &&
  						 doc.getAttribute("/In/Route/fromListView").equals
						 (TradePortalConstants.INDICATOR_YES);

  // if there are route selection errors, return to the route page
  if (doc.getAttribute("/Out/Route/RouteSelectionErrors") != null &&
  	  doc.getAttribute("/Out/Route/RouteSelectionErrors").equals
	  (TradePortalConstants.INDICATOR_YES))
  {
    newCurrPage = "RouteItems";
	forwardLink = NavigationManager.getNavMan().getPhysicalPage(newCurrPage, request);
  } // if from listview, return to the listview
  else if (fromListView)
  {
  	if(fromPage.equalsIgnoreCase(TradePortalConstants.INSTRUMENT_SUMMARY)) {
  		newCurrPage = TradePortalConstants.INSTRUMENT_SUMMARY;
  	}
  	else {
  	 // SureshL IR-T36000029491 REl9.0 06/25/2014 Start 
  		 SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
         String returnAction = backPage.getLinkAction();
 
   if("goToMessagesHome".equals(returnAction))
  	newCurrPage = "MessagesHome";
  	else
  	newCurrPage = "TradePortalHome";
  	}
  	// SureshL IR-T36000029491 REL9.0 06/25/2014 End
	forwardLink = NavigationManager.getNavMan().getPhysicalPage(newCurrPage, request);
  }  // if there are errors, return to the detail page
  else if (doc.getAttributeInt("/Error/maxerrorseverity")>=ErrorManager.ERROR_SEVERITY)
  {
    // if there are errors from the detail page, return to the detail
	// page and pass in the message oid in mailMessageOid
    newCurrPage = "MailMessageDetail";

    forwardLink = NavigationManager.getNavMan().getPhysicalPage(newCurrPage, request) +
	          "?mailMessageOid=" +
		  EncryptDecrypt.encryptStringUsingTripleDes(
		  doc.getAttribute("/In/MessageList/Message/messageOid"), userSession.getSecretKey());

  }
  else
  {
Debug.debug("  calling fromPage : " + session.getAttribute("fromPage"));
    // if there are no errors, return to the detail page's originating page
    newCurrPage = (String) session.getAttribute("fromPage");
    if(newCurrPage==null || newCurrPage == "")
    	newCurrPage = "MessagesHome";
    //Added by dillip for T36000011226 at 12-Feb-2013
    forwardLink = NavigationManager.getNavMan().getPhysicalPage(newCurrPage, request)+"?returning=true";
  }
  formMgr.setCurrPage(newCurrPage);
Debug.debug("&&&&& forwardLink: is : " + forwardLink);

%>
  <jsp:forward page='<%=forwardLink%>' />

