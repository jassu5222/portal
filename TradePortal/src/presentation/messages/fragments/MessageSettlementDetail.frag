<%--
*******************************************************************************
                            Mail Message Detail

  Description: This page is used to display Settlement Instruction Request detail data.

*******************************************************************************
--%>
<%--
*
*     Copyright  ? 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>

 <% if ( TradePortalConstants.SETTLEMENT_REQ_PAYMENT.equals(settlementInstrFlag) ) { 
	 String presentationDat = "";
	 if(StringFunction.isNotBlank(message.getAttribute("presentation_date"))){
		 presentationDat = TPDateTimeUtility.formatMessageDate(new Date(message.getAttribute("presentation_date")), loginLocale);
	 }
  %>
 
       <%= widgetFactory.createTextField("PresentationAmount", "MailMessage.PresentationAmount", currencyCode + "   " + amount,
                                                      "35", isReadOnly, false, false, "", "", "inline") %>
                                                      
       <%= widgetFactory.createTextField("OtherParty", "MailMessage.OtherParty", counterPartyName,
                                                      "35", isReadOnly, false, false, "", "", "inline") %>
       
       <div style="clear: both;"></div>
     
      <%= widgetFactory.createTextField("PresentationNumber", "MailMessage.PresentationNumber", sequenceNumber,
                                                "35", isReadOnly, false, false, "", "", "inline") %>
                                                      
      <%= widgetFactory.createTextField("PresentationDate", "MailMessage.PresentationDate", 
    		  presentationDat, "35", isReadOnly, false, false, "", "", "inline") %>
 
 <% } else if (  TradePortalConstants.SETTLEMENT_REQ_LIQUIDATE.equals(settlementInstrFlag) ) {
	 String issueDat = "";
	 if(StringFunction.isNotBlank(instrument.getAttribute("issue_date"))){
		 issueDat = TPDateTimeUtility.formatDate(instrument.getAttribute("issue_date"), TPDateTimeUtility.SHORT, loginLocale);
	 }
	 String maturityDat = "";
	 if(StringFunction.isNotBlank(message.getAttribute("funding_date"))){
		 maturityDat = TPDateTimeUtility.formatMessageDate(new Date(message.getAttribute("funding_date")), loginLocale);
	 }
	 %>
 
         <%= widgetFactory.createTextField("PresentationAmount", "MailMessage.Amount", currencyCode + "   " + amount,
                                                      "35", isReadOnly, false, false, "", "", "inline") %>
         <%= widgetFactory.createTextField("OtherParty", "MailMessage.OtherParty", counterPartyName,
                                                      "35", isReadOnly, false, false, "", "", "inline") %>
                                                      
          <div style="clear: both;"></div>
          
          <%= widgetFactory.createTextField("issueDate", "MailMessage.IssueDate", 
        		  issueDat, "35", isReadOnly, false, false, "", "", "inline") %>
          
          <%= widgetFactory.createTextField("maturityDate", "MailMessage.MaturityDate",  
        		  maturityDat, "35", isReadOnly, false, false, "", "", "inline") %>                                   
                                                                               
 <% } else if (  TradePortalConstants.SETTLEMENT_REQ_ROLLOVER.equals(settlementInstrFlag) ) { 
	 String issueDat = "";
	 if(StringFunction.isNotBlank(instrument.getAttribute("issue_date"))){
		 issueDat = TPDateTimeUtility.formatDate(instrument.getAttribute("issue_date"), TPDateTimeUtility.SHORT, loginLocale);
	 }
	 String maturityDat = "";
	 if(StringFunction.isNotBlank(message.getAttribute("funding_date"))){
		 maturityDat = TPDateTimeUtility.formatMessageDate(new Date(message.getAttribute("funding_date")), loginLocale);
	 }
	 %>
            <%= widgetFactory.createTextField("PresentationAmount", "MailMessage.Amount", currencyCode + "   " + amount,
                                                      "35", isReadOnly, false, false, "", "", "inline") %>
                                                      
          <%
           String termsPartyOid = InstrumentServices.getLatestPartyOid(instrument.getAttribute("instrument_oid"),"c_FIRST_TERMS_PARTY_OID");
           TermsPartyWebBean termsPartyApplicant = null;
           String borrowerName = "";
           if ( StringFunction.isNotBlank(termsPartyOid) ) {
              termsPartyApplicant  = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
		      termsPartyApplicant.setAttribute("terms_party_oid", termsPartyOid);
		      termsPartyApplicant.getDataFromAppServer();   
		      borrowerName = termsPartyApplicant.getAttribute("name");		      		      
		   }
          %>
         <%= widgetFactory.createTextField("OtherParty", "MailMessage.Borrower", borrowerName,
                                                      "35", isReadOnly, false, false, "", "", "inline") %>
                                                      
          <div style="clear: both;"></div>
          
          <%= widgetFactory.createTextField("issueDate", "MailMessage.LoanStartDate", 
        		  issueDat, "35", isReadOnly, false, false, "", "", "inline") %>
            
          <%= widgetFactory.createTextField("maturityDate", "MailMessage.MaturityDate",  
        		  maturityDat, "35", isReadOnly, false, false, "", "", "inline") %>                                                                                 
         
         <% 
        
         TermsWebBean termsForLoan        	  = null;
         TransactionWebBean originalTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");
         originalTransaction.getById(instrument.getAttribute("original_transaction_oid"));
         String bankReleasedTerms	= originalTransaction.getAttribute("c_BankReleasedTerms");
         boolean hasBankReleasedTerms  = StringFunction.isNotBlank( bankReleasedTerms );
         if( hasBankReleasedTerms ) {
 	         termsForLoan = originalTransaction.registerBankReleasedTerms();
         }

         String loanTermsType = null;
         if (termsForLoan != null) {
    	    loanTermsType = termsForLoan.getAttribute("loan_terms_type");
         }
         if ( StringFunction.isNotBlank(loanTermsType) ) {
    	   if (loanTermsType.equals(TradePortalConstants.LOAN_DAYS_AFTER)) {       
         %>
                <%= widgetFactory.createTextField("LoanTerms", "TransactionTerms.LoanTerms", termsForLoan.getAttribute("loan_terms_number_of_days")+" " + 
                     resMgr.getText("LoanRequest.DaysFrom", TradePortalConstants.TEXT_BUNDLE), "60", true, false,false, "", "", "inline")%>
         <%} else {
        	 String lonTermsFixMatDat = "";
        	 if(StringFunction.isNotBlank(termsForLoan.getAttribute("loan_terms_fixed_maturity_dt"))){
        		 String loanTermsDt = termsForLoan.getAttribute("loan_terms_fixed_maturity_dt");
        		 lonTermsFixMatDat = TPDateTimeUtility.formatDate( loanTermsDt, TPDateTimeUtility.LONG, loginLocale );
        	 }
          %>
           <%=widgetFactory.createTextField("LoanTerms", "TransactionTerms.LoanTerms", resMgr.getText("LoanRequest.AtFixedMaturityDate",TradePortalConstants.TEXT_BUNDLE)+ 
           " " +lonTermsFixMatDat, "60", true, false,false, "", "", "inline")%>
          <% } %> 
        <% } else {%>
         <%= widgetFactory.createTextField("LoanTerms", "TransactionTerms.LoanTerms", "", "60", true, false,false, "", "", "inline")%>
         <% } %>
 
 <% } %>
 
 
 