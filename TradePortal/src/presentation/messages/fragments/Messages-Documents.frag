<%

     /************************************
      * Start of Documents Section       *
      ************************************/
      Debug.debug("****** Start of Documents Section ******");

try {
   //jgadela R90 IR T36000026319 - SQL INJECTION FIX
   List<Object> sqlParams = new ArrayList();
   StringBuilder query = new StringBuilder();

   //cquinton 11/16/2011 Rel 7.1 r shul111638456 add hash
   query.append("select doc_image_oid, image_id, doc_name, hash ");
   query.append("from document_image ");

   if ((mailMessageOid != null) && (mailMessageOid.trim().length() > 0)) {
     query.append("where mail_message_oid = ? ");
     sqlParams.add( mailMessageOid);
   }
   else {
     query.append("where mail_message_oid = ? ");
     sqlParams.add( messageOid.toString());
   }

   // [BEGIN] IR-YYUH032255414 - jkok
   query.append(" and form_type = ? ");
   sqlParams.add( TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED);
   // [END] IR-YYUH032255414 - jkok
   // [BEGIN] IR-YYUH032637780 - jkok - this assumes that the image_id is related to the
   //                                   order that the documents were attached
   query.append(" and p_transaction_oid is null");
   query.append(" order by image_id");
   // [END] IR-YYUH032637780 - jkok

   Debug.debug("Query is : " + query);
   DocumentHandler dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), false, sqlParams);

   listviewVector = null;
   if((dbQuerydoc != null)
      && (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0)) {
      Debug.debug("**** dbQuerydoc --> " + dbQuerydoc.toString());
      listviewVector = dbQuerydoc.getFragments("/ResultSetRecord/");

      int liTotalDocs = listviewVector.size();
      int liDocCounter = liTotalDocs;
%>
	<br>
	<span class="attachIcon inline formItem">
      	<a></a>
   	</span>
    <%= widgetFactory.createLabel("", "common.InstrumentAttachments", false, false, false, "")%>
    <div style="clear:both;"></div>
<%
      for(int y=0; y < ((liTotalDocs+2)/3); y++) {
         for (int x=0; x < 3 ; x++) {
            if (liDocCounter==0) break;

            myDoc = (DocumentHandler)listviewVector.elementAt((y*3)+x);

            // cquinton 7/21/2011 Rel 7.1 ir shul111638456 - add hash parm
            String imageHash = myDoc.getAttribute("/HASH");
            String encryptedImageHash = "";
            if ( imageHash != null && imageHash.length()>0 ) {
               encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( imageHash, userSession.getSecretKey() );
            } else {
               //send something rather than nothing, which ensures we know we are not allowing
               // attempts to view images when hash parm is just forgotten
               encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( TradePortalConstants.NO_HASH, userSession.getSecretKey() );
            }

            // cquinton 7/21/2011 Rel 7.1 ir shul111638456 - add hash parm
            String urlParam = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) +
               "&hash=" + encryptedImageHash;

            String encodedURL = response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParam);
            String userRights = userSession.getSecurityRights();
            String widgetID = "AttachedDocument"+liDocCounter;

            if ((!isReadOnly) && (hasDelDocMsg)) {
				//EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/DOC_IMAGE_OID"), userSession.getSecretKey())
%>
				<%= widgetFactory.createCheckboxField("AttachedDocument"+liDocCounter, "",
						EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/DOC_IMAGE_OID"), userSession.getSecretKey()), false, false, false, "", "", "inline") %>
				<% //Changes done for PortalRefresh IR - T36000004336- Sandeep Sikhakolli
				//Changes are required as the existing logic does not work for the dojo checkboxes.
				%>
				<input type="hidden" name="AttachedDocument" id="<%=widgetID%>" value="<%=widgetID%>" />
 <%           }
%>
	            <%= widgetFactory.createLabel("", "<a href='"+encodedURL+"' target='_blank'>"+myDoc.getAttribute("/DOC_NAME")+"</a>", false, false, false, "")%>
<%
            liDocCounter--;
         }
         if (liDocCounter==0) {
            for (int z=liTotalDocs % 3; z > 0; z--) {
            }
         }
      }
%>
<%-- PHILC PUT IMAGES IN 3 COLUMN TABLE --%>

<%
   } // if((dbQuerydoc != null)
     //    && (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0)) {
} catch(Exception e) {
   System.err.println("[Messages-Documents.frag] Caught an exception while trying to get list of documents associated with mail message "
   	+String.valueOf(messageOid));
   e.printStackTrace();
}
%>
