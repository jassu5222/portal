<%--
*******************************************************************************
                            Pre Debit Fundng Mail Messages Home

  Description:
	 The Messages Home page is used to display all messages sent by
	 the user organization's bank, all messages routed to a user by
	 other users in the organization or related child organizations,
	 and all draft messages that the belong to the user, his/her
	 organization, or child organization users (assuming the user
	 has the appropriate security rights).
*******************************************************************************
--%>
<%--
*
*     Copyright  ? 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  	String gridId = "DebitFundMailMsgDataGridId";
  	String curOrgOption = request.getParameter("curOrgOption");
  	final String MY_MAIL_AND_UNASSIGNED = "Mail.MeAndUnassigned";
  	final String ALL_MAIL = "Mail.AllMail";
  	
  	 final String ALL_MAILS = "Mail.AllMailMESS";
     final String MAIL_READ  = "Mail.ReadMailMESS";
     final String MAIL_UNREAD = "Mail.UnreadMailMESS";

  	if (curOrgOption == null) {
  		curOrgOption = (String) session.getAttribute("curOrgOption");

  		// If current org still has no value, default to the user's org
  		if ((curOrgOption == null) || (curOrgOption.equals(""))) {
  			curOrgOption = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
  		}
  	}

  	session.setAttribute("curOrgOption", curOrgOption);
  	curOrgOption = EncryptDecrypt.decryptStringUsingTripleDes(curOrgOption, userSession.getSecretKey());

  	String currentDebFundMailOption = EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey());

    StringBuffer debFundMailOrgOptions = new StringBuffer();
    // If the user has rights to view child organization mail, retrieve the list of dropdown
    // options from the DatabaseQueryBean results doc (containing an option for each child org);
    // otherwise, simply use the user's org option to build the dropdown list (in addition
    // to the default 'Me' or 'Me and Unassigned' option).
    debFundMailOrgOptions.append("<option value=\"").
      append(EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey())).
      append("\" selected >");
    debFundMailOrgOptions.append(resMgr.getText( MY_MAIL_AND_UNASSIGNED, TradePortalConstants.TEXT_BUNDLE));
    debFundMailOrgOptions.append("</option>");
    if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_MESSAGES)) {
       debFundMailOrgOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME",
                                                         currentDebFundMailOption, userSession.getSecretKey()));
       // Only display the 'All Mail' option if the user's org has child organizations
       if (totalOrganizations > 1) {
          debFundMailOrgOptions.append("<option value=\"").
            append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_MAIL, userSession.getSecretKey())).
            append("\"");
          if (currentDebFundMailOption.equals(ALL_MAIL)) {
             debFundMailOrgOptions.append(" selected");
          }
          debFundMailOrgOptions.append(">");
          debFundMailOrgOptions.append(resMgr.getText( ALL_MAIL, TradePortalConstants.TEXT_BUNDLE));
          debFundMailOrgOptions.append("</option>");
       }
    }
    else {
       debFundMailOrgOptions.append("<option value=\"").
         append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())).
         append("\"");
       if (currentDebFundMailOption.equals(userOrgOid)) {
          debFundMailOrgOptions.append(" selected");
       }
       debFundMailOrgOptions.append(">");
       debFundMailOrgOptions.append(userSession.getOrganizationName());
       debFundMailOrgOptions.append("</option>");
    }
    
    String datePattern = userSession.getDatePattern();
    String dateWidgetOptions = "placeHolder:'" + datePattern + "'"; 
    String currency = "";
    String ccyOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
%>
<%-- ********************* HTML for page begins here *********************  --%>

<%--cquinton 9/20/2012 ir#4556 move Header.jsp to MessagesHome so common html is accurate--%>

	<div class="pageMainNoSidebar">
		<div class="pageContent">
			<%@ include file="MessagesSecondaryNavigation.frag" %>

			<form id="PreDebitFundingListForm" name="PreDebitFundingListForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
				<jsp:include page="/common/ErrorSection.jsp" />
      
				<div class="formContentNoSidebar">
					<span class="searchHeaderActions">
						<jsp:include page="/common/gridShowCount.jsp">
							<jsp:param name="gridId" value="<%=gridId%>" />
						</jsp:include>
						
						<%-- SSikhakolli - Rel-9.1 CR-944 IR-T36000031676 - Adding Grid Customization Popup --%>
						<span id="preDebitFundingGridEdit" class="gridHeaderEdit"></span>
				        <%=widgetFactory.createHoverHelp("preDebitFundingGridEdit", "CustomizeListImageLinkHoverText") %>
					</span>				
        
					<input type=hidden value="" name=buttonName>

<%
  	// Store the user's oid and security rights in a secure hashtable for the form
  	secureParms.put("SecurityRights", userSecurityRights);
  	secureParms.put("UserOid",        userOid);  	
  	secureParms.put("UserOrgOid",     userOrgOid);

  	defaultValue = "";
  
  %>
  		<%= widgetFactory.createSearchSelectField( "DebitFundMailOrg", "Home.Mail.Show", "",
  		 debFundMailOrgOptions.toString()) %>
	<%-- CR49930 - Start--%> 
              <%
                
                 	StringBuffer dropdownMailOptions = new StringBuffer();
        	     	boolean userCanFilterOrg = true;
      
       				dropdownMailOptions.append("<option value='"+ALL_MAILS+"' selected='true' >");
        			dropdownMailOptions.append(resMgr.getText( ALL_MAILS, TradePortalConstants.TEXT_BUNDLE));
        			dropdownMailOptions.append("</option>");
     
        			dropdownMailOptions.append("<option value='N' selected='true' >");
        			dropdownMailOptions.append(resMgr.getText( MAIL_READ, TradePortalConstants.TEXT_BUNDLE));
        			dropdownMailOptions.append("</option>");
      	
        			dropdownMailOptions.append("<option value='Y' selected='true' >");
        			dropdownMailOptions.append(resMgr.getText( MAIL_UNREAD, TradePortalConstants.TEXT_BUNDLE));
        			dropdownMailOptions.append("</option>");
     		 %>          
        	<%=widgetFactory.createSearchSelectField("MailReadUnread", "Mail.ReadUnreadMessMail", "", 
        				dropdownMailOptions.toString())%> 
		
		<%=widgetFactory.createSubLabel("Home.DebitFundingNotifications.Section.PaymentDate")%>
		<%=widgetFactory.createSearchDateField("PaymentDateFrom","Home.DebitFundingNotifications.PaymentDateFrom","class='char10'",dateWidgetOptions) %>
		<%=widgetFactory.createSearchDateField("PaymentDateTo","Home.DebitFundingNotifications.PaymentDateTo","class='char10'",dateWidgetOptions) %>
		<%=widgetFactory.createSearchSelectField("DebFundCcy", "Home.DebitFundingNotifications.CCY", " ", ccyOptions,"class='char15'") %>
<%
        			
  	DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  	String gridLayout = dgFactory.createGridLayout("DebitFundMailMsgDataGridId", "DebitFundingMailMsgDataGrid");
  	String gridHtml = dgFactory.createDataGrid("DebitFundMailMsgDataGridId","DebitFundingMailMsgDataGrid",null);
%>
        			<%=gridHtml%>
        		</div><%-- end of formContentNoSidebar div --%>

        		<%= formMgr.getFormInstanceAsInputField("PreDebitFundingListForm", secureParms) %>
        	</form>
        </div><%-- end of pageContent div --%>
    </div><%-- end of pageMainNoSidebar div --%>

	<jsp:include page="/common/Footer.jsp">
	  	<jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	  	<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>
	
	<%@ include file="/common/GetSavedSearchQueryData.frag" %>
	
	<jsp:include page="/common/gridShowCountFooter.jsp">
	  	<jsp:param name="gridId" value="<%=gridId%>" />
	</jsp:include>

<script type="text/javascript">
var gridLayout = <%= gridLayout %>;

require(["dojo/query", "dijit/registry", "dojo/dom", "dojo/on", "t360/popup", "t360/datagrid", "dojo/ready", "dojo/domReady!"], 
		function(query, registry, dom, on, popup, datagrid, ready){
	  ready(function(){
		  
		  var savedSort = savedSearchQueryData["sort"]; 
          if("" == savedSort || null == savedSort || "undefined" == savedSort){
      		  savedSort = '-DateAndTime';
      	 }
          var org = savedSearchQueryData["org"]; 
          if("" == org || null == org || "undefined" == org)
      		org='<%= EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey()) %>';
      	else if(registry.byId("DebitFundMailOrg")){
      		registry.byId("DebitFundMailOrg").attr('value',org, false);
      		
      		
      	}
      	var readDebit = savedSearchQueryData["UnreadFlag"]; 
          if("" == readDebit || null == readDebit || "undefined" == readDebit)
      		readDebit='<%= EncryptDecrypt.encryptStringUsingTripleDes(ALL_MAILS, userSession.getSecretKey()) %>';
      	else if(registry.byId("MailReadUnread")){
      		registry.byId("MailReadUnread").attr('value',readDebit, false);
      	
      	}
	
		var initSearchParms = "org="+org+"&UnreadFlag="+readDebit; 
		var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("DebitFundingMailMsgDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
		createDataGrid("DebitFundMailMsgDataGridId", viewName,gridLayout, initSearchParms,savedSort);
		
	    if(registry.byId('DebitFundMailOrg')){
	    	registry.byId('DebitFundMailOrg').on("change", function() {
	    		searchDebitFundMailMessageGrid();
	        });
	    }
	    if(registry.byId('MailReadUnread')){
	    	registry.byId('MailReadUnread').on("change", function() {
	    		searchDebitFundMailMessageGrid();
	        });
	    }
	    if(registry.byId('PaymentDateFrom')){
		    registry.byId('PaymentDateFrom').on("change", function() {
		  	  searchDebitFundMailMessageGrid();
		    });
		}
		if(registry.byId('PaymentDateTo')){
		    registry.byId('PaymentDateTo').on("change", function() {
		  	  searchDebitFundMailMessageGrid();
		    });
		}
		if(registry.byId('DebFundCcy')){
		    registry.byId('DebFundCcy').on("change", function() {
		  	  searchDebitFundMailMessageGrid();
		    });
		}
		
		 <%-- Surrewsh - Rel-9.4  IR-T36000038363 -Applying bold style for unread messages  Start --%>
	    var myGrid = registry.byId("DebitFundMailMsgDataGridId");
      	dojo.connect(myGrid, 'onStyleRow' , this, function(row) {
             var item = myGrid.getItem(row.index);

              if (item) {
                  var type = myGrid.store.getValue(item, "UnreadFlag");

                  if (type == 'Y') {
                        row.customStyles += "font-weight: bold;";
                  }
              }

              myGrid.focus.styleRow(row);
              myGrid.edit.styleRow(row);
          });
      	 <%-- Surrewsh - Rel-9.4  IR-T36000038363  End --%>    
		
	  });
	  
	  <%-- SSikhakolli - Rel-9.1 CR-944 IR-T36000031676  - Adding Grid Customization Popup --%>
	  query('#preDebitFundingGridEdit').on("click", function() {
          var columns = datagrid.getColumnsForCustomization('DebitFundMailMsgDataGridId');
          var parmNames = [ "gridId", "gridName", "columns" ];
          var parmVals = [ "DebitFundMailMsgDataGridId", "DebitFundingMailMsgDataGrid", columns ];
          popup.open(
            'preDebitFundingGridEdit', 'gridCustomizationPopup.jsp',
            parmNames, parmVals,
            null, null);  //callbacks
        });
	});
			
function searchDebitFundMailMessageGrid(){
	  
  	require(["dijit/registry", "dojo/dom"], function(registry, dom){    
		
  		var searchParms = "";
		var debFundOrgSelect = registry.byId("DebitFundMailOrg");
		var debFundOrgOid = debFundOrgSelect.get('value');
		
		var debFundDebitOrgSelect = registry.byId("MailReadUnread");
		var debFundDebitOrgOid = debFundDebitOrgSelect.get('value');
		
		var payDateFrom = (dom.byId("PaymentDateFrom").value).toUpperCase();
		var payDateTo = (dom.byId("PaymentDateTo").value).toUpperCase();;
		var currency = registry.byId("DebFundCcy");
		var ccySelect = "";
		var dPattern = '<%=StringFunction.xssCharsToHtml(userSession.getDatePattern())%>';
		
		if ( debFundOrgSelect && (null != debFundOrgSelect.get('value')) ) {
			debFundOrgOid = debFundOrgSelect.get('value');
		}
		if ( debFundDebitOrgSelect && (null != debFundDebitOrgSelect.get('value')) ) {
			debFundDebitOrgOid = debFundDebitOrgSelect.get('value');
		}
		<%-- Commenting below lines dues a script error
		if( payDateFrom && (payDateFrom.value != 'Invalid Date') ){	 
			payDateFrom = dojo.date.locale.format(payDateFrom.value, {datePattern: dPattern, selector: "date"});
	     }
		if( payDateTo && (payDateTo.value != 'Invalid Date') ){	   	  
			payDateTo = dojo.date.locale.format(payDateTo.value, {datePattern: dPattern, selector: "date"});
		}
		--%>
		if( currency && (null != currency.get('value')) ){
			ccySelect = currency.get('value');
		}
		
		searchParms ="org="+debFundOrgOid+"&UnreadFlag="+debFundDebitOrgOid+"&payDateFrom="+payDateFrom+"&payDateTo="+payDateTo+"&currency="+ccySelect+"&dPattern="+dPattern;
		
		searchDataGrid("DebitFundMailMsgDataGridId", "DebitFundingMailMsgDataView", searchParms);
    });
	   
  }
	
function routeDebitFundingMail(){
    openRouteDialog('routeDialog', routeItemDialogTitle,'DebitFundMailMsgDataGridId','<%=TradePortalConstants.INDICATOR_YES%>','<%=TradePortalConstants.FROM_MESSAGES_DEBIT_FUND%>',getSelectedGridRowKeys('DebitFundMailMsgDataGridId')) ;
    return false;
}

function deleteDebitFundingMail(){
    var formName = "PreDebitFundingListForm";
   var buttonName = '<%=TradePortalConstants.BUTTON_DELETE%>';
     var   confirmMessage = "Are you sure you want to delete the selected item(s)?";
   <%--get array of rowkeys from the grid--%>
   var rowKeys = getSelectedGridRowKeys("DebitFundMailMsgDataGridId");
   if(rowKeys == ""){
           alert("You need to select one or more items in order to 'delete'.");
           return;
     }
     if (!confirm(confirmMessage)){
           return false;
     }else{
   <%--submit the form with rowkeys
       becuase rowKeys is an array, the parameter name for
       each will be checkbox + an iteration number -
       i.e. checkbox0, checkbox1, checkbox2, etc --%>
     submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
   }
}

</script>

