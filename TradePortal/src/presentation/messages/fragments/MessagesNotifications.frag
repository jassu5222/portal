<%--
*******************************************************************************
                            Notifications Home

  Description:
     This page is used to display transactions that have been cancelled or
  processed by the bank.  Notifications appear here when a status change occurs
  for items initiated in Trade Portal or in OTL.  Users can filter by
  organization, can select individual transactions to view, or can delete one
  or more transactions.

*******************************************************************************
--%>
<%--
*
*     Copyright  ? 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>

<%
  	//cquinton 3/6/2013 make all value constant, not the translated value
  	// so do not have to pass for comparison
  	final String ALL_NOTIFS = "Notifications.All";

 //Jaya Mamidala - CR49930 - Start	
    final String ALL_NOTIFRUN ="Notifications.AllRUN";
    final String READ_NOTIFS = "Notifications.ReadNotifications";
    final String UNREAD_NOTIFS = "Notifications.UnreadNotifications";
 //Jaya Mamidala - CR49930 - End 
    
		  String currentOrgOption = null;
%>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  	String gridId = "notificationsDataGridId";
  	currentOrgOption = request.getParameter("currentOrgOption");

  	if (currentOrgOption == null) {
  		currentOrgOption = (String) session.getAttribute("currentOrgOption");

  		// If current org still has no value, default to the user's org
  		if ((currentOrgOption == null) || (currentOrgOption.equals(""))) {
  			currentOrgOption = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
  		}
  	}

  	session.setAttribute("currentOrgOption", currentOrgOption);
  	currentOrgOption = EncryptDecrypt.decryptStringUsingTripleDes(currentOrgOption, userSession.getSecretKey());

 	//cquinton 3/6/2013 add variable to help with whether org is filtered
  	boolean userCanFilterOrg = false;

  	// If the user has rights to view child organizations, retrieve the list of
  	// dropdown options from the DatabaseQueryBean results doc (containing an
  	// option for each child org), including an "All" option.  Otherwise,
  	// simply use the user's org as the only option.
  	if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_MESSAGES)) {
  		dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME", currentOrgOption, userSession.getSecretKey()));

  		// Only display the 'All' option if the user's org has child orgs
  		if (totalOrganizations > 1) {
  			dropdownOptions.append("<option value=\"");
  			dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_NOTIFS, userSession.getSecretKey()));
  			dropdownOptions.append("\"");

  			if (currentOrgOption.equals(ALL_NOTIFS)) {
  				dropdownOptions.append(" selected");
  			}

  			dropdownOptions.append(">");
  			dropdownOptions.append(resMgr.getText( ALL_NOTIFS, TradePortalConstants.TEXT_BUNDLE));
  			dropdownOptions.append("</option>");
  		}
  	} else {
  		dropdownOptions.append("<option value=\"");
  		dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
  		dropdownOptions.append("\"");

  		if (currentOrgOption.equals(userOrgOid)) {
  			dropdownOptions.append(" selected");
  		}

  		dropdownOptions.append(">");
  		dropdownOptions.append(userOrgName);
  		dropdownOptions.append("</option>");
  	}

  	extraTags.append("onchange=\"location='");
  	extraTags.append(formMgr.getLinkAsUrl("goToMessagesHome", response));
  	extraTags.append("&amp;current2ndNav=" + TradePortalConstants.NAV__NOTIFICATIONS);
  	extraTags.append("&amp;currentOrgOption='+this.options[this.selectedIndex].value\"");

  	//cquinton 1/18/2013 remove old close action behavior
%>
<%-- ********************* HTML for page begins here *********************  --%>

<%--cquinton 9/20/2012 ir#4556 move Header.jsp to MessagesHome so common html is accurate--%>

	<div class="pageMainNoSidebar">
		<div class="pageContent">
			<%@ include file="MessagesSecondaryNavigation.frag" %>

			<form id="NotificationsListForm" name="NotificationsListForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
				<jsp:include page="/common/ErrorSection.jsp" />
      
				<div class="formContentNoSidebar">
					<span class="searchHeaderActions">
						<jsp:include page="/common/gridShowCount.jsp">
							<jsp:param name="gridId" value="<%=gridId%>" />
						</jsp:include>
						
						<%-- SSikhakolli - Rel-9.1 CR-944 IR-T36000031676 - Adding Grid Customization Popup --%>
						<span id="notificationsGridEdit" class="gridHeaderEdit"></span>
				        <%=widgetFactory.createHoverHelp("notificationsGridEdit", "CustomizeListImageLinkHoverText") %>
					</span>				
        
					<input type=hidden value="" name=buttonName>

<%
  	// Store the user's oid and security rights in a secure hashtable for the form
  	secureParms.put("SecurityRights", userSecurityRights);
  	secureParms.put("UserOid",        userOid);
  	
  	//cquinton 2/6/2013 ir#1454 add secure parms for delete all - these are the ones that do not change
  	secureParms.put("UserOrgOid",     userOrgOid);
  	secureParms.put("confInd",        confInd);
  	secureParms.put("UserOid",        userOid);

  	defaultValue = "";
  
  	if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_MESSAGES) && (totalOrganizations > 1)) {
  		userCanFilterOrg = true;
%> <%-- SureshL IR-T36000027750 05/22/2014 Start --%>
   <%=widgetFactory.createSearchSelectField("OrgOption", "Home.Notifications.criteria.Show", defaultValue, 
                    dropdownOptions.toString(), "onChange='filterNotifications("+userCanFilterOrg+")'")%>
     <%-- SureshL IR-T36000027750 05/22/2014 End --%>
 <%
  	}
  	//note: if no OrgOption field is given means the display defaults to the user's own org

  	dropdownOptions = new StringBuffer();
  // SureshL IR-T36000027750 05/22/2014 Start 
  	dropdownOptions.append("<option value='"+ALL_NOTIFS+"' selected='true' >");
  	dropdownOptions.append(resMgr.getText( ALL_NOTIFS, TradePortalConstants.TEXT_BUNDLE));
  	dropdownOptions.append("</option>");
  // SureshL IR-T36000027750 05/22/2014 End
  
  	dropdownOptions.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK, loginLocale)).
    append("</option>");
  	
  	dropdownOptions.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT, loginLocale)).
    append("</option>");
  	
  	dropdownOptions.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK, loginLocale)).
    append("</option>");
  	
  	dropdownOptions.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK, loginLocale)).
    append("</option>");
  	
  	dropdownOptions.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT, loginLocale)).
    append("</option>");
  	//SPenke T36000006260 10-10-2012 End
%>             
    <%-- SureshL IR-T36000027750 05/22/2014 Start --%>
    <%=widgetFactory.createSearchSelectField("status", "Home.Notifications.Status", defaultValue, 
        		   dropdownOptions.toString(), "onChange='filterNotifications("+userCanFilterOrg+")'")%>
    <%-- SureshL IR-T36000027750 05/22/2014 End --%>
     
<%--Jaya Mamidala - CR49930 - Start--%> 

<%  dropdownOptions = new StringBuffer();
  
  	dropdownOptions.append("<option value='"+ALL_NOTIFRUN+"' selected='true' >");
  	dropdownOptions.append(resMgr.getText( ALL_NOTIFRUN, TradePortalConstants.TEXT_BUNDLE));
  	dropdownOptions.append("</option>");
 
    dropdownOptions.append("<option value= 'N' selected='true' >");
    dropdownOptions.append(resMgr.getText( READ_NOTIFS, TradePortalConstants.TEXT_BUNDLE));
    dropdownOptions.append("</option>");
  	
    dropdownOptions.append("<option value= 'Y' selected='true' >");
    dropdownOptions.append(resMgr.getText( UNREAD_NOTIFS, TradePortalConstants.TEXT_BUNDLE));
    dropdownOptions.append("</option>");

%>          
    <%=widgetFactory.createSearchSelectField("Read/Unread", "Home.Notifications.criteria.Read/Unread", defaultValue, 
        			dropdownOptions.toString(), "onChange='filterNotifications("+userCanFilterOrg+")'")%>

 <%--Jaya Mamidala - CR49930 - End--%> 

 
<%               
  	DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  	String gridLayout = dgFactory.createGridLayout("notificationsDataGridId", "NotificationsDataGrid");
  	String gridHtml = dgFactory.createDataGrid("notificationsDataGridId","NotificationsDataGrid",null);
%>
        			<%=gridHtml%>
        			<%=widgetFactory.createHoverHelp("Notifications_DelSelectedItemsText","NotificationsDelete") %>
        			<%=widgetFactory.createHoverHelp("Notifications_DeleteAll","NotificationsDeleteAll") %>
        		</div><%-- end of formContentNoSidebar div --%>

        		<%= formMgr.getFormInstanceAsInputField("NotificationsListForm", secureParms) %>
        	</form>
        </div><%-- end of pageContent div --%>
    </div><%-- end of pageMainNoSidebar div --%>

	<jsp:include page="/common/Footer.jsp">
	  	<jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	  	<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>
	
	<%@ include file="/common/GetSavedSearchQueryData.frag" %>
	
	<jsp:include page="/common/gridShowCountFooter.jsp">
	  	<jsp:param name="gridId" value="<%=gridId%>" />
	</jsp:include>

<script type="text/javascript">
var gridLayout = <%= gridLayout %>;
	var userCanFilterOrg = <%= userCanFilterOrg %>;
require(["dojo/query", "dijit/registry", "dojo/dom", "dojo/on", "t360/popup", "t360/datagrid", "dojo/ready", "dojo/domReady!"], 
		function(query, registry, dom, on, popup, datagrid, ready) {
		ready(function() {
			
			var savedSort = savedSearchQueryData["sort"];  
			var orgOption = savedSearchQueryData["currentOrgOption"]; 
			var status = savedSearchQueryData["status"]; 
	
			if("" == savedSort || null == savedSort || "undefined" == savedSort){
<%--cquinton 10/25/2013 Rel 8.3 ir#21594 get the initial sort correct--%>
		        savedSort = '-NotificationDate';
			}
		 
			if("" == orgOption || null == orgOption || "undefined" == orgOption){
				<%--  orgOption=registry.byId("OrgOption").attr('value'); --%>
				orgOption="<%=EncryptDecrypt.encryptStringUsingTripleDes(currentOrgOption, userSession.getSecretKey())%>";
			}else if(userCanFilterOrg == true){
				registry.byId("OrgOption").set('value',orgOption);<%-- SSIkhakolli - Rel-8.3 BMO UAT IR# T36000022179 on 10/23/2013  --%>
			}
			
			if("" == status || null == status || "undefined" == status){
				status=registry.byId("status").attr('value');
			}else{
				registry.byId("status").set('value',status);
			}
		
			var initSearchParms = "currentOrgOption="+orgOption+"&status="+status+"&confInd=<%=confInd%>";
			var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("NotificationsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
			createDataGrid("notificationsDataGridId", viewName,gridLayout, initSearchParms,savedSort);
		
 <%-- Added by Jaya Mamidala - CR49930 - Applying bold style for unread messages  Start--%> 	

	        var myGrid = registry.byId("notificationsDataGridId");
	        
      	    dojo.connect(myGrid, 'onStyleRow' , this, function(row)
      	     {
      	       var item = myGrid.getItem(row.index);
               if (item) 
               {
                  var type = myGrid.store.getValue(item, "UnreadFlag");
                  if (type != 'N') 
                  {
                    row.customStyles += "font-weight: bold;";
                  }
               }

              myGrid.focus.styleRow(row);
              myGrid.edit.styleRow(row);
            });
<%-- Jaya Mamidala - CR49930  End --%>});
  
		
		<%-- SSikhakolli - Rel-9.1 CR-944 IR-T36000031676 - Adding Grid Customization Popup --%>
		query('#notificationsGridEdit').on("click", function() {
            var columns = datagrid.getColumnsForCustomization('notificationsDataGridId');
            var parmNames = [ "gridId", "gridName", "columns" ];
            var parmVals = [ "notificationsDataGridId", "NotificationsDataGrid", columns ];
            popup.open(
              'notificationsGridEdit', 'gridCustomizationPopup.jsp',
              parmNames, parmVals,
              null, null);  <%-- callbacks --%>
          });
	});
  

	function filterNotifications(userCanFilterOrg) {
		require(["dojo/dom", "dijit/registry"],function(dom, registry)
		{
			var orgOption = '';
   			if(userCanFilterOrg) 
   			{
				orgOption = registry.byId("OrgOption").attr('value');
			}
			<%-- IR T36000018995 start - if user Can't Filter Org then set correct corpOrg oid --%>
			else
			{
				orgOption="<%=EncryptDecrypt.encryptStringUsingTripleDes(currentOrgOption, userSession.getSecretKey())%>";
			}
			<%-- IR T36000018995 end --%>

			var statusVal = registry.byId("status").attr('value');
			
         <%--Jaya Mamidala - CR49930 - Start--%> 			
			var readStatusVal = registry.byId("Read/Unread").attr('value');
         <%--Jaya Mamidala - CR49930 - End--%> 			
		 
			var searchParms = "currentOrgOption="+orgOption+"&status="+statusVal+"&UnreadFlag="
			                     +readStatusVal+"&confInd=<%=confInd%>";

			searchDataGrid("notificationsDataGridId", "NotificationsDataView", searchParms);
		});
	}

	<%--delete the selected records--%>
	function deleteSelectedNotifications() {
		var formName = 'NotificationsListForm';
		var buttonName = '<%=TradePortalConstants.BUTTON_DELETE%>';
		<%-- IR T36000048558 Rel9.5 05/09/2016 --%>
        var   selectRecordMessage = "<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("Mail.NotificationSelectRecord",
        TradePortalConstants.TEXT_BUNDLE)) %>";
		
		<%--get array of rowkeys from the grid--%>
		var rowKeys = getSelectedGridRowKeys("notificationsDataGridId");

		if(rowKeys == ""){
			alert(selectRecordMessage);
			return;
		}

		<%--  Sandeep IR-T36000003655 07/18/2012 Start --%>
		<%-- IR T36000048558 Rel9.5 05/09/2016 --%>
		var confirmDelete = confirm("<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("Mail.NotificationDelete",
		        TradePortalConstants.TEXT_BUNDLE)) %>");

		if(confirmDelete == true){
			<%--submit the form with rowkeys becuase rowKeys is an array, the parameter name for
			each will be checkbox + an iteration number - i.e. checkbox0, checkbox1, checkbox2, etc --%>

			submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
		}
		<%--  Sandeep IR-T36000003655 07/18/2012 End --%>
	}

	<%--  Sandeep IR-T36000003657 07/18/2012 Start --%>
	function deleteAllNotifications(){
		require(["dijit/registry","t360/dialog", "dojo/domReady!"], function(registry,dialog) {
			var grid = registry.byId("notificationsDataGridId");
			var totalRowCount=grid.store._numRows;
      
			if ( totalRowCount > 0 ) {
				dialog.open('deleteAllConfirmationDialog', deleteAllConfirmationDialogTitle, 'deleteAllConfirmationDialog.jsp', 'notificationsCount', totalRowCount, 'deleteAll', this.deleteAllConfirmationCallback);
			}else {
				alert("No notifications to delete. Please set criteria to select notifications to delete.");
			}
		});
	}

	function deleteAllConfirmationCallback(){
		require(["dijit/registry", "t360/common"], function(registry, common) {
			var formName = 'NotificationsListForm';
			var buttonName = '<%=TradePortalConstants.BUTTON_DELETE_ALL%>';
			common.submitForm(formName, buttonName);
		});
	}
	<%--  Sandeep IR-T36000003657 07/18/2012 End --%>
</script>

<%
  	//cquinton 1/18/2013 remove old close action behavior
%>
