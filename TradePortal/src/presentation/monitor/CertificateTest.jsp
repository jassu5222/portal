<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This page provides details about the parameters in the URL for debugging purposes.
 * It decrypts the parameters to determine if they would be valid for a certificate logon.
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*, java.security.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.common.*, java.text.*,java.util.*" %>

<%
  // Get page parameters 
  String orgId               = request.getParameter("organization");
  String brandingDirectory   = request.getParameter("branding");
  String locale              = request.getParameter("locale");
  String bankDate            = request.getParameter("time");
  String certData     = request.getParameter("identification");
  String requestedAuthMethod = TradePortalConstants.AUTH_CERTIFICATE;
  
  //Validation to check Cross Site Scripting
  if(orgId != null){
	  orgId = StringFunction.xssCharsToHtml(orgId);
  }
  
  String decryptedBankDate;
  String decryptedCertData = null;  
  String result = ""; 
  String certificateId = null;
  GregorianCalendar lowerBound = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
  GregorianCalendar upperBound = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
  GregorianCalendar currentTimeGregorian = new GregorianCalendar(TimeZone.getTimeZone("UTC"));

  Date formattedBankDate = null;
  Date currentTime = new java.util.Date();

  boolean noErrors = true;

  // Check for an organization ID and branding directory - these are mandatory
  if( (orgId == null) || (brandingDirectory == null) || (orgId.equals("")) || (brandingDirectory.equals("")))
   {
     result = "A branding directory and organiztion ID must be specified";
     noErrors = false;
   }
  else
   {
     // Make it so that the organization ID is not case sensitive
     orgId = orgId.toUpperCase();
   }

  if((bankDate == null) || (bankDate.equals("")))
    {
       result = "The 'time' parameter must be included in the URL";
       noErrors = false;
    }


      //////////////////////////////////////////////////////
      // VERIFY THE CERTIFICATE DATA THAT WAS PASSED IN   //
      //////////////////////////////////////////////////////

      PrivateKey portalPrivateKey = null;
      PublicKey orgPublicKey = null;
   
      // If no problems so far, proceed with verifying the date passed in
      if(noErrors)
       {
          portalPrivateKey = StoredCertificateData.getPortalPrivateKeyFromFile();
          orgPublicKey     = StoredCertificateData.getPublicKeyFromFile(orgId);

	         String encryptedCertIdData  = certData.substring(0, certData.length() - 172);
	         String certDataSignature    = certData.substring(certData.length() - 172, certData.length());

	         // Decrypt the certData URL parameter using the portal's private key
	         // The result of this decryption will be the certificate ID appended with a 
	         // digital signature in base64 format
	         decryptedCertData = EncryptDecrypt.decryptUsingPrivateKey(encryptedCertIdData, portalPrivateKey);

	         // Verify that digital signature for the certificate ID is valid using the bank's public key
	         if (!EncryptDecrypt.verifyDigitalSignature(decryptedCertData, certDataSignature, orgPublicKey)) 
	          {
	              result = "Certificate data from link does not have a valid signature: certificate ID = 	"+decryptedCertData;
	              noErrors = false;
	          }
          
         // The date and the signature must be parsed out
         // The signature is always the same length (172 base64 characters) if 1024 bit keys are used
         String encryptedDate      = bankDate.substring(0, bankDate.length() - 172);
         String dateSignature      = bankDate.substring(bankDate.length() - 172, bankDate.length());  

         // Decrypt the date URL parameter using the portal's private key
         // The result of this decryption will be the date appended with a digital signature in base64 format
         decryptedBankDate = EncryptDecrypt.decryptUsingPrivateKey(encryptedDate, portalPrivateKey);

         // Verify that digital signature is valid using the bank's public key
         if(!EncryptDecrypt.verifyDigitalSignature(decryptedBankDate, dateSignature, orgPublicKey))
          {
            noErrors = false;
            result = "Bank date from link does not have a valid signature";
          }
         else
          {
            // Remove hard coded characters from the date string.
            // The string is sent in as YYYY-MM-DDThh:mm:ssZ
            //    For example, 2002-01-15T02:13:22Z
            // Removed the T and the Z
            char[] decryptedDateChars = decryptedBankDate.toCharArray();
            decryptedDateChars[10] = ' ';
            decryptedBankDate = new String(decryptedDateChars, 0, 19); 
           SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            
            try {
                 formattedBankDate = formatter.parse(decryptedBankDate);
            } catch (ParseException e) {
                 result = "ParseException when parsing date";
                 noErrors  = false;
            }
            
            int toleranceAmt = 5;

            upperBound.setTime(currentTime);
            upperBound.add(Calendar.MINUTE, toleranceAmt);

            toleranceAmt *= -1;

            lowerBound.setTime(currentTime);
            lowerBound.add(Calendar.MINUTE, toleranceAmt);

            currentTimeGregorian.setTime(currentTime);

            if (formattedBankDate.after(lowerBound.getTime()) && formattedBankDate.before(upperBound.getTime())) {
                 noErrors  = true;
            } else {
                 noErrors  = false;
                 result = "Decrypted date is more than 5 minutes different from the portal's date";
            }
         }
         
        }
         
  %>

<font face="Arial" size=3>
<b>Trade Portal Certificate Link Diagnostic Page</b>
<br>
<br>
<font face="Arial" size=2>
The portal will examine the URL to get the parameters used for a certificate logon.   Those parameters
must be present for this page to function.  You do not need a certificate to use this page.   Just copy and
paste the parameters (everything after and inclusive of the question mark) from the certificate link to the URL 
for this page.
<br>
<br>
<%
	// GGAYLE - 06/21/05 - IR GGUF062154590
	PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
	String serverName = portalProperties.getString("serverName");
	// GGAYLE - 06/21/05 - IR GGUF062154590
%>
<table border=1>
<tr><td><font face="Arial" size="2"><b>Server Instance</td><td><font face="Arial" size="2"><%=serverName%></td></tr>
<tr><td><font face="Arial" size="2"><b>Organization ID</td><td><font face="Arial" size="2"><%=StringFunction.xssCharsToHtml(orgId)%></td></tr>
<tr><td><font face="Arial" size="2"><b>Decrypted Date/Time</td><td><font face="Arial" size="2"><%=formattedBankDate%></td></tr>
<tr><td><font face="Arial" size="2"><b>Current Date/Time on Portal Server</td><td><font face="Arial" size="2"><%=currentTimeGregorian.getTime() %></td></tr>
<tr><td><font face="Arial" size="2"><b>Lower Bound of Valid Time</td><td><font face="Arial" size="2"><%=lowerBound.getTime()%></td></tr>
<tr><td><font face="Arial" size="2"><b>Upper Bound of Valid Time</td><td><font face="Arial" size="2"><%=upperBound.getTime()%></td></tr>
<tr><td><font face="Arial" size="2"><b>Decrypted Cert Data (Cert ID)</td><td><font face="Arial" size="2"><%=decryptedCertData %></td></tr>
</table>
<br><br>
<table>
<%
if(result.equals(""))
 {%>
   <tr><td><font face="Arial" size="3"><b>Result:</td><td><font face="Arial" size="2" color="green"><b>Link Data is Valid</td></tr>
<% } else { %>
    <tr><td><font face="Arial" size="3"><b>Result:</td><td><font face="Arial" size="3" color="red"><b>Not Valid.  Reason = <%=result%></td></tr>  
<% } %>
</table>







