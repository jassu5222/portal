<%--
 *
 *     Copyright  � 2002
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
 * This page performs a test of running a servlet (dynamically generating this page),
 * accessing an EJB (QueryListView) and reading data from the database.
 *
 *
--%>
<%@ page import="com.amsinc.ecsg.web.*,
                 com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,
                 com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*,
                 com.ams.tradeportal.common.*,
                 javax.naming.Context,
                 javax.naming.InitialContext,
                 javax.resource.cci.ConnectionFactory,
                 javax.resource.cci.Connection,
                 java.text.*,java.util.*" %>
<%
    boolean success = false;
    try
    {
%>
<html>
  <head>
    <title>Trade Portal Imaging Diagnostic Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>
  </head>

  <jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
<%
        formMgr.init(request.getSession(true), "AmsServlet");
%>
  </jsp:useBean>

<%
        // Make sure formMgr is initialized
        formMgr.init(request.getSession(true), "AmsServlet");

        // Because this is just a test page, kill the session
        request.getSession(false).invalidate();

        //cquinton 11/2/2011 Rel 7.1 ir ciul112853396 start
        //default to use ISRA, but if property is set use VeniceBridge
        PropertyResourceBundle tpProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
        boolean useISRA = true;
       // Commenting VeniceBridge Rpasupulati #T36000013473 start
//         try {
//             String imageImpl = tpProperties.getString("imageImpl");
//             if ( "VeniceBridge".equalsIgnoreCase( imageImpl ) ) {
//                 useISRA = false;
//             }
//         } catch ( Exception ex ) {
//             //do nothing, already default to useISRA=true
//         }
// Commenting VeniceBridge Rpasupulati end
        //cquinton 11/2/2011 Rel 7.1 ir ciul112853396 end
        

        ImagingIdAndPassword idandpass = ImagingServices.getNextIdAndPassword();
        String userName = idandpass.getId();
        String password = idandpass.getPassword();

        //cquinton 11/2/2011 Rel 7.1 ir ciul112853396 start
        if ( useISRA ) {
            javax.resource.cci.Connection connection = null;
            try {
                PropertyResourceBundle servletProperties = (PropertyResourceBundle) 
                    PropertyResourceBundle.getBundle("TradePortal");
                Context context = new InitialContext();
                ConnectionFactory connectionFactory = (ConnectionFactory)
                    context.lookup(servletProperties.getString(TradePortalConstants.ISRA_JNDI));

                com.filenet.is.ra.cci.FN_IS_CciConnectionSpec connectionSpec =
                    new com.filenet.is.ra.cci.FN_IS_CciConnectionSpec(userName,password,0);
                connection = connectionFactory.getConnection(connectionSpec);

                if (connection != null)
                {
                    success = true;
                }
            }
            catch(Exception e){
                success = false;
                System.out.println("tp_imaging_diagnostic: Exception: " +
                    " " + e.toString() );
                e.printStackTrace();
            }
            finally {
                //close the connection
                if ( connection!=null ) {
                    try {
                        connection.close();
                        connection=null;
                    }
                    catch(Exception e){
                        System.out.println("tp_imaging_diagnostic: Problem closing connection: " +
                            " " + e.toString() );
                        connection=null;
                    }
                }
            }
        }
        else {
        //cquinton 11/2/2011 Rel 7.1 ir ciul112853396 end
            com.venetica.vbr.client.Repository vbrRepository = null;
            com.venetica.vbr.client.Content vbrContent = null;

            try
            {
                com.venetica.vbr.client.User vbrUser = new com.venetica.vbr.client.User();
                vbrUser.initialize();

                if (vbrUser != null)
                {
                    vbrRepository = vbrUser.getRepositoryByID("PanagonIS");
                    vbrRepository.logon(userName,password,"");
                    vbrRepository.logoff();
                    success = true;
                }
            }
            catch (Exception e)
            {
                success = false;
                System.out.println("tp_imaging_diagnostic: Exception: " +
                    " " + e.toString() );
                e.printStackTrace();
            }
        //cquinton 11/2/2011 Rel 7.1 ir ciul112853396 start
        }
        //cquinton 11/2/2011 Rel 7.1 ir ciul112853396 end
    }
    catch(Exception e)
    {
        // If there were any exceptions, set to false
        success = false;
    }
    finally
    {
        if(success)
        {
%>
  <body bgcolor="#009900">
    <table width="100%" height="100%">
      <tr>
        <td bgcolor="#009900">
<%
        }
        else
        {
%>
  <body bgcolor="#FF0000">
    <table width="100%" height="100%">
      <tr>
        <td bgcolor="#FF0000">
<%
        }
    }
%>
          <div align="center"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"><strong>
<%
    PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
    String serverName = portalProperties.getString("serverName");
%>
     <%=serverName %> &nbsp;Imaging
          </strong></font></div>
        </td>
      </tr>
    </table>
  </body>
</html>
