<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved Checking CheckOut checkIn
--%>
<%--
 *
 *  This page is to be used for testing only.  It provides an easy
 *  access point to the Trade Portal.  For users that use certificates
 *  for authentication, it provides a sample of how the client banks
 *  will encrypt and sign URL parameters to be sent to the portal.
 *
--%>
<%-- Checking whether Check in Check out is working or not --%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.html.*,com.amsinc.ecsg.frame.*, java.security.*,
        com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.html.*, 
	  com.ams.tradeportal.common.*, java.text.*, java.util.ArrayList" %>


<html>

<head>
  <META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>
</head>
<font face="Arial" size="3">
<b><i>Trade Portal</i> Testing Start Page</b>
<br><br>
<%

	PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");

	String serverName = portalProperties.getString("serverName");
        String SSLportNumber;

        if((serverName != null) && (!serverName.equals("")))
         {
            out.print("Server Instance: "+serverName);
         }
        
        String version = "";
        try {
            PropertyResourceBundle portalVersionProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortalVersion");
            version = portalVersionProperties.getString("version");
        } catch (MissingResourceException e) { }

        out.print("<br>Version: " + version);


        // Get the port that links should be set to for certificates
        // Usually, for the Apache web server, this port is the same as the
        // non-certificate port.  For IIS or WebLogic web server, this port sometimes
        // needs to be different
        try
         {
            SSLportNumber = portalProperties.getString("webServerSSLPort"); 
            if(SSLportNumber.equals(""))
                 SSLportNumber = Integer.toString(request.getServerPort());
         }
        catch(Exception e)
         {
            SSLportNumber = Integer.toString(request.getServerPort());
         }
%>

<br><br>
<font color=red><b>This page is only for testing.   It will NOT be in the production version.</b></font>
<br>
<br>
In production, users will access the Trade Portal by clicking a link of their bank's web site. The link needs to contain certain information in order for the 
user to properly access the system.   The links below simulate the links that will be present on the bank's web site.
<br>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
  <%
    formMgr.init(request.getSession(true), "AmsServlet");
  %>
</jsp:useBean>

<%
  // We always need to initialize the form manager anytime we return to this page.
  // This is because the session timeout/invalidation does something strange to this
  // bean.  Calling init takes care of this problem.  Multiple init's does not 
  // have any adverse affect.
  formMgr.init(request.getSession(true), "AmsServlet");

StringBuilder sql;
  Vector v;
%>
<hr>
<br>
The appropriate link is displayed below for each user.   Each user logs using either a password or a certificate.    
Note that Login IDs are case sensitive.   The links for users that use certificates expire quickly and require you to have a certificate for that user.
The list is sorted by organization type, organization name, login ID.</font>
<br>
<%

    ////////////////////////////////////////
    // CREATE LIST OF USERS AT ALL LEVELS //
    ////////////////////////////////////////

  //cquinton 4/21/2011 Rel 7.0.0 ir#cuul022550966 - add performance timings
  long queryBeginTime = System.currentTimeMillis();

  DocumentHandler usersDoc = new DocumentHandler();


 
  try {
     
	// Build SQL to get a list of users, secure cert key, and their organization
      sql = new StringBuilder();
      sql.append(" select login_id as Login, user_identifier as ID, b.name as OrgName");
      sql.append(" ,'organization=ASP&branding=' || b.branding_directory as urlParms");//No branded buttons for ASP
      sql.append(" , b.authentication_method as AUTH_METHOD, users.authentication_method as USER_AUTH_METHOD"); //Nar CR-1071 take users.authentication_method instead null
      sql.append(" , users.certificate_id as CERT_ID");
      sql.append(" , 'ASP' as otl_id");
      sql.append(" , locale as LOCALE");
      sql.append(" , null as AESKEY");
      //cquinton 4/4/2011 Rel 7.0.0 add ssoid
      sql.append(" , users.sso_id as SSOID");
      sql.append(" , 1 as org_type"); // W Zhu 6/22/2015 Rel 9.3.5 CR-1033 add org_type for better sorting
      sql.append(" from users, global_organization b");
      sql.append(" where users.ownership_level = 'GLOBAL'");
      sql.append(" and users.p_owner_org_oid = b.organization_oid");
      sql.append(" and users.activation_status = 'ACTIVE'");      
	  
	  sql.append(" union");

      sql.append(" select login_id as Login, user_identifier as ID, b.name as OrgName");
      sql.append(" ,'organization=' || b.otl_id || '&branding=' || b.branding_directory as urlParms");
      sql.append(" , b.authentication_method as AUTH_METHOD, users.authentication_method as USER_AUTH_METHOD");//Nar CR-1071 take users.authentication_method instead null
      sql.append(" , users.certificate_id as CERT_ID");
      sql.append(" , b.otl_id as otl_id");
      sql.append(" , locale as LOCALE");
      sql.append(" , b.encryption_salt as AESKEY");
      //cquinton 4/4/2011 Rel 7.0.0 add ssoid
      sql.append(" , users.sso_id as SSOID");
      sql.append(" , 2 as org_type");
      sql.append(" from users, client_bank b");
      sql.append(" where users.ownership_level = 'BANK'");
      sql.append(" and users.p_owner_org_oid = b.organization_oid");
      sql.append(" and users.activation_status = 'ACTIVE'");

      sql.append(" union");

      sql.append(" select login_id as Login, user_identifier as ID, b.name as OrgName");
      sql.append(" ,'organization=' || c.otl_id || '&branding=' || c.branding_directory as urlParms");
      sql.append(" , c.authentication_method as AUTH_METHOD, users.authentication_method as USER_AUTH_METHOD");//Nar CR-1071 take users.authentication_method instead null
      sql.append(" , users.certificate_id as CERT_ID");
      sql.append(" , c.otl_id as organizationID");
      sql.append(" , locale as LOCALE");
      sql.append(" , c.encryption_salt as AESKEY");
      //cquinton 4/4/2011 Rel 7.0.0 add ssoid
      sql.append(" , users.sso_id as SSOID");
      sql.append(" , 3 as org_type");
      sql.append(" from users, bank_organization_group b, client_bank c");
      sql.append(" where users.ownership_level = 'BOG'");
      sql.append(" and users.p_owner_org_oid = b.organization_oid");
      sql.append(" and users.activation_status = 'ACTIVE'");
      sql.append(" and b.p_client_bank_oid = c.organization_oid");
      sql.append(" union");

      sql.append(" select login_id as Login, user_identifier as ID, b.name as OrgName");
      sql.append(" ,'organization=' || c.otl_id || '&branding=' || d.branding_directory as urlParms");
      sql.append(" , b.authentication_method as AUTH_METHOD, users.authentication_method as USER_AUTH_METHOD");
      sql.append(" , users.certificate_id as CERT_ID");
      sql.append(" , c.otl_id as organizationID");
      sql.append(" , locale as LOCALE");
      sql.append(" , c.encryption_salt as AESKEY");
      //cquinton 4/4/2011 Rel 7.0.0 add ssoid
      sql.append(" , users.sso_id as SSOID");
      sql.append(" , 4 as org_type");
      sql.append(" from users, corporate_org b, client_bank c, bank_organization_group d");
      sql.append(" where users.ownership_level = 'CORPORATE'");
      sql.append(" and users.p_owner_org_oid = b.organization_oid");
      sql.append(" and users.activation_status = 'ACTIVE'");
      sql.append(" and users.a_client_bank_oid = c.organization_oid");
      sql.append(" and b.a_bank_org_group_oid = d.organization_oid");
      sql.append(" order by org_type, orgname, login");
      usersDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, new ArrayList());

    } 
   catch (Exception e)
    {
      e.printStackTrace();
    } 
  
  //cquinton 4/21/2011 Rel 7.0.0 ir#cuul022550966 - add performance timings
  long queryEndTime = System.currentTimeMillis();


  v = usersDoc.getFragments("/ResultSetRecord");

    ////////////////////////////
    //  BUILD TABLE OF USERS  //
    ////////////////////////////

  //cquinton 4/21/2011 Rel 7.0.0 ir#cuul022550966 - add performance timings
  long renderBeginTime = System.currentTimeMillis();
%>
<br>
<table cellspacing=0 border=0 cellpadding=0>
  <tr bgcolor=eeeecc>
    <%--cquinton 4/19/2011 Rel 7.0.0 add SSO ID to Login ID column--%>
    <td width=5>&nbsp;</td><td><font face="Arial" size="2"><b>Login ID/SSO ID</b></td>
    <td width=30>&nbsp;</td>
    <td><font face="Arial" size="2"><b>Name</b></td>
    <td width=30>&nbsp;</td>
    <td><font face="Arial" size="2"><b>Region Setting</b></td>
    <td width=5>&nbsp;</td>
    <td><font face="Arial" size="2"><b>Organization</b></td>
    <td width=5>&nbsp;</td>
    <td><font face="Arial" size="2"><b>Login Type</b></td>
    <td width=30>&nbsp;</td>
  </tr>
<%

  String authMethod = "";
  String urlParms = "";
  String name = "";
  String certLogonPage = NavigationManager.getNavMan().getPhysicalPage("CertificateLogon", request);
  String passwordLogonPage = NavigationManager.getNavMan().getPhysicalPage("PasswordLogon", request);
  
  //Ravindra - 10/01/2011 - CR-541 - Start
  String tradePortalLogonPage = NavigationManager.getNavMan().getPhysicalPage("TradePortalLogon", request);
  //Ravindra - 10/01/2011 - CR-541 - End
  
  String logonLink;
  String loginID;
  //cquinton 4/4/2011 Rel 7.0.0 add ssoid
  String ssoID;

  // Create a row for each user 
  int loginCount = 0;
  for(int i=0;i<v.size();i++)
   {
       String bgcolor;
       // Alternate color of rows
       if((i%2) == 0)
        bgcolor="E5E5E5";
       else
        bgcolor="CCCCCC";

       name = ((DocumentHandler)v.elementAt(i)).getAttribute("/ID");
       // NShrestha - 07/09/2009 - IR#RRUJ061662340 - Begin --
       name = StringFunction.xssCharsToHtml(name);
       // NShrestha - 07/09/2009 - IR#RRUJ061662340 - End --
      
       authMethod = ((DocumentHandler)v.elementAt(i)).getAttribute("/AUTH_METHOD");
       
       urlParms = ((DocumentHandler)v.elementAt(i)).getAttribute("/URLPARMS");
       urlParms += "&locale="+((DocumentHandler)v.elementAt(i)).getAttribute("/LOCALE");

       // If authentication method is set at the user level, user the user's auth method instead
       if(authMethod.equals(TradePortalConstants.AUTH_PERUSER))
        { 
            authMethod = ((DocumentHandler)v.elementAt(i)).getAttribute("/USER_AUTH_METHOD");
        }  
       
       loginID = ((DocumentHandler)v.elementAt(i)).getAttribute("/LOGIN");
       //cquinton 4/4/2011 Rel 7.0.0 add ssoid
       ssoID = ((DocumentHandler)v.elementAt(i)).getAttribute("/SSOID");

       //cquinton 3/18/2011 Rel 7.0.0 add extra links
       String extraLinks = " ";

       if (authMethod.equals(TradePortalConstants.AUTH_CERTIFICATE))
        {
          try
           {
	            ////////////////////////////
	            //  CERTIFICATE USER
	            //    1. Get system time
	            //    2. Digitally sign system time (formatted as YYYY-MM-DDThh:mm:ssZ ['T' and 'Z' are hardcoded])
	            //    3. Encrypt system time and signature using Trade Portal public key
	            //    4. Digitally sign certificate ID
	            //    5. Encrypt certificate ID and signature using Trade Portal pub 
	            ////////////////////////////
	
	          SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	          String isoDate = "";
	
	  	      isoDate = iso.format(GMTUtility.getGMTDateTime());
	          isoDate = isoDate.replace(' ','T');
	          isoDate = isoDate + "Z";
	
	          Debug.debug("Date being used in certificate link is: "+isoDate);
	
	          String certId = ((DocumentHandler)v.elementAt(i)).getAttribute("/CERT_ID");
	          String orgName = ((DocumentHandler)v.elementAt(i)).getAttribute("/OTL_ID");
	 
	          // Get keys used for signing and encryption
	          PrivateKey bankPrivateKey = StoredCertificateData.getPrivateKeyFromFile(orgName);
	          PublicKey portalPublicKey = StoredCertificateData.getPortalPublicKeyFromFile();

                  if((bankPrivateKey != null) && (portalPublicKey != null))
                   {
		          // Create digital signatures of ISO Date and Certificate ID
		          String signatureOfIsoDate = EncryptDecrypt.createDigitialSignature(isoDate, bankPrivateKey);
		          String signatureOfCertId = EncryptDecrypt.createDigitialSignature(certId, bankPrivateKey);
	
		          // Encrypt the signature appended to the text for ISO date and certificate ID
		          String encryptedIsoDate = 
		                        EncryptDecrypt.encryptUsingPublicKey(isoDate, portalPublicKey); 
		          String encryptedCertId = 
		                        EncryptDecrypt.encryptUsingPublicKey(certId, portalPublicKey);
		
                          String cr99 = request.getParameter("cr99");

                          if(cr99 == null)
			   {
		              // Add the encrypted values to the URL params
		              urlParms += "&time=" + encryptedIsoDate + signatureOfIsoDate;
		              urlParms += "&identification=" + encryptedCertId + signatureOfCertId;
			   }

		          // Build link to CertLogon page using port number specified in TradePortal.properties
       		          logonLink = "<A HREF=\"https://" + request.getServerName() + ":"+SSLportNumber;
                          logonLink = logonLink + request.getContextPath()+ certLogonPage + "?" + urlParms + "\"";
                          logonLink = logonLink + " target=" + name + ">" + name + "</a>";            
                   }
                  else
                   {
                      // Only display the ID if a certificate link cannot be created
                      logonLink = name;
                   }
           }
          catch(Exception e)
           {
                System.out.println("Exception caught while creating testing logon link for: "+((DocumentHandler)v.elementAt(i)).getAttribute("/CERT_ID"));
                logonLink="";
           }

       } 

      //cquinton 3/18/2011 add SSO - default is still password
      else if (authMethod.equals(TradePortalConstants.AUTH_SSO))
      {
	 // cquinton 4/19/2011 Rel 7.0.0 password doesn't make sense this should be the sso method
         // TODO: build link for SSO auth method!!! note aes256 sso is secondary, done below
         //logonLink = "<A HREF=\""+ request.getContextPath()+ passwordLogonPage + "?" + urlParms + "\"";
         //logonLink = logonLink + " target=" + name + ">" + name + "</a>";       
         logonLink = name;

         //now build an extra link for AUTH_AES256...
         try { // if any problems just don't display the link
            String userId = ssoID;
            String aesEncryptionSalt = ((DocumentHandler)v.elementAt(i)).getAttribute("/AESKEY");

            //decrypt the stored key
            byte[] saltBytes = EncryptDecrypt.base64StringToBytes(aesEncryptionSalt);
            byte[] decryptedSalt = EncryptDecrypt.decryptUsingDes3(saltBytes);
            String decryptedAESKey = EncryptDecrypt.bytesToBase64String(decryptedSalt);

            //build identification parm and encrypt it
            SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //cquinton 4/18/2011 Rel 7.0.0 - don't use override
            String isoDate = iso.format(GMTUtility.getGMTDateTime(true,false));
            isoDate = isoDate.replace(' ','T');
            isoDate = isoDate + "Z";
            userId = userId + ";" + isoDate;
            String encryptedUserIdAndTime = EncryptDecrypt.encryptUsingAESKey(userId, decryptedAESKey);
            if(encryptedUserIdAndTime != null && encryptedUserIdAndTime.indexOf('+')>=0){
               encryptedUserIdAndTime = encryptedUserIdAndTime.replaceAll("\\+", "%2B");
            }
            String ssoAESParms = urlParms + "&identification=" + ((encryptedUserIdAndTime!=null)?encryptedUserIdAndTime:"");
            ssoAESParms += "&auth=" + TradePortalConstants.AUTH_AES256;

            String ssoAESlink = "<A HREF=\"" + request.getContextPath()+ tradePortalLogonPage + "?" + ssoAESParms + "\"";
            ssoAESlink = ssoAESlink + " target=" + name + ">AES256</a>";
            extraLinks += ssoAESlink;
         } catch (Exception ex) {
            // if any problems just don't display the link
         }
      }
      //cquinton 3/18/2011 - CR-541 - End
        
      else
       {
          // Build link to PasswordLogon page 
          // This link assumes user is going to the same host and port as the TestingLogon page is running on
       	  logonLink = "<A HREF=\""+ request.getContextPath()+ passwordLogonPage + "?" + urlParms + "\"";
          logonLink = logonLink + " target=" + name + ">" + name + "</a>";       

       }


       String authMethodDescr = authMethod;
       String localeDescr = ((DocumentHandler)v.elementAt(i)).getAttribute("/LOCALE");
       try {
    	   authMethodDescr = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.AUTHENTICATION_METHOD, authMethod);
       }
       catch (Exception e)  {}
       try {
           localeDescr = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.LOCALE, localeDescr);
       }
       catch (Exception e) {}

       // Create the HTML for the user's row
       out.print("<tr bgcolor='"+bgcolor+"'><td width=5>&nbsp;</td><td><font face='Arial' size='1'>");
       // NShrestha - 07/09/2009 - IR#RRUJ061662340 - Begin --
       //out.print(((DocumentHandler)v.elementAt(i)).getAttribute("/LOGIN"));
       //cquinton 4/19/2007 conditionally set SSO ID
       if (TradePortalConstants.AUTH_SSO.equals(authMethod)) {
           //cquinton 4/4/2011 Rel 7.0.0 add ssoid
           if ( ssoID != null && ssoID.length()>0 ) {
               out.print(StringFunction.xssCharsToHtml( ssoID ));
           }
       }
       else {
           out.print(StringFunction.xssCharsToHtml(loginID));
       }
       // NShrestha - 07/09/2009 - IR#RRUJ061662340 - End --
       
       out.print("</td><td width=30>&nbsp;</td><td><font face='Arial' size='1'>");
       out.print(logonLink);
       out.print("</td><td width=30>&nbsp;</td><td><font face='Arial' size='1'>");
       out.print(localeDescr);
       out.print("</td><td width=30>&nbsp;</td><td><font face='Arial' size='1'>");
       // NShrestha - 07/09/2009 - IR#RRUJ061662340 - Begin --
       //out.print(((DocumentHandler)v.elementAt(i)).getAttribute("/ORGNAME"));
       out.print(StringFunction.xssCharsToHtml(((DocumentHandler)v.elementAt(i)).getAttribute("/ORGNAME")));
       // NShrestha - 07/09/2009 - IR#RRUJ061662340 - End --
       
       out.print("</td><td width=30>&nbsp;</td><td><font face='Arial' size='1'>");
       out.print(authMethodDescr);
       //cquinton 3/18/2011 - CR-541 -add extra links after the main auth method
       out.print(extraLinks);
       out.print("</td><td width=5>&nbsp;</td></tr>");

       loginCount++;
  }

  //cquinton 4/21/2011 Rel 7.0.0 ir#cuul022550966 - add performance timings
  long renderEndTime = System.currentTimeMillis();

  long queryDuration = queryEndTime - queryBeginTime;
  long renderDuration = renderEndTime - renderBeginTime;

%>
</table>

<%--cquinton 4/21/2011 Rel 7.0.0 ir#cuul022550966 add performance timings--%>
<br/>
<font face="Arial" size="1">
Rendered <%=loginCount%> rows.  Query Time: <%=queryDuration%>ms  Render Time: <%=renderDuration%>ms
<br/>

</html>

<%
	// Because this is just a test page...
	// Kill the session so that the next page 
	// is self-sufficient in terms of the session 
	request.getSession(false).invalidate();
%>

