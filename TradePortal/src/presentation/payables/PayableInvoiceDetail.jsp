<%--
*******************************************************************************
                        Invoice Detail

  Description:
     This page is used to display the detail for an invoice.
  Although it is modeled after the standard ref data pages, this is a readonly
  page.  It is only used to display data and can never update anything.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 
--%>
<%@page import="com.ams.tradeportal.busobj.InvoicesSummaryDataBean"%>
<%@page import="com.ams.tradeportal.busobj.ArMatchingRule"%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,java.math.*,java.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%		
  boolean isReadOnly = true;

  String oid = "";

  boolean showDelete = true;
  boolean showSave   = false;

  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;

  DocumentHandler doc;

  String financeStatus = null;
  String invoiceStatus = null;
  String buyerInfo = null;
  String            helpSensitiveLink      = null;
  String currency = null;
  String useCurrency = null;	
  String current2ndNav      = null;
  DocumentHandler invoiceSummaryGoodsResult         = null;
  DocumentHandler invoiceListDetailResult           = null;
  DocumentHandler invoiceTradingPartnerDetailResult = null;
  DocumentHandler invoiceSummaryDataResult			= null;
  String reportingCode1 = "";
  String reportingCode2 = "";

  DocumentHandler invoiceGoodsResult = null;

  InvoiceWebBean_Base invoice = beanMgr.createBean(InvoiceWebBean_Base.class,  "Invoice");
  InvoicesSummaryDataWebBean_Base InvoicesSummaryData = beanMgr.createBean(InvoicesSummaryDataWebBean_Base.class, "InvoicesSummaryData"); 
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  String loginLocale = userSession.getUserLocale();
  String corpOrgOid = userSession.getOwnerOrgOid();
  boolean ignoreDefaultPanelValue = false;
  current2ndNav = request.getParameter("current2ndNav");
  // Get the document from the cache.  We'll use it to repopulate the screen 
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();

  Debug.debug("doc from cache is " + doc.toString());
  helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/trans_recvbl_mgmt_invoice_detail.htm",  resMgr, userSession);

  if (doc.getDocumentNode("/In/Invoice") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item)

     oid = request.getParameter("invoice_oid");
     if (oid != null) {
        oid = EncryptDecrypt.decryptStringUsingTripleDes(oid, userSession.getSecretKey());
     }
     if (oid == null) {
       oid = "0";
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
     }

  } else {
     // The doc does exist so check for errors.  If highest severity < WARNING,
     // its a good update.

     String maxError = doc.getAttribute("/Error/maxerrorseverity");

     if (maxError.compareTo(
                  String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
        // We've returned from a delete that was successful
        // We shouldn't be here.  Forward to the ReceivablesManagementHome page.
        formMgr.setCurrPage("goToPayableTransactionsHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("goToPayableTransactionsHome", request);

%>
        <jsp:forward page='<%=physicalPage%>' />
<%
        return;
     } else {
        // We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.

        retrieveFromDB = false;
        getDataFromDoc = true;

        // Use oid from input doc.
        oid = doc.getAttribute("/In/Invoice/invoice_oid");
     }
  }
	
	
  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Display error.
     getDataFromDoc = false;

     invoice.setAttribute("invoice_oid",oid);
     invoice.getDataFromAppServer();
     
     if (invoice.getAttribute("invoice_oid").equals("")) {
       oid = "0";
     }
     
   } 
	
	
	
  currency = StringFunction.xssCharsToHtml(invoice.getAttribute("currency_code"));
  if (StringFunction.isNotBlank(currency))
  	useCurrency = currency;
  else useCurrency = "2";
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); 
  }
  else {
    userSession.addPage("goToPayableInvoiceDetail", request);
  }
  
  
	 //jgadela  R92 IR - SQL FIX
  String invoiceSummaryGoodsSQL = "select * from invoice_goods where p_invoice_oid = ?";
  invoiceSummaryGoodsResult = DatabaseQueryBean.getXmlResultSet(invoiceSummaryGoodsSQL, false, new Object[]{oid});
  
  //Added below sql for Rel9.4 CR-1001
  String invoiceSummaryDataSQL = "select * from invoices_summary_data where invoice_id = ?";
  invoiceSummaryDataResult = DatabaseQueryBean.getXmlResultSet(invoiceSummaryDataSQL, false, new Object[]{invoice.getAttribute("invoice_reference_id")});
  if(invoiceSummaryDataResult!= null){
	  reportingCode1 = invoiceSummaryDataResult.getAttribute("/ResultSetRecord/REPORTING_CODE_1");
	  reportingCode2 = invoiceSummaryDataResult.getAttribute("/ResultSetRecord/REPORTING_CODE_2");
  }
  
  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String closeAction = backPage.getLinkAction();
  String closeParms = backPage.getLinkParametersString() + "&returning=true";
  String closeLink = formMgr.getLinkAsUrl(closeAction,closeParms,response);
 %>

<%-- ********************* HTML for page begins here *********************  --%>



<%-- Body tag included as part of common header --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
		
<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" 
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag"  
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
		
<div class="pageMain">
  <div class="pageContent">
    <div class="secondaryNav">
      <div class="secondaryNavTitle">
        <%=resMgr.getText("SecondaryNavigation.Payables",
             TradePortalConstants.TEXT_BUNDLE)%> 
      </div>
   
<%
     String matchingSelected   = TradePortalConstants.INDICATOR_NO;
  	 String invoicesSelected = TradePortalConstants.INDICATOR_YES;
     String inquiriesSelected = TradePortalConstants.INDICATOR_NO;
    
     String navLinkParms = "";
     
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__PAY_INVOICES; %>
     <jsp:include page="/common/NavigationLink.jsp">
        <jsp:param name="linkTextKey"  value="SecondaryNavigation.Payables.Invoices" />
        <jsp:param name="linkSelected" value="<%= invoicesSelected %>" />
        <jsp:param name="action"       value="goToPayableTransactionsHome" />
        <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
        <jsp:param name="hoverText"    value="PayableTransaction.invoicesList" />
      </jsp:include>
    <%
 	%>
     <div id="secondaryNavHelp" class="secondaryNavHelp">
        <%=helpSensitiveLink %>
      </div>
	  <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %>	
      <div style="clear:both;"></div>
    </div>

    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem"> 
        <span><%=resMgr.getText("InvoiceDetail.InvoiceNumber",TradePortalConstants.TEXT_BUNDLE)%></span>
        <span> 
          <% out.print("-"); %>
          <%=StringFunction.xssCharsToHtml(invoice.getAttribute("invoice_reference_id"))%>
          <%out.print("-"); %> <%out.print("("); %>
<%
  invoiceStatus = invoice.getAttribute("invoice_status");
  if (StringFunction.isBlank(invoiceStatus)) {
            out.print("&nbsp;");
  } 
  else {
    invoiceStatus = invoiceStatus.trim();
    StringBuilder dropdownOptions = new StringBuilder();
    dropdownOptions.append("<option value='");
    dropdownOptions.append(invoiceStatus);
    dropdownOptions.append("' selected ");
    dropdownOptions.append(">");
    dropdownOptions.append(ReferenceDataManager.getRefDataMgr().getDescr(
				      TradePortalConstants.INVOICE_STATUS, 
				       invoiceStatus, loginLocale));
    dropdownOptions.append("</option>");

    out.print(widgetFactory.createSelectField( "invoiceStatus" ,""," ", dropdownOptions.toString(), isReadOnly,false,false,"","","none"));
  }
%>
<%out.print(")"); if(TPDateTimeUtility.formatDateTime(invoice.getAttribute("activity_sequence_datetime"),TPDateTimeUtility.SHORT,loginLocale)!=null && !TPDateTimeUtility.formatDateTime(invoice.getAttribute("activity_sequence_datetime"),TPDateTimeUtility.SHORT,loginLocale).equals("")){
out.print(" - "+TPDateTimeUtility.formatDateTime(StringFunction.xssCharsToHtml(invoice.getAttribute("activity_sequence_datetime")),TPDateTimeUtility.SHORT,loginLocale));
}%>
        </span>
      </span>
      <span class="pageSubHeader-right">
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
       			'<%=closeLink %>';<%--- 'RPasupulati' removing xssCharsToHtml method as it effecting navigation T36000036379---%>
          </script>
        </button> 
      </span>
      <%=widgetFactory.createHoverHelp("CloseButton","InvoiceDetailCloseHoverText") %>
      <div style="clear:both;"></div>
    </div>


		<form name="InvoiceDetail" method="post" id="InvoiceDetail" data-dojo-type="dijit.form.Form"
     													 action="<%=formMgr.getSubmitAction(response)%>">
 		 <input type=hidden value="" name=buttonName>

<%
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.
  Hashtable <String,String> secureParms = new Hashtable();
  secureParms.put("UserOid", userSession.getUserOid());

  secureParms.put("invoice_oid", oid);
  secureParms.put("invoice_id", invoice.getAttribute("invoice_reference_id"));
  
%>

<%= formMgr.getFormInstanceAsInputField("InvoicesForm", secureParms) %>
<%@ include file="/transactions/fragments/LoadPanelLevelAliases.frag" %>

	<div class="formContentNoSidebar">
  <table>
    <tr>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
          
           <%=widgetFactory.createSubLabel("InvoiceDetail.IssueDate") %>                 
          </span>
          <br>
          <span class="ListText">
            <b><%=TPDateTimeUtility.formatDate(StringFunction.xssCharsToHtml(invoice.getAttribute("invoice_issue_datetime")),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
          </span>
        </p>
      </td>
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
        
                             <%=widgetFactory.createSubLabel("InvoiceDetail.DueDate") %> 
            </span>
          <br>
          <span class="ListText">
            <b><%=TPDateTimeUtility.formatDate(StringFunction.xssCharsToHtml(invoice.getAttribute("invoice_due_date")),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
	      </span>
        </p>
      </td>
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
        
                             <%=widgetFactory.createSubLabel("InvoiceDefinition.PaymentDate") %> 
            </span>
          <br>
          <span class="ListText">
           <% if (StringFunction.isNotBlank(invoice.getAttribute("invoice_payment_date"))) { %>
         
            <b><%=TPDateTimeUtility.formatDate(StringFunction.xssCharsToHtml(invoice.getAttribute("invoice_payment_date")),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
	      <% } else { %>
              &nbsp;
            <% } %>
	      </span>
        </p>
      </td>
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>      
      <td nowrap> 
        <p> <span class="ControlLabel">
        
                             <%=widgetFactory.createSubLabel("InvoiceDefinition.SupplierToSendDate") %> 
            </span>
          <br>
          <span class="ListText">
          <% if (StringFunction.isNotBlank(invoice.getAttribute("send_to_supplier_date"))) { %>
            
            <b><%=TPDateTimeUtility.formatDate(StringFunction.xssCharsToHtml(invoice.getAttribute("send_to_supplier_date")),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
             <% } else { %>
              &nbsp;
            <% } %>
	      </span>
        </p>
      </td>      
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
          
                            <%=widgetFactory.createSubLabel("Invoices.Amount") %>
          </span>
          <br>
          <span class="ListText">
            <% if (StringFunction.isNotBlank(invoice.getAttribute("invoice_total_amount"))) { %>
             <b> <%=currency%>&nbsp;
             <%=TPCurrencyUtility.getDisplayAmount(StringFunction.xssCharsToHtml(invoice.getAttribute("invoice_total_amount")), useCurrency, loginLocale)%></b>
            <% } else { %>
              &nbsp;
            <% } %>
	  </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
          
                            <%=widgetFactory.createSubLabel("UploadInvoices.PaymentAmount") %>
          </span>
          <br>
          <span class="ListText">
            <% if (StringFunction.isNotBlank(invoice.getAttribute("invoice_payment_amount"))) { %>
             <b> <%=currency%>&nbsp;
             <%=TPCurrencyUtility.getDisplayAmount(StringFunction.xssCharsToHtml(invoice.getAttribute("invoice_payment_amount")), useCurrency, loginLocale)%></b>
            <% } else { %>
              &nbsp;
            <% } %>
	  </span>
        </p>
      </td>

      
      
      
          </tr>
  </table>
  <table>
    <tr>
 
  
      <%--net invoice amount --%>
      <td width="2%" nowrap>&nbsp;</td>
     <td >
    <p> <%=widgetFactory.createSubLabel("UploadInvoices.CreditNoteAppliedAmt") %>
    <br>
    <% if(StringFunction.isNotBlank(invoice.getAttribute("total_credit_note_amount"))) {%>
    <b><%=currency%></b>&nbsp;
        <b><%=TPCurrencyUtility.getDisplayAmount(invoice.getAttribute("total_credit_note_amount"), useCurrency, loginLocale)%></b>
    <% } else { %> &nbsp; <% } %> </p>
    </td>      

<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>
      
    <td >
    <p> <%=widgetFactory.createSubLabel("UploadInvoices.NetInvoiceAmount") %>
    <br>
    <% String invTotalAmt =  (StringFunction.isNotBlank(invoice.getAttribute("invoice_payment_amount"))
    		&& new BigDecimal(invoice.getAttribute("invoice_payment_amount")).compareTo(BigDecimal.ZERO) != 0) ?
            invoice.getAttribute("invoice_payment_amount"):invoice.getAttribute("invoice_total_amount");
            BigDecimal amt = new BigDecimal(invTotalAmt);
    		BigDecimal totalCRAmt = BigDecimal.ZERO;
    		if(StringFunction.isNotBlank(invoice.getAttribute("total_credit_note_amount"))){
    		totalCRAmt=new BigDecimal(invoice.getAttribute("total_credit_note_amount"));
    		}
    		
    amt = amt.subtract(totalCRAmt.abs());
    if(StringFunction.isNotBlank(amt.toPlainString())) {  %>
    <b><%=currency%></b>&nbsp;
        <b><%=TPCurrencyUtility.getDisplayAmount(amt.toPlainString(), useCurrency, loginLocale)%></b>
    <% } else { %> &nbsp; <% } %> </p>
    </td>
   
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>
     
       <td> 
        <p> <span class="ControlLabel">
         
                            <%=widgetFactory.createSubLabel("InvoiceDetail.FinanceStatus") %>
          </span>
          <br>
          <span class="ListText">
<%
  financeStatus = invoice.getAttribute("finance_status");
if (StringFunction.isBlank(financeStatus)) {
	financeStatus="";
} else {
	financeStatus = financeStatus.trim();
	financeStatus = ReferenceDataManager.getRefDataMgr().getDescr(
				      TradePortalConstants.INVOICE_FINANCE_STATUS, 
				      financeStatus, loginLocale);
 
}
%>
	  </span>
        <b><%=financeStatus %></b>
</p>
      </td>    
 
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	

 
      <td>
        <p> <span class="ControlLabel">
         
                            <%=widgetFactory.createSubLabel("InvoiceDetail.InvoiceStatus") %>
          </span>
          <br>
          <span class="ListText">
 <%
  String invoicestatus = invoice.getAttribute("invoice_status");
  if (StringFunction.isBlank(invoicestatus)) {
	  invoicestatus="";
  } else {
    invoicestatus = invoicestatus.trim();
   invoicestatus = ReferenceDataManager.getRefDataMgr().getDescr(
				      TradePortalConstants.INVOICE_STATUS, 
				       invoicestatus, loginLocale);
   
  }
%> 
	  </span>
       <b><%=invoicestatus %></b>
</p>
</td>
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>
    <p> <span class="ControlLabel">
          <%=widgetFactory.createSubLabel("UploadInvoices.EndToEndId") %>
        </span>
        <br>
       <%  if(StringFunction.isNotBlank(invoice.getAttribute("end_to_end_id"))) {  %>
     <b><%=invoice.getAttribute("end_to_end_id") %></b>&nbsp;
       
    <% } else { %> &nbsp; <% } %> </p>
    
</td>
     
    </tr>
  </table>
&nbsp;
 <div class="formContentNoBorder">
					<div class="columnLeftNoBorder">
					 <%=widgetFactory.createSubsectionHeader("InvoicesOfferedDetail.Seller")%>
   
                      <div class="formItem">
         
                 <%=StringFunction.xssCharsToHtml(invoice.getAttribute("seller_party_identifier"))%>  <br/>
                 <%=StringFunction.xssCharsToHtml(invoice.getAttribute("seller_address_line_1"))%>  <br/>
  	      		 <%=StringFunction.xssCharsToHtml(invoice.getAttribute("seller_address_line_2"))%>  <br/>
  	      		 <%=StringFunction.xssCharsToHtml(invoice.getAttribute("seller_address_country"))%>  <br/>
  	      		  
          <div style="clear: both"></div>
          </div>                  
        </div>
       
         </div>  


  <div style="clear: both"></div>
<div class="formContentNoBorder">
<span class="columnLeftNoBorder" style="width: 125%;">
<%=widgetFactory.createWideSubsectionHeader("UploadInvoiceDetail.InvoiceSummaryDetail") %>

<% if( invoiceSummaryGoodsResult != null ) {
     List<DocumentHandler> summaryGoodsList = invoiceSummaryGoodsResult.getFragmentsList("/ResultSetRecord");        
           
     int totalSummaryGoods = summaryGoodsList.size();
     //BSL IR NLUM040346023 04/09/2012 BEGIN
     if (totalSummaryGoods > 0) {
    	 // Only display the first Goods Summary
    	 totalSummaryGoods = 1;
     }
     //BSL IR NLUM040346023 04/09/2012 END
     for( DocumentHandler summaryGoods : summaryGoodsList) {	      
   %>
<div class="columnLeftNoBorder">
<div class="formItem">
<table class="poContent">
<%--DK IR T36000014551 Rel8.2 03/12/2013 --%>
<% if(!StringFunction.isBlank(summaryGoods.getAttribute("/GOODS_DESCRIPTION")) ||
		!StringFunction.isBlank(summaryGoods.getAttribute("/INCOTERM")) ||
		!StringFunction.isBlank(summaryGoods.getAttribute("/COUNTRY_OF_LOADING")) ||
		!StringFunction.isBlank(summaryGoods.getAttribute("/COUNTRY_OF_DISCHARGE")) ||
		!StringFunction.isBlank(summaryGoods.getAttribute("/VESSEL")) ||
		!StringFunction.isBlank(summaryGoods.getAttribute("/CARRIER")) ||
		!StringFunction.isBlank(summaryGoods.getAttribute("/ACTUAL_SHIP_DATE")) ||
		!StringFunction.isBlank(summaryGoods.getAttribute("/PO_REFERENCE_ID"))){%>
	<tr> 
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.GoodsDescr")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(summaryGoods.getAttribute("/GOODS_DESCRIPTION"))%></b>
		
		</td>
	</tr>

	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.Incoterm")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(summaryGoods.getAttribute("/INCOTERM"))%></b>
		
		</td>
	</tr>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.PortofLoading")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(summaryGoods.getAttribute("/COUNTRY_OF_LOADING"))%></b>
		
		</td>
	</tr>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.PortofDischarge")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(summaryGoods.getAttribute("/COUNTRY_OF_DISCHARGE"))%></b>
		
		</td>
	</tr>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.Vessel")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(summaryGoods.getAttribute("/VESSEL"))%></b>
		
		</td>
	</tr>
	<%-- BSL IR NLUM040346023 04/11/2012 BEGIN --%>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.Carrier")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(summaryGoods.getAttribute("/CARRIER"))%></b>
		
		</td>
	</tr>
	<%-- BSL IR NLUM040346023 04/11/2012 END --%>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.ActualShpDate")%>
		
		</td>
		<td >
		<b><%=TPDateTimeUtility.formatDate(StringFunction.xssCharsToHtml(summaryGoods.getAttribute("/ACTUAL_SHIP_DATE")),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
		</td>
	</tr>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.PurchaseOrderId")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(summaryGoods.getAttribute("/PO_REFERENCE_ID"))%></b>
		
		</td>
	</tr>
<%} %>
</table>
</div>
</div>
<div class="columnRightNoBorder">
<div class="formItem">
<table>
	<% for (int i = 1; i<= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i++) {
        if(!StringFunction.isBlank(summaryGoods.getAttribute("/SELLER_INFORMATION_LABEL" + i ))){%>
	<tr>
		<td nowrap>
		<%=StringFunction.xssCharsToHtml(summaryGoods.getAttribute("/SELLER_INFORMATION_LABEL" + i ))%>
		
		</td>
		<td nowrap>&nbsp;</td>
		<td>
		<b><%=StringFunction.xssCharsToHtml(summaryGoods.getAttribute("/SELLER_INFORMATION" + i))%></b>
		
		</td>
	</tr>
	<%}}%>
	
</table>

</div>
</div>
<%} }%>
<%--DK IR T36000014551 Rel8.2 03/12/2013 --%>
</div>

<%-- Srinivasu_D CR#1006 Rel9.3 05/06/2015 Added Payment section --%>

<div style="clear: both"></div>
<%
				
	if (StringFunction.isNotBlank(invoice.getAttribute("pay_method"))){
%>
<div class="formContentNoBorder">
<%=widgetFactory.createWideSubsectionHeader("UploadInvoiceDetail.PaymentDetails") %><br>

<div class="columnLeft33NoBorder">
<div class="formItem">
<table class="poContent" style="width:110%;" cellspacing="2">
			<tr>	
				<td >
				<p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.PaymentMethod") %> :      <br/><b><%=invoice.getAttribute("pay_method")%></b> 
		         </p>
				</td>
			</tr>
			<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenAcctNumber") %> :      <br/><b><%=invoice.getAttribute("ben_acct_num")%></b> </p>
				</td>
			</tr>
			<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenAddr1") %> :     <br/><b><%=invoice.getAttribute("ben_add1")%></b> </p>
				</td>
			</tr>
			<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenAddr2") %> :      <br/><b><%=invoice.getAttribute("ben_add2")%></b> </p>
				</td>
			</tr>
			<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenAddr3") %> :      <br/><b><%=invoice.getAttribute("ben_add3")%></b> </p>
				</td>
			</tr>
			<tr>		
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenCountry") %> : <br/><b><%=invoice.getAttribute("ben_country")%></b> </p>
				</td>
			</tr>
			

</table>
</div>
</div>
   
 <div class="columnLeft33NoBorder">
<div class="formItem">
<table class="poContent" style="width:110%;" cellspacing="2">
			<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("InvoiceUploadRequest.ben_email_addr") %> : <br/><b><%=invoice.getAttribute("ben_email_addr")%></b> </p>
				</td>
		
		</tr>
			<tr>
				
				<td >
				<p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankName") %> :       <br/><b><%=invoice.getAttribute("ben_bank_name")%></b>  </p>
				</td>
			</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankBranchCode") %> : <br/><b><%=invoice.getAttribute("ben_branch_code")%></b> </p>
				</td>
		
		</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("InvoiceUploadRequest.ben_bank_sort_code") %> : <br/><b><%=invoice.getAttribute("ben_bank_sort_code")%></b> </p>
				</td>
		
		</tr>		
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankBranchAddr1") %> : <br/><b><%=invoice.getAttribute("ben_branch_add1")%></b> </p>
				</td>
		</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankBranchAddr2") %> : <br/><b><%=invoice.getAttribute("ben_branch_add2")%></b> </p>
				</td>
		</tr>
				
		
</table>
</div>
</div>

 <div class="columnLeft33NoBorder">
<div class="formItem">
<table class="poContent" style="width:110%;" cellspacing="2">
			 
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankCity") %> : <br/><b><%=invoice.getAttribute("ben_bank_city")%></b> </p>
				</td>
		
		</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankProvinceCountry") %> : 
				<br/><b>
					<%=StringFunction.isBlank(invoice.getAttribute("ben_bank_province")) ? "" :invoice.getAttribute("ben_bank_province")%></b>  <b><%=invoice.getAttribute("ben_branch_country")%></b></p>
				</td>
		
		</tr>
		
		<tr>			
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.Charges") %> : <br/><b><%=StringFunction.isBlank(invoice.getAttribute("charges")) ?"" : invoice.getAttribute("charges")%></b> </p>
				</td>
			</tr>

		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.CentralReporting1") %> : <br/><b><%=invoice.getAttribute("central_bank_rep1")%></b> </p>
				</td>
		</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.CentralReporting2") %> : <br/><b><%=invoice.getAttribute("central_bank_rep2")%></b> </p>
				</td>
		</tr>
		<tr>
				<td >
					<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.CentralReporting3") %> : <br/><b><%=invoice.getAttribute("central_bank_rep3")%></b> </p>
				</td>
		</tr>
		<%---RPasupulati CR 1001 Start --%>
	<tr>
		<td >
		<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.reportingCode1") %> : <br/><b><%=reportingCode1%></b> </p>
		</td>
	</tr>
	<tr>
		<td >
		<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.reportingCode2") %> : <br/><b><%=reportingCode2%></b> </p>
		</td>
	</tr>
	<%---Rpasupulati CR 1001 End --%>
</table>

</div>
</div>

</div>
<% } %> 
   <div style="clear: both"></div>  

<%-- Srinivasu_D CR#1006 Rel9.3 05/06/2015  End  --%>

<%-- 914A start--%>
<div style="clear: both"></div>
<div class="formContentNoBorder">
<%=widgetFactory.createWideSubsectionHeader("UploadInvoices.CreditNoteAppliedDetails") %>
   
<div class="columnLeftNoBorder">
<div class="formItem">
<table class="poContent">
   <tr>
		<td >
		<p><%=widgetFactory.createSubLabel("UploadInvoices.CreditNoteId") %>
		
		</td>
		<td >
		
		<p><%=widgetFactory.createSubLabel("UploadInvoices.AppliedAmt") %>
		
		</td>
	</tr>


<%   String linkToInstr = invoice.getAttribute("link_to_instrument_type");
     if ( StringFunction.isNotBlank(linkToInstr))  {
		 linkToInstr = linkToInstr.trim();
	 }
     if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(linkToInstr)){
     StringBuilder creditNoteSQL = new StringBuilder();
     creditNoteSQL.append("SELECT distinct c.invoice_id, r.credit_note_applied_amount FROM invoices_credit_notes_relation r,credit_notes c")
	.append(" WHERE r.a_upload_credit_note_oid=c.upload_credit_note_oid AND  r.a_upload_invoice_oid = ? ");
     DocumentHandler appliedCreditNoteDoc = DatabaseQueryBean.getXmlResultSet(creditNoteSQL.toString(), false, new Object[]{oid});    
     if( appliedCreditNoteDoc != null ) {
     List<DocumentHandler> creditNoteList = appliedCreditNoteDoc.getFragmentsList("/ResultSetRecord");        
     for( DocumentHandler creditDoc : creditNoteList) {	      
     %>
	<tr>
		<td >
		<b><%=StringFunction.xssCharsToHtml(creditDoc.getAttribute("/INVOICE_ID"))%></b>
		
		</td>
		<td >
		<b><%=currency%></b>&nbsp;
		<%String creditNoteAppAmt = StringFunction.xssCharsToHtml(creditDoc.getAttribute("/CREDIT_NOTE_APPLIED_AMOUNT"));%>
		<b><%=TPCurrencyUtility.getDisplayAmount(creditNoteAppAmt, useCurrency, loginLocale)%></b>
		</td>
	</tr>
   <% } 
 }} %>
</table>
</div>
</div>

</div>
<%--CR 914A end--%>
 
   <div style="clear: both"></div>  
 <%
  DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  String LogGridHtml = dgFactory.createDataGrid("invoiceHistoryLogGrid","InvoiceHistoryLogDataGrid",null);
  String logGridLayout = dgFactory.createGridLayout("InvoiceHistoryLogDataGrid");
  %>
   
<div class="formItem">
	<div class="GridHeader" >
	<span class="invGridHeaderTitle" style="border-top:1px solid #DBDBDB;">
	<%=resMgr.getText("UploadInvoiceSummary.TransactionLog",TradePortalConstants.TEXT_BUNDLE)%>
      </span>
	</div>
	<div style="clear: both"></div>
		 <%=LogGridHtml %>
		 </div>
	</div>
</form>
</div>


<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script type="text/javascript">
require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
  <%--get grid layout from the data grid factory--%>
  var logGridLayout = <%= logGridLayout %>;
  <%--set the initial search parms--%>

  <%-- var init='upload_invoice_oid=<%=StringFunction.xssCharsToHtml(oid)%>';--%>
  <%--MEer IR-36419 Corrected the view name for payables invoice history --%>
   var init='upload_invoice_oid=<%=EncryptDecrypt.encryptStringUsingTripleDes(oid, userSession.getSecretKey())%>&invoice_id=<%=EncryptDecrypt.encryptStringUsingTripleDes(invoice.getAttribute("invoice_reference_id"), userSession.getSecretKey())%>';
   var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PayInvoiceHistoryLogDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>

  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var invoiceHistoryLogGrid = onDemandGrid.createOnDemandGrid("invoiceHistoryLogGrid", viewName, logGridLayout, init,1);
});
  function panelLevelAliasesFormatter(columnValues){
	  var panelLevel = columnValues[0];
	  var panelAlias="";
	  if(panelLevel != ""){
		  panelAlias = panelStore.get(panelLevel).name;
	  }
	  return panelAlias;
	  
  }

</script>
  </div> 
  <jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="invoiceHistoryLogGrid" />
</jsp:include>
</body>

<%
  // Finally, reset the cached document to eliminate carryover of 
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>

</html>
