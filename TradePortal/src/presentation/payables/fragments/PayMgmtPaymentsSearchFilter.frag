<%--
*******************************************************************************
                  Instrument Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
  
 
  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.


*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<div id="PaySearch">
<div class="searchDetail">
    <span class="searchCriteria">
    <%=widgetFactory.createSearchDateField("PaymentDateFrom","CashMgmtTransactionSearch.PayDateFrom","class='char8' onKeydown='filterPayTranOnEnter(window.event, \"PaymentDateFrom\", \"Pay\");'","constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'") %>
    <%=widgetFactory.createSearchDateField("PaymentDateTo","InvoiceSearch.To","class='char8' onKeydown='filterPayTranOnEnter(window.event, \"PaymentDateTo\", \"Pay\");'","constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'") %>
    <% 
    options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
    out.println(widgetFactory.createSearchSelectField("PayCurrency", "InstSearch.Currency", " ", options,"class='char22em'  onKeydown='filterPayTranOnEnter(window.event, \"PayCurrency\", \"Pay\");'"));
    %>
    </span>
    
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchPayInquiries">
      <%= resMgr.getText("PartySearch.Search", TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchPayments();</script>
      </button>
    </span>
    <div style="clear:both"></div>
    <span class="searchCriteria">
    <%=widgetFactory.createSearchAmountField("PayAmountFrom", "InstSearch.From","","maxlength = '18' class='char7em' onKeydown='filterPayTranOnEnter(window.event, \"PayAmountFrom\", \"Pay\");'", "") %>
    <%=widgetFactory.createSearchAmountField("PayAmountTo", "InstSearch.To", "","maxlength = '18' class='char7em' onKeydown='filterPayTranOnEnter(window.event, \"PayAmountTo\", \"Pay\");'", "") %>
    </span>
    
    <div style="clear:both"></div>
  </div>
</div>
<%
  gridHtml = dgFactory.createDataGrid("payPaymentsGrid","PayablesMgmtPaymentsDataGrid", null);
  gridLayout = dgFactory.createGridLayout("payPaymentsGrid", "PayablesMgmtPaymentsDataGrid");
  initSearchParms="selectedStatus="+selectedStatus+"&userOrgOid="+userOrgOid;
%>
<%=gridHtml%>
 
<%=widgetFactory.createHoverHelp("searchPayInquiries", "SearchHoverText") %>