<script>

  var formatGridLinkChild=function(columnValues, idx, level){

    var gridLink="";
            
    if(level == 0){
      gridLink = "<a href='" + columnValues[1] + "&fromPayables=Y" + "'>" + columnValues[0] + "</a>";
      return gridLink;
      
    }else if(level ==1){
      if ( columnValues[4] == 'INV' ) {
        gridLink = "<a href='" + columnValues[2] + "&fromPayables=Y" + "'>" + columnValues[0] + "</a>";
      }
      else if ( columnValues[4] == 'INVSum' ) {
        gridLink = "<a href='" + columnValues[3] +"&fromPayables=Y" + "'>" + columnValues[0] + "</a>";
      }
      return gridLink;
    }
  }
 
<%
if (TradePortalConstants.NAV__PAY_CREDIT_NOTES.equals(current2ndNav)) {
%>

	function initializeCreditNotesDataGrid(DataGridId,DataView){
	  require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"],
	  function(dom, registry, t360grid ) {
	
		var creditNoteID = savedSearchQueryData["creditNoteID"]; 
	    var tradingPartner = savedSearchQueryData["tradingPartner"]; 
	    var savedSort = savedSearchQueryData["sort"];  
	    var amountFrom = savedSearchQueryData["amountFrom"];
	    var amountTo = savedSearchQueryData["amountTo"];
	    var amountType = savedSearchQueryData["amountType"];
	    var currency = savedSearchQueryData["currency"];
	    var statusType = savedSearchQueryData["statusType"];
	    var status = savedSearchQueryData["status"];
	    var endToEndID = savedSearchQueryData["endToEndID"];
	    
		  if("" == savedSort || null == savedSort || "undefined" == savedSort){
		      savedSort = '0';
		   }
		  
		  if("" == creditNoteID || null == creditNoteID || "undefined" == creditNoteID)
	          creditNoteID=dom.byId("CreditNoteID").value.toUpperCase();
	           else
	              dom.byId("CreditNoteID").value = creditNoteID;
	              
	      if("" == tradingPartner || null == tradingPartner || "undefined" == tradingPartner)
	          tradingPartner=dom.byId("TradingPartner").value.toUpperCase();
	           else
	              dom.byId("TradingPartner").value = tradingPartner;
	              
	      if("" == statusType || null == statusType || "undefined" == statusType)
	          statusType=dom.byId("CreditStatusType").value;
	           else
	               registry.byId("CreditStatusType").set('value',statusType);
	           
		  if("" == status || null == status || "undefined" == status)
	          status=dom.byId("CreditStatus").value;
	           else
	               registry.byId("CreditStatus").set('value',status);
	               
	      if("" == currency || null == currency || "undefined" == currency)
	          currency=dom.byId("CreditCurrency").value;
	           else
	               registry.byId("CreditCurrency").set('value',currency);
	               
	     if("" == amountType || null == amountType || "undefined" == amountType)
	          amountType=dom.byId("CreditAmountType").value;
	           else
	               registry.byId("CreditAmountType").set('value',amountType);   
	                      
	     if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
	          amountFrom=dom.byId("CreditAmountFrom").value.toUpperCase();
	           else
	              dom.byId("CreditAmountFrom").value = amountFrom;
	              
	     if("" == amountTo || null == amountTo || "undefined" == amountTo)
	          amountTo=dom.byId("CreditAmountTo").value.toUpperCase();
	           else
	              dom.byId("CreditAmountTo").value = amountTo;
	                            
	      if("" == endToEndID || null == endToEndID || "undefined" == endToEndID)
	          endToEndID=dom.byId("EndToEndID").value.toUpperCase();
	           else
	              dom.byId("EndToEndID").value = endToEndID;
	    
	    <%--set the initial search parms--%>
	    initSearchParms = initSearchParms+'&creditNoteID='+creditNoteID+'&tradingPartner='+tradingPartner+'&statusType='+statusType+'&status='+status;
	    initSearchParms = initSearchParms+"&currency="+currency+"&amountType="+amountType+"&amountFrom="+amountFrom+"&amountTo="+amountTo;
	    initSearchParms = initSearchParms+"&endToEndID="+endToEndID+"&userOrgOid="+<%=userOrgOid%>;
	    <%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>

	  
	    //SSikhakolli - Rel-9.2 - CR-914A - Moving Grid layout to PayablesMgmtCreditNoteDataGrid.xml for Grid Customization.
	    //var creditNotesGrid = t360grid.createLazyTreeDataGrid(DataGridId,DataView,creditNotesGridLayout,initSearchParms,savedSort);
	    var creditNotesGrid = t360grid.createLazyTreeDataGrid(DataGridId,DataView,<%=gridLayout%>,initSearchParms,savedSort);
	    creditNotesGrid.set('autoHeight',20);
	  });
	}

<%	
}
%>



function setStatusOptions() {
      require(["dijit/registry", "dojo/dom", "dojo/dom-style"],
    		    function(registry, dom, domStyle) {
       		    	    var selectedValue = registry.byId("CreditStatusType").get('value'); 
       		    	    var statusStore = null;       		    		
    		         	if(selectedValue!= null){
    		         		if(selectedValue == '<%=TradePortalConstants.CREDIT_NOTE_UTILIZED_STATUS_TYPE%>'){
	    		         		statusStore = new dojo.store.Memory({
		       		              <%=utilisedStatusType%>
		       		             });
		       		        }else if(selectedValue == '<%=TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_TYPE%>'){
		       		        	statusStore = new dojo.store.Memory({
		       		              <%=appliedStatusType%>
		       		             });
		       		        }
		       		        var status = dijit.byId("CreditStatus");
		       				status.attr('value','');
		       				status.store = statusStore;	 
    		         	}else{
    		         	var status = dijit.byId("CreditStatus");
		       				status.attr('value','');
    		         		status.set('disabled',true);
    		         	}
    		         	
    		         	
    		  });
    		  
  }
  
  function searchCreditNotes() {
	    require(["dijit/registry","dojo/dom", "t360/datagrid","dojo/ready", "dojo/domReady!"],
	      function(registry,dom, t360grid, ready){

	    	var creditNoteID=(dom.byId("CreditNoteID").value).toUpperCase();
	    	var tradingPartner=(dom.byId("TradingPartner").value).toUpperCase();
	    	
	    	var currency=(registry.byId("CreditCurrency").value).toUpperCase();
	    	var amountType=(registry.byId("CreditAmountType").value).toUpperCase();
	    	var amountFrom=(dom.byId("CreditAmountFrom").value).toUpperCase();
	    	var amountTo=(dom.byId("CreditAmountTo").value).toUpperCase();

			var statusType=(registry.byId("CreditStatusType").value).toUpperCase();
			var status=(registry.byId("CreditStatus").value).toUpperCase();
			
			var endToEndID=(dom.byId("EndToEndID").value).toUpperCase();
	    	
	        var searchParms = "userOrgOid="+<%=userOrgOid%>+"&creditNoteID="+creditNoteID+"&tradingPartner="+
	        tradingPartner+"&currency="+currency+"&amountType="+amountType+"&amountFrom="+amountFrom+"&amountTo="+amountTo+"&statusType="+
	        statusType+"&status="+status+"&endToEndID="+endToEndID;
	        console.log("searchParms-"+searchParms);
			
	        var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PayablesMgmtCreditNoteDataView",userSession.getSecretKey())%>"; 
    		
	      //SSikhakolli - Rel-9.2 - CR-914A - Moving Grid layout to PayablesMgmtCreditNoteDataGrid.xml for Grid Customization.
	        //t360grid.searchLazyTreeDataGrid("creditNotesGrid", viewName,creditNotesGridLayout, searchParms);
	        var newGridLayout = registry.byId('creditNotesGrid').get('structure');

    		t360grid.searchLazyTreeDataGrid("creditNotesGrid", viewName, newGridLayout, searchParms);
    		
    		ready(function() {
    			 //SSikhakolli - Rel-9.2 CR-914A 02/17/2015 - Addinng below code to protect child rows from mouse selection.
	          	var myGrid = registry.byId("creditNotesGrid");
	          myGrid.onCanSelect=function(idx){
					var selectedItem = this.getItem(idx).i;
					var isChild = (selectedItem.CHILDREN != "true");
				   	return (!isChild);
				};
    		});
	 	  });
	  }
</script>