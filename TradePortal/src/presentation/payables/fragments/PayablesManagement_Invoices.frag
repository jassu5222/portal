<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Payables Invoices Tab

  Description:
    Contains HTML to create the History tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:

*******************************************************************************
--%>

<%
      secureParms.put("UserOid",        userSession.getUserOid());
      secureParms.put("OrgOid",        userSession.getOwnerOrgOid());
      if (userSession.getSavedUserSession() == null) {
	        secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_YES);
	  }else{
		  secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_NO);
	  }

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);

      String newSearchTrans = request.getParameter("NewSearch");
      loginLocale = userSession.getUserLocale();

   String optionsAmountType = null;
   String optionsDateType = null;
   String invoiceID = null;
 
%>

<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">
      <%= resMgr.getText("TransactionsMenu.Payables.Invoices",TradePortalConstants.TEXT_BUNDLE) %>

    </span>
    <span class="searchHeaderActions">
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="invoicesGrid" />
      </jsp:include>
 	  <span id="invoicesRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("invoicesRefresh", "RefreshImageLinkHoverText") %>
      <span id="invoicesGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("invoicesGridEdit","CustomizeListImageLinkHoverText") %>  
     </span>
    <div style="clear:both;"></div>
  </div>

<%
  gridHtml = dgFactory.createDataGridMultiFooter("invoicesGrid","PayablesMgmtInvoicesDataGrid", "5");
  gridLayout = dgFactory.createGridLayout("invoicesGrid", "PayablesMgmtInvoicesDataGrid");
   initSearchParms="userOrgOid="+userOrgOid;
%>

<div class="searchDivider"></div>

<%@ include file="/payables/fragments/PayInvoiceSearchFilter.frag" %>
</div>
<input type=hidden name=SearchType value="Y">

<%=gridHtml%>

