<%--
**********************************************************************************
  Direct Debit Transactions Home

  Description:  
     This page is used as the main Direct Debits page.

**********************************************************************************
--%> 

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
 
<%
  StringBuffer      onLoad             = new StringBuffer();
  Hashtable         secureParms        = new Hashtable();
  String            helpSensitiveLink  = null;
  String            userSecurityRights = null;
  String            userSecurityType   = null;
  String            current2ndNav      = null;
  String            userOrgOid         = null;
  String            userOid            = null;
  String            formName           = "";
  StringBuffer      newLink            = new StringBuffer();
  StringBuffer orgList = new StringBuffer();
  boolean           canViewDirectDebitInstruments = false;  // NSX CR-509 12/07/2009 
  WidgetFactory widgetFactory = new WidgetFactory (resMgr);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
   
   boolean orgListDropDown = false;

   //rbhaduri - CR-374 - 14th Oct 08
   String 	     loginLocale 		  = null;
   
   userSession.setCurrentPrimaryNavigation("NavigationBar.DirectDebitTransaction");
                                        
   //cquinton 1/18/2013 set return page
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToDirectDebitTransactionsHome");
   }

   //IR T36000026794 Rel9.0 07/24/2014 starts
   session.setAttribute("startHomePage", "DirectDebitTransactionsHome");
   session.removeAttribute("fromTPHomePage");
   //IR T36000026794 Rel9.0 07/24/2014 End
  
   StringBuffer newSearchReceivableCriteria = new StringBuffer();//PPramey - CR-434 - 22th Nov 08                                         

   //cquinton 1/18/2013 remove old close action behavior
   Map searchQueryMap =  userSession.getSavedSearchQueryData();
   Map savedSearchQueryData =null;
  
   userSecurityRights           = userSession.getSecurityRights();
   userSecurityType             = userSession.getSecurityType();
   userOrgOid                   = userSession.getOwnerOrgOid();
   userOid                      = userSession.getUserOid();

   CorporateOrganizationWebBean org = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   org.getById(userOrgOid);
   String  acctOid ="";
     
   canViewDirectDebitInstruments = SecurityAccess.hasRights(userSecurityRights,  SecurityAccess.ACCESS_DIRECTDEBIT_AREA); // NSX CR-509 12/07/2009 
   
   current2ndNav = request.getParameter("current2ndNav");
   if (current2ndNav == null) {
      current2ndNav = (String) session.getAttribute("directDebitTransactions2ndNav");

      if (current2ndNav == null) {
         current2ndNav = TradePortalConstants.NAV__PENDING_TRANSACTIONS;
      }
   }
   
   // PKUK012160488 End
   
   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current tab as its context
   PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(current2ndNav);

   if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav)||
		   TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) {
	   formName     = "DirectDebit-TransactionForm";
	   %>
       <%@ include file="/receivables/fragments/DirectDebit-TranSearchErrorParms.frag" %>
 <%          
   }
   if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav)){
	   helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/default_no_help_available.htm",  resMgr, userSession);
   }else if(TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)){
	   helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/direct_debit_history.htm",  resMgr, userSession);
   }
  // NSX CR-509 12/07/2009 ADD END
   String searchNav = "dbTranHome."+current2ndNav;
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>

<%-- Vshah - 08/02/2012 - Rel8.1 - Portal-refresh --%>
<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

<div class="pageMain">
<div class="pageContent">

<div class="secondaryNav">

  <div class="secondaryNavTitle">
    <%=resMgr.getText("SecondaryNavigation.DirectDebit",
                      TradePortalConstants.TEXT_BUNDLE)%>
  </div>

  <%
     String pendingSelected   = TradePortalConstants.INDICATOR_NO;
     String inquiriesSelected = TradePortalConstants.INDICATOR_NO;
     if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav)) {
       pendingSelected = TradePortalConstants.INDICATOR_YES;
     }
     else {
       inquiriesSelected = TradePortalConstants.INDICATOR_YES;
     }

     String navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__PENDING_TRANSACTIONS;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.DirectDebit.PendingTransactions" />
    <jsp:param name="linkSelected" value="<%= pendingSelected %>" />
    <jsp:param name="action"       value="goToDirectDebitTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
	<jsp:param name="hoverText"    value="DirectDebit.pendingList" />
  </jsp:include>
  <%
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__INQUIRIES;
  
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.DirectDebit.Inquiries" />
    <jsp:param name="linkSelected" value="<%= inquiriesSelected %>" />
    <jsp:param name="action"       value="goToDirectDebitTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
	<jsp:param name="hoverText"    value="DirectDebit.inquiryList" />
  </jsp:include>
  
 
  <span id="secondaryNavHelp" class="secondaryNavHelp">
  	<%=helpSensitiveLink%>
  </span>
  <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %> 
  <div style="clear:both;"></div>

</div>


<% //cr498 reauthenticate if necessary
    String certAuthURL = ""; //needed for openReauth frag
    Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
    String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
    boolean matchRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_MATCH_RESPONSE);
    boolean approveDiscountRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_APPROVE_DISCOUNT);
    boolean closeInvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_CLOSE_INVOICE);
    boolean financeInvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_FIN_INVOICE);
    boolean disputeInvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_DISPUTE_INVOICE);
    boolean ddiRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__DDI_ISS);
    String authorizeLink = "";
    String approveDiscountLink = "";
    String closeInvoiceLink = "";
    String financeInvoiceLink = "";
    String disputeInvoiceLink = "";
    String ddiLink = "";
    if ( matchRequireAuth||approveDiscountRequireAuth||
         closeInvoiceRequireAuth||financeInvoiceRequireAuth||disputeInvoiceRequireAuth||
         ddiRequireAuth ) {
%>   
  <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
        authorizeLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_AUTHORIZE + "'," +
            "'PayRemit')";
        approveDiscountLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
	    "'" + formName + "','ApproveDiscountAuthorize'," +
            "'PayRemit')";
        closeInvoiceLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_CLOSE_SELECTED_ITEMS + "'," +
            "'Invoice')";
        financeInvoiceLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_FINANCE_SELECTED_ITEMS + "'," +
            "'Invoice')";
        disputeInvoiceLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_DISPUTEUNDISPUTE_SELECTEDITEMS + "'," +
            "'Invoice')";
	//IR T36000014931 REL ER 8.1.0.4 BEGIN    
        ddiLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_AUTHORIZE + "'," +
            "'Transaction','ddPendGrid')";
	//IR T36000014931 REL ER 8.1.0.4 END    
    }
    //cr498 end
%>

<form name="<%=formName%>" method="POST" 
      action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">
  <input type=hidden value="" name=buttonName>
<jsp:include page="/common/ErrorSection.jsp" />
 
  <div class="formContentNoSidebar">
<%  //cr498 add reauthentication hidden fields
    if ( matchRequireAuth||approveDiscountRequireAuth||
         closeInvoiceRequireAuth||financeInvoiceRequireAuth||disputeInvoiceRequireAuth || ddiRequireAuth) {
%> 

      <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
      <input type=hidden name="reCertOK">
      <input type=hidden name="logonResponse">
      <input type=hidden name="logonCertificate">

<%
    }
    //cr498 end
%>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   //cquinton 3/26/2013 Rel PR ir#15207 use constants rather than translated values
   final String ALL_ORGS = "AuthorizedTransactions.AllOrganizations";
   final String ALL_WORK = "PendingTransactions.AllWork";
   final String MY_WORK = "PendingTransactions.MyWork";

   String			 instrumentId 		   = "";
   String			 refNum			       = "";
   String			 amountFrom     	   = "";
   String			 amountTo              = "";
   String 			 currency              = "";
   String 			 dayFrom 			   = "";
   String 			 monthFrom 			   = "";
   String 			 yearFrom 			   = "";
   String 			 dayTo 				   = "";
   String 			 monthTo 			   = "";
   String 			 yearTo 			   = "";
   String			 searchType		       = null;
   String 			 creditAcct		       = null;
   Vector            instrumentTypes       = null;
   String 			 link 			       = null;
   String 			 linkText 		       = "";   
   
   DocumentHandler   hierarchyDoc                 = null;
   StringBuffer      dropdownOptions              = new StringBuffer();
   int               totalOrganizations           = 0;
   StringBuffer      extraTags                    = new StringBuffer();
            
   String            userDefaultWipView           = null; 
   String            selectedWorkflow             = null;
   String            selectedStatus               = "";
   String            selectedOrg                  = null;  
   String            linkParms                    = "";   
   DataGridFactory dgFac = new DataGridFactory(resMgr, userSession, formMgr, response);
   String gridHtml = "", gridLayout = "";
   userDefaultWipView = userSession.getDefaultWipView();
   loginLocale = userSession.getUserLocale();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";
    
   // Construct the Corporate Organization
   CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   
   // Store the user's oid and security rights in a secure hashtable for the form
   secureParms.put("instrumentType",InstrumentType.DIRECT_DEBIT_INSTRUCTION);
   secureParms.put("bankBranch",corpOrg.getAttribute("first_op_bank_org")); 
   
   secureParms.put("UserOid",userSession.getUserOid());
   secureParms.put("SecurityRights",userSecurityRights); 
   secureParms.put("clientBankOid",userSession.getClientBankOid()); 
   secureParms.put("ownerOrg",userSession.getOwnerOrgOid());

   
   // This is the query used for populating the workflow drop down list; it retrieves
   // all active child organizations that belong to the user's current org.
   StringBuilder sqlQuery = new StringBuilder();
   sqlQuery.append("select organization_oid, name");
   sqlQuery.append(" from corporate_org");
   sqlQuery.append(" where activation_status = ?");
   sqlQuery.append(" start with organization_oid = ?");
   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
   sqlQuery.append(" order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));
   

   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false,TradePortalConstants.ACTIVE,userOrgOid);
	  
   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
   totalOrganizations = orgListDoc.size();
   
   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName

   // Set the performance statistic object to have the current tab as its context
   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(current2ndNav);

   // ***********************************************************************
   // Data setup for the Pending Transaction tab
   // ***********************************************************************
   if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav))
   {
      // Based on the statusType dropdown, build the where clause to include one or more
      // statuses.
      Vector statuses = new Vector();
      int numStatuses = 0;

      selectedStatus = request.getParameter("transStatusType");
      if (selectedStatus == null) 
      {
         selectedStatus = (String) session.getAttribute("transStatusType");
      }

      if (TradePortalConstants.STATUS_READY.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE);
      }
      else if (TradePortalConstants.STATUS_PARTIAL.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED);
      }
      else if (TradePortalConstants.STATUS_AUTH_FAILED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED);
      }
      else if (TradePortalConstants.STATUS_STARTED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_STARTED);
      }
      else // default is ALL
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_STARTED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE);
         selectedStatus = TradePortalConstants.STATUS_ALL;
      }

      session.setAttribute("transStatusType", selectedStatus);

     

      // Based on the workflow dropdown, build the where clause to include transactions at
      // various levels of the organization.
      selectedWorkflow = request.getParameter("workflow");

      if (selectedWorkflow == null)
      {
         selectedWorkflow = (String) session.getAttribute("workflow");

         if (selectedWorkflow == null)
         {
            if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
            {
               selectedWorkflow = userOrgOid;
            }
            else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
            {
               selectedWorkflow = ALL_WORK;
            }
            else
            {
               selectedWorkflow = MY_WORK;
            }
            selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
         }
      }

      session.setAttribute("workflow", selectedWorkflow);

      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());

      //cquinton 3/26/2013 Rel PR ir#15207 remove workflow criteria here as it is unused

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/default_no_help_available.htm",  resMgr, userSession);
      onLoad.append("document.DirectDebit-TransactionForm.Workflow.focus();");
%>

<%-- **************** JavaScript for Pending tab only *********************  --%>

<script language="JavaScript" type="text/javascript">

      
         <%--
         // This function is used to display a popup message confirming
         // that the user wishes to delete the selected items in the
         // listview.
         --%>
         function confirmDelete()
         {
           var   confirmMessage = "<%=resMgr.getText("PendingTransactions.PopupMessage", 
                                                     TradePortalConstants.TEXT_BUNDLE) %>";

            if (!confirm(confirmMessage)) 
             {
                formSubmitted = false;
                return false;
             }
           else
           {
              return true;
           }
        }

  

      </script>

<%
   } // End data setup for Pending tab

   else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav))
   {
       helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/direct_debit_history.htm",  resMgr, userSession);
       
       // Determine the organization to select in the dropdown
       selectedOrg = request.getParameter("historyOrg");
       if (selectedOrg == null)
       {
       	  selectedOrg = (String) session.getAttribute("historyOrg");
       	  if (selectedOrg == null)
          {
            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
          }
       }

       session.setAttribute("historyOrg", selectedOrg);
       selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
       
   	  String newSearch = request.getParameter("NewSearch");
	  String newDropdownSearch = request.getParameter("NewDropdownSearch");
      Debug.debug("New Search is " + newSearch);
      Debug.debug("New Dropdown Search is " + newDropdownSearch);
   	  searchType   = request.getParameter("SearchType");
   		
   		if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
      	{
         	// Default search type for instrument status on the transactions history page is ACTIVE.
         	session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
     	}
   		
   		 if (searchType == null)
        {
          		  searchType = TradePortalConstants.SIMPLE;
        }
   		
        
         if (searchType.equals(TradePortalConstants.ADVANCED))
      	{
         	linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
         	link       = formMgr.getLinkAsUrl("goToReceivablesManagement", linkParms, response);
         	linkText   = resMgr.getText("InstSearch.BasicSearch", TradePortalConstants.TEXT_BUNDLE);

         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "advanced", resMgr, userSession);
      	}
      	else
      	{
        	linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
         	link       = formMgr.getLinkAsUrl("goToReceivablesManagement", linkParms, response);
         	linkText   = resMgr.getText("InstSearch.AdvancedSearch", TradePortalConstants.TEXT_BUNDLE);

         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "basic", resMgr, userSession);
     	 }

         //cquinton 3/26/2013 Rel PR ir#15207 remove where clause for orgs as unused here
     	 
     	 // Based on the statusType dropdown, build the where clause to include one or more
     	 // instrument statuses.
     	 Vector statuses = new Vector();
     	 int numStatuses = 0;

      	selectedStatus = request.getParameter("instrStatusType");
        if (selectedStatus == null) 
     	 {
       		  selectedStatus = (String) session.getAttribute("instrStatusType");
     	 }

      	if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) )
      	{
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
      	}

      	else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) )
      	{
       		 statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
         	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
      	}

      	else // default is ALL (actually not all since DELeted instruments
           // never show up (handled by the SQL in the listview XML)
      	{
        	 selectedStatus = TradePortalConstants.STATUS_ALL;
      	}

     	session.setAttribute("instrStatusType", selectedStatus);

%> 

<%-- JavaScript for Instrument History link to enable form submission by enter.
           event.which works for NetScape and event.keyCode works for IE  --%>
<script LANGUAGE="JavaScript" type="text/javascript">
    
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  
     
      </script>

<%      	
     }
     
   Debug.debug("form " + formName);
%>


<%
  // Based on the current tab, display the appropriate HTML for that tab
  if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav))
  {
	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	   savedSearchQueryData = (Map)searchQueryMap.get("DirectDebitPendingDataView");
	   
  }
%>
<%@ include file="/receivables/fragments/DirectDebit-PendingListView.frag"%>
<%
  }
  else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) 
  {
	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	   savedSearchQueryData = (Map)searchQueryMap.get("DirectDebitHistoryDataView");
	   
  }
%>
<%@ include file="/receivables/fragments/DirectDebit-HistoryListView.frag"%>
<%
  }
%>


<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
</div>
</form>

</div>
</div>

<%--cquinton 3/16/2012 Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton 3/16/2012 Portal Refresh - end--%>
<%@ include file="/common/GetSavedSearchQueryData.frag" %> 
<%
  // Based on the current tab, display the appropriate HTML for that tab
  if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav))
  {
%>
<%@ include file="/receivables/fragments/DirectDebit-PendingListViewFooter.frag"%>
<%
  }
  else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) 
  {
%>
<%@ include file="/receivables/fragments/DirectDebit-HistoryListViewFooter.frag"%>
<%
  }
%>






</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());

   session.setAttribute("directDebitTransactions2ndNav", current2ndNav);
%>
