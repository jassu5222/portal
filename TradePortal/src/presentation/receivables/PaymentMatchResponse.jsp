<%--
*******************************************************************************
                            Payment Match Response

  Description:  

  Note:         

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,java.text.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  DocumentHandler xmlDoc                        = null;  
  PayRemitWebBean payRemitWebBean               = null;
  List payMatchResultChildren                   = new ArrayList();
  StringBuffer receivablesMatchListTitle        = new StringBuffer();  
  StringBuffer sqlQuery                         = new StringBuffer(); 
  String link                                   = null;  
  String formName                               = "PaymentMatchResponseForm";
  String emptySearchFilter                      = " 1=0";
  String userSecurityRights                     = userSession.getSecurityRights();  
  String loginLocale                            = StringFunction.xssCharsToHtml(userSession.getUserLocale());
  Vector listviewVector                         = null; 
  String payRemitInvoiceOid						=	"";
  boolean multiplRemittancesExist				= false;
  boolean allowMultipleInvoices					= true;
  boolean unapplyRemainingBalance               = false; 
  String unapply = "";
  boolean showAddBuyerToPaymentButton           = false;
  QueryListView queryListView                   = null;  
  boolean isDiscounted 				= false; //Pramey IR ANUJ032044466  
  String buttonClicked = request.getParameter("buttonName");
  String buyerSearchAddressTitle			=	resMgr.getText("BuyerSearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);	
   String transactionType						="";
   boolean withoutRemitItemsInd = true; //IValavala
  boolean isVascoOTP = false; //IR#DNUM062245488 -  This will check is a user is set to use OTP Vasco type
  boolean noRemittanceItems = false; //Srinivasu_D CR#997 Rel9.3 - Added this flag to determine whether remittance checkbox to enable or not
  String payRemitInvOids = "";
  StringBuilder      sqlQueryStr   = new StringBuilder(); //Vshah - IR#DWUM043037276 
  userSession.setCurrentPrimaryNavigation("NavigationBar.ReceivablesManagement");
  //cquinton 5/24/2014 Rel 8.2 ir#15597 add page to page flow so close button works correctly
  //IR 20282 BEGIN
  //userSession.addPage("goToPaymentMatchResponse", request);
  if ( "true".equals( request.getParameter("returning") ) ) {
	    userSession.pageBack(); 
	  }
	  else {
		  userSession.addPage("goToPaymentMatchResponse", request);
	  }
  //IR 20282 END
	
  // Initialize the reference data.
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
  Hashtable invoiceFinanceStatus = refData.getAllCodeAndDescr("INVOICE_FINANCE_STATUS",loginLocale);
    
  String payRemitOid = null;
  DocumentHandler doc = formMgr.getFromDocCache();
  //Srinivasu_D CR#997 Rel9.3 04/04/2015 - Added for determing whether any server side error occured
  Vector error = null;
   String hasError = "N";
  error = doc.getFragments("/Error/errorlist/error");
  //System.out.println("error:"+error);
  String orgID = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
  //Rel8.2 IR T36000014337 - 04/04/2013 - Check for ERP Setting
	CorporateOrganizationWebBean corpoOrg = null;
	String erpSettings = "";	
	beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
	corpoOrg = (CorporateOrganizationWebBean) beanMgr.getBean("CorporateOrganization"); 
	corpoOrg.getById(orgID);
	erpSettings = corpoOrg.getAttribute("erp_transaction_rpt_reqd");

  //System.out.println("erpSetting;"+erpSettings);
  String errRemOid = "";
  session.setAttribute("showErrorDialog","N");
  if(TradePortalConstants.INDICATOR_YES.equals(erpSettings)){
  errRemOid = doc.getAttribute("/In/Misc/remitOid");
  if(StringFunction.isNotBlank(errRemOid)){
	  errRemOid = EncryptDecrypt.encryptStringUsingTripleDes(errRemOid, userSession.getSecretKey());
  }
  String errorIndicator = doc.getAttribute("/Out/errorInd");
	if(TradePortalConstants.INDICATOR_YES.equals(errorIndicator)){
			hasError = "Y";
			session.setAttribute("showErrorDialog","Y");
			session.setAttribute("errorDoc",error);
	}
  }
  String showPopup ="N";
  String autoPayOid = doc.getAttribute("/Out/payInvOid");
  String autoInvoiceID = doc.getAttribute("/Out/payInvoiceID");
  
  if(StringFunction.isNotBlank(autoPayOid)){
		showPopup = "Y";
  }
  String payRemInvoiceOid = "";
  String autoButtonName = doc.getAttribute("/In/AutoPayment/autoMatchButton"); 
  if(StringFunction.isNotBlank(autoButtonName) && TradePortalConstants.AUTO_PAY_SEARCH.equals(autoButtonName)){
	  Vector <DocumentHandler> newMatchItems = doc.getFragments("/In/NewMatchItem");	
	   for (DocumentHandler currDoc: newMatchItems) {
		  		  payRemInvoiceOid = currDoc.getAttribute("/payRemitInvoiceOid");
	   }
  }
  Debug.debug("payRemInvoiceOid:"+payRemInvoiceOid);
  //Srinivasu_D CR#997 Rel9.3 04/04/2015 - End
  //CR 742 related - start
  //Variables used to build the list of Document Images 
  String            current2ndNav      = null;
  String 			       currency				= null;
  int 			       numofPayMatchResult;
  Vector 			       paymatchlistviewVector;
 String payremitOid=null; 
  DocumentHandler dbQuerydoc = null;
  StringBuffer query = null;
  String attribute;
  DocumentHandler myDoc;
  String linkArgs[] = {"","","",""};
  String displayText;
  String encryptVal1;
  String encryptVal2;
  String encryptVal3;
  String customerOID;
  boolean showPDFLinks = false;
  String			       selectClause			= null;
  //CR742 - end
  Debug.debug("The input document is " + doc);
  //out.println("doc="+doc);
  
  if (doc.getDocumentNode("/In/PayRemit") == null ) {
    // Get the pay_remit_oid from the request.
    payRemitOid = StringFunction.xssCharsToHtml(request.getParameter("pay_remit_oid"));
    if (payRemitOid != null) {
      payRemitOid = EncryptDecrypt.decryptStringUsingTripleDes(payRemitOid, userSession.getSecretKey());
    } else{
      payRemitOid = (String)session.getAttribute("SessionPayRemitOid");
    }
  }else{
    payRemitOid = doc.getAttribute("/In/PayRemit/payRemitOid");
  }
  //out.println("pay_remit_oid ="+payRemitOid);
  session.setAttribute("SessionPayRemitOid", payRemitOid);
  session.setAttribute("PAYMENTREMITDETAIL","goToPaymentMatchResponse");
  
  
  session.setAttribute("pay_remit_oid", payRemitOid);
  session.setAttribute("fromPaymentMatchResponse", "true");
  Debug.debug("####################################################");
  Debug.debug("####################################################");
  Debug.debug("####################################################");
  Debug.debug("The pay remit oid is " + payRemitOid);
  payRemitWebBean = beanMgr.createBean(PayRemitWebBean.class, "PayRemit");
  payRemitWebBean.getById(StringFunction.xssCharsToHtml(payRemitOid));

  Hashtable secureParms = new Hashtable();  
  secureParms.put("UserOid",userSession.getUserOid());
  secureParms.put("SecurityRights",userSecurityRights); 
  secureParms.put("pay_remit_oid",payRemitOid);

  String receiptDatetime = payRemitWebBean.getAttribute("receipt_datetime");
  /* String trimedValuDate=TPDateTimeUtility.formatDate(valueDate, TPDateTimeUtility.LONG, loginLocale); */
  String reference_id = StringFunction.xssCharsToHtml(payRemitWebBean.getAttribute("payment_invoice_reference_id"));
  String status = refData.getDescr(TradePortalConstants.TRANSACTION_STATUS,payRemitWebBean.getAttribute("transaction_status"),loginLocale) ;
  String rowKey = payRemitOid+"/"+reference_id;

  if(receiptDatetime == null || receiptDatetime.equals("")){
      receiptDatetime = "";
  } else {
      receiptDatetime = TPDateTimeUtility.convertGMTDateTimeForTimezoneAndFormat(receiptDatetime, 
                                                                                  TPDateTimeUtility.SHORT, 
                                                                                  resMgr.getResourceLocale(), 
                                                                                  userSession.getTimeZone());
  }
  
  userSession.getUserLocale();
  
  
  
  receivablesMatchListTitle.append(resMgr.getText("PaymentMatchResponse.ReceivablesMatchListTitle", TradePortalConstants.TEXT_BUNDLE));
  receivablesMatchListTitle.append(" - ");
  receivablesMatchListTitle.append(reference_id);
  receivablesMatchListTitle.append(" - ");
  receivablesMatchListTitle.append(receiptDatetime);
  
  isDiscounted = payRemitWebBean.getAttribute("discount_indicator").equals(TradePortalConstants.INDICATOR_YES);
  // Determine if the Add buyer button should be shown.
  String buyerId = payRemitWebBean.getAttribute("seller_id_for_buyer") == null ? "" : StringFunction.xssCharsToHtml(payRemitWebBean.getAttribute("seller_id_for_buyer"));  
  String buyerName = StringFunction.xssCharsToHtml(payRemitWebBean.getAttribute("seller_name_for_buyer"));
  if("".equals(buyerId)){
    if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_ADD_BUYER_TO_PAYMENT)){
      showAddBuyerToPaymentButton = true;
      // Determine the buyer information if necessary - this would come from the add buyer page.
      if(doc.getAttribute("/In/BuyerSearchInfo/BuyerIdAndName") != null){
        String buyerIdAndName = doc.getAttribute("/In/BuyerSearchInfo/BuyerIdAndName"); 
        buyerId = buyerIdAndName.substring(0,buyerIdAndName.indexOf("/"));
        buyerName = buyerIdAndName.substring(buyerIdAndName.indexOf("/")+1);
        // find the buyerName based on the buyerId
        
      }
    }
  }


  // Initialize query to get child pay_match_result object
  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
  sqlQuery = new StringBuffer();
  sqlQuery.append("select pay_match_result_oid, invoice.invoice_outstanding_amount");
  sqlQuery.append("  from  pay_match_result, invoice");
  sqlQuery.append("  where pay_match_result.p_pay_remit_oid = ?");
  sqlQuery.append("  and pay_match_result.a_invoice_oid = invoice.invoice_oid(+)");

  
  Debug.debug("The sqlquery is "  + sqlQuery.toString());
  queryListView.setSQL(sqlQuery.toString(),new Object[]{payRemitOid});
  queryListView.getRecords();
  
  xmlDoc = queryListView.getXmlResultSet();
  listviewVector = xmlDoc.getFragments("/ResultSetRecord");
  
  for(int i = 0; i < listviewVector.size(); i++){
    DocumentHandler resultRow = (DocumentHandler)listviewVector.elementAt(i);
    PayMatchResultWebBean obj = beanMgr.createBean(PayMatchResultWebBean.class, "PayMatchResult");
    String oid = resultRow.getAttribute("/PAY_MATCH_RESULT_OID");
    obj.getById(oid);
    obj.setAttribute("invoice_outstanding_amount", resultRow.getAttribute("/INVOICE_OUTSTANDING_AMOUNT"));
    payMatchResultChildren.add(obj);
  }
  
  //out.println("payMatchResultChildren size="+payMatchResultChildren.size());
  // Get value for Unapply Remaining Balance
  if( payRemitWebBean.getAttribute("unapplied_payment_amount") != null ){
    String temp = payRemitWebBean.getAttribute("unapplied_payment_amount");
    if(temp != null && !"".equals(temp)){
      BigDecimal value = new BigDecimal(temp);
      if(value.compareTo(BigDecimal.valueOf(0)) > 0){
        unapplyRemainingBalance = true;
      }
    }
  } 
  
  if (unapplyRemainingBalance) {
	  unapply = "Y";
  } else {
	  unapply = "N";
  }
  
  
  // format the payment amount
  String baseCurrencyCode = StringFunction.xssCharsToHtml(payRemitWebBean.getAttribute("currency_code"));
  
  String paymentAmount = payRemitWebBean.getAttribute("payment_amount");
  StringBuffer formattedPaymentAmount = new StringBuffer();
  formattedPaymentAmount.append(baseCurrencyCode);
  formattedPaymentAmount.append(" ");
  formattedPaymentAmount.append(TPCurrencyUtility.getDisplayAmount(paymentAmount, baseCurrencyCode, loginLocale));
                                                              
                                                              
  StringBuffer formattedTotalInvoiceAmount = new StringBuffer();
  formattedTotalInvoiceAmount.append(baseCurrencyCode);
  formattedTotalInvoiceAmount.append(" ");
  formattedTotalInvoiceAmount.append(TPCurrencyUtility.getDisplayAmount(payRemitWebBean.getAttribute("total_invoice_amount"), 
                                                              baseCurrencyCode, loginLocale));
  

  // Must initialize formatting of currency values for javascript formatting.
  String decimalPlaces = (String)ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, baseCurrencyCode);
  
  // if number of decimal places do not exist, set the default as 2
  if (decimalPlaces == null){
    decimalPlaces = "2";
  }
  int decimalCount = Integer.parseInt(decimalPlaces);
  DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(new Locale(loginLocale.substring(0,2),loginLocale.substring(3,5)));

  //cr 742 - begin
  payremitOid = payRemitOid;
    //cr 742. IValavala getting A_SELLER_CORP_ORG_OID.
	selectClause  = "select P.PAYMENT_INVOICE_REFERENCE_ID, P.A_SELLER_CORP_ORG_OID, P.SELLER_ID_FOR_BUYER, P.SELLER_NAME_FOR_BUYER, P.BUYER_NAME, P.BUYER_CUSTOMER_ID, P.PAYMENT_SOURCE_TYPE, P.PAYMENT_VALUE_DATE, P.TRACER_LINE, P.PAYMENT_AMOUNT, P.BUYER_REFERENCE, P.NARRATIVE, P.CHEQUE_REFERENCE, P.SELLER_REFERENCE, P.REMIT_SOURCE_TYPE, P.TOTAL_INVOICE_AMOUNT, P.TOTAL_NUMBER_OF_INVOICES, P.SELLER_NAME, P.SELLER_CONTACT_NAME, P.SELLER_BANK_DETAILS, P.SELLER_ADDRESS, P.BUYER_CONTACT_NAME, P.BUYER_INVOICE_REFERENCE_ID, P.BUYER_ADDRESS, /*P.MATCHED_PAYMENT_STATUS,*/ P.CURRENCY_CODE"
                    + " from PAY_REMIT P"
                    + " where P.PAY_REMIT_OID = ?" ;

	//DocumentHandler xmlDoc = new DocumentHandler();
	xmlDoc = new DocumentHandler();
	xmlDoc = DatabaseQueryBean.getXmlResultSet(selectClause, false,new Object[]{payremitOid});
   	listviewVector = xmlDoc.getFragments("/ResultSetRecord");
   	DocumentHandler resultRow = (DocumentHandler)listviewVector.elementAt(0);
  //IValavala CR742
   	customerOID =  resultRow.getAttribute("/A_SELLER_CORP_ORG_OID");

	selectClause = "";
	selectClause = "select M.MATCHED_AMOUNT, M.INVOICE_MATCHING_STATUS, M.MATCH_EXPLANATION"
				   + " from PAY_MATCH_RESULT M"
				   + " where M.P_PAY_REMIT_OID = ? " ;


   	DocumentHandler paymatchxmlDoc = new DocumentHandler();
	//paymatchxmlDoc = new DocumentHandler();
	paymatchxmlDoc = DatabaseQueryBean.getXmlResultSet(selectClause, false, new Object[]{payremitOid});
   	paymatchlistviewVector = paymatchxmlDoc.getFragments("/ResultSetRecord");
   	numofPayMatchResult = paymatchlistviewVector.size();

   	currency = resultRow.getAttribute("/CURRENCY_CODE");
   	current2ndNav = "N";
   	
   	//cr 742 - end
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<div class="pageMain">
  <div class="pageContent">
    <div class="secondaryNav">
      <div class="secondaryNavTitle">
        <%=resMgr.getText("SecondaryNavigation.Receivables",
            TradePortalConstants.TEXT_BUNDLE)%> 
      </div>

      <div>
<%
     //String current2ndNav      = null;
     WidgetFactory widgetFactory = new WidgetFactory(resMgr);
     current2ndNav = request.getParameter("current2ndNav");
     String matchingSelected   = TradePortalConstants.INDICATOR_YES;
  	 String invoicesSelected = TradePortalConstants.INDICATOR_NO;
     String inquiriesSelected = TradePortalConstants.INDICATOR_NO;
     if (TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES.equals(current2ndNav)) {
    	 matchingSelected = TradePortalConstants.INDICATOR_YES;
     }else if (TradePortalConstants.NAV__INVOICES.equals(current2ndNav)) {
    	 invoicesSelected = TradePortalConstants.INDICATOR_YES;
     }
     else if(TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)){
       inquiriesSelected = TradePortalConstants.INDICATOR_YES;
     }
     String navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES;
%>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Matching" />
    <jsp:param name="linkSelected" value="<%= matchingSelected %>" />
    <jsp:param name="action"       value="goToReceivableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
    <jsp:param name="hoverText"    value="ReceivableTransaction.matchingList" />
  </jsp:include>
  <%
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__INVOICES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Invoices" />
    <jsp:param name="linkSelected" value="<%= invoicesSelected %>" />
    <jsp:param name="action"       value="goToReceivableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
    <jsp:param name="hoverText"    value="ReceivableTransaction.invoicesList" />
  </jsp:include>
  <%
  navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__INQUIRIES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Inquiries" />
    <jsp:param name="linkSelected" value="<%= inquiriesSelected %>" />
    <jsp:param name="action"       value="goToReceivableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
    <jsp:param name="hoverText"    value="ReceivableTransaction.inquiryList" />
  </jsp:include>
    
        <div id="secondaryNavHelp" class="secondaryNavHelp">
          <%= OnlineHelp.createContextSensitiveLink("customer/payment_match_response.htm", resMgr, userSession) %>
        </div>
        <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %>	
        <div style="clear:both;"></div>
      </div>
    </div>

    <div class="subHeaderDivider"></div>
	
    <div class="transactionSubHeader">
      <%=StringFunction.xssCharsToHtml(receivablesMatchListTitle.toString()) %>
    </div>

  <%
  //Vshah - IR#DWUM043037276 - Rel8.0 - 04/30/2012 - <Begin>
  QueryListView queryListViewUsers = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 
  sqlQueryStr.append("select auth2fa_subtype from users where user_oid = ?");
   
  queryListViewUsers.setSQL(sqlQueryStr.toString(),new Object[]{userSession.getUserOid()});
  queryListViewUsers.getRecords();
     
  DocumentHandler sqlResult = queryListViewUsers.getXmlResultSet();
  //String tokenType = sqlResult.getAttribute("/ResultSetRecord(0)/TOKEN_TYPE");
  String authSubtype = sqlResult.getAttribute("/ResultSetRecord(0)/AUTH2FA_SUBTYPE");
  if (TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equals(authSubtype)) isVascoOTP = true; //IR#DNUM062245488 -  This will check if a user is set to use OTP Vasco type
  //Vshah - IR#DWUM043037276 - Rel8.0 - 04/30/2012 - <End> 
  %> 
  <% //cr498 reauthenticate if necessary
    String certAuthURL = ""; //needed for openReauth frag
    Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
    String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
    boolean matchRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_MATCH_RESPONSE);
    boolean approveDiscountRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_APPROVE_DISCOUNT);
    String authorizeLink = "";
    String approveDiscountLink = "";
    //CR711 - 04/18/2012
    String proxyAuthLink = "";
    //AAlubala - CR-711 Rel8.0 - 04/23/2012 - Proxy Authorize check - start
    boolean canProxyAuthorize = SecurityAccess.canProxyAuthorizeInstrument(userSecurityRights, InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, transactionType);
    //CR-711 - end
    if (matchRequireAuth||approveDiscountRequireAuth) {
    	
  %>   
  <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
  <%
        //NOTE: although there are lists on the screen, this is specific to the pay remit
        authorizeLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_AUTHORIZE + "'," +
            "'PayRemit')";
        approveDiscountLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
	    "'" + formName + "','" + TradePortalConstants.BUTTON_APPROVEDISCOUNTAUTHORIZE + "'," +
            "'PayRemit')";
        //AAlubala - CR711 Rel8.0 - Proxy Authorize link
        proxyAuthLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
	    "'" + formName + "','" + TradePortalConstants.BUTTON_PROXY_AUTHORIZE + "'," +
            "'PayRemit')";
    }
    //cr498 end
  %>
  <%
  //cr742remove definition for selectclause
		 selectClause  = "select P.PAYMENT_INVOICE_REFERENCE_ID, P.SELLER_ID_FOR_BUYER,"+ 
		"P.SELLER_NAME_FOR_BUYER, P.BUYER_NAME, P.BUYER_CUSTOMER_ID, P.PAYMENT_SOURCE_TYPE, "+
		"P.PAYMENT_VALUE_DATE, P.TRACER_LINE, P.PAYMENT_AMOUNT, P.BUYER_REFERENCE, P.NARRATIVE, "+
		"P.CHEQUE_REFERENCE, P.SELLER_REFERENCE, P.REMIT_SOURCE_TYPE, P.TOTAL_INVOICE_AMOUNT, "+
		"P.TOTAL_NUMBER_OF_INVOICES, P.SELLER_NAME, P.SELLER_CONTACT_NAME, P.SELLER_BANK_DETAILS, "+
		"P.SELLER_ADDRESS, P.BUYER_CONTACT_NAME, P.BUYER_INVOICE_REFERENCE_ID, P.BUYER_ADDRESS, "+
		"/*P.MATCHED_PAYMENT_STATUS,*/ P.CURRENCY_CODE"
	    + " from PAY_REMIT P"
	    + " where P.PAY_REMIT_OID = ? " ;

		DocumentHandler xmlDoc1 = new DocumentHandler();
		xmlDoc1 = DatabaseQueryBean.getXmlResultSet(selectClause, false,new Object[]{payRemitOid});
	   	Vector listviewVector1 = xmlDoc1.getFragments("/ResultSetRecord");
	   	//cr742 DocumentHandler resultRow = (DocumentHandler)listviewVector1.elementAt(0);
	   	 resultRow = (DocumentHandler)listviewVector1.elementAt(0);
	   	
	   	
	   	
	   	
	   	selectClause = "";
		selectClause = "select M.MATCHED_AMOUNT, M.INVOICE_MATCHING_STATUS, M.MATCH_EXPLANATION"
					   + " from PAY_MATCH_RESULT M"
					   + " where M.P_PAY_REMIT_OID = ? " ;


	   	//DocumentHandler paymatchxmlDoc = new DocumentHandler();
		paymatchxmlDoc = DatabaseQueryBean.getXmlResultSet(selectClause, false, new Object[]{payRemitOid});
	   	//Vector 
	   	paymatchlistviewVector = paymatchxmlDoc.getFragments("/ResultSetRecord");
	   	//int 
	   	numofPayMatchResult = paymatchlistviewVector.size();

		buyerName = buyerName.trim().length() > 0? buyerName : " ";
		buyerId = buyerId.trim().length() > 0 ? buyerId : " ";
		//out.println("buyerName="+buyerName);
		//out.println("buyerId length="+buyerId.trim().length());
		
%>     
 
<form name="<%=formName%>" id="<%=formName%>" method="POST" action="<%= formMgr.getSubmitAction(response) %>" 
    data-dojo-type="dijit.form.Form" style="margin-bottom: 0px;">
      <%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
<div class="formArea">
<% if(TradePortalConstants.INDICATOR_NO.equals(hasError)){ %>
<jsp:include page="/common/ErrorSection.jsp" />

<%
	}
%>

<div class="formContent">

<%= widgetFactory.createSectionHeader("1","PaymentMatchResponse.PayRemittanceSummary") %>
<div>
<div class="columnLeftNoBorder">
<%= widgetFactory.createSubsectionHeader( "PaymentRemittance.PaymentInformation", false, false, false, "") %>
		<div>
		<div class="formItem readOnly" >
			<div>
			<label for="DataBuyerName">
			<%=resMgr.getText("PaymentMatchResponse.BuyerName",
                      TradePortalConstants.TEXT_BUNDLE)%>
			</label> 
			<%-- If buyer button should not be shown if buyerId already there, then uncomment below code --%>
			<% if (buyerId.trim().length() == 0) {%>
			<span class="searchButton">
			<a href="javascript:'#'" name="" onClick="openBuyerSearchDialog();return false;">
			
			</a>
			</span>
			<label class="formItemWithIndent4" for="DataBuyerID">
			<%} else{%>
			<label style="padding-left:60px;" for="DataBuyerID"><%} %>
			<%=resMgr.getText("PaymentMatchResponse.BuyerID",
                      TradePortalConstants.TEXT_BUNDLE)%>
                      </label>
			</div>
		</div>
		
		
<div class="formItem inline readOnly" >
<input data-dojo-type="dijit.form.TextBox" name="DataBuyerName" id="DataBuyerName" maxLength="30" class="char6" value="<%=buyerName %>" readonly="true">
</div>		
<div class="formItem inline readOnly" >
<input data-dojo-type="dijit.form.TextBox" name="DataBuyerID" id="DataBuyerID" maxLength="30" class="char6" value="<%=buyerId %>" readonly="true">
</div>
		
		<div style="clear:both;"></div>
		</div>
		<table>
		<tr><td><%= widgetFactory.createTextField("DataBuyerPaymentSource","PaymentMatchResponse.PaymentSource", ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.PAYMENT_SOURCE_TYPE, payRemitWebBean.getAttribute("payment_source_type"),loginLocale), "30",
		true,false,false,"","","inline") %></td><td>
		
		<%String valueDate =  resultRow.getAttribute("/PAYMENT_VALUE_DATE");
		String trimedValuDate=TPDateTimeUtility.formatDate(valueDate, TPDateTimeUtility.SHORT, loginLocale);
		if (InstrumentServices.isNotBlank(valueDate)) valueDate.substring(0, valueDate.length()-11);
		%>
		<%= widgetFactory.createTextField("DataBuyerValueDate","PaymentRemittance.ValueDate",trimedValuDate, "30",
		true,false,false,"","","inline") %>
		
		</td><td><%= widgetFactory.createTextField("DataBuyerTracerLine","PaymentRemittance.BankTracerLine", resultRow.getAttribute("/TRACER_LINE"), "30",
		true,false,false,"","","inline") %></td></tr>

		</table>
		
				 
</div>


<div class="columnRight">
<%= widgetFactory.createSubsectionHeader( "PaymentMatchResponse.RemittanceDetails", false, false, false, "") %>

			<div class="formItem inline readOnly">
		<%= widgetFactory.createLabel("","PaymentMatchResponse.SellersReferenceNumber",false,false,false,"none")%><br/>
		<b>	<%= payRemitWebBean.getAttribute("seller_reference") %></b>
		</div><div class="formItem inline readOnly">
		<%= widgetFactory.createLabel("","PaymentMatchResponse.RemittanceSource",false,false,false,"none")%><br/>
		<b><%= ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.PAYMENT_SOURCE_TYPE, payRemitWebBean.getAttribute("remit_source_type"),loginLocale) %></b>
	</div>
	<div style="clear:both;">&nbsp;</div>
	<div class="formItem inline readOnly">	
		<%= widgetFactory.createLabel("","PaymentMatchResponse.TotalNumberInvoicesCovered",false,false,false,"none")%><br/>
		<b><%=payRemitWebBean.getAttribute("total_number_of_invoices") %></b>	
		</div><div class="formItem inline readOnly" style="padding-left:23px;">
		<%= widgetFactory.createLabel("","PaymentMatchResponse.TotalInvoiceAmount",false,false,false,"none")%><br/>
		<b><%= StringFunction.xssCharsToHtml(formattedTotalInvoiceAmount.toString())%></b>
		</div>
		
		
		
</div>
<div style="clear:both;"></div>

	
		<div style="clear:both;"></div>

		<%--		
		IR#T36000017805
		AiA - Add Doc image link on matching page
		START
		 --%>
		<%= widgetFactory.createWideSubsectionHeader("Payremit.DocumentImage")%>
		<div>
		<%@ include file="./fragments/Receivables-PayMatch-Documents.frag" %> 
		</div>
<%-- IR#T36000017805 END --%>		
		

</br>
</div>
<%= widgetFactory.createSectionHeader("2", "PaymentMatchResponse.MiddleSection") %>
<div>
<div class="columnLeftNoBorder">
<table>
		<tr>
		<td><%= widgetFactory.createLabel("","PaymentRemittance.Seller",false,false,false,"none")%></td>
		<td><b><%= resultRow.getAttribute("/SELLER_NAME") %></b></td>
		</tr>
		<tr>
		<td><%= widgetFactory.createLabel("","PaymentRemittance.Contact",false,false,false,"none")%></td>
		<td><b><%= resultRow.getAttribute("/SELLER_CONTACT_NAME") %></b></td>
		</tr>
		<tr>
		<td><%= widgetFactory.createLabel("","PaymentRemittance.Bank",false,false,false,"none")%></td>
		<td><b><%= resultRow.getAttribute("/SELLER_BANK_DETAILS") %></b></td>
		</tr>
		<tr>
		<td><%= widgetFactory.createLabel("","PaymentRemittance.Address",false,false,false,"none")%></td>
		<td><b><%= resultRow.getAttribute("/SELLER_ADDRESS") %></b></td>
		</tr>
</table>
</div>
<div class="columnRight">
<table>
		<tr>
		<td><%= widgetFactory.createLabel("","PaymentRemittance.Buyer",false,false,false,"none")%></td>
		<td><b><%= resultRow.getAttribute("/BUYER_NAME") %></b></td>
		</tr>
		<tr>
		<td><%= widgetFactory.createLabel("","PaymentRemittance.Contact",false,false,false,"none")%></td>
		<td><b><%= resultRow.getAttribute("/BUYER_CONTACT_NAME") %></b></td>
		</tr>
		<tr>
		<td><%= widgetFactory.createLabel("","PaymentRemittance.Reference",false,false,false,"none")%></td>
		<td><b><%= resultRow.getAttribute("/BUYER_REFERENCE") %></b></td>
		</tr>
		<tr>
		<td><%= widgetFactory.createLabel("","PaymentRemittance.Address",false,false,false,"none")%></td>
		<td><b><%= resultRow.getAttribute("/BUYER_ADDRESS") %></b></td>
		</tr>
</table>
</div>
</div>
</div> <%-- ends div for  Seller and Buyer Information from Remittance Notice--%> 
</div> <%-- end of title pane for payment summary --%>

<%= widgetFactory.createSectionHeader("3", "PaymentMatchResponse.Matching") %>
<div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
    <tr>
      <td>&nbsp;</td>
      <td><i> 
        <%= resMgr.getText("PaymentMatchResponse.SelectRemittanceItemsText", TradePortalConstants.TEXT_BUNDLE) %>
      	</i>
      </td>
    </tr>
  </table>
<div class="columnLeftNoBorder">
<%= widgetFactory.createSubsectionHeader("Remittance Items") %>  
  <%-- Begin - Remittance List section --%>
 <div>
  <%@ include file="./fragments/PaymentMatchResponse_RemittanceList.frag" %>
  </div>
  <div class="remittanceInd">
  <%=widgetFactory.createCheckboxField("withoutRemitItemInd","WithoutRemitItemInd.text",withoutRemitItemsInd,(false||noRemittanceItems),false, "onClick='refreshInvoicesWithoutInd();'", "", "")%>  
 
  </div>
  <%-- End - Remittance List section --%>
</div>
<div class="columnRight">
  <%= widgetFactory.createSubsectionHeader("Amount") %>
  <% //String 
  currency = resultRow.getAttribute("/CURRENCY_CODE"); %>
  <table id="paymentTable" width="100%" border="0" cellspacing="0" cellpadding="0" >
    <tr>
      <td><span class="formItem"><%= widgetFactory.createLabel("","PaymentRemittance.PaymentAmount",false,false,false,"none")%></span></td>
      <td><%= currency %></td>
      <td><%=StringFunction.xssCharsToHtml(TPCurrencyUtility.getDisplayAmount(resultRow.getAttribute("/PAYMENT_AMOUNT"),currency, loginLocale)) %></td>
    </tr>
    <tr><td colspan='3'>&nbsp;</td></tr>
    <tr>
      <td><span class="formItem"><%= widgetFactory.createLabel("","Total Matched",false,false,false,"none")%></span></td>
      <td><%= currency %></td>
<%
  BigDecimal matchedAmount = BigDecimal.ZERO;
  for (int tmpLoop=0;tmpLoop<numofPayMatchResult;tmpLoop++) {
    DocumentHandler matchcountDoc = (DocumentHandler) paymatchlistviewVector.elementAt(tmpLoop);
    String matchingStatus = matchcountDoc.getAttribute("/INVOICE_MATCHING_STATUS");
    if (matchingStatus.equals(TradePortalConstants.MATCHING_STATUS_AUTO_MATCHED) ||	
        matchingStatus.equals(TradePortalConstants.MATCHING_STATUS_MANUAL_MATCH)||
        matchingStatus.equals(TradePortalConstants.MATCHING_STATUS_PARTIALLY_MATCHED)) { 
      matchedAmount = matchedAmount.add(matchcountDoc.getAttributeDecimal("/MATCHED_AMOUNT"));
    }
  }
%>
      <td><%=TPCurrencyUtility.getDisplayAmount(matchedAmount.toString(),currency, loginLocale) %></td>
    </tr>
    <tr><td colspan='3'>&nbsp;</td></tr>
    <tr>
      <td><span class="formItem"><%= widgetFactory.createLabel("","Unmatched",false,false,false,"none")%></span></td>
      <td><%= currency %></td>
<%
  BigDecimal 		       unmatchedAmount 			= BigDecimal.ZERO;
  unmatchedAmount = ((resultRow.getAttributeDecimal("/PAYMENT_AMOUNT")).subtract(matchedAmount));
%>
      <td><%= TPCurrencyUtility.getDisplayAmount(unmatchedAmount.toString(),currency, loginLocale) %></td>
    </tr>
    <tr><td colspan='3'>&nbsp;</td></tr>
  </table>

  <%= widgetFactory.createCheckboxField("UnapplyRemainingBalanceHidden1", "PaymentMatchResponse.UnapplyRemainingBalance",
        unapplyRemainingBalance, false, false, 	"onClick='updateUnapplyValue();'", "", "")%>
</div>
<div style="clear:both;"></div>
</div>

<%= widgetFactory.createWideSubsectionHeader("PaymentMatchResponse.SelectInvoiceItemsText")%>
  <%@ include file="./fragments/PaymentMatchResponse_InvoiceList.frag" %>
</div>
		
	  <input type=hidden value="" name="tempPayRemInvOid"  id="tempPayRemInvOid"> 
	  <input type=hidden value="<%=orgID%>" name="corpOrgOid"  id="corpOrgOid"> 
	  <input type=hidden value="false" name="remitInd" id="remitInd">
	  <input type=hidden value="" name="applied_amount" id="applied_amount">
      <input type=hidden value="" name="remitAmount" id="remitAmount">
      <input type=hidden value="" name="invoiceID" id="invoiceID">
	  <input type=hidden value="" name="OTLInvoiceUoid" id="OTLInvoiceUoid">
	  <input type=hidden value="N" name="autoPayInd" id="autoPayInd">
	  <input type=hidden value="<%=erpSettings%>" name="erpSettings" id="erpSettings">
	   <input type=hidden value="" name="autoMatchButton" id="autoMatchButton">
	   <input type=hidden value="<%=userSession.getLoginOrganizationId()%>" name="cbName" id="cbName"> 
       <input type=hidden value="<%=payRemitOid%>" name="PayRemitOid">
	  <input type=hidden value="" name="payInvOid"  id="payInvOid"> 
	   <input type=hidden value="" name=buttonName>
      <input type=hidden value="<%=payRemitOid%>" name="PayRemitOidHidden">
      <input type=hidden value='<%=payRemitWebBean.getAttribute("payment_invoice_reference_id")%>' name="PaymentInvoiceReferenceIdHidden">
      <input type=hidden value='<%=unmatchedAmount %>' name="AmountUnmatchedHidden" id="AmountUnmatchedHidden">
      <input type=hidden value='<%=matchedAmount %>' name="AmountMatchedHidden" id="AmountMatchedHidden">
      
      <input type=hidden value="<%=unapply%>" name="UnapplyRemainingBalanceHidden" id="UnapplyRemainingBalanceHidden">
      <input type=hidden value="<%=buyerId%>" name="HiddenBuyerID" id="HiddenBuyerID">
      <input type=hidden value="<%=buyerName%>" name="HiddenBuyerName" id="HiddenBuyerName">
      <input type=hidden name="fromListView" value="<%=TradePortalConstants.INDICATOR_YES%>">
      <input type=hidden name="fromPayRemit" value="<%=TradePortalConstants.INDICATOR_YES%>">
	   <input type=hidden value="" name="tempRemitOid" id="tempRemitOid">
  <% //cr498 add reauthentication hidden fields
    if (matchRequireAuth||approveDiscountRequireAuth) {
  %> 

      <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
      <input type=hidden name="reCertOK">
      <input type=hidden name="logonResponse">
      <input type=hidden name="logonCertificate">

  <%
    }
    //cr498 end
  %> 
  </div> <%-- close formContent --%>
</div> <%-- close formArea --%> 

  <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'PaymentMatchResponseForm'">
    <%@ include file="./fragments/PaymentMatchResponse_Sidebar.frag" %>
  </div> <%--closes sidebar area--%>
</form>      
  
</div>
</div> <%-- close page content --%>
</div> <%--  close page main --%>
<div class="pageFooter">
<div class="pageContent">
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton 5/22/2013 Rel 8.2 ir#14780 move sidebar behavior to footer--%>
<%@ include file="./fragments/PaymentMatchResponse_SidebarFooter.frag" %>
</div>
</div>
<div id="PayDiscountDetailDialog" ></div>
<div id="PayDiscountDetailErrorDialog" ></div>
<%
if (multiplRemittancesExist) {
%>
<%@ include file="./fragments/PaymentMatchResponse_RemittanceListFooter.frag" %>
<%} %>
 <%@ include file="./fragments/PaymentMatchResponse_InvoiceListFooter.frag" %>
 <div id ="invoiceSearchDialog"></div>
 <div id="buyerSearchDialog"></div>
</body>

<script language="Javascript">

var isAmountUpdated = false;

function openBuyerSearchDialog() {
	

require(["t360/dialog"], function(dialog ) {
	dialog.open('buyerSearchDialog', '<%=buyerSearchAddressTitle%>',
	'BuyerSearchDialog.jsp',
	'','','select', this.setBuyerDetails);
	});
}

function setBuyerDetails(buyerDetails) {
	
	require(["dijit/registry"], 
			function(registry) {
		registry.byId('DataBuyerName').attr('value',buyerDetails[0]);
		document.getElementById('HiddenBuyerName').value = buyerDetails[0];
		registry.byId('DataBuyerID').attr('value',buyerDetails[1]);
		document.getElementById('HiddenBuyerID').value = buyerDetails[1];
	
	});
}


function updateUnapplyValue(){
	require(["dijit/registry"], 
			function(registry) {
		var value = registry.byId('UnapplyRemainingBalanceHidden1').checked;
		if (value){
			console.log('UnapplyRemainingBalanceHidden1 has been checked');
			document.getElementById('UnapplyRemainingBalanceHidden').value = 'Y';
		} else {
			console.log('UnapplyRemainingBalanceHidden1 has been unchecked');
			document.getElementById('UnapplyRemainingBalanceHidden').value = 'N';
		}
	
	});
	
	
}

function closePopup(){
	
	require(["t360/dialog"], 
			function(dialog) {
	dialog.hide('paymentRemittanceGrid');

});
}

</script>



</html>




<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   if(TradePortalConstants.INDICATOR_NO.equals(hasError)){
   xmlDoc = formMgr.getFromDocCache();

   xmlDoc.removeAllChildren("/");

   formMgr.storeInDocCache("default.doc", doc);
   }
%>
