<%--
**********************************************************************************
                               Payment & Remittance Detail Page

  Description:
     This page is used to show the Payment & Remittance details.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*, com.amsinc.ecsg.html.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.util.*,java.math.*,java.text.SimpleDateFormat,java.util.Date" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="" />
</jsp:include>
<%-- Added for this page only --%>
<style type="text/css">
	.subsectionHeader h3 {
	width: 411px;
	}
</style>

<%--
*********************************** Data Retrival Logic for Payment and Remittance Section *******************************
--%>
<%
  String                         payremitOid                 	= null;
  DocumentHandler 	       paymentInfo			= null;
  Vector                         listviewVector;
  Vector 			       paymatchlistviewVector;
  String            current2ndNav      = null;
  String			       selectClause			= null;
  String 			       loginLocale 			= userSession.getUserLocale();
  String 			       currency				= null;
  String 			       matchExplanation			= null;
  //long 			       unmatchAmount 			= 0 ;
  //long			       matchAmount 			= 0;
  BigDecimal 		       unmatchAmount 			= BigDecimal.ZERO;
  BigDecimal		       matchAmount 			= BigDecimal.ZERO;
  int 			       numofPayMatchResult;
  int 			       tmpLoop; 
  
  //CR 742 related
  //Variables used to build the list of Document Images 
  DocumentHandler dbQuerydoc = null;
  StringBuffer query = null;
  String attribute;
  DocumentHandler myDoc;
  String linkArgs[] = {"","","",""};
  String displayText;
  String encryptVal1;
  String encryptVal2;
  String encryptVal3;
  String customerOID;
  boolean showPDFLinks = false;
  
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  if(request.getParameter("pay_remit_oid") != null) {
    payremitOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("pay_remit_oid"), userSession.getSecretKey());
    //out.println(payremitOid);
  }

  //cquinton 1/18/2013 get return link
  userSession.addPage("goToPaymentRemittance", request);

  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String closeAction = backPage.getLinkAction();
  String closeParms = backPage.getLinkParametersString() + "&returning=true";
  String closeLink = formMgr.getLinkAsUrl(closeAction,closeParms,response);

    //cr 742. IValavala getting A_SELLER_CORP_ORG_OID.
	selectClause  = "select P.PAYMENT_INVOICE_REFERENCE_ID, P.A_SELLER_CORP_ORG_OID, P.SELLER_ID_FOR_BUYER, P.SELLER_NAME_FOR_BUYER, P.BUYER_NAME, P.BUYER_CUSTOMER_ID, P.PAYMENT_SOURCE_TYPE, P.PAYMENT_VALUE_DATE, P.TRACER_LINE, P.PAYMENT_AMOUNT, P.BUYER_REFERENCE, P.NARRATIVE, P.CHEQUE_REFERENCE, P.SELLER_REFERENCE, P.REMIT_SOURCE_TYPE, P.TOTAL_INVOICE_AMOUNT, P.TOTAL_NUMBER_OF_INVOICES, P.SELLER_NAME, P.SELLER_CONTACT_NAME, P.SELLER_BANK_DETAILS, P.SELLER_ADDRESS, P.BUYER_CONTACT_NAME, P.BUYER_INVOICE_REFERENCE_ID, P.BUYER_ADDRESS, /*P.MATCHED_PAYMENT_STATUS,*/ P.CURRENCY_CODE"
                    + " from PAY_REMIT P"
                    + " where P.PAY_REMIT_OID = ? " ;
	
	DocumentHandler xmlDoc = new DocumentHandler();
	xmlDoc = DatabaseQueryBean.getXmlResultSet(selectClause, false, new Object[]{payremitOid});
   	listviewVector = xmlDoc.getFragments("/ResultSetRecord");
   	DocumentHandler resultRow = (DocumentHandler)listviewVector.elementAt(0);
  //IValavala CR742
   	customerOID =  resultRow.getAttribute("/A_SELLER_CORP_ORG_OID");

	selectClause = "";
	selectClause = "select M.MATCHED_AMOUNT, M.INVOICE_MATCHING_STATUS, M.MATCH_EXPLANATION"
				   + " from PAY_MATCH_RESULT M"
				   + " where M.P_PAY_REMIT_OID = ? ";


   	DocumentHandler paymatchxmlDoc = new DocumentHandler();
	paymatchxmlDoc = DatabaseQueryBean.getXmlResultSet(selectClause, false, new Object[]{payremitOid});
   	paymatchlistviewVector = paymatchxmlDoc.getFragments("/ResultSetRecord");
   	numofPayMatchResult = paymatchlistviewVector.size();

   	currency = StringFunction.xssCharsToHtml(resultRow.getAttribute("/CURRENCY_CODE"));
   	current2ndNav = "N";
   	
   	
%>

<%--
*********************************** Presentation Stuff for Payment and Remittance Section *******************************
--%>


<div class="pageMainNoSidebar">
  <div class="pageContent">
    <div class="secondaryNav">

      <span class="secondaryNavTitle">
        <%=resMgr.getText("SecondaryNavigation.Receivables",
                      TradePortalConstants.TEXT_BUNDLE)%> 
      </span>

  <%
     String matchingSelected   = TradePortalConstants.INDICATOR_NO;
  	 String invoicesSelected = TradePortalConstants.INDICATOR_NO;
     String inquiriesSelected = TradePortalConstants.INDICATOR_NO;
     if (TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES.equals(current2ndNav)) {
    	 matchingSelected = TradePortalConstants.INDICATOR_YES;
     }else if (TradePortalConstants.NAV__INVOICES.equals(current2ndNav)) {
    	 invoicesSelected = TradePortalConstants.INDICATOR_YES;
     }
     else if(TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)){
       inquiriesSelected = TradePortalConstants.INDICATOR_YES;
     }
     String helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/payment_remittance_summary.htm",  resMgr, userSession);
     String navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Matching" />
    <jsp:param name="linkSelected" value="<%= matchingSelected %>" />
    <jsp:param name="action"       value="goToReceivableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
    <jsp:param name="hoverText"    value="ReceivableTransaction.matchingList" />
  </jsp:include>
  <%
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__INVOICES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Invoices" />
    <jsp:param name="linkSelected" value="<%= invoicesSelected %>" />
    <jsp:param name="action"       value="goToReceivableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
    <jsp:param name="hoverText"    value="ReceivableTransaction.invoicesList" />
  </jsp:include>
  <%
  navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__INQUIRIES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Inquiries" />
    <jsp:param name="linkSelected" value="<%= inquiriesSelected %>" />
    <jsp:param name="action"       value="goToReceivableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
    <jsp:param name="hoverText"    value="ReceivableTransaction.inquiryList" />
  </jsp:include>

      <span id="secondaryNavHelp" class="secondaryNavHelp">
        <%=helpSensitiveLink %>
      </span>
	  <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %>
      <div style="clear:both;"></div>
    </div>

    <div class="subHeaderDivider"></div>

    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
     	<%=resMgr.getText("PaymentRemittance.PaymentRemittanceSummary", TradePortalConstants.TEXT_BUNDLE)%> - <%=resMgr.getText("PaymentRemittance.Payment", TradePortalConstants.TEXT_BUNDLE)%> <%=resultRow.getAttribute("/PAYMENT_INVOICE_REFERENCE_ID")%>
      </span>
      <%--cquinton 10/31/2012 ir#7015 replace return links with close button--%>
      <span class="pageSubHeader-right">
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href = "<%= closeLink %>";
          </script>
        </button> 
      </span>
      <%=widgetFactory.createHoverHelp("CloseButton","PaymentRemittanceCloseHoverText") %>
      <div style="clear:both;"></div>
    </div>

		<div class="formContentNoSidebar" >
		<div class="columnLeft">
			<%= widgetFactory.createSubsectionHeader("PaymentRemittance.PaymentInformation")%>
				
			<table>
	            <tr>
		            <td>
			            <%= widgetFactory.createLabel("","PaymentRemittance.BuyerName",false,false,false,"")%>
			            <div class="formItem readOnly newLine"><%= resultRow.getAttribute("/SELLER_NAME_FOR_BUYER") %></div>
		            </td>
		         <td>&nbsp;</td>
		            <td>
			            <%= widgetFactory.createLabel("","PaymentRemittance.BuyerID",false,false,false,"")%>
			            <div class="formItem readOnly newLine"><%= resultRow.getAttribute("/SELLER_ID_FOR_BUYER") %></div>
		            </td>
	            </tr>
	            
	            <tr>
		            <td>
			            <%
			            	ReferenceDataManager rdm = ReferenceDataManager.getRefDataMgr();
			            	String formattedValue = "";
			            	String value= resultRow.getAttribute("/PAYMENT_SOURCE_TYPE");
			                //return empty string if value is blank
			                if (!InstrumentServices.isBlank(value)) {
			              
			                 try  {
			                    formattedValue = rdm.getDescr("PAYMENT_SOURCE_TYPE", value, loginLocale);
			                    //When Description is not fetched let the code be used instead of description
			                    if(formattedValue==null||(formattedValue!=null&&formattedValue.equals("")))
			                    {
			                        formattedValue=value;
			                    }
			            
			                } catch (AmsException e) {
			                    System.out.println("Error looking up refdata code: " + value);
			                    System.out.println("  Error is " + e.toString());
			                    formattedValue = value;
			                }
			                 }
			            %>
			            <%= widgetFactory.createLabel("","PaymentRemittance.PaymentSource",false,false,false,"")%>
						<div class="formItem readOnly newLine"><%= StringFunction.xssCharsToHtml(formattedValue) %></div>
		            </td>
		            <td>
		            	<%
			            	SimpleDateFormat sdf=new SimpleDateFormat("dd MMM yyyy");
							SimpleDateFormat isoDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							String valueDate = resultRow.getAttribute("/PAYMENT_VALUE_DATE");
							Date date = null;
							if (InstrumentServices.isNotBlank(valueDate)) {
								date= isoDateFormatter.parse(valueDate);
							    valueDate = sdf.format(date);
							}
		            	%>
						<%= widgetFactory.createLabel("","PaymentRemittance.ValueDate",false,false,false,"")%>
						<div class="formItem readOnly newLine"><%= valueDate %></div>
					</td>
		            <td valign="top">
			            <%= widgetFactory.createLabel("","PaymentRemittance.BankTracerLine",false,false,false,"")%>
						<div class="formItem readOnly newLine"><%= resultRow.getAttribute("/TRACER_LINE") %></div>
		            </td>
	            </tr>
            
            </table>

				<%-- <%= resMgr.getText("PaymentRemittance.ReferenceNumber", TradePortalConstants.TEXT_BUNDLE) %>:
				<%= resultRow.getAttribute("/PAYMENT_INVOICE_REFERENCE_ID") %> --%>
				<div class="clear"></div>
				<%= widgetFactory.createSubsectionHeader("PaymentRemittance.Amounts")%>
				<table>
					<tr>
						<td><%= widgetFactory.createLabel("","PaymentRemittance.PaymentAmount",false,false,false,"inline")%></td>
						<td><div class="formItem readOnly newLine"><%= currency + " " +  TPCurrencyUtility.getDisplayAmount(resultRow.getAttribute("/PAYMENT_AMOUNT"),currency, loginLocale) %></div></td>
					</tr>
					<tr>
						<td><%= widgetFactory.createLabel("","PaymentRemittance.AmountMatched",false,false,false,"inline")%></td>
						<td>
							<%
			       		 		for (tmpLoop=0;tmpLoop<numofPayMatchResult;tmpLoop++)
				       		 	{
			       					DocumentHandler matchcountDoc = (DocumentHandler) paymatchlistviewVector.elementAt(tmpLoop);
			       					String matchingStatus = matchcountDoc.getAttribute("/INVOICE_MATCHING_STATUS");
			       					if (matchingStatus.equals(TradePortalConstants.MATCHING_STATUS_AUTO_MATCHED)) {
			       						matchAmount = matchAmount.add(matchcountDoc.getAttributeDecimal("/MATCHED_AMOUNT"));
			       					}
			
			    	   		 	}
			       		 	%>
			         	 	<div class="formItem readOnly newLine"><%= currency + "\t" + TPCurrencyUtility.getDisplayAmount(matchAmount.toString(), currency, loginLocale) %></div>
						</td>
					</tr>
					<tr>
						<td><%= widgetFactory.createLabel("","PaymentRemittance.ManualMatched",false,false,false,"inline")%></td>
						<td>
							<%
			       		 		for (tmpLoop=0;tmpLoop<numofPayMatchResult;tmpLoop++)
				       		 	{
			       					DocumentHandler matchcountDoc = (DocumentHandler) paymatchlistviewVector.elementAt(tmpLoop);
			       					String matchingStatus = matchcountDoc.getAttribute("/INVOICE_MATCHING_STATUS");
			       					if (matchingStatus.equals(TradePortalConstants.MATCHING_STATUS_MANUAL_MATCH)) {
			       						matchAmount = matchAmount.add(matchcountDoc.getAttributeDecimal("/MATCHED_AMOUNT"));
			       					}
			
			    	   		 	}
			       		 	%>
			         	 	<span class="formItem readOnly newLine"><%= currency + "\t" + TPCurrencyUtility.getDisplayAmount(matchAmount.toString(), currency, loginLocale) %></span>
						</td>
					</tr>
				
					<tr>
						<td><%= widgetFactory.createLabel("","PaymentRemittance.AmountUnmatched",false,false,false,"inline")%></td>
						<td>
							<%
			       		 	unmatchAmount = ((resultRow.getAttributeDecimal("/PAYMENT_AMOUNT")).subtract(matchAmount));
			       		 	%>
			         	 	<div class="formItem readOnly newLine"><%=  currency + "\t" +  TPCurrencyUtility.getDisplayAmount(unmatchAmount.toString(),currency, loginLocale) %></div>
						</td>
					</tr>
					
					<tr>
						<td><%= widgetFactory.createLabel("","PaymentRemittance.MatchExplanation",false,false,false,"inline")%></td>
						<td>
							<%
					     		for (tmpLoop=0;tmpLoop<numofPayMatchResult;tmpLoop++)
					     		{
					     			DocumentHandler matchexDoc = (DocumentHandler) paymatchlistviewVector.elementAt(tmpLoop);
					     			matchExplanation = 	matchexDoc.getAttribute("/MATCH_EXPLANATION");
					     			if (! InstrumentServices.isBlank(matchExplanation))
					     			{
					     	%>
					     	
					         	 	<%-- <div class="formItem newLine"><%= matchExplanation %></div> --%>
					        	 
					     	<%
					     			}
					     		}
					     	%>
					     	<div class="formItem readOnly newLine"><%= matchExplanation %></div>
						</td>
					</tr>
				</table>
		</div><%-- End ColumnLeft --%>
		
		<div class="columnRight">
			<%= widgetFactory.createSubsectionHeader("PaymentRemittance.RemittanceInformation")%>
			<table width="95%">
				<tr>
					<td>
						<%= widgetFactory.createLabel("","PaymentRemittance.SellerReferenceNumber",false,false,false,"")%>
					<div class="formItem readOnly newLine"><%= resultRow.getAttribute("/SELLER_REFERENCE") %></div></td>
					<%
			            	
			            	formattedValue = "";
			            	value= resultRow.getAttribute("/REMIT_SOURCE_TYPE");
			                //return empty string if value is blank
			                if (!InstrumentServices.isBlank(value)) {
			              
			                 try  {
			                    formattedValue = rdm.getDescr("REMIT_SOURCE_TYPE", value, loginLocale);
			                    //When Description is not fetched let the code be used instead of description
			                    if(formattedValue==null||(formattedValue!=null&&formattedValue.equals("")))
			                    {
			                        formattedValue=value;
			                    }
			            
			                } catch (AmsException e) {
			                    System.out.println("Error looking up refdata code: " + value);
			                    System.out.println("  Error is " + e.toString());
			                    formattedValue = value;
			                }
			                 }
			            %>
					
					<td><%= widgetFactory.createLabel("","PaymentRemittance.RemittanceSource",false,false,false,"")%>
					<div class="formItem readOnly newLine"><%= StringFunction.xssCharsToHtml(formattedValue) %></div></td>
				</tr>
				
				<tr>
					<td><%= widgetFactory.createLabel("","PaymentMatchResponse.TotalNumberInvoicesCovered",false,false,false,"")%>
					<div class="formItem readOnly newLine"><%= resultRow.getAttribute("/TOTAL_NUMBER_OF_INVOICES") %></div></td>
					<td><%= widgetFactory.createLabel("","PaymentRemittance.TotalInvoiceAmount",false,false,false,"")%>
					<div class="formItem readOnly newLine"><%= currency + "\t" + TPCurrencyUtility.getDisplayAmount(resultRow.getAttribute("/TOTAL_INVOICE_AMOUNT"), currency, loginLocale) %></div></td>
				</tr>
				
			</table>
			
			
			
			<%= widgetFactory.createSubsectionHeader("PaymentRemittance.ReferenceOnPayments")%>
			 
			<table width="95%">
				<tr>
					<td width="25%"><%= widgetFactory.createLabel("","PaymentRemittance.BuyerReference",false,false,false,"")%></td>
					<td width="50%"><%= widgetFactory.createLabel("","PaymentRemittance.Narrative",false,false,false,"")%></td>
					<td width="25%"><%= widgetFactory.createLabel("","PaymentRemittance.ChequeReference",false,false,false,"")%></td>
				</tr>
				<tr>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/BUYER_INVOICE_REFERENCE_ID") %></div></td>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/NARRATIVE") %></div></td>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/CHEQUE_REFERENCE") %></div></td>
				</tr>
			</table>
		</div><%-- End ColumnRight --%>
		<%--
		IR#T36000017805 - remove from here
		<%-- 742 start  --%>
		<div style="clear:both"></div>
		<%= widgetFactory.createWideSubsectionHeader("Payremit.DocumentImage")%>
		<div class="columnLeftNoBorder">
		<%@ include file="./fragments/Receivables-PayMatch-Documents.frag" %> 
		</div>
		<%-- 742 start  --%>
		
		<div style="clear:both"></div>
		
		<%= widgetFactory.createWideSubsectionHeader("PaymentRemittance.SellerBuyerInfo")%>
		<div class="columnLeft">
			<table>
				<tr>
					<td><%= widgetFactory.createLabel("","PaymentRemittance.Seller",false,false,false,"inline")%></td>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/SELLER_NAME") %></div></td>
				<tr>
					<td><%= widgetFactory.createLabel("","PaymentRemittance.ContactName",false,false,false,"inline")%></td>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/SELLER_CONTACT_NAME") %></div></td>
				</tr>
				<tr>
					<td><%= widgetFactory.createLabel("","PaymentRemittance.BankDetails",false,false,false,"inline")%></td>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/SELLER_BANK_DETAILS") %></div></td>
				</tr>
				<tr>
					<td><%= widgetFactory.createLabel("","PaymentRemittance.Address",false,false,false,"inline")%></td>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/SELLER_ADDRESS") %></div></td>
				</tr>
			</table>
		</div><%-- End columnLeft --%>
		
		<div class="columnRight">
			<table>
				<tr>
					<td><%= widgetFactory.createLabel("","PaymentRemittance.Buyer",false,false,false,"inline")%></td>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/BUYER_NAME") %></div></td>
				</tr>
				<tr>
					<td><%= widgetFactory.createLabel("","PaymentRemittance.ContactName",false,false,false,"inline")%></td>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/BUYER_CONTACT_NAME") %></div></td>
				</tr>
				<tr>
					<td><%= widgetFactory.createLabel("","PaymentRemittance.Reference",false,false,false,"inline")%></td>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/BUYER_REFERENCE") %></div></td>
				</tr>
				<tr>
					<td><%= widgetFactory.createLabel("","PaymentRemittance.Address",false,false,false,"inline")%></td>
					<td><div class="formItem readOnly newLine"><%= resultRow.getAttribute("/BUYER_ADDRESS") %></div></td>
				</tr>
			</table>
		</div><%-- End columnRight --%>
		<div style="clear:both"></div>
		<%= widgetFactory.createWideSubsectionHeader("PaymentRemittance.InvoiceInfo")%>
		<table>
			<tr>
				<td><%= widgetFactory.createLabel("","PaymentRemittance.InvoiceID",false,false,false,"")%></td>
				<td><%= widgetFactory.createLabel("","PaymentRemittance.AmountPaid",false,false,false,"")%></td>
				<td><%= widgetFactory.createLabel("","PaymentRemittance.OriginalAmount",false,false,false,"")%></td>
				<td><%= widgetFactory.createLabel("","PaymentRemittance.Discounted",false,false,false,"")%></td>
				<td><%= widgetFactory.createLabel("","PaymentRemittance.DiscountByBuyer",false,false,false,"")%></td>
				<%--AAlubala Rel8.2 CR 741 - 12/27/2012 - Add discount and comments column headers BEGIN--%>
				<td><%= widgetFactory.createLabel("","PaymentRemittance.DiscountCodeDesc",false,false,false,"")%></td>
				<td><%= widgetFactory.createLabel("","PaymentRemittance.CommentsByBuyer",false,false,false,"")%></td>				
				<%--CR741 END --%>
			</tr>
		
		<%--
		*********************************** Data Retrival as well as Presentaion Logic for Invoice and PO Section *******************************
		--%>
		
		<%
		
		DocumentHandler 			   invoiceList				= null;
		DocumentHandler 			   poList				= null;
		DocumentHandler 			   invoiceDoc				= null;
		DocumentHandler 			   poDoc				= null;
		Vector                         		   invoiceVector=null;
		Vector					   poVector=null;
		String				           selectInvoiceClause			= null;
		String					   tmpInvValue				= null;
		String					   selectPoClause			= null;
		String 					   discountInd			        = null;
		int 					   numInvoices=0;
		int 					   numPos;
		int					   iLoop;
		int					   jLoop;
		
		//AAlubala - Rel8.2 CR741 - 12/27/2012 - Modify SQL by adding BUYER_DISC_CODE and BUYER_DISC_COMMENTS - BEGIN
			selectInvoiceClause = "select PAY_REMIT_INVOICE_OID, INVOICE_REFERENCE_ID, INVOICE_PAYMENT_AMOUNT, INVOICE_BALANACE_AMOUNT, INVOICE_ORIGINAL_AMOUNT, BUYER_DISC_CODE, BUYER_DISC_COMMENTS, INVOICE_DISCOUNT_AMOUNT "
								  + " from PAY_REMIT_INVOICE"
								  + " where PAY_REMIT_INVOICE.P_PAY_REMIT_OID = ?" ;
		//CR741 - END
		  	invoiceList = DatabaseQueryBean.getXmlResultSet(selectInvoiceClause, false,new Object[]{payremitOid});
		  	if (invoiceList!= null) {
			invoiceVector = invoiceList.getFragments("/ResultSetRecord");
		   	numInvoices = invoiceVector.size();
		    Debug.debug("Number of Invoice " + numInvoices);
		  	}
		
		  	 for (iLoop=0;iLoop<numInvoices;iLoop++)
		  	 	{
					invoiceDoc = (DocumentHandler) invoiceVector.elementAt(iLoop);
					tmpInvValue = invoiceDoc.getAttribute("/PAY_REMIT_INVOICE_OID");
					// MUUJ010737162 Use BigDecimal to check both empty string and 0 correctly.
					BigDecimal invoiceDiscountAmount = BigDecimal.ZERO;
					try {
					   invoiceDiscountAmount = new BigDecimal(invoiceDoc.getAttribute("/INVOICE_DISCOUNT_AMOUNT"));
					}
					catch (NumberFormatException e) {
					}
					//if (invoiceDoc.getAttribute("/INVOICE_DISCOUNT_AMOUNT")!= null && !invoiceDoc.getAttribute("/INVOICE_DISCOUNT_AMOUNT").equals(""))
					if (invoiceDiscountAmount.compareTo(BigDecimal.ZERO) != 0)
					{
						discountInd = resMgr.getText("PaymentRemittance.DiscountedYes", TradePortalConstants.TEXT_BUNDLE);
					}
					else
					{
						discountInd = resMgr.getText("PaymentRemittance.DiscountedNo", TradePortalConstants.TEXT_BUNDLE) ;
					}
		%>
		
					
						<tr>
							<td>
								<div class="formItem readOnly newLine"><%=  invoiceDoc.getAttribute("/INVOICE_REFERENCE_ID") %></div>
							</td>
							<td>
								<div class="formItem readOnly newLine"><%=  currency + " " + TPCurrencyUtility.getDisplayAmount(invoiceDoc.getAttribute("/INVOICE_PAYMENT_AMOUNT"), currency, loginLocale) %></div>
							</td>
							<td>
								<div class="formItem readOnly newLine"><%= currency + " " + TPCurrencyUtility.getDisplayAmount(invoiceDoc.getAttribute("/INVOICE_ORIGINAL_AMOUNT"), currency, loginLocale) %></div>
							</td>
							<td>
								<div class="formItem readOnly newLine"><%=  discountInd %></div>
							</td>
							<td>
								<div class="formItem readOnly newLine"><%= currency + " " + TPCurrencyUtility.getDisplayAmount(invoiceDoc.getAttribute("/INVOICE_DISCOUNT_AMOUNT"), currency, loginLocale) %></div>
							</td>
							<%--AAlubala -Rel8.2 CR741 - 12/27/2012 Add buyer discount and discount comments  BEGIN--%>
<%
//Rel8.2 IR T36000015370 - If there is a discount description, include it
//String code = InvoicesSummaryData.getAttribute("cred_discount_code");
String code = invoiceDoc.getAttribute("/BUYER_DISC_CODE");
String desc = "";
String theDisplay = "";

//Note - As per requirements, No duplicates expected
String discSQL = "SELECT DISCOUNT_DESCRIPTION as DISCOUNT_DESCRIPTION FROM CUSTOMER_DISCOUNT_CODE WHERE DISCOUNT_CODE = ? union select descr as DISCOUNT_DESCRIPTION from bankrefdata where code = ?";

	 
DocumentHandler discCodeDoc = DatabaseQueryBean.getXmlResultSet(discSQL, false,code,code);
if (discCodeDoc != null){
	desc = discCodeDoc.getAttribute("/ResultSetRecord(0)/DISCOUNT_DESCRIPTION");
}

if(StringFunction.isNotBlank(desc)){
	theDisplay = code +" - "+desc;
}else{
	theDisplay = code;
}
%>							
							
							
							<td>
								<div class="formItem readOnly newLine"><%=StringFunction.xssCharsToHtml(theDisplay) %></div>
							</td>
							<td>
								<div class="formItem readOnly newLine"><%=  invoiceDoc.getAttribute("/BUYER_DISC_COMMENTS") %></div>
							</td>														
							<%--END CR741 --%>							
						</tr>
						<%} %>
					</table>
					
		  			
					<div style="margin-left:20px">
					<%= widgetFactory.createWideSubsectionHeader("PaymentRemittance.POInformationFromRemittance")%>	
			  		<table>
						<tr>
							<td><%= widgetFactory.createLabel("","PaymentRemittance.POID",false,false,false,"")%></td>
							<td><%= widgetFactory.createLabel("","PaymentRemittance.POIssueDate",false,false,false,"")%></td>
							<td><%= widgetFactory.createLabel("","PaymentRemittance.POType",false,false,false,"")%></td>
							<td><%= widgetFactory.createLabel("","PaymentRemittance.Description",false,false,false,"")%></td>
						</tr>		
		 				<%
				 			
		 					for (iLoop=0;iLoop<numInvoices;iLoop++){
		 					invoiceDoc = (DocumentHandler) invoiceVector.elementAt(iLoop);
		 					tmpInvValue = invoiceDoc.getAttribute("/PAY_REMIT_INVOICE_OID");
		 					selectPoClause = "SELECT PO_REFERENCE_ID,PO_ISSUE_DATE,PO_TYPE,PO_DESCRIPTION FROM PAY_REMIT_INVOICE_PO "
				 					+" WHERE PAY_REMIT_INVOICE_PO.P_PAY_REMIT_INVOICE_OID = ? ";
		
		
		 					poList = DatabaseQueryBean.getXmlResultSet(selectPoClause, false, new Object[]{tmpInvValue});
				  		  	poVector = poList.getFragments("/ResultSetRecord");
		  				  	numPos = poVector.size();
		
				  		  	Debug.debug("number of POs " + numPos);
		
		  				  	for (jLoop=0;jLoop<numPos;jLoop++)
				  		  	{
		  				  		
		  				  		poDoc = (DocumentHandler) poVector.elementAt(jLoop);
								tmpInvValue = invoiceDoc.getAttribute("/PAY_REMIT_INVOICE_OID");
		 				%>
		 						
		         	 			
		         	 				<tr>
		         	 					<td>
		         	 						<div class="formItem readOnly newLine"><%= poDoc.getAttribute("/PO_REFERENCE_ID") %></div>
		         	 					</td>
		         	 					<td>
		         	 						<div class="formItem readOnly newLine"><%= TPDateTimeUtility.formatDate(poDoc.getAttribute("/PO_ISSUE_DATE"), TPDateTimeUtility.LONG, loginLocale) %></div>
		         	 					</td>
		         	 					<td>
		         	 						<div class="formItem readOnly newLine"><%= poDoc.getAttribute("/PO_TYPE") %></div>
		         	 					</td>
		         	 					<td>
		         	 						<div class="formItem readOnly newLine"><%= poDoc.getAttribute("/PO_DESCRIPTION") %></div>
		         	 					</td>
		         	 				</tr>
		         	 			
						   <%
									}
		 					}
							%>
							</table>
							</div>
		</div>

	<div style="clear:both"></div>
	
	
	

	</div><%-- End pageContent --%>
</div><%-- End pageMain --%>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
