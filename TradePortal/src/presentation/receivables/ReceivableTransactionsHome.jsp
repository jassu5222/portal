<%--
**********************************************************************************
  Receivables Transactions Home

  Description:
     This page is used as the main Receivables Management page. It displays one of
     three tabs: Receivable Match Notices, Invoices, Receivables Instruments.

**********************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<% 
  WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  final String      STATUS_ALL = resMgr.getText("common.StatusAll",TradePortalConstants.TEXT_BUNDLE);
  StringBuffer      onLoad                 = new StringBuffer();
  Hashtable         secureParms            = new Hashtable();
  String            helpSensitiveLink      = null;
  String            userSecurityRights     = null;
  String            userSecurityType       = null;
  String            current2ndNav          = null;
  String            userOrgOid             = null;
  String            userOid                = null;
  String            formName               = "";
  StringBuffer      newLink                = new StringBuffer();
  boolean           canViewRecMatchNotices = false;
  boolean           canViewRecInstruments  = false;

  String loginLocale = userSession.getUserLocale();

  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryData =null;
  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";
   
   //Added for all the options
   String initSearchParms="";
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
   String gridHtml = "";
   String gridLayout = "";
   String encryptedSelectedWorkflow ="";
   //rbhaduri - CR-374 - 14th Oct 08

   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

   //cquinton 1/18/2013 set return page
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToReceivableTransactionsHome");
   }

   //IR T36000026794 Rel9.0 07/24/2014 starts
   session.setAttribute("startHomePage", "ReceivableTransactionsHome");
   session.removeAttribute("fromTPHomePage");
   //IR T36000026794 Rel9.0 07/24/2014 End
   
   StringBuffer newSearchReceivableCriteria = new StringBuffer();//PPramey - CR-434 - 22th Nov 08

   //cquinton 1/18/2013 remove old close action behavior
 
   userSecurityRights           = userSession.getSecurityRights();
   userSecurityType             = userSession.getSecurityType();
   userOrgOid                   = userSession.getOwnerOrgOid();
   userOid                      = userSession.getUserOid();

   //IR - SAUJ122835498 - Start
   CorporateOrganizationWebBean org = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   org.getById(userOrgOid);
   //IR - SAUJ122835498 - End

   canViewRecMatchNotices = SecurityAccess.hasRights(userSecurityRights,  SecurityAccess.ACCESS_REC_NOTICES_AREA);
   canViewRecInstruments  = SecurityAccess.hasRights(userSecurityRights,  SecurityAccess.ACCESS_RECEIVABLES_AREA);

   current2ndNav = request.getParameter("current2ndNav");
   if (current2ndNav == null)
   {
      current2ndNav = (String) session.getAttribute("receivableTransactions2ndNav");

      if (current2ndNav == null)
      {
         current2ndNav = TradePortalConstants.NAV__INQUIRIES;
      }
   }
   if (TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES.equals(current2ndNav)) {
	   if (org.allowSomeReceivablesTransaction()) {
		   if (!canViewRecMatchNotices && canViewRecInstruments) {
	   	     current2ndNav = TradePortalConstants.NAV__INVOICES;
		   }
	   }
   }

   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current tab as its context
   PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(current2ndNav);

   // ***********************************************************************
   // Data setup for the Receivables Match Notices tab
   // ***********************************************************************
   if (TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES.equals(current2ndNav))
   {
      formName           = "ReceivablesMatchNoticesForm";

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/receivables_matching_listview.htm",  resMgr, userSession);

   }

   // ***********************************************************************
   // Data setup for the Invoices tab
   // ***********************************************************************
   if (TradePortalConstants.NAV__INVOICES.equals(current2ndNav))
   {
      formName     = "InvoiceListForm";

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoice_listview.htm",  resMgr, userSession);

   %>
   <%@ include file="/receivables/fragments/InvoiceErrorParms.frag" %>
  <%
     } // End data setup for Invoices tab

   // ***********************************************************************
   // Data setup for the Receivables Instruments tab
   // ***********************************************************************
   else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav))
   {
      formName        = "ReceivablesInstrumentsForm";
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/receivables_instruments_listview.htm",  resMgr, userSession);
      %>
            <%@ include file="/receivables/fragments/InstrumentsSearchParms.frag" %>




   <%
   }  // End data setup for Receivables Instruments tab.
   String searchNav = "recTranHome."+current2ndNav;
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>

<%-- Vshah - 08/02/2012 - Rel8.1 - Portal-refresh --%>
<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>


<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

<div class="pageMainNoSidebar">
<div class="pageContent">

<div class="secondaryNav">

  <div class="secondaryNavTitle">
    <%=resMgr.getText("SecondaryNavigation.Receivables",
                      TradePortalConstants.TEXT_BUNDLE)%>
  </div>

  <%
    String matchingSelected  = TradePortalConstants.INDICATOR_NO;
    String invoicesSelected  = TradePortalConstants.INDICATOR_NO;
    String inquiriesSelected = TradePortalConstants.INDICATOR_NO;
    if (TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES.equals(current2ndNav))
    {
      matchingSelected = TradePortalConstants.INDICATOR_YES;
    }
    else if (TradePortalConstants.NAV__INVOICES.equals(current2ndNav))
    {
      invoicesSelected = TradePortalConstants.INDICATOR_YES;
    }
    else
    {
      inquiriesSelected = TradePortalConstants.INDICATOR_YES;
    }

    String linkParms = "";
    if(canViewRecMatchNotices == true) {
      linkParms = "current2ndNav=" + TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Matching" />
    <jsp:param name="linkSelected" value="<%= matchingSelected %>" />
    <jsp:param name="action"       value="goToReceivableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= linkParms %>" />
    <jsp:param name="hoverText"    value="ReceivableTransaction.matchingList" />
  </jsp:include>
  <%
    }
    if(canViewRecInstruments == true) {
      linkParms = "current2ndNav=" + TradePortalConstants.NAV__INVOICES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Invoices" />
    <jsp:param name="linkSelected" value="<%= invoicesSelected %>" />
    <jsp:param name="action"       value="goToReceivableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= linkParms %>" />
    <jsp:param name="hoverText"    value="ReceivableTransaction.invoicesList" />
  </jsp:include>
  <%
    }
    if(canViewRecInstruments == true) {
      linkParms = "current2ndNav=" + TradePortalConstants.NAV__INQUIRIES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Inquiries" />
    <jsp:param name="linkSelected" value="<%= inquiriesSelected %>" />
    <jsp:param name="action"       value="goToReceivableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= linkParms %>" />
    <jsp:param name="hoverText"    value="ReceivableTransaction.inquiryList" />
  </jsp:include>
  <%
    }
  %>

  <div id="secondaryNavHelp" class="secondaryNavHelp">
    <%=helpSensitiveLink%>
  </div>
  <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %>
  <div style="clear:both;"></div>

</div>


<% //cr498 reauthenticate if necessary
    String certAuthURL = ""; //needed for openReauth frag
    Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());
    String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
    boolean matchRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_MATCH_RESPONSE);
    boolean approveDiscountRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_APPROVE_DISCOUNT);
    boolean closeInvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_CLOSE_INVOICE);
    boolean financeInvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_FIN_INVOICE);
    boolean disputeInvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_DISPUTE_INVOICE);
    boolean ddiRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__DDI_ISS);
    String authorizeLink = "";
    String approveDiscountLink = "";
    String closeInvoiceLink = "";
    String financeInvoiceLink = "";
    String disputeInvoiceLink = "";
    String ddiLink = "";
    if ( matchRequireAuth||approveDiscountRequireAuth||
         closeInvoiceRequireAuth||financeInvoiceRequireAuth||disputeInvoiceRequireAuth||
         ddiRequireAuth ) {
%>
  <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
//IR T36000014931 REL ER 8.1.0.4 BEGIN
        authorizeLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_AUTHORIZE + "'," +
            "'PayRemit','matchNoticesGrid')";
        approveDiscountLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
	    "'" + formName + "','ApproveDiscountAuthorize'," +
            "'PayRemit','matchNoticesGrid')";
        closeInvoiceLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_CLOSE_SELECTED_ITEMS + "'," +
            "'RMInvoice','invoicesGrid')";
        financeInvoiceLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_FINANCE_SELECTED_ITEMS + "'," +
            "'RMInvoice','invoicesGrid')";
        disputeInvoiceLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_DISPUTEUNDISPUTE_SELECTEDITEMS + "'," +
            "'RMInvoice','invoicesGrid')";
//IR T36000014931 REL ER 8.1.0.4 END	    
        ddiLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_AUTHORIZE + "'," +
            "'Transaction')";
    }

%>

<form name="<%=formName%>" id="<%=formName%>" method="POST"
      action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">

 <jsp:include page="/common/ErrorSection.jsp" />

 <div class="formContentNoSidebar">
  <input type=hidden value="" name=buttonName>

<%  //cr498 add reauthentication hidden fields
    if ( matchRequireAuth||approveDiscountRequireAuth||
         closeInvoiceRequireAuth||financeInvoiceRequireAuth||disputeInvoiceRequireAuth || ddiRequireAuth) {
%>

      <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
      <input type=hidden name="reCertOK">
      <input type=hidden name="logonResponse">
      <input type=hidden name="logonCertificate">

<%
    }
%>


<%
  // Based on the current link, display the appropriate HTML

  if (TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES.equals(current2ndNav))
  {if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("MatchNoticesDataView");
	}
%>
    <%@ include file="/receivables/fragments/ReceivablesManagement_Notices.frag" %>
<%
  }
  else if (TradePortalConstants.NAV__INVOICES.equals(current2ndNav))
  {if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("InvoicesDataView");
	}
%>
    <%@ include file="/receivables/fragments/ReceivablesManagement_Invoices.frag" %>
<%
  }
  else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav))
  {if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("ARMInstrumentDataView");
	}
%>
    <%@ include file="/receivables/fragments/ReceivablesManagement_Instruments.frag" %>
<%
  }
%>

<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
</div>
</form>

</div>
</div>

<%--PMitnala Cr 821 Rel 8.3 START --%>
<form method="post" id="Receivables-NotifyPanelUserForm" name="Receivables-NotifyPanelUserForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden name="buttonName" value="">
<input type=hidden name="payRemitOid" value=""> 
<%= formMgr.getFormInstanceAsInputField("Receivables-NotifyPanelUserForm", secureParms) %>
 </form>
 <%--PMitnala Cr 821 Rel 8.3 END --%>
 
<%--cquinton 3/16/2012 Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton 3/16/2012 Portal Refresh - end--%>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>

<script>

<%
  if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) {
%>

  function dialogQuickView(rowKey){
    var dialogDiv = document.createElement("div");
    dialogDiv.setAttribute('id',"quickviewdialogdivid");
    document.forms[0].appendChild(dialogDiv);
 
    var title="";

    require(["t360/dialog"], function(dialog ) {
      
      dialog.open('quickviewdialogdivid',title ,
                  'TransactionQuickViewDialog.jsp',
                  ['rowKey','DailogId'],[rowKey,'quickviewdialogdivid'], <%-- parameters --%>
                  'select', quickViewCallBack);
    });
  }

  var quickViewFormatter=function(columnValues, idx, level) {
    var gridLink="";

    if(level == 0){

      if ( columnValues.length >= 2 ) {
        gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
      }
      else if ( columnValues.length == 1 ) {
        <%-- no url parameters, just return the display --%>
        gridLink = columnValues[0];
      }
      return gridLink;

    }else if(level ==1){
      console.log(columnValues[3]);
      if(columnValues[3]=='PROCESSED_BY_BANK'){
        if(columnValues[2]){
          return "<a href='javascript:dialogQuickView("+columnValues[2]+");'>Quick View</a> "+columnValues[0]+ " ";
        }else{
          return columnValues[0];	
        }

      }else{
        return columnValues[0];
      }

    }
  }

  var formatGridLinkChild=function(columnValues, idx, level){
    var gridLink="";

    if(level == 0){
      return columnValues[0];

    }else if(level ==1){
      if ( columnValues.length >= 2 ) {
        gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
      }
      else if ( columnValues.length == 1 ) {
        <%-- no url parameters, just return the display --%>
        gridLink = columnValues[0];
      }
      return gridLink;
    }
  }
	
  <%-- function to set the title after ajax call of dialog value[2]- dialogId, value[0]-complete instrument id, value[1]-transaction type --%>
  function quickViewCallBack(key,value){
    dijit.byId(value[2]).set('title','Transaction Quick View - '+value[0]+' - '+value[1]);
  }

  var historyGridLayout = [
    {name:"<%=resMgr.getText("InstrumentHistory.INSTRUMENT",TradePortalConstants.TEXT_BUNDLE)%>", field:"INSTRUMENT", fields:["INSTRUMENT","INSTRUMENT_linkUrl","transaction_oid","STATUSFORQVIEW","Type"], formatter:quickViewFormatter,width:"180px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.Type",TradePortalConstants.TEXT_BUNDLE)%>", field:"Type", fields:["Type","Type_linkUrl"], formatter:formatGridLinkChild, width:"180px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.CCY",TradePortalConstants.TEXT_BUNDLE)%>", field:"CCY", width:"80px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.AMOUNT",TradePortalConstants.TEXT_BUNDLE)%>", field:"AMOUNT", width:"100px", cellClasses:"gridColumnRight"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.STATUS",TradePortalConstants.TEXT_BUNDLE)%>", field:"STATUS", width:"100px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.REFERENCE",TradePortalConstants.TEXT_BUNDLE)%>", field:"REFERENCE", fields:["REFERENCE","REFERENCE_linkUrl"], formatter:formatGridLink, width:"150px"} ];
<%
  }
%>
var statusFormatter = function(columnValues){
	  var gridLink = "";
		if(columnValues[2] == 'PARTIALLY_AUTHORIZED' || columnValues[2] == 'AVAILABLE_FOR_AUTHORIZATION' || columnValues[2] == 'AUTHORIZE_FAILED'){
			var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1]; 
			gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"matchNoticesGrid\",\"RM\");'>"+columnValues[0]+"</a>";
	    	 return gridLink;
		}else{
			return columnValues[0];
		}
	}

	var initSearchParms="";
	var loginLocale="";
	var instrument="";

	function initializeDataGrid(DataGridId,DataView){

			var gridLayout = <%=gridLayout%>;
		
		<%--set the initial search parms--%>
		initSearchParms = "<%=initSearchParms%>";
		var savedSort = savedSearchQueryData["sort"];  
		 require(["t360/datagrid", "dojo/dom","dijit/registry"],
			        function( t360grid,dom,registry ) {
		if(DataGridId == "invoicesGrid"){
			if("" == savedSort || null == savedSort || "undefined" == savedSort){
		        savedSort = 'BuyerIdentifier';
		     }
			var dispute = savedSearchQueryData["dispute"]; 
         	var finance = savedSearchQueryData["finance"];
			var currency = savedSearchQueryData["currency"]; 
         	var amountFrom = savedSearchQueryData["amountFrom"]; 
         	var amountTo = savedSearchQueryData["amountTo"]; 
         	var fromDate = savedSearchQueryData["fromDate"]; 
         	var toDate = savedSearchQueryData["toDate"]; 
         	var amountType = savedSearchQueryData["amountType"]; 
        	var dateType = savedSearchQueryData["dateType"]; 
        	var invoiceStatus = savedSearchQueryData["selectedStatus"]; 
        	
        	var invoiceID = savedSearchQueryData["invoiceID"]; 
         	var partyFilterText = savedSearchQueryData["partyFilterText"]; 
         	var poNumber = savedSearchQueryData["poNumber"]; <%-- IR T36000020287- use poNumber- savedSearchQueryData["toDate"];  --%>
         	
         	if("" == dispute || null == dispute || "undefined" == dispute){
		   		if(dom.byId("Disputed").checked)
		   			dispute="Y";
		   	}
         	else{
         		if(dispute=="Y")
    				registry.byId("Disputed").set('checked',true);
    			   else
    				   registry.byId("Disputed").set('checked',false);
    		   	}
			if("" == finance || null == finance || "undefined" == finance){
		   		if(dom.byId("Financed").checked)
		   			finance="Y";
		   	}else{
			   if(finance=="Y")
				registry.byId("Financed").set('checked',true);
			   else
				   registry.byId("Financed").set('checked',false);
		   	}
         	if("" == currency || null == currency || "undefined" == currency)
           		currency=dom.byId("Currency").value;
      		 else
      		 {
      			registry.byId("Currency").set('value',currency);
	  		  	}
           	if("" == invoiceID || null == invoiceID || "undefined" == invoiceID)
           		invoiceID=dom.byId("InvoiceID").value.toUpperCase();
      		 else
      			dom.byId("InvoiceID").value = invoiceID;
           	
           	if("" == partyFilterText || null == partyFilterText || "undefined" == partyFilterText)
           		partyFilterText=dom.byId("BuyerIdentifier").value.toUpperCase();
      		 else
      			dom.byId("BuyerIdentifier").value = partyFilterText;
           	
           	if("" == poNumber || null == poNumber || "undefined" == poNumber)
           		poNumber=dom.byId("PONumber").value.toUpperCase();
      		 else
      			dom.byId("PONumber").value = poNumber;
           	
        	if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
           		amountFrom=dom.byId("AmountFrom").value;
      		 else
      			dom.byId("AmountFrom").value = amountFrom;

           	if("" == amountTo || null == amountTo || "undefined" == amountTo)
           		amountTo=dom.byId("AmountTo").value;
      		 else
      			dom.byId("AmountTo").value = amountTo;
        	
        	if("" == fromDate || null == fromDate || "undefined" == fromDate)
        		fromDate=dom.byId("DateFrom").value;
      		 else{
      			registry.byId("DateFrom").set('displayedValue',fromDate);
      		 }
        	
	    	if("" == toDate || null == toDate || "undefined" == toDate)
	    		toDate=dom.byId("DateTo").value;
	  		 else
	  			registry.byId("DateTo").set('displayedValue',toDate);
	    	
	    	if("" == amountType || null == amountType || "undefined" == amountType)
	    		amountType=dom.byId("AmountType").value;
	  		 else
	  		 {
	  			registry.byId("AmountType").set('value',amountType);
	  		  	}
	    	if("" == dateType || null == dateType || "undefined" == dateType)
	    		dateType=dom.byId("DateType").value;
	  		 else
	  		 {
	  			registry.byId("DateType").set('value',dateType);
	  		  	}
	    	if("" == invoiceStatus || null == invoiceStatus || "undefined" == invoiceStatus)
	    		invoiceStatus=dijit.byId("invoiceStatusType").value;
	  		 else{
		  			registry.byId("invoiceStatusType").set('value',invoiceStatus);
	  		  	}
	    	
	    	initSearchParms = initSearchParms+"&userOrgOid="+<%=userOrgOid%>+"&selectedStatus="+invoiceStatus+"&finance="+finance+"&dispute="+dispute+"&invoiceID="+invoiceID+"&partyFilterText="+partyFilterText+"&poNumber="
		        +poNumber+"&currency="+currency+"&amountType="+amountType+"&amountFrom="+amountFrom+"&amountTo="+amountTo+"&dateType="+dateType+"&fromDate="+fromDate
		        +"&toDate="+toDate;
	    	   var tempDatePattern = encodeURI('<%=datePattern%>');
		        initSearchParms = initSearchParms+"&dPattern="+tempDatePattern;
		}
		else if(DataGridId == "matchNoticesGrid"){
			
			 var paid = savedSearchQueryData["paid"]; 
		      var pending = savedSearchQueryData["pending"]; 
		      var workflow = savedSearchQueryData["selectedWorkflow"]; 
		   	var PaymentReference = savedSearchQueryData["paymentReference"]; 
		   	var BuyerName = savedSearchQueryData["buyerName"]; 
		   	var BuyerId = savedSearchQueryData["buyerID"]; 
			var PaymentSource = savedSearchQueryData["paymentSource"]; 
		   	if("" == savedSort || null == savedSort || "undefined" == savedSort){
		        savedSort = 'ReceiptDateTime';
		     }
		   	
			if("" == paid || null == paid || "undefined" == paid){
		   		if(dom.byId("Paid").checked)
		   			paid="Paid";
		   	}
			else{
				registry.byId("Paid").set('checked',true);
			}
			if("" == pending || null == pending || "undefined" == pending){
					pending="Pending";
		   	}
			else{
				registry.byId("Pending").set('checked',true);
			}
			if("" == workflow || null == workflow || "undefined" == workflow)
			workflow='<%=encryptedSelectedWorkflow%>';
			else
				registry.byId("Workflow").set('value',workflow);
			if("" == PaymentReference || null == PaymentReference || "undefined" == PaymentReference)
		  		PaymentReference=dom.byId("PaymentReference").value.toUpperCase();
				 else
					dom.byId("PaymentReference").value = PaymentReference;

		    if("" == BuyerName || null == BuyerName || "undefined" == BuyerName)
		    	BuyerName=dom.byId("BuyerName").value.toUpperCase();
				 else
					dom.byId("BuyerName").value = BuyerName;
		  	
		  	if("" == BuyerId || null == BuyerId || "undefined" == BuyerId)
		  		BuyerId=dom.byId("BuyerId").value.toUpperCase();
				 else
					dom.byId("BuyerId").value = BuyerId;
		  	if("" == PaymentSource || null == PaymentSource || "undefined" == PaymentSource)
		  		PaymentSource=dom.byId("PaymentSource").value.toUpperCase();
				 else
				 {
					 registry.byId("PaymentSource").set('value',PaymentSource);
		  		
				 }
		  	
		  	initSearchParms = "paid="+paid+"&pending="+pending+"&paymentReference="+PaymentReference+"&buyerName="+BuyerName
	    	+"&buyerID="+BuyerId+"&paymentSource="+PaymentSource+"&selectedWorkflow="+workflow;
		}
		
		loginLocale="<%=loginLocale%>";
		console.log("initSearchParms: " + initSearchParms);
		<%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
		
		<%-- Sandeep PR IR-T36000011164 (PR ANZ-729) 02/05/2013 - Start --%>
		<%-- If 'DataGridId' is 'invoicesGrid' then passing sort paramenter as '0' to get sort icon on 'Buyer Id' column. --%>
		
			createDataGrid(DataGridId, DataView,gridLayout, initSearchParms,savedSort);
		<%-- Sandeep PR IR-T36000011164 (PR ANZ-729) 02/05/2013 - End --%>
		 });
	}


<%
  if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) {
%>
  function initializeLazyDataGrid(DataGridId,DataView){
    <%-- cquinton 3/11/2013 --%>
    require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"],
	function(dom, registry, t360grid ) {


      var Active = savedSearchQueryData["Active"]; 
      var InActive = savedSearchQueryData["InActive"]; 

      var Currency = savedSearchQueryData["Currency"]; 
   	var InqAmountFrom = savedSearchQueryData["InqAmountFrom"]; 
   	var InqAmountTo = savedSearchQueryData["InqAmountTo"]; 
   	var InstrumentID = savedSearchQueryData["InstrumentID"]; 
   	var savedSort = savedSearchQueryData["sort"];  
   	if("" == savedSort || null == savedSort || "undefined" == savedSort){
        savedSort = '0';
     }
   
   	if("" == Active || null == Active || "undefined" == Active){
   		if(dom.byId("Active").checked)
   	        Active="Y";
   	}
   		 else{
	if(Active=="Y")
		registry.byId("Active").set('checked',true);
	   	  else
	   		registry.byId("Active").set('checked',false);
	 
	 }
   	if("" == InActive || null == InActive || "undefined" == InActive){
   		if(dom.byId("InActive").checked)
   			InActive="Y";
   	}
   	 else{
		 if(InActive=="Y")
			 registry.byId("InActive").set('checked',true);
	   	  else
	   		registry.byId("InActive").set('checked',false);
	   	
	 }
	 if("" == Currency || null == Currency || "undefined" == Currency)
   	  	Currency=dom.byId("Currency").value;
		 else
		 {
			 registry.byId("Currency").set('value',Currency);
		 
		}
  	if("" == InqAmountFrom || null == InqAmountFrom || "undefined" == InqAmountFrom)
  		InqAmountFrom=dom.byId("InqAmountFrom").value;
		 else
			dom.byId("InqAmountFrom").value = InqAmountFrom;

    if("" == InqAmountTo || null == InqAmountTo || "undefined" == InqAmountTo)
     		InqAmountTo=dom.byId("InqAmountTo").value;
		 else
			dom.byId("InqAmountTo").value = InqAmountTo;
  	
  	if("" == InstrumentID || null == InstrumentID || "undefined" == InstrumentID)
  		InstrumentID=dom.byId("InstrumentID").value.toUpperCase();
		 else
			dom.byId("InstrumentID").value = InstrumentID;
	  
      <%--set the initial search parms--%>
      initSearchParms = '<%=initSearchParms%>'+'&Active='+Active+'&InActive='+InActive;
      initSearchParms = initSearchParms+"&InqAmountFrom="+InqAmountFrom+"&InqAmountTo="+InqAmountTo+
   	 "&Currency="+Currency+"&InstrumentID="+InstrumentID+"&userOrgOid="+<%=userOrgOid%>;
      <%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
      t360grid.createLazyTreeDataGrid(DataGridId,DataView,historyGridLayout,initSearchParms,savedSort);
    });
  }
<%
  }
%>

	function booleanFormat(item){
		if(item=='true')
			return true;
		else
			return false;
	}
	<%--provide a search function that collects the necessary search parameters--%>
	  function searchInvoices() {
	    require(["dojo/dom"],
	      function(dom){

	    	var dispute="";
	    		if(dom.byId("Disputed").checked)
	    			dispute="Y";
	    		else
	    			dispute="N";

	    	var finance="";
	    		if(dom.byId("Financed").checked)
	    			finance="Y";
	    		else
	    			finance="N";

	    	var invoiceStatus = dijit.byId("invoiceStatusType").attr('value');
	    	var invoiceID=(dom.byId("InvoiceID").value).toUpperCase();
	    	var partyFilterText=(dom.byId("BuyerIdentifier").value).toUpperCase();
	    	var poNumber=(dom.byId("PONumber").value).toUpperCase();

	    	var currency=(dijit.byId("Currency").value).toUpperCase();
	    	var amountType=(dijit.byId("AmountType").value).toUpperCase();
	    	var amountFrom=(dom.byId("AmountFrom").value).toUpperCase();
	    	var amountTo=(dom.byId("AmountTo").value).toUpperCase();

	    	var dateType=(dijit.byId("DateType").value).toUpperCase();
	    	var fromDate=(dom.byId("DateFrom").value).toUpperCase();
	    	var toDate=(dom.byId("DateTo").value).toUpperCase();

	        var searchParms = "userOrgOid="+<%=userOrgOid%>+"&selectedStatus="+invoiceStatus+"&finance="+finance+"&dispute="+dispute+"&invoiceID="+invoiceID+"&partyFilterText="+partyFilterText+"&poNumber="
	        +poNumber+"&currency="+currency+"&amountType="+amountType+"&amountFrom="+amountFrom+"&amountTo="+amountTo+"&dateType="+dateType+"&fromDate="+fromDate
	        +"&toDate="+toDate;
			
	        var tempDatePattern = encodeURI('<%=datePattern%>');
	 		searchParms=searchParms+"&dPattern="+tempDatePattern;
	 		
	        console.log("SearchParms: " + searchParms);
	        searchDataGrid("invoicesGrid", "InvoicesDataView",
	                       searchParms);
	      });
	  }
	  require(["dijit/registry", "dojo/query", "dojo/on", "dojo/dom-class", "t360/dialog", 
	           "dojo/dom", "dijit/layout/ContentPane", "dijit/TooltipDialog", "dijit/popup",
	           "t360/common", "t360/popup", "t360/datagrid",
	           "dojo/ready", "dojo/domReady!"],
	      function(registry, query, on, domClass, dialog,
	               dom, ContentPane, TooltipDialog, popup, 
	               common, t360popup, t360grid,
	               ready ) {
		
	    <%--register widget event handlers--%>
	    ready(function() {
	    	<%-- Based on the conditions Datagrid will be intialize --%>
	    	<% if (TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES.equals(current2ndNav)){
	    		String viewName = EncryptDecrypt.encryptStringUsingTripleDes("MatchNoticesDataView",userSession.getSecretKey());  //Rel9.2 IR T36000032596  
	    	%>
	    			<%-- To Intialize Datagrid for MATCH_NOTICES --%>
	    			initializeDataGrid("matchNoticesGrid","<%=viewName%>");
	    	<%} %>

	    	<% if (TradePortalConstants.NAV__INVOICES.equals(current2ndNav)){
	    		String viewName = EncryptDecrypt.encryptStringUsingTripleDes("InvoicesDataView",userSession.getSecretKey());  //Rel9.2 IR T36000032596
	    	%>
	    			<%-- To Intialize Datagrid for INVOICES --%>
	    			initializeDataGrid("invoicesGrid","<%=viewName%>");
	    	<%} %>

	    	<% if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)){
	    		String dataViewName = EncryptDecrypt.encryptStringUsingTripleDes("ARMInstrumentDataView",userSession.getSecretKey());  //Rel9.2 IR T36000032596
	    	%>
	    			<%-- To Intialize LazyTreeDatagrid for INQUIRIES --%>
	    			initializeLazyDataGrid("rcvInstrumentGrid","<%=dataViewName%>");
	    	<%} %>
	      

	      <%--search event handlers--%>
	      var Workflow = registry.byId("Workflow");
	      if ( Workflow ) {
	    	  Workflow.on("change", function() {
	    		  searchMatchNotices();
	        });
	      }
	      
	      var Paid = registry.byId("Paid");
	      if ( Paid ) {
	    	  Paid.on("change", function() {
	    		  searchMatchNotices();
	        });
	      }
	      
	      var Pending = registry.byId("Pending");
	      if ( Pending ) {
	    	  Pending.on("change", function() {
	    		  searchMatchNotices();
	        });
	      }
	      
	      var invoiceStatusType = registry.byId("invoiceStatusType");
	      if ( invoiceStatusType ) {
	    	  invoiceStatusType.on("change", function() {
	    		  searchInvoices();
	        });
	      }
	      
	     var Disputed = registry.byId("Disputed");
	      if ( Disputed ) {
	    	  Disputed.on("change", function() {
	    		  searchInvoices();
	        });
	      }
	      
	      var Financed = registry.byId("Financed");
	      if ( Financed ) {
	    	  Financed.on("change", function() {
	    		  searchInvoices();
	        });
	      }
	      
<%
  if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)){
%>
	      var Active = registry.byId("Active");
	      if ( Active ) {
	    	  Active.on("change", function() {
	    		  searchInstrument();
	        });
	      }
	      
	      var InActive = registry.byId("InActive");
	      if ( InActive ) {
	    	  InActive.on("change", function() {
	    		  searchInstrument();
	        });
	      }
<%
  }
%>
	
	    });
	  });
	  
	  
	  
	
	  function searchMatchNotices() {
		    require(["dojo/dom"],
		      function(dom){

		    	var paid="";
		    		if(dom.byId("Paid").checked)
		    			paid="Paid";

		    	var pending="";
		    		if(dom.byId("Pending").checked)
		    			pending="Pending";

				var workflow = dijit.byId("Workflow").attr('value');				
		    	var PaymentReference=(dom.byId("PaymentReference").value).toUpperCase();
		    	var BuyerName=(dom.byId("BuyerName").value).toUpperCase();
		    	var BuyerId=(dom.byId("BuyerId").value).toUpperCase();
		    	var PaymentSource=(dijit.byId("PaymentSource").value).toUpperCase();

                        <%--cquinton 3/26/2013 Rel PR ir#15207 remove encrypted parm, workflow is always encrypted--%>
		    	var searchParms = "&paid="+paid+"&pending="+pending+"&paymentReference="+PaymentReference+"&buyerName="+BuyerName
		    	+"&buyerID="+BuyerId+"&paymentSource="+PaymentSource+"&selectedWorkflow="+workflow;
				console.log("SearchParms: " + searchParms);
		        searchDataGrid("matchNoticesGrid", "MatchNoticesDataView",
		                       searchParms);
		      });
		  }
	  
<%
  if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)){
%>
  <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
  <%-- Modified the function in order for enter key to work in Firefox --%>
   function filterRecTranOnEnter(evt, fieldID, buttonId){
	  require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldID), "keypress", function(evt) {
	  if(evt && evt.keyCode==13){
    searchInstrument();
  }
		    });
	  });
 }<%-- end of filterOnEnter() --%>
  <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>
	  
  function searchInstrument() {
    <%-- cquinton 3/11/2013 --%>
    require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"],
	function(dom, registry, t360grid ) {

      var Active="";
      if(dijit.byId("Active").checked)
        Active="Y";
      else
        Active="N";

      var InActive="";
      if(dijit.byId("InActive").checked)
        InActive="Y";
      else
        InActive="N";

      var InqAmountFrom=dom.byId("InqAmountFrom");
      var InqAmountTo=dom.byId("InqAmountTo");
      Currency = dijit.byId("Currency").attr('value');
		    	
      if(InqAmountTo){
	<%-- IR T36000020287 --use dom.byId instead of dijit.byId --%>
        InqAmountTo=dom.byId("InqAmountTo").value;
      }
      if(InqAmountFrom){
    	<%-- IR T36000020287 --use dom.byId instead of dijit.byId --%>
    	InqAmountFrom=dom.byId("InqAmountFrom").value;
      }
      if (isNaN(InqAmountTo))
        InqAmountTo='';
      if (isNaN(InqAmountFrom))
        InqAmountFrom='';
      var InstrumentID=dom.byId("InstrumentID").value;
      var searchParms = "Active="+Active+"&InActive="+InActive+"&InqAmountFrom="+InqAmountFrom+"&InqAmountTo="+InqAmountTo+"&Currency="+Currency+"&InstrumentID="+InstrumentID+"&userOrgOid="+<%=userOrgOid%>;
      console.log("SearchParms: " + searchParms);
      var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("ARMInstrumentDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
      t360grid.searchLazyTreeDataGrid("rcvInstrumentGrid", viewName,
        historyGridLayout, searchParms);
    });
  }
<%
  }
%>

	  function financeInvoices(){
		  <% 
		  //IR T36000014931 REL ER 8.1.0.4 BEGIN
		  if ( financeInvoiceRequireAuth && StringFunction.isNotBlank(certAuthURL)) {
		  %>
		  		openURL("<%=financeInvoiceLink%>");
		  <%} else {
		  //IR T36000014931 REL ER 8.1.0.4 END
		  %>		
		    var formName = '<%=formName%>';
		    var buttonName = '<%=TradePortalConstants.BUTTON_FINANCE_SELECTED_ITEMS%>';

		    <%--get array of rowkeys from the grid--%>
		    var rowKeys = getSelectedGridRowKeys("invoicesGrid");

		    <%--submit the form with rowkeys
		        becuase rowKeys is an array, the parameter name for
		        each will be checkbox + an iteration number -
		        i.e. checkbox0, checkbox1, checkbox2, etc --%>
		    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
		    <%}%>
	  }
	  function closeInvoices(){
		  <% 
		  //IR T36000014931 REL ER 8.1.0.4 BEGIN
		  if ( closeInvoiceRequireAuth && StringFunction.isNotBlank(certAuthURL)) {
		  %>
		  		openURL("<%=closeInvoiceLink%>");
		  <%} else {
		  //IR T36000014931 REL ER 8.1.0.4 END
		  %>		
		 	 var formName = '<%=formName%>';
		    var buttonName = '<%=TradePortalConstants.BUTTON_CLOSE_SELECTED_ITEMS%>';

		    <%--get array of rowkeys from the grid--%>
		    var rowKeys = getSelectedGridRowKeys("invoicesGrid");

		    <%--submit the form with rowkeys
		        becuase rowKeys is an array, the parameter name for
		        each will be checkbox + an iteration number -
		        i.e. checkbox0, checkbox1, checkbox2, etc --%>
		    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
		    <%}%>
	  }

	  function disputeInvoices(){
		  <%
		  //IR T36000014931 REL ER 8.1.0.4 BEGIN 
		  if ( disputeInvoiceRequireAuth && StringFunction.isNotBlank(certAuthURL)) {
		  %>
		  		openURL("<%=disputeInvoiceLink%>");
		  <%} else {
		  //IR T36000014931 REL ER 8.1.0.4 END
		  %>		
		  	var formName = '<%=formName%>';
		    var buttonName = '<%=TradePortalConstants.BUTTON_DISPUTEUNDISPUTE_SELECTEDITEMS%>';

		    <%--get array of rowkeys from the grid--%>
		    var rowKeys = getSelectedGridRowKeys("invoicesGrid");

		    <%--submit the form with rowkeys
		        becuase rowKeys is an array, the parameter name for
		        each will be checkbox + an iteration number -
		        i.e. checkbox0, checkbox1, checkbox2, etc --%>
		    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
		    <%}%>
	  }

	  function routeNotices(){
		  openRouteDialog('routeItemDialog', routeItemDialogTitle, 
					'matchNoticesGrid', 
					'<%=TradePortalConstants.INDICATOR_YES%>', 
					'<%=TradePortalConstants.FROM_PAYREMIT%>','') ;
	  }

	  function authoriseNotices(){
		  <%
		  //IR T36000014931 REL ER 8.1.0.4 BEGIN 
		  if ( matchRequireAuth && StringFunction.isNotBlank(certAuthURL)) {
		  %>
		  		openURL("<%=authorizeLink%>");
		  <%} else {
		  //IR T36000014931 REL ER 8.1.0.4 END
		  %>		
		  var formName = '<%=formName%>';
		    var buttonName = '<%=TradePortalConstants.BUTTON_AUTHORIZE%>';

		    <%--get array of rowkeys from the grid--%>
		    var rowKeys = getSelectedGridRowKeys("matchNoticesGrid");

		    <%--submit the form with rowkeys
		        becuase rowKeys is an array, the parameter name for
		        each will be checkbox + an iteration number -
		        i.e. checkbox0, checkbox1, checkbox2, etc --%>
		    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
		    <%
		     }
		   %>
	  }
<%--IR T36000014931 REL ER 8.1.0.4 BEGIN --%>
	  function openURL(URL){
		    if (isActive =='Y') {
		    	if (URL != '' && URL.indexOf("javascript:") == -1) {
			    	var cTime = (new Date()).getTime();
			        URL = URL + "&cTime=" + cTime;
			        URL = URL + "&prevPage=" + context;
		    	}
		    }
		    document.location.href  = URL;
			return false;
	}
<%--IR T36000014931 REL ER 8.1.0.4 END --%>

	

  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
      function(query, on, t360grid, t360popup){

<%
  if (TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES.equals(current2ndNav)){
%>
    query('#matchNoticesGridEdit').on("click", function() {
      var columns = t360grid.getColumnsForCustomization('matchNoticesGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "matchNoticesGrid", "MatchNoticesDataGrid", columns ];
      t360popup.open(
        'matchNoticesGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
    
    query('#matchNoticesRefresh').on("click", function() {
    	searchMatchNotices();
      });
<%
  }
  if (TradePortalConstants.NAV__INVOICES.equals(current2ndNav)){
%>
   require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	      function(query, on, t360grid, t360popup){
	    query('#invoicesRefresh').on("click", function() {
	    	searchInvoices();
	    });
    query('#invoicesGridEdit').on("click", function() {
      var columns = t360grid.getColumnsForCustomization('invoicesGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "invoicesGrid", "InvoicesDataGrid", columns ];
      t360popup.open(
        'invoicesGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
   });
<%
  }
  if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)){
%>
query('#inquiresRefresh').on("click", function() {
	searchInstrument();
});
    query('#rcvInstrGridEdit').on("click", function() {
      var columns = t360grid.getColumnsForCustomization('rcvInstrumentGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "rcvInstrumentGrid", "ARMInstrumentDataGrid", columns ];
      t360popup.open(
        'rcvInstrGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
<%
  }
%>

  });

  
  
 
  
  
</script>
<%
  if (TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES.equals(current2ndNav)){
%>
<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="matchNoticesGrid" />
</jsp:include>
<%
  }
  if (TradePortalConstants.NAV__INVOICES.equals(current2ndNav)){
%>
<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="invoicesGrid" />
</jsp:include>
<%
  }
  if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)){
%>
<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="rcvInstrumentGrid" />
</jsp:include>
<%
  }
%>


</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());

   session.setAttribute("receivableTransactions2ndNav", current2ndNav);
 //CR913-used in InvoiceDetail.jsp to differentiate rec and pay invoice navigation
   session.setAttribute("fromPayables", "N");
%>
