<%--
*******************************************************************************
  DirectDebit History List View Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%
   gridLayout = dgFac.createGridLayout("ddInquiryGrid", "DirectDebitHistoryDataGrid");
   String encryptedSelectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
%>
<%  //include a hidden form for new instrument submission
//this is used for all of the new instrument types available from the menu
Hashtable newInstrSecParms = new Hashtable();
newInstrSecParms.put("UserOid", userSession.getUserOid());
newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
%>
<form method="post" name="NewInstrumentForm1" action="<%=formMgr.getSubmitAction(response)%>">
<input type="hidden" name="bankBranch" />
<input type="hidden" name="transactionType"/>
<input type="hidden" name="instrumentType" />
<input type="hidden" name="copyInstrumentOid" />
<input type="hidden" name="mode" value="CREATE_NEW_INSTRUMENT" />
<input type="hidden" name="copyType" value="Instr" />

<%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
</form>
<%
Hashtable newTempSecParms = new Hashtable();
newTempSecParms.put("userOid", userSession.getUserOid());
newTempSecParms.put("securityRights", userSession.getSecurityRights());
newTempSecParms.put("clientBankOid", userSession.getClientBankOid());
newTempSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
newTempSecParms.put("ownerLevel", userSession.getOwnershipLevel());
%>
<form method="post" name="NewTemplateForm1" action="<%=formMgr.getSubmitAction(response)%>">
<input type="hidden" name="name" />
<input type="hidden" name="bankBranch" />
<input type="hidden" name="transactionType"/>
<input type="hidden" name="InstrumentType" />
<input type="hidden" name="copyInstrumentOid" />
<input type="hidden" name="mode"  />
<input type="hidden" name="CopyType" />
<input type="hidden" name="expressFlag"  />
<input type="hidden" name="fixedFlag" />
<input type="hidden" name="PaymentTemplGrp" />
<input type="hidden" name="validationState" />

<%= formMgr.getFormInstanceAsInputField("NewTemplateForm",newTempSecParms) %>
</form>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  var initSearchParms = "";
   var savedSort = savedSearchQueryData["sort"];
 var searchType = savedSearchQueryData["searchType"];
  var inactive = savedSearchQueryData["inactive"];
  var active = savedSearchQueryData["active"];
 var selectedOrg = savedSearchQueryData["selectedOrg"];
 function initSearch(){
	 require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"], 
		      function(dom, registry, t360grid) {
	  if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg){
		  selectedOrg = '<%=encryptedSelectedOrg%>';	
		  } 
		  else if(registry.byId('org')){
			 registry.byId("org").set('value',selectedOrg);
		  }
	 
	  if("" == inactive || null == inactive || "undefined" == inactive){
	    	if(registry.byId("InActive").get('checked'))
	    		inactive=true;
	  } 
	  else{
		  if(inactive=="true")
		  	registry.byId("InActive").set('checked',true);
		  
		  else
			  registry.byId("InActive").set('checked',false);
	  }
	  if("" == active || null == active || "undefined" == active){
	    		active=true;
	  }
	  else{
		  if(active=="true")
		  	registry.byId("Active").set('checked',true);
		  else
			registry.byId("Active").set('checked',false);
	  }
	    	if("" == savedSort || null == savedSort || "undefined" == savedSort)
	    		  savedSort = '0';
	       if("" == searchType || null == searchType || "undefined" == searchType){
	        	searchType = registry.byId('SearchType');
	          }
	       initSearchParms= "selectedOrg="+selectedOrg+"&inactive="+inactive+"&active="+active;
          if('A'==searchType){
      		   
	        	var  amountFrom = savedSearchQueryData["fromAmount"];
	        	var amountTo = savedSearchQueryData["toAmount"];
	        	var dateFrom = savedSearchQueryData["fromDate"];
	        	var dateTo = savedSearchQueryData["toDate"];
	        	 var  refNum = savedSearchQueryData["Reference"];	
	           	var CreditAcct = savedSearchQueryData["CreditAcct"];
	           	var instrumentId = savedSearchQueryData["InstrumentId"];
	           	if("" == instrumentId || null == instrumentId || "undefined" == instrumentId)
	           		instrumentId = checkString( dijit.byId("InstrumentIdAdv") );
	           	else
	           	 dom.byId("InstrumentIdAdv").value=instrumentId;
	           	if("" == CreditAcct || null == CreditAcct || "undefined" == CreditAcct)
	           		CreditAcct = checkString( dijit.byId("CreditAcctAdv") );
	           	else
	           		registry.byId("CreditAcctAdv").set('value',CreditAcct);
	           	 
	           	 if("" == refNum || null == refNum || "undefined" == refNum)
	           		 refNum = checkString( dijit.byId("RefNumAdv") );
	           	 else 
	                dom.byId("ReferenceAdv").value=refNum;
	        	 if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
	        		 amountFrom = registry.byId("AmountFrom").get('value');
	        	 else
	        		 registry.byId("AmountFrom").set('value',amountFrom);
	        	if("" == amountTo || null == amountTo || "undefined" == amountTo)
	        		 amountTo = registry.byId("AmountTo").get('value');
	        	else
	        		registry.byId("AmountTo").set('value',amountTo);
	        	if("" == dateFrom || null == dateFrom || "undefined" == dateFrom)
	        		 dateFrom = dojo.attr(dom.byId('FromDate'), 'value');
	        	else
	        		registry.byId("FromDate").set('displayedValue',dateFrom);
	        	
	        	if("" == dateTo || null == dateTo || "undefined" == dateTo)
	        		 dateTo = dojo.attr(dom.byId('ToDate'), 'value');
	        	else
	        		registry.byId("ToDate").set('displayedValue',dateTo);
	        	

	            dom.byId("advancePmtFilter").style.display='block';
	            dom.byId("basicPmtFilter").style.display='none';
	            
	   			var tempDatePattern = encodeURI('<%=datePattern%>');
       			initSearchParms=initSearchParms+"&dPattern="+tempDatePattern;
       			
     			initSearchParms = initSearchParms+"&Reference="+refNum+"&searchType="+searchType+"&InstrumentId="+instrumentId+"&CreditAcct="+CreditAcct+"&fromAmount="+amountFrom+"&toAmount="+amountTo+"&fromDate="+dateFrom+"&toDate="+dateTo;
     			
	        }
          else{
        	  var  refNum = savedSearchQueryData["Reference"];	
            	var CreditAcct = savedSearchQueryData["CreditAcct"];
            	var instrumentId = savedSearchQueryData["InstrumentId"];
            	if("" == instrumentId || null == instrumentId || "undefined" == instrumentId)
            		instrumentId = checkString( dijit.byId("InstrumentId") );
            	if("" == CreditAcct || null == CreditAcct || "undefined" == CreditAcct)
            		CreditAcct = checkString( dijit.byId("CreditAcct") );
            	else
            		registry.byId("CreditAcct").set('value',CreditAcct);
            	
            	 if("" == refNum || null == refNum || "undefined" == refNum)
            		 refNum = checkString( dijit.byId("RefNum") );
                  dom.byId("InstrumentId").value=instrumentId;
                 dom.byId("Reference").value=refNum;
                initSearchParms = initSearchParms+"&Reference="+refNum+"&searchType="+searchType+"&InstrumentId="+instrumentId+"&CreditAcct="+CreditAcct;
                
        	  dom.byId("advancePmtFilter").style.display='none';
              dom.byId("basicPmtFilter").style.display='block';
          }
          var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("DirectDebitHistoryDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
      	 var directDebitHistoryGridId = createDataGrid("ddInquiryGrid", viewName, gridLayout, initSearchParms,savedSort);      
  
  
  
 
  });
 }
  <%-- cquinton 3/26/2013 Rel PR ir#15207 do not pass orgList - dataview generates --%>
 <%--  var initSearchParms = "active=true&inactive=false&selectedOrg=<%=encryptedSelectedOrg%>"; --%>
  console.log ('initParams='+initSearchParms);
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
 
 
  function filterDDIHistoryOnEnter(fieldId){
		<%-- 
	  	* The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
	  	 --%>
	  	require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldId), "keypress", function(event) {
		        if (event && event.keyCode == 13) {
		        	dojo.stopEvent(event);
		        	searchDirectDebitHistory();
		        }
		    });
		});
	}
   function shuffleFilterPmtSearch(linkValue){
	   require(["dojo/dom","dijit/registry","dojo/domReady!"],
		        function(dom,registry){
      console.log("inside shuffleFilter(): "+linkValue);
  		if(linkValue=='Advance'){
        dom.byId("advancePmtFilter").style.display='block';
        dom.byId("basicPmtFilter").style.display='none';
        <%-- clearing Basic Filter --%>
        dom.byId("InstrumentId").value='';
        dom.byId("CreditAcct").value='';
        dom.byId("Reference").value='';
        document.getElementById("SearchType").value = "A";
      }
      if(linkValue=='Basic'){
        dom.byId("advancePmtFilter").style.display='none';
        dom.byId("basicPmtFilter").style.display='block';
        <%-- clearing Advance Filter --%>
        dom.byId("InstrumentIdAdv").value='';
        dom.byId("CreditAcctAdv").value='';
        dom.byId("ReferenceAdv").value='';
        dom.byId("AmountFrom").value='';
        dom.byId("AmountTo").value='';
        dom.byId("FromDate").value='';
        dom.byId("ToDate").value='';
        document.getElementById("SearchType").value = "S";
        <%-- this.resetDates(); --%>
      }
	   });
    }
  function searchDirectDebitHistory(){
  require(["dojo/dom","dijit/registry"],
        function(dom,registry){
    console.log('search invoked');
    var inact = dom.byId("InActive").checked;
    var act = dom.byId("Active").checked;
    if (<%=orgListDropDown%> == true)
      var org = registry.byId('org').attr('value');
    else
      var org = "<%=encryptedSelectedOrg%>";
      var instId = registry.byId('InstrumentId').get('value');
      var caId = registry.byId('CreditAcct').value;
      var refId = registry.byId('Reference').get('value');
	
      <%-- cquinton 3/26/2013 Rel PR ir#15207 do not pass orgList - dataview generates --%>
      var searchParams= "inactive="+inact+"&active="+act+"&selectedOrg="+org+
        "&searchType=S"+"&InstrumentId="+instId+"&CreditAcct="+caId+"&Reference="+refId;
    
      var tempDatePattern = encodeURI('<%=datePattern%>');
      searchParams=searchParams+"&dPattern="+tempDatePattern;
      console.log('searchParams='+searchParams);
      searchDataGrid("ddInquiryGrid", "DirectDebitHistoryDataView",
                     searchParams);
    });
	
  }
  function searchDirectDebitAdvHistory(){
	  require(["dojo/dom","dijit/registry","dojo/domReady!"],
	        function(dom,registry){
	    console.log('search invoked');
	    var inact = dom.byId("InActive").checked;
	    var act = dom.byId("Active").checked;
	    if (<%=orgListDropDown%> == true)
	      var org = registry.byId('org').attr('value');
	    else
	      var org = "<%=encryptedSelectedOrg%>";
	      var instId = registry.byId('InstrumentIdAdv').get('value');
	      var caId = registry.byId('CreditAcctAdv').value;
	      var refId = registry.byId('ReferenceAdv').get('value');
		
	      <%-- cquinton 3/26/2013 Rel PR ir#15207 do not pass orgList - dataview generates --%>
	      var searchParams= "inactive="+inact+"&active="+act+"&selectedOrg="+org+
	        "&searchType=A"+"&InstrumentId="+instId+"&CreditAcct="+caId+"&Reference="+refId;
	        searchParams=searchParams+"&fromDate="+dojo.attr(dom.byId('FromDate'), 'value')+
		  "&toDate="+dojo.attr(dom.byId('ToDate'), 'value');
	        console.log('AmountTo='+registry.byId('AmountTo').get('value'));
	        if (registry.byId('AmountFrom').get('value').toString() != "NaN")
	          searchParams=searchParams+"&fromAmount="+registry.byId('AmountFrom').get('value');
	        if (registry.byId('AmountTo').get('value').toString() != "NaN")
	          searchParams=searchParams+"&toAmount="+registry.byId('AmountTo').get('value');
	      
	      var tempDatePattern = encodeURI('<%=datePattern%>');
	      searchParams=searchParams+"&dPattern="+tempDatePattern;
	      console.log('searchParams='+searchParams);
	      searchDataGrid("ddInquiryGrid", "DirectDebitHistoryDataView",
	                     searchParams);
	    });
		
	  }
  
  function resetDates() {
    dijit.byId('FromDate').reset();
    dijit.byId('ToDate').reset();
  }
  
  function openCopySelectedDialogHelper(callBackFunction){
      require(["t360/dialog"], function(dialog) {
        var selectedRow = getSelectedGridRowKeys("ddInquiryGrid");
        if(selectedRow.length == 0){
      	  alert('<%=resMgr.getTextEscapedJS("CopySelected.PromptMessage4",TradePortalConstants.TEXT_BUNDLE) %>');
        }else if(selectedRow.length >1){
      	  alert('<%=resMgr.getTextEscapedJS("CopySelected.PromptMessage5",TradePortalConstants.TEXT_BUNDLE) %>');
        }else {
      	  dialog.open('copySelectedInstrument', '<%=resMgr.getTextEscapedJS("CopySelected.Title",TradePortalConstants.TEXT_BUNDLE) %>',
                    'copySelectedInstrumentDialog.jsp',
                    "instrumentType", instrType, <%-- parameters --%>
                    'select', callBackFunction);
        }	  
      });
  }

  function copySelectedToInstrument(bankBranchOid, copyType, templateName, templateGroup, flagExpressFixed){
  	var rowKeys ;
  	 require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
  		        function(registry, query, on, dialog ) {
  		      <%--get array of rowkeys from the grid--%>
  		      rowKeys = getSelectedGridRowKeys("ddInquiryGrid");
  	 });
  	if(copyType == 'I'){ 
  		if ( document.getElementsByName('NewInstrumentForm1').length > 0 ) {
  		    var theForm = document.getElementsByName('NewInstrumentForm1')[0];
  		    theForm.bankBranch.value=bankBranchOid;
  		    theForm.copyInstrumentOid.value=rowKeys;
  		    theForm.submit();
  		  }
  	}	
  	if(copyType == 'T'){ 
  		if ( document.getElementsByName('NewTemplateForm1').length > 0 ) {
  			var flag = flagExpressFixed.split('/');
  			var expressFlag = flag[0];
  			var fixedFlag = flag[1];
  			var theForm = document.getElementsByName('NewTemplateForm1')[0];
  		    theForm.name.value=templateName;
  		    theForm.mode.value="<%=TradePortalConstants.NEW_TEMPLATE%>";
  		    theForm.CopyType.value="<%=TradePortalConstants.FROM_INSTR%>";
  		    theForm.fixedFlag.value=fixedFlag;
  		    theForm.expressFlag.value=expressFlag;
  		    theForm.PaymentTemplGrp.value=templateGroup;
  		    theForm.copyInstrumentOid.value=rowKeys;
  		    theForm.validationState.value = "";
  		    theForm.submit();
  		  }
  	}
  }

  require(["dojo/_base/array", "dijit/registry","dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "t360/dialog", "dojo/ready", "dojo/domReady!"],
      function(baseArray, registry, query, on, t360grid, t360popup, dialog, ready){
    query('#ddInquiryGridEdit').on("click", function() {
      var columns = t360grid.getColumnsForCustomization('ddInquiryGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "ddInquiryGrid", "DirectDebitHistoryDataGrid", columns ];
      t360popup.open(
        'ddInquiryGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });

    ready(function(){
    	initSearch();
      var myGrid = registry.byId("ddInquiryGrid");

      myGrid.on("SelectionChanged", function(){
        var items = this.selection.getSelected();
        baseArray.forEach(items, function(item){
          instrType = item.i.InstrumentTypeCode;
        });
      });
    });	

  });
  
  function checkString(inquiryStr){
		if(null == inquiryStr)
			return "";
		else
			return inquiryStr.attr('value');
	}
  
</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="ddInquiryGrid" />
</jsp:include>
