<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Direct Debit Pending Transactions Tab

  Description:
    Contains HTML to create the Pending tab for the Direct Debit Pending Transactions page

  This is not a standalone JSP.  
*******************************************************************************
--%>

<%
        StringBuffer prependText = new StringBuffer();
        prependText.append(resMgr.getText("PendingTransactions.WorkFor", 
                                           TradePortalConstants.TEXT_BUNDLE));
        prependText.append(" ");

        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(userSecurityRights, 
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
           dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                               "ORGANIZATION_OID", "NAME", 
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));

           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
              dropdownOptions.append("<option value=\"");
              dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              dropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 dropdownOptions.append(" selected");
              }

              dropdownOptions.append(">");
              dropdownOptions.append(resMgr.getText(ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
              dropdownOptions.append("</option>");
           }
        }
        else
        {
           dropdownOptions.append("<option value=\"");
           dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
           dropdownOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
              dropdownOptions.append(" selected");
           }

           dropdownOptions.append(">");
           dropdownOptions.append(prependText.toString());
           dropdownOptions.append(userSession.getOrganizationName());
           dropdownOptions.append("</option>");
        }

        extraTags.append("onchange=\"location='");
        extraTags.append(formMgr.getLinkAsUrl("goToDirectDebitTransactionsHome", response));
        extraTags.append("&amp;current2ndNav=");
        extraTags.append(TradePortalConstants.NAV__PENDING_TRANSACTIONS);
        extraTags.append("&amp;workflow='+this.options[this.selectedIndex].value\"");

        String defaultValue = "";
        String defaultText  = "";

        if(!userSession.hasSavedUserSession())
         {
           defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
           defaultText = MY_WORK;
         }
%>

<%
         newLink.append(formMgr.getLinkAsUrl("goToDirectDebitTransactionsHome", response));
         newLink.append("&current2ndNav=");
         newLink.append(TradePortalConstants.NAV__PENDING_TRANSACTIONS);
         newLink.append("&workflow=");
         newLink.append(EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey()));    
%> 

  <%--cquinton 8/30/2012 add grid search header--%>
  <div class="gridSearch">
    <div class="searchHeader">
      <span class="searchHeaderCriteria">
<%
         out.println( widgetFactory.createSearchSelectField("Workflow", "PendingTransactions.Show", "", dropdownOptions.toString(), "onChange='searchPending();'"));
         dropdownOptions = Dropdown.createRcvMgmtTransStatusOptions(resMgr,selectedStatus);
         out.println(widgetFactory.createSearchSelectField("transStatusType", "DirectDebit.Status", "", dropdownOptions.toString(), "onChange='searchPending();'" ));
%>
		<%=widgetFactory.createHoverHelp("Workflow","InstPendingTrans.Show") %> 
		<%=widgetFactory.createHoverHelp("transStatusType","InstPendingTrans.Status") %> 

      </span>
      <span class="searchHeaderActions">
        <%--cquinton 10/8/2012 include gridShowCounts --%>
        <jsp:include page="/common/gridShowCount.jsp">
          <jsp:param name="gridId" value="ddPendGrid" />
        </jsp:include>

        <span id="ddPendRefresh" class="searchHeaderRefresh"></span>
        <%=widgetFactory.createHoverHelp("ddPendRefresh","RefreshImageLinkHoverText") %>
        <span id="ddPendGridEdit" class="searchHeaderEdit"></span>
        <%=widgetFactory.createHoverHelp("ddPendGridEdit","CustomizeListImageLinkHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>
  </div>

<%
  gridHtml = dgFac.createDataGrid("ddPendGrid","DirectDebitPendingDataGrid", null);
  gridLayout = dgFac.createGridLayout("ddPendGrid","DirectDebitPendingDataGrid"); 
%>
  <%=gridHtml %>