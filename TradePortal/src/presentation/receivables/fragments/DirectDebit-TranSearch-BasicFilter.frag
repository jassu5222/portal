<%--
*******************************************************************************
                    Direct Debit Transaction History Search Basic Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Basic Filter fields for the  
  Direct Debit Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>

<input type="hidden" name="NewSearch" value="Y">

  <div id="basicPmtFilter"  class="searchDetail lastSearchDetail">
    <span class="searchCriteria">

<%-- Jyoti 16-08-2012 3248 --%>
      <%= widgetFactory.createSearchTextField("InstrumentId","DirectDebitInquiry.criteria.InstrumentId",
				"30", "onKeydown='Javascript: filterDDIHistoryOnEnter(\"InstrumentId\");' class='char10'" ) %>
				
	  <%-- Kiran  05/13/2013  Rel8.2 IR-T36000014861  Start --%>
	  <%-- Added the Javascript code in order for enter key to work in Firefox--%>								
      <%=widgetFactory.createSearchSelectField( "CreditAcct", "DirectDebitInquiry.criteria.CreditAccountNo", "", options.toString(),  "onKeydown='Javascript: filterDDIHistoryOnEnter(\"CreditAcct\");'") %>
      <%-- Kiran  05/13/2013  Rel8.2 IR-T36000014861  End --%>
<%-- 3248 End --%>

      <%= widgetFactory.createSearchTextField("Reference","DirectDebitInquiry.criteria.ReferenceNo",
				"30", "onKeydown='Javascript: filterDDIHistoryOnEnter(\"Reference\");' class='char10'" )%>
    </span>
    <span class="searchActions">
      <button class="gridSearchButton" id="Search1" data-dojo-type="dijit.form.Button" type="button">
        <%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          searchDirectDebitHistory();return false; 
        </script>
      </button>
     <a id="searchTypeToggleBasic" class="searchTypeToggle" href="javascript:shuffleFilterPmtSearch('Advance');">
     <br>
     <%=resMgr.getText("ExistingDirectDebitSearch.Advanced", TradePortalConstants.TEXT_BUNDLE)%></a>	
      <%=widgetFactory.createHoverHelp("searchTypeToggleBasic","common.AdvancedSearchHypertextLink") %>
      </span>
     <%=widgetFactory.createHoverHelp("Search1","SearchHoverText") %>
    <div style="clear:both"></div>
  </div>

<script>
function disableOtherSearchFields(type) {
console.log('type='+type);
var ca = dijit.byId('CreditAcct');
var inst = dijit.byId('InstrumentId');
var re = dijit.byId('Reference');
if (type=='I'){
inst.attr('enabled','true');
ca.attr('disabled','true');
re.attr('disabled','true');
} 
if (type=='C'){
ca.attr('enabled','true');
inst.attr('disabled','true');
re.attr('disabled','true');
} 
if (type=='R'){
re.attr('enabled','true');
ca.attr('disabled','true');
inst.attr('disabled','true');
} 

}
</script>
