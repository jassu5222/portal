<%--
*********************************************************************************************
                  Direct Debit Transaction History Search - Search Parms include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %>
  tag.  It extracts search criteria from the request and builds a  where clause
  suitable for use in a listview (use InstrumentSearchListView as a model).
  As it is included with <%@ include %> rather than with the <jsp:include>
  directive, it is not a standalone servlet.  This JSP was created to allow
  a degree of reusability in multiple pages.

  This JSP assumes that searchType, dynamicWhereClause, searchListViewName and
  newSearchCriteria has previously been declared and assigned a
  value.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*********************************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%

  // This boolean and MediatorServices provides error handling for invalid
  // search criteria fields.
  boolean errorsFound = false;
  MediatorServices medService = new MediatorServices();
  medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());

  // Choose a character for the delimiter that will never appear in any data
  // The data must be base64ed first so that we can guarantee that the delimiter
  // will not appear
  String DELIMITER = "@";
 
     if (searchType.equals(TradePortalConstants.ADVANCED)) {

    	 instrumentId = request.getParameter("InstrumentId");

         if (instrumentId != null && !instrumentId.trim().equals("")) {
             instrumentId = instrumentId.trim().toUpperCase(); // SLUK012560747
          }
         
   	    creditAcct = request.getParameter("CreditAcct");
        
        refNum = request.getParameter("RefNum");
       
        amountFrom = request.getParameter("AmountFrom");
        if (amountFrom != null) {
           amountFrom = amountFrom.trim();
           
        } else {
          amountFrom = "";
        }
       
        amountTo = request.getParameter("AmountTo");
        if (amountTo != null) {
           amountTo = amountTo.trim();
          
        } else {
           amountTo = "";
        }
        
        dayFrom   = request.getParameter("DayFrom");
        monthFrom = request.getParameter("MonthFrom");
        yearFrom  = request.getParameter("YearFrom");

        dayTo   = request.getParameter("DayTo");
        monthTo = request.getParameter("MonthTo");
        yearTo  = request.getParameter("YearTo");
 
        } else {

        // Unlike the ADVANCED search, the SIMPLE search values are ORed together.
        // To handle this we'll add an 'AND (simple conditions ored together)'
        // clause.  This is only added if at least one of the search fields has a
        // value.

        instrumentId = request.getParameter("InstrumentId");

        if (instrumentId != null) {
          instrumentId = instrumentId.trim().toUpperCase();
        } else {
          instrumentId = "";
        }

        refNum = request.getParameter("RefNum");
        if (refNum != null) {
           refNum = refNum.trim().toUpperCase();
        } else {
           refNum = "";
        }
 
        creditAcct = request.getParameter("CreditAcct");
        if (creditAcct != null) {
           creditAcct = creditAcct.trim();
        } else {
           creditAcct = "";
        }
       
     }  // end if-else (oldSearchCriteria != null)
  

  Debug.debug("The search criteria is " + newSearchCriteria.toString());

%>
