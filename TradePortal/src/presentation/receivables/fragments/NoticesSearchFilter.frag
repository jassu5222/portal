<%--
*******************************************************************************
                  Instrument Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
  
  This page is currently included in ReceivablesManagement_Notices.frag. 
    
  Each originating page should contain the following code:
  
    <input type="hidden" name="NewSearch" value="Y">
  
  - this will allow a new search criteria to be created.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<script LANGUAGE="JavaScript">

         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }

function filterNoticeSearchOnEnter(fieldID){
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	searchMatchNotices();
	        }
	    });
	});	<%-- end require --%>
}<%-- end of filterNoticeSearchOnEnter() --%>
</script>

<input type="hidden" name="NewSearch" value="Y">

  <%
   StringBuilder sqlQuery = new StringBuilder();
   sqlQuery.append("select organization_oid, name");
   sqlQuery.append(" from corporate_org");
   sqlQuery.append(" where activation_status = ?");
   sqlQuery.append("start with organization_oid = ?");
   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
   sqlQuery.append(" order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));

	
   DocumentHandler hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false,TradePortalConstants.ACTIVE,userOrgOid);
   
   Vector _orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
   int _totalOrganizations = _orgListDoc.size();
   

        StringBuffer prependText = new StringBuffer();
		prependText.append(resMgr.getText("PendingTransactions.WorkFor", TradePortalConstants.TEXT_BUNDLE));
		prependText.append(" ");
        
        StringBuffer dropdownOptions              = new StringBuffer();
        String defaultValue = "";
        String defaultText  = "";
        
        if(!userSession.hasSavedUserSession())
	     {
	       defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
	       defaultText = MY_WORK;
	       
	       dropdownOptions.append("<option value=\"");
	       dropdownOptions.append(defaultValue);
	       dropdownOptions.append("\"");
	       
	       if (selectedWorkflow.equals(defaultText))
	       {
	          dropdownOptions.append(" selected");
	       }
	
	       dropdownOptions.append(">");
	       dropdownOptions.append(resMgr.getText( defaultText, TradePortalConstants.TEXT_BUNDLE));
	       dropdownOptions.append("</option>");
	     } 
	   
        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(userSecurityRights, 
                                     SecurityAccess.VIEW_CHILD_ORG_RECEIVABLES))
        {
           dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                               "ORGANIZATION_OID", "NAME", 
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));

           // Only display the 'All Work' option if the user's org has child organizations
           if (_totalOrganizations > 1)
           {
              dropdownOptions.append("<option value=\"");
              dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              dropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 dropdownOptions.append(" selected");
              }

              dropdownOptions.append(">");
              dropdownOptions.append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
              dropdownOptions.append("</option>");
           }
        }
        else
        {
           dropdownOptions.append("<option value=\"");
           dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
           dropdownOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
              dropdownOptions.append(" selected");
           }

           dropdownOptions.append(">");
           dropdownOptions.append(prependText.toString());
           dropdownOptions.append(userSession.getOrganizationName());
           dropdownOptions.append("</option>");
        }

        StringBuffer      extraTags                    = new StringBuffer();
        extraTags.append("onchange=\"location='");
        extraTags.append(formMgr.getLinkAsUrl("goToReceivableTransactionsHome", response));
        extraTags.append("&amp;current2ndNav=");
        extraTags.append(TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES);
        extraTags.append("&amp;workflow='+this.options[this.selectedIndex].value\"");

  %>

<%--cquinton 8/30/2012 add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">

<%
  StringBuffer statusCriteria = new StringBuffer();  
  statusCriteria.append(widgetFactory.createSearchSelectField("Workflow","ARMatchNotices.Show","","", dropdownOptions.toString(), false,"" ));
  statusCriteria.append("&nbsp;&nbsp;&nbsp;");
  statusCriteria.append(widgetFactory.createInlineLabel("", "ARMatchNotices.StatusField"));
  statusCriteria.append(widgetFactory.createCheckboxField("Paid", "ARMatchNotices.Paid", false, false, false, "", "", "none")); //not a form item
  statusCriteria.append(widgetFactory.createCheckboxField("Pending", "ARMatchNotices.Pending", true, false, false, "", "", "none")); //not a form item
  widgetFactory.wrapSearchItem(statusCriteria, "");//no extra classes
%>
      <%=statusCriteria.toString()%> 
    </span>
    <%=widgetFactory.createHoverHelp("Workflow","ReceivableManagementNoticeWorkFlow") %>
    <%=widgetFactory.createHoverHelp("Paid","ReceivableManagementPaidNotices") %>
    <%=widgetFactory.createHoverHelp("Pending","ReceivableManagementPendingNotices") %>
    <span class="searchHeaderActions">
      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="matchNoticesGrid" />
      </jsp:include>
	
	  <span id="matchNoticesRefresh" class="searchHeaderRefresh"></span>
      <span id="matchNoticesGridEdit" class="searchHeaderEdit"></span>
    </span>
    
    <%=widgetFactory.createHoverHelp("matchNoticesRefresh","RefreshImageLinkHoverText") %>
    <%=widgetFactory.createHoverHelp("matchNoticesGridEdit","CustomizeListImageLinkHoverText") %>
    <div style="clear:both;"></div>
  </div>

  <div class="searchDivider"></div>

  <div class="searchDetail">
    <span class="searchCriteria">

 
<%        
        dropdownOptions = new StringBuffer();
        String[] paymentSourceValues = {TradePortalConstants.PAYMENT_SOURCE_TYPE_BPY, 
                                        TradePortalConstants.PAYMENT_SOURCE_TYPE_CCD, 
                                        TradePortalConstants.PAYMENT_SOURCE_TYPE_DIR, 
                                        TradePortalConstants.PAYMENT_SOURCE_TYPE_HPA, 
                                        TradePortalConstants.PAYMENT_SOURCE_TYPE_LBX, 
                                        TradePortalConstants.PAYMENT_SOURCE_TYPE_RTG, 
                                        TradePortalConstants.PAYMENT_SOURCE_TYPE_SMT, 
                                        TradePortalConstants.PAYMENT_SOURCE_TYPE_SWF };
        dropdownOptions.append("<option value=''></option>");
        for (int i = 0; i < paymentSourceValues.length; i++) {
           dropdownOptions.append("<option value='");
           dropdownOptions.append(paymentSourceValues[i]);
           dropdownOptions.append("' ");
           if (paymentSourceValues[i].equals(paymentSource)) {
              dropdownOptions.append(" selected ");
           }
           dropdownOptions.append(">");
           dropdownOptions.append(ReferenceDataManager.getRefDataMgr().getDescr(
                                     TradePortalConstants.PAYMENT_SOURCE_TYPE, 
                                     paymentSourceValues[i], userLocale));
           dropdownOptions.append("</option>");
        }           
%>

      <%=widgetFactory.createSearchTextField("PaymentReference", "ARMatchNotices.PaymentReferenceField", "", "onKeydown='Javascript: filterNoticeSearchOnEnter( \"PaymentReference\");' 20")%>
      <%=widgetFactory.createSearchTextField("BuyerName", "ARMatchNotices.BuyerNameField", "", "onKeydown='Javascript: filterNoticeSearchOnEnter( \"BuyerName\");' 20") %>
    </span>
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchMatchNotices"><%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchMatchNotices();</script>
      </button>
    </span>
    <%=widgetFactory.createHoverHelp("searchMatchNotices","SearchHoverText") %>
    <div style="clear:both"></div>
  </div>
  <div class="searchDetail">
    <span class="searchCriteria">
      <%=widgetFactory.createSearchTextField("BuyerId", "ARMatchNotices.BuyerId", "", "onKeydown='Javascript: filterNoticeSearchOnEnter( \"BuyerId\");' 20") %>
      <%=widgetFactory.createSearchSelectField("PaymentSource", "ARMatchNotices.PaymentSourceField", "", dropdownOptions.toString()) %>
    </span>
    <%=widgetFactory.createHoverHelp("PaymentSource","ReceivableManagementPaymentSource") %>
    <div style="clear:both"></div>
  </div>
</div>
