<%--
**********************************************************************************
                        Receivables Instruments Tab

  Description:
    Build the where clause to be included in the Notices tab for the Receivables 
    Management Home page (ReceivablesManagement_Notices.frag)

    This is not a standalone JSP.  It MUST be included in a jsp page.
*******************************************************************************
--%> 

<%--
************************************************************************************
   Based on the workflow dropdown, build the where clause to include pay remit at
   various levels of the organization.
*************************************************************************************
--%>

<%
	final String ALL_WORK = "common.allWork";
	final String MY_WORK = "common.myWork";
      String selectedWorkflow = request.getParameter("workflow");
      
      
      String userDefaultWipView           = userSession.getDefaultWipView();

      selectedWorkflow = request.getParameter("workflow");

      if (selectedWorkflow == null)
      {
         selectedWorkflow = (String) session.getAttribute("workflow"); 

         if (selectedWorkflow == null)
         {
            if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
            {
               selectedWorkflow = userOrgOid;
            }
            else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
            {
               selectedWorkflow = ALL_WORK;
            }
            else
            {
               selectedWorkflow = MY_WORK;
            }
            selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
         }
      }

      session.setAttribute("selectedWorkflow", selectedWorkflow);

      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());      
	
     
%>

<%-- 
**************************************************************************************
   Based on the statusType dropdown, build the where clause to include one or more
   statuses.
**************************************************************************************/
--%>

<%
  String userLocale = userSession.getUserLocale();

%>
<%

   // W Zhu 1/6/2009 PHUI120454412 use pending and paid status properly
   final String  STATUS_PENDING  = resMgr.getText("common.StatusPending",TradePortalConstants.TEXT_BUNDLE);   
   final String  STATUS_PAID     = resMgr.getText("common.StatusPaid",TradePortalConstants.TEXT_BUNDLE);   
                                                                   
       String selectedStatus = request.getParameter("transStatusType");

      if (selectedStatus == null) // Try to get from session variable
      {
         selectedStatus = (String) session.getAttribute("noticesTransStatusType");
      }

      if (selectedStatus == null || selectedStatus.equals("")) // default is pending
      {
          selectedStatus = TradePortalConstants.STATUS_PENDING;
      }

       session.setAttribute("noticesTransStatusType", selectedStatus);

%>



<%       
/*
 ***********************************************************************************
 *
 *  Based on the other search criteria, build the where clause.
 *  .
 ************************************************************************************
*/

      
  String paymentReference = null;
  String buyerID = null;
  String buyerName = null;
  String paymentSource = null;

  
  String newSearchTrans = request.getParameter("NewSearch");
  String DELIMITER = "/";
  if (newSearchTrans != null && newSearchTrans.equals(TradePortalConstants.INDICATOR_YES))
  {
     paymentReference = request.getParameter("PaymentReference");
     if (paymentReference == null) paymentReference = "";
     buyerID = request.getParameter("BuyerID");
     if (buyerID == null) buyerID = "";
     buyerName = request.getParameter("BuyerName");
     if (buyerName == null) buyerName = "";
     paymentSource = request.getParameter("PaymenSource");
     if (paymentSource == null) paymentSource = "";
    
    
  }
  
   
%>
