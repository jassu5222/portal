<script language="Javascript">

  function processPaymentMatchList(){
  <%--  find the checked rows in paymentInvoiceGrid and then pass it on to processCheckedRow --%>
  <%--  deselect all selected checkboxes/ radio --%>
   console.log('entered processPaymentMatchList');
    var container = document.getElementById("Tab1Content"); 
    var totalElements = container.getElementsByTagName("input");
    for(var i = 0; i < totalElements.length; i++ ){
      var current = totalElements[i];
      if(current.type == "checkbox"  && current.getAttribute("name") != "sAll"){
        if(current.checked == true){    
          <%--  this will update the MatchContext to the appropriate match status --%>
          processCheckedRow(current);
        }      
      }
    }
    selectAll(0);
    updateMatchListView(container);   
	console.log('exit processPaymentMatchList');
  }

  function updateMatchListView1(container){
   console.log('entered updateMatchListView');
    var rowList = getRows(container);
    <%--  Loop through all rows being displayed. --%>
    for(var i = 0; i < rowList.length; i++ ){
      var row = rowList[i];
      var inputs = row.getElementsByTagName("input");
      for(var j = 0; j < inputs.length; j++){
        if(inputs[j].getAttribute("name") == "checkbox"+i){
          updateMatchListRow(row, inputs[j].value);
        }
      }     
    }
    matchContext.updateTotals();
    console.log('exit updateMatchListView')
  }


 

  
  function updateMatchListRow(row, oid){
  <%--  fetch matchContext item for given oid and set status, manualProposed and amountMatched field-readonly or editable --%>			
	<%--  then update MatchAMount value --%>
  console.log('entered updateMatchListRow');
    var matchListItem = matchContext.getMatchListItem(oid);
    if(matchListItem != null){
      var statusElement = getScopedElementsByName(row, "span", "MatchingStatus")[0];
      statusElement.innerHTML = matchListItem.getMatchStatusDescription();
      
      <%--  Set the Manual/Proposed column appropriately --%>
      var manualProposed = getScopedElementsByName(row, "span", "ManualProposed")[0];
      manualProposed.innerHTML = matchListItem.getManualIndicatorDescription();
      
      if(matchListItem.isManualRecord()){
        var matchAmount = getScopedElementsByName(row, "span","MatchedAmount")[0];
        var td = matchAmount.parentNode;
        td.innerHTML = "";
        <%-- var newtd = createListViewInputTextItem(matchListItem, "MatchedAmount", row, "", matchListItem.getMatchAmount()); --%>
        <%-- td.innerHTML = newtd.innerHTML; --%>
        var span = createListViewInputTextItem(matchListItem, "MatchedAmount", row, "", matchListItem.getMatchAmount());
        td.appendChild(span);
        
        
        matchAmount = getScopedElementsByName(row, "span","MatchedAmount")[0];
      }
    } 
	console.log('exit updateMatchListRow');
  }

  <%--  Update the javascript matching object based on the checked items. --%>
  function processCheckedRow(inputElement){
  <%--  set MatchListItem to the current row and updateMatch --%>
  console.log('processCheckedRow')
    var matchListItem = matchContext.getMatchListItem(inputElement.value);
    matchListItem.updateMatch();
  }
  
  function updateNewMatchMatchAmount(input, matchListOid){
   console.log('entered updateNewMatchMatchAmount');
    var matchListItem = matchContext.getMatchListItem(matchListOid);
    var fmt = matchContext.getCurrencyFormatter();
    try{
      var fieldValue = fmt.toNumber(input.value);    
      matchListItem.setMatchAmount(fieldValue); 
      input.value = fmt.formatNumber(fieldValue);
      matchContext.updateTotals();
    }catch(exc){
      alert("Please enter a valid number.");
      return false;
    }
	console.log('entered updateNewMatchMatchAmount');
	return true;  
  }
  
</script>

  <%
    String remitInvoiceOid          = StringFunction.xssCharsToHtml(request.getParameter("pay_remit_invoice_oid"));
    //out.println("payRemitInvoiceOid="+payRemitInvoiceOid);
    payRemitOid = payRemitOid == null ? StringFunction.xssCharsToHtml(request.getParameter("pay_remit_oid")) : payRemitOid;
    //out.println("payRemitOid="+payRemitOid);
    StringBuffer matchWhere = new StringBuffer();
    matchWhere.append(" and PAY_MATCH_RESULT.P_PAY_REMIT_OID = " + payRemitOid);    
    if(remitInvoiceOid != null){
    	remitInvoiceOid = EncryptDecrypt.decryptStringUsingTripleDes(remitInvoiceOid, userSession.getSecretKey());
      matchWhere.append(" and PAY_MATCH_RESULT.A_PAY_REMIT_INVOICE_OID = " + remitInvoiceOid);
    }
  %>  
  
  <%
  DataGridFactory dgFactory1 = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml1 = dgFactory1.createDataGrid("paymentInvoiceGrid","PaymentMatchInvoiceDataGrid",null);
%>

<%= gridHtml1 %> 

<%--cquinton 5/24/2013 add div to hold match/unmatch hidden fields--%>
<div id="matchUnmatchHiddenFields"></div>
