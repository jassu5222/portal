<% 
	String orgId = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
	String gridLayout1 = dgFactory1.createGridLayout("paymentInvoiceGrid","PaymentMatchInvoiceDataGrid");
	//out.println("gridLayout1="+gridLayout1);
	
	//Rel8.2 IR T36000014337 - 04/04/2013 - Check for ERP Setting
	CorporateOrganizationWebBean corporateOrg = null;
	String erpSetting = "";
	String proposedType = "";
	beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
	corporateOrg = (CorporateOrganizationWebBean) beanMgr.getBean("CorporateOrganization"); 
	corporateOrg.getById(orgId );
	erpSetting = corporateOrg.getAttribute("erp_transaction_rpt_reqd");
	//IR#T36000016617 AiA - When ERP is turned on, matchAmount should not be editable
	//otherwise, editable by default
	boolean editable = true;
	if(TradePortalConstants.INDICATOR_YES.equals(erpSetting)){
			editable = false;
	}
%>

<script type="text/javascript">
  var amountUpdated = false;
  var matchedInvoices = new Array();
  var unmatchedInvoices = new Array();
  <%-- ie8,7 do not support Array.indexOf - use this contains function instead! --%>
  Array.prototype.contains = function(k) {
    for(var p in this) {
      if(this[p] === k) {
        return true;
      }
    }
    return false;
  }
  String.prototype.contains = function(it) { return this.indexOf(it) != -1; };
  var initialLoad;
  var firstLoad='N';
  var oldMatchAmount = 0;
  var count = 0;
  var ucount = 0;
  var gridType;
  var selected;
  var divCreated = false;
  var unmatchdivCreated = false;
  var matchdivCreated = false;
  var writeToSameDiv = false;
  var uniquestore;
  var items;
  var payRemitInvoiceOid = "<%=StringFunction.xssCharsToHtml(remitInvoiceOid)%>";
  var lastAction = '';
  var newNamearr =  ["newMatchItem","newMatchItemInvoiceOutstandingAmount","newMatchItemMatchAmount","newMatchItemStatus","newMatchItemPayRemInvOid","newMatchItemId","newMatchItemOTLUoid"];
  var Namearr =  ["matchItem","matchItemInvoiceOutstandingAmount","matchItemMatchAmount","matchItemStatus"];
  var xhrObj;
  var payRemInvoiceOid = "<%=payRemInvoiceOid%>";
  function matchFormatter(value){
	
	formatSelection();
	  if(value == "MMD" || value == "AMD" || value == "PMD"){
		  return "<%= resMgr.getText("PaymentMatchResponse.Matched", TradePortalConstants.TEXT_BUNDLE) %>";
	    } else {
	      return "<%= resMgr.getText("PaymentMatchResponse.Unmatched", TradePortalConstants.TEXT_BUNDLE) %>";
	    }
  }
  
  function matchExplanationFormatter(value) {
	  if (value == 'y' || value == "Y"){
		  return "Manual";
	  }
	  else {
		  return "Proposed";
  	 }
  } 

  
  var initSearchParms = "payRemitInvoiceOid=<%=remitInvoiceOid%>";
  
  if (payRemitInvoiceOid != "null"){
    
    initSearchParms= initSearchParms+"&payRemitOid=<%=payRemitOid%>";  	
  }
  else {
    initSearchParms="payRemitOid=<%=payRemitOid%>";
  }
  initSearchParms =initSearchParms+"&withoutRemitItemsInd=<%=withoutRemitItemsInd%>"; <%-- T36000018280 --%>
  
  	
  	
   require(["t360/cgi/QueryReadStore","dojo/on","dojo/store/Observable", "dojo/data/ObjectStore","dojox/grid/DataGrid", 
             "dijit/registry", "dojo/ready", "dojox/grid/DataGrid","dojo/store/Memory", "dojox/grid/_RadioSelector",
             "dojox/grid/_CheckBoxSelector"], 
			function(QueryReadStore,on, Observable, ObjectStore, DataGrid, registry, ready, DataGrid, Memory) {
			ready(function() {

				uniquestore = new QueryReadStore({
					target: "/portal/getViewData?vName=" + "<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentMatchInvoiceDataView",userSession.getSecretKey())%>" + "&" + initSearchParms});<%--Rel9.2 IR T36000032596 --%>
				uniquestore = new Observable(uniquestore);
				var dataStore = new ObjectStore({
					objectStore: uniquestore
				});
				firstLoad = 'Y';
				<%--//
				//AAlubala Rel8.2 CR741 12/27/2012 - Add ERP columns i.e Seller Details, Discount Amount, Payment and Over-Payment Amounts
				//to the structure/display
				//TODO: Add an ERP check flag to be used to conditionally display
				//the ERP related columns
				//--%>
				if (<%=multiplRemittancesExist%> == true){
					
				 var structure = [{type: "dojox.grid._RadioSelector"},
				                  [{name:"rowKey", field:"rowKey", hidden:"true"} ,
				                   {name:"newlyAdded", field:"newlyAdded", hidden:"true"},
								   {name:"OTLInvoiceUoid", field:"OTLInvoiceUoid", hidden:"true"},
				                   {name:"Invoice ID", field:"InvoiceID", 
				                	  fields:["InvoiceID","InvoiceID_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"125px"} ,
				                   {name:"Matched/Unmatched", field:"MatchingStatus",formatter:matchFormatter, width:"140px"} ,
				                   {name:"Due Date", field:"DueDate", width:"100px"} ,
				                   {name:"Amount", field:"Amount", width:"80px"} ,
				                   {name:"Outstanding Amount", field:"OutstandingAmount", width:"130px"} ,
				                   {name:"Match Amount", field:"MatchAmount", width:"80px", editable: <%=editable%>, widgetClass: dijit.form.CurrencyTextBox} ,
				                   <%if(TradePortalConstants.INDICATOR_YES.equals(erpSetting)){%>
				                   {name:"Payment Amount", field:"PayAmount", width:"100px"} ,
				                   {name:"Over-Payment Amount", field:"OverPayAmount", width:"100px"} ,
				                   {name:"Discount Amount", field:"DiscountAmount", width:"100px"} ,
				                   <%}%>
				                   {name:"Finance Status", field:"FinanceStatus", width:"100px"} ,				                   
				                   {name:"Finance %", field:"FinancePercentage", width:"80px"} ,
				                   {name:"Match Explanation", field:"ManualProposed",formatter:matchExplanationFormatter, width:"100px", editable: true}
				                   <%
				                   //jgadela - Rel 8.3 IR - T36000017601, removed this link in the Invoice dialog 
				                   //and added to access the invoice details and payment details from the matching page itself
				                   if(TradePortalConstants.INDICATOR_YES.equals(erpSetting)){%>,
				                   {name:"Seller Details", field:"SellerDetails", 
					                	  fields:["SellerDetails","SellerDetails_linkUrl","pay_match_result_oid","rowKey"], formatter:quickViewFormatter, width:"125px"}	
				                   <%} %>
   									<%-- Srinivasu_D IR#T36000039818 Rel9.3 - commented DETAILS link --%>
								<%--	<% if(!TradePortalConstants.INDICATOR_YES.equals(erpSetting)){%>,
				                    {name:"Seller Details", field:"SellerDetails", 
					                	  fields:["SellerDetails","SellerDetails_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"125px"}	
				                   <%} %> --%>
				                   ]];
				} else {
					
					var structure = [{type: "dojox.grid._CheckBoxSelector"},
					                  [{name:"rowKey", field:"rowKey", hidden:"true"} ,
					                   {name:"newlyAdded", field:"newlyAdded", hidden:"true"},
									   {name:"OTLInvoiceUoid", field:"OTLInvoiceUoid", hidden:"true"},
					                   {name:"Invoice ID", field:"InvoiceID", 
					                	  fields:["InvoiceID","InvoiceID_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"125px"},
					                   {name:"Matched/Unmatched", field:"MatchingStatus",formatter:matchFormatter, width:"140px"} ,
					                   {name:"Due Date", field:"DueDate", width:"100px"} ,
					                   {name:"Amount", field:"Amount", width:"80px"} ,
					                   {name:"Outstanding Amount", field:"OutstandingAmount", width:"130px"} ,
					                   {name:"Match Amount", field:"MatchAmount", width:"80px", editable: <%=editable%>, widgetClass: dijit.form.CurrencyTextBox} ,
					                   <%if(TradePortalConstants.INDICATOR_YES.equals(erpSetting)){%>
					                   {name:"Payment Amount", field:"PayAmount", width:"100px"} ,
					                   {name:"Over-Payment Amount", field:"OverPayAmount", width:"100px"} ,
					                   {name:"Discount Amount", field:"DiscountAmount", width:"100px"} ,
					                   <%}%>
					                   {name:"Finance Status", field:"FinanceStatus", width:"100px"} ,				                   
					                   {name:"Finance %", field:"FinancePercentage", width:"80px"} ,
					                   {name:"Match Explanation", field:"ManualProposed",formatter:matchExplanationFormatter, width:"100px", editable: true}
					                   <%
					                   //jgadela - Rel 8.3 IR - T36000017601, removed this link in the Invoice dialog 
				                       //and added to access the invoice details and payment details from the matching page itself
					                   if(TradePortalConstants.INDICATOR_YES.equals(erpSetting)){%>,
					                     {name:"Seller Details", field:"SellerDetails", 
					                	  fields:["SellerDetails","SellerDetails_linkUrl","pay_match_result_oid","rowKey"], formatter:quickViewFormatter, width:"125px"}
					                   <%}%>
									<%-- Srinivasu_D IR#T36000039818 Rel9.3 - commented DETAILS link --%>
									<%--	<%
					                    if(!TradePortalConstants.INDICATOR_YES.equals(erpSetting)){%>,
					                   {name:"Seller Details", field:"SellerDetails", 
						                	  fields:["SellerDetails","SellerDetails_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"125px"}	
					                   <%}%> --%>
					                   ]];
					}
				<%--  Create our grid! --%>
				var grid = new DataGrid({
					store: dataStore,
					structure: structure,
					escapeHTMLInData:false,
					columnReordering:true,
					noDataMessage: "<span class='dojoxGridNoData'>No data found.</span>",
					canEdit: function(inCell, inRowIndex){
						var _canEdit = this._canEdit;
						if (!_canEdit) { console.log('1. returning _canEdit='+_canEdit); return _canEdit; }
						if (typeof inCell == "undefined" || typeof inRowIndex == "undefined" ) 
						{ console.log ('2. didnt get arguments. returning canEdit='+_canEdit); return _canEdit; }
						if (inCell.field !== "MatchAmount") 
						{ console.log('3. inCell.field != MatchAmount. returning false'); return false; }
						var item = this.getItem(inRowIndex);
						var mpValue = this.store.getValue(item, "ManualProposed");
						console.log('mpValue='+mpValue);
						if (mpValue == "Y")
						{console.log('4. value = Manual. returning true'); return true;}
						else
						{console.log('4. value = Proposed. returning false'); return false;}					
						}
				}, "paymentInvoiceGrid");
				var sortInfo;
				if ( typeof sortInfo == 'undefined' ||
				           sortInfo == null ||
				           sortInfo == ''  ) {
				        <%-- when no sort info is specific,  --%>
				        <%--  do the default, which is to  --%>
				        <%--  sort on the first non-hidden column ascending --%>
				        var defaultSortIndex = 0; <%-- default it --%>
				        <%-- identify the first non-hidden column --%>
				        var gridColumns = this.getGridColumns(structure);
				        for (var i in gridColumns) {
				          var myHidden = gridColumns[i].hidden;
				          if (myHidden) {
				            <%-- do nothing --%>
				          }
				          else {
				            defaultSortIndex = parseInt(i); <%-- this is necessary becuase i is a string --%>
				              <%-- and for some reason setSortIndex really only works with numbers  --%>
				            break;
				          }
				        }
				        grid.setSortIndex(defaultSortIndex);
				      }
				      else {
				        <%-- if sortInfo is specified attempt it --%>
				        <%--  if sortInfo is invalid for any reason --%>
				        <%--  we just won't sort --%>
				        if ( isNaN(sortInfo) ) {
				          <%-- assume its a column id --%>
				          <%-- translate into numeric sortInfo --%>
				          var sortDirection = '';
				          var sortColId = sortInfo;
				          if ( sortInfo.indexOf('-')==0 ) {
				            sortColId = sortInfo.substring(1);
				            sortDirection = '-';
				          }
				          var sortColIdx = this.getColumnIndex( structure, sortColId);
				          if ( sortColIdx >= 0 ) {
				            sortInfo = sortDirection + (sortColIdx+1);
				            grid.setSortInfo(sortInfo);
				          }
				        }
				        else {
				          if (sortInfo=='0') { <%-- a safety net --%>
				            <%-- fudge to first column --%>
				            sortInfo='1';
				          }
				          grid.setSortInfo(sortInfo);
				        }
				      }
				
				grid.startup();		
				grid.set('autoHeight',true);
			grid = registry.byId("paymentRemittanceGrid");
			if (grid != undefined) {
				remittanceGridSize = grid.store._items.length;	
			}

			
			grid = registry.byId("paymentInvoiceGrid");
			
            dojo.connect(grid, 'onSelected', function (inRowIndex)
			{
            <%-- isAmountUpdated = false;	 --%>
			
    		grid.updateRow(inRowIndex);
			});
			dojo.connect(grid, 'onDeselected', function (inRowIndex)
			{
			isAmountUpdated = false;
			
    		grid.updateRow(inRowIndex);
			});
			
			dojo.connect(grid, 'onApplyCellEdit', function (inValue, inRowIndex, inFieldIndex)
					{
					isAmountUpdated=true;
					});
			
			
			dojo.connect(grid, 'onCellDblClick', function (e)
					{
					
					
					isAmountUpdated=true;
					var grid = registry.byId("paymentInvoiceGrid");	 
					oldMatchAmount = grid.store.getValue(grid.getItem(e.rowIndex), "MatchAmount");
		    		
					});
			
			registry.byId("PaymentMatchInvoiceList_Un-MatchAll").on("click", function() { unmatchSelectedInvoices();});
			registry.byId("PaymentMatchInvoiceList_SearchInvoice").on("click", function() {openInvoicesDialog();  });
			
			var hasErrorFound = "<%=hasError%>";
			var errorRemitOid = "<%=errRemOid%>";
			if(hasErrorFound == 'Y'){
				dialogPayDiscountDetail(errorRemitOid);
			}
			var auoPayOid = "<%=autoPayOid%>";
			var showPopup = "<%=showPopup%>";
			if(showPopup =='Y'){
				dialogPayDiscountDetail(auoPayOid);
			}
			console.log("payRemInvoiceOid:"+payRemInvoiceOid);
			<%-- if(payRemInvoiceOid.length>0){ --%>
			<%-- 	selectInvoiceRemit(payRemInvoiceOid); --%>
			<%-- } --%>
	      });
	    });                 
   
   var quickViewFormatter=function(columnValues, idx, level) {
	  
	   var gridLink="";
	   var colVal = "";
      if ( columnValues.length >= 2 ) {
		  var payRemitRowKey = ""+columnValues[2]+"";		
		  gridLink= "<a href=\"javascript:dialogPayDiscountDetail(\'"+payRemitRowKey+"\');\">DETAILS</a>"+""+" ";
      }
      else if ( columnValues.length == 1 ) {
        <%-- no url parameters, just return the display --%>
        gridLink = columnValues[0];
      }
	
      return gridLink;
 
  }
	function getFormattedValue(colVal){
		  require(["dojo/ready","dijit/registry","dojo/_base/array"], 
	    		  function(ready,registry,baseArray) {
	    	  var myGrid = registry.byId('paymentInvoiceGrid');
	    	  var myGridRows = myGrid.store.objectStore.data.length;
			  for (var k=0; k<myGridRows; k++) {
			var aRowItem = myGrid.store.objectStore.data[k];
			var rowKey = aRowItem.rowKey;
			
			if ( (rowKey == colVal[2])) {
	
				myGrid.selection.select(k);
			
				break;
				}
			}
		  });
	}

    function dialogPayDiscountDetail(payRemitRowKey){ 
	var showDialog = "<%=showPopup%>";
	var autoInvoiceID="";
	if(showDialog=='Y'){
	autoInvoiceID="<%=autoInvoiceID%>";
	}
	var flg = '0';
	 require(["dijit/registry"],
		  		    function(registry) {
			var nmGrid = registry.byId('paymentInvoiceGrid');
				var nmGridRows = nmGrid.store.objectStore.data.length;				
				for (var an=0; an<nmGridRows; an++) {
				var nmRowItem = nmGrid.store.objectStore.data[an];
				var rowId = nmRowItem.id;	
				if(rowId == payRemitRowKey){
					if(nmRowItem.MatchingStatus == "NMD" || nmRowItem.MatchingStatus == "PPD" || nmRowItem.MatchingStatus == "ANM"){
						flg = '1';
						alert('<%=resMgr.getText("PaymentMatchResponse.AlertInvoicesMatchCanPayDetails", 
							  TradePortalConstants.TEXT_BUNDLE)%>');
						return false;
					}
				}
			}
		});

    require(["t360/dialog"], function(dialog) {
	 if(flg == '0'){
      dialog.open('PayDiscountDetailDialog',"PAYMENT AND DISCOUNT DETAILS" ,
                  'PaymentDiscountDetailsDialog.jsp',
                  ['pay_match_result_oid','dialogId','autoInvID'],[payRemitRowKey,'PayDiscountDetailDialog',autoInvoiceID], <%-- parameters --%>
                  null, null);
	 }
    });
  }

   function getGridColumns (structure) {
	   return structure[1];
	   
   }
   
   function getColumnIndex(structure,columnId) {
	      var colIdx = -1;
	      var gridColumns = this.getGridColumns(structure);
	      for (var i = 0; i<gridColumns.length; i++) {
	        if ( columnId == gridColumns[i].field ) {
	          colIdx = parseInt(i);
	          break;
	        }
	      }
	      return colIdx;
	    }
  
  <%--cquinton 5/23/2013 Rel 8.2 ir#15597 add function to individually match an item.
      Arguments:
        item - payment match row
      Output:
        hidden fields for item added to form
        added to matchedInvoices array
  --%>
  
  <%--Nar IR T36000022111 01/24/2013 Rel 8.3 Add Begin
  overloaded method to add flag parameter to not submit the form if match button is click. submit the form only when coming back from invoice search page. --%> 
  function matchInvoice (item) {
	  matchInvoice(item, false); 
  }
  <%--Nar IR T36000022111 01/24/2013 Rel 8.3 Add END --%>
  
  function matchInvoice(item, shouldSave) {
  	
  	var numberOfCurrencyPlaces = <%=Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currency))%>;
  	if (numberOfCurrencyPlaces == null || numberOfCurrencyPlaces == '' )
  		numberOfCurrencyPlaces = 2;
  		
    lastAction = 'M';

    var matchAmount = item.MatchAmount;
    matchAmount = matchAmount.replace(/\,/g,"");
    matchAmount = Number(matchAmount);

    var str1 = document.getElementById('AmountUnmatchedHidden').value;
    var str2 = document.getElementById('AmountMatchedHidden').value;
    var unmatchedAmount = Number(str1);
    var matchedAmount = Number(str2);
    
    

    matchedAmount = matchedAmount + matchAmount;
    unmatchedAmount = unmatchedAmount - matchAmount;
    
    

    setNewAmount(matchedAmount.toFixed(numberOfCurrencyPlaces),unmatchedAmount.toFixed(numberOfCurrencyPlaces));

    document.getElementById('AmountUnmatchedHidden').value = unmatchedAmount.toFixed(numberOfCurrencyPlaces);
    document.getElementById('AmountMatchedHidden').value = matchedAmount;
    <%-- IValavala Rel 8.2 T36000018280 --%>
    console.log('IValavala debug #1' + item.rowKey + '		'+ item.MatchAmount+'		'+item.ManualProposed);	
    console.log('IValavala debug #2 Manual Proposed Value:' + item.ManualProposed);	
    console.log('IValavala debug #3 Manual matchedAmount:unmatchedAmount:' + matchedAmount + ':' + unmatchedAmount);	
	<%-- IValavala Rel 8.3 --%>
	var remitInd =  document.getElementById('withoutRemitItemInd').checked;
	if (remitInd == true && item.Status == 'PPD'){
	console.log('IValavala debug #4. Will attempt to match the Proposed one');
	  var rowKey = getSelectedGridRowKeys('paymentRemittanceGrid');
      var Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,MatchStatus(item.Status),rowKey,item.InvoiceID,item.OTLInvoiceUoid];
      
	   document.getElementById('tempPayRemInvOid').value = rowKey;
      addMatchDivToForm('hiddenMatchDiv',newNamearr,Valuearr);
	}
    else if (item.ManualProposed == 'Y' && item.FinancePercentage == ''){		
      var rowKey = getSelectedGridRowKeys('paymentRemittanceGrid');	console.log('IValavala debug #5. Will attempt to match the Proposed one');
      var Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,'MMD',rowKey,item.InvoiceID,item.OTLInvoiceUoid];
	  document.getElementById('tempPayRemInvOid').value = rowKey;
      
      addMatchDivToForm('hiddenMatchDiv',newNamearr,Valuearr);
    }
    else{	console.log('IValavala debug #6 Will attempt to match the Proposed one');
      var Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,MatchStatus(item.Status)];
      addDivToForm('hiddendiv',Namearr,Valuearr);
    }      	
    matchedInvoices.push(item.InvoiceID);
    <%--Nar IR T36000022111 01/24/2013 Rel 8.3 Add Begin --%>
    if (shouldSave == true) {
      <%-- jgadela - Rel 8.3 IR - T36000017601, added to save the list data, when the user navigates from the Invoice dialog selection --%>
      document.forms[0].submit();
    }
    <%--Nar IR T36000022111 01/24/2013 Rel 8.3 Add End --%>
  } 

  function updateRowStatus (rowKeyVal, curStatus) {
	      require(["dojo/ready","dijit/registry","dojo/_base/array"], 
	    		  function(ready,registry,baseArray) {
	    	  var grid = registry.byId('paymentInvoiceGrid');
	    	  var objStore = grid.store.objectStore;
	    	  var item = objStore.get(rowKeyVal);	    	  
	    	  item.MatchingStatus = curStatus;
	    	  
	    	  objStore.put(item);
	    	  grid.store.objectStore = objStore;
	    	  grid.sort();
          });
   }
   <%-- Srinivasu_D CR#997 Rel9.3 04/04/2015 - Added this make selection --%>
   function formatSelection() {
	
    require(["dijit/registry", "dojo/_base/array"], 
        function(registry,array) {
		<%-- start --%>
		
			var remitInd =  document.getElementById('withoutRemitItemInd').checked;
			var noRemitValue = <%=noRemittanceItems%>;
			
			var fmtGrid = registry.byId('paymentInvoiceGrid');
			var items = getSelectedGridItems('paymentInvoiceGrid');
			
			if(items.length==0){
			var fmtGridRows = fmtGrid.store.objectStore.data.length;	
			
			for (var k=0; k<fmtGridRows; k++) {
			var aRowItem = fmtGrid.store.objectStore.data[k];
			var matStatus = aRowItem.MatchingStatus;	
			if(firstLoad == 'Y'){
			if ( (matStatus == 'NMD' || matStatus == 'PPD' || matStatus == 'ANM') && document.getElementById('withoutRemitItemInd').checked == true) {			
				fmtGrid.selection.select(k);	
				firstLoad = 'X';
				break;
				}
			}	
		 }
		}	
		    
    });
  }
   <%-- Srinivasu_D CR#997 Rel9.3 04/04/2015 - End --%>
  
  function unmatchSelectedInvoices () {
    
    require(["dijit/registry", "dojo/_base/array"], 
        function(registry,array) {
      var items = getSelectedGridItems('paymentInvoiceGrid');
      var Namearr =  ["unmatchItem","unmatchItemInvoiceOutstandingAmount","unmatchItemMatchAmount","unmatchItemStatus"];
      var Valuearr;
	  var invoiceID = '';
      var numberOfCurrencyPlaces = <%=Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currency))%>;
  	if (numberOfCurrencyPlaces == null || numberOfCurrencyPlaces == '' )
  		numberOfCurrencyPlaces = 2;
  	  var type ='U';	
      var retunVal = verifyInvoiceStatus(type);
	  if(retunVal == 1){
		  return false;
	  }
      <%-- deleteDiv('hiddenUnMatchDiv'); --%>
      var data = registry.byId('paymentInvoiceGrid').store.objectStore.data;
      array.forEach(items, function(item){
        if(item.Status==undefined){
          item.Status='NMD';
        }
        
        if (item.MatchingStatus == 'AMD'||item.MatchingStatus == 'PMD' ||item.MatchingStatus  == 'MMD'){
          updateRowStatus(item.rowKey, unMatchStatus(item.Status));
		  invoiceID=item.InvoiceID;
          lastAction = 'U';
          var str1 = document.getElementById('AmountUnmatchedHidden').value;
          var str2 = document.getElementById('AmountMatchedHidden').value;
          var matchAmount = item.MatchAmount;
          matchAmount = matchAmount.replace(/\,/g,"");
          matchAmount = Number(matchAmount);
          
          var unmatchedAmount = Number(str1);
          var matchedAmount = Number(str2);
          unmatchedAmount = unmatchedAmount + matchAmount;
          
          matchedAmount = matchedAmount - matchAmount;
          
          setNewAmount(matchedAmount.toFixed(numberOfCurrencyPlaces),unmatchedAmount.toFixed(numberOfCurrencyPlaces));
          document.getElementById('AmountUnmatchedHidden').value = unmatchedAmount;
          document.getElementById('AmountMatchedHidden').value = matchedAmount;
          
          Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,unMatchStatus(item.Status)];
          
          
          <%--cquinton 5/16/2013 Rel 8.2 ir#15597 use contains for ie7,8--%>
          if (matchedInvoices.contains(item.InvoiceID)){
            <%--  delete the div containing theInvoiceId --%>
            if (item.ManualProposed == 'Y' && item.FinancePercentage == ''){
              deleteDiv('hiddenMatchDiv',item.rowKey);
            }
            else{
              deleteDiv('hiddenDiv',item.rowKey);          
            } 
            matchedInvoices.pop(item.InvoiceId);
          } else {
            addUnMatchDivToForm('hiddendiv',Namearr,Valuearr);
            unmatchedInvoices.push(item.InvoiceID);
          }
        }
		document.getElementById('autoMatchButton').value="UnMatch";
		document.getElementById('autoPayInd').value='Y';		
		document.getElementById('invoiceID').value = invoiceID;	
		
		<%-- RPasupulati submiting page on changing Match status IR T36000035079. --%>
      	document.forms[0].submit();
      });
    });

  }
  
  function preCheckMatchSelectedInvoices(){
	  var val = checkNumberOfMatchedInvoices();
	  
	  var type='M';
	  var rV = verifyInvoiceStatus(type);
	  if(rV == 0){
	  if(val == 1){
		  matchSelectedInvoices();
		  return false;
	  }else{
		  return false;
	  }
	 }
  }
  
  function verifyInvoiceStatus(type){
	
	 var rtVal = 0;
	 require(["dijit/registry", "dojo/_base/array"], 
     function(registry,array) {
	    var data = registry.byId('paymentInvoiceGrid').store.objectStore.data; 
	    
      var items = getSelectedGridItems('paymentInvoiceGrid');
	 
	  if(items.length>0 && data.length>0){
		   array.forEach(items, function(item){
			 
			 if(type == 'M'){
		     if(item.MatchingStatus == "MMD" || item.MatchingStatus == "AMD" || item.MatchingStatus == "PMD"){
			   rtVal = 1;
			   alert('<%=resMgr.getText("PaymentMatchResponse.AlertInvoicesAlreadyMatchStatus", 
							  TradePortalConstants.TEXT_BUNDLE)%>');
			    return false;
		     }
			 }else if(type == 'U'){	 
				<%--  if(item.MatchingStatus == "NMD"){ --%>
				if(item.MatchingStatus == "NMD" || item.MatchingStatus == "PPD" || item.MatchingStatus == "ANM"){
				rtVal = 1;
				alert('<%=resMgr.getText("PaymentMatchResponse.AlertInvoicesAlreadyUnMatchedStatus", 
							  TradePortalConstants.TEXT_BUNDLE)%>');
			    return false;
		     }
			 }
		   });
	  }
   });
   return rtVal;
  }

  function matchSelectedInvoices () {

    
    require(["dijit/registry", "dojo/_base/array"], 
        function(registry,array) {
	  var data = registry.byId('paymentInvoiceGrid').store.objectStore.data; 
	   
      var items = getSelectedGridItems('paymentInvoiceGrid');
	
      var Namearr =  ["matchItem","matchItemInvoiceOutstandingAmount","matchItemMatchAmount","matchItemStatus"];
      var Valuearr;

      
      
		var rmt = registry.byId('paymentRemittanceGrid');
		var rmtGrid;
		var rmtGridRows;
		if(rmt != undefined){
		 rmtGrid = rmt.store._items;
		 rmtGridRows = rmtGrid.length;
		}
		var payInvOid = '';
	  	var payRemKey = getSelectedGridRowKeys('paymentRemittanceGrid');		 
		<%--  var payRemItems = getSelectedGridItems('paymentRemittanceGrid'); --%>
		 var remitInd =  document.getElementById('withoutRemitItemInd').checked;
		 document.getElementById('remitInd').value = document.getElementById('withoutRemitItemInd').checked;
		 var remitAmount = 0;
		 var outstandAmt = 0;
		 var invoiceID = '';
		 var otlInvOid = '';
		var rowKeys = [];
			var rowKeyIdx = 0;
			 array.forEach(rmtGrid, function(selectedItem){
              if (selectedItem !== null) {
                rowKeys[rowKeyIdx] = rmt.store.getValue(selectedItem,'id'); 			
				 if(rowKeys[rowKeyIdx] == payRemKey){
				   remitAmount = rmt.store.getValue(selectedItem,'Amount');
				   
				  }else{
	 				  rowKeyIdx++;
				 }
              }
            });
			
		    array.forEach(items, function(item){
				outstandAmt = item.Amount;
				invoiceID = item.InvoiceID;
				payInvOid = item.id;
				otlInvOid = item.OTLInvoiceUoid;
			});
		
		if(remitAmount !='' && outstandAmt != ''){
			
		<%-- 	createAutoPayDetails(remitAmount,outstandAmt,invoiceID,payRemKey,payInvOid); --%>
		}else if(remitInd == true){			
			remitAmount = "<%=unmatchedAmount %>";
			
		<%-- 	createAutoPayDetails(remitAmount,outstandAmt,invoiceID,"",payInvOid); --%>
		}	console.log("otlInvOid:"+otlInvOid);
		document.getElementById('autoPayInd').value='Y';		
		document.getElementById('invoiceID').value=invoiceID;
		document.getElementById('applied_amount').value=outstandAmt;
		document.getElementById('remitAmount').value=remitAmount;
		document.getElementById('autoMatchButton').value="Match";
		document.getElementById('payInvOid').value=payInvOid;
		document.getElementById('OTLInvoiceUoid').value=otlInvOid;
      array.forEach(items, function(item){
        
        
        
        if (unmatchedInvoices.contains(item.InvoiceID) && registry.byId('paymentRemittanceGrid') != undefined){
          deleteDiv('hiddenUnMatchDiv',item.rowKey);
          unmatchedInvoices.pop();
        } else {
          deleteDiv('hiddenUnMatchDiv',item.rowKey);
        }
		  
        if(item.Status==undefined){
          item.Status='NMD';
        }

        
        if (item.MatchingStatus == 'ANM' ||item.MatchingStatus  == 'NMD' || item.MatchingStatus == 'PPD'){

          updateRowStatus(item.rowKey, MatchStatus(item.Status));
          matchInvoice( item );

        } 
        else if (item.MatchingStatus=='MMD' && isAmountUpdated) {
			  
	    	  
	    	    var matchAmount = item.MatchAmount;
		       
		        matchAmount = matchAmount.replace(/\,/g,"");
		        matchAmount = Number(matchAmount);
		        if(oldMatchAmount.contains(',') > -1){
		        	oldMatchAmount = oldMatchAmount.replace(/\,/g,"");
		        }
		        
		        oldMatchAmount = Number (oldMatchAmount);
		        
	    	  var difference =oldMatchAmount - matchAmount;
	    	  difference = Number(difference);
	    	 
	    	  if (difference != 0){
	    		   	var numberOfCurrencyPlaces = <%=Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currency))%>;
  	if (numberOfCurrencyPlaces == null || numberOfCurrencyPlaces == '' )
  		numberOfCurrencyPlaces = 2;
	    		 
	    		  Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,MatchStatus(item.Status)];
			    	
			    	var str1 = document.getElementById('AmountUnmatchedHidden').value; <%-- String("<%=unmatchedAmount%>"); --%>
			        var str2 = document.getElementById('AmountMatchedHidden').value; <%-- String("<%=matchedAmount%>"); --%>
			        var unmatchedAmount = Number(str1);
			    	var matchedAmount = Number(str2);
			    	
			    	
			    	matchedAmount = matchedAmount - difference;
			    	unmatchedAmount = unmatchedAmount + difference;
			    	
			    	
			    	setNewAmount(matchedAmount.toFixed(numberOfCurrencyPlaces),unmatchedAmount.toFixed(numberOfCurrencyPlaces));
			    	document.getElementById('AmountUnmatchedHidden').value = unmatchedAmount.toFixed(numberOfCurrencyPlaces);
			    	document.getElementById('AmountMatchedHidden').value = matchedAmount;
			    	deleteDiv('hiddenMatchDiv',item.rowKey);
			    	if (item.ManualProposed == 'Y' && item.FinancePercentage == ''){
						  var rowKey = getSelectedGridRowKeys('paymentRemittanceGrid');
						  Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,'MMD',rowKey];
						 
						  if (matchedInvoices.contains(item.InvoiceID)){
							 document.getElementById('tempPayRemInvOid').value = rowKey;
							  addMatchDivToForm('hiddenMatchDiv',newNamearr,Valuearr);
						  } else {
							  addDivToForm('hiddenDiv',Namearr,Valuearr);
						  }
						  
					  }else{
				      	  Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,MatchStatus(item.Status)];
				      	addDivToForm('hiddenDiv',Namearr,Valuearr);	      	
				      }
			    	matchedInvoices.push(item.InvoiceID);
	    	  }	      	 
	      }
		  document.forms[0].submit();
		});
		
	
	  });
  }
  function unMatchStatus (currentStatus){
	  <%-- Srinivasu_D IR#T36000040243 Rel9.3 06/11/2015 - status for AMD to return ANM --%>
	 	     if (currentStatus == "AMD") return "ANM";
	    else if (currentStatus == "ANM") return "AMD";
	    else if (currentStatus == "PPD") return "PPD";
	    else if (currentStatus == "PMD") return "PPD";
	    else if (currentStatus == "MMD") return "NMD";
	    else if (currentStatus == "NMD") return "NMD";
	   
	  
  }
   function MatchStatus (currentStatus){
	   <%-- Srinivasu_D IR#T36000040243 Rel9.3 06/11/2015 - status for ANM to return AMD --%>
	 	     if (currentStatus == "ANM") return "AMD";
	    else if (currentStatus == "AMD") return "ANM";
	    else if (currentStatus == "PMD") return "PMD";
	    else if (currentStatus == "PPD") return "PMD";
	    else if (currentStatus == "NMD") return "MMD";
	    else if (currentStatus == "MMD") return "MMD";
	  
  }
   
   
  function invoiceSelected(isCellUpdated) {
  
  require(["dijit/registry", "dojo/_base/array", "dojo/store/DataStore"], 
			function(registry, array, DataStore) {
			var gridItems = getSelectedGridItems('paymentInvoiceGrid');
			
	    	var Valuearr;
	    	array.forEach(gridItems, function(item){
	    	
	    	
	    		if (gridItems != null && gridItems.length > 1) {
	    			<%--  multiple invoices have been selected. write to same div --%>
	    			deleteDiv('hiddenDiv',item.rowKey);
	    			}
	      	if (item.ManualProposed == 'Y' && item.FinancePercentage == ''){
	      		var rowKey = getSelectedGridRowKeys('paymentRemittanceGrid');
	      		Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,'MMD',rowKey];
	      		addDivToForm('hiddendiv',newNamearr,Valuearr);
	      	} else {

	      		if ((item.Status == 'ANM' || item.Status == 'NMD' || item.Status == 'PPD')){
	      			
	      			var str1 = document.getElementById('AmountUnmatchedHidden').value; <%-- String("<%=unmatchedAmount%>"); --%>
			        
			        var matchAmount = item.MatchAmount;
			        matchAmount = matchAmount.replace(/\,/g,"");
			        matchAmount = Number(matchAmount);
			        
			    	var unmatchedAmount = Number(str1);
			    	unmatchedAmount = unmatchedAmount - matchAmount;
			    	
			    	document.getElementById('AmountUnmatchedHidden').value = unmatchedAmount;
	      			Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,unMatchStatus(item.Status)];	
	      			addDivToForm('hiddendiv',Namearr,Valuearr);	
	      		} else if (item.Status == 'MMD' && isCellUpdated == true) {
	      			
	      			isAmountUpdated = true;
					var str1 = document.getElementById('AmountUnmatchedHidden').value; <%-- String("<%=unmatchedAmount%>"); --%>
			        
			        var matchAmount = item.MatchAmount;
			        
			        matchAmount = matchAmount.replace(/\,/g,"");
			        matchAmount = Number(matchAmount);
			        
			        oldMatchAmount = oldMatchAmount.replace(/\,/g,"");
			        oldMatchAmount = Number (oldMatchAmount);
			        
			        matchAmount = matchAmount - oldMatchAmount;
			        
			    	var unmatchedAmount = Number(str1);
			    	unmatchedAmount = unmatchedAmount - matchAmount;
			    	
			    	document.getElementById('AmountUnmatchedHidden').value = unmatchedAmount;
	      			Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,item.Status];	
	      			addDivToForm('hiddendiv',Namearr,Valuearr);	
	      			
	      		}
	      		
	      	}
				
			});

		});
   
  }
  
  function refreshInvoices2(invoiceid) {
  var searchParams="payRemitOid=<%=payRemitOid%>&payRemitInvoiceOid="+invoiceid;
  
  searchDataGrid("paymentInvoiceGrid", "PaymentMatchInvoiceDataView",
                         searchParams);
  }
 
  function refreshInvoices(invoiceid) {
	  var searchParams="payRemitOid=<%=payRemitOid%>&payRemitInvoiceOid="+invoiceid;
	  initialLoad = invoiceid;
	  console.log('4    refreshInvoices()....searchparams ='+searchParams);

	  require(["dijit/registry", "dojo/dom"],
	  		    function(registry, dom) {
	     		    	    if(invoiceid!=''){
	     		    	    	registry.getEnclosingWidget(document.getElementById('withoutRemitItemInd')).set('checked', false);
	     		    	    }
	     		    	    else{
	     		    	    	registry.getEnclosingWidget(document.getElementById('withoutRemitItemInd')).set('checked', true);
	     		    	    }
		  });
	  var remitInd =  document.getElementById('withoutRemitItemInd').checked;	 
	  searchParams = searchParams + "&withoutRemitItemsInd="+remitInd;	 
	  require(["t360/cgi/QueryReadStore","dojo/on","dojo/store/Observable", "dojo/data/ObjectStore","dojox/grid/DataGrid", 
	             "dijit/registry", "dojo/ready", "dojox/grid/DataGrid","dojo/_base/array", "dojo/aspect","dojox/grid/_RadioSelector","dojox/grid/_CheckBoxSelector"], 
				function(QueryReadStore,on, Observable, ObjectStore, DataGrid, registry, ready, DataGrid,array,aspect) {
				ready(function() {
					var grid = registry.byId('paymentInvoiceGrid');
					uniquestore = new QueryReadStore({
						target: "/portal/getViewData?vName=" + "<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentMatchInvoiceDataView",userSession.getSecretKey())%>" + "&" + searchParams});<%--Rel9.2 IR T36000032596 --%>

					uniquestore = new Observable(uniquestore);
					var dataStore = new ObjectStore({
						objectStore: uniquestore
					});
					grid.store.close();
					grid.set('store',dataStore);
					grid.sort();
					grid.set('autoHeight',true);	
					aspect.after(grid, "_onFetchComplete", validatePayGrid);				
					
		      });
		    }); 		
		
	}

 <%-- Srinivasu_D CR#997 Rel9.3 04/04/2015 - Selecting a unmatched remittance item  --%>
  function validatePayGrid(){
		
		if(document.getElementById('tempRemitOid').value != null && document.getElementById('withoutRemitItemInd').checked == false){
			  require(["dijit/registry","dojo/_base/array"],
		  		    function(registry,array) {
			var rmt = registry.byId('paymentRemittanceGrid');
			var rmtGrid = rmt.store._items;
			var rmtGridRows = rmtGrid.length;
			
			if(rmtGridRows>0){			
			var ky = document.getElementById('tempRemitOid').value;
			if(ky != undefined){
			var rowKeys = [];
			var rowKeyIdx = 0;
			 array.forEach(rmtGrid, function(selectedItem){
              if (selectedItem !== null) {
                rowKeys[rowKeyIdx] = rmt.store.getValue(selectedItem,'id');    			
				
				 if(rowKeys[rowKeyIdx] == ky){
				   rmt.selection.select(rowKeyIdx);
				   
				  }else{
	 				  rowKeyIdx++;
				 }
              }
            });
		   }
		   document.getElementById('tempRemitOid').value=''; <%-- reset back to blank; --%>
		 }			
						
		});
	 } 
	
	if(initialLoad != ''){
		updateInvoiceGridSelection();
	}else{		
		
		 require(["dijit/registry"],
		  		    function(registry) {
			var nmGrid = registry.byId('paymentInvoiceGrid');
				var nmGridRows = nmGrid.store.objectStore.data.length;				
				for (var an=0; an<nmGridRows; an++) {
				var nmRowItem = nmGrid.store.objectStore.data[an];
				var nmStatus = nmRowItem.MatchingStatus;				
				 if (nmStatus == 'NMD' || nmStatus == 'PPD' || nmStatus == 'ANM') {					
					nmGrid.selection.select(an);					
					break;
				 }
				}
				});
	}
}
<%--  Added this for selecting a invoice after determing the unmatched invoice --%>
function updateInvoiceGridSelection(){
	
	  require(["dijit/registry"],
		  		    function(registry) {		   
				var mmGrid = registry.byId('paymentInvoiceGrid');

				<%-- mmGrid.selection.clear(); --%>
				var mmGridRows = mmGrid.store.objectStore.data.length;
				var fnd = '';
								
				for (var am=0; am<mmGridRows; am++) {
				var mmRowItem = mmGrid.store.objectStore.data[am];
				var mmStatus = mmRowItem.MatchingStatus;				
				if (mmStatus == 'MMD' || mmStatus == 'AMD' || mmStatus == 'PMD') {	
					fnd = 'M';					
					mmGrid.selection.select(am);
					break;
				  }
				}				
				if(fnd == ''){
				var nmGrid = registry.byId('paymentInvoiceGrid');
				var nmGridRows = nmGrid.store.objectStore.data.length;				
				for (var an=0; an<nmGridRows; an++) {
				var nmRowItem = nmGrid.store.objectStore.data[an];
				var nmStatus = nmRowItem.MatchingStatus;				
				 if (nmStatus == 'NMD' || nmStatus == 'PPD' || nmStatus == 'ANM') {					
					nmGrid.selection.select(an);					
					break;
				 }
				}			
				}
			  });
			 
}
<%-- Srinivasu_D CR#997 Rel9.3 04/04/2015 - End	 --%>
 function updateItem(){
	
    var itemFound='' ;
    require(["dijit/registry","dojo/dom"], function(registry){
      if (xhrObj.readyState == 4 && xhrObj.status == 200) {	 
        itemFound = xhrObj.responseText;
		itemFound = itemFound.replace(/^\s+|\s+$/g, '');
		
      } 
    });
	
	<%-- initialLoad='N'; --%>
    if(itemFound != ''){  		
		document.getElementById('tempRemitOid').value = itemFound;		
		refreshInvoices(itemFound); 		
    }	
	else{
		
		  require(["dijit/registry"],
		  function(registry) {
			  var payGrid = registry.byId("paymentRemittanceGrid");
		      payGrid.selection.select(0);
			  itemFound =  getSelectedGridRowKeys('paymentRemittanceGrid');
			  
			  refreshInvoices(itemFound); 
		  });
	}
	
  }

 function findUnMatchedRemittance(){
	  require(["dijit/registry"],
				function(registry ) {
					
					<%-- if (!xhReq)					 --%>
					xhrObj = getXmlHttpRequest();				
					var req = "payRemitInvOids=<%=payRemitInvOids%>&payRemitOid=<%=payRemitOid%>";
					
					xhrObj.open("POST", '/portal/receivables/RemittanceItem.jsp?'+req, false);
					xhrObj.onreadystatechange = updateItem;
					xhrObj.send(null);
					
		 });
  }

	  <%-- IR T36000018280 start- called on change of withoutRemittanceInd checkbox --%>
  function refreshInvoicesWithoutInd() {
	  var items = getSelectedGridRowKeys('paymentRemittanceGrid');
	 
	  <%--  SureshL IR-T36000031139 12/23/2014 Start  --%>
	 if(document.getElementById('withoutRemitItemInd').checked == true){
		  document.getElementById('tempRemitOid').value = "";
		  require(["dijit/registry"],
		  		    function(registry) {
			  var payGrid = registry.byId("paymentRemittanceGrid");
		      payGrid.selection.clear();
			   refreshInvoices("");
		      items=null;
				var myGrid = dijit.byId('paymentInvoiceGrid');
				var myGridRows = myGrid.store.objectStore.data.length;				
				for (var k=0; k<myGridRows; k++) {
				var aRowItem = myGrid.store.objectStore.data[k];
				var matStatus = aRowItem.MatchingStatus;
				if ( matStatus == 'NMD' || matStatus == 'PPD' || matStatus == 'ANM') {
					
					myGrid.selection.select(k);
					<%-- myGrid.updateRow(k);					 --%>
					break;
					}
				}
			  });
		  
	  }
	  
	  
	 
if(document.getElementById('withoutRemitItemInd').checked == false){
	
	 require(["dijit/registry","dojo/_base/array"],
		  		    function(registry,array) {
	  					
	  var items = getSelectedGridItems('paymentInvoiceGrid');
      var Namearr =  ["unmatchItem","unmatchItemInvoiceOutstandingAmount","unmatchItemMatchAmount","unmatchItemStatus"];
      var Valuearr;
      var numberOfCurrencyPlaces = <%=Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currency))%>;
  	if (numberOfCurrencyPlaces == null || numberOfCurrencyPlaces == '' )
  		numberOfCurrencyPlaces = 2;
  		
      
    
      var data = registry.byId('paymentInvoiceGrid').store.objectStore.data;


      array.forEach(items, function(item){
        if(item.Status==undefined){
          item.Status='NMD';
        }
        
        if (item.MatchingStatus == 'AMD'||item.MatchingStatus == 'PMD' ||item.MatchingStatus  == 'MMD'){
          updateRowStatus(item.rowKey, unMatchStatus(item.Status));
          lastAction = 'U';
          var str1 = document.getElementById('AmountUnmatchedHidden').value;
          var str2 = document.getElementById('AmountMatchedHidden').value;
          var matchAmount = item.MatchAmount;
          matchAmount = matchAmount.replace(/\,/g,"");
          matchAmount = Number(matchAmount);
        
          var unmatchedAmount = Number(str1);
          var matchedAmount = Number(str2);
          unmatchedAmount = unmatchedAmount + matchAmount;
          
          matchedAmount = matchedAmount - matchAmount;
          
          setNewAmount(matchedAmount.toFixed(numberOfCurrencyPlaces),unmatchedAmount.toFixed(numberOfCurrencyPlaces));
          document.getElementById('AmountUnmatchedHidden').value = unmatchedAmount;
          document.getElementById('AmountMatchedHidden').value = matchedAmount;
          
          Valuearr = [item.rowKey,item.OutstandingAmount,item.MatchAmount,unMatchStatus(item.Status)];
          
          
          <%--cquinton 5/16/2013 Rel 8.2 ir#15597 use contains for ie7,8--%>
          if (matchedInvoices.contains(item.InvoiceID)){
            <%--  delete the div containing theInvoiceId --%>
            if (item.ManualProposed == 'Y' && item.FinancePercentage == ''){
              deleteDiv('hiddenMatchDiv',item.rowKey);
            }
            else{
              deleteDiv('hiddenDiv',item.rowKey);          
            } 
            matchedInvoices.pop(item.InvoiceId);
          } else {
            addUnMatchDivToForm('hiddendiv',Namearr,Valuearr);
            unmatchedInvoices.push(item.InvoiceID);
          }
        }
				
	}); 
	});
	
	
		updateManualUnCheck();

		
		 <%-- RPasupulati submiting page on changing Match status IR T36000035079. --%>
      <%-- 	document.forms[0].submit(); --%>

	}  
	<%-- Srinivasu_D CR#997 Rel9.3 04/04/2015 - Commented this out --%>
	 <%--  SureshL IR-T36000031139 12/23/2014 End --%>
	 <%--  
	  var searchParams="payRemitOid=<%=payRemitOid%>&payRemitInvoiceOid="+items;
  	  var remitInd =  document.getElementById('withoutRemitItemInd').checked;
	  searchParams = searchParams + "&withoutRemitItemsInd="+remitInd;
	  require(["t360/cgi/QueryReadStore","dojo/on","dojo/store/Observable", "dojo/data/ObjectStore","dojox/grid/DataGrid", 
					"dijit/registry", "dojo/ready", "dojox/grid/DataGrid","dojox/grid/_RadioSelector","dojox/grid/_CheckBoxSelector"], 
				function(QueryReadStore,on, Observable, ObjectStore, DataGrid, registry, ready, DataGrid) {
				ready(function() {
					var grid = registry.byId('paymentInvoiceGrid');
					uniquestore = new QueryReadStore({
							target: "/portal/getViewData?vName=" + "<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentMatchInvoiceDataView",userSession.getSecretKey())%>" + "&" + searchParams});
							
					uniquestore = new Observable(uniquestore);
					var dataStore = new ObjectStore({
							objectStore: uniquestore
							});
										
					grid.store.close();
						
					grid.set('store',dataStore);
					grid.sort();
					grid.set('autoHeight',true);
							});
				});  
				 --%>
	  }
	<%-- IR T36000018280 end --%>
	<%-- Srinivasu_D CR#997 Rel9.3 04/04/2015 - End --%>
	
	<%-- Srinivasu_D CR#997 Rel9.3 04/04/2015 - Added this to find unmatched invoice if Without Remit Ind is unchecked --%>
	function updateManualUnCheck(){
		
		if(document.getElementById('withoutRemitItemInd').checked == false){
			
			  require(["dijit/registry","dojo/_base/array"],
		  		    function(registry,array) {
			var mG = registry.byId('paymentRemittanceGrid');
			var myGGrid = mG.store._items;
			var myGGridRows = myGGrid.length;			
			if(myGGridRows>0){
			findUnMatchedRemittance();
			
			}			
						
		 });
	  }
	
	}
	<%-- Srinivasu_D CR#997 Rel9.3 04/04/2015 -End --%>

  function addSelectedInvoices(returnedInvoices) {
    
    require(["dijit/registry","dojo/store/Observable", "dojo/data/ObjectStore"], 
        function(registry, Observable, ObjectStore) {

      <%--cquinton 5/23/2013 Rel 8.2 ir#15597 start
          invoices when added, should be matched.  if already in the list, 
          show an error and do not readd to the list--%>
      var invoiceGrid = registry.byId('paymentInvoiceGrid');
      var invoicesAlreadyInList = "";
      var addCount = 0;
	  var invoiceID='';
	  var outstandAmt = 0;
      for (j=0; j < returnedInvoices.length; j++){
        

        <%-- now check if the invoice is already in the list. if so do not add it --%>
        var alreadyInList = false;
        var returnedInvoiceId = returnedInvoices[j][1];
        var mynumrows = invoiceGrid.store.objectStore.data.length;
        for (var k=0; k<mynumrows; k++) {
          var aRowItem = invoiceGrid.store.objectStore.data[k];
          var rowInvoiceId = aRowItem.InvoiceID;
          if ( returnedInvoiceId == rowInvoiceId ) {
            alreadyInList = true;
            break;
          }else{
			invoiceID = returnedInvoiceId;
			
		  }
        }

        if ( alreadyInList ) {
          if ( invoicesAlreadyInList.length > 0 ) {
            invoicesAlreadyInList += ", ";
          }
          invoicesAlreadyInList += returnedInvoiceId;
        }
        else {
          <%-- add to list --%>

          var item1 = {
            rowKey: returnedInvoices[j][0],
            MatchingStatus: '<%=TradePortalConstants.MATCHING_STATUS_MANUAL_MATCH %>',
            InvoiceID: returnedInvoices[j][1],
            InvoiceID_linkUrl: returnedInvoices[j][2],
            DueDate: returnedInvoices[j][3],
            Amount: returnedInvoices[j][5],
            OutstandingAmount: returnedInvoices[j][5],
            MatchAmount: returnedInvoices[j][5],
            FinanceStatus: returnedInvoices[j][9],
            FinancePercentage: "",
            ManualProposed: "Y",
            <%-- IR T36000016617 - Display Discount columns and DETAILS link --%>
            PayAmount: returnedInvoices[j][10],
            OverPayAmount: returnedInvoices[j][11],
            DiscAmount: returnedInvoices[j][12],
            SellerDetails: returnedInvoices[j][13],
            SellerDetails_linkUrl: returnedInvoices[j][14],
		    OTLInvoiceUoid: returnedInvoices[j][15]
          };
          
          uniquestore.put(item1);
		  outstandAmt =  item1.OutstandingAmount;
		  invoiceID = item1.InvoiceID;
	  <%-- PMitnala Rel 8.3 IR T36000022445 - Form submission moved out of the loop --%>
          matchInvoice(item1, false);
          addCount++;
        }
      }  
		document.getElementById('autoPayInd').value='Y';		
		document.getElementById('autoMatchButton').value="Search";
      <%-- PMitnala Rel 8.3 IR T36000022445 - Form submission moved out of the loop to save multiple invoices --%>
      document.forms[0].submit();

      if ( invoicesAlreadyInList.length > 0 ) {
        alert('<%=resMgr.getText("PaymentMatchResponse.AlertInvoicesAlreadyPresent", TradePortalConstants.TEXT_BUNDLE)%>' +
          invoicesAlreadyInList);
      }

      if ( addCount>0 ) {
        uniquestore = new Observable(uniquestore);
        var dataStore = new ObjectStore({
        objectStore: uniquestore });		
        invoiceGrid.set('store',dataStore);
        invoiceGrid.sort();
        invoiceGrid.set('autoHeight', true);
      }
      <%--cquinton 5/23/2013 Rel 8.2 ir#15597 end --%>
    }); 
  }
  
  function openInvoicesDialog() {
	    require(["dijit/registry","t360/dialog"], function(registry,dialog ) {
	    <%-- IValavala 6/18/2013 Rel 8.2 --%>
	      var remitInd =  document.getElementById('withoutRemitItemInd').checked;
	    if (remitInd == false){
	      var len = 1;
	      if (registry.byId('paymentRemittanceGrid') != undefined){
	        len = getSelectedGridRowKeys('paymentRemittanceGrid').length;   
	      }

	      
	      <%--  if the payment has remittances, then one must be selected before searching new invoices --%>
	      if (<%=multiplRemittancesExist%> == true){
	        if( len == 0 ){
	          alert('<%=resMgr.getText("PaymentMatchResponse.AlertRemittanceItemMustBeSelected", TradePortalConstants.TEXT_BUNDLE)%>');
	          return false;
	        }
	      }

	      <%-- cquinton 5/24/2013 Rel 8.2 ir#15597 if already have a match shouldn't open dialog --%>
	      <%--PMitnala Rel 8.3 IR T36000022445 - Passing remitInd parameter --%>
	      if ( checkNumberOfMatchedInvoices() == 1 ) {
	        dialog.open('invoiceSearchDialog', 'Invoice Search',
	          'invoiceSearchDialog.jsp',
	          ['userOrgOid','loginLocale','currency','remitInd'],['<%=orgId%>', '<%=loginLocale%>','<%=baseCurrencyCode%>',remitInd], 
	          'select', this.addSelectedInvoices);
	      }
	      }
	      else{
      	      <%--PMitnala Rel 8.3 IR T36000022445 - Passing remitInd parameter --%>     
	      dialog.open('invoiceSearchDialog', 'Invoice Search',
	          'invoiceSearchDialog.jsp',
	          ['userOrgOid','loginLocale','currency','remitInd'],['<%=orgId%>', '<%=loginLocale%>','<%=baseCurrencyCode%>',remitInd], 
	          'select', this.addSelectedInvoices);
	          
	      }
	    });
	  }
	  
  
  function deleteDiv(divid, rowKey) {
	  require(["dijit/registry"], function(registry){	
		  if (registry.byId('paymentRemittanceGrid') != undefined){  
			  var child = document.getElementById(divid);
		  }else{
			  var child = document.getElementById(divid+'-'+rowKey);
		  }
		   
		   if (child != undefined){
		  	var parent = child.parentNode;
		  	parent.removeChild(child);
		  	divCreated = false;
		  	count = 0;
		  }
		  });
  }
  
  function addDivToForm(divId,parmName, parmValue) {	
    require(["dojo/dom", "dijit/registry"], function(dom, registry){  
      if (registry.byId('paymentRemittanceGrid') != undefined){    
            if (!divCreated || isAmountUpdated){
              var hiddenDiv = document.createElement("div");
              hiddenDiv.setAttribute('id','hiddenDiv');
            } else {
              var hiddenDiv = document.getElementById('hiddenDiv');
            }
      }else{
          var hiddenDiv = document.createElement("div");
        hiddenDiv.setAttribute('id','hiddenDiv-'+parmValue[0]);
      }
      if (parmName instanceof Array && parmValue instanceof Array) {
        for(var i=0;i<parmName.length;i++){        
				
          if (parmName[i]!="" && parmValue[i]!=""){
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", parmName[i]+count);
            hiddenField.setAttribute("value", parmValue[i]);
            if (parmName[i]=='matchItem')
              hiddenField.setAttribute("id", parmValue[i]);
            hiddenDiv.appendChild(hiddenField);
          }  
        }
      }
      else{					
        if(parmName!="" && parmValue!=""){
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("name", parmName+count);
          hiddenField.setAttribute("value", parmValue);
          if (parmName[i]=='matchItem')
            hiddenField.setAttribute("id", parmValue[i]);
          hiddenDiv.appendChild(hiddenField); 
        }
      } 
      
      var hiddens = dom.byId("matchUnmatchHiddenFields");
      if ( hiddens ) {
        hiddens.appendChild(hiddenDiv);
      }
          
      divCreated = true;
      count= count+1;
    });
  }
  
  function addUnMatchDivToForm(divId,parmName, parmValue) {
    require(["dojo/dom", "dijit/registry"], function(dom, registry){  
      if (registry.byId('paymentRemittanceGrid') != undefined){
        if (!unmatchdivCreated){
          var hiddenDiv = document.createElement("div");
          hiddenDiv.setAttribute('id','hiddenUnMatchDiv');
        } else {
          var hiddenDiv = document.getElementById('hiddenUnMatchDiv');
        }
      }else{ <%-- Incase of RTGS --%>
        var hiddenDiv = document.createElement("div");
        hiddenDiv.setAttribute('id','hiddenUnMatchDiv-'+parmValue[0]);
      }
      if (parmName instanceof Array && parmValue instanceof Array) {
        for(var i=0;i<parmName.length;i++){        
        
          if (parmName[i]!="" && parmValue[i]!=""){
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", parmName[i]+ucount);
            hiddenField.setAttribute("value", parmValue[i]);
      <%--       if (parmName[i]=='unmatchItem') --%>
            hiddenField.setAttribute("id", parmValue[0]);
            hiddenDiv.appendChild(hiddenField);
          }  
        }
      }
      else{
        if(parmName!="" && parmValue!=""){
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("name", parmName+ucount);
          hiddenField.setAttribute("value", parmValue);
<%--           if (parmName[i]=='unmatchItem') --%>
          hiddenField.setAttribute("id", parmValue[0]);
          hiddenDiv.appendChild(hiddenField); 
        }
      } 
      
      var hiddens = dom.byId("matchUnmatchHiddenFields");
      if ( hiddens ) {
        hiddens.appendChild(hiddenDiv);
      }
      
      unmatchdivCreated = true;
      ucount= ucount+1;
      
    });
  }
      
  function addMatchDivToForm(divId,parmName, parmValue) {
    require(["dojo/dom", "dijit/registry"], function(dom, registry){  
      if (registry.byId('paymentRemittanceGrid') != undefined){
        if (!matchdivCreated || isAmountUpdated){
          var hiddenDiv = document.createElement("div");
          hiddenDiv.setAttribute('id','hiddenMatchDiv');
        } else {
          var hiddenDiv = dom.byId('hiddenMatchDiv');
        }
      }else{
        var hiddenDiv = document.createElement("div");
        hiddenDiv.setAttribute('id','hiddenMatchDiv-'+parmValue[0]);  
      }
      if (parmName instanceof Array && parmValue instanceof Array) {
        for(var i=0;i<parmName.length;i++){        

          if (parmName[i]!="" && parmValue[i]!=""){
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", parmName[i]+ucount);
			if(parmName[i]=='newMatchItemPayRemInvOid'){
			 hiddenField.setAttribute("value", document.getElementById('tempPayRemInvOid').value);
			}
			else{
            hiddenField.setAttribute("value", parmValue[i]);
			}
            if (parmName[i]=='newMatchItem')
              hiddenField.setAttribute("id", parmValue[i]);
            hiddenDiv.appendChild(hiddenField);
          }  
        }
      }
      else{	 
        if(parmName!="" && parmValue!=""){
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("name", parmName+ucount);
          hiddenField.setAttribute("value", parmValue);
          if (parmName[i]=='newMatchItem')
            hiddenField.setAttribute("id", parmValue[i]);
          hiddenDiv.appendChild(hiddenField); 
        }
      } 
      
      var hiddens = dom.byId("matchUnmatchHiddenFields");
      if ( hiddens ) {
        hiddens.appendChild(hiddenDiv);
      }
   
      matchdivCreated = true;
      ucount= ucount+1;
      
    });
  }
	    
   function selectInvoiceRemit(payRemitOid){
		
		<%-- if(document.getElementById('tempRemitOid').value != null && document.getElementById('withoutRemitItemInd').checked == false){ --%>
			  require(["dijit/registry","dojo/_base/array"],
		  		    function(registry,array) {
			var rmt = registry.byId('paymentRemittanceGrid');
			var rmtGrid = rmt.store._items;
			var rmtGridRows = rmtGrid.length;
			
			if(rmtGridRows>0){			
			<%-- var ky = document.getElementById('tempRemitOid').value; --%>
			<%-- if(ky != undefined){ --%>
			var rowKeys = [];
			var rowKeyIdx = 0;
			 array.forEach(rmtGrid, function(selectedItem){
              if (selectedItem !== null) {
                rowKeys[rowKeyIdx] = rmt.store.getValue(selectedItem,'id');    			
				
				 if(rowKeys[rowKeyIdx] == payRemitOid){
				   rmt.selection.select(rowKeyIdx);
				   
				  }else{
	 				  rowKeyIdx++;
				 }
              }
			 });
			}
            });
		   }
		  <%--  document.getElementById('tempRemitOid').value=''; //reset back to blank; --%>
		 			
						
		<%-- }); --%>
	<%--  }  --%>
	 
  var xhReq;
  function setNewAmount(matchamount, unmatchamount){
	    	
	  	  require(["dijit/registry"],
	  			    function(registry ) {
	  		  			var ccyCode = '<%=currency%>';
	  	  				if (!xhReq)
	  	  					xhReq = getXmlHttpRequest();
	  	  				var req = "MatchAmount=" + matchamount + "&UnMatchAmount="+unmatchamount+"&currencyCode="+ccyCode;
	  	  				xhReq.open("GET", '/portal/receivables/fragments/MatchUnmatchAmountFormatter.jsp?'+req, true);
	  	  					xhReq.onreadystatechange = updateAmount;
	  	  					xhReq.send(null);
	  	  				
	  	  			  });
  }

  function getXmlHttpRequest() {
	      var xmlHttpObj;
	      if (window.XMLHttpRequest) {
	        xmlHttpObj = new XMLHttpRequest();
	      }
	      else {
	        try {
	          xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
	        } catch (e) {
	          try {
	            xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
	          } catch (e) {
	            xmlHttpObj = false;
	          }
	        }
	      }
	      
	      return xmlHttpObj;
  }
	    
  function updateAmount(){
    var formattedAmt ;
    require(["dijit/registry","dojo/dom"], function(registry){
      if (xhReq.readyState == 4 && xhReq.status == 200) {	 
        formattedAmt = xhReq.responseText;	  					
      } 
    });
    var table = document.getElementById('paymentTable');
    var rowMatched = table.rows[2];
    var rowUnmatched = table.rows[4];
    if(formattedAmt != undefined){
      var amount = formattedAmt.split("*");
      var matchamount = amount[0];
      var unmatchamount = amount[1];
      rowMatched.cells[2].innerHTML = matchamount;
      rowUnmatched.cells[2].innerHTML = unmatchamount;
    }
  }

</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="paymentInvoiceGrid" />
</jsp:include>
