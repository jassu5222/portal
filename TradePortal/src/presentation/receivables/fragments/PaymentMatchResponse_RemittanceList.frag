<%
	//out.println("trying sql");
  StringBuffer remittanceItemWhere = new StringBuffer();
  remittanceItemWhere.append(" and pay_remit.pay_remit_oid = " + payRemitOid);
  String sql = "select PAY_REMIT_INVOICE.PAY_REMIT_INVOICE_OID, PAY_REMIT.PAY_REMIT_OID, PAY_REMIT_INVOICE.INVOICE_REFERENCE_ID as InvoiceID, "+
       " PAY_REMIT.CURRENCY_CODE Currency, PAY_REMIT_INVOICE.INVOICE_PAYMENT_AMOUNT as Amount, PAY_REMIT_INVOICE.INVOICE_DISCOUNT_AMOUNT as DiscountAmount" +
	" from PAY_REMIT_INVOICE, PAY_REMIT "+
	" where PAY_REMIT_INVOICE.P_PAY_REMIT_OID = PAY_REMIT.PAY_REMIT_OID and pay_remit.pay_remit_oid = ?  order by InvoiceID ";
   StringBuffer sb = new StringBuffer();

   String payRemitInvOid = "";
  DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{payRemitOid}); 
  //System.out.println("result:"+result);
  DocumentHandler tempDoc = null;
  if (result != null) {
  Vector remittances = result.getFragments("/ResultSetRecord");
	
  if (remittances.size() > 0) {
    multiplRemittancesExist = true;
    allowMultipleInvoices = false; // if there are no remittances in this payment
	
    //out.println("multiplRemittancesExist ="+ multiplRemittancesExist);
	 for(int i=0;i<remittances.size();i++){
		   tempDoc = (DocumentHandler) remittances.elementAt(i);		
		   payRemitInvOid = tempDoc.getAttribute("/PAY_REMIT_INVOICE_OID");
		   sb.append(payRemitInvOid+",");		
		 }
	if(sb.length()>0){
		sb.delete(sb.length()-1,sb.length());
	}
	payRemitInvOids = sb.toString();
  }
  }else{
	noRemittanceItems = true;  //Srinivasu_D CR#997 Rel9.3 03/18/2015 - Added to set if no remit available
	payRemitInvOids = ""; 
}

%>

  <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = dgFactory.createDataGrid("paymentRemittanceGrid","PaymentMatchRemittanceDataGrid",null);
  
%>
<%=gridHtml %>