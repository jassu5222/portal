<%
   String gridLayout = dgFactory.createGridLayout("PaymentMatchRemittanceDataGrid");   
%>

<script type="text/javascript">
  var remittanceGridSize;
  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  <%--set the initial search parms--%>
  var initSearchParms = "payRemitOid=<%=payRemitOid%>";
  console.log ('initParams='+initSearchParms);
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentMatchRemittanceDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var paymentRemittanceGrid = 
    createDataGrid("paymentRemittanceGrid", viewName,
                    gridLayout, initSearchParms);
   
   require(["dijit/registry", "dojo/ready", "dojox/grid/DataGrid","dojo/store/Memory","dojo/dom","dojo/domReady!"], 
			function(registry, ready, DataGrid, Memory,dom) {
			ready(function() {

			var grid = registry.byId("paymentRemittanceGrid");
			remittanceGridSize = grid.store._items.length;
			console.log('1    grid size=='+remittanceGridSize);
			grid.set('autoHeight',true);
			dojo.connect(grid, "onClick",
                    dojo.hitch(grid, refreshInvoiceGrid, dom.byId("paymentRemittanceGrid")));

	      });
	    });
    
   function refreshInvoiceGrid(node){ 
   			require(["dijit/registry"], 
			function(registry) {
			console.log('2   inside refreshInvoiceGrid()..');
                    var items = getSelectedGridRowKeys('paymentRemittanceGrid');
                    console.log("3   items:"+items);
                    refreshInvoices(items);                                 
	    });
	  }
  
  
</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="paymentRemittanceGrid" />
</jsp:include>
