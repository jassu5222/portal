<script>

  function openURL(URL){
    if (isActive =='Y') {
	    if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
        }
    }
    document.location.href  = URL;
    return false;
  }

  function checkNumberOfMatchedInvoices() {
    var rtrn = 0;
    require(["dijit/registry"], function(registry){
		  
		  <%--  If no remittance has been selected, then total Matched invoices --%>
		  <%--  should not be greater than total number of remittances --%>
		  <%--  If a remittance is selected, then only ine invoice can me matched to it. --%>
		  <%-- IR T36000018280 - check withoutRemitItemInd value --%>
		    var remitInd =  document.getElementById('withoutRemitItemInd').checked;
		  if (registry.byId('paymentRemittanceGrid') != undefined){
				 var remittances = registry.byId('paymentRemittanceGrid').store._items.length;
				  var matchedInvoicesCount = getNumberOfMatchedInvoices();
				  var selectedRemittance = getSelectedGridRowKeys('paymentRemittanceGrid');
				  
				  
				  if (lastAction == 'U' || (unmatchdivCreated == true && divCreated == false && matchdivCreated == false)) {
					  <%--  possible scenario of Unmatching, Only Buyer Update, Chabge of Unapply Flag --%>
					
					  rtrn = 1;
				  } else if (selectedRemittance.length == 0) {

					  
					  <%-- IValavala Rel 82. T36000018280 If withoutremititemsInd is true continue matching. --%>
					 
					  if (matchedInvoicesCount >= remittances && remitInd == false){
						  <%-- AiA --%>
						  if (isAmountUpdated == true){
						  alert('<%=resMgr.getText("PaymentMatchResponse.AlertMultipleMatchesToPayRemitInvoiceOid", 
								  TradePortalConstants.TEXT_BUNDLE)%>');
						  rtrn = 0;
						  } else {
							  rtrn = 1;  
						  }
					  } else {
						  rtrn = 1;
					  }
				  } else if (matchedInvoicesCount > 0){
					  <%-- IR T36000018280 - add withoutRemitItemInd condition --%>
					  if (isAmountUpdated == true && remitInd == false){
					  alert('<%=resMgr.getText("PaymentMatchResponse.AlertMultipleMatchesToPayRemitInvoiceOid", 
							  TradePortalConstants.TEXT_BUNDLE)%>');
					  rtrn = 0;
					  } else {
						rtrn = 1;  
					  }
				  } else {
					<%--  Change of Buyer, Change on unapply value, Update of match amount for already matched invoice --%>
					  rtrn = 1;
				  } 
			 } else{
				 rtrn = 1 ;
				 }
			  		  
		  });
	  return rtrn;
  }  
  
  function getNumberOfMatchedInvoices(){
    var matchedCount = 0;
    require(["dijit/registry","dojo/_base/array"], 
        function(registry,baseArray) {
      var items = registry.byId('paymentInvoiceGrid').store.objectStore.data;
			
		  		baseArray.forEach(items, function(item){
			    		
					  if(item.MatchingStatus == "MMD" || 
							  item.MatchingStatus == "AMD" || 
							  	item.MatchingStatus == "PMD"){
						  matchedCount = matchedCount + 1; 
					  }
				});
			    });
	  return matchedCount;
  }

  require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
      function(registry, on, common, ready) {
    ready(function() {
      <%--register event handlers--%>
      <%--todo: only execute when necessary--%>
      <%--todo: use event propagation to reduce registry associations--%>
      var myButton = registry.byId("SaveButton");
      if (myButton) {
        myButton.on("click", function() {
          common.setButtonPressed('Save','0');
          document.forms[0].submit();
        });
      }
      var myButton = registry.byId("SaveCloseButton");
      if (myButton) {
        myButton.on("click", function() {
          common.setButtonPressed('SaveAndClose','0');
          document.forms[0].submit();
        });
      }
      var myButton = registry.byId("RouteButton");
      if (myButton) {
        myButton.on("click", function() {
          openRouteDialog('routeItemDialog', routeItemDialogTitle,'',
            '<%=TradePortalConstants.INDICATOR_NO%>','<%=TradePortalConstants.FROM_PAYREMIT%>','<%=StringFunction.xssCharsToHtml(rowKey)%>',
            '<%=reference_id%>', '<%=buyerName%>', '<%=status%>') ;
          return false;			
        });
      }
<% 
//AiA IR#T36000018138 - 07/17/2013 - START
//Authorize when a pay remit is discounted and ERP is set on
//does not have ApproveDiscountButton, so, add a check for ERP setting
//if(isDiscounted) { 
	String orgIdx = userSession.getOwnerOrgOid();
	CorporateOrganizationWebBean corporatOrg = null;
	String erpS = "";
	beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
	corporatOrg = (CorporateOrganizationWebBean) beanMgr.getBean("CorporateOrganization"); 
	corporatOrg.getById(orgIdx );
	erpS = corporatOrg.getAttribute("erp_transaction_rpt_reqd");
	boolean isERPon = false;
	if(TradePortalConstants.INDICATOR_YES.equals(erpS)){
		isERPon = true;
	}
	
if(isDiscounted && !isERPon) { 
	  
//IR#T36000018138 - END
%>  

      var myButton = registry.byId("ApproveDiscountButton");
      if (myButton) {
        myButton.on("click", function() {
<% 
    if ( matchRequireAuth||approveDiscountRequireAuth ) {
%> 
          openURL("<%=StringFunction.xssCharsToHtml(approveDiscountLink)%>"); 
<% 
    }
    else {
%> 
      setButtonPressed('ApproveDiscountAuthorize', '0');
      document.forms[0].submit();					
<% 
    }
%> 
        });
      }
      var myButton = registry.byId("DeclineDiscountAuthorizeButton");
      if (myButton) {
        myButton.on("click", function() {
<% 
    if ( matchRequireAuth ) {
%> 
          openURL("<%=StringFunction.xssCharsToHtml(authorizeLink)%>"); 
<% 
    }
    else {
%> 
          setButtonPressed('Authorize', '0');
          document.forms[0].submit();
<% 
    }
%> 
        });
      }
<% 
  }
  else { //!isDiscounted [AiA: or is discounted and ERP is on]
%> 
 
      var myButton = registry.byId("AuthorizeButton");
      if (myButton) {
        myButton.on("click", function() {
<% 
    if ( matchRequireAuth ) {
%> 
          openURL("<%=StringFunction.xssCharsToHtml(authorizeLink)%>"); 
<% 
    }
    else {
%> 
          setButtonPressed('Authorize', '0');
          document.forms[0].submit();
<% 
    }
%> 
        });
      }
<% 
  }

  //cquinton 5/24/2013 Rel 8.2 ir#15597 use pageflow to get last page so works correctly
  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String returnAction = backPage.getLinkAction();
  String returnParms = backPage.getLinkParametersString();
  returnParms+="&returning=true";
%> 
      var myButton = registry.byId("CloseButton");
      if (myButton) {
        myButton.on("click", function() {
          openURL("<%=formMgr.getLinkAsUrl( returnAction, returnParms, response )%>");
        });
      }
    });
  });
</script>
