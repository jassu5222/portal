<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
     /***************************************************************************************************
      * Start of Documents Section 
      ************************************/  	       Debug.debug("****** Start of Documents Section ******");
 
  String urlParam;
%>

 
<%-- PHILC PUT IMAGES IN 3 COLUMN TABLE --%> 
<%

boolean displayedDocsGeneratedTable = false; //Vshah CR-452
try {

Debug.debug("PayRemitOid= " + payremitOid);

	query = new StringBuffer();
	//cquinton 8/25/2011 Rel 7.1.0 ppx240 - add hash
	//AiA IR#T36000017806 - Retrieve document name (doc_name) 
	//so as to be used as the link name
	query.append("select form_type, image_id, hash, source_type, doc_name ");
	query.append("from document_image ");
	query.append("where p_transaction_oid = ? ");
	dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), true, new Object[]{payremitOid});

	if((dbQuerydoc != null)
      && (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0)) 
    {		
		Debug.debug("**** dbQuerydoc --> " + dbQuerydoc.toString());
		listviewVector = dbQuerydoc.getFragments("/ResultSetRecord/");
		displayedDocsGeneratedTable = true; //Vshah CR-452
		
		//CR 742. Get CustomerID
		String customerID = "";
		StringBuffer queryCust = new StringBuffer();
		queryCust.append("SELECT PROPONIX_CUSTOMER_ID FROM corporate_org WHERE organization_oid = ? "); 
		DocumentHandler dbQuerydocCust = DatabaseQueryBean.getXmlResultSet(queryCust.toString(), true, new Object[]{customerOID});
		if((dbQuerydocCust != null)
      		&& (dbQuerydocCust.getFragments("/ResultSetRecord/").size() > 0)){
      		
				Vector listviewVectorCust = dbQuerydocCust.getFragments("/ResultSetRecord");
		   	    DocumentHandler resultRowCust = (DocumentHandler)listviewVectorCust.elementAt(0);
		   	    customerID  =  resultRowCust.getAttribute("/PROPONIX_CUSTOMER_ID");
   	    }
%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="ListText">
	  <span class="ControlLabel">
            <%=resMgr.getText("Payremit.DocumentsFromBank", 
                               TradePortalConstants.TEXT_BUNDLE)%>
	  </span>
	</p>
      </td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
<%
		//This loop is designed to scroll through the ResultSetRecords and display the Links.
		//Note that the links need parameters passed with them.
		Debug.debug("Walk the tree to display the text in the listview.");
		int liTotalDocs = listviewVector.size();
		int liDocCounter = liTotalDocs;
			
		for(int y=0; y < ((liTotalDocs+2)/3); y++) {
%>
  <tr> 
    <td width="40" nowrap>&nbsp;</td>
<%	
			for (int x=0; x < 3 ; x++) {
				if (liDocCounter==0) break;
%>
    <td nowrap width="250">
		<p class="ListText">
<%
					myDoc       = (DocumentHandler)listviewVector.elementAt((y*3)+x);
					attribute   = myDoc.getAttribute("/FORM_TYPE");
					//AiA IR#T36000017806 - Retrieve document name (doc_name) 
					//so as to be used as the link name					
					displayText = myDoc.getAttribute("/DOC_NAME");
					//IR#T36000017806 - END 
					attribute   = myDoc.getAttribute("/IMAGE_ID");
					encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( attribute, userSession.getSecretKey() );
					encryptVal3 = EncryptDecrypt.encryptStringUsingTripleDes( displayText, userSession.getSecretKey() );
					customerID = EncryptDecrypt.encryptStringUsingTripleDes( customerID, userSession.getSecretKey() );
                   //IValavala CR 742. get source_type
                    String sourceType = myDoc.getAttribute("/SOURCE_TYPE");
                    String en_sourceType = EncryptDecrypt.encryptStringUsingTripleDes(sourceType, userSession.getSecretKey());
                    String en_payremitOid = EncryptDecrypt.encryptStringUsingTripleDes(payremitOid, userSession.getSecretKey());
					urlParam    = "&image_id=" + encryptVal2 +  "&formType=" + encryptVal3 + "&tran_id=" + en_payremitOid + "&fromPayrem=" + "Y" + "&source_type=" + en_sourceType +
									"&customer_id=" + customerID;
					Debug.debug("urlParam from paymatch doc:" + urlParam);
%>			
				<a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParam)%>' target='_blank'><%= displayText %></a><span style="padding-left:20px"></a>

				 
		</p>
	</td>	
	<td></td>
<%
				liDocCounter--;
			}
			if (liDocCounter==0) {
				for (int z=liTotalDocs % 3; z > 0; z--) {
%>
    <td nowrap width="100">&nbsp;</td>
	<td></td>	
<%
				}			
			}
%>
	<td width="100%">&nbsp;</td>
  </tr>
<%
		}
%>
</table>  
<%
	}
} catch(Exception e) {
	Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Document Image table *******");
	e.printStackTrace();
}

%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">  
    <tr>
       <td>&nbsp;</td>
    </tr>
  </table>
