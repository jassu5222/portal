<%--
*******************************************************************************
                            Direct Debit - Transaction Page

  Description: **ToDo**
     
 

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%-- ********************* JavaScript for page begins here *********************  --%>



<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations", 
                                                                   TradePortalConstants.TEXT_BUNDLE);
   final String      ALL_WORK                     = resMgr.getText("PendingTransactions.AllWork", 
                                                                   TradePortalConstants.TEXT_BUNDLE);
   final String      MY_WORK                      = resMgr.getText("PendingTransactions.MyWork",  

   String			 instrumentId 		   = "";
   String			 refNum			       = "";
   String			 amountFrom     	   = "";
   String			 amountTo              = "";
   String 			 currency              = "";
   String 			 dayFrom 			   = "";
   String 			 monthFrom 			   = "";
   String 			 yearFrom 			   = "";
   String 			 dayTo 				   = "";
   String 			 monthTo 			   = "";
   String 			 yearTo 			   = "";
   String			 searchType		       = null;
   String 			 creditAcct		       = null;
   Vector            instrumentTypes       = null;
   String 			 link 			       = null;
   String 			 linkText 		       = "";   
 
   DocumentHandler   hierarchyDoc                 = null;
   StringBuffer      dropdownOptions              = new StringBuffer();
   int               totalOrganizations           = 0;
   StringBuffer      extraTags                    = new StringBuffer();
            
   String            userDefaultWipView           = null; 
   String            selectedWorkflow             = null;
   String            selectedStatus               = "";
   String            selectedOrg                  = null;  
   String 			 linkParms   				  = "";   
 
   userDefaultWipView = userSession.getDefaultWipView();
   loginLocale = userSession.getUserLocale();
   
   // Construct the Corporate Organization
   CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   
   // Store the user's oid and security rights in a secure hashtable for the form
   secureParms.put("instrumentType",InstrumentType.DIRECT_DEBIT_INSTRUCTION);
   secureParms.put("bankBranch",corpOrg.getAttribute("first_op_bank_org")); 
   
   //VShah/IAZ IR-SLUK012866351 02/02/10 Begin 
    secureParms.put("UserOid",userSession.getUserOid());
   secureParms.put("SecurityRights",userSecurityRights); 
   //VShah/IAZ IR-SLUK012866351 02/02/10 End
   secureParms.put("clientBankOid",userSession.getClientBankOid()); 
   secureParms.put("ownerOrg",userSession.getOwnerOrgOid());

   
   // This is the query used for populating the workflow drop down list; it retrieves
   // all active child organizations that belong to the user's current org.
   StringBuffer sqlQuery = new StringBuffer();
   sqlQuery.append("select organization_oid, name");
   sqlQuery.append(" from corporate_org");
   sqlQuery.append(" where activation_status = ?");
   sqlQuery.append(" start with organization_oid = ?");
   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
   sqlQuery.append(" order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));

 
   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, TradePortalConstants.ACTIVE, userOrgOid);
	  
   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
   totalOrganizations = orgListDoc.size();
   
   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName

   // Set the performance statistic object to have the current tab as its context
   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(currentTab);

   // ***********************************************************************
   // Data setup for the Pending Transaction tab
   // ***********************************************************************
   if (currentTab.equals(TradePortalConstants.RECEIVABLES_DIRECT_DEBIT_TAB))
   {
      // Based on the statusType dropdown, build the where clause to include one or more
      // statuses.
      Vector statuses = new Vector();
      int numStatuses = 0;

      selectedStatus = request.getParameter("transStatusType");
      if (selectedStatus == null) 
      {
         selectedStatus = (String) session.getAttribute("transStatusType");
      }

      if (TradePortalConstants.STATUS_READY.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE);
       }
      else if (TradePortalConstants.STATUS_PARTIAL.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED);
      }
      else if (TradePortalConstants.STATUS_AUTH_FAILED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED);
      }
      else if (TradePortalConstants.STATUS_STARTED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_STARTED);
      }
      else // default is ALL
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_STARTED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE);
         selectedStatus = TradePortalConstants.STATUS_ALL;
      }

      session.setAttribute("transStatusType", selectedStatus);

      // Build a where clause using the vector of statuses we just set up.

      // Based on the workflow dropdown, build the where clause to include transactions at
      // various levels of the organization.
      selectedWorkflow = request.getParameter("workflow");

      if (selectedWorkflow == null)
      {
         selectedWorkflow = (String) session.getAttribute("workflow");

         if (selectedWorkflow == null)
         {
            if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
            {
               selectedWorkflow = userOrgOid;
            }
            else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
            {
               selectedWorkflow = ALL_WORK;
            }
            else
            {
               selectedWorkflow = MY_WORK;
            }
            selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
         }
      }

      session.setAttribute("workflow", selectedWorkflow);

      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/pend_trans.htm",  resMgr, userSession);
      onLoad.append("document.DirectDebit-TransactionForm.Workflow.focus();");
%>

<%-- **************** JavaScript for Pending tab only *********************  --%>

<script language="JavaScript" type="text/javascript">

      
         <%--
         // This function is used to display a popup message confirming
         // that the user wishes to delete the selected items in the
         // listview.
         --%>
         function confirmDelete()
         {
           var   confirmMessage = "<%=resMgr.getText("PendingTransactions.PopupMessage", 
                                                     TradePortalConstants.TEXT_BUNDLE) %>";

            if (!confirm(confirmMessage)) 
             {
                formSubmitted = false;
                return false;
             }
           else
           {
              return true;
           }
        }

     

      </script>

<%
   } // End data setup for Pending tab

   else if (currentTab.equals(TradePortalConstants.DIRECT_DEBIT_TRANSACTION_HISTORY))
   {
       helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/cashManagement.htm",  resMgr, userSession);
       
       // Determine the organization to select in the dropdown
       selectedOrg = request.getParameter("historyOrg");
       if (selectedOrg == null)
       {
       	  selectedOrg = (String) session.getAttribute("historyOrg");
       	  if (selectedOrg == null)
          {
            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
          }
       }

   	  session.setAttribute("historyOrg", selectedOrg);
	  selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
       
   	  String newSearch = request.getParameter("NewSearch");
	  String newDropdownSearch = request.getParameter("NewDropdownSearch");
      Debug.debug("New Search is " + newSearch);
      Debug.debug("New Dropdown Search is " + newDropdownSearch);
   	  searchType   = request.getParameter("SearchType");
   		
   		if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
      	{
         	// Default search type for instrument status on the transactions history page is ACTIVE.
         	session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
     	}
   		
   		if (searchType == null) 
	    {
   			 searchType = TradePortalConstants.SIMPLE;
        	
      	}	
      
        
         if (searchType.equals(TradePortalConstants.ADVANCED))
      	{
         	linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
         	link       = formMgr.getLinkAsUrl("goToReceivablesManagement", linkParms, response);
         	linkText   = resMgr.getText("InstSearch.BasicSearch", TradePortalConstants.TEXT_BUNDLE);

         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "advanced", resMgr, userSession);
      	}
      	else
      	{
        	linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
         	link       = formMgr.getLinkAsUrl("goToReceivablesManagement", linkParms, response);
         	linkText   = resMgr.getText("InstSearch.AdvancedSearch", TradePortalConstants.TEXT_BUNDLE);

         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "basic", resMgr, userSession);
     	 }

	 // Determine the organizations to select for listview
     	 if (selectedOrg.equals(ALL_ORGANIZATIONS))
     	 {
        	 // Build a comma separated list of the orgs
		 DocumentHandler orgDoc = null;
        	 StringBuffer orgList = new StringBuffer();

	         for (int i = 0; i < totalOrganizations; i++)
	         {
	    	    orgDoc = (DocumentHandler) orgListDoc.get(i);
        		orgList.append(orgDoc.getAttribute("ORGANIZATION_OID"));
            	if (i < (totalOrganizations - 1) )
            		{
	             		 orgList.append(", ");
    	       		}
        	 }
         }
      		 
     	 // Based on the statusType dropdown, build the where clause to include one or more
     	 // instrument statuses.
     	 Vector statuses = new Vector();
     	 int numStatuses = 0;

      	selectedStatus = request.getParameter("instrStatusType");
        if (selectedStatus == null) 
     	 {
       		  selectedStatus = (String) session.getAttribute("instrStatusType");
     	 }

      	if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) )
      	{
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
      	}

      	else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) )
      	{
       		 statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
         	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
      	}

      	else // default is ALL (actually not all since DELeted instruments
           // never show up (handled by the SQL in the listview XML)
      	{
        	 selectedStatus = TradePortalConstants.STATUS_ALL;
      	}

     	session.setAttribute("instrStatusType", selectedStatus);
     	
%>

<%@ include file="/receivables/fragments/DirectDebit-TranSearch-SearchParms.frag" %>

<%-- JavaScript for Instrument History tab to enable form submission by enter.
           event.which works for NetScape and event.keyCode works for IE  --%>
<script LANGUAGE="JavaScript" type="text/javascript">
    
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  
 
      </script>

<%      	
     }
     
   Debug.debug("form " + formName);
%>

<%-- ********************* HTML for page begins here *********************  --%>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="BankColor">
	<tr>
		<td width="15" nowrap>&nbsp;</td>
		<td width="100%" height="30" valign="middle">
		<p class="ListTextWhite"><%=resMgr.getText("CashMgmtPendingTransactions.PendingTransactionText", 
                            TradePortalConstants.TEXT_BUNDLE)%></p>
		</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		&nbsp;
	</tr>
	<tr>
		<td class="ListText" width="20" nowrap>&nbsp;</td>
		<%                    
         currentTabLink = "currentTab=" + TradePortalConstants.RECEIVABLES_DIRECT_DEBIT_TAB;
         if (currentTab.equals(TradePortalConstants.RECEIVABLES_DIRECT_DEBIT_TAB)) {
            tabOn = TradePortalConstants.INDICATOR_YES;
         } else {
            tabOn = TradePortalConstants.INDICATOR_NO;
         }
      %>
		<jsp:include page="/common/Tab.jsp">
			<jsp:param name="tabOn" value="<%= tabOn %>" />
			<jsp:param name="linkTextKey" value="PendingTransactions.TabName" />
			<jsp:param name="action" value="goToReceivablesManagement" />
			<jsp:param name="linkParms" value="<%= currentTabLink %>" />
		</jsp:include>

		<%
         currentTabLink = "currentTab=" + TradePortalConstants.DIRECT_DEBIT_TRANSACTION_HISTORY;
         if (currentTab.equals(TradePortalConstants.DIRECT_DEBIT_TRANSACTION_HISTORY)) {
            tabOn = TradePortalConstants.INDICATOR_YES;
         } else {
            tabOn = TradePortalConstants.INDICATOR_NO;
         }
      %>
		<jsp:include page="/common/Tab.jsp">
			<jsp:param name="tabOn" value="<%= tabOn %>" />
			<jsp:param name="linkTextKey" value="DirectDebitTransactionHistory.TabName" />
			<jsp:param name="action" value="goToReceivablesManagement" />
			<jsp:param name="linkParms" value="<%= currentTabLink %>" />
		</jsp:include>
		<td class="ListText" width="100%">&nbsp;</td>
		<td class="ListText">&nbsp;</td>
		<td class="ListText" width="20" nowrap>&nbsp;</td>
	</tr>
</table>

<%
  // Based on the current tab, display the appropriate HTML for that tab
  if (currentTab.equals(TradePortalConstants.RECEIVABLES_DIRECT_DEBIT_TAB))
  {
%>
<%@ include file="DirectDebit-PendingListView.frag"%>
<%
  }
  else if (currentTab.equals(TradePortalConstants.DIRECT_DEBIT_TRANSACTION_HISTORY)) 
  {
%>
<%@ include file="DirectDebit-HistoryListView.frag"%>
<%
  }
%>
