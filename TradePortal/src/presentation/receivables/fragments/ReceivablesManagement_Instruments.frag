<%--
 *
 *     Copyright  � 2008                         
 *     CGI 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Receivables Instruments Tab

  Description:
    Contains HTML to create the Receivable Instruments for the Receivables Management Home page

  This is not a standalone JSP.  It MUST be included in a jsp page.
*******************************************************************************
--%> 
 
 
	
	 <input type="hidden" name="NewARMSearch" value="Y">
<%
	StringBuffer statusOptions = new StringBuffer();
   	StringBuffer statusExtraTags = new StringBuffer();
   	
   	String searchListViewName = "ARMInstrumentListView.xml"; 
   	
   	
  
   	String selectedStatus = session.getAttribute("instrStatusType").toString();
   	
   	String instrumentId =  request.getParameter("InstrumentId");
   	String amountTo = request.getParameter("AmountTo");
	String amountFrom= request.getParameter("AmountFrom");
	String currency= request.getParameter("Currency");
	
	 if (newSearchReceivableCriteria != null)
  	{
	     String DELIMITER = "@";
	
	     // Use StringTokeniser to separate each of the values
	     // Each key/value pair is separated by a delimiter and the key
	     // is separated from value by a "=", so the string may look like
	     // InstrumentType=PEN/RefNo=123
	     StringTokenizer links = new StringTokenizer(newSearchReceivableCriteria.toString(), DELIMITER);
	     String keyAndValue = null;
	     String key = null;
	     String value = null;
	     int sep;
	
	     while (links.hasMoreElements())
	     {
	       try
	       {
	          keyAndValue = (String) links.nextElement();
	          sep = keyAndValue.indexOf("=");
	          key = keyAndValue.substring(0, sep);
	          value = keyAndValue.substring(sep+1);
	          // set the variable value for redisplay
	          if (key.equals("InstrumentId"))
	          {
	             instrumentId = EncryptDecrypt.base64StringToString(value).toUpperCase();
	          }
	          else if (key.equals("Currency"))
	          {
	             currency = EncryptDecrypt.base64StringToString(value);
	          }
	          else if (key.equals("AmountFrom"))
	          {
	             amountFrom = EncryptDecrypt.base64StringToString(value);
	          }
	          else if (key.equals("AmountTo"))
	          {
	             amountTo = EncryptDecrypt.base64StringToString(value);
	          }
	      }
	      catch (NoSuchElementException e)
	      {
	         e.printStackTrace();
	      }
	    }  // end while

  } else {
   	 instrumentId =  request.getParameter("InstrumentId");
   	 amountTo = request.getParameter("AmountTo");
	 amountFrom= request.getParameter("AmountFrom");
	 currency= request.getParameter("Currency");
  }
  
	
	
	String options="";

        // NSX - PRUK080636392 - 08/16/10 - Begin
	statusOptions = Dropdown.createActiveInActiveStatusOptions(resMgr,selectedStatus);

   // Upon changing the status selection, automatically go back to the Receivable Management 
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)

   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToReceivableTransactionsHome", response));
   statusExtraTags.append("&amp;current2ndNav=");
   statusExtraTags.append(current2ndNav);
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;instrStatusType='+this.options[this.selectedIndex].value\"");
   %> 

<%--cquinton 8/30/2012 add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">
<%
  StringBuffer instrCriteria = new StringBuffer();
  instrCriteria.append(widgetFactory.createInlineLabel("", "InstrumentHistory.Show"));
  instrCriteria.append(widgetFactory.createCheckboxField("Active", "InstSearch.Active", true, false, false, "", "", "none")); //not a form item
  instrCriteria.append(widgetFactory.createCheckboxField("InActive", "InstSearch.InActive", false, false, false, "", "", "none")); //not a form item
  widgetFactory.wrapSearchItem(instrCriteria, "");//no extra classes
%>
      <%=instrCriteria.toString()%> 
			 
    </span>
    <span class="searchHeaderActions">
    
      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="rcvInstrumentGrid" />
             </jsp:include>
		<span id="inquiresRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("inquiresRefresh", "RefreshImageLinkHoverText") %>
    
    </span>
    <div style="clear:both;"></div>
  </div>

  <div class="searchDivider"></div>

  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">
	<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
	<%-- changed event to window.event to all the fields in order for enter key to work in Firefox --%>
      <%=widgetFactory.createSearchTextField("InstrumentID", "InstSearch.InstrumentID", "20",  "class='char10'  onKeydown='filterRecTranOnEnter(window.event, \"InstrumentID\", \"Basic\");'") %>
<% 
  options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
  out.println(widgetFactory.createSearchSelectField("Currency", "InstSearch.Currency", " ", options,"class='char10'  onKeydown='filterRecTranOnEnter(window.event, \"Currency\", \"Basic\");'"));
%>
	    <%=widgetFactory.createSearchAmountField("InqAmountFrom", "InstSearch.From","","maxlength = '18' style='width:10em' class='char10' onKeydown='filterRecTranOnEnter(window.event, \"InqAmountFrom\", \"Basic\");'", "") %>

      <%=widgetFactory.createSearchAmountField("InqAmountTo", "InstSearch.To", "","maxlength = '18' style='width:10em' class='char10' onKeydown='filterRecTranOnEnter(window.event, \"InqAmountTo\", \"Basic\");'", "") %>
    </span>
    <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>
    
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchRecInquiries">
      <%= resMgr.getText("PartySearch.Search", TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchInstrument();</script>
      </button>
    </span>
    <div style="clear:both"></div>
  </div>
</div>
  			
  		
<%
  gridHtml = dgFactory.createDataGrid("rcvInstrumentGrid","ARMInstrumentDataGrid", null);
  gridLayout = dgFactory.createGridLayout("rcvInstrumentGrid", "ARMInstrumentDataGrid");
  initSearchParms="selectedStatus="+selectedStatus+"&userOrgOid="+userOrgOid;
%>
<%=gridHtml%>
 
<%=widgetFactory.createHoverHelp("searchRecInquiries", "SearchHoverText") %>
