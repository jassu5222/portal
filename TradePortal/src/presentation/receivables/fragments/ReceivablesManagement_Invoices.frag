<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Transactions Home History Tab

  Description:
    Contains HTML to create the History tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%>

<%
//   String            searchType                   = null;
      secureParms.put("UserOid",        userSession.getUserOid());
      secureParms.put("OrgOid",        userSession.getOwnerOrgOid());

       String newDropdownSearch = request.getParameter("NewDropdownSearch");
      Debug.debug("New Dropdown Search is " + newDropdownSearch);

      String searchType   = request.getParameter("SearchType");

      String selectedStatus = request.getParameter("invoiceStatusType");

      if (selectedStatus == null)
      {
      	 if (session.getAttribute("invoiceStatusType") == null || session.getAttribute("invoiceStatusType").equals("")) {
            selectedStatus = TradePortalConstants.INVOICE_SEARCH_STATUS_OPEN;
         } else {
            selectedStatus = (String) session.getAttribute("invoiceStatusType");
         }
      }

      session.setAttribute("invoiceStatusType", selectedStatus);

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);

      String newSearchTrans = request.getParameter("NewSearch");

      loginLocale = userSession.getUserLocale();

   //rbhaduri - CR-374 - 14th Oct 08 - added options variables for invoice tab, Amount Type and Date Type dropdown
   String optionsAmountType = null;
   String optionsDateType = null;
   String invoiceID = null;

   StringBuffer statusExtraTags = new StringBuffer();
 
   // Build the status dropdown options
   // re-selecting the most recently selected option.
   StringBuffer dropdownOptions = new StringBuffer();
   dropdownOptions.append("<option value='");
   dropdownOptions.append(TradePortalConstants.STATUS_ALL);
   dropdownOptions.append("' ");
   if (TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
      dropdownOptions.append(" selected ");
   }
   dropdownOptions.append(">");
   dropdownOptions.append(STATUS_ALL);
   dropdownOptions.append("</option>");
   String[] statusOptions = { TradePortalConstants.INVOICE_SEARCH_STATUS_OPEN};
   for (int i = 0; i < statusOptions.length; i++) {
      dropdownOptions.append("<option value='");
      dropdownOptions.append(statusOptions[i]);
      dropdownOptions.append("' ");
      if (statusOptions[i].equals(selectedStatus)) {
         dropdownOptions.append(" selected ");
      }
      dropdownOptions.append(">");
      dropdownOptions.append(ReferenceDataManager.getRefDataMgr().getDescr(
				      TradePortalConstants.INVOICE_SEARCH_STATUS,
				       statusOptions[i], loginLocale));
      dropdownOptions.append("</option>");
   }

   // Upon changing the status selection, automatically go back to the Receivable Home
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)
   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToReceivableTransactionsHome", response));
   statusExtraTags.append("&amp;current2ndNav=");
   statusExtraTags.append(current2ndNav);
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;invoiceStatusType='+this.options[this.selectedIndex].value\"");
%>

<%--cquinton 8/30/2012 add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">
<%-- defect T36000006196 by dillip     --%>
      <%= widgetFactory.createSearchSelectField("invoiceStatusType", "InvoiceSearch.Status", "", dropdownOptions.toString(),"onChange='searchInvoices();'") %>
 <%=widgetFactory.createHoverHelp("invoiceStatusType","InvoiceSearch.Status") %> 
<%
  StringBuffer invCriteria = new StringBuffer();
  invCriteria.append(widgetFactory.createCheckboxField("Disputed", "InvoiceSearch.Disputed", false, false, false, "", "", "none"));
  invCriteria.append(widgetFactory.createCheckboxField("Financed", "InvoiceSearch.Financed", false, false, false, "", "", "none"));
  widgetFactory.wrapSearchItem(invCriteria, "");//no extra classes
%>
      <%=invCriteria.toString()%>

    </span>
    <span class="searchHeaderActions">
      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="invoicesGrid" />
      </jsp:include>
 	  <span id="invoicesRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("invoicesRefresh", "RefreshImageLinkHoverText") %>
      <span id="invoicesGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("invoicesGridEdit","CustomizeListImageLinkHoverText") %>  
     </span>
    <div style="clear:both;"></div>
  </div>


<%
  gridHtml = dgFactory.createDataGrid("invoicesGrid","InvoicesDataGrid", null);
  gridLayout = dgFactory.createGridLayout("invoicesGrid", "InvoicesDataGrid");
  //initSearchParms="selectedStatus="+selectedStatus+"&userOrgOid="+userOrgOid;
  //Modified for defect 5152 and T36000006196 by dillip
   initSearchParms="userOrgOid="+userOrgOid+"&selectedStatus="+selectedStatus;

%>

<div class="searchDivider"></div>

<%@ include file="/receivables/fragments/InvoiceSearchFilter.frag" %>
</div>
<input type=hidden name=SearchType value="Y">
<%=gridHtml%>
