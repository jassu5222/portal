
<%--
*******************************************************************************
                               ATP Creation Rule Detail

  Description:
  This page is used to maintain ATP creation rules.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2007                         
 *     CGI
 *     All rights reserved
--%>
<%@ page
      import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.*,java.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
      class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
      scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
      boolean isReadOnly = false;

      boolean isViewInReadOnly = false;

      String defnName = "";

      String options = "", optionsATP = "", optionsLC = "", optionsATPBB = "", optionsLCBB = "";
      String storeATP = "", storeLC = "", storeATPBB = "", storeLCBB = "";
      String fieldOptions = "";
      String defaultText;
      String link;

      String oid = null;
      String definitionOid = "";
      
        String buttonPressed = "";
        Vector error = null;

      // These booleans help determine the mode of the JSP.  
      boolean getDataFromDoc = false;
      boolean retrieveFromDB = false;
      boolean insertMode = false;
      boolean isNew=false;

      boolean showSave = false;
      boolean showDelete = false;
      boolean showSaveClose =false;
      boolean showClose= true;
      DocumentHandler doc;

      // Must initialize these 2 doc, otherwise get a compile error.
      DocumentHandler templatesDocATP = null, templatesDocLC = null;
      DocumentHandler opBankOrgsDocATP = null, opBankOrgsDocLC = null;

      //widgetfactory instance

      WidgetFactory widgetFactory = new WidgetFactory(resMgr);

      LCCreationRuleWebBean rule = beanMgr.createBean(LCCreationRuleWebBean.class, "LCCreationRule");
      POUploadDefinitionWebBean uploadDefn = beanMgr.createBean(POUploadDefinitionWebBean.class, "POUploadDefinition");
      
      //Srinivasu_D CR707 rel8.0 02/23/2012 Start
      PurchaseOrderDefinitionWebBean poDefinitionRef = beanMgr.createBean(PurchaseOrderDefinitionWebBean.class, "PurchaseOrderDefinition");

      CorporateOrganizationWebBean corporateOrgRef = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
      corporateOrgRef.setAttribute("organization_oid", userSession.getOwnerOrgOid());
      corporateOrgRef.getDataFromAppServer();
      String uploadFormat =  corporateOrgRef.getAttribute("po_upload_format");
      //Srinivasu_D CR707 rel8.0 02/23/2012 End

      String loginLocale;
      String loginRights;
      String viewInReadOnly;

      viewInReadOnly = request.getParameter("fromHomePage");

      //If we got to this page from the Home Page, then we need to explicitly be in Read only mode.
      //Also, the close button action needs to take us back to the home page.  This flag will help
      //us keep track of where to go.

      if ((InstrumentServices.isNotBlank(viewInReadOnly))
                  && (viewInReadOnly.equals("true"))) {
            isViewInReadOnly = true;
      }

      // Get the webbeans for the login in user and his security profile.  Set the
      // readonly access appropriately.
      loginLocale = userSession.getUserLocale();
      loginRights = userSession.getSecurityRights();
      
      String ruleTypeSecurityAccFlag = null;
      
      String ruleType = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("RuleType"), userSession.getSecretKey());
    
      if("LC".equals(ruleType)){
            ruleType = "IMP_DLC";
      }
      
      if (StringFunction.isNotBlank(ruleType)){
            if (ruleType.equalsIgnoreCase("IMP_DLC")) {
                  if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LC_CREATE_RULES)      ) {
                        isReadOnly = false;
                  } else if (SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_LC_CREATE_RULES)) {
                        isReadOnly = true;
                  }     
            } else if (ruleType.equalsIgnoreCase("ATP")){
                  if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_CREATE_RULES)     ) {
                        isReadOnly = false;
                  } else if (SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_ATP_CREATE_RULES)) {
                        isReadOnly = true;
                  }     
            }
      }
      // We should only be in this page for corporate users.  If the logged in
      // user's level is not corporate, send back to ref data page.  This
      // condition should never occur but is caught just in case.
      String ownerLevel = userSession.getOwnershipLevel();
      if (!ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
            formMgr.setCurrPage("RefDataHome");
            String physicalPage = NavigationManager.getNavMan()
                        .getPhysicalPage("RefDataHome", request);
%>
<jsp:forward page='<%= physicalPage %>' />
<%
      return;
      }

      if (isViewInReadOnly)
            isReadOnly = true;

      Debug.debug("login_rights is " + loginRights);

      // Get the document from the cache.  We'll use it to repopulate the screen 
      // if the user was entering data with errors.
      doc = formMgr.getFromDocCache();
       buttonPressed =doc.getAttribute("/In/Update/ButtonPressed");
      error = doc.getFragments("/Error/errorlist/error");
      String maxErrorSev = doc.getAttribute("/Error/maxerrorseverity");        
      if (InstrumentServices.isBlank(maxErrorSev)) {
      	 maxErrorSev = "0";
      }

      Debug.debug("doc from cache is " + doc.toString());

      if (doc.getDocumentNode("/In/LCCreationRule") == null) {
            // The document does not exist in the cache.  We are entering the page from
            // the listview page (meaning we're opening an existing item or creating 
            // a new one.)

            if (request.getParameter("oid") != null) {
        oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
            } else {
                  oid = null;
            }

            if ((oid == null) || (oid.equals(""))) {
                  getDataFromDoc = false;
                  retrieveFromDB = false;
                  insertMode = true;
                  oid = "0";
            } else if (oid.equals("0")) {
                  getDataFromDoc = false;
                  retrieveFromDB = false;
                  insertMode = true;
            } else {
                  // We have a good oid so we can retrieve.
                  getDataFromDoc = false;
                  retrieveFromDB = true;
                  insertMode = false;
            }
      } else {
            // The doc does exist so check for errors.  If highest severity < WARNING,
            // its a good update.
            retrieveFromDB = false;
            getDataFromDoc = true;

            String maxError = doc.getAttribute("/Error/maxerrorseverity");
            oid = doc.getAttribute("/Out/LCCreationRule/lc_creation_rule_oid");
          if (oid == null ) {
            oid = doc.getAttribute("/In/LCCreationRule/lc_creation_rule_oid");
          }
            
             if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0 ) {
                  // We've returned from a save/update/delete that was successful
                  // We shouldn't be here.  Forward to the RefDataHome page.
                  
            } else {
                  // We've returned from a save/update/delete but have errors.
                  // We will display data from the cache.  Determine if we're
                  // in insertMode by looking for a '0' oid in the input section
                  // of the doc.
                  String newOid = doc
                              .getAttribute("/In/LCCreationRule/lc_creation_rule_oid");

                  if (newOid.equals("0")) {
                        insertMode = true;
                        oid = "0";
                  } else {
                        // Not in insert mode, use oid from input doc.
                        insertMode = false;
                        oid = doc
                                    .getAttribute("/In/LCCreationRule/lc_creation_rule_oid");
                  }
            }
      }

      if (retrieveFromDB) {
            // Attempt to retrieve the item.  It should always work.  Still, we'll
            // check for a blank oid -- this indicates record not found.  Set to 
            // insert mode and display error.

            getDataFromDoc = false;

            rule.setAttribute("lc_creation_rule_oid", oid);
            rule.getDataFromAppServer();

            if (rule.getAttribute("lc_creation_rule_oid").equals("")) {
                  insertMode = true;
                  oid = "0";
                  getDataFromDoc = false;
            } else {
                  insertMode = false;
            }
      }

      if (getDataFromDoc) {
            // Populate the user bean with the input doc.
            try {
                  rule.populateFromXmlDoc(doc.getComponent("/In"));
                  ruleType = rule.getAttribute("instrument_type_code");
                  if("LC".equals(ruleType)){
                      ruleType = "IMP_DLC";
                 }
            } catch (Exception e) {
                  out.println("Contact Administrator: Unable to get ATP Creation Rule attributes "
                              + " for oid: " + oid + " " + e.toString());
            }
      }
      
      Hashtable secureParms = new Hashtable();
      String CurrentOrgOid = userSession.getOwnerOrgOid();
      String existingOrgOid = rule.getAttribute("owner_org_oid");

      if (InstrumentServices.isNotBlank(existingOrgOid)) {
            secureParms.put("owner_org_oid", existingOrgOid);

            if (!CurrentOrgOid.equals(existingOrgOid)) {
                  isReadOnly = true;
            }

      } else {
            secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
      }

      // This println is useful for debugging the state of the jsp.
      Debug.debug("retrieveFromDB: " + retrieveFromDB
                  + " getDataFromDoc: " + getDataFromDoc + " insertMode: "
                  + insertMode + " oid: " + oid);

      if(insertMode && !isReadOnly)
      {
            showSave = true;
            showSaveClose = true;
            showDelete = false;
      }else if(!insertMode && !isReadOnly){
            showSave = true;
        showSaveClose = true;
        showDelete = true;
      }else if(isReadOnly){
            showSave = false;
        showSaveClose = false;
        showDelete = false;
      }
      
%>

<%
      // Retrieve the data used to populate some of the dropdowns.
      StringBuilder sqlATP = new StringBuilder();
      StringBuilder sqlLC = new StringBuilder();

      QueryListView queryListView = null;

      try {
            queryListView = (QueryListView) EJBObjectFactory
                        .createClientEJB(formMgr.getServerLocation(),
                                    "QueryListView");
            List<Object> sqlParamsATP = new ArrayList();
            List<Object> sqlParamsLC = new ArrayList();
            sqlATP.append("select t.template_oid, name ");
            sqlLC.append("select t.template_oid, name ");
            sqlATP.append(" from template t, instrument i");
            sqlLC.append(" from template t, instrument i");
            sqlATP.append(" where t.p_owner_org_oid in (?");
            sqlLC.append(" where t.p_owner_org_oid in (?");
            sqlParamsATP.add(userSession.getOwnerOrgOid());
            sqlParamsLC.add(userSession.getOwnerOrgOid());
           
            // Also include templates from the user's actual organization if using subsidiary access
            if (userSession.showOrgDataUnderSubAccess()) {
                  sqlATP.append(",?");
                  sqlLC.append(",?");
                  sqlParamsATP.add( userSession.getSavedUserSession().getOwnerOrgOid());
                  sqlParamsLC.add( userSession.getSavedUserSession().getOwnerOrgOid());
            }

            sqlATP.append(") and i.instrument_oid = t.c_template_instr_oid");
            sqlLC.append(") and i.instrument_oid = t.c_template_instr_oid");
            sqlATP.append(" and i.instrument_type_code = ?");
            sqlLC.append(" and i.instrument_type_code = ?");
          
            sqlATP.append(" order by ");
            sqlLC.append(" order by ");
            sqlATP.append(resMgr.localizeOrderBy("name"));
            sqlLC.append(resMgr.localizeOrderBy("name"));
            //out.println("\n sqlATP1="+sqlATP.toString());
            //out.println("\n sqlLC1="+sqlLC.toString());
            sqlParamsATP.add(InstrumentType.APPROVAL_TO_PAY);
            sqlParamsLC.add(InstrumentType.IMPORT_DLC);

            queryListView.setSQL(sqlATP.toString(),sqlParamsATP);
            queryListView.getRecords();
            templatesDocATP = queryListView.getXmlResultSet();

            queryListView.setSQL(sqlLC.toString(),sqlParamsLC);
            queryListView.getRecords();
            templatesDocLC = queryListView.getXmlResultSet();
            

            //out.println("\n templatesDocATP="+templatesDocATP.toString());
            //out.println("\n templatesDocLC="+templatesDocLC.toString());
            Debug.debug(templatesDocATP.toString());
            Debug.debug(templatesDocLC.toString());

            sqlATP = new StringBuilder();
            sqlLC = new StringBuilder();
           
            sqlATP.append("select b.organization_oid, b.name ");
            sqlLC.append("select b.organization_oid, b.name ");
            sqlATP.append(" from corporate_org c, operational_bank_org b");
            sqlLC.append(" from corporate_org c, operational_bank_org b");
            sqlATP.append(" where c.organization_oid = ?");
            sqlLC.append(" where c.organization_oid = ?");
            sqlATP.append(" and b.organization_oid in (c.a_first_op_bank_org, c.a_second_op_bank_org, ");
            sqlLC.append(" and b.organization_oid in (c.a_first_op_bank_org, c.a_second_op_bank_org, ");
            sqlATP.append(" c.a_third_op_bank_org, c.a_fourth_op_bank_org)");
            sqlLC.append(" c.a_third_op_bank_org, c.a_fourth_op_bank_org)");
            sqlATP.append(" order by ");
            sqlLC.append(" order by ");
            sqlATP.append(resMgr.localizeOrderBy("b.name"));
            sqlLC.append(resMgr.localizeOrderBy("b.name"));
            //out.println("\n sqlATP2="+sqlATP.toString());
            //out.println("\n sqlLC2="+sqlLC.toString());

            queryListView.setSQL(sqlATP.toString(),new Object[]{ userSession.getOwnerOrgOid()});
            queryListView.getRecords();
            opBankOrgsDocATP = queryListView.getXmlResultSet();

            queryListView.setSQL(sqlLC.toString(),new Object[]{ userSession.getOwnerOrgOid()});
            queryListView.getRecords();
            opBankOrgsDocLC = queryListView.getXmlResultSet();

            //out.println("\n opBankOrgsDocATP="+opBankOrgsDocATP);
            //out.println("\n opBankOrgsDocLC="+opBankOrgsDocLC);
            Debug.debug(opBankOrgsDocATP.toString());
            Debug.debug(opBankOrgsDocLC.toString());

      } catch (Exception e) {
            e.printStackTrace();
      } finally {
            try {
                  if (queryListView != null) {
                        queryListView.remove();
                  }
            } catch (Exception e) {
                  System.out
                              .println("error removing querylistview in CreationRuleDetail.jsp");
            }
      } // try/catch/finally block

      // Now attempt to retrieve the PO Upload Definition object.  This is used to
      // display the Upload Defn name and fill in the dropdown fields with the data
      // items.

      try {
            // In insert mode we are passed a po upload definition oid.  Otherwise we
            // use the oid from the ATP Creation Rule webbean.
         if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat)){
            if (insertMode) {
                  definitionOid = request.getParameter("UploadDefinition");
                  String defOidFromPoUploadPage = doc
                              .getAttribute("/Out/oid");
                  if (InstrumentServices.isBlank(definitionOid)) {
                        if (InstrumentServices.isBlank(doc
                                    .getAttribute("/Out/oid"))) {
                              // In the case where we're in insert mode but have errors, the upload defn
                              // oid needs to be retrieved from the document rather than the request.
                              // We'll assume that a blank definition means we had errors.
                              definitionOid = doc
                                          .getAttribute("/In/LCCreationRule/po_upload_def_oid");
                        } else {
                              // We're coming from the PO Upload Definition page and the user has
                              // selected to create an ATP creation rule for a PO Upload Definition
                              // Get the PO upload def oid from the results of the mediator
             definitionOid = EncryptDecrypt.decryptStringUsingTripleDes(doc.getAttribute("/Out/oid"), userSession.getSecretKey());
                        }
                  } else {
                        // Otherwise, we found an encrypted oid from the request.
        definitionOid = EncryptDecrypt.decryptStringUsingTripleDes(definitionOid, userSession.getSecretKey());
                  }
            } else {
                  // Not in insert mode, get the oid from the webbean.
                  definitionOid = rule.getAttribute("po_upload_def_oid");
            }
            uploadDefn.setAttribute("po_upload_definition_oid",
                        definitionOid);
            uploadDefn.getDataFromAppServer();

            fieldOptions = uploadDefn.buildFieldOptionList();
            defnName = uploadDefn.getAttribute("name");
         }else if(TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(uploadFormat)){
              if (insertMode) {
                definitionOid = request.getParameter("UploadDefinition");
                String defOidFromPoUploadPage = doc.getAttribute("/Out/oid");
                if (InstrumentServices.isBlank(definitionOid))
                {
                  if(InstrumentServices.isBlank(doc.getAttribute("/Out/oid")))
                  {
                    // In the case where we're in insert mode but have errors, the upload defn
                    // oid needs to be retrieved from the document rather than the request.
                    // We'll assume that a blank definition means we had errors.
                    definitionOid = doc.getAttribute("/In/LCCreationRule/po_struct_pload_def_oid");
                  }
                  else
                  {
                    // We're coming from the PO Upload Definition page and the user has
                    // selected to create an ATP creation rule for a PO Upload Definition
                    // Get the PO upload def oid from the results of the mediator
                    definitionOid = EncryptDecrypt.decryptStringUsingTripleDes(doc.getAttribute("/Out/oid"), userSession.getSecretKey());
                  }
                }  else {
                  // Otherwise, we found an encrypted oid from the request.
                  definitionOid = EncryptDecrypt.decryptStringUsingTripleDes(definitionOid, userSession.getSecretKey());
                }
              } else {
                // Not in insert mode, get the oid from the webbean.
                definitionOid = rule.getAttribute("po_struct_pload_def_oid");
              }
              if(definitionOid==null)
              definitionOid="0";

              poDefinitionRef.setAttribute("purchase_order_definition_oid", definitionOid);
              poDefinitionRef.getDataFromAppServer();
              fieldOptions = poDefinitionRef.buildLCOptionList(resMgr);
              defnName = poDefinitionRef.getAttribute("name");
           }

      } catch (Exception e) {
            e.printStackTrace();
      }
%>

<%-- ********************* HTML for page begins here *********************  --%>


<%
      // Auto save the form when time-out if not readonly.  
      // (Header.jsp will check for auto-save setting of the corporate org).
      String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO: TradePortalConstants.INDICATOR_YES;
%>


<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
      <jsp:param name="includeNavigationBarFlag"
            value="<%=TradePortalConstants.INDICATOR_YES%>" />
      <jsp:param name="includeErrorSectionFlag"
            value="<%=TradePortalConstants.INDICATOR_YES%>" />
      <jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp" />

<div class="pageMain">
<div class="pageContent"><jsp:include
      page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="CreationRuleDetail.CreationRule" />
      <jsp:param name="helpUrl" value="customer/lc_creation_rule.htm" />
</jsp:include> <%
  	//cquinton 11/1/2012 ir#7015 remove return link
  	String subHeaderTitle = "";

	//Sandeep - Rel 8.3 Post Conversion Issue IR# T36000020385 09/02/2013 - Begin 
	subHeaderTitle = resMgr.getText( "ATPCreationRuleDetail.NewCreationRule",   TradePortalConstants.TEXT_BUNDLE); 
	//Sandeep - Rel 8.3 Post Conversion Issue IR# T36000020385 09/02/2013 - End

  	if(!insertMode){
        subHeaderTitle = rule.getAttribute("name");
  	}
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>      
            <jsp:include page="/common/ErrorSection.jsp" />

            <form name="ATPCreationRuleDetail" method="post" id="ATPCreationRuleDetail" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
                  <input type=hidden value="" name=buttonName>
                  <%
                        // Store values such as the userid, his security rights, and his org in a secure
                        // hashtable for the form.  The mediator needs this information.
                        secureParms.put("login_oid", userSession.getUserOid());
                        secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
                        secureParms.put("login_rights", loginRights);
                        secureParms.put("opt_lock", rule.getAttribute("opt_lock"));
                        secureParms.put("lc_creation_rule_oid", oid);
                        if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat))
                          secureParms.put("po_upload_def_oid", definitionOid);
                        else if(TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(uploadFormat))
                        secureParms.put("po_struct_pload_def_oid", definitionOid);
                  %>

                  <%=formMgr.getFormInstanceAsInputField("ATPCreationRuleDetailForm", secureParms)%>
                  

<div class="formArea">
                  <div class="formContent">
                        <%=widgetFactory.createSectionHeader("CreationRuleDetail.General", "CreationRuleDetail.General")%>
                        <div class="columnLeft">
                        <input type="hidden" name="CreationRule" value="" id="CreationRule" />
                        <%
                        // This block is to select the default ruleType when new rule is created
                        if( ruleType == null ){
                              // set the default value      
                              if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LC_CREATE_RULES) && 
                                                SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_CREATE_RULES)) {
                                          ruleType = "ATP";
                                    } else if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_CREATE_RULES)){
                                          ruleType = "ATP";
                                    }else if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LC_CREATE_RULES)) {
                                          ruleType = "IMP_DLC";
                                    }
                        }
                        
                        if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LC_CREATE_RULES) && 
                                    SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_CREATE_RULES)) {
                              ruleTypeSecurityAccFlag = "BOTH";
                        } else if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_CREATE_RULES))    {
                              ruleTypeSecurityAccFlag = "ATP_ONLY";
                        }else if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LC_CREATE_RULES)){
                              ruleTypeSecurityAccFlag = "LC_ONLY";
                        }else{
                              ruleTypeSecurityAccFlag = "NO_ACCESS";
                        }
                        //out.println("singlePayerChecked ="+ singlePayerChecked);
                        %>
                        <input type="hidden" name="InstrumentType" value="<%=StringFunction.xssCharsToHtml(ruleType)%>" id ="InstrumentType">
                        <%=widgetFactory.createLabel("CreationRuleDetail.RuleType","CreationRuleDetail.RuleType", isReadOnly, false,false, "")%>
                        <div class="formItem">
                <%
                if("BOTH".equals(ruleTypeSecurityAccFlag) || isReadOnly == true){
                             	 boolean ATPCreationRuleChecked = true;
                                          if (ruleType != null) {
                                                if (ruleType.equalsIgnoreCase("IMP_DLC")) {
                                                      ATPCreationRuleChecked = false;
                                                }
                                          }
               %>
                               	<%if(isReadOnly != true || "ATP".equalsIgnoreCase(ruleType)){%>
	                                   <%=widgetFactory.createRadioButtonField("CreationRule",
	                                   "CreationRule", "ATPCreationRuleDetail.ATPCreationRule", "ATP", ATPCreationRuleChecked,
	                                   isReadOnly, "onClick='setcreateRuleType(\"ATP\");'", "")%> <br/>
    							  <%}%>
    							  
    							   <%if(isReadOnly != true || "IMP_DLC".equalsIgnoreCase(ruleType)){%>
	                                   <%=widgetFactory.createRadioButtonField("CreationRule",
	                                   "LCCreationRule", "ATPCreationRuleDetail.LCCreationRule", "IMP_DLC", !ATPCreationRuleChecked,
	                                   isReadOnly, "onClick='setcreateRuleType(\"IMP_DLC\");'", "")%>
                                 <%}%>
                        <%}else if("ATP_ONLY".equals(ruleTypeSecurityAccFlag)){ %>
                                          <%=widgetFactory.createRadioButtonField("CreationRule", "CreationRule", "ATPCreationRuleDetail.ATPCreationRule", "ATP", true,isReadOnly, "onClick='setcreateRuleType(\"ATP\");'", "")%> 
                        <%}else if("LC_ONLY".equals(ruleTypeSecurityAccFlag)){%>
                                          <%=widgetFactory.createRadioButtonField("CreationRule", "LCCreationRule", "ATPCreationRuleDetail.LCCreationRule", "IMP_DLC", true,isReadOnly, "onClick='setcreateRuleType(\"IMP_DLC\");'", "")%>
                        <%}%>
                        
                              
                  </div>            
                  <div style="clear:both"></div>
                              <%=widgetFactory.createTextField("RuleName",
                              "CreationRuleDetail.RuleName", rule.getAttribute("name"),
                              "35", isReadOnly, true, false, "class = 'char35'", "", "")%>
      
                              <%=widgetFactory.createTextField("Description",
                              "CreationRuleDetail.Description",
                              rule.getAttribute("description"), "65", isReadOnly, true,
                              false, "class = 'char35'", "", "")%>

<div style="font-style:italic"><%=widgetFactory.createLabel("CreationRuleDetail.POsAreGrouped",
                                          "CreationRuleDetail.POsAreGrouped", true, false,
                                          false, "")%></div>
            
                        </div><%-- END ColumnLeft --%>


                        <div class="columnRight">
                              <%=widgetFactory.createTextField("POUploadDefinition",
                              "CreationRuleDetail.POUploadDefinition", defnName, "22",
                              true)%>


                              <%
                                    optionsATP = ListBox.createOptionList(templatesDocATP,
                                                "TEMPLATE_OID", "NAME", rule.getAttribute("template_oid"),
                                                // Encrypt the OID
                                                userSession.getSecretKey());
                                    optionsLC = ListBox.createOptionList(templatesDocLC,
                                                "TEMPLATE_OID", "NAME", rule.getAttribute("template_oid"),
                                                // Encrypt the OID
                                                userSession.getSecretKey());
                                    if (insertMode) {
                                          defaultText = resMgr.getText(
                                                      "CreationRuleDetail.selectTemplate",
                                                      TradePortalConstants.TEXT_BUNDLE);
                                    } else {
                                          defaultText = "";
                                    }
                                    
                                    String tempOptions = "";
                                    if("ATP".equalsIgnoreCase(ruleType)){
                                    	tempOptions = optionsATP;
                                    }else if("IMP_DLC".equalsIgnoreCase(ruleType)){
                                    	tempOptions = optionsLC;
                                    }
                                    		
                                    out.println(widgetFactory.createSelectField("TemplateChooser",
                                                "CreationRuleDetail.Template", defaultText, tempOptions, isReadOnly, true,
                                                false, "class = 'char30' ", "", ""));
                                    optionsATPBB = ListBox.createOptionList(opBankOrgsDocATP,
                                                "ORGANIZATION_OID", "NAME",
                                                rule.getAttribute("op_bank_org_oid"),
                                                // Encrypt the OID
                                                userSession.getSecretKey());
                                    optionsLCBB = ListBox.createOptionList(opBankOrgsDocLC,
                                                "ORGANIZATION_OID", "NAME",
                                                rule.getAttribute("op_bank_org_oid"),
                                                // Encrypt the OID
                                                userSession.getSecretKey());
                                    if (insertMode) {
                                          defaultText = resMgr.getText("CreationRuleDetail.selectBranch",
                                                      TradePortalConstants.TEXT_BUNDLE);
                                    } else {
                                          defaultText = "";
                                    }
                                    //out.println("options ="+optionsATPBB);
                                    out.println(widgetFactory.createSelectField("BankBranchChooser",
                                                "CreationRuleDetail.BankBranch", defaultText, optionsATPBB, isReadOnly, true,
                                                false, "class = 'char30'", "", ""));
                              %>
                              <%=widgetFactory.createTextField("CreationRuleDetail.CCY",
                              "CreationRuleDetail.CCY",
                              userSession.getBaseCurrencyCode(), "3", true, false, false,
                              "", "", "inline")%>

                              <%
                                    String displayMaximumAmount;
                                    // Don't format the amount if we are reading data from the doc
                                    //if (doc.getDocumentNode("/In/LCCreationRule") == null){
				if (!insertMode && maxErrorSev.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0 ) {
                                          displayMaximumAmount = TPCurrencyUtility.getDisplayAmount(rule.getAttribute("maximum_amount"),userSession.getBaseCurrencyCode(), loginLocale);
                                    }else{
                                          displayMaximumAmount = rule.getAttribute("maximum_amount");
                                    }
                                    //displayMaximumAmount = rule.getAttribute("maximum_amount");  

                                    %>
                              <%//widgetFactory.createTextField("MaxAmount","CreationRuleDetail.MaximumAmount", displayMaximumAmount, "19", isReadOnly,false, false, "", "regExp:'[0-9]+'", "inline")%>
                              
                              <%=widgetFactory.createAmountField("MaxAmount","CreationRuleDetail.MaximumAmount",displayMaximumAmount,"",isReadOnly,false,false,"","","inline")%>
                        </div><%-- ColumnRight --%>
                  </div>

                  <%=widgetFactory.createSectionHeader(
                              "CreationRuleDetail.GroupingCriteria",
                              "CreationRuleDetail.GroupingCriteria")%>

                        <div class="formItem">
                        <%=resMgr.getText("CreationRuleDetail.IncludePOs",TradePortalConstants.TEXT_BUNDLE)%>
                        
                        
                        <%=widgetFactory.createTextField("MinDays", "",
                              rule.getAttribute("min_last_shipment_date"), "3",
                              isReadOnly, false, false, "", "regExp:'[0-9]+'", "none")%>&nbsp;&nbsp;
                              <%=resMgr.getText("CreationRuleDetail.DaysLessThan",TradePortalConstants.TEXT_BUNDLE)%>
                        
                        <%=widgetFactory.createTextField("MaxDays", "",
                              rule.getAttribute("max_last_shipment_date"), "3",
                              isReadOnly, false, false, "", "regExp:'[0-9]+'", "none")%>
                              
                              <%=resMgr.getText("CreationRuleDetail.DaysFrom",
                                                                              TradePortalConstants.TEXT_BUNDLE)%>
           </div>
<div style="clear:both"></div>
                  <div style="font-style:italic">     <%=widgetFactory.createLabel("CreationRuleDetail.InValue",
                              "CreationRuleDetail.InValue", isReadOnly, false, false, "")%></div>

                        <span class="asterisk">*</span>
                  <table class="formDocumentsTable" width="75%">
                  <thead>
                              <tr>
                              <th >
                              <%-- <%=widgetFactory.createLabel(
                              "CreationRuleDetail.PODataItem",
                              "CreationRuleDetail.PODataItem", true, false, false,
                              "")%>  --%>
                              <%=resMgr.getText("CreationRuleDetail.PODataItem",TradePortalConstants.TEXT_BUNDLE)%>
                              <%//= widgetFactory.createLabel("","CreationRuleDetail.PODataItem",isReadOnly, false, false, "", "") %></th>
                                    <th>&nbsp;</th>
                                    <th><%//= widgetFactory.createLabel("","CreationRuleDetail.Value",isReadOnly, false, false, "", "") %></b>                         
                                    <%=resMgr.getText("CreationRuleDetail.Value",TradePortalConstants.TEXT_BUNDLE)%></b></th>
                                    <th>&nbsp;</th>
                              </tr>

                  </thead>
                              <%
                                    // There are 6 rows of criteria.  Display each of them (except if we're in
                                    // readonly mode and neither the data nor value fields have values.
                                    String dataAttribute = "";
                                    String valueAttribute = "";

                                    for (int x = 1; x <= 6; x++) {
                                          dataAttribute = rule.getAttribute("criterion" + x + "_data");
                                          valueAttribute = rule.getAttribute("criterion" + x + "_value");
                                          if (isReadOnly && InstrumentServices.isBlank(dataAttribute)
                                                      && InstrumentServices.isBlank(valueAttribute)) {
                                                // Do nothing
                                          } else {
                              %>
                              <tr>

                                    <td width="30%" align="center">
                                          <%
                                                // For the data item dropdowns, we'll add a blank entry.  This allows
                                                            // the user to deselect a value.  We'll use the fieldOptions list
                                                            // and attempt to 'select' one of the entries before building the
                                                            // select html.
                                                            defaultText = " ";
                                                    if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat))
                                                            options = uploadDefn.selectFieldInList(fieldOptions,dataAttribute);
                                                    else if(TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(uploadFormat))
                                                    options = poDefinitionRef.selectFieldInList(fieldOptions, dataAttribute);
                                                            out.println(widgetFactory.createSelectField("Data" + x, "",defaultText, options, isReadOnly));
                                          %>
                                    </td>

                                    <td width="15%" align="center"><%=widgetFactory.createLabel("CreationRuleDetail.Equal","CreationRuleDetail.Equal", isReadOnly, false,false, "")%></td>
                                    <td width="45%" align="center">
                                          <%-- A width of 40 allows for about 35 characters to be typed --%>
                                          <%=widgetFactory.createTextArea("Value" + x, "",valueAttribute,isReadOnly,false,false,"class = 'char25'","","")%>
                                                <%//=widgetFactory.createTextField("Value" + x,"",valueAttribute, "70", isReadOnly, false, false, "class = 'char25'", "", "")%>
                                    </td>

                                    <%
                                          // Display the "and" text for rows 1-5.
                                                      if (x != 7) {
                                    %>
                                    <td width="10%" align="center"><%=widgetFactory.createLabel("CreationRuleDetail.And","CreationRuleDetail.And", isReadOnly, false,false, "")%></td>
                                    <%
                                          } // end if x!=6
                                                } // end if isReadOnly and both data and value are blank
                                    %>
                              </tr>
                              <%
                                    } // end for
                              %>
                        </table>

                        <%
                              if (isViewInReadOnly) {
                                    link = "goToTradePortalHome";
                              } else {
                                    link = "goToRefDataHome";
                              }
                        %>
                  </div>
      </div>
<div style="clear:both";></div>

</div>

<% if("NO_ACCESS".equals(ruleTypeSecurityAccFlag)){
      showSave = false;
      showSaveClose = false;
      showDelete = false;
}%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar"
      data-dojo-props="form: 'ATPCreationRuleDetail'">
      <jsp:include page="/common/RefDataSidebar.jsp">
            <jsp:param name="showSaveButton" value="<%=showSave%>" />
            <jsp:param name="saveOnClick" value="none" />
            <jsp:param name="showSaveCloseButton" value="<%=showSaveClose%>" />
             <jsp:param name="saveCloseOnClick" value="none" />
            <jsp:param name="showDeleteButton" value="<%=showDelete%>" />
            <jsp:param name="cancelAction" value="goToRefDataHome" />
            <jsp:param name="showHelpButton" value="false" />
            <jsp:param name="showLinks" value="true" />
            <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
        <jsp:param name="error" value="<%= error%>" />   
      </jsp:include>
</div>
</form>

</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>

<%
  //save behavior defaults to without hsm
  
  //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  //String saveOnClick = "common.setButtonPressed('SaveTrans','0');  document.forms[0].submit();";
  //String saveCloseOnClick = "common.setButtonPressed('SaveCloseTrans','0'); document.forms[0].submit();";
%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />
  

</body>
</html>
<%
  String focusField = "RuleName";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
</script>
<%
  }
%>

<%
      String lcs = "data: [{name:\"Created By Balls4\", id:\"7qCUzc9dSmc=\"},{name:\"Created by BALLS 8\", id:\"epHzv!NJQuo=\"},{name:\"LC3\", id:\"L3\"},{name:\"LC4\", id:\"L4\"},{name:\"LC5\", id:\"L5\"}]";
      try {
            StringBuilder acs = new StringBuilder();
            String[] arr = null;
            //out.println("start formatting");
            //out.println("optionsATP="+optionsATP.length());
            //out.println("optionsATPBB="+optionsATPBB.length());
            //out.println("optionsLC="+optionsLC.length());
            //out.println("optionsLCBB="+optionsLCBB.length());

            if (optionsATP.length() > 0) {
                  int count = 0;
                  arr = optionsATP.split("</option>");
                  acs = new StringBuilder("data: [");
                  //    out.println("array length ="+arr.length);
                  for (count = 0; count < arr.length - 1; count++) {
                        String str = arr[count];
                        //           out.println("count ="+count);
                        //           out.println("\n arr="+str);
                        int start = str.indexOf("=");
                        //          out.println("\n start="+start);
                        int end = str.indexOf(">");
                        //          out.println("\n end="+end);
                        String key = str.substring(start + 2, end - 1);
                        //          out.println("\n key="+key);
                        String val = str.substring(end + 1, str.length());
                        //          out.println("\n valu=" + val);
                        acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                        acs.append(",");
                        //          out.println(acs);

                  }
                  //    out.println("out of for loop");
                  //    out.println(acs);
                  acs.deleteCharAt(acs.length() - 1);
                  //    out.println(", deleted" +acs);
                  acs.append("]");
                  //    out.println("] appended"+acs);      
                  storeATP = acs.toString();
                  //out.println("storeATP="+storeATP);
                  //out.println("*************************************************");
            }
            if (optionsLC.length() > 0) {
                  arr = optionsLC.split("</option>");
                  acs = new StringBuilder("data: [");
                  int count = 0;
                  //    out.println("array length ="+arr.length);
                  for (count = 0; count < arr.length - 1; count++) {
                        String str = arr[count];
                        //           out.println("count ="+count);
                        //           out.println("\n arr="+str);
                        int start = str.indexOf("=");
                        //          out.println("\n start="+start);
                        int end = str.indexOf(">");
                        //          out.println("\n end="+end);
                        String key = str.substring(start + 2, end - 1);
                        //          out.println("\n key="+key);
                        String val = str.substring(end + 1, str.length());
                        //          out.println("\n valu=" + val);
                        acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                        acs.append(",");
                        //          out.println(acs);

                  }
                  //    out.println("out of for loop");
                  //    out.println(acs);
                  acs.deleteCharAt(acs.length() - 1);
                  //    out.println(", deleted" +acs);
                  acs.append("]");
                  //    out.println("] appended"+acs);      
                  storeLC = acs.toString();
                  //out.println("storeLC="+storeLC);
                  //out.println("*************************************************");
            }

            if (optionsATPBB.length() > 0) {
                  int count = 0;
                  arr = optionsATPBB.split("</option>");
                  acs = new StringBuilder("data: [");
                  //    out.println("array length ="+arr.length);
                  for (count = 0; count < arr.length - 1; count++) {
                        String str = arr[count];
                        //           out.println("count ="+count);
                        //           out.println("\n arr="+str);
                        int start = str.indexOf("=");
                        //          out.println("\n start="+start);
                        int end = str.indexOf(">");
                        //          out.println("\n end="+end);
                        String key = str.substring(start + 2, end - 1);
                        //          out.println("\n key="+key);
                        String val = str.substring(end + 1, str.length());
                        //          out.println("\n valu=" + val);
                        acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                        acs.append(",");
                        //          out.println(acs);

                  }
                  //    out.println("out of for loop");
                  //    out.println(acs);
                  acs.deleteCharAt(acs.length() - 1);
                  //    out.println(", deleted" +acs);
                  acs.append("]");
                  //    out.println("] appended"+acs);      
                  storeATPBB = acs.toString();
                  //out.println("storeATPBB="+storeATPBB);
                  //out.println("*************************************************");
            }

            if (optionsLCBB.length() > 0) {
                  arr = optionsLCBB.split("</option>");
                  acs = new StringBuilder("data: [");
                  int count = 0;
                  //    out.println("array length ="+arr.length);
                  for (count = 0; count < arr.length - 1; count++) {
                        String str = arr[count];
                        //           out.println("count ="+count);
                        //           out.println("\n arr="+str);
                        int start = str.indexOf("=");
                        //          out.println("\n start="+start);
                        int end = str.indexOf(">");
                        //          out.println("\n end="+end);
                        String key = str.substring(start + 2, end - 1);
                        //          out.println("\n key="+key);
                        String val = str.substring(end + 1, str.length());
                        //          out.println("\n valu=" + val);
                        acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                        acs.append(",");
                        //          out.println(acs);

                  }
                  //    out.println("out of for loop");
                  //    out.println(acs);
                  acs.deleteCharAt(acs.length() - 1);
                  //    out.println(", deleted" +acs);
                  acs.append("]");
                  //    out.println("] appended"+acs);      
                  storeLCBB = acs.toString();
                  //out.println("storeLCBB="+storeLCBB);
                  //out.println("*************************************************");     
            }

      } catch (Exception ex) {
            ex.printStackTrace();
      }
%>

<script>

function setcreateRuleType(ruleType) {
      console.log('started setcreateRuleType');
      document.getElementById('InstrumentType').value = ruleType;
       var tcStore = new dojo.store.Memory({
    });
       var bbStore = new dojo.store.Memory({
   });
      
      if (ruleType == 'IMP_DLC'){
            console.log("rule type = LC");
            tcStore = new dojo.store.Memory({
              <%=storeLC%>
          });
             bbStore = new dojo.store.Memory({
               <%=storeLCBB%>
         });
      } else if (ruleType == 'ATP'){
            console.log("rule type = ATP");
            tcStore = new dojo.store.Memory({
              <%=storeATP%>
          });
             bbStore = new dojo.store.Memory({
               <%=storeATPBB%>
         });      
      }
      
      var ATPstore = new dojo.store.Memory({
        data: [
            {name:"ATP1", id:"A1"},
            {name:"ATP2", id:"A2"},
            {name:"ATP3", id:"A3"},
            {name:"ATP4", id:"A4"},
            {name:"ATP5", id:"A5"}]
    });

var tc = dijit.byId("TemplateChooser");
var bb = dijit.byId("BankBranchChooser");
tc.attr('value','');
bb.attr('value','');
tc.store = tcStore;
bb.store = bbStore;
console.log('completed setcreateRuleType');
}

</script>
<%
      // Finally, reset the cached document to eliminate carryover of 
      // information to the next visit of this page.
      formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
