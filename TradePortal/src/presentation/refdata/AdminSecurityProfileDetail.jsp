
<%--
*******************************************************************************
                          Admin Security Profile Detail

  Description:
     This page is used to maintain customer security profiles.  It supports
  insert, update and delete.  Errors found during a save are redisplayed on the 
  same page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.cache.*,
                 java.util.Vector, java.util.Hashtable " %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" 
             scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" 
             scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" 
             scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String links="";
  String certAuthURL =  "";
  String templateFlag =  "";
  
  boolean showSaveClose=true;
  boolean showDelete=true;
  boolean showSave=true;
  
  boolean isReadOnly = false;
  boolean debug = false;
  String buttonPressed = "";
  Vector error = null;
  String pageTitleKey = "";
  String securityProfileRights = "";

  String oid = "";

  // These booleans help determine the mode of the JSP.  
  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;
  boolean insertMode = true;

  // Constants for ref data access.
  String NO_ACCESS = "N";
  String VIEW_ONLY = "V";
  String MAINTAIN  = "M";

  DocumentHandler doc;

  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.SecurityProfileWebBean",
                        "SecurityProfile");

  SecurityProfileWebBean securityProfile =
                   (SecurityProfileWebBean)beanMgr.getBean("SecurityProfile");
  
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  isReadOnly = !SecurityAccess.hasRights(loginRights,
                                         SecurityAccess.MAINTAIN_ADM_SEC_PROF);

  // If the user can't even access ref data, we shouldn't be here.
  // Return to ref data home page.
  if (!SecurityAccess.hasRights(loginRights,
                                SecurityAccess.ACCESS_ADM_REFDATA_AREA)) {
     // If the user can't access ref data, we shouldn't even be here.
     formMgr.setCurrPage("AdminRefDataHome");

     String physicalPage = NavigationManager.getNavMan().getPhysicalPage("AdminRefDataHome", request);
%>
     <jsp:forward page='<%= physicalPage %>' />
<%
     return;
  }

  // We should only be in this page for corporate users.  If the logged in
  // user's level is not corporate, send back to ref data page.  This
  // condition should never occur but is caught just in case.
  String ownerLevel = userSession.getOwnershipLevel();
  if (ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
        formMgr.setCurrPage("AdminRefDataHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("AdminRefDataHome", request);
%>
     <jsp:forward page='<%= physicalPage %>' />
<%
        return;
  }


  // Get the document from the cache.  We'll use it to repopulate the screen if
  // the user was entering data with errors.
  doc = formMgr.getFromDocCache();
  Debug.debug("doc from cache is " + doc.toString());
  
  if (doc.getDocumentNode("/In/Update/ButtonPressed") != null) 
  {
      buttonPressed  = doc.getAttribute("/In/Update/ButtonPressed");
      error = doc.getFragments("/Error/errorlist/error");
  }

  if (doc.getDocumentNode("/In/SecurityProfile") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item or creating 
     // a new one.)

     if(request.getParameter("oid") != null)
      {
        oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
      }
     else
      {
	  oid = null;
      }

     if (oid == null) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
       oid = "0";
     } else if (oid.equals("0")) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
       insertMode = false;
     }

  } else {
     // The doc does exist so check for errors.  If highest severity >=
     // WARNING, we had errors.

     String maxError = doc.getAttribute("/Error/maxerrorseverity");

     if (maxError.compareTo(
                  String.valueOf(ErrorManager.ERROR_SEVERITY)) > 0) {
    	 
    	 // We've returned from a save/update/delete that was successful
         // We shouldn't be here.  Forward to the RefDataHome page.
         // Should never get here: jPylon and Navigation.xml should take care
         // of this situation.
         formMgr.setCurrPage("AdminRefDataHome");

         String physicalPage = NavigationManager.getNavMan().getPhysicalPage("AdminRefDataHome", request);
 %>
          <jsp:forward page='<%= physicalPage %>' />
 <%
         return;
      } 

    	     
          // We've returned from a save/update/delete but have errors.
          // We will display data from the cache.  Determine if we're
          // in insertMode by looking for a '0' oid in the input section
          // of the doc.

        retrieveFromDB = false;
        getDataFromDoc = true;

        String newOid = 
               doc.getAttribute("/In/SecurityProfile/security_profile_oid");

          if (newOid.equals("0")) {
             insertMode = true;
             oid = "0";
          } else {
             // Not in insert mode, use oid from input doc.
             insertMode = false;
             oid = doc.getAttribute("/In/SecurityProfile/security_profile_oid");
          }
       
  }

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll 
     // check for a blank oid -- this indicates record not found.  Set to insert
     // mode and display error.

     getDataFromDoc = false;

     securityProfile.setAttribute("security_profile_oid", oid);
     securityProfile.getDataFromAppServer();

     if (securityProfile.getAttribute("security_profile_oid").equals("")) {
       insertMode = true;
       oid = "0";
       getDataFromDoc = false;
     } else {
       insertMode = false;
     }
  } 

  if (getDataFromDoc) {
     // Populate the securityProfile bean with the input doc.
     try {
        securityProfile.populateFromXmlDoc(doc.getComponent("/In"));
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to get SecurityProfile "
             + "attributes for oid: " + oid + " " + e.toString());
     }
  }


  // This value gets used all over the place.  Get it once and then refer to the
  // local variable.
  securityProfileRights = securityProfile.getAttribute("security_rights");

  // Make the page readonly if the security profile does not belong to the user's org
  String ownerOid = securityProfile.getAttribute("owner_org_oid");
  if (!insertMode && !ownerOid.equals(userSession.getOwnerOrgOid()))
            isReadOnly = true;

  // This println is useful for debugging the state of the jsp.
  Debug.debug("retrieveFromDB: " + retrieveFromDB + " getDataFromDoc: " 
              + getDataFromDoc + " insertMode: " + insertMode + " oid: " 
              + oid);

  //IAZ CR-475 06/21/09 and 06/28/09 Begin
  //if users of this corp (as driven by Client Bank settings) cannot modify thier own security profiles,
  //make sure the mode isReadOnly if user access his/her own security profile or cust access security profile.
  if (InstrumentServices.isNotBlank(userSession.getClientBankOid())) 
  {
    try
    {
  		Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  		DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
  		String canModifyProfile = CBCResult.getAttribute("/ResultSetRecord(0)/CAN_EDIT_OWN_PROFILE_IND");
  		if (TradePortalConstants.INDICATOR_NO.equals(canModifyProfile))
  		{
  			try
  			{
  				UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");      
  				thisUser.setAttribute("user_oid", userSession.getUserOid());
  				thisUser.getDataFromAppServer();
  				if (thisUser.getAttribute("security_profile_oid").equals(oid))
  				{
  					isReadOnly = true;
  				}
  			}
  			catch (Exception any)
  			{
  				 // we can't detarmine if user is trying to chnage his own security profile - we must not allow
  				 // user to do any updates
  				 System.out.println("Exception verifying if user can edit security profile. Will set readOnly mode.");
				 isReadOnly = true;	  
  			}
  		}
  	}
  	catch (Exception any_e)
  	{
		System.out.println("General exception " + any_e.toString() + "verifying if admin user can edit security profile.  Will use default mode");
  	}  	
  }
  //IAZ CR-475 06/21/09 and 06/28/09 End

  if (isReadOnly || insertMode) showDelete = false;
  if (isReadOnly) {
	  	showSave = false;
  		showSaveClose = false;
  }
%>

<%-- ********************* HTML for page begins here *********************  --%>

<%
  
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
%>



<%-- Body tag included as part of common header --%>
<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
	<jsp:param name="templateFlag" value="<%=templateFlag%>" />
</jsp:include>

<div class="pageMain">
<div class="pageContent">
<%	
	String helpUrl = "admin/admin_sec_profile_detail.htm ";

%>
<%
	pageTitleKey = resMgr.getText("AdminSecProfileDetail.AdminSecurityProfiles",TradePortalConstants.TEXT_BUNDLE);
%>
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>
<%-- ------------------- Sub header start ------------------- --%>
<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if ( insertMode) {
    subHeaderTitle = resMgr.getText("AdminSecProfileDetail.NewAdminSecurityProfile",TradePortalConstants.TEXT_BUNDLE);
  } else {
    subHeaderTitle = securityProfile.getAttribute("name");
  }

  // figures out how to return to the calling page
  StringBuffer parms = new StringBuffer();
  parms.append("&returnAction=");
  /* parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToRefDataHome", userSession.getSecretKey()));
  String returnLink = formMgr.getLinkAsUrl("goToRefDataHome", parms.toString(), response); */
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToAdminRefDataHome", userSession.getSecretKey()));
  String returnLink = formMgr.getLinkAsUrl("goToAdminRefDataHome", "&tab=user", response);
%>
<jsp:include page="/common/PageSubHeader.jsp">
  <jsp:param name="title" value="<%= subHeaderTitle %>" />
</jsp:include>
<%-- ------------------- Sub header end ------------------- --%>
<form id="AdminSecurityProfileDetailForm" name="AdminSecurityProfileDetailForm"
	method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
	<jsp:include page="/common/ButtonPrep.jsp" />
<%
  // Store values such as the userid, his security rights, and his org in a
  // secure hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("login_rights", loginRights);
  secureParms.put("opt_lock", securityProfile.getAttribute("opt_lock"));
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("security_profile_oid", oid);
  secureParms.put("Type", TradePortalConstants.ADMIN);
  secureParms.put("OwnershipLevel", userSession.getOwnershipLevel());

  // Used for the "Added By" column
  if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
	secureParms.put("OwnershipType", TradePortalConstants.OWNER_TYPE_ADMIN);
  else
	secureParms.put("OwnershipType", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
%>


    <div class="formArea">
    <jsp:include page="/common/ErrorSection.jsp" />

    <div class="formContent"> <%-- Form Content Area starts here --%>
	<input type=hidden value="" name=buttonName>
	<div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
	<div class="dijitTitlePaneContentInner">
			     
<span class="asterisk">*</span>	<%=widgetFactory.createTextField("Name","AdminSecProfileDetail.ProfileName", securityProfile.getAttribute("name"),
			"35", isReadOnly, true, false,"", "", "none")%>
			
		<div style="clear:both">&nbsp;</div>	
	<table class="formDocumentsTable" width="100%">
		<tr>
			<th width="40%">&nbsp;</th>
			<th width="20%" ><%=resMgr.getText("AdminSecProfileDetail.NoAccess", TradePortalConstants.TEXT_BUNDLE)%></th>
			<th width="20%"><%=resMgr.getText("AdminSecProfileDetail.ViewOnly", TradePortalConstants.TEXT_BUNDLE)%></th>
			<th  width="20%"><%=resMgr.getText("AdminSecProfileDetail.View&Maintain", TradePortalConstants.TEXT_BUNDLE)%></th>				
		</tr>
		<tr>
			<th colspan="4" >
             <%=widgetFactory.createCheckboxField("noAccessFlag","AdminSecProfileDetail.SetAllNoAccess",
            		 false,isReadOnly, false,
		"onClick=\"checkNoAccess();\"", "", "")%>                           
			</th>
			<%--  --%>
		</tr>
		
	<tbody>
	<%-- ASP Admin USers Begin --%>
		<tr>
			<td class="tableSubHeader" colspan="4"><%=resMgr.getText("AdminSecProfileDetail.ASPAdminUsers", TradePortalConstants.TEXT_BUNDLE)%></td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.ClientBanks")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCBByPx","NO_ACCESS_CB_BY_PX","",NO_ACCESS,!SecurityAccess.hasEitherRight(securityProfileRights, 
	                 SecurityAccess.VIEW_CB_BY_PX,SecurityAccess.MAINTAIN_CB_BY_PX), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCBByPx","VIEW_ONLY_CB_BY_PX","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
	                 SecurityAccess.VIEW_CB_BY_PX), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCBByPx","MAINTAIN_CB_BY_PX","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
	                            SecurityAccess.MAINTAIN_CB_BY_PX), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<%-- ASP Admin USers End --%>
		<%-- Client Bank Admin USers Begins --%>
		<tr>
			<td class="tableSubHeader" colspan="4"><%=resMgr.getText("AdminSecProfileDetail.ClientBanksAdminUsers", TradePortalConstants.TEXT_BUNDLE)%></td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.BankGroups")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainBGByCB","NO_ACCESS_BG_BY_CB","",NO_ACCESS,!SecurityAccess.hasEitherRight(securityProfileRights, 
                        SecurityAccess.VIEW_BG_BY_CB,SecurityAccess.MAINTAIN_BG_BY_CB), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainBGByCB","VIEW_ONLY_BG_BY_CB","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_BG_BY_CB), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainBGByCB","MAINTAIN_BG_BY_CB","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_BG_BY_CB), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.CorpCustomers")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCorpByCB","NO_ACCESS_CORP_BY_CB","",NO_ACCESS,!SecurityAccess.hasEitherRight(securityProfileRights, 
                        SecurityAccess.VIEW_CORP_BY_CB,SecurityAccess.MAINTAIN_CORP_BY_CB), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCorpByCB","VIEW_ONLY_CORP_BY_CB","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_CORP_BY_CB), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCorpByCB","MAINTAIN_CORP_BY_CB","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_CORP_BY_CB), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.OpBankOrgs")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainOpbankByCB","NO_ACCESS_OPBANK_BY_CB","",NO_ACCESS,!SecurityAccess.hasEitherRight(securityProfileRights, 
                        SecurityAccess.VIEW_OPBANK_BY_CB,SecurityAccess.MAINTAIN_OPBANK_BY_CB), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainOpbankByCB","VIEW_ONLY_OPBANK_BY_CB","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OPBANK_BY_CB), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainOpbankByCB","MAINTAIN_OPBANK_BY_CB","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_OPBANK_BY_CB), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
				
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.ExternalBanks")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainExternalBanks","NO_ACCESS_EXTERNAL_BANK","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_EXTERNAL_BANKS), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainExternalBanks","VIEW_ONLY_EXTERNAL_BANK","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_EXTERNAL_BANK), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainExternalBanks","MAINTAIN_EXTERNAL_BANK","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_EXTERNAL_BANK), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>

		
		
		
		
		
		
		<%-- Client Bank Admin USers End --%>
		<%-- Bank Group Admin USers Begin --%>
		<tr>
			<td class="tableSubHeader" colspan="4"><%=resMgr.getText("AdminSecProfileDetail.BankGroupAdminUsers", TradePortalConstants.TEXT_BUNDLE)%></td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.CorpCustomers")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCorpByBG","NO_ACCESS_CORP_BY_BG","",NO_ACCESS,!SecurityAccess.hasEitherRight(securityProfileRights, 
                        SecurityAccess.VIEW_CORP_BY_BG,SecurityAccess.MAINTAIN_CORP_BY_BG), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCorpByBG","VIEW_ONLY_CORP_BY_BG","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_CORP_BY_BG), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCorpByBG","MAINTAIN_CORP_BY_BG","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_CORP_BY_BG), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<%-- Bank Group Admin USers End --%>
		<%-- Admin Begins --%>
		<tr>
			<td class="tableSubHeader" colspan="4"><%=resMgr.getText("AdminSecProfileDetail.AdminAccess", TradePortalConstants.TEXT_BUNDLE)%></td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.AdminSecurityProfiles")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainAdmSecProfile","NO_ACCESS_ADM_SEC_PROF","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_ADM_SEC_PROF), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainAdmSecProfile","VIEW_ONLY_ADM_SEC_PROF","",VIEW_ONLY,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_ADM_SEC_PROF), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainAdmSecProfile","MAINTAIN_ADM_SEC_PROF","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_ADM_SEC_PROF), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.AdminUsers")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainAdmUser","NO_ACCESS_ADM_USERS","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_ADM_USERS), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainAdmUser","VIEW_ONLY_ADM_USERS","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_ADM_USERS), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainAdmUser","MAINTAIN_ADM_USERS","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_ADM_USERS), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<%-- IR T36000005615 Starts --%>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.Announcements")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainAnnouncements","NO_ACCESS_ANNOUNCEMENT","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_ANNOUNCEMENT), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainAnnouncements","VIEW_ONLY_ANNOUNCEMENT","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_ANNOUNCEMENT), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainAnnouncements","MAINTAIN_ANNOUNCEMENT","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_ANNOUNCEMENT), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<%-- IR T36000005615 Ends --%>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.LockedOutUsers")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainLockedUser","NO_ACCESS_LOCKED_OUT_USERS","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_LOCKED_OUT_USERS), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainLockedUser","VIEW_ONLY_LOCKED_OUT_USERS","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_LOCKED_OUT_USERS), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainLockedUser","MAINTAIN_LOCKED_OUT_USERS","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_LOCKED_OUT_USERS), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<%-- Admin Ends --%>
		<%-- Report Begin --%>
		<tr>
			<td class="tableSubHeader" colspan="4"><%=resMgr.getText("AdminSecProfileDetail.Reports", TradePortalConstants.TEXT_BUNDLE)%></td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.CustomReport")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCustomReport","NO_ACCESS_CUSTOM_REPORT","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_CUSTOM_REPORT), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCustomReport","VIEW_ONLY_CUSTOM_REPORT","",VIEW_ONLY,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_CUSTOM_REPORT), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCustomReport","MAINTAIN_CUSTOM_REPORT","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_CUSTOM_REPORT), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.StandardReport")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainStandardReport","NO_ACCESS_STANDARD_REPORT","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_STANDARD_REPORT), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainStandardReport","VIEW_ONLY_STANDARD_REPORT","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_STANDARD_REPORT), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainStandardReport","MAINTAIN_STANDARD_REPORT","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_STANDARD_REPORT), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<%-- Reports End --%>
		<%-- Ref Data Begins --%>
		<tr>
			<td class="tableSubHeader" colspan="4"><%=resMgr.getText("AdminSecProfileDetail.RefData", TradePortalConstants.TEXT_BUNDLE)%></b></td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.BankBranchRules")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainBankBranchRules","NO_ACCESS_BANK_BRANCH_RULES","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_BANK_BRANCH_RULES), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainBankBranchRules","VIEW_ONLY_BANK_BRANCH_RULES","",VIEW_ONLY,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_BANK_BRANCH_RULES), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainBankBranchRules","MAINTAIN_BANK_BRANCH_RULES","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_BANK_BRANCH_RULES), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<%-- pgedupudi - Rel9.3 CR976A 03/09/2015 START --%>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.BankGrpRestrictRules")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainBankGrpRestrictRules","NO_ACCESS_BANK_GROUP_RESTRICT_RULES","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_BANK_GROUP_RESTRICT_RULES), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainBankGrpRestrictRules","VIEW_ONLY_BANK_GROUP_RESTRICT_RULES","",VIEW_ONLY,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_BANK_GROUP_RESTRICT_RULES), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainBankGrpRestrictRules","MAINTAIN_BANK_GROUP_RESTRICT_RULES","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_BANK_GROUP_RESTRICT_RULES), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<%-- pgedupudi - Rel9.3 CR976A 03/09/2015 END --%>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.CalendarMaintenance")%><%=widgetFactory.createSubLabel("AdminSecProfileDetail.ASPAdminUsersOnly")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCalendar","NO_ACCESS_CALENDAR","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_CALENDAR), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCalendar","VIEW_ONLY_CALENDAR","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_CALENDAR), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCalendar","MAINTAIN_CALENDAR","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_CALENDAR), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.CrossRateRule")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCrossRateRules","NO_ACCESS_CROSS_RATE_RULE","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_CROSS_RATE_RULE), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCrossRateRules","VIEW_ONLY_CROSS_RATE_RULE","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_CROSS_RATE_RULE), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCrossRateRules","MAINTAIN_CROSS_RATE_RULE","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_CROSS_RATE_RULE), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.CurrencyCalendarRules")%><%=widgetFactory.createSubLabel("AdminSecProfileDetail.ASPAdminUsersOnly")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCurrencyCalendarRules","NO_ACCESS_CURRENCY_CALENDAR_RULES","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_CURRENCY_CALENDAR_RULES), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCurrencyCalendarRules","VIEW_ONLY_CURRENCY_CALENDAR_RULES","",VIEW_ONLY,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_CURRENCY_CALENDAR_RULES), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainCurrencyCalendarRules","MAINTAIN_CURRENCY_CALENDAR_RULES","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_CURRENCY_CALENDAR_RULES), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.ForexRates")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainForexRates","NO_ACCESS_FX_RATE","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_FX_RATE), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainForexRates","VIEW_ONLY_FX_RATE","",VIEW_ONLY,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_FX_RATE), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainForexRates","MAINTAIN_FX_RATE","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_FX_RATE), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.GenericMessageCategories")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainGenMsgCat","NO_ACCESS_GM_CAT_GRP","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_GM_CAT_GRP), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainGenMsgCat","VIEW_ONLY_GM_CAT_GRP","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_GM_CAT_GRP), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainGenMsgCat","MAINTAIN_GM_CAT_GRP","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_GM_CAT_GRP), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.InstTemplates")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainTemplate","NO_ACCESS_TEMPLATE","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_TEMPLATE), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainTemplate","VIEW_ONLY_TEMPLATE","",VIEW_ONLY,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_TEMPLATE), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainTemplate","MAINTAIN_TEMPLATE","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_TEMPLATE), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.InterestDiscountRates")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainInterestRates","NO_ACCESS_INTEREST_RATE","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_INTEREST_RATE), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainInterestRates","VIEW_ONLY_INTEREST_RATE","",VIEW_ONLY,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_INTEREST_RATE), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">	
			&nbsp;			
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.NotificationRuleTemplates")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainNotifRuleTemplates","NO_ACCESS_NOTIF_RULE_TEMPLATES","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_NOTIF_RULE_TEMPLATES), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainNotifRuleTemplates","VIEW_ONLY_NOTIF_RULE_TEMPLATES","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_NOTIF_RULE_TEMPLATES), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainNotifRuleTemplates","MAINTAIN_NOTIF_RULE_TEMPLATES","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_NOTIF_RULE_TEMPLATES), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>


		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.Parties")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainParty","NO_ACCESS_PARTIES","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_PARTIES), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainParty","VIEW_ONLY_PARTIES","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_PARTIES), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainParty","MAINTAIN_PARTIES","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_PARTIES), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<%-- Srinivasu_D CR-599 Rel8.4 11/21/2013 - start --%>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.RefDataPaymentDef")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPaymentFileDef","NO_ACCESS_PAYMENT_DEF","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_PAYMENT_FILE_DEF), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPaymentFileDef","VIEW_ONLY_PAYMENT_DEF","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_PAYMENT_FILE_DEF), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPaymentFileDef","MAINTAIN_PAYMENT_DEF","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_PAYMENT_FILE_DEF), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<%-- Srinivasu_D CR-599 Rel8.4 11/21/2013 - End --%>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.PaymentMethVal")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPaymentMethVal","NO_ACCESS_PAYMENT_METH_VAL","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_PAYMENT_METH_VAL), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPaymentMethVal","VIEW_ONLY_PAYMENT_METH_VAL","",VIEW_ONLY,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_PAYMENT_METH_VAL), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPaymentMethVal","MAINTAIN_PAYMENT_METH_VAL","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_PAYMENT_METH_VAL), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.Phrases")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPhrase","NO_ACCESS_PHRASE","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_PHRASE), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPhrase","VIEW_ONLY_PHRASE","",VIEW_ONLY,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_PHRASE), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPhrase","MAINTAIN_PHRASE","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_PHRASE), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.SecurityProfiles")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainSecProf","NO_ACCESS_SEC_PROF","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_SEC_PROF), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainSecProf","VIEW_ONLY_SEC_PROF","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_SEC_PROF), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainSecProf","MAINTAIN_SEC_PROF","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_SEC_PROF), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>
		<tr>
			<td><%=widgetFactory.createSubLabel("AdminSecProfileDetail.TemplateGroups")%></td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPaymentTemplateGroup","NO_ACCESS_PAYMENT_TEMPLATE_GRP","",NO_ACCESS,!SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_OR_MAINTAIN_PAYMENT_TEMPLATE_GRP), isReadOnly)%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPaymentTemplateGroup","VIEW_ONLY_PAYMENT_TEMPLATE_GRP","",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.VIEW_PAYMENT_TEMPLATE_GRP), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
			<td class="formAdminTableCenter">
				<%=widgetFactory.createRadioButtonField("MaintainPaymentTemplateGroup","MAINTAIN_PAYMENT_TEMPLATE_GRP","",MAINTAIN,SecurityAccess.hasRights(securityProfileRights, 
                        SecurityAccess.MAINTAIN_PAYMENT_TEMPLATE_GRP), isReadOnly, "onClick=\"unCheckNoAccessCheckBox();\"", "")%>
			</td>
		</tr>





		<%-- Ref Data End --%>
	</tbody>
	</table>
	
		<br/>
		<%-- SSikhakolli - Rel-9.3.5 CR-1029 - adding Admin Update Centre section - Begin --%>
		<%= widgetFactory.createSubsectionHeader("AdminSecProfileDetail.AdminUpdateCentre") %>
		<div class="formItem">
			<%=widgetFactory.createCheckboxField("AccessAdminUpdateCenter","AdminSecProfileDetail.NoAccess",
					!SecurityAccess.hasRights(securityProfileRights, SecurityAccess.ACCESS_ADMIN_UPDATE_CENTRE),
					isReadOnly,false,"onClick=\"noAccesstoAllAUC();\"","","")%>
		</div>
		<table  class="formDocumentsTable" style="table-layout: fixed;width:690px;">
	    	<thead>
	      		<tr>
		            <th style="width:250px;"  >&nbsp;</th>
		            <th style="width:66px;" ><%=resMgr.getText("AdminSecProfileDetail.ApplyUpdates", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th style="width:66px;"><%=resMgr.getText("AdminSecProfileDetail.Approve", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th style="width:66px;"><%=resMgr.getText("AdminSecProfileDetail.SendForRepair", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th style="width:70px;"><%=resMgr.getText("AdminSecProfileDetail.AttachDocs", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th style="width:70px;"><%=resMgr.getText("AdminSecProfileDetail.DeleteDocs", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th width="6%">&nbsp;</th>
	      		</tr>
	    	</thead>
	    	<tr>
            	<th colspan=7 style="text-align: left;">
					<%=widgetFactory.createRadioButtonField("SelectAllBankTransGrp", "SelectAllBankTrans", "AdminSecProfileDetail.SelectAll","Y", false, isReadOnly,"onClick=\"selectAllBankTrans(true);\"","")%>
            		&nbsp; &nbsp;&nbsp;&nbsp; 
            		<%=widgetFactory.createRadioButtonField("SelectAllBankTransGrp", "SelectNoneBankTrans", "AdminSecProfileDetail.SelectNone","N", false, isReadOnly,"onClick=\"selectAllBankTrans(false);\"","")%>
				</th>
      		</tr>
      		<tr>
	            <td><%=resMgr.getText("AdminSecProfileDetail.TransStatusUpdates", TradePortalConstants.TEXT_BUNDLE)%></td>  
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankTransApplyUpdates","",SecurityAccess.hasRights(securityProfileRights,
		        	SecurityAccess.BANK_TRANS_APPLY_UPDATE),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
		       	</td>  
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankTransApprove","",SecurityAccess.hasRights(securityProfileRights,
		        			SecurityAccess.BANK_TRANS_APPROVE),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
	            </td> 
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankTransSendforRepair","",SecurityAccess.hasRights(securityProfileRights,
		        			SecurityAccess.BANK_TRANS_SEND_FOR_REPAIR),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
	            </td> 
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankTransAttachDocs","",SecurityAccess.hasRights(securityProfileRights,
	        		        SecurityAccess.BANK_TRANS_ATTACH_DOC),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
	            </td> 
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankTransDeleteDocs","",SecurityAccess.hasRights(securityProfileRights,
	        		        SecurityAccess.BANK_TRANS_DELETE_DOC),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
	            </td> 
	            <td align="center">
	            	&nbsp;
	            </td>
      		</tr>
	    </table>
	    
	    <table  class="formDocumentsTable" style="table-layout: fixed;width:690px;">
	    	<thead>
	      		<tr>
		            <th style="width:250px;">&nbsp;</th>
		            <th style="width:66px;"><%=resMgr.getText("AdminSecProfileDetail.Download", TradePortalConstants.TEXT_BUNDLE)%></th>
		           <th>&nbsp;</th>
		            
	      		</tr>
	    	</thead>
	    	<tr>
            	<th colspan=3 style="text-align: left;">
					<%=widgetFactory.createRadioButtonField("SelectAllBankXMLGrp", "SelectAllBankXML", "AdminSecProfileDetail.SelectAll","Y", false, isReadOnly,"onClick=\"selectAllBankXML(true);\"","")%>
            		&nbsp; &nbsp;&nbsp;&nbsp; 
            		<%=widgetFactory.createRadioButtonField("SelectAllBankXMLGrp", "SelectNoneBankXML", "AdminSecProfileDetail.SelectNone","N", false, isReadOnly,"onClick=\"selectAllBankXML(false);\"","")%>
				</th>
      		</tr>
      		<tr>
	            <td><%=resMgr.getText("AdminSecProfileDetail.XMLDownloads", TradePortalConstants.TEXT_BUNDLE)%></td>  
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankXMLDownload","",SecurityAccess.hasRights(securityProfileRights,
	        		        SecurityAccess.BANK_XML_DOWNLOAD),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
	            </td>  
	            <td align="center">&nbsp;</td> 
      		</tr>
	    </table>
	    
	    <table width="100%" class="formDocumentsTable" style="table-layout: fixed;width:690px;">
	    	<thead>
	      		<tr>
		            <th style="width:250px;">&nbsp;</th>
		            <th style="width:66px;"><%=resMgr.getText("AdminSecProfileDetail.Delete", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th style="width:66px;"><%=resMgr.getText("AdminSecProfileDetail.CreateReply", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th style="width:66px;"><%=resMgr.getText("AdminSecProfileDetail.SendToCust", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th style="width:70px;"><%=resMgr.getText("AdminSecProfileDetail.AttachDocs", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th style="width:70px;"><%=resMgr.getText("AdminSecProfileDetail.DeleteDocs", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th width="6%">&nbsp;</th>
	      		</tr>
	    	</thead>
	    	<tr>
            	<th colspan=7 style="text-align: left;">
					<%=widgetFactory.createRadioButtonField("SelectAllBankMailGrp", "SelectAllBankMail", "AdminSecProfileDetail.SelectAll","Y", false, isReadOnly,"onClick=\"selectAllBankMail(true);\"","")%>
            		&nbsp; &nbsp;&nbsp;&nbsp; 
            		<%=widgetFactory.createRadioButtonField("SelectAllBankMailGrp", "SelectNoneBankMail", "AdminSecProfileDetail.SelectNone","N", false, isReadOnly,"onClick=\"selectAllBankMail(false);\"","")%>
				</th>
      		</tr>
      		<tr>
	            <td><%=resMgr.getText("AdminSecProfileDetail.AdminMailMessages", TradePortalConstants.TEXT_BUNDLE)%></td>  
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankMailDelete","",SecurityAccess.hasRights(securityProfileRights,
	        		        SecurityAccess.BANK_MAIL_DELETE),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
	            </td>  
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankMailCreateReply","",SecurityAccess.hasRights(securityProfileRights,
	        		        SecurityAccess.BANK_MAIL_CREATE_REPLY),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
	            </td> 
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankMailSendtoCust","",SecurityAccess.hasRights(securityProfileRights,
	        		        SecurityAccess.BANK_MAIL_SEND_TO_CUST),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
	            </td> 
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankMailAttachDocs","",SecurityAccess.hasRights(securityProfileRights,
	        		        SecurityAccess.BANK_MAIL_ATTACH_DOC),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
	            </td> 
	            <td align="center">
	            	<%=widgetFactory.createCheckboxField("BankMailDeleteDocs","",SecurityAccess.hasRights(securityProfileRights,
	        		        SecurityAccess.BANK_MAIL_DELETE_DOC),isReadOnly,false,"onClick=\"pickAdminUpdateCenterAccess();\"","","none")%>
	            </td> 
	            <td align="center">&nbsp;</td>
      		</tr>
	    </table>
	    <%-- SSikhakolli - Rel-9.3.5 CR-1029 - adding Admin Update Centre section - End --%>
	</div>
  </div>	
  </div><%--formContent--%>
  </div><%--formArea--%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'AdminSecurityProfileDetailForm'">
                   <jsp:include page="/common/RefDataSidebar.jsp">
                              <jsp:param name="showSaveButton" value="<%= showSave %>" />
							<jsp:param name="showSaveCloseButton" value="<%= showSaveClose %>" />
					        <jsp:param name="showDeleteButton" value="<%= showDelete %>" />
                             <jsp:param name="cancelAction" value="goToAdminRefDataHome" />
                             <jsp:param name="showHelpButton" value="true" />  
                             <jsp:param name="helpLink" value="admin/admin_sec_profile_detail.htm" />                                                        
			            	<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
			            	<jsp:param name="showtips" value="true" />
			            	<jsp:param name="showTop" value="true" />
			            	<jsp:param name="error" value="<%= error%>" />
			            	<jsp:param name="formName" value="0" />
                   </jsp:include>        
          </div> <%--closes sidebar area--%>
<%= formMgr.getFormInstanceAsInputField("AdminSecurityProfileDetailForm", secureParms) %>

</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />


<script LANGUAGE="JavaScript">
  function checkNoAccess() {
	  if(dijit.byId("noAccessFlag").checked == true)
		  {
		  dijit.byId("NO_ACCESS_CB_BY_PX").set('checked',true);
		    dijit.byId("NO_ACCESS_CB_BY_PX").set('value','NO_ACCESS');
		    dijit.byId("NO_ACCESS_BG_BY_CB").set('checked',true);
		    dijit.byId("NO_ACCESS_CORP_BY_CB").set('checked',true);
		    dijit.byId("NO_ACCESS_OPBANK_BY_CB").set('checked',true);
		    dijit.byId("NO_ACCESS_CORP_BY_BG").set('checked',true);
		    dijit.byId("NO_ACCESS_ADM_SEC_PROF").set('checked',true);
		    dijit.byId("NO_ACCESS_ADM_USERS").set('checked',true);
		    dijit.byId("NO_ACCESS_ANNOUNCEMENT").set('checked',true);	<%-- IR T36000005615 Change --%>	    
		    dijit.byId("NO_ACCESS_LOCKED_OUT_USERS").set('checked',true);
		    dijit.byId("NO_ACCESS_CUSTOM_REPORT").set('checked',true);
		    dijit.byId("NO_ACCESS_STANDARD_REPORT").set('checked',true);
		    dijit.byId("NO_ACCESS_BANK_BRANCH_RULES").set('checked',true);
		    dijit.byId("NO_ACCESS_CALENDAR").set('checked',true);
		    dijit.byId("NO_ACCESS_CROSS_RATE_RULE").set('checked',true);
		    dijit.byId("NO_ACCESS_CURRENCY_CALENDAR_RULES").set('checked',true);
		    dijit.byId("NO_ACCESS_FX_RATE").set('checked',true);
		    dijit.byId("NO_ACCESS_GM_CAT_GRP").set('checked',true);
		    dijit.byId("NO_ACCESS_TEMPLATE").set('checked',true);
		    dijit.byId("NO_ACCESS_INTEREST_RATE").set('checked',true);
		    dijit.byId("NO_ACCESS_NOTIF_RULE_TEMPLATES").set('checked',true);
		    dijit.byId("NO_ACCESS_PARTIES").set('checked',true);
		    console.log("BEFORE - NO_ACCESS_PAYMENT_METH_VAL - "+dijit.byId("NO_ACCESS_PAYMENT_METH_VAL").checked);
		    dijit.byId("NO_ACCESS_PAYMENT_METH_VAL").set('checked',true);
		    console.log("AFTER - NO_ACCESS_PAYMENT_METH_VAL - "+dijit.byId("NO_ACCESS_PAYMENT_METH_VAL").checked);    
		    dijit.byId("NO_ACCESS_PHRASE").set('checked',true);
		    dijit.byId("NO_ACCESS_SEC_PROF").set('checked',true);
		    dijit.byId("NO_ACCESS_PAYMENT_TEMPLATE_GRP").set('checked',true);
			dijit.byId("NO_ACCESS_EXTERNAL_BANK").set('checked',true);
			dijit.byId("NO_ACCESS_PAYMENT_DEF").set('checked',true);
		  }
    
  }
  
  function unCheckNoAccessCheckBox() {
    dijit.byId("noAccessFlag").set('checked',false);
  }

  function checkNoAccessReverse() {
	  if (
			    (dijit.byId("NO_ACCESS_CB_BY_PX").checked == true) &&
			    (dijit.byId("NO_ACCESS_BG_BY_CB").checked == true) &&
			    (dijit.byId("NO_ACCESS_CORP_BY_CB").checked == true) &&
			    (dijit.byId("NO_ACCESS_OPBANK_BY_CB").checked == true) &&
			    (dijit.byId("NO_ACCESS_CORP_BY_BG").checked == true) &&
			    (dijit.byId("NO_ACCESS_ADM_SEC_PROF").checked == true) &&
			    (dijit.byId("NO_ACCESS_ADM_USERS").checked == true) &&
			    (dijit.byId("NO_ACCESS_ANNOUNCEMENT").checked == true) && <%-- IR T36000005615 Change --%>	
			    (dijit.byId("NO_ACCESS_LOCKED_OUT_USERS").checked == true) &&
			    (dijit.byId("NO_ACCESS_CUSTOM_REPORT").checked == true) &&
			    (dijit.byId("NO_ACCESS_STANDARD_REPORT").checked == true) &&
			    (dijit.byId("NO_ACCESS_BANK_BRANCH_RULES").checked == true) &&
			    (dijit.byId("NO_ACCESS_CALENDAR").checked == true) &&
			    (dijit.byId("NO_ACCESS_CROSS_RATE_RULE").checked == true) &&
			    (dijit.byId("NO_ACCESS_CURRENCY_CALENDAR_RULES").checked == true) &&
			    (dijit.byId("NO_ACCESS_FX_RATE").checked == true) &&
			    (dijit.byId("NO_ACCESS_GM_CAT_GRP").checked == true) &&
			    (dijit.byId("NO_ACCESS_TEMPLATE").checked == true) &&
			    (dijit.byId("NO_ACCESS_INTEREST_RATE").checked == true) &&
			    (dijit.byId("NO_ACCESS_NOTIF_RULE_TEMPLATES").checked == true) &&
			    (dijit.byId("NO_ACCESS_PARTIES").checked == true) &&
			    (dijit.byId("NO_ACCESS_PAYMENT_METH_VAL").checked == true) &&    
			    (dijit.byId("NO_ACCESS_PHRASE").checked == true) &&
			    (dijit.byId("NO_ACCESS_SEC_PROF").checked == true) &&
				(dijit.byId("NO_ACCESS_PAYMENT_DEF").checked == true) &&
			    (dijit.byId("NO_ACCESS_PAYMENT_TEMPLATE_GRP").checked == true)&&
			    (dijit.byId("NO_ACCESS_EXTERNAL_BANK").checked == true)	    
	  			) {
			    dijit.byId("noAccessFlag").set('checked',true) ;
			  }    
  }
  
  
		<%-- SSikhakolli - Rel-9.3.5 CR-1029 - adding Admin Update Centre section - Begin --%>
		function noAccesstoAllAUC(){
			var result = dijit.byId('AccessAdminUpdateCenter').checked;
			if(result){
				resetAdminUpdateCentre(!result);
			}
		}
		
		function pickAdminUpdateCenterAccess() {
		    <%--
		    // Based on whether any of the instrument access rights are checked,
		    // check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
		    // for Admin Update Center.
			--%>
		    var result = dijit.byId('BankTransApplyUpdates').checked
		    				|| dijit.byId('BankTransApprove').checked
		    				|| dijit.byId('BankTransSendforRepair').checked
		    				|| dijit.byId('BankTransAttachDocs').checked
		    				|| dijit.byId('BankTransDeleteDocs').checked		    			

		    				|| dijit.byId('BankXMLDownload').checked
		    				|| dijit.byId('BankMailDelete').checked
		    				|| dijit.byId('BankMailCreateReply').checked
		    				|| dijit.byId('BankMailSendtoCust').checked
		    				|| dijit.byId('BankMailAttachDocs').checked
		    				|| dijit.byId('BankMailDeleteDocs').checked;
		    dijit.byId('AccessAdminUpdateCenter').setChecked(!result);
		}
		
		function selectAllBankTrans(checkedFlag) { 
			dijit.byId('BankTransApplyUpdates').setChecked(checkedFlag);
			dijit.byId('BankTransApprove').setChecked(checkedFlag);
			dijit.byId('BankTransSendforRepair').setChecked(checkedFlag);
			dijit.byId('BankTransAttachDocs').setChecked(checkedFlag)
			dijit.byId('BankTransDeleteDocs').setChecked(checkedFlag);			

			pickAdminUpdateCenterAccess();
		}
		
		function selectAllBankXML(checkedFlag) { 
			dijit.byId('BankXMLDownload').setChecked(checkedFlag);
			pickAdminUpdateCenterAccess();
		}
		
		function selectAllBankMail(checkedFlag) { 
			dijit.byId('BankMailDelete').setChecked(checkedFlag);
			dijit.byId('BankMailCreateReply').setChecked(checkedFlag);
			dijit.byId('BankMailSendtoCust').setChecked(checkedFlag);
			dijit.byId('BankMailAttachDocs').setChecked(checkedFlag);
			dijit.byId('BankMailDeleteDocs').setChecked(checkedFlag);
			pickAdminUpdateCenterAccess();
		}
		  
		function resetAdminUpdateCentre(checkedFlag){
			dijit.byId('BankTransApplyUpdates').setChecked(checkedFlag);
			dijit.byId('BankTransApprove').setChecked(checkedFlag);
			dijit.byId('BankTransSendforRepair').setChecked(checkedFlag);
			dijit.byId('BankTransAttachDocs').setChecked(checkedFlag);
			dijit.byId('BankTransDeleteDocs').setChecked(checkedFlag);
		
			
			dijit.byId('BankXMLDownload').setChecked(checkedFlag);
			
			dijit.byId('BankMailDelete').setChecked(checkedFlag);
			dijit.byId('BankMailCreateReply').setChecked(checkedFlag);
			dijit.byId('BankMailSendtoCust').setChecked(checkedFlag);
			dijit.byId('BankMailAttachDocs').setChecked(checkedFlag);
			dijit.byId('BankMailDeleteDocs').setChecked(checkedFlag);
			
			dijit.byId('SelectAllBankTrans').setChecked(checkedFlag);
			dijit.byId('SelectNoneBankTrans').setChecked(checkedFlag);
			
			dijit.byId('SelectAllBankXML').setChecked(checkedFlag);
			dijit.byId('SelectNoneBankXML').setChecked(checkedFlag);
			
			dijit.byId('SelectAllBankMail').setChecked(checkedFlag);
			dijit.byId('SelectNoneBankMail').setChecked(checkedFlag);
		}
		<%-- SSikhakolli - Rel-9.3.5 CR-1029 - adding Admin Update Centre section - End --%>
</script>

</body>
</html>

<%
  // Finally, reset the cached document to eliminate carryover of 
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

  // All done with the bean, so get rid of it.
  beanMgr.unregisterBean("SecurityProfile");

%>
<script>
  require(["dijit/registry", "dojo/on", "dojo/ready"],
      function(registry, on, ready) {
    ready( function(dom){      
	 checkNoAccessReverse();
    });
  });
</script>