<%--
*******************************************************************************
                                  Admin User Detail


  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same
  page.  Successful updates return the user to the ref data home page.

  Assumption: The organization and level of the org are always known.
  Therefore, for a new user that information is passed in to this page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, com.ams.tradeportal.common.cache.*, java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
<%

  boolean isAdminUser = true;
  // [START] CR-482
  // Check if this TradePortal instance is configured for HSM encryption / decryption
  boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
  // [END] CR-482
%>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

  boolean isReadOnly = false;

  boolean isViewInReadOnly = false;

  String options;
  String defaultText;
  String link;
//Naveen 06-August-2012 IR-T36000003304
  String buttonPressed ="";
  String oid = "";

  // These booleans help determine the mode of the JSP.
  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;
  boolean insertMode = true;
  boolean errorsFound = false;

  String password1 = "";
  String password2 = "";

  boolean showSave = true;
  boolean showDelete = true;
  boolean saveClose = true;
//jgadela rel8.3 CR501 [START]
  boolean showApprove = false;
  boolean showReject = false;
//jgadela rel8.3 CR501 [END]

  DocumentHandler doc;

  // Must initialize these 2 doc, otherwise get a compile error.
  DocumentHandler corpSecProfileDoc = new DocumentHandler();
  DocumentHandler adminSecProfileDoc = new DocumentHandler();
  DocumentHandler orgDoc = new DocumentHandler();
  //pgedupudi - Rel-9.3 CR-976A 03/26/2015
  DocumentHandler bnkGrpRestrictRuleDoc = new DocumentHandler();

  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.UserWebBean", "User");

  UserWebBean user = (UserWebBean)beanMgr.getBean("User");

  String loginLocale;
  String loginRights;
  String viewInReadOnly;

  String ownershipLevel = "";
  String orgAndLevel = "";
  String userOrgOid = "";
  String orgName = "";

  String displayExtendedListviewInd;
  boolean defaultRadioButtonValue1 = true;
  boolean defaultRadioButtonValue2 = false;

  viewInReadOnly = request.getParameter("fromHomePage");

  // If we got to this page from the Home Page, then we need to explicitly
  // be in Read only mode.  Also, the close button action needs to take us
  // back to the home page.  This flag will help us keep track of where to go.

  if( (InstrumentServices.isNotBlank(viewInReadOnly)) &&
      (viewInReadOnly.equals("true")) )
  {
      isViewInReadOnly = true;
  }
  //Vasavi CR 580 06/01/10 Begin
   String         userSecurityType        = null;



  // Retrieve the user's security type (i.e., ADMIN or NON_ADMIN)
  if (userSession.getSavedUserSession() == null) {
            userSecurityType = userSession.getSecurityType();
  }else {
         userSecurityType = userSession.getSavedUserSessionSecurityType();
  }
  Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
  //Vasavi CR 580 06/01/10 End

  String canViewIDFields="";

  //jgadela rel8.3 CR501 [START]
  String pendingReferenceDataOid = "";
  String pendingChangedObjectOid		 = "0";
  String adminUserDualCtrlReqdInd = "N";
 //jgadela rel8.3 CR501 [END]
%>

<%
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();

  if (SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_ADM_USERS)) {
     isReadOnly = false;
  } else if (SecurityAccess.hasRights(loginRights,
                                      SecurityAccess.VIEW_ADM_USERS)) {
     isReadOnly = true;
  } else {
     // Don't have maintain or view access, send the user back.
        formMgr.setCurrPage("AdminRefDataHome");
        String physicalPage =
                 NavigationManager.getNavMan().getPhysicalPage("AdminRefDataHome", request);
%>
        <jsp:forward page='<%= physicalPage %>'/>
<%
        return;
  }

  // We should only be in this page for non-corporate users.  If the logged in
  // user's level is corporate, send back to ref data page.  This condition
  // should never occur but is caught just in case.
  String ownerLevel = userSession.getOwnershipLevel();
  if (ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
        formMgr.setCurrPage("AdminRefDataHome");
        String physicalPage =
                 NavigationManager.getNavMan().getPhysicalPage("AdminRefDataHome", request);
%>
        <jsp:forward page='<%= physicalPage %>'/>
<%
        return;
  }

  if( isViewInReadOnly )
      isReadOnly = true;

  Debug.debug("login_rights is " + loginRights);

  // Get the document from the cache.  We'll use it to repopulate the screen
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();
//Naveen 06-August-2012 IR-T36000003304- Start. For Printing Time stamp above save button.
  buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
//Naveen 06-August-2012 IR-T36000003304- End

  Debug.debug("doc from cache is " + doc.toString());

  if (doc.getDocumentNode("/In/User") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item or creating
     // a new one.)

     if (request.getParameter("oid") != null) {
         oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
         pendingChangedObjectOid		 = oid;
     } else {
        oid = null;
     }

     if (oid == null) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
       oid = "0";
     } else if (oid.equals("0")) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
       insertMode = false;
     }
  } else {

         oid = doc.getAttribute("/Out/User/user_oid");
         if("0".equals(oid) || oid==null){
               oid = doc.getAttribute("/In/User/user_oid");
         }
       //jgadela rel8.3 CR501 [START]
         pendingChangedObjectOid		 = oid;
         pendingReferenceDataOid = doc.getAttribute("/Out/PendingReferenceData/oid");
         if(pendingReferenceDataOid == null || "".equals(pendingReferenceDataOid) || "null".equals(pendingReferenceDataOid)){
         	  pendingReferenceDataOid = doc.getAttribute("/In/PendingReferenceData/oid");
           }
         //jgadela rel8.3 CR501 [END]
     // The doc does exist so check for errors.  If highest severity >= WARNING,
     // we have errors

     String maxError = doc.getAttribute("/Error/maxerrorseverity");
      //Naveen 06-August-2012 IR-T36000003304. Modified the condition for printing the time stamp above Save button..
      //Prateep Adding <= in condition for navigation issues for save , save and close.
     if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY))>=0) {
        // We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.

        errorsFound = true;

        // Get the passwords the user typed.  We will redisplay them in the
        // password fields.  This only occurs if errors were found.
        password1 = doc.getAttribute("/In/User/newPassword");
        if (password1 == null) password1 = "";
        password2 = doc.getAttribute("/In/User/retypePassword");
        if (password2 == null) password2 = "";

        retrieveFromDB = false;
        getDataFromDoc = true;

        String newOid = doc.getAttribute("/In/User/user_oid");



        if (newOid.equals("0")) {
           insertMode = true;
           oid = "0";
        } else {
           // Not in insert mode, use oid from input doc.
           insertMode = false;
           oid = doc.getAttribute("/In/User/user_oid");
           pendingChangedObjectOid		 = oid;
           password1 = "";
           password2 = "";
        }
     } else {
        // We've returned from a save/update/delete that was successful
        // We shouldn't be here.  Forward to the AdminRefDataHome page.
        // This normally should be handled by jPylon and Navigation.xml.
         retrieveFromDB = false;
         getDataFromDoc = true;
         password1 = "";
         password2 = "";
     }
  }

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Set to
     // insert mode and display error.

     getDataFromDoc = false;

     user.setAttribute("user_oid", oid);
     //user.getDataFromAppServer();

   //jgadela rel8.3 CR501 [START]
     if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction"))){
    	  showApprove = true;
    	 showReject = true;
     }
     String ownerShipLevel = userSession.getOwnershipLevel();
     if(TradePortalConstants.OWNER_GLOBAL.equals(ownerShipLevel) || TradePortalConstants.OWNER_BOG.equals(ownerShipLevel)){
    	 ownerShipLevel = TradePortalConstants.OWNER_BANK;
     }
     ReferenceDataPendingWebBean pendingObj = beanMgr.createBean(ReferenceDataPendingWebBean.class,"ReferenceDataPending");
	 user = (UserWebBean) pendingObj.getReferenceData(user, userSession.getUserOid(),showApprove);
	 //If changed by same user (or) No Maintain access (or)  then do not show approve/reject buttons
	 if (userSession.getUserOid().equals(pendingObj.getAttribute("change_user_oid")) || isReadOnly ||
			 !ownerShipLevel.equals(pendingObj.getAttribute("ownership_level")) || !userSecurityType.equals(pendingObj.getAttribute("changed_user_security_type"))){
		 showApprove = false;
    	 showReject = false;
	 }
			 
	 pendingReferenceDataOid = pendingObj.getAttribute("pending_data_oid");
	//IR 21408 - if user has maintain access then show error alert
	 if (pendingObj.isReadOnly() && !isReadOnly) {
	     isReadOnly = true;
	     if(showApprove == false){
		     DocumentHandler errorDoc = new DocumentHandler();
		     String errorMessage = resMgr.getText(TradePortalConstants.PENDING_USER_ALREADY_EXISTS2,"ErrorMessages");
		     String textM = resMgr.getText("PendingRefdata.User","TextResources");
		     errorMessage = errorMessage.replace("{0}",textM);
		     errorDoc.setAttribute("/severity", "5");
		     errorDoc.setAttribute("/message", errorMessage);
		     doc.addComponent("/Error/errorlist/error(0)", errorDoc);
	     }
	 }
	 //jgadela rel8.3 CR501 [END]

     if (user.getAttribute("user_oid").equals("") || user.getAttribute("user_oid") ==null) {
       insertMode = true;
       oid = "0";
       getDataFromDoc = false;
     } else {
    	//jgadela rel8.3 CR501 -- Added if condition if the case of approving newly added pending record
    	 String change_type = pendingObj.getAttribute("change_type");
    	 if("C".equalsIgnoreCase(change_type)){
    	 	insertMode = true;
    	 	oid = "0";
    	 	getDataFromDoc = false;
    	 }else{
     		  insertMode = false;
    	 }
     }
  }


  // Make sure that users cannot delete themselves
  String thisUserOid;
  thisUserOid = userSession.getUserOid();
  //IAZ CR-475 06/21/09 and 06/28/09 Begin
  //if bank users (as driven by Client Bank settings) cannot modify thier own profiles,
  //make sure the mode isReadOnly if user access his/her own profile
  //if ( thisUserOid.equals(oid) ) showDelete = false;
  try {
  if ( thisUserOid.equals(oid) )
  {
      showDelete = false;
      if (!isReadOnly)
      {
            if (InstrumentServices.isNotBlank(userSession.getClientBankOid()))
            {
                  String canModifyProfile = CBCResult.getAttribute("/ResultSetRecord(0)/CAN_EDIT_OWN_PROFILE_IND");
                  if (TradePortalConstants.INDICATOR_NO.equals(canModifyProfile)){
                        isReadOnly = true;
            }
            }
      }
  }
  }
  catch (Exception any_e)
  {
            System.out.println("General exception " + any_e.toString() + "verifying if admin user can edit own profile.  Will use default mode.");
  }

  //IAZ CR-475 06/21/09 and 06/28/09 End

  if (getDataFromDoc) {
	//jgadela rel8.3 CR501 [START]  - IR T36000018219
	  com.ams.tradeportal.busobj.webbean.ReferenceDataPendingWebBean pendingBean = null;
	  beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.ReferenceDataPendingWebBean", "ReferenceDataPending");

	  pendingBean = (ReferenceDataPendingWebBean) beanMgr.getBean("ReferenceDataPending");
	  pendingBean.getById(pendingReferenceDataOid);

	  String change_type = pendingBean.getAttribute("change_type");
	  if("C".equalsIgnoreCase(change_type)){
		  insertMode = true;
		  oid = "0";
	  }
	   //jgadela rel8.3 CR501 [END]

     // Populate the user bean with the input doc.
     try {
        user.populateFromXmlDoc(doc.getComponent("/In"));
        // org and level is an "attribute" we have created for this web page
        // combines the org selected and the level of that org
        orgAndLevel = doc.getAttribute("/In/User/OwnerOrgAndLevel");
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to get User attributes "
             + " for oid: " + oid + " " + e.toString());
     }
  }

  // This println is useful for debugging the state of the jsp.
  Debug.debug("retrieveFromDB: " + retrieveFromDB + " getDataFromDoc: "
              + getDataFromDoc + " insertMode: " + insertMode + " oid: "
              + oid);

  if (isReadOnly || insertMode) showDelete = false;
  if (isReadOnly){ showSave = false; saveClose = false;}

  String additionalAuthType = user.getAttribute("additional_auth_type");
  String additionalAuth2FAType = user.getAttribute("auth2fa_subtype"); 


%>

<%

  // Originally we allowed the user to select/set the organization on this page.
  // Now there is a org selection page where the user must select and org before
  // creating a new user.  Because of this, we now know the org (and level of that
  // org) when entering the page for a new user, or from the webbean for an
  // existing user.  The org and level are passed to the mediator as a combination
  // value (i.e., 'org/level')  [could have passed as two parms but kept as one
  // for historical purposes.]

  try {
    // In insert mode we are passed an org and level.  In non insert mode we use
    // the values from the user webbean.
    if (insertMode) {
      orgAndLevel = request.getParameter("OrgAndLevel");
      if (InstrumentServices.isBlank(orgAndLevel)) {
        // In the case where we're in insert mode but have errors, the org and level
        // need to be retrieved from the document rather than the request.
        // We'll assume that a blank org/level means we had errors.
        if(doc == null){
       	 orgAndLevel = doc.getAttribute("/In/User/OwnerOrgAndLevel");
        }else if(user!=null){
    	  ownershipLevel = user.getAttribute("ownership_level");
          userOrgOid = user.getAttribute("owner_org_oid");
          orgAndLevel = userOrgOid + "/" + ownershipLevel;
        }
      }
      else {
        // Otherwise, we found an encrypted oid from the request.
        // Commented following line for IR T36000005754 -Komal
        //orgAndLevel = EncryptDecrypt.decryptStringUsingTripleDes(orgAndLevel, userSession.getSecretKey());
    	  // Parse the decrypted org/level into pieces.
          int slashAt = orgAndLevel.indexOf("/");
		  if(slashAt != -1){
          userOrgOid = orgAndLevel.substring(0,slashAt);
          ownershipLevel = orgAndLevel.substring(slashAt+1, orgAndLevel.length());
		  }
      }
    } else {
      // Not in insert mode, get the oid from the webbean.
      // ownershipLevel represents the level for the user record being edited.
      ownershipLevel = user.getAttribute("ownership_level");
      userOrgOid = user.getAttribute("owner_org_oid");
      orgAndLevel = userOrgOid + "/" + ownershipLevel;
    }

  } catch (Exception e) {
    e.printStackTrace();
    //out.println(e.getMessage());
  }

  // Retrieve the data used to populate some of the dropdowns.

  StringBuilder sql = new StringBuilder();

  QueryListView queryListView = null;
  String parentOrgOid = null;

  try {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
                        formMgr.getServerLocation(), "QueryListView");

      // Get a list of ADMIN security profiles.  We ue the organization (and level)
      // of the user being edited and select profiles at his level and higher.
      // (e.g., BOG user sees the BOG's profiles plus his client bank's profiles
      // plus global org profiles)

      sql = new StringBuilder();
      sql.append("select security_profile_oid, name ");
      sql.append(" from security_profile");
      sql.append(" where security_profile_type =?");
      List<Object> sqlParamsAdminList = new ArrayList();
      sqlParamsAdminList.add(TradePortalConstants.ADMIN);
      // Onwership level represents the ownership level of the user being edited.
      // (NOT the level of the logged in user).
      if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
         // For Global Org users, the list of profiles is those belonging to
         // the global org and the client banks
         sql.append(" and p_owner_org_oid = ?");
         sqlParamsAdminList.add( userSession.getGlobalOrgOid());
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
         // For Client Bank users, the list of profiles is those belonging to
         // the global org and the client bank.
         sql.append(" and (p_owner_org_oid = ?");
         sql.append(" or p_owner_org_oid = ?)" );
         sqlParamsAdminList.add( userSession.getGlobalOrgOid());
         sqlParamsAdminList.add(userOrgOid);
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
         // For BOG users, the list of profiles is those belonging to
         // the global org, the client bank, or the BOG.  We know that BOG users can
         // only be edited by other BOG users in that BOG or client bank users who
         // own that BOG--therefore, the client bank of the logged in user must be
         // the same as the client bank of the user being edited.
         sql.append(" and (p_owner_org_oid = ?");
         sql.append(" or p_owner_org_oid = ?");
         sql.append(" or p_owner_org_oid = ?)");
         sqlParamsAdminList.add( userSession.getGlobalOrgOid());
         sqlParamsAdminList.add( userSession.getClientBankOid());
         sqlParamsAdminList.add(userOrgOid);
      }

      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("name"));
      Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString(),sqlParamsAdminList);

      queryListView.getRecords();

      adminSecProfileDoc = queryListView.getXmlResultSet();
      Debug.debug(adminSecProfileDoc.toString());
      
            // Now get a list of NON_ADMIN security profiles.

      sql = new StringBuilder();
      List<Object> sqlParamsNonAdmin = new ArrayList();
      sql.append("select security_profile_oid, name ");
      sql.append(" from security_profile");
      sql.append(" where security_profile_type = ?");
      sqlParamsNonAdmin.add(TradePortalConstants.NON_ADMIN);
      
      if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
         // For global org users, the list of profiles is those belonging to
         // global org only
         sql.append(" and p_owner_org_oid = ?");
         sqlParamsNonAdmin.add( userSession.getGlobalOrgOid());
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
         // For Client Bank users, the list of profiles is those belonging to
         // global org or the client bank.
         sql.append(" and (p_owner_org_oid = ?");
         sql.append(" or p_owner_org_oid = ?)");
         sqlParamsNonAdmin.add( userSession.getGlobalOrgOid());
         sqlParamsNonAdmin.add(userOrgOid);
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
         // For BOG users, the list of profiles is those belonging to
         // global org, the client bank, or the BOG.  We know that BOG users can
         // only be edited by other BOG users in that BOG or client bank users who
         // own that BOG--therefore, the client bank of the logged in user must be
         // the same as the client bank of the user being edited.
         sql.append(" and (p_owner_org_oid = ?" );
         sql.append(" or p_owner_org_oid = ?");
         sql.append(" or p_owner_org_oid = ?)");
         sqlParamsNonAdmin.add( userSession.getGlobalOrgOid());
         sqlParamsNonAdmin.add( userSession.getClientBankOid());
         sqlParamsNonAdmin.add(userOrgOid);
      }


      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("name"));
      Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString(), sqlParamsNonAdmin);

      queryListView.getRecords();

      corpSecProfileDoc = queryListView.getXmlResultSet();
      Debug.debug(corpSecProfileDoc.toString());


      // Now get the name for the user's org.

      sql = new StringBuilder();

      if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
         sql.append("select name from global_organization");
         queryListView.setSQL(sql.toString(),new ArrayList<Object>());
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
    	  /* IR#T36000040515 pgedupudi - Rel-9.3 CR-976A 06/17/2015 */
         sql.append("select name, ORGANIZATION_OID from client_bank");
         sql.append(" where organization_oid = ?");
         queryListView.setSQL(sql.toString(),new Object[]{userOrgOid});
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
         sql.append("select name, p_client_bank_oid from bank_organization_group");
         sql.append(" where organization_oid = ?");
         queryListView.setSQL(sql.toString(),new Object[]{userOrgOid});
      }
      //Debug.debug("()"+sql.toString()+"\t userOrgOid:"+userOrgOid);
	  if(StringFunction.isNotBlank(userOrgOid)){
      queryListView.getRecords();

      orgDoc = queryListView.getXmlResultSet();
      Debug.debug(orgDoc.toString());
      orgName = orgDoc.getAttribute("/ResultSetRecord(0)/NAME");
	  }
      if(TradePortalConstants.OWNER_BOG.equals(ownershipLevel))
       {
         parentOrgOid = orgDoc.getAttribute("/ResultSetRecord(0)/P_CLIENT_BANK_OID");
       }
      /* pgedupudi - Rel-9.3 CR-976A 03/26/2015 Start*/
      /* IR#T36000039962 pgedupudi - Rel-9.3 CR-976A 06/04/2015 */
      if(TradePortalConstants.OWNER_BANK.equals(ownershipLevel)){
    	  StringBuilder bnkGrpRestctRlssql = new StringBuilder();
    	  bnkGrpRestctRlssql.append("select BANK_GRP_RESTRICT_RULES_OID,NAME   ");
    	  bnkGrpRestctRlssql.append(" from BANK_GRP_RESTRICT_RULES");
    	  bnkGrpRestctRlssql.append(" where P_CLIENT_BANK_OID in (?) ");
    	  bnkGrpRestctRlssql.append(" order by ");
    	  bnkGrpRestctRlssql.append(resMgr.localizeOrderBy("name"));
          Debug.debug(bnkGrpRestctRlssql.toString());
          List<Object> params=new ArrayList<Object>();
          /* IR#T36000040515 pgedupudi - Rel-9.3 CR-976A 06/17/2015 */
          params.add(orgDoc.getAttribute("/ResultSetRecord(0)/ORGANIZATION_OID"));
          bnkGrpRestrictRuleDoc = DatabaseQueryBean.getXmlResultSet(bnkGrpRestctRlssql.toString(),false,params);
      } 
      /* pgedupudi - Rel-9.3 CR-976A 03/26/2015 End*/
     } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
          if (queryListView != null) {
              queryListView.remove();
          }
      } catch (Exception e) {
          System.out.println("error removing querylistview in UserDetail.jsp");
      }
  }  // try/catch/finally block
 
 

  boolean passwordsUsed = true;
  String passwordSql;
//jgadela R90 IR T36000026319 - SQL INJECTION FIX
  List<Object> sqlParams = new ArrayList();

  // Get the authentication method for a global organization user (from global_organization)
  // or a client bank or BOG user (from client_bank)
  if (TradePortalConstants.OWNER_GLOBAL.equals(ownershipLevel)) {
     passwordSql = "select authentication_method from global_organization "
                 + "where organization_oid = ? ";
     sqlParams.add(userSession.getOwnerOrgOid());
  } else if(TradePortalConstants.OWNER_BANK.equals(ownershipLevel)){
  
        passwordSql = "select authentication_method "
                    + "from client_bank where organization_oid = ? ";
        sqlParams.add(userOrgOid);
  }else{
        passwordSql = "select authentication_method "
                    + "from client_bank where organization_oid = ? ";
        sqlParams.add(parentOrgOid);
  }

  DocumentHandler result = DatabaseQueryBean.getXmlResultSet(passwordSql, false, sqlParams);

  String corpAuthMethod = result.getAttribute("/ResultSetRecord(0)/AUTHENTICATION_METHOD");
  // Admin user actually does not have user specific authentication_method.  Should be empty string.
  // Use userAuthMethod to be consistent with UserSecuritySection.frag
  String userAuthMethod = user.getAttribute("authentication_method");

      // [START] CR-482
      String stricterPasswordSetting = null;
      String minimumPasswordLength = null;

      if(instanceHSMEnabled)
      {
            if(ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
            {
                  Debug.debug("[AdminUserDetail.jsp] User's ownership level is OWNER_GLOBAL");
                  stricterPasswordSetting = SecurityRules.getProponixRequireUpperLowerDigit();
                  Debug.debug("[AdminUserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
                  minimumPasswordLength = String.valueOf(SecurityRules.getProponixPasswordMinimumLengthLimit());
                  Debug.debug("[AdminUserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
            } // if(ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
            else
            {
                  ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class, "ClientBank");

                  // [START] PAUK020539189
                  String clientBankOid = null;
                  if(InstrumentServices.isNotBlank(userSession.getClientBankOid()))
                  {
                        clientBankOid = userSession.getClientBankOid();
                        Debug.debug("[AdminUserDetail.jsp] Got clientBankOid from userSession.getClientBankOid");
                  } // if(InstrumentServices.isNotBlank(userSession.getClientBankOid()))
                  else if(InstrumentServices.isNotBlank(doc.getAttribute("/In/User/user_oid")))
                  {
                        clientBankOid = doc.getAttribute("/In/User/user_oid");
                        Debug.debug("[AdminUserDetail.jsp] Got clientBankOid from doc.getAttribute(\"/In/User/user_oid\")");
                  } // else if(InstrumentServices.isNotBlank(doc.getAttribute("/In/User/user_oid")))
                  else if(InstrumentServices.isNotBlank(user.getAttribute("client_bank_oid")))
                  {
                        clientBankOid = user.getAttribute("client_bank_oid");
                        Debug.debug("[AdminUserDetail.jsp] Got clientBankOid from user.getAttribute(\"client_bank_oid\")");
                  } // else if(InstrumentServices.isNotBlank(user.getAttribute("client_bank_oid")))

                  if(clientBankOid != null)
                  {
                        Debug.debug("[AdminUserDetail.jsp] Getting client bank with oid = "+clientBankOid);
                        clientBank.getById(clientBankOid);

                        if(ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
                        {
                              Debug.debug("[AdminUserDetail.jsp] User's ownership level is OWNER_CORPORATE");
                              stricterPasswordSetting = clientBank.getAttribute("corp_password_addl_criteria");
                              Debug.debug("[AdminUserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
                              minimumPasswordLength = clientBank.getAttribute("corp_min_password_length");
                              Debug.debug("[AdminUserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
                        } // if(ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
                        else
                        {
                              stricterPasswordSetting = clientBank.getAttribute("bank_password_addl_criteria");
                              Debug.debug("[AdminUserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
                              minimumPasswordLength = clientBank.getAttribute("bank_min_password_len");
                              Debug.debug("[AdminUserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
                        } // else
                  } // if(clientBankOid != null)
            } // else

            // Default values for stricterPasswordSetting and minimumPasswordLength
            if((stricterPasswordSetting == null) || (stricterPasswordSetting.trim().length() == 0))
            {
                  stricterPasswordSetting = TradePortalConstants.INDICATOR_YES;
            }
            if((minimumPasswordLength == null) || (minimumPasswordLength.trim().length() == 0))
            {
                  minimumPasswordLength = "8";
            }
            // [END] PAUK020539189
      } // if(instanceHSMEnabled)
      // [END] CR-482


//jgadela rel8.3 CR501 [START]
  if(InstrumentServices.isNotBlank(userSession.getClientBankOid()) && !ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)){
  	adminUserDualCtrlReqdInd = CBCResult.getAttribute("/ResultSetRecord(0)/ADMIN_USER_DUAL_CTRL_REQD_IND");
  }else{
	  int slashAt = orgAndLevel.indexOf("/");
      userOrgOid = orgAndLevel.substring(0,slashAt);
      CBCResult = (DocumentHandler)CBCache.get(userOrgOid);
      if(CBCResult != null ){
      	adminUserDualCtrlReqdInd = CBCResult.getAttribute("/ResultSetRecord(0)/ADMIN_USER_DUAL_CTRL_REQD_IND");
      }
  }
//jgadela rel8.3 CR501 [END]
  
  // jgadela Rel 8.4 CR-854 T36000024797 [BEGIN]- Adding reporting language option                      
  String cbReportingLangOptionInd = null;                      
  if(InstrumentServices.isNotBlank(userSession.getClientBankOid())){
	  cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND");
  } 
  //jgadela Rel 8.4 CR-854 [END]


%>

<%-- ********************* HTML for page begins here *********************  --%>

<%-- <jsp:include page="/common/ButtonPrep.jsp" /> --%>

<%
  // onLoad is set to default the cursor to the UserId field but only if the
  // page is not in readonly mode.
  String onLoad = "";
  if (!isReadOnly) {
    onLoad = "document.AdminUserDetail.UserId.focus();";
  }
%>
<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>

<%--cquinton 10/8/2012 ir#5991 move hsm encryption javascript to footer --%>

<div class="pageMain">
<div class="pageContent">
<%
      String pageTitleKey = StringFunction.xssCharsToHtml(resMgr.getText("UsersAndSecurityMenu.AdminUser",TradePortalConstants.TEXT_BUNDLE));
      String helpUrl = "admin/admin_user_detail.htm";
%>

<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>
<%//--------------------- Sub header start --------------------- %>



<%  if ( insertMode) {
    pageTitleKey = StringFunction.xssCharsToHtml(resMgr.getText("AdminUserDetail.AdminNewUser", TradePortalConstants.TEXT_BUNDLE));
  } else {
    pageTitleKey = user.getAttribute("user_identifier");
  }
      // figures out how to return to the calling page
      StringBuffer parms = new StringBuffer();
      parms.append("&returnAction=");

      String returnLink = null;
    if ( isViewInReadOnly ) {
      parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToTradePortalHome", userSession.getSecretKey()));
      returnLink = "goToTradePortalHome";
    } else {
      parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToAdminRefDataHome", userSession.getSecretKey()));
      returnLink = formMgr.getLinkAsUrl("goToAdminRefDataHome", "&tab=user", response);
     }
%>

<div class="subHeaderDivider"></div>
<div class="transactionSubHeader">
  <span>
    <%=pageTitleKey%>
  </span>
  <div class="transactionReturn">

  </div>
</div>
<%//--------------------- Sub header end --------------------- %>




<form id="AdminUserDetail"  name="AdminUserDetail" method="post" data-dojo-type="dijit.form.Form"  action="<%=formMgr.getSubmitAction(response)%>">

<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">
  <input type=hidden value="" name=buttonName>
<%
      // [START] CR-482
      if (instanceHSMEnabled)
      {
%>
<%-- [START] CR-543 --%>
  <input type=hidden name="protectedNewPassword" value="">
  <input type=hidden name="encryptedRetypedNewPassword" value="">
  
  
<%-- [END] CR-543 --%>
  <input type=hidden name="passwordValidationError" value="">
<%
      } // if (instanceHSMEnabled)
      // [END] CR-482
%>

<input type=hidden name="UserType" value="BANK">
<%--jgadela rel8.3 CR501 [START] --%>
<input type=hidden name="PendingReferenceDataOid" value="<%=pendingReferenceDataOid %>">
<input type=hidden name="pendingChangedObjectOid" value="<%=StringFunction.xssCharsToHtml(pendingChangedObjectOid)%>">
<%
if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(adminUserDualCtrlReqdInd)){
%>
  <input type=hidden name="isDualControlRequired" value="Y">
<%
}else{
%>
  <input type=hidden name="isDualControlRequired" value="N">
<%}%>
<%--jgadela rel8.3 CR501 [END] --%>
<%
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("login_rights", loginRights);
  secureParms.put("opt_lock",  user.getAttribute("opt_lock"));
  secureParms.put("user_oid", oid);
  secureParms.put("ownership_level", "BANK");
  secureParms.put("SubAccessViewParentOrgData", TradePortalConstants.INDICATOR_NO);
%>
<input type=hidden value="N" name="DisplayExtendedListviewInd">

<%= formMgr.getFormInstanceAsInputField("AdminUserDetailForm", secureParms) %>


<%= widgetFactory.createSectionHeader( "1","SecurityProfileDetail.1General") %>
 <%-- include frag --%>
 <%@ include file="fragments/AdminUserDetail-General.frag" %>
</div>

<%= widgetFactory.createSectionHeader( "2","AdminUserDetail.Security2") %>
      <%@ include file="fragments/UserSecuritySection.frag" %>
</div>

<%= widgetFactory.createSectionHeader( "3","AdminUserDetail.AssignedTo3") %>
      <%-- include frag --%>
      <%@ include file="fragments/AdminUserDetail-AssignedTo.frag" %>
</div>

<%= widgetFactory.createSectionHeader( "4","AdminUserDetail.CorporateAccess4") %>
      <%-- include frag --%>
      <%@ include file="fragments/AdminUserDetail-CorporateAccess.frag" %>
</div>
<% if( isViewInReadOnly ){
      link = "goToTradePortalHome";
   }else{
      link = "goToAdminRefDataHome";
   }
%>

</div><%--formContent--%>
</div><%--formArea--%>

<%//jgadela rel8.3 CR501 [START] %>
<%
String cancelAction = "goToAdminRefDataHome";
 if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction")) || "ApproveButton".equalsIgnoreCase(buttonPressed) || "RejectButton".equalsIgnoreCase(buttonPressed)){
	 cancelAction = "goToRefDataApproval";
 }
%>
<%//jgadela rel8.3 CR501 [END] %>

  <%-- side bar --%>
<%String links = ""; %>
<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'AdminUserDetail'">
                              <jsp:include page="/common/RefDataSidebar.jsp">
                                    <jsp:param value="<%=links %>" name="links"/>
                                    <jsp:param name="showSaveButton" value="<%=showSave%>" />
                                    <jsp:param name="saveOnClick" value="none" />
                                    <jsp:param name="showSaveCloseButton" value="<%=saveClose%>" />
                                    <jsp:param name="saveCloseOnClick" value="none" />
                                	<jsp:param name="showDeleteButton" value="<%=showDelete%>" />
                                	<jsp:param name="showCloseButton" value="true" />
                             		<jsp:param name="showCloseButton" value="false" />
                            		<jsp:param name="showHelpButton" value="false" />
                            		<jsp:param name="cancelAction" value="<%=cancelAction%>" />
                            		<jsp:param name="showLinks" value="true" />
                             		<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
                             		<jsp:param name="error" value="<%= error%>" />
                             		<jsp:param name="showApproveButton" value="<%=showApprove%>" />
                             		<jsp:param name="showRejectButton" value="<%=showReject%>" />
                             		
                            </jsp:include>
</div> <%--closes sidebar area--%>

 </form>

</div><%--closes pageContent area--%>
</div><%--closes pageMain area--%>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
</jsp:include>

<%--cquinton 10/8/2012 ir#5991 start
    move hsm encryption javascript to footer --%>
<%
  //save behavior defaults to without hsm
  String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  String saveCloseOnClick = "common.setButtonPressed('SaveCloseTrans','0'); document.forms[0].submit();";

  // [START] CR-482
  // Include javascript functions for encryption
  if (instanceHSMEnabled) {
%><%-- [START] PAUK020539189 --%>
<script src="/portal/js/crypto/pidcrypt.js"></script>
<script src="/portal/js/crypto/pidcrypt_util.js"></script>
<script src="/portal/js/crypto/asn1.js"></script><%-- needed for parsing decrypted rsa certificate --%>
<script src="/portal/js/crypto/jsbn.js"></script><%-- needed for rsa math operations --%>
<script src="/portal/js/crypto/rng.js"></script><%-- needed for rsa key generation --%>
<script src="/portal/js/crypto/prng4.js"></script><%-- needed for rsa key generation --%>
<script src="/portal/js/crypto/rsa.js"></script><%-- needed for rsa en-/decryption --%>
<script src="/portal/js/crypto/md5.js"></script><%-- needed for key and iv generation --%>
<script src="/portal/js/crypto/aes_core.js"></script><%-- needed block en-/decryption --%>
<script src="/portal/js/crypto/aes_cbc.js"></script><%-- needed for cbc mode --%>
<script src="/portal/js/crypto/sha256.js"></script>
<script src="/portal/js/crypto/tpprotect.js"></script><%-- needed for cbc mode --%>
<%-- [END] PAUK020539189 --%>

<script>
  function encryptHsmFields() {
    var public_key = '-----BEGIN PUBLIC KEY-----\n<%= HSMEncryptDecrypt.getPublicKey() %>\n-----END PUBLIC KEY-----';
    <%--var public_key = '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvmMiU1irZ6aejIm0B+cKL9foNO

+iDdftWwFS0eUYmu8A4IJWxoq66hgJ1+z84TuKrkHYhWGYz1iK1sxsf5CgbN86UumXX99c3KhfP3YJrgHzohvpU0yt/HAlSLa7JgnuSTNYcQiaGW/y1lkuS8bzSw2Hb/yeXUI1yt7KhwTPjE9hJnh5Jk7YERebRXP7MCOUx

WJBuEjidmYIRjRkqzJ3FDEG1WByLjco5cmMaWZYdz9GUUEF3lIW7/Zb0kt40B2Ct0b3sIPhH5g6UF+nCzXxzBy5FOMMdUxh23re2ob88Ec+9AfYcD3qL9HS9IQ3ztAHTPZbhcwj8++8b9sXrfo/+QIDAQAB\n-----END

PUBLIC KEY-----';--%>
    var iv = '<%= HSMEncryptDecrypt.getNewIV() %>';
    <%--var iv = 'zhXaSTr+/981U4TPJSu/Yg==';--%>

    var protector = new TPProtect();
    document.AdminUserDetail.passwordValidationError.value = "";

    if((document.AdminUserDetail.Password1.value != "") || (document.AdminUserDetail.Password2.value != "")) <%-- PIUK012685963 - Run the following

logic only if the password fields are not blank --%>
    {

      <%-- Ensure that the new password and retyped password match --%>
      if (document.AdminUserDetail.passwordValidationError.value == "") {
        document.AdminUserDetail.passwordValidationError.value = protector.MatchingEntries({errorCode: "<%=
          TradePortalConstants.ENTRIES_DONT_MATCH %>", newPassword: document.AdminUserDetail.Password1.value, retypePassword:

document.AdminUserDetail.Password2.value});
      }

      <%-- Ensure that the new password is not the same as the user_identifier --%>
      if (document.AdminUserDetail.passwordValidationError.value == "") {
        document.AdminUserDetail.passwordValidationError.value = protector.IsSameAs({errorCode: "<%=
          TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.AdminUserDetail.Password1.value, sameAsString: "<%= user.getAttribute("user_identifier")

%>"});
      }

      <%-- Ensure that the new password is not the same as the first_name --%>
      if (document.AdminUserDetail.passwordValidationError.value == "") {
        document.AdminUserDetail.passwordValidationError.value = protector.IsSameAs({errorCode: "<%=
          TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.AdminUserDetail.Password1.value, sameAsString: "<%= user.getAttribute("first_name") %>"});
      }

      <%-- Ensure that the new password is not the same as the last_name --%>
      if (document.AdminUserDetail.passwordValidationError.value == "") {
        document.AdminUserDetail.passwordValidationError.value = protector.IsSameAs({errorCode: "<%=
          TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.AdminUserDetail.Password1.value, sameAsString: "<%= user.getAttribute("last_name") %>"});
      }

      <%-- Ensure that the new password is not the same as the login_id --%>
      if (document.AdminUserDetail.passwordValidationError.value == "") {
        document.AdminUserDetail.passwordValidationError.value = protector.IsSameAs({errorCode: "<%=
          TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.AdminUserDetail.Password1.value, sameAsString: "<%= user.getAttribute("login_id") %>"});
      }

      <%-- Ensure that the new password does not start or end with a space --%>
      if (document.AdminUserDetail.passwordValidationError.value == "") {
        document.AdminUserDetail.passwordValidationError.value = protector.StartEndWithSpace({errorCode: "<%=
          TradePortalConstants.PASSWORD_SPACE %>", newPassword: document.AdminUserDetail.Password1.value});
      }

      <%-- Ensure that the new password is the required length --%>
      if (document.AdminUserDetail.passwordValidationError.value == "") {
        document.AdminUserDetail.passwordValidationError.value = protector.IsRequiredLength({errorCode: "<%=
          TradePortalConstants.PASSWORD_TOO_SHORT %>", newPassword: document.AdminUserDetail.Password1.value, reqLength: <%= minimumPasswordLength %>});
      }

      <%-- Ensure that the new password meets the consecutive characters requirements --%>
      if (document.AdminUserDetail.passwordValidationError.value == "") {
        document.AdminUserDetail.passwordValidationError.value = protector.CheckConsecutiveChars({errorCode: "<%=
          TradePortalConstants.PASSWORD_CONSECUTIVE %>", newPassword: document.AdminUserDetail.Password1.value});
      }

<%  if(stricterPasswordSetting.equals(TradePortalConstants.INDICATOR_YES)) { %>
      if (document.AdminUserDetail.passwordValidationError.value == "") {
        document.AdminUserDetail.passwordValidationError.value = protector.CheckPasswordComplexity({errorCode: "<%=
          TradePortalConstants.PASSWORD_UPPER_LOWER_DIGIT %>", newPassword: document.AdminUserDetail.Password1.value});
      }
<%  } %>
      <%-- [START] CR-543 --%>
      <%-- Encrypt the MAC of the new password --%>
      var passwordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password:
        document.AdminUserDetail.Password1.value, challenge: iv});
      	var combinedNewPassword = passwordMac + '|' +  document.AdminUserDetail.Password1.value;
       	document.AdminUserDetail.protectedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv,
        data: combinedNewPassword});
      <%-- [END] CR-543 --%>

      if (document.AdminUserDetail.Password2) {
          document.AdminUserDetail.encryptedRetypedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.AdminUserDetail.Password2.value});  
       }  
      
      <%-- Blank out the password fields --%>
      document.AdminUserDetail.Password1.value = "";
      document.AdminUserDetail.Password2.value = "";
    } <%--  if(document.AdminUserDetail.Password1 && document.AdminUserDetail.Password2) --%>
  }
</script>
<%
  
  	//(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
    //saveOnClick = "encryptHsmFields();" + saveOnClick;
    //saveCloseOnClick = "encryptHsmFields();" + saveCloseOnClick;
  }
%>

<jsp:include page="/common/RefDataSidebarFooter.jsp" />
   
 
<%--cquinton 10/8/2012 ir#5991 end--%>

<%--cquinton 2/19/2013 ir#11753 modify javascript so we fields on change of radio selection, etc.
    note that we MUST defensively check these because when auth method is not PERUSER, the radio
    buttons are not created.
    Also: javascript should be just before closing body tag.--%>
<script>

  var local = {};
  var insertMode = <%=insertMode%>;

  require(["dojo/dom", "dojo/dom-class", "dojo/on", "dijit/registry", "dojo/ready", "dojo/domReady!"],
      function(dom, domClass, on, registry, ready) {

    <%-- make a widget required or not --%>
    <%-- manipulates both the widget validation and the formItem required class --%>
    local.setRequired = function(widget, requiredness) {
      if ( widget ) {
        var parentNode = widget.domNode.parentNode;
        if ( requiredness ) {
          widget.required = true;
          domClass.add(parentNode, "required");
        }
        else {
          widget.required = false;
          domClass.remove(parentNode, "required");
        }
      }
    };





    ready(function(){
    	<%-- On Page lode functionality - set divs to Display:none if isAdminUser true --%>
      	var certificateFieldsDiv = '';
      	var twoFAFieldsDiv = '';
      	var ssoFieldsDiv = '';
      	var passwordFieldsDiv = '';
      	var defaultDisplayDiv = '<%=defaultDisplayDiv%>';
      	
		if(dom.byId('CertificateFieldsDiv')){
      		certificateFieldsDiv = dom.byId('CertificateFieldsDiv');
      		certificateFieldsDiv.style.display = 'none';
      	}
		if(dom.byId('2FAFieldsDiv')){
			twoFAFieldsDiv = dom.byId('2FAFieldsDiv');
			twoFAFieldsDiv.style.display = 'none';
      	}
		if(dom.byId('SSOFieldsDiv')){
			ssoFieldsDiv = dom.byId('SSOFieldsDiv');
			ssoFieldsDiv.style.display = 'none';
      	}
		if(dom.byId('PasswordFieldsDiv')){
			passwordFieldsDiv = dom.byId('PasswordFieldsDiv');
			passwordFieldsDiv.style.display = 'none';
      	}
		
		<%-- based on defaultDisplayDiv set display: block for that particular div --%>
		<%-- defaultDisplayDiv criteria will be decided based on either the  --%>
		<%-- selected auth_method option if it is existing User or --%>
		<%-- selected radio button in ClientBank under Admin User Auth section if it is InsertMode --%>
		if (defaultDisplayDiv.localeCompare('PasswordFields') == 0){
			passwordFieldsDiv.style.display = 'block';
			domClass.remove("PWDcolumnRight", "columnRight");
			domClass.remove("UserLoginPwd", "indentHalf");
		}else if (defaultDisplayDiv.localeCompare('CertificateFields') == 0){
			certificateFieldsDiv.style.display = 'block';
			passwordFieldsDiv.style.display = 'block';
			domClass.remove("UserLoginPwd", "indentHalf");
		}else if (defaultDisplayDiv.localeCompare('SSOFields') == 0){
			ssoFieldsDiv.style.display = 'block';
		}else if (defaultDisplayDiv.localeCompare('2FAFields') == 0){
			twoFAFieldsDiv.style.display = 'block';
			passwordFieldsDiv.style.display = 'block';
			domClass.remove("UserLoginPwd", "indentHalf");
		}
<%
  //only do the security field manipulation if auth is per user
  if ( TradePortalConstants.AUTH_PERUSER.equals(corpAuthMethod) ) {
%>
   
      <%-- get the various authentication checkboxes --%>
      var authPass = registry.byId("TradePortalConstants.AUTH_PASSWORD");
      var auth2fa = registry.byId("TradePortalConstants.AUTH_2FA");
      var authSso = registry.byId("TradePortalConstants.AUTH_SSO");
      var authCert = registry.byId("TradePortalConstants.AUTH_CERTIFICATE");

      <%-- if peruser, the potentially required fields are --%>
      <%--  initially set as required so they are validation textboxes --%>
      <%-- here set them not required on page load --%>

      <%-- determine whether loginID should be required or not --%>
      if((authSso && authSso.checked) ||
         (authCert && authCert.checked) ) {
        <%-- loginId not required --%>
        var loginId = registry.byId("loginID");
        local.setRequired(loginId, false);
      }
      <%-- if nothing checked nothing is required --%>
      if((authPass && !authPass.checked) &&
         (auth2fa && !auth2fa.checked) &&
         (authCert && !authCert.checked) &&
         (authSso && !authSso.checked) ) {
        var loginId = registry.byId("loginID");
        local.setRequired(loginId, false);
      }

      <%-- now set onchange events so requiredness is set appropriately --%>
      if ( authPass ) {
        on(authPass, "change", function(){
          if(dom.byId("TradePortalConstants.AUTH_PASSWORD").checked){
            var loginId = registry.byId("loginID");
            local.setRequired(loginId, true);
          }
        });
      }
      if ( auth2fa ) {
        on(auth2fa, "change", function(){
          if(dom.byId("TradePortalConstants.AUTH_2FA").checked){
            var loginId = registry.byId("loginID");
            local.setRequired(loginId, true);
          }
        });
      }
      if ( authSso ) {
        on(authSso, "change", function(){
          if(dom.byId("TradePortalConstants.AUTH_SSO").checked){
            var loginId = registry.byId("loginID");
            local.setRequired(loginId, false);
          }
        });
      }
      if ( authCert ) {
        on(authCert, "change", function(){
          if(dom.byId("TradePortalConstants.AUTH_CERTIFICATE").checked){
            <%-- set loginId required = false --%>
            var loginId = registry.byId("loginID");
            local.setRequired(loginId, false);
          }
        });
      }


<%
  } //if auth per user
%>
    });
  });

 <%-- On Auth_method change event - set respective div to display: block based on user selection and hide the rest --%>
 function toggleAuthMethodFields(){
	 require(["dojo/dom", "dojo/dom-class", "dojo/domReady!"],
		      function(dom, domClass) {
  		var selectedAuthMethod = dom.byId('AuthenticationMethod').value;
  		
  		if (selectedAuthMethod.localeCompare('Password') == 0){
  			dom.byId('CertificateFieldsDiv').style.display = 'none';
  			dom.byId('PasswordFieldsDiv').style.display = 'block';
  			dom.byId('2FAFieldsDiv').style.display = 'none';
  			dom.byId('SSOFieldsDiv').style.display = 'none';
  			domClass.remove("PWDcolumnRight", "columnRight");
		}else if (selectedAuthMethod.localeCompare('Certificate') == 0){
			dom.byId('CertificateFieldsDiv').style.display = 'block';
  			dom.byId('PasswordFieldsDiv').style.display = 'block';
  			dom.byId('2FAFieldsDiv').style.display = 'none';
  			dom.byId('SSOFieldsDiv').style.display = 'none';
  			domClass.add("PWDcolumnRight", "columnRight");
		}else if (selectedAuthMethod.localeCompare('Single Sign-On') == 0){
			dom.byId('CertificateFieldsDiv').style.display = 'none';
  			dom.byId('PasswordFieldsDiv').style.display = 'none';
  			dom.byId('2FAFieldsDiv').style.display = 'none';
  			dom.byId('SSOFieldsDiv').style.display = 'block';
		}else if (selectedAuthMethod.localeCompare('2-Factor Authentication') == 0){
			dom.byId('CertificateFieldsDiv').style.display = 'none';
  			dom.byId('PasswordFieldsDiv').style.display = 'block';
  			dom.byId('2FAFieldsDiv').style.display = 'block';
  			dom.byId('SSOFieldsDiv').style.display = 'none';
  			domClass.add("PWDcolumnRight", "columnRight");
		}else{<%-- if IndividualConfig is selected display nothing --%>
			dom.byId('CertificateFieldsDiv').style.display = 'none';
  			dom.byId('PasswordFieldsDiv').style.display = 'none';
  			dom.byId('2FAFieldsDiv').style.display = 'none';
  			dom.byId('SSOFieldsDiv').style.display = 'none';
		}
	 });
  	}
</script>

<%--
  //for debugging
  out.println("corpAuthMethod="+corpAuthMethod);
--%>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

  // All done with the bean, so get rid of it.
  beanMgr.unregisterBean("User");
%>

<%
	if (instanceHSMEnabled){
%>

		<script>
			
			var preSave = function(){
 				encryptHsmFields();
 				return true;
			}
			
			var preSaveAndClose = function(){
  				encryptHsmFields();
 				return true;
			}
			
		</script>
<%
	}
%>

</body>
</html>
