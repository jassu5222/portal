<%--
***************************************************************************************************
Approve Reference data changes

Description:
      Displays listviews of ref data pending data which is waiting fore approval, can approve or reject  corporate customer additions, 
      changes or deletions as long as it is a different user than the one that made the initial add, change or delete request.  .    

Request Parameters:
      P_OWNER_OID  
***************************************************************************************************
--%>

<%-- comments, descriptions --%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.Map,java.util.Iterator,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"  class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<%
	//Rel 8.3 IR T36000020947 - New Help file created
	String helpLink = "";
	 if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
	    userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
	    helpLink = "admin/admin_ref_data_req_approver.htm";
	  }else{
	    userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
	    helpLink = "customer/ref_data_req_approval.htm";
	  }

      String initSearchParms="";
      //DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
      DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
	  String gridHtml = "";
	  String gridLayout = "";
	  String admingridLayout = ""; 
	  
	  //widgetfactory instance
      WidgetFactory widgetFactory = new WidgetFactory(resMgr);
      Map searchQueryMap =  userSession.getSavedSearchQueryData();
      Map savedSearchQueryData =null;
      if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		   savedSearchQueryData = (Map)searchQueryMap.get("PendingRefDataView");
	   }  
%>


<%-- Begin HTML - include navigation, etc --%>
<%-- Begin HTML header declarations (this includes the body tag) --%>

<jsp:include page="/common/Header.jsp">
      <jsp:param name="includeNavigationBarFlag"
                     value="<%=TradePortalConstants.INDICATOR_YES%>" />
      <jsp:param name="additionalOnLoad" value="" />
</jsp:include>


<div class="pageMainNoSidebar">
      <div class="pageContent">
                  <jsp:include page="/common/PageHeader.jsp">
                    <jsp:param name="titleKey" value="ApproveRefData.TitleRefData"/>
                    <jsp:param name="helpUrl"  value="<%=helpLink%>"/>
                  </jsp:include>

<%-- End HTML header declarations --%>
<%-- depending on where the current tab is selected ... hi-light it --%>

		
<form name="OrganisationAccessHome" method="post"    action="<%=formMgr.getSubmitAction(response)%>">
    
      <input type="hidden" name="NewSearch"  value="<%=TradePortalConstants.INDICATOR_YES%>">      
      <jsp:include page="/common/ErrorSection.jsp" />
	<div class="formContentNoSidebar">
		<div class="gridHeader">
			<span class="gridHeaderTitle">
				<%if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){ %>
				<%=resMgr.getText("ApproveRefData.TitleGridOne", TradePortalConstants.TEXT_BUNDLE)%>
				<%} %>
			</span>
			<span class="gridHeader-right">
				<jsp:include page="/common/gridShowCount.jsp">
			      <jsp:param name="gridId" value="PendingRefDataGridId" />
			    </jsp:include>
			    <span id="appRefRefresh" class="searchHeaderRefresh"></span>
	  			<%=widgetFactory.createHoverHelp("pmtFutureRefresh","RefreshImageLinkHoverText") %> 
			</span>
			<div style="clear:both"></div>
		</div>
		<% 
		/* gridHtml = dgFactory.createDataGrid("PendingRefDataGridId","PendingRefDataGrid", null);
        gridLayout = dgFactory.createGridLayout("PendingRefDataGridId","PendingRefDataGrid"); */
        gridHtml = dGridFactory.createDataGrid("PendingRefDataGridId","PendingRefDataGrid", null);
        gridLayout = dGridFactory.createGridLayout("PendingRefDataGridId","PendingRefDataGrid");
	   %>
	  <%=gridHtml %>
	</div>
	<%-- Prateep: Added condition to view at admin user level access only and not in customer access --%>
	  <% if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
			/* gridHtml = dgFactory.createDataGrid("AdminPendingRefDataGridId","AdminPendingRefDataGrid", null);
	        admingridLayout = dgFactory.createGridLayout("AdminPendingRefDataGridId","AdminPendingRefDataGrid"); */
	        gridHtml = dGridFactory.createDataGrid("AdminPendingRefDataGridId","AdminPendingRefDataGrid", null);
	        admingridLayout = dGridFactory.createGridLayout("AdminPendingRefDataGridId","AdminPendingRefDataGrid");
	   %>
	<div class="formContentNoSidebar">
		<div class="gridHeader">
			<span class="gridHeaderTitle">
				<%=resMgr.getText("ApproveRefData.TitleGridTwo", TradePortalConstants.TEXT_BUNDLE)%>
			</span>
			<span class="gridHeader-right">
				<jsp:include page="/common/gridShowCount.jsp">
		        <jsp:param name="gridId" value="AdminPendingRefDataGridId" />
		      	</jsp:include>
		      
		      <span id="appRefRefreshAdmin" class="searchHeaderRefresh"></span>
			  <%=widgetFactory.createHoverHelp("pmtFutureRefresh","RefreshImageLinkHoverText") %>  
			</span>
			<div style="clear:both"></div>
		</div>
	  <%=gridHtml %>
	   <%} %> 
	</div>
</form>

</div>
</div>

<%--cquinton Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton Portal Refresh - end--%>
<%@ include file="/common/GetSavedSearchQueryData.frag" %> 
<script>
  var local;
  var initSearchParms = "";
  var savedSort = savedSearchQueryData["sort"];
  require(["t360/OnDemandGrid", "dojo/dom-construct", "t360/datagrid", "dojo/dom", "dijit/registry"], function(onDemandGrid, domConstruct,t360grid, dom, registry) {
    
    local = {
      initializeDataGrid: function(dataGridId, viewName,  gridLayout){
        <%--get grid layout from the data grid factory--%>
        var gridLayout = gridLayout;
        var adminRefDataViewName = "";
        <%--set the initial search parms--%>
        initSearchParms = "<%=initSearchParms%>";
        if("" == savedSort || null == savedSort || "undefined" == savedSort)
			  savedSort = '0';
        <%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
        <%-- t360grid.createDataGrid(dataGridId, dataView, gridLayout, initSearchParms,savedSort); --%>
        onDemandGrid.createOnDemandGrid(dataGridId, viewName, gridLayout, initSearchParms,savedSort);        
      }
    };
    var gridLayout=<%=gridLayout%>;
    <% // jgadela 11/30/2014  IR Rel9.2 IR T36000034943 - Fixed the issue which is broken during T36000032596 fix.%>
    var vewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PendingRefDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 fix --%>
    local.initializeDataGrid("PendingRefDataGridId", vewName, gridLayout);
    <%-- Prateep: Added condition to view at admin user level access only and not in customer access --%>
    <% if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){%>
    	var admingridLayout=<%=admingridLayout%>;
    	var adminViewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("AdminPendingRefDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    	local.initializeDataGrid("AdminPendingRefDataGridId", adminViewName, admingridLayout);
    <%} %> 
    
  });
  
  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	      function(query, on, t360grid, t360popup){
	    query('#appRefRefresh').on("click", function() {
	      <%-- searchFutureTrans(); --%>
	      onDemandGrid.searchDataGrid("PendingRefDataGridId", "PendingRefDataView", initSearchParms);
	    });
	    
	    <%-- Prateep: Added condition to view at admin user level access only and not in customer access --%>
	    <% if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){%>
	    query('#appRefRefreshAdmin').on("click", function() {
	    	onDemandGrid.searchDataGrid("AdminPendingRefDataGridId", "AdminPendingRefDataView", initSearchParms);
		      });
	    <%} %>
	  });

	</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="PendingRefDataGridId" />
</jsp:include>

		  <%-- Prateep: Added condition to view at admin user level access only and not in customer access --%>
	      <% if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){%>
	      	<jsp:include page="/common/dGridShowCountFooter.jsp">
			  <jsp:param name="gridId" value="AdminPendingRefDataGridId" />
			</jsp:include>
	      <%} %> 

</body>
</html>
<%--  ****** End HTML  ****** --%>
<%-- ******* Clean Up ******* --%>
<%
      // Finally, reset the cached document to eliminate carryover of
      // information to the next visit of this page.
      formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
