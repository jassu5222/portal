<%--
**********************************************************************************
                              ArMatchingRule Detail

  Description:
     This page is used to maintain individual corporate customer organizations. It
     supports insert, update, and delete.  Errors found during a save are
     redisplayed on the same page. Successful updates return the user to the
     Organizations Home page.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2012
 *	   @ Developer Komal     
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,java.util.*"%>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	DocumentHandler errorDoc = new DocumentHandler();
	String focusField = "";
	DocumentHandler xmlDoc = null;
	QueryListView queryListView = null;
	QueryListView qqueryListView = null;
	StringBuffer sqlQuery = null;
	Hashtable secureParams = new Hashtable();
	boolean getDataFromXmlDocFlag = false;
	boolean showDeleteButtonFlag = true;
	boolean showSaveButtonFlag = true;
	boolean retrieveFromDBFlag = false;
	boolean hasMultipleBuyerNameAlias = false;
	boolean insertModeFlag = true;
	boolean isReadOnly = false;
	boolean errorFlag = false;
	boolean isBuyerNameIDReadOnly = false;
	String userSecurityRights = null;
	String editArMatchingRuleName = null;
	String aroid = null;
	String buttonPressed = "";
	Vector error = null;
	String maxError = "";
	String defaultText = null;
	String userOrgOid = null;
	String userLocale = null;
	String userType = null;
	String sValue = null;
	String editArMatchingRuleTimeoutAutoSaveInd = null;
	String defaultAuthMethod = null;
	int iLoop;
	int jLoop;
	int i;
	String userSecurityType1 = "";
	final int DEFAULT_ROWS = 4;
	int numItems = 0;
	int numItemsName = 0;
	int numItemsID = 0;
	int numPayInvInstr = 0;
	String attributeOID = null;

	String oid = "";
	String loginRights = userSession.getSecurityRights();
	String ownerLevel = userSession.getOwnershipLevel();
	String desigBankOid = null;
	Vector vVector;
	Vector iVector;

	int aliasNameCount = 0;
	int aliasIdCount = 0;
	int marginRulesCount = 4;
	int tempMod = 0;
	String refDataHome = "goToRefDataHome"; //used for the close button and Breadcrumb link
	boolean showDelete = true;
	boolean showSave = true;
	boolean showSaveClose = true;
	String onLoad = null;
	String javascriptLabel;
	String cancelAction;
	String trade_matching_rule_oid = "";
	String currency_code = "";
	String inst_type = "";
	String threshold_amt = "";
	String rate_type = "";
	String margin = "";
	int DEFAULT_MARGINRULE_COUNT = 4;
	List tradePartnerMarginList = new ArrayList(DEFAULT_MARGINRULE_COUNT);
	List<PayableInvPayInstWebBean> payableInvPayInstList = new ArrayList<PayableInvPayInstWebBean>(); //Nar Cr 913 Release9.0.0.0 23-Jan-2014
	int row2Count = 0;
	int payInvRowCount = 4;
	TradePartnerMarginRulesWebBean tradePartnerMargin = null;
	PayableInvPayInstWebBean payableInvPayInst = null; //Nar Cr 913 Release9.0.0.0 23-Jan-2014
	String mValue = null;
	int mLoop;
	int numMarginItems;

	// Register a corporate org web bean
	//  beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");

	//corporateOrg = (CorporateOrganizationWebBean) beanMgr.getBean("CorporateOrganization");

	//Matching Rule Web Bean
	beanMgr.registerBean(
			"com.ams.tradeportal.busobj.webbean.ArMatchingRuleWebBean",
			"ArMatchingRule");
	ArMatchingRuleWebBean editArMatchingRule = (ArMatchingRuleWebBean) beanMgr
			.getBean("ArMatchingRule");

	// Retrieve the user's locale, security rights, and organization oid
	userSecurityRights = userSession.getSecurityRights();
	userLocale = userSession.getUserLocale();
	userOrgOid = userSession.getOwnerOrgOid();
	/* 	clientBankOid      = userSession.getClientBankOid(); */
	isReadOnly = !SecurityAccess.hasRights(userSecurityRights,
			SecurityAccess.MAINTAIN_RECEIVABLES_MATCH_RULES);

	// Get the document from the cache.  We'll use it to repopulate the screen if the

	// user was entering data with errors.
	xmlDoc = formMgr.getFromDocCache();
	Debug.debug("doc from cache is " + xmlDoc.toString());
	if (xmlDoc.getDocumentNode("/In/Update/ButtonPressed") != null) {
		buttonPressed = xmlDoc.getAttribute("/In/Update/ButtonPressed");
		error = xmlDoc.getFragments("/Error/errorlist/error");
	}

	if (xmlDoc.getDocumentNode("/In/PayRemit") == null)
		cancelAction = "PaymentMatchResponse";
	else
		cancelAction = "PaymentMatchResponse";

	if (xmlDoc.getDocumentNode("/In/ArMatchingRule") == null) {
		// The document does not exist in the cache.  We are entering the page from
		// the listview page (meaning we're opening an existing item or creating
		// a new one.) or back from the multiple address page
		if (request.getParameter("oid") != null) {
			aroid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
		} else {
			//aroid = xmlDoc.getAttribute("/In/ArMatchingRule/corp_org_oid");//null if NOT back from address page
			aroid = "0";
		}

		if (aroid == null) {
			/* getDataFromXmlDocFlag = false;
			retrieveFromDBFlag = false; */
			insertModeFlag = true;
			aroid = "0";
		} else if (aroid.equals("0")) {
			/* getDataFromXmlDocFlag = false;
			retrieveFromDBFlag = false; */
			insertModeFlag = true;
		} else {
			// We have a good oid so we can retrieve.
			/* getDataFromXmlDocFlag = false; */
			retrieveFromDBFlag = true;
			/* insertModeFlag = false; */
		}
	} else {
		maxError = xmlDoc.getAttribute("/Error/maxerrorseverity");
		if(maxError != null && maxError.compareTo( String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0){
	        //No error. Get Data from DB
	        aroid = xmlDoc.getAttribute("/Out/ArMatchingRule/ar_matching_rule_oid");
	        //if this was an update not an insert, we need to get from in
            if (aroid == null ) {
                aroid = xmlDoc.getAttribute("/In/ArMatchingRule/ar_matching_rule_oid");
            }     
	        getDataFromXmlDocFlag=false;
	        retrieveFromDBFlag = true;
	        insertModeFlag = false;
	    }else{
	        //Error exists. Get Data from Doc
	        aroid = xmlDoc.getAttribute("/In/ArMatchingRule/ar_matching_rule_oid");
	        if ("0".equals(aroid)) {
	            insertModeFlag = true;
	        } else {
	            // Not in insert mode, use oid from output doc.
	            insertModeFlag = false;
	        }
	        retrieveFromDBFlag = false;
	        getDataFromXmlDocFlag = true;
	    }
	}
	//Srinivasu_D fix of IR#T36000014556 Rel8.2 start
	if((xmlDoc.getAttribute("/In/Update/ButtonPressed")!=null) && xmlDoc.getAttribute("/Out/ArMatchingRule/ar_matching_rule_oid")!=null && 
	(maxError != null && maxError.compareTo( String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0)){
		
		aroid=xmlDoc.getAttribute("/Out/ArMatchingRule/ar_matching_rule_oid");
		editArMatchingRule.setAttribute("ar_matching_rule_oid",aroid);
		getDataFromXmlDocFlag=false;
		retrieveFromDBFlag = true;
	}
	// Srinivasu_D fix of IR#T36000014556 Rel8.2 end
	if (retrieveFromDBFlag) {
		// Attempt to retrieve the item.  It should always work.  Still, we'll
		// check for a blank oid -- this indicates record not found.  Set to insert
		// mode and display error.

		getDataFromXmlDocFlag = false;
		editArMatchingRule.getById(aroid);
		if (editArMatchingRule.getAttribute("ar_matching_rule_oid")
				.equals("")) {
			insertModeFlag = true;
			aroid = "0";
			getDataFromXmlDocFlag = false;
		} else {
			insertModeFlag = false;
		}
	}
	/* numItemsName = DEFAULT_ROWS;
	numItemsID = DEFAULT_ROWS; */
	aliasIdCount = numItemsID;
	aliasNameCount = numItemsName;
	if (insertModeFlag)
		aroid = "0";
	// ArBuyerNameAlias Web Bean
	ArBuyerNameAliasWebBean arbuyernamealias[] = new ArBuyerNameAliasWebBean[51];
	// ArBuyerIdAlias Web Bean
	ArBuyerIdAliasWebBean arbuyeridalias[] = new ArBuyerIdAliasWebBean[51];

	if (!retrieveFromDBFlag && !getDataFromXmlDocFlag) {
		for (int count = 0; count < DEFAULT_ROWS; count++) {
			tradePartnerMarginList.add(beanMgr.createBean(TradePartnerMarginRulesWebBean.class, "TradePartnerMarginRules"));
			
			payableInvPayInstList.add(beanMgr.createBean(PayableInvPayInstWebBean.class, "PayableInvPayInst"));  //Nar Cr 913 Release9.0.0.0 23-Jan-2014
		}
	}

	if (getDataFromXmlDocFlag) {
		try {
			editArMatchingRule.populateFromXmlDoc(xmlDoc
					.getFragment("/In"));
		} catch (Exception e) {
			System.out
					.println("Contact Administrator: Unable to get CorporateOrganization attributes "
							+ "for oid: " + aroid + " " + e.toString());
		}
		//     Populate arbuyernamealias[] bean with ArBuyerNameAlias info from document handler
		vVector = xmlDoc
				.getFragments("/In/ArMatchingRule/ArBuyerNameAliasList");
		numItemsName = vVector.size();

		Debug.debug(xmlDoc.toString());

		DocumentHandler acctXMLDoc = null;
		for (iLoop = 0; iLoop < numItemsName; iLoop++) {
			acctXMLDoc = (DocumentHandler) vVector.elementAt(iLoop);

			arbuyernamealias[iLoop] = beanMgr.createBean(ArBuyerNameAliasWebBean.class, "ArBuyerNameAlias");
			try {
				sValue = acctXMLDoc
						.getAttribute("/ar_buyer_name_alias_oid");
			} catch (Exception e) {
				sValue = "";
			}

			arbuyernamealias[iLoop].setAttribute(
					"ar_buyer_name_alias_oid", sValue);

			acctXMLDoc = (DocumentHandler) vVector.elementAt(iLoop);
			try {
				sValue = acctXMLDoc.getAttribute("/buyer_name_alias");

			} catch (Exception e) {
				sValue = "";
			}

			arbuyernamealias[iLoop].setAttribute("buyer_name_alias",
					sValue);
		}

		//     Populate arbuyeridalias[] bean with ArBuyerIdAlias info from document handler
		iVector = xmlDoc
				.getFragments("/In/ArMatchingRule/ArBuyerIdAliasList");
		numItemsID = iVector.size();

		Debug.debug(xmlDoc.toString());

		for (jLoop = 0; jLoop < numItemsID; jLoop++) {
			DocumentHandler acctDoc = (DocumentHandler) iVector
					.elementAt(jLoop);
			arbuyeridalias[jLoop] = beanMgr.createBean(ArBuyerIdAliasWebBean.class, "ArBuyerIdAlias");
			try {
				sValue = acctDoc.getAttribute("/ar_buyer_id_alias_oid");
			} catch (Exception e) {
				sValue = "";
			}
			arbuyeridalias[jLoop].setAttribute("ar_buyer_id_alias_oid",
					sValue);

			acctDoc = (DocumentHandler) iVector.elementAt(jLoop);
			try {
				sValue = acctDoc.getAttribute("/buyer_id_alias");
			} catch (Exception e) {
				sValue = "";
			}
			arbuyeridalias[jLoop]
					.setAttribute("buyer_id_alias", sValue);
		}

		// Populate tradePartnerMargin[] bean with TradePartnerMarginRules info from document handler
		iVector = xmlDoc.getFragments("/In/ArMatchingRule/TradePartnerMarginRulesList");
		numItems = iVector.size();
		Debug.debug("Size = " + numItems);
		Debug.debug(xmlDoc.toString());
		marginRulesCount = numItems;

		for (jLoop = 0; jLoop < numItems; jLoop++) {
			tradePartnerMargin = beanMgr.createBean(TradePartnerMarginRulesWebBean.class, "TradePartnerMarginRules");
			DocumentHandler marginDoc = (DocumentHandler) iVector.elementAt(jLoop);

			mValue = marginDoc.getAttribute("/trade_margin_rule_oid");
			tradePartnerMargin.setAttribute("trade_margin_rule_oid", mValue);

			mValue = marginDoc.getAttribute("/currency_code");
			tradePartnerMargin.setAttribute("currency_code", mValue);

			mValue = marginDoc.getAttribute("/margin_instrument_type");
			tradePartnerMargin.setAttribute("margin_instrument_type",
					mValue);

			mValue = marginDoc.getAttribute("/threshold_amount");
			tradePartnerMargin.setAttribute("threshold_amount", mValue);

			mValue = marginDoc.getAttribute("/rate_type");
			tradePartnerMargin.setAttribute("rate_type", mValue);

			mValue = marginDoc.getAttribute("/margin");
			tradePartnerMargin.setAttribute("margin", mValue);

			tradePartnerMarginList.add(tradePartnerMargin);

		}
		
		if(numItems < DEFAULT_MARGINRULE_COUNT) {
			  for(mLoop = numItems; mLoop < DEFAULT_MARGINRULE_COUNT; mLoop++) {
				  tradePartnerMarginList.add(beanMgr.createBean(TradePartnerMarginRulesWebBean.class, "TradePartnerMarginRules"));
			  }
		  }
		
		//Nar Cr 913 Release9.0.0.0 23-Jan-2014 Start
		// Populate PayableInvPayInst bean with PayableInvPayInst info from document handler
		iVector = xmlDoc.getFragments("/In/ArMatchingRule/PayableInvPayInstList");
		numPayInvInstr = iVector.size();
		if (numPayInvInstr > DEFAULT_ROWS) {
			 payInvRowCount = numPayInvInstr;
		}

		for (jLoop = 0; jLoop < numPayInvInstr; jLoop++) {
			payableInvPayInst = beanMgr.createBean(PayableInvPayInstWebBean.class, "PayableInvPayInst");
			DocumentHandler PayableInvPayInstrDoc = (DocumentHandler) iVector.elementAt(jLoop);
			payableInvPayInst.setAttribute("payable_inv_pay_inst_oid", PayableInvPayInstrDoc.getAttribute("/payable_inv_pay_inst_oid"));
			payableInvPayInst.setAttribute("currency_code", PayableInvPayInstrDoc.getAttribute("/currency_code"));
			payableInvPayInst.setAttribute("inv_pay_instruction_ind", PayableInvPayInstrDoc.getAttribute("/inv_pay_instruction_ind"));
			payableInvPayInstList.add(payableInvPayInst);
		}
		
		if(numPayInvInstr < DEFAULT_ROWS) {
			  for(mLoop = numPayInvInstr; mLoop < DEFAULT_ROWS; mLoop++) {
				  payableInvPayInstList.add(beanMgr.createBean(PayableInvPayInstWebBean.class, "PayableInvPayInst"));
			  }
		}
		//Nar Cr 913 Release9.0.0.0 23-Jan-2014 End
	}
	String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO
			: TradePortalConstants.INDICATOR_YES;
	// Determine whether the Delete and Save buttons should be displayed
	if (isReadOnly || insertModeFlag) {
		showDelete = false;

		if (isReadOnly) {
			showSave = false;
			showSaveClose = false; // Nar IR-T36000015158 Rel 8.2
		}
	}

	// Set the text to display next to the Corporate Customers link based on whether the Create
	// button was selected or an existing Corporate Organization was selected from the previous page
	// Auto save the form when time-out if not readonly.
	// (Header.jsp will check for auto-save setting of the corporate org).

	if (!insertModeFlag || errorFlag) {
		focusField = "buyer_name";
	} else {
		focusField = "";
	}

	//     Populate arbuyernamealias[] bean with ArBuyerNameAlias info from document handler
	StringBuffer sql = new StringBuffer();

	queryListView = null;

	if (retrieveFromDBFlag) {
		try {
			queryListView = (QueryListView) EJBObjectFactory
					.createClientEJB(formMgr.getServerLocation(),
							"QueryListView");

			sql.append("select buyer_name_alias,ar_buyer_name_alias_oid");
			sql.append(" from AR_BUYER_NAME_ALIAS");
			sql.append(" where p_ar_matching_rule_oid in(" + aroid
					+ ")");
			sql.append(" order by ");
			//RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 changed sort by field - Start
			sql.append("ar_buyer_name_alias_oid");
			//RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 changed sort by field - End
			Debug.debug("SQL query: ");
			Debug.debug(sql.toString());

			queryListView.setSQL(sql.toString());
			queryListView.getRecords();

			DocumentHandler nameList = new DocumentHandler();
			nameList = queryListView.getXmlResultSet();
			Debug.debug(nameList.toString());

			vVector = nameList.getFragments("/ResultSetRecord");
			numItemsName = vVector.size();

			for (iLoop = 0; iLoop < numItemsName; iLoop++) {
				DocumentHandler acctDoc = (DocumentHandler) vVector
						.elementAt(iLoop);
				arbuyernamealias[iLoop] = beanMgr.createBean(ArBuyerNameAliasWebBean.class, "ArBuyerNameAlias");
				try {
					sValue = acctDoc
							.getAttribute("/AR_BUYER_NAME_ALIAS_OID");
				} catch (Exception e) {
					sValue = "";
				}
				arbuyernamealias[iLoop].setAttribute(
						"ar_buyer_name_alias_oid", sValue);
				try {
					sValue = acctDoc.getAttribute("/BUYER_NAME_ALIAS");
				} catch (Exception e) {
					sValue = "";
				}
				arbuyernamealias[iLoop].setAttribute(
						"buyer_name_alias", sValue);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (queryListView != null) {
					queryListView.remove();
				}
			} catch (Exception e) {
				System.out
						.println("Error removing queryListView in ArMatchingRule.jsp");

			}
		}

		//     Populate arbuyeridalias[] bean with ArBuyerIdAlias info from document handler
		StringBuffer ssql = new StringBuffer();

		qqueryListView = null;

		try {
			qqueryListView = (QueryListView) EJBObjectFactory
					.createClientEJB(formMgr.getServerLocation(),
							"QueryListView");

			ssql.append("select buyer_id_alias,ar_buyer_id_alias_oid");
			ssql.append(" from AR_BUYER_ID_ALIAS");
			ssql.append(" where p_ar_matching_rule_oid in(" + aroid
					+ ")");
			ssql.append(" order by ");
			//RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 changed sort by field - Start
			ssql.append("ar_buyer_id_alias_oid");
			//RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 changed sort by field - End
			Debug.debug("SQL query: ");
			Debug.debug(ssql.toString());

			qqueryListView.setSQL(ssql.toString());
			qqueryListView.getRecords();
			Debug.debug("No. of records:: ");

			DocumentHandler nameList = new DocumentHandler();
			nameList = qqueryListView.getXmlResultSet();
			Debug.debug(nameList.toString());

			iVector = nameList.getFragments("/ResultSetRecord");
			numItemsID = iVector.size();
			Debug.debug("SQL query numItems: " + numItemsID);
			for (jLoop = 0; jLoop < numItemsID; jLoop++) {
				DocumentHandler acctDoc = (DocumentHandler) iVector
						.elementAt(jLoop);
				arbuyeridalias[jLoop] =  beanMgr.createBean(ArBuyerIdAliasWebBean.class, "ArBuyerIdAlias");
				try {
					sValue = acctDoc
							.getAttribute("/AR_BUYER_ID_ALIAS_OID");
				} catch (Exception e) {
					sValue = "";
				}
				arbuyeridalias[jLoop].setAttribute(
						"ar_buyer_id_alias_oid", sValue);

				try {
					sValue = acctDoc.getAttribute("/BUYER_ID_ALIAS");
				} catch (Exception e) {
					sValue = "";
				}
				arbuyeridalias[jLoop].setAttribute("buyer_id_alias",
						sValue);

			}
			//jgadela R90 IR T36000026319 - SQL INJECTION FIX
			aliasIdCount = (DatabaseQueryBean.getCount(
					"ar_buyer_id_alias_oid", "AR_BUYER_ID_ALIAS",
					"p_ar_matching_rule_oid = ? ", false, new Object[]{ aroid}));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (qqueryListView != null) {
					qqueryListView.remove();
				}
			} catch (Exception e) {
				System.out
						.println("Error removing queryListView in ArMatchingRule.jsp");
			}
		}
		//  Populate tradePartnerMargin[] bean with TradePartnerMatchingRule info from document handler
		StringBuffer marginSql = new StringBuffer();

		qqueryListView = null;

		try {
			qqueryListView = (QueryListView) EJBObjectFactory
					.createClientEJB(formMgr.getServerLocation(),
							"QueryListView");
			marginSql
					.append("select trade_margin_rule_oid,currency_code, margin_instrument_type, threshold_amount,rate_type,margin,p_ar_matching_rule_oid");
			marginSql.append(" from TRADING_PARTNER_MARGIN_RULES");
			marginSql.append(" where p_ar_matching_rule_oid in("
					+ aroid + ")");
			marginSql.append(" order by ");
			marginSql.append(resMgr
					.localizeOrderBy("trade_margin_rule_oid"));
			Debug.debug("SQL query: ");
			Debug.debug(marginSql.toString());

			qqueryListView.setSQL(marginSql.toString());
			qqueryListView.getRecords();
			Debug.debug("No. of records are below: ");

			DocumentHandler nameList = new DocumentHandler();
			nameList = qqueryListView.getXmlResultSet();
			Debug.debug(nameList.toString());

			iVector = nameList.getFragments("/ResultSetRecord");
			numItems = iVector.size();

			if (numItems > DEFAULT_MARGINRULE_COUNT) {
				row2Count = numItems;
			}

			for (mLoop = 0; mLoop < numItems; mLoop++) {
				tradePartnerMargin = beanMgr.createBean(TradePartnerMarginRulesWebBean.class, "TradePartnerMarginRules");
				DocumentHandler marginDoc = (DocumentHandler) iVector
						.elementAt(mLoop);
				mValue = marginDoc
						.getAttribute("/TRADE_MARGIN_RULE_OID");
				tradePartnerMargin.setAttribute(
						"trade_margin_rule_oid", mValue);

				mValue = marginDoc.getAttribute("/CURRENCY_CODE");
				tradePartnerMargin
						.setAttribute("currency_code", mValue);

				mValue = marginDoc
						.getAttribute("/MARGIN_INSTRUMENT_TYPE");
				tradePartnerMargin.setAttribute(
						"margin_instrument_type", mValue);

				mValue = marginDoc.getAttribute("/THRESHOLD_AMOUNT");
				tradePartnerMargin.setAttribute("threshold_amount",
						mValue);

				mValue = marginDoc.getAttribute("/RATE_TYPE");
				tradePartnerMargin.setAttribute("rate_type", mValue);

				mValue = marginDoc.getAttribute("/MARGIN");
				tradePartnerMargin.setAttribute("margin", mValue);

				tradePartnerMarginList.add(tradePartnerMargin);
			}
			// Fill Empty Objects to make it Count 4.
			if (numItems < DEFAULT_MARGINRULE_COUNT) {
				for (mLoop = numItems; mLoop < DEFAULT_MARGINRULE_COUNT; mLoop++) {
					tradePartnerMarginList.add(beanMgr.createBean(TradePartnerMarginRulesWebBean.class, "TradePartnerMarginRules"));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (qqueryListView != null) {
					qqueryListView.remove();
				}
			} catch (Exception e) {
				System.out
						.println("Error removing queryListView in ArMatchingRule.jsp");
			}
		}
		//Nar Cr 913 Release9.0.0.0 23-Jan-2014 Start
	    // Populate PayableInvPayInst bean with PayableInvPayInst info from document handler
		StringBuilder payableInvPayInstSql = new StringBuilder();
		qqueryListView = null;
		try {
			qqueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
			payableInvPayInstSql.append(" SELECT payable_inv_pay_inst_oid, currency_code, inv_pay_instruction_ind, p_ar_matching_rule_oid ");
			payableInvPayInstSql.append(" FROM payable_inv_pay_inst ");
			payableInvPayInstSql.append(" WHERE p_ar_matching_rule_oid =?"  );
			payableInvPayInstSql.append(" ORDER BY payable_inv_pay_inst_oid ");
			Debug.debug(payableInvPayInstSql.toString());

			qqueryListView.setSQL(payableInvPayInstSql.toString(),new Object[]{aroid});
			qqueryListView.getRecords();

			DocumentHandler nameList = qqueryListView.getXmlResultSet();
			iVector = nameList.getFragments("/ResultSetRecord");
			numPayInvInstr = iVector.size();
			if (numItems > DEFAULT_ROWS) {
			 payInvRowCount = numPayInvInstr;
			}

			for (mLoop = 0; mLoop < numPayInvInstr; mLoop++) {
				payableInvPayInst = beanMgr.createBean(PayableInvPayInstWebBean.class, "PayableInvPayInst");
		       DocumentHandler PayableInvPayInstrDoc = (DocumentHandler) iVector.elementAt(mLoop);
		       payableInvPayInst.setAttribute("payable_inv_pay_inst_oid", PayableInvPayInstrDoc.getAttribute("/PAYABLE_INV_PAY_INST_OID"));
		       payableInvPayInst.setAttribute("currency_code", PayableInvPayInstrDoc.getAttribute("/CURRENCY_CODE"));
		       payableInvPayInst.setAttribute("inv_pay_instruction_ind", PayableInvPayInstrDoc.getAttribute("/INV_PAY_INSTRUCTION_IND"));
		       payableInvPayInstList.add(payableInvPayInst);
			}
			// Fill Empty Objects to make it Count 4.
			if (numPayInvInstr < DEFAULT_ROWS) {
				for (mLoop = numPayInvInstr; mLoop < DEFAULT_ROWS; mLoop++) {
					payableInvPayInstList.add(beanMgr.createBean(PayableInvPayInstWebBean.class, "PayableInvPayInst"));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (qqueryListView != null) {
					qqueryListView.remove();
				}
			} catch (Exception e) {
				e.printStackTrace();
				Debug.debug("Error removing queryListView in ArMatchingRule.jsp");
			}
		}
		//Nar Cr 913 Release9.0.0.0 23-Jan-2014 End
	}
	//BSL IR NEUM041853493 04/23/2012 Rel 8.0 BEGIN
	if (InstrumentServices.isNotBlank(editArMatchingRule
			.getAttribute("ar_matching_rule_oid"))) {
		isBuyerNameIDReadOnly = !"0".equals(editArMatchingRule
				.getAttribute("ar_matching_rule_oid"));
	}

	if ((insertModeFlag || errorFlag) && !isBuyerNameIDReadOnly) {
		onLoad = "document.ArMatchingRuleDetail.buyer_name.focus();";
	} else {
		onLoad = "";
	}
	//BSL IR NEUM041853493 04/23/2012 Rel 8.0 END
	if (numItemsName < DEFAULT_ROWS) {
		for (iLoop = numItemsName; iLoop < DEFAULT_ROWS; iLoop++) {
			arbuyernamealias[iLoop] = beanMgr.createBean(ArBuyerNameAliasWebBean.class, "ArBuyerNameAlias");
		}
		numItemsName = DEFAULT_ROWS;
	} else if (numItemsName % 2 == 1) {
		arbuyernamealias[numItemsName] = beanMgr.createBean(ArBuyerNameAliasWebBean.class, "ArBuyerNameAlias");
	}
	if (numItemsID < DEFAULT_ROWS) {
		for (jLoop = numItemsID; jLoop < DEFAULT_ROWS; jLoop++) {
			arbuyeridalias[jLoop] = beanMgr.createBean(ArBuyerIdAliasWebBean.class, "ArBuyerIdAlias");
		}
		numItemsID = DEFAULT_ROWS;
	} else if (numItemsName % 2 == 1) {
		arbuyeridalias[numItemsName] = beanMgr.createBean(ArBuyerIdAliasWebBean.class, "ArBuyerIdAlias");
	}
%>

<%
	Debug.debug("***START SECURE PARAMS********* ");
	secureParams.put("oid", aroid);
	secureParams.put("corp_org_oid", userSession.getOwnerOrgOid());
	secureParams.put("login_oid", userSession.getUserOid());
	secureParams.put("login_rights", userSession.getSecurityRights());
	secureParams.put("opt_lock",
			editArMatchingRule.getAttribute("opt_lock"));
%> 	
<%
	if (InstrumentServices.isNotBlank(editArMatchingRule
			.getAttribute("ar_matching_rule_oid")))
		isBuyerNameIDReadOnly = editArMatchingRule.getAttribute(
				"ar_matching_rule_oid").equals("0") ? false : true;

	if ((insertModeFlag || errorFlag) && !isBuyerNameIDReadOnly) {
		focusField = "buyer_name";
	} else
		focusField = "";
%>


<%--  ****** Begin HTML  ****** --%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />

	<jsp:param name="includeErrorSectionFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
</jsp:include>


<div class="pageMain">
  <div class="pageContent">
<%
  String pageTitleKey = resMgr.getText(
    "ArMatchingRuleDetail.MatchingRules",
    TradePortalConstants.TEXT_BUNDLE);
  String helpUrl = "customer/rec_matching_rules.htm";
%>

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
      <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
    </jsp:include>
<%--------------------- Sub header start ---------------------%>
<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (insertModeFlag) {
    subHeaderTitle = resMgr.getText(
      "ArMatchingRuleDetail.NewMatchingRule",
      TradePortalConstants.TEXT_BUNDLE);
  } 
  else {
    subHeaderTitle = editArMatchingRule.getAttribute("buyer_name");
  }

  // figures out how to return to the calling page
  StringBuffer parms = new StringBuffer();
  parms.append("&returnAction=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes(
    "goToRefDataHome", userSession.getSecretKey()));
  String returnLink = formMgr.getLinkAsUrl("goToRefDataHome",
    parms.toString(), response);
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>

<form id="ArMatchingRuleDetailForm" name="ArMatchingRuleDetailForm" data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden value="" name=buttonName>
<jsp:include page="/common/ButtonPrep.jsp" />
<div class="formArea">
	<jsp:include page="/common/ErrorSection.jsp" />


	<div class="formContent">		<%--  Form Content Start --%>
 
		
   <%=widgetFactory.createSectionHeader("1",
					"ArMatchingRuleDetail.Aliases")%>
          	<%
          		if (isBuyerNameIDReadOnly) {
          	%>
				<input type=hidden  name=ArMatchingRuleBuyerId value='<%=editArMatchingRule.getAttribute("buyer_id")%>' size=30 maxlength=30 class=ListText >
		<%
			}
		%>			
		<%@ include file="fragments/ArMatchingRule-AliasNameTable.frag"%> 						
 		</div>
		<%-- Alias Name Tab Ends Here--%>
   
   <%=widgetFactory.createSectionHeader("2",
					"ArMatchingRuleDetail.TradingPartID")%>
					 <span class="asterisk">*</span>
	<%=widgetFactory.createTextField("ArMatchingRuleBuyerId",
					"ArMatchingRuleDetail.BuyerId",
					editArMatchingRule.getAttribute("buyer_id"), "30",
					isBuyerNameIDReadOnly, true, false, "", "", "none")%>
					<br><br>
	<%@ include file="fragments/ArMatchingRule-AliasIDTable.frag"%> 			
		</div>
		<%-- Buyer ID tab Ends Here --%>
   
   <%-- T36000015530 - Rel8.2 - Removing the link of section "3. Invoices".
   Also updated the section# of the following sections to reflect the correct order.
   So now, section 4 - will become section 3 and section 5 will become section 4
   
   <%=widgetFactory.createSectionHeader("3",
					"ArMatchingRuleDetail.Invoices")%>
		<%@ include file="fragments/TradePartnerRules-Invoice.frag" %>	 			
   </div>	 
   --%>   

   
   <%=widgetFactory.createSectionHeader("3",
					"ArMatchingRuleDetail.RecMgmt")%>
		<%@ include file="fragments/TradePartnerRules-RecvMgmt.frag" %>				
   </div>	
   
   <%=widgetFactory.createSectionHeader("4",
					"ArMatchingRuleDetail.Authorisation")%>
		<%@ include file="fragments/TradePartnerRules-Authorisations.frag" %>			
   </div>	
   
   <%
	      	// if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN) ){				
	      	if (userSession.getSavedUserSession() == null) {
	      		userSecurityType1 = userSession.getSecurityType();
	      	} else {
	      		userSecurityType1 = userSession
	      				.getSavedUserSessionSecurityType();
	      	}

	      	if (userSecurityType1.equals(TradePortalConstants.ADMIN)
	      			&& SecurityAccess
	      					.hasRights(
	      							loginRights,
	      							SecurityAccess.VIEW_OR_MAINTAIN_RECEIVABLES_MATCH_RULES)) {
	      %> 
   <%=widgetFactory.createSectionHeader("6",
						"ArMatchingRuleDetail.BankDef")%>
		<%@ include file="fragments/TradePartnerRules-BankDefRules.frag" %>			
   </div>		
   <%
		   	}
		   %>
   	
 	</div>		<%--  Form Content End --%>

</div> <%--  Form Area End --%>
<%
	if (xmlDoc.getDocumentNode("/In/PayRemit") == null) {
%>
<%=formMgr.getFormInstanceAsInputField(
						"ArMatchingRuleDetailForm", secureParams)%>
<%
	formMgr.storeInDocCache("default.doc", new DocumentHandler());
	} else
%>
<%=formMgr.getFormInstanceAsInputField(
					"ArMatchingRuleDetailForPayMatchForm", secureParams)%>
<%String pay_remit_oid = (String)session.getAttribute("pay_remit_oid"); 
cancelAction = "";%>	
<%if(("true").equals(session.getAttribute("fromPaymentMatchResponse"))){
	cancelAction = 	"goToPaymentMatchResponse";		
}

%>
<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'ArMatchingRuleDetailForm'">
                   <jsp:include page="/common/RefDataSidebar.jsp">
	                        <jsp:param name="showSaveButton" value="<%=showSave%>" />
	                        <jsp:param name="showSaveCloseButton" value="<%=showSaveClose%>" />
					        <jsp:param name="showDeleteButton" value="<%=showDelete%>" />
                             <jsp:param name="cancelAction" value="<%=cancelAction %>" />
                             <jsp:param name="pay_remit_oid" value="<%=pay_remit_oid %>" />
                             <jsp:param name="showHelpButton" value="true" />                                                          
			            	<jsp:param name="showLinks" value="true" />
			            	<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
			            	<jsp:param name="error" value="<%=error%>" />                      
                             <jsp:param name="helpLink" value="customer/party_detail.htm" />
                             <jsp:param name="saveOnClick" value="none" />
            <jsp:param name="saveCloseOnClick" value="none" />
                   </jsp:include>        
</div> <%--closes sidebar area--%>
</form>
</div>		<%--  Page Content End --%> 
</div>		<%--  Page Main End --%>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />


<script>


  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
    	  
<%
      if (!isReadOnly && StringFunction.isNotBlank(focusField)) {
%>
        var focusFieldId = '<%=focusField%>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
<%
  	}
%>
		<%--  T36000015898  --%>
	 <%--  these calls are no longer valid since 'section 3' for invoice was removed under --%>
	 <%--  T36000015530. This was causing a page load issue. --%>
	 
    <%-- displayPayLoanType(); // Paybale Inv: loan type should display only for Loan instrument --%>
    <%-- displayRecLoanType(); // Rec Inv: loan type should display only for Loan instrument --%>
      });
  });

	    	  	    	 
	    function displayPayLoanType(){ <%--  display LoanTue	 --%>
	     require(["dojo/dom", "dijit/registry","dojo/dom-style"],
	    		    function(dom, registry,domStyle) {
	    	var payInstrType = registry.byId("pay_instrument_type");
 	    	var PayLoanTypeSec = document.getElementById("PayLoanTypeSection");
	    	
		    	if('<%=InstrumentType.LOAN_RQST%>' == payInstrType){
		    		domStyle.set(dom.byId("PayLoanTypeSection"),'display', 'none');
	
		        }else{
		        	domStyle.set(dom.byId("PayLoanTypeSection"),'display', 'block');
		        }
	     });
	    } 
	   
	    function displayRecLoanType(){
	      require(["dojo/dom", "dijit/registry"],
		    		    function(dom, registry) {
	    	var recInstrType = registry.byId("rec_instrument_type");
	    	var recLoanTypeSec = dom.byId("RecLoanTypeSection");
	        if('<%=InstrumentType.LOAN_RQST%>' == recInstrType){
	        	recLoanTypeSec.style.visibility = 'visible';
	        }else{
	        	recLoanTypeSec.style.visibility = 'hidden';	
	        }
	      });
	    }
	    
		function unCheckPartial(){
		    require(["dijit/registry"],
				  	function(registry) {
						if(document.forms[0].ArMatchingRulePayToPortalInd.checked){
							registry.byId('ArMatchingRulePayToPortalInd').set("checked",true);
 						}
						else {
							registry.byId('ArMatchingRulePayToPortalInd').set("checked",false);
						}
			}); 
		}	
		
		<%-- Narayan CR-913 27-Jan-2014 Rel9.0.0.0 Begin --%>
		function add4MorePaybaleInvPayInstr(){
			require([ "dojo/dom", "t360/common", "dojo/ready"], function(dom, common, ready) {
			var table = dom.byId('payableInvPayInstTable');			
			var insertRowIdx = table.rows.length;
			dom.byId("payInvNumOfRows").value = (insertRowIdx-2)+4; <%--  table has two header --%>
			var lastElement = parseInt(insertRowIdx)-1;
			common.appendAjaxTableRows(table,"payableInvPayInstIndex","/portal/refdata/PayableInvPayInstrRows.jsp?payableInvPayInstIndex="+(lastElement));
			});	
		}
		<%-- Narayan CR-913 27-Jan-2014 Rel9.0.0.0 End --%>
		
</script>

<%
	/**********
	 * CLEAN UP
	 **********/

	beanMgr.unregisterBean("ArMatchingRule");
	//beanMgr.unregisterBean("ArBuyerNameAlias");
	//   beanMgr.unregisterBean("CorporateOrganization");

	// Finally, reset the cached document to eliminate carryover of
	// information to the next visit of this page.
	//formMgr.storeInDocCache("default.doc", new DocumentHandler());
	Debug.debug("***END********************ArMatchingRule*DETAIL**************************END***");
%>

</body>
</html>
