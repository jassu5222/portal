<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
****
<%
    Debug.debug("***START********************DIRECTOR**************************START***");
    DocumentHandler doc = null;

    String logicalPage  = null;
    String physicalPage = null;

    doc = formMgr.getFromDocCache();

    Debug.debug("DOC FROM DOC CACHE IS: \n" + doc.toString());

    logicalPage = doc.getAttribute("/In/BankSearchInfo/ReturnToPage");

    if (logicalPage == null)
    {
        Debug.debug("NO RETURN PAGE DEFINED");
        Debug.debug("NO RETURN PAGE DEFINED");
        Debug.debug("NO RETURN PAGE DEFINED");
	  physicalPage = NavigationManager.getNavMan().getPhysicalPage("PartySearch", request);
    }
    else
    {
        Debug.debug("Forwarding to the path '" + logicalPage + "'");
        formMgr.setCurrPage(logicalPage);
        Debug.debug("*****");
        physicalPage = NavigationManager.getNavMan().getPhysicalPage(logicalPage, request);
        Debug.debug("++++");
    }
%>
            <jsp:forward page='<%= physicalPage %>' />
