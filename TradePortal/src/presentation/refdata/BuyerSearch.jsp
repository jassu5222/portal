<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
		 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
		 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<%
Debug.debug("***START********************BUYER*SEARCH**************************START***");
/******************************************************************************
 * BuyerSearch.jsp
 *
 * Filtering:
 * This page originally displays a listview containing all possible buyers,
 * but the buyers can be filtered using a text box and a link that redisplays
 * the page with the filter text passed as a url parameter.  JavaScript is used
 * to dynamically build that link.
 *
 * Selecting:
 * JavaScript is used to detect which radio button is selected, and the value
 * of that button is passed as a url parameter to page from which the search
 * page was called.
 ******************************************************************************/

String returnAction     = null;
String filterText=null;
String onLoad           = "document.FilterForm.filterText.focus();";
String parentOrgID      = userSession.getOwnerOrgOid();

StringBuffer newLink    = new StringBuffer();
boolean showCreateButton = true;
boolean isReadOnly=false;

Hashtable secureParams = new Hashtable();

DocumentHandler doc;

// Get the document from the cache.  We'll use it to repopulate the screen if the
// user was entering data with errors.
doc = formMgr.getFromDocCache();

Debug.debug("doc from cache is " + doc.toString());

returnAction    = doc.getAttribute("/In/BuyerSearchInfo/ReturnAction");
filterText     = doc.getAttribute("/In/BuyerSearchInfo/FilterText");


if (filterText != null) {
    filterText = filterText.toUpperCase();
} else {
    filterText = "";
}

%>

<%-- Body tag included as part of common header --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>

<script LANGUAGE="JavaScript">

  function getCheckedRadioButton()
  {
    <%--
    // Before going to the next step, the user must have selected one of the
    // radio buttons.  If not, give an error.  Since the selection radio button
    // is dynamic, it is possible the field doesn't even exist.  First check
    // for the existence of the field and then check if a selection was made.
    --%>
    numElements = document.ChooseBuyerForm.elements.length;
    i = 0;
    foundField = false;

    <%-- First determine if the field even exists. --%>
    for (i=0; i < numElements; i++) {
       if (document.ChooseBuyerForm.elements[i].name == "selection") {
          foundField = true;
       }
    }

    if (foundField) {
        var size=document.ChooseBuyerForm.selection.length;
        <%--
        //Handle the case where there is only one radio button
        //Handle the case where there are two or more radio buttons
        --%>
        for (i=0; i<size; i++)
        {
            if (document.ChooseBuyerForm.selection[i].checked)
            {
                return document.ChooseBuyerForm.selection[i].value;
            }
        }
        if (document.ChooseBuyerForm.selection.checked)
        {
            return document.ChooseBuyerForm.selection.value;
        }

    }
    return 0;
  }

  function confirmSelection()
  {
    if (getCheckedRadioButton()==0)
    {
        alert('<%=resMgr.getText("BuyerSearch.SelectBankWarning",TradePortalConstants.TEXT_BUNDLE)%>');
        return false;
    }
    return true;
  }

</script>

<%
/*************************************************************
 * FilterForm
 * This form contains the filter text field and submit button.
 *************************************************************/
%>
	<%--ctq portal refresh add grid html start--%>
	
	<%
	  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	
	  //TODO: build header fields including standard refresh and edit buttons
	%>
	
	<%
	  //TODO: build the displayRights map for the footer items and evaluate within
	  String gridHtml = dgFactory.createDataGrid("buyerSearchGrid","BuyerSearchDataGrid",null);
	%>
	
	<%--ctq portal refresh add grid html end--%>
	 <div class="pageMainNoSidebar">
	<div class="pageContent">
	<div class="secondaryNav"></div>
	
<form method="post" id="FilterForm" name="FilterForm" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
	<div class="formContentNoSidebar">
	
  <input type=hidden value="" name=buttonName>
  
          <%=resMgr.getText("BuyerSearch.TabHeading",TradePortalConstants.TEXT_BUNDLE)%></br>
      
<%-- 	    <%= OnlineHelp.createContextSensitiveLink("customer/buyer_search.htm",resMgr, userSession) %> --%>
     
 
<%--           <%=resMgr.getText("BuyerSearch.ToFilter",TradePortalConstants.TEXT_BUNDLE)%> --%>
       
        <%
        /********************************
         * START FILTER INPUT FIELD
         ********************************/
        Debug.debug("Filter Field");
        %>
<%--         <%=resMgr.getText("BuyerSearch.NameStartsWithExample",TradePortalConstants.TEXT_BUNDLE)%><br> --%>

       
       	<%=widgetFactory.createRadioButtonField("FilterText","filterText1","BuyerSearch.BuyerName","",true,isReadOnly,"onChange=preSearch();style='display:inline';","") %>
       	      	
         <%=widgetFactory.createTextField("BuyerName","","","30",isReadOnly,false,false,"","","none" )%>
         
         <%=widgetFactory.createRadioButtonField("FilterText","filterText2","BuyerSearch.BuyerID","",false,isReadOnly,"onChange=preSearch();style='display:inline';","") %>
         
         <%=widgetFactory.createTextField("BuyerId","","","30",isReadOnly,false,false,"","","none" )%></td>
         
         <button data-dojo-type="dijit.form.Button" type="button" id="Search">Search

		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			searchBuyer();
		</script>
		</button>
		
         
        <%
        /********************************
         * END FILTER INPUT FIELD
         ********************************/%>
   
            <%
            /********************
            * START FILTER BUTTON
            *********************/
            Debug.debug("Filter Button");
            %>


	   
<%
if (showCreateButton){
  newLink.append(formMgr.getLinkAsUrl("selectARMMatchRuleNew", response));// PPRAKASH IR RDUJ031157026
%>

	<a href="<%=newLink%>"><button data-dojo-type="dijit.form.Button" type="button" id="CreateNewBuyer">New</button></a>
                   


<%
} else 
{
  out.print("&nbsp;");
}
%>
          
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="BankColor">&nbsp;</td>
    </tr>
  </table>
  <%= formMgr.getFormInstanceAsInputField("BuyerSearchFilterForm", secureParams) %>
</form>

<%
/*********************************************************************
 * ChooseBuyerForm
 * This form contains the list view and the choose and cancel buttons.
 *********************************************************************/
%>

<form method="post" name="ChooseBuyerForm" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>
  <%= gridHtml %>
 
<%
   /***********************************************************************
   * Begin list view for Buyers List View Page
   ***********************************************************************/

   //Build the where clause

     
	 StringBuffer where=new StringBuffer();
     where.append(" where ar_matching_rule.p_corp_org_oid in (").append(parentOrgID).append(")");

     if (!filterText.equals("")){
        String sqlFilter = SQLParamFilter.filter(filterText);
        where.append(" and ( upper(ar_matching_rule.buyer_name) like '").append(sqlFilter).append("%'");
        where.append(" or upper(ar_matching_rule.buyer_id) like '").append(sqlFilter).append("%')");
     }
     Debug.debug("*** where-> " + where);
%>

<%
    Debug.debug("Listview complete");
    /***********************************************************************
     * End list view for Parties List View Page
     ***********************************************************************/
%>
 
     
    <%= formMgr.getFormInstanceAsInputField("BuyerSearchChooseForm", secureParams) %>
    </div>
  </form>

	</div>
	</div>
 
  <%--cquinton Portal Refresh - start--%>
	<jsp:include page="/common/Footer.jsp">
	  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
	  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
	</jsp:include>
	
	
  <%--cquinton 3/16/2012 Portal Refresh - end--%>
  
  
  <%
  //DataGridFactory dgFactory = new DataGridFactory(resMgr);
  String gridLayout = dgFactory.createGridLayout("buyerSearchGrid", "BuyerSearchDataGrid");
	%>
	
	<script type="text/javascript">
	
		  
		  
		  
	  <%--get grid layout from the data grid factory--%>
	  var gridLayout = <%= gridLayout %>;
	
	  <%--set the initial search parms--%>
	<% 
	  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
	  //search - note that the value in the dropdown is encrypted
	  //String encryptedOrgOid = EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
	%>
	  var initSearchParms = "parentOrgID=<%=StringFunction.xssCharsToHtml(parentOrgID)%>";
	  var SearchParms = "";
	
	  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
	  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("BuyerSearchDataView",userSession.getSecretKey())%>"; <%--Rel9.2 IR T36000032596 --%>
	  var buyerSearchGrid = 
	    createDataGrid("buyerSearchGrid", viewName,
	                    gridLayout, initSearchParms);
	  function preSearch(){
		  require(["dojo/dom", "dojo/domReady!"], function(dom) {
			  
			  if(dom.byId("filterText1").checked){
				  console.log("BuyerName Checked");
				  
				  dom.byId("BuyerId").value="";
				  dijit.byId("BuyerId").set('disabled',true);
				  dijit.byId("BuyerName").set('disabled',false);
				  
				  
			  }else if(dom.byId("filterText2").checked){
				  console.log("BuyerId Checked");
				  
				  dom.byId("BuyerName").value="";
				  dijit.byId("BuyerName").set('disabled',true);
				  dijit.byId("BuyerId").set('disabled',false);
				  
				  
			  }
			  
		  });
	  }
	  function searchBuyer(){
		  require(["dojo/dom"],function(dom){
			  if(dom.byId("filterText1").checked){
				  var filterText1=(dom.byId("BuyerName").value).toUpperCase();
				  console.log("BuyerName Checked");
				  searchParms="filterText1="+filterText1;
				  }else if(dom.byId("filterText2").checked){
				  var filterText2=(dom.byId("BuyerId").value).toUpperCase();
				  searchParms="filterText2="+filterText2;
				  console.log("BuyerName Checked");
				  }
			  	searchParms=initSearchParms+"&"+searchParms;
				  console.log("SearchParms: " + searchParms);
				  searchDataGrid("buyerSearchGrid", "BuyerSearchDataView",searchParms);});
	  }
	
	<%--  Require the DOM resource --%>
	  
	       
	function chooseBuyer() {
    require(["dojo/dom"],
      function(dom){
    	var rowKeys = getSelectedGridRowKeys("buyerSearchGrid");
    	console.log('rowKeys='+rowKeys[0]);
    	console.log('rowKeys='+rowKeys[1]);
    	var formName = 'ChooseBuyerForm';
        var buttonName = 'Search';
        submitFormWithParms(formName, buttonName, "selection", rowKeys);
        
      });
  }
	</script>
	<script type="text/javascript">

require(["dojo/ready"], function(ready){
         ready(function(){
        	 preSearch();
         });
     });


</script> 
</body>

</html>
