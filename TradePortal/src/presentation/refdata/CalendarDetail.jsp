<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*"%>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
<%
      //ctq portal refresh comment out
      //userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");

      /****************************************************************
       * THIS SCRIPTLET WILL FIND OUT IF WE ARE EDITING A CALENDAR
       * CREATING A NEW ONE OR RETURNING FROM AN ERROR FROM A PREVIOUS
       * VERSION OF THIS PAGE.        WE WILL ALSO SET THE PAGE GLOBALS
       * AND POPULATE THE SECURE PARAMETER HASHTABLE THAT WILL BE SENT
       * TO THE FORM MANAGER.
       ****************************************************************/

      /**********
       * GLOBALS *
       **********/
      final String HOLIDAY_COLOR = "red";
      //String WEEKEND_COLOR = "#0099FF";
      final String WEEKEND_COLOR = "#3333FF";
      final String WORKDAY_COLOR = "#808080";

       
    boolean isNew = false; 
      boolean isReadOnly = false; 
      boolean getDataFromDoc = false;
      boolean showSave = true;
      boolean showSaveClose   = true;
      boolean showDelete = true;
      String buttonPressed ="";
      Map calYearMap = null;
      Hashtable secureParams = new Hashtable();
      DocumentHandler doc = null;
      String oid = "";
      String loginLocale = StringFunction.xssCharsToHtml(userSession.getUserLocale());
      String loginRights = userSession.getSecurityRights();

      beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.TPCalendarWebBean","TPCalendar");
      TPCalendarWebBean editCalendar = (TPCalendarWebBean) beanMgr.getBean("TPCalendar");
      TPCalendarYearWebBean editCalendarYear = null;

      isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CALENDAR);
      Debug.debug("MAINTAIN_CALENDAR == " + SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CALENDAR));

      // Get the document from the cache.  We'll use it to repopulate the screen if the
      // user was entering data with errors.
      doc = formMgr.getFromDocCache();
      buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
      Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");

      Debug.debug("doc from cache is " + doc.toString());

   // The logic has been changed to fix opt lock issue on Save & SaveAndClose button 
   	  //- TP Refresh Change -11/27/2012
      if (doc.getDocumentNode("/In/TPCalendar") != null) {
    	  String maxError = doc.getAttribute("/Error/maxerrorseverity");
          getDataFromDoc = true;
          
          oid = doc.getAttribute("/Out/TPCalendar/tp_calendar_oid");
          if (oid == null ) {
                 oid = doc.getAttribute("/In/TPCalendar/tp_calendar_oid");
          }
          
          if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
          		 //no errors, Insert/Update was success
              
          } else{
                  // We've returned from a save/update/delete but have errors.
                  // We will display data from the cache.  Determine if we're
                  // in insertMode by looking for a '0' oid in the input section
                  // of the doc.
				  oid = doc.getAttribute("/In/TPCalendar/tp_calendar_oid");
                  
                  if (oid.equals("0")) {
                        Debug.debug("Create Calendar Error");
                        isNew = true;
                  }
                  else {
                        Debug.debug("Update Calendar Error");
                  }
          }
       }
      

      if (getDataFromDoc) {
            DocumentHandler doc2 = doc.getComponent("/In");
            Debug.debug("/In  " + doc2);
            editCalendar.populateFromXmlDoc(doc2);
            if (!isNew) {
                  editCalendar.setAttribute("tp_calendar_oid", oid);
                  oid = editCalendar.getAttribute("tp_calendar_oid");
            }
      } else if (request.getParameter("oid") != null) {
            isNew = false;
		oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
            Debug.debug("OID -> " + oid);
            editCalendar.getById(oid);
            Debug.debug("Editing Calendar with oid: " + editCalendar.getAttribute("tp_calendar_oid")); 
            Debug.debug("isReadOnly == " + isReadOnly);
      } else      {
            isNew = true;
            oid = null;
      }

      String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
      int currentYear = (new GregorianCalendar()).get(Calendar.YEAR);

      if (isReadOnly) {
            showSave = false;
            showDelete = false;
            showSaveClose   = false;
            editCalendar.setAttribute("tp_calendar_oid", oid);
            if (InstrumentServices.isNotBlank(request.getParameter("calRefresh"))) {
                  editCalendar.populateCalendarYear(request);
                  editCalendarYear = editCalendar.getCalendarYear(request.getParameter("editYear"));
            }
            else {
                  editCalendar.retrieveCalendarYear(formMgr.getServerLocation(),resMgr);
                  editCalendarYear = editCalendar.getCalendarYear(Integer.toString(currentYear));
            }
      } else if (isNew) {
            oid = "0";
            editCalendar.setAttribute("tp_calendar_oid", oid);
            if (InstrumentServices.isNotBlank(request.getParameter("calRefresh"))) {
                  editCalendar.populateCalendarYear(request);
                  editCalendarYear = editCalendar.getCalendarYear(request.getParameter("editYear"));
                  editCalendar.setAttribute("calendar_name",request.getParameter("calendar_name"));
            }
            else {
                  editCalendar.setAttribute("weekend_day_1","-1");
                  editCalendar.setAttribute("weekend_day_2","-1");
                  editCalendarYear = editCalendar.getCalendarYear(Integer.toString(currentYear));
            }
            showDelete = false;
      }
      else {
            if (InstrumentServices.isNotBlank(request.getParameter("calRefresh"))) {
                  editCalendar.populateCalendarYear(request);
                  editCalendarYear = editCalendar.getCalendarYear(request.getParameter("editYear"));
            }
            else {
                  editCalendar.retrieveCalendarYear(formMgr.getServerLocation(),resMgr);
				  String yr = editCalendar.getRetrievedCalendarYear(Integer.toString(currentYear));				  
				  if(StringFunction.isNotBlank(yr)){
	                  editCalendarYear = editCalendar.getCalendarYear(yr);
				  }else{
                  editCalendarYear = editCalendar.getCalendarYear(Integer.toString(currentYear));
            }
      }
			
      }
 
%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
</jsp:include>


<jsp:include page="/common/ButtonPrep.jsp"></jsp:include>

<script type="text/javascript">


function setHoliday(id) {
      var daysInYear = getDaysInYear();
      var start = parseInt(id) -1;
      var end = parseInt(id) ;
      var newDaysInYear="";
      <%--  Start Added by Prateep  --%>
      var holidays=document.getElementById("calendar_holidays").value;
   var year=document.getElementById("editYear").value;
   var locale="<%=loginLocale%>";
   var daysinyear=dojo.byId("days_in_year").value;
   <%--  End Added by Prateep  --%>
   
      if (daysInYear.charAt(start) == '<%=TradePortalConstants.TPCALENDAR_WORK_DAY%>') {
         newDaysInYear = daysInYear.slice(0,start) + "<%=TradePortalConstants.TPCALENDAR_HOLIDAY%>" + daysInYear.slice(end);
         document.getElementById(id).style.fontWeight = 'bold';
         document.getElementById(id).style.color  = '<%=HOLIDAY_COLOR%>';
         
         <%--  Added by Prateep to getDateforday start --%>
         console.log("Beforeset:"+daysinyear);
         dojo.byId("days_in_year").value=setHolidaysInYear(daysinyear,id-1);
         console.log("Afterset:"+dojo.byId("days_in_year").value);
         getHolidaysInYear();
         
         <%--  require(["dojo/_base/xhr"],
                      function(xhr) {
                           
                         
                          xhr.get({
                            
                              url: "/portal/calendardetails?day="+id+"&year="+year+"&locale="+locale,
                          
                              load: function(result) {
                                  console.log(result);
                                  
                                 if(holidays != "" ){
                                 document.getElementById("calendar_holidays").value=document.getElementById("calendar_holidays").value+","+result;
                                 }else{
                                       document.getElementById("calendar_holidays").value=result; }
                              }
                          });
                           
                  });  --%>

        }   
                  
           
      
      else {
            newDaysInYear = daysInYear.slice(0,start) + "<%=TradePortalConstants.TPCALENDAR_WORK_DAY%>" + daysInYear.slice(end);
            document.getElementById(id).style.fontWeight = 'normal';
            document.getElementById(id).style.color  = '<%=WORKDAY_COLOR%>';
            <%--  Start Added by Prateep  --%>
            dojo.byId("days_in_year").value=setHolidaysInYear(daysinyear,id-1);
            getHolidaysInYear();
            <%--  End Added by Prateep  --%>
            
      }
      setDaysInYear(newDaysInYear);
}

function refreshCalendar() {
      console.log("refreshCalendar called");
    setButtonPressed('refreshCalendar', 0);
    document.forms[0].submit();
}
<%--  Start getDateFromDay by Prateep  --%>
function getDateFromDay(day,year,localestr){
      var spec=[31,getDaysInFebruary(year),31,30,31,30,31,31,30,31,30,31];
       var balance = day;
       var month =0;
       
       for (month =0; month <12; month ++) {

               if (balance > spec[month]){
                     balance = balance - spec[month];   
               } else {

                     break;
               }
         }

         var givenDate = new Date (year, month, balance);
         var date;
         var locallangstr=localestr.split("_")[0];
         require(["dojo/date/locale"],
                      function(locale) {
                         date=locale.format(givenDate,{selector: "date", datePattern: "dd MMM yyyy", locale: locallangstr});  
                           
                  });
               
         return date;
}<%--  End getDateFromDay by Prateep  --%>

<%--  Start getDaysInFebruary by Prateep  --%>
function getDaysInFebruary(year){
      if ( (year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0) ) )
               return 29;
             else
               return 28;
}<%--  End getDaysInFebruary by Prateep  --%>

<%--  Start KeepCount by Prateep  --%>
      function KeepCount(id) {		
			<%--  var weekends = [0,0,0,0,0,0,0];
            
            if (weekend_day_1 > 0)
            	 weekends[dojo.byId("weekend_day_1").value - 1] =1;
            if (weekend_day_2 > 0)
            	 weekends[dojo.byId("weekend_day_2").value - 1] = 1;
            console.log("weekends =" + weekends);  --%>
            console.log("********New Call********");
      var NewCount = 0;
      
      for(var i=1; i<8; i++){
            var checkbox=dijit.byId("weekend"+i);
            console.log(checkbox.id +" checkbox.checked ="+checkbox.checked);
            if(checkbox.checked){
                  if (NewCount == 0) {
                        console.log("NewCount = 0");
                        NewCount = NewCount + 1;
                        if (id == i)
                              dojo.byId("weekend_day_1").value=checkbox.value;
                        <%-- setWeekEnd1(id); --%>
                  } else if (NewCount == 1) {
                        console.log("NewCount = 1");
                        NewCount = NewCount + 1;
                        if (id == i) {
                              if ( dojo.byId("weekend_day_1").value > -1) 
                                    dojo.byId("weekend_day_2").value=checkbox.value;
                              else  
                                    dojo.byId("weekend_day_1").value=checkbox.value;
                        }
                              
                        <%-- setWeekEnd2(id); --%>
                  } else {
                        console.log("DeadCode");
                        console.log(checkbox.id +" checkbox.checked ="+checkbox.checked);
                        console.log("NewCount"+NewCount);
                  }
            } else {
                  console.log("check to unset");
                  if (dojo.byId("weekend_day_1").value == id && dojo.byId("weekend_day_1").value == i) {
                        console.log("unset weekend_day_1");
                        dojo.byId("weekend_day_1").value = -1;
                  } else  if (dojo.byId("weekend_day_2").value == id && dojo.byId("weekend_day_2").value == i){
                        console.log("unset weekend_day_2");
                        dojo.byId("weekend_day_2").value = -1;
                  }
                  
            }
      console.log("weekend_day_1="+dojo.byId("weekend_day_1").value+"##weekend_day_2="+dojo.byId("weekend_day_2").value);      
      }
      console.log("NewCount"+NewCount);
      if (NewCount == 2) {
            for(var i=1; i<8; i++) {
             if(!checkbox.checked){
                        checkbox.set('disabled',true);
                  }
            }
      } else {
            for(var i=1; i<8; i++) {
                   if(checkbox.checked){
                              checkbox.set('disabled',false);
                        }
                  }
            
      }
      
      
            refreshCalendar();
      
      }
<%--  End KeepCount by Prateep  --%>
      <%--  Start set and get WeekEnds by Prateep  --%>           
      
      
      function setWeekEnd1(id){
            console.log("setWeekEnd1 called for " +id);                       
            dojo.byId("weekend_day_1").value=id;
            refreshCalendar();            
                  }     
      function setWeekEnd2(id){
            console.log("setWeekEnd2 called for " +id);
            dojo.byId("weekend_day_2").value=id;
            refreshCalendar();
      }
      
      
      function getWeekEnds(){
    	  
            var weekendday1=dojo.byId("weekend_day_1").value;
            var weekendday2=dojo.byId("weekend_day_2").value;
            var weekend1Set, weekend2Set = false;
            console.log("weekendday1=" + weekendday1);
            console.log("weekendday2=" + weekendday2);
            <%-- KMehta on 09 Mar @ Rel 9200 for IR T36000027812 Change - Start --%>
            if (!(weekendday1 == "")){
            	if( weekendday1 > -1) {
                  weekend1Set = true;
                  var weekend1box=dijit.byId("weekend"+weekendday1);
                  weekend1box.set('checked',true);
            	}
            }
            if (!(weekendday2 == "")){
            	if (weekendday2 > -1) {
                  weekend2Set = true;
                  var weekend2box=dijit.byId("weekend"+weekendday2);
                  weekend2box.set('checked',true);
            	}
            }
            <%-- KMehta on 09 Mar @ Rel 9200 for IR T36000027812 Change - End --%>
            if (weekend1Set && weekend2Set) {
                  for(var i=1; i<8; i++){
                        var checkbox=dijit.byId("weekend"+i);
                        console.log(checkbox.id+" checkbox.checked ="+checkbox.checked);
                        if(!checkbox.checked){
                              checkbox.set('disabled',true);
                        }
                  }
            }
            
            }     <%--  End set and get WeekEnds by Prateep  --%> 
      
      <%--  Start getHolidaysInYear by Prateep  --%>
            function getHolidaysInYear(){
                  var daysinyear=dojo.byId("days_in_year").value;
                  console.log("Day in year"+daysinyear);
                  var year=document.getElementById("editYear").value;
                  var locale="<%=loginLocale%>";
                  var holidays="";
                  for(var i=0;i<367;i++){
                        console.log("Day in year"+i+":"+daysinyear.charAt(i));
                        if(daysinyear.charAt(i)=="1"){
                              var newdate=getDateFromDay(i+1,year,locale);
                              console.log("Day in year"+i+":"+newdate);
                              if(holidays != "" ){
                                    holidays=holidays+" , "+newdate;
                                }else{
                                      holidays=newdate; }
                              
                        }
                  }
                  
                  var holidays_area=dijit.byId("calendar_holidays");
                  holidays_area.setValue(holidays);
            }<%--  End getHolidaysInYear by Prateep  --%>
            
            
            <%--  Start setHolidaysInYear by Prateep  --%>
            function setHolidaysInYear(str,index) {
                  if(index > str.length-1) return str;
                  if(str.charAt(index)=="1"){
                        return str.substr(0,index) + "0" + str.substr(index+1);
                  }else if(str.charAt(index)=="0"){
                        return str.substr(0,index) + "1" + str.substr(index+1);
                  }
                  
            }<%--  End setHolidaysInYear by Prateep  --%>
      
      



</script> 



<div class="pageMain">
  <div class="pageContent">
            
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="RefDataHome.Calendars"/>
      <jsp:param name="helpUrl"  value="customer/calendars.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (isNew) {
    subHeaderTitle = resMgr.getText("CalendarDetail.NewCalendar", TradePortalConstants.TEXT_BUNDLE);
  } else {
    subHeaderTitle = editCalendar.getAttribute("calendar_name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>
                              
                                    <%
                                          /*******************
                                           * START SECUREPARAMS
                                           ********************/
                                          secureParams.put("oid", oid);
                                          secureParams.put("owner_oid", userSession.getOwnerOrgOid());
                                          secureParams.put("ownership_level", userSession.getOwnershipLevel());
                        
                                          // Used for the "Added By" column
                                          if (userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
                                                secureParams.put("ownership_type", TradePortalConstants.OWNER_TYPE_ADMIN);
                                          else
                                                secureParams.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
                        
                                          secureParams.put("login_oid", userSession.getUserOid());
                                          secureParams.put("login_rights", loginRights);
                                          secureParams.put("opt_lock", editCalendar.getAttribute("opt_lock"));
                                          /*****************
                                           * END SECUREPARAMS
                                           ******************/%>
                                                      
                        <form name="CalendarDetail" id="CalendarDetail" data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>" >
                          <input type="hidden" value="" name="buttonName">
                          <input type="hidden" value="true" name="calRefresh">
                          <input type="hidden" value="<%=editCalendar.getAttribute("weekend_day_1")%>" name="weekend_day_1" id="weekend_day_1">
                          <input type="hidden" value="<%=editCalendar.getAttribute("weekend_day_2")%>" name="weekend_day_2" id="weekend_day_2">
                          <input type="hidden" value="<%=editCalendarYear.getAttribute("days_in_year")%>" name="days_in_year" id="days_in_year">
                          
                        <div class="formArea">
                                <jsp:include page="/common/ErrorSection.jsp" />
                          
                        <div class="formContent">
                        <div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
						<div class="dijitTitlePaneContentInner">
						
                        <%  out.print(widgetFactory.createTextField("calendar_name","RefDataCalendar.Name", editCalendar.getAttribute("calendar_name"), "35",isReadOnly,true));

                              out.print(widgetFactory.createWideSubsectionHeader("RefDataCalendar.wh"));
                                   %>
                        <div class="formItem">
                        <%=widgetFactory.createSubLabel("RefDataCalendar.wends") %> <br /> 
                        <%---hard coding italic font style ,if we take from portal styles it is over riding the font--%>
                        <div style="font-style:italic;">
                        <%if(!isReadOnly){
                              out.println(widgetFactory.createSubLabel("RefDataCalendar.selectmax2"));
                        } %> <br />
                        </div>
                        </div>
                        <% if (isReadOnly) {
                                         out.println(TPDateTimeUtility.getWeekDayName(loginLocale,editCalendar.getAttribute("weekend_day_1")));
                                     }
                                     else {
                                           
                                           out.println(widgetFactory.createCheckboxField("weekend1", "RefDataCalendar.sun", false, isReadOnly, false, "value='1' onClick='javascript:KeepCount(this.value);'", "", "inline"));
                                           out.println(widgetFactory.createCheckboxField("weekend2", "RefDataCalendar.mon", false, isReadOnly, false, "value='2' onClick='javascript:KeepCount(this.value);'", "", "inline"));
                                           out.println(widgetFactory.createCheckboxField("weekend3", "RefDataCalendar.tue", false, isReadOnly, false, "value='3' onClick='javascript:KeepCount(this.value);'", "", "inline"));
                                           out.println(widgetFactory.createCheckboxField("weekend4", "RefDataCalendar.wed", false, isReadOnly, false, "value='4' onClick='javascript:KeepCount(this.value);'", "", "inline"));
                                           out.println(widgetFactory.createCheckboxField("weekend5", "RefDataCalendar.thu", false, isReadOnly, false, "value='5' onClick='javascript:KeepCount(this.value);'", "", "inline"));
                                           out.println(widgetFactory.createCheckboxField("weekend6", "RefDataCalendar.fri", false, isReadOnly, false, "value='6' onClick='javascript:KeepCount(this.value);'", "", "inline"));
                                           out.println(widgetFactory.createCheckboxField("weekend7", "RefDataCalendar.sat", false, isReadOnly, false, "value='7' onClick='javascript:KeepCount(this.value);'", "", "inline"));
                                          
                                           //out.println(TPDateTimeUtility.getWeekDayDropDown("weekend_day_1", editCalendar.getAttribute("weekend_day_1"), loginLocale,"","onchange='javascript:refreshCalendar();'"));
                                     }
                        %>
                        <div style="clear:both"></div>
                        <div class="formItem">
                        <% if (isReadOnly) {
                                          out.println(TPDateTimeUtility.getWeekDayName(loginLocale,editCalendar.getAttribute("weekend_day_2")));
                                     }
                                     else {
                                           //out.println(TPDateTimeUtility.getWeekDayDropDown("weekend_day_2", editCalendar.getAttribute("weekend_day_2"), loginLocale,"","onchange='javascript:refreshCalendar();'"));
                                     }
                        %>    
                        
                        <br /><%=widgetFactory.createSubLabel("RefDataCalendar.holidays") %> <br />
                        <%---hard coding italic font style ,if we take from portal styles it is over riding the font--%>
                        <div style="font-style:italic;">
                        <%if(!isReadOnly){
                        	
                              out.println(widgetFactory.createSubLabel("RefDataCalendar.setupholidays"));
                        } %> 
                        
                        </div>
                            </div>                    
                        <%-- <%=resMgr.getText("common.Year",TradePortalConstants.TEXT_BUNDLE)%> --%>
                        <%
						String options=TPDateTimeUtility.getYearOptions(editCalendarYear.getAttribute("year"),null,-1);
                        out.print(widgetFactory.createSelectField("editYear","common.Year","",options,isReadOnly,false,false,"onchange='javascript:refreshCalendar();'","",""));
                        //out.print(TPDateTimeUtility.getYearDropDown("editYear", editCalendarYear.getAttribute("year"),null,"onchange='javascript:refreshCalendar();'"));
                        %>
                        <%
                              if(!isReadOnly){
                              out.print(widgetFactory.createTextArea("calendar_holidays","RefDataCalendar.selectedholidays","",true,false,false,"","",""));
                              }
                        %>
                        
                        
                        
                        <% 
                        if (isReadOnly) {
                              %>
                              <input type="hidden" value='<%=editCalendar.getAttribute("weekend_day_1")%>' name="weekend_day_1">
                              <input type="hidden" value='<%=editCalendar.getAttribute("weekend_day_2")%>' name="weekend_day_2">
                              <input type="hidden" value='<%=editCalendarYear.getAttribute("year")%>' name="editYear">
                              <input type="hidden" value='<%=editCalendar.getAttribute("calendar_name")%>' name="calendar_name">
                        <%    
                        }
                        %>
                         
                        <%
                        calYearMap = editCalendar.getCalendarYearComponentsMap();
                        Iterator it=calYearMap.keySet().iterator();
                        int iLoop = 0;
                        int idx = -1;
                        String s_year = null;
                        while (it.hasNext()) {
                              s_year = (String)it.next();
                              TPCalendarYearWebBean calBean = (TPCalendarYearWebBean)calYearMap.get(s_year);  
                        %>    
                                 <input type="hidden" value='<%=calBean.getAttribute("tp_calendar_year_oid")%>' name="_calendar_id<%=iLoop%>">
                                 <input type="hidden" value='<%=calBean.getAttribute("year")%>' name="_year<%=iLoop%>">
                                 <input type="hidden" value='<%=calBean.getAttribute("days_in_year")%>' name="_days_in_year<%=iLoop%>">
                        
                        <%    
                          if (s_year.equals(editCalendarYear.getAttribute("year"))) {
                                idx = iLoop;
                          }
                          iLoop++;
                        }
                        %>
                        
                        <script type="text/javascript">
                       
                        function getDaysInYear() {
                              return document.forms[0]._days_in_year<%=idx%>.value;
                        }
                        
                        function setDaysInYear(val) {
                              document.forms[0]._days_in_year<%=idx%>.value = val;
                        }
                     
                        </script> 
                        
                        
                        <%--  calendar start  --%> 
                        <table cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="15" nowrap>&nbsp;</td>
                            <td>
                             <table cellpadding="2" cellspacing="0">
                                <tbody>
                                    <tr> 
                                          <td></td>
                                          <%
                                                int day = 1;
                                              int col = 0;
                                                int blankCol = 0;
                                                int dayofWeek = 0;
                                                GregorianCalendar cal = null;
                                                int weekend1 = 0;
                                                String days_in_year = editCalendarYear.getAttribute("days_in_year");
                                                if (InstrumentServices.isNotBlank(editCalendar.getAttribute("weekend_day_1"))) {
                                                      weekend1 = Integer.parseInt(editCalendar.getAttribute("weekend_day_1"));
                                                }
                                                int weekend2 = 0;
                                                if (InstrumentServices.isNotBlank(editCalendar.getAttribute("weekend_day_2"))) {
                                                      weekend2 = Integer.parseInt(editCalendar.getAttribute("weekend_day_2"));
                                                }
                                                // get months 
                                                final String months[] = TPDateTimeUtility.getMonthNames(loginLocale);
                                                
                                                // get days
                                                final String days[] = {
                                                            resMgr.getText("CalendarDetail.Sunday", TradePortalConstants.TEXT_BUNDLE),
                                                            resMgr.getText("CalendarDetail.Monday", TradePortalConstants.TEXT_BUNDLE),
                                                            resMgr.getText("CalendarDetail.Tuesday", TradePortalConstants.TEXT_BUNDLE),
                                                            resMgr.getText("CalendarDetail.Wednesday", TradePortalConstants.TEXT_BUNDLE),
                                                            resMgr.getText("CalendarDetail.Thursday", TradePortalConstants.TEXT_BUNDLE),
                                                            resMgr.getText("CalendarDetail.Friday", TradePortalConstants.TEXT_BUNDLE),
                                                            resMgr.getText("CalendarDetail.Saturday", TradePortalConstants.TEXT_BUNDLE)
                                                             };
                                                
                                                // The days in month.
                                                final int dom[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                        
                                                int year = Integer.parseInt(editCalendarYear.getAttribute("year")); 
                                                for (int mm = 0; mm < 12; mm++) {
                                                      cal = new GregorianCalendar(year, mm, 1);
                                                      blankCol = cal.get(Calendar.DAY_OF_WEEK) - 1;
                                                      dayofWeek = cal.get(Calendar.DAY_OF_WEEK);
                                                      int daysInMonth = dom[mm];
                                                      if (cal.isLeapYear(cal.get(cal.YEAR)) && mm == 1)
                                                            ++daysInMonth;
                        
                                                      if (col % 3 == 0 || col == 0) {
                                          %>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                          <%
                                                } else {
                                          %>
                                          <td width="15">&nbsp;</td>
                                          <%
                                                }
                                          %>
                                          <td style="border:solid 1px black" valign="top">
                                          <table border="0" cellpadding="3" cellspacing="0">
                                                <thead>
                                                      <tr>
                                                            <td align="center" colspan="7" style="color:black;font-weight: bold"><%=months[col]%></td>
                                                      </tr>
                                                      <tr >
                                                      <%
                                                            for (int k = 1; k <= days.length; k++) {
                                                                  String clr = "black";
                                                                  if (k == weekend1  ||  k == weekend2) {
                                                                        clr = WEEKEND_COLOR;
                                                                  }
                                                      %>          
                                                            
                                                            <td style="border-bottom:solid 2px black;font-weight: bold;color:<%=clr %>" align="center"><%=days[k-1]%></td>
                                                    <%
                                                            }
                                                    %>            
                        
                                                      </tr>
                                                </thead>
                                                <tbody>
                                                      <%
                                                               // Fill blank columns before 1st day of month
                                                                  for (int j = 0; j < blankCol; j++) {
                                                                        out.print("<td>&nbsp;</td>");
                                                                  }
                                                                  // Populate days of month.
                                                                  for (int i = 1; i <= daysInMonth; i++) {
                                                                        
                                                                        out.print("<td align=\"center\">");
                                                                        
                                                                        if (dayofWeek == weekend1  ||  dayofWeek == weekend2) {
                                                                              out.print("<span id=\""+ day + "\" style=\"color:" + WEEKEND_COLOR+ "\" >");
                                                                        }
                                                                        else {
                                                                              if (days_in_year.charAt(day-1) == TradePortalConstants.TPCALENDAR_HOLIDAY.charAt(0)) {
                                                                                    if (isReadOnly) {
                                                                                          out.print("<span id=\""+ day + "\" style=\"font-weight:bold;color:" + HOLIDAY_COLOR + "\" >");                                                                
                                                                                    } else {
                                                                                        out.print("<a id=\""+ day + "\" style=\"text-decoration:none;font-weight:bold;color:" + HOLIDAY_COLOR + "\"  href=\"#\" onClick=\"return false\" ondblclick=\"javascript:setHoliday('" + day + "');\">");
                                                                                    } 
                                                                              }
                                                                              else {
                                                                                    if (isReadOnly) {
                                                                                        out.print("<span id=\""+ day + "\" style=\"color:" + WORKDAY_COLOR+ "\"  >");
                                                                                    }
                                                                                    else {
                                                                                    out.print("<a id=\""+ day + "\" style=\"text-decoration:none;color:" + WORKDAY_COLOR+ "\"  href=\"#\" onClick=\"return false\" ondblclick=\"javascript:setHoliday('" + day + "');\">");
                                                                                }
                                                                              }
                                                                        }
                                                                        out.print(i);
                                                                        if (dayofWeek == weekend1  ||  dayofWeek == weekend2) {
                                                                            out.print("</span>");
                                                                        } else {
                                                                              if (isReadOnly) {
                                                                                    out.print("</span>");
                                                                              }
                                                                              else {
                                                                                out.print("</a>");
                                                                              }
                                                                        }
                                                                        
                                                                        
                                                                        if ((blankCol + i) % 7 == 0) { // next line
                                                                              out.println("</tr>");
                                                                              out.print("<tr>");
                                                                              dayofWeek = 0;
                                                                        }
                                                                        day++;
                                                                        dayofWeek++;
                                                                  }
                                                      %>
                                                </tbody>
                                          </table>
                                          </td>
                                          <%
                                          col++;
                                          }
                                        %>
                                    </tr>
                              </tbody>
                           </table>
                          </td>
                         </tr>
                        </table>   
                        
                           <%-- calendar end  --%>
                          
                        <%=formMgr.getFormInstanceAsInputField("CalendarDetailForm", secureParams)%>
                                          
						</div>
						</div>
                        </div><%--formContent--%>
                        </div><%--formArea--%>
                        
                        <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'CalendarDetail'">

                        <jsp:include page="/common/RefDataSidebar.jsp">
                                <jsp:param name="showSaveButton" value="<%= showSave %>" />
                                <jsp:param name="saveOnClick" value="none" />
                                <jsp:param name="showSaveCloseButton" value="<%= showSaveClose %>" />
                                <jsp:param name="saveCloseOnClick" value="none" />
                                <jsp:param name="showDeleteButton" value="<%= showDelete %>" />
                                <jsp:param name="showHelpButton" value="true" />
                                <jsp:param name="cancelAction" value="CalendarCancel" />
                                <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
                               	<jsp:param name="error" value="<%= error%>" />
                        </jsp:include>
                        </div>
                        <%-- End formSidebar --%>
                        
                        </form>

                  
            
      </div><%-- End pageContent --%>
</div><%-- End pageMain --%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<%--cquinton 10/8/2012 ir#5988 start
    move hsm encryption javascript to footer area--%>
<%
  //save behavior defaults to without hsm
  
  //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  
  // String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  // String saveCloseOnClick = "common.setButtonPressed('SaveAndClose','0'); document.forms[0].submit();";

 
%>
  

<jsp:include page="/common/RefDataSidebarFooter.jsp" />
 

<%--cquinton 10/8/2012 ir#5988 end--%>

<script type="text/javascript">
<%-- KMehta on 2nd Dec 2014 @ Rel 9200 for IR T36000027812 Change - Start --%>
    	  require(["dojo/ready","dijit/registry", "dojo/domReady!"], function(ready,registry,domReady){
    			ready(function(){

    			     getHolidaysInYear();
    				getWeekEnds();  
       
<%
  String focusField = "calendar_name";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
<%
  }
%>
            
    });
  });
  
   function preSaveAndClose(){
  	 	 
 	  document.forms[0].buttonName.value = "SaveAndClose";
 	  return true;

	}
  
</script>       
<%-- KMehta on 2nd Dec 2014 @ Rel 9200 for IR T36000027812 Change - End  --%>
</body>

</html>

<%
      /**********
       * CLEAN UP
       **********/

      beanMgr.unregisterBean("TPCalendar");

      // Finally, reset the cached document to eliminate carryover of
      // information to the next visit of this page.
      formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
