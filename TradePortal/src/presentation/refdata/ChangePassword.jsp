<%--
*******************************************************************************
  Change Password

  Description:
    Displays a page requiring a user to change his password.
    Note: after portal refresh, ability to change password here in a 
      non-forced way is moved to user preferences.  However,
      
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,
                 com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"/>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"/>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"/>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"/>

<%

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  // [START] CR-482  
  // Check if this TradePortal instance is configured for HSM encryption / decryption
  boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
  //for hsm debugging comment out above and uncomment out below
  //boolean instanceHSMEnabled = true;
  // [END] CR-482

  // Determine information about page based on parameters passed in

  // Resource key for the name of the page
  String pageTitle;

  // Resource key for instructions given to the user on the page
  String instructions;

  // The logical name of the form to which the data is submitted
  String formName;

  // Is the user being forced to change their password?
  boolean forcedToChangePassword = false;

  // Is this user a new user?
  boolean isNewUser = false;

  String forcedToChangePasswordParm;
  String reasonParam;
  String helpFileName;


  // Get the document from the cache.  We'll use it to repopulate the screen if the
  // user was entering data with errors.
  DocumentHandler doc = formMgr.getFromDocCache();   

  // Either get the page parameters from the input cache or from the JSP parameters
  if ( (doc.getAttribute("/Error/maxerrorseverity") == null) ||
       (doc.getAttribute("/Error/maxerrorseverity").compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) ) {
    Debug.debug("getting values from params...");
    forcedToChangePasswordParm = request.getParameter("force");
    reasonParam = request.getParameter("reason");
  }
  else {
    Debug.debug("getting values from input doc...");
    forcedToChangePasswordParm = doc.getAttribute("/In/force");
    reasonParam = doc.getAttribute("/In/reason");
  }


  if ( (forcedToChangePasswordParm != null) &&
      (forcedToChangePasswordParm.equals("true") ) ) {
    forcedToChangePassword = true;
  }

  if( (reasonParam != null) && (reasonParam.equals("new")) ) {
    isNewUser = true;
    pageTitle = "ChangePassword.NewUserTitle";
    instructions = "ChangePassword.NewUserInstructions";
    formName = "ChangePasswordNewUser";
    //helpFileName = "customer/change_passwd_first.htm";
    helpFileName = "admin/reset_password.htm";
  }
  else if( (reasonParam != null) && (reasonParam.equals("expired")) ) {
    pageTitle = "ChangePassword.Title";
    instructions = "ChangePassword.ExpiredInstructions";
    formName = "ChangePasswordExpire";
    //helpFileName = "customer/change_passwd_expired.htm";
    helpFileName = "admin/reset_password.htm";
  }
  else {
    pageTitle = "ChangePassword.Title";
    instructions = "ChangePassword.RegularInstructions";
    formName = "ChangePassword";
    //helpFileName = "customer/change_passwd_voluntary.htm";
    helpFileName = "admin/reset_password.htm";
  }

  // Determine text resource key suffix
  // Different instructions are displayed if stricter password rules
  // are in effect
  String stricterPasswordSetting;    
  String minimumPasswordLength; // CR-482
  String suffix = "";

  if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_GLOBAL)) {
    stricterPasswordSetting = SecurityRules.getProponixRequireUpperLowerDigit();
    // [START] CR-482
    minimumPasswordLength = String.valueOf(SecurityRules.getProponixPasswordMinimumLengthLimit());
    // [END] CR-482
  }
  else {
    ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class,  "ClientBank");
    clientBank.getById(userSession.getClientBankOid());

    // [START] CR-482 Updated indenting and added minimumPasswordLength
    if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE)) {
      stricterPasswordSetting = clientBank.getAttribute("corp_password_addl_criteria");
      minimumPasswordLength = clientBank.getAttribute("corp_min_password_length");
    }
    else {
      stricterPasswordSetting = clientBank.getAttribute("bank_password_addl_criteria");
      minimumPasswordLength = clientBank.getAttribute("bank_min_password_len");
    }
    // [END] CR-482
  }

  if (stricterPasswordSetting.equals(TradePortalConstants.INDICATOR_YES)) {
    suffix = "StricterRules";
  }
%>



<%-- Body tag included as part of common header --%>

<%
  String minimalHeader = TradePortalConstants.INDICATOR_NO;
  if (forcedToChangePassword) {
    minimalHeader = TradePortalConstants.INDICATOR_YES;
  }
  else {
    minimalHeader = TradePortalConstants.INDICATOR_NO;
  }
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="minimalHeaderFlag" value="<%= minimalHeader %>" />
  <jsp:param name="checkChangePassword" value="<%= TradePortalConstants.INDICATOR_NO %>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%= pageTitle %>" />
      <jsp:param name="helpUrl"  value="<%= helpFileName %>" />
    </jsp:include>

<form id="ChangePassword" name="ChangePassword" method="post" action="<%=formMgr.getSubmitAction(response)%>">

  <jsp:include page="/common/ErrorSection.jsp" />

  <div class="formContentNoSidebar">

    <div class="padded">

<%
  // [START] CR-482
  if (instanceHSMEnabled)
  {
%>
      <input type=hidden name="encryptedCurrentPassword" value="">
      <%-- [START] CR-543 --%>
      <input type=hidden name="protectedCurrentPassword" value="">
      <input type=hidden name="protectedNewPassword" value="">
      <input type=hidden name="encryptedRetypedNewPassword" value="">
      
      <%-- [END] CR-543 --%>
      <input type=hidden name="passwordValidationError" value="">
<%
  } // if (instanceHSMEnabled)
  // [END] CR-482

%>


<%
  Hashtable addlParms = new Hashtable();

  // The system must keep track of how many times the user has entered an invalid current password
  // This is done to prevent someone from attempting to test for different passwords
  // when a user is away from their computer.  

  // If the maximum number has been reached, the user is forwarded
  // to the "Too Many Logons" page, which also logs them out    
  int numberOfWrongPasswords;
  
  try {
    numberOfWrongPasswords = Integer.parseInt(doc.getAttribute("/Out/numberOfWrongPasswords"));
  }
  catch(Exception e) {
    numberOfWrongPasswords = 0;
  }


  if ( numberOfWrongPasswords == SecurityRules.getLoginAttemptsBeforeBeingRedirected() ) {
    String tooManyLogonsPage = NavigationManager.getNavMan().getPhysicalPage("TooManyInvalidLogonAttempts", request);

    StringBuffer parms = new StringBuffer();
    parms.append("?branding=");
    parms.append(userSession.getBrandingDirectory());
    parms.append("&organization=");
    parms.append(userSession.getClientBankCode());
    parms.append("&locale=");
    parms.append(userSession.getUserLocale());

%>
      <jsp:forward page='<%= tooManyLogonsPage + parms %>' />
<%
  }

  // Add the numberOfWrongPasswords to formManager
  addlParms.put("numberOfWrongPasswords", String.valueOf(numberOfWrongPasswords));


  addlParms.put("userOID",userSession.getUserOid());

  // The ChangePasswordMediator (where the data is sent from this page)
  // is also used to unlock users.  These flags tell the mediator that we're not unlocking a user here
  addlParms.put("changingPasswordFlag", "true");
  addlParms.put("resettingUserFlag", "false");

  if(forcedToChangePassword) {
    addlParms.put("force",forcedToChangePasswordParm);
    addlParms.put("checkCurrentPasswordFlag", "false");
  }
  else {
    addlParms.put("checkCurrentPasswordFlag", "true");
  }
      
  if(reasonParam != null) {
    addlParms.put("reason", reasonParam);
  }
%>

      <%= formMgr.getFormInstanceAsInputField(formName, addlParms) %>

      <div class="instruction formItem">
<%
  String instructionsText = resMgr.getText(instructions+"1"+suffix, TradePortalConstants.TEXT_BUNDLE) +
    " <a href=" + OnlineHelp.getURLForHelp("/customer/password_constraints.htm", resMgr) +
    ">" + resMgr.getText(instructions+"2", TradePortalConstants.TEXT_BUNDLE)+
    "</a>"+ resMgr.getText(instructions+"3"+suffix, TradePortalConstants.TEXT_BUNDLE);
%>
        <%= instructionsText %>
      </div>
<%
  if (!forcedToChangePassword) {
%>
      <%= widgetFactory.createTextField( "currentPassword", "ChangePassword.CurrentPassword",
                                       "", "32", false, false, false, "type=\"password\" AutoComplete=\"off\"", "", "" ) %>
<%
  }
%>
      <%= widgetFactory.createTextField( "newPassword", "ChangePassword.NewPassword",
                                       "", "32", false, false, false, "type=\"password\" AutoComplete=\"off\"", "", "" ) %>

      <%= widgetFactory.createTextField( "retypePassword", "ChangePassword.RetypeNewPassword",
                                       "", "32", false, false, false, "type=\"password\" AutoComplete=\"off\"", "", "" ) %>

<%
  if(!forcedToChangePassword) {
%>
           <%-- Rel9.1 IR T36000032594 Start--%>
            <button id="saveCloseButton" data-dojo-type="dijit.form.Button"
              type="submit" class="formItem" >
            <%=resMgr.getText("common.SaveCloseText",TradePortalConstants.TEXT_BUNDLE)%>
            </button>
<%
    String link = formMgr.getLinkAsUrl("goToTradePortalHome", response);
%>
         
            <button id="cancelButton" data-dojo-type="dijit.form.Button"
              type="button" class="formItem" >
            <%=resMgr.getText("common.CancelText",TradePortalConstants.TEXT_BUNDLE)%>
            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                openURL("<%=link%>");
            </script>
            </button>
            <%-- Rel9.1 IR T36000032594 End--%>
<%
  } else {
%>
      <button id="saveButton" data-dojo-type="dijit.form.Button"
              type="submit" class="formItem" >
        <%=resMgr.getText("common.SaveText",TradePortalConstants.TEXT_BUNDLE)%>
      </button>
<%
  }
%>

    </div>
  </div>




















































</form>

  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="minimalHeaderFlag" value="<%= minimalHeader %>" />
  <jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>


<%
  // [START] CR-482
  // Include javascript functions for encryption
  if (instanceHSMEnabled)
  {
%>
<script src="js/crypto/pidcrypt.js"></script>
<script src="js/crypto/pidcrypt_util.js"></script>
<script src="js/crypto/asn1.js"></script><%-- needed for parsing decrypted rsa certificate --%>
<script src="js/crypto/jsbn.js"></script><%-- needed for rsa math operations --%>
<script src="js/crypto/rng.js"></script><%-- needed for rsa key generation --%>
<script src="js/crypto/prng4.js"></script><%-- needed for rsa key generation --%>
<script src="js/crypto/rsa.js"></script><%-- needed for rsa en-/decryption --%>
<script src="js/crypto/md5.js"></script><%-- needed for key and iv generation --%>
<script src="js/crypto/aes_core.js"></script><%-- needed block en-/decryption --%>
<script src="js/crypto/aes_cbc.js"></script><%-- needed for cbc mode --%>
<script src="js/crypto/sha256.js"></script>
<script src="js/crypto/tpprotect.js"></script><%-- needed for cbc mode --%> 
<%
  } // if (instanceHSMEnabled)
%>

<script>

  <%--cquinton 10/9/2012 ir#6030 start
      use dojo/ready rather than domReady! to ensure eventhandler
      is registered.
      also set focusFields correctly--%>
  require(["dijit/registry", "dojo/ready"],
      function(registry, ready) {
    ready(function() {

      <%--set focus--%>
<%
  if(!forcedToChangePassword) {
%>
      var focusField = registry.byId("currentPassword");
<%
  }
  else {
%>
      var focusField = registry.byId("newPassword");
<%
  }
%>
      focusField.focus();

    <%-- cquinton 10/12/2012 ir#6030 start
         refactor encryptHsmFields so can be used by enter event handler (specifically for ie)--%>
    });
  });

<%
  if (instanceHSMEnabled) {
    UserWebBean user = beanMgr.createBean(UserWebBean.class,   "User");
    user.getById(userSession.getUserOid());    
%>
  function encryptHsmFields() {
    var public_key = '-----BEGIN PUBLIC KEY-----\n<%= HSMEncryptDecrypt.getPublicKey() %>\n-----END PUBLIC KEY-----';
    var iv = '<%= HSMEncryptDecrypt.getNewIV() %>';
    <%-- for debugging hsm in dev comment out above and comment in below --%>
    <%-- var public_key = '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvmMiU1irZ6aejIm0B+cKL9foNO+iDdftWwFS0eUYmu8A4IJWxoq66hgJ1+z84TuKrkHYhWGYz1iK1sxsf5CgbN86UumXX99c3KhfP3YJrgHzohvpU0yt/HAlSLa7JgnuSTNYcQiaGW/y1lkuS8bzSw2Hb/yeXUI1yt7KhwTPjE9hJnh5Jk7YERebRXP7MCOUxWJBuEjidmYIRjRkqzJ3FDEG1WByLjco5cmMaWZYdz9GUUEF3lIW7/Zb0kt40B2Ct0b3sIPhH5g6UF+nCzXxzBy5FOMMdUxh23re2ob88Ec+9AfYcD3qL9HS9IQ3ztAHTPZbhcwj8++8b9sXrfo/+QIDAQAB\n-----END PUBLIC KEY-----'; --%>
    <%-- var iv = 'zhXaSTr+/981U4TPJSu/Yg=='; --%>

    var protector = new TPProtect();
      
    <%-- Ensure that the new password and retyped password match --%>
    if (document.ChangePassword.passwordValidationError.value == "") {
      document.ChangePassword.passwordValidationError.value = protector.MatchingEntries({errorCode: "<%= TradePortalConstants.ENTRIES_DONT_MATCH %>", newPassword: document.ChangePassword.newPassword.value, retypePassword: document.ChangePassword.retypePassword.value});
    }
      
    <%-- Ensure that the new password is not the same as the user_identifier --%>
    if (document.ChangePassword.passwordValidationError.value == "") {
      document.ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("user_identifier") %>"});
    }

    <%-- Ensure that the new password is not the same as the first_name --%>
    if (document.ChangePassword.passwordValidationError.value == "") {
      document.ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("first_name") %>"});
    }
      
    <%-- Ensure that the new password is not the same as the last_name --%>
    if (document.ChangePassword.passwordValidationError.value == "") {
      document.ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("last_name") %>"});
    }

    <%-- Ensure that the new password is not the same as the login_id --%>
    if (document.ChangePassword.passwordValidationError.value == "") {
      document.ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("login_id") %>"});
    }

    <%-- Ensure that the new password does not start or end with a space --%>
    if (document.ChangePassword.passwordValidationError.value == "") {
      document.ChangePassword.passwordValidationError.value = protector.StartEndWithSpace({errorCode: "<%= TradePortalConstants.PASSWORD_SPACE %>", newPassword: document.ChangePassword.newPassword.value});
    }

    <%-- Ensure that the new password is the required length --%>
    if (document.ChangePassword.passwordValidationError.value == "") {
      document.ChangePassword.passwordValidationError.value = protector.IsRequiredLength({errorCode: "<%= TradePortalConstants.PASSWORD_TOO_SHORT %>", newPassword: document.ChangePassword.newPassword.value, reqLength: <%= minimumPasswordLength %>});
    }

    <%-- Ensure that the new password meets the consecutive characters requirements --%>
    if (document.ChangePassword.passwordValidationError.value == "") {
      document.ChangePassword.passwordValidationError.value = protector.CheckConsecutiveChars({errorCode: "<%= TradePortalConstants.PASSWORD_CONSECUTIVE %>", newPassword: document.ChangePassword.newPassword.value});
    }

<%
    if(stricterPasswordSetting.equals(TradePortalConstants.INDICATOR_YES)) {
%>
    if (document.ChangePassword.passwordValidationError.value == "") {
      document.ChangePassword.passwordValidationError.value = protector.CheckPasswordComplexity({errorCode: "<%= TradePortalConstants.PASSWORD_UPPER_LOWER_DIGIT %>", newPassword: document.ChangePassword.newPassword.value});
    }
<%
    }
%>

    <%-- Encrypt two versions of the current password, clear-text and SHA-256 hashed --%>
    if (document.ChangePassword.currentPassword) {
      document.ChangePassword.encryptedCurrentPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.ChangePassword.currentPassword.value});          
      <%-- [START] CR-543 --%>
      var currentPasswordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password: document.ChangePassword.currentPassword.value, challenge: iv});
      document.ChangePassword.protectedCurrentPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: currentPasswordMac});                    
    }
    <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- START --%>
    if (document.ChangePassword.retypePassword) {
        document.ChangePassword.encryptedRetypedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.ChangePassword.retypePassword.value});  
     }    
    
    <%-- Encrypt the MAC of the new password --%>
    if (document.ChangePassword.newPassword) {
       	
        var newPasswordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password: document.ChangePassword.newPassword.value, challenge: iv});
        var combinedNewPassword = newPasswordMac + '|' + document.ChangePassword.newPassword.value;
        document.ChangePassword.protectedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: combinedNewPassword});  

    }          
    <%-- [END] CR-543 --%>
    <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- END --%>  
    <%-- Blank out the password fields --%>
    if (document.ChangePassword.currentPassword) {
      document.ChangePassword.currentPassword.value = "";
    }
    if (document.ChangePassword.newPassword) {
      document.ChangePassword.newPassword.value = "";
    }
    if(document.ChangePassword.retypePassword) {
      document.ChangePassword.retypePassword.value = "";
    }
  } <%--  function encryptHsmFields() --%>
<%
  } // if (instanceHSMEnabled)
%>

  require(["dojo/dom", "dojo/query", "dojo/on", "dojo/ready"],
      function(dom, query, on, ready) {
    ready(function() {

      <%--register event handlers--%>
      query('#saveButton').on("click", function() {

        setButtonPressed('<%= TradePortalConstants.BUTTON_SAVE %>','0');
<%
  if (instanceHSMEnabled) {
%>
        encryptHsmFields();
<%
  } // if (instanceHSMEnabled)
%>
      });
      <%-- Rel9.1 IR T36000032594 Start--%>
      var myButton = registry.byId("saveCloseButton");
      if (myButton) {
        myButton.on("click", function() {
        	  setButtonPressed('<%= TradePortalConstants.BUTTON_SAVE %>','0');
        	  document.forms[0].submit();
         });
      }
      <%-- Rel9.1 IR T36000032594 End--%>
      <%--for ie, we need an event handler for enter--%>
      var changePasswordForm = dom.byId("ChangePassword");
      on(changePasswordForm, "keypress", function(event) {
        if (event && event.keyCode == 13) {
<%
  if (instanceHSMEnabled) {
%>
          encryptHsmFields();
<%
  } // if (instanceHSMEnabled)
%>
          changePasswordForm.submit();
        }
      });

    });
    <%-- cquinton 10/12/2012 ir#6030 end --%>
  });
  <%--cquinton 10/9/2012 ir#6030 end --%>

</script>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</body>
</html>
