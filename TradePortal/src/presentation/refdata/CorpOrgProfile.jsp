

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.*, com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,java.util.*,com.ams.tradeportal.common.cache.*,net.sf.json.*,net.sf.json.xml.*;" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
	Debug.debug("***START********************My*ORGANIZATION*PROFILE*DETAIL**************************START***");
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	CorporateOrganizationWebBean corporateOrg = null;
	DocumentHandler operationalBankOrgsDoc = null; 
	DocumentHandler xmlDoc = null;
	String corporateOrgOid = null;
	
	boolean showSaveButtonFlag = true;
	boolean showSaveCloseButtonFlag = true;
	boolean retrieveFromDBFlag = false;
	Hashtable secureParms = new Hashtable();
	String onLoad = null;
	boolean errorFlag = false;
	boolean getDataFromXmlDocFlag = false;
	boolean insertModeFlag = true;
	DocumentHandler doc = null;
	String saveButtonPressed = "";
	String                         userSecurityRights              = null;
	Vector error = null;
	boolean                        isReadOnly                      = false;
	DocumentHandler   hierarchyDoc                 = null;
	String resultSet  = null;
   	boolean defaultCheckBoxValue = false;  
	boolean payDefCheckBoxValue = false;
	userSecurityRights = userSession.getSecurityRights();
	corporateOrgOid = (String) userSession.getOwnerOrgOid();
	boolean ignoreDefaultPanelValue = true;
	 String payDefCheckboxIndicator    = null;
	 String clientBankOid = null;
	 
	 int row2Count = 0; 
	 int DEFAULT_ALIAS_COUNT = 4;
	 List aliasList = new ArrayList(DEFAULT_ALIAS_COUNT);
	userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
	//IR T36000026794 Rel9.0 07/24/2014 starts
	 session.setAttribute("startHomePage", "OrgProfileHome");
	 session.removeAttribute("fromTPHomePage");
	//IR T36000026794 Rel9.0 07/24/2014 End
	//Register a corporate org web bean
	beanMgr.registerBean( "com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
	corporateOrg = (CorporateOrganizationWebBean) beanMgr.getBean("CorporateOrganization");
	
	  CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	   corpOrg.getById(corporateOrgOid);
	 clientBankOid = corpOrg.getAttribute("client_bank_oid");
	 Debug.debug("clientBankOid:"+clientBankOid);
      
	 isReadOnly = !SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_ORG_PROFILE);
	 
	 Debug.debug("MAINTAIN_ORG_PROFILE == " + SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_ORG_PROFILE));
	 String loginLocale = userSession.getUserLocale();

	
        String clientBankID = userSession.getClientBankOid();
        String bogID = userSession.getBogOid();
        String globalID = userSession.getGlobalOrgOid();
        String parentOrgID = userSession.getOwnerOrgOid();
        String ownershipLevel = userSession.getOwnershipLevel();

     //jgadela R90 IR T36000026319 - SQL INJECTION FIX
     List<Object> sqlParams = new ArrayList();
	 StringBuffer rSql = new StringBuffer("SELECT '' user_pref_pay_def_oid, '' a_pay_upload_definition_oid, '' || payment_definition_oid PAY_UPLOAD_DEFINITION_OID, name DESCR, ");
     rSql.append(" '' pmt_file_default_ind FROM payment_Definitions  WHERE a_owner_org_oid in (?,?") ;
     sqlParams.add(corporateOrgOid);
     sqlParams.add(globalID);
     if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
            	 rSql.append(",?");
            	 sqlParams.add(clientBankID);
                 if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
                	 sqlParams.add(bogID);
                	 rSql.append(",?");
                 }
     }

     rSql.append(")");
	 
	 DocumentHandler fileTypesDoc = DatabaseQueryBean.getXmlResultSet(rSql.toString(), false, sqlParams);
	 Debug.debug("standard types sql:"+rSql+"\t fileTypesDoc:"+fileTypesDoc);
	 DocumentHandler fullPayDefDetails = null;
	//if(payDefinitionList != null){
		 //fullPayDefDetails = payDefinitionList;
		 // DK IR T36000023383 Rel8.4 - adding check for null
		 if(fileTypesDoc!= null)
		 	//fullPayDefDetails.addComponent("/",fileTypesDoc);
	 //}else{
		 fullPayDefDetails = fileTypesDoc;
//	 }
	 Debug.debug("fullPayDefDetails:"+fullPayDefDetails);

	//jgadela R90 IR T36000026319 - SQL INJECTION FIX
	String pmtSql = "select user_pref_pay_def_oid,a_pay_upload_definition_oid,p_owner_org_oid,pmt_file_default_ind from user_preferences_pay_def "+
					"where a_pay_upload_definition_oid is not null and   p_owner_org_oid = ? order by user_pref_pay_def_oid";

	Debug.debug("pmt Sql.toString(){}"+pmtSql.toString());
	DocumentHandler payChoiceList = DatabaseQueryBean.getXmlResultSet(pmtSql.toString(),false, new Object[]{corporateOrgOid});
	Debug.debug("\n payChoiceSql:"+payChoiceList);
	
	xmlDoc = formMgr.getFromDocCache();
	
	if (xmlDoc.getDocumentNode("/In/Update/ButtonPressed") != null) {
		saveButtonPressed = xmlDoc.getAttribute("/In/Update/ButtonPressed");
		error = xmlDoc.getFragments("/Error/errorlist/error");
	}
	Debug.debug("doc from cache is " + xmlDoc.toString());
	
	

	if (xmlDoc.getDocumentNode("/In/CorporateOrganization") == null) {
		getDataFromXmlDocFlag = false;
		retrieveFromDBFlag = true;
	}else {
		retrieveFromDBFlag = false;
		getDataFromXmlDocFlag = true;

		String maxError = xmlDoc.getAttribute("/Error/maxerrorseverity");

		if (maxError != null&& maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
			//no errors, get data from db
			errorFlag = false;
			getDataFromXmlDocFlag = false;
			retrieveFromDBFlag = true;
			corporateOrgOid = xmlDoc.getAttribute("/Out/CorporateOrganization/organization_oid");
			//if this was an update not an insert, we need to get from in
			if (corporateOrgOid == null) {
				corporateOrgOid = xmlDoc.getAttribute("/In/CorporateOrganization/organization_oid");
			}
		}else {
			errorFlag = true;
			corporateOrgOid = xmlDoc.getAttribute("/In/CorporateOrganization/organization_oid");
		}
	}
	System.out.println("retrieveFromDBFlag:"+retrieveFromDBFlag+"\t getDataFromXmlDocFlag:"+getDataFromXmlDocFlag);
	
	if (retrieveFromDBFlag) {
		Debug.debug("Retrieving data from DB..");
		getDataFromXmlDocFlag = false;
		corporateOrg.getById(corporateOrgOid);
		//jgadela rel8.3 CR501 [START] -- The page should open in read only mode when  The bank is in the process of updating the corporation's profile at the bank
		ReferenceDataPendingWebBean pendingObj = beanMgr.createBean(ReferenceDataPendingWebBean.class,"ReferenceDataPending");
		corporateOrg = (com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean) pendingObj.getReferenceData(corporateOrg, userSession.getUserOid(), false);

		 if (pendingObj.isReadOnly()) {
		     isReadOnly = true;
			     DocumentHandler errorDoc = new DocumentHandler();
			     String errorMessage = resMgr.getText("E1051","ErrorMessages");
			     errorDoc.setAttribute("/severity", "5");
			     errorDoc.setAttribute("/message", errorMessage);
			     xmlDoc.addComponent("/Error/errorlist/error(0)", errorDoc);
		 }
		//jgadela rel8.3 CR501 [END]
	//Srinivasu_D CR-599 Rel8.4 11/21/2013 - start
	Vector mVector = payChoiceList!=null?payChoiceList.getFragments("/ResultSetRecord"):null;	
    int numMarginItems = mVector!=null?mVector.size():0;
	Debug.debug("numMarginItems:"+numMarginItems);
	String defaultCheck ="";
	int defaultPos = -1;
	 UserPreferencesPayDefWebBean invoiceRule = beanMgr.createBean(UserPreferencesPayDefWebBean.class, "UserPrefPay");

	if(numMarginItems>0){
	//if(numMarginItems>0 && numMarginItems < DEFAULT_ALIAS_COUNT){
		 for (int mLoop = 0; mLoop < numMarginItems; mLoop++) {
			DocumentHandler marginDoc = (DocumentHandler) mVector.elementAt(mLoop);
		    defaultCheck = marginDoc.getAttribute("/PMT_FILE_DEFAULT_IND");
			//if(!TradePortalConstants.INDICATOR_YES.equals(defaultCheck)){
				invoiceRule = beanMgr.createBean(UserPreferencesPayDefWebBean.class, "UserPrefPay");
				invoiceRule.setAttribute("pmt_file_default_ind",defaultCheck);
				 invoiceRule.setAttribute("user_pref_pay_def_oid",marginDoc.getAttribute("/USER_PREF_PAY_DEF_OID"));
			     invoiceRule.setAttribute("a_pay_upload_definition_oid",marginDoc.getAttribute("/A_PAY_UPLOAD_DEFINITION_OID"));
				 aliasList.add(invoiceRule);
			//}	else if(TradePortalConstants.INDICATOR_YES.equals(defaultCheck)){
				//	defaultPos = mLoop;
			//}
		 }
	
// 		 if(defaultPos!=-1){

// 			 invoiceRule = beanMgr.createBean(UserPreferencesPayDefWebBean.class, "UserPrefPay");
// 			 DocumentHandler marginDoc = (DocumentHandler)mVector.elementAt(defaultPos);			
// 			 invoiceRule.setAttribute("pmt_file_default_ind",defaultCheck);
// 			 invoiceRule.setAttribute("user_pref_pay_def_oid",marginDoc.getAttribute("/USER_PREF_PAY_DEF_OID"));
// 			 invoiceRule.setAttribute("a_pay_upload_definition_oid",marginDoc.getAttribute("/A_PAY_UPLOAD_DEFINITION_OID"));
// 			 aliasList.add(defaultPos,invoiceRule);
// 		 }
	}		
		if( numMarginItems < DEFAULT_ALIAS_COUNT){
		  for(int mLoop = numMarginItems; mLoop < DEFAULT_ALIAS_COUNT; mLoop++) {
				aliasList.add(beanMgr.createBean(UserPreferencesPayDefWebBean.class, "UserPrefPay"));
		  }
		}
		Debug.debug("aliasList.size->"+aliasList.size()+"aliasList:"+aliasList);
	}
	
	//for debugging if necessary
	String xmlDocString = xmlDoc.toString();
	if (getDataFromXmlDocFlag) {
		Debug.debug("************************Inside getDataFromXmlDocFlag ***********************************");
		// Populate the corporate organization web bean with the input doc.
		try {
			corporateOrg.populateFromXmlDoc(xmlDoc.getFragment("/In"));
		} catch (Exception e) {
			System.out.println("Contact Administrator: Unable to get CorporateOrganization attributes "+ "for oid: "
							+ corporateOrgOid	+ " "+ e.toString());
		}

	Vector payDefVector = xmlDoc.getFragments("/In/CorporateOrganization/UserPreferencesPayDefList");
    int numMarginItems = payDefVector.size();
    int defaultPos = -1;
    String defaultCheck = "";
    Debug.debug("numMarginItems dSize = "+numMarginItems);
    Debug.debug("xmlDoc.toString()"+xmlDoc.toString());	
	UserPreferencesPayDefWebBean payDefRule = beanMgr.createBean(UserPreferencesPayDefWebBean.class, "UserPrefPay");
	if(numMarginItems>0){
	
	 for (int mLoop = 0; mLoop < numMarginItems; mLoop++) {
      
      DocumentHandler payDefDoc = (DocumentHandler) payDefVector.elementAt(mLoop);
	    defaultCheck = payDefDoc.getAttribute("/pmt_file_default_ind");
	   if(!TradePortalConstants.INDICATOR_YES.equals(defaultCheck)){		  
		   payDefRule.setAttribute("pmt_file_default_ind",defaultCheck);		  		 
		   payDefRule.setAttribute("user_pref_pay_def_oid",payDefDoc.getAttribute("/user_pref_pay_def_oid"));	  
		   payDefRule.setAttribute("a_pay_upload_definition_oid",payDefDoc.getAttribute("/a_pay_upload_definition_oid"));		   
		   aliasList.add(payDefRule);		
		   payDefRule = beanMgr.createBean(UserPreferencesPayDefWebBean.class, "UserPrefPay");
	   }else if(TradePortalConstants.INDICATOR_YES.equals(defaultCheck)){
				defaultPos = mLoop;
		}
	 }
	 if(defaultPos!=-1){
			 payDefRule = beanMgr.createBean(UserPreferencesPayDefWebBean.class, "UserPrefPay");
			 DocumentHandler marginDoc = (DocumentHandler)payDefVector.elementAt(defaultPos);				
			 payDefRule.setAttribute("pmt_file_default_ind",defaultCheck);	
			 payDefRule.setAttribute("user_pref_pay_def_oid",marginDoc.getAttribute("/user_pref_pay_def_oid"));
			 payDefRule.setAttribute("a_pay_upload_definition_oid",marginDoc.getAttribute("/a_pay_upload_definition_oid"));
			 aliasList.add(defaultPos,payDefRule);				
	  }

	 if( numMarginItems < DEFAULT_ALIAS_COUNT){
		  for(int mLoop = numMarginItems; mLoop < DEFAULT_ALIAS_COUNT; mLoop++) {
				aliasList.add(beanMgr.createBean(UserPreferencesPayDefWebBean.class, "UserPrefPay"));
		  }
		}
		
	}

	Debug.debug("numMarginItems:"+numMarginItems +"\t aliasList.size:"+aliasList.size() +"\t aliasList:"+aliasList);
	
	//Srinivasu_D CR-599 Rel8.4 11/21/2013 - End
	}
	
	if (isReadOnly ) {
	      showSaveButtonFlag = false;
	      showSaveCloseButtonFlag = false;
	}
	Debug.debug("finally aliasList:"+aliasList.size());  

%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
 	<jsp:param name="additionalOnLoad"         value="<%=onLoad%>" />
</jsp:include>
 		
 <div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="CorpCustProfileDetail.MyOrganizationProfile"/>
       <jsp:param name="helpUrl" value="customer/my_orgs_profile.htm"/> 
    </jsp:include>
    
    <form name="CorpOrgProfileForm" id="CorpOrgProfileForm" method="POST" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
      <input type=hidden value="" name="buttonName">
      <div class="formArea">
        <jsp:include page="/common/ErrorSection.jsp" />
        <div class="formContent">
        	
        <%
        //MEer Rel 9.2 CR-934A Modifications- Moved Mobile Banking Ind to Customer Profile page
      
        	String parentCorpOrgId = corporateOrg.getAttribute("parent_corp_org_oid");

			corporateOrg.setAttribute("myorg_profile_validation", TradePortalConstants.MY_ORGANIZATION_PROFILE);
        	//MEer Rel 8.3 IR-T36000021992
        	String corpOrgOid = null;
        	if(StringFunction.isNotBlank(parentCorpOrgId)){
        		corpOrgOid = parentCorpOrgId;
        	}
			String pmtBeneSort = corporateOrg.getAttribute("pmt_bene_sort_default"); // DK CR-886 Rel8.4 10/15/2013
        	
            if(TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("inherit_panel_auth_ind"))){
            	defaultCheckBoxValue = true;         	
            }else if(TradePortalConstants.INDICATOR_NO.equals(corporateOrg.getAttribute("inherit_panel_auth_ind"))||"".equals(corporateOrg.getAttribute("inherit_panel_auth_ind"))){
            	defaultCheckBoxValue = false;
            }
        	
    //MEer Rel 8.3 IR-T36000021992 removing the data retrieval as this is moved to fragment
        
    		
        	secureParms.put("UserOid", userSession.getUserOid());
        	secureParms.put("login_oid", userSession.getUserOid());
        	secureParms.put("login_rights", userSession.getSecurityRights());
        	secureParms.put("CorporateOrganizationOid", corporateOrgOid);
        	secureParms.put("Name", corporateOrg.getAttribute("name"));
        	secureParms.put("MyOrgProfileValidation", TradePortalConstants.MY_ORGANIZATION_PROFILE);
        	
       %>
         
       <%=formMgr.getFormInstanceAsInputField("CorpOrgProfileForm",  secureParms)%>
           
       <%--Rel9.5 CR 927B - The Supplier Portal and Host To Host/Credit Note Approval Email Alert Details will be removed from My Organisation Profile and will be added to the new Notification Rules  --%>
        <%=widgetFactory.createSectionHeader("1", "OrgProfileDetail.PanelAuthorisation")%>
       <table id="panelAuthor" width="100%" border="0" cellspacing="0" cellpadding="0">
		 <tr>
			<td colspan="2">
			<div id="inheritParentPanelInd" style="margin-left: -25px">
				<%=widgetFactory.createCheckboxField("InheritPanelAuthInd","OrgProfileDetail.InheritParentPanel", defaultCheckBoxValue, false, false,"onClick=\"inheritPanelAliases();\"","","")%>
			</div>
			</td>	
	     </tr>
		<tr>
		 <td style="padding-right: 10px">
		 <table id="panel1" class="formDocumentsTable" width="100%" border="0" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th width="30%" align="center"><%=resMgr.getText("OrgProfileDetail.PanelLevel",TradePortalConstants.TEXT_BUNDLE)%></th>
					<th width="70%" align="center"><%=resMgr.getText("OrgProfileDetail.PanelAlias", TradePortalConstants.TEXT_BUNDLE)%></th>
				</tr>
				</thead>
				<tr>
					<td align="center"><%=resMgr.getText("OrgProfileDetail.PanelA", TradePortalConstants.TEXT_BUNDLE)%></td>
					<td align="center" style="padding-top: 10px;padding-right: 10px">					
					 <%=  widgetFactory.createTextField("PanelLevelAAlias", "", corporateOrg.getAttribute("panel_level_A_alias"), "30", isReadOnly, false, false, "style=\"width: 200px;\"", "", "" ) %>				
					</td>
				</tr>
				<tr>
					<td align="center"><%=resMgr.getText("OrgProfileDetail.PanelB",TradePortalConstants.TEXT_BUNDLE)%></td>
					<td align="center" style="padding-top: 10px;padding-right: 10px">	
					 <%=  widgetFactory.createTextField("PanelLevelBAlias", "", corporateOrg.getAttribute("panel_level_B_alias"), "30", isReadOnly, false, false, "style=\"width: 200px;\"", "", "" ) %>	
					</td>
				</tr>
				<tr>
					<td align="center"><%=resMgr.getText("OrgProfileDetail.PanelC",	TradePortalConstants.TEXT_BUNDLE)%></td>
					<td align="center" style="padding-top: 10px;padding-right: 10px">	
					 <%=  widgetFactory.createTextField("PanelLevelCAlias", "", corporateOrg.getAttribute("panel_level_C_alias"), "30", isReadOnly, false, false, "style=\"width: 200px;\"", "", "" ) %>				
					</td>
				</tr>
				<tr>
					<td align="center"><%=resMgr.getText("OrgProfileDetail.PanelD",TradePortalConstants.TEXT_BUNDLE)%></td>
					<td align="center" style="padding-top: 10px;padding-right: 10px">
					 <%=  widgetFactory.createTextField("PanelLevelDAlias", "", corporateOrg.getAttribute("panel_level_D_alias"), "30", isReadOnly, false, false, "style=\"width: 200px;\"", "", "" ) %>
					</td>
				</tr>
				<tr>
					<td align="center"><%=resMgr.getText("OrgProfileDetail.PanelE",TradePortalConstants.TEXT_BUNDLE)%></td>
					<td align="center" style="padding-top: 10px;padding-right: 10px">
					 <%=  widgetFactory.createTextField("PanelLevelEAlias", "", corporateOrg.getAttribute("panel_level_E_alias"), "30", isReadOnly, false, false, "style=\"width: 200px;\"", "", "" ) %>	
					</td>
				</tr>	
		</table>

		</td>
		<td style="padding-left: 10px">
		<table id="panel2" class="formDocumentsTable" width="100%" border="0" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th width="30%" style="text-align: center"><%=resMgr.getText("OrgProfileDetail.PanelLevel", TradePortalConstants.TEXT_BUNDLE)%></th>
					<th width="70%" style="text-align: center"><%=resMgr.getText("OrgProfileDetail.PanelAlias", TradePortalConstants.TEXT_BUNDLE)%></th>
				</tr></thead>
				<tr>
					<td align="center"><%=resMgr.getText("OrgProfileDetail.PanelF",	TradePortalConstants.TEXT_BUNDLE)%></td>
					<td align="center" style="padding-top: 10px;padding-right: 10px">
					 <%=  widgetFactory.createTextField("PanelLevelFAlias", "", corporateOrg.getAttribute("panel_level_F_alias"), "30", isReadOnly, false, false, "style=\"width: 200px;\"", "", "" ) %>
					</td>
				</tr>
				<tr>
					<td align="center"><%=resMgr.getText("OrgProfileDetail.PanelG",TradePortalConstants.TEXT_BUNDLE)%></td>
					<td align="center" style="padding-top: 10px;padding-right: 10px">	
					 <%=  widgetFactory.createTextField("PanelLevelGAlias", "", corporateOrg.getAttribute("panel_level_G_alias"), "30", isReadOnly, false, false, "style=\"width: 200px;\"", "", "" ) %>		
					</td>
				</tr>
				<tr>
					<td align="center"><%=resMgr.getText("OrgProfileDetail.PanelH",	TradePortalConstants.TEXT_BUNDLE)%></td>
					<td align="center" style="padding-top: 10px;padding-right: 10px">		
					 <%=  widgetFactory.createTextField("PanelLevelHAlias", "", corporateOrg.getAttribute("panel_level_H_alias"), "30", isReadOnly, false, false, "style=\"width: 200px;\"", "", "" ) %>
					</td>
				</tr>
				<tr>
					<td align="center"><%=resMgr.getText("OrgProfileDetail.PanelI",	TradePortalConstants.TEXT_BUNDLE)%></td>
					<td align="center" style="padding-top: 10px;padding-right: 10px">
					 <%=  widgetFactory.createTextField("PanelLevelIAlias", "", corporateOrg.getAttribute("panel_level_I_alias"), "30", isReadOnly, false, false, "style=\"width: 200px;\"", "", "" ) %>		
					</td>
				</tr>
				<tr>
					<td align="center"><%=resMgr.getText("OrgProfileDetail.PanelJ",TradePortalConstants.TEXT_BUNDLE)%></td>
					<td align="center" style="padding-top: 10px;padding-right: 10px">
					 <%=  widgetFactory.createTextField("PanelLevelJAlias", "", corporateOrg.getAttribute("panel_level_J_alias"), "30", isReadOnly, false, false, "style=\"width: 200px;\"", "", "" ) %>	
					</td>
				</tr>
		</table>
		</td>
	</tr>
	</table>   
	</div>
	
       
	

		<%-- Srinivasu_D CR-599 Rel8.4 11/21/2013 - start --%>
		<% String paymentFileFormat = corpOrg.getAttribute("man_upld_pmt_file_frmt_ind");	
		//jgadela 06/05/2014 R8.4 IR T36000028464.
		if( TradePortalConstants.PAYMENT_MGMT_FILE_DEF.equals(paymentFileFormat) ||
				TradePortalConstants.PAYMENT_MGMT_PILE_FIXED_FORMAT.equals(paymentFileFormat)){ %>
		<%=widgetFactory.createSectionHeader("2","OrgProfileDetail.PaymentFileDefHeader1") %>
		<%	
		 //jgadela 02/25/2013 R8.4 IR T36000026580 - commented the condition - Modified the condition logic in the MyOrgProfile-PaymentFileUploadDef-AddRow.frag
		 // if(TradePortalConstants.PAYMENT_MGMT_FILE_DEF.equals(paymentFileFormat)){
		%>
			<%@ include file="fragments/MyOrgProfile-PaymentFileUploadDef-AddRow.frag" %>
			
		
		
	</div>
<% } %>
	
		<%-- Srinivasu_D CR-599 Rel8.4 11/21/2013 - End --%>
		
  		</div> <%--formContent --%>
       </div> 
       
 
 <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'CorpOrgProfileForm'">
      <jsp:include page="/common/RefDataSidebar.jsp">
        <jsp:param name="showSaveButton" value="<%= showSaveButtonFlag %>" />
        <jsp:param name="saveOnClick" value="none" /> 
        <jsp:param name="showSaveCloseButton" value="<%= showSaveCloseButtonFlag %>" />
        <jsp:param name="saveCloseOnClick" value="none" />
        <jsp:param name="cancelAction" value="goToTradePortalHome" />
        <jsp:param name="showHelpButton" value="false" />
        <jsp:param name="showLinks" value="true" />
        <jsp:param name="helpLink" value="customer/default_no_help_available.htm" />
        <jsp:param name="buttonPressed" value="<%=saveButtonPressed%>" />   
        <jsp:param name="error" value="<%=error%>" /> 
      </jsp:include>
    </div>
    </form>
    <%--MEer Rel 8.3 IR-T36000021992 included a fragment to get panel values of the parent --%>
    <%if(StringFunction.isNotBlank(parentCorpOrgId)){ %>
    	<%@ include file="/transactions/fragments/LoadPanelLevelAliases.frag" %>
    <%} %>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%  

	//(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
   //String saveCloseOnClick;
   // saveCloseOnClick = "common.setButtonPressed('SaveAndClose','0'); document.forms[0].submit();";
	  
	
%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />



<script type="text/javascript">	

function resetPaymentDefIndicator(){
	require(["dijit/registry", "dojo/dom"],   function(registry, dom) {
		if ( registry.byId("DefaultInd").checked == false){
			registry.getEnclosingWidget(document.forms[0].DefaultInd).set('checked', false);
		}
	});
}


<% if(InstrumentServices.isBlank(parentCorpOrgId)){%>
	require(["dojo/ready", "dojo/dom","dojo/dom-style"],function( ready, dom, domStyle) {
		ready(function(){   
			var id = dom.byId("inheritParentPanelInd");		
			 domStyle.set(id,'display','none');
		});
		});
<%}else{
if(TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("inherit_panel_auth_ind"))){%>
require(["dojo/ready"],function( ready) {
	ready(function(){   
		inheritPanelAliases();
	});
});
<%}%>
<%-- MEer Rel 8.3 IR-T36000021992 to get panel values from panelStore--%>
function getPanelLevelAlias(panelLevel){		  
	  panelAlias = panelStore.get(panelLevel).name;		 
	  return panelAlias;
}

function inheritPanelAliases(){
	require(["dijit/registry", "dojo/dom"],   function(registry, dom) {
		if ( registry.byId("InheritPanelAuthInd").checked == true){
			var panelA =getPanelLevelAlias('A');
			var panelB = getPanelLevelAlias('B');
			var panelC =getPanelLevelAlias('C');
			var panelD = getPanelLevelAlias('D');
			var panelE =getPanelLevelAlias('E');
			var panelF = getPanelLevelAlias('F');
			var panelG =getPanelLevelAlias('G'); 
			var panelH = getPanelLevelAlias('H');
			var panelI =getPanelLevelAlias('I');
			var panelJ = getPanelLevelAlias('J');	
		        
			registry.byId("PanelLevelAAlias").set('value',panelA );
			registry.byId("PanelLevelAAlias").set('disabled',true);
			registry.byId("PanelLevelBAlias").set('value', panelB);
			registry.byId("PanelLevelBAlias").set('disabled',true);
			registry.byId("PanelLevelCAlias").set('value', panelC );
			registry.byId("PanelLevelCAlias").set('disabled',true);
			registry.byId("PanelLevelDAlias").set('value', panelD);
			registry.byId("PanelLevelDAlias").set('disabled',true);
			registry.byId("PanelLevelEAlias").set('value', panelE );
			registry.byId("PanelLevelEAlias").set('disabled',true);
			registry.byId("PanelLevelFAlias").set('value', panelF);
			registry.byId("PanelLevelFAlias").set('disabled',true);
			registry.byId("PanelLevelGAlias").set('value', panelG);
			registry.byId("PanelLevelGAlias").set('disabled',true);
			registry.byId("PanelLevelHAlias").set('value', panelH);
			registry.byId("PanelLevelHAlias").set('disabled',true);
			registry.byId("PanelLevelIAlias").set('value', panelI);
			registry.byId("PanelLevelIAlias").set('disabled',true);
			registry.byId("PanelLevelJAlias").set('value', panelJ);
			registry.byId("PanelLevelJAlias").set('disabled',true);
			
		}else if ( registry.byId("InheritPanelAuthInd").checked == false){
				registry.byId("PanelLevelAAlias").set('disabled',false);
				registry.byId("PanelLevelAAlias").set('value','' );
				registry.byId("PanelLevelBAlias").set('disabled',false);
				registry.byId("PanelLevelBAlias").set('value','' );
				registry.byId("PanelLevelCAlias").set('disabled',false);
				registry.byId("PanelLevelCAlias").set('value', '' );
				registry.byId("PanelLevelDAlias").set('disabled',false);
				registry.byId("PanelLevelDAlias").set('value','');
				registry.byId("PanelLevelEAlias").set('disabled',false);
				registry.byId("PanelLevelEAlias").set('value','');
				registry.byId("PanelLevelFAlias").set('disabled',false);
				registry.byId("PanelLevelFAlias").set('value','');
				registry.byId("PanelLevelGAlias").set('disabled',false);
				registry.byId("PanelLevelGAlias").set('value','');
				registry.byId("PanelLevelHAlias").set('disabled',false);
				registry.byId("PanelLevelHAlias").set('value','');
				registry.byId("PanelLevelIAlias").set('disabled',false);
				registry.byId("PanelLevelIAlias").set('value','');
				registry.byId("PanelLevelJAlias").set('disabled',false);
				registry.byId("PanelLevelJAlias").set('value','');
		}
	});
}
<%}%>

function add4MoreAlias() {
	<%-- so that user can only add 4 rows --%>
	var tbl = document.getElementById('paymentAliasTable');<%-- to identify the table in which the row will get insert --%>
	
	var lastElement = tbl.rows.length;	
	for(i=lastElement;i<lastElement+4;i++){
		
   	 	var newRow = tbl.insertRow(i);<%-- creation of new row --%>
		var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
   	 	var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
		var cell2 = newRow.insertCell(2);<%-- three  cell in the row	 --%>
		var cell3 = newRow.insertCell(3);<%-- fourth  cell in the row	 --%>
		newRow.id = "PaymentDef_"+i;		
		j = i-1;

		cell0.innerHTML =" &nbsp;&nbsp;"+(i+1)+".";
		cell0.className = "paymentCol1";

        cell1.innerHTML ="&nbsp;&nbsp;&nbsp;<%=resMgr.getText("OrgProfileDetail.PaymentFileDefLabel",	TradePortalConstants.TEXT_BUNDLE)%>&nbsp;";
        cell2.innerHTML ='&nbsp;&nbsp;&nbsp;<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"PaymentDef'+(j+1)+'\" style=\"width: 14.6em;\" id=\"PaymentDef'+(j+1)+'\" ></select><INPUT TYPE=HIDDEN NAME=\"paymentAliasOid'+(j+1)+'\" VALUE=\"\">';       
		cell3.innerHTML ="";
		 require(["dojo/parser", "dijit/registry"], function(parser, registry) {
         	parser.parse(newRow.id);
         	var paymentDefOid = registry.byId("PaymentDef0");
            if(paymentDefOid!=null){
              var theSelData = paymentDefOid.store.data;
              for(var i=0; i<theSelData.length;i++){				
                      registry.byId('PaymentDef'+(j+1)).store.data.push({name:theSelData[i].name,value:theSelData[i].value, id:theSelData[i].value});					
              }				
              registry.byId('PaymentDef'+(j+1)).attr('displayedValue', "");
            }
    	 });
	}
	<%-- document.getElementById("numberOfMultipleObjects").value = eval(lastElement+3);  --%>
}

function preSaveAndClose(){
  	 	 
 	  document.forms[0].buttonName.value = "SaveAndClose";
 	  return true;

}


</script>
 
</body>
</html>

<%	// Unregister the Corporate Org web bean that was used in this page
	beanMgr.unregisterBean("CorporateOrganization");
	// Finally, reset the cached document to eliminate carryover of
	// information to the next visit of this page.
	formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>