<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.ArrayList, java.util.Hashtable" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 

int DEFAULT_PANELGROUP_COUNT = 1;
  String parmValue1 = parmValue1 = request.getParameter("panelIndex");
  int panelIndex = 0;
  if ( parmValue1 != null ) {
    try {
    	panelIndex = (new Integer(parmValue1)).intValue();
    } 
    catch (Exception ex ) {
      //todo
    }
  }

  String parmValue2 = parmValue2 = request.getParameter("rowIndex");
  int rowIndex = 0;
  if ( parmValue2 != null ) {
    try {
    	rowIndex = (new Integer(parmValue2)).intValue();
    } 
    catch (Exception ex ) {
      //todo
    }
  }
  
  String parmValue3 = parmValue3 = request.getParameter("myDivIdIdx");
  int selectedGridIndex = 0;
  if ( parmValue3 != null ) {
	    try {
	    	selectedGridIndex = (new Integer(parmValue3)).intValue();
	    } 
	    catch (Exception ex ) {
	      //todo
	    }
  }
  
  String loginLocale = userSession.getUserLocale();
  boolean isReadOnlyAcc = Boolean.parseBoolean(request.getParameter("isReadOnly"));
  String corporateOrgOid = request.getParameter("corporateOrgOid");
  
  //other necessaries
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  //MEer Rel 8.3 IR-22490 For PaymentsList-- Retrieve only the panel groups defined for Payment
  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
  String panelAuthGroupSql = "select panel_auth_group_oid, pag.panel_auth_group_id as PAGID ";
  panelAuthGroupSql+="from panel_auth_group pag where payment_type is not null and instrument_group_type='PYMTS' and p_corp_org_oid IN ( ";
  panelAuthGroupSql+="SELECT CONNECT_BY_ROOT organization_oid AS parent_oid FROM corporate_org";
  panelAuthGroupSql+=" WHERE organization_oid = ? CONNECT BY prior organization_oid = p_parent_corp_org_oid";
  panelAuthGroupSql+=" AND INHERIT_PANEL_AUTH_IND = ?)";
  DocumentHandler panelAuthgroupList = DatabaseQueryBean.getXmlResultSet(panelAuthGroupSql.toString(), false, corporateOrgOid, "Y" );
  String pagOptionsPayments = "<option  value=\"\"></option>";
	pagOptionsPayments += ListBox.createOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","");
  
  %>


<%@ include file="fragments/CorporateCustomerPanelRows.frag" %>
