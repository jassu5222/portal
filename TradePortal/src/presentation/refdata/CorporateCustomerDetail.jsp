<%--
**********************************************************************************
                              Corporate Customer Detail
  Description:
     This page is used to maintain individual corporate customer organizations. It
     supports insert, update, and delete.  Errors found during a save are
     redisplayed on the same page. Successful updates return the user to the
     Organizations Home page.s

**********************************************************************************
--%>


<%--
*
*     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*,java.util.*,com.ams.tradeportal.common.cache.*,net.sf.json.*, net.sf.json.xml.*;" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = "";
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  Hashtable secureParms1 = new Hashtable();
  CorporateOrganizationWebBean corporateOrg = null;
  DocumentHandler operationalBankOrgsDoc = null; //xml doc of all op bank orgs for the given client bank
  DocumentHandler reportCategsDoc                 = null; 
  DocumentHandler rateTypeDoc				      = null; 
  DocumentHandler instrumentDoc					  = null; 
  DocumentHandler corporateOrgUsersDoc            = null;
  DocumentHandler corporateOrgsDoc                = null;
  DocumentHandler bankGroupsDoc                   = null;
  DocumentHandler notificationRulesDoc            = null;
  DocumentHandler xmlDoc                          = null;
  QueryListView                  queryListView                   = null;
  StringBuilder                   sqlQuery                        = null;
  Hashtable                      secureParms                     = new Hashtable();
  boolean getDataFromXmlDocFlag = false;
  boolean                        showDeleteButtonFlag            = true;
  boolean                        showSaveButtonFlag              = true;
  boolean                        showSaveCloseButtonFlag         = true;
//jgadela rel8.3 CR501 [START] 
  boolean showApprove = false;
  boolean showReject = false;
//jgadela rel8.3 CR501 [END] 
  boolean                        retrieveFromDBFlag              = false;
  boolean				 InvoiceManagementIndicatorValue2		  =false;
  boolean				 InvoiceManagementIndicatorValue1		  =false;
  boolean				 InterestFinanceAmountIndicator1		  = false;
  boolean				 InterestFinanceAmountIndicator2		  = false;
  boolean                        defaultRadioButtonValue1        = false;
  boolean                        defaultRadioButtonValue2        = false;
  boolean                        defaultRadioButtonValue3        = false;
  boolean                		  defaultRadioButtonValue4        = false;
  boolean                		  defaultRadioButtonValue5        = false;
  boolean                        defaultRadioButtonValueA        = false;
  boolean                        defaultRadioButtonValueB        = false;
  boolean                        defaultRadioButtonValueC        = false;
  boolean                        defaultRadioButtonValueD        = false;
  boolean                        defaultCheckBoxValue            = false;
  boolean                        hasCorporateOrgUsers            = false;
  boolean                        isClientBankUser                = false;
  boolean                        hasCorporateOrgs                = false;
  boolean                        hasNotificationRules            = false;
  boolean                        hasMultipleAddresses            = false;
  boolean insertModeFlag = true;
  boolean                        isReadOnly                      = false;
  boolean errorFlag = false;
  boolean                        isOtherAccount                  = false;
  boolean 						  showButton					  = true; 
  boolean isPayCrNoteUploadAllowed = false;
  boolean isPayCrNoteUploadNotAllowed = false;
   boolean isEndToEndIdProcessAllowed = false;
  boolean isManualCreditNoteApplicationAllowed= false;
  boolean isPropotionateApplication  = false;
  boolean isSequentialApplication  = false;
  
  boolean isBasedOnInvoice = false;
  boolean isPaymentDueDate = false;
  boolean isHighToLowAmount=false;
  boolean isLowToHighAmount=false;
  
  String payCreditNoteAvailableDays =null;
 
  String                     autoLCCreate                    = null;
  String                 processPaymentUploadInd             = null; 
  String                         straightThroughAuthorizeInd      = null; 
  String                         partialPayAuthorizeInd      = null; //Rel9.2 CR 988 11/04/2014 Add
  String                         h2hSystemUserOid            = null; 
  String                         uploadSystemUserOid      	  = null; 
  String autoATPCreate = null; 
  String                         corporateOrgSLCUseOnlyGuarantee = null;
  String                     corporateOrgSLCUseBoth          = null;
  String                         corporateOrgBankTypePartiesInd  = null;
  String                         corporateOrgDocPrepInd          = null;
  String                         corporateOrgLastEntryUserInd    = null;
  String                         corporateOrgBankOrgGroupOid     = "0";
  String                         corporateOrgDefaultUserOid      = null;
  String                         corporateOrgOpBankOrg1Oid       = null;
  String                         corporateOrgOpBankOrg2Oid       = null;
  String                         corporateOrgOpBankOrg3Oid       = null;
  String                         corporateOrgOpBankOrg4Oid       = null;
  String                 corporateOrgReptCateg1Oid     = null;
  String                 corporateOrgReptCateg2Oid     = null;
  String                 corporateOrgReptCateg3Oid     = null;
  String                 corporateOrgReptCateg4Oid     = null;
  String                 corporateOrgReptCateg5Oid     = null;
  String                 corporateOrgReptCateg6Oid     = null;
  String                 corporateOrgReptCateg7Oid     = null;
  String                 corporateOrgReptCateg8Oid     = null;
  String                 corporateOrgReptCateg9Oid     = null;
  String                 corporateOrgReptCateg10Oid    = null;
  String                 pSql                          = null;

  String						  iSql							  = null;
  String						  rSql							  = null;

  String                         corporateOrgParentOrgOid        = null;
  String                         corporateOrgBaseCurrency        = null;
  String                         corporateOrgFTRDualInd          = null;

  String						  corporateOrgARMInvoiceInd		  = null;
  String 						  corporateOrgARMInvoiceDualInd	  = null;

   String						  corporateOrgCreditNoteInd		  = null;
   String 						  corporateOrgCreditNoteDualInd	  = null;

   String                         corporateOrgLRDualInd           = null;
   String                         corporateOrgRADualInd           = null;
   String                         corporateOrgSPDualInd           = null; //Ravindra - CR-708B
   String                         corporateOrgSGTEEDualInd        = null;
   String                         corporateOrgGTEEDualInd         = null;
   String                         corporateOrgECOLDualInd         = null;
   String                         corporateOrgNECOLDualInd        = null;//Vasavi CR 524 03/31/2010 Add
   String                         corporateOrgILCDualInd          = null;
   String                         corporateOrgSLCDualInd          = null;
   String                         corporateOrgELCDualInd          = null;
   String                         corporateOrgAWBDualInd          = null;
   String                         corporateOrgDRDualInd           = null;
   String                         corporateOrgSIDualInd           = null; //SSikhakolli - Rel-9.4 CR-818
   String                         corporateOrgImportColDualInd    = null; //SSikhakolli - Rel-9.4 CR-818
   String                         corporateOrgATPDualInd          = null; //Krishna CR 375-D 07/19/2007
   String                 corporateOrgARMDualInd        = null; //Pratiksha CR-434 09/24/2008
   String                         corporateOrgARMInd              = null; //Pratiksha CR-434 09/24/2008
   String                                   corporateOrgDDDualInd           = null; //Vasavi CR-509 12/10/2009
   String                         corporateOrgDDInd               = null; //Vasavi CR-509 12/10/2009
   String						  corporateOrgARMFileuploadInd	  = null; //Leelavathi CR-710 03/11/2011
   String						  corporateOrgGRInvoicesUploadInd	  = null; //Leelavathi CR-710 Rel - 8.0.0 23/11/2011
   String						  corporateOrgRestrictedInvoicesUploadInd	  = null; //Leelavathi CR-710 Rel 8.0.0 24/11/2011
   String						  corporateOrgRestrictedInvoicesUploadDirectory	  = null; //Leelavathi CR-710 Rel 8.0.0 24/11/2011
   String						  corporateOrgInterestDiscountInd	  = null; //Leelavathi CR-710 Rel - 8.0.0 29/11/2011
   String						  corporateOrgCalculateDiscountInd	  = null; //Leelavathi CR-710 Rel - 8.0.0 29/11/2011
   String						  corporateOrgSPCalculateDiscountInd	  = null; //Ravindra - CR-708B
   String						  corporateOrgPOInd				  = null; //Leelavathi CR-707 Rel-8.0.0.0 01/02/2012
   String						  corporateOrgPODualInd				  = null; //Leelavathi CR-707 Rel-8.0.0.0 01/02/2012
   String						  corporateOrgRestrictedPOUploadDirectory	  = null; //Leelavathi CR-707 Rel 8.0.0 03/02/2012
   String						  corporateOrgRestrictedPurchaseOrderUploadInd	  = null; //Leelavathi CR-707 Rel 8.0.0 24/11/2012
   String                         corporateOrgSGTEEInd            = null;
   String                         corporateOrgFTRInd              = null;
   String                         corporateOrgLRInd               = null;
   String                         corporateOrgNonPortalOrigInstrInd    = null;      //IAZ IR-GAUJ052680790 102709 Add
   String                         corporateOrgRAInd               = null;
   String                         corporateOrgSPInd               = null; //Ravindra - CR-708B
   String corporateOrgCountry             = null;
   String                         corporateOrgECOLInd             = null;
   String                         corporateOrgNECOLInd             = null; //Vasavi CR 524 03/31/2010 Add
   String                         corporateOrgImportCOLInd             = null; //SSikhakolli - Rel-9.4 CR-818
   String                         corporateOrgSIInd             = null; //SSikhakolli - Rel-9.4 CR-818
   String                         corporateOrgGTEEInd             = null;
   String                         corporateOrgILCInd              = null;
   String                         corporateOrgSLCInd              = null;
   String                         corporateOrgELCInd              = null;
   String                         corporateOrgAWBInd              = null;
   String                         corporateOrgATPInd              = null;
   String                         userSecurityRights              = null;
   String                         corporateOrgName                = null;
   String                         corporateOrgOid                 = null;
   String                         dropdownOptions                 = null;
   String                         clientBankOid                   = null;
   String                         defaultText                     = null;
   String                         userOrgOid                      = null;
   String                         userLocale                      = null;
   String                         userType                        = null;
   String                         onLoad                          = null;
   String                          corporateOrgNotifRuleOid        = null;
   String                         corporateOrgEmailLanguage   = null;
   String                         corporateOrgTimeoutAutoSaveInd  = null;
   String                         allowManualPOEntry              = null;
   String                         allowATPManualPOEntry           = null;
   String                         defaultAuthMethod               = null;
   String                                   corporateOrgTBAInd                  = null;
   String                                 corporateOrgDPInd                     = null;
   String                                   corporateOrgDualTBAInd          = null;
   String                                   corporateOrgDualDPInd               = null;
   String                                   allowPymtInstrumentAuth         = null;
   String                                   allowPanelAuthForPymts            = null;
   String                                   buttonPressed                             = "";
   String                                   enforceDailyLimitTransfer       = null;
   String                                   enforceDailyLimitDomesticPayment= null;
   String                                   enforceDailyLimitIntlPymt       = null;
   String                                   otherAccountsHidden = "visibility:hidden";
   String                                   allowPayByAnotherAccnt          = null;
   String tpsIntegCustomer = null;
    String atpProcessInvoice = null;
   String failedInvoiceInd = null;
   String allowPaymentDate = null;
   String daysBefore = null;
   String daysAfter = null;
   String						  corporateErpReporting		  = null; //AAlubala - Rel8.2 CR741 - ERP Reporting 
	String						     corporateDiscountCodeType = null; //AAlubala - Rel8.2 CR741 - Corporate discount type
	String						  corporateOrgPayProgName			  = null; 
	  String corporateOrgAWBCheckerAuthIndicator = null;
	  String corporateOrgATPCheckerAuthIndicator = null;
	  String corporateOrgECOLCheckerAuthInd = null;
	  String corporateOrgExpDLCCheckerAuthInd = null;
	  String corporateOrgNewExpCollCheckerAuthInd = null;
	  String corporateOrgImportDLCCheckerAuthInd = null;
	  String corporateOrgLoanRequestCheckerAuthInd = null;
	  String corporateOrgGuaranteesCheckerAuthInd = null;
	  String corporateOrgSLCCheckerAuthInd = null;
	  String corporateOrgSHPCheckerAuthInd = null;
	  String corporateOrgRQACheckerAuthInd = null;
	  String corporateOrgSPCheckerAuthInd = null;
	  String corporateOrgDRCheckerAuthInd = null;
	  String corporateOrgSICheckerAuthInd = null; //SSikhakolli - Rel-9.4 CR-818
	  String corporateOrgIncludeSettleInstrInd = null; //SSikhakolli - Rel-9.4 CR-818
	  String corporateOrgImportCollCheckerAuthInd = null; //SSikhakolli - Rel-9.4 CR-818
	  String corporateOrgFTRQCheckerAuthInd = null;
	  String corporateOrgFTDPCheckerAuthInd = null;
	  String corporateOrgFTBACheckerAuthInd = null;
	  String corporateOrgMatchingApprCheckerAuthInd = null;
	  String corporateOrgInvoiceAuthCheckerAuthInd = null;
	  String corporateOrgCreditAuthCheckerAuthInd = null;
	 String corporateOrgPaymentBenAuthInd = null;
	  String corporateOrgPayMgmtFileInd = null;
      String corporateOrgPayMgmtDirInd = null;
	  String corporateOrgPayMgmtDirLoc = null;
	  String corporateOrgPayInvAuthInd = null;
	 String corporateOrgPayInvMgmtInd = null;
	 String corporateOrgPayCrNoteMgmtInd = null;
	 String corporateOrgDualPayInvAuthUploadedInd = null;
	 String corporateOrgDualPayInvAuthInd = null;
	 String corporateOrgDualPayCrNoteAuthInd = null;
	 String corporateOrgInvFileUploadPayInd = null;
	 String corporateOrgInvFileUploadRecInd = null;
	 String corporateOrgPayInvGroupingInd = null;
	 String corporateOrgPayInvUtilisedInd = null;
	 String corporateOrgRecInvUtilisedInd = null;
	 String corporateOrgPreDebitOptionInd    = null;
	 String corporateOrgMobileBankingInd = null;
	//#RKAZI CR1006 04-24-2015  - BEGIN
	 String corporateOrgDecRECInvoiceDualInd	  = null;
    String corporateOrgDecPAYInvoiceDualInd	  = null;
    String corporateOrgPayDecCRNDualInd = null;
    String corporateOrgDecRECInvoiceInd	= null;
    String corporateOrgDecPAYInvoiceInd	= null;
    String corporateOrgPayDecCRNInd = null;
    String corporateOrgRecInvAllowH2hApprovalInd = null;
    String corporateOrgPayInvAllowH2hApprovalInd = null;
    String corporateOrgPayCrnAllowH2hApprovalInd = null;
	//#RKAZI CR1006 04-24-2015  - END
	
	String oidDocFinal = "";
    String textDocFinal = "";
	
	//SURREWSH -Rel-9.4 CR-932
	String    corporateOrgBillingInstrumentsIndicator = null;
	boolean isUserDefinedFieldEnable = false;
    DocumentHandler doc = null;
  String savebuttonPressed ="";

  doc = formMgr.getFromDocCache();
  savebuttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  Vector error = doc.getFragments("/Error/errorlist/error");
  
  //jgadela - Rel8.3 CR ANZ501 03/28/2013 START
  String corpUserDualCtrlReqdInd   = null;    
  String panelAuthorisationDualCtrlReqdInd   = null;   
  String pendingReferenceDataOid = "";
  String pendingChangedObjectOid		 = "0";
  Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
  String adminUserDualCtrlReqdInd = CBCResult.getAttribute("/ResultSetRecord(0)/CORP_ORG_DUAL_CTRL_REQD_IND");
  String utilizedAddlField = CBCResult.getAttribute("/ResultSetRecord(0)/UTILIZE_ADDITIONAL_BANK_FIELDS");
  if (TradePortalConstants.INDICATOR_YES.equals(utilizedAddlField)) {
	   isUserDefinedFieldEnable = true;
  }
  //jgadela - Rel8.3 CR ANZ501 03/28/2013 END
  //MEer Rel 9.1 Updates to CR-934A
  String cbMobileBankingInd = CBCResult.getAttribute("/ResultSetRecord(0)/MOBILE_BANKING_ACCESS_IND"); 
  
  //Getting the resources for Dialog Titles
  String addSecondaryAddressTitle = resMgr.getText("addSecondaryAddressDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String updateSecondaryAddressTitle = resMgr.getText("updateSecondaryAddressDialog.title", TradePortalConstants.TEXT_BUNDLE);

  boolean panelPageWithErrors = false;
  String corporateOrgTimeZone = null;

  Vector accountVector = null;	

//DK CR709 Rel8.2 10/31/2012 Begin
   String                corporateOrgProcessInvoicesFileUploadInd    = null;
   String                corporateOrgTradeLoanInd    = null;
   String                corporateOrgImportLoanInd    = null;
   String                corporateOrgExportLoanInd    = null;
   String                corporateOrgInvoiceFinancingInd   = null;
   String                corporateOrgAutoLoanRequestInvUpldInd   = null;
   boolean		 UsePaymentDateAllowedIndicatorValue2		  =false;
   boolean		 UsePaymentDateAllowedIndicatorValue1		  =false;
   String                corporateOrgUsePaymentDateAllowedInd    = null;
   String                corporateOrgAllowedDaysBeforePaymentDate    = null;
   String                corporateOrgAllowedDaysAfterPaymentDate    = null;
   String                corporateOrgFinancingDaysBeforeLoanMaturity    = null;
   String                corporateOrgInvoicePercentToFinanceTradeLoan    = null;
   String                         corporateOrgAutoATPInvUpldInd   = null;
   //DK CR709 Rel8.2 10/31/2012 End
  int DEFAULT_ALIAS_COUNT = 4;
  List aliasList = new ArrayList(DEFAULT_ALIAS_COUNT);

  int DEFAULT_MARGINRULE_COUNT = 4;
  List marginRuleList = new ArrayList(DEFAULT_MARGINRULE_COUNT); 
  
  //CR 821 - Rel 8.3
  int DEFAULT_PANELGROUP_COUNT = 2;
  int numAlias;
  int aliasRowCount = 0;
  if(request.getParameter("numberOfAliasObjects")!=null)
    aliasRowCount = Integer.parseInt(request.getParameter("numberOfAliasObjects"));
  else
    aliasRowCount =4;
   
  String transferDailyLimitAmt = null;
  String domesticPmtDailyLimitAmt = null;
  String intlPmtDailyLimitAmt = null;
  //Kyriba CR 268 Start
 
  DocumentHandler externalBankOrgsDoc = null;
  StringBuilder extBankSql = new StringBuilder();
  int DEFAULT_EXTERNAL_BANK_COUNT = 6;
  List externalBankList = new ArrayList(DEFAULT_EXTERNAL_BANK_COUNT);
  int numExtBank=0;  
  
  //numberOfMultipleObjects3 is used in amsServlet/xmlTransport to get/set the external bank Object count
  int extBankObjCount = 0;
  if(request.getParameter("numberOfMultipleObjects3")!=null)
	  extBankObjCount = Integer.parseInt(request.getParameter("numberOfMultipleObjects3"));
  else
	  extBankObjCount = 6;
  
  //Kyriba CR 268 End
	//jgadela Rel 8.4 CR-854 [END]- Added condition to filter the default locale categories
  pSql = "select CODE, DESCR from refdata where table_type = ? and locale_name is NULL";
  reportCategsDoc = DatabaseQueryBean.getXmlResultSet(pSql, false, new Object[]{"REPORTING_CATEGORY"});
 
   rSql = "select CODE, DESCR from refdata where table_type = ?";
  rateTypeDoc = DatabaseQueryBean.getXmlResultSet(rSql, false,  new Object[]{"REFINANCE_RATE_TYPE"});
  
  iSql = "select CODE, DESCR from refdata where table_type = ?";
  instrumentDoc = DatabaseQueryBean.getXmlResultSet(iSql, false, new Object[]{"INSTRUMENT_TYPE"});

  // Register a corporate org web bean
  beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");

  corporateOrg = (CorporateOrganizationWebBean) beanMgr.getBean("CorporateOrganization");
  xmlDoc = formMgr.getFromDocCache();
 
  if (xmlDoc.getDocumentNode("/In/Update/ButtonPressed") != null) {
    buttonPressed = xmlDoc.getAttribute("/In/Update/ButtonPressed");
  }

  String options;
  String loginLocale = userSession.getUserLocale();

  int row2Count = 0; 
  if(request.getParameter("numberOfMultipleObjects2")!=null)
    row2Count = Integer.parseInt(request.getParameter("numberOfMultipleObjects2"));
  else
    row2Count =8;
  
  //Ravindra - CR-708B - Start
  int DEFAULT_SP_MARGINRULE_COUNT = 4;
  List spMarginRuleList = new ArrayList(DEFAULT_SP_MARGINRULE_COUNT);
  int spMarginRulesCount = 0; 
  if(request.getParameter("numberOfMultipleObjects1")!=null)
	  spMarginRulesCount = Integer.parseInt(request.getParameter("numberOfMultipleObjects1"));
  else
	  spMarginRulesCount =4;

  // Retrieve the user's locale, security rights, and organization oid
  userSecurityRights = userSession.getSecurityRights();
  userLocale         = userSession.getUserLocale();
  userOrgOid         = userSession.getOwnerOrgOid();
  clientBankOid      = userSession.getClientBankOid();

  // Retrieve the type of user so that we can get the appropriate read only rights; we can only be on this
  // page if we're a client bank user or a BOG user
  userType = userSession.getOwnershipLevel();

//Retrieve the user's security type (i.e., ADMIN or NON_ADMIN)
String         userSecurityType        = null;
  if (userSession.getSavedUserSession() == null) {
  		userSecurityType = userSession.getSecurityType();
   }else {
	   userSecurityType = userSession.getSavedUserSessionSecurityType();
   }
  if (userType.equals(TradePortalConstants.OWNER_BANK))
  {
    isClientBankUser = true;

    isReadOnly = !SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_CORP_BY_CB);
  }
  else
  {
    isReadOnly = !SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_CORP_BY_BG);
  }

  // Get the document from the cache.  We'll use it to repopulate the screen if the
  // user was entering data with errors.
  xmlDoc = formMgr.getFromDocCache();
  boolean addAccountButtonPressed = buttonPressed.equals("AddAccounts");

  if (xmlDoc.getDocumentNode("/In/CorporateOrganization") == null || addAccountButtonPressed)
  {
    // The document does not exist in the cache.  We are entering the page from
    // the listview page (meaning we're opening an existing item or creating
    // a new one.) or back from the multiple address page
    if(request.getParameter("oid") != null)
    {
      corporateOrgOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
    }
    //This holds the value of organization_oid if AddAcounts button is pressed.
    //if AddAcount button is pressed we need to display the page values from DB instead of XML from cache.
    //But the initial value we are getting from teh XML Cache .
    //This step is perfomred since the ajax call doesnot hold the value of the subsidary accounts in the
    //inputDoc
    else if(addAccountButtonPressed == true)
    {
      corporateOrgOid = xmlDoc.getAttribute("/In/CorporateOrganization/organization_oid");
    }
    else
    {
      corporateOrgOid = xmlDoc.getAttribute("/In/Address/corp_org_oid");//null if NOT back from address page
    }
    pendingChangedObjectOid		 = corporateOrgOid;

    if (corporateOrgOid == null)
    {
      getDataFromXmlDocFlag = false;
      retrieveFromDBFlag    = false;
      insertModeFlag        = true;
      corporateOrgOid       = "0";
    }
    else if (corporateOrgOid.equals("0"))
    {
      getDataFromXmlDocFlag = false;
      retrieveFromDBFlag    = false;
      insertModeFlag        = true;
    }
    else
    {
      // We have a good oid so we can retrieve.
      getDataFromXmlDocFlag = false;
      retrieveFromDBFlag    = true;
      insertModeFlag        = false;
    }
  }
  else
  {
    // We've returned from a save/update/delete
    // We will display data from the cache.  
    retrieveFromDBFlag    = false;
    getDataFromXmlDocFlag = true;
     pendingReferenceDataOid = doc.getAttribute("/Out/PendingReferenceData/oid");
    if(pendingReferenceDataOid == null || "".equals(pendingReferenceDataOid) || "null".equals(pendingReferenceDataOid)){
  	  pendingReferenceDataOid = doc.getAttribute("/In/PendingReferenceData/oid");
    }


    // Examine the document to see if we have errors or not
    String maxError = xmlDoc.getAttribute("/Error/maxerrorseverity");
    
    if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
      //no errors, get data from db
      errorFlag = false;
      
      
      corporateOrgOid = xmlDoc.getAttribute("/Out/CorporateOrganization/organization_oid");
      //if this was an update not an insert, we need to get from in
      if (corporateOrgOid == null ) {
        corporateOrgOid = xmlDoc.getAttribute("/In/CorporateOrganization/organization_oid");
      }
      pendingChangedObjectOid		 = corporateOrgOid;
      insertModeFlag = false;
       if(pendingReferenceDataOid == null || "".equals(pendingReferenceDataOid)){
    	  getDataFromXmlDocFlag = false;
    	  retrieveFromDBFlag = true;
      }
    }
    else { //error
      errorFlag = true;

      //Determine if we're in insertMode by looking for a '0' oid in the input section
      // of the doc.
      corporateOrgOid = xmlDoc.getAttribute("/In/CorporateOrganization/organization_oid");

      // IR AAUI120245963 & ALUI120250326 Chandrakanth 12/05/2008 End
      if ("0".equals(corporateOrgOid))
      {
        insertModeFlag = true;
      }
      else
      {
        // Not in insert mode, use oid from output doc.
        insertModeFlag = false;
      }
    }
  }

  if(!retrieveFromDBFlag && !getDataFromXmlDocFlag) {
    //DK CR-587 Rel7.1 BEGINS
    for (int i = 0; i < DEFAULT_ALIAS_COUNT; i++) {
      aliasList.add(beanMgr.createBean(CustomerAliasWebBean.class, "CustomerAlias"));
    }
    //DK CR-587 Rel7.1 ENDS
    //Suresh CR-713 Rel 8.0 10/07/11 Begin
    for(int i = 0; i < DEFAULT_MARGINRULE_COUNT; i++) {
      marginRuleList.add(beanMgr.createBean(CustomerMarginRuleWebBean.class, "CustomerMarginRule"));
    }
    //Suresh CR-713 Rel 8.0 10/07/11 End
    
    //Ravindra - CR-708B - Start
    for(int i = 0; i < DEFAULT_SP_MARGINRULE_COUNT; i++) {
      spMarginRuleList.add(beanMgr.createBean(SPMarginRuleWebBean.class, "SPMarginRule"));
    }
  //Ravindra - CR-708B - End
  
  //Kyriba CR 268
  //Create Default ExternalBankWebBean Objects
  for(int i=0; i<DEFAULT_EXTERNAL_BANK_COUNT; i++) {
      externalBankList.add(beanMgr.createBean(ExternalBankWebBean.class, "ExternalBank"));
    }
  }

  if (retrieveFromDBFlag)
  {
    // Attempt to retrieve the item.  It should always work.  Still, we'll
    // check for a blank oid -- this    indicates record not found.  Set to insert
    // mode and display error.

    getDataFromXmlDocFlag = false;

    corporateOrg.getById(corporateOrgOid);
    
  //jgadela rel8.3 CR501 [START] 
     String loggedInUserId = userSession.getUserOid();
    if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction"))){
     showApprove = true;
   	 showReject = true;
    }
    String ownerShipLevel = userSession.getOwnershipLevel();
    if(TradePortalConstants.OWNER_GLOBAL.equals(ownerShipLevel) || TradePortalConstants.OWNER_BOG.equals(ownerShipLevel)){
   	 ownerShipLevel = TradePortalConstants.OWNER_BANK;
    }
    ReferenceDataPendingWebBean pendingObj = beanMgr.createBean(ReferenceDataPendingWebBean.class,"ReferenceDataPending");
    corporateOrg = (com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean) pendingObj.getReferenceData(corporateOrg, loggedInUserId, showApprove);
   //If changed by same user then do not show approve/reject buttons
    if (userSession.getUserOid().equals(pendingObj.getAttribute("change_user_oid")) || isReadOnly ||
			 !ownerShipLevel.equals(pendingObj.getAttribute("ownership_level")) || !userSecurityType.equals(pendingObj.getAttribute("changed_user_security_type"))){
		 showApprove = false;
   	   showReject = false;
	 }
		
    pendingReferenceDataOid = pendingObj.getAttribute("pending_data_oid");
    //IR 21408 - if user has maintain access then show error alert
	if (pendingObj.isReadOnly() && !isReadOnly) {
	     isReadOnly = true;
	     if(showApprove == false){
		     DocumentHandler errorDoc = new DocumentHandler();
		     String errorMessage = resMgr.getText(TradePortalConstants.PENDING_USER_ALREADY_EXISTS2,"ErrorMessages");
		     String textM = resMgr.getText("PendingRefdata.Customer","TextResources");
		     errorMessage = errorMessage.replace("{0}",textM);
		     errorDoc.setAttribute("/severity", "5");
		     errorDoc.setAttribute("/message", errorMessage);
		     doc.addComponent("/Error/errorlist/error(0)", errorDoc);
	     }
	 }
	 if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction")) ){
		 getDataFromXmlDocFlag = true;
		 retrieveFromDBFlag = false;
	 	 String xmlData = StringFunction.xssHtmlToChars(pendingObj.getAttribute("changed_object_data"));
	 	 DocumentHandler tempDoc =  new DocumentHandler(xmlData,false);
	 	 xmlDoc = new DocumentHandler();
	 	 xmlDoc.setComponent ("/In", tempDoc);
    }else{
    	getDataFromXmlDocFlag = false;
    }
	//jgadela rel8.3 CR501 [END]

    if (corporateOrg==null || "".equals(corporateOrg.getAttribute("organization_oid"))) {
      //this should not happen! todo: raise an error?
      insertModeFlag        = true;
      corporateOrgOid       = "0";
    }
    else {
    	//jgadela rel8.3 CR501 -- Added if condition if the case of approving newly added pending record
   	 String change_type = pendingObj.getAttribute("change_type");
   	 if("C".equalsIgnoreCase(change_type)){
   		insertModeFlag = false; 
   		corporateOrgOid = "0";
   	 }else{
   		insertModeFlag = false;
   	 }
      
    }

  }

  //for debugging if necessary
  String xmlDocString = xmlDoc.toString();

  AccountWebBean account = null;
  ExternalBankWebBean extBankWebBean = null; //kyriba
  if (getDataFromXmlDocFlag) {
	  
	//jgadela rel8.3 CR501 [START]  - IR T36000018219
	  com.ams.tradeportal.busobj.webbean.ReferenceDataPendingWebBean pendingBean = null;
	  beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.ReferenceDataPendingWebBean", "ReferenceDataPending");

	  pendingBean = (ReferenceDataPendingWebBean) beanMgr.getBean("ReferenceDataPending");
	  pendingBean.getById(pendingReferenceDataOid);
	    
	  String change_type = pendingBean.getAttribute("change_type");
	  if("C".equalsIgnoreCase(change_type)){
	   		insertModeFlag = false;
	   		corporateOrgOid = "0";
	  }
	   //jgadela rel8.3 CR501 [END]

    // Populate the corporate organization web bean with the input doc.
    try {
      corporateOrg.populateFromXmlDoc(xmlDoc.getFragment("/In"));
    }
    catch (Exception e) {
      System.out.println("Contact Administrator: Unable to get CorporateOrganization attributes " +
                         "for oid: " + corporateOrgOid + " " + e.toString());
    }

    // Populate account[] bean with account info from document handler
    accountVector = xmlDoc.getFragments("/In/CorporateOrganization/AccountList");
  }

 
  System.out.println ("getDataFromXmlDocFlag-- >" + getDataFromXmlDocFlag);
  System.out.println ("retrieveFromDBFlag-- >" + retrieveFromDBFlag);
  
  if (getDataFromXmlDocFlag) {	  
    // Populate customerAlias[] bean with customerAlias info from document handler
    Vector aliasVector = xmlDoc.getFragments("/In/CorporateOrganization/CustomerAliasList");
    numAlias = aliasVector.size();

     for (int iLoop = 0; iLoop < numAlias; iLoop++) {
      CustomerAliasWebBean customerAlias = beanMgr.createBean(CustomerAliasWebBean.class, "CustomerAlias");
      DocumentHandler aliasDoc = (DocumentHandler) aliasVector.elementAt(iLoop);
      String sValue = aliasDoc.getAttribute("/alias");
      customerAlias.setAttribute("alias", sValue);

      sValue = aliasDoc.getAttribute("/customer_alias_oid");
      customerAlias.setAttribute("customer_alias_oid", sValue);

      sValue = aliasDoc.getAttribute("/gen_message_category_oid");
      customerAlias.setAttribute("gen_message_category_oid", sValue);

      aliasList.add(customerAlias);
    }

    if (numAlias < DEFAULT_ALIAS_COUNT) {
      for (int iLoop = numAlias; iLoop < DEFAULT_ALIAS_COUNT; iLoop++) {
        aliasList.add(beanMgr.createBean(CustomerAliasWebBean.class, "CustomerAlias"));
      }
    }
    //Kyriba CR 268 Start
    // Get ExternalBankList from the document Cache.
    Vector extBankVector = xmlDoc.getFragments("/In/CorporateOrganization/ExternalBankList");
    numExtBank = extBankVector.size();

    Debug.debug("From Doc Cache : numExtBank = " + numExtBank);
    Debug.debug("extBankVector = "+extBankVector);
	//Get all external Bank Details and store it in webbean
    for (int iLoop = 0; iLoop < numExtBank; iLoop++) {
      extBankWebBean = beanMgr.createBean(ExternalBankWebBean.class, "ExternalBank");
      DocumentHandler extBankDoc = (DocumentHandler) extBankVector.elementAt(iLoop);
      String sValue = extBankDoc.getAttribute("/external_bank_oid");
      extBankWebBean.setAttribute("external_bank_oid", sValue);

      sValue = extBankDoc.getAttribute("/corp_org_oid");
      extBankWebBean.setAttribute("corp_org_oid", sValue);

      sValue = extBankDoc.getAttribute("/op_bank_org_oid");
      extBankWebBean.setAttribute("op_bank_org_oid", sValue);

      externalBankList.add(extBankWebBean);
    }
    if (numExtBank < DEFAULT_EXTERNAL_BANK_COUNT) {
        for (int iLoop = numExtBank; iLoop < DEFAULT_EXTERNAL_BANK_COUNT; iLoop++) {
        	externalBankList.add(beanMgr.createBean(ExternalBankWebBean.class, "ExternalBank"));
        }
      }
    //Kyriba CR 268 End
  }

  if (getDataFromXmlDocFlag) {

    Vector mVector = xmlDoc.getFragments("/In/CorporateOrganization/CustomerMarginRuleList");
    int numMarginItems = mVector.size();
  
    for (int mLoop=0; mLoop<numMarginItems; mLoop++) {
      CustomerMarginRuleWebBean marginRule = beanMgr.createBean(CustomerMarginRuleWebBean.class, "CustomerMarginRule");
      DocumentHandler marginRuleDoc = (DocumentHandler) mVector.elementAt(mLoop);
      String mValue = marginRuleDoc.getAttribute("/customer_margin_rule_oid");		
      marginRule.setAttribute("customer_margin_rule_oid", mValue);
 
      mValue = marginRuleDoc.getAttribute("/currency");		 
      marginRule.setAttribute("currency", mValue);
 		 
      mValue = marginRuleDoc.getAttribute("/instrument_type");		 
      marginRule.setAttribute("instrument_type", mValue);
 
      mValue = marginRuleDoc.getAttribute("/threshold_amt");		 
      marginRule.setAttribute("threshold_amt", mValue);
 		 
      mValue = marginRuleDoc.getAttribute("/rate_type");		 
      marginRule.setAttribute("rate_type", mValue);
 
      mValue = marginRuleDoc.getAttribute("/margin");		 
      marginRule.setAttribute("margin", mValue);
      marginRuleList.add(marginRule);
    }
    if(numMarginItems < DEFAULT_MARGINRULE_COUNT) {
      for(int mLoop = numMarginItems; mLoop < DEFAULT_MARGINRULE_COUNT; mLoop++) {
        marginRuleList.add(beanMgr.createBean(CustomerMarginRuleWebBean.class, "CustomerMarginRule"));
      }
    }	
    
    //Ravindra - CR-708B - Start
    mVector = xmlDoc.getFragments("/In/CorporateOrganization/SPMarginRuleList");
    int numSpMarginItems = mVector.size();
     
    for (int mLoop=0; mLoop<numSpMarginItems; mLoop++) {
      SPMarginRuleWebBean spMarginRule = beanMgr.createBean(SPMarginRuleWebBean.class, "SPMarginRule");
      DocumentHandler spMarginRuleDoc = (DocumentHandler) mVector.elementAt(mLoop);
      String mValue = spMarginRuleDoc.getAttribute("/sp_margin_rule_oid");		
      spMarginRule.setAttribute("sp_margin_rule_oid", mValue);
 
      mValue = spMarginRuleDoc.getAttribute("/currency");			 
      spMarginRule.setAttribute("currency", mValue);
      
      mValue = spMarginRuleDoc.getAttribute("/threshold_amt");			 
      spMarginRule.setAttribute("threshold_amt", mValue);
 		 
      mValue = spMarginRuleDoc.getAttribute("/rate_type");		 
      spMarginRule.setAttribute("rate_type", mValue);
      
      mValue = spMarginRuleDoc.getAttribute("/margin");		 
      spMarginRule.setAttribute("margin", mValue);
 
      spMarginRuleList.add(spMarginRule);
    }
    if(numSpMarginItems < DEFAULT_SP_MARGINRULE_COUNT) {
      for(int mLoop = numSpMarginItems; mLoop < DEFAULT_SP_MARGINRULE_COUNT; mLoop++) {
        spMarginRuleList.add(beanMgr.createBean(SPMarginRuleWebBean.class, "SPMarginRule"));
      }
    }	
   }
 
  // Determine whether the Delete and Save buttons should be displayed
  if (isReadOnly || insertModeFlag) {
    showDeleteButtonFlag = false;

    if (isReadOnly) {
      showSaveButtonFlag = false;
      showSaveCloseButtonFlag = false;
    }
  }

  // Set the text to display next to the Corporate Customers link based on whether the Create
  // button was selected or an existing Corporate Organization was selected from the previous page
  
  if (corporateOrg != null) {
    // Retrieve the corporate organization name and country from the corporate organization that was selected
    
    tpsIntegCustomer				= corporateOrg.getAttribute("cust_is_not_intg_tps");//SSikhakolli - Rel-9.3.5 CR-1029
    corporateOrgName                = corporateOrg.getAttribute("name");
    corporateOrgCountry             = corporateOrg.getAttribute("address_country");
    corporateOrgBaseCurrency        = corporateOrg.getAttribute("base_currency_code");
    corporateOrgDefaultUserOid      = corporateOrg.getAttribute("default_user_oid");
    corporateOrgParentOrgOid        = corporateOrg.getAttribute("parent_corp_org_oid");
    corporateOrgLastEntryUserInd    = corporateOrg.getAttribute("auth_user_different_ind");
    corporateOrgBankTypePartiesInd  = corporateOrg.getAttribute("allow_create_bank_parties");
    corporateOrgDocPrepInd          = corporateOrg.getAttribute("doc_prep_ind");
    corporateOrgBankOrgGroupOid     = corporateOrg.getAttribute("bank_org_group_oid");
    if((corporateOrgBankOrgGroupOid == null) || corporateOrgBankOrgGroupOid.equals(""))
      corporateOrgBankOrgGroupOid = "0";
    corporateOrgSLCUseOnlyGuarantee = corporateOrg.getAttribute("standbys_use_guar_form_only");
    corporateOrgSLCUseBoth              = corporateOrg.getAttribute("standbys_use_either_form");
    corporateOrgILCInd              = corporateOrg.getAttribute("allow_import_DLC");

    corporateOrgSLCInd              = corporateOrg.getAttribute("allow_SLC");
    corporateOrgELCInd              = corporateOrg.getAttribute("allow_export_LC");
    corporateOrgECOLInd             = corporateOrg.getAttribute("allow_export_collection");
    corporateOrgNECOLInd             = corporateOrg.getAttribute("allow_new_export_collection"); //Vasavi CR 524 03/31/2010 Add
    corporateOrgImportCOLInd             = corporateOrg.getAttribute("allow_imp_col"); //SSikhakolli - Rel-9.4 CR-818
    corporateOrgSIInd             = corporateOrg.getAttribute("allow_settle_instruction"); //SSikhakolli - Rel-9.4 CR-818
    corporateOrgGTEEInd             = corporateOrg.getAttribute("allow_guarantee");
    corporateOrgAWBInd              = corporateOrg.getAttribute("allow_airway_bill");
    corporateOrgSGTEEInd            = corporateOrg.getAttribute("allow_shipping_guar");
    corporateOrgFTRInd              = corporateOrg.getAttribute("allow_funds_transfer");
    corporateOrgLRInd               = corporateOrg.getAttribute("allow_loan_request");
    corporateOrgNonPortalOrigInstrInd = corporateOrg.getAttribute("allow_non_prtl_org_instr_ind");  //IAZ IR-GAUJ052680790 102709 Add
    corporateOrgBillingInstrumentsIndicator = corporateOrg.getAttribute("enable_billing_instruments"); //SURREWSH -Rel-9.4 CR-932
    corporateOrgRAInd               = corporateOrg.getAttribute("allow_request_advise");
    corporateOrgSPInd               = corporateOrg.getAttribute("allow_supplier_portal"); //Ravindra - CR-708B
    corporateOrgATPInd              = corporateOrg.getAttribute("allow_approval_to_pay"); //Krishna CR 375-D
    corporateOrgARMInd              = corporateOrg.getAttribute("allow_arm"); //Pratiksha CR-434 09/24/2008
    corporateOrgARMInvoiceDualInd   =  corporateOrg.getAttribute("dual_auth_arm_invoice"); //Leelavathi CR-710 Rel - 8.0.0 25/10/2011
    corporateOrgCreditNoteDualInd   =  corporateOrg.getAttribute("dual_auth_credit_note"); //Leelavathi CR-710 Rel - 8.0.0 23/11/2011
    corporateOrgARMInvoiceInd		  = corporateOrg.getAttribute("allow_arm_invoice");//Leelavathi CR-710 Rel - 8.0.0 25/10/2011
    corporateOrgCreditNoteInd		  = corporateOrg.getAttribute("allow_credit_note");//Leelavathi CR-710 Rel 8.0.0 23/11/2011
    corporateOrgDDInd              = corporateOrg.getAttribute("allow_direct_debit"); //Vasavi CR-509 12/10/2009
    corporateOrgARMFileuploadInd		  = corporateOrg.getAttribute("allow_invoice_file_upload");//Leelavathi CR-710 03/11/2011
    corporateOrgGRInvoicesUploadInd	= corporateOrg.getAttribute("allow_grouped_invoice_upload");//Leelavathi CR-710 23/11/2011
    corporateOrgInterestDiscountInd	= corporateOrg.getAttribute("interest_discount_ind");//Leelavathi CR-710 29/11/2011
    corporateOrgCalculateDiscountInd	= corporateOrg.getAttribute("calculate_discount_ind");//Leelavathi CR-710 29/11/2011
    corporateOrgSPCalculateDiscountInd	= corporateOrg.getAttribute("sp_calculate_discount_ind");//Ravindra - CR-708
    corporateOrgRestrictedInvoicesUploadInd	= corporateOrg.getAttribute("allow_restricted_inv_upload");//Leelavathi CR-710 Rel - 8.0.0 24/11/2011
    corporateOrgRestrictedInvoicesUploadDirectory	= corporateOrg.getAttribute("restricted_inv_upload_dir");//Leelavathi CR-710 Rel - 8.0.0 24/11/2011
    corporateOrgPOInd				  =corporateOrg.getAttribute("allow_purchase_order_upload");	//Leelavathi CR-707 Rel-8.0.0.0 02/02/2012 Add
    corporateOrgPODualInd				=  corporateOrg.getAttribute("po_upload_format");	//Leelavathi CR-707 Rel-8.0.0.0 02/02/2012 Add
    corporateOrgRestrictedPOUploadDirectory	= corporateOrg.getAttribute("restricted_po_upload_dir");//Leelavathi CR-707 Rel - 8.0.0 03/02/2012
    corporateOrgRestrictedPurchaseOrderUploadInd	= corporateOrg.getAttribute("allow_restricted_po_upload");//Leelavathi CR-707 Rel - 8.0.0 03/02/2012
    corporateOrgILCDualInd          = corporateOrg.getAttribute("dual_auth_import_DLC");
    corporateOrgSLCDualInd          = corporateOrg.getAttribute("dual_auth_SLC");
    corporateOrgELCDualInd          = corporateOrg.getAttribute("dual_auth_export_LC");
    corporateOrgECOLDualInd         = corporateOrg.getAttribute("dual_auth_export_coll");
    corporateOrgNECOLDualInd         = corporateOrg.getAttribute("dual_auth_new_export_coll"); //Vasavi CR 524 03/31/2010 Add
    corporateOrgGTEEDualInd         = corporateOrg.getAttribute("dual_auth_guarantee");
    corporateOrgAWBDualInd          = corporateOrg.getAttribute("dual_auth_airway_bill");
    corporateOrgSGTEEDualInd        = corporateOrg.getAttribute("dual_auth_shipping_guar");
    corporateOrgFTRDualInd          = corporateOrg.getAttribute("dual_auth_funds_transfer");
    corporateOrgLRDualInd           = corporateOrg.getAttribute("dual_auth_loan_request");
    corporateOrgRADualInd           = corporateOrg.getAttribute("dual_auth_request_advise");
    corporateOrgSPDualInd           = corporateOrg.getAttribute("dual_auth_supplier_portal");//Ravindra - CR-708B
    corporateOrgATPDualInd          = corporateOrg.getAttribute("dual_auth_approval_to_pay");//Krishna CR 375-D
    corporateOrgARMDualInd           = corporateOrg.getAttribute("dual_auth_arm"); //Pratiksha CR-434 09/24/2008
    corporateOrgDDDualInd           = corporateOrg.getAttribute("dual_auth_direct_debit"); //Vasavi CR-509 12/10/2009
    corporateOrgDRDualInd           = corporateOrg.getAttribute("dual_auth_disc_response");
    corporateOrgSIDualInd           = corporateOrg.getAttribute("dual_settle_instruction"); //SSikhakolli - Rel-9.4 CR-818
    corporateOrgImportColDualInd    = corporateOrg.getAttribute("dual_imp_col"); //SSikhakolli - Rel-9.4 CR-818
    corporateOrgOpBankOrg1Oid       = corporateOrg.getAttribute("first_op_bank_org");
    corporateOrgOpBankOrg2Oid       = corporateOrg.getAttribute("second_op_bank_org");
    corporateOrgOpBankOrg3Oid       = corporateOrg.getAttribute("third_op_bank_org");
    corporateOrgOpBankOrg4Oid       = corporateOrg.getAttribute("fourth_op_bank_org");

    corporateOrgReptCateg1Oid             = corporateOrg.getAttribute("first_rept_categ");
    corporateOrgReptCateg2Oid             = corporateOrg.getAttribute("second_rept_categ");
    corporateOrgReptCateg3Oid             = corporateOrg.getAttribute("third_rept_categ");
    corporateOrgReptCateg4Oid             = corporateOrg.getAttribute("fourth_rept_categ");
    corporateOrgReptCateg5Oid             = corporateOrg.getAttribute("fifth_rept_categ");
    corporateOrgReptCateg6Oid             = corporateOrg.getAttribute("sixth_rept_categ");
    corporateOrgReptCateg7Oid             = corporateOrg.getAttribute("seventh_rept_categ");
    corporateOrgReptCateg8Oid             = corporateOrg.getAttribute("eighth_rept_categ");
    corporateOrgReptCateg9Oid             = corporateOrg.getAttribute("nineth_rept_categ");
    corporateOrgReptCateg10Oid      = corporateOrg.getAttribute("tenth_rept_categ");

    autoLCCreate                              = corporateOrg.getAttribute("allow_auto_lc_create");
    processPaymentUploadInd             = corporateOrg.getAttribute("allow_payment_file_upload"); //MDB IR# PMUL011037699 1/12/11
    straightThroughAuthorizeInd      = corporateOrg.getAttribute("straight_through_authorize"); //CJR CR-593 3/30/11
    partialPayAuthorizeInd      = corporateOrg.getAttribute("allow_partial_pay_auth"); //Rel 9.2 CR 988 11/04/2014
    h2hSystemUserOid                = corporateOrg.getAttribute("h2h_system_user_oid"); // CJR CR-593 3/30/11
    uploadSystemUserOid                = corporateOrg.getAttribute("upload_system_user_oid");//Leelavathi CR-710 Rel-8.0 08/12/2011 added
    allowManualPOEntry              = corporateOrg.getAttribute("allow_manual_po_entry");
    autoATPCreate                 = corporateOrg.getAttribute("allow_auto_atp_create");    //Krishna CR 375-D
    allowATPManualPOEntry           = corporateOrg.getAttribute("allow_atp_manual_po_entry"); //Krishna CR 375-D
    corporateOrgNotifRuleOid        = corporateOrg.getAttribute("notify_rule_oid");
    corporateOrgEmailLanguage           = corporateOrg.getAttribute("email_language");
    corporateOrgTimeoutAutoSaveInd  = corporateOrg.getAttribute("timeout_auto_save_ind");

      corporateOrgTBAInd                    = corporateOrg.getAttribute("allow_xfer_btwn_accts");
      corporateOrgDPInd                     = corporateOrg.getAttribute("allow_domestic_payments");
      corporateOrgDualTBAInd          = corporateOrg.getAttribute("dual_xfer_btwn_accts");
      corporateOrgDualDPInd           = corporateOrg.getAttribute("dual_domestic_payments");
      allowPymtInstrumentAuth         = corporateOrg.getAttribute("allow_pymt_instrument_auth");
      allowPanelAuthForPymts          = corporateOrg.getAttribute("allow_panel_auth_for_pymts");
      enforceDailyLimitTransfer             = corporateOrg.getAttribute("enforce_daily_limit_transfer");
      enforceDailyLimitDomesticPayment = corporateOrg.getAttribute("enforce_daily_limit_dmstc_pymt");
      enforceDailyLimitIntlPymt       = corporateOrg.getAttribute("enforce_daily_limit_intl_pymt");
      corporateOrgTimeZone            = corporateOrg.getAttribute("timezone_for_daily_limit");
      corporateErpReporting			  = corporateOrg.getAttribute("erp_transaction_rpt_reqd"); //AAlubala - Rel8.2 CR741 - ERP Transaction Reporting Required
      corporateDiscountCodeType			  = corporateOrg.getAttribute("corp_discount_code_type"); //AAlubala - Rel8.2 CR741 - Type of discount code

    //Srinivasu_D CR-708 Rel8.1 05/11/2012 Start	 
    atpProcessInvoice = corporateOrg.getAttribute("atp_invoice_indicator");
    failedInvoiceInd = corporateOrg.getAttribute("failed_invoice_indicator");	
    allowPaymentDate = corporateOrg.getAttribute("payment_day_allow");
    daysBefore = corporateOrg.getAttribute("days_before");
    daysAfter = corporateOrg.getAttribute("days_after");  	  
        corporateOrgProcessInvoicesFileUploadInd = corporateOrg.getAttribute("process_invoice_file_upload");
      corporateOrgTradeLoanInd = corporateOrg.getAttribute("trade_loan_type");
      corporateOrgImportLoanInd = corporateOrg.getAttribute("import_loan_type");
      corporateOrgExportLoanInd = corporateOrg.getAttribute("export_loan_type");
      corporateOrgInvoiceFinancingInd = corporateOrg.getAttribute("invoice_financing_type");
      corporateOrgAutoLoanRequestInvUpldInd = corporateOrg.getAttribute("allow_auto_loan_req_inv_upld");
      corporateOrgUsePaymentDateAllowedInd = corporateOrg.getAttribute("use_payment_date_allowed_ind");
      corporateOrgAllowedDaysBeforePaymentDate    = corporateOrg.getAttribute("days_before_payment_date");
      corporateOrgAllowedDaysAfterPaymentDate    = corporateOrg.getAttribute("days_after_payment_date");
      corporateOrgFinancingDaysBeforeLoanMaturity   = corporateOrg.getAttribute("fin_days_before_loan_maturity");
      corporateOrgInvoicePercentToFinanceTradeLoan   = corporateOrg.getAttribute("inv_percent_to_fin_trade_loan"); 
      corporateOrgAutoATPInvUpldInd = corporateOrg.getAttribute("allow_auto_atp_inv_only_upld");
       corporateOrgAWBCheckerAuthIndicator = corporateOrg.getAttribute("air_checker_auth_ind");
	  corporateOrgATPCheckerAuthIndicator = corporateOrg.getAttribute("atp_checker_auth_ind");
	  corporateOrgECOLCheckerAuthInd = corporateOrg.getAttribute("exp_col_checker_auth_ind");
	  corporateOrgExpDLCCheckerAuthInd = corporateOrg.getAttribute("exp_dlc_checker_auth_ind");
	  corporateOrgNewExpCollCheckerAuthInd = corporateOrg.getAttribute("exp_oco_checker_auth_ind");
	  corporateOrgImportDLCCheckerAuthInd = corporateOrg.getAttribute("imp_dlc_checker_auth_ind");
	  corporateOrgLoanRequestCheckerAuthInd = corporateOrg.getAttribute("lrq_checker_auth_ind");
	  /*IR T36000018638 Prateep fix. guar_checker_auth_ind to gua_checker_auth_ind*/
	  corporateOrgGuaranteesCheckerAuthInd = corporateOrg.getAttribute("gua_checker_auth_ind");
	  corporateOrgSLCCheckerAuthInd = corporateOrg.getAttribute("slc_checker_auth_ind");
	  corporateOrgSHPCheckerAuthInd = corporateOrg.getAttribute("shp_checker_auth_ind");
	  corporateOrgRQACheckerAuthInd = corporateOrg.getAttribute("rqa_checker_auth_ind");
	  corporateOrgSPCheckerAuthInd = corporateOrg.getAttribute("sp_checker_auth_ind");
	  corporateOrgDRCheckerAuthInd = corporateOrg.getAttribute("dcr_checker_auth_ind");
	  corporateOrgIncludeSettleInstrInd = corporateOrg.getAttribute("include_settle_instruction"); //SSikhakolli - Rel-9.4 CR-818
	  corporateOrgSICheckerAuthInd = corporateOrg.getAttribute("sit_checker_auth_ind"); //SSikhakolli - Rel-9.4 CR-818
	  corporateOrgImportCollCheckerAuthInd = corporateOrg.getAttribute("imp_col_checker_auth_ind"); //SSikhakolli - Rel-9.4 CR-818
	  corporateOrgFTRQCheckerAuthInd = corporateOrg.getAttribute("ftrq_checker_auth_ind");
	  corporateOrgFTDPCheckerAuthInd = corporateOrg.getAttribute("ftdp_checker_auth_ind");
	  corporateOrgFTBACheckerAuthInd = corporateOrg.getAttribute("ftba_checker_auth_ind");
	  corporateOrgMatchingApprCheckerAuthInd = corporateOrg.getAttribute("mtch_apprv_checker_auth_ind");
	  corporateOrgInvoiceAuthCheckerAuthInd = corporateOrg.getAttribute("inv_auth_checker_auth_ind");
	  corporateOrgCreditAuthCheckerAuthInd = corporateOrg.getAttribute("credit_auth_checker_auth_ind");
    	corporateOrgPaymentBenAuthInd = corporateOrg.getAttribute("pmt_bene_panel_auth_ind");
	
      corpUserDualCtrlReqdInd          = corporateOrg.getAttribute("corp_user_dual_ctrl_reqd_ind");
	  panelAuthorisationDualCtrlReqdInd = corporateOrg.getAttribute("panel_auth_dual_ctrl_reqd_ind");
	  corporateOrgPayProgName = corporateOrg.getAttribute("payables_prog_name");
	
	  corporateOrgPayMgmtFileInd			= corporateOrg.getAttribute("man_upld_pmt_file_frmt_ind");	
      corporateOrgPayMgmtDirInd				= corporateOrg.getAttribute("man_upld_pmt_file_dir_ind");
	  corporateOrgPayMgmtDirLoc				= corporateOrg.getAttribute("restricted_pay_mgmt_upload_dir");
	  System.out.println("man_upld_pmt_file_frmt_ind:"+corporateOrgPayMgmtFileInd+"\t man_upld_pmt_file_dir_ind:"+corporateOrgPayMgmtDirInd+"\t restricted_pay_mgmt_upload_dir:"+corporateOrgPayMgmtDirLoc);
	  
	   corporateOrgPayInvAuthInd = corporateOrg.getAttribute("allow_payables_upl");  //Rel9.0 CR 913
	   corporateOrgPayInvMgmtInd = corporateOrg.getAttribute("allow_payables");  //Rel9.0 CR 913
	   corporateOrgPayCrNoteMgmtInd = corporateOrg.getAttribute("allow_pay_credit_note");  //Rel9.2 CR 914A
	   corporateOrgDualPayInvAuthUploadedInd = corporateOrg.getAttribute("dual_auth_upload_pay_inv");  //Rel9.0 CR 913
	   corporateOrgDualPayInvAuthInd = corporateOrg.getAttribute("dual_auth_payables_inv");  //Rel9.0 CR 913 
	   corporateOrgDualPayCrNoteAuthInd = corporateOrg.getAttribute("dual_auth_pay_credit_note");  //Rel9.2 CR 914A
	   corporateOrgInvFileUploadPayInd = corporateOrg.getAttribute("invoice_file_upload_pay_ind");  //Rel9.0 CR 913 
	   corporateOrgInvFileUploadRecInd = corporateOrg.getAttribute("invoice_file_upload_rec_ind");  //Rel9.0 CR 913 
	   corporateOrgPayInvGroupingInd = corporateOrg.getAttribute("pay_invoice_grouping_ind");  //Rel9.0 CR 913 
	   corporateOrgPayInvUtilisedInd = corporateOrg.getAttribute("pay_invoice_utilised_ind");  //Rel9.0 CR 913
	   corporateOrgRecInvUtilisedInd = corporateOrg.getAttribute("rec_invoice_utilised_ind");  //Rel9.0 CR 913
	   corporateOrgPreDebitOptionInd = corporateOrg.getAttribute("pre_debit_funding_option_ind");
      corporateOrgDecRECInvoiceDualInd   =  corporateOrg.getAttribute("dual_auth_rec_dec_inv");
      corporateOrgDecPAYInvoiceDualInd   =  corporateOrg.getAttribute("dual_auth_pay_dec_inv");
      corporateOrgPayDecCRNDualInd =  corporateOrg.getAttribute("dual_auth_pay_dec_crn");
      corporateOrgDecRECInvoiceInd		  = corporateOrg.getAttribute("allow_rec_dec_inv");
      corporateOrgDecPAYInvoiceInd		  = corporateOrg.getAttribute("allow_pay_dec_inv");
      corporateOrgPayDecCRNInd = corporateOrg.getAttribute("allow_pay_dec_crn");
      corporateOrgRecInvAllowH2hApprovalInd = corporateOrg.getAttribute("allow_h2h_rec_inv_approval");
      corporateOrgPayInvAllowH2hApprovalInd = corporateOrg.getAttribute("allow_h2h_pay_inv_approval");
      corporateOrgPayCrnAllowH2hApprovalInd = corporateOrg.getAttribute("allow_h2h_pay_crn_approval");
      
	   if(StringFunction.isNotBlank(cbMobileBankingInd) &&  TradePortalConstants.INDICATOR_NO.equals(cbMobileBankingInd)){
		   corporateOrg.setAttribute("mobile_banking_access_ind",TradePortalConstants.INDICATOR_NO );	   
	   }
	   corporateOrgMobileBankingInd = corporateOrg.getAttribute("mobile_banking_access_ind"); 
	   
	   String allowUpload =  corporateOrg.getAttribute("allow_pay_credit_note_upload");
	   
	   
	   if (StringFunction.isNotBlank(allowUpload)&& 
	    		 TradePortalConstants.INDICATOR_YES.equals(allowUpload)){
	    	isPayCrNoteUploadAllowed = true;
	    }
	   
	   if (StringFunction.isNotBlank(allowUpload)&& 
	    		 TradePortalConstants.INDICATOR_NO.equals(allowUpload)){
	    	isPayCrNoteUploadNotAllowed = true;
	    }
		   
	   String endToEnd = corporateOrg.getAttribute("allow_end_to_end_id_process");
	   if (StringFunction.isNotBlank(endToEnd)&& 
	    		 TradePortalConstants.INDICATOR_YES.equals(endToEnd)){
	    	isEndToEndIdProcessAllowed = true;
	    }
	    
	   String allowManual =  corporateOrg.getAttribute("allow_manual_cr_note_apply");
	   
	   if (StringFunction.isNotBlank(allowManual)&& 
	    		 TradePortalConstants.INDICATOR_YES.equals(allowManual)){
	    	isManualCreditNoteApplicationAllowed = true;
	   }
	   String applyCreditNote = corporateOrg.getAttribute("apply_pay_credit_note");
	   if (StringFunction.isNotBlank(applyCreditNote)&& 
	    		TradePortalConstants.CREDIT_NOTE_APPLICATION_PROPOTIONATE.equals(applyCreditNote)){
	    	isPropotionateApplication = true;
	    }
	   
	   if (StringFunction.isNotBlank(applyCreditNote)&& 
	    		TradePortalConstants.CREDIT_NOTE_APPLICATION_SEQUENTIAL.equals(applyCreditNote)){
	    	isSequentialApplication = true;
	    }
	    
	   payCreditNoteAvailableDays = corporateOrg.getAttribute("pay_credit_note_available_days");
  	   
  		String seqType = corporateOrg.getAttribute("pay_credit_sequential_type");
 	   
 	   if (StringFunction.isNotBlank(seqType)){
 		   if (TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_INVOICE_ID.equalsIgnoreCase(seqType))
 			  isBasedOnInvoice = true;
 		   else if (TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_DATES.equalsIgnoreCase(seqType))
 			  isPaymentDueDate  = true;
 		   else if (TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_AMOUNT_HIGHTOLOW.equals(seqType))
 			  isHighToLowAmount  = true;
 		   else if (TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_AMOUNT_LOWTOHIGH.equals(seqType))
 			  isLowToHighAmount  = true;
 		   
 	   }
 	  
   }
   else
   {
      corporateOrgName = resMgr.getText("CorpCust.NewCorpCust", TradePortalConstants.TEXT_BUNDLE);
   }

   //IAZ CR-511 11/09/09 Begin - these are represented now by hidden fields (vs boxes) so must be initialized
   //                            if not set in db (as in new customer)
   if (InstrumentServices.isBlank(allowPymtInstrumentAuth))
            allowPymtInstrumentAuth = TradePortalConstants.INDICATOR_NO;
   if (InstrumentServices.isBlank(allowPanelAuthForPymts))
            allowPanelAuthForPymts = TradePortalConstants.INDICATOR_NO;
   //IAZ CR-511 11/09/09 End


  // Populate account[] bean with account info from the database
  StringBuilder sql = new StringBuilder();

  queryListView = null;

  if(retrieveFromDBFlag) {
    queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
    sql.append("select account_oid, account_number, currency, bank_country_code, account_type, account_name, source_system_branch, available_for_internatnl_pay, available_for_xfer_btwn_accts,  available_for_direct_debit, available_for_loan_request, available_for_domestic_pymts, available_for_settle_instr, fx_rate_group, a_op_bank_org_oid, othercorp_customer_indicator, other_account_owner_oid, proponix_customer_id, deactivate_indicator, pending_transaction_balance, pending_transac_credit_balance,");
    sql.append("a_panel_auth_group_oid_1, a_panel_auth_group_oid_2, a_panel_auth_group_oid_3, a_panel_auth_group_oid_4,a_panel_auth_group_oid_5, a_panel_auth_group_oid_6, a_panel_auth_group_oid_7, a_panel_auth_group_oid_8,a_panel_auth_group_oid_9, a_panel_auth_group_oid_10, a_panel_auth_group_oid_11, a_panel_auth_group_oid_12,a_panel_auth_group_oid_13, a_panel_auth_group_oid_14, a_panel_auth_group_oid_15, a_panel_auth_group_oid_16,a_regulatory_account_type");
    sql.append(" from account"); //Query moidfied for IR#T36000046817 by dillip 
    sql.append(" where p_owner_oid = ?");
    List<Object> sqlParamsAcc = new ArrayList();
    sqlParamsAcc.add(corporateOrgOid);
    if (InstrumentServices.isBlank(allowPayByAnotherAccnt)){
      sqlQuery = new StringBuilder();

      sqlQuery.append("select allow_pay_by_another_accnt");
      sqlQuery.append(" from client_bank");
      sqlQuery.append(" where organization_oid = ?");

      //out.println("QUERY: "+sqlQuery.toString());
      queryListView.setSQL(sqlQuery.toString(),new Object[]{clientBankOid});
      queryListView.getRecords();

      DocumentHandler result = queryListView.getXmlResultSet();
      allowPayByAnotherAccnt = result.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
    }

    //cquinton 9/23/2011 Rel 7.1 account for null values

    if (!TradePortalConstants.INDICATOR_YES.equals(allowPayByAnotherAccnt)){
      sql.append(" and (othercorp_customer_indicator != ?");
      sql.append(" or othercorp_customer_indicator is null) ");
      sqlParamsAcc.add(TradePortalConstants.INDICATOR_YES);
    }

    sql.append(" order by ");
    sql.append(resMgr.localizeOrderBy("account_number"));
    Debug.debug(sql.toString());

    queryListView.setSQL(sql.toString(),sqlParamsAcc);
    queryListView.getRecords();

    DocumentHandler acctList = new DocumentHandler();
    acctList = queryListView.getXmlResultSet();
    Debug.debug(acctList.toString());

    accountVector = acctList.getFragments("/ResultSetRecord");

    if (queryListView != null) {
      queryListView.remove();
    }
    
    //Kyriba CR 268 Start
    //Get External Bank details from DB for the given corp id.
    QueryListView extBankQryListView = (QueryListView) EJBObjectFactory
      .createClientEJB(formMgr.getServerLocation(),
        "QueryListView");
    StringBuilder eBankSql = new StringBuilder();
    eBankSql.append("select EXTERNAL_BANK_OID, P_ORGANIZATION_OID, A_OP_ORGANIZATION_OID");
    eBankSql.append(" from external_bank");
    eBankSql.append(" where P_ORGANIZATION_OID = ?");
    eBankSql.append(" order by ");
    eBankSql.append(resMgr.localizeOrderBy("EXTERNAL_BANK_OID"));
    Debug.debug("eBankSql : "+eBankSql.toString());
    extBankQryListView.setSQL(eBankSql.toString(),new Object[]{corporateOrgOid});
    extBankQryListView.getRecords();

    DocumentHandler extBankDocObj = new DocumentHandler();
    extBankDocObj = extBankQryListView.getXmlResultSet();

    Vector eBankVector = extBankDocObj.getFragments("/ResultSetRecord");
    
    numExtBank = eBankVector.size();
    if (numExtBank > DEFAULT_EXTERNAL_BANK_COUNT) {
    	extBankObjCount = numExtBank;
    }
    //iterate thro the extracted records from DB, set the same in web bean.
    for (int iLoop = 0; iLoop < numExtBank; iLoop++) {
      extBankWebBean = beanMgr.createBean(ExternalBankWebBean.class, "ExternalBank");
      DocumentHandler extDoc = (DocumentHandler) eBankVector.elementAt(iLoop);
      String sValue = extDoc.getAttribute("/EXTERNAL_BANK_OID");
      extBankWebBean.setAttribute("external_bank_oid", sValue);

      sValue = extDoc.getAttribute("/P_ORGANIZATION_OID");
      extBankWebBean.setAttribute("corp_org_oid", sValue);

      sValue = extDoc.getAttribute("/A_OP_ORGANIZATION_OID");
      extBankWebBean.setAttribute("op_bank_org_oid", sValue);

      externalBankList.add(extBankWebBean);

    }
    if (numExtBank < DEFAULT_EXTERNAL_BANK_COUNT) {
        for (int iLoop = numExtBank; iLoop < DEFAULT_EXTERNAL_BANK_COUNT; iLoop++) {
        	externalBankList.add(beanMgr.createBean(ExternalBankWebBean.class, "ExternalBank"));
        }
      }
   
    if (extBankQryListView != null) {
    	extBankQryListView.remove();
    }
  //Kyriba CR 268 End

    QueryListView aliasQueryListView = (QueryListView) EJBObjectFactory
      .createClientEJB(formMgr.getServerLocation(),
        "QueryListView");
    StringBuilder aliasSql = new StringBuilder();
    aliasSql.append("select customer_alias_oid, alias, a_gen_message_category_oid");
    aliasSql.append(" from customer_alias");
    aliasSql.append(" where p_owner_oid = ?");
    aliasSql.append(" order by ");
    aliasSql.append(resMgr.localizeOrderBy("customer_alias_oid"));
    Debug.debug(aliasSql.toString());
    aliasQueryListView.setSQL(aliasSql.toString(),new Object[]{corporateOrgOid});
    aliasQueryListView.getRecords();

    DocumentHandler custAliasList = new DocumentHandler();
    custAliasList = aliasQueryListView.getXmlResultSet();
    Debug.debug(custAliasList.toString());

    Vector aliasVector = custAliasList.getFragments("/ResultSetRecord");
    numAlias = aliasVector.size();

    if (numAlias > DEFAULT_ALIAS_COUNT) {
      aliasRowCount = numAlias;
    } 

    for (int iLoop = 0; iLoop < numAlias; iLoop++) {
      CustomerAliasWebBean customerAlias = beanMgr.createBean(CustomerAliasWebBean.class, "CustomerAlias");
      DocumentHandler aliasDoc = (DocumentHandler) aliasVector
        .elementAt(iLoop);
      String sValue = aliasDoc.getAttribute("/ALIAS");
      customerAlias.setAttribute("alias", sValue);

      sValue = aliasDoc.getAttribute("/CUSTOMER_ALIAS_OID");
      customerAlias.setAttribute("customer_alias_oid", sValue);

      sValue = aliasDoc.getAttribute("/A_GEN_MESSAGE_CATEGORY_OID");
      customerAlias.setAttribute("gen_message_category_oid", sValue);

      aliasList.add(customerAlias);

    }
    if (numAlias < DEFAULT_ALIAS_COUNT) {
      for (int iLoop = numAlias; iLoop < DEFAULT_ALIAS_COUNT; iLoop++) {
        aliasList.add(beanMgr.createBean(CustomerAliasWebBean.class, "CustomerAlias"));
      }
    }

    if (aliasQueryListView != null) {
      aliasQueryListView.remove();
    }


    StringBuilder sql2 = new StringBuilder();
    QueryListView marginQueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
    sql2.append("select customer_margin_rule_oid, currency, instrument_type, threshold_amt, rate_type, margin");
    sql2.append(" from customer_margin_rule");
    sql2.append(" where p_owner_oid = ?");
    sql2.append(" order by ");
    sql2.append(resMgr.localizeOrderBy("customer_margin_rule_oid"));
    Debug.debug(sql2.toString());

    marginQueryListView.setSQL(sql2.toString(),new Object[]{corporateOrgOid});
    marginQueryListView.getRecords();

    DocumentHandler marginList = new DocumentHandler();
    marginList = marginQueryListView.getXmlResultSet();
    Debug.debug(marginList.toString());

    Vector mVector = marginList.getFragments("/ResultSetRecord");
    int numMarginItems = mVector.size();
  
    if (numMarginItems > DEFAULT_MARGINRULE_COUNT) {
      row2Count = numMarginItems;
    }

    for (int mLoop = 0; mLoop < numMarginItems; mLoop++) {
      CustomerMarginRuleWebBean marginRule = beanMgr.createBean(CustomerMarginRuleWebBean.class, "CustomerMarginRule");
      DocumentHandler marginDoc = (DocumentHandler) mVector
          .elementAt(mLoop);
      String mValue = marginDoc
          .getAttribute("/CUSTOMER_MARGIN_RULE_OID");
      marginRule.setAttribute("customer_margin_rule_oid",mValue);

      mValue = marginDoc.getAttribute("/CURRENCY");
      marginRule.setAttribute("currency", mValue);

      mValue = marginDoc.getAttribute("/INSTRUMENT_TYPE");
      marginRule.setAttribute("instrument_type", mValue);

      mValue = marginDoc.getAttribute("/THRESHOLD_AMT");
      marginRule.setAttribute("threshold_amt", mValue);
      
      mValue = marginDoc.getAttribute("/RATE_TYPE");
      marginRule.setAttribute("rate_type", mValue);
      
      mValue = marginDoc.getAttribute("/MARGIN");
      marginRule.setAttribute("margin", mValue);

      marginRuleList.add(marginRule);
    }
    // Fill Empty Objects to make it Count 8.
    if(numMarginItems < DEFAULT_MARGINRULE_COUNT) {
      for (int mLoop = numMarginItems; mLoop < DEFAULT_MARGINRULE_COUNT; mLoop++) {
        marginRuleList.add(beanMgr.createBean(CustomerMarginRuleWebBean.class, "CustomerMarginRule"));
      }
    }
    if (marginQueryListView != null) {
      marginQueryListView.remove();
    }
    //Ravindra - CR-708B - Start
    sql2 = new StringBuilder();
    QueryListView spMarginQueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
    sql2.append("select sp_margin_rule_oid, currency, threshold_amt, rate_type, margin");
    sql2.append(" from sp_margin_rule");
    sql2.append(" where p_owner_oid = ?");
    sql2.append(" order by ");
    sql2.append(resMgr.localizeOrderBy("sp_margin_rule_oid"));
    Debug.debug(sql2.toString());

    spMarginQueryListView.setSQL(sql2.toString(),new Object[]{corporateOrgOid});
    spMarginQueryListView.getRecords();

    DocumentHandler spMarginList = new DocumentHandler();
    spMarginList = spMarginQueryListView.getXmlResultSet();
    Debug.debug(spMarginList.toString());

    mVector = spMarginList.getFragments("/ResultSetRecord");
    int numSpMarginItems = mVector.size();
  
    if (numSpMarginItems > DEFAULT_SP_MARGINRULE_COUNT) {
    	spMarginRulesCount = numSpMarginItems;
    }
    
    for (int mLoop = 0; mLoop < numSpMarginItems; mLoop++) {
    	SPMarginRuleWebBean spMarginRule = beanMgr.createBean(SPMarginRuleWebBean.class, "SPMarginRule");
        DocumentHandler spMarginDoc = (DocumentHandler) mVector.elementAt(mLoop);
        String mValue = spMarginDoc.getAttribute("/SP_MARGIN_RULE_OID");
        spMarginRule.setAttribute("sp_margin_rule_oid",mValue);

        mValue = spMarginDoc.getAttribute("/CURRENCY");
        spMarginRule.setAttribute("currency", mValue);

        mValue = spMarginDoc.getAttribute("/THRESHOLD_AMT");
        spMarginRule.setAttribute("threshold_amt", mValue);
        
        mValue = spMarginDoc.getAttribute("/RATE_TYPE");
        spMarginRule.setAttribute("rate_type", mValue);
        
        mValue = spMarginDoc.getAttribute("/MARGIN");
        spMarginRule.setAttribute("margin", mValue);

        spMarginRuleList.add(spMarginRule);
      }
 // Fill Empty Objects to make it Count 4.
    if(numSpMarginItems < DEFAULT_SP_MARGINRULE_COUNT) {
      for (int mLoop = numSpMarginItems; mLoop < DEFAULT_SP_MARGINRULE_COUNT; mLoop++) {
        spMarginRuleList.add(beanMgr.createBean(SPMarginRuleWebBean.class, "SPMarginRule"));
      }
    }
    if (spMarginQueryListView != null) {
      spMarginQueryListView.remove();
    }
  //Ravindra - CR-708B - End
  } 


  try {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");

      if (!insertModeFlag)
      {
         // This is the query used for populating the Default User drop down list; it
         // retrieves all active users for the corporate organization that was selected
         // in the corporate organizations listview
         sqlQuery = new StringBuilder();

         sqlQuery.append("select user_oid, user_identifier");
         sqlQuery.append(" from users");
         sqlQuery.append(" where p_owner_org_oid = ?");
         sqlQuery.append(" and activation_status = ?");
         sqlQuery.append(" order by ");
         sqlQuery.append(resMgr.localizeOrderBy("user_identifier"));

         queryListView.setSQL(sqlQuery.toString(),new Object[]{corporateOrgOid,TradePortalConstants.ACTIVE});
         queryListView.getRecords();

         corporateOrgUsersDoc = queryListView.getXmlResultSet();

         if (queryListView.getRecordCount() > 0)
         {
            hasCorporateOrgUsers = true;
         }
      }

      // This is the query used for populating the Parent Corporation drop down list. For
      // client bank users, it retrieves all active corporate organizations that belong to
      // the user's client bank, excluding the one being edited. For BOG users, it retrieves
      // all active corporate organizations that belong to the user's bank organization
      // group, excluding the one being edited.
      sqlQuery = new StringBuilder();
      List<Object> sqlParamsDropDown = new ArrayList();
      sqlQuery.append("select organization_oid, name");
      sqlQuery.append(" from corporate_org");
      sqlQuery.append(" where organization_oid not in (");
      sqlQuery.append("select organization_oid");
      sqlQuery.append(" from corporate_org");
      sqlQuery.append(" start with organization_oid = ?");
      sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
      sqlQuery.append(" and activation_status = ?");
      sqlParamsDropDown.add(corporateOrgOid);
      sqlParamsDropDown.add(TradePortalConstants.ACTIVE);
    

      if (isClientBankUser)
      {
         sqlQuery.append(" and a_client_bank_oid = ?");
         sqlParamsDropDown.add(userOrgOid);
      }
      else
      {
         sqlQuery.append(" and a_bank_org_group_oid =? ");
         sqlParamsDropDown.add(userOrgOid);
      }

      sqlQuery.append(" order by ");
      sqlQuery.append(resMgr.localizeOrderBy("name"));

      queryListView.setSQL(sqlQuery.toString(),sqlParamsDropDown);
      queryListView.getRecords();

      if (queryListView.getRecordCount() > 0)
      {
         hasCorporateOrgs = true;
      }

      corporateOrgsDoc = queryListView.getXmlResultSet();

      if (isClientBankUser)
      {
         // This is the query used for populating the Bank Group drop down list; it retrieves
         // all active bank organization groups that belong to the user's client bank.
         sqlQuery = new StringBuilder();
         /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 Start*/
         List bnkgrps= new ArrayList();
             if(StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid())){
             	String query="select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ";
	    		DocumentHandler bankGrpList = new DocumentHandler();
	    		List params=new ArrayList();
	    		params.add(userSession.getBankGrpRestrictRuleOid());
	            bankGrpList = DatabaseQueryBean.getXmlResultSet(query,false,params);
	            Vector bgVector = bankGrpList.getFragments("/ResultSetRecord");
	            for (int iLoop=0; iLoop<bgVector.size(); iLoop++)
			    {
			 	     DocumentHandler bankGrpforrestricDoc = (DocumentHandler) bgVector.elementAt(iLoop);
			 	     bnkgrps.add(bankGrpforrestricDoc.getAttribute("/BANK_ORGANIZATION_GROUP_OID"));
				}
             	
             }
         /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 End*/
		List<Object> sqlParamsBankGrop = new ArrayList();
         sqlQuery.append("select organization_oid, name, enable_admin_update_centre");
         sqlQuery.append(" from bank_organization_group");
         sqlQuery.append(" where p_client_bank_oid = ?");
         sqlQuery.append(" and activation_status = ?");
         sqlParamsBankGrop.add(userOrgOid);
         sqlParamsBankGrop.add(TradePortalConstants.ACTIVE);
         /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 Start*/
         if(StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid())){
        	 sqlQuery.append(" and organization_oid not in "+ StringFunction.toSQLString(bnkgrps));
		 }
		 /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 End*/
         sqlQuery.append(" order by ");
         sqlQuery.append(resMgr.localizeOrderBy("name"));

         queryListView.setSQL(sqlQuery.toString(),sqlParamsBankGrop);
         queryListView.getRecords();

         bankGroupsDoc = queryListView.getXmlResultSet();
         
         //SSikhakolli - Rel-9.3.5 CR-1029 IR#T36000042510 - Code changes per Design Change - Begin
         try{
        	 String  oid;
             String  text;
            Vector v = bankGroupsDoc.getFragments ("/ResultSetRecord");
            int numItems = v.size ();

            for (int i = 0; i < numItems; i++){
               DocumentHandler oidDoc = (DocumentHandler)v.elementAt (i);
               // Extract the value and text attributes from the document.
               try{
                  oid = oidDoc.getAttribute ("/ORGANIZATION_OID");
               } catch (Exception e) {
                  oid = "0";
               }
               
               try {
                  text = oidDoc.getAttribute ("/ENABLE_ADMIN_UPDATE_CENTRE");
               } catch (Exception e) {
                  text = "";
               }
               
                  oid = EncryptDecrypt.encryptStringUsingTripleDes(oid, userSession.getSecretKey());
                  oidDocFinal+=oid;
                  textDocFinal+=text;
                  if(i < (numItems-1)){
                	  oidDocFinal+=",";
                	  textDocFinal+=",";
                  }
               }
          }catch (Exception e){
          	System.out.println ("Error in Corp Cust JSP " + e.toString ());
          }
       	//SSikhakolli - Rel-9.3.5 CR-1029 IR#T36000042510 - Code changes per Design Change - End
      }


      // This is the query used for populating the Operational Bank Org drop down lists; it
      // retrieves all active operational bank organizations that belong to the user's
      // client bank, regardless of whether or not he or she is a client bank user or a BOG
      // user.
      sqlQuery = new StringBuilder();
      List<Object> sqlParamsOpBank = new ArrayList();
      sqlQuery.append("select organization_oid, name");
      sqlQuery.append(" from operational_bank_org");
      sqlQuery.append(" where p_owner_bank_oid = ?");
      sqlParamsOpBank.add(clientBankOid);
      //T36000021026
      extBankSql.append(sqlQuery);
      
      sqlQuery.append(" and (external_bank_ind is null or external_bank_ind =?)");
      sqlQuery.append(" and activation_status = ?");
      sqlParamsOpBank.add(TradePortalConstants.INDICATOR_NO);
      sqlParamsOpBank.add(TradePortalConstants.ACTIVE);
       
      sqlQuery.append(" order by ");
      sqlQuery.append(resMgr.localizeOrderBy("name"));
 
      queryListView.setSQL(sqlQuery.toString(),sqlParamsOpBank);
      queryListView.getRecords();
      System.out.println ("sqlQuery.toString()-- >" + sqlQuery.toString());
 
      operationalBankOrgsDoc = queryListView.getXmlResultSet();
        
      //Kyriba CR 268 start
      /*External Bank details are stored in Operational Bank Org Table. 
      	A New indicator external_bank_ind = 'Y' has been added to differentiate 
      	external bank records from operational bank org recods*/
         
      	//T36000021026
      	extBankSql.append(" and external_bank_ind =?");
      	extBankSql.append(" and activation_status = ?");
        
      	extBankSql.append(" order by ");
      	extBankSql.append(resMgr.localizeOrderBy("name"));
      	List<Object> sqlParamsExtBank = new ArrayList();
      	sqlParamsExtBank.add(clientBankOid);
      	sqlParamsExtBank.add(TradePortalConstants.INDICATOR_YES);
      	sqlParamsExtBank.add(TradePortalConstants.ACTIVE);
      queryListView.setSQL(extBankSql.toString(),sqlParamsExtBank);
      queryListView.getRecords();
      System.out.println ("extBankSql.toString()-- >" + extBankSql.toString());
      externalBankOrgsDoc = queryListView.getXmlResultSet();
      //Kyriba CR 268 end

      // This is the query used to determine whether the client bank allows passwords.
      // If not (i.e., forced to use certificates or sso), the security section does not
      // display
      sqlQuery = new StringBuilder();

      sqlQuery.append("select corp_auth_method,allow_pay_by_another_accnt");
      sqlQuery.append(" from client_bank");
      sqlQuery.append(" where organization_oid = ?");

      //out.println("QUERY: "+sqlQuery.toString());
      queryListView.setSQL(sqlQuery.toString(),new Object[]{clientBankOid});
      queryListView.getRecords();

      DocumentHandler result = queryListView.getXmlResultSet();
      defaultAuthMethod = result.getAttribute("/ResultSetRecord(0)/CORP_AUTH_METHOD");
      allowPayByAnotherAccnt = result.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");

      // This is the query used for populating the Notification Rule drop down list.
      // It retrieves all notification rules that belong to this user's client bank or
      // bank group.
      sqlQuery = new StringBuilder();
      sqlQuery.append("select notification_rule_oid, name");
      sqlQuery.append(" from notification_rule");
     //commenting the below query as per client requirement IR no :-T36000013696 Start rakesh pasupulati
	 //IR 20602 start- Uncomment and use correct where clause
      sqlQuery.append(" where p_owner_org_oid in (?,?");
      List<Object> sqlParamsNotiRule = new ArrayList();
      sqlParamsNotiRule.add(clientBankOid);
      sqlParamsNotiRule.add(userSession.getGlobalOrgOid());
      if (userType.equals(TradePortalConstants.OWNER_BOG))
      {
       sqlQuery.append(",?");
       sqlParamsNotiRule.add(corporateOrgBankOrgGroupOid);
      }
      sqlQuery.append(")");

      // Include the notification rule currently assigned to the corp cust if one exists
      // Without including this sql the assigned notification rule would not appear if a
      // client bank user was viewing the corp cust page and the notification rule was
      // create at the bank group level
     if (!InstrumentServices.isBlank(corporateOrgNotifRuleOid))
      {
        sqlQuery.append(" or notification_rule_oid =? ");
        sqlParamsNotiRule.add(corporateOrgNotifRuleOid);
      }
    //IR 20602 end
	//commenting the query as per client requirement IR no :-T36000013696 END rakesh pasupulati
      sqlQuery.append(" order by ");
      sqlQuery.append(resMgr.localizeOrderBy("name"));
      queryListView.setSQL(sqlQuery.toString(),sqlParamsNotiRule);
      queryListView.getRecords();

      if (queryListView.getRecordCount() > 0)
      {
         hasNotificationRules = true;
      }
      notificationRulesDoc = queryListView.getXmlResultSet();


      // Determine whether the corporate org customer
      // has multiple addresses.  If it does we display "Manage Addresses" button,
      // otherwise we display "Add Address" button.
      //jgadela  R90 IR T36000026319 - SQL Injection FIX
      if (DatabaseQueryBean.getCount("address_oid", "address", "p_corp_org_oid =  ? ", false, new Object[]{corporateOrgOid}) > 0 )
      {
         hasMultipleAddresses = true;
      }

   }

   catch (Exception e)
   {
      e.printStackTrace();
   }
   finally
   {
      try
      {
         if (queryListView != null)
         {
            queryListView.remove();
         }
      }
      catch (Exception e)
      {
         System.out.println("Error removing querylistview in CorporateCustomerDetail.jsp!");
      }
   }

   //IAZ CR-511 11/09/09 : get list of panel authorization groups IDs and OIDs and store IDs in a hidden <select>
   //                           so it can be used with add4account javascript.  The list will aslo used with each of
   //                           existing accounts later in the code
  String panelAuthGroupSql = "select panel_auth_group_oid, pag.panel_auth_group_id as PAGID, AIR_IND, ATP_IND, EXP_COL_IND, EXP_DLC_IND,";
   panelAuthGroupSql+="EXP_OCO_IND, IMP_DLC_IND, LRQ_IND, GUA_IND, SLC_IND, SHP_IND, RQA_IND, SP_IND, DCR_IND, MATCHIN_APPROVAL_IND, IMPORT_COL_IND, ";
   panelAuthGroupSql+="INV_AUTH_IND, CREDIT_AUTH_IND, PAY_MGM_INV_AUTH_IND, PAY_UPL_INV_AUTH_IND, PAY_CREDIT_NOTE_AUTH_IND, SETTLEMENT_INSTR_IND ";
   panelAuthGroupSql+="from panel_auth_group pag where p_corp_org_oid IN ( ";
   panelAuthGroupSql+="SELECT CONNECT_BY_ROOT organization_oid AS parent_oid FROM corporate_org";
   panelAuthGroupSql+=" WHERE organization_oid = ? CONNECT BY prior organization_oid = p_parent_corp_org_oid";
   panelAuthGroupSql+=" AND INHERIT_PANEL_AUTH_IND = ?)";
   DocumentHandler panelAuthgroupList = DatabaseQueryBean.getXmlResultSet(panelAuthGroupSql.toString(), false, corporateOrgOid, "Y");
   
   //MEer Rel 8.3 IR-22490 For PaymentsList-- Retrieve only the panel groups defined for Payment
   String panelAuthGroupPaymentsSql = "select panel_auth_group_oid, pag.panel_auth_group_id as PAGID ";
   panelAuthGroupPaymentsSql+="from panel_auth_group pag where payment_type is not null and instrument_group_type='PYMTS' and p_corp_org_oid IN (";
   panelAuthGroupPaymentsSql+="SELECT CONNECT_BY_ROOT organization_oid AS parent_oid FROM corporate_org";
   panelAuthGroupPaymentsSql+=" WHERE organization_oid = ? CONNECT BY prior organization_oid = p_parent_corp_org_oid";
   panelAuthGroupPaymentsSql+=" AND INHERIT_PANEL_AUTH_IND = ?)";
   DocumentHandler panelAuthgroupPaymentsList = DatabaseQueryBean.getXmlResultSet(panelAuthGroupPaymentsSql.toString(), false, corporateOrgOid, "Y");
    String curPAGId ="";
	String pagOptions = "<option  value=\"\"></option>";
	pagOptions += ListBox.createOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID",curPAGId);
	
	String pagOptionsPayments = "<option  value=\"\"></option>";
	pagOptionsPayments += ListBox.createOptionList(panelAuthgroupPaymentsList, "PANEL_AUTH_GROUP_OID", "PAGID",curPAGId);
   
	String pagOptionsNew = "<option  value=\"\"></option>";
	pagOptionsNew += createOptionList(panelAuthgroupPaymentsList, "PANEL_AUTH_GROUP_OID", "PAGID",curPAGId);
//CR 821 - Rel 8.3 END	
      String msgCatSql = "select gen_message_category_oid, gmc.description as DESCR from generic_msg_category gmc order by description";

      DocumentHandler msgCategoryList = DatabaseQueryBean.getXmlResultSet(msgCatSql.toString(), false ,new ArrayList<Object>());

        if(TradePortalConstants.INDICATOR_YES.equals(allowPayByAnotherAccnt))
        {
            otherAccountsHidden = "";
        }

    
%>

<%-- ********************* HTML for page begins here *********************  --%>

<%
  String bButtonPressed = xmlDoc.getAttribute("/In/Update/ButtonPressed");
  if(!isReadOnly) {
    if(TradePortalConstants.ADD_MODIFY_PANEL_LEVEL_BUTTON.equals(bButtonPressed)) {
      onLoad = "document.forms[0].AllowPanelAuthForPaymts[0].focus();";
    } else {
      onLoad = "document.CorporateCustomerDetailForm.Name.focus();";
    }
 }
 else
   onLoad = "";
%>


<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad%>" />
</jsp:include>

<div id="nonvis1" style="visibility: hidden; position: absolute;">
<%
      String pagOptionsMsg = ListBox.createOptionList(msgCategoryList,
			"DESCR", "DESCR", "");
      out.println(InputField.createSelectField("genMessageCategory", "", "----",
                  pagOptionsMsg, "ListText", isReadOnly, ""));
%>
</div>

<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="AdminCorpCust.CorpCust"/>
      <jsp:param name="helpUrl" value="admin/corp_customer_detail.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (insertModeFlag) {
    subHeaderTitle = resMgr.getText("CorpCust.NewCorpCust",TradePortalConstants.TEXT_BUNDLE);
  }
  else {
  subHeaderTitle = corporateOrg.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>

    <form name="CorporateCustomerDetailForm" id="CorporateCustomerDetailForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
      <input type=hidden value="" name="buttonName">
      <%--jgadela rel8.3 CR501 [START] --%>
		<input type=hidden name="PendingReferenceDataOid" value="<%=pendingReferenceDataOid %>">
		<input type=hidden name="pendingChangedObjectOid" value="<%=pendingChangedObjectOid %>">
		<%
		if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(adminUserDualCtrlReqdInd)){
		%>
		  <input type=hidden name="isDualControlRequired" value="Y">
		<%
		}else{
		%>
		  <input type=hidden name="isDualControlRequired" value="N">
		<%}%>
		<%--jgadela rel8.3 CR501 [END] --%>


      <div class="formArea">
        <jsp:include page="/common/ErrorSection.jsp" />

        <div class="formContent">
<%
  // Store the Corporate Org oid (if there is one) and other necessary info in a secure hashtable for the form
  secureParms.put("CorporateOrganizationOid", corporateOrgOid);
  secureParms.put("UserOid",                  userSession.getUserOid());
  secureParms.put("login_oid",                userSession.getUserOid());
  secureParms.put("login_rights",             userSession.getSecurityRights());
  secureParms.put("ownership_level", userType);
 //IR 23327 Start
  if (userType.equals(TradePortalConstants.OWNER_BOG))
  {
    secureParms.put("BankOrgGroupOid", EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()) );
  }
 //IR 23327 End
  if (insertModeFlag) {
    secureParms.put("ClientBankOid", clientBankOid);
  
  }
  else {
	  secureParms.put("ClientBankOid", clientBankOid); //Rel 935
    secureParms.put("OptimisticLockId", corporateOrg.getAttribute("opt_lock"));
  }
  if (defaultAuthMethod != null
      && !defaultAuthMethod.equals("")
      && !defaultAuthMethod.equals(TradePortalConstants.AUTH_PERUSER)) {
    secureParms.put("AuthenticationMethod",defaultAuthMethod);
  }
%>

          <%= formMgr.getFormInstanceAsInputField("CorporateCustomerDetailForm", secureParms) %>

         <%= widgetFactory.createSectionHeader("1", "CorpCust.General") %>
           <%@ include file="fragments/CorporateCustomerSection1.frag" %>
         </div>

         <%= widgetFactory.createSectionHeader("2", "CorpCust.Settings") %>
           <%@ include file="fragments/CorporateCustomerSection2.frag" %>   
         </div>


<%
       if (defaultAuthMethod == null  || defaultAuthMethod.equals("")  || defaultAuthMethod.equals(TradePortalConstants.AUTH_PERUSER)) {
%>
         <%= widgetFactory.createSectionHeader("3", "CorpCust.Security") %>
           <%@ include file="fragments/CorporateCustomerSection4.frag" %>   
         </div>
<%
   }
%>

         <%= widgetFactory.createSectionHeader("4", "CorpCust.BankUse") %>
             <%@ include file="fragments/CorporateCustomerSection5.frag" %>   
         </div>

        <%= widgetFactory.createSectionHeader("5" ,"CorpCust.InstrumentCapabilities") %>
            <%@ include file="fragments/CorporateCustomerSection6.frag" %>   
        </div>

		<%= widgetFactory.createSectionHeader("6", "CorpCust.OperationalBankOrgs") %>
              <%@ include file="fragments/CorporateCustomerSection7.frag" %>   
        </div>

		<%= widgetFactory.createSectionHeader("7","CorpCust.FundTransferDailyLimits") %>
              <%@ include file="fragments/CorporateCustomerSection8.frag" %>   
		</div>

<%
       if ("Y".equals(reportCategInd)) {
%>
		<%= widgetFactory.createSectionHeader("8","CorpCust.ReportCategs") %>
              <%@ include file="fragments/CorporateCustomerSection9.frag" %>   		
        </div>	   
<% 
   } 
%> 

		<%= widgetFactory.createSectionHeader("9","CorpCust.CustomerAccounts") %>
              <%@ include file="fragments/CorporateCustomerSection10.frag" %>   		
		</div>

		<%=widgetFactory.createSectionHeader("10","CorpCust.CustomerAlias") %>
              <%@ include file="fragments/CorporateCustomerSection11.frag" %>   		
		</div>

		<%=widgetFactory.createSectionHeader("11","CorpCust.CustomerMarginRules") %>
              <%@ include file="fragments/CorporateCustomerSection12.frag" %>   		
		</div>
		<%=widgetFactory.createSectionHeader("12","CorpCust.SupplierPortalDetails") %>
              <%@ include file="fragments/CorporateCustomerSection13.frag" %>   		
		</div>
		<%=widgetFactory.createSectionHeader("13","CorpCust.ExternalBank") %>
              <%@ include file="fragments/CorporateCustomerSection14.frag" %>   		
		</div>
  </div><%--formContent--%>
  </div><%--formArea--%>
  
<%//jgadela rel8.3 CR501 [START] %>
<%
String cancelAction = "goToOrganizationsHome";
 if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction")) || "ApproveButton".equalsIgnoreCase(buttonPressed) || "RejectButton".equalsIgnoreCase(buttonPressed)){
	 cancelAction = "goToRefDataApproval";
 }
%>
<%//jgadela rel8.3 CR501 [END] %>

    <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'CorporateCustomerDetailForm'">
      <jsp:include page="/common/RefDataSidebar.jsp">
        <jsp:param name="showSaveButton" value="<%= showSaveButtonFlag %>" />
        <jsp:param name="saveOnClick" value="none" />
        <jsp:param name="showDeleteButton" value="<%= showDeleteButtonFlag %>" />
        <jsp:param name="showSaveCloseButton" value="<%= showSaveCloseButtonFlag %>" />
        <jsp:param name="saveCloseOnClick" value="none" />
        <jsp:param name="cancelAction" value="<%= cancelAction %>" />
        <jsp:param name="showHelpButton" value="false" />
        <jsp:param name="showLinks" value="true" />
        <jsp:param name="helpLink" value="customer/thresh_groups_detail.htm" />
        <jsp:param name="buttonPressed" value="<%=savebuttonPressed%>" />
        <jsp:param name="error" value="<%=error%>" />
         <jsp:param name="showApproveButton" value="<%=showApprove%>" />
         <jsp:param name="showRejectButton" value="<%=showReject%>" />
         </jsp:include>
    </div>
  </form>

  </div>
  </div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

<div id="addSecondaryAddressDialog" ></div>
<div id="updateSecondaryAddressDialog" ></div>
<div id="addSubsidiaryAccDialog"></div>

<%
//Getting the formatted data for accounts to pass to the data grid
StringBuffer accountGridData = new StringBuffer();
if(accountVector!=null && accountVector.size()>0){
	accountGridData = getAccountGridData(accountVector, beanMgr, userSession); 
}
%>


<%
  String addrGridLayout = dgFactory.createGridLayout("CorporateAddressDataGrid");
  String acctGridLayout = dgFactory.createGridLayout("CorporateCustAccountSetupDataGrid");
%>

<%-- ********************* JavaScript for page begins here *********************  --%>

<script type="text/javascript">

	var oidDocFinal = '<%=oidDocFinal%>';
	var textDocFinal = '<%=textDocFinal%>';
	
  <%-- global vars --%>
  var addressCount = <%=addressCount%>;
  var accountCount = <%=accountCount%>;
  var addrGrid;
  var acctGrid;
  var pagOptionsNew;
  <%-- the selected address rowkey and divId --%>
  var selectedAddrRowKey = "";
  var selectedAddrDivId = "";
  <%-- the selected account rowkey and divId --%>
  var selectedAcctRowKey;
  var selectedAcctDivId;

  var local = {};

  var fxRateGroupOrg = '<%=corporateOrg.getAttribute("fx_rate_group")%>';

  <%-- Variables to store current state when switching between upload allowed or not indicators. --%>
  var InvoiceManagementIndicator1			= false;
  var InvoiceManagementIndicator2			= false;
  var GRInvoiceIndicator1					= false;
  var GRInvoiceIndicator2					= false;
  var RestrictedInvoiceUploadIndicator1	= false;
  var RestrictedInvoiceUploadIndicator2	= false;
  var SpecifyRestrictedInvoiceUploadDir	= '';
  var InterestFinanceAmountIndicator1		= false;
  var InterestFinanceAmountIndicator2		= false;
  var CalculateDiscountIndicator1			= false;
  var CalculateDiscountIndicator2			= false;
   var PayablesInvoicesIndicator             = false;
  var ReceivablesInvoicesIndicator          = false;
  var PayablesInvoicesGrouped               = false;
  var PayablesInvoicesIntegrated            = false;
  var ReceivablesProgramUtilised            = false;
  <%--  Variables to store current invoiceUploadDir. --%>
  var invoiceUploadDir	= '';
<%-- code to find out whrether the page is oened in approver mode. --%>
  var isApproverPage					= <%=((showApprove==true)?"true":"false")%>;


  require(["t360/datagrid", "dijit/form/CheckBox", "dojo/domReady!"],
      function( t360grid, CheckBox ) {

    <%-- first the grid formatters --%>
<%
  if (!isReadOnly) {
%>
    function deleteAddressFormatter(){
      return "<a href='javascript:local.deleteAddressRow();'><%=resMgr.getText("common.delete",TradePortalConstants.TEXT_BUNDLE)%></a>";
    }
    function deleteAccountFormatter(myFieldValue){
      var myRowKey = myFieldValue[0];
      var myDivId = myFieldValue[1];
      return "<a href='javascript:local.deleteAccountRow(\""+myRowKey+"\",\""+myDivId+"\");'><%=resMgr.getText("common.delete",TradePortalConstants.TEXT_BUNDLE)%></a>";
    }
<%
  }
%>
    function disabledCheckBoxFormatter(myFieldValue){
      var flag = false;
      if(myFieldValue=="Y") {
        flag = true;
      }
      var w = new CheckBox({ checked: flag, disabled: true });
      w._destroyOnRemove=true;
      return w;
    }

    <%-- create the address grid --%>
    var addrGridLayout=[
<%
  if (!isReadOnly) {
%>
      {type: "dojox.grid._RadioSelector"},[
<%
  }
%>
        {name:"rowKey", field:"rowKey", hidden:"true"}
        ,{name:"divId", field:"divId", hidden:"true"}
        ,{name:'<%=resMgr.getText("AddressListView.SeqNo",TradePortalConstants.TEXT_BUNDLE)%>', field:"SeqNo", width:"60px"}
        ,{name:'<%=resMgr.getText("AddressListView.Name",TradePortalConstants.TEXT_BUNDLE)%>', field:"Name", width:"180px"}
        ,{name:'<%=resMgr.getText("AddressListView.Address",TradePortalConstants.TEXT_BUNDLE)%>', 
         fields:["AddressLine1","AddressLine2","City","StateProvince","Country","PostalCode"],
         formatter:t360gridFormatters.addressFormatter, width:"300px"}
<%
  if (!isReadOnly) {
%>
        ,{name:'<%=resMgr.getText("AddressListView.Action",TradePortalConstants.TEXT_BUNDLE)%>', field:"", formatter:deleteAddressFormatter, width:"60px"}
      ]
<%
  }
%>
    ];
    var addrGridData = [ <%= addressGridData.toString() %> ];

    <%--create a local memory grid--%>
    t360grid.createMemoryDataGrid("CorporateAddressDataGridId", addrGridData, addrGridLayout);


    <%-- create the account grid --%>
    var acctGridLayout = <%= acctGridLayout %>;
    acctGridLayout[1][7] = 
      new Object ({name: '<%=resMgr.getText("CorporateCustomerAccount.SubsidiaryAccount",TradePortalConstants.TEXT_BUNDLE)%>', 
                   field: 'OTHERCORP_CUSTOMER_INDICATOR', width: "60px", formatter: disabledCheckBoxFormatter });
<%
  if (!isReadOnly) {
%>
    acctGridLayout[1][8] = 
      new Object ({name: '<%=resMgr.getText("CorporateCustomerAccount.Action",TradePortalConstants.TEXT_BUNDLE)%>', 
                   field: "divId", fields:["rowKey","divId"], width: "45px", formatter:deleteAccountFormatter });
<%
  }
%>
    acctGridLayout[1][9] = new Object ({name:"divId", field:"divId", hidden:"true"});

    var acctGridData = [ <%= accountGridData.toString() %> ];
    <%--create a local memory grid--%>
    acctGrid = t360grid.createMemoryDataGrid("acctGrid", acctGridData, acctGridLayout);
    acctGrid.set('autoHeight',10);
  });
 
  <%-- register event handlers --%>
<%
//AiA - IRT36000016834 - This javascript needs to be available both when readonly and when not
//jgadela CR501 IR# T36000018065 - Added the showApprove in if condition.
%>
  require(["dojo/_base/array", "dojo/_base/xhr", "dojo/dom", "dojo/dom-construct", "dojo/dom-style", "dijit/registry", "dojo/parser","t360/common", "dojo/ready"], 
      function(baseArray, xhr, dom, domConstruct, domStyle, registry, parser, common, ready) {

    ready(function(){
    	<%-- Sandeep CR708BC IR# T36000015147 03/25/2013 - Start --%>
    	var spMarginRowCount = <%=spMarginRulesCount%>;
    	var spRateTypeWidg, spMarginWidg, idx;
    	
    	for (idx=0; idx<spMarginRowCount; idx++) { 
    		spRateTypeWidg = registry.byId("SpRateType"+idx);
    		spMarginWidg = registry.byId("SpMargin"+idx);
    		
    		<%-- jgadela CR501 IR# T36000018065 - Added code  for null check. --%>
    		if(spRateTypeWidg != null && spRateTypeWidg.value == "FXD"){
    			if(spMarginWidg)
    				spMarginWidg.set('disabled', true);
    			if(spMarginWidg)
    				spMarginWidg.set('value', '0.00');
            }else{
            	if(spMarginWidg)
            		spMarginWidg.set('disabled', false);
            }
    	}
    	<%-- ON Loading the page Disable  "SPStraightDiscount, SPDiscountToYield" radio buttons; if "Supplier Portal" check box is not selected --%>
    	var spIndicator = registry.byId("SupplierPortalIndicator").checked;
    	
    	if(!spIndicator){
    		registry.byId("SPStraightDiscount").set('checked', false);
    		registry.byId("SPDiscountToYield").set('checked', false);
    		
    		registry.byId("SPStraightDiscount").set('disabled', true);
    		registry.byId("SPDiscountToYield").set('disabled', true);
    	}
    	
      var myGrid = registry.byId("CorporateAddressDataGridId");
       if(registry.byId("UpdateAddressButton"))
      	registry.byId("UpdateAddressButton").set('disabled',true);
      myGrid.on("SelectionChanged", function(){
        <%--   this will be an array of dojo/data items --%>
        var items = myGrid.selection.getSelected();
        if(items.length!=0){
       	  registry.byId("UpdateAddressButton").set('disabled',false);
        }
        else{
          registry.byId("UpdateAddressButton").set('disabled',true);
        }
        <%-- save key data to global vars for future use --%>
        baseArray.forEach(items, function(item){
          selectedAddrRowKey = myGrid.store.getValue(item, "rowKey");
          selectedAddrDivId = myGrid.store.getValue(item, "divId");
        }, myGrid);
      }, true);

      <%-- use Selected instead of SelectionChanged so we --%>
      <%--  do not include de-selections --%>
      acctGrid.on("Selected", function() {
        <%--  this will be an array of dojo/data items --%>
        var items = this.selection.getSelected();
        if (items.length<=0) return; 
        <%-- get the key data and then use that to get data from --%>
        <%--  the appropriate hidden fields --%>
        baseArray.forEach(items, function(item){
          selectedAcctRowKey = this.store.getValue(item, "@ID");
          selectedAcctDivId = this.store.getValue(item, "divId");
        }, this);
        var myDivIdIdx = selectedAcctDivId.substring("accountHidden".length);
        var myWidget;
        var acctOid = dom.byId("acctOid"+myDivIdIdx).value;
        var acctNum = dom.byId("acctNum"+myDivIdIdx).value;
        var acctName = dom.byId("acctName"+myDivIdIdx).value;
        var acctType = dom.byId("acctType"+myDivIdIdx).value;
        var acctFxRateGrp = dom.byId("fxRateGroup"+myDivIdIdx).value;
        var acctSource = dom.byId("sourceSystemBranch"+myDivIdIdx).value;
        var acctCustId = dom.byId("PropCustomerId"+myDivIdIdx).value;
        var acctDDInd = dom.byId("availableForDirectDebit"+myDivIdIdx).value;
        var acctIntPmtInd = dom.byId("availableForIntPmt"+myDivIdIdx).value;
        var acctTBAInd = dom.byId("availableForTBA"+myDivIdIdx).value;
        var acctPmtInd = dom.byId("availableForDomesticPymt"+myDivIdIdx).value;
        var acctSettlInstrInd = dom.byId("availableForSettlementInstr"+myDivIdIdx).value;
        var acctLoanReqInd = dom.byId("availableForLoanReq"+myDivIdIdx).value;
        var acctDeactInd = dom.byId("deactivateIndicator"+myDivIdIdx).value;
        var acctOpBank = dom.byId("opBankOrgId"+myDivIdIdx).value;
        var acctCountry = dom.byId("bankCountryCode"+myDivIdIdx).value;
        var acctCurr = dom.byId("acctCurr"+myDivIdIdx).value;
        <%-- added for regulatory account type --%>
        var regulatoryAccountType = dom.byId("regulatoryAccountType"+myDivIdIdx).value;	
        var table = document.getElementById("panelTable");
        var rowCount = table.rows.length;
 		var rowCountDup = rowCount;
            for(var i=1; i<rowCount; i++) {
            	table.deleteRow(i);
            	rowCount--;
                i--;
           }
           
            var k=0;
            for(var i=0;i<rowCountDup-1;i++){
            	dijit.byId("pagIdSelectionEdit"+(k+i+1))!=null ? dijit.byId("pagIdSelectionEdit"+(k+i+1)).destroy():null;
            	dijit.byId("pagIdSelectionEdit"+(k+i+2))!=null ? dijit.byId("pagIdSelectionEdit"+(k+i+2)).destroy():null;
            	k++;
            }
                       
    	        var panelGroupsList = new Array();
        var acctPagId1 = (dom.byId("pagIdSelection1_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection1_"+myDivIdIdx).value : dom.byId("pagIdSelection1"+myDivIdIdx).value;
        var acctPagId2 = (dom.byId("pagIdSelection2_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection2_"+myDivIdIdx).value : dom.byId("pagIdSelection2"+myDivIdIdx).value;
        var acctPagId3 = (dom.byId("pagIdSelection3_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection3_"+myDivIdIdx).value : dom.byId("pagIdSelection3"+myDivIdIdx).value;
        var acctPagId4 = (dom.byId("pagIdSelection4_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection4_"+myDivIdIdx).value : dom.byId("pagIdSelection4"+myDivIdIdx).value;
        var acctPagId5 = (dom.byId("pagIdSelection5_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection5_"+myDivIdIdx).value : dom.byId("pagIdSelection5"+myDivIdIdx).value;
        var acctPagId6 = (dom.byId("pagIdSelection6_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection6_"+myDivIdIdx).value : dom.byId("pagIdSelection6"+myDivIdIdx).value;
        var acctPagId7 = (dom.byId("pagIdSelection7_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection7_"+myDivIdIdx).value : dom.byId("pagIdSelection7"+myDivIdIdx).value;
        var acctPagId8 = (dom.byId("pagIdSelection8_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection8_"+myDivIdIdx).value : dom.byId("pagIdSelection8"+myDivIdIdx).value;
        var acctPagId9 = (dom.byId("pagIdSelection9_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection9_"+myDivIdIdx).value : dom.byId("pagIdSelection9"+myDivIdIdx).value;
        var acctPagId10 = (dom.byId("pagIdSelection10_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection10_"+myDivIdIdx).value : dom.byId("pagIdSelection10"+myDivIdIdx).value;
        var acctPagId11 = (dom.byId("pagIdSelection11_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection11_"+myDivIdIdx).value : dom.byId("pagIdSelection11"+myDivIdIdx).value;
        var acctPagId12 = (dom.byId("pagIdSelection12_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection12_"+myDivIdIdx).value : dom.byId("pagIdSelection12"+myDivIdIdx).value;
        var acctPagId13 = (dom.byId("pagIdSelection13_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection13_"+myDivIdIdx).value : dom.byId("pagIdSelection13"+myDivIdIdx).value;
        var acctPagId14 = (dom.byId("pagIdSelection14_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection14_"+myDivIdIdx).value : dom.byId("pagIdSelection14"+myDivIdIdx).value;
        var acctPagId15 = (dom.byId("pagIdSelection15_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection15_"+myDivIdIdx).value : dom.byId("pagIdSelection15"+myDivIdIdx).value;
        var acctPagId16 = (dom.byId("pagIdSelection16_"+myDivIdIdx)!=null) ? dom.byId("pagIdSelection16_"+myDivIdIdx).value : dom.byId("pagIdSelection16"+myDivIdIdx).value;
        
        
        if(acctPagId1 != null && acctPagId1 !=""){
        	panelGroupsList.push(acctPagId1);
        }
        if(acctPagId2 != null && acctPagId2 !=""){
        	panelGroupsList.push(acctPagId2);
        }
        if(acctPagId3 != null && acctPagId3 !=""){
        	panelGroupsList.push(acctPagId3);
        }
        if(acctPagId4 != null && acctPagId4 !=""){
        	panelGroupsList.push(acctPagId4);
        }
        if(acctPagId5 != null && acctPagId5 !=""){
        	panelGroupsList.push(acctPagId5);
        }
        if(acctPagId6 != null && acctPagId6 !=""){
        	panelGroupsList.push(acctPagId6);
        }
        if(acctPagId7 != null && acctPagId7 !=""){
        	panelGroupsList.push(acctPagId7);
        }
        if(acctPagId8 != null && acctPagId8 !=""){
        	panelGroupsList.push(acctPagId1);
        }
        if(acctPagId9 != null && acctPagId9 !=""){
        	panelGroupsList.push(acctPagId9);
        }
        if(acctPagId10 != null && acctPagId10 !=""){
        	panelGroupsList.push(acctPagId10);
        }
        if(acctPagId11 != null && acctPagId11 !=""){
        	panelGroupsList.push(acctPagId11);
        }
        if(acctPagId12 != null && acctPagId12 !=""){
        	panelGroupsList.push(acctPagId12);
        }
        if(acctPagId13 != null && acctPagId13 !=""){
        	panelGroupsList.push(acctPagId13);
        }
        if(acctPagId14 != null && acctPagId14 !=""){
        	panelGroupsList.push(acctPagId14);
        }
        if(acctPagId15 != null && acctPagId15 !=""){
        	panelGroupsList.push(acctPagId15);
        }
        if(acctPagId16 != null && acctPagId16 !=""){
        	panelGroupsList.push(acctPagId16);
        }
        pagOptionsNew = '<%=pagOptionsNew%>';
        console.log("panelGroupsList-"+panelGroupsList);
        k=0;
        var addButton = registry.byId("add2MorePanel");
        if ( addButton ) {
        	addButton.set('disabled',false);
        }
        var rowsToCreate = Math.ceil(panelGroupsList.length/2);
        if(rowsToCreate%2 >0){ 
        	rowsToCreate = rowsToCreate+1;
        }
        if(rowsToCreate == 0){
        	rowsToCreate = 2;
        }
        for(var j=0;j<rowsToCreate;j++){
	        var row = table.insertRow(j+1);
		  	row.id = "panelIndex"+j;
	   	 	var cell0 = row.insertCell(0);<%-- first  cell in the row --%>
			var cell1 = row.insertCell(1);<%-- second  cell in the row --%>
		          cell0.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"pagIdSelectionEdit'+(j+k+1)+'\" id=\"pagIdSelectionEdit'+(j+k+1)+'\"  required=\"false\" class=\"char9\"> '+pagOptionsNew+' </select>';        
		          cell1.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"pagIdSelectionEdit'+(j+k+2)+'\" id=\"pagIdSelectionEdit'+(j+k+2)+'\"  required=\"false\" class=\"char9\"> '+pagOptionsNew+' </select>';       
		          require(["dojo/parser"], function(parser) {
		         	parser.parse(row.id);
		    	 });
		        
		        k++;
        }
        var acctOtherCorp;
        myWidget = dom.byId("otherCorpsAccount"+myDivIdIdx);
        if ( myWidget ) acctOtherCorp = myWidget.value;
        var acctOtherOwnerOid;
        myWidget = dom.byId("otherAccountOwnerOid"+myDivIdIdx);
        if ( myWidget ) acctOtherOwnerOid = myWidget.value;
        var pendingTranBal;
        myWidget = dom.byId("pendingTransactionBalance"+myDivIdIdx);
        if ( myWidget ) pendingTranBal = myWidget.value;
        var pendTranCreditBal;
        myWidget = dom.byId("pendingTransactionCreditBalance"+myDivIdIdx);
        if ( myWidget ) pendTranCreditBal = myWidget.value;

        dom.byId("acctOidEdit").value = acctOid;
        dom.byId("acctNumEdit").value = acctNum;
	dom.byId("acctNumEdit").readOnly=<%=isReadOnly%>;
        dom.byId("acctNameEdit").value = acctName;
	dom.byId("acctNameEdit").readOnly=<%=isReadOnly%>; 
        dom.byId("acctTypeEdit").value = acctType;
	dom.byId("acctTypeEdit").readOnly=<%=isReadOnly%>; 
        if(acctFxRateGrp == null || acctFxRateGrp == ""){
          acctFxRateGrp = fxRateGroupOrg; <%-- default if nothing there --%>
        }
        if(dom.byId("fxRateGroupFEdit")){
        	dom.byId("fxRateGroupFEdit").value = acctFxRateGrp;
        }
        <%--for regulatory account type--%>
        if(dom.byId("regulatoryAccountTypeEdit")){
        	
        	registry.byId("regulatoryAccountTypeEdit").attr('value', regulatoryAccountType);
      	  dom.byId("regulatoryAccountTypeEdit").readOnly=<%=isReadOnly%>;  
        }
        dom.byId("sourceSystemBranchEdit").value = acctSource;
	dom.byId("sourceSystemBranchEdit").readOnly=<%=isReadOnly%>; 
        if(dom.byId("acctCurrEdit")!=null){
          registry.byId("acctCurrEdit").attr('value', acctCurr);
         dom.byId("acctCurrEdit").readOnly=<%=isReadOnly%>; 
	 }
        if(dom.byId("acctCurr_D")!=null){
          registry.byId("acctCurr_D").attr('value', acctCurr);
	  dom.byId("acctCurr_D").readOnly=<%=isReadOnly%>; 
        }
        if(dom.byId("bankCountryCodeEdit")!=null){ 
          registry.byId("bankCountryCodeEdit").attr('value', acctCountry);
	  dom.byId("bankCountryCodeEdit").readOnly=<%=isReadOnly%>;
	}
        if(dom.byId("bankCountryCode_D")!=null){ 
          registry.byId("bankCountryCode_D").attr('value', acctCountry);
	  dom.byId("bankCountryCode_D").readOnly=<%=isReadOnly%>; 
	}
        myWidget = registry.byId("pagIdSelectionEdit1");
        if ( myWidget ) {
          if(acctPagId1 == null || acctPagId1 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId1);
          }
	  dom.byId("pagIdSelectionEdit1").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit2");
        if ( myWidget ) {
          if(acctPagId2 == null || acctPagId2 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId2);
          }
	  dom.byId("pagIdSelectionEdit2").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit3");
        if ( myWidget ) {
          if(acctPagId3 == null || acctPagId3 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId3);
          }
	  dom.byId("pagIdSelectionEdit3").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit4");
        if ( myWidget ) {
          if(acctPagId4 == null || acctPagId4 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId4);
          }
	  dom.byId("pagIdSelectionEdit4").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit5");
        if ( myWidget ) {
          if(acctPagId5 == null || acctPagId5 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId5);
          }
	  dom.byId("pagIdSelectionEdit5").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit6");
        if ( myWidget ) {
          if(acctPagId6 == null || acctPagId6 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId6);
          }
	  dom.byId("pagIdSelectionEdit6").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit7");
        if ( myWidget ) {
          if(acctPagId7 == null || acctPagId7 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId7);
          }
	  dom.byId("pagIdSelectionEdit7").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit8");
        if ( myWidget ) {
          if(acctPagId8 == null || acctPagId8 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId8);
          }
	  dom.byId("pagIdSelectionEdit8").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit9");
        if ( myWidget ) {
          if(acctPagId9 == null || acctPagId9 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId9);
          }
	  dom.byId("pagIdSelectionEdit9").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit10");
        if ( myWidget ) {
          if(acctPagId10 == null || acctPagId10 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId10);
          }
	  dom.byId("pagIdSelectionEdit10").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit11");
        if ( myWidget ) {
          if(acctPagId11 == null || acctPagId11 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId11);
          }
	  dom.byId("pagIdSelectionEdit11").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit12");
        if ( myWidget ) {
          if(acctPagId12 == null || acctPagId12 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId12);
          }
	  dom.byId("pagIdSelectionEdit12").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit13");
        if ( myWidget ) {
          if(acctPagId13 == null || acctPagId13 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId13);
          }
	  dom.byId("pagIdSelectionEdit13").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit14");
        if ( myWidget ) {
          if(acctPagId14 == null || acctPagId14 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId14);
          }
	  dom.byId("pagIdSelectionEdit14").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit15");
        if ( myWidget ) {
          if(acctPagId15== null || acctPagId15 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId15);
          }
	dom.byId("pagIdSelectionEdit15").readOnly=<%=isReadOnly%>;
        }
        myWidget = registry.byId("pagIdSelectionEdit16");
        if ( myWidget ) {
          if(acctPagId16 == null || acctPagId16 == ""){
            myWidget.set('value', "");
          }
          else{
            myWidget.set('value', acctPagId16);
          }
	  dom.byId("pagIdSelectionEdit16").readOnly=<%=isReadOnly%>;
        }

        if(registry.byId("PropCustomerIdEdit")){
        	registry.byId("PropCustomerIdEdit").set('value', acctCustId);
        }
        var deAct = dom.byId("deactivateIndicatorEdit")
        var deAct1 = registry.getEnclosingWidget(deAct);
        if(acctDeactInd == "Y"){
           deAct1.set('checked',true);
           deAct1.set('value',"Y");
        }else{
           deAct1.set('checked',false);
        }
        var directDe = dom.byId("availableForDirectDebitEdit")
        var directDe1 = registry.getEnclosingWidget(directDe);
        if(acctDDInd == "Y"){
          directDe1.set('checked',true);
          directDe1.set('value',"Y");
        }else{
          directDe1.set('checked',false);
        }
        var intPmt = dom.byId("availableForIntPmtEdit")
        var intPmt1 = registry.getEnclosingWidget(intPmt);
        if(acctIntPmtInd == "Y"){
          intPmt1.set('checked',true);
          intPmt1.set('value',"Y");
        }else{
          intPmt1.set('checked',false);
        }

        var tba = dom.byId("availableForTBAEdit")
        var tba1 = registry.getEnclosingWidget(tba);
        if(acctTBAInd == "Y"){
          tba1.set('checked',true);
          tba1.set('value',"Y");
        }else{
          tba1.set('checked',false);
        }
        var pay = dom.byId("availableForDomesticPymtEdit")
        var pay1 = registry.getEnclosingWidget(pay);
        if(acctPmtInd == "Y"){
          pay1.set('checked',true);
          pay1.set('value',"Y");
        }else{
          pay1.set('checked',false);
        }
        
        var settlInstr = dom.byId("availableForSettlementInstrEdit")
        var settlInstr1 = registry.getEnclosingWidget(settlInstr);
        if(acctSettlInstrInd == "Y"){
        	settlInstr1.set('checked',true);
        	settlInstr1.set('value',"Y");
        }else{
        	settlInstr1.set('checked',false);
        }

        var loanReq = dom.byId("availableForLoanReqEdit")
        var loanReq1 = registry.getEnclosingWidget(loanReq);
        if(acctLoanReqInd == "Y"){
          loanReq1.set('checked',true);
          loanReq1.set('value',"Y");
        }else{
          loanReq1.set('checked',false);
        }
        if(acctOtherOwnerOid == 'null' ||  acctOtherOwnerOid == null || acctOtherOwnerOid == undefined ) {
            acctOtherOwnerOid = "";
         }
        dom.byId("ValOtherCorpsAccount").value = acctOtherCorp;
        dom.byId("ValOtherAccountOwnerOid").value = acctOtherOwnerOid;
										  
        if(pendingTranBal == 'null' ||  pendingTranBal == null || pendingTranBal == undefined ) {
          pendingTranBal = "";
        }
        if(pendTranCreditBal == 'null' || pendTranCreditBal == null || pendTranCreditBal == undefined){
          pendTranCreditBal = "";
        }
        if((pendingTranBal.toString() != "") || 
           ((pendTranCreditBal !=null && pendTranCreditBal.toString() != "")&& pendTranCreditBal.toString() != "0") ||
           ( acctOtherCorp && "Y" == acctOtherCorp ) ) {
          registry.byId("acctNumEdit").attr('disabled', true);
          registry.byId("acctNameEdit").attr('disabled', true);
          registry.byId("acctCurrEdit").attr('disabled', true);
          registry.byId("acctTypeEdit").attr('disabled', true);
          registry.byId("bankCountryCodeEdit").attr('disabled', true);
          registry.byId("sourceSystemBranchEdit").attr('disabled', true);
         
          registry.byId("opBankOrgIdEdit").attr('disabled', true);
        } else {
          registry.byId("acctNumEdit").attr('disabled', false);
          registry.byId("acctNameEdit").attr('disabled', false);
          registry.byId("acctCurrEdit").attr('disabled', false);
          registry.byId("acctTypeEdit").attr('disabled', false);
          registry.byId("bankCountryCodeEdit").attr('disabled', false);
          registry.byId("sourceSystemBranchEdit").attr('disabled', false);

          if(registry.byId("opBankOrgIdEdit")){
        	  registry.byId("opBankOrgIdEdit").attr('disabled', false); 
          }
          
        }
        <%-- to disable the controls in approver page --%>
        if(isApproverPage == true) {
	           registry.byId("acctNumEdit").attr('disabled', true);
	           registry.byId("acctNameEdit").attr('disabled', true);
	           registry.byId("acctTypeEdit").attr('disabled', true);
	           if(registry.byId("fxRateGroupFEdit")){
	        	   registry.byId("fxRateGroupFEdit").attr('disabled', true);
			    }
	           if(registry.byId("opBankOrgIdEdit")){
		         	  registry.byId("opBankOrgIdEdit").attr('disabled', true); 
		           }
	         if(registry.byId("regulatoryAccountTypeEdit")){
	           registry.byId("regulatoryAccountTypeEdit").attr('disabled', true);
	         }
	           registry.byId("sourceSystemBranchEdit").attr('disabled', true);
	           registry.byId("acctCurrEdit").attr('disabled', true);
	           registry.byId("bankCountryCodeEdit").attr('disabled', true);
	           registry.byId("deactivateIndicatorEdit").attr('disabled', true);
	           
	           registry.byId("availableForDirectDebitEdit").attr('disabled', true);
	           registry.byId("availableForIntPmtEdit").attr('disabled', true);
	           registry.byId("availableForTBAEdit").attr('disabled', true);
	           registry.byId("availableForDomesticPymtEdit").attr('disabled', true);
	           registry.byId("availableForLoanReqEdit").attr('disabled', true);
	           registry.byId("availableForSettlementInstrEdit").attr('disabled', true);
            
		      if(registry.byId("pagIdSelectionEdit1")){
		    	  registry.byId("pagIdSelectionEdit1").attr('disabled', true);
		      }
		      if(registry.byId("pagIdSelectionEdit2")){
		    	  registry.byId("pagIdSelectionEdit2").attr('disabled', true);
		      }
		      if(registry.byId("pagIdSelectionEdit3")){
		    	  registry.byId("pagIdSelectionEdit3").attr('disabled', true);
		      }
		      if(registry.byId("pagIdSelectionEdit4")){
		    	  registry.byId("pagIdSelectionEdit4").attr('disabled', true);
		      }
              if(registry.byId("pagIdSelectionEdit5")){
              	registry.byId("pagIdSelectionEdit5").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit6")){
              	registry.byId("pagIdSelectionEdit6").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit7")){
            	  registry.byId("pagIdSelectionEdit7").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit8")){
            	  registry.byId("pagIdSelectionEdit8").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit9")){
            	  registry.byId("pagIdSelectionEdit9").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit10")){
            	  registry.byId("pagIdSelectionEdit10").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit11")){
            	  registry.byId("pagIdSelectionEdit11").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit12")){
            	  registry.byId("pagIdSelectionEdit12").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit13")){
            	  registry.byId("pagIdSelectionEdit13").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit14")){
            	  registry.byId("pagIdSelectionEdit14").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit15")){
            	  registry.byId("pagIdSelectionEdit15").attr('disabled', true);
              }
              if(registry.byId("pagIdSelectionEdit16")){
            	  registry.byId("pagIdSelectionEdit16").attr('disabled', true);
              }
	          if ( addButton ) {
	            	addButton.set('disabled',true);
	          }
      }     
        <%-- don't know if we came from initial or subsequent. make visible and enable --%>
        var myAdd = registry.byId("addAcc");
        if ( myAdd ) {
          domStyle.set(myAdd.domNode,'display','inline-block');
          myAdd.set('disabled',false);
        }
        var myAddNew = registry.byId('addNewAccount');
        if ( myAddNew ) {
          domStyle.set(myAddNew.domNode, 'display', 'none');
        }
        var myUpdate = registry.byId('editAccount');
        if ( myUpdate ) {
          domStyle.set(myUpdate.domNode, 'display', 'inline-block');
        }

        local.resetAcctOpBankEdit(acctOtherCorp, acctOpBank);

      }, true);


    });

  });

  require(["dojo/dom", "t360/dialog", "dijit/registry"],
      function( dom, dialog, registry ) {
    <%--  To open update secondary address popup  --%>
    local.updateSecAddressPop = function(){
      if(registry.byId("addSecondaryAddressDialog")) {
        dialog.close('addSecondaryAddressDialog');
      }
      var myDivIdIdx = selectedAddrDivId.substring("addressHidden".length);
      var addrSeqNo = dom.byId("SecAddressSeqNum"+myDivIdIdx).value;
      var addrName = dom.byId("SecName"+myDivIdIdx).value;
      var addrLine1 = dom.byId("SecAddressLine1"+myDivIdIdx).value;
      var addrLine2 = dom.byId("SecAddressLine2"+myDivIdIdx).value;
      var addrCity = dom.byId("SecCity"+myDivIdIdx).value;
      var addrState = dom.byId("SecStateProvince"+myDivIdIdx).value;
      var addrPostal = dom.byId("SecPostalCode"+myDivIdIdx).value;
      var addrCountry = dom.byId("SecCountry"+myDivIdIdx).value;
      var userDefinedField1 = null;
      var userDefinedField2 = null;
      var userDefinedField3 = null;
      var userDefinedField4 = null;
      <% if ( isUserDefinedFieldEnable ) { %>
        userDefinedField1 = dom.byId("SecUserDefinedField1"+myDivIdIdx).value;
        userDefinedField2 = dom.byId("SecUserDefinedField2"+myDivIdIdx).value;
        userDefinedField3 = dom.byId("SecUserDefinedField3"+myDivIdIdx).value;
        userDefinedField4 = dom.byId("SecUserDefinedField4"+myDivIdIdx).value;
      <%}%>
      dialog.open('updateSecondaryAddressDialog', '<%=updateSecondaryAddressTitle%>',
                  'CorpCustAddress.jsp',
                  ['mode', 'divId','rowKey','AddressLine1','AddressLine2','City','StateProvince','Country','PostalCode','SeqNo','Name','userDefinedField1','userDefinedField2','userDefinedField3','userDefinedField4'],
                  ['update', selectedAddrDivId, selectedAddrRowKey, addrLine1, addrLine2, addrCity, addrState, addrCountry, addrPostal, addrSeqNo, addrName,userDefinedField1,userDefinedField2,userDefinedField3,userDefinedField4],  <%-- parameters --%>
                  'updateAddress', local.updateAddress);
    };

    <%--  To open add secondary address popup  --%>
    local.addSecAddressPop = function(){
      if(registry.byId('updateSecondaryAddressDialog')){
        dialog.close('updateSecondaryAddressDialog'); 
      }
      dialog.open('addSecondaryAddressDialog', '<%=addSecondaryAddressTitle%>',
                  'CorpCustAddress.jsp',
                  ['mode'],
                  ['add'], <%-- parameters --%>
                  'addAddress', local.addAddress);
    };

    local.addSubsidiaryAccount = function(){
      dialog.open('addSubsidiaryAccDialog', 'Add Subsidiary Accounts',
                  'AddSubsidiaryAccDialog.jsp',
                  ['corporateOrgOid'],['<%=corporateOrgOid%>'], <%-- parameters --%>
                  'selectAddSub', local.addSubCallBack);
    };

    

    local.setAccountOpBankOrg = function(corporateOrgOid, currentDDInd) {
<%
if(InstrumentServices.isBlank(otherAccountsHidden)) {
%>
      <%--  not being removed.  the below code now uses the 'store' api instead of the --%>
      <%--  'data' api.  becuase 'data' api was being used to add and 'store' to remove --%>
      <%--  the store api could not find the id to remove the item, now its consistent --%>
      try{
        var ValOtherCorpsAccount = document.getElementById("ValOtherCorpsAccount").value;
        if (ValOtherCorpsAccount != 'Y') {
          var currentDD = registry.byId("OperationalBankOrg"+ currentDDInd);
          var currentDDValue = currentDD.get('value');
          <%-- the ...Hid input allows us to get previously selected values --%>
          var currentHid = dom.byId("OperationalBankOrgHid"+ currentDDInd)
          var currentHidValue = currentHid.value;
          var theSel = registry.byId("opBankOrgIdEdit");
          var theSelValue = theSel.get('value');
          <%-- replace the dropdown value --%>
          var addIt = true;
          var newItem;
          if ( currentDDValue && currentDDValue!="" && currentDDValue!=" " ) {
            newItem = {id:currentDDValue, name:currentDD.get('displayedValue')};
          }
          else {
            addIt = false;
          }
          var replacedIt = false;
          if ( currentHidValue && currentHidValue!="" && currentHidValue!=" " ) {
            for (var optindex = 0; optindex < theSel.store.data.length; optindex++) {
              var theSelDataValue = theSel.store.data[optindex].id;
              if (theSelDataValue == currentHidValue) {
                replacedIt = true; 
                <%-- we want to remove, but before doing that --%>
                <%-- check to ensure none of the other op orgs actually --%>
                <%--  have this set --%>
                var otherHas = false;
                for (var otherDDNum = 1; otherDDNum <= 4; otherDDNum++) {
                  var otherDD = registry.byId("OperationalBankOrg"+otherDDNum);
                  if ( otherDD ) {
                    var otherDDValue = otherDD.get('value');
                    if ( otherDDValue && otherDDValue == currentHidValue ) {
                      otherHas = true;
                    }
                  }
                }
                if ( !otherHas ) {
                  <%-- remove the previously selected value --%>
                  theSel.store.remove(currentHidValue);
                }
                if ( addIt ) {
                  <%-- add the new one --%>
                  <%-- if already existing, will not double add --%>
                  theSel.store.put(newItem, {id:newItem.id});
                }
                break;
              }
            }
          }
          if (addIt && !replacedIt) {
            theSel.store.put(newItem, {id:newItem.id});
          }
          if (theSelValue == currentHidValue){
            theSel.set('item',newItem);
          }
          currentHid.value = currentDDValue;
        }
      }catch(error){alert(error)}
<%
}
%>
    }


  });

  function setERPTransactionCheckBox()
  {
  	document.forms[0].ERPTransactionIndicator.checked = true;
  	
  	var checkedObj = dijit.getEnclosingWidget(document.forms[0].ERPTransactionIndicator);
  	checkedObj.set('checked',true);
  	}

  function setERPRadioButtons()
  {
  	   if (document.forms[0].ERPTransactionIndicator.checked == false)
  	   {
  	      document.forms[0].ERPTransactionSelector[0].checked = false;
  	      document.forms[0].ERPTransactionSelector[1].checked = false;

  	      var checkedFlag = false;
  	      var checkedObj = dijit.getEnclosingWidget(document.forms[0].ERPTransactionSelector[0]);
  	       checkedObj.set('checked',checkedFlag);
  	      checkedObj = dijit.getEnclosingWidget(document.forms[0].ERPTransactionSelector[1]);
  	       checkedObj.set('checked',checkedFlag);
  	    }
  	   	   
  	   if(document.forms[0].ERPTransactionIndicator.checked == true){
  			if((document.forms[0].ERPTransactionSelector[0].checked == false) && (document.forms[0].ERPTransactionSelector[1].checked == false)){
  					document.forms[0].ERPTransactionSelector[0].value="B";
  					document.forms[0].ERPTransactionSelector[1].value="";
  			}
  		   var checkedFlag = true;
  	      checkedObj = dijit.getEnclosingWidget(document.forms[0].ERPTransactionSelector[0]);
  	      checkedObj.set('checked',checkedFlag);
  	   }	  



  }

function addMoreExternalBanks() {
	
	var tbl = document.getElementById('ExternalBankTable');
	var lastElement = tbl.rows.length;
	var i,j; 
	
	<%	
	String extBankOptions = Dropdown.createSortedOptions(externalBankOrgsDoc, "ORGANIZATION_OID", "NAME", "", userSession.getSecretKey(), resMgr.getResourceLocale());
    extBankOptions = "<option value=\"\"> </option>"+extBankOptions;	
	%>
	
	for(i=lastElement;i<lastElement+1;i++){
		
	var newRow = tbl.insertRow(i);
	var cell0 = newRow.insertCell(0);
	cell0.setAttribute("align","center");
	var cell1 = newRow.insertCell(1);
	cell1.setAttribute("align","center");
	var cell2 = newRow.insertCell(2);
	cell2.setAttribute("align","center");
	newRow.id = "extBankRow_"+i;
	j = ((i-1)*3);
	
	cell0.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"OperationalBankOid'+(j)+'\" id=\"OperationalBankOid'+(j)+'\" '+' '+'<%=isReadOnly%>'+'>'+'<%=extBankOptions%>'+'</select><INPUT TYPE=HIDDEN NAME=\"ExternalBankOid'+(j)+'\" VALUE=\"\"><INPUT TYPE=HIDDEN NAME=\"CorporateOrgOid'+(j)+'\" VALUE=\"'+<%=corporateOrgOid%>+'\">';
    cell1.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"OperationalBankOid'+(j+1)+'\" id=\"OperationalBankOid'+(j+1)+'\" '+' '+'<%=isReadOnly%>'+'>'+'<%=extBankOptions%>'+'</select><INPUT TYPE=HIDDEN NAME=\"ExternalBankOid'+(j+1)+'\" VALUE=\"\"><INPUT TYPE=HIDDEN NAME=\"CorporateOrgOid'+(j+1)+'\" VALUE=\"'+<%=corporateOrgOid%>+'\">';
    cell2.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"OperationalBankOid'+(j+2)+'\" id=\"OperationalBankOid'+(j+2)+'\" '+' '+'<%=isReadOnly%>'+'>'+'<%=extBankOptions%>'+'</select><INPUT TYPE=HIDDEN NAME=\"ExternalBankOid'+(j+2)+'\" VALUE=\"\"><INPUT TYPE=HIDDEN NAME=\"CorporateOrgOid'+(j+2)+'\" VALUE=\"'+<%=corporateOrgOid%>+'\">';
    
        require(["dojo/parser", "dijit/registry"], function(parser, registry) {
         	parser.parse(newRow.id);         	
    	 });		
	}
	document.getElementById("numberOfMultipleObjects3").value = eval((lastElement*3)); 
	
}
</script>
<script type="text/javascript" src="/portal/js/page/corpCustDetail.js"></script>


</body>

</html>
<%
   // Unregister the Corporate Org web bean that was used in this page
   beanMgr.unregisterBean("CorporateOrganization");

      // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.

         formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<%!
  public String getJson(DocumentHandler xmlDoc)
    throws Exception {
      String xmlResultStr = null;
      JSON json = null;
      if(xmlDoc == null)
            return null;

      xmlResultStr = xmlDoc.toString();
      XMLSerializer xmlSerializer = new XMLSerializer();
      json = xmlSerializer.read( xmlResultStr );
      return "{\"numRows\":"+json.size()+",\"items\":"+json.toString()+"}";
}
%>
<%!
StringBuffer getAccountGridData(Vector aVector, BeanManager beanMgr, SessionWebBean userSession)
{
   StringBuffer data = new StringBuffer();
   if(aVector == null){
		   return data;
   }
   
   int numItems = aVector.size();
   
   for (int iLoop=0;iLoop<numItems;iLoop++) {
       DocumentHandler acctDoc = (DocumentHandler) aVector.elementAt(iLoop);
       String accOid = acctDoc.getAttribute("/ACCOUNT_OID") != null ? acctDoc.getAttribute("/ACCOUNT_OID") : acctDoc.getAttribute("/account_oid");
       String accNum = acctDoc.getAttribute("/ACCOUNT_NUMBER") != null ? acctDoc.getAttribute("/ACCOUNT_NUMBER") : acctDoc.getAttribute("/account_number");
       String accName = acctDoc.getAttribute("/ACCOUNT_NAME")!= null ? acctDoc.getAttribute("/ACCOUNT_NAME") : acctDoc.getAttribute("/account_name");
       String cur = acctDoc.getAttribute("/CURRENCY")!= null ? acctDoc.getAttribute("/CURRENCY") : acctDoc.getAttribute("/currency");
       String accType = acctDoc.getAttribute("/ACCOUNT_TYPE")!= null ? acctDoc.getAttribute("/ACCOUNT_TYPE") : acctDoc.getAttribute("/account_type");
       String bankCountryCode = acctDoc.getAttribute("/BANK_COUNTRY_CODE")!= null ? acctDoc.getAttribute("/BANK_COUNTRY_CODE") : acctDoc.getAttribute("/bank_country_code");;
       String ssb = acctDoc.getAttribute("/SOURCE_SYSTEM_BRANCH")!= null ? acctDoc.getAttribute("/SOURCE_SYSTEM_BRANCH") : acctDoc.getAttribute("/source_system_branch");
       String otherCustInd = acctDoc.getAttribute("/OTHERCORP_CUSTOMER_INDICATOR")!= null ? acctDoc.getAttribute("/OTHERCORP_CUSTOMER_INDICATOR") : acctDoc.getAttribute("/othercorp_customer_indicator");
       //dpatra Rel-9.5 -added for regulatory account type CR-1051- Modified for IR#T36000046817 by dillip 
       String regulatoryAccountType = acctDoc.getAttribute("/A_REGULATORY_ACCOUNT_TYPE")!= null ? acctDoc.getAttribute("/A_REGULATORY_ACCOUNT_TYPE") : acctDoc.getAttribute("/regulatory_account_type");;
       //check if for delete indicator (i.e acctOid is not null, but all main fields are)
       // todo: would be better to use explicit deleteInd but this works for now 
       // note that we leave the actual account rather than leaving row empty...
       if (!InstrumentServices.isBlank(accOid) &&
           InstrumentServices.isBlank(accNum) && InstrumentServices.isBlank(accName) &&
           InstrumentServices.isBlank(cur) && InstrumentServices.isBlank(accType) &&
           InstrumentServices.isBlank(ssb)) {
         //todo: put this data in an array and remove the original one too from display...
         
       }
       else {

         String acctOidEn = null;
         if(accOid!=null){
             acctOidEn = EncryptDecrypt.encryptStringUsingTripleDes(accOid, userSession.getSecretKey());
         }
        //jgadela 03/05/2014 - R8.4 IR T36000025764 [BEGIN]- When we delete the accout from grid extra row was adding in to doc and numItems increased. So comma is adding at the end grid was not displaying. 
         if ( iLoop != 0) {
        	 data.append(", ");
         }
        //jgadela 03/05/2014 - R8.4 IR T36000025764 [END]
                                                    
         //cquinton 11/29/2012 apply xssCharsToHtml, which encodes html chars and single quote char
         data.append("{ ");
         data.append("rowKey:'").append(acctOidEn).append("'");
         data.append(", ");
         data.append("divId:'").append("accountHidden"+iLoop).append("'");
         data.append(", ");
         data.append("ACCOUNT_NUMBER:'").append(StringFunction.xssCharsToHtml(accNum)).append("'");
         data.append(", ");
         data.append("BANK_COUNTRY_CODE:'").append(bankCountryCode).append("'");
         data.append(", ");
         //dpatra Rel-9.5 -added for regulatory account type CR-1051
         data.append("REGULATORY_ACCOUNT_TYPE:'").append(regulatoryAccountType).append("'");
         data.append(", ");
         data.append("CURRENCY:'").append(cur).append("'");
         data.append(", ");
         data.append("ACCOUNT_TYPE:'").append(StringFunction.xssCharsToHtml(accType)).append("'");
         data.append(", ");
         data.append("ACCOUNT_NAME:'").append(StringFunction.xssCharsToHtml(accName)).append("'");
         data.append(", ");
         data.append("SOURCE_SYSTEM_BRANCH:'").append(StringFunction.xssCharsToHtml(ssb)).append("'");
         data.append(", ");
         data.append("OTHERCORP_CUSTOMER_INDICATOR:'").append(otherCustInd).append("'");
  
         data.append(" }");
         
       }
   }
   return data;
}


String createHiddenAccountElements(Vector aVector, BeanManager beanMgr, SessionWebBean userSession)
{
   StringBuffer newRowBuf = new StringBuffer();
   if(aVector == null){
     return "";
   }
   
   int numItems = aVector.size();
   for (int i=0;i<numItems;i++)
   {
       DocumentHandler acctDoc = (DocumentHandler) aVector.elementAt(i);
       System.out.println("accOid="+acctDoc.getAttribute("/ACCOUNT_OID"));
       String accOid = acctDoc.getAttribute("/ACCOUNT_OID") != null ? acctDoc.getAttribute("/ACCOUNT_OID") : acctDoc.getAttribute("/account_oid");
       String accNum = acctDoc.getAttribute("/ACCOUNT_NUMBER") != null ? acctDoc.getAttribute("/ACCOUNT_NUMBER") : acctDoc.getAttribute("/account_number");
       String cur = acctDoc.getAttribute("/CURRENCY")!= null ? acctDoc.getAttribute("/CURRENCY") : acctDoc.getAttribute("/currency");
       String accName = acctDoc.getAttribute("/ACCOUNT_NAME")!= null ? acctDoc.getAttribute("/ACCOUNT_NAME") : acctDoc.getAttribute("/account_name");
       String bankCountryCode = acctDoc.getAttribute("/BANK_COUNTRY_CODE")!= null ? acctDoc.getAttribute("/BANK_COUNTRY_CODE") : acctDoc.getAttribute("/bank_country_code");;
       String accType = acctDoc.getAttribute("/ACCOUNT_TYPE")!= null ? acctDoc.getAttribute("/ACCOUNT_TYPE") : acctDoc.getAttribute("/account_type");
       String ssb = acctDoc.getAttribute("/SOURCE_SYSTEM_BRANCH")!= null ? acctDoc.getAttribute("/SOURCE_SYSTEM_BRANCH") : acctDoc.getAttribute("/source_system_branch");
       String add = acctDoc.getAttribute("/AVAILABLE_FOR_DIRECT_DEBIT")!= null ? acctDoc.getAttribute("/AVAILABLE_FOR_DIRECT_DEBIT") : acctDoc.getAttribute("/available_for_direct_debit");           
       String alrq = acctDoc.getAttribute("/AVAILABLE_FOR_LOAN_REQUEST")!= null ? acctDoc.getAttribute("/AVAILABLE_FOR_LOAN_REQUEST") : acctDoc.getAttribute("/available_for_loan_request");
       String intPay = acctDoc.getAttribute("/AVAILABLE_FOR_INTERNATNL_PAY")!= null ? acctDoc.getAttribute("/AVAILABLE_FOR_INTERNATNL_PAY") : acctDoc.getAttribute("/available_for_internatnl_pay");           
       String xferAcc = acctDoc.getAttribute("/AVAILABLE_FOR_XFER_BTWN_ACCTS")!= null ? acctDoc.getAttribute("/AVAILABLE_FOR_XFER_BTWN_ACCTS") : acctDoc.getAttribute("/available_for_xfer_btwn_accts");    
       String dPay = acctDoc.getAttribute("/AVAILABLE_FOR_DOMESTIC_PYMTS")!= null ? acctDoc.getAttribute("/AVAILABLE_FOR_DOMESTIC_PYMTS") : acctDoc.getAttribute("/available_for_domestic_pymts");
       String settInstr = acctDoc.getAttribute("/AVAILABLE_FOR_SETTLE_INSTR")!= null ? acctDoc.getAttribute("/AVAILABLE_FOR_SETTLE_INSTR") : acctDoc.getAttribute("/available_for_settle_instr");
       String deActInd = acctDoc.getAttribute("/DEACTIVATE_INDICATOR")!= null ? acctDoc.getAttribute("/DEACTIVATE_INDICATOR") : acctDoc.getAttribute("/deactivate_indicator");
        String pendTransBal = acctDoc.getAttribute("/PENDING_TRANSACTION_BALANCE")!= null ? acctDoc.getAttribute("/PENDING_TRANSACTION_BALANCE") : acctDoc.getAttribute("/pending_transaction_balance");
       String pendTranCreBal = acctDoc.getAttribute("/PENDING_TRANSAC_CREDIT_BALANCE")!= null ? acctDoc.getAttribute("/PENDING_TRANSAC_CREDIT_BALANCE") : acctDoc.getAttribute("/pending_transac_credit_balance");   
       String proCustId = acctDoc.getAttribute("/PROPONIX_CUSTOMER_ID")!= null ? acctDoc.getAttribute("/PROPONIX_CUSTOMER_ID") : acctDoc.getAttribute("/proponix_customer_id");
       String fxRateGrp = acctDoc.getAttribute("/FX_RATE_GROUP")!= null ? acctDoc.getAttribute("/FX_RATE_GROUP") : acctDoc.getAttribute("/fx_rate_group");
       String opBankOrgId = acctDoc.getAttribute("/A_OP_BANK_ORG_OID")!= null ? acctDoc.getAttribute("/A_OP_BANK_ORG_OID") : acctDoc.getAttribute("/a_op_bank_org_oid");
       String otherCustInd = acctDoc.getAttribute("/OTHERCORP_CUSTOMER_INDICATOR")!= null ? acctDoc.getAttribute("/OTHERCORP_CUSTOMER_INDICATOR") : acctDoc.getAttribute("/othercorp_customer_indicator");
       String otherAccountOwnerOid = acctDoc.getAttribute("/OTHER_ACCOUNT_OWNER_OID")!= null ? acctDoc.getAttribute("/OTHER_ACCOUNT_OWNER_OID") : acctDoc.getAttribute("/other_account_owner_oid");
       String panAuthGId1 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_1")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_1") : acctDoc.getAttribute("/panel_auth_group_oid_1");
       String panAuthGId2 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_2")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_2") : acctDoc.getAttribute("/panel_auth_group_oid_2");
       String panAuthGId3 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_3")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_3") : acctDoc.getAttribute("/panel_auth_group_oid_3");
       String panAuthGId4 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_4")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_4") : acctDoc.getAttribute("/panel_auth_group_oid_4");
       String panAuthGId5 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_5")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_5") : acctDoc.getAttribute("/panel_auth_group_oid_5");
       String panAuthGId6 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_6")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_6") : acctDoc.getAttribute("/panel_auth_group_oid_6");
       String panAuthGId7 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_7")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_7") : acctDoc.getAttribute("/panel_auth_group_oid_7");
       String panAuthGId8 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_8")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_8") : acctDoc.getAttribute("/panel_auth_group_oid_8");
       String panAuthGId9 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_9")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_9") : acctDoc.getAttribute("/panel_auth_group_oid_9");
       String panAuthGId10 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_10")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_10") : acctDoc.getAttribute("/panel_auth_group_oid_10");
       String panAuthGId11 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_11")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_11") : acctDoc.getAttribute("/panel_auth_group_oid_11");
       String panAuthGId12 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_12")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_12") : acctDoc.getAttribute("/panel_auth_group_oid_12");
       String panAuthGId13 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_13")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_13") : acctDoc.getAttribute("/panel_auth_group_oid_13");
       String panAuthGId14 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_14")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_14") : acctDoc.getAttribute("/panel_auth_group_oid_14");
       String panAuthGId15 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_15")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_15") : acctDoc.getAttribute("/panel_auth_group_oid_15");
       String panAuthGId16 = acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_16")!= null ? acctDoc.getAttribute("/A_PANEL_AUTH_GROUP_OID_16") : acctDoc.getAttribute("/panel_auth_group_oid_16");
       //dpatra Rel-9.5 -added for regulatory account type CR-1051 - Modified for IR#T36000046817 by dillip 
       String regulatoryAccountType = acctDoc.getAttribute("/A_REGULATORY_ACCOUNT_TYPE")!= null ? acctDoc.getAttribute("/A_REGULATORY_ACCOUNT_TYPE") : acctDoc.getAttribute("/regulatory_account_type");
       String acctOidEn = null;
       if(accOid!=null && accOid.length()>0){
         acctOidEn =  EncryptDecrypt.encryptStringUsingTripleDes(accOid, userSession.getSecretKey());
       }
       String opBankOrgIdEn = "";
       if(opBankOrgId!=null && opBankOrgId.length()>0 ){
         opBankOrgIdEn =  EncryptDecrypt.encryptStringUsingTripleDes(opBankOrgId, userSession.getSecretKey());
       }

       //cquinton 11/29/2012 apply xssCharsToHtml, which encodes html chars and single quote char
       newRowBuf.append("<div id=accountHidden"+i+">\n");
       newRowBuf.append("<input id=\"acctOid"+i+"\" name=\"acctOid"+i+"\"");
       newRowBuf.append(" value=\""+(acctOidEn==null?"":acctOidEn)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"acctNum"+i+"\" name=\"acctNum"+i+"\"");
       newRowBuf.append(" value=\""+(accNum==null?"":StringFunction.xssCharsToHtml(accNum))+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"acctName"+i+"\" name=\"acctName"+i+"\"");
       newRowBuf.append(" value=\""+(accName==null?"":StringFunction.xssCharsToHtml(accName))+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"acctCurr"+i+"\" name=\"acctCurr"+i+"\"");
       newRowBuf.append(" value=\""+(cur==null?"":cur)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"acctType"+i+"\" name=\"acctType"+i+"\"");
       newRowBuf.append(" value=\""+(accType==null?"":StringFunction.xssCharsToHtml(accType))+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"bankCountryCode"+i+"\" name=\"bankCountryCode"+i+"\"");
       newRowBuf.append(" value=\""+(bankCountryCode==null?"":bankCountryCode)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"sourceSystemBranch"+i+"\" name=\"sourceSystemBranch"+i+"\"");
       newRowBuf.append(" value=\""+(ssb==null?"":ssb)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"fxRateGroup"+i+"\" name=\"fxRateGroup"+i+"\"");
       newRowBuf.append(" value=\""+(fxRateGrp==null?"":fxRateGrp)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"deactivateIndicator"+i+"\" name=\"deactivateIndicator"+i+"\"");
       newRowBuf.append(" value=\""+deActInd+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"PropCustomerId"+i+"\" name=\"PropCustomerId"+i+"\"");
       newRowBuf.append(" value=\""+(proCustId==null?"":StringFunction.xssCharsToHtml(proCustId))+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"opBankOrgId"+i+"\" name=\"opBankOrgId"+i+"\"");
       newRowBuf.append(" value=\""+(opBankOrgIdEn==null?"":opBankOrgIdEn)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"availableForDirectDebit"+i+"\" name=\"availableForDirectDebit"+i+"\"");
       newRowBuf.append(" value=\""+add+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"availableForLoanReq"+i+"\" name=\"availableForLoanReq"+i+"\"");
       newRowBuf.append(" value=\""+alrq+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"availableForIntPmt"+i+"\" name=\"availableForIntPmt"+i+"\"");
       newRowBuf.append(" value=\""+intPay+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"availableForTBA"+i+"\" name=\"availableForTBA"+i+"\"");
       newRowBuf.append(" value=\""+xferAcc+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"availableForDomesticPymt"+i+"\" name=\"availableForDomesticPymt"+i+"\"");
       newRowBuf.append(" value=\""+dPay+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"availableForSettlementInstr"+i+"\" name=\"availableForSettlementInstr"+i+"\"");
       newRowBuf.append(" value=\""+settInstr+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"otherCorpsAccount"+i+"\" name=\"otherCorpsAccount"+i+"\"");
       newRowBuf.append(" value=\""+otherCustInd+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"otherAccountOwnerOid"+i+"\" name=\"otherAccountOwnerOid"+i+"\"");
       newRowBuf.append(" value=\""+(otherAccountOwnerOid==null?"":otherAccountOwnerOid)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pendingTransactionBalance"+i+"\" name=\"pendingTransactionBalance"+i+"\"");
       newRowBuf.append(" value=\""+(pendTransBal==null?"":pendTransBal)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pendingTransactionCreditBalance"+i+"\" name=\"pendingTransactionCreditBalance"+i+"\"");
       newRowBuf.append(" value=\""+(pendTranCreBal==null?"":pendTranCreBal)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection1_"+i+"\" name=\"pagIdSelection1_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId1==null?"":panAuthGId1)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection2_"+i+"\" name=\"pagIdSelection2_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId2==null?"":panAuthGId2)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection3_"+i+"\" name=\"pagIdSelection3_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId3==null?"":panAuthGId3)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection4_"+i+"\" name=\"pagIdSelection4_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId4==null?"":panAuthGId4)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection5_"+i+"\" name=\"pagIdSelection5_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId5==null?"":panAuthGId5)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection6_"+i+"\" name=\"pagIdSelection6_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId6==null?"":panAuthGId6)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection7_"+i+"\" name=\"pagIdSelection7_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId7==null?"":panAuthGId7)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection8_"+i+"\" name=\"pagIdSelection8_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId8==null?"":panAuthGId8)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection9_"+i+"\" name=\"pagIdSelection9_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId9==null?"":panAuthGId9)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection10_"+i+"\" name=\"pagIdSelection10_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId10==null?"":panAuthGId10)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection11_"+i+"\" name=\"pagIdSelection11_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId11==null?"":panAuthGId11)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection12_"+i+"\" name=\"pagIdSelection12_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId12==null?"":panAuthGId12)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection13_"+i+"\" name=\"pagIdSelection13_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId13==null?"":panAuthGId13)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection14_"+i+"\" name=\"pagIdSelection14_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId14==null?"":panAuthGId14)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection15_"+i+"\" name=\"pagIdSelection15_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId15==null?"":panAuthGId15)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"pagIdSelection16_"+i+"\" name=\"pagIdSelection16_"+i+"\"");
       newRowBuf.append(" value=\""+(panAuthGId16==null?"":panAuthGId16)+"\" type=\"hidden\">\n");
       newRowBuf.append("<input id=\"regulatoryAccountType"+i+"\" name=\"regulatoryAccountType"+i+"\"");
       newRowBuf.append(" value=\""+(regulatoryAccountType==null?"":regulatoryAccountType)+"\" type=\"hidden\">\n");
       newRowBuf.append("</div>\n");
   }
   return newRowBuf.toString();
}
String createOptionList (DocumentHandler inputDoc,
        String valueAttribute, 
        String textAttribute,
        String selectedOption)
{
StringBuffer out = new StringBuffer ();
String  value;
String  text;

if (inputDoc == null) {
return "";
}

try
{
Vector v = inputDoc.getFragments ("/ResultSetRecord");
int numItems = v.size ();

for (int i = 0; i < numItems; i++)
{
DocumentHandler doc = (DocumentHandler)v.elementAt (i);
try
{
value = doc.getAttribute ("/" + valueAttribute);
}
catch (Exception e)
{
value = "0";
}
try
{
text = doc.getAttribute ("/" + textAttribute);
text = StringFunction.xssCharsToHtml(text); 
}
catch (Exception e)
{
text = "";
}
//add SecretKey parameter to allow different keys for each session for better security.            
if (selectedOption == null)
{
out.append ("<option value=\"" + value + "\">" + text +
"</option>");
}
else
{
if (value.equals (selectedOption))
{
out.append ("<option selected value=\"" + value + "\">" +
text + "</option>");
}
else
{
out.append ("<option value=\"" + value + "\">" + text +
"</option>");
}
}
}
}
catch (Exception e)
{
System.out.println ("Error in ListBox.createOptionList " +
e.toString ());
}
return out.toString ();
}
%>

