<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*, com.amsinc.ecsg.html.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   Debug.debug("***START********************CURRENCY*CALENDAR*RULE*DETAIL**************************START***");      
   //ctq portal refresh comment out
   //userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");

   /****************************************************************
    * THIS SCRIPTLET WILL FIND OUT IF WE ARE EDITING A CurrencyCalendarRule
    * CREATING A NEW ONE OR RETURNING FROM AN ERROR FROM A PREVIOUS
    * VERSION OF THIS PAGE.        WE WILL ALSO SET THE PAGE GLOBALS
    * AND POPULATE THE SECURE PARAMETER HASHTABLE THAT WILL BE SENT
    * TO THE FORM MANAGER.
    ****************************************************************/

    /*********\
    * GLOBALS *
    \*********/
   boolean isNew = false; //tells if this is a new currency calendar rule
   boolean isReadOnly = false; //tell if the page is read only
   boolean getDataFromDoc = false;
   boolean showSave = true;
   boolean showDelete = true;
   boolean showSaveButton = true;
   boolean showDeleteButton = true;
   boolean showSaveCloseButton = true;
   String oid = "";
   String loginRights = null;
   String buttonPressed ="";
   //CurrencyCalendarRule Web Bean
   beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.CurrencyCalendarRuleWebBean", "CurrencyCalendarRule");
   CurrencyCalendarRuleWebBean editCurrencyCalendarRule = (CurrencyCalendarRuleWebBean)beanMgr.getBean("CurrencyCalendarRule");
   
   Hashtable secureParams = new Hashtable();

   DocumentHandler doc = null;

   //Get security rights
   loginRights = userSession.getSecurityRights();
   isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CURRENCY_CALENDAR_RULES);
   Debug.debug("MAINTAIN_CURRENCY_CALENDAR_RULES == " + SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CURRENCY_CALENDAR_RULES));

     // Get the document from the cache.  We'll use it to repopulate the screen if the
   // user was entering data with errors.
   doc = formMgr.getFromDocCache();
   buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
      Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");

   Debug.debug("doc from cache is " + doc.toString());

   if (doc.getDocumentNode("/In/CurrencyCalendarRule") != null ) {
   // The doc does exist so check for errors.

       String maxError = doc.getAttribute("/Error/maxerrorseverity");
         
       oid = doc.getAttribute("/Out/CurrencyCalendarRule/currency_calendar_rule_oid");
         if(oid == null || "0".equals(oid) || "".equals(oid)){
              oid = doc.getAttribute("/In/CurrencyCalendarRule/currency_calendar_rule_oid");
       }

       if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
       // We've returned from a save/update/delete that was successful
       // We shouldn't be here.  Forward to the RefDataHome page.
         getDataFromDoc = true;
       
       } else {
       // We've returned from a save/update/delete but have errors.
       // We will display data from the cache.  Determine if we're
       // in insertMode by looking for a '0' oid in the input section
       // of the doc.

           getDataFromDoc = true;

           if(doc.getAttribute("/In/CurrencyCalendarRule/currency_calendar_rule_oid").equals("0"))
           {
               Debug.debug("Create CurrencyCalendarRule Error");
               isNew = true;//else it stays false
           }

           if (!isNew)
           // Not in insert mode, use oid from input doc.
           {
               Debug.debug("Update CurrencyCalendarRule Error");
               oid = doc.getAttribute("/In/CurrencyCalendarRule/currency_calendar_rule_oid");
           }
       }
   }
   if (getDataFromDoc)
   {
       // We create a new docHandler to work around an aparent bug in AMSEntityWebbean
       // This hurts performance and should be removed as soon as populateFromXmlDoc(doc,path) works
       DocumentHandler doc2 = doc.getComponent("/In");
       Debug.debug("Populating currency calendar rule with:\n" + doc2.toString(true));
       editCurrencyCalendarRule.populateFromXmlDoc(doc2);
       if (!isNew)
           editCurrencyCalendarRule.setAttribute("currency_calendar_rule_oid",oid);
           oid = editCurrencyCalendarRule.getAttribute("currency_calendar_rule_oid");
   }
   else if (request.getParameter("oid") != null) //EDIT CurrencyCalendarRule
   {
       isNew = false;
       //This getParameter is done twice to prevent overwriting a valid oid
       oid = EncryptDecrypt.decryptStringUsingTripleDes(StringFunction.xssHtmlToChars(request.getParameter("oid")), userSession.getSecretKey());
       Debug.debug("Editing CurrencyCalendarRule with oid -> " + oid);
       editCurrencyCalendarRule.getById(oid);
       Debug.debug("Editing Currency Calendar Rule with oid: " + editCurrencyCalendarRule.getAttribute("currency_calendar_rule_oid")); //DEBUG
       Debug.debug("isReadOnly == " + isReadOnly);
         }
   else 
    {
       isNew = true;
         oid = null;
    }

   // Auto save the form when time-out if not readonly.  
   // (Header.jsp will check for auto-save setting of the corporate org).
   String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

   if (isReadOnly)
   {
         showSaveButton = false;
         showSaveCloseButton = false;
       showDeleteButton=false;
   }
   else if (isNew)
   {
       oid = "0";
       editCurrencyCalendarRule.setAttribute("currency_calendar_rule_oid",oid);
       showDelete = false;
       showDeleteButton=false;
   }
%>

<%-- Body tag included as part of common header --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp" />


<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="CurrencyCalendarRuleDetail.CurrencyCalendarRule"/>
      <jsp:param name="helpUrl" value="/customer/Currency_Calendar_Rules_Form.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (isNew) {
    oid = "0";
    editCurrencyCalendarRule.setAttribute("currency_calendar_rule_oid",oid);
    subHeaderTitle = resMgr.getText("CurrencyCalendarRuleDetail.NewCurrencyCalendarRule",TradePortalConstants.TEXT_BUNDLE);
  }
  else {
    subHeaderTitle = editCurrencyCalendarRule.getAttribute("rule_name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>

<form name="CurrencyCalendarRuleDetail" id="CurrencyCalendarRuleDetail" data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>

                  <%    
            /*******************
            * START SECUREPARAMS
            ********************/
            secureParams.put("oid",oid);
            secureParams.put("login_oid",userSession.getUserOid());
            secureParams.put("login_rights",loginRights);
            secureParams.put("opt_lock", editCurrencyCalendarRule.getAttribute("opt_lock"));
            secureParams.put("owner_oid",userSession.getOwnerOrgOid());
            /*****************
            * END SECUREPARAMS
            ******************/

          %>
          
                                           <div class="formArea">
                          <jsp:include page="/common/ErrorSection.jsp" />
                          
                        <div class="formContent">
                        <div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
                                    <div class="dijitTitlePaneContentInner">
            
        <%
        /********************************
         * START RULE NAME INPUT FIELD
         ********************************/
        Debug.debug("Building Rule Name field");
        
        
        out.print(widgetFactory.createTextField( "ruleName", "CurrencyCalendarRuleDetail.RuleName",editCurrencyCalendarRule.getAttribute("rule_name"), "30", isReadOnly, true, false,"","","inline"));
        /********************************
         * END RULE NAME INPUT FIELD
         ********************************/
         %>
      
            <%
            /********************************
             * START CALENDAR NAME DROPDOWN
             ********************************/
                     // Retrieve the data used to populate some of the dropdowns.
                     // This data will be used in the default users dropdown

                     StringBuffer sql = new StringBuffer();
                     QueryListView queryListView = null;
                     DocumentHandler queryDoc = null;

                     try {
                          queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
                                      formMgr.getServerLocation(), "QueryListView");

                          // Get a list of the users for the client bank
                          sql = new StringBuffer();
                          sql.append("select TP_CALENDAR_OID, CALENDAR_NAME");
                          sql.append(" from TP_CALENDAR");
                          Debug.debug(sql.toString());
                          queryListView.setSQL(sql.toString(), new ArrayList());

                          queryListView.getRecords();

                          queryDoc = queryListView.getXmlResultSet();
                          Debug.debug("queryDoc -- > " + queryDoc.toString());

                     } catch (Exception e) {
                          e.printStackTrace();
                     } finally {
                          try {
                                 if (queryListView != null) {
                                        queryListView.remove();
                                 }
                          } catch (Exception e) {
                                 System.err.println("error removing querylistview in CurrencyCalendarRuleDetail.jsp");
                          }
                     }  // try/catch/finally block
            Debug.debug("Building Calendar Name field");

            String options = Dropdown.createSortedOptions(queryDoc,"TP_CALENDAR_OID", "CALENDAR_NAME",
                                                  editCurrencyCalendarRule.getAttribute("tp_calendar_oid"), userSession.getSecretKey(), resMgr.getResourceLocale());
            Debug.debug(options);

            if (isNew)
               options = "<option> </option>" + options;
         
            out.print(widgetFactory.createSelectField( "tpCalendar" ,"CurrencyCalendarRuleDetail.CalendarNameYear","", options, isReadOnly,true, false, "", "", "inline"));
            /********************************
             * END CALENDAR NAME DROPDOWN
             ********************************/
            %>
         
            <%
            /********************************
             * START CURRENCY DROPDOWN
             ********************************/
            Debug.debug("Building Currency field");
                        options = Dropdown.createSortedCurrencyCodeOptions(editCurrencyCalendarRule.getAttribute("currency"), resMgr.getResourceLocale());
            if (isNew)
                   options =options;
                //MEer Rel 9.3 XSS CID 11920
            out.print(widgetFactory.createCurrencySelectField("currency","CurrencyCalendarRuleDetail.Currency",options,StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(editCurrencyCalendarRule.getAttribute("currency"))),isReadOnly,true,false));
            /********************************
             * END CURRENCY DROPDOWN
             ********************************/
            %>
            
</div>
</div>
  </div><%--formContent--%>
  </div><%--formArea--%>

  <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'CurrencyCalendarRuleDetail'">
     <jsp:include page="/common/RefDataSidebar.jsp">
       <jsp:param name="showSaveButton" value="<%= showSaveButton %>" />
       <jsp:param name="saveOnClick" value="none" />
       <jsp:param name="showDeleteButton" value="<%= showDeleteButton %>" />
       <jsp:param name="showSaveCloseButton" value="<%= showSaveCloseButton %>" />
       <jsp:param name="saveCloseOnClick" value="none" />
       <jsp:param name="cancelAction" value="goToRefDataHome" />
       <jsp:param name="showHelpButton" value="false" />
       <jsp:param name="helpLink" value="/customer/currency_calendar_rules_overview.htm" />
       <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
       <jsp:param name="error" value="<%= error%>" />
     </jsp:include>        
   </div> <%--closes sidebar area--%>
            
    
  <%= formMgr.getFormInstanceAsInputField("CurrencyCalendarRuleForm", secureParams) %>
  
</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<%
  //save behavior defaults to without hsm
   
  //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  
  // String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  // String saveCloseOnClick = "common.setButtonPressed('SaveCloseTrans','0'); document.forms[0].submit();";

 
%>
  

<jsp:include page="/common/RefDataSidebarFooter.jsp" />
  

<%
  String focusField = "ruleName";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
</script>
<%
  }
%>

</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("CurrencyCalendarRule");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   Debug.debug("***END********************CurrencyCalendarRule*DETAIL**************************END***");
%>
