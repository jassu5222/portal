<%--
 *
 *     AAlubala - Rel8.2 CR741 - 01/24/2013                        
 *     DISCOUNT GL CODES - Refdata
 *     
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
   
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   boolean isNew = false;
   boolean showSaveAs = true;
   //
   //start - 

  String 	loginLocale 		= userSession.getUserLocale();
  String 	loginTimezone		= userSession.getTimeZone();
  String 	loginRights 		= userSession.getSecurityRights();
  String parentOrgID        = userSession.getOwnerOrgOid();
  String 	tabPressed		= request.getParameter("tabPressed");	// This is a local attribute from the webBean
 
  DocumentHandler defaultDoc		= formMgr.getFromDocCache();
  Hashtable 	secureParams		= new Hashtable();
  boolean	hasEditRights		= SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_DISCOUNT_CODES );
  boolean	isReadOnly		= true;
  boolean  insertMode      = true;
  boolean  showDelete      = true;
  boolean  showSave        = true;
  boolean  showSaveClose        = true;
  boolean       getDataFromDoc          = false;   
   //end
   String oid =  request.getParameter("oid");
   //String loginRights;  
  
   beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.CustomerDiscountCodeWebBean", "CustomerDiscountCode");
   CustomerDiscountCodeWebBean customerDiscountCode = (CustomerDiscountCodeWebBean)beanMgr.getBean("CustomerDiscountCode");

   DocumentHandler doc      = null;
   
   //Get WidgetFactory instance..
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   String   refDataHome       = "goToRefDataHome";                //used for the close button and Breadcrumb link
   String buttonPressed ="";
   //Get security rights
   //loginRights = userSession.getSecurityRights();
   //IR T36000011362 - isReadOnly was being set to false
   //isReadOnly = false;

   doc = formMgr.getFromDocCache();
   buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
   
   Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");
   Debug.debug("doc from cache is " + doc.toString());

   //isNew = true;
   // oid = null; 
    //showSaveAs = true;   

    // Auto save the form when time-out if not readonly.  
    // (Header.jsp will check for auto-save setting of the corporate org).
    String onLoad = "";

	  if( hasEditRights ) 			//If the user has the right to Maintain/edit data SET Readonly to False.
		isReadOnly = false;
	
	  if( InstrumentServices.isBlank(oid) ) {
		oid = defaultDoc.getAttribute("/Out/oid");
	  }
	
	  if( InstrumentServices.isNotBlank(parentOrgID) )
	  	customerDiscountCode.setAttribute("p_owner_oid", parentOrgID);
	  if( InstrumentServices.isNotBlank(oid) ) {
	     insertMode = false;
	     oid = EncryptDecrypt.decryptStringUsingTripleDes( oid, userSession.getSecretKey() );
	     Debug.debug("***** customer discount oid == " + oid);
	     customerDiscountCode.setAttribute("customer_disc_code_oid", oid);
	     
	     customerDiscountCode.getDataFromAppServer();
	  }else {
	      oid = defaultDoc.getAttribute("/Out/CustomerDiscountCode/customer_disc_code_oid");
	    
	      if( InstrumentServices.isNotBlank(oid) ) {
	    	  customerDiscountCode.setAttribute("customer_disc_code_oid", oid);
	    	  customerDiscountCode.getDataFromAppServer();
	      }
	      else {
	         oid = "0";
	         insertMode = true;
	         isNew = true;
	          showSaveAs = true;  	         
	      }
	
	  }    
    
    
    String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

    if (isReadOnly)
    {
        showSave = false;
        showDelete = false;
        showSaveClose = false;
        showSaveAs = false;
    }
    else if (isNew){
        showDelete = false;
        showSaveAs = false;
    }
    secureParams.put("login_rights", loginRights);
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<div class="pageMain">        <%-- Page Main Start --%>
  <div class="pageContent">   <%--  Page Content Start --%>


    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="Customer Discount Codes"/>
      <jsp:param name="helpUrl" value="customer/discount_details.htm"/>
    </jsp:include>

<% 
  String subHeaderTitle = "";

%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>      

<form name="DiscountCodesDetailForm" id="DiscountCodesDetailForm" data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name=buttonName>
  <input type=hidden value="" name=saveAsFlag>

<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">           <%--  Form Content Start --%>
   <div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
      <div class="dijitTitlePaneContentInner"> 
    <%--  RPasupulati modified table IR no :-T36000016909 Starts  --%>
	<table id="codesIDTable" style="width:70%" border='0'>
		<thead>
			<tr style="height:36px">
			<input type=hidden id="discountCode">
				<th style="width:25%" align=left valign='bottom'><span class=requiredAstrickWithOutFormItem><label for=DiscountCode><%=resMgr.getText("DiscountCodes.DiscountCode",TradePortalConstants.TEXT_BUNDLE)%></label></span></th>
				<th style="width:50%" align=left valign='bottom'><span class=requiredAstrickWithOutFormItem><label for=DiscountCode><%=resMgr.getText("DiscountCodes.Description",TradePortalConstants.TEXT_BUNDLE)%></label></span></th>
				<th style="width:18%" align=left valign='top'><%= ""+widgetFactory.createSubLabel("DiscountCodes.DefaultDiscountCode") %></th>
				<th style="width:25%" align=left valign='bottom'><%= "&nbsp;&nbsp;"+widgetFactory.createSubLabel("DiscountCodes.Deactivate") %></th>			
			</tr>
		</thead>	
		<tr id="disccode1" valign='bottom'>
			<td>      
			  <%=widgetFactory.createTextField("discountCode1","",
				customerDiscountCode.getAttribute("discount_code") ,"2",isReadOnly,true,false, "style='width: 75px'", "", "none") %>
			</td> 
 			
			<td>      
			  <%=widgetFactory.createTextField("desc1","",
				customerDiscountCode.getAttribute("discount_description") ,"30",isReadOnly,true,false, "style='width: 175px'", "", "none") %>
			</td>
			<%--  Srinivasu_D CR#997 Rel9.3 03/10/2015 - Added new field --%>
			 <td>         
			<%=widgetFactory.createCheckboxField("default_disc_rate_ind1", "", 
			customerDiscountCode.getAttribute("default_disc_rate_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "none")%> 
			</td>
			<td>         
			<%=widgetFactory.createCheckboxField("deactivate1", "", 
			customerDiscountCode.getAttribute("deactivate_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "none")%> 
			</td>		                                                
		</tr>
		<%if (isNew){ 
			for(int i=2; i<9; i++){
		%>		
		<tr id="disccode"<%=i%>>
			<td>      
			  <%=widgetFactory.createTextField("discountCode"+i,"",
				customerDiscountCode.getAttribute("discount_code") ,"2",isReadOnly,true,false, "style='width: 75px'", "", "none") %>
			</td> 
 			
			<td>      
			  <%=widgetFactory.createTextField("desc"+i,"",
				customerDiscountCode.getAttribute("discount_description") ,"30",isReadOnly,true,false, "style='width: 175px'", "", "none") %>
			</td>
			<%--  Srinivasu_D CR#997 Rel9.3 03/10/2015 - Added Default disc rate Ind --%>
			 <td>         
			<%=widgetFactory.createCheckboxField("default_disc_rate_ind"+i, "", 
			customerDiscountCode.getAttribute("default_disc_rate_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "none")%> 
			</td>	
			<%--  Srinivasu_D CR#997 Rel9.3 03/10/2015 - End --%>
			<td>         
			<%=widgetFactory.createCheckboxField("deactivate"+i, "", 
			customerDiscountCode.getAttribute("deactivate_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "none")%> 
			</td>		                                                
		</tr>		
		<%
			} 
		
		}
	
		%>		
	</table> 

<br/>
	<%
	//IR T36000015498
	 if(isNew){	
	if (!(isReadOnly)) {	
		// jgadela 09/25/2014 r9.1 IR T36000032483 - Add 4 more rows issue%>
			<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreCodes">
				<%=resMgr.getText("common.Add4MoreDiscountCodesText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					add4MoreCodes();
				</script>
			</button>
		<% } else {		%>
					&nbsp;
		<% 
		}
  }	
  if(isNew){
  for(int ii=1; ii<9; ii++){ %>
	  <input type=hidden value="<%=StringFunction.xssCharsToHtml(oid) %>" name=oid<%=ii%>>
	  <input type=hidden value="<%=StringFunction.xssCharsToHtml(parentOrgID) %>" name=powner<%=ii%>>
  <%}
  }else{
  %>
	  <input type=hidden value="<%=StringFunction.xssCharsToHtml(oid) %>" name=oid1>
	  <input type=hidden value="<%=StringFunction.xssCharsToHtml(parentOrgID) %>" name=powner1>		
 <%} %>
		
 
   </div>
  </div>
  </div>          <%--  Form Content End --%>
</div><%--formArea--%>

 <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'DiscountCodesDetailForm'">
<jsp:include page="/common/RefDataSidebar.jsp">
      <jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
      <jsp:param name="showSaveButton" value="<%= showSave %>" />
      <jsp:param name="saveOnClick" value="none" />
      <jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
      <jsp:param name="saveCloseOnClick" value="none" />
      <jsp:param name="showHelpButton" value="true" />
      <jsp:param name="cancelAction" value="goToRefDataHome" />
      <jsp:param name="helpLink" value="" />
      <jsp:param name="saveAsDialogId" value="saveAsDialogId" />
      <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
      <jsp:param name="error" value="<%=error%>" />
      <jsp:param name="showLinks" value="false" />
</jsp:include>
</div> <%-- Side Bar End--%>

  <%= formMgr.getFormInstanceAsInputField("DiscountCodesDetailForm", secureParams) %>
</form>

</div>            <%--  Page Content End --%> 
</div>            <%--  Page Main End --%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<%
  //save behavior defaults to without hsm
  
  	//(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  
   // String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
   // String saveCloseOnClick = "common.setButtonPressed('SaveAndClose','0'); document.forms[0].submit();";
%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />
   

 
 <div id="saveAsDialogId" style="display: none;"></div>
 
<%
  String focusField = "nameField";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          if(focusField){ 
                  focusField.focus();
            }
        }
      });
  });
  
  function submitSaveAs() 
  {
          if(window.document.saveAsForm.newProfileName.value == ""){
            return false;
          }
            document.DiscountCodesDetailForm.nameField.value=window.document.saveAsForm.newProfileName.value;
            document.DiscountCodesDetailForm.oid.value="0";
            <%-- Sets a hidden field with the 'button' that was pressed.  This --%>
            <%-- value is passed along to the mediator to determine what --%>
            <%-- action should be done. --%>
            document.DiscountCodesDetailForm.saveAsFlag.value = "Y";
            document.DiscountCodesDetailForm.buttonName.value = "SaveAndClose";
            document.DiscountCodesDetailForm.submit();
            hideDialog("saveAsDialogId");
   }
  
	function add4MoreCodes() {
		<%--  IR T36000016909 Starts Rpasupulati --%>
		<%-- so that user can only add 4 rows --%>
		var tbl = document.getElementById('codesIDTable');<%-- to identify the table in which the row will get insert --%>
		var lastElement = tbl.rows.length;
		var NewRow="";
			 
		for(i=lastElement;i<lastElement+4;i++){
			
	   	 	var newRow = tbl.insertRow(i);<%-- creation of new row --%>
	   	 	var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
			var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
			var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
			<%--  jgadela 09/25/2014 r9.1 IR T36000032483 - Add 4 more rows issue --%>
			var cell3 = newRow.insertCell(3);<%-- third  cell in the row --%>
			var cell4 = newRow.insertCell(4);<%-- third  cell in the row --%>
			var cell5 = newRow.insertCell(5);<%-- third  cell in the row --%>
			newRow.id = "disccode"+i;
			j = i;
			var name = "discountCode"+j;
	        cell0.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"discountCode'+j+'\" id=\"discountCode'+j+'\"  class=\'char15\'  style=\"width:75px;\"  maxLength=\"2\">'
	       	cell1.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"desc'+j+'\" id=\"desc'+j+'\" style=\"width:175px;\"  maxLength=\"30\">'
	        cell2.innerHTML='<input data-dojo-type=\'dijit.form.CheckBox\' name=\"default_disc_rate_ind'+j+'\" id=\"default_disc_rate_ind'+j+'\"  value=\'N\'>'
			cell3.innerHTML='<input data-dojo-type=\'dijit.form.CheckBox\' name=\"deactivate'+j+'\" id=\"deactivate'+j+'\"  value=\'Y\'>'
	        
	       <%--  jgadela 09/25/2014 r9.1 IR T36000032483 - Add 4 more rows issue --%>
	        cell4.innerHTML='<input type=hidden value=\"0\" name=\"oid'+j+'\">'
	        var value = '<%=StringFunction.xssCharsToHtml(parentOrgID) %>'
	        cell5.innerHTML='<input type=hidden value=\"'+value+'\" name=\"powner'+j+'\">'
	        
	        require(["dojo/parser", "dijit/registry"], function(parser, registry) {
	         	parser.parse(newRow.id);
	         	
	    	 });
			
		}
		document.getElementById("discountCode").value = eval(lastElement+3);  
		<%--  IR T36000016909 Ends Rpasupulati --%>
	}  
  <%--  RPasupulati modification IR no :-T36000016909 Ends --%>

 function preSaveAndClose(){
  	 	 
 	  document.forms[0].buttonName.value = "SaveAndClose";
 	  return true;

}



</script>
<%
  }
%>

</body>

</html>

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("CustomerDiscountCode");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
