<!DOCTYPE HTML>
<%--
**********************************************************************************
                              FX Rate Detail

  Description:
     This page is used to maintain individual foreign exchange rates.  It supports
     insert, update, and delete.  Errors found during a save are redisplayed on
     the same page. Successful updates return the user to the ref data home page.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page
      import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,java.util.*,
      com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*"%>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
      scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
      scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
      scope="session"></jsp:useBean>
<jsp:useBean id="userSession"
      class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
      scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>
<%	  
   	  String focusField = "";
      DocumentHandler xmlDoc = null;
      QueryListView queryListView = null;
      FXRateWebBean fxRate = null;
      StringBuffer updateText = new StringBuffer();
      StringBuffer sqlQuery = new StringBuffer();
      Hashtable secureParms = new Hashtable();
      boolean getDataFromXmlDocFlag = false;
      //boolean showDeleteButton = false;
      //Modfied by dillip for defect #T36000005318
      boolean showDeleteButton = true;
      boolean showCloseButton = false;
      boolean showSaveButton = true;
      boolean showSaveCloseButton = true;
      boolean retrieveFromDBFlag = false;
      boolean insertModeFlag = true;
      boolean isReadOnly = false;
      boolean errorFlag = false;
      // KMehta - 10 Sep 2014 - Rel9.1 IR-T36000031902 - Add  - Begin
      DocumentHandler errorDoc = new DocumentHandler();
      String maxError = null;
      boolean currencyVisibleFlag = true;
      // KMehta - 10 Sep 2014 - Rel9.1 IR-T36000031902 - Add  - End
      String userSecurityRights = null;
      String multiplyIndicator = "";
      String baseCurrencyCode = null;
      String dropdownOptions = null;
      String fxRateCurrency = null;
      String currencyCode = null;
      String defaultText = null;
      String userOrgOid = null;
      String userLocale = null;
      String fxRateOid = null;
      String onLoad = null;
      String buttonPressed="";

      Vector codesToExclude = new Vector();
	
      String regExpRange = "";
	  regExpRange = "[0-9]{0,5}([.][0-9]{0,8})?";
	  regExpRange = "regExp:'" + regExpRange + "'";
      //WidgetFactory object is creating here

      WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);

      beanMgr.registerBean(
                  "com.ams.tradeportal.busobj.webbean.FXRateWebBean",
                  "FXRate");

      fxRate = (FXRateWebBean) beanMgr.getBean("FXRate");

      // Retrieve the user's locale and security rights
      userSecurityRights = userSession.getSecurityRights();
      userLocale = userSession.getUserLocale();

      // Determine whedther the user has read-only or edit access
      isReadOnly = !SecurityAccess.hasRights(userSecurityRights,
                  SecurityAccess.MAINTAIN_FX_RATE);

      // Get the document from the cache.  We'll use it to repopulate the screen if the
      // user was entering data with errors.
      xmlDoc = formMgr.getFromDocCache();
      buttonPressed = xmlDoc.getAttribute("/In/Update/ButtonPressed");
      Vector error = null;
      error = xmlDoc.getFragments("/Error/errorlist/error");
      if (xmlDoc.getDocumentNode("/In/FXRate") == null) {
            // The document does not exist in the cache.  We are entering the page from
            // the listview page (meaning we're opening an existing item or creating
            // a new one.)

      if(request.getParameter("oid") != null)
       {
          fxRateOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
       }
	else
	 {
                  fxRateOid = null;
            }

            if (fxRateOid == null) {
                  getDataFromXmlDocFlag = false;
                  retrieveFromDBFlag = false;
                  insertModeFlag = true;
                  fxRateOid = "0";
            } else if (fxRateOid.equals("0")) {
                  getDataFromXmlDocFlag = false;
                  retrieveFromDBFlag = false;
                  insertModeFlag = true;
            } else {
                  // We have a good oid so we can retrieve.
                  getDataFromXmlDocFlag = false;
                  retrieveFromDBFlag = true;
                  insertModeFlag = false;
            }
      } else {
            // We've returned from a save/update/delete but have errors.
            // We will display data from the cache.  Determine if we're
            // in insertMode by looking for a '0' oid in the input section
            // of the doc.
            retrieveFromDBFlag = false;
            getDataFromXmlDocFlag = true;
            errorFlag = true;
	          // KMehta - 10 Sep 2014 - Rel9.1 IR-T36000031902 - Add  - Begin
            errorDoc.addComponent("/Error",xmlDoc.getComponent("/Error"));
	    maxError = errorDoc.getAttribute("/Error/maxerrorseverity");
	          // KMehta - 10 Sep 2014 - Rel9.1 IR-T36000031902 - Add  - End

            fxRateOid = xmlDoc.getAttribute("/In/FXRate/fx_rate_oid");
      // KMehta - 10 Sep 2014 - Rel9.1 IR-T36000031902 - Change  - Begin
//	                if (fxRateOid.equals("0")) {
      if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY))>=0) {

                  insertModeFlag = true;
            } else {
                  // Not in insert mode, use oid from output doc.
                  insertModeFlag = false;
            }
      }

      if (retrieveFromDBFlag) {
            // Attempt to retrieve the item.  It should always work.  Still, we'll
            // check for a blank oid -- this indicates record not found.  Set to insert
            // mode and display error.

            getDataFromXmlDocFlag = false;

            fxRate.getById(fxRateOid);

            if (fxRate.getAttribute("fx_rate_oid").equals("")) {
                  insertModeFlag = true;
                  fxRateOid = "0";
                  getDataFromXmlDocFlag = false;
            } else {
                  insertModeFlag = false;
            }
      }

      if (getDataFromXmlDocFlag) {
            // Populate the FX rate web bean with the output doc.
            try {
                  fxRate.populateFromXmlDoc(xmlDoc.getComponent("/In"));
            } catch (Exception e) {
                  System.out.println("Contact Administrator: Unable to get FXRate attributes "
                                          + "for oid: "
                                          + fxRateOid
                                          + " "
                                          + e.toString());
            }
      }

      //BSL IR# BIUL050954395 BEGIN
      //Page should be read-only if corporate customer opens rate that was defined on client bank
      if (!insertModeFlag && !isReadOnly) {
            userOrgOid = userSession.getOwnerOrgOid();
            if (InstrumentServices.isNotBlank(userOrgOid)
                        && !userOrgOid.equals(fxRate
                                    .getAttribute("owner_org_oid"))) {
                  isReadOnly = true;
            }
      }
      //BSL IR# BIUL050954395 END

      // Determine whether the Delete and Save buttons should be displayed
      //Modified by dillip for defect T36000005318
      if (isReadOnly || insertModeFlag) {

    	  showDeleteButton = false;

            if (isReadOnly) {
                  showSaveButton= false;
                  showSaveCloseButton=false;
                  showDeleteButton = false;

            }
      }
//Ended Here
      // Set the text to display next to the FX Rates link based on whether the Create button was selected or
      // an existing FX Rate was selected from the previous page
      if (insertModeFlag) {
            fxRateCurrency = widgetFactory.createLabel(
                        "FXRatesDetail.NewFXRate", "FXRatesDetail.NewFXRate",
                        isReadOnly, false, false, "");

            // If we're creating a new FX Rate, retrieve the list of FX currencies available to choose from
            // (excluding the user's base currency and all existing FX Rate currencies
            try {
                  queryListView = (QueryListView) EJBObjectFactory
                              .createClientEJB(formMgr.getServerLocation(),
                                          "QueryListView");

                  // Retrieve the user's org oid from the session for substitution in the
                  // query to retrieve the selectable FX rate currencies
                  userOrgOid = userSession.getOwnerOrgOid();

                  // Get list of already existing FX Rates for this org
                  sqlQuery.append("select currency_code");
                  sqlQuery.append(" from fx_rate");
                  sqlQuery.append(" where p_owner_org_oid = ?"); //AAlubala - Rel7000 CR610 - Modify to use p_owner_org_oid
                
                  queryListView.setSQL(sqlQuery.toString(),new Object[]{userOrgOid});
                  queryListView.getRecords();

                  // Loop through the result set, adding each of the currency codes to the list of codes
                  // that should be excluded from the dropdown
                  for (int i = 0; i < queryListView.getRecordCount(); i++) {
                        queryListView.scrollToRow(i);
                        String alreadyExistingCode = queryListView
                                    .getRecordValue("CURRENCY_CODE");
                        codesToExclude.addElement(alreadyExistingCode);
                  }

                  // Always exclude the user's org's base currency from the dropdown
                  codesToExclude
                              .addElement(userSession.getBaseCurrencyCode());
            } catch (Exception e) {
                  e.printStackTrace();
            } finally {
                  try {
                        if (queryListView != null) {
                              queryListView.remove();
                        }
                  } catch (Exception e) {
                        System.out
                                    .println("Error removing querylistview in FXRateDetail.jsp!");
                  }
            }
      } else {
            // Retrieve the FX Rate currency description from the fx rate that was selected
            currencyCode = fxRate.getAttribute("currency_code");
            multiplyIndicator = fxRate.getAttribute("multiply_indicator");

            fxRateCurrency = ReferenceDataManager.getRefDataMgr().getDescr(
                        "CURRENCY_CODE", currencyCode, userLocale);
      }
%>

<%-- ********************* HTML for page begins here *********************  --%>

<%
      if (insertModeFlag) {
            //focusField = "FxRateCurrencyCode";
      } else if (!isReadOnly) {
            focusField = "DivideRadio";
      }

      String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO
                  : TradePortalConstants.INDICATOR_YES;
%>
<%
String helpUrl = "";
if (insertModeFlag) {
helpUrl = "customer/fx_rate_detail.htm";
}else{
helpUrl = "customer/editing_fx_rate.htm";
}
%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
  <jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="FXRatesDetail.ForeignExchangeRate" />
      <jsp:param name="helpUrl" value="<%=helpUrl%>" />
    </jsp:include> 

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (insertModeFlag) {
    subHeaderTitle = resMgr.getText("FXRatesDetail.NewForeignExchangeRate", TradePortalConstants.TEXT_BUNDLE);
  } 
  else {
    subHeaderTitle = resMgr.getText("FXRatesDetail.ForeignExchangeRate", TradePortalConstants.TEXT_BUNDLE) + " - " + fxRateCurrency;
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>


            <form name="FxRateDetailForm" method="POST" id="FxRateDetailForm"
                  data-dojo-type="dijit.form.Form"
                  action="<%=formMgr.getSubmitAction(response)%>">
                  <input type=hidden value="" name=buttonName>


                  <%--AAlubala IR#IAUL050662274 save the org's base currency --%>
                  <%
                        String bCurrency = "";
                        //if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE)){
                        if (insertModeFlag
                                    && userSession.getOwnershipLevel().equals(
                                                TradePortalConstants.OWNER_CORPORATE)) {//BSL IR# BIUL050954395 - use base currency from user session in Insert Mode only
                              bCurrency = userSession.getBaseCurrencyCode();
                        } else {
                              bCurrency = fxRate.getAttribute("base_currency_code");
                        }
                  %>
                  <input type=hidden value="<%=InstrumentServices.isBlank(bCurrency) ? "" : bCurrency%>" name="OrgBaseCurrency"> 
                  <%--END IR#IAUL050662274 --%>
                  <%
                        // Retrieve the text to use in the error message and audit log
                        updateText.append(resMgr.getText("error.FxRate",
                                    TradePortalConstants.TEXT_BUNDLE));
                        updateText.append(": ");

                        if (!insertModeFlag) {
                              updateText.append(fxRateCurrency);
                        }

                        // Store the FX rate oid (if there is one) and other necessary info in a secure hashtable for the form
   						secureParms.put("FxRateOid",             EncryptDecrypt.encryptStringUsingTripleDes(fxRateOid, userSession.getSecretKey()));
   						secureParms.put("FxRateOwnerOrgOid", EncryptDecrypt.encryptStringUsingTripleDes(userSession.getOwnerOrgOid(), userSession.getSecretKey()));
                        secureParms.put("FxRateCurrencyDescr", updateText.toString());
                        secureParms.put("UserOid", userSession.getUserOid());
                        secureParms.put("MaxDealAmount", fxRate.getAttribute("max_deal_amount"));
                        secureParms.put("MaxSpotRateAmount", fxRate.getAttribute("max_spot_rate_amount"));
                        secureParms.put("FXBaseCurrency", fxRate.getAttribute("base_currency_code"));
                        secureParms.put("login_rights", userSecurityRights);


                        if (!insertModeFlag) {
                              secureParms.put("FxRateCurrencyCode",
                                          fxRate.getAttribute("currency_code"));
                              secureParms
                                          .put("FxRateLockId", fxRate.getAttribute("opt_lock"));
                              secureParms.put("FxRateLastModifiedDate",
                                          fxRate.getAttribute("last_updated_date"));
                        }
                  %>

                  <%=formMgr.getFormInstanceAsInputField("FxRateDetailForm",
                              secureParms)%>

                  <div class="formArea">
                    <jsp:include page="/common/ErrorSection.jsp" />

                  <div class="formContent">
					<div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
						<div class="dijitTitlePaneContentInner">



<% String baseCurrencyDesc = ReferenceDataManager.getRefDataMgr().getDescr("CURRENCY_CODE", bCurrency,userSession.getUserLocale());
%>
<%if (insertModeFlag) { %>
	<%=widgetFactory.createTextField("BaseCurrencyCode","FXRatesDetail.BaseCurrencyLabel",InstrumentServices.isBlank(bCurrency) ? "" : bCurrency,"22",true, false, false,"","","inline")%>
	
<% 
}else{	
	if (errorFlag) {
	 if (!(maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY))>=0)){
		 bCurrency = userSession.getBaseCurrencyCode();
	 } 
	}
%>
	  <%-- <div class="formItem"><%= resMgr.getText("RefDataFXRate.BaseCurrencyCode", TradePortalConstants.TEXT_BUNDLE) %></div> --%>
	  <%-- <% String baseCurrencyDesc = ReferenceDataManager.getRefDataMgr().getDescr("CURRENCY_CODE", bCurrency, userSession.getUserLocale());  %>--%> 	  
	<%=widgetFactory.createTextField("BaseCurrencyCode","FXRatesDetail.BaseCurrencyLabel",InstrumentServices.isBlank(bCurrency) ? "" : bCurrency,"22",true, false, false,"","","inline")%>
	
	<% if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType())) {%>
		<%=widgetFactory.createTextField("MaxSpotRateAmount","FXRatesDetail.MaxSpotRateAmount", TPCurrencyUtility.getDisplayAmount(fxRate.getAttribute("max_spot_rate_amount"),bCurrency, userLocale), "19", false,false, false, "disabled", "", "inline")%>
		<%=widgetFactory.createTextField("MaxDealAmount","FXRatesDetail.MaxDealAmount", TPCurrencyUtility.getDisplayAmount(fxRate.getAttribute("max_deal_amount"),fxRate.getAttribute("currency_code"),userLocale), "19", false, false, false,"disabled", "", "inline")%>
	<%} %>
<%} %>
<div style="clear: both"></div>
<%
if (insertModeFlag) {
// If we're returning to the page with errors, get the previously selected currency code if one exists
      // KMehta - 10 Sep 2014 - Rel9.1 IR-T36000031902 - Change  - Begin
	if (errorFlag) {
		currencyCode = fxRate.getAttribute("currency_code");
		 if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY))>=0) {
		        // We've returned from a save/update/delete but have errors.
		        // We will display data from the cache.  Determine if we're
		        // in insertMode by looking for a '0' oid in the input section
		        // of the doc. 
			 currencyVisibleFlag = true;
		     dropdownOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE,currencyCode, resMgr.getResourceLocale(),codesToExclude);
             defaultText = "";		        
		 }else{
			 currencyVisibleFlag = false;
			 currencyCode = fxRate.getAttribute("currency_code");
	         multiplyIndicator = fxRate.getAttribute("multiply_indicator");

	         fxRateCurrency = ReferenceDataManager.getRefDataMgr().getDescr(
	                        "CURRENCY_CODE", currencyCode, userLocale);
			// We've returned from a save/update/delete that was successful	
			 dropdownOptions = "<option value='' selected>" + fxRateCurrency + "</option>";
			 defaultText = "";
		 }
      } else {
            // Create the list of currencies that the user may choose from to create the FX rate
            currencyVisibleFlag = true;
            dropdownOptions = Dropdown.createSortedRefDataOptions(
                        TradePortalConstants.CURRENCY_CODE, null,
                        resMgr.getResourceLocale(), codesToExclude);

            defaultText = widgetFactory.createLabel("FXRatesDetail.SelectCurrency","FXRatesDetail.SelectCurrency", isReadOnly, false,false, "");
	  		}
     } else {
    	      currencyVisibleFlag = false;
    	      dropdownOptions = "<option value='' selected>" + fxRateCurrency + "</option>";
            }

            // Create the currency dropdown box; note that the insert mode flag is used to determine whether
            // simple text is returned or the dropdown box is generated. This is because the user may never
            // update the currency code for an existing FX rate; an FX rate can only be created or deleted,
            // regardless of whether the user is in read-only mode or not.

      if(currencyVisibleFlag){%>
            <%=widgetFactory.createSelectField("FxRateCurrencyCode","FXRatesDetail.ConvertfromCurrency", defaultText,dropdownOptions, !insertModeFlag, true, false, "class='char25'", "placeHolder:'<Select a Currency>'", "")%>
      <% } %>

<div style="clear: both"></div>
<% if (!currencyVisibleFlag) { %>                       
<div style="padding-left:6px">
    	<%=widgetFactory.createWideSubsectionHeader("FXRatesDetail.ConverttoBaseCurrencyFrom",!insertModeFlag, false, false, "")%>
</div>
<div>
        	<%=widgetFactory.createSelectField("FxRateCurrencyCode","FXRatesDetail.Currency", defaultText,dropdownOptions, !currencyVisibleFlag, false, false, "class='char25'", "", "inline")%>
             <%if (!("".equals(fxRate.getAttribute("last_updated_date")))){ %>
             <%=widgetFactory.createDateField("last_updated_date","FXRatesDetail.LastModified",fxRate.getAttribute("last_updated_date"),!currencyVisibleFlag, false, false, "", "", "inline")%>
			<%} %>
</div> 
<div style="clear: both"></div>            
<%   }  
      // KMehta - 10 Sep 2014 - Rel9.1 IR-T36000031902 - Change  - End
%>

<%  if (insertModeFlag) { %>
<div class="formItem">
	<%= widgetFactory.createNote("FXRatesDetail.CurrencyNote1")%><br/>
	<%= widgetFactory.createNote("FXRatesDetail.CurrencyNote2")%>
		<a href="<%=formMgr.getLinkAsUrl("editAllFXRates", response)%>"> 
			<i><%= resMgr.getText("FXRatesDetail.CurrencyNoteHyperlink", TradePortalConstants.TEXT_BUNDLE) %></i>
        </a>
    <%=widgetFactory.createNote("FXRatesDetail.CurrencyNote3","none") %>
</div>        
<%} %>
<%
	boolean isSelected = true;
	if(fxRate.getAttribute("multiply_indicator").equals(TradePortalConstants.MULTIPLY)){
	  isSelected = false;
	} %>
<div class="formItem">	
<table>
	<tr>		
        <td colspan=2><span class="asterisk">*</span><%= widgetFactory.createLabel("FXRatesDetail.CalculationMethod","FXRatesDetail.CalculationMethod",isReadOnly, false, false,"none")%></td>
        <td><span class="formItem"></span><%= widgetFactory.createLabel("BuyRate","FXRatesDetail.BuyRate",isReadOnly, false, false,"none")%></td>
        <td><span class="formItem"></span><%= widgetFactory.createLabel("MidRate","FXRatesDetail.MidRate",isReadOnly, false, false,"none")%></td>
        <td><span class="formItem"></span><%= widgetFactory.createLabel("SellRate","FXRatesDetail.SellRate",isReadOnly, false, false,"none")%></td>
        <td><span class="formItem"></span><%= widgetFactory.createLabel("fxRateGroup","CorpCust.fxRateGroup",isReadOnly, false, false,"none")%></td>
    </tr>
    <tr>		
        <td valign="top"><%=widgetFactory.createRadioButtonField("FxRateMultiplyIndicator","DivideRadio","CrossRateCalculationRuleDetail.Divide",TradePortalConstants.DIVIDE,isSelected,isReadOnly)%></td>
        <td valign="top"><%=widgetFactory.createRadioButtonField("FxRateMultiplyIndicator","MultiplyRadio","CrossRateCalculationRuleDetail.Multiply",TradePortalConstants.MULTIPLY,fxRate.getAttribute("multiply_indicator").equals(TradePortalConstants.MULTIPLY),isReadOnly)%></td>
        <%  String displayRate;
                 // Don't format the amount if we are reading data from the doc
                 if (isReadOnly)
                       displayRate = TPCurrencyUtility.getDisplayAmount(
                                   fxRate.getAttribute("buy_rate"), "", userLocale);
                 else
                       displayRate = fxRate.getAttribute("buy_rate");
             %> 
        <td><%=widgetFactory.createTextField("FxRateBuyRate","", displayRate, "14",isReadOnly, false,false, "class='char10'",regExpRange,"inline")%></td>
        <%
			// Don't format the amount if we are reading data from the doc
			if (isReadOnly)
			      displayRate = TPCurrencyUtility.getDisplayAmount(
			                  fxRate.getAttribute("rate"), "", userLocale);
			else
			      displayRate = fxRate.getAttribute("rate");
			%>
        <td><%=widgetFactory.createTextField("FxRateRate","", displayRate, "14", isReadOnly, true,false, "class='char10'", regExpRange,"inline")%></td>
            <%
				// Don't format the amount if we are reading data from the doc
				if (isReadOnly)
				      displayRate = TPCurrencyUtility.getDisplayAmount(
				                  fxRate.getAttribute("sell_rate"), "", userLocale);
				else
				      displayRate = fxRate.getAttribute("sell_rate");
				%>				
				
		<td><%=widgetFactory.createTextField("FxRateSellRate","", displayRate, "14", isReadOnly, false,false, "class='char10'", regExpRange, "inline")%></td>
			<% displayRate = fxRate.getAttribute("fx_rate_group");%>
         <td><%=widgetFactory.createTextField("FXRateGroup","", displayRate, "3", isReadOnly,false, false, "class='char5'", "", "inline")%></td>
 	
    </tr>
</table> 
</div>   
 </div>
</div>
             </div><%--formContent--%>
</div><%--formArea--%>


                  <div class="formSidebar" data-dojo-type="widget.FormSidebar"
                        data-dojo-props="form: 'FxRateDetailForm'">
                        <jsp:include page="/common/RefDataSidebar.jsp">
                              <jsp:param name="showSaveButton" value="<%= showSaveButton %>" />
                              <jsp:param name="showSaveCloseButton" value="<%= showSaveCloseButton %>" />
                              <jsp:param name="showDeleteButton"  value="<%= showDeleteButton %>" />
                              <jsp:param name="cancelAction" value="goToRefDataHome" />
                              <jsp:param name="showHelpButton" value="false" />
                              <jsp:param name="helpLink"
                                    value="customer/thresh_groups_detail.htm" />
                                    <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
                       <jsp:param name="error" value="<%= error%>" />

                        </jsp:include>
                        <%= formMgr.getFormInstanceAsInputField("FxRateDetailForm",secureParms) %>
                  </div>

            </form>


      </div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
</script>
<%
  }
%>

</body>
</html>

<%
      // Unregister the FX rate web bean that was used in this page
      beanMgr.unregisterBean("FXRate");

      // Finally, reset the cached document to eliminate carryover of
      // information to the next visit of this page.
      formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
