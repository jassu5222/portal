<%--
*******************************************************************************
  Srini_D CR-713 Rel8.0 10/10/2011  

  Interest Rate Home

  Description:
     This page is to display interest discount rates for the selected group
	 which will have 30/90/120/180/360 days interest details.
*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*,  com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   //userSession.setGlobalNavigationTab(resMgr.getText("NavigationBar.RefData", TradePortalConstants.TEXT_BUNDLE));
%>

<%!
	String group_id = "";
	String interest_disc_rate_group_oid = "";
	String description = "";
	QueryListView queryListView = null;
%>

<%-- ************** Data retrieval page setup begins here ****************  --%>
<%

    HttpSession theSession    = request.getSession(false);   
    String onLoad = "";
    String formName = "";
    String gridHtml = "";
	String gridLayout = "";	
	//DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
	DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
		
	//Below sql query is to get the Description and group id from database for the selected interested_disc_rate_group_oid
	//jgadela 02/24/2015 - T36000036839 - Fixed Sensitive Data Disclosure issue
	interest_disc_rate_group_oid = request.getParameter("group_id");
	Debug.debug("interest_disc_rate_group_oid- " + interest_disc_rate_group_oid);
	
	if(interest_disc_rate_group_oid!=null){

			DocumentHandler resultDoc = null;
			  try {
				//jgadela R90 IR T36000026319 - SQL INJECTION FIX
				  String sql = "select group_id,description from interest_discount_rate_group where interest_disc_rate_group_oid = ?";
				  Debug.debug(sql.toString());
				//jgadela 02/24/2015 - T36000036839 - Fixed Sensitive Data Disclosure issue
				  String interesDiscRateGrpOid=EncryptDecrypt.decryptStringUsingTripleDes(interest_disc_rate_group_oid, userSession.getSecretKey());
				  resultDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, new Object[]{interesDiscRateGrpOid});
				  if (resultDoc != null) {
					  description = resultDoc.getAttribute("/ResultSetRecord(0)/DESCRIPTION");
					  group_id = resultDoc.getAttribute("/ResultSetRecord(0)/GROUP_ID");
					   Debug.debug("group_id"+group_id+"\tdescription:"+resultDoc.toString());
					   
				}

			  } catch (Exception e) {
				  e.printStackTrace();
			  } finally {
			  }  
      }
		
%>


<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag"
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"
              value="<%=onLoad%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="RefDataInterestGroup.InterestGroup"/>
      <jsp:param name="helpUrl" value="/customer/interest_discount_rates.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";

  String title = resMgr.getText("RefDataInterestGroup.InterestDiscountRate", TradePortalConstants.TEXT_BUNDLE);
  subHeaderTitle = title + " - " + group_id + " - " + description;
  String returnUrl = request.getParameter("returnUrl");
%>
   	
<div class="subHeaderDivider"></div>
<div class="transactionSubHeader">

  <%--cquinton 9/7/2012 added pageSubHeaderItem so it floats left for ie7--%>
<span class="pageSubHeaderItem">
<%
 if (subHeaderTitle!=null) {
%>
    <%= subHeaderTitle %>
<%
  }
%>
  </span>
    <span class="pageSubHeader-right">
        <button data-dojo-type="dijit.form.Button"  name="CloseButton" id="CloseButton" type="button"  >
    	  <%=resMgr.getText("Sidebar.Close", TradePortalConstants.TEXT_BUNDLE)%>
     	 <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	 <%--KMehta - 15 Sep 2014 - Rel9.1 IR-T36000032158 - Change --%>
        openURL("<%=formMgr.getLinkAsUrl("goToRefDataHome", response)%>");
      </script>
    </button>
<%   
    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
%>
    <%=widgetFactory.createHoverHelp("CloseButton","InvoiceDetailCloseHoverText") %>
    
    </span>
  <div style="clear:both;"></div>
</div>							

          
<%-- Below HTML code displays the Interest Rate header with close button--%>
<form method="post" name="<%=formName%>" action="<%=formMgr.getSubmitAction(response)%>">
 
    <div class="formMainHeaderNoSidebar">        
  	</div>
     <div class="formAreaNoSidebar">
  	 <jsp:include page="/common/ErrorSection.jsp" />
     <div class="formContentNoSidebar">

        <%
        //gridHtml =   dgFactory.createDataGrid("InterestRatesDataGridId","InterestRatesDataGrid", null);
		//gridLayout = dgFactory.createGridLayout("InterestRatesDataGrid");
		
		gridHtml =   dGridFactory.createDataGrid("InterestRatesDataGridId","InterestRatesDataGrid", null);
		//KMehta - 15 Sep 2014 - Rel9.1 IR-T36000032158 - Change 
	    gridLayout = dGridFactory.createGridLayout("InterestRatesDataGridId","InterestRatesDataGrid");
        %>

       <%=gridHtml%>
 </div>
 </div>
</form>
</div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
</jsp:include>
<%--KMehta - 15 Sep 2014 - Rel9.1 IR-T36000032158 - Change Begin --%>
<script type="text/javascript">
	require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
     var interestGridLayout = <%=gridLayout%>;
     var initSearchParms = "";
     var intGroupOid = "";
     <%-- Rel 9.2 XSS CID 11316 --%>
     intGroupOid= '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(interest_disc_rate_group_oid))%>';
	 initSearchParms="groupOid="+intGroupOid;
     <%-- //initSearchParms = 'groupOid=<%=interest_disc_rate_group_oid%>'; --%>
    <%--   var InterestRatesDataGridId = createDataGrid("InterestRatesDataGridId", "InterestRatesDataView", interestGridLayout, initSearchParms);  --%>
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InterestRatesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
     var intGridId = "";
    	 intGridId = onDemandGrid.createOnDemandGrid("InterestRatesDataGridId", viewName,interestGridLayout, initSearchParms, '0');

	});
     </script>
<%--KMehta - 15 Sep 2014 - Rel9.1 IR-T36000032158 - Change -End --%>     
</body>
</html>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<script type="text/javascript">
		 function openURL(URL){
			    if (isActive =='Y') {
			    	if (URL != '' && URL.indexOf("javascript:") == -1) {
				        var cTime = (new Date()).getTime();
				        URL = URL + "&cTime=" + cTime;
				        URL = URL + "&prevPage=" + context;
			    	}
			    }
			 document.location.href  = URL;
			 return false;
		 }
	</script>
