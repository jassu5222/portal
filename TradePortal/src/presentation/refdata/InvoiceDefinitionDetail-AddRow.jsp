<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<% 
boolean	isReadOnly		= false;


//Initialize WidgetFactory.
WidgetFactory widgetFactory = new WidgetFactory(resMgr); 

String parmValue = "";
int invUploadTableIndex = 0;

parmValue = request.getParameter("invUploadTableIndex");
//Validation to check Cross Site Scripting
if(parmValue != null){
	parmValue = StringFunction.xssCharsToHtml(parmValue);
}

if ( parmValue != null ) {
	try {
		invUploadTableIndex = (new Integer(parmValue)).intValue();
	}
	catch (Exception ex ) {
	}
}

String bType = parmValue = request.getParameter("bType");

//Validation to check Cross Site Scripting
if(bType != null){
	bType = StringFunction.xssCharsToHtml(bType);
}

%>

<tr>
 	      <td nowrap class="ListText" width="30" align="center">
              <%=widgetFactory.createCheckboxField(bType + invUploadTableIndex + "_req", "",
                            false, isReadOnly, false,  "", "", "none")%>
          </td> 
          <td nowrap width="150">
		     <%=widgetFactory.createTextField(bType + invUploadTableIndex + "_label","","" ,"25",isReadOnly,true,false,"", "", "none") %>
 		  </td>
 		  <td> 			
 			140
 		  </td>
</tr>