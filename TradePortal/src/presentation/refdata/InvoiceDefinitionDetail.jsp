<%--
*******************************************************************************
                       Invoice Definition Detail Page

  Description: This jsp sets up the data being used for the Invoice Upload file Defn.
  The idea is that this jsp will call the jsps that create the 2 tabs (Generan Defn and field order)
  associated with the Invoice upload definition.  This jsp will set up the data that is used in
  the called jsp(s).  The only way a user can get to this page is via Refdata
  home.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval/page setup begins here ****************  --%>

<%

  Debug.debug("*********************** START: Invoice Definition Detail Page **************************");

  String 	invoiceDefOid		= request.getParameter("oid");
  String 	loginLocale 		= userSession.getUserLocale();
  String 	loginTimezone		= userSession.getTimeZone();
  String 	loginRights 		= userSession.getSecurityRights();
  String 	tabPressed		= StringFunction.xssCharsToHtml(request.getParameter("tabPressed"));	// This is a local attribute from the webBean
  String	refDataHome		= "goToRefDataHome";    		//used for the close button and Breadcrumb link
  String    onLoad = "";

  DocumentHandler defaultDoc		= formMgr.getFromDocCache();
  Hashtable 	secureParms		= new Hashtable();
  boolean	hasEditRights		= false;
  boolean	isReadOnly	  = true;
  boolean  insertMode      = false;
  boolean  showDelete      = true;
  boolean  showSave        = true;
  boolean showSaveClose = true;
  boolean  getDataFromDoc		= false;
  boolean  retrieveFromDB       = false;
  String   partName        = "";
  boolean invAssigned = false;
  boolean  showSaveAs        = false;

 final String  general		= TradePortalConstants.INV_GENERAL;  	//Convienence - more readable
 final String  file			= TradePortalConstants.INV_FILE;
 HashSet invSummOptinalVal = new HashSet();
 invSummOptinalVal.add("linked_to_instrument_type");
 invSummOptinalVal.add("payment_date");
 invSummOptinalVal.add("buyer_name");
 invSummOptinalVal.add("buyer_id");
 invSummOptinalVal.add("seller_id");
 invSummOptinalVal.add("seller_name");
 invSummOptinalVal.add("invoice_type");
 invSummOptinalVal.add("pay_method");
 invSummOptinalVal.add("ben_acct_num");
 invSummOptinalVal.add("ben_add1");
 invSummOptinalVal.add("ben_add2");
 invSummOptinalVal.add("ben_add3");
 invSummOptinalVal.add("ben_email_addr");
 invSummOptinalVal.add("ben_country");
 invSummOptinalVal.add("ben_bank_name");
 invSummOptinalVal.add("ben_bank_sort_code");
 invSummOptinalVal.add("ben_branch_code");
 invSummOptinalVal.add("ben_branch_add1");
 invSummOptinalVal.add("ben_branch_add2");
 invSummOptinalVal.add("ben_bank_city");
 invSummOptinalVal.add("ben_bank_province");
 invSummOptinalVal.add("ben_branch_country");
 invSummOptinalVal.add("charges");
 invSummOptinalVal.add("central_bank_rep1");
 invSummOptinalVal.add("central_bank_rep2");
 invSummOptinalVal.add("central_bank_rep3");
 invSummOptinalVal.add("reporting_code_1");
 invSummOptinalVal.add("reporting_code_2");
 invSummOptinalVal.add("credit_note");
 invSummOptinalVal.add("send_to_supplier_date");
 invSummOptinalVal.add("buyer_acct_currency");
 invSummOptinalVal.add("buyer_acct_num");
 invSummOptinalVal.add("end_to_end_id");
 
 String maxError = defaultDoc.getAttribute("/Error/maxerrorseverity");
 Vector error = null;
 error = defaultDoc.getFragments("/Error/errorlist/error");
  //WebBeans to access data
  InvoiceDefinitionWebBean invoiceDef = beanMgr.createBean(InvoiceDefinitionWebBean.class,   "InvoiceDefinition");
//Initialize WidgetFactory.
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

   // if user is ADMIN and having maintin invoice definition rights, then only create invoice button shoudl be visible
    String userSecurityType1 = "";
    if (userSession.getSavedUserSession() == null) {
  		userSecurityType1 = userSession.getSecurityType();
   }else {
	    userSecurityType1 = userSession.getSavedUserSessionSecurityType();
        }
   if(userSecurityType1.equals(TradePortalConstants.ADMIN) && SecurityAccess.hasRights(loginRights,
              SecurityAccess.MAINTAIN_INVOICE_DEFINITION)){
	  hasEditRights    = true;
	}
    else {
	  hasEditRights   = false;
     }

  if( hasEditRights ) 			//If the user has the right to Maintain/edit data SET Readonly to False.
	isReadOnly = false;

 //SHR
 if (defaultDoc.getDocumentNode("/In/InvoiceDefinition") == null ) {
	     // The document does not exist in the cache.  We are entering the page from
	     // the listview page (meaning we're opening an existing item or creating
	     // a new one.)
  
     if(invoiceDefOid != null)
      {
    	 invoiceDefOid = EncryptDecrypt.decryptStringUsingTripleDes(invoiceDefOid,userSession.getSecretKey());
      }
     else
      {
    	 invoiceDefOid = null;
      }

     if (invoiceDefOid == null) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
       invoiceDefOid = "0";
     } else if (invoiceDefOid.equals("0")) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
       insertMode = false;
       showSaveAs = hasEditRights;
     }    
  }else{
	// The doc does exist so check for errors.  If highest severity >=
	     // WARNING, we had errors.
	     
	     invoiceDefOid = defaultDoc.getAttribute("/In/InvoiceDefinition/inv_upload_definition_oid");
	       if(invoiceDefOid == null || "0".equals(invoiceDefOid) || "".equals(invoiceDefOid)){
	    	   invoiceDefOid = defaultDoc.getAttribute("/Out/InvoiceDefinition/inv_upload_definition_oid");
	       }else if(invoiceDefOid != null && !"0".equals(invoiceDefOid) && !"".equals(invoiceDefOid))
	       {
	    	   getDataFromDoc = false;
	    	   insertMode = false;
	           retrieveFromDB = true;
	           showSaveAs = true;
	           showDelete=true;
	       }
	     
	     if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
	       
	    	 getDataFromDoc = false;
	  	   	 insertMode = false;
	         retrieveFromDB = true;
	         showSaveAs = true;
	         showDelete=true;
	       
	       
	         // We've returned from a save/update/delete that was successful
	         // We shouldn't be here.  Forward to the RefDataHome page.
	         // Should never get here: jPylon and Navigation.xml should take care
	         // of this situation.
	      
	     } else {
	    	 //out.println("error");
	            // We've returned from a save/update/delete but have errors.
	            // We will display data from the cache.  Determine if we're
	            // in insertMode by looking for a '0' oid in the input section
	            // of the doc.
	            retrieveFromDB = false;
	            getDataFromDoc = true;
	            String newOid =
	            	defaultDoc.getAttribute("/In/InvoiceDefinition/inv_upload_definition_oid");
	            
	            if (newOid.equals("0")) {
	               insertMode = true;
	               invoiceDefOid = "0";
	            } else {
	               // Not in insert mode, use oid from output doc.
	               insertMode = false;
	               invoiceDefOid = newOid;
	            }
	      }
	 }
 
	  if (retrieveFromDB) {
		     // Attempt to retrieve the item.  It should always work.  Still, we'll
		     // check for a blank oid -- this indicates record not found.  Set to insert
		     // mode and display error.

		     getDataFromDoc = false;

		     invoiceDef.setAttribute("inv_upload_definition_oid", invoiceDefOid);
		     invoiceDef.getDataFromAppServer();

		     if (invoiceDef.getAttribute("inv_upload_definition_oid").equals("")) {
		       insertMode = true;
		       invoiceDefOid = "0";
		       getDataFromDoc = false;
		     } else {
		       insertMode = false;
		     }
		}

	    if (getDataFromDoc) {
		     // Populate the securityProfile bean with the output doc.
		     try {
		    	 invoiceDef.populateFromXmlDoc(defaultDoc.getComponent("/In"));
		     } catch (Exception e) {
		        out.println("Contact Administrator: Unable to get Purchase Order Definition "
		             + "attributes for oid: " + invoiceDefOid + " " + e.toString());
		     }
		 }
 //SHR
	 //T36000031630 Start
	 if(InstrumentServices.isNotBlank(invoiceDefOid) && !"0".equals(invoiceDefOid)){
		  showSaveAs= hasEditRights;
		  StringBuffer       whereClause              = new StringBuffer();
		  whereClause.append("a_upload_definition_oid = ? ");
	
	  if (DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", whereClause.toString(), false, new Object[]{invoiceDefOid}) != 0)
	  {
		  Debug.debug("There is an invoice associated to invDef");
		  invAssigned= true;
	  	}
	  //IR T36000016188 Start
	  whereClause.append(" and a_instrument_oid is not null");
	  if (DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", whereClause.toString(), false, new Object[]{invoiceDefOid}) != 0)
	  {
		  Debug.debug("This invDef is already been used by an invoice which is linked to an instrument");
		 isReadOnly = true;
	  	}
	  //IR T36000016188 End
	  }
	//T36000031630 End

//Now check to see if we came back with errors - other wise we need to display the list.
//-----------Maybe just cache this off and let the header do it's thing
Debug.debug("**** Default Doc == " + defaultDoc.toString() );

  String buttonPressed = defaultDoc.getAttribute("/In/Update/ButtonPressed");

  String existingOrgOid = invoiceDef.getAttribute("owner_org_oid");
  String CurrentOrgOid = userSession.getOwnerOrgOid();

  if(InstrumentServices.isNotBlank( existingOrgOid )) {
      secureParms.put("owner_oid",existingOrgOid);
      if (!CurrentOrgOid.equals(existingOrgOid)) {
         isReadOnly = true;
      }
  } else {
      secureParms.put("owner_oid",userSession.getOwnerOrgOid());
  }

  secureParms.put("oid",invoiceDefOid);
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("login_rights",loginRights);
  secureParms.put("opt_lock", invoiceDef.getAttribute("opt_lock"));

  // Auto save the form when time-out if not readonly.
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  if (isReadOnly || insertMode) showDelete = false;
  if (isReadOnly) showSave = false;
  if (isReadOnly) showSaveClose = false;
  String newInvDefOid = null;
 
       if(insertMode && !isReadOnly)
      {
            showSave = true;
            showSaveClose = true;
            showDelete = false;
      }else if(!insertMode && !isReadOnly){
            showSave = true;
        showSaveClose = true;
        showDelete = true;
      }else if(isReadOnly){
            showSave = false;
        showSaveClose = false;
        showDelete = false;
      }

%>


<%-- ============================== HTML Starts HERE =================================== --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>


<div class="pageMain">		<%-- Page Main Start --%>
  <div class="pageContent">	<%--  Page Content Start --%>

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="InvoiceDefinition.InvoiceDefinition"/>
      <jsp:param name="helpUrl" value="customer/invoice_definition_form.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if( InstrumentServices.isBlank(invoiceDefOid) ||
      invoiceDefOid.equals("0")) {
    subHeaderTitle = resMgr.getText("InvoiceDefinition.NewInvoiceUploadDef",
      TradePortalConstants.TEXT_BUNDLE);
  }
  else {
    subHeaderTitle = invoiceDef.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include> 	

 <form id="InvoiceDefinitionDetailForm" name="InvoiceDefinitionDetailForm" method="post" 
             data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

<input type=hidden value="" name=saveAsFlag>
<input type=hidden value="<%=invoiceDefOid %>" name=oid>
<input type=hidden value="<%=invoiceDefOid %>" name=origOid>
  <div class="formArea">
	<jsp:include page="/common/ErrorSection.jsp" />

     <div class="formContent">

    		<%-- General Title Panel details start --%>
        <%@ include file="fragments/InvoiceDefinitionDetail-General.frag" %>

		<%= widgetFactory.createSectionHeader("3", "InvUploadDefinitionDetail.InvLineItemDetail") %>

			<%@ include file="fragments/InvoiceDefinitionDetail-Line.frag" %>

		</div>
		
 	</div>		<%--  Form Content End --%>
  </div> <%--  Form Area End --%>



     <input type=hidden name ="part_to_validate" value="<%=tabPressed%>">
     <input type="hidden" name="buttonName" value="">
     <input type=hidden name ="reqInvSummaryCount" value="<%=invoiceDef.getReqInvSummaryCount()%>">
     <input type=hidden name ="reqInvGoodsCount" value="<%=invoiceDef.getReqInvGoodsCount()%>">
     <input type=hidden name ="reqInvLineitemCount" value="<%=invoiceDef.getReqInvLineitemCount()%>">
<%

  Debug.debug("*********************** END: Invoice Upload Definition Detail Page **************************");
%>

   <%= formMgr.getFormInstanceAsInputField("InvoiceDefinitionDetailForm", secureParms) %>
<%
	defaultDoc.removeAllChildren("/Error");
    formMgr.storeInDocCache("default.doc", defaultDoc);
%>

  <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'InvoiceDefinitionDetailForm'">

    	  <jsp:include page="/common/RefDataSidebar.jsp">
    		<jsp:param name="showSaveButton" value="<%= showSave %>" />
    		<jsp:param name="showDeleteButton" value="<%= showDelete%>" />
    		<jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
    		<jsp:param name="showSaveAsButton" value="<%=showSaveAs%>" />
   			<jsp:param name="saveCloseOnClick" value="none" />
    		<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
    		<jsp:param name="error" value="<%= error%>" />
    		<jsp:param name="saveAsDialogPageName" value="InvoiceDefinitionSaveAs.jsp" />
      		<jsp:param name="saveAsDialogId" value="saveAsDialogId" />
    		<jsp:param name="cancelAction" value="goToRefDataHome" />
    		<jsp:param name="showHelpButton" value="false" />
    		<jsp:param name="helpLink" value="customer/thresh_groups_detail.htm" />
    		<jsp:param name="showLinks" value="true" />
    		<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
    		<jsp:param name="error" value="<%=error%>" />
    	</jsp:include>
  </div>

 </form>
</div>		<%--  Page Content End --%>
</div>		<%--  Page Main End --%>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/common/RefDataSidebarFooter.jsp" />
  
<div id="saveAsDialogId" style="display: none;"></div>
<script type="text/javascript">

var local = {};

require(["dojo/_base/array", "dojo/dom",
         "dojo/dom-construct", "dojo/dom-class", 
         "dojo/dom-style", "dojo/query",
         "dijit/registry", "dojo/on", 
         "t360/common",
         "dojo/ready", "dojo/NodeList-traverse"],
    function(arrayUtil, dom,
             domConstruct, domClass,
             domStyle, query,
             registry, on, 
             common, 
             ready ) {
	
	preSaveAndClose =function (){
 	 	 
	 	  document.forms[0].buttonName.value = "SaveAndClose";
	 	  return true;

		}

	submitSaveAs =function () 
	{
       if(window.document.saveAsForm.newProfileName.value == ""){
         return false;
       }
         document.InvoiceDefinitionDetailForm.name.value=window.document.saveAsForm.newProfileName.value
         <%--  Sets a hidden field with the 'button' that was pressed.  This
         value is passed along to the mediator to determine what
         action should be done.  --%>
         document.InvoiceDefinitionDetailForm.saveAsFlag.value = "Y";    
         document.InvoiceDefinitionDetailForm.oid.value = "0";
         document.InvoiceDefinitionDetailForm.buttonName.value = "SaveAndClose";
         document.InvoiceDefinitionDetailForm.submit();
         hideDialog("saveAsDialogId");             
 } 

	local.checkInvLabelLabelVal =function (labelVal, reqCheck, errorText) {
	      if(labelVal.replace(/^\s+|\s+$/g, '') == ""){	
	    	   alert( ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("UserDefLabel.Required1", TradePortalConstants.TEXT_BUNDLE))%>' + ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("BuyerUserDfn.Required", TradePortalConstants.TEXT_BUNDLE))%>' + ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("UserDefLabel.Required2", TradePortalConstants.TEXT_BUNDLE))%>');
	          var checkedObj = dijit.getEnclosingWidget(reqCheck);
	          checkedObj.set('checked',false);
	          return false;
	       }          
	     return true;
	   }
	local.checkInvSellerLabelVal =function (labelVal, reqCheck, errorText) {
	      if(labelVal.replace(/^\s+|\s+$/g, '') == ""){	
	    	   alert( ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("UserDefLabel.Required1", TradePortalConstants.TEXT_BUNDLE))%>' + ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("SellerUserDfn.Required", TradePortalConstants.TEXT_BUNDLE))%>' + ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("UserDefLabel.Required2", TradePortalConstants.TEXT_BUNDLE))%>');
	          var checkedObj = dijit.getEnclosingWidget(reqCheck);
	          checkedObj.set('checked',false);
	          return false;
	       }          
	     return true;
	   }
	local.checkInvLabelCreditLabelVal =function (labelVal, reqCheck, errorText) {
	      if(labelVal.replace(/^\s+|\s+$/g, '') == ""){	
	    	   alert(  ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("UserDefLabel.Required1", TradePortalConstants.TEXT_BUNDLE))%>' + ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("InvoiceUploadRequest.credit_note", TradePortalConstants.TEXT_BUNDLE))%>' + ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("UserDefLabel.Required2", TradePortalConstants.TEXT_BUNDLE))%>');
	          var checkedObj = dijit.getEnclosingWidget(reqCheck);
	          checkedObj.set('checked',false);
	          return false;
	       }          
	     return true;
	   }
	
	local.checkProdLabelLabelVal =function (labelVal, reqCheck, errorText) {
	      if(labelVal.replace(/^\s+|\s+$/g, '') == ""){	
	    	   alert( ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("UserDefLabel.Required1", TradePortalConstants.TEXT_BUNDLE))%>' + ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("ProdUserDfn.Required", TradePortalConstants.TEXT_BUNDLE))%>' + ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("UserDefLabel.Required2", TradePortalConstants.TEXT_BUNDLE))%>');
	          var checkedObj = dijit.getEnclosingWidget(reqCheck);
	          checkedObj.set('checked',false);
	          return false;
	       }          
	     return true;
	   }
	
	local.checkRequiredVal =function (reqVal, reqCheck) {
		   if(reqVal==false){		        		  
			   alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("InvoiceDefinition.DataReqText", TradePortalConstants.TEXT_BUNDLE))%>');
	          var checkedObj = dijit.getEnclosingWidget(reqCheck);
	          checkedObj.set('checked',false);
	          return false;
	       }          
	   }

	local.checkPayMethodRequiredVal =function (reqVal, reqCheck) {
		   if(reqVal==false){		        		  
			   alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("InvoiceDefinition.PaymentMethodReqText", TradePortalConstants.TEXT_BUNDLE))%>');
	          var checkedObj = dijit.getEnclosingWidget(reqCheck);
	          checkedObj.set('checked',false);
	          return false;
	       }  
		   return true;		   
	   }
	local.checkBankNameOrBranchCodeRequiredVal =function (reqBankNameVal, reqBankBranchCodeVal, reqCheck) {
		   if(reqBankNameVal==false && reqBankBranchCodeVal==false){		        		  
			   alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("InvoiceDefinition.BankNameOrBranchCodeReqText", TradePortalConstants.TEXT_BUNDLE))%>');
	          var checkedObj = dijit.getEnclosingWidget(reqCheck);
	          checkedObj.set('checked',false);
	          return false;
	       }  
		   return true;
	   }
	
	local.checkBankAdd1RequiredVal = function (reqVal, reqCheck) {
		   if(reqVal==false){		        		  
			   alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("InvoiceDefinition.BankAdd1ReqText", TradePortalConstants.TEXT_BUNDLE))%>');
	          var checkedObj = dijit.getEnclosingWidget(reqCheck);
	          checkedObj.set('checked',false);
	          return false;
	       }  
		   return true;
	   }
		 		
	});
	
	


</script>
<script type="text/javascript" src="/portal/js/page/invDefDetail.js"></script>
</body>
</html>
