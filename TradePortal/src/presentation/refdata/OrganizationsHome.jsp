<%--
***************************************************************************************************
Organisations

Description:
      Displays listviews of bank groups, operational bank orgs or
      corporate customers.

Request Parameters:
      orgSelect - Oid of Threshold Group selected from listview
      /Out/ThresholdGroup - ThresholdGroup node if returning from transaction


***************************************************************************************************
--%>

<%-- comments, descriptions --%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*, java.util.*" %>

<jsp:useBean id="beanMgr"  class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<%
      
		 // global constants for this JSP
      final String BANKGROUPS = "bank_groups";
      final String OPORGS = "operational_bank_orgs";
      final String CORPORATE = "corporate_customers";
      final String CLIENTBANKS = "client_banks";
      final String EXTERNALBANKS = "external_banks";

      String current2ndNav = null;
      String newButtonText = "";
      String newLink = null;
      String buttonImageName = "";
    
      boolean showClientBanks = false;
      boolean showBankGroups = false;
      boolean showOperationalBankOrgs = false;
      boolean showCorporateCustomers = false;

      boolean maintainClientBanks = false;
      boolean maintainBankGroups = false;
      boolean maintainOperationalBankOrgs = false;
      boolean maintainCorporateCustomer = false;

      boolean maintainAccess = false;
      boolean viewAccess = false;

      boolean isClientBankUser = false;

      DocumentHandler selectBanksDoc = null;
      String dropdownOptions = null;
      String selectedBankOid = null;
      String BankOid= null;
      String filterText = null;
      StringBuffer searchCriteria = new StringBuffer();
      List bnkgrps= new ArrayList();
      String BankGroupDataView = "";
      String initSearchParms="";
    
   //CR 590 using dgrid instead of datagrid
     DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);

    String gridHtml = "";
    String gridLayout = ""; 
    /* Added for Secondary Nav */
	String titleKey ="";
	String helpUrl="";
	String loginRights = userSession.getSecurityRights();
	String parentOrgID = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
	String bogID = userSession.getBogOid();
	String clientBankID = userSession.getClientBankOid();
	String globalID = userSession.getGlobalOrgOid();
	String ownershipLevel = userSession.getOwnershipLevel();

    WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  	 String gridId = "";

	// set the navigation tab to Organizations
	if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
		userSession.setCurrentPrimaryNavigation("AdminNavigationBar.Organizations");
	}else{
	   userSession.setCurrentPrimaryNavigation("NavigationBar.Organizations");
	}
	
	Map searchQueryMap =  userSession.getSavedSearchQueryData();
	   Map savedSearchQueryData =null;

if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
{
      showClientBanks = SecurityAccess.hasEitherRight(loginRights,
      SecurityAccess.VIEW_CB_BY_PX,SecurityAccess.MAINTAIN_CB_BY_PX );
}
else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK))
{
      isClientBankUser = true;

      showBankGroups = SecurityAccess.hasEitherRight(loginRights,
            SecurityAccess.VIEW_BG_BY_CB, SecurityAccess.MAINTAIN_BG_BY_CB);
      showOperationalBankOrgs = SecurityAccess.hasEitherRight(
            loginRights, SecurityAccess.VIEW_OPBANK_BY_CB,
            SecurityAccess.MAINTAIN_OPBANK_BY_CB);
      showCorporateCustomers = SecurityAccess.hasEitherRight(
            loginRights,SecurityAccess.VIEW_CORP_BY_CB,
            SecurityAccess.MAINTAIN_CORP_BY_CB);
}
else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG))
{
      showCorporateCustomers = SecurityAccess.hasEitherRight(loginRights,
            SecurityAccess.VIEW_CORP_BY_BG, SecurityAccess.MAINTAIN_CORP_BY_BG);
}

//jgadela 10/01/2014 R9.1 T36000032989  -- Vara code task - Encrypt the parameters.
// Encrypt the parameters.
// if clicked as a link on the tab, tihs will specify the tab to use
   if (request.getParameter("orgType") != null) //a type of refdata has been selected
   {
       current2ndNav = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("orgType"), userSession.getSecretKey());
       session.setAttribute("OrgTab", current2ndNav);
   }
   else {
 	// if returning from an organisation detail page,
       // this will specify the current tab used
 	  current2ndNav = (String) session.getAttribute("OrgTab");
   }

      // if current2ndNav is not set, set it according to the rights
if (current2ndNav == null)
{
      if (showClientBanks)
            current2ndNav = CLIENTBANKS;
      else if (showBankGroups)
            current2ndNav = BANKGROUPS;
      else if (showOperationalBankOrgs)
            current2ndNav = OPORGS;
      else if (showCorporateCustomers)
            current2ndNav = CORPORATE;
      else current2ndNav = "";
}

  // Add context to the performance statistic so that we can determine which tab the user is on
  PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

  if((perfStat != null) && (perfStat.isLinkAction()))
        perfStat.setContext(current2ndNav);

// for the selected tab, set up new Button text, where clause, listview xml,
// etc that is unique to each tab


if (current2ndNav.equals(BANKGROUPS))
{
    gridId = "BankGroupDataGridId";

      maintainAccess = SecurityAccess.hasRights(loginRights,
            SecurityAccess.MAINTAIN_BG_BY_CB);

      if (maintainAccess)
      {
            // set create button display name and link
            newButtonText = "AdminBankGroup.CreateNewBankGrp";
            buttonImageName = "common.CreateNewBankGroupImg";
            newLink = formMgr.getLinkAsUrl("selectBankGroup", response);
      }


      if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
 		 savedSearchQueryData = (Map)searchQueryMap.get("AdminUserDataView");
 	}
}
else if (current2ndNav.equals(OPORGS))
{
    gridId = "OperationalBankOrgsGridId";

      maintainAccess = SecurityAccess.hasRights(loginRights,
            SecurityAccess.MAINTAIN_OPBANK_BY_CB);

      if (maintainAccess)
      {
            newButtonText = "AdminOpBankOrg.CreateNewOpBankOrg";
            buttonImageName = "common.CreateNewOperationalBankOrgImg";
            newLink = formMgr.getLinkAsUrl("selectOperationalBankOrg", response);
      }

  
      if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
 		 savedSearchQueryData = (Map)searchQueryMap.get("AdminUserDataView");
 	}
}
else if (current2ndNav.equals(CORPORATE))
{
             if (isClientBankUser) {
      gridId = "CorporateDataGridId";
                  maintainAccess = SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CORP_BY_CB);
                  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
               		 savedSearchQueryData = (Map)searchQueryMap.get("CorporateDataView");
               	}
            } else {
      gridId = "BOGCorporateDataGridId";
                  maintainAccess = SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CORP_BY_BG);
                   if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
               		 savedSearchQueryData = (Map)searchQueryMap.get("BOGCorporateDataView");
               	}
            }
             if (maintainAccess) {
                  newButtonText = "AdminCorpCust.CreateNewCust";
                  buttonImageName = "common.CreateNewCorporateCustomerImg";
                  newLink = formMgr.getLinkAsUrl("selectCorporateCustomer", response);
            }

            selectedBankOid = request.getParameter("BankOid");
            filterText = request.getParameter("FilterText");

            // Now, look at the state for the listview (if it exists) and get the search
            // criteria the user entered.  If we found it, extract the user's values 
            // into the search parms and redisplay on the page.

            if (StringFunction.isNotBlank(selectedBankOid)) {
                  // Store this value in the search criteria
                  searchCriteria.append("BankOid=");
                  searchCriteria.append(selectedBankOid);
                  searchCriteria.append("|");
            }

            if (StringFunction.isNotBlank(filterText)) {
                  // Store this value in the search criteria
                  filterText = filterText.toUpperCase();
                  searchCriteria.append("FilterText=");
                  searchCriteria.append(filterText);
            } 
             /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 Start*/
             if(StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid())){
             	String query="select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ";
	    		DocumentHandler bankGrpList = new DocumentHandler();
	    		List params=new ArrayList();
	    		params.add(userSession.getBankGrpRestrictRuleOid());
	            bankGrpList = DatabaseQueryBean.getXmlResultSet(query,false,params);
	            Vector bgVector = bankGrpList.getFragments("/ResultSetRecord");
	            for (int iLoop=0; iLoop<bgVector.size(); iLoop++)
			    {
			 	     DocumentHandler bankGrpforrestricDoc = (DocumentHandler) bgVector.elementAt(iLoop);
			 	     bnkgrps.add(bankGrpforrestricDoc.getAttribute("/BANK_ORGANIZATION_GROUP_OID"));
				}
             	
             }
             /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 End*/

            // get list of bank groups
            //jgadela R90 IR T36000026319 - SQL INJECTION FIX
            // pgedupudi - Rel-9.3 CR-976A 03/30/2015
            StringBuffer sqlQuery = new StringBuffer();
            sqlQuery.append("select organization_oid, name from bank_organization_group where p_client_bank_oid = ? and activation_status = ? ");
              
			if(StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid())){
				sqlQuery.append(" and organization_oid not in "+ StringFunction.toSQLString(bnkgrps));
			}
			
			sqlQuery.append(" order by "+resMgr.localizeOrderBy("name"));    
			selectBanksDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false,userSession.getClientBankOid(), TradePortalConstants.ACTIVE);

            if (StringFunction.isNotBlank(selectedBankOid)) {
			selectedBankOid = EncryptDecrypt.decryptStringUsingTripleDes(selectedBankOid, userSession.getSecretKey());
                     }
       
      }

else if (current2ndNav.equals(CLIENTBANKS))
{
    gridId = "ClientBankDataGridId";

      maintainAccess = SecurityAccess.hasRights(loginRights,
            SecurityAccess.MAINTAIN_CB_BY_PX);
       newLink = formMgr.getLinkAsUrl("selectClientBank", response);

      if (maintainAccess)
      {
            newButtonText = "AdminClientBank.CreateNewClientBank";
            buttonImageName = "common.CreateNewClientBankImg";
      }
      if(searchQueryMap!=null && !searchQueryMap.isEmpty()){	
 		 savedSearchQueryData = (Map)searchQueryMap.get("ClientBankDataView");
 		}
	}else if (current2ndNav.equals(EXTERNALBANKS))

		{
	
				 maintainAccess = SecurityAccess.hasRights(loginRights,
				            SecurityAccess.MAINTAIN_EXTERNAL_BANK);
				 
				newLink = formMgr.getLinkAsUrl("selectExternalBank", response);
				gridId = "ExternalBankDataGridId";
				
		}

String searchNav = "orgHome."+current2ndNav;  
%>


<%-- Begin HTML - include navigation, etc --%>
<%-- Begin HTML header declarations (this includes the body tag) --%>

<jsp:include page="/common/Header.jsp">
      <jsp:param name="includeNavigationBarFlag"
                     value="<%=TradePortalConstants.INDICATOR_YES%>" />
      <jsp:param name="additionalOnLoad" value="" />
</jsp:include>

<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>
<div class="pageMainNoSidebar">
      <div class="pageContent">
                  <% if(current2ndNav.equals(BANKGROUPS))
                  { 
                        /* Initializing for secondary nav */
                  titleKey="OrganizationsMenu.BankGroups";
                  helpUrl="admin/bank_groups.htm";
                  }
                  %>
                  <% if(current2ndNav.equals(CORPORATE)){
                	  
                   if (isClientBankUser) {
                	  
                	  /* Initializing for secondary nav */
                      titleKey="OrganizationsMenu.CorporateCustomers";
                      helpUrl="admin/corp_customers.htm";
                      
                      } else {
                    /* Initializing for secondary nav */
                    titleKey="OrganizationsMenu.CorporateCustomers";
                    helpUrl="admin/corp_customers.htm";
                         }
                 
                   }
                  %><% if(current2ndNav.equals(CLIENTBANKS))
                  { 
                        /* Initializing for secondary nav */
                  titleKey="OrganizationsMenu.ClientBanks";
                  helpUrl="admin/client_banks.htm";
                  }
                  %><% if(current2ndNav.equals(OPORGS))
                  { 
                        /* Initializing for secondary nav */
                  titleKey="OrganizationsMenu.OperationalBankOrgs";
                  helpUrl="admin/op_bank_orgs.htm";
                  }
                  %><% if(current2ndNav.equals(EXTERNALBANKS))
                  { 
                        /* Initializing for secondary nav */
                  titleKey="OrganizationsMenu.ExternalBanks";
                  helpUrl="admin/ext_bank_list.htm";
                  }%>
                  
                  
                  <jsp:include page="/common/PageHeader.jsp">
                    <jsp:param name="titleKey" value="<%=titleKey %>"/>
                    <jsp:param name="helpUrl"  value="<%=helpUrl %>"/>
                  </jsp:include>

<%-- End HTML header declarations --%>

<%-- depending on where the current tab is selected ... hi-light it --%>

		
<form name="OrganisationAccessHome" method="post"
      action="<%=formMgr.getSubmitAction(response)%>">
     
      
      <input type="hidden" name="NewSearch"  value="<%=TradePortalConstants.INDICATOR_YES%>">      
      
      <jsp:include page="/common/ErrorSection.jsp" />

      <div class="formContentNoSidebar">

  <%-- display listview sort message and create new button message --%>
  <div class="searchHeader">
    <div class="searchHeaderActions">

      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="<%=gridId%>" />
      </jsp:include>

<%
  // if there is maintain access, display the Create button
  if (maintainAccess) {
%>

      <button data-dojo-type="dijit.form.Button" type="button" id="newButton" class="searchHeaderButton" >
        <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-args="evt" data-dojo-event="onClick">
				var newLink = '<%=newLink%>';
        	//alert (newLink);
        	if (isActive =='Y') {
				if (newLink != '' && newLink.indexOf("javascript:") == -1) {
			        var cTime = (new Date()).getTime();
			        newLink = newLink + "&cTime=" + cTime;
			        newLink = newLink + "&prevPage=" + context;
				}
		    }
        	window.location = newLink;
			return true;
        </script>
      </button>	
      <%--cquinton 11/13/2012 add hover help--%>
      <%=widgetFactory.createHoverHelp("newButton","NewHoverText") %>

<%
  }
%>
    </div>
    <div style="clear:both"></div>
  </div>
	
<%
//NSX CR-519  03/09/10 - Begin
  if (current2ndNav.equals(CORPORATE)) {
    // If the user is a global org or client bank user, display the appropriate text and filter dropdown list
   
%>
  <div class="searchDivider"></div>

  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">
      <%=widgetFactory.createSearchTextField("FilterText", "CorpCustSearch.CustomersName", "25","onKeydown='Javascript: filterCorpCustomerOnEnter(\"FilterText\");'") %>

<%
  dropdownOptions = ListBox.createOptionList(selectBanksDoc,
    "ORGANIZATION_OID", "NAME", selectedBankOid, userSession.getSecretKey());
  String defaultText = "  "; 
%>
      <%-- UAT IR T36000006212 Pavani BEGIN --%>
      <%=widgetFactory.createSearchSelectField("BankOid", "CorpCustSearch.CustomersBankGroup", defaultText, dropdownOptions, "class='char40' onChange='local.searchCorporate();'") %>
      <%-- UAT IR T36000006212 Pavani END --%>
    </span>
    <span class="searchActions for2LineLabelCriteria">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchButton" >
        <%= resMgr.getText("PartySearch.Search", TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">local.searchCorporate();</script>
      </button>
      <%--cquinton 11/13/2012 add hover help--%>
      <%=widgetFactory.createHoverHelp("searchButton","SearchHoverText") %>
    </span>
    <div style="clear:both"></div>
  </div>
<%
  }
//NSX CR-519  03/09/10 - End

      // display the listview of the selected tab
      
      
            if (current2ndNav.equals(CLIENTBANKS))
            {
                  //gridHtml = dgFactory.createDataGrid(gridId,"ClientBankDataGrid", null);
                  //gridLayout = dgFactory.createGridLayout(gridId,"ClientBankDataGrid");
              
                  gridHtml = dGridFactory.createDataGrid(gridId,"ClientBankDataGrid",null);
                  gridLayout = dGridFactory.createGridLayout(gridId,"ClientBankDataGrid");
                 
       %>
               <%=gridHtml %>
            
              
<%
      }
%>
<%            if (current2ndNav.equals(BANKGROUPS))
    {
       
          gridHtml = dGridFactory.createDataGrid(gridId,"BankGroupDataGrid", null);
          gridLayout = dGridFactory.createGridLayout(gridId,"BankGroupDataGrid");
%>


            <%=gridHtml %>

<%
}
%>

  <% if(current2ndNav.equals(CORPORATE)){
                	  
                   if (isClientBankUser) {
                	  
                	   gridHtml = dGridFactory.createDataGrid(gridId,"CorporateDataGrid", null);
                       gridLayout = dGridFactory.createGridLayout(gridId,"CorporateDataGrid");
   %>
  <%=gridHtml %>
                  
  <%  } else {                     	 
	   
      gridHtml = dGridFactory.createDataGrid(gridId,"BOGCorporateDataGrid", null);
      gridLayout = dGridFactory.createGridLayout(gridId,"BOGCorporateDataGrid");
   %>             
  <%=gridHtml %>
   <% }}%> 




<%           if (current2ndNav.equals(OPORGS))
    {
           gridHtml = dGridFactory.createDataGrid(gridId,"OperationalBankOrgsDataGrid", null);
          gridLayout = dGridFactory.createGridLayout(gridId,"OperationalBankOrgsDataGrid");
      
%>


            <%=gridHtml %>

<%
}
%>
<%--smanohar  Kyriba PPX268 - start--%>

<%           if (current2ndNav.equals(EXTERNALBANKS))
    {
          gridHtml = dGridFactory.createDataGrid(gridId,"ExternalBankDataGrid", null);
          gridLayout = dGridFactory.createGridLayout(gridId,"ExternalBankDataGrid");
%>


            <%=gridHtml %>

<%
}
%>
<%--smanohar  Kyriba PPX268 - end--%>
</div>
</form>

</div>
</div>

<%--cquinton Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton Portal Refresh - end--%>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>
<script>

  function filterCorpCustomerOnEnter(fieldID){
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	local.searchCorporate();
	        }
	    });
	});	<%-- end require --%>
  }<%-- end of filterCorpCustomerOnEnter() --%>

  var local;
  <%--  KMehta Rel 9400 IR T36000038754 on 07 Sep 2015 Add Start  --%>
  function convert(str) {
		str = str.replace("&#60;", "<");
		str = str.replace("&#62;", ">");
		str = str.replace("&#38;", "&");
		str = str.replace("&#34;", "\"");
		str = str.replace("&#39;", "'");
		str = str.replace("&#40;", "(");
		str = str.replace("&#41;", ")");
		str = str.replace("&#47;", "/");  
		return str;
	} 
  <%--  KMehta Rel 9400 IR T36000038754 on 07 Sep 2015 Add End  --%>

  require(["t360/OnDemandGrid","dojo/dom","dojo/dom-construct", "dijit/registry"], function(onDemandGrid,dom,domConstruct,registry) {
    
    local = {
      initializeDataGrid: function(dataGridId,dataView,encryptedDataView){
        <%--get grid layout from the data grid factory--%>
        var gridLayout = <%=gridLayout%>;
        <%--set the initial search parms--%>
        var initSearchParms = "<%=initSearchParms%>";
        <%-- IR 16481 start --%>
        var savedSort = savedSearchQueryData["sort"];  
       	if("" == savedSort || null == savedSort || "undefined" == savedSort){
            savedSort = '0';
         }
       	
       	if(dataView=='BOGCorporateDataView'){
   		 
       	var filterText = savedSearchQueryData["filterText"];   	
       	if("" == filterText || null == filterText || "undefined" == filterText)
			filterText=(dom.byId("FilterText").value).toUpperCase();

  		 else
  			dom.byId("FilterText").value = filterText;
    		 initSearchParms = initSearchParms+"&filterText="+filterText; 
       	 }
       	 else if(dataView=='CorporateDataView' ){
       		 var selectedClientBankID = savedSearchQueryData["selectedClientBankID"];   
       		 var filterText = savedSearchQueryData["filterText"];   
        		 if("" == selectedClientBankID || null == selectedClientBankID || "undefined" == selectedClientBankID)
        			 selectedClientBankID=dom.byId("BankOid").value;
        		 else
	  				registry.byId("BankOid").set('value',selectedClientBankID);
 	  		  	
        		 <%--  KMehta Rel 9400 IR T36000038754 on 07 Sep 2015  Change - Start --%> 	
        		if("" == filterText || null == filterText || "undefined" == filterText){
        			filterText=(dom.byId("FilterText").value).toUpperCase();

        		}else{        			
        			filterText = convert(filterText);          			
          			dom.byId("FilterText").value = filterText;
        		
          		 }
        		<%--  KMehta Rel 9400 IR T36000038754 on 07 Sep 2015  Change - End --%>
        		 initSearchParms = initSearchParms+"&selectedClientBankID="+selectedClientBankID+"&filterText="+filterText;
       	  
        
        <%-- IR 16481 end --%>
        <%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
        
      }
       	onDemandGrid.createOnDemandGrid(dataGridId, encryptedDataView, gridLayout, initSearchParms,savedSort);
      }
<%
  if (CORPORATE.equals(current2ndNav)) {
%>
      
      ,searchCorporate: function() {
        console.log("searchCorporate()");

<%
    if(isClientBankUser) {
%>
        var selectedClientBankID = registry.byId("BankOid").attr('value');
        var filterText = registry.byId("FilterText").attr('value');

        var searchParms="selectedClientBankID="+selectedClientBankID+"&filterText="+filterText;
       	
        console.log("searchParms="+searchParms);
        onDemandGrid.searchDataGrid("CorporateDataGridId","CorporateDataView",searchParms);
        console.log("Query fired"); 
<%
    } 
    else {
%>
        var filterText = registry.byId("FilterText").attr('value');

        var searchParms="filterText="+filterText;

        console.log("searchParms="+searchParms);
        onDemandGrid.searchDataGrid("BOGCorporateDataGridId", "BOGCorporateDataView",searchParms);
        console.log("Query fired");
<%
    } 
%>
      }
<%
  } %>
  }
  });


  
  require(["t360/OnDemandGrid","dojo/dom-construct","dijit/registry", "dojo/on", "dojo/ready", "dojo/domReady!"], function(onDemandGrid, domConstruct, registry) {
      console.log('page ready');
  <%-- make sure widgets are parsed before using them  --%>
      <% 
        if (BANKGROUPS.equals(current2ndNav)){
      %>
          var gridLayout = <%=gridLayout%>;
          initSearchParms="parentOrgID=<%=parentOrgID%>";
          var savedSort = savedSearchQueryData["sort"];  
         	if("" == savedSort || null == savedSort || "undefined" == savedSort){
              savedSort = '0';
           }
         	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("BankGroupDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
          <%-- t360grid.createDataGrid("BankGroupDataGridId","BankGroupDataView", gridLayout, initSearchParms,savedSort); --%>
          onDemandGrid.createOnDemandGrid("BankGroupDataGridId",viewName, gridLayout, initSearchParms,savedSort);
      <%
        } 
        else if (CORPORATE.equals(current2ndNav)) {
           if(isClientBankUser) {
      %>
         local.initializeDataGrid("<%=gridId%>","CorporateDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("CorporateDataView",userSession.getSecretKey())%>");
      <%
          } 
          else {
      %>
          local.initializeDataGrid("<%=gridId%>","BOGCorporateDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("BOGCorporateDataView",userSession.getSecretKey())%>");
      <%
          }
        } 
        else if (OPORGS.equals(current2ndNav)){
      %>
          local.initializeDataGrid("<%=gridId%>","OperationalBankOrgsDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("OperationalBankOrgsDataView",userSession.getSecretKey())%>");
      <%
        } 
        else if (CLIENTBANKS.equals(current2ndNav)) {
      %>
          local.initializeDataGrid("<%=gridId%>","ClientBankDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("ClientBankDataView",userSession.getSecretKey())%>");
      <%
        } else if (EXTERNALBANKS.equals(current2ndNav)) {
      %>
      	local.initializeDataGrid("<%=gridId%>","ExternalBankDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("ExternalBankDataView",userSession.getSecretKey())%>");
      <%
        }
      %>
      console.log('NewLink='+'<%=newLink%>');

  });

  
</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<%--jgadela 08/27/2014 R 9.1 IR T36000031632  Fixed the scroll bar issue--%>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="<%=gridId%>" />
</jsp:include>

</body>

</html>

<%--  ****** End HTML  ****** --%>


<%-- ******* Clean Up ******* --%>
<%
      // Finally, reset the cached document to eliminate carryover of
      // information to the next visit of this page.
      formMgr.storeInDocCache("default.doc", new DocumentHandler());

      session.setAttribute("OrgTab", current2ndNav);
%>
