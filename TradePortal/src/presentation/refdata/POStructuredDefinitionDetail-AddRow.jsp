<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<% 
boolean	isReadOnly		= false;


//Initialize WidgetFactory.
WidgetFactory widgetFactory = new WidgetFactory(resMgr); 

String parmValue = "";
int poUploadTableIndex = 0;

parmValue = request.getParameter("poUploadTableIndex");

if ( parmValue != null ) {
	try {
		//Validation to check Cross Site Scripting
		parmValue = StringFunction.xssCharsToHtml(parmValue);
		poUploadTableIndex = (new Integer(parmValue)).intValue();
	}
	catch (Exception ex ) {
	}
}

String bType = parmValue = request.getParameter("bType");
if(bType != null){
	bType = StringFunction.xssCharsToHtml(bType);
}
%>

<tr>


 	      <td nowrap width="4">
              <%=widgetFactory.createCheckboxField(bType + poUploadTableIndex + "_req", "",
                            false, isReadOnly, false,  "", "", "none")%>
              </td> 
              <td nowrap width="150">
		<%=widgetFactory.createTextField(bType + poUploadTableIndex + "_label","",
			"" ,"25",
			isReadOnly,true,false,"", "", "none") %>
 		  </td>
 		  <td width="60" align='center'>
 			<p class="ControlLabel">
 			140
 		   </p>
 		  </td>
</tr>