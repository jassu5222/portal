<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<% 
String 	poUploadDefOid		= request.getParameter("oid");
String 	loginLocale 		= userSession.getUserLocale();
String 	loginTimezone		= userSession.getTimeZone();
String 	loginRights 		= userSession.getSecurityRights();
String 	tabPressed		= request.getParameter("tabPressed");	// This is a local attribute from the webBean
String	refDataHome		= "goToRefDataHome";    		//used for the close button and Breadcrumb link
String    onLoad = "";

DocumentHandler defaultDoc		= formMgr.getFromDocCache();
Hashtable 	secureParms		= new Hashtable();
boolean	hasEditRights		= SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_PO_UPLOAD_DEFN );
boolean	isReadOnly		= false;
boolean  insertMode      = true;
boolean  showDelete      = true;
boolean  showSave        = true;
boolean       getDataFromDoc          = false;

final String  general			= TradePortalConstants.PO_GENERAL;  	//Convienence - more readable
final String  file			= TradePortalConstants.PO_FILE;
final String  goods			= TradePortalConstants.PO_GOODS;
final String  checkLength		= TradePortalConstants.BUTTON_CHECK_LINE_LENGTH;

//WebBeans to access data
POUploadDefinitionWebBean poUploadDef = beanMgr.createBean(POUploadDefinitionWebBean.class, "POUploadDefinition");




//Initialize WidgetFactory.
WidgetFactory widgetFactory = new WidgetFactory(resMgr); 

String parmValue = "";
int poUploadTableIndex = 0;

String po_fielddatatype_options= "";		//List of refdata options for Dropdown(s)

parmValue = request.getParameter("poUploadTableIndex");

	
if ( parmValue != null ) {
try {
	poUploadTableIndex = (new Integer(parmValue)).intValue();
}
catch (Exception ex ) {
}
}

  String k = request.getParameter("k");
  //XSS validation
  if(k != null){
		k = StringFunction.xssCharsToHtml(k);
	}
  
%>

<%@ include file="fragments/POUploadDefinitionDetail-AddRow.frag" %>
