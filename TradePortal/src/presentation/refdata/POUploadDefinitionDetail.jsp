

<%--
*******************************************************************************
                       PO Upload Definition Detail Page
  
  Description: This jsp sets up the data being used for the PO Upload Defn.
  The idea is that this jsp will call the jsps that create the 3 tabs associated
  with the PO upload definition.  This jsp will set up the data that is used in 
  the called jsp(s).  The only way a user can get to this page is via Refdata 
  home. 
*******************************************************************************
--%>
 
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval/page setup begins here ****************  --%>

<%

  Debug.debug("*********************** START: PO Upload Definition Detail Page **************************");

  String 	poUploadDefOid		= request.getParameter("oid");
  String 	loginLocale 		= userSession.getUserLocale();
  String 	loginTimezone		= userSession.getTimeZone();
  String 	loginRights 		= userSession.getSecurityRights();
  String 	tabPressed		= request.getParameter("tabPressed");	// This is a local attribute from the webBean
  String	refDataHome		= "goToRefDataHome";    		//used for the close button and Breadcrumb link
  String    onLoad = "";
 
  DocumentHandler defaultDoc		= formMgr.getFromDocCache();
  Hashtable 	secureParms		= new Hashtable();
  boolean	hasEditRights		= SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_PO_UPLOAD_DEFN );
  boolean showCreationRule      = SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_ATP_CREATE_RULES);
  boolean	isReadOnly		= true;
  boolean  insertMode      = true;
  boolean  showDelete      = true;
  boolean  showSave        = true;
  boolean  showSaveClose   = true;
  boolean  getDataFromDoc  = false;
  boolean  retrieveFromDB  = false;

  final String  general			= TradePortalConstants.PO_GENERAL;  	//Convienence - more readable
  final String  file			= TradePortalConstants.PO_FILE;
  final String  goods			= TradePortalConstants.PO_GOODS;
  final String  checkLength		= TradePortalConstants.BUTTON_CHECK_LINE_LENGTH;

  //WebBeans to access data
  POUploadDefinitionWebBean poUploadDef = beanMgr.createBean(POUploadDefinitionWebBean.class, "POUploadDefinition");
   
//Initialize WidgetFactory.
  WidgetFactory widgetFactory = new WidgetFactory(resMgr); 

  String maxError = defaultDoc.getAttribute("/Error/maxerrorseverity");
  Vector error = null;
  error = defaultDoc.getFragments("/Error/errorlist/error");

String links = "";

  if( hasEditRights ) 			//If the user has the right to Maintain/edit data SET Readonly to False.
	isReadOnly = false;
  
  
  
  if (defaultDoc.getDocumentNode("/In/POUploadDefinition") == null ) {
	     // The document does not exist in the cache.  We are entering the page from
	     // the listview page (meaning we're opening an existing item or creating
	     // a new one.)

  if(poUploadDefOid != null)
   {
	  poUploadDefOid = EncryptDecrypt.decryptStringUsingTripleDes(poUploadDefOid,userSession.getSecretKey());
   }
  else
   {
	  poUploadDefOid = null;
   }

  if (poUploadDefOid == null) {
    getDataFromDoc = false;
    retrieveFromDB = false;
    insertMode = true;
    poUploadDefOid = "0";
  } else if (poUploadDefOid.equals("0")) {
    getDataFromDoc = false;
    retrieveFromDB = false;
    insertMode = true;
  } else {
    // We have a good oid so we can retrieve.
    getDataFromDoc = false;
    retrieveFromDB = true;
    insertMode = false;
  }    
}else{
	// The doc does exist so check for errors.  If highest severity >=
	     // WARNING, we had errors.
	     
	     poUploadDefOid = defaultDoc.getAttribute("/In/POUploadDefinition/po_upload_definition_oid");
	       if(poUploadDefOid == null || "0".equals(poUploadDefOid) || "".equals(poUploadDefOid)){
	    	   poUploadDefOid = defaultDoc.getAttribute("/Out/POUploadDefinition/po_upload_definition_oid");
	       }else if(poUploadDefOid != null && !"0".equals(poUploadDefOid) && !"".equals(poUploadDefOid))
	       {
	    	   getDataFromDoc = false;
	    	   insertMode = false;
	           retrieveFromDB = true;
	           showDelete=true;
	       }
	     
	     if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
	       
	    	 getDataFromDoc = false;
	  	   	 insertMode = false;
	         retrieveFromDB = true;
	         showDelete=true;
	       
	       
	         // We've returned from a save/update/delete that was successful
	         // We shouldn't be here.  Forward to the RefDataHome page.
	         // Should never get here: jPylon and Navigation.xml should take care
	         // of this situation.
	      
	     } else {
	    	 //out.println("error");
	            // We've returned from a save/update/delete but have errors.
	            // We will display data from the cache.  Determine if we're
	            // in insertMode by looking for a '0' oid in the input section
	            // of the doc.
	            retrieveFromDB = false;
	            getDataFromDoc = true;
	            String newOid =
	            	defaultDoc.getAttribute("/In/POUploadDefinition/po_upload_definition_oid");
	            
	            if (newOid.equals("0")) {
	               insertMode = true;
	               poUploadDefOid = "0";
	            } else {
	               // Not in insert mode, use oid from output doc.
	               insertMode = false;
	               poUploadDefOid = newOid;
	            }
	      }
	 }


if (retrieveFromDB) {
	     // Attempt to retrieve the item.  It should always work.  Still, we'll
	     // check for a blank oid -- this indicates record not found.  Set to insert
	     // mode and display error.

	     getDataFromDoc = false;

	     poUploadDef.setAttribute("po_upload_definition_oid", poUploadDefOid);
	     poUploadDef.getDataFromAppServer();

	     if (poUploadDef.getAttribute("po_upload_definition_oid").equals("")) {
	       insertMode = true;
	       poUploadDefOid = "0";
	       getDataFromDoc = false;
	     } else {
	       insertMode = false;
	     }
	}

 if (getDataFromDoc) {
	     // Populate the securityProfile bean with the output doc.
	     try {
	    	 poUploadDef.populateFromXmlDoc(defaultDoc.getComponent("/In"));
	     } catch (Exception e) {
	        out.println("Contact Administrator: Unable to get Purchase Order Definition "
	             + "attributes for oid: " + StringFunction.xssCharsToHtml(poUploadDefOid) + " " + e.toString());
	     }
	 }


if(InstrumentServices.isBlank( tabPressed )) {
	tabPressed = "General";
}

String buttonPressed = defaultDoc.getAttribute("/In/Update/ButtonPressed");

// TLE - 01/23/07 - IR#FRUH012138122 - Add Begin 
//secureParms.put("owner_oid",userSession.getOwnerOrgOid());
String existingOrgOid = poUploadDef.getAttribute("owner_org_oid");
String CurrentOrgOid = userSession.getOwnerOrgOid();
String poDefinitionType = poUploadDef.getAttribute("definition_type");

if(InstrumentServices.isNotBlank( existingOrgOid )) {
   secureParms.put("owner_oid",existingOrgOid);
   if (!CurrentOrgOid.equals(existingOrgOid)) {
      isReadOnly = true;
   }
} else {
   secureParms.put("owner_oid",userSession.getOwnerOrgOid()); 
}
// TLE - 01/23/07 - IR#FRUH012138122 - Add End 

secureParms.put("oid",poUploadDefOid);
secureParms.put("login_oid", userSession.getUserOid());
secureParms.put("login_rights",loginRights);
secureParms.put("opt_lock", poUploadDef.getAttribute("opt_lock"));

// Auto save the form when time-out if not readonly.  
// (Header.jsp will check for auto-save setting of the corporate org).
String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
if (isReadOnly || insertMode) showDelete = false;
if (isReadOnly){
	  showSave = false;
	  showSaveClose = false;
	  showCreationRule = false;
}
    
  //Get the poupload option list.  To determine the data item rows.
  String poUploadTableCount = poUploadDef.buildFieldOptionList();
  int tempOtherIndex = 0;
  int NUMBER_OF_OTHER_FIELDS = 4;
  
  Debug.debug("General : poUploadDef.buildFieldOptionList() : "+poUploadTableCount);
  if(null != poUploadTableCount && !"".equalsIgnoreCase(poUploadTableCount)){
	  
	  tempOtherIndex = (poUploadTableCount.split("value=other").length)-1;
	  Debug.debug("General :  tempOtherIndex : "+tempOtherIndex);
	  
	  if( (tempOtherIndex % 4) == 0 && (tempOtherIndex / 4) > 0 ){
		  tempOtherIndex = (tempOtherIndex / 4);
		  NUMBER_OF_OTHER_FIELDS = tempOtherIndex * 4;
	  }else{
	  
		  tempOtherIndex = (tempOtherIndex / 4);
		  
		  if((tempOtherIndex * 4) > 24){
			  NUMBER_OF_OTHER_FIELDS = 24;
		  }else{
			  NUMBER_OF_OTHER_FIELDS = NUMBER_OF_OTHER_FIELDS +(tempOtherIndex * 4);
		  }
	  }
	  Debug.debug("General : NUMBER_OF_OTHER_FIELDS : "+NUMBER_OF_OTHER_FIELDS);
	 
  }

%>


<%-- ============================== HTML Starts HERE =================================== --%>
<%-- <jsp:include page="/common/ButtonPrep.jsp" />  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>



<div class="pageMain">		<%-- Page Main Start --%>
  <div class="pageContent">	<%--  Page Content Start --%>

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="POUploadDefinitionDetail.POUploadDefinition"/>
      <jsp:param name="helpUrl" value="customer/po_definition.htm"/>
    </jsp:include>

<% 
  //cquinton 11/1/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if( InstrumentServices.isBlank(poUploadDefOid) ||  
      poUploadDefOid.equals("0")) {
    subHeaderTitle = resMgr.getText("POUploadDefinitionDetail.NewPODefinition", 
      TradePortalConstants.TEXT_BUNDLE); 
  }
  else {
    subHeaderTitle = poUploadDef.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include> 	


<form id="POUploadDefinitionDetailForm" name="POUploadDefinitionDetailForm" data-dojo-type="dijit.form.Form" method="post" 
		action="<%=formMgr.getSubmitAction(response)%>">
		
		
		
<div class="formArea">
	<jsp:include page="/common/ErrorSection.jsp" />
 

	<div class="formContent">		<%--  Form Content Start --%>
		<%@ include file="fragments/POUploadDefinitionDetail-General.frag" %>  
		
		
		
		<%-- General Title Panel details start --%>
		<%= widgetFactory.createSectionHeader("3", "POUploadDefinitionDetail.FieldOrder") %>
		 
			<%@ include file="fragments/POUploadDefinitionDetail-File.frag" %>
		
		</div>
		<%-- General Title Panel End --%>
		
		<%-- General Title Panel details start --%>
		<%= widgetFactory.createSectionHeader("4", "POUploadDefinitionDetail.GoodsDescription") %>
		
			<%@ include file="fragments/POUploadDefinitionDetail-Goods.frag" %>
		
		</div>
		<%-- General Title Panel End --%>
 	</div>		<%--  Form Content End --%>

</div> <%--  Form Area End --%>

	<input type=hidden name ="part_to_validate" value="<%=TradePortalConstants.PO_DEFINITION_PAGE%>">
        <input type="hidden" name="buttonName" value="">
   
   	
   
<%	    

  Debug.debug("*********************** END: PO Upload Definition Detail Page **************************");
%>

   <%= formMgr.getFormInstanceAsInputField("POUploadDefinitionDetailForm", secureParms) %>
<%
	defaultDoc.removeAllChildren("/Error");
        formMgr.storeInDocCache("default.doc", defaultDoc);
%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'POUploadDefinitionDetailForm'">

      <jsp:include page="/common/RefDataSidebar.jsp">
			<jsp:param name="showSaveButton" value="<%= showSave %>" />
			<jsp:param name="showDeleteButton" value="<%= showDelete%>" />
			<jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
			<jsp:param name="cancelAction" value="goToRefDataHome" />
			<jsp:param name="showHelpButton" value="false" />
			<jsp:param name="helpLink" value="customer/thresh_groups_detail.htm" />
			<jsp:param name="showLinks" value="true" />	
			<jsp:param name="showCreateCreationRule" 		value="<%=showCreationRule%>" />
			<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
			<jsp:param name="error" value="<%= error%>" /> 
	
		</jsp:include>
    
</div> <%--closes sidebar area--%>
<input type=hidden name="tempLineLength" id="tempLineLength" value="<%=poUploadDef.getLineLengthString()%>">
       
</form>

</div>		<%--  Page Content End --%> 
</div>		<%--  Page Main End --%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

<%
  String focusField = "goods_descr_order_1";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          if(focusField){
          	focusField.focus();
          }
        }   
        
        
        document.getElementById('total_line_length').innerHTML = '<%=poUploadDef.getLineLengthString()%>';
        <%-- Few Data Definition details be shown mandatory, based upon poDefinitionType selection --%>
        require(["dojo/dom-class"], function(domClass){
	       <%
	        if(poDefinitionType != null || !"".equals(poDefinitionType)){
	        	if( TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD.equalsIgnoreCase(poDefinitionType) ||
	        			TradePortalConstants.PO_DEFINITION_TYPE_BOTH.equalsIgnoreCase(poDefinitionType) ){        		
	        %>	        		
	        		domClass.add('ben_name_req', 'requiredAstrickWithOutFormItem');
	        		domClass.add('ben_cur_req', 'requiredAstrickWithOutFormItem');
	        		domClass.add('ben_amt_req', 'requiredAstrickWithOutFormItem');
	        <%
	        	}else{
	        	%>
	        		domClass.remove('ben_name_req', 'requiredAstrickWithOutFormItem');
	        		domClass.remove('ben_cur_req', 'requiredAstrickWithOutFormItem');
	        		domClass.remove('ben_amt_req', 'requiredAstrickWithOutFormItem');
	        	<%
	        	}
	        }
	       %>
        });
      });
  });
  
</script>
<%
  }
%>
<script>
function showRequiredData(){
	require(["dijit/registry", "dojo/dom-class"], function(registry, domClass){
	 if(document.getElementById("TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD").checked ||			  
		  document.getElementById("TradePortalConstants.PO_DEFINITION_TYPE_BOTH").checked){
		
		 
	 	domClass.add('ben_name_req', 'requiredAstrickWithOutFormItem');
		domClass.add('ben_cur_req', 'requiredAstrickWithOutFormItem');
		domClass.add('ben_amt_req', 'requiredAstrickWithOutFormItem');
		
		
		
		
		if(document.getElementById("podataformatdivid"))
 			document.getElementById("podataformatdivid").style.display = "block";
		
		
	  }
	  else{
		 
		  domClass.remove('ben_name_req', 'requiredAstrickWithOutFormItem');		  
	 		domClass.remove('ben_cur_req', 'requiredAstrickWithOutFormItem');
	 		domClass.remove('ben_amt_req', 'requiredAstrickWithOutFormItem');
	 		
	 		if(document.getElementById("podataformatdivid"))
	 			document.getElementById("podataformatdivid").style.display = "none";
	 		
	 		
	  }
	});
}

 
</script>
<%-- RD35- Below Added by Jyoti on 2nd Jan 2013 for Po_Text filed visible  --%>
<script type="text/javascript">

require(["dojo/ready","dijit/registry", "dojo/on","dojo/dom","dojo/domReady!"], function(ready,registry, on,dom){
	ready(function(){
		
		var potextfield = registry.byId("po_text_field_name");
		var include_po = registry.byId("include_po_text");
		var include_po_text_div=dom.byId("include_po_text_div");
		visible_Po_TextField(potextfield,include_po,include_po_text_div);<%-- Calling this function for work onload --%>
		on(potextfield, "change", function(){
			visible_Po_TextField(potextfield,include_po,include_po_text_div);<%-- Calling this function for work onChange --%>
		});	
	});
	
	if(<%=poUploadDef.getAttribute("definition_type").equals(TradePortalConstants.PO_DEFINITION_TYPE_MANUAL)%>){
		document.getElementById("podataformatdivid").style.display = "none";
		
	}else{
		document.getElementById("podataformatdivid").style.display = "block";
		
	}
	
	function visible_Po_TextField(potextfield,include_po,include_po_text_div){
		
		if(potextfield.value){
			include_po_text_div.style.visibility="visible";
			include_po.set("disabled",false);
			
		}else{
			include_po.set("disabled",true);
			include_po_text_div.style.visibility="hidden";
		}
	}
});


</script>

</body>
</html>
