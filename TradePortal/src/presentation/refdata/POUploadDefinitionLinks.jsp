<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*,java.math.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session" />

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<%
     // The purpose of this JSP is to build a link that the user will use to navigate
     // between different "tabs" of the PO Upload Definition page.   
     // The link will submit the data in the page on the form.
     
     // The following parameters are read by this page:
     //     selected - indicates if this link represents the tab the user is currently viewing
     //     textResourceKey - the text resource key for the link's label
     //     readOnly  - indicates if the page is in read only mode or not	
     //     tabName  - "General", "File", "Goods", etc.  This becomes the name of the button that is submitted
     //                along with the form
     boolean selected;

     if(request.getParameter("selected").equals("true"))
      {
         selected = true;
      }
     else
      {
         selected = false; 
      }

     String linkLabel = resMgr.getText(request.getParameter("textResourceKey"), TradePortalConstants.TEXT_BUNDLE);
     
     String tabName = request.getParameter("tabName");
  
  
  	 //Validation to check Cross Site Scripting
     if(tabName != null){
    	 tabName = StringFunction.xssCharsToHtml(tabName);
     }
     boolean isReadOnly;

     if(request.getParameter("readOnly").equals("true"))
	{
	   isReadOnly = true;
	}
     else
	{
	   isReadOnly = false;
	}

%>

      <td width="20" nowrap height="30" class="BankColor">&nbsp;</td>
      <td nowrap align="center" <%= selected ? "" : "class=\"BankColor\""%> > 
	  <%
            // If not selected, display as a link
            // otherwise, just show the label

		String link;

		if(isReadOnly)
  		 {
			// If in read only mode, make it a link back to Instrument Navigator, passing
			// the part name
			link = formMgr.getLinkAsUrl("selectPOUploadDefinition", "&tabPressed="+tabName, response);
		 }
		else
		 {
			// If not in read only mode, make the link submit the form
			link = "javascript:document.forms[0].submit()";
		 }

	if(!selected)
             {
        %>
               <p class="Links">
		   <img src="/portal/images/Blank_15.gif" width="15" height="15">
		   <%-- Rel 9.2 XSS CID 11375, 11395, 11489 --%>
 		   <a href="<%=link%>" name="<%=StringFunction.xssCharsToHtml(tabName)%>" class="Links" onClick="setButtonPressed('<%=StringFunction.escapeQuotesforJS(tabName)%>',0);">
			<%= StringFunction.xssCharsToHtml(linkLabel) %>
		   </a>
	  <%	
             }
            else
             {
   	  %>
                <p class="ControlLabel">
		    <img src="/portal/images/Blank_15.gif" width="15" height="15">
	   	    <%= StringFunction.xssCharsToHtml(linkLabel) %>
        <%
             }
        %>
	   <img src="/portal/images/Blank_15.gif" width="15" height="15">
        </p>
      </td>
