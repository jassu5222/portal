
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*, com.amsinc.ecsg.html.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.common.cache.Cache,com.ams.tradeportal.common.cache.TPCacheManager" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   Debug.debug("***START********************PARTY*DETAIL**************************START***");
   //ctq portal refresh comment out
   //userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");

   /****************************************************************
    * THIS SCRIPTLET WILL FIND OUT IF WE ARE EDITING A PARTY
    * CREATING A NEW ONE OR RETURNING FROM AN ERROR FROM A PREVIOUS
    * VERSION OF THIS PAGE.        WE WILL ALSO SET THE PAGE GLOBALS
    * AND POPULATE THE SECURE PARAMETER HASHTABLE THAT WILL BE SENT
    * TO THE FORM MANAGER.
    ****************************************************************/

    /*********\
    * GLOBALS *
    \*********/
   boolean isNew = false; //tells if this is a new party
   boolean isReadOnly = false; //tell if the page is read only
   boolean getDataFromDoc = false;
   boolean showSave = true;
   boolean showDelete = true;
   boolean showSaveButton=true;
   boolean showCloseButton=true;
   boolean showDeleteButton=true;
   boolean showSaveClose=true;
   boolean saveAndCLoseToInstrument=false;
   boolean isUserDefinedFieldEnable = false;
   String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
      String parentOrgID      = userSession.getOwnerOrgOid();
      String bogID            = userSession.getBogOid();
      String clientBankID     = userSession.getClientBankOid();
      String globalID         = userSession.getGlobalOrgOid();
      String ownershipLevel   = userSession.getOwnershipLevel();
      String identifier;
   identifier = "designatedBank";
   String section;
   section = "partydesibank";
   int     iLoop;
   Vector  vVector;
   int     numItems;
   String  sValue;
   String buttonPressed ="";

   String oid = "";
   String loginRights = null;
   String ownerLevel = userSession.getOwnershipLevel();
   String desigBankOid = null;
   String links = "";
   String cancelAction = StringFunction.xssCharsToHtml(request.getParameter("cancelAction"));
   //Validation to check Cross Site Scripting
   if(cancelAction != null){
	   cancelAction = StringFunction.xssCharsToHtml(cancelAction);
   }
   if (cancelAction == null || cancelAction.equals(""))
       cancelAction = "goToRefDataHome";
   else if (!cancelAction.equals("goToRefDataHome")){
       saveAndCLoseToInstrument=true;
       
 }

   if ( TradePortalConstants.OWNER_GLOBAL.equals(ownershipLevel) ) {
	   isUserDefinedFieldEnable = true;
   } else if ( !ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE) ||
           (userSession.hasSavedUserSession() && (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))) ) 
   {
	   Cache CBCache = TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
	   DocumentHandler CBCResult = (DocumentHandler)CBCache.get(clientBankID);
	   String utilizedAddlField = CBCResult.getAttribute("/ResultSetRecord(0)/UTILIZE_ADDITIONAL_BANK_FIELDS");
	   if (TradePortalConstants.INDICATOR_YES.equals(utilizedAddlField)) {
		   isUserDefinedFieldEnable = true;
	   }
   }
 
   //"PartyDetail.DesignatedBank","PartyDetail.SettlementAccounts"
   //Party Web Bean
   beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.PartyWebBean", "Party");
   PartyWebBean editParty = (PartyWebBean)beanMgr.getBean("Party");

   //Corporate Org Web Bean
   beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
   CorporateOrganizationWebBean corpOrg = (CorporateOrganizationWebBean)beanMgr.getBean("CorporateOrganization");

   // Account Web Bean
   AccountWebBean account[] = new AccountWebBean[8];
   for (iLoop=0; iLoop<8; iLoop++)
   {
     account[iLoop] = beanMgr.createBean(AccountWebBean.class, "Account");
   }

   Hashtable secureParams = new Hashtable();

   DocumentHandler doc = null;

   //Get security rights
   loginRights = userSession.getSecurityRights();
   isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_PARTIES);
   Debug.debug("MAINTAIN_PARTIES == " + SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_PARTIES));

   // Get the document from the cache.  We'll use it to repopulate the screen if the
   // user was entering data with errors.
   doc = formMgr.getFromDocCache();
   buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
   Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");


   Debug.debug("doc from cache is " + doc.toString());

   if (doc.getDocumentNode("/In/Party") != null ) {
       // We've returned from a save/update/delete but have errors.
       // We will display data from the cache.  Determine if we're
       // in insertMode by looking for a '0' oid in the input section
       // of the doc.
        oid = doc.getAttribute("/Out/Party/party_oid");
            //if this was an update not an insert, we need to get from in
            if (oid == null ) {
              oid = doc.getAttribute("/In/Party/party_oid");
            }
        String maxError = doc.getAttribute("/Error/maxerrorseverity");
            
        if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
       //no errors, get data from db
            getDataFromDoc = false;
            /* if("SaveTrans".equalsIgnoreCase(buttonPressed)){
               getDataFromDoc=false;
               } */
      }else{
                        getDataFromDoc = true;
            
                   if(doc.getAttribute("/In/Party/party_oid").equals("0"))
                   {
                       Debug.debug("Create Phrase Error");
                       isNew = true;//else it stays false
                   }
            
                   if (!isNew)
                   // Not in insert mode, use oid from input doc.
                   {
                       Debug.debug("Update Phrase Error");
                       oid = doc.getAttribute("/In/Party/party_oid");
                   }
      }
      }

   /* //This next section checks to see if we are coming back from the Party Search page
   desigBankOid = doc.getAttribute("/In/PartySearchInfo/PartyOid");
   if (desigBankOid != null || request.getParameter("useCache") != null)
   {
       getDataFromDoc = true;
       oid = doc.getAttribute("/In/Party/party_oid");
   } */

   if (getDataFromDoc)
   {
       // We create a new docHandler to work around an aparent bug in AMSEntityWebbean
       // This hurts performance and should be removed as soon as populateFromXmlDoc(doc,path) works
       DocumentHandler doc2 = doc.getComponent("/In");
       Debug.debug("Populating party with:\n" + doc2.toString(true));
       editParty.populateFromXmlDoc(doc2);
         
         // Populate account[] bean with account info from document handler
         vVector = doc2.getFragments("/Party/AccountList");
         numItems = vVector.size();
         Debug.debug("Size = "+numItems);

         for (iLoop=0; iLoop<numItems; iLoop++)
         {
           DocumentHandler acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
             try { sValue = acctDoc.getAttribute("/account_oid"); }
             catch (Exception e) { sValue = ""; }
             account[iLoop].setAttribute("account_oid", sValue);
             
             acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
             try { sValue = acctDoc.getAttribute("/account_number"); }
             catch (Exception e) { sValue = ""; }
             account[iLoop].setAttribute("account_number", sValue);
             
             acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
             try { sValue = acctDoc.getAttribute("/currency"); }
             catch (Exception e) { sValue = ""; }
             account[iLoop].setAttribute("currency", sValue);
         }
             
       if (!isNew){
           editParty.setAttribute("party_oid",oid);
           oid = editParty.getAttribute("party_oid");
       }
   }
   else if (request.getParameter("oid") != null || (oid!=null && !"".equals(oid.trim()) && !"0".equals(oid))) //EDIT PARTY
   {
       isNew = false;
       
       if(oid!=null && !"".equals(oid.trim()) && !"0".equals(oid)){
            //We are back in this page from a successful save.
            //Set PartyOid to session, we can use it in instruments page.
    	   session.setAttribute("party_oid",oid);
       }
       else if(request.getParameter("oid")!=null){
       //This getParameter is done twice to prevent overwriting a valid oid
       oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
       Debug.debug("OID -> " + oid);
       }
       editParty.getById(oid);
       Debug.debug("Editing party with oid: " + editParty.getAttribute("party_oid")); //DEBUG
       Debug.debug("isReadOnly == " + isReadOnly);
       //Make sure that a user can only modify parties from his org
       String partyOwnerOid = editParty.getAttribute("owner_org_oid");
       if (!partyOwnerOid.equals(userSession.getOwnerOrgOid()))
           isReadOnly = true;
       Debug.debug("isReadOnly == " + isReadOnly);
         
          // Populate account[] bean with account info from the database
            StringBuffer sql = new StringBuffer();
                        
            QueryListView queryListView = null;
            
            try
            {
              queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
              sql.append("select account_oid, account_number, currency");
              sql.append(" from account");
              sql.append(" where p_owner_oid = ?");
              sql.append(" order by ");
              sql.append(resMgr.localizeOrderBy("account_number"));
              Debug.debug(sql.toString());
            
              queryListView.setSQL(sql.toString(),new Object[]{oid});
              queryListView.getRecords();
            
              DocumentHandler acctList = new DocumentHandler();
              acctList = queryListView.getXmlResultSet();
              Debug.debug(acctList.toString());
            
              vVector = acctList.getFragments("/ResultSetRecord");
              numItems = vVector.size();
            
              for (iLoop=0;iLoop<numItems;iLoop++)
            {
                DocumentHandler acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
            
                try { sValue = acctDoc.getAttribute("/ACCOUNT_OID"); }
                catch (Exception e) { sValue = ""; }
                account[iLoop].setAttribute("account_oid", sValue);
            
                try { sValue = acctDoc.getAttribute("/ACCOUNT_NUMBER"); }
                catch (Exception e) { sValue = ""; }
                account[iLoop].setAttribute("account_number", sValue);
            
                try { sValue = acctDoc.getAttribute("/CURRENCY"); }
                catch (Exception e) { sValue = ""; }
                account[iLoop].setAttribute("currency", sValue);
              }
            }
            catch (Exception e) { e.printStackTrace(); }
            finally {
            try {
              if (queryListView != null) {
                queryListView.remove();
              }
            } catch (Exception e) { System.out.println("Error removing queryListView in PartyDetail.jsp"); }
            }
            
            
   }
   else 
    {
       isNew = true;
       oid = null;
    }

   //Populate the corpOrg Webbean
   if (ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE))
   {
       Debug.debug("owner org " + userSession.getOwnerOrgOid());
       corpOrg.getById(userSession.getOwnerOrgOid());
       Debug.debug("Editing for corporation " + corpOrg.getAttribute("name"));
   }

   //Populate the corpOrg Webbean
   /*We discuss moving this inside the same if statement that the partyType dropdown is in, but we
     can't because this bean needs to be populated in order for the if() to test right.*/
   if (desigBankOid == null || desigBankOid.equals("cancel"))
   {
       Debug.debug("Pulling desigBankOid from bean");
       desigBankOid = editParty.getAttribute("designated_bank_oid");
   }

   // onLoad is set to default the cursor to the partyName field but only if the
   // page is not in isReadOnly mode.
   // Auto save the form when time-out if not readonly.  
   // (Header.jsp will check for auto-save setting of the corporate org).
   String onLoad = "";
   if (!isReadOnly) {
     onLoad = "document.PartyDetail.partyName.focus();";
   }

   String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

   if (isReadOnly)
   {
       showSave = false;
       showSaveButton=false;
       showDelete = false;
       showDeleteButton=false;
       showSaveClose=false;
   }
   else if (isNew)
   {
       oid = "0";
       editParty.setAttribute("party_oid",oid);
       showDelete = false;
       showDeleteButton=false;
   }
%>

<%-- Body tag included as part of common header --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp" />

<script LANGUAGE="JavaScript">

  function clearAddr()
  {
    <%--
    //This Function is called by the "Clear Selected Bank" button.  It clears the designated bank
    //text area and the designated bank oid hidden input field
    --%>
    document.PartyDetail.designatedBank.value='';
    document.PartyDetail.designated_bank_oid.value='';
  }

</script>

<div class="pageMain">
<div class="pageContent">
<jsp:include page="/common/PageHeader.jsp">
  <jsp:param name="titleKey" value="PartyDetail.Parties"/>
  <jsp:param name="helpUrl" value="/customer/party_detail.htm"/>
</jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (!isNew) {
    subHeaderTitle = editParty.getAttribute("name");
  }
  else {
    subHeaderTitle = resMgr.getText("PartyDetail.NewParty",TradePortalConstants.TEXT_BUNDLE);
  }
%>
<jsp:include page="/common/PageSubHeader.jsp">
  <jsp:param name="title" value="<%= subHeaderTitle %>" />
</jsp:include>    

<%
            /*******************
            * START SECUREPARAMS
            ********************/
            secureParams.put("oid",oid);
            secureParams.put("owner_oid",userSession.getOwnerOrgOid());
            secureParams.put("ownership_level",userSession.getOwnershipLevel());

            // Used for the "Added By" column
            if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
                  secureParams.put("ownership_type", TradePortalConstants.OWNER_TYPE_ADMIN);
            else
                  secureParams.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);

            //secureParams.put("designated_bank_oid",desigBankOid);
            secureParams.put("login_oid",userSession.getUserOid());
            secureParams.put("login_rights",loginRights);
            secureParams.put("opt_lock", editParty.getAttribute("opt_lock"));
            secureParams.put("return_to_page", "PartyDetail"); //This tells the party search page what page to return to.
            secureParams.put("searchPartyType",TradePortalConstants.DESIG_BANK); //This tells the party search page what type of parties to show
            secureParams.put("return_action", "selectParty"); //This tells the party search page what page to return to.
            String partyTypeParm= request.getParameter("partyType");
            if(InstrumentServices.isNotBlank(partyTypeParm))
            	secureParams.put("originalSearchPartyType", request.getParameter("partyType"));
            //set Party type to session.
            if(!getDataFromDoc)
            	session.setAttribute("searchPartyType", TradePortalConstants.DESIG_BANK);
            		
            Debug.debug("*******************partyType -> " + (String) secureParams.get("partyType"));
            /*****************
            * END SECUREPARAMS
            ******************/
%>                
                  
<form name="PartyDetail" id="PartyDetail" data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>
<INPUT TYPE="hidden" NAME="cancelAction" id="cancelAction" VALUE='<%= cancelAction %>'>
    <div class="formMainHeader">        
 
      </div>
        <div class="formArea">
        <jsp:include page="/common/ErrorSection.jsp" />
          <div class="formContent">
        <%= widgetFactory.createSectionHeader("1", "PartyDetail.General") %>
        <%=widgetFactory.createWideSubsectionHeader("PartyDetail.Parties")%>
      
        <div class="columnLeft">
        <%
          /********************************
           * START PARTY NAME INPUT FIELD
           ********************************/
           Debug.debug("Party Name Field");
           %>
           <%= widgetFactory.createTextField( "partyName", "PartyDetail.PartyName", editParty.getAttribute("name"), "35", isReadOnly, true, false, "class='char30'","","")%>
          
          <%
          /********************************
           * END PARTY NAME INPUT FIELD
           ********************************/%>
        
         <% if ((!ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE)) ||
                                  corpOrg.getAttribute("allow_create_bank_parties").equals(TradePortalConstants.INDICATOR_YES))
         {
           /********************************
            * START PARTY TYPE DROPDOWN
            ********************************/
           Debug.debug("Building Party Type field - " + editParty.getAttribute("party_type_code"));
           %>
           
           <%
           String options2 = Dropdown.createSortedRefDataOptions("PARTY_TYPE", editParty.getAttribute("party_type_code"), resMgr.getResourceLocale());
           Debug.debug(options2);
           out.print(widgetFactory.createSelectField( "partyType", "PartyDetail.PartyType","", options2, isReadOnly,true, false,"class='char10'", "", ""));
            %>
            <%
           /********************************
            * END PARTY TYPE DROPDOWN
            ********************************/
         }
         else
         {
            secureParams.put("partyType", TradePortalConstants.PARTY_TYPE_CORP);
         }
         %>
      
          <%
          /********************************
           * START VENDOR ID INPUT FIELD
           ********************************/
           Debug.debug("Vendor ID Field");
           %>
          
          <%= widgetFactory.createTextField( "vendorId", "PartyDetail.VendorId", editParty.getAttribute("vendor_id"), "15", isReadOnly, false, false, "class='char10'","","")%>           
          <%
          /********************************
           * END VENDOR ID INPUT FIELD
           ********************************/%>
          <% 
          
          // Get Variables Here
      String     corporateOrgBankTypePartiesInd = corpOrg.getAttribute("allow_create_bank_parties");
      //corpOrg.getById(userSession.getOwnerOrgOid());
      // String  corporateOrgBankTypePartiesInd = corpOrg.getAttribute("allow_shipping_guar");
      boolean customerIDFieldExists = false;
          
      if ( !ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE) ||
              (userSession.hasSavedUserSession() && (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))) ||
              (corporateOrgBankTypePartiesInd.equals(TradePortalConstants.INDICATOR_YES)))
              {
     %>
              
     <%             
             if ( !ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE) ||
                    (userSession.hasSavedUserSession() && (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))))
              {
                  /********************************
                   * START CUSTOMER ID INPUT FIELD
                   ********************************/
                  Debug.debug("Customer ID Field");
     %>
                   
                   <%= widgetFactory.createTextField( "customerId", "PartyDetail.CustomerID", editParty.getAttribute("OTL_customer_id"), "35", isReadOnly, false, false, "class='char30'","","")%>
                    
     <%
                  /********************************
                   * END CUSTOMER ID INPUT FIELD
                   ********************************/
                   customerIDFieldExists = true;
                   }
                  if (!ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE) ||
                      corporateOrgBankTypePartiesInd.equals(TradePortalConstants.INDICATOR_YES))
                  {
                        if (!customerIDFieldExists)
                              {
     %>
                                
     <%
                              }                             
     %>
                    
                   <%= widgetFactory.createTextField( "bankBranchCode", "PartyDetail.BankBranchCode", editParty.getAttribute("branch_code"), "30", isReadOnly, false, false, "class='char30'","","")%>                                         
                    
     <%                 
                  }
                if ( !ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE) ||
                    (userSession.hasSavedUserSession() && (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))))
                  {
     %>                 
                   
     <%                 
                  }
     %>
     <% 
            /********************************
             * END CUSTOMER ID/BRANCH CODE INPUT FIELDs
             ********************************/
             }
     %>                         
     <%       
      if ( !ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE) ||
              (userSession.hasSavedUserSession() && (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))) ||
              (corporateOrgBankTypePartiesInd.equals(TradePortalConstants.INDICATOR_YES)))
              {
     %>
             
     <% 
            /********************************
             * END CUSTOMER ID/BRANCH CODE INPUT FIELDs
             ********************************/
              }
     %>           
     <%-- Added as a fix for IR T36000004424
            Party Form - The seperator line between the right and left sides of the section should go all the way down the section. --%>
  
     
     
     <%-- End of IR T36000004424--%>
     </div>
        <div class="columnRight">
        <%
        /********************************
         * START ADDRESS1 INPUT FIELD
         ********************************/
        Debug.debug("Address Line 1 Field");
        %>
        
        <%= widgetFactory.createTextField( "address1", "PartyDetail.AddressLine1", editParty.getAttribute("address_line_1"), "35", isReadOnly, true, false,"class='char30'","","")%>                                                                  
        <%
        /********************************
         * END ADDRESS1 INPUT FIELD
         ********************************/%>
     
        <%
        /********************************
         * START ADDRESS2 INPUT FIELD
         ********************************/
        Debug.debug("Address Line 2 Field");
        %>
        
         <%= widgetFactory.createTextField( "address2", "PartyDetail.AddressLine2", editParty.getAttribute("address_line_2"), "35", isReadOnly, false, false,"class='char30'","","")%>                                                                 
        <%
        /********************************
         * END ADDRESS2 INPUT FIELD
         ********************************/%>
      
        <%
        /********************************
         * START CITY INPUT FIELD
         ********************************/
        Debug.debug("City Field");
        %>
        <%-- KMehta Rel 8.4 IR-T36000023381 Chg field length from 13 to 23 Start --%>
         <%= widgetFactory.createTextField( "city", "PartyDetail.City", editParty.getAttribute("address_city"), "23", isReadOnly, true, false,"class='char30'","","")%>                                                               
        <%-- KMehta Rel 8.4 IR-T36000023381 End --%>
        <%
        /********************************
         * END CITY INPUT FIELD
         ********************************/%>
      
        <%
        /**********************************
         * START PROVINCE INPUT FIELD
         **********************************/
        Debug.debug("Province/State Field");
        %>
       
        <%= widgetFactory.createTextField( "province", "PartyDetail.ProvinceState", editParty.getAttribute("address_state_province"), "8", isReadOnly, false, false, "style='width:70px'","","inline")%>
        
        <%
        /********************************
         * END PROVINCE INPUT FIELD
         ********************************/%>
        
        <%
        /********************************
         * START POSTAL CODE INPUT FIELD
         ********************************/
        Debug.debug("Postal Code Field");
        %>
        
         <%= widgetFactory.createTextField( "postalCode", "PartyDetail.PostalCode", editParty.getAttribute("address_postal_code"), "8", isReadOnly, false, false,"style='width:70px'","","inline")%>                              
        <%
        /********************************
         * END POSTAL CODE INPUT FIELD
         ********************************/%>
        
      <div style="clear: both"></div>
       
        <%
        /********************************
         * START COUNTRY DROPDOWN BOX
         ********************************/
        Debug.debug("Country Dropdown Box - " + editParty.getAttribute("address_country"));
        %>
        
        <%
        String options3 = Dropdown.createSortedRefDataOptions("COUNTRY", editParty.getAttribute("address_country"),
                                                              resMgr.getResourceLocale());
        //Debug.debug(options3);
        out.print(widgetFactory.createSelectField( "country", "PartyDetail.Country"," ", options3, isReadOnly,true, false,"class='char30'", "", ""));
        /********************************
         * END COUNTRY DROPDOWN BOX
         ********************************/
        %>
        </div>  
        <div style="clear: both"></div>  
        <%-- Nar Rel9500 CR-1132 01/27/2016 Add- Begin --%>       
        <% if ( isUserDefinedFieldEnable ) { %>
            <%=widgetFactory.createWideSubsectionHeader("PartyDetail.PortalOnlyAdditionalFields")%>
            <div class="columnLeft">
                <%= widgetFactory.createTextField( "userDefinedField1", "PartyDetail.UserDefinedField1", editParty.getAttribute("user_defined_field_1"), "20", isReadOnly, false, false, "class='char20'","","")%> 
                <%= widgetFactory.createTextField( "userDefinedField2", "PartyDetail.UserDefinedField2", editParty.getAttribute("user_defined_field_2"), "20", isReadOnly, false, false, "class='char20'","","")%>   
            </div>           
             <div class="columnRight">
                <%= widgetFactory.createTextField( "userDefinedField3", "PartyDetail.UserDefinedField3", editParty.getAttribute("user_defined_field_3"), "20", isReadOnly, false, false, "class='char20'","","")%>  
                <%= widgetFactory.createTextField( "userDefinedField4", "PartyDetail.UserDefinedField4", editParty.getAttribute("user_defined_field_4"), "20", isReadOnly, false, false, "class='char20'","","")%>  
             </div>           
             <div style="clear: both"></div>
         <% } %>
          <%-- Nar Rel9500 CR-1132 01/27/2016 Add- End --%> 
         </div>
         <%= widgetFactory.createSectionHeader("2", "PartyDetail.Communications") %>
         <div class="columnLeft"> 
         <%=widgetFactory.createSubsectionHeader("PartyDetail.ContactParty")%>
        <%
        /****************************************
         * START GENERAL PHONE NUMBER INPUT FIELD
         ****************************************/
        Debug.debug("General Phone Number Field");
        %>
        
         <%= widgetFactory.createTextField( "generalPhoneNumber", "PartyDetail.GeneralPhoneNumber", editParty.getAttribute("phone_number"), "20", isReadOnly, false, false, "class='char30'","","")%>                              
        <%
        /**************************************
         * END GENERAL PHONE NUMBER INPUT FIELD
         **************************************/%>
     
        <%
        /****************************************
         * START SWIFT ADDRESS INPUT FIELD
         ****************************************/
        Debug.debug("Swift Address Field");
        %>
       
        
        <%= widgetFactory.createTextField( "swiftPart1", "PartyDetail.SwiftAddress", editParty.getAttribute("swift_address_part1"), "8", isReadOnly, false, false, "style='width:70px'","","inline")%>                               
       
        <%= widgetFactory.createTextField( "swiftPart2", "&nbsp;", editParty.getAttribute("swift_address_part2"), "3", isReadOnly, false, false,"style='width:30px'","","none")%> 
        <%
        /**************************************
         * END SWIFT ADDRESS INPUT FIELD
         **************************************/%>
       <div style="clear: both"></div>
        <%
        /****************************************
         * START FAX NUMBER 1 INPUT FIELD
         ****************************************/
        Debug.debug("Fax Number 1 Field");
        %>
    
        <%= widgetFactory.createTextField( "faxNumber1", "PartyDetail.FaxNumber1", editParty.getAttribute("fax_1"), "20", isReadOnly, false, false,"class='char30'","","")%>                               
        <%
        /**************************************
         * END FAX NUMBER 1 INPUT FIELD
         **************************************/%>
      
        <%
        /****************************************
         * START FAX NUMBER 2 INPUT FIELD
         ****************************************/
        Debug.debug("Fax Number 2 Field");
        %>
        
        <%= widgetFactory.createTextField( "faxNumber2", "PartyDetail.FaxNumber2", editParty.getAttribute("fax_2"), "20", isReadOnly, false, false,"class='char30'","","")%>                               
        <%
        /**************************************
         * END FAX NUMBER 2 INPUT FIELD
         **************************************/%>
        </div>
        <div class="columnRight">
              
        <%=widgetFactory.createSubsectionHeader("PartyDetail.ContactPerson",isReadOnly,false,false,"")%>
        
        <%
        /******************************************
         * START NAME OF CONTACT PERSON INPUT FIELD
         ******************************************/
        Debug.debug("Name of Contact Person Field");
        %>
        
         <%= widgetFactory.createTextField( "contactName", "PartyDetail.NameofContactPerson", editParty.getAttribute("contact_name"), "35", isReadOnly, false, false,"class='char30'","","")%>                              
        <%
        /****************************************
         * END NAME OF CONTACT PERSON INPUT FIELD
         ****************************************/%>
      
        <%
        /*******************************************
         * START TITLE OF CONTACT PERSON INPUT FIELD
         *******************************************/
        Debug.debug("Title of Contact Person Field");
        %>
        
        <%= widgetFactory.createTextField( "contactTitle", "PartyDetail.TitleofContactPerson", editParty.getAttribute("contact_title"), "35", isReadOnly, false, false, "class='char30'","","")%>                               
        <%
        /*****************************************
         * END TITLE OF CONTACT PERSON INPUT FIELD
         *****************************************/%>
      
        <%
        /*******************************************
         * START CONTACT'S PHONE NUMBER INPUT FIELD
         *******************************************/
        Debug.debug("Contact's Phone Number Field");
        %>
        
         <%= widgetFactory.createTextField( "contactPhone", "PartyDetail.ContactPhoneNumber", editParty.getAttribute("contact_phone"), "20", isReadOnly, false, false,"class='char30'","","")%>                               
        <%
        /****************************************
         * END CONTACT'S PHONE NUMBER INPUT FIELD
         ****************************************/%>
     
        <%
        /*******************************************
         * START CONTACT'S FAX NUMBER INPUT FIELD
         *******************************************/
        Debug.debug("Contact's Fax Number Field");
        %>
        
        <%= widgetFactory.createTextField( "contactFax", "PartyDetail.ContactFaxNumber", editParty.getAttribute("contact_fax"), "20", isReadOnly, false, false,  "class='char30'","","")%>                               
        <%
        /*******************************************
         * END CONTACT'S FAX NUMBER INPUT FIELD
         *******************************************/%>
      
        <%
        /*******************************************
         * START CONTACT'S EMAIL ADDRESS INPUT FIELD
         *******************************************/
        Debug.debug("Contact's Email Address Field");
        %>
        
        <% //jgadela 10/25/2013- Rel 8.3 IR T36000022319 - Corrected the display attribute name - contact_email%>
        <%= widgetFactory.createTextField( "contactEmail", "PartyDetail.ContactEmailAddress", editParty.getAttribute("contact_email"), "50", isReadOnly, false, false,  "class='char30'","","")%>
                                         
        <%
        /*****************************************
         * END CONTACT'S EMAIL ADDRESS INPUT FIELD
         *****************************************/%>
        </div>
        <div style="clear: both"></div>
        </div>
        <%= widgetFactory.createSectionHeader("3", "PartyDetail.DesignatedBank") %>
        <% if (!isReadOnly){%>
         <%= widgetFactory.createLabel("", "PartyDetail.DesignatedBankText", false, false, false, "inline") %>
         <%}else{%>
        <%= widgetFactory.createLabel("", "PartyDetail.DesignatedBankText", false, false, false, "") %>         
         
      <%}%> <div class="formItem">
      <%if(!isReadOnly)%>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   &nbsp;&nbsp;&nbsp;&nbsp;
     <%
         if (!isReadOnly){%>
        <%= widgetFactory.createPartySearchButton(identifier+",designated_bank_oid",section,false,TradePortalConstants.DESIG_BANK,false) %>
         
         <%= widgetFactory.createPartyClearButton("PartyDetail.ClearSelection","clearAddr()",false,"") %>
         <%} %>
        </div>
      
        <%
        /*********************************
         * START DESIGNATED BANK TEXT AREA
         *********************************/
         
        Debug.debug("Building Designated Bank Text field with oid '" + desigBankOid + "'");
        StringBuffer address = new StringBuffer();
        if (desigBankOid != null && !desigBankOid.equals("cancel") && !desigBankOid.equals(""))
        {
            Debug.debug("desigBankOid = '" + desigBankOid + "'");
            /*We're done with the editParty bean, but we'll reuse the bean here for the designated bank party
              to keep from having to register the same type of bean twice*/
            editParty.getById(desigBankOid);
            Debug.debug("Designated bank set to: " + editParty.getAttribute("name"));
            String eol = "\n";
            if (isReadOnly)
                eol = "<br/>";
                address =address.append(editParty.getAttribute("name").trim()).append(eol).append(editParty.getAttribute("address_line_1")).append(eol);
            if (!editParty.getAttribute("address_line_2").equals(""))
                address.append(editParty.getAttribute("address_line_2")).append(eol);
            address.append(editParty.getAttribute("address_city"));
            if (!editParty.getAttribute("address_state_province").equals(""))
                address.append(", ").append(editParty.getAttribute("address_state_province"));
            address.append(" ").append(editParty.getAttribute("address_postal_code")).append(" ");
            address.append(editParty.getAttribute("address_country"));
        }

      %>
      <div class="formItem">
      <%if (isReadOnly)
        {
            if (address != null)
                out.print(StringFunction.xssCharsToHtml(address.toString()));
        }
        else
        {%>
			<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
		 </div>
		<div class="formItemParty">
	 <%--	
         <%= widgetFactory.createTextArea( "designatedBank", "", address.toString(), isReadOnly || false,false, false, "style='width:auto;' rows='4' cols='35'", "","none" ) %>

		
	--%>
	 <%= widgetFactory.createAutoResizeTextArea("designatedBank", "",address.toString(), isReadOnly || false, false,false, "class='autoHeightPartyTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='50'", "", "")%>	
			   
		<%-- Srinivasu_D  T36000034559 Rel9.1 11/13/2015 - encryption added --%>

            <INPUT TYPE="hidden" NAME="designated_bank_oid" id="designated_bank_oid" VALUE='<%= EncryptDecrypt.encryptStringUsingTripleDes(desigBankOid,userSession.getSecretKey()) %>'>
      
       <% }
        /********************************
         * END DESIGNATED BANK TEXT AREA
         ********************************/
        %>
        </div>
        </div>
        
         <%= widgetFactory.createSectionHeader("4", "PartyDetail.SettlementAccounts") %> 
     <div class="columnLeft"> 
     <table><tr><td>&nbsp;</td>
     <td>
     <%= widgetFactory.createLabel("PartyDetail.SettlementAccountNumber","PartyDetail.SettlementAccountNumber", false, false, false,"inline") %>
     </td><td>
     <%= widgetFactory.createLabel("PartyDetail.SettlementAccountCurrency","PartyDetail.SettlementAccountCurrency", false, false,false,"inline") %></tr>
    
<%
      String options;
      String loginLocale = userSession.getUserLocale();
      String currency = null;
      for (iLoop=0; iLoop<4; iLoop++) { 
%>
      
                        <tr>
            <td> <%= widgetFactory.createLabel("acctNumb" + iLoop,"PartyDetail.SettlementAccount" + (iLoop+1), false, false,false,"inline") %></td>
            <td>
             <%= widgetFactory.createTextField( "acctNum"+iLoop, "", account[iLoop].getAttribute("account_number"), "30", isReadOnly, false, false, "class='char20'","","inline")%>
             
             
             </td>
             <td> 
            <% options = Dropdown.createSortedCurrencyCodeOptions(account[iLoop].getAttribute("currency"), loginLocale); %>
              
                <%= widgetFactory.createSelectField( "acctCurr" + iLoop,"","&nbsp;", options, isReadOnly,true, false,"style='width:60px'", "", "inline")%>
            
              <% out.print("<INPUT TYPE=HIDDEN NAME='acctOid" + iLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes(account[iLoop].getAttribute("account_oid"), userSession.getSecretKey()) + "'>"); %>
             </td>
             </tr>
            
<%
      }
%>    
 </table>
</div>
<div class="columnRight"> 
<table><tr><td></td><td>
<%= widgetFactory.createLabel("PartyDetail.SettlementAccountNumber","PartyDetail.SettlementAccountNumber", false, false, false,"inline") %></td>
<td>
<%= widgetFactory.createLabel("PartyDetail.SettlementAccountCurrency","PartyDetail.SettlementAccountCurrency", false, false,false,"inline") %>
</td>
</tr>
<%
      
      for (iLoop=4; iLoop<8; iLoop++) { 
%>
       
      
            <tr>
          <td> <%= widgetFactory.createLabel("acctNumb" + iLoop,"PartyDetail.SettlementAccount" + (iLoop+1), false, false,false,"inline") %></td>
          <td>
             <%= widgetFactory.createTextField( "acctNum"+iLoop, "", account[iLoop].getAttribute("account_number"), "30", isReadOnly, false, false, "class='char20'","","inline")%>
             
             
             </td>
             <td> 
        <% options = Dropdown.createSortedCurrencyCodeOptions(account[iLoop].getAttribute("currency"), loginLocale); %>
              
              <%= widgetFactory.createSelectField( "acctCurr" + iLoop,"","&nbsp;", options, isReadOnly,true, false,"style='width:60px'", "", "inline")%>

             <% out.print("<INPUT TYPE=HIDDEN NAME='acctOid" + iLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes(account[iLoop].getAttribute("account_oid"), userSession.getSecretKey()) + "'>"); %>
             </td>
             </tr>
            
            
<%
      }
%>    
 </table>
      </div>
<div style="clear: both"></div>
    </div>
           

</div><%--formContent--%>
</div><%--formArea--%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'PartyDetail'">
      
                         <jsp:include page="/common/RefDataSidebar.jsp">
                                                
                                   <jsp:param name="showSaveButton" value="<%= showSaveButton %>" />
                                   <jsp:param name="saveOnClick" value="none" />
                                   <jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
                                   <jsp:param name="saveCloseOnClick" value="none" />
                                   <jsp:param name="showDeleteButton" value="<%=showDelete %>" /> 
                                   <jsp:param name="cancelAction" value="<%= cancelAction%>" />      
                                   <jsp:param name="showHelpButton" value="false" />
                                   <jsp:param name="showLinks" value="true" />      
                                   <jsp:param name="helpLink" value="/customer/party_detail.htm" />
                                   <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
                                           <jsp:param name="error" value="<%= error%>" /> 
                               </jsp:include>
</div>

<%= formMgr.getFormInstanceAsInputField("PartyDetailForm", secureParams) %>
<div id="PartySearchDialog"></div>
</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<%
  //save behavior defaults to without hsm
  String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  String saveCloseOnClick;
  if (saveAndCLoseToInstrument == true ) 
	  saveCloseOnClick = "common.setButtonPressed('SaveAndCloseToInstrument','0'); document.forms[0].submit();";
  else
	  saveCloseOnClick = "common.setButtonPressed('SaveAndClose','0'); document.forms[0].submit();";
	  
	  Debug.debug("VD ''' "+saveAndCLoseToInstrument);
%>
  

<jsp:include page="/common/RefDataSidebarFooter.jsp" />
  

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("Party");
   beanMgr.unregisterBean("CorporateOrganization");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   Debug.debug("***END********************PARTY*DETAIL**************************END***");
%>

<script type="text/javascript">
  var itemid;
  var section;
  function SearchParty(identifierStr, sectionName,partyType){

    itemid = String(identifierStr);
    section = String(sectionName);
    partyType= String(partyType);
    <%-- itemid = 'OrderingPartyName,OrderingPartyAddress1,OrderingPartyAddress2,OrderingPartyAddress3,OrderingPartyAddress4,OrderingPartyCountry'; --%>

    <%--cquinton 2/7/2013 - set returnAction to selectDesignatedBank - 
        ie. cannot be selectTransactionParty...--%>
    require(["t360/dialog"], function(dialog ) {
      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
        'PartySearch.jsp',
        ['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
        ['selectDesignatedBank','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section], <%-- parameters --%>
        'select', null);
    });
  }
 
 
</script>

<script type="text/javascript">

 
      
 
       function preSaveAndClose () {

       require(["t360/common"],function(common){
 	   <% 
   		if (saveAndCLoseToInstrument == true ){ %>
  			document.forms[0].buttonName.value = "SaveAndCloseToInstrument"
		<%}else{%>	  
 	  		document.forms[0].buttonName.value = "SaveAndClose"
		<%}%>
		});
		return true;
	  }
</script>
 
</body>
</html>
