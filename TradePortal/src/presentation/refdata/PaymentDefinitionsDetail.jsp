<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<jsp:useBean id="payDefUtil" class="com.ams.tradeportal.common.PaymentDefinitionsUtil"
   scope="session">
</jsp:useBean>



<%

  Debug.debug("*********************** START: Invoice Definition Detail Page **************************");

  String paymentDefOid          = StringFunction.xssCharsToHtml(request.getParameter("oid"));
  String 	loginLocale 		= userSession.getUserLocale();
  String 	loginTimezone		= userSession.getTimeZone();
  String 	loginRights 		= userSession.getSecurityRights();
  String 	tabPressed		= StringFunction.xssCharsToHtml(request.getParameter("tabPressed"));	// This is a local attribute from the webBean
  String	refDataHome		= "goToRefDataHome";    		//used for the close button and Breadcrumb link
  String    onLoad = "";

  DocumentHandler defaultDoc		= formMgr.getFromDocCache();
  Hashtable 	secureParms		= new Hashtable();
  boolean	hasEditRights		= false;
  boolean	isReadOnly	  = true;
  boolean  insertMode      = false;
  boolean  showDelete      = true;
  boolean  showSave        = true;
  boolean showSaveClose = true;
  boolean  getDataFromDoc		= false;
  boolean  retrieveFromDB       = false;
  String   partName        = "";
  boolean invAssigned = false;
  boolean  showSaveAs        = false;

 final String  general		= TradePortalConstants.INV_GENERAL;  	//Convienence - more readable
 final String  file			= TradePortalConstants.INV_FILE;
 Set pmtSummOptionalVal = payDefUtil.getPmtSummOtions();
 Set pmtSummReqVal = payDefUtil.getPmtSummReqVal();
    
 String maxError = defaultDoc.getAttribute("/Error/maxerrorseverity");
 Vector error =  defaultDoc.getFragments("/Error/errorlist/error");
  //WebBeans to access data
  PaymentDefinitionsWebBean paymentDef = beanMgr.createBean(PaymentDefinitionsWebBean.class, "PaymentDefinitions");
//Initialize WidgetFactory.
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

   // if user is ADMIN and having maintin PaymentMethod definition rights, then only create PaymentMethod button shoudl be visible
    String userSecurityType1 = "";
    if (userSession.getSavedUserSession() == null) {
  		userSecurityType1 = userSession.getSecurityType();
   }else {
	    userSecurityType1 = userSession.getSavedUserSessionSecurityType();
        }
   if( SecurityAccess.hasRights(loginRights,
              SecurityAccess.MAINTAIN_PAYMENT_FILE_DEF)){
	  hasEditRights    = true;
	}
    else {
	  hasEditRights   = false;
     }

  if( hasEditRights ) 			//If the user has the right to Maintain/edit data SET Readonly to False.
	isReadOnly = false;


 //SHR
 if (defaultDoc.getDocumentNode("/In/PaymentDefinitions") == null ) {
	     // The document does not exist in the cache.  We are entering the page from
	     // the listview page (meaning we're opening an existing item or creating
	     // a new one.)
  
     if(paymentDefOid != null)
      {
    	 paymentDefOid = EncryptDecrypt.decryptStringUsingTripleDes(paymentDefOid,userSession.getSecretKey());
      }
     else
      {
    	 paymentDefOid = null;
      }

     if (paymentDefOid == null) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
       paymentDefOid = "0";
     } else if (paymentDefOid.equals("0")) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
       insertMode = false;
       showSaveAs = hasEditRights;
     }    
  }else{
	// The doc does exist so check for errors.  If highest severity >=
	     // WARNING, we had errors.
	     
	     paymentDefOid = defaultDoc.getAttribute("/In/PaymentDefinitions/payment_definition_oid");
	       if(paymentDefOid == null || "0".equals(paymentDefOid) || "".equals(paymentDefOid)){
	    	   paymentDefOid = defaultDoc.getAttribute("/Out/PaymentDefinitions/payment_definition_oid");
	       }else if(paymentDefOid != null && !"0".equals(paymentDefOid) && !"".equals(paymentDefOid))
	       {
	    	   getDataFromDoc = false;
	    	   insertMode = false;
	           retrieveFromDB = true;
	           showSaveAs = true;
	           showDelete=true;
	       }
	     
	     if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
	       
	    	 getDataFromDoc = false;
	  	   	 insertMode = false;
	         retrieveFromDB = true;
	         showSaveAs = true;
	         showDelete=true;
	       
	       
	         // We've returned from a save/update/delete that was successful
	         // We shouldn't be here.  Forward to the RefDataHome page.
	         // Should never get here: jPylon and Navigation.xml should take care
	         // of this situation.
	      
	     } else {
	    	 //out.println("error");
	            // We've returned from a save/update/delete but have errors.
	            // We will display data from the cache.  Determine if we're
	            // in insertMode by looking for a '0' oid in the input section
	            // of the doc.
	            retrieveFromDB = false;
	            getDataFromDoc = true;
	            String newOid =
	            	defaultDoc.getAttribute("/In/PaymentDefinitions/payment_definition_oid");
	            
	            if (newOid.equals("0")) {
	               insertMode = true;
	               paymentDefOid = "0";
	            } else {
	               // Not in insert mode, use oid from output doc.
	               insertMode = false;
	               paymentDefOid = newOid;
	            }
	      }
	 }
 
	  if (retrieveFromDB) {
		     // Attempt to retrieve the item.  It should always work.  Still, we'll
		     // check for a blank oid -- this indicates record not found.  Set to insert
		     // mode and display error.

		     getDataFromDoc = false;

		     paymentDef.setAttribute("payment_definition_oid", paymentDefOid);
		     paymentDef.getDataFromAppServer();

		     if (paymentDef.getAttribute("payment_definition_oid").equals("")) {
		       insertMode = true;
		       paymentDefOid = "0";
		       getDataFromDoc = false;
		     } else {
		       insertMode = false;
		     }
		}

	    if (getDataFromDoc) {
		     // Populate the securityProfile bean with the output doc.
		     try {
		    	 paymentDef.populateFromXmlDoc(defaultDoc.getComponent("/In"));
		     } catch (Exception e) {
		        out.println("Contact Administrator: Unable to get Payment Method Definition "
		             + "attributes for oid: " + paymentDefOid + " " + e.toString());
		     }
		 }
 //SHR
 
 
 
  if(InstrumentServices.isNotBlank(paymentDefOid) && !"0".equals(paymentDefOid)){
	  showSaveAs= hasEditRights;

  }

  String buttonPressed = defaultDoc.getAttribute("/In/Update/ButtonPressed");

  String existingOrgOid = paymentDef.getAttribute("owner_org_oid");
  String CurrentOrgOid = userSession.getOwnerOrgOid();

  if(InstrumentServices.isNotBlank( existingOrgOid )) {
      secureParms.put("owner_oid",existingOrgOid);
      if (!CurrentOrgOid.equals(existingOrgOid)) {
         isReadOnly = true;
      }
  } else {
      secureParms.put("owner_oid",userSession.getOwnerOrgOid());
  }
  secureParms.put("current_owner_oid", CurrentOrgOid);	
  secureParms.put("oid", paymentDefOid);
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("login_rights",loginRights);
  secureParms.put("opt_lock", paymentDef.getAttribute("opt_lock"));
  secureParms.put("ownership_level",userSession.getOwnershipLevel());

  // Used for the "Added By" column
  if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
	  secureParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_ADMIN);
  else
	  secureParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);

  // Auto save the form when time-out if not readonly.
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  if (isReadOnly || insertMode) showDelete = false;
  if (isReadOnly) showSave = false;
  if (isReadOnly) showSaveClose = false;
  String newInvDefOid = null;
 // if (!isReadOnly) showSaveAs = false;
 
       if(insertMode && !isReadOnly)
      {
            showSave = true;
            showSaveClose = true;
            showDelete = false;
      }else if(!insertMode && !isReadOnly){
            showSave = true;
        showSaveClose = true;
        showDelete = true;
      }else if(isReadOnly){
            showSave = false;
        showSaveClose = false;
        showDelete = false;
      }
    String  fileFormatType = StringFunction.xssCharsToHtml(paymentDef.getAttribute("file_format_type"));
    boolean isABADef = TradePortalConstants.PAYMENT_FILE_TYPE_ABA.equals(fileFormatType) || TradePortalConstants.PAYMENT_FILE_TYPE_PNG.equals(fileFormatType) ? true : false;
	if (isABADef){
		showDelete = false;
        showSave = false;
        showSaveAs = false;
        showSaveClose = false;
        isReadOnly = true;
    }
%>


<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>


<div class="pageMain">		
  <div class="pageContent">	

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="PaymentDefinitions.PaymentDefinitions"/>
      <jsp:param name="helpUrl" value="customer/payment_file_detail.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if( InstrumentServices.isBlank(paymentDefOid) ||
      paymentDefOid.equals("0")) {
    subHeaderTitle = resMgr.getText("PaymentDefinitions.NewPaymentMethodUploadDef",
      TradePortalConstants.TEXT_BUNDLE);
  }
  else {
    subHeaderTitle = paymentDef.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include> 	

 <form id="PaymentDefinitionsDetailForm" name="PaymentDefinitionsDetailForm" method="post"
             data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

<input type=hidden value="" name=saveAsFlag>
<input type=hidden value="<%=paymentDefOid%>" name=origOid>
<input type=hidden value="<%=paymentDefOid%>" name=oid>
  <div class="formArea">
	<jsp:include page="/common/ErrorSection.jsp" />

     <div class="formContent">

    	
        <%@ include file="fragments/PaymentDefinitionsDetail-General.frag" %>
		
 	</div>		<%--  Form Content End --%>

  </div> <%--  Form Area End --%>



     <input type=hidden name ="part_to_validate" value="<%=tabPressed%>">
     <input type="hidden" name="buttonName" value="">
     <input type=hidden name ="reqPmtMthdSummaryCount" value="<%=paymentDef.getReqInvSummaryCount()%>">
<%

  Debug.debug("*********************** END: PaymentMethod Upload Definition Detail Page **************************");
%>

   <%= formMgr.getFormInstanceAsInputField("PaymentDefinitionsDetailForm", secureParms) %>
<%
	defaultDoc.removeAllChildren("/Error");
    formMgr.storeInDocCache("default.doc", defaultDoc);
    if(isABADef)
		showSaveAs = false; //Srinivasu_D Rel8.2 IR#T36000016778
	else
		showSaveAs = hasEditRights;		
		
%>

  <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'PaymentDefinitionsDetailForm'">

    	  <jsp:include page="/common/RefDataSidebar.jsp">
    		<jsp:param name="showSaveButton" value="<%= showSave %>" />
    		<jsp:param name="showDeleteButton" value="<%= showDelete%>" />
    		<jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
    		<jsp:param name="showSaveAsButton" value="<%=showSaveAs%>" />
   			<jsp:param name="saveCloseOnClick" value="none" />
    		<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
    		<jsp:param name="error" value="<%= error%>" />
    		<jsp:param name="saveAsDialogPageName" value="PaymentDefinitionsSaveAs.jsp" />
      		<jsp:param name="saveAsDialogId" value="saveAsDialogId" />
    		<jsp:param name="cancelAction" value="goToRefDataHome" />
    		<jsp:param name="showHelpButton" value="false" />
    		<jsp:param name="helpLink" value="customer/thresh_groups_detail.htm" />
    		<jsp:param name="showLinks" value="true" />
    		<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
    		<jsp:param name="error" value="<%=error%>" />
    	</jsp:include>
  </div>

 </form>
</div>		<%--  Page Content End --%>
</div>		<%--  Page Main End --%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>

<%
  //save behavior defaults to without hsm
  //T36000011959, pass 'none' to 'saveCloseOnClick' of RefDataSidebar.jsp
  // and below action for RefDataSidebarFooter.jsp
  
  //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  
  // String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  // String saveCloseOnClick = "common.setButtonPressed('SaveAndClose','0'); document.forms[0].submit();";
 %>
 
<jsp:include page="/common/RefDataSidebarFooter.jsp" />

<div id="saveAsDialogId" style="display: none;"></div>
<script type="text/javascript">
var isDLMDivLoaded =  false;
var isFFDivLoaded = false;
function submitSaveAs() 
{
        if(window.document.saveAsForm.newProfileName.value == ""){
          return false;
        }
        if(window.document.saveAsForm.newDescription.value == ""){
          return false;
        }
          document.PaymentDefinitionsDetailForm.name.value=window.document.saveAsForm.newProfileName.value
          document.PaymentDefinitionsDetailForm.description.value=window.document.saveAsForm.newDescription.value
          <%--  Sets a hidden field with the 'button' that was pressed.  This
          value is passed along to the mediator to determine what
          action should be done.  --%>
          document.PaymentDefinitionsDetailForm.saveAsFlag.value = "Y";
          document.PaymentDefinitionsDetailForm.oid.value = "0";
          document.PaymentDefinitionsDetailForm.buttonName.value = "SaveAndClose";
          document.PaymentDefinitionsDetailForm.submit();
          hideDialog("saveAsDialogId");             
 } 

function add4MoreDefined(tableToIncrease){
	
	var bType = "seller_users_def";
	var toalUserDef = 10;
	if (tableToIncrease == "InvBuyerDefinedTableId")
		bType = "buyer_users_def";
	if (tableToIncrease =="InvUserDefinitionLineTableId"){
		bType = "prod_chars_ud";
		toalUserDef = 7;
	}
	
	var table = dojo.byId(tableToIncrease);
		
	<%-- get index# for field naming --%>
	var insertRowIdx = table.rows.length;
	var cnt = 0;	  
	  
	for(cnt = 0; cnt<4; cnt++){
	  	
	  	<%-- k is used to check the total data item rows. Maximum = 10. --%>
	  	<%-- we need to subtract 2 to remove header lines --%>
	  	k=insertRowIdx+1-2;
	  	if(k > toalUserDef){
			  break;
	    }
		<%-- add the table row --%>
	  	var row = table.insertRow(insertRowIdx);
	  	row.id = bType+(insertRowIdx-1);
   	 	var cell0 = row.insertCell(0);<%-- first  cell in the row --%>
		var cell1 = row.insertCell(1);<%-- second  cell in the row --%>
		var cell2 = row.insertCell(2);<%-- third  cell in the row --%>
		var cell3 = row.insertCell(3);<%-- third  cell in the row --%>
		cell0.setAttribute("align","center");
		cell2.setAttribute("align","right");
	    cell2.setAttribute("class","formItem");
	    cell3.setAttribute("align","center");
		if(bType == "prod_chars_ud"){
          cell0.innerHTML ='<input data-dojo-type=\"dijit.form.CheckBox\" name=\"'+bType+k+'_type_req\" id=\"'+bType+k+'_type_req\" value=\"Y\">'        
          cell1.innerHTML ='<input data-dojo-type="dijit.form.ValidationTextBox" name=\"'+bType+k+'_type\" id=\"'+bType+k+'_type\"   maxLength=\"35\" width=\'10\' value=\"\">'        
          cell2.innerHTML ='<label>35</label>'
          cell3.innerHTML ='<input  data-dojo-type=\"dijit.form.CheckBox\" name=\"'+bType+k+'_type_data_req\" id=\"'+bType+k+'_type_data_req\" value=\"Y\">'
      	    
		}else{
	      cell0.innerHTML ='<input data-dojo-type=\"dijit.form.CheckBox\" name=\"'+bType+k+'_req\" id=\"'+bType+k+'_req\" value=\"Y\">'        
	      cell1.innerHTML ='<input data-dojo-type="dijit.form.ValidationTextBox" name=\"'+bType+k+'_label\" id=\"'+bType+k+'_label\"   maxLength=\"35\" width=\'10\' required=\"true\" value=\"\">'        
	      cell2.innerHTML ='<label >140</label>'	
	      cell3.innerHTML ='<input  data-dojo-type=\"dijit.form.CheckBox\" name=\"'+bType+k+'_data_req\" id=\"'+bType+k+'_data_req\" value=\"Y\">'
	     }
    
        require(["dojo/parser"], function(parser) {
         	parser.parse(row.id);
    	 });
	   	insertRowIdx = insertRowIdx+1;
	  }  
} 

function updateDefOrder(gIndex, filedName)
{
	var temp = "";
	var index = 0;
	var temp_upload_order_value = "";
	gIndex = gIndex-1;	
	var isSwaped = false;
	<%-- Validate text box --%>
	for( i=1; i < gIndex; i++ ){
		textValue = document.getElementById('text'+i).value;
		<%-- Check for in-correct value --%>
		if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < 1) || (textValue >= gIndex) ) {
				document.getElementById('text'+i).focus();
                raiseOrderMissingAlert();
				return false;
		}else{
			document.getElementById('text'+i).value = textValue ;
		}

	}

	for(var i=1; i < gIndex; i++){
		<%-- check the entered text box value with the i value  --%>
		if( i != document.getElementById('text'+(i)).value ){
			<%--  check if textbox value's are repeated or not --%>
			
			if(document.getElementById('text'+i).value == document.getElementById('text'+(document.getElementById('text'+i).value)).value){
				document.getElementById('text'+i).focus();
                raiseOrderNotUniqueAlert();
				return false;
			}
			
			<%-- Swap fieldname data --%>
			temp = document.getElementById('fileFieldName'+i).innerHTML;
			document.getElementById('fileFieldName'+ i).innerHTML = document.getElementById('fileFieldName'+document.getElementById('text'+(i)).value).innerHTML;
			document.getElementById("fileFieldName"+document.getElementById('text'+(i)).value).innerHTML = temp;
			
			<%-- swap even the ids --%>
            temp_upload_order_value = document.getElementById("upload_order_"+i).value;
			document.getElementById("upload_order_"+i).value = document.getElementById("upload_order_"+(document.getElementById('text'+i).value) ).value;
			document.getElementById("upload_order_"+(document.getElementById('text'+i).value) ).value = temp_upload_order_value;

			<%-- Swap the index value ie, textbox value --%>
			index = document.getElementById('text'+(i)).value
			document.getElementById('text'+(i)).value =  document.getElementById('text'+index).value;
			document.getElementById('text'+index).value = index;
			isSwaped = true;
			
		}
		
	}	
if(isSwaped){
		
		var totalPOSummGoodOrderDara = (document.getElementById("PmtDLMDefFileOrderTableId").rows.length) - (2); <%--  two row for header --%>
		var rowToModify = "";
		var spanId="";
		var idx;
		for(var row=0; row<totalPOSummGoodOrderDara; row++){
			rowToModify = document.getElementById("PmtDLMDefFileOrderTableId").rows[row+2]; <%--  two for header --%>
			if(rowToModify.cells[1].getElementsByTagName("span")[0]){
				
			spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
			<%-- idx = spanId.indexOf("_label"); --%>
			if(spanId){
				<%-- spanId = spanId.substring(0,idx); --%>
			<%-- 	rowToModify.id = spanId; --%>
			}
			<%-- rowToModify.id = spanId; --%>
			rowToModify.id = spanId + "_req_order";
		}}
	}
	
}

function updateFFDefOrder(gIndex, filedName)
{
    var temp = "";
    var index = 0;
    var temp_upload_order_value = "";
    var temp_position_value ="";
    var temp_size_value =";"

    gIndex = gIndex-1;
    var isSwaped = false;
    <%-- Validate text box --%>
    for( i=1; i < gIndex; i++ ){
        textValue = document.getElementById('ftext'+i).value;
        <%-- Check for in-correct value --%>
        if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < 1) || (textValue >= gIndex) ) {
            document.getElementById('ftext'+i).focus();
            raiseOrderMissingAlert();
            return false;
        }else{
            document.getElementById('ftext'+i).value = textValue ;
        }

    }

    for(var i=1; i < gIndex; i++){
        <%-- check the entered text box value with the i value --%>
        if( i != document.getElementById('ftext'+(i)).value ){
            <%--  check if textbox value's are repeated or not --%>
            if(document.getElementById('ftext'+i).value == document.getElementById('ftext'+(document.getElementById('ftext'+i).value)).value){
                document.getElementById('ftext'+i).focus();
                raiseOrderNotUniqueAlert();
                return false;
            }

            <%-- Swap fieldname data --%>
            temp = document.getElementById('ffileFieldName'+i).innerHTML;
            document.getElementById('ffileFieldName'+ i).innerHTML = document.getElementById('ffileFieldName'+document.getElementById('ftext'+(i)).value).innerHTML;
            document.getElementById("ffileFieldName"+document.getElementById('ftext'+(i)).value).innerHTML = temp;

            <%-- swap even the ids --%>
            temp_upload_order_value = document.getElementById("fupload_order_"+i).value;
            document.getElementById("fupload_order_"+i).value = document.getElementById("fupload_order_"+(document.getElementById('ftext'+i).value) ).value;
            document.getElementById("fupload_order_"+(document.getElementById('ftext'+i).value) ).value = temp_upload_order_value;

            <%-- swap size value --%>
            temp_sizevalue = document.getElementById("fsize"+i).value;
            document.getElementById("fsize"+i).value = document.getElementById("fsize"+(document.getElementById('ftext'+i).value) ).value;
            document.getElementById("fsize"+(document.getElementById('ftext'+i).value) ).value = temp_sizevalue;

            <%-- Swap the index value ie, textbox value --%>
            index = document.getElementById('ftext'+(i)).value
            document.getElementById('ftext'+(i)).value =  document.getElementById('ftext'+index).value;
            document.getElementById('ftext'+index).value = index;
            isSwaped = true;

        }

    }
    if(isSwaped){

        var totalPOSummGoodOrderDara = (document.getElementById("PmtFFDefFileOrderTableId").rows.length) - (2); <%--  two row for header --%>
        var rowToModify = "";
        var spanId="";
        var idx;
        for(var row=0; row<totalPOSummGoodOrderDara; row++){
            rowToModify = document.getElementById("PmtFFDefFileOrderTableId").rows[row+2]; <%--  two for header --%>
            if(rowToModify.cells[1].getElementsByTagName("span")[0]){

                spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
                <%-- idx = spanId.indexOf("_label"); --%>
                if(spanId){
                    <%-- spanId = spanId.substring(0,idx); --%>
                    <%-- 	rowToModify.id = spanId; --%>
                }
                <%-- rowToModify.id = spanId; --%>
                rowToModify.id = spanId + "_req_order";
            }}
    }
    for(var row=1; row<=totalPOSummGoodOrderDara; row++){
        <%-- swap position value --%>

        var a;
        if (row == 1){
            a = Number(1);
        } else{
        	temp_position_value = document.getElementById("fposition"+(row-1)).value;         
            var fieldName = document.getElementById('fsize'+(row-1)).value;
            var tempSize = Number(document.getElementById(document.getElementById('fsize'+(row-1)).value+"_size").value);
            if (fieldName == 'payment_amount'){
                var tempVal = document.getElementById(fieldName+"_size").value;

                if (tempVal){
                    tempVal = tempVal.substring(0, tempVal.indexOf(","));
                    tempSize = Number(tempVal) + 4;
                }
            }
            a = Number(temp_position_value ) + tempSize;
        }
        document.getElementById("fposition"+row).value = a;

    }

}

function updateFFHdrDefOrder(gIndex, filedName)
{
    var temp = "";
    var index = 0;
    var temp_upload_order_value = "";
    var temp_position_value ="";
    var temp_size_value =";"
    gIndex = gIndex-1;
    var isSwaped = false;
    <%-- Validate text box --%>
    for( i=1; i < gIndex; i++ ){
        textValue = document.getElementById('htext'+i).value;
        <%-- Check for in-correct value --%>
        if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < 1) || (textValue >= gIndex) ) {
            document.getElementById('htext'+i).focus();
            raiseOrderMissingAlert();
            return false;
        }else{
            document.getElementById('htext'+i).value = textValue ;
        }
    }

    for(var i=1; i < gIndex; i++){    	
        <%-- check the entered text box value with the i value --%>
        if( i != document.getElementById('htext'+(i)).value ){        	
            <%--  check if textbox value's are repeated or not --%>
            if(document.getElementById('htext'+i).value == document.getElementById('htext'+(document.getElementById('htext'+i).value)).value){
                document.getElementById('htext'+i).focus();
                raiseOrderNotUniqueAlert();
                return false;
            }

            <%-- Swap fieldname data --%>
            temp = document.getElementById('hfileFieldName'+i).innerHTML;         
            document.getElementById('hfileFieldName'+ i).innerHTML = document.getElementById('hfileFieldName'+document.getElementById('htext'+(i)).value).innerHTML;
            document.getElementById("hfileFieldName"+document.getElementById('htext'+(i)).value).innerHTML = temp;
          
            <%-- swap even the ids --%>
            temp_upload_order_value = document.getElementById("hupload_order_"+i).value;
            document.getElementById("hupload_order_"+i).value = document.getElementById("hupload_order_"+(document.getElementById('htext'+i).value) ).value;
            document.getElementById("hupload_order_"+(document.getElementById('htext'+i).value) ).value = temp_upload_order_value;

            <%-- swap size value            --%>
            temp_sizevalue = document.getElementById("hsize"+i).value;              
           document.getElementById("hsize"+i).value = document.getElementById("hsize"+(document.getElementById('htext'+i).value) ).value;
            document.getElementById("hsize"+(document.getElementById('htext'+i).value) ).value = temp_sizevalue;
          
            <%-- Swap the index value ie, textbox value --%>
            index = document.getElementById('htext'+(i)).value
            document.getElementById('htext'+(i)).value =  document.getElementById('htext'+index).value;
            document.getElementById('htext'+index).value = index;

            isSwaped = true;

        }

    }
    if(isSwaped){

        var totalPOSummGoodOrderDara = (document.getElementById("PmtFFHdrDefFileOrderTableId").rows.length) - (2); <%--  two row for header --%>
        var rowToModify = "";
        var spanId="";
        var idx;       
        for(var row=0; row<totalPOSummGoodOrderDara; row++){
            rowToModify = document.getElementById("PmtFFHdrDefFileOrderTableId").rows[row+2]; <%--  two for header --%>
            if(rowToModify.cells[1].getElementsByTagName("span")[0]){

                spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
                <%-- idx = spanId.indexOf("_label"); --%>
                if(spanId){
                    <%-- spanId = spanId.substring(0,idx); --%>
                    <%-- 	rowToModify.id = spanId; --%>
                }
                <%-- rowToModify.id = spanId; --%>
                rowToModify.id = spanId + "_req_order";
            }}
    }

    for(var row=1; row<=totalPOSummGoodOrderDara; row++){
        <%-- swap position value --%>
        var a;
        if (row == 1){
              a = Number(1);
        } else{
            temp_position_value = document.getElementById("hposition"+(row-1)).value;         
            var sizeValue = document.getElementById(document.getElementById('hsize'+(row-1)).value+"_size").value;
            if (sizeValue.indexOf(",") != -1){            	        	 
        	  	var sizeVal = sizeValue.split(","); 
          		sizeValue = Number(sizeVal[0]) + 1 + Number(sizeVal[1]); 
            }
            a = Number(temp_position_value ) +  Number(sizeValue);        
        }
        document.getElementById("hposition"+row).value = a;
    }

}

function updateFFInvDefOrder(gIndex, filedName)
{
    var temp = "";
    var index = 0;
    var temp_upload_order_value = "";
    var temp_position_value ="";
    var temp_size_value =";"

    gIndex = gIndex-1;
    var isSwaped = false;
    <%-- Validate text box --%>
    for( i=1; i < gIndex; i++ ){
        textValue = document.getElementById('itext'+i).value;
        <%-- Check for in-correct value --%>
        if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < 1) || (textValue >= gIndex) ) {
            document.getElementById('itext'+i).focus();
            raiseOrderMissingAlert();
            return false;
        }else{
            document.getElementById('itext'+i).value = textValue ;
        }

    }

    for(var i=1; i < gIndex; i++){
        <%-- check the entered text box value with the i value --%>
        if( i != document.getElementById('itext'+(i)).value ){
            <%--  check if textbox value's are repeated or not --%>

            if(document.getElementById('itext'+i).value == document.getElementById('itext'+(document.getElementById('itext'+i).value)).value){
                document.getElementById('itext'+i).focus();
                raiseOrderNotUniqueAlert();
                return false;
            }

            <%-- Swap fieldname data --%>
            temp = document.getElementById('ifileFieldName'+i).innerHTML;
            document.getElementById('ifileFieldName'+ i).innerHTML = document.getElementById('ifileFieldName'+document.getElementById('itext'+(i)).value).innerHTML;
            document.getElementById("ifileFieldName"+document.getElementById('itext'+(i)).value).innerHTML = temp;

            <%-- swap even the ids --%>
            temp_upload_order_value = document.getElementById("iupload_order_"+i).value;
            document.getElementById("iupload_order_"+i).value = document.getElementById("iupload_order_"+(document.getElementById('itext'+i).value) ).value;
            document.getElementById("iupload_order_"+(document.getElementById('itext'+i).value) ).value = temp_upload_order_value;

            <%-- swap size value --%>
            temp_sizevalue = document.getElementById("isize"+i).value;
            document.getElementById("isize"+i).value = document.getElementById("isize"+(document.getElementById('itext'+i).value) ).value;
            document.getElementById("isize"+(document.getElementById('itext'+i).value) ).value = temp_sizevalue;

            <%-- Swap the index value ie, textbox value --%>
            index = document.getElementById('itext'+(i)).value
            document.getElementById('itext'+(i)).value =  document.getElementById('itext'+index).value;
            document.getElementById('itext'+index).value = index;
            isSwaped = true;

        }

    }
    if(isSwaped){

        var totalPOSummGoodOrderDara = (document.getElementById("PmtFFInvDefFileOrderTableId").rows.length) - (2); <%--  two row for header --%>
        var rowToModify = "";
        var spanId="";
        var idx;
        for(var row=0; row<totalPOSummGoodOrderDara; row++){
            rowToModify = document.getElementById("PmtFFInvDefFileOrderTableId").rows[row+2]; <%--  two for header --%>
            if(rowToModify.cells[1].getElementsByTagName("span")[0]){

                spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
                <%-- idx = spanId.indexOf("_label"); --%>
                if(spanId){
                    <%-- spanId = spanId.substring(0,idx); --%>
                    <%-- 	rowToModify.id = spanId; --%>
                }
                <%-- rowToModify.id = spanId; --%>
                rowToModify.id = spanId + "_req_order";
            }}
    }
    for(var row=1; row<=totalPOSummGoodOrderDara; row++){
        <%-- swap position value --%>

        var a;
        if (row == 1){
            a = Number(1);
        } else{
            temp_position_value = document.getElementById("iposition"+(row-1)).value;
            a = Number(temp_position_value ) + Number(document.getElementById(document.getElementById('isize'+(row-1)).value+"_size").value);
        }
        document.getElementById("iposition"+row).value = a;



    }

}

function updateSumDefOrder(gIndex, sIndex, filedName)
{
	var temp = "";
	var index = 0;
	var temp_upload_order_value = "";
	
	sIndex = sIndex - 1;
	gIndex = gIndex -1;
	totalOrder = sIndex + gIndex -2;
	var isSwaped = false;
	
	for( var j = 1; j < gIndex; j++ ){

		textValue = document.getElementById('userDefText'+(j)).value;
		<%-- Check for in-correct value --%>
		if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < sIndex) || (textValue > totalOrder) ) {
				document.getElementById('userDefText'+j).focus();
                raiseOrderMissingAlert();
				return false;
		}else{
			document.getElementById('userDefText'+j).value = textValue ;
		}

	}

	for(var i=1; i < gIndex; i++){
		<%-- check the entered text box value with the i value  --%>
		var userDefTextVal = document.getElementById('userDefText'+(i)).value;
		var invSummGoodsStart = sIndex + i -1;
		if( invSummGoodsStart != userDefTextVal ){
			<%--  check if textbox value's are repeated or not --%>

		   var swapFieldPosition = userDefTextVal - sIndex +1;
			if(document.getElementById('userDefText'+(i)).value == document.getElementById('userDefText'+swapFieldPosition).value){
				document.getElementById('userDefText'+i).focus();
                raiseOrderNotUniqueAlert();
				return false;
			}
			
			<%-- Swap fieldname data --%>
			temp = document.getElementById('userDefFileFieldName'+(i)).innerHTML;					
			document.getElementById('userDefFileFieldName'+ i).innerHTML = document.getElementById('userDefFileFieldName'+ swapFieldPosition).innerHTML;
			document.getElementById("userDefFileFieldName"+ swapFieldPosition).innerHTML = temp;
			
			<%-- swap even the ids --%>
            temp_upload_order_value = document.getElementById("upload_userdef_order_"+i).value;
			document.getElementById("upload_userdef_order_"+i).value = document.getElementById("upload_userdef_order_"+ swapFieldPosition).value;
			document.getElementById("upload_userdef_order_"+ swapFieldPosition).value = temp_upload_order_value;

			<%-- Swap the index value ie, textbox value --%>
			index = (document.getElementById('userDefText'+(i)).value);
			document.getElementById('userDefText'+(i)).value =  document.getElementById('userDefText'+ swapFieldPosition).value;
			document.getElementById('userDefText'+ swapFieldPosition).value = index;
			isSwaped = true;
		}
	}	
	<%--  update row is according to data after swapping --%>
	if(isSwaped){
		
		var totalPOSummGoodOrderDara = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2); <%--  two row for header --%>
		var rowToModify = "";
		var spanId="";
		var idx;
		for(var row=0; row<totalPOSummGoodOrderDara; row++){
			rowToModify = document.getElementById("poStructSumDataOrderTableId").rows[row+2]; <%--  two for header --%>
			spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
			idx = spanId.indexOf("_label");
			if( idx > -1){
				spanId = spanId.substring(0,idx);
			}
			rowToModify.id = spanId + "_req_order";
		}
	}
}


function updateCreditNoteDefOrder(gIndex, sIndex, filedName)
{

	var temp = "";
	var index = 0;
	var temp_upload_order_value = "";
	
	sIndex = sIndex - 1;
	gIndex = gIndex -1;
	totalOrder = sIndex + gIndex -2;
	var isSwaped = false;
	
	for( var j = 1; j < gIndex; j++ ){

		textValue = document.getElementById('1userDefText'+(j)).value;

		if( textValue == "" || textValue.length == 0 || isNaN(textValue)){
				document.getElementById('1userDefText'+j).focus();
                raiseOrderMissingAlert();
				return false;
		}else{
			document.getElementById('1userDefText'+j).value = textValue ;
		}

	}

	for(var i=1; i < gIndex; i++){
		<%-- check the entered text box value with the i value  --%>
		var userDefTextVal = document.getElementById('1userDefText'+(i)).value;
		var invSummGoodsStart = sIndex + i -1;
		if( invSummGoodsStart != userDefTextVal ){
			<%--  check if textbox value's are repeated or not --%>

		   var swapFieldPosition = userDefTextVal - sIndex +1;
			if(document.getElementById('1userDefText'+(i)).value == document.getElementById('1userDefText'+swapFieldPosition).value){
				document.getElementById('1userDefText'+i).focus();
                raiseOrderNotUniqueAlert();
				return false;
			}
			
			<%-- Swap fieldname data --%>
			temp = document.getElementById('1userDefFileFieldName'+(i)).innerHTML;					
			document.getElementById('1userDefFileFieldName'+ i).innerHTML = document.getElementById('userDefFileFieldName'+ swapFieldPosition).innerHTML;
			document.getElementById("1userDefFileFieldName"+ swapFieldPosition).innerHTML = temp;
			
			<%-- swap even the ids --%>
            temp_upload_order_value = document.getElementById("1upload_userdef_order_"+i).value;
			document.getElementById("1upload_userdef_order_"+i).value = document.getElementById("1upload_userdef_order_"+ swapFieldPosition).value;
			document.getElementById("1upload_userdef_order_"+ swapFieldPosition).value = temp_upload_order_value;

			<%-- Swap the index value ie, textbox value --%>
			index = (document.getElementById('1userDefText'+(i)).value);
			document.getElementById('1userDefText'+(i)).value =  document.getElementById('1userDefText'+ swapFieldPosition).value;
			document.getElementById('1userDefText'+ swapFieldPosition).value = index;
			isSwaped = true;
		}
	}	
	<%--  update row is according to data after swapping --%>
	if(isSwaped){
		
		var totalPOSummGoodOrderDara = (document.getElementById("discountCodeDataOrderTableId").rows.length) - (2); <%--  two row for header --%>
		var rowToModify = "";
		var spanId="";
		var idx;
		for(var row=0; row<totalPOSummGoodOrderDara; row++){
			rowToModify = document.getElementById("discountCodeDataOrderTableId").rows[row+2]; <%--  two for header --%>
			spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
			idx = spanId.indexOf("_label");
			if( idx > -1){
				spanId = spanId.substring(0,idx);
			}
			rowToModify.id = spanId + "_req_order";
		}
	}
}



function updateSumLineItemOrder(gIndex, sIndex, filedName)
{
	var temp = "";
	var index = 0;
	var temp_upload_order_value = "";
	<%-- var j = 1;	 --%>
	
	sIndex = sIndex - 3;
	gIndex = gIndex -1;
	totalOrder = sIndex + gIndex -2;
	
	for( var j = 1; j < gIndex; j++ ){        
		textValue = document.getElementById('userDefLineText'+j).value;
		<%-- Check for in-correct value --%>
		if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < sIndex) || (textValue > totalOrder) ) {
				document.getElementById('userDefLineText'+j).focus();
                raiseOrderMissingAlert();
				return false;
		}else{
			document.getElementById('userDefLineText'+j).value = textValue ;
		}

	}

	for(var i=1; i < gIndex; i++){
		<%-- check the entered text box value with the i value  --%>
		var userDefTextVal = document.getElementById('userDefLineText'+(i)).value;
		var invSummLineItemStart = sIndex + i -1;
		if( invSummLineItemStart != userDefTextVal ){
			<%--  check if textbox value's are repeated or not --%>
            var swapFieldPosition = userDefTextVal - sIndex +1;
			if(document.getElementById('userDefLineText'+(i)).value == document.getElementById('userDefLineText'+swapFieldPosition).value){
				document.getElementById('userDefLineText'+i).focus();
                raiseOrderNotUniqueAlert();
				return false;
			}
			
			<%-- Swap fieldname data --%>
			temp = document.getElementById('userDefLineFileFieldName'+(i)).innerHTML;					
			document.getElementById('userDefLineFileFieldName'+ i).innerHTML = document.getElementById('userDefLineFileFieldName'+swapFieldPosition).innerHTML;
			document.getElementById("userDefLineFileFieldName"+swapFieldPosition).innerHTML = temp;
			
			<%-- swap even the ids --%>
            temp_upload_order_value = document.getElementById("upload_userDefLine_order_"+i).value;
			document.getElementById("upload_userDefLine_order_"+i).value = document.getElementById("upload_userDefLine_order_"+swapFieldPosition).value;
			document.getElementById("upload_userDefLine_order_"+swapFieldPosition ).value = temp_upload_order_value;

			<%-- Swap the index value ie, textbox value --%>
			index = (document.getElementById('userDefLineText'+(i)).value);
			document.getElementById('userDefLineText'+(i)).value =  document.getElementById('userDefLineText'+swapFieldPosition).value;
			document.getElementById('userDefLineText'+swapFieldPosition).value = index;
			
		}
	}	
	
}

function reorderInvDefFileOrder(){
	
	<%-- 	var invSummDataOrderCount = (document.getElementById("PmtDLMDefFileOrderTableId").rows.length) - (2); --%>
		var invSummDataOrderCount = 0;	
		for(var i=1; i <= invSummDataOrderCount; i++){
			document.getElementById("text" + i).value = (invSummDataOrderCount) + (i);
		}		
}

function reorderInvSummGoodsOrder(){
	
	var invSummGoodsOrderCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
	if(invSummGoodsOrderCount > 0){
		var invSummDataOrderCount = (document.getElementById("PmtDLMDefFileOrderTableId").rows.length) - (2);
		for(var i=1; i <= invSummGoodsOrderCount; i++){
			document.getElementById("userDefText" + i).value = (invSummDataOrderCount) + (i);
		}		
	}	
}

function reorderInvSummLineItemOrder(){
	
	var invSummLineItemOrderCount = (document.getElementById("poStructSumLineItemOrderTableId").rows.length) - (2);
	if(invSummLineItemOrderCount > 0){
		var invSummDataOrderCount = (document.getElementById("PmtDLMDefFileOrderTableId").rows.length) - (2);
		var invSummGoodsOrderCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
		for(var i=1; i <= invSummLineItemOrderCount; i++){
			
			document.getElementById("userDefLineText" + i).value = (invSummDataOrderCount) + (invSummGoodsOrderCount) + (i);
		}		
	}	
}

function reorderInvCreditNoteItemOrder(){
	
	var invSummLineItemOrderCount = (document.getElementById("discountCodeDataOrderTableId").rows.length) - (2);
	if(invSummLineItemOrderCount > 0){
		var invSummDataOrderCount = (document.getElementById("PmtDLMDefFileOrderTableId").rows.length) - (2);
		var invSummGoodsOrderCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
		var invSummGoodsOrderCount1 = (document.getElementById("poStructSumLineItemOrderTableId").rows.length) - (2);
		for(var i=1; i <= invSummLineItemOrderCount; i++){
			
			document.getElementById("1userDefText" + i).value = (invSummDataOrderCount) + (invSummGoodsOrderCount) +(invSummGoodsOrderCount1)+ (i);
		}		
	}	
}

<%-- CR-741 - end --%>
function raiseOrderMissingAlert(){
    alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("PaymentDefinitions.DefOrderMissingError", TradePortalConstants.TEXT_BUNDLE))%>');
}

function raiseOrderNotUniqueAlert(){
    alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("PaymentDefinitions.DefOrderNotUniqueError", TradePortalConstants.TEXT_BUNDLE))%>');
}
<%--  KMehta IR T36000026012 @Rel8400 12 Mar 2014 Start  --%>
function setBenAcctMandate(option){
	  require(["dijit/registry", "dojo/dom", "dojo/dom-style","dojo/dom-class","dijit/registry"],
		    function(registry, dom, domStyle, domClass) {
		    	     var payment_method = registry.byId("payment_method");
		    	     if (dom.byId('benAcct')){
		    	    	 
		    	     
		    	     var inHtml = dom.byId('benAcct').innerHTML;
		 	  			if (payment_method == 'ACH' || payment_method == 'BKT' || payment_method == 'RTGS' || payment_method == 'CBFT' ){
		 	  				option = true;
		 	  				if(dijit.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX") && dijit.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX").checked == true){
		 	  					inHtml = inHtml.replace('<span class=\"asterisk\">*</span>','');
		 	  					inHtml = '<span class=\"asterisk\">*</span>' +inHtml;			 	  				
		 	  					dom.byId('benAcct').innerHTML = inHtml;
			 	  				markWidgetMandtory(dom.byId('benAcct'),domClass,option);
		 	  				}		    	   
		      	}else{
		      		inHtml = inHtml.replace('<span class=\"asterisk\">*</span>','');
		      		dom.byId('benAcct').innerHTML = inHtml;
		      	}
		    	     }
		 	  			  
	  });
}

function markWidgetMandtory(widget,domClass,option){
		
		if (widget){
			if (option){	         
	         widget.required = true;
	         domClass.add(widget, "required");
	 		}else{	       
	           widget.required = false;
	           domClass.remove(widget, "required");
	 		}
		}
	}
require(["dijit/registry", "dojo/on", "dojo/ready"],
	      function(registry, on, ready) {
	    ready( function(dom){
	    	var payment_method = registry.byId("payment_method");
	  			if (payment_method == 'ACH' || payment_method == 'BKT' || payment_method == 'RTGS' || payment_method == 'CBFT' ){
	  				if(dijit.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX") && dijit.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX").checked == true){
	    				setBenAcctMandate(true);
	  				}
	    	}    	
	    });
	  });

<%--  KMehta IR T36000026012 @Rel8400 12 Mar 2014 End  --%>
<%-- DK IR T36000024548 Rel8.4 02/03/2014 Starts --%>
function setDelimitedFileFormat(){
	  require(["dijit/registry", "dojo/dom", "dojo/dom-style"],
		    function(registry, dom, domStyle) {
   		    	    var delimiter_char = registry.byId("delimiter_char");
   		    	    if(delimiter_char!=''){
   		    	    	registry.getEnclosingWidget(document.getElementById('TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT')).set('checked', true);
   		    	    }
   		    	    else{
   		    	    	registry.getEnclosingWidget(document.getElementById('TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT')).set('checked', false);
   		    	    }
	  });
}
<%-- DK IR T36000024548 Rel8.4 02/03/2014 Ends  --%>

require(["dojo/_base/array", 'dojo/_base/lang', "dojo/dom",
         "dojo/dom-construct", "dojo/dom-class", 
         "dojo/dom-style", "dojo/query",
         "dijit/registry", "dojo/on", 
         "t360/common",
         "dojo/ready", "dojo/NodeList-traverse"],
    function(arrayUtil, lang, dom,
             domConstruct, domClass,
             domStyle, query,
             registry, on, 
             common, 
             ready ) {
                      
    
    ready( function (arrayUtil, dom,
            domConstruct, domClass,
            domStyle, query,
            registry, on, 
            common, 
            ready ){
          
          console.log("hello in require in frag");
           isDLMDivLoaded =  true;
           isFFDivLoaded = true;
    });

        function checkInvLabelLabelVal(labelVal, reqCheck, errorText) {
            if(labelVal.replace(/^\s+|\s+$/g, '') == ""){
                alert( ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("UserDefLabel.Required1", TradePortalConstants.TEXT_BUNDLE))%>' + ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("BuyerUserDfn.Required", TradePortalConstants.TEXT_BUNDLE))%>' + ' <%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("UserDefLabel.Required2", TradePortalConstants.TEXT_BUNDLE))%>');
                var checkedObj = dijit.getEnclosingWidget(reqCheck);
                checkedObj.set('checked',false);
                return false;
            }
            return true;
        }

    function checkRequiredVal(reqVal, reqCheck) {
           if(reqVal==false){                         
               alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("PaymentDefinitions.DataReqText", TradePortalConstants.TEXT_BUNDLE))%>');
              var checkedObj = dijit.getEnclosingWidget(reqCheck);
              checkedObj.set('checked',false);
              return false;
           }          
       }

    function checkPayMethodRequiredVal(reqVal, reqCheck) {
           if(reqVal==false){                         
               alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("PaymentDefinitions.PaymentMethodReqText", TradePortalConstants.TEXT_BUNDLE))%>');
              var checkedObj = dijit.getEnclosingWidget(reqCheck);
              checkedObj.set('checked',false);
              return false;
           }  
           return true;        
       }
    function checkBankNameOrBranchCodeRequiredVal(reqBankNameVal, reqBankBranchCodeVal, reqCheck) {
           if(reqBankNameVal==false && reqBankBranchCodeVal==false){                          
               alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("PaymentDefinitions.BankNameOrBranchCodeReqText", TradePortalConstants.TEXT_BUNDLE))%>');
              var checkedObj = dijit.getEnclosingWidget(reqCheck);
              checkedObj.set('checked',false);
              return false;
           }  
           return true;
       }
    
    function unsetDataReq(reqField){
        var checkedObj = dijit.getEnclosingWidget(reqField);
        if (checkedObj){
            checkedObj.set('checked',false);
            return false;
        }
    }
    function unsetDataDelDLMOrder(id){
        unsetDataReq(dom.byId(id.replace('_req','_data_req')));
        var row = dom.byId(id + "_order");
        deleteOrderField(row, dom.byId("PmtDLMDefFileOrderTableId"), "text");   
    }
    function unsetDataDelFFHdrOrder(id){
        unsetDataReq(dom.byId(id.replace('_req','_data_req')));
        row = dom.byId(id + "_order");
        deleteOrderField(row, dom.byId("PmtFFHdrDefFileOrderTableId"), "htext");
    }
    function calPositionWithDecimals(fieldSize){
        var sizeValues = fieldSize.split(",");
        fieldSize = Number(sizeValues[0]) + 1 + Number(sizeValues[1]);                    
        return fieldSize;
    }
    function checkBankAdd1RequiredVal(reqVal, reqCheck) {
           if(reqVal==false){                         
               alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("PaymentDefinitions.BankAdd1ReqText", TradePortalConstants.TEXT_BUNDLE))%>');
              var checkedObj = dijit.getEnclosingWidget(reqCheck);
              checkedObj.set('checked',false);
              return false;
           }  
           return true;
       }
    function resetPaymentFields(resetForm){
        var thisForm = resetForm;
        <%-- RKAZI Rel 8.2 T36000011727 - START --%>
        resetField(thisForm.ben_acct_num_req,thisForm.ben_acct_num_data_req,"ben_act_num", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_add1_req,thisForm.ben_add1_data_req,"ben_add1", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_add2_req,thisForm.ben_add2_data_req,"ben_add2", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_add3_req,thisForm.ben_add3_data_req,"ben_add3", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_country_req,thisForm.ben_country_data_req,"ben_country", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_bank_name_req,thisForm.ben_bank_name_data_req,"ben_bank_name", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_branch_code_req,thisForm.ben_branch_code_data_req,"ben_branch_code", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_branch_add1_req,thisForm.ben_branch_add1_data_req,"ben_branch_add1", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_branch_add2_req,thisForm.ben_branch_add2_data_req,"ben_branch_add2", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_bank_city_req,thisForm.ben_bank_city_data_req,"ben_bank_city", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_bank_province_req,thisForm.ben_bank_province_data_req,"ben_bank_province", "PmtDLMDefFileOrderTableId", "text");
        resetField(thisForm.ben_branch_country_req,thisForm.ben_branch_country_data_req,"ben_branch_country", "PmtDLMDefFileOrderTableId", "text");
        <%-- RKAZI Rel 8.2 T36000011727 - END --%>
    }
    function resetProCharType(prodCharType){
        var prodCharTypeLocal = registry.getEnclosingWidget(prodCharType);
        if (prodCharTypeLocal){
            prodCharTypeLocal.set('value', '');
        }
    }
    function resetField(reqField, reqDataField, fieldName, tableId, fieldType){
        var checkedObj = dijit.getEnclosingWidget(reqField);
        if (checkedObj){
            checkedObj.set('checked',false);
        }
        var checkedDataObj = dijit.getEnclosingWidget(reqDataField);
        if (checkedDataObj){
            checkedDataObj.set('checked',false);
        }
      var row = document.getElementById(fieldName+"_req_order");
      deleteOrderField(row, document.getElementById(tableId), fieldType);   

    }
    function deleteOrderField(deleteRow, table, idName){
          if(deleteRow){
        var tabSize = (table.rows.length)- (2); <%--  two row for header --%>
        var replaceRow = deleteRow.rowIndex;
        var tableId = table.id;

        for(var row= replaceRow; row <= tabSize; row++){
            table.rows[ row ].id = table.rows[ row + 1 ].id;
            table.rows[ row ].cells[0].childNodes[0].value = table.rows[ row+1 ].cells[0].childNodes[0].value;
            table.rows[ row ].cells[0].childNodes[1].value = table.rows[ row+1 ].cells[0].childNodes[1].value;
            if (tableId== "PmtFFInvDefFileOrderTableId" || tableId == "PmtFFDefFileOrderTableId" || tableId =="PmtFFHdrDefFileOrderTableId") {
                if("htext" == idName){
                    document.getElementById(('hupload_order_'+(row-1))).value=document.getElementById(('hupload_order_'+(row))).value;
                    var size = dom.byId(document.getElementById(('hsize'+(row-2))).value + "_size").value;
                    if (size.indexOf(",") != -1){
                        size = calPositionWithDecimals(size);
                    }                   
                    var newPosition = Number(document.getElementById(('hposition'+(row-2))).value) + Number(size);                    
                    document.getElementById(('hposition'+(row-1))).value=newPosition;
                    document.getElementById(('hsize'+(row-1))).value=document.getElementById(('hsize'+(row))).value;
                } else if ("ftext" == idName){
                    document.getElementById(('fupload_order_'+(row-1))).value=document.getElementById(('fupload_order_'+(row))).value;
                    var newPosition = Number(document.getElementById(('fposition'+(row-2))).value) + Number(dom.byId(document.getElementById(('fsize'+(row-2))).value + "_size").value);
                    document.getElementById(('fposition'+(row-1))).value=newPosition;
                    document.getElementById(('fsize'+(row-1))).value=document.getElementById(('fsize'+(row))).value;
                } else if("itext" == idName){
                    document.getElementById(('iupload_order_'+(row-1))).value=document.getElementById(('iupload_order_'+(row))).value;
                    var newPosition = Number(document.getElementById(('iposition'+(row-2))).value) + Number(dom.byId(document.getElementById(('isize'+(row-2))).value + "_size").value);
                    document.getElementById(('iposition'+(row-1))).value=newPosition;
                    document.getElementById(('isize'+(row-1))).value=document.getElementById(('isize'+(row))).value;
                }
                table.rows[ row ].cells[1].innerHTML = table.rows[ row+1 ].cells[1].innerHTML;

            }else{
                document.getElementById(('upload_order_'+(row-1))).value=document.getElementById(('upload_order_'+(row))).value;
                table.rows[ row ].cells[1].innerHTML = table.rows[ row+1 ].cells[1].innerHTML;
            }
        }
        
        table.deleteRow(tabSize+1); 
        var idNumberToDestroy = (table.rows.length) - (1);


        registry.byId(idName + idNumberToDestroy).value="";
        registry.byId(idName + idNumberToDestroy).destroy(true);
        if (tableId== "PmtFFInvDefFileOrderTableId" || tableId == "PmtFFDefFileOrderTableId" || tableId =="PmtFFHdrDefFileOrderTableId") {
             if("htext" == idName){
                 registry.byId("hposition" + idNumberToDestroy).value="";
                 registry.byId("hposition" + idNumberToDestroy).destroy(true);

             } else if ("ftext" == idName){
                 registry.byId("fposition" + idNumberToDestroy).value="";
                 registry.byId("fposition" + idNumberToDestroy).destroy(true);

             } else if("itext" == idName){
                 registry.byId("iposition" + idNumberToDestroy).value="";
                 registry.byId("iposition" + idNumberToDestroy).destroy(true);

             }
        }
      }
    }
    
         <%-- add most functions to thisPage for convenience of access --%>
         var thisPage = {
                 
                 selectDLMPmtSummItem: function(checkboxWidget) {

                     var invSummDataTable = document.getElementById("PmtDLMDefFileOrderTableId");
                     var rowCount = invSummDataTable.rows.length;
                     var row = invSummDataTable.insertRow(rowCount);
                     
                     var cell1 = row.insertCell(0);
                     var cell2 = row.insertCell(1);
                    
                     var summOrderCount = rowCount-1;
                     var DefCharCheckbox = new dijit.form.TextBox({
                            name: "",
                            id: "text"+ summOrderCount,
                            style:"width: 25px;",
                            value: summOrderCount
                        });

                        cell1.appendChild(DefCharCheckbox.domNode);
                        cell1.align="center";
                        var val1 = document.createElement("input"); 
                        val1.type = 'hidden';
                        val1.id = 'upload_order_'+summOrderCount;
                        val1.name = 'pmt_mthd_summary_order'+summOrderCount;
                                                                                           
                        cell2.setAttribute("id","fileFieldName"+summOrderCount);
                        cell2.setAttribute("align","left");
                                            
                        var checkboxId = checkboxWidget.id;
                        row.id = checkboxId + "_order";
                        var idx = checkboxId.indexOf("_req");
                       
                       var val2 = document.createElement("span"); 
                       val2.className = "deleteSelectedItem";  
                       val2.style.display = "";
                       val2.style.verticalAlign="top";
                        if( idx != -1){   
                            
                             var addSectionId = checkboxId.substring(0,idx);                        
                             val1.value = addSectionId;
                             cell1.appendChild(val1);
                             cell2.innerHTML = '<label class=\"pageSubHeaderItem\" style=\"width: 50% ;padding: 3px 0.1em 0px 1px;\">' + document.getElementById(addSectionId).value + '</label>'+'<span id=\"'+addSectionId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';  
                             
                             <%--  reorder invoice summary goods anf line item order --%>
                             reorderInvDefFileOrder();

                        }   
                        
                       
                 },

             selectFFPmtSummItem: function(checkboxWidget) {

                 var invSummDataTable = document.getElementById("PmtFFDefFileOrderTableId");
                 var rowCount = invSummDataTable.rows.length;
                 var row = invSummDataTable.insertRow(rowCount);

                 var cell1 = row.insertCell(0);
                 var cell2 = row.insertCell(1);
                 var cell3 = row.insertCell(2);

                 var summOrderCount = rowCount-1;
                 var DefCharCheckbox = new dijit.form.TextBox({
                     name: "",
                     id: "ftext"+ summOrderCount,
                     style:"width: 25px;",
                     value: summOrderCount
                 });

                 cell1.appendChild(DefCharCheckbox.domNode);
                 cell1.align="center";
                 var val1 = document.createElement("input");
                 val1.type = 'hidden';
                 val1.id = 'fupload_order_'+summOrderCount;
                 val1.name = 'fpmt_pay_summary_order'+summOrderCount;

                 cell2.setAttribute("id","ffileFieldName"+summOrderCount);
                 cell2.setAttribute("align","left");

                 var prevFieldSize = dom.byId(dom.byId("fsize"+(rowCount-2)).value+ "_size").value;
                 var fieldPosition = Number(dom.byId("fposition"+(rowCount-2)).value) + Number(prevFieldSize);
                 var DefCharCheckbox1 = new dijit.form.TextBox({
                     name: "",
                     id: "fposition"+ summOrderCount,
                     style:"width: 30px; border-color: #ffffff; background-color: #ffffff;",
                     value: fieldPosition,
                     readOnly : "readOnly"
                 });
                 domClass.add(DefCharCheckbox1.domNode,"ordField");
                 cell3.appendChild(DefCharCheckbox1.domNode);
                 cell3.align="right";
                 var val3 = document.createElement("input");
                 val3.type = 'hidden';
                 val3.id = 'fsize'+summOrderCount;
                 val3.name = 'fsize'+summOrderCount;


                 var checkboxId = checkboxWidget.id;
                 row.id = checkboxId + "_order";
                 var idx = checkboxId.indexOf("_req");

                 var val2 = document.createElement("span");
                 val2.className = "deleteSelectedItem";
                 val2.style.display = "";
                 val2.style.verticalAlign="top";
                 if( idx != -1){

                     var addSectionId = checkboxId.substring(0,idx);


                     if (addSectionId.indexOf("f") == 0){
                         val1.value = addSectionId.substr(1);
                         val3.value = addSectionId.substr(1);
                     } else {
                         val1.value = addSectionId;
                         val3.value = addSectionId;
                     }
                     cell1.appendChild(val1);
                     cell3.appendChild(val3);
                     cell2.innerHTML = '<label class=\"pageSubHeaderItem\" style=\"width: 50% ;padding: 3px 0.1em 0px 1px;\">' + document.getElementById(addSectionId).value + '</label>'+'<span id=\"'+addSectionId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';

                     <%--  reorder invoice summary goods anf line item order --%>
                     reorderInvDefFileOrder();

                 }

             },
             selectFFPmtHdrSummItem: function(checkboxWidget) {

                 var invSummDataTable = document.getElementById("PmtFFHdrDefFileOrderTableId");
                 
                 var rowCount = invSummDataTable.rows.length;                
                 var row = invSummDataTable.insertRow(rowCount);

                 var cell1 = row.insertCell(0);
                 var cell2 = row.insertCell(1);
                 var cell3 = row.insertCell(2);

                 var summOrderCount = rowCount-1;
                 var DefCharCheckbox = new dijit.form.TextBox({
                     name: "",
                     id: "htext"+ summOrderCount,
                     style:"width: 25px;",
                     value: summOrderCount
                 });
    
                 cell1.appendChild(DefCharCheckbox.domNode);
                 cell1.align="center";
                 var val1 = document.createElement("input");
                 val1.type = 'hidden';
                 val1.id = 'hupload_order_'+summOrderCount;
                 val1.name = 'hpmt_hdr_summary_order'+summOrderCount;

                 cell2.setAttribute("id","hfileFieldName"+summOrderCount);
                 cell2.setAttribute("align","left");
                 var prevFieldSize = dom.byId(dom.byId("hsize"+(rowCount-2)).value+ "_size").value;
                 
                 <%-- MEer Rel 9.0 CR-921 Calculate position value when size has decimal format --%>
                 if (prevFieldSize.indexOf(",") != -1){
                     var preSizeValue = prevFieldSize.split(",");
                     prevFieldSize = Number(preSizeValue[0]) + 1 + Number(preSizeValue[1]);                    
                 }
                 
                 var fieldPosition = Number(dom.byId("hposition"+(rowCount-2)).value) + Number(prevFieldSize); 
                 var DefCharCheckbox1 = new dijit.form.TextBox({
                     name: "",
                     id: "hposition"+ summOrderCount,
                     style:"width: 30px; border-color: #ffffff; background-color: #ffffff; ",
                     value: fieldPosition,
                     readOnly: "readOnly"
                });
                 domClass.add(DefCharCheckbox1.domNode,"ordField");
                 cell3.appendChild(DefCharCheckbox1.domNode);
                 cell3.align="right";
                 var val3 = document.createElement("input");
                 val3.type = 'hidden';
                 val3.id = 'hsize'+summOrderCount;
                 val3.name = 'hsize'+summOrderCount;
                 <%-- cell2.setAttribute("class","formItem"); --%>
                 <%--  cell2.style.margin= "0px -4px 2px 2px"; --%>

                 var checkboxId = checkboxWidget.id;
                 row.id = checkboxId + "_order";
                 var idx = checkboxId.indexOf("_req");

                 var val2 = document.createElement("span");
                 val2.className = "deleteSelectedItem";
                 val2.style.display = "";
                 val2.style.verticalAlign="top";
                 if( idx != -1){

                     var addSectionId = checkboxId.substring(0,idx);
                    
                     if (addSectionId.indexOf("h") == 0){
                        val1.value = addSectionId.substr(1);
                        val3.value = addSectionId.substr(1);
                     } else {
                        val1.value = addSectionId;
                        val3.value = addSectionId;
                     }                   
                     cell1.appendChild(val1);
                     cell3.appendChild(val3);
                     cell2.innerHTML = '<label class=\"pageSubHeaderItem\" style=\"width: 50% ;padding: 3px 0.1em 0px 1px;\">' + document.getElementById(addSectionId).value + '</label>'+'<span id=\"'+addSectionId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';

                     <%--  reorder invoice summary goods anf line item order --%>
                     reorderInvDefFileOrder();

                 }

             },
             selectFFPmtInvSummItem: function(checkboxWidget) {
                 
                 var invSummDataTable = document.getElementById("PmtFFInvDefFileOrderTableId");
                 var rowCount = invSummDataTable.rows.length;
                 var row = invSummDataTable.insertRow(rowCount);

                 var cell1 = row.insertCell(0);
                 var cell2 = row.insertCell(1);
                 var cell3 = row.insertCell(2);

                 var summOrderCount = rowCount-1;
                 var DefCharCheckbox = new dijit.form.TextBox({
                     name: "",
                     id: "itext"+ summOrderCount,
                     style:"width: 25px;",
                     value: summOrderCount
                 });

                 cell1.appendChild(DefCharCheckbox.domNode);
                 cell1.align="center";
                 var val1 = document.createElement("input");
                 val1.type = 'hidden';
                 val1.id = 'iupload_order_'+summOrderCount;
                 val1.name = 'ipmt_inv_summary_order'+summOrderCount;

                 cell2.setAttribute("id","ifileFieldName"+summOrderCount);
                 cell2.setAttribute("align","left");

                 var prevFieldSize;
                 var fieldPosition;
                 if (rowCount == 2){
                     fieldPosition = Number("1") ;
                 }else{
                     prevFieldSize = dom.byId(dom.byId("isize"+(rowCount-2)).value+ "_size").value;
                     fieldPosition = Number(dom.byId("iposition"+(rowCount-2)).value) + Number(prevFieldSize);
                 }
                 var DefCharCheckbox1 = new dijit.form.TextBox({
                     name: "",
                     id: "iposition"+ summOrderCount,
                     style:"width: 30px; border-color: #ffffff; background-color: #ffffff;",
                     value: fieldPosition,
                     readOnly :"readOnly"
                 });
                 domClass.add(DefCharCheckbox1.domNode,"ordField");

                 cell3.appendChild(DefCharCheckbox1.domNode);
                 cell3.align="right";
                 var val3 = document.createElement("input");
                 val3.type = 'hidden';
                 val3.id = 'isize'+summOrderCount;
                 val3.name = 'isize'+summOrderCount;


                 var checkboxId = checkboxWidget.id;
                 row.id = checkboxId + "_order";
                 var idx = checkboxId.indexOf("_req");

                 var val2 = document.createElement("span");
                 val2.className = "deleteSelectedItem";
                 val2.style.display = "";
                 val2.style.verticalAlign="top";
                 if( idx != -1){

                     var addSectionId = checkboxId.substring(0,idx);


                     if (addSectionId.indexOf("i") == 0){
                         val1.value = addSectionId.substr(1);
                         val3.value = addSectionId.substr(1);                      
                     } else {
                         val1.value = addSectionId;
                         val3.value = addSectionId;
                     }
                     cell1.appendChild(val1);
                     cell3.appendChild(val3);
                     cell2.innerHTML = '<label class=\"pageSubHeaderItem\" style=\"width: 50% ;padding: 3px 0.1em 0px 1px;\">' + document.getElementById(addSectionId).value + '</label>'+'<span id=\"'+addSectionId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';

                     <%--  reorder invoice summary goods anf line item order --%>
                     reorderInvDefFileOrder();

                 }

             },
             deleteEventHandler: function(deleteButton, tableID, orderId) {
                        <%-- uncheck the available item --%>
                        <%-- this automatically does the onchange mechanism there --%>
                        <%-- including removing this item and decreasing the total count --%>
                       <%--  var deleteSelectItem = deleteButton.parentNode; --%>
                        var deleteSelectItemId = deleteButton.id;
                        var userdef = deleteSelectItemId.indexOf("_label");
               
                        if(userdef != -1){
                            deleteSelectItemId = deleteSelectItemId.substring(0, userdef);                         
                        }   
                        var checkbox = registry.byId(deleteSelectItemId+'_req');  
                        if ( checkbox ) {
                          checkbox.set('checked',false);
                        }
                        var checkbox = registry.byId(deleteSelectItemId+'_data_req');
                        if ( checkbox ) {
                         checkbox.set('checked',false);
                        }
                        var row = document.getElementById(deleteSelectItemId + '_req_order');
                        deleteOrderField(row, tableID, orderId);    
                      }
         }
         function toggleLink(hideLink) {
             arrayUtil.forEach(dom.byId("navContainer").childNodes,lang.hitch(this, function(child){ 
                 var linkText = child.outerHTML;
                 if ("dlm" == hideLink){
                     if (linkText.indexOf("Delimited File Format") != -1){
                         child.style.display ='block'; 
                     }
                     if (linkText.indexOf("Fixed File Format") != -1){
                         child.style.display ='none'; 
                     }
                     
                 }
                 if ("fix" == hideLink){
                     if (linkText.indexOf("Delimited File Format") != -1){
                         child.style.display ='none'; 
                     }
                     if (linkText.indexOf("Fixed File Format") != -1){
                         child.style.display ='block'; 
                     }
                     
                 }
                 if ("none" == hideLink){
                     if (linkText.indexOf("Delimited File Format") != -1){
                         child.style.display ='none'; 
                     }
                     if (linkText.indexOf("Fixed File Format") != -1){
                         child.style.display ='none'; 
                     }
                     
                 }
                 
             }));
         }
         
        function setFieldsOptional(onload, fieldName) {
            var defaultRequired =[""];
            var excludeString = ["bene_account_number,bene_bank_branch_code,payable_location,print_location,delvry_mth_n_delvrto","beneficiary_country,bene_address1,beneficiary_country,central_bank_rep1","beneficiary_country,bene_address1,beneficiary_country,bene_account_number,bene_bank_branch_code,central_bank_rep1,payable_location,print_location,delvry_mth_n_delvrto"];
            var fieldReq = registry.byId(fieldName+"_req");
            var fieldDataReq = registry.byId(fieldName+"_data_req");
            var isCheckedFieldReq = fieldReq.checked && onload;
            var isCheckedFieldDataReq = fieldDataReq.checked && onload;
            var tdField = dom.byId("td_"+fieldName);
            fieldReq.set("style", "display: block;");
            fieldReq.set("checked", isCheckedFieldReq);
            fieldDataReq.set("style", "display: block;");
            fieldDataReq.set("checked", isCheckedFieldDataReq);
            if (tdField){
                var inHtml = tdField.innerHTML;
                
                inHtml =inHtml.substring(inHtml.indexOf("<label>"));
                if(navigator.appVersion.indexOf("MSIE 8")!=-1){
                    inHtml =inHtml.substring(inHtml.indexOf("<LABEL>"));    
                }
                markWidgetMandtory(tdField,domClass,false);
                tdField.innerHTML = inHtml;
            }


            var payMthd = registry.byId("payment_method").value;
            var dilimitFormat = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT");
            var fixedfFormat = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX");
            if (fixedfFormat.checked) {
                excludeString = ["fbene_account_number,fbene_bank_branch_code,fpayable_location,fprint_location,fdelvry_mth_n_delvrto",
                    "fbene_account_number,fbeneficiary_country,fbene_address1,fbeneficiary_country,fcentral_bank_rep1",
                    "fbene_account_number,fbene_bank_branch_code,fbeneficiary_country,fbene_address1,fbeneficiary_country,fpayable_location,fprint_location,fdelvry_mth_n_delvrto,fcentral_bank_rep1"];
            }

            if (onload){
                var row = dom.byId(fieldReq.id + "_order");

                if (row == undefined && isCheckedFieldReq) {
                    if (dilimitFormat.checked)
                        thisPage.selectDLMPmtSummItem(fieldReq);
                    if (fixedfFormat.checked) {
                        if (fieldName.indexOf("h") == 0){                        
                            thisPage.selectFFPmtHdrSummItem(fieldReq);
                        } else if (fieldName.indexOf("f") == 0){
                            thisPage.selectFFPmtSummItem(fieldReq);
                        } else if (fieldName.indexOf("i") == 0){
                            thisPage.selectFFPmtInvSummItem(fieldReq);
                        }
                    }
                }else{
                    var fieldId = fieldReq.id.replace("_req","");                   
                    console.log(fieldId);
                    if ((payMthd == "CCHK" || payMthd =="BCHK") && isCheckedFieldReq && (excludeString[1].indexOf(fieldName) == -1 )){


                        var inHtml = row.cells[1].innerHTML;
                        inHtml = inHtml + '<span id=\"'+fieldId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';
                        row.cells[1].innerHTML = inHtml;

                    } else if (payMthd =="CBFT" && isCheckedFieldReq && (excludeString[0].indexOf(fieldName) == -1 )) {


                        var inHtml = row.cells[1].innerHTML;
                        inHtml = inHtml + '<span id=\"'+fieldId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';
                        row.cells[1].innerHTML = inHtml;

                    } else if ((payMthd == "ACH" || payMthd =="BKT" || payMthd =="RTGS") && isCheckedFieldReq && (excludeString[2].indexOf(fieldName) == -1 )) {

                        var inHtml = row.cells[1].innerHTML;
                        inHtml = inHtml + '<span id=\"'+fieldId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';
                        row.cells[1].innerHTML = inHtml;
                    }
                }

            }else{

                var row = dom.byId(fieldReq.id + "_order");
                var orderTableId = "";
                var textField = "";
                if (dilimitFormat.checked){
                    orderTableId =  "PmtDLMDefFileOrderTableId";
                    textField = "text";
                }
                if (fixedfFormat.checked) {
                    if (fieldName.indexOf("h") == 0){
                        orderTableId =  "PmtFFHdrDefFileOrderTableId";
                        textField = "htext";
                    } else if (fieldName.indexOf("f") == 0){
                        orderTableId =  "PmtFFDefFileOrderTableId";
                        textField = "ftext";
                    } else if (fieldName.indexOf("i") == 0){
                        orderTableId =  "PmtFFInvDefFileOrderTableId";
                        textField = "itext";
                    }
                }
                deleteOrderField(row, dom.byId(orderTableId), textField);
            }

        }

        function setFieldsRequired(onload, fieldName) {
            var excludeString = ["bene_account_number, bene_bank_branch_code,payable_location,print_location,delvry_mth_n_delvrto","beneficiary_country,bene_address1,beneficiary_country","bene_account_number, bene_bank_branch_code,payable_location,print_location,delvry_mth_n_delvrto"];
            var fieldReq = registry.byId(fieldName+"_req");
            var fieldDataReq = registry.byId(fieldName+"_data_req");
            var tdField = dom.byId("td_"+fieldName);
            fieldReq.set("checked", true);
            fieldReq.set("style", "display: none;");
            fieldDataReq.set("style", "display: none;");
            fieldDataReq.set("checked", true);
            if (tdField){
                var inHtml = tdField.innerHTML;
                if (inHtml.indexOf("asterisk")==-1){
                    inHtml ='<span class=\"asterisk\">*</span> ' + inHtml;
                    markWidgetMandtory(tdField,domClass,true);
                }
                tdField.innerHTML = inHtml;
            }

            var payMthd = registry.byId("payment_method").value;
            var dilimitFormat = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT");
            var fixedfFormat = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX");
            if (fixedfFormat.checked) {
                excludeString = ["fbene_account_number,fbene_bank_branch_code,fpayable_location,fprint_location,fdelvry_mth_n_delvrto",
                                 "fbene_account_number,fbene_bank_branch_code,fbeneficiary_country,fbene_address1,fbeneficiary_country",
                                 "fbene_account_number,fbene_bank_branch_code,fbeneficiary_country,fbene_address1,fbeneficiary_country,fpayable_location,fprint_location,fdelvry_mth_n_delvrto"];
            }
                var row = dom.byId(fieldReq.id + "_order");
            if (row == undefined) {
                if (dilimitFormat.checked)
                    thisPage.selectDLMPmtSummItem(fieldReq);
                if (fixedfFormat.checked) {
                    if (fieldName.indexOf("h") == 0){                       
                        thisPage.selectFFPmtHdrSummItem(fieldReq);
                    } else if (fieldName.indexOf("f") == 0){
                        thisPage.selectFFPmtSummItem(fieldReq);
                    } else if (fieldName.indexOf("i") == 0){
                        thisPage.selectFFPmtInvSummItem(fieldReq);
                    }
                }

                if (payMthd != "CCHK" || payMthd !="BCHK" || payMthd !="CBFT" || payMthd !="ACH" || payMthd !="BKT" || payMthd !="RTGS"){
                    row = dom.byId(fieldReq.id + "_order");
                    var inHtml = row.cells[1].innerHTML;
                    if(navigator.appVersion.indexOf("MSIE 8")!=-1){
                        if (inHtml.indexOf("<SPAN") != -1){
                            inHtml = inHtml.substring(0, inHtml.indexOf("<SPAN"));
                            row.cells[1].innerHTML = inHtml;
                        }
                    }else {
                        if (inHtml.indexOf("<span") != -1){
                            inHtml = inHtml.substring(0, inHtml.indexOf("<span"));
                            row.cells[1].innerHTML = inHtml;
                        }
                    }
                }
            } else {
                var row = dom.byId(fieldReq.id + "_order");
                var inHtml = row.cells[1].innerHTML;
                var fId = fieldReq.id.replace("_req","");
                if (inHtml.indexOf("deleteSelectedItem") == -1){
                    if ((payMthd == "CCHK" || payMthd =="BCHK") && (excludeString[1].indexOf(fieldName) == -1 )){

                        inHtml = inHtml + '<span id=\"'+fId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';
                        row.cells[1].innerHTML = inHtml;
                    } else if (payMthd =="CBFT" && (excludeString[0].indexOf(fieldName) == -1 )){
                        inHtml = inHtml + '<span id=\"'+fId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';
                        row.cells[1].innerHTML = inHtml;
                    } else if ((payMthd == "ACH" || payMthd =="BKT" || payMthd =="RTGS") && (excludeString[2].indexOf(fieldName) == -1 )) {
                        inHtml = inHtml + '<span id=\"'+fId+'\" class=\"deleteSelectedItem\" style=\"margin: 0.3em 1em;\"/>';
                        row.cells[1].innerHTML = inHtml;
                    }
                }else{
                    if(navigator.appVersion.indexOf("MSIE 8")!=-1){
                        if (inHtml.indexOf("<SPAN") != -1){
                            inHtml = inHtml.substring(0, inHtml.indexOf("<SPAN"));
                            row.cells[1].innerHTML = inHtml;
                        }
                    }else {
                        if (inHtml.indexOf("<span") != -1){
                            inHtml = inHtml.substring(0, inHtml.indexOf("<span"));
                            row.cells[1].innerHTML = inHtml;
                        }
                    }
                }
            }
        }
        
        
        function setMandatoryFields(onload){
            var dilimitFormat = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT");
            var fixedfFormat = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX");
            var payment_method = registry.byId("payment_method");
            
            var selecValue = payment_method.value;
            if ('CCHK' == selecValue || 'BCHK' == selecValue){
                if (dilimitFormat.checked){
                    setFieldsOptional(onload,"bene_account_number");
                    setFieldsOptional(onload,"bene_bank_branch_code");
                    setFieldsOptional(onload,"beneficiary_country");
                    setFieldsOptional(onload,"bene_address1");
                    setFieldsOptional(onload,"central_bank_rep1");
                    setFieldsRequired(onload,"payable_location");
                    setFieldsRequired(onload,"print_location");
                    setFieldsRequired(onload,"delvry_mth_n_delvrto");
                }else if (fixedfFormat.checked){
                    setFieldsOptional(onload,"fbene_account_number");
                    setFieldsOptional(onload,"fbene_bank_branch_code");
                    setFieldsOptional(onload,"fbeneficiary_country");
                    setFieldsOptional(onload,"fbene_address1");
                    setFieldsOptional(onload,"fcentral_bank_rep1");
                    setFieldsRequired(onload,"fpayable_location");
                    setFieldsRequired(onload,"fprint_location");
                    setFieldsRequired(onload,"fdelvry_mth_n_delvrto");

                }
            }else if ('CBFT' == selecValue){
                if (dilimitFormat.checked){
                    setFieldsRequired(onload,"bene_account_number");
                    setFieldsRequired(onload,"bene_bank_branch_code");
                    setFieldsRequired(onload,"beneficiary_country");
                    setFieldsRequired(onload,"bene_address1");
                    setFieldsRequired(onload,"central_bank_rep1");
                    setFieldsOptional(onload,"payable_location");
                    setFieldsOptional(onload,"print_location");
                    setFieldsOptional(onload,"delvry_mth_n_delvrto");
                }else if (fixedfFormat.checked){
                    setFieldsRequired(onload,"fbene_account_number");
                    setFieldsRequired(onload,"fbene_bank_branch_code");
                    setFieldsRequired(onload,"fbeneficiary_country");
                    setFieldsRequired(onload,"fbene_address1");
                    setFieldsRequired(onload,"fcentral_bank_rep1");
                    setFieldsOptional(onload,"fpayable_location");
                    setFieldsOptional(onload,"fprint_location");
                    setFieldsOptional(onload,"fdelvry_mth_n_delvrto");

                }
            }else if ('ACH' == selecValue || 'BKT' == selecValue || 'RTGS' == selecValue ){
                if (fixedfFormat.checked){
                    setFieldsRequired(onload,"fbene_account_number");
                    setFieldsRequired(onload,"fbene_bank_branch_code");
                    setFieldsOptional(onload,"fbeneficiary_country");
                    setFieldsOptional(onload,"fbene_address1");
                    setFieldsOptional(onload,"fcentral_bank_rep1");
                    setFieldsOptional(onload,"fpayable_location");
                    setFieldsOptional(onload,"fprint_location");
                    setFieldsOptional(onload,"fdelvry_mth_n_delvrto");
                }else if (dilimitFormat.checked){
                    setFieldsRequired(onload,"bene_account_number");
                    setFieldsRequired(onload,"bene_bank_branch_code");
                    setFieldsOptional(onload,"beneficiary_country");
                    setFieldsOptional(onload,"bene_address1");
                    setFieldsOptional(onload,"central_bank_rep1");
                    setFieldsOptional(onload,"payable_location");
                    setFieldsOptional(onload,"print_location");
                    setFieldsOptional(onload,"delvry_mth_n_delvrto");
                }
            }else{
                if (dilimitFormat.checked){
                    setFieldsRequired(onload,"bene_account_number");
                    setFieldsRequired(onload,"bene_bank_branch_code");
                    setFieldsOptional(onload,"beneficiary_country");
                    setFieldsOptional(onload,"bene_address1");
                    setFieldsOptional(onload,"central_bank_rep1");
                    setFieldsOptional(onload,"payable_location");
                    setFieldsOptional(onload,"print_location");
                    setFieldsOptional(onload,"delvry_mth_n_delvrto");
                } else if (fixedfFormat.checked){
                    setFieldsOptional(onload,"fbene_account_number");
                    setFieldsOptional(onload,"fbene_bank_branch_code");
                    setFieldsOptional(onload,"fbeneficiary_country");
                    setFieldsOptional(onload,"fbene_address1");
                    setFieldsOptional(onload,"fcentral_bank_rep1");
                    setFieldsOptional(onload,"fpayable_location");
                    setFieldsOptional(onload,"fprint_location");
                    setFieldsOptional(onload,"fdelvry_mth_n_delvrto");

                }

            }
        }
        
        

 
                
    
             
                    ready(function() {
                console.log(("hello in 0 arg ready"));
             var fileFormatTypeFF = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX");
             var fileFormatTypeDLM = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT");
             var isABADef = <%=isABADef%>;
             if(isABADef == true) {
                
                 var name = registry.byId("name");
               
                 if (name){
                     <%-- name.readOnly = true; --%>
                     name.set("readOnly", true);
                 }
                 var description = registry.byId("description");
                 if (description){
                     <%-- description.readOnly = true; --%>
                     description.set("readOnly", true);
                 }
                 
                 var payment_method = registry.byId("payment_method");
                 if (payment_method){
                     
                     <%-- payment_method.readonly = true; --%>
                     payment_method.set("readOnly", true);
                 }
                 var date_format = registry.byId("date_format");
                 if (date_format){
                     <%-- date_format.readonly = true; --%>
                     date_format.set("readOnly", true);
                 }
                 if (fileFormatTypeDLM){
                     <%-- fileFormatTypeDLM.readOnly = true; --%>
                     fileFormatTypeDLM.set("readOnly", true);
                 }
                 if (fileFormatTypeFF){
                     <%-- fileFormatTypeFF.readOnly = true; --%>
                     fileFormatTypeFF.set("readOnly", true);
                 }
                 var delimiter_char = registry.byId("delimiter_char");
                 if (delimiter_char){
                     <%-- delimiter_char.readonly = true; --%>
                     delimiter_char.set("readOnly", true);
                 }
                 var include_column_headers = registry.byId("include_column_headers");
                 if (include_column_headers){
                     <%-- include_column_headers.readOnly = true; --%>
                     include_column_headers.set("readOnly", true);
                 }

             }
             var dlmDiv = dom.byId("DLMFFDiv");
             var ffDiv = dom.byId("FFFDiv");
             if (!(fileFormatTypeDLM.checked && fileFormatTypeFF.checked)){
                 toggleLink("none");
                 if (dlmDiv){
                     dlmDiv.style.display = "none";
                 }
                 if (ffDiv){
                     ffDiv.style.display = "none";
                 }
             }

             if (fileFormatTypeFF){
                 if ( fileFormatTypeFF.checked ) {
                     toggleLink("fix");
                     if (dlmDiv){
                         dlmDiv.style.display = "none";
                     }
                     
                     if (ffDiv){
                         ffDiv.style.display = "block";
                     }
                 }
             }
             if (fileFormatTypeDLM){
                 if ( fileFormatTypeDLM.checked ) {
                     toggleLink("dlm");
                     if (dlmDiv){
                         dlmDiv.style.display = "block";
                     }
                     if (ffDiv){
                         ffDiv.style.display = "none";
                     }
                 }
             }

             var invTypeIndicator = registry.byId("TradePortalConstants.INVOICE_TYPE_REC_MGMT");
               if (invTypeIndicator) {
                   invTypeIndicator.on("change", function(checkValue) {
                       if ( checkValue ) {
                            document.getElementById('buyerId').style.display  = '';
                            document.getElementById('buyerName').style.display  = '';
                            document.getElementById('sellerId').style.display  = 'none';
                            document.getElementById('sellerName').style.display  = 'none';
                            registry.getEnclosingWidget(document.getElementById('seller_id_req')).set('checked', false);
                            registry.getEnclosingWidget(document.getElementById('seller_name_req')).set('checked', false);
                       }
                    });
               }
               var invTypeIndicator = registry.byId("TradePortalConstants.INVOICE_TYPE_PAY_MGMT");
               if (invTypeIndicator) {
                   invTypeIndicator.on("change", function(checkValue) {
                       if ( checkValue ) {
                            
                                document.getElementById('sellerId').style.display  = '';
                                document.getElementById('sellerName').style.display  = '';
                                document.getElementById('buyerId').style.display  = 'none';
                                document.getElementById('buyerName').style.display  = 'none';
                                registry.getEnclosingWidget(document.getElementById('buyer_id_req')).set('checked', false);
                                registry.getEnclosingWidget(document.getElementById('buyer_name_req')).set('checked', false);
                            
                       }
                    });
               }
             
             var fixedFileFormatType = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX");
             if (fixedFileFormatType) {
                 fixedFileFormatType.on("click", function(checkValue) {
                     if ( this.checked ) {
                         toggleLink("fix");
                         var dlmDiv = dom.byId("DLMFFDiv");
                         if (dlmDiv){
                             dlmDiv.style.display = "none";
                         }
                         var ffDiv = dom.byId("FFFDiv");
                         if (ffDiv){
                             if (isFFDivLoaded){
                                 ffDiv.style.display = "block";
                             }else{
                                
                                <%-- fetchDefinition('FFFDiv','<%=paymentDefOid%>','<%=retrieveFromDB%>','<%=getDataFromDoc%>') ; --%>
                                isFFDivLoaded = true;
                                ffDiv.style.display = "block";
                             }
                             
                         }
                     }

                 });
             }
             var dlmFileFormatType = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT");
             if (dlmFileFormatType) {
                 dlmFileFormatType.on("click", function(checkValue) {
                     if ( this.checked ) {
                         toggleLink("dlm");
                         var dlmDiv = dom.byId("DLMFFDiv");
                         if (dlmDiv){
                             if (isDLMDivLoaded){
                                 dlmDiv.style.display = "block";
                             }else{
                                <%-- fetchDefinition('DLMFFDiv','<%=paymentDefOid%>','<%=retrieveFromDB%>','<%=getDataFromDoc%>') ; --%>
                                isDLMDivLoaded = true;
                                dlmDiv.style.display = "block";
                             }
                             

                         }
                         var ffDiv = dom.byId("FFFDiv");
                         if (ffDiv){
                             ffDiv.style.display = "none";

                         }

                     }

                 });
             }

             var payment_method = registry.byId("payment_method");
             if (payment_method){
                 setMandatoryFields(true);
                 }
                 var dlmFormat = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT");
                 var ffFormat = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX");
                 dlmFormat.on("change", function(checkValue) {
                     if (!isDLMDivLoaded){
                            <%-- fetchDefinition('DLMFFDiv','<%=paymentDefOid%>','<%=retrieveFromDB%>','<%=getDataFromDoc%>') ; --%>
                            isDLMDivLoaded = true;
                            
                     }
                     setMandatoryFields(false);
                 });
                 ffFormat.on("change", function(checkValue) {
                     if (!isFFDivLoaded){
                         <%-- fetchDefinition('FFFDiv','<%=paymentDefOid%>','<%=retrieveFromDB%>','<%=getDataFromDoc%>') ; --%>
                         isFFDivLoaded = true;
                  }
                     setMandatoryFields(false);
                 });
                 payment_method.on("change", function(checkValue) {
                    var selectedValue = checkValue;
                     var dlmFormat = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT");
                     var ffFormat = registry.byId("TradePortalConstants.PAYMENT_FILE_TYPE_FIX");

                     if (selectedValue != '' ){
                         if ('CCHK' == selectedValue || 'BCHK' == selectedValue){
                               if (dlmFormat.checked){
                                   setFieldsOptional(false,"bene_account_number");
                                   setFieldsOptional(false,"bene_bank_branch_code");
                                   setFieldsOptional(false,"beneficiary_country");
                                   setFieldsOptional(false,"bene_address1");
                                   setFieldsOptional(false,"central_bank_rep1");
                                   setFieldsRequired(false,"payable_location");
                                   setFieldsRequired(false,"print_location");
                                   setFieldsRequired(false,"delvry_mth_n_delvrto");

                               }else if (ffFormat.checked){
                                   setFieldsOptional(false,"fbene_account_number");
                                   setFieldsOptional(false,"fbene_bank_branch_code");
                                   setFieldsOptional(false,"fbeneficiary_country");
                                   setFieldsOptional(false,"fbene_address1");
                                   setFieldsOptional(false,"fcentral_bank_rep1");
                                   setFieldsRequired(false,"fpayable_location");
                                   setFieldsRequired(false,"fprint_location");
                                   setFieldsRequired(false,"fdelvry_mth_n_delvrto");

                               }
                         }else if ('CBFT' == selectedValue){
                             if (dlmFormat.checked){
                                 setFieldsRequired(false,"bene_account_number");
                                 setFieldsRequired(false,"bene_bank_branch_code");
                                 setFieldsRequired(false,"beneficiary_country");
                                 setFieldsRequired(false,"bene_address1");
                                 setFieldsRequired(false,"central_bank_rep1");
                                 setFieldsOptional(false,"payable_location");
                                 setFieldsOptional(false,"print_location");
                                 setFieldsOptional(false,"delvry_mth_n_delvrto");
                             }else if (ffFormat.checked){
                                 setFieldsRequired(false,"fbene_account_number");
                                 setFieldsRequired(false,"fbene_bank_branch_code");
                                 setFieldsRequired(false,"fbeneficiary_country");
                                 setFieldsRequired(false,"fbene_address1");
                                 setFieldsRequired(false,"fcentral_bank_rep1");
                                 setFieldsOptional(false,"fpayable_location");
                                 setFieldsOptional(false,"fprint_location");
                                 setFieldsOptional(false,"fdelvry_mth_n_delvrto");

                             }
                         }else if ('ACH' == selectedValue || 'BKT' == selectedValue || 'RTGS' == selectedValue ){
                              if (ffFormat.checked){
                                  setFieldsRequired(false,"fbene_account_number");
                                  setFieldsRequired(false,"fbene_bank_branch_code");
                                  setFieldsOptional(false,"fbeneficiary_country");
                                  setFieldsOptional(false,"fbene_address1");
                                  setFieldsOptional(false,"fcentral_bank_rep1");
                                  setFieldsOptional(false,"fpayable_location");
                                  setFieldsOptional(false,"fprint_location");
                                  setFieldsOptional(false,"fdelvry_mth_n_delvrto");
                              } else if (dlmFormat.checked){
                                  setFieldsRequired(false,"bene_account_number");
                                  setFieldsRequired(false,"bene_bank_branch_code");
                                  setFieldsOptional(false,"beneficiary_country");
                                  setFieldsOptional(false,"bene_address1");
                                  setFieldsOptional(false,"central_bank_rep1");
                                  setFieldsOptional(false,"payable_location");
                                  setFieldsOptional(false,"print_location");
                                  setFieldsOptional(false,"delvry_mth_n_delvrto");

                              }
                         }else{
                             if (dlmFormat.checked){
                                 setFieldsRequired(false,"bene_account_number");
                                 setFieldsRequired(false,"bene_bank_branch_code");
                                 setFieldsOptional(false,"beneficiary_country");
                                 setFieldsOptional(false,"bene_address1");
                                 setFieldsOptional(false,"central_bank_rep1");
                                 setFieldsRequired(false,"payment_currency");
                                 setFieldsRequired(false,"payment_amount");
                                 setFieldsOptional(false,"payable_location");
                                 setFieldsOptional(false,"print_location");
                                 setFieldsOptional(false,"delvry_mth_n_delvrto");

                             } else if (ffFormat.checked){
                                 setFieldsOptional(false,"fbene_account_number");
                                 setFieldsOptional(false,"fbene_bank_branch_code");
                                 setFieldsOptional(false,"fbeneficiary_country");
                                 setFieldsOptional(false,"fbene_address1");
                                 setFieldsOptional(false,"fcentral_bank_rep1");
                                 setFieldsOptional(false,"fpayable_location");
                                 setFieldsOptional(false,"fprint_location");
                                 setFieldsOptional(false,"fdelvry_mth_n_delvrto");

                             }
                         }
                     }
                 });
                 var isReadOnly = <%=isReadOnly%>;
                 if (isReadOnly){
                    query("input[id$='_size']").forEach(function(node){
                        node.disabled='disabled';
                    });
                }            

             dojo.query("#PmtFFDefTableId").query("input[id$='_size']").on("change", function(checkValue) {
                 var srcElement = checkValue.currentTarget;
                 if (srcElement){
                     if (srcElement.id == 'payment_amount_size'){
                         if (srcElement.value.indexOf(",") == -1){
                             var val = srcElement.value;
                             val = val + ",3";
                             srcElement.value = val;
                         }
                     }
                     
                 }
                 var totalPOSummGoodOrderDara = (document.getElementById("PmtFFDefFileOrderTableId").rows.length) - (2); <%--  two row for header --%>
                 for(var row=1; row<=totalPOSummGoodOrderDara; row++){
                     <%-- swap position value --%>
                     var a;
                     if (row == 1){
                         a = Number(1);
                     } else{
                         temp_position_value = document.getElementById("fposition"+(row-1)).value;
                         var fieldName = document.getElementById('fsize'+(row-1)).value;
                         var tempSize = Number(document.getElementById(document.getElementById('fsize'+(row-1)).value+"_size").value);
                         if (fieldName == 'payment_amount'){
                             var tempVal = document.getElementById(fieldName+"_size").value;

                             if (tempVal){
                                 tempVal = tempVal.substring(0, tempVal.indexOf(","));
                                 tempSize = Number(tempVal) + 4;
                             }
                         }
                         a = Number(temp_position_value ) + tempSize;
                     }
                     document.getElementById("fposition"+row).value = a;
                 }

             });
             dojo.query("#PmtFFDefHdrTableId").query("input[id$='_size']").on("change", function(checkValue) {
                 var totalPOSummGoodOrderDara = (document.getElementById("PmtFFHdrDefFileOrderTableId").rows.length) - (2); <%--  two row for header               --%>
                 for(var row=1; row<=totalPOSummGoodOrderDara; row++){
                     <%-- swap position value --%>
                     var a;
                     if (row == 1){
                         a = Number(1);
                     } else{
                         temp_position_value = document.getElementById("hposition"+(row-1)).value;                         
                         var size = document.getElementById(document.getElementById('hsize'+(row-1)).value+"_size").value;
                         if (size.indexOf(",") != -1){
                            size = calPositionWithDecimals(size);
                         } 
                         a = Number(temp_position_value ) + Number(size);                       
                     }
                     document.getElementById("hposition"+row).value = a;
                 }
                
                 var srcElement = checkValue.currentTarget;
                 if (srcElement){
                     if (srcElement.id == 'fx_contract_rate_size'){
                         if (srcElement.value.indexOf(",") == -1){
                             var val = srcElement.value;
                             val = val + ",8";
                             srcElement.value = val;
                         }
                     }
                     
                 }
                 

             });
             dojo.query("#PmtFFDefInvTableId").query("input[id$='_size']").on("change", function(checkValue) {
                 var totalPOSummGoodOrderDara = (document.getElementById("PmtFFInvDefFileOrderTableId").rows.length) - (2); <%--  two row for header --%>
                 for(var row=1; row<=totalPOSummGoodOrderDara; row++){
                     <%-- swap position value --%>
                     var a;
                     if (row == 1){
                         a = Number(1);
                     } else{
                         temp_position_value = document.getElementById("iposition"+(row-1)).value;                         
                         a = Number(temp_position_value ) + Number(document.getElementById(document.getElementById('isize'+(row-1)).value+"_size").value);
                     }
                     document.getElementById("iposition"+row).value = a;
                 }

             });
             dojo.query("#PmtDLMDefTableId").query("input[id$='_req']").on("click", function(checkValue) {
                    var srcElement = checkValue.currentTarget;
                    if (srcElement){
                        var id = srcElement.id;
                        if (id.indexOf('data_req') != -1){
                            return;
                        }
                        if (this.checked){

                            if(checkInvLabelLabelVal(dom.byId(id).value, srcElement,
                                    "PaymentDefinitions.Label")){
                                thisPage.selectDLMPmtSummItem(this);
                               
                                if(id =="fx_contract_number_req"){                                      
                                    var fxContractRate =registry.byId('fx_contract_rate_req');                                   
                                     fxContractRate.set('checked', true);
                                     thisPage.selectDLMPmtSummItem(fxContractRate);                                     
                                }
                                else if(id =="fx_contract_rate_req"){                                    
                                    var fxContractNumber = registry.byId('fx_contract_number_req');                                     
                                    fxContractNumber.set('checked', true);
                                    thisPage.selectDLMPmtSummItem(fxContractNumber);                
                                }
                            }
                        } else {
                            unsetDataDelDLMOrder(id);                                               
                          
                            if(id =="fx_contract_number_req"){
                                var fxContractRate =registry.byId('fx_contract_rate_req');
                                fxContractRate.set('checked', false);                               
                                unsetDataDelDLMOrder(fxContractRate.id);                                                            
                            }
                            else if(id =="fx_contract_rate_req"){
                                 var fxContractNumber = registry.byId('fx_contract_number_req');
                                fxContractNumber.set('checked', false);                                 
                                unsetDataDelDLMOrder(fxContractNumber.id);              
                            }                 
                        }

                    }

                });
             dojo.query("#PmtDLMDefTableId").query("input[id$='_data_req']").on("click", function(checkValue) {
                 var srcElement = checkValue.currentTarget;
                 if (srcElement){
                     var id = srcElement.id;

                     if (id.indexOf('data_req') == -1){
                         return;
                     }
                     if (this.checked){
                         id = id.replace('_data','');
                         checkRequiredVal(dom.byId(id).checked, srcElement);

                     }

                 }

             });

             dojo.query("#PmtFFDefTableId").query("input[id$='_req']").on("click", function(checkValue) {
                 var srcElement = checkValue.currentTarget;
                 if (srcElement){
                     var id = srcElement.id;
                     if (id.indexOf('data_req') != -1){
                         return;
                     }
                     if (this.checked){

                         if(checkInvLabelLabelVal(dom.byId(id).value, srcElement,
                                 "PaymentDefinitions.Label")){
                             thisPage.selectFFPmtSummItem(this);                            
                         }
                     } else {
                         var tempId = id;
                         unsetDataReq(dom.byId(tempId.replace('_req','_data_req')));
                         var row = dom.byId(id + "_order");
                         deleteOrderField(row, dom.byId("PmtFFDefFileOrderTableId"), "ftext");
                     }

                 }

             });
             dojo.query("#PmtFFDefTableId").query("input[id$='_data_req']").on("click", function(checkValue) {
                 var srcElement = checkValue.currentTarget;
                 if (srcElement){
                     var id = srcElement.id;

                     if (id.indexOf('data_req') == -1){
                         return;
                     }
                     if (this.checked){
                         id = id.replace('_data','');
                         if (id.indexOf("_label") != -1) {
                             id = id.replace("_label","");
                         }
                         checkRequiredVal(dom.byId(id).checked, srcElement);

                     }

                 }

             });
             query("#PmtFFDefHdrTableId").query("input[id$='_req']").on("click", function(checkValue) {
                 var srcElement = checkValue.currentTarget;
                 if (srcElement){
                     var id = srcElement.id;
                     if (id.indexOf('data_req') != -1){
                         return;
                     }
                     if (this.checked){
                         if(checkInvLabelLabelVal(dom.byId(id).value, srcElement,
                                 "PaymentDefinitions.Label")){
                             thisPage.selectFFPmtHdrSummItem(this);
                            
                             if(id =="hfx_contract_number_req"){                                
                                 var hfxContractRate =registry.byId('hfx_contract_rate_req');                               
                                 hfxContractRate.set('checked', true);
                                 thisPage.selectFFPmtHdrSummItem(hfxContractRate);                                      
                            }
                             else if(id =="hfx_contract_rate_req"){                             
                                 var hfxContractNumber = registry.byId('hfx_contract_number_req');                              
                                 hfxContractNumber.set('checked', true);
                                 thisPage.selectFFPmtHdrSummItem(hfxContractNumber);                
                            }
                         }
                     } else {
                         unsetDataDelFFHdrOrder(id);                           
                        
                         if(id =="hfx_contract_number_req"){
                             var hfxContractRate =registry.byId('hfx_contract_rate_req');
                             hfxContractRate.set('checked', false);                             
                             unsetDataDelFFHdrOrder(hfxContractRate.id);                                                                                        
                         }
                         else if(id =="hfx_contract_rate_req"){
                             var hfxContractNumber = registry.byId('hfx_contract_number_req');
                             hfxContractNumber.set('checked', false);
                             unsetDataDelFFHdrOrder(hfxContractNumber.id);                                      
                         }
                     }

                 }

             });
             query("#PmtFFDefHdrTableId").query("input[id$='_data_req']").on("click", function(checkValue) {
                 var srcElement = checkValue.currentTarget;
                 if (srcElement){
                     var id = srcElement.id;

                     if (id.indexOf('data_req') == -1){
                         return;
                     }
                     if (this.checked){
                         id = id.replace('_data','');
                         checkRequiredVal(dom.byId(id).checked, srcElement);

                     }

                 }

             });
             query("#PmtFFDefInvTableId").query("input[id$='_req']").on("click", function(checkValue) {
                 var srcElement = checkValue.currentTarget;
                 if (srcElement){
                     var id = srcElement.id;
                     if (id.indexOf('data_req') != -1){
                         return;
                     }
                     if (this.checked){

                         if(checkInvLabelLabelVal(dom.byId(id).value, srcElement,
                                 "PaymentDefinitions.Label")){
                             thisPage.selectFFPmtInvSummItem(this);
                         }
                     } else {
                         unsetDataReq(dom.byId(id.replace('_req','_data_req')));
                         var row = dom.byId(id + "_order");
                         deleteOrderField(row, dom.byId("PmtFFInvDefFileOrderTableId"), "itext");
                     }

                 }

             });
             query("#PmtFFDefInvTableId").query("input[id$='_data_req']").on("click", function(checkValue) {
                 var srcElement = checkValue.currentTarget;
                 if (srcElement){
                     var id = srcElement.id;

                     if (id.indexOf('data_req') == -1){
                         return;
                     }
                     if (this.checked){
                         id = id.replace('_data','');
                         checkRequiredVal(dom.byId(id).checked, srcElement);

                     }

                 }

             });

        if (!isReadOnly){
             query('#PmtDLMDefFileOrderTableId').on(".deleteSelectedItem:click", function (evt) {               
                 thisPage.deleteEventHandler(this, document.getElementById("PmtDLMDefFileOrderTableId"), "text");
                
                 if(this.id =="fx_contract_number"){    
                     thisPage.deleteEventHandler( document.getElementById("fx_contract_rate"), document.getElementById("PmtDLMDefFileOrderTableId"), "text");
                    }
                    else if(this.id =="fx_contract_rate"){
                          thisPage.deleteEventHandler( document.getElementById("fx_contract_number"), document.getElementById("PmtDLMDefFileOrderTableId"), "text");
                    }
             });
             query('#PmtFFDefFileOrderTableId').on(".deleteSelectedItem:click", function (evt) {                
                 thisPage.deleteEventHandler(this, document.getElementById("PmtFFDefFileOrderTableId"), "ftext");

             });
             query('#PmtFFHdrDefFileOrderTableId').on(".deleteSelectedItem:click", function (evt) {             
                 thisPage.deleteEventHandler(this, document.getElementById("PmtFFHdrDefFileOrderTableId"), "htext");
               
                 if(this.id =="hfx_contract_number"){   
                     thisPage.deleteEventHandler( document.getElementById("hfx_contract_rate"), document.getElementById("PmtFFHdrDefFileOrderTableId"), "htext");
                    }
                    else if(this.id =="hfx_contract_rate"){
                          thisPage.deleteEventHandler( document.getElementById("hfx_contract_number"), document.getElementById("PmtFFHdrDefFileOrderTableId"), "htext");
                    }
             });
             query('#PmtFFInvDefFileOrderTableId').on(".deleteSelectedItem:click", function (evt) {
                 thisPage.deleteEventHandler(this, document.getElementById("PmtFFInvDefFileOrderTableId"), "itext");

             });
             query('#poStructSumDataOrderTableId').on(".deleteSelectedItem:click", function (evt) {
                 thisPage.deleteEventHandler(this, document.getElementById("poStructSumDataOrderTableId"), "userDefText");

             });
             query('#poStructSumLineItemOrderTableId').on(".deleteSelectedItem:click", function (evt) {
                 thisPage.deleteEventHandler(this, document.getElementById("poStructSumLineItemOrderTableId"), "userDefLineText");

             });
             
             query('#discountCodeDataOrderTableId').on(".deleteSelectedItem:click", function (evt) {

                 thisPage.deleteEventHandler(this, document.getElementById("discountCodeDataOrderTableId"), "1userDefText");

             });
        }
         });
	});

	 function preSaveAndClose(){
	  	 	 
	 	  document.forms[0].buttonName.value = "SaveAndClose";
	 	  return true;
	
	}

</script> 

</body>
</html>
