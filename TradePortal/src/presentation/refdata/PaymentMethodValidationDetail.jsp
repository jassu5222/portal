<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*, com.amsinc.ecsg.html.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   Debug.debug("***START***************PAYMENT*METHOD*VALIDATION*DETAIL**************************START***");
   //ctq portal refresh comment out
   //userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");

   /****************************************************************
    * THIS SCRIPTLET WILL FIND OUT IF WE ARE EDITING A PAYMENT METHOD
    * CREATING A NEW ONE OR RETURNING FROM AN ERROR FROM A PREVIOUS
    * VERSION OF THIS PAGE.        WE WILL ALSO SET THE PAGE GLOBALS
    * AND POPULATE THE SECURE PARAMETER HASHTABLE THAT WILL BE SENT
    * TO THE FORM MANAGER.
    ****************************************************************/

    /*********\
    * GLOBALS *
    \*********/
   boolean isNew = false; //tells if this is a new bank branch rule
   boolean isReadOnly = false; //tell if the page is read only
   boolean getDataFromDoc = false;
   boolean showSave = true;
   boolean showDelete = true;
   boolean showSaveClose   = true;
   //boolean showSaveButton=true;
   //boolean showDeleteButton=true;
   boolean showCloseButton=true;
   String oid = "";
   String loginRights = null;
   String ownerLevel = userSession.getOwnershipLevel();
   String desigBankOid = null;
   WidgetFactory widgetFactory=new WidgetFactory(resMgr);
   String buttonPressed ="";
   //PaymentMethodValidation Web Bean
   beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.PaymentMethodValidationWebBean", "PaymentMethodValidation");
   PaymentMethodValidationWebBean editPaymentMethodValidation = (PaymentMethodValidationWebBean) beanMgr.getBean("PaymentMethodValidation");
   
   Hashtable secureParams = new Hashtable();

   DocumentHandler doc = null;

   //Get security rights
   loginRights = userSession.getSecurityRights();
   isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_PAYMENT_METH_VAL);
   Debug.debug("MAINTAIN_PAYMENT_METH_VAL == " + SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_PAYMENT_METH_VAL));

     // Get the document from the cache.  We'll use it to repopulate the screen if the
   // user was entering data with errors.
   doc = formMgr.getFromDocCache();
	buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
   
   Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");
   Debug.debug("doc from cache is " + doc.toString());

   if (doc.getDocumentNode("/In/PaymentMethodValidation") != null ) {
   // The doc does exist so check for errors.

       String maxError = doc.getAttribute("/Error/maxerrorseverity");
       oid = doc.getAttribute("/Out/PaymentMethodValidation/payment_method_validation_oid");
       //if this was an update not an insert, we need to get from in
     if (oid == null ) {
         oid = doc.getAttribute("/In/PaymentMethodValidation/payment_method_validation_oid");
    }
       if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
       // We've returned from a save/update/delete that was successful
       // We shouldn't be here.  Forward to the RefDataHome page.
          /*  formMgr.setCurrPage("RefDataHome"); */

	     /* String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request); */
%>
           <%-- <jsp:forward page='<%= physicalPage %>' /> --%>
<%
           /* return; */
       } else {
       // We've returned from a save/update/delete but have errors.
       // We will display data from the cache.  Determine if we're
       // in insertMode by looking for a '0' oid in the input section
       // of the doc.

           getDataFromDoc = true;

           if(doc.getAttribute("/In/PaymentMethodValidation/payment_method_validation_oid").equals("0"))
           {
               Debug.debug("Create PaymentMethVal Error");
               isNew = true;//else it stays false
           }

           if (!isNew)
           // Not in insert mode, use oid from input doc.
           {
               Debug.debug("Update PaymentMethVal Error");
               oid = doc.getAttribute("/In/PaymentMethodValidation/payment_method_validation_oid");
           }
       }
   }
   if (getDataFromDoc)
   {
       // We create a new docHandler to work around an aparent bug in AMSEntityWebbean
       // This hurts performance and should be removed as soon as populateFromXmlDoc(doc,path) works
       DocumentHandler doc2 = doc.getComponent("/In");
       Debug.debug("Populating Payment Method Validation with:\n" + doc2.toString(true));
       editPaymentMethodValidation.populateFromXmlDoc(doc2);
       if (!isNew)
           editPaymentMethodValidation.setAttribute("payment_method_validation_oid",oid);
           oid = editPaymentMethodValidation.getAttribute("payment_method_validation_oid");
   }
   else if (request.getParameter("oid") != null || (oid!=null && !"".equals(oid.trim()) && !"0".equals(oid)) ) //EDIT PaymentMethVal
   {
       isNew = false;
       
       if(oid!=null && !"".equals(oid.trim()) && !"0".equals(oid)){
           //We are back in this page from a successful save.
       }
       else if(request.getParameter("oid")!=null){
                 Debug.debug("Oid in request if"+request.getParameter("oid"));
                 oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
       }
       //This getParameter is done twice to prevent overwriting a valid oid
       
       Debug.debug("Editing paymentmethval with oid -> " + oid);
       editPaymentMethodValidation.getById(oid);
       Debug.debug("Editing paymentmethval with oid: " + editPaymentMethodValidation.getAttribute("paymentmethval_oid")); //DEBUG
       Debug.debug("isReadOnly == " + isReadOnly);
	   }
   else 
    {
       isNew = true;
	   oid = null;
    }

   // Auto save the form when time-out if not readonly.  
   // (Header.jsp will check for auto-save setting of the corporate org).
   String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

   if (isReadOnly)
   {
       showSave = false;
       showSaveClose   = false;
       showDelete = false;
   }
   else if (isNew)
   {
       oid = "0";
       editPaymentMethodValidation.setAttribute("payment_method_validation_oid",oid);
       showDelete = false;
   }
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>


<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="RefDataHome.PaymentMethodValidation"/>
      <jsp:param name="helpUrl"  value="admin/payment_method_rules_form.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (!isNew) {
    ReferenceDataManager ref = ReferenceDataManager.getRefDataMgr (formMgr.getServerLocation());
    subHeaderTitle = ref.getDescr("PAYMENT_METHOD", editPaymentMethodValidation.getAttribute("payment_method"), userSession.getUserLocale())
      + ": " + ref.getDescr("COUNTRY", editPaymentMethodValidation.getAttribute("country"), userSession.getUserLocale())
      + ", " + ref.getDescr("CURRENCY_CODE", editPaymentMethodValidation.getAttribute("currency"), userSession.getUserLocale());
  }
  else {
    subHeaderTitle = resMgr.getText("PaymentMethodValidationDetail.NewPaymentMethodValidation", TradePortalConstants.TEXT_BUNDLE);  
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>

	
    <form name="PaymentMethodValidationDetailForm" id="PaymentMethodValidationDetailForm" data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>">
      <input type=hidden value="" name=buttonName>

      <div class="formArea">
      <jsp:include page="/common/ErrorSection.jsp" />

        <div class="formContent">
				<div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
				<div class="dijitTitlePaneContentInner">		     	
				  
				          <%
				            /***********************
				            * START BREAD CRUMB NAME
				            ************************/
				            
				            /***********************
				            * END BREAD CRUMB NAME
				            ************************/
				
				            /*******************
				            * START SECUREPARAMS
				            ********************/
				            secureParams.put("PaymentMethodValidationOid",oid);
				
				            secureParams.put("login_oid",userSession.getUserOid());
				            secureParams.put("login_rights",loginRights);
				            secureParams.put("opt_lock", editPaymentMethodValidation.getAttribute("opt_lock"));
				            /*****************
				            * END SECUREPARAMS
				            ******************/
				
				          %>
				         
				     
				      
				  
				      	<div class="clear"></div>
				 		<div class="columnLeft">
				          <%
				        /********************************
				         * START PAYMENT METHOD DROPDOWN BOX
				         ********************************/
				        Debug.debug("Payment Method Dropdown Box - " + editPaymentMethodValidation.getAttribute("payment_method"));
				        %>
				         
				          <%
				          Vector codesToExclude = new Vector();
                          codesToExclude.add(TradePortalConstants.PAYMENT_METHOD_IACC);
				        String options1 = Dropdown.createSortedRefDataOptions("PAYMENT_METHOD", editPaymentMethodValidation.getAttribute("payment_method"),
				                                                              resMgr.getResourceLocale(),codesToExclude);
				       String value1=editPaymentMethodValidation.getAttribute("payment_method");
				       //out.println("Prateep"+editPaymentMethodValidation.getAttribute("payment_method"));
				        out.print(widgetFactory.createSelectField("PaymentMethod","PaymentMethodValidationDetail.PaymentMethod","",options1,isReadOnly,true,false,"value='"+value1+"' class='char35'","",""));
				        
				        /********************************
				         * END PAYMENT METHOD DROPDOWN BOX
				         ********************************/
				        %>
				      
				        <%
				           /********************************
				            * START COUNTRY TYPE DROPDOWN
				            ********************************/
				           
				           %>
				       
				        <%
				           String options2 = Dropdown.createSortedRefDataOptions("COUNTRY", editPaymentMethodValidation.getAttribute("country"), resMgr.getResourceLocale());
				        String value2=editPaymentMethodValidation.getAttribute("country");
				           out.print(widgetFactory.createSelectField("Country","PaymentMethodValidationDetail.Country","",options2,isReadOnly,true,false,"value='"+value2+"' class='char35'","",""));
						%>
				        
				       </div>
				       <div class="columnRight">
				        <%
				           /********************************
				            * START CURRENCY DROPDOWN
				            ********************************/
				           
				           %>
				      
				        <%
				           String options3 = Dropdown.createSortedRefDataOptions("CURRENCY_CODE", editPaymentMethodValidation.getAttribute("currency"), resMgr.getResourceLocale());
				        String value3=editPaymentMethodValidation.getAttribute("currency");
				           out.print(widgetFactory.createSelectField("Currency","PaymentMethodValidationDetail.Currency","",options3,isReadOnly,true,false,"value='"+value3+"' class='char35'","",""));   
				        
						%>
				        
				      
				        <%
				           /********************************
				            * START NO OF OFFSET DAYS DROPDOWN
				            ********************************/
				           Debug.debug("Building Payment Validation Offset Days field - " + editPaymentMethodValidation.getAttribute("offset_days"));
				      
				           %>
				        
				        <%
				           DropdownOptions daysDropdown = new DropdownOptions();
				           daysDropdown.addOption("0", "0");
				           daysDropdown.addOption("1", "1");
				           daysDropdown.addOption("2", "2");
				           daysDropdown.addOption("3", "3");
				           daysDropdown.addOption("4", "4");
				           daysDropdown.addOption("5", "5");
				           daysDropdown.addOption("6", "6");
				           daysDropdown.setSelected(editPaymentMethodValidation.getAttribute("offset_days"));
				           String options4 = daysDropdown.createSortedOptionListInHtml(resMgr.getResourceLocale());
				           out.print(widgetFactory.createSelectField("OffsetDays","PaymentMethodValidationDetail.OffsetDays","",options4,isReadOnly,true,false,"","",""));
				           
						%>
				        
					  </div>
				    
			  		<%= formMgr.getFormInstanceAsInputField("PaymentMethodValidationForm", secureParams) %>
			</div>
          </div>
        </div><%--formContent--%>
      </div><%--formArea--%>

      <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'PaymentMethodValidationDetailForm'">

				<jsp:include page="/common/RefDataSidebar.jsp">
				<jsp:param name="showSaveButton" value="<%= showSave%>" />
				<jsp:param name="showSaveCloseButton" value="<%= showSaveClose %>" />
				<jsp:param name="showDeleteButton" value="<%= showDelete%>" />
				<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
      			<jsp:param name="error" value="<%=error%>" />
				<jsp:param name="cancelAction" value="goToRefDataHome" />
				<jsp:param name="showHelpButton" value="false" />
				<jsp:param name="helpLink" value="customer/payment_method_ruels_form.htm" />
				</jsp:include>

      </div> <%--formSidebar end--%>
			
    </form>
  </div><%-- pageContent end --%>
</div><%-- pageMain end --%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />

<%
  String focusField = "PaymentMethod";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
</script>
<%
  }
%>

</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("PaymentMethodValidation");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   Debug.debug("***END********************PaymentMethVal*DETAIL**************************END***");
%>
