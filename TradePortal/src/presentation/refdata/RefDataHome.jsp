
<%--
*******************************************************************************
                                    Ref Data Home

  Description:
     This page is the main page for corporate reference data.  It provides a
  dropdown (filtered by what tables the user has access to).  The dropdown 
  selection displays a list of items for the selected reference data type.

*******************************************************************************
--%>



<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%@ page import="com.ams.tradeportal.busobj.webbean.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,java.util.Map,java.util.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*,  com.ams.tradeportal.html.*,
                 com.amsinc.ecsg.html.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
  if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
    userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
  }else{
    userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
  }

  //cquinton 1/18/2013 set return page
  //here on ref data home this is primarily used for templates
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); //to keep it correct
  }
  else {
    userSession.addPage("goToRefDataHome");
  }

 //IR T36000026794 Rel9.0 07/24/2014 starts
  session.setAttribute("startHomePage", "RefDataHome");
  session.removeAttribute("fromTPHomePage");
 //IR T36000026794 Rel9.0 07/24/2014 End
 
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryData =null;
  //KMehta - 01 Aug 2014 - Rel9.1 IR-T36000030566 - Add  - Begin
  Vector<DocumentHandler> fxRateList = new Vector();
  int fxRateListCount = 0;
  //KMehta - 01 Aug 2014 - Rel9.1 IR-T36000030566 - Add  - End
  
%>


<%-- ************** Data retrieval page setup begins here ****************  --%>
<%
    /***********************************************************************
     * GLOBAL CONSTANTS FOR THIS JSP
     * GLOBAL VARIABLES FOR THIS JSP
     ***********************************************************************/

    String subAccessUserOrgID = null;
    String filterText = null;
    String parentOrgID        = userSession.getOwnerOrgOid();
    String bogID              = userSession.getBogOid();
    String clientBankID       = userSession.getClientBankOid();
    String globalID           = userSession.getGlobalOrgOid();
    String ownershipLevel     = userSession.getOwnershipLevel();
    StringBuffer dropdownList = new StringBuffer();
    HttpSession theSession    = request.getSession(false);
    //Added for all the options
    String initSearchParms="";
    String initSearchParmsCreationRules = "";
    
 
    DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  	
  
    String gridHtml = "";
	String gridLayout = "";	
	/* Added for Secondary Nav */
	String titleKey ="";
	String helpUrl="";

    String refDataType = "";
    String onLoad = "";
    String formName = "";
    String ruleTypeSecurityAccFlag = null;

    Debug.debug("ownerOID,bogID,ClientBankID,GlobalID,ownership_level- "
          + parentOrgID +", "+ bogID +", "+ clientBankID +", "+ globalID +", "
          + ownershipLevel);
    /* KMehta IR-T36000025579 Rel 9300 on 17-Jun-2015 Add - Begin*/
    String instrumentSearchDialogTitle=resMgr.getTextEscapedJS("InstSearch.HeadingGeneric",TradePortalConstants.TEXT_BUNDLE);
    /* KMehta IR-T36000025579 Rel 9300 on 17-Jun-2015 Add - End*/
    //Get security rights
    String loginRights = userSession.getSecurityRights();
    boolean readOnly = true;
    boolean allowCreate = true;//BSL IR# BIUL050954395 - sometimes the Create button should be hidden even if page is not read-only
    boolean isFilterReadOnly = false;
   boolean hasNotiTemplatesExists = false;
   boolean hasNotifRule = false;
    /***********************************************************************
     * The following scriplet builds the drop down box from which the user
     * chooses what type of data will be viewed in the list view.
     *
     * In order to acomplish this, all possible values for the drop down
     * box are stored in the vector choices.  Note: the 0th element in that
     * vector is understood to be the prompt asking the user to select data.
     * All other options may be in any order.
     ***********************************************************************/
  

    // If using subsidiary access and user can access their own data when using sub. access,
    // include the user's actual organization OID
    boolean includeSubAccessUserOrg = userSession.showOrgDataUnderSubAccess();

    if(includeSubAccessUserOrg)
       subAccessUserOrgID = userSession.getSavedUserSession().getOwnerOrgOid();

   
    if (request.getParameter("refdata") != null) //a type of refdata has been selected
    {
        
        refDataType = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("refdata"), userSession.getSecretKey());
        theSession.setAttribute("refdata",refDataType);
    }
    else {
        if (theSession.getAttribute("refdata") != null) {
            refDataType = (String)theSession.getAttribute("refdata");
        }
    }

    String newLink = "";
    String linkAltText = "";
    String linkImage = "";
    String helpLink = "";
    int newButtonWidth = 0;

  String gridId = "";

          CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
          corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
          corporateOrg.getDataFromAppServer();
          
       boolean poStructUpld = true;

       if (TradePortalConstants.PO_UPLOAD_TEXT.equals(corporateOrg.getAttribute("po_upload_format")))
       		poStructUpld = false;
       		//poStructUpld =false;

//CR-707 Refresh IAZ End
		//Rel9.5 CR-927B START
       StringBuilder sqlQryNotifTemplates = new StringBuilder();
		List<Object> sqlParamsNotifTemplates = new ArrayList();
		
		 sqlQryNotifTemplates.append("select notification_rule_oid, name from notification_rule ");
		 sqlQryNotifTemplates.append(" where p_owner_org_oid in ( ?,?"  );
		  sqlParamsNotifTemplates.add(userSession.getClientBankOid());
		  sqlParamsNotifTemplates.add(userSession.getOwnerOrgOid());
  		     // Also include Notification Templates from the user's actual organization if using subsidiary access
  		  if(userSession.showOrgDataUnderSubAccess()){
  			sqlQryNotifTemplates.append(",?");
      		sqlParamsNotifTemplates.add(userSession.getSavedUserSession().getOwnerOrgOid());
   		  }

  		sqlQryNotifTemplates.append(") and template_ind = ?");
  		sqlQryNotifTemplates.append(" order by ");
  		sqlQryNotifTemplates.append(resMgr.localizeOrderBy("name"));
		sqlParamsNotifTemplates.add(TradePortalConstants.INDICATOR_YES);
		DocumentHandler notificationTemDefListDoc = DatabaseQueryBean.getXmlResultSet(sqlQryNotifTemplates.toString(), true,sqlParamsNotifTemplates);
		
		Vector<DocumentHandler> notificationTempList = new Vector();
		
		if ( notificationTemDefListDoc != null ){
			notificationTempList = notificationTemDefListDoc.getFragments("/ResultSetRecord");
		}
		
		if(notificationTempList.size() != 0 && notificationTempList.size() > 0){
			hasNotiTemplatesExists = true;
		}
		
		StringBuilder sqlQryNotifRules = new StringBuilder();
		List<Object> sqlParamsNotifRules = new ArrayList();
		
		 sqlQryNotifRules.append("select notification_rule_oid, name from notification_rule ");
		 sqlQryNotifRules.append(" where p_owner_org_oid = ?"  );
		 sqlParamsNotifRules.add(userSession.getOwnerOrgOid());
  		 sqlQryNotifRules.append(" and template_ind = ?");
  		 sqlParamsNotifRules.add(TradePortalConstants.INDICATOR_NO);

		DocumentHandler notificationRuleDefListDoc = DatabaseQueryBean.getXmlResultSet(sqlQryNotifRules.toString(), true,sqlParamsNotifRules);
		
		Vector<DocumentHandler> notificationRuleList = new Vector();
		
		if ( notificationRuleDefListDoc != null ){
			notificationRuleList = notificationRuleDefListDoc.getFragments("/ResultSetRecord");
		}
		
		if(notificationRuleList.size() != 0 && notificationRuleList.size() > 0){
			hasNotifRule = true;
		}
		//Rel9.5 CR-927B END
   // Based on the selection, determine the link to use and the text
    // to display
   
    if (TradePortalConstants.REFDATA__PARTIES.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_PARTIES);
       newLink     = formMgr.getLinkAsUrl("selectParty",response);
       linkImage   = "common.CreateNewPartyImg";
       linkAltText = "RefDataParties.CreateNewParty";
       helpLink    = "customer/party_search.htm";
       onLoad      = "document.PartyFilterForm.filterText.focus();";
       formName    = "PartyFilterForm";
       gridId      = "PartiesDataGridId";
    } else if (TradePortalConstants.REFDATA__PHRASES.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_PHRASE);
       newLink     = formMgr.getLinkAsUrl("selectPhrase",response);
       linkImage   = "common.CreateNewPhraseImg";
       linkAltText = "RefDataPhrase.CreateNewPhrase";
       helpLink    = "customer/ref_data_home.htm#phrases";
       gridId      = "PhrasesDataGridId";
    }
    /* pgedupudi - Rel9.3 CR976A 03/09/2015 START */ 
     else if (TradePortalConstants.REFDATA__BANKGRPRESTRICTRULES.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_BANK_GROUP_RESTRICT_RULES);
       newLink     = formMgr.getLinkAsUrl("selectBankGrpRestrictRule",response);
       linkImage   = "common.CreateNewPhraseImg";
       linkAltText = "RefDataPhrase.CreateNewPhrase";
       helpLink    = "admin/bank_group_restriction_rules.htm";
       gridId      = "BankGrpRestrnRulesDataGridId";
    }
    /* pgedupudi - Rel9.3 CR976A 03/09/2015 END  */ 
    else if (TradePortalConstants.REFDATA__USERS.equals(refDataType)) {

       readOnly = false;

       if(SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_USERS)) {
         // NAR-SAUJ060247671 24-SEP-2010
         //if user is admin, force certificate condition should not be check for admin user.if admin has user maintinance access reghts,
         // he will be able to edit customer user data and can create user without checking whether he has 2FA and Certificate rights.
         
         String userSecurityType1 = "";
         if (userSession.getSavedUserSession() == null) {
           userSecurityType1 = userSession.getSecurityType();
         }else {
           userSecurityType1 = userSession.getSavedUserSessionSecurityType();
         }          
         if(!userSecurityType1.equals(TradePortalConstants.ADMIN)){    
           if (corporateOrg.getAttribute("force_certs_for_user_maint").equals(TradePortalConstants.INDICATOR_YES)) { 
             // Corporate Org requires cert for users that maintain users.
             UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
             thisUser.setAttribute("user_oid", userSession.getUserOid());
             thisUser.getDataFromAppServer();
             
             String authMethod = StringFunction.isNotBlank(thisUser.getAttribute("authentication_method")) ?
            		 thisUser.getAttribute("authentication_method") : corporateOrg.getAttribute("authentication_method");
            		 
             if (!(TradePortalConstants.AUTH_CERTIFICATE.equals(authMethod)
               || TradePortalConstants.AUTH_2FA.equals(authMethod)))
            	
               readOnly = true;
               
           }
         }
       } 
       else
       {
         readOnly = true;
       }

       newLink     = formMgr.getLinkAsUrl("selectUser",response);
       linkImage   = "common.CreateNewUserImg";
       linkAltText = "RefDataHome.CreateNewUser";
       helpLink    = "customer/ref_data_home.htm#users";
       gridId      = "UsersDataGridId";
    } else if (TradePortalConstants.REFDATA__SECURITY.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_SEC_PROF);
       newLink     = formMgr.getLinkAsUrl("selectSecurityProfile",response);
       linkImage   = "common.CreateNewSecurityProfileImg";
       linkAltText = "RefDataHome.CreateNewSecurityProfile";
       helpLink    = "customer/ref_data_home.htm#sec_profiles";
       gridId      = "secProfGridId";
    } else if (TradePortalConstants.REFDATA__TEMPLATE.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_TEMPLATE);
       String urlParms = "&startPage=" + EncryptDecrypt.encryptStringUsingTripleDes(NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request), userSession.getSecretKey());
       newLink = "javascript:openNewTemplateDialog();";
       linkImage   = "common.CreateNewTemplateImg";
       linkAltText = "RefDataHome.CreateNewTemplate";
       helpLink    = "customer/template_search.htm";
       //PMitnala Rel 8.3 IR#T36000022643 - Setting gridId for bank level users START
       if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
       		gridId      = "templateBankGridId";
       }else{
    	    gridId      = "templateGridId";   
       }
       //PMitnala Rel 8.3 IR#T36000022643 END
    } else if (TradePortalConstants.REFDATA__THRESHOLD.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_THRESH_GRP);
       newLink     = formMgr.getLinkAsUrl("selectThresholdGroup",response);
       linkImage   = "common.CreateNewThresholdImg";
       linkAltText = "RefDataHome.CreateNewThresholdGroup";
       helpLink    = "customer/ref_data_home.htm#thresh_groups";
       gridId      = "ThresholdGroupDataGridId";
    } 
     else if (TradePortalConstants.REFDATA__PANELGROUPS.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_PANEL_AUTH_GRP);
       newLink     = formMgr.getLinkAsUrl("selectPanelGroup",response);
       linkImage   = "common.CreateNewPanelAuthImg";
       linkAltText = "RefDataHome.CreateNewPanelAuthGroup";
       helpLink    = "customer/ref_data_home.htm#panel_auth_group";
       gridId      = "PanelGroupsDataGridId";
    }
     else if (TradePortalConstants.REFDATA__FXRATES.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_FX_RATE);
       if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType())) {
         // Hide the Create FX Rate button if user is admin
         allowCreate = false;
       }
       newLink     = formMgr.getLinkAsUrl("selectFXRate",response);
       linkImage   = "common.CreateNewFXRateImg";
       linkAltText = "RefDataFXRate.CreateExchangeRate";
       helpLink    = "customer/ref_data_home.htm#fx_rates";
       onLoad      = "document.FXRateFilterForm.filterText.focus();";
       formName    = "FXRateFilterForm";
       gridId      = "fxRateGrid";
    } else if (TradePortalConstants.REFDATA__LCCREATERULES.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_LC_CREATE_RULES);
       newLink     = formMgr.getLinkAsUrl("goToPOUploadDefnSelection",response);
       linkImage   = "common.CreateNewLCCreateRuleImg";
       linkAltText = "RefDataHome.CreateLCCreateRule";
       helpLink    = "customer/ref_data_home.htm#lc_creation_rules";
       gridId      = "creationRuleGridId";
    } else if (TradePortalConstants.REFDATA__ATPCREATERULES.equals(refDataType)) {
       readOnly    = !(SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_ATP_CREATE_RULES) ||
                      SecurityAccess.hasRights(loginRights,
                                                SecurityAccess.MAINTAIN_LC_CREATE_RULES));
       newLink     = formMgr.getLinkAsUrl("goToATPPOUploadDefnSelection",response);
       linkImage   = "common.CreateNewATPCreateRuleImg";
       linkAltText = "RefDataHome.CreateATPCreateRule";
       helpLink    = "customer/creation_rules_list.htm"; //TBD       
       gridId      = "creationRuleGridId";
    } else if (TradePortalConstants.REFDATA__INVONLY.equals(refDataType)) {
        readOnly    = !(SecurityAccess.hasRights(loginRights,
                                                SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES) ||
                       SecurityAccess.hasRights(loginRights,
                                                 SecurityAccess.MAINTAIN_LR_CREATE_RULES));
    	if (readOnly)
			allowCreate = false;
    	


        newLink     = formMgr.getLinkAsUrl("goToATPPOUploadDefnSelection",response);
        linkImage   = "common.CreateNewATPCreateRuleImg";
        linkAltText = "RefDataHome.CreateATPCreateRule";
        helpLink    = "customer/creation_rules_list.htm"; //TBD       
        gridId      = "InvOnlyCreateRuleDataGridId";
    } else if (TradePortalConstants.REFDATA__POUPLOADDEFN.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_PO_UPLOAD_DEFN);
       if (!poStructUpld) {
         newLink     = formMgr.getLinkAsUrl("selectPOUploadDefinition",response);
         gridId      = "POUploadDefinitionDataGridId";
       }
       else {
         newLink     = formMgr.getLinkAsUrl("selectPOStructDefinition",response);
         gridId      = "POStructuredDefinitionDataGridId";
       }
       linkImage   = "common.CreateNewPOUploadDefnImg";
       linkAltText = "RefDataHome.CreatePOUploadDefn";
       helpLink    = "customer/ref_data_home.htm#po_definitions";
    } else if (TradePortalConstants.REFDATA__NOTIFRULES.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
               SecurityAccess.MAINTAIN_NOTIFICATION_RULE);
       if(hasNotiTemplatesExists){
	   	newLink = "javascript:openNewNotificationTemplateDialog();";
	   }else{
		newLink     = formMgr.getLinkAsUrl("selectNotificationRule",response);
	   }
       linkImage   = "common.CreateNewNotificationRuleImg";
       linkAltText = "RefDataNotificationRules.CreateNewNotificationRule";
       helpLink    = "customer/working_notify_rules.htm";
       gridId      = "notificationRulesGridId";
    }else if (TradePortalConstants.REFDATA__WORKGROUPS.equals(refDataType)) {
       readOnly    = !SecurityAccess.hasRights(loginRights,
                                               SecurityAccess.MAINTAIN_WORK_GROUPS);
       newLink     = formMgr.getLinkAsUrl("selectWorkGroup",response);
       linkImage   = "common.CreateNewWorkGroupImg";
       linkAltText = "RefDataWorkGroups.CreateNewWorkGroup";
       helpLink    = "customer/ref_data_home.htm#work_groups";
       gridId      = "workGroupGridId";
    }
    //Rel9.5 CR 927B
    else if (TradePortalConstants.REFDATA__NOTIFRULE_TEMPLATES.equals(refDataType)) {
        readOnly    = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_NOTIF_RULE_TEMPLATES);
		newLink     = formMgr.getLinkAsUrl("selectNotificationRuleTemplate",response);
		linkImage   = "common.CreateNewNotificationRuleImg";
		linkAltText = "RefDataNotificationRuleTemplates.CreateNewNotificationRuleTemplate";
		helpLink    = "admin/working_notify_template.htm";
		gridId      = "notificationRuleTemplatesGridId";
	}
    //  Pratiksha - 10/06/08 - CR-434 - Add Begin 
    else if (TradePortalConstants.REFDATA__ARMMATCHRULES.equals(refDataType)) {
      readOnly    = !SecurityAccess.hasRights(loginRights,
                                                 SecurityAccess.MAINTAIN_RECEIVABLES_MATCH_RULES);
      newLink     = formMgr.getLinkAsUrl("selectARMMatchRule",response);
      linkImage   = "common.CreateNewARMMatchRulesImg";
      linkAltText = "RefDataReceivablesMatchingRules.CreateNewReceivablesMatchingRule";
      helpLink    = "customer/ref_data_home.htm#lc_creation_rules"; //TBD
      onLoad      = "document.BuyerFilterForm.BuyerName.focus();";
      formName    = "BuyerFilterForm";
      gridId      = "ARMMatchingRuleDataGridId";
    }
    //  Pratiksha - 10/06/08 - CR-434 - Add End 
    else if (TradePortalConstants.REFDATA__BANKBRANCHRULES.equals(refDataType)) {
      readOnly    = !SecurityAccess.hasRights(loginRights,
                                              SecurityAccess.MAINTAIN_BANK_BRANCH_RULES);
      newLink     = formMgr.getLinkAsUrl("selectBankBranchRule",response);
      linkImage   = "common.CreateBankBranchRulesImg";
      linkAltText = "RefDataHome.CreateBankBranchRules";
      helpLink    = "admin/bank_branch_rules_list.htm"; 
      gridId      = "BankBranchRulesDataGridId";
    }
    else if (TradePortalConstants.REFDATA__PAYMENTMETHVAL.equals(refDataType)) {
      readOnly    = !SecurityAccess.hasRights(loginRights,
                                              SecurityAccess.MAINTAIN_PAYMENT_METH_VAL);
      newLink     = formMgr.getLinkAsUrl("selectPaymentMethodValidation",response);
      linkImage   = "common.CreatePaymentMethodValidationImg";
      linkAltText = "RefDataHome.CreatePaymentMethodValidation";
      helpLink    = "admin/payment_method_rule_ovw.htm"; 
      gridId      = "paymentMethodValidationDataGridId";
    }
//RKAZI REL 8.4 11/21/2013 CR-599 - ADD - START
    else if (TradePortalConstants.REFDATA__PAYMENTDEFN.equals(refDataType)) {
        readOnly    = !SecurityAccess.hasRights(loginRights,
                                                SecurityAccess.MAINTAIN_PAYMENT_FILE_DEF);
        newLink     = formMgr.getLinkAsUrl("selectPaymentDefinitions",response);
        linkImage   = "common.CreatePaymentDefinitionsImg";
        linkAltText = "RefDataHome.CreatePaymentDefinitions";
        helpLink    = "customer/payment_file_listview.htm"; 
        gridId      = "paymentDefinitionDataGridId";
      }
  //RKAZI REL 8.4 11/21/2013 CR-599 - ADD - END
//AAlubala - Rel8.2 - CR741 - BEGIN
    else if (TradePortalConstants.REFDATA__ERPGLCODES.equals(refDataType)) {
        readOnly    = !SecurityAccess.hasRights(loginRights,
                                                SecurityAccess.MAINTAIN_ERP_GL_CODES);
        newLink     = formMgr.getLinkAsUrl("selectErpGlCode",response);
        linkImage   = "common.CreateErpGlCodeImg";
        linkAltText = "RefDataHome.CreateErpGlCodes";
        helpLink    = "admin/erp_gl_codes_ovw.htm"; 
        gridId      = "ErpGlCodesDataGridId";
      }
      else if (TradePortalConstants.REFDATA__DISCOUNTCODES.equals(refDataType)) {
        readOnly    = !SecurityAccess.hasRights(loginRights,
                                                SecurityAccess.MAINTAIN_DISCOUNT_CODES);
        newLink     = formMgr.getLinkAsUrl("selectDiscountCodes",response);
        linkImage   = "common.CreateDiscountCodeImg";
        linkAltText = "RefDataHome.CreateDiscountCodes";
        helpLink    = "customer/discount_codes.htm"; 
        gridId      = "DiscountCodesDataGridId";
      }
    else if (TradePortalConstants.REFDATA__CALENDARS.equals(refDataType)) {
      readOnly    = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CALENDAR);
      newLink     = formMgr.getLinkAsUrl("selectCalendar",response);
      linkImage   = "common.CreateNewCalendarImg";
      linkAltText = "RefDataCalendar.CreateNewCalendar";
      helpLink    = "customer/ref_data_home.htm#calendars";
      gridId      = "CalendarsDataGridId";
    } 
     else if (TradePortalConstants.REFDATA__PAYTEMPLATEGROUP.equals(refDataType)) {
      readOnly    = !SecurityAccess.hasRights(loginRights,
                                              SecurityAccess.MAINTAIN_PAYMENT_TEMPLATE_GRP);
      newLink     = formMgr.getLinkAsUrl("selectPaymentTemplateGroup",response);
      linkImage   = "common.CreateNewPayTemplGrpImg";
      linkAltText = "RefDataPaymentTemplateGroup.CreateNewPaymentTemplateGrp";
      helpLink    = "customer/ref_data_home.htm#phrases";
      gridId      = "paymentTemplateGroupGridId";
    }
     else if (TradePortalConstants.REFDATA__CURRENCYCALENDARRULE.equals(refDataType)) {
      readOnly    = !SecurityAccess.hasRights(loginRights,
                                              SecurityAccess.MAINTAIN_CURRENCY_CALENDAR_RULES);
      newLink     = formMgr.getLinkAsUrl("selectCurrencyCalendarRule",response);
      linkImage   = "common.CreateCurrencyCalendarRuleImg";
      linkAltText = "RefDataCurrencyCalendarRule.CreateCurrencyCalendarRule";
      helpLink    = "admin/ref_data_home.htm#calendar_calendar_rule.htm"; 
      gridId      = "CurrencyCalendarRuleDataGridId";
    }
    else if (TradePortalConstants.REFDATA__GENERICMSGCATEGORIES.equals(refDataType)) {
      readOnly    = !SecurityAccess.hasRights(loginRights,
                                              SecurityAccess.MAINTAIN_GM_CAT_GRP);
      newLink     = formMgr.getLinkAsUrl("selectGenericMessageCategory",response);
      linkImage   = "common.CreateNewMsgCategoryImg";
      linkAltText = "RefDataHome.CreateNewGenMsgCat";
      helpLink    = "customer/ref_data_home.htm#message_category";
      gridId      = "GenericMessageCategoryDataGridId";
    }
    else if (TradePortalConstants.REFDATA__CROSSRATERULE.equals(refDataType)) {
      readOnly    = !SecurityAccess.hasRights(loginRights,
                                              SecurityAccess.MAINTAIN_CROSS_RATE_RULE);
      newLink     = formMgr.getLinkAsUrl("selectCrossRateCalculationRule",response);
      linkImage   = "common.CreateCrossRateRuleImg";
      linkAltText = "RefDataHome.CreateCrossRateRule";
      helpLink    = "customer/ref_data_home.htm#cross_rate_rule.htm";
      gridId      = "CrossRateRuleDataGridId";
    }
     else if (TradePortalConstants.REFDATA__INVOICEDEFN.equals(refDataType)) {
      // if user is ADMIN and having maintin invoice definition rights, then only create invoice button shoudl be visible
      String userSecurityType1 = "";
      if (userSession.getSavedUserSession() == null) {
        userSecurityType1 = userSession.getSecurityType();
      }else {
        userSecurityType1 = userSession.getSavedUserSessionSecurityType();
      }          
      if(userSecurityType1.equals(TradePortalConstants.ADMIN) && SecurityAccess.hasRights(loginRights,
         SecurityAccess.MAINTAIN_INVOICE_DEFINITION)){                
        readOnly    = false;
		allowCreate = true;
      }
      else {
        readOnly   = true;
      }	            
      newLink     = formMgr.getLinkAsUrl("selectInvoiceDefinition",response);
      linkImage   = "common.CreateInvoiceDefinitionImg";
      linkAltText = "RefDataHome.CreateInvoiceDefinition";
      helpLink    = "customer/invoice_definitions_list.htm";
      gridId      = "InvoiceDefinitionDataGridId";
    } else if (TradePortalConstants.REFDATA__INTDISRATE.equals(refDataType)) {
      readOnly    = true;
      newLink     = formMgr.getLinkAsUrl("selectType",response);
      linkImage   = "common.CreateInvoiceDefinitionImg";
      linkAltText = "RefDataHome.InterestDiscountRates";
      helpLink    = "customer/interest_discount_rates_list.htm";
      gridId      = "InterestGroupRatesDataGridId";
    }
  	
    if (helpLink.equals("")) {
      helpLink = "customer/ref_data_home.htm";
    }


    // Add context to the performance statistic so that we can determine which listview the user is on 
    PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

    if((perfStat != null) && (perfStat.isLinkAction()))
      perfStat.setContext(refDataType);


    ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class, "ClientBank");
    clientBank.setAttribute("organization_oid", clientBankID);
    clientBank.getDataFromAppServer();

  String searchNav = "refDataHome."+refDataType;    
%>



<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>
 		

<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>
<div class="pageMainNoSidebar">
	<div class="pageContent">
		<div class="secondaryNav">
			<% if (TradePortalConstants.REFDATA__PARTIES.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.Parties";
		    	helpUrl="customer/party_search.htm ";
			}else if (TradePortalConstants.REFDATA__PHRASES.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.Phrases";
		    	helpUrl="customer/ref_data_home.htm#phrases";
			}
			/* pgedupudi - Rel9.3 CR976A 03/09/2015 START */
			else if (TradePortalConstants.REFDATA__BANKGRPRESTRICTRULES.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.BankGrpRestrictRules";
		    	helpUrl="admin/bank_group_restriction_rules.htm";
			}
			/* pgedupudi - Rel9.3 CR976A 03/09/2015 END */
			else if (TradePortalConstants.REFDATA__INVONLY.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.InvOnlyCreateRule";
		    	helpUrl="customer/ref_data_home.htm#phrases";
			}else if (TradePortalConstants.REFDATA__CALENDARS.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.Calendars";
		    	helpUrl="customer/Calendars_List.htm";
			}else if (TradePortalConstants.REFDATA__CROSSRATERULE.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.CrossRateRule";
		    	helpUrl="customer/Cross_Rate_Calculation_Rules_List.htm";
			}else if (TradePortalConstants.REFDATA__CURRENCYCALENDARRULE.equals(refDataType)) {
				titleKey="RefDataHome.CurrencyCalendarRule";
		    	helpUrl="customer/Currency_Calendar_Rules_List.htm";
			
			}else if (TradePortalConstants.REFDATA__PANELGROUPS.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.PanelAuthGroups";
		    	helpUrl="customer/ref_data_home.htm#panel_auth_group";
			}
			else if (TradePortalConstants.REFDATA__FXRATES.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataMenu.ForeignExchangeRates";
		    	helpUrl="customer/ref_data_home.htm#fx_rates";
			}else if (TradePortalConstants.REFDATA__USERS.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.Users";
		    	helpUrl="customer/ref_data_home.htm#users";
		    }else if (TradePortalConstants.REFDATA__ATPCREATERULES.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.CreationRules";
		    	helpUrl="customer/creation_rules_list.htm";				
		    }else if (TradePortalConstants.REFDATA__SECURITY.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.SecurityProfiles";
		    	helpUrl="customer/ref_data_home.htm#sec_profiles";
		    }else if (TradePortalConstants.REFDATA__PAYMENTMETHVAL.equals(refDataType)) {
		    	titleKey="AdminRefDataMenu.PaymentMethodRules";
		    	helpUrl="admin/payment_method_rule_ovw.htm";
		    }else if (TradePortalConstants.REFDATA__PAYMENTDEFN.equals(refDataType)) {
		    	titleKey="AdminRefDataMenu.PaymentDefinitions";
		    	helpUrl="customer/payment_file_listview.htm";
		    }
			else if (TradePortalConstants.REFDATA__NOTIFRULES.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.NotificationRule";
		    	helpUrl="customer/working_notify_rules.htm";
		    	
			}
			else if (TradePortalConstants.REFDATA__TEMPLATE.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.InstrumentTemplates";
		    	helpUrl="customer/ref_data_home.htm#instr_temps";
		    	
			}
			//Rel9.5 CR 927B
			else if (TradePortalConstants.REFDATA__NOTIFRULE_TEMPLATES.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.NotificationRuleTemplates";
		    	helpUrl="admin/working_notify_template.htm";
			}

			else if (TradePortalConstants.REFDATA__PAYTEMPLATEGROUP.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.PayTemplateGroups";
		    	helpUrl="customer/Template_Group.htm";
		    	
			}
			else if (TradePortalConstants.REFDATA__WORKGROUPS.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="WorkGroupDetail.WorkGroupsLabel";
		    	helpUrl="customer/ref_data_home.htm#work_groups";
		    	
			}else if (TradePortalConstants.REFDATA__THRESHOLD.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.ThresholdGroups";
		    	helpUrl="customer/ref_data_home.htm#thresh_groups";
			}else if (TradePortalConstants.REFDATA__GENERICMSGCATEGORIES.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.GenericMessageCategories";
		    	helpUrl="admin/Generic_Message_Categories.htm";
			}else if (TradePortalConstants.REFDATA__ARMMATCHRULES.equals(refDataType)) {
				/* Initializing for secondary nav */
		    	titleKey="RefDataHome.ReceivablesMatchingRules";
		    	helpUrl="customer/Trading_Partner_Rules.htm";
			}
			else if (TradePortalConstants.REFDATA__POUPLOADDEFN.equals(refDataType)) {
				if (poStructUpld) {
				 	titleKey="RefDataHome.POStructDefinition";
		         	helpUrl = "customer/structured_po_definition_list.htm ";
				}else{
					titleKey="RefDataHome.POUploadDefn";
		         	helpUrl = "customer/ref_data_home.htm#po_definitions";
				}
		    }else if (TradePortalConstants.REFDATA__BANKBRANCHRULES.equals(refDataType)) {
		    	titleKey   = "RefDataHome.BankBranchRules.Search";
		    	helpUrl    = "admin/bank_branch_rules_list.htm"; 
		    }else if (TradePortalConstants.REFDATA__INVOICEDEFN.equals(refDataType)) {
			    	titleKey="RefDataHome.InvoiceDefinitions";
			    	helpUrl="customer/invoice_definitions_list.htm";
			 }else if (TradePortalConstants.REFDATA__INTDISRATE.equals(refDataType)) {
			    	titleKey="RefDataHome.InterestDiscountRates";
			    	helpUrl="customer/interest_discount_rates_list.htm";
			 }

			 else if (TradePortalConstants.REFDATA__ERPGLCODES.equals(refDataType)) {
			    	titleKey="RefDataMenu.ErpGlCodes";
			    	helpUrl="customer/ref_data_home.htm#erp_gl_codes.htm";
			 }else if (TradePortalConstants.REFDATA__DISCOUNTCODES.equals(refDataType)) {
			    	titleKey="RefDataMenu.DiscountCodes";
			    	helpUrl="customer/discount_codes.htm";
			 }
		
			%>
			<jsp:include page="/common/PageHeader.jsp">
	              <jsp:param name="titleKey" value="<%=titleKey %>"/>
	              <jsp:param name="helpUrl"  value="<%=helpUrl %>"/>
	        </jsp:include>
		</div>
		<form method="post" name="<%=formName%>" action="<%=formMgr.getSubmitAction(response)%>">

                <jsp:include page="/common/ErrorSection.jsp" />

		<div class="formContentNoSidebar">
			<%    if (TradePortalConstants.REFDATA__PARTIES.equals(refDataType) || TradePortalConstants.REFDATA__FXRATES.equals(refDataType))
		       //We are on the Phrases and FX Rates List View Page
		    {
		%>
		<input type=hidden value="" name=buttonName>
		<input type=hidden name="refdata" value="<%= refDataType %>">
		<input type="hidden" name="NewSearch" value="Y">
		<%
		}
		%>
		

  <div class="searchHeader">
    <div class="searchHeaderActions">
		<%    if (!TradePortalConstants.REFDATA__NOTIFRULES.equals(refDataType))
				{
		%>	
			      	<jsp:include page="/common/dGridShowCount.jsp">
			        	<jsp:param name="gridId" value="<%=gridId%>" />
			      	</jsp:include>
		
    
		<% }
		           
		
		              /***********************************************************************
		               * Begin create 'New' Button for List View Page.  The 'button' consists
		               * of a left side, a center (a link and an image) and a right side.
		               ***********************************************************************/
		       
		           String uploadFormat = null;			    		   
			       uploadFormat =  corporateOrg.getAttribute("po_upload_format");
			       if (!readOnly && allowCreate) { //ctq && currentChoice > 0) {//BSL IR# BIUL050954395 - Hide the Create button in certain scenarios
			    	   
			    	   if (TradePortalConstants.REFDATA__ATPCREATERULES.equals(refDataType)){
			    		   
			    		   DocumentHandler poUplDefDoc = new DocumentHandler();
			    		   StringBuilder poUplDefnSQL = new StringBuilder();
			    		   List<Object> sqlParamsATP = new ArrayList();
						   QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 
						   if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat)){
							  poUplDefnSQL.append("select po_upload_definition_oid, name ");
							  poUplDefnSQL.append(" from po_upload_definition");
							  poUplDefnSQL.append(" where a_owner_org_oid in (?");
							  sqlParamsATP.add(userSession.getOwnerOrgOid());
					  		  // Also include PO defs from the user's actual organization if using subsidiary access
					  		  if(userSession.showOrgDataUnderSubAccess()){
					  			poUplDefnSQL.append(",?");
					  			sqlParamsATP.add(userSession.getSavedUserSession().getOwnerOrgOid());
					   		  }
					
					  		  poUplDefnSQL.append(") and definition_type in (?,?)");
					  		  poUplDefnSQL.append(" order by ");
					  		  poUplDefnSQL.append(resMgr.localizeOrderBy("name"));
					  		
					  		sqlParamsATP.add(TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD);
					  		sqlParamsATP.add(TradePortalConstants.PO_DEFINITION_TYPE_BOTH);
					  		
					  		
							  Debug.debug(poUplDefnSQL.toString());
						   }else { 
								 // Build a list of PO Structured Definitions for the user's org.
					          poUplDefnSQL.append("select purchase_order_definition_oid, name ");
					          poUplDefnSQL.append(" from purchase_order_definition");
					          poUplDefnSQL.append(" where a_owner_org_oid in (?"  );
					          sqlParamsATP.add(userSession.getOwnerOrgOid());
					             // Also include PO defs from the user's actual organization if using subsidiary access
					          if(userSession.showOrgDataUnderSubAccess())
					           {
					        	  poUplDefnSQL.append(",?");
					        	  sqlParamsATP.add(userSession.getSavedUserSession().getOwnerOrgOid());
					        	  
					           }
					          poUplDefnSQL.append(")");					      
					          poUplDefnSQL.append(" order by ");
					          poUplDefnSQL.append(resMgr.localizeOrderBy("name"));
					          Debug.debug(poUplDefnSQL.toString());						  
							}
							
							queryListView.setSQL(poUplDefnSQL.toString(),sqlParamsATP);
						    queryListView.getRecords();
						    poUplDefDoc = queryListView.getXmlResultSet();
						    Debug.debug(poUplDefDoc.toString());
						    
						    String definition = "";
						    if (queryListView.getRecordCount() == 1) {
						      if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat)) {						    
								 definition = poUplDefDoc.getAttribute("/ResultSetRecord(0)/PO_UPLOAD_DEFINITION_OID");
						      }
						      else { 
						    	 definition = poUplDefDoc.getAttribute("/ResultSetRecord(0)/PURCHASE_ORDER_DEFINITION_OID");  
						      }
								definition = EncryptDecrypt.encryptStringUsingTripleDes(definition, userSession.getSecretKey());
					       		formMgr.setCurrPage("ATPCreationRuleDetail");
					       		String physicalPage = NavigationManager.getNavMan().getPhysicalPage("ATPCreationRuleDetail", request);
				%>
								      <button data-dojo-type="dijit.form.Button" type="button" id="newButton1">
								        <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
								        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
											openPODefinationSelectionCallback('<%=definition%>');
        								</script>
								      </button>	
				<%
			    	   		}else{
			    %>
						      <button data-dojo-type="dijit.form.Button" type="button" id="newButton1">
						        <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
						        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
									openPODefinationSelection();
        						</script>
						      </button>	
				<%
			    	   		}
		              	  }else if (TradePortalConstants.REFDATA__INVONLY.equals(refDataType)){
		              		  // prepare dialog to be displayed for the new button 
  	              		   DocumentHandler invUplDefDoc = new DocumentHandler();
			    		   StringBuffer invDefnSQL = new StringBuffer();
						   QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 
						   List<Object> sqlParamsInv = new ArrayList();
						   invDefnSQL.append("select inv_upload_definition_oid, name ");
						   invDefnSQL.append(" from invoice_definitions");
						   invDefnSQL.append(" where a_owner_org_oid in (?");
						   sqlParamsInv.add( userSession.getOwnerOrgOid());
				          if(userSession.showOrgDataUnderSubAccess())
				           {
				        	  invDefnSQL.append(",?");
				        	  sqlParamsInv.add(userSession.getSavedUserSession().getOwnerOrgOid());
				           }
				          invDefnSQL.append(")");					      
				          invDefnSQL.append(" order by ");
				          invDefnSQL.append(resMgr.localizeOrderBy("name"));
				          Debug.debug(invDefnSQL.toString());						  
					  		  
						  queryListView.setSQL(invDefnSQL.toString(),sqlParamsInv);
						  queryListView.getRecords();
						  invUplDefDoc = queryListView.getXmlResultSet();
						  Debug.debug(invUplDefDoc.toString());
						  
						  String definition = "";

 						    if (queryListView.getRecordCount() == 1) { // auto selection
 						    	definition = invUplDefDoc.getAttribute("/ResultSetRecord(0)/INV_UPLOAD_DEFINITION_OID");

 						    definition = EncryptDecrypt.encryptStringUsingTripleDes(definition, userSession.getSecretKey());
 						    formMgr.setCurrPage("InvOnlyCreationRuleDetails");
 						    String physicalPage = NavigationManager.getNavMan().getPhysicalPage("InvOnlyCreationRuleDetails", request);
 				%>
				
						      <button data-dojo-type="dijit.form.Button" type="button" id="newButton1">
						        <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
						        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
											openInvDefinationSelectionCallback('<%=definition%>');
      								</script>
						      </button>	
				<%
			    	   		}else{ // more than one, hence provide popup
			    %>
						      <button data-dojo-type="dijit.form.Button" type="button" id="newButton1">
						        <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
						        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
									openInvDefinationSelection();
      						</script>
						      </button>	
				<%
			    	   		}
 		              	} // end of inoice only
 		              	else if (TradePortalConstants.REFDATA__NOTIFRULES.equals(refDataType)) {
 		              			if(!hasNotifRule){
 		         %>     				
 		              				<button data-dojo-type="dijit.form.Button" type="button" id="newButton">
							        <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
							      </button>	
 				<%
 		              			}
 		              			
                                  //cquinton 10/12/2012 restoring new button for non-atp
 		              	}else {
%>
								      <button data-dojo-type="dijit.form.Button" type="button" id="newButton">
								        <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
								      </button>	
     
<%
                                  }  
				
			       }
		               /***********************************************************************
		               * End create 'New' Button for List View Page.
		               ***********************************************************************/
				%>
				 <%=widgetFactory.createHoverHelp("newButton","NewHoverText") %>
				 
				 <%=widgetFactory.createHoverHelp("newButton1","CreationRuleList.New") %>
    </div>
    <div style="clear:both"></div>
  </div>
				
	
		<%
		    /***********************************************************************
		     * Begin search and list view for Parties
		     ***********************************************************************/
		    if (TradePortalConstants.REFDATA__PARTIES.equals(refDataType))
		       //We are on the Parties List View Page
		    {
		    	
		    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 		   savedSearchQueryData = (Map)searchQueryMap.get("PartiesDataView");
		 	   } 
		    	
		    	
		    	gridHtml = dGridFactory.createDataGrid("PartiesDataGridId","PartiesDataGrid", null);
				gridLayout = dGridFactory.createGridLayout("PartiesDataGridId","PartiesDataGrid");
				
				
				String newSearch = request.getParameter("NewSearch");
		      // Retrieve the search criteria from the request if we are coming from a form submission.
		      // Otherwise get the previous search criteria from the session object (e.g. coming back from party details).
		      if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
		      {
		         filterText = request.getParameter("filterText");
		      }
		     
		      if (filterText != null)  filterText = filterText.toUpperCase();
		      else filterText = "";
		
		/*************************************************************
		 * PartyFilterForm
		 * This form contains the filter text field and submit button to filter parties
		 *************************************************************/
		%>
<%
  if (!readOnly && allowCreate) {
%>
  <div class="searchDivider"></div>
<%
  }
%>
  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">
       <%=widgetFactory.createSearchTextField("filterText", "PartySearch.PartyName", "30", "onKeydown='Javascript: filterPartiesOnEnter(\"filterText\");'") %>
    
    </span>
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchParty">
        <%= resMgr.getText("PartySearch.Search", TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchParties();</script>
      </button>
    </span>
    <%=widgetFactory.createHoverHelp("searchParty", "SearchHoverText") %>
    <div style="clear:both"></div>
  </div>
		  
		 
		<%
		        //Build the where clause
		        String where = "where p.p_owner_org_oid in ("+ parentOrgID +","+ globalID;
		        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
		        {
		            where = where +","+ clientBankID;
		            if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
		                where = where +","+ bogID;
		        }
		
		        // Possibly include the subsidiary access user org's data also
		        if(includeSubAccessUserOrg)
		           where +=","+subAccessUserOrgID;
		
		        where = where +")";
		
		        if (filterText != null && !filterText.equals(""))
		        {
		            where += " and upper(name) like unistr('"+ StringFunction.toUnistr(SQLParamFilter.filter(filterText)) + "%')";
		        }
		
		        Debug.debug("*** where-> " + where);
		        
		        initSearchParms = "";
		        
		%>
				<%=gridHtml %>
		<%
		    }
		    /***********************************************************************
		     * End list view for Parties List View Page
		     ***********************************************************************/
		     
		      /***********************************************************************
		     *  pgedupudi - Rel9.3 CR976A 03/09/2015 START
		     *  Begin list view for Bank Group Restriction Rules List View Page
		     ***********************************************************************/
		     if (TradePortalConstants.REFDATA__BANKGRPRESTRICTRULES.equals(refDataType))
		     {
		     	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("BankGrpRestrnRulesDataView");
			 	   }
		    	//Initializing for dgrid 
		    	
		    	gridHtml = dGridFactory.createDataGrid("BankGrpRestrnRulesDataGridId","BankGrpRestrnRulesDataGrid", null);
				gridLayout = dGridFactory.createGridLayout("BankGrpRestrnRulesGridId","BankGrpRestrnRulesDataGrid");
				
		        String where = "where p.p_owner_org_oid in ("+ parentOrgID +","+ globalID;
		        
		        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
		        {
		            where = where +","+ clientBankID;
		            if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
		                where = where +","+ bogID;
		        }
		
		        // Possibly include the subsidiary access user org's data also
		        if(includeSubAccessUserOrg)
		           where +=","+subAccessUserOrgID;
		
		        where = where +")";
		        initSearchParms = "";
		%>
		    <%=gridHtml %> 
		<%
			}     
		      /***********************************************************************
		     * pgedupudi - Rel9.3 CR976A 03/09/2015 END
		     * End list view for Bank Group Restriction Rules List View Page
		     ***********************************************************************/
		
		    /***********************************************************************
		     * Begin list view for Phrase List View Page
		     ***********************************************************************/
		
		    if (TradePortalConstants.REFDATA__PHRASES.equals(refDataType))
		       //We are on the Phrases List View Page
		    {
		    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("PhrasesDataView");
			 	   }
		    	//Initializing for dgrid 
		    	
		    	gridHtml = dGridFactory.createDataGrid("PhrasesDataGridId","PhrasesDataGrid", null);
				gridLayout = dGridFactory.createGridLayout("PhrasesDataGridId","PhrasesDataGrid");
				
		        String where = "where p.p_owner_org_oid in ("+ parentOrgID +","+ globalID;
		        
		        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
		        {
		            where = where +","+ clientBankID;
		            if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
		                where = where +","+ bogID;
		        }
		
		        // Possibly include the subsidiary access user org's data also
		        if(includeSubAccessUserOrg)
		           where +=","+subAccessUserOrgID;
		
		        where = where +")";
		        initSearchParms = "";
		        Debug.debug("where-> " + where);
		%>
		        
		      
				
				<%=gridHtml %>
					
				
		<%
		    }


		    /***********************************************************************
		     * End list view for Invoice only List View Page
		     ***********************************************************************/
		
		
		    /***********************************************************************
		     * Begin list view for Payment Template Group List View Page 
		     * VS CR-586 Begin
		     ***********************************************************************/
		
		    if (TradePortalConstants.REFDATA__PAYTEMPLATEGROUP.equals(refDataType))
		       //We are on the Payment Template Group List View Page
		    {
		    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("PaymentTemplateGroupDataView");
			 	   }
		    	initSearchParms = "";
		    	
		    	
		        gridHtml = dGridFactory.createDataGrid("paymentTemplateGroupGridId","PaymentTemplateGroupDataGrid", null);
				gridLayout = dGridFactory.createGridLayout("paymentTemplateGroupdGridId","PaymentTemplateGroupDataGrid");
				
				%>
		        
		      
				
				<%=gridHtml %>
		<%
		    }
		    /***********************************************************************
		     * End list view for Payment Template Group List View Page
		     ***********************************************************************/
		
		     /**********************************************************************
		     *ERP 
		     AAlubala - Rel8.2 - CR741 - 
		     ***********************************************************************/
			    if (TradePortalConstants.REFDATA__ERPGLCODES.equals(refDataType))
				       //We are on the Payment Template Group List View Page
				    {
			    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
				 		   savedSearchQueryData = (Map)searchQueryMap.get("ErpGlCodesDataView");
				 	   }
				    	initSearchParms = "";
				    	
				    	gridHtml = dGridFactory.createDataGrid("ErpGlCodesDataGridId","ErpGlCodesDataGrid", null);
						gridLayout = dGridFactory.createGridLayout("ErpGlCodesDataGridId","ErpGlCodesDataGrid");	
						
						
						initSearchParms = "parentOrgID="+parentOrgID;				    	
				        	
						%>
				        
				        <%-- Start Datagrid --%>
						
						<%=gridHtml %>
				<%
				    }		     
			    if (TradePortalConstants.REFDATA__DISCOUNTCODES.equals(refDataType))
				       //We are on the Payment Template Group List View Page
				    {
			    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
				 		   savedSearchQueryData = (Map)searchQueryMap.get("DiscountCodesDataView");
				 	   }
				    	initSearchParms = "";
				    	
				    	gridHtml = dGridFactory.createDataGrid("DiscountCodesDataGridId","DiscountCodesDataGrid", null);
						gridLayout = dGridFactory.createGridLayout("DiscountCodesDataGridId","DiscountCodesDataGrid");
						
					
				    	initSearchParms = "parentOrgID="+parentOrgID;				    	
				        	
						%>
				        
				       
						
						<%=gridHtml %>
				<%
				    }		     
		     
		     /**********************************************************************
		     *END CR741
		     **********************************************************************/
		     
		     
		    /**********************************************************************
		     * Begin list view for Threshold Group List View Page
		     **********************************************************************/
		
		  // assumption: threshold groups can only be seen by CORPORATE orgs
			    if (TradePortalConstants.REFDATA__THRESHOLD.equals(refDataType))
			    {
			    	
			    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
				 		   savedSearchQueryData = (Map)searchQueryMap.get("ThresholdGroupDataView");
				 	   }
					 
				    	gridHtml = dGridFactory.createDataGrid("ThresholdGroupDataGridId","ThresholdGroupDataGrid", null);
						gridLayout = dGridFactory.createGridLayout("ThresholdGroupDataGridId","ThresholdGroupDataGrid");
						
					
				      // Build the dynamic part of the where clause
				     
				      initSearchParms = "pOid="+parentOrgID;
				%>
		       <%=gridHtml %>
					
			<%
				}
		
		
		    /**********************************************************************
		     * End list view for Threshold Group List View Page
		     **********************************************************************/
		
		    //IAZ CR-511 11/09/09 Begin
		    /**********************************************************************
		     * Begin list view for Panel Group List View Page
		     **********************************************************************/
		    
			// assumption: panel groups can only be seen by CORPORATE orgs or via Corp Access
		    if (TradePortalConstants.REFDATA__PANELGROUPS.equals(refDataType))
		    {
		    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("PanelGroupsDataView");
			 	   }
		    	//Initializing for dgrid
		    	
				gridHtml = dGridFactory.createDataGrid("PanelGroupsDataGridId","PanelGroupsDataGrid", null);
				gridLayout = dGridFactory.createGridLayout("PanelGroupsDataGridId","PanelGroupsDataGrid");
				
	
		    	initSearchParms = "parentOrgID="+parentOrgID;
		%>
		
				<%=gridHtml %>				
		<%
			}
		
		    /**********************************************************************
		     * End list view for Panel Auth Group List View Page
		     **********************************************************************/
		     //IAZ CR-511 11/09/09 End
		         
		    /***********************************************************************
		     * Begin list view for User List View Page
		     ***********************************************************************/
		
		    if (TradePortalConstants.REFDATA__USERS.equals(refDataType)) {
		    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("UsersDataView");
			 	   }
		   
		     
		    	gridHtml = dGridFactory.createDataGrid("UsersDataGridId","UsersDataGrid", null);
				gridLayout = dGridFactory.createGridLayout("UsersDataGridId","UsersDataGrid");
		
		      initSearchParms = "parentOrgID="+parentOrgID;
		
		
		%>
		      <%=gridHtml %>
		      
		<%
		    }
		    /***********************************************************************
		     * End list view for User List View Page
		     ***********************************************************************/
		
		    /***********************************************************************
		     * Begin list view for Security Profiles List View Page
		     ***********************************************************************/
		
		    if (TradePortalConstants.REFDATA__SECURITY.equals(refDataType)) {
		    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("SecurityProfileDataView");
			 	   }
		    
		      initSearchParms="";
		
		      initSearchParms+="&ownershipLevel="+ownershipLevel;
		    
		      gridHtml = dGridFactory.createDataGrid("secProfGridId","SecurityProfileDataGrid", null);
			  gridLayout = dGridFactory.createGridLayout("secProfGridId","SecurityProfileDataGrid");
			  
		
		%>
		<%= gridHtml%>

		<%
		    }
		    /***********************************************************************
		     * End list view for Security Profiles List View Page
		     ***********************************************************************/
		
		    /***********************************************************************
		     * Begin list view for Templates List View Page
		     ***********************************************************************/
		
		    if (TradePortalConstants.REFDATA__TEMPLATE.equals(refDataType)) {
		    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		    		
		    		if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
			 		   savedSearchQueryData = (Map)searchQueryMap.get("TemplateBankDataView");
		    		}
		    		else{
		    			savedSearchQueryData = (Map)searchQueryMap.get("TemplateDataView");
		    		}
			 	   }
		    	
		    	UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
		         thisUser.setAttribute("user_oid", userSession.getUserOid());
		         thisUser.getDataFromAppServer();
		         String confInd = "";
		         if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))
		         	confInd = TradePortalConstants.INDICATOR_YES;
		         else if (userSession.getSavedUserSession() == null)
		            confInd = thisUser.getAttribute("confidential_indicator");
		         else   
		         {
		            if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
		                confInd = TradePortalConstants.INDICATOR_YES;
		            else	
		                confInd = thisUser.getAttribute("subsid_confidential_indicator");  
		         }       
		        
		         initSearchParms="confInd="+confInd;
		    	
		      
		         if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
		    		gridHtml = dGridFactory.createDataGrid("templateBankGridId","TemplateBankDataGrid", null);
					gridLayout = dGridFactory.createGridLayout("templateBankGridId","TemplateBankDataGrid");
					
		    	}else {
		    		gridHtml = dGridFactory.createDataGrid("templateGridId","TemplateDataGrid", null);
					gridLayout = dGridFactory.createGridLayout("templateGridId","TemplateDataGrid");
					
		    	}
		      
		
  if (!readOnly && allowCreate) {
%>
  <div class="searchDivider"></div>
<%
  }
%>
  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">    
    <%-- DK IR T36000024612 Rel8.4 02/03/2014 changed field name from TemplateName to TemplateNm --%>
    <%=widgetFactory.createSearchTextField("TemplateNm", "LCCreationRuleList.TemplateName", "35", "","") %>
    <%=widgetFactory.createHoverHelp("TemplateNm", "LCCreationRuleList.TemplateName") %>
    </span>
  
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchTemplate">
        <%= resMgr.getText("PartySearch.Search", TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchTemplate();</script>
      </button>
    </span>
    <%=widgetFactory.createHoverHelp("searchTemplate", "SearchHoverText") %>    
    <div style="clear:both"></div>
  </div>  
 <%-- DK IR T36000016490 Rel8.4 01/07/2014 ends --%>
		<%= gridHtml%>
		       
		
		<%
		    }
		    /***********************************************************************
		     * End list view for Templates List View Page
		     ***********************************************************************/
		
		    /***********************************************************************
		     * Begin list view for FX Rates List View Page
		     ***********************************************************************/
		
		    if (TradePortalConstants.REFDATA__FXRATES.equals(refDataType))
		    {
		    //KMehta - 01 Aug 2014 - Rel9.1 IR-T36000030566 - Add  - Begin
		    StringBuffer sql = new StringBuffer();
		    DocumentHandler fxratesDoc = new DocumentHandler();
		    QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
			  sql.append("select fx_rate_oid as count ");
          	     sql.append(" from fx_rate");
          	     sql.append(" where p_owner_org_oid in (?)");
          	     DocumentHandler fxRateListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true,new Object[]{userSession.getClientBankOid()});
          	
	   		if ( fxRateListDoc != null ){
	   			fxRateList = fxRateListDoc.getFragments("/ResultSetRecord");
	   		}
	   		fxRateListCount = fxRateList.size();
	   		Debug.debug(sql.toString());
			//KMehta - 01 Aug 2014 - Rel9.1 IR-T36000030566 - Add  - Ends
		    
		    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("FXRateDataViewSortCur");
			 	   }
		    	
		    	
		    	gridHtml = dGridFactory.createDataGrid("fxRateGrid","FXRateDataGridSortCur", null);
		    	gridLayout = dGridFactory.createGridLayout("fxRateGrid","FXRateDataGridSortCur");	
		    	
		    	
				String newSearch = request.getParameter("NewSearch");
		      // Retrieve the search criteria from the request if we are coming from a form submission.
		      // Otherwise get the previous search criteria from the session object (e.g. coming back from party details).
		      if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
		      {
		         filterText = request.getParameter("filterText");
		      }
		     		     
		    	  %>		      
		      		      
		     
<%
  if (!readOnly && allowCreate) {
%>
  <div class="searchDivider"></div>
  
<%
  }
%>
<%if (!allowCreate) { %>
  <div>&nbsp;</div>
  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">
      <%=widgetFactory.createSearchTextField("filterText", "RefDataFXRate.FXRateGroupLabel", "10","onKeydown='Javascript: filterFxrategroupOnEnter(\"filterText\");'") %>
    </span>
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchFXRate">Search
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchFXRates();</script>
      </button>
    </span>
    <div style="clear:both;"></div>
    <%=widgetFactory.createHoverHelp("searchFXRate", "SearchHoverText") %>
  </div>
<% }  %>
			 
		  <%
		      if (filterText != null)  filterText = filterText.toUpperCase();
		      else filterText = "";
		        // We need to display the FX Rate List View Page
		        // Build the dynamic part of the where clause
		       
		        initSearchParms= "";
		   %>
				<%=gridHtml%>
		<%
		    }
		    /***********************************************************************
		     * End list view for FX Rates List View Page
		     ***********************************************************************/
		
		
		    /***********************************************************************
		     * Begin list view for PO Upload Definition List View Page
		     ***********************************************************************/
		
		     if (TradePortalConstants.REFDATA__POUPLOADDEFN.equals(refDataType)) {
			    	
			    	if (poStructUpld)
			    	{
			    	 if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
					 		   savedSearchQueryData = (Map)searchQueryMap.get("POStructuredDefinitionDataView");
					 }
			    	
			    	gridHtml = dGridFactory.createDataGrid("POStructuredDefinitionDataGridId","POStructuredDefinitionDataGrid", null);
				  	gridLayout = dGridFactory.createGridLayout("POStructuredDefinitionDataGrid");
				  	
				  }
				  else
				  {
					  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
				 		   savedSearchQueryData = (Map)searchQueryMap.get("POUploadDefinitionDataView");
					  }
			   	  	gridHtml = dGridFactory.createDataGrid("POUploadDefinitionDataGridId","POUploadDefinitionDataGrid", null);
				  	gridLayout = dGridFactory.createGridLayout("POUploadDefinitionDataGrid");	
				  	
				  }
				  
				   // Build the dynamic part of the where clause
				 
				  initSearchParms = "parentOrgID="+parentOrgID+"&subAccessUserOrgID="+subAccessUserOrgID;
				  
				  if(includeSubAccessUserOrg){
					  initSearchParms = "parentOrgID="+parentOrgID+"&subAccessUserOrgID="+subAccessUserOrgID+"&includeSubAccessUserOrg=Y";
				  }
				%>
		       <%=gridHtml %>
			     
			<%
			    }
		    
		     if (TradePortalConstants.REFDATA__INVOICEDEFN.equals(refDataType)) {
		    	 if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("InvoiceDefinitionDataView");
				  }	
		    	 
		    	
		   	  	gridHtml = dGridFactory.createDataGrid("InvoiceDefinitionDataGridId","InvoiceDefinitionDataGrid", null);
		   	    gridLayout = dGridFactory.createGridLayout("InvoiceDefinitionDataGridId","InvoiceDefinitionDataGrid");	
		   	
			  
			   // Build the dynamic part of the where clause
		
			  initSearchParms = "parentOrgID="+parentOrgID+"&subAccessUserOrgID="+subAccessUserOrgID;
			  
			  if(includeSubAccessUserOrg){
				  initSearchParms = "parentOrgID="+parentOrgID+"&subAccessUserOrgID="+subAccessUserOrgID+"&includeSubAccessUserOrg=Y";
			  }
			%>
	       <%=gridHtml %>
		     
		<%
		    }
		     
		    /***********************************************************************
		     * End list view for PO Upload Definition List View Page
		     ***********************************************************************/
		     
		     //TLE - 07/20/07 - CR-375 - Add Begin 
		    /***********************************************************************
		     * Begin list view for ATP Creation Rules List View Page
		     ***********************************************************************/
		
		    if (TradePortalConstants.REFDATA__ATPCREATERULES.equals(refDataType)) {
		    	  DocumentHandler definitionsDoc = new DocumentHandler();
		    
		    	  List<Object> sqlParams = new ArrayList();
		    	  StringBuilder sql = new StringBuilder();
		    	  QueryListView queryListView = null;
	    		  uploadFormat =  corporateOrg.getAttribute("po_upload_format");
		    	  String defaultDefnOid = "";
		    	  StringBuilder optionsRT = new StringBuilder(); 
		    	  StringBuilder options = new StringBuilder(); 
		    	try {
		    		queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
		    				formMgr.getServerLocation(), "QueryListView");
		    		 List<Object> sqlParamsPo = new ArrayList();
		    	  if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat)){
		    		 sql.append("select po_upload_definition_oid, name ");
			         sql.append(" from po_upload_definition");
			         sql.append(" where a_owner_org_oid in (?");
			        
			         sqlParamsPo.add(userSession.getOwnerOrgOid());

			         // Also include PO defs from the user's actual organization if using subsidiary access
			         if(userSession.showOrgDataUnderSubAccess())
			         {
			             sql.append(",?");
			             sqlParamsPo.add(userSession.getSavedUserSession().getOwnerOrgOid());
			         }

			         sql.append(") and definition_type in (?,?)"); 
			         sql.append(" order by ");
			         sql.append(resMgr.localizeOrderBy("name"));
			         sqlParamsPo.add(TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD);
			         sqlParamsPo.add( TradePortalConstants.PO_DEFINITION_TYPE_BOTH);
			         
		    	  }else {
						 // Build a list of PO Structured Definitions for the user's org.
			          sql.append("select purchase_order_definition_oid, name ");
			          sql.append(" from purchase_order_definition");
			          sql.append(" where a_owner_org_oid in (?");
			          sqlParamsPo.add(userSession.getOwnerOrgOid());
			             // Also include PO defs from the user's actual organization if using subsidiary access
			          if(userSession.showOrgDataUnderSubAccess())
			          {
			        	  sql.append(",?");
			        	  sqlParamsPo.add(userSession.getSavedUserSession().getOwnerOrgOid());
			          }
			          sql.append(")");					      
			          sql.append(" order by ");
			          sql.append(resMgr.localizeOrderBy("name"));
			          Debug.debug(sql.toString());						  
				  }
			         
			         queryListView.setSQL(sql.toString(),sqlParamsPo);
			         queryListView.getRecords();
			         definitionsDoc = queryListView.getXmlResultSet();

			         if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat)){
				         sql = new StringBuilder("select po_upload_definition_oid");
				         sql.append(" from po_upload_definition ");
				         sql.append(" where a_owner_org_oid = ? ");
				         sql.append(" and default_flag='Y'");
				         Debug.debug(sql.toString());
				         sqlParams.add(userSession.getOwnerOrgOid());
				      
				         DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParams);
				         if (resultDoc != null) {
				           defaultDefnOid = resultDoc.getAttribute("/ResultSetRecord(0)/PO_UPLOAD_DEFINITION_OID");
				         }
				         
		    		   	options.append("<option value= \"\" >").
				  		append(resMgr.getText("Notifications.All", TradePortalConstants.TEXT_BUNDLE)).
			    	  	append("</option>").append(
		    		     	ListBox.createOptionList(definitionsDoc, 
	                            "PO_UPLOAD_DEFINITION_OID", "NAME", 
	                            defaultDefnOid, userSession.getSecretKey()));					
	    		     
			         }else { 
	    				
		    		      sql = new StringBuilder("select purchase_order_definition_oid");
		    		      sql.append(" from purchase_order_definition ");
		    		      sql.append(" where a_owner_org_oid = ? ");
		    		      sql.append(" and default_flag='Y'");
		    		      Debug.debug(sql.toString());
		    		      sqlParams.add(userSession.getOwnerOrgOid());
		    		
		    		      DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParams);
		    		      if (resultDoc != null) {
		    		        defaultDefnOid = resultDoc.getAttribute("/ResultSetRecord(0)/PURCHASE_ORDER_DEFINITION_OID");
		    		       }
		    		      options.append("<option value= \"\" >").
				  			append(resMgr.getText("Notifications.All", TradePortalConstants.TEXT_BUNDLE)).
			    	 		 append("</option>").append(
		    		     	ListBox.createOptionList(definitionsDoc, 
	                          "PURCHASE_ORDER_DEFINITION_OID", "NAME", 
	                          defaultDefnOid, userSession.getSecretKey()));
	    			 }
			        
	 				 optionsRT.append("<option value= \"\" >").
		    		    	        append(resMgr.getText("Notifications.All", TradePortalConstants.TEXT_BUNDLE)).
		    		    	        append("</option>");
		    		    		 optionsRT.append("<option value='").
		    		    	        append(InstrumentType.IMPORT_DLC).
		    		    	        append("'>").
		    		    	        append(resMgr.getText("RefdataHome.IMP_LC", TradePortalConstants.TEXT_BUNDLE)).
		    		    	        append("</option>");
		    		    		 optionsRT.append("<option value='").
		    		    	        append(InstrumentType.APPROVAL_TO_PAY).
		    		    	        append("'>").
		    		    	        append(resMgr.getText("RefdataHome.APPROVAL_TO_PAY", TradePortalConstants.TEXT_BUNDLE)).
		    		    	        append("</option>");
	    		    
		    	} catch (Exception e) {
		    	      e.printStackTrace();
		        } finally {
		          try {
		              if (queryListView != null) {
		                  queryListView.remove();
		              }
		          } catch (Exception e) {
		              System.out.println("error removing querylistview in POUploadATPDefnSelection.jsp");
		          }
		      }  // try/catch/finally block
		    	
		    	
		      String listview;
		   
		      if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		    	  if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat))
		 		   savedSearchQueryData = (Map)searchQueryMap.get("CreationRuleDataView");
		 		   else 
		 			  savedSearchQueryData = (Map)searchQueryMap.get("CreationRuleStructPODataView");
			  }
		      
		  
		      gridHtml = dGridFactory.createDataGrid("creationRuleGridId","CreationRuleDataGrid", null);
			  gridLayout = dGridFactory.createGridLayout("creationRuleGridId","CreationRuleDataGrid");
			 
			  
			  initSearchParms = "parentOrgID="+parentOrgID+"&includeSubAccessUserOrg="+includeSubAccessUserOrg+"&subAccessUserOrgID="+subAccessUserOrgID;
		%>
		
<%
  if (!readOnly && allowCreate) {
%>
  <div class="searchDivider"></div>
<%
  }
%>
  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">
    <%-- DK IR T36000016490 Rel8.4 01/07/2014 starts --%>
    <%=widgetFactory.createSearchTextField("RuleName", "LCCreationRuleList.RuleName", "35", "","") %>
    <%=widgetFactory.createHoverHelp("RuleName", "LCCreationRuleList.RuleName") %>
      <%= widgetFactory.createSearchSelectField("RuleType", "LCCreationRuleList.RuleType",
    			"", optionsRT.toString(), "onChange='searchCreationRules();'") %>	
    <%=widgetFactory.createHoverHelp("RuleType", "LCCreationRuleList.RuleType") %>
      <%= widgetFactory.createSearchSelectField("PODefn", "LCCreationRuleList.POUploadDefn",
    			"", options.toString(), "onChange='searchCreationRules();'") %>
    <%=widgetFactory.createHoverHelp("PODefn", "LCCreationRuleList.PODefn") %>
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchPCreationsRule">
        <%= resMgr.getText("PartySearch.Search", TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchCreationRules();</script>
      </button>
    </span>
    <%=widgetFactory.createSearchTextField("TemplateName", "LCCreationRuleList.TemplateName", "35", "","") %>
    <%=widgetFactory.createHoverHelp("TemplateName", "LCCreationRuleList.TemplateName") %>
    <%-- DK IR T36000016490 Rel8.4 01/07/2014 ends --%>
    </span>
  
    
    <%=widgetFactory.createHoverHelp("searchPCreationsRule", "SearchHoverText") %>
    
    <div style="clear:both"></div>
  </div>
    	
		<%= gridHtml %>
		

		<%
		    }
		    /***********************************************************************
		     * End list view for ATP Creation Rules List View Page
		     ***********************************************************************/
		     //TLE - 07/20/07 - CR-375 - Add End 
		
		    if (TradePortalConstants.REFDATA__INVONLY.equals(refDataType)) {
		    	 
		    	  DocumentHandler definitionsDoc = new DocumentHandler();
		    	  StringBuffer sql = new StringBuffer();
		    	  QueryListView queryListView = null;
 		    	  String defaultDefnOid = "";
 		    	  StringBuilder optionsRT = new StringBuilder(); 
 		    	  StringBuilder options = new StringBuilder(); 
		    	  try {
		    		
		    		  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
		    				formMgr.getServerLocation(), "QueryListView"); 
 
			    	  sql.append("select inv_upload_definition_oid, name ");
			          sql.append(" from invoice_definitions");
			          sql.append(" where a_owner_org_oid in (?");
			          List<Object> sqlParamsInvDef = new ArrayList();
			          sqlParamsInvDef.add(userSession.getOwnerOrgOid());
			             // Also include PO defs from the user's actual organization if using subsidiary access
			          if(userSession.showOrgDataUnderSubAccess())
			           {
			        	  sql.append(",?");
			        	  sqlParamsInvDef.add(userSession.getSavedUserSession().getOwnerOrgOid());
			           }
			          sql.append(")");					      
			          sql.append(" order by ");
			          sql.append(resMgr.localizeOrderBy("name"));
			          Debug.debug(sql.toString());						
		    	  
		    	  
		    	  	 queryListView.setSQL(sql.toString(), sqlParamsInvDef);
			         queryListView.getRecords();
			         definitionsDoc = queryListView.getXmlResultSet();
 			         
	    		      options.append("<option value=\"\">").
			      append(resMgr.getText("Notifications.All", TradePortalConstants.TEXT_BUNDLE)).
		    	  append("</option>").append(ListBox.createOptionList(definitionsDoc, 
                            "INV_UPLOAD_DEFINITION_OID", "NAME", 
                            "", userSession.getSecretKey()));
	    		     
		    		
	 				 optionsRT.append("<option value=\"\" >").
		    	        append(resMgr.getText("Notifications.All", TradePortalConstants.TEXT_BUNDLE)).
		    	        append("</option>");
		    		 optionsRT.append("<option value='").
		    	        append(InstrumentType.LOAN_RQST).
		    	        append("'>").
		    	        append(resMgr.getText("NewInstrumentsMenu.Trade.LoanRequest", TradePortalConstants.TEXT_BUNDLE)).
		    	        append("</option>");
		    		 optionsRT.append("<option value='").
		    	        append(InstrumentType.APPROVAL_TO_PAY).
		    	        append("'>").
		    	        append(resMgr.getText("RefdataHome.APPROVAL_TO_PAY", TradePortalConstants.TEXT_BUNDLE)).
		    	        append("</option>");
		    		    		 
	    		    
		    	} catch (Exception e) {
		    	      e.printStackTrace();
		        } finally {
		          try {
		              if (queryListView != null) {
		                  queryListView.remove();
		              }
		          } catch (Exception e) {
		              System.out.println("error removing querylistview in POUploadATPDefnSelection.jsp");
		          }
		      }  // try/catch/finally block
		    	
		    	
		      String listview;
		     
		      if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 		   savedSearchQueryData = (Map)searchQueryMap.get("InvOnlyCreateRuleDataView");
			  } 
		      
		   
		      gridHtml = dGridFactory.createDataGrid("InvOnlyCreateRuleDataGridId","InvOnlyCreateRuleDataGrid", null);
			  gridLayout = dGridFactory.createGridLayout("InvOnlyCreateRuleDataGridId","InvOnlyCreateRuleDataGrid");
			  
 			  
			  initSearchParms = "parentOrgID="+parentOrgID+"&includeSubAccessUserOrg="+includeSubAccessUserOrg+"&subAccessUserOrgID="+subAccessUserOrgID;
 		    
 			  if (!SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_OR_MAINTAIN_ATP_INV_CREATE_RULES)){
		    		initSearchParms+= "&ruleType=" + InstrumentType.LOAN_RQST;
	 				optionsRT.append("<option value='").
	    	        append(InstrumentType.LOAN_RQST).
	    	        append("' selected>").
	    	        append(resMgr.getText("RefdataHome.LRQ", TradePortalConstants.TEXT_BUNDLE)).
	    	        append("</option>");
	 				isFilterReadOnly = true; 
	    		     
	    		    	
 			  }


 		  	  if (!SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_OR_MAINTAIN_LR_CREATE_RULES)){
 		  			initSearchParms+= "&ruleType=" + InstrumentType.APPROVAL_TO_PAY;
 		  			optionsRT.append("<option value='").
	    	        append(InstrumentType.APPROVAL_TO_PAY).
	    	        append("' selected>").
	    	        append(resMgr.getText("RefdataHome.ATP", TradePortalConstants.TEXT_BUNDLE)).
	    	        append("</option>");
 		  			isFilterReadOnly = true;
 		  	  }
 		 	    	
			  
		%>

<%
  if (!readOnly && allowCreate) {
%>
  <div class="searchDivider"></div>
<%
  }
%>


			
  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">
      
      <%= widgetFactory.createSearchSelectField("RuleType", "LCCreationRuleList.RuleType",
    			"", optionsRT.toString(), "onChange='searchInvOnlyCreationRules();'") %>	
      
      <%= widgetFactory.createSearchSelectField("InvDefn", "RefDataHome.InvoiceDefinitions",
    			"", options.toString(), "onChange='searchInvOnlyCreationRules();'") %>
    </span>
  
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchPCreationsRule">
        <%= resMgr.getText("PartySearch.Search", TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchInvOnlyCreationRules();</script>
      </button>
    </span>
    <%=widgetFactory.createHoverHelp("searchPCreationsRule", "SearchHoverText") %>
    
    <div style="clear:both"></div>
  </div>
    	
		<%= gridHtml %>


		<%
	}

		   /***********************************************************************
		    * End list view for Invoice only Creation Rules List View Page
		    ***********************************************************************/

 		     
		    /***********************************************************************
		     * Begin list view for Notification Rule List View Page
		     ***********************************************************************/
		     
		     if (TradePortalConstants.REFDATA__NOTIFRULES.equals(refDataType)) 
		    {
		    	 if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("NotificationRulesDataView");
				  }
		    	  initSearchParms="";
		    	  
		    	
			      gridHtml = dGridFactory.createDataGrid("notificationRulesGridId","NotificationRulesDataGrid", null);
				  gridLayout = dGridFactory.createGridLayout("notificationRulesGridId","NotificationRulesDataGrid");
				  
			%>
			<%=gridHtml%>
						
			
		<%
		    }
		    /***********************************************************************
		     * End list view for Notification Rule List View Page
		     ***********************************************************************/
		     
		     /***********************************************************************
			     * Begin list view for Notification Rule Templates List View Page
			     ***********************************************************************/
			     
			     if (TradePortalConstants.REFDATA__NOTIFRULE_TEMPLATES.equals(refDataType)) 
			    {
			    	 if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
				 		   savedSearchQueryData = (Map)searchQueryMap.get("NotificationRuleTemplatesDataView");
					  }
			    	  initSearchParms="";
				      gridHtml = dGridFactory.createDataGrid("notificationRuleTemplatesGridId","NotificationRuleTemplatesDataGrid", null);
					  gridLayout = dGridFactory.createGridLayout("notificationRuleTemplatesGridId","NotificationRuleTemplatesDataGrid");
				%>
				<%=gridHtml%>
		 <%
		    }
		    /***********************************************************************
		     * End list view for Notification Rule Templates List View Page
		     ***********************************************************************/

		     
		     /***********************************************************************
		      * Begin list view for Bank Branch Rule List View Page  
		      ***********************************************************************/
		
		     if (TradePortalConstants.REFDATA__BANKBRANCHRULES.equals(refDataType)) 
		     {
		    	 if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		     
		 		   savedSearchQueryData = (Map)searchQueryMap.get("BankBranchRulesDataView");
			  }
		 %>
		
		  <%@ include file="/refdata/fragments/RefDataHome-BankBranchRule.frag" %>
		
		   <%
		     }
		     /***********************************************************************
		      * End list view for Bank Branch Rule List View Page
		      ***********************************************************************/
		    /***********************************************************************
		      * Begin list view for Payment Method Validation Rule List View Page  
		 	 * CR-507 Tang 12/01/09
		      ***********************************************************************/
		
		      if (TradePortalConstants.REFDATA__PAYMENTMETHVAL.equals(refDataType)) 
			     {
		    	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("PaymentMethodValidationDataView");
				  }   
		    	  //Build the where clause
		    	  
		    	  
			        gridHtml = dGridFactory.createDataGrid("paymentMethodValidationDataGridId","PaymentMethodValidationDataGrid", null);
					gridLayout = dGridFactory.createGridLayout("paymentMethodValidationDataGridId","PaymentMethodValidationDataGrid");
					
			 %>
			       <%=gridHtml%><br>
			 <%
			     }
		     /***********************************************************************
		      * End list view for Payment Method Validation Rule List View Page
		      ***********************************************************************/	 
			    /***********************************************************************
			      * Begin list view for Payment Method Definition List View Page  
			 	 * CR-599 RKAZI 11/21/2013
			      ***********************************************************************/
			
			      if (TradePortalConstants.REFDATA__PAYMENTDEFN.equals(refDataType))
				     {
			    	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
				 		   savedSearchQueryData = (Map)searchQueryMap.get("PaymentDefinitionsDataView");
					  }   
			    	  //Build the where clause
			    	  
			    	 
				        gridHtml = dGridFactory.createDataGrid("paymentDefinitionDataGridId","PaymentDefinitionsDataGrid", null);
						gridLayout = dGridFactory.createGridLayout("paymentDefinitionDataGridId","PaymentDefinitionsDataGrid");
						
				 %>
				       <%=gridHtml%><br>
				 <%
				     }
			     /***********************************************************************
			      * End list view for Payment Method Definition List View Page
			      ***********************************************************************/	 
		
		     /***********************************************************************
		      * Begin list view for Currency Calendar Rule List View Page  
		 	   * [START] CR-507 Currency Calendar Rule
		      ***********************************************************************/
		
		     if (TradePortalConstants.REFDATA__CURRENCYCALENDARRULE.equals(refDataType)) 
		     {
		    	 if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("CurrencyCalendarRuleDataView");
				  }
		    	
				gridHtml = dGridFactory.createDataGrid("CurrencyCalendarRuleDataGridId","CurrencyCalendarRuleDataGrid", null);
				gridLayout = dGridFactory.createGridLayout("CurrencyCalendarRuleDataGridId","CurrencyCalendarRuleDataGrid");
				
		 %>
				<%=gridHtml%>
				
		 <%
		     }
		     /***********************************************************************
		      * End list view for Currency Calendar Rule List View Page
		      * [END] CR-507 Currency Calendar Rule
		      ***********************************************************************/
		
		    /***********************************************************************
		     * Begin list view for Work Group List View Page
		     ***********************************************************************/
		
		    if (TradePortalConstants.REFDATA__WORKGROUPS.equals(refDataType)) {
		    	
		    	 if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("WorkGroupDataView");
				  }
		      initSearchParms="parentOrgID="+parentOrgID+"&subAccessUserOrgID="+subAccessUserOrgID+"&includeSubAccessUserOrg="+includeSubAccessUserOrg;
		    
		    gridHtml = dGridFactory.createDataGrid("workGroupGridId","WorkGroupDataGrid", null);
			  gridLayout = dGridFactory.createGridLayout("workGroupGridId","WorkGroupDataGrid");
			  
		%>

		<%=gridHtml %>
 
 		<%
		    }
		
		    /***********************************************************************
		     * End list view for PO Upload Definition List View Page
		     ***********************************************************************/
		%>
		
		<%
			  /***********************************************************************
		      * Begin list view for Receivables Matching Rules List View Page
		      ***********************************************************************/
		      if (TradePortalConstants.REFDATA__ARMMATCHRULES.equals(refDataType))
		          //We are on the Receivables Matching Rule List View Page
		       { if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 		   savedSearchQueryData = (Map)searchQueryMap.get("ARMMatchingRuleDataView");
				  }
		          %>
		  		<%@ include file="/refdata/fragments/RefDataHome-ArMatchRule.frag" %>
		    <%
		    }
		       /***********************************************************************
		       * End list view for Receivables Matching Rules List View Page
		       ***********************************************************************/
		    %>
		
		    <%
		    /***********************************************************************
		     * Begin list view for Calendar List View Page
		     ***********************************************************************/
		
		    if (TradePortalConstants.REFDATA__CALENDARS.equals(refDataType))
		    {
		    	
		    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("CalendarsDataView");
					}
		    	
		    	gridHtml = dGridFactory.createDataGrid("CalendarsDataGridId","CalendarsDataGrid", null);
				gridLayout = dGridFactory.createGridLayout("CalendarsDataGridId","CalendarsDataGrid");
				
  		%>
			
			<%=gridHtml %>
		       
		<%
		    }
		    /***********************************************************************
		     * End list view for Calendar List View Page
		     ***********************************************************************/
		 %>    
		 
		 <%
		     /***********************************************************************
		      * DK CR-587 Rel7.1 Begin list view for Gen Msg Categories List View Page
		      ***********************************************************************/
		 
		      if (TradePortalConstants.REFDATA__GENERICMSGCATEGORIES.equals(refDataType))
			     {
		    	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("GenericMessageCategoryDataView");
					}	
		    	  
		    	  
		    	  
			     	gridHtml = dGridFactory.createDataGrid("GenericMessageCategoryDataGridId","GenericMessageCategoryDataGrid", null);
					gridLayout = dGridFactory.createGridLayout("GenericMessageCategoryDataGridId","GenericMessageCategoryDataGrid");
					
			 %>
				<%=gridHtml %>
			       
			 <%
			     }
		     /***********************************************************************
		      * DK CR-587 Rel7.1 End list view for Gen Msg Categories List View Page
		      ***********************************************************************/
		 %>
		 
		 <%
		     /***********************************************************************
		      * DK CR-581/640 Rel7.1 Begin list view for Cross Rate Calculation Rule List View Page
		      ***********************************************************************/
		 
		     if (TradePortalConstants.REFDATA__CROSSRATERULE.equals(refDataType))
		     {
		    	 if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("CrossRateRuleDataView");
					}
		    	 
		    	gridHtml = dGridFactory.createDataGrid("CrossRateRuleDataGridId","CrossRateRuleDataGrid", null);
				gridLayout = dGridFactory.createGridLayout("CrossRateRuleDataGridId","CrossRateRuleDataGrid");
				
		 %>
		         <%=gridHtml %>

		 <%
		     }

		      if (TradePortalConstants.REFDATA__INTDISRATE.equals(refDataType))
			     {
		    	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 		   savedSearchQueryData = (Map)searchQueryMap.get("InterestGroupRatesDataView");
					}	
		    	  
					gridHtml = dGridFactory.createDataGrid("InterestGroupRatesDataGridId","InterestGroupRatesDataGrid", null);
					gridLayout = dGridFactory.createGridLayout("InterestGroupRatesDataGridId","InterestGroupRatesDataGrid");
					
		 %>
		        <%=gridHtml %>
		 <%
		   }
		 %>   
		</div>		
		<input type="hidden" name="cTime" id="cTime" />
		<input type="hidden" name="prevPage" id="prevPage" />
		</form>
	</div>
</div>


<div id="newTemplateID" ></div>



<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%@ include file="/common/GetSavedSearchQueryData.frag" %> 
<%  //include a hidden form for new instrument submission
    // this is used for all of the new instrument types available from the menu
    Hashtable newInstrSecParms = new Hashtable();
    
	if(TradePortalConstants.REFDATA__TEMPLATE.equals(refDataType)){
		newInstrSecParms.put("userOid", userSession.getUserOid());
		newInstrSecParms.put("securityRights", userSession.getSecurityRights());
	    newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
	    newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
	    newInstrSecParms.put("ownerLevel", userSession.getOwnershipLevel());
	}   
		
    
%>
<div id="poUploadDefnSelectionDialog" ></div>
<div id="InvDefinitionSelectorDialog" ></div>
  <form method="post" name="NewTemplateForm" action="<%=formMgr.getSubmitAction(response)%>">
    <input type="hidden" name="mode" />
    <input type="hidden" name="CopyType" />
    <input type="hidden" name="InstrumentType" />
    <input type="hidden" name="copyInstrumentOid" />
    <input type="hidden" name="PaymentTemplGrp" />
    <input type="hidden" name="name" />
    <input type="hidden" name="expressFlag" />
    <input type="hidden" name="fixedFlag" />
    <input type="hidden" name="validationState" />
    <%= formMgr.getFormInstanceAsInputField("NewTemplateForm",newInstrSecParms) %>
  </form>

<div id="notificationTemplateSelectionDialog" ></div>

<script>

function filterPartiesOnEnter(fieldID){
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	searchParties();
	        }
	    });
	});	<%-- end require --%>
}<%-- end of filterPartiesOnEnter() --%>
function filterFxrategroupOnEnter(fieldID){
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	searchFXRates();
	        }
	    });
	});	<%-- end require --%>
}<%-- end of filterFxrategroupOnEnter() --%>
function filterBankBranchRuleOnEnter(fieldID){
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	searchBankBranchRules();
	        }
	    });
	});	<%-- end require --%>
}<%-- end of filterBankBranchRuleOnEnter() --%>
	
	var initSearchParms="";
	var copyType="";
	var templateName="";
	var templateGroup="";
	var flagExpressFixed="";
	var instrType="";
	
	
	
	function initializeDataGrid(DataGridId,DataView,encryptedDataView){
		require(["dojo/dom", "dijit/registry", "t360/OnDemandGrid", "dojo/dom-construct"], function(dom, registry, onDemandGrid, domConstruct){
			<%--get grid layout from the data grid factory--%>
			var gridLayout = <%=gridLayout%>;
	
			<%--set the initial search parms--%>
			initSearchParms = "<%=initSearchParms%>";
			console.log("initSearchParms: " + initSearchParms);
	
			<%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
			<%--cquinton 9/12/2012 Rel portal refresh ir#t36000004008 remove default sort on column 0 as this is default in the function--%>
		 
   			var savedSort = savedSearchQueryData["sort"];   	
    		
    	 	if("" == savedSort || null == savedSort || "undefined" == savedSort){
    		 	if(DataGridId=='fxRateGrid' )
    			 	savedSort = 'FXRateGroup';
    		 	else
			  		savedSort = '0';
    	 	}
    	 
    	 	if(DataGridId=='PartiesDataGridId' || DataGridId=='fxRateGrid'){
    	 		var filterText = savedSearchQueryData["filterText"];   
    	 		if("" == filterText || null == filterText || "undefined" == filterText ){
 					if(registry.byId("filterText"))
    	  				filterText=(dom.byId("filterText").value).toUpperCase();
 					else
 						filterText="";
 		 		} else {
 		 			registry.byId("filterText").set('value',filterText);
 		 		}
    	 		
 		 		initSearchParms = initSearchParms+"&filterText="+filterText; 
    	 	} else if(DataGridId=='creationRuleGridId' ){
    		 	var ruleType = savedSearchQueryData["ruletype"];   
    		 	var podef = savedSearchQueryData["podef"];   
     		 	if("" == ruleType || null == ruleType || "undefined" == ruleType)
     				ruleType=(dijit.byId("RuleType").value).toUpperCase();
     		 	else
     				registry.byId("RuleType").set('value',ruleType);
     		
     		 	if("" == podef || null == podef || "undefined" == podef)
     				podef=dijit.byId("PODefn").attr('value');
       		 	else
       				registry.byId("PODefn").set('value',podef);
     		
     		 	initSearchParms = initSearchParms+"&ruletype="+ruleType+"&podef="+podef;
    	 	} else if(DataGridId=='InvOnlyCreateRuleDataGridId' ){
    		 	var ruleType = savedSearchQueryData["ruleType"];   
    		 	var podef = savedSearchQueryData["invDef"];   
     		 	if("" == ruleType || null == ruleType || "undefined" == ruleType)
     				ruleType=(dijit.byId("RuleType").value).toUpperCase();
     		 	else
     				registry.byId("RuleType").set('value',ruleType);
     		 
     	 		if("" == podef || null == podef || "undefined" == podef)
     				podef=dijit.byId("InvDefn").attr('value');
       			else
       				registry.byId("InvDefn").set('value',podef);
     		
     		 	initSearchParms = initSearchParms+"&ruleType="+ruleType+"&invDef="+podef;
    	 	} else if(DataGridId=='ARMMatchingRuleDataGridId' ){
    		 	var buyerName = savedSearchQueryData["buyerName"];   
    		 	var buyerId = savedSearchQueryData["buyerId"];   
     		 
    		 	if("" == buyerName || null == buyerName || "undefined" == buyerName)
     				buyerName=(dom.byId("BuyerName").value).toUpperCase();
     		 	else
     				dom.byId("BuyerName").value = buyerName;
     		 
     	 		if("" == buyerId || null == buyerId || "undefined" == buyerId)
     	 			buyerId=(dom.byId("BuyerId").value).toUpperCase();
       			else
       				dom.byId("BuyerId").value = buyerId;
     		
     		 	initSearchParms = initSearchParms+"&buyerName="+buyerName+"&buyerId="+buyerId;
    	 	} else if(DataGridId=='BankBranchRulesDataGridId' ){
    		 	var PaymentMethod = savedSearchQueryData["PaymentMethod"];   
    		 	var BankBranchCode = savedSearchQueryData["BankBranchCode"];
    		 	var BranchName = savedSearchQueryData["BranchName"];   
    		 	var BankName = savedSearchQueryData["BankName"];
    		 	var City = savedSearchQueryData["City"];
    		 	var Country = savedSearchQueryData["Country"];
     		 
    		 	if("" == PaymentMethod || null == PaymentMethod || "undefined" == PaymentMethod)
     				PaymentMethod=(dom.byId("PaymentMethod").value).toUpperCase();
     		 	else
     				registry.byId("PaymentMethod").set('value',PaymentMethod);
     		 
     	 		if("" == BankBranchCode || null == BankBranchCode || "undefined" == BankBranchCode)
     	 			BankBranchCode=(dom.byId("BankBranchCode").value).toUpperCase();
       			else
       				dom.byId("BankBranchCode").value = BankBranchCode;
     	 	
     	 		if("" == BranchName || null == BranchName || "undefined" == BranchName)
     	 			BranchName=(dom.byId("BranchName").value).toUpperCase();
       			else
       				dom.byId("BranchName").value = BranchName;
     	 	
     	 		if("" == BankName || null == BankName || "undefined" == BankName)
     	 			BankName=(dom.byId("BankName").value).toUpperCase();
       			else
       				dom.byId("BankName").value = BankName;
     		
     	 		if("" == City || null == City || "undefined" == City)
     	 			City=(dom.byId("City").value).toUpperCase();
       			else
       				dom.byId("City").value = City;
     	 		
    		 	if("" == Country || null == Country || "undefined" == Country)
     				Country=(dom.byId("Country").value).toUpperCase();
     		 	else
     				registry.byId("Country").set('value',Country);     	 		
     	 		
     		 
     	 		initSearchParms = initSearchParms+"&PaymentMethod="+PaymentMethod+"&BankBranchCode="+BankBranchCode+"&BranchName="+BranchName+
     		 						"&BankName="+BankName+"&City="+City+"&Country="+Country;
     		 
     	 		if(BankBranchCode!='' || BranchName!='' || BankName!='' || City!='' || Country!=''){
     				var div = document.getElementById('bankBranchRulesGrid') ;
            		div.hidden = false;
     		 	}
    	 	} 
    	
    		onDemandGrid.createOnDemandGrid(DataGridId, encryptedDataView,gridLayout, initSearchParms,savedSort);
			
	 	});
	}

	
	 
	function searchParties() {
	    require(["dojo/dom", "t360/OnDemandGrid"], function(dom, onDemandGrid){
	        var filterText=(dom.byId("filterText").value).toUpperCase();
	        console.log("filterText: " + filterText);
	      
	        var searchParms = "<%=initSearchParms%>"+"&filterText="+filterText; 
	        console.log("SearchParms: " + searchParms);
	        onDemandGrid.searchDataGrid("PartiesDataGridId", "PartiesDataView", searchParms);
	    });
	}
	 
	function searchFXRates() {
		require(["dojo/dom", "t360/OnDemandGrid"], function(dom, onDemandGrid){		    
			var filterText=(dom.byId("filterText").value).toUpperCase();
	        console.log("filterText: " + filterText);
	  
	        var searchParms = "<%=initSearchParms%>"+"&filterText="+filterText; 
	        console.log("SearchParms: " + searchParms);
	        onDemandGrid.searchDataGrid("fxRateGrid", "FXRateDataViewSortCur", searchParms);
		});
	}
	 
	  
	  
			function searchCreationRules() {
			  	require(["dojo/dom", "t360/OnDemandGrid"], function(dom, onDemandGrid){
			        var ruleType=dijit.byId("RuleType").attr('value');
			        var podef=dijit.byId("PODefn").attr('value');
			        var ruleName=dijit.byId("RuleName").attr('value');
			        var templateName=dijit.byId("TemplateName").attr('value');
			        console.log("ruleType: " + ruleType);
			        console.log("podef: " + podef);
			        console.log("ruleName: " + ruleName);
			        console.log("templateName: " + templateName);
			      
			        var searchParms = "<%=initSearchParms%>"+"&ruletype="+ruleType+"&podef="+podef+"&ruleName="+ruleName+"&templateName="+templateName;
			        console.log("SearchParms: " + searchParms);
			        <%	if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat)){%>
			        	onDemandGrid.searchDataGrid("creationRuleGridId", "CreationRuleDataView", searchParms);
					<%}	else { %>
						onDemandGrid.searchDataGrid("creationRuleGridId", "CreationRuleStructPODataView", searchParms);
			        <%}%>
			    });
			}
	 

		  	function searchTemplate() {
		  		require(["dojo/dom", "t360/OnDemandGrid"], function(dom, onDemandGrid){
			        var templateName=dijit.byId("TemplateNm").attr('value');
			        console.log("templateName: " + templateName); 
			        var searchParms = "<%=initSearchParms%>"+"&templateName="+templateName;			        
			        console.log("SearchParms: " + searchParms);
			    <%	if(ownershipLevel.equals(TradePortalConstants.OWNER_BANK)){%>
			    		onDemandGrid.searchDataGrid("templateBankGridId","TemplateBankDataView", searchParms);
				<%	} else { %>
						onDemandGrid.searchDataGrid("templateGridId","TemplateDataView", searchParms);
		        <%}%>
				});
			}		  
	


		  	function searchInvOnlyCreationRules() {
		  		require(["dojo/dom", "t360/OnDemandGrid"], function(dom, onDemandGrid){			       
			    	var ruleType =dijit.byId("RuleType").attr('value');
			        var invDef	 =dijit.byId("InvDefn").attr('value');
			       
			        console.log("ruleType: " + ruleType);
			        console.log("InvDef: " + invDef);
			        
			        var searchParms = "<%=initSearchParms%>"+"&ruleType="+ruleType+"&invDef="+invDef;
			        console.log("SearchParms: " + searchParms);
			        onDemandGrid.searchDataGrid("InvOnlyCreateRuleDataGridId", "InvOnlyCreateRuleDataView", searchParms);
			    });
			}		  
	  
	 

			function searchARMRules() {
				require(["dojo/dom", "t360/OnDemandGrid"], function(dom, onDemandGrid){	
			        var BuyerName=(dom.byId("BuyerName").value).toUpperCase();
			        var BuyerId=(dom.byId("BuyerId").value).toUpperCase();
			        console.log("BuyerName: " + BuyerName);
			        console.log("buyerId: " + BuyerId);
			        
			        var searchParms;
			        searchParms="<%=initSearchParms%>";
			   		
			        	searchParms= searchParms+"&buyerName="+BuyerName;
			    	
			        	searchParms= searchParms+"&buyerId="+BuyerId; 
			   		
			        	
			        console.log("SearchParms: " + searchParms);
			        onDemandGrid.searchDataGrid("ARMMatchingRuleDataGridId", "ARMMatchingRuleDataView", searchParms);
		      	});
		  	}
	 
	  
	  require(["dijit/registry", "dojo/on", 
	           "dojo/ready"],
	      function(registry, on, ready ) {
	    ready(function() {
	    	dojo.query("input[type='button']").forEach(function(node){
				registry.getEnclosingWidget(node).on( "click", function(evt){
					console.log(evt);
					var cTime = evt.timeStamp;
					document.getElementById("cTime").value= (new Date(cTime)).getTime();
					document.getElementById("cTime").text= (new Date(cTime)).getTime();
					document.getElementById("prevPage").value= context;
					document.getElementById("prevPage").text= context;
					});
		  	});
	    	
	    	
	    	var fxRateListCount = "<%= fxRateListCount%>";
	    	
	    	console.log('page ready');
	    	<%
			  String editAllLink = formMgr.getLinkAsUrl("editAllFXRates", response);
			%>
	    	console.log('editAllLink='+'<%=editAllLink%>');	
	     	      <%--search event handlers--%>
	      var editFxRatesButton = registry.byId("RefDataFXRate_EditAllExistingRates");
	      if(fxRateListCount != 0 && fxRateListCount > 1){
	    	  if ( editFxRatesButton ) {
		    	  editFxRatesButton.on("click", function() {
		    		  window.location = "<%= editAllLink %>";
		        });
		      }
	      }else{  
	    	  if ( editFxRatesButton ) {
	    	  	editFxRatesButton.domNode.style.display="none";
	    	  }
	      }	      
	    });	  
	  });
	 
	 
	  function openNewNotificationTemplateDialog(){
		  console.log("inside openNewNotificationTemplateDialog()");
		  
		  require(["t360/dialog"], function(dialog ) {
		      dialog.open('notificationTemplateSelectionDialog', '<%=resMgr.getText("NotificationTemplate.NotificationTemplateSelector", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'notificationRuleTemplateSelectorDialog.jsp',
		                  null, null, //parameters
		                  'select', this.openNotificationTemplateCallback);
		  });
	  }
	  
	  	  function openNotificationTemplateCallback(notifiTemplateOid){
		  console.log("inside openNotificationTemplateCallback()");		  
		  var theForm = document.getElementsByName('NotificationRuleForm')[0];
		  theForm.notificationTemplateId.value=notifiTemplateOid;
		  theForm.submit();
	  }

		function searchBankBranchRules() {
			require(["t360/OnDemandGrid"], function(onDemandGrid){
				var PaymentMethod = dijit.byId("PaymentMethod").attr('value');
			    var BankBranchCode=dijit.byId("BankBranchCode").attr('value'); 
			    var BranchName= dijit.byId("BranchName").attr('value'); 
			    var BankName=dijit.byId("BankName").attr('value'); 
			    var City= dijit.byId("City").attr('value'); 
			    var Country = dijit.byId("Country").attr('value');
			    var flag = true; 
		    
			   
		
		    	var searchParms;
	        	searchParms="<%=initSearchParms%>";
	        
	        	if(PaymentMethod !=null && "" != PaymentMethod){
	        		searchParms= searchParms+"&PaymentMethod="+PaymentMethod;
	        	}
	        	if(BankBranchCode !=null && "" != BankBranchCode){
	        		searchParms= searchParms+"&BankBranchCode="+BankBranchCode; 
	        	}
	        	if(BankName !=null && "" != BankName){
	        		searchParms= searchParms+"&BankName="+BankName; 
	        	}
	        	if(BranchName !=null && "" != BranchName){
	        		searchParms= searchParms+"&BranchName="+BranchName; 
	        	}
	        	if(City !=null && "" != City){
	        		searchParms= searchParms+"&City="+City; 
	        	}
	        	if(Country !=null && "" != Country){
	        		searchParms= searchParms+"&Country="+Country; 
	        	}	        	
	        
	        	bankBranchRulesSearchParms = searchParms;
				console.log("SearchParmsaa: " + searchParms);
	        <%--  jgadela 10/29/2015 R 94 T36000045171 - Fixed Payment Method and Country combination. --%>
				if(BankBranchCode!='' || BranchName!='' || BankName!='' || City!='' || Country!=''){
					onDemandGrid.searchDataGrid("BankBranchRulesDataGridId", "BankBranchRulesDataView", searchParms);
	        		var div = document.getElementById('bankBranchRulesGrid') ;
	        		div.hidden = false;
	        	}
			});
		} 
	  
	
	  
	  function openNewTemplateDialog(){
		  require(["t360/dialog"], function(dialog) {
			  dialog.open('newTemplateID', '<%=resMgr.getText("NewTemplate.Title",TradePortalConstants.TEXT_BUNDLE) %>',
                  'newTemplateDialog.jsp',
                  null, null, 
                  'select', this.createTemplate);
		  });	  
	  }
	
	  function createTemplate(copyType1, templateName1, templateGroup1, flagExpressFixed1, instrType1){
		  console.log("copyType - "+copyType1+ " templateName - "+templateName1+ " templateGroup - "+templateGroup1+ " flagExpress/flagFixed - "+flagExpressFixed1+ " instrType1 - "+instrType1);
		  copyType=copyType1;
		  templateName = templateName1;
		  templateGroup = templateGroup1;
		  flagExpressFixed = flagExpressFixed1;
		  instrType = instrType1;
		  <%--  KMehta IR-T36000025579 Rel 9300 on 17-Jun-2015 Change - DialogTitle --%>
		  if(copyType == 'Instr')
		  {
			  require(["t360/dialog"], function(dialog) {		 	      
		 	   	  dialog.open('instrumentSearchDialog', '<%=instrumentSearchDialogTitle%>',
			                      'instrumentSearchDialog.jsp',
			                      'createTemplFromInstrFlag', 'Y', <%-- parameters --%>
			                      'select', this.createNewTemplateFromInstrumentSelected);
			  });  
		  }else if(copyType == 'Templ'){
			  require(["t360/dialog"], function(dialog) {
				  dialog.open('templateSearchDialog', templateSearchDialogTitle,
			                'templateSearchDialog.jsp',
			                'createTemplFromTemplFlag', 'Y', <%-- parameters --%>
			                'select', this.createNewInstrumentTemplateSelected); 
			  });
		  }else if(copyType == 'Blank'){
			  createNewBlankTemplateSelected();
		  }
	  }
	  
	  function createNewTemplateFromInstrumentSelected(){
		  
		  if ( document.getElementsByName('NewTemplateForm').length > 0 ) {
		        var theForm = document.getElementsByName('NewTemplateForm')[0];
		        theForm.mode.value='CREATE_NEW_TEMPLATE';
		        theForm.CopyType.value=copyType;
		        theForm.name.value=templateName;
		        theForm.PaymentTemplGrp.value=templateGroup;
		        theForm.copyInstrumentOid.value=rowKeys;
		        theForm.submit();
		  } else {
		        alert('Problem in createNewInstrumentFromExisting: cannot find NewInstrumentForm');
		  }
	  }
	  
	  function createNewInstrumentTemplateSelected(){
		  
		  if ( document.getElementsByName('NewTemplateForm').length > 0 ) {
		        var theForm = document.getElementsByName('NewTemplateForm')[0];
		        theForm.mode.value='CREATE_NEW_TEMPLATE';
		        theForm.CopyType.value=copyType;
		        theForm.name.value=templateName;
		        theForm.PaymentTemplGrp.value=templateGroup;
		        theForm.copyInstrumentOid.value=rowKeys;
		        theForm.submit();
		  } else {
		        alert('Problem in createNewInstrumentFromExisting: cannot find NewInstrumentForm');
		  }
	  }
	  
	  function createNewBlankTemplateSelected(){
		  
		  if ( document.getElementsByName('NewTemplateForm').length > 0 ) {
		        var theForm = document.getElementsByName('NewTemplateForm')[0];
		        theForm.mode.value='CREATE_NEW_TEMPLATE';
		        theForm.CopyType.value=copyType;
		        theForm.name.value=templateName;
		        theForm.PaymentTemplGrp.value=templateGroup;
		        theForm.InstrumentType.value = instrType;
		        theForm.validationState.value='VALIDATE_NEW_TEMP';
		        theForm.expressFlag.value = flagExpressFixed.split('/')[0];
		        theForm.fixedFlag.value = flagExpressFixed.split('/')[1];
		        theForm.submit();
		  } else {
		        alert('Problem in createNewInstrumentFromExisting: cannot find NewInstrumentForm');
		  }
	  }

	  function openPODefinationSelection(){
		  console.log("inside openPODefinationSelection()");
		  
		  require(["t360/dialog"], function(dialog ) {
		      dialog.open('poUploadDefnSelectionDialog', '<%=resMgr.getText("POUploadDefnSelector.PODefinitionSelector", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'POUploadDefinitionSelectorDialog.jsp',
		                  null, null, <%-- parameters --%>
		                  'select', this.openPODefinationSelectionCallback);
		  });
	  }
	  
	  function openPODefinationSelectionCallback(poUplDefnOId){
		  console.log("inside openPODefinationSelectionCallback()");
		  
		  
		  var theForm = document.getElementsByName('POATPUploadDefnSelection')[0];
		  theForm.UploadDefinition.value=poUplDefnOId;
		  theForm.submit();
	  }
	  	  function openInvDefinationSelection(){
		  console.log("inside openInvDefinationSelection()");
		  
		  require(["t360/dialog"], function(dialog ) {
		      dialog.open('InvDefinitionSelectorDialog', '<%=resMgr.getText("InvOnlyRuleDefnSelector.InvDefinitionSelector", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'InvDefinitionSelectorDialog.jsp',
		                  null, null, <%-- parameters --%>
		                  'select', this.openInvDefinationSelectionCallback);
		  });
		  console.log("closing openInvDefinationSelection()");
	  }
	  
	  function openInvDefinationSelectionCallback(poUplDefnOId){
		  console.log("inside openPODefinationSelectionCallback()");
		 
		  
		  var theForm = document.getElementsByName('InvDefnSelection')[0];
		  theForm.UploadDefinition.value=poUplDefnOId;
		  theForm.submit();
	  }
	  
</script>
	<form name="POATPUploadDefnSelection" method="POST" 
      action="<%= request.getContextPath()+NavigationManager.getNavMan().getPhysicalPage("ValidateATPDefinitionSelection", request) %>">
      <input type=hidden value="" name=UploadDefinition>
     </form>

	<form name="InvDefnSelection" method="POST" 
      action="<%= request.getContextPath()+NavigationManager.getNavMan().getPhysicalPage("ValidateATPDefinitionSelection", request) %>">
      <input type=hidden value="" name=UploadDefinition>
      <input type=hidden value="INO" name=InsType>
     </form>
	
		<form name="NotificationRuleForm" method="POST" 
      action="<%= request.getContextPath()+NavigationManager.getNavMan().getPhysicalPage("NotificationRuleDetail", request) %>">
      <input type=hidden value="" name=notificationTemplateId>
     </form>

<%-- Moved all initializations under ready() function--%>

	<jsp:include page="/common/dGridShowCountFooter.jsp">
  		<jsp:param name="gridId" value="<%=gridId%>" />
	</jsp:include>
	
    
<script type="text/javascript">
  require(["dijit/registry", "dojo/on", "dojo/ready"],
    function(registry, on, ready ) {
        ready(function() {
    
        	<% if (TradePortalConstants.REFDATA__PARTIES.equals(refDataType)){%>
        		initializeDataGrid("PartiesDataGridId","PartiesDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("PartiesDataView",userSession.getSecretKey())%>");
    			
    	<%} %>

    	<% if (TradePortalConstants.REFDATA__INVONLY.equals(refDataType)){%>
    			initializeDataGrid("InvOnlyCreateRuleDataGridId","InvOnlyCreateRuleDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("InvOnlyCreateRuleDataView",userSession.getSecretKey())%>");
    	
    	<%} if (TradePortalConstants.REFDATA__PHRASES.equals(refDataType)){%>
    		
    			initializeDataGrid("PhrasesDataGridId","PhrasesDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("PhrasesDataView",userSession.getSecretKey())%>");
    	<%--  pgedupudi - Rel9.3 CR976A 03/09/2015 START  --%> 
    	<%} if (TradePortalConstants.REFDATA__BANKGRPRESTRICTRULES.equals(refDataType)){%>
    		
    			initializeDataGrid("BankGrpRestrnRulesDataGridId","BankGrpRestrnRulesDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("BankGrpRestrnRulesDataView",userSession.getSecretKey())%>");
    	<%--  pgedupudi - Rel9.3 CR976A 03/09/2015 END  --%>	
    	<%} if (TradePortalConstants.REFDATA__CALENDARS.equals(refDataType)){%>
    		
    			initializeDataGrid("CalendarsDataGridId","CalendarsDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("CalendarsDataView",userSession.getSecretKey())%>");
    		
    	<%}  if (TradePortalConstants.REFDATA__CROSSRATERULE.equals(refDataType)){%>
    			initializeDataGrid("CrossRateRuleDataGridId","CrossRateRuleDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("CrossRateRuleDataView",userSession.getSecretKey())%>");
    		
    	<%}  if (TradePortalConstants.REFDATA__PANELGROUPS.equals(refDataType)){%>
    			initializeDataGrid("PanelGroupsDataGridId","PanelGroupsDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("PanelGroupsDataView",userSession.getSecretKey())%>");
    		
    	<%}  if (TradePortalConstants.REFDATA__FXRATES.equals(refDataType)){%>
    		<%--  To Intialize Datagrid for Phrases -- > --%>
    			initializeDataGrid("fxRateGrid","FXRateDataViewSortCur","<%=EncryptDecrypt.encryptStringUsingTripleDes("FXRateDataViewSortCur",userSession.getSecretKey())%>");
    	
    	<%}  if (TradePortalConstants.REFDATA__USERS.equals(refDataType)){%>
    			initializeDataGrid("UsersDataGridId","UsersDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("UsersDataView",userSession.getSecretKey())%>");
    		
    	<%} if (TradePortalConstants.REFDATA__SECURITY.equals(refDataType)){%>
    			<%--  To Intialize Datagrid for PanelGroups --%>
    			initializeDataGrid("secProfGridId","SecurityProfileDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("SecurityProfileDataView",userSession.getSecretKey())%>");
    	<%}if (TradePortalConstants.REFDATA__ATPCREATERULES.equals(refDataType)){
    		if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat)){%>
    			<%--  To Intialize Datagrid for PanelGroups  --%>
    			initializeDataGrid("creationRuleGridId","CreationRuleDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("CreationRuleDataView",userSession.getSecretKey())%>");
    		<%}	else { //if(TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(uploadFormat)){%>
    			<%--  To Intialize Datagrid for PanelGroups  --%>
    			initializeDataGrid("creationRuleGridId","CreationRuleStructPODataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("CreationRuleStructPODataView",userSession.getSecretKey())%>");
    	<%}} 
    		if (TradePortalConstants.REFDATA__NOTIFRULES.equals(refDataType)) {%>
    		<%-- To Intialize Datagrid for Notification Rules --%>
    		initializeDataGrid("notificationRulesGridId","NotificationRulesDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("NotificationRulesDataView",userSession.getSecretKey())%>");
    		<%}
    		if (TradePortalConstants.REFDATA__NOTIFRULE_TEMPLATES.equals(refDataType)){%>
    		<%-- To Intialize Datagrid for Notification Rule Templates --%>
    		initializeDataGrid("notificationRuleTemplatesGridId","NotificationRuleTemplatesDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("NotificationRuleTemplatesDataView",userSession.getSecretKey())%>");
    		<%}
    		if (TradePortalConstants.REFDATA__TEMPLATE.equals(refDataType)) {%>
    		<%-- To Intialize Datagrid for Notification Rules --%>
    		<%if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {%>
    			initializeDataGrid("templateBankGridId","TemplateBankDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("TemplateBankDataView",userSession.getSecretKey())%>");
    		<%}else{%>
    			initializeDataGrid("templateGridId","TemplateDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("TemplateDataView",userSession.getSecretKey())%>");
    		<%}%>
    		
    	<%} 
    		if (TradePortalConstants.REFDATA__PAYTEMPLATEGROUP.equals(refDataType)) { %>
    			initializeDataGrid("paymentTemplateGroupGridId","PaymentTemplateGroupDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentTemplateGroupDataView",userSession.getSecretKey())%>");
    	<% } 
    	if (TradePortalConstants.REFDATA__WORKGROUPS.equals(refDataType)) { %>
    			initializeDataGrid("workGroupGridId","WorkGroupDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("WorkGroupDataView",userSession.getSecretKey())%>");
    	<% } 
    	if (TradePortalConstants.REFDATA__CURRENCYCALENDARRULE.equals(refDataType)){ %>
    	 		initializeDataGrid("CurrencyCalendarRuleDataGridId","CurrencyCalendarRuleDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("CurrencyCalendarRuleDataView",userSession.getSecretKey())%>");
    	 <%} 
    	if (TradePortalConstants.REFDATA__THRESHOLD.equals(refDataType)){%>
    			<%-- To Intialize Datagrid for REFDATA__THRESHOLD --%>
    			initializeDataGrid("ThresholdGroupDataGridId","ThresholdGroupDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("ThresholdGroupDataView",userSession.getSecretKey())%>");
    	<%}  if (TradePortalConstants.REFDATA__GENERICMSGCATEGORIES.equals(refDataType)){%>
    	 		<%-- To Intialize Datagrid for generic messages --%>
    			initializeDataGrid("GenericMessageCategoryDataGridId","GenericMessageCategoryDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("GenericMessageCategoryDataView",userSession.getSecretKey())%>");
    		
    	<%}  if (TradePortalConstants.REFDATA__ARMMATCHRULES.equals(refDataType)){%>
    	 
    			<%-- To Intialize Datagrid for AR matching rules --%>
    			initializeDataGrid("ARMMatchingRuleDataGridId","ARMMatchingRuleDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("ARMMatchingRuleDataView",userSession.getSecretKey())%>");
    			
    	<%} 
    	if (TradePortalConstants.REFDATA__POUPLOADDEFN.equals(refDataType)){
    	 	if (poStructUpld){
    	 %>
    	 
    	 	<%-- To Intialize Datagrid for po upload definitions --%>
    				initializeDataGrid("POStructuredDefinitionDataGridId","POStructuredDefinitionDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("POStructuredDefinitionDataView",userSession.getSecretKey())%>");
    	<%	} 
    		else
    		{
    	%>	
    	 			initializeDataGrid("POUploadDefinitionDataGridId","POUploadDefinitionDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("POUploadDefinitionDataView",userSession.getSecretKey())%>");
    	<%			
    		}
    	     }
    	%>
    	<%  if (TradePortalConstants.REFDATA__BANKBRANCHRULES.equals(refDataType)){%>
    			<%-- To Intialize Datagrid for bank branch rules --%>
    			initializeDataGrid("BankBranchRulesDataGridId","BankBranchRulesDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("BankBranchRulesDataView",userSession.getSecretKey())%>");
    	
    	<%} %>
    	
    	
    	<%
    		if (TradePortalConstants.REFDATA__PAYMENTMETHVAL.equals(refDataType)) 
        	{
    	%>
    			<%-- To Intialize Datagrid for PanelGroups --%>
    			initializeDataGrid("paymentMethodValidationDataGridId","PaymentMethodValidationDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentMethodValidationDataView",userSession.getSecretKey())%>");
    	
    	<%	} %>

    	<%
			if (TradePortalConstants.REFDATA__PAYMENTDEFN.equals(refDataType))
	    	{
		%>
			<%-- To Intialize Datagrid for Payment Method Definition --%>
				initializeDataGrid("paymentDefinitionDataGridId","PaymentDefinitionsDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentDefinitionsDataView",userSession.getSecretKey())%>");
	
		<%	} %>
    	
    	
    	 <%  if (TradePortalConstants.REFDATA__INVOICEDEFN.equals(refDataType)){%>
    	 
    			<%-- To Intialize Datagrid for Invoice Definition --%>
    			initializeDataGrid("InvoiceDefinitionDataGridId","InvoiceDefinitionDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoiceDefinitionDataView",userSession.getSecretKey())%>");
    		
    	<%} %>
    	
    	<%  if (TradePortalConstants.REFDATA__INTDISRATE.equals(refDataType)){%>
    	
    			<%-- To Intialize Datagrid for Invoice Definition --%>
    			initializeDataGrid("InterestGroupRatesDataGridId","InterestGroupRatesDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("InterestGroupRatesDataView",userSession.getSecretKey())%>");
    			
    	<%} %>
    	
    	
    	<%  
    	//AAlubala - Rel8.2 CR741 - ERP - BEGIN
    	
    	if (TradePortalConstants.REFDATA__ERPGLCODES.equals(refDataType)){%>
    	
    			<%-- To Intialize Datagrid for ERP GL Codes --%>
    			initializeDataGrid("ErpGlCodesDataGridId","ErpGlCodesDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("ErpGlCodesDataView",userSession.getSecretKey())%>");
    		
    	<%} %>
    	
    	<%  if (TradePortalConstants.REFDATA__DISCOUNTCODES.equals(refDataType)){%>
    	
    			<%-- To Intialize Datagrid for Discount Codes --%>
    			initializeDataGrid("DiscountCodesDataGridId","DiscountCodesDataView","<%=EncryptDecrypt.encryptStringUsingTripleDes("DiscountCodesDataView",userSession.getSecretKey())%>");
    		
    	<%} 
    	
    	%>
      
      var newButton = registry.byId("newButton");
      if ( newButton ) {
        newButton.on("click", function() {
        	var newLink = '<%=newLink%>';
        
        	if (isActive =='Y') {
        		if (newLink != '' && newLink.indexOf("javascript:") == -1) {
	        		var cTime = (new Date()).getTime();
			        newLink = newLink + "&cTime=" + cTime;
			        newLink = newLink + "&prevPage=" + context;
        		}
		    }
        	window.location = newLink;
        });
      }
    });
  });
</script>

</body>

</html>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>