
<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 
String parmValue = "";
String dropdownOptions = null;
boolean isReadOnly = false;
  
  parmValue = request.getParameter("marginIndex");
  int marginIndex = 0;
  if ( parmValue != null ) {
    try {
    	marginIndex = (new Integer(parmValue)).intValue();
    } 
    catch (Exception ex ) {
    }
  }

  //other necessaries
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  List<TradePartnerMarginRulesWebBean> marginRuleList = new ArrayList<TradePartnerMarginRulesWebBean>();
  marginRuleList.add(beanMgr.createBean(TradePartnerMarginRulesWebBean.class,"TradePartnerMarginRules") );
  marginRuleList.add(beanMgr.createBean(TradePartnerMarginRulesWebBean.class,"TradePartnerMarginRules") );
  marginRuleList.add(beanMgr.createBean(TradePartnerMarginRulesWebBean.class,"TradePartnerMarginRules") );
  marginRuleList.add(beanMgr.createBean(TradePartnerMarginRulesWebBean.class,"TradePartnerMarginRules") );

  //when included per ajax, the business objects will be blank
 // TradePartnerMarginRulesWebBean tradeMarginRow =beanMgr.createBean(TradePartnerMarginRulesWebBean.class,"TradePartnerMarginRules");
%>

<%@ include file="fragments/TradeAddMarginRows.frag" %>
