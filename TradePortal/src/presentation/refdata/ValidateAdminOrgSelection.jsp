<%--
*******************************************************************************
                             Validate Admin Org Selection

  Description:
     This page is an internal navigation page.  Based on the admin user org
  selected by the user on the AdminUserOrgSelection page, we validate
  that a selection was actually made.  If so, forward to the AdminUserDetail
  page with the encrypted oid (org and level) for the selected org.  Otherwise
  issue an error and return to the selection page.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.frame.*,
                 com.amsinc.ecsg.util.*,com.ams.tradeportal.common.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<%
   
DocumentHandler doc = formMgr.getFromDocCache();

   String orgAndLevel = request.getParameter("OrgAndLevel");

   String nextPage = "AdminUserDetail";

   // If no selection was made, issue and error and return to the selection page
   if (orgAndLevel.equals("")) {
      MediatorServices medService = new MediatorServices();
      medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
      medService.getErrorManager().issueError(
                                    TradePortalConstants.ERR_CAT_1,
                                    TradePortalConstants.SELECT_ADMIN_USER_ORG);
      medService.addErrorInfo();
      doc.setComponent("/Error", medService.getErrorDoc());
      formMgr.storeInDocCache("default.doc", doc);
      nextPage = "AdminUserOrgSelection";
   }

   formMgr.setCurrPage(nextPage);
   String physicalPage = NavigationManager.getNavMan().getPhysicalPage(nextPage, request);

%>
   <jsp:forward page='<%= physicalPage %>'>
      <jsp:param name="OrgAndLevel" value="<%=orgAndLevel%>" />
   </jsp:forward>
