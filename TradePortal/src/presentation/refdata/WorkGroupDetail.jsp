<%--
***************************************************************************************************
Work Groups

Description:
	Displays Threshold Group reference data.
	This page may be retrieved from several actions:
			Creating New Work Group from main Reference Data page
			Selecting a Work Group in the Work Group listview
			Errors resulting from Saving and Deleting the Work Group
		
Request Parameters:
	Oid - Oid of Work Group selected from listview
	/In/WorkGroup - WorkGroup node if returning from transaction
	

***************************************************************************************************
--%>

<%-- ****** Begin Page imports, bean imports declaration ******* --%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>


<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%-- ****** End Page imports, bean imports declaration  ******* --%>


<%--  ****** Begin data retrieval/display logic ****** --%>
<%

	
  DocumentHandler doc;
  DocumentHandler errorDoc = new DocumentHandler();
  WorkGroupWebBean workGroup;
  String workGroupOid = null;
  String buttonPressed="";
  boolean insertMode=false;
  boolean getFromDB=false;
  boolean getFromDoc=false;
	
  boolean isReadOnly=false;
  boolean displayErrors=false;
  
  boolean showCloseButton=true;
  boolean showDeleteButton=false;
  boolean showSaveButton=false;
  boolean showSaveClose=false;
   
  boolean nullValue=true;
  boolean debugMode=true;
  
  String loginLocale;
  String loginRights;
  
  String formattedAmount="";
  String baseCurrency;

  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();

  // retrieve user's corporate base currency

  baseCurrency = userSession.getBaseCurrencyCode();

  Debug.debug("Login locale is " + loginLocale);
  isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_WORK_GROUPS);
  
  // if user does not have access to view this page, return back to Reference
  // Home page
  if (!SecurityAccess.hasRights(loginRights, SecurityAccess.VIEW_OR_MAINTAIN_WORK_GROUPS))
  {
	formMgr.setCurrPage("RefDataHome");
      String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request); 
  %>
  	<jsp:forward page='<%= physicalPage %>' />
  <%
  }

  // register Work Group bean

  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.WorkGroupWebBean", "WorkGroup");
  workGroup = (WorkGroupWebBean) beanMgr.getBean("WorkGroup");
	
  doc = formMgr.getFromDocCache();
  buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
	Vector error = null;
	error = doc.getFragments("/Error/errorlist/error");

  if (doc.getDocumentNode("/In/WorkGroup") == null)
  {
	// This must come from the Reference Data page 
      if(request.getParameter("oid") != null)
       {
		workGroupOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());	
       }
      else
       {
		workGroupOid = null;
       }  

	if (workGroupOid == null )
	{
		insertMode=true;
	}
	else if (workGroupOid.equals("0"))
	{
		insertMode=true;
	}
	else
	{
		getFromDB = true;	
	}
  }
  else
  {// Logic has been changed to fix opt lock issue and duplicate error while update. TP Refresh - 12-05-2012
	// This must be returned from WorkGroup with errors
    // Check that maxerrorseverity is valid, redirect otherwise
		getFromDoc = true;
		getFromDB = false;
		errorDoc.addComponent("/Error",doc.getComponent("/Error"));
		String maxError = errorDoc.getAttribute("/Error/maxerrorseverity");
		Debug.debug("Error Fragment is: " + errorDoc.toString());
		
		// get oid form the doc
	    workGroupOid = doc.getAttribute("/Out/WorkGroup/work_group_oid");
		if (workGroupOid == null ) {
			workGroupOid = doc.getAttribute("/In/WorkGroup/work_group_oid");
		}
		
		// maxerrorseverity should be more then 3, otherwise redirect back to RefDataHome
		if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0)
		{
				// We've returned from a save/update/delete that was successful
		}else{
			  // We've returned from a save/update/delete but have errors.
		       // We will display data from the cache.  Determine if we're
		       // in insertMode by looking for a '0' oid in the input section
		       // of the doc.
			    displayErrors = true;
			    workGroupOid = doc.getAttribute("/In/WorkGroup/work_group_oid");
			    
			    if (workGroupOid == null)
			    {
				  insertMode=true;
			    }
			    else if (workGroupOid.equals("0"))
			    {
				  insertMode=true;
			    }else
				{
					getFromDB = true;	
					getFromDoc = false;
				}
		}
  }

  if (getFromDB)
  {
  	workGroup.getById(workGroupOid);	
  }

  if (getFromDoc)
  {
	workGroup.populateFromXmlDoc(doc.getComponent("/In"));
  }

  if (insertMode)
  {
	workGroupOid="0";
  }
  
  // determine which buttons to show
  if (!isReadOnly)
  {
	showSaveButton=true;
	showSaveClose=true;
	if (!insertMode)
		showDeleteButton=true;
  }
  
Debug.debug(" getFromDoc: " + getFromDoc + " getFromDB: " + getFromDB +
					" insertMode " + insertMode + 
					"workGroupOid: " + workGroupOid);
	
    
%>
<%--  ****** End data retrieval/display logic  ****** --%>

<%--  ****** Begin HTML  ****** --%>

<%
    // Auto save the form when time-out if not readonly.  
    // (Header.jsp will check for auto-save setting of the corporate org).
    String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

%>

<%-- Begin HTML header declarations (this includes the body tag) --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>

<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<%-- End HTML header declarations --%>

<%-- Begin inclusion of JavaScript functions, image rollovers, etc --%>
 
 <jsp:include page="/common/ButtonPrep.jsp" />

<%-- End JavaScript functions --%>

<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="RefDataHome.WorkGroups"/>
      <jsp:param name="helpUrl" value="/customer/work_group_form.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if ( insertMode) {
    subHeaderTitle = resMgr.getText("WorkGroupDetail.NewWorkGroup",TradePortalConstants.TEXT_BUNDLE);
  }
  else {
    subHeaderTitle = workGroup.getAttribute("work_group_name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>
		
<form name="WorkGroupDetail" id="WorkGroupDetail" data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>

<%

// build parameters for inclusion to the WorkGroupDetailForm
// which is used by the mediator, eg. security rights and locks

Hashtable addlParms = new Hashtable();
if (!insertMode)
	addlParms.put("work_group_oid", workGroupOid);
else
	addlParms.put("work_group_oid", "0");


addlParms.put("corp_org_oid", userSession.getOwnerOrgOid());
addlParms.put("login_oid", userSession.getUserOid());
addlParms.put("login_rights", userSession.getSecurityRights());
addlParms.put("opt_lock", workGroup.getAttribute("opt_lock"));
%>

<%= formMgr.getFormInstanceAsInputField("WorkGroupDetailForm", addlParms) %>

<%-- Begin Table with Threshold Groups navigation links and buttons --%>

  
  <%-- End Table with Threshold Groups navigation links and buttons --%>

  <%-- Begin Table with Work Group Name and Description --%>
 
  <div class="formArea">
  <jsp:include page="/common/ErrorSection.jsp" />

  <div class="formContent">
  <div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
	<div class="dijitTitlePaneContentInner">
 								
 								<%---------------------Start of WorkGroupName-----------------------%>
 								
 								
    	<%=widgetFactory.createTextField( "WorkGroupName", "WorkGroupDetail.WorkGroupName", workGroup.getAttribute("work_group_name"), "35", isReadOnly, true, false,"class='char30'","","")%>
    	
    							<%--------------------End Of WorkGroupName---------------------%>
    							
    							<%-------------Start of WorkGroupDecription----------------%>
    							
		<%=widgetFactory.createTextField( "WorkGroupDescription", "WorkGroupDetail.Description", workGroup.getAttribute("work_group_description"), "65", isReadOnly, true, false,"class='char45'","","")%>
		
								<%----------------End of WorkGroupDecription-------------------%>
								
								<br>
								<br>
								<br>
								<br>
								<br>
     </div>
     </div>
  </div><%--formContent--%>
  <%-- End Table with Work Group Name and Description --%>
  </div><%--formArea--%>

              <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'WorkGroupDetail'">
	
	                   <jsp:include page="/common/RefDataSidebar.jsp">	
	                             <jsp:param name="showSaveButton" value="<%= showSaveButton %>" />
	                             <jsp:param name="saveOnClick" value="none" />	
	                             <jsp:param name="showDeleteButton" value="<%= showDeleteButton %>" />	
	                             <jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
	                             <jsp:param name="saveCloseOnClick" value="none" />	
	                             <jsp:param name="cancelAction" value="goToRefDataHome" />	
	                             <jsp:param name="showHelpButton" value="false" />
	                             <jsp:param name="helpLink" value="customer/thresh_groups_detail.htm" />
	                             <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
           			     <jsp:param name="error" value="<%= error%>" />
           			     <jsp:param name="showTips" value="true" />	
           			     <jsp:param name="showTop" value="false" />
	                   </jsp:include>        
	
	          </div> <%--closes sidebar area--%>

 
</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>

<%
  //save behavior defaults to without hsm
  
  //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  
  // String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  // String saveCloseOnClick = "common.setButtonPressed('SaveCloseTrans','0'); document.forms[0].submit();";

 
%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />


<%
  String focusField = "WorkGroupName";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
</script>
<%
  }
%>

</body>

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("WorkGroup");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</html>

<%--  ****** End HTML  ****** --%>
