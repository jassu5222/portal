<%--
*******************************************************************************
                                  Admin User Detail - General Section

  Description:
     This page is used to maintain admin users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
<div class="columnLeft"> 
	<%= widgetFactory.createTextField( "UserId", "AdminUserDetail.UserID", user.getAttribute("user_identifier"), "30", isReadOnly, true, false,  "", "","" ) %>      

 
 	<%= widgetFactory.createTextField( "FirstName", "AdminUserDetail.FirstName", user.getAttribute("first_name"), "15", isReadOnly, true, false,  "", "","inline" ) %>     

	<%= widgetFactory.createTextField( "MiddleInitial", "AdminUserDetail.MiddleInitial", user.getAttribute("middle_initial"), "1", isReadOnly, false, false,  "", "","inline" ) %>                          

	<div style="clear: both"></div>
	<%= widgetFactory.createTextField( "LastName", "AdminUserDetail.LastName", user.getAttribute("last_name"), "30", isReadOnly, true, false,  "", "","" ) %>        

	<%
          options = Dropdown.createSortedRefDataOptions("LOCALE",
                                 user.getAttribute("locale"), loginLocale);
          if (insertMode) {
            defaultText = resMgr.getText("AdminUserDetail.selectRegion",
                                       TradePortalConstants.TEXT_BUNDLE);
          } else {
            defaultText = "";
          }
         
	%>
	<%=widgetFactory.createSelectField( "RegionSetting", "AdminUserDetail.RegionSetting",defaultText, options, isReadOnly, true, false, "", "", "")  %> 

<%// jgadela R 8.4 CR-854 T36000024797
if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)){
		String repLang = user.getAttribute("reporting_language");
		if(StringFunction.isBlank(repLang)){
			repLang = "en_CA";
		}
		
		options = Dropdown.createSortedRefDataOptions("REPORTING_LANGUAGE", repLang, loginLocale);
		if (insertMode) {
			defaultText = resMgr.getText("UserDetail.selectReportingLanguage",
		                  TradePortalConstants.TEXT_BUNDLE);
		} else {
			defaultText = "";
		}
	%>
	<%=widgetFactory.createSelectField("ReportingLanguage",
					"UserDetail.ReportingLanguage", " ", options, isReadOnly,
					true, false, "class='char25' onChange='onChangeRepLang()'", "", "")%>
	
	<% // jgadela Rel 8.4 CR-854 [END]- Adding reporting language option%>
	<%}%>
</div>


<div class="columnRight"> 
<%= widgetFactory.createTextField( "PhoneNumber", "AdminUserDetail.PhoneNumber", user.getAttribute("telephone_num"), "20", isReadOnly, false, false,  "", "","" ) %>      

                    
<%= widgetFactory.createTextField( "FaxNumber", "AdminUserDetail.FaxNumber", user.getAttribute("fax_num"), "20", isReadOnly, false, false,  "", "","" ) %>                
<div style="clear: both"></div>
          
<%= widgetFactory.createTextField( "EmailAddress", "AdminUserDetail.EmailAddress", user.getAttribute("email_addr"), "50", isReadOnly, false, false,  "class='char32'", "","" ) %>       
            
<%
        options = Dropdown.createSortedRefDataOptions("DATEPATTERN",
                                user.getAttribute("datepattern"),
                                loginLocale);       
	%>
	<%if (!isReadOnly) {%>	
	<%=widgetFactory.createSelectField("DatePattern",
					"UserDetail.DatePattern", " ", options, isReadOnly,
					true, false, "", "", "")%>
    <%}%>              
</div>