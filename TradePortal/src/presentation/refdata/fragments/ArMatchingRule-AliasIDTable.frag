<%--
**********************************************************************************
                              ArMatchingRule Detail

  Description:
     This page is used to maintain individual corporate customer organizations. It
     supports insert, update, and delete.  Errors found during a save are
     redisplayed on the same page. Successful updates return the user to the
     Organizations Home page.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2012
 *	   @ Developer Komal     
 *     All rights reserved
--%>
<div>
<table id="aliasIDTable" class="formDocumentsTable">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<%---<th><%=widgetFactory.createSubLabel("ArMatchingRuleDetail.AliasId")%></th>---%>
			<th class="genericCol"><b><%=resMgr.getText("ArMatchingRuleDetail.AliasId", TradePortalConstants.TEXT_BUNDLE)%></b></th>
			<th>&nbsp;</th>
			<%---<th><%=widgetFactory.createSubLabel("ArMatchingRuleDetail.AliasId")%></th>---%>
			<th class="genericCol"><b><%=resMgr.getText("ArMatchingRuleDetail.AliasId", TradePortalConstants.TEXT_BUNDLE)%></b></th>
		</tr>
	   </thead>
	<tbody>	
			<%
			iLoop = 0;
			while(iLoop < numItemsID){%>
			<%@ include file="ArMatchingRule-AliasIDTermRows.frag"%>
			<%iLoop = iLoop+2; 		
				}			
			%>
	</tbody>
</table>
<br/>
	<% if (!(isReadOnly)) {		%>
			<button data-dojo-type="dijit.form.Button" type="submit" id="add4MoreAliasIDs">
				<%=resMgr.getText("common.Add4MoreText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					add4MoreAliasID();
				</script>
			</button>
			<%=widgetFactory.createHoverHelp("add4MoreAliasIDs","POStructure.Add4More") %>
		<% } else {		%>
					&nbsp;
		<% }		%>

 <input type=hidden value='<%=numItemsID%>' name="numItemsID" id="numItemsOfID">  
	</div>			
<script language="JavaScript">  
	
	function add4MoreAliasID() {
		
		require([ "dojo/_base/array", "dojo/dom", "dojo/dom-construct",
					"dojo/dom-class", "dojo/dom-style", "dojo/query",
					"dijit/registry", "dojo/on", "t360/common", "t360/dialog",
					"dojo/ready", "dojo/NodeList-traverse","dojo/domReady!" ], function(arrayUtil,
					dom, domConstruct, domClass, domStyle, query, registry, on,
					common, dialog, ready) {
		var table = dom.byId('aliasIDTable');
		var insertRowIdx = table.rows.length;
		var lastElement = parseInt(insertRowIdx)-1;
		var iLoop = eval((lastElement * 2)); 
		<%--  var iLoop = document.getElementById("aliasNameCount").value;
		iLoop= eval(parseInt(iLoop)+1);  --%>
		for (i=0;i<2;i++) {
			<%-- var row = table.insertRow(insertRowIdx); --%>
			<%-- row.id = 'AliasName'+insertRowIdx ; --%>
			
			common.appendAjaxInvTableRows(table,"aliasIDIndex"+iLoop,"/portal/refdata/fragments/ArMatchingRule-AliasIDTermRows.jsp?iLoop="+iLoop);  
			
			insertRowIdx = insertRowIdx+1;
			 iLoop = (parseInt(iLoop))+2; 
			
		}	 
		});
	}
</script>
