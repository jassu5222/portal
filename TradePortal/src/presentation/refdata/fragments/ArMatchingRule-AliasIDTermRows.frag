<%--
**********************************************************************************
                              ArMatchingRule Detail

  Description:
     This page is used to maintain individual corporate customer organizations. It
     supports insert, update, and delete.  Errors found during a save are
     redisplayed on the same page. Successful updates return the user to the
     Organizations Home page.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2012
 *	   @ Developer Komal     
 *     All rights reserved
--%>
<tr id="aliasIDIndex<%=iLoop%>">							
	<% for( int iColumnCount = 0; iColumnCount < 2; iColumnCount++){ %>
		<td><%=(iLoop + iColumnCount +1) + "."%></td>
		<td>
		<% //jgadela 09/22/2014 -  R91 IR T36000032850 - Fixed null object issue. 
		    String val = null;
		 	String enVal = null;
		 	if(arbuyeridalias[iLoop+iColumnCount]!=null) {
		 		val = arbuyeridalias[iLoop+iColumnCount].getAttribute("buyer_id_alias");
		 		if(arbuyeridalias[iLoop+iColumnCount].getAttribute("ar_buyer_id_alias_oid")!=null) 
		 			enVal = EncryptDecrypt.encryptStringUsingTripleDes(arbuyeridalias[iLoop+iColumnCount].getAttribute("ar_buyer_id_alias_oid"), userSession.getSecretKey());
		 	}
		%>
		
			<%=widgetFactory.createTextField("ArMatchingRuleIdAlias" +(iLoop+iColumnCount),"", val ,"35", isReadOnly, false, false,"style='width:300px'", "", "none")%>
			<%out.print("<INPUT TYPE=HIDDEN NAME='BuyerIdAliasoid" + (iLoop+iColumnCount)+ "' VALUE='"+enVal+"'>");%> 			
		 </td>
	<% } %>			
</tr>
