<%--this fragment is included in BankGroupDetail.jsp for adding Payable Email management section. 
as part of CR-913 (Payable Invoices)--%>
<%
   dropdownOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.INSTRUMENT_LANGUAGE,
                                             bankGroup.getAttribute("payable_email_language"), userLocale);
%>
 <%=widgetFactory.createSectionHeader("PayableEmailDetails", "BankGroups.PayableEmailDetails") %>
    <%=widgetFactory.createSelectField("payableEmailLanguage","BankGroups.EmailLanguage"," ", dropdownOptions, isReadOnly,false, false,"class='char10'", "", "")%>
    <%=widgetFactory.createTextField("payableSystemName","BankGroups.PayableSystemName",bankGroup.getAttribute("payable_system_name"),"35",isReadOnly,false,false,"class='char30'","","" )%>
    <%=widgetFactory.createTextField("payableSenderName","BankGroups.SenderName",bankGroup.getAttribute("payable_sender_name"),"35",isReadOnly,false,false,"class='char30'","","" )%>
    <%=widgetFactory.createTextField("payableSenderEmailAddress","BankGroups.SenderEmailAddress",bankGroup.getAttribute("payable_sender_email_address"),"35",isReadOnly,false,false,"class='char30'","","" )%>
    <%=widgetFactory.createTextField("payableBankUrl","BankGroups.BankURL",bankGroup.getAttribute("payable_bank_url"),"65",isReadOnly,false,false,"class='char30'","","" )%>
    <div style="clear:both">&nbsp;</div>
    <div style="padding-left:14px;">
       <%=widgetFactory.createCheckboxField("payableDoNotReplyInd","",TradePortalConstants.INDICATOR_YES.equals(bankGroup.getAttribute("payable_do_not_reply_ind")),isReadOnly,false,"","","none")%>
       <%=resMgr.getText("BankGroups.DoNotReplyPayables",TradePortalConstants.TEXT_BUNDLE)%>    
    </div>
    <div style="clear:both">&nbsp;</div>
    <div style="padding-left:14px;">
       <%=widgetFactory.createCheckboxField("incldBnkURLPayableMgmt","",TradePortalConstants.INDICATOR_YES.equals(bankGroup.getAttribute("incld_bnk_url_payable_mgmt")),isReadOnly,false,"","","none")%>
       <%=resMgr.getText("BankGroups.IncldBnkURLPayableMngmt",TradePortalConstants.TEXT_BUNDLE)%>    
    </div>
 </div>
