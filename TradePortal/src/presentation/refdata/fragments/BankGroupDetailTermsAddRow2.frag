<%--
*******************************************************************************
      BankGroupDetail Page
  
Description:   This jsp simply displays the html for the general portion of 
               the BankGroupDetail.jsp.  This page is called from 
               BankGroupDetail.jsp  and is called by:

	       <%@ include file="BankGroupDetail.jsp" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%> 


	<tr>
		
		<td>
			
			<%=widgetFactory.createTextField("report2Code" + bankGroupDetailIndex2,"",bankPmtRptCode.getAttribute("code"),"3",isReadOnly,false,false,"onblur=\"this.value=this.value.toUpperCase()\"class='char8'","" ,"none")%>
		
		</td>
		
		<td>
		 <%=widgetFactory.createTextField("report2ShortDescription" + bankGroupDetailIndex2,"",  bankPmtRptCode.getAttribute("short_description"),"15",isReadOnly,false,false,"class='char35'","" ,"none")%>
		</td>
		
		<td>
		<%=widgetFactory.createTextField("report2Description" + bankGroupDetailIndex2,"",bankPmtRptCode.getAttribute("description"),"30",isReadOnly,false,false,"class='char35'","" ,"none")%>
		</td>
	</tr>
