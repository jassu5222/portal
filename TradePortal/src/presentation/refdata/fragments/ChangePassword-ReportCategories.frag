<%--
*******************************************************************************
                                    User Detail - Template Groups Section

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
<% 
  int select = 0; 
  int counter = 1;	
  defaultText = " ";

  sqlQuery = new StringBuffer();
  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
  sqlQuery.append("select report_categories_ind from client_bank");
  sqlQuery.append(" where organization_oid =? " );
  Debug.debug(sqlQuery.toString());
  queryListView.setSQL(sqlQuery.toString(),new Object[]{ userSession.getClientBankOid()});
  queryListView.getRecords();
  String reportCategInd = queryListView.getRecordValue("report_categories_ind");
  if ("Y".equals(reportCategInd)) {
%>
  <table class="formDocumentsTable" style="width:50%">
    <thead>
      <tr>
        <th>&nbsp;</th>
        <%--cquinton 11/16/2012 ir#6864 left align headers--%>
        <th class="headingLeft"><%=widgetFactory.createSubLabel("ChangePassword.ReportCategoriesTitle")%></th>
      </tr>
    </thead>
    <tbody>
<%
    dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
      userDetailReptCateg1Oid, userSession.getSecretKey());
    select = dropdownOptions.indexOf("selected");
    if (select > -1) { 
%>
      <tr>
        <td><%=counter%>.</td>
        <td>
          <%=widgetFactory.createSelectField("ReportingCategory1", "", defaultText,
               dropdownOptions, isReadOnly, false, false, "", "", "")%> 
        </td>
      </tr>

<% 
      counter++;
    } 

    dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
      userDetailReptCateg2Oid, userSession.getSecretKey()); 
    select = dropdownOptions.indexOf("selected");
    if (select > -1) {
%>
      <tr>
        <td><%=counter%>.</td>
        <td>
          <%=widgetFactory.createSelectField("ReportingCategory2", "", defaultText,
            dropdownOptions, isReadOnly, false, false, "", "", "")%>
        </td>
      </tr>
<% 
      counter++;
    } 

    dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
      userDetailReptCateg3Oid, userSession.getSecretKey()); 
    select = dropdownOptions.indexOf("selected");
    if (select > -1) {	
%>
		<tr>
			<td><%=counter%>.</td>
			<td>			
        	<%=widgetFactory.createSelectField("ReportingCategory3", "", defaultText,
							dropdownOptions, isReadOnly, false, false, "", "", "")%> 
			</td>
		</tr>
		<% counter++;
			} %>
		<% dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   										userDetailReptCateg4Oid, userSession.getSecretKey()); 
		select = dropdownOptions.indexOf("selected");
		if (select > -1) {	 %>
		<tr>
			<td><%=counter%>.</td>
			<td>			
			<%=widgetFactory.createSelectField("ReportingCategory4", "", defaultText,
							dropdownOptions, isReadOnly, false, false, "", "", "")%>         		   										
			</td>
		</tr>
		<% counter++;
			} %>
		<%  dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   									   userDetailReptCateg5Oid, userSession.getSecretKey());	
		select = dropdownOptions.indexOf("selected");
		if (select > -1) {	%>
		<tr>
			<td><%=counter%>.</td>
			<td>
        	<%=widgetFactory.createSelectField("ReportingCategory5", "", defaultText,
							dropdownOptions, isReadOnly, false, false, "", "", "")%> 
			</td>
		</tr>
		<% counter++;
			} %>
		<% dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   										userDetailReptCateg6Oid, userSession.getSecretKey());  
		select = dropdownOptions.indexOf("selected");
		if (select > -1) {	%>
		<tr>
			<td><%=counter%>.</td>
			<td>
			<%=widgetFactory.createSelectField("ReportingCategory6", "", defaultText,
							dropdownOptions, isReadOnly, false, false, "", "", "")%>         		   										
			</td>
		</tr>
		<% counter++;
			} %>
		<% dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   									   userDetailReptCateg7Oid, userSession.getSecretKey());	
		select = dropdownOptions.indexOf("selected");
		if (select > -1) {	%>
		<tr>
			<td><%=counter%>.</td>
			<td>
        	<%=widgetFactory.createSelectField("ReportingCategory7", "", defaultText,
							dropdownOptions, isReadOnly, false, false, "", "", "")%> 
			</td>
		</tr>
		<% counter++;
			} %>
		<% dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   										userDetailReptCateg8Oid, userSession.getSecretKey());  
		select = dropdownOptions.indexOf("selected");
		if (select > -1) {	%>
		<tr>
			<td><%=counter%>.</td>
			<td>
			<%=widgetFactory.createSelectField("ReportingCategory8", "", defaultText,
							dropdownOptions, isReadOnly, false, false, "", "", "")%>         		   										
			</td>
		</tr>
		<% counter++;
			} %>
		<% dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   									   userDetailReptCateg9Oid, userSession.getSecretKey());	
		select = dropdownOptions.indexOf("selected");
		if (select > -1) {	%>
		<tr>
			<td><%=counter%>.</td>
			<td>
        	<%=widgetFactory.createSelectField("ReportingCategory9", "", defaultText,
							dropdownOptions, isReadOnly, false, false, "", "", "")%>
			</td>							 
		</tr>
		<% counter++;
			} %>
		<% dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   										userDetailReptCateg10Oid, userSession.getSecretKey());  
		select = dropdownOptions.indexOf("selected");
		if (select > -1) {	%>
		<tr>
			<td><%=counter%>.</td>
			<td>
			<%=widgetFactory.createSelectField("ReportingCategory10", "", defaultText,
							dropdownOptions, isReadOnly, false, false, "", "", "")%>         		   										
			</td>
		</tr>
		<% } %>
<%
    //cquinton 11/15/2012 don't display empty table
    //fill out to 4 lines if needed
    while ( counter < 5 ) {
%>
      <tr>
        <td><%=counter%>.</td>
        <td>&nbsp;</td>
      </tr>
<%
      counter++;
    }
%>
    </tbody>
  </table>	
<%
  } 
%>
