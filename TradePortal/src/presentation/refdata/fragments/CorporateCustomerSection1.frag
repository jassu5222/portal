<%-- this fragment is included in CorporateCustomerDetail.jsp --%>

  <%=widgetFactory.createWideSubsectionHeader("CorpCust.Name") %>
  <div class="columnLeft">
    <%= widgetFactory.createTextField( "Name", "CorpCust.CorpCustName", corporateOrg.getAttribute("name"), "35", isReadOnly,true,false,"","","inline") %>
  </div>
  <div class="columnRight">
    <%= widgetFactory.createTextField( "AlternateName", "CorpCust.AltrCorpCustName", corporateOrg.getAttribute("alternate_name"), "34", isReadOnly) %>
    <div style="clear: both;"></div>
  </div>
  <div style="clear: both;"></div>

  <%=widgetFactory.createWideSubsectionHeader("CorpCust.Address") %>

<%
  gridHtml = dgFactory.createDataGrid("CorporateAddressDataGridId","CorporateAddressDataGrid", null);
%>

  <div class="columnLeft">
    <%--SPenke T36000006187 10/11/2012 Begin --%>
   
    <%= widgetFactory.createTextField( "AddressLine1", "CorpCust.AddressLine1", corporateOrg.getAttribute("address_line_1"), "35", isReadOnly,true) %>
    <%= widgetFactory.createTextField( "AddressLine2", "CorpCust.AddressLine2", corporateOrg.getAttribute("address_line_2"), "35", isReadOnly) %>
                 
    <br><br><br><br>
    <%--SPenke T36000006187 10/11/2012 End --%>             
    <div style="clear: both;"></div>
  </div>

  <div class="columnRight">
    <%= widgetFactory.createTextField( "City", "CorpCust.City", corporateOrg.getAttribute("address_city"), "23", isReadOnly,true) %>

    <%= widgetFactory.createTextField( "StateProvince", "CorpCust.ProvinceState", corporateOrg.getAttribute("address_state_province"), "8", 
          isReadOnly, false, false, "", "", "inline") %>
    <%= widgetFactory.createTextField( "PostalCode", "CorpCust.PostalCode", corporateOrg.getAttribute("address_postal_code"), "8", 
          isReadOnly, false, false, "", "", "inline") %>
    <div style="clear:both;"></div>

<%
  String countryWidgetProps = "placeHolder:'" +
    resMgr.getText("CorpCust.SelectACountry", TradePortalConstants.TEXT_BUNDLE) + "'";
  //cquinton 12/14/2012 ir#8895 use the widget 'value' to set default as its more accurate
  // than relying on options.
  if (!InstrumentServices.isBlank(corporateOrgCountry)) {
    countryWidgetProps += ", value:'" + corporateOrgCountry + "'";
  }
  else {
    countryWidgetProps += ", value:''"; //set initial value to blank rather than the first select item
  }
  dropdownOptions = Dropdown.createSortedRefDataOptions("COUNTRY", corporateOrgCountry, userLocale);
  out.print(widgetFactory.createSelectField( "Country", "CorpCust.Country", "", dropdownOptions, isReadOnly,true,false,"",countryWidgetProps,""));
%>
  </div>
  <div style="clear: both;"></div>      
        <% if ( isUserDefinedFieldEnable ) { %>
            <%=widgetFactory.createWideSubsectionHeader("CorpCust.PortalOnlyAdditionalFields")%>
            <div class="columnLeft">
                <%= widgetFactory.createTextField( "userDefinedField1", "CorpCust.UserDefinedField1", corporateOrg.getAttribute("user_defined_field_1"), "20", isReadOnly, false, false, "class='char20'","","")%> 
                <%= widgetFactory.createTextField( "userDefinedField2", "CorpCust.UserDefinedField2", corporateOrg.getAttribute("user_defined_field_2"), "20", isReadOnly, false, false, "class='char20'","","")%>   
            </div>           
             <div class="columnRight">
                <%= widgetFactory.createTextField( "userDefinedField3", "CorpCust.UserDefinedField3", corporateOrg.getAttribute("user_defined_field_3"), "20", isReadOnly, false, false, "class='char20'","","")%>  
                <%= widgetFactory.createTextField( "userDefinedField4", "CorpCust.UserDefinedField4", corporateOrg.getAttribute("user_defined_field_4"), "20", isReadOnly, false, false, "class='char20'","","")%>  
             </div>           
             <div style="clear: both"></div>
         <% } %>

<%
  if (!isReadOnly) {
%>
  <div class="gridHeader">
    <span class="gridHeader-right">
      <button data-dojo-type="dijit.form.Button" type="button" id="AddAddressButton">
        <%=resMgr.getText("CorpCust.AddAddress",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">local.addSecAddressPop();</script>
      </button>
      <%=widgetFactory.createHoverHelp("AddAddressButton","CorpCust.AddAddress") %>
    </span>
  </div>
<%
  }
%>

  <%=gridHtml %>

  <br/>

  <div id="addressHiddenFields">
<%
  //cquinton 11/1/2012 create hidden input fields for each secondary address
  // we do this directly from xml or db bypassing web beans,
  // becuase the web beans won't carry through invalid values in some cases.
  //becuase we are already looping we also build the javascript for populating the data grid
  int addressCount = 0; //this helps later with indexing
  StringBuffer addressGridData = new StringBuffer();

  if (getDataFromXmlDocFlag) {
    Vector<DocumentHandler> addressDocList = xmlDoc.getFragments("/In/CorporateOrganization/AddressList");
    addressCount = addressDocList.size();
    for ( int addressIdx = 0; addressIdx<addressCount; addressIdx++ ) {
      DocumentHandler addressDoc = (DocumentHandler) addressDocList.get(addressIdx);
%>
    <div id="addressHidden<%=addressIdx%>">
      <input id="SecAddressOid<%=addressIdx%>" name="SecAddressOid<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/address_oid")%>' >
      <input id="SecAddressSeqNum<%=addressIdx%>" name="SecAddressSeqNum<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/address_seq_num")%>' >
      <input id="SecName<%=addressIdx%>" name="SecName<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/name"))%>' >
      <input id="SecAddressLine1<%=addressIdx%>" name="SecAddressLine1<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/address_line_1"))%>' >
      <input id="SecAddressLine2<%=addressIdx%>" name="SecAddressLine2<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/address_line_2"))%>' >
      <input id="SecCity<%=addressIdx%>" name="SecCity<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/city"))%>' >
      <input id="SecStateProvince<%=addressIdx%>" name="SecStateProvince<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/state_province"))%>' >
      <input id="SecPostalCode<%=addressIdx%>" name="SecPostalCode<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/postal_code"))%>' >
      <input id="SecCountry<%=addressIdx%>" name="SecCountry<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/country")%>' >
      <input id="SecOwnershipLevel<%=addressIdx%>" name="SecOwnershipLevel<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/ownership_level")%>' >
      <input id="SecUserDefinedField1<%=addressIdx%>" name="SecUserDefinedField1<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/user_defined_field_1")%>' >
      <input id="SecUserDefinedField2<%=addressIdx%>" name="SecUserDefinedField2<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/user_defined_field_2")%>' >
      <input id="SecUserDefinedField3<%=addressIdx%>" name="SecUserDefinedField3<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/user_defined_field_3")%>' >
      <input id="SecUserDefinedField4<%=addressIdx%>" name="SecUserDefinedField4<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/user_defined_field_4")%>' >
    </div>
<%
      addressGridData.append("{ ");
  
      addressGridData.append("rowKey:'").append(addressDoc.getAttribute("/address_oid")).append("'");
      addressGridData.append(", ");
      addressGridData.append("divId:'addressHidden").append(addressIdx).append("'");
      addressGridData.append(", ");
      addressGridData.append("SeqNo:'").append(addressDoc.getAttribute("/address_seq_num")).append("'");
      addressGridData.append(", ");
      addressGridData.append("Name:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/name"))).append("'");
      addressGridData.append(", ");
      addressGridData.append("AddressLine1:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/address_line_1"))).append("'");
      addressGridData.append(", ");
      addressGridData.append("AddressLine2:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/address_line_2"))).append("'");
      addressGridData.append(", ");
      addressGridData.append("City:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/city"))).append("'");
      addressGridData.append(", ");
      addressGridData.append("StateProvince:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/state_province"))).append("'");
      addressGridData.append(", ");
      addressGridData.append("PostalCode:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/postal_code"))).append("'");
      addressGridData.append(", ");
      addressGridData.append("Country:'").append(addressDoc.getAttribute("/country")).append("'");
      addressGridData.append(", ");
      addressGridData.append("OwnershipLevel:'").append(addressDoc.getAttribute("/ownership_level")).append("'");
  
      addressGridData.append(" }");
  
      if ( addressIdx < addressCount-1) {
        addressGridData.append(", ");
      }
    }
  }
  else if (retrieveFromDBFlag) {
    try {
    
	//jgadela R90 IR T36000026319 - SQL INJECTION FIX
      String addressSql = "select address_oid, address_seq_num, name, address_line_1, address_line_2, city, state_province, postal_code, country, ownership_level, "
                          + " user_defined_field_1, user_defined_field_2, user_defined_field_3, user_defined_field_4 "
       					  + " from address  where p_corp_org_oid = ?  order by address_seq_num ";

      DocumentHandler addressListDoc = DatabaseQueryBean.getXmlResultSet(addressSql.toString(), true, new Object[]{corporateOrgOid});
      if ( addressListDoc != null ) {
        Vector addressDocList = addressListDoc.getFragments("/ResultSetRecord");
        addressCount = addressDocList.size();
        for ( int addressIdx = 0; addressIdx<addressCount; addressIdx++ ) {
          DocumentHandler addressDoc = (DocumentHandler) addressDocList.get(addressIdx);
%>
    <div id="addressHidden<%=addressIdx%>">
      <input id="SecAddressOid<%=addressIdx%>" name="SecAddressOid<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/ADDRESS_OID")%>' >
      <input id="SecAddressSeqNum<%=addressIdx%>" name="SecAddressSeqNum<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/ADDRESS_SEQ_NUM")%>' >
      <input id="SecName<%=addressIdx%>" name="SecName<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/NAME"))%>' >
      <input id="SecAddressLine1<%=addressIdx%>" name="SecAddressLine1<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/ADDRESS_LINE_1"))%>' >
      <input id="SecAddressLine2<%=addressIdx%>" name="SecAddressLine2<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/ADDRESS_LINE_2"))%>' >
      <input id="SecCity<%=addressIdx%>" name="SecCity<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/CITY"))%>' >
      <input id="SecStateProvince<%=addressIdx%>" name="SecStateProvince<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/STATE_PROVINCE"))%>' >
      <input id="SecPostalCode<%=addressIdx%>" name="SecPostalCode<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/POSTAL_CODE"))%>' >
      <input id="SecCountry<%=addressIdx%>" name="SecCountry<%=addressIdx%>" 
             type="hidden" value='<%=StringFunction.xssCharsToHtml(addressDoc.getAttribute("/COUNTRY"))%>' >
      <input id="SecOwnershipLevel<%=addressIdx%>" name="SecOwnershipLevel<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/OWNERSHIP_LEVEL")%>' >
      <input id="SecUserDefinedField1<%=addressIdx%>" name="SecUserDefinedField1<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/USER_DEFINED_FIELD_1")%>' >
      <input id="SecUserDefinedField2<%=addressIdx%>" name="SecUserDefinedField2<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/USER_DEFINED_FIELD_2")%>' >
      <input id="SecUserDefinedField3<%=addressIdx%>" name="SecUserDefinedField3<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/USER_DEFINED_FIELD_3")%>' >
      <input id="SecUserDefinedField4<%=addressIdx%>" name="SecUserDefinedField4<%=addressIdx%>" 
             type="hidden" value='<%=addressDoc.getAttribute("/USER_DEFINED_FIELD_4")%>' >
    </div>
<%
          addressGridData.append("{ ");
      
          addressGridData.append("rowKey:'").append(addressDoc.getAttribute("/ADDRESS_OID")).append("'");
          addressGridData.append(", ");
          addressGridData.append("divId:'addressHidden").append(addressIdx).append("'");
          addressGridData.append(", ");
          addressGridData.append("SeqNo:'").append(addressDoc.getAttribute("/ADDRESS_SEQ_NUM")).append("'");
          addressGridData.append(", ");
          addressGridData.append("Name:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/NAME"))).append("'");
          addressGridData.append(", ");
          addressGridData.append("AddressLine1:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/ADDRESS_LINE_1"))).append("'");
          addressGridData.append(", ");
          addressGridData.append("AddressLine2:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/ADDRESS_LINE_2"))).append("'");
          addressGridData.append(", ");
          addressGridData.append("City:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/CITY"))).append("'");
          addressGridData.append(", ");
          addressGridData.append("StateProvince:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/STATE_PROVINCE"))).append("'");
          addressGridData.append(", ");
          addressGridData.append("PostalCode:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/POSTAL_CODE"))).append("'");
          addressGridData.append(", ");
          addressGridData.append("Country:'").append(StringFunction.xssCharsToHtml(addressDoc.getAttribute("/COUNTRY"))).append("'");
          addressGridData.append(", ");
          addressGridData.append("OwnershipLevel:'").append(addressDoc.getAttribute("/OWNERSHIP_LEVEL")).append("'");
      
          addressGridData.append(" }");
      
          if ( addressIdx < addressCount-1) {
            addressGridData.append(", ");
          }
        }
      }

    } catch (AmsException e) {
      //todo: should throw an error
      Debug.debug("queryListView threw an exception");
      e.printStackTrace();
    }
  }
%>
  </div>
<input type="hidden" value="<%=addressCount%>" name="numberOfMultipleObjects4" id="noOfAddrRows">
<%
  if (!isReadOnly) {
    //note the update address button is only displayed when there is at least one address
%>
  <div id="addressGridActions" class="formActions" <%=(addressCount<=0)?"style='display:none;'":""%> >
    <span class="formActions-right">
      <button data-dojo-type="dijit.form.Button" type="button" id="UpdateAddressButton" >
        <%=resMgr.getText("CorpCust.UpdateAddress",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">local.updateSecAddressPop();</script>
      </button>
      <%=widgetFactory.createHoverHelp("UpdateAddressButton","CorpCust.UpdateAddress") %>
    </span>
    <div style="clear:both;"></div>
  </div>
<%
  }
%>
