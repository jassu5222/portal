<%--this fragment is included in CorporateCustomerDetail.jsp --%>

  <%--cquinton 11/21/2012 move creation of hidden elements here to get accurate account size--%>
  <div id="acctFormHiddenElements">
<% 
 //jgadela CR501 IR# T36000018065 - defined separate read only flag for account section, so that can review account details in the Approver view.
  boolean isReadOnlyAcc = isReadOnly ;

  int accountCount = 0;
  if(accountVector != null){
    accountCount = accountVector.size();
    String hiddenAccElements = createHiddenAccountElements(accountVector, beanMgr, userSession); 
    out.println(hiddenAccElements);
  }
%>
  </div>

<%
  if (!isReadOnly) {
%>
  <div class="gridHeader">
    <span class="gridHeader-right">
      <%--always start in add mode with the add button non-visible--%>
      <button data-dojo-type="dijit.form.Button" id="addAcc" style='display:none;' 
              type="button" onClick="local.prepareForNewAccount();">
        <%=resMgr.getText("CorpCust.AddAcc",TradePortalConstants.TEXT_BUNDLE)%> 
      </button>
      <%=widgetFactory.createHoverHelp("addAcc","CorpCust.AddAccount") %>
<%
    //only display the button if user has ability to work with  other accounts
    if(InstrumentServices.isBlank(otherAccountsHidden)) {
%>
      <button data-dojo-type="dijit.form.Button" id="addSubsidiaryAcc"
 	      type="button" onClick="local.addSubsidiaryAccount()">
        <%=resMgr.getText("CorpCust.AddSubsidiaryAcc",TradePortalConstants.TEXT_BUNDLE)%>
      </button>
      <%=widgetFactory.createHoverHelp("addSubsidiaryAcc","CorpCust.AddSubsidiaryAccount") %>
<%
    }
%>
    </span>
  </div>


<%
  }
%>
  <div style="clear: both;"></div>



<%
  String acctGridHtml = dgFactory.createDataGrid("acctGrid","CorporateCustAccountSetupDataGrid",null);
%>
  <%= acctGridHtml %>
  <br>
  <br>



  <div class="columnLeft">
    <%--we have to keep track of this so xmldoc is set correctly--%>
    <input type="hidden" value="<%=accountCount%>" name="numberOfMultipleObjects" id="noOfRows">

    <input type="hidden" value="<%=aliasRowCount%>" name="numberOfAliasObjects" id="noOfAliasRows"> 

<%
  defaultCheckBoxValue = false;
  String otherAccountDisabled = "";

  if(InstrumentServices.isBlank(otherAccountsHidden)){
                        
    //out.print(InputField.createCheckboxField("otherCorpsAccount"+ iLoop, TradePortalConstants.INDICATOR_YES, "",defaultCheckBoxValue, "ListText", "ListText", true,true, ""));
    out.println("<input type=hidden name='ValOtherCorpsAccount' id = 'ValOtherCorpsAccount' VALUE =''>");
    out.println("<input type=hidden name='ValOtherAccountOwnerOid' id = 'ValOtherAccountOwnerOid' VALUE =''>");
                        
    //RKazi, IAZ 06/20/2011 HRUL061749069 Adding this hidden field as it is needed for validation. - Start
  }
  else{
    //Maheswar IR 3252 8/4/2011 End
                       
    out.print("<input type=hidden name='otherCorpsAccount' id='otherCorpsAccount' VALUE =''>");
    out.print("<input type=hidden name='ValOtherCorpsAccount' id='ValOtherCorpsAccount' VALUE =''>");
    out.print("<input type=hidden name='otherAccountOwnerOid' id='otherAccountOwnerOid' VALUE =''>");
    out.print("<input type=hidden name='ValOtherAccountOwnerOid' id='ValOtherAccountOwnerOid' VALUE =''>");

    //  String fxRateGroupHd = account.getAttribute("fx_rate_group");
    //if (InstrumentServices.isBlank(fxRateGroupHd))
    //    fxRateGroupHd = corporateOrg.getAttribute("fx_rate_group");
    //out.print("<INPUT TYPE=hidden NAME='fxRateGroup' VALUE='" + fxRateGroupHd + "'>");
    //out.print("<INPUT TYPE=hidden NAME='fxRateGroupF' VALUE='" + fxRateGroupHd + "'>");
    out.print("<INPUT TYPE=hidden NAME='fxRateGroup' id='fxRateGroup' VALUE=''>");
    out.print("<INPUT TYPE=hidden NAME='fxRateGroupFEdit' id='fxRateGroupF' VALUE=''>");
  }

  String disabledFlag = "";
  if(isReadOnlyAcc == true){
	  disabledFlag =  "DISABLED";
  }
  //SSikhakolli  - Rel-8.3 ANZ IR_Issue#34 IR#T36000021611 on 10/15/2013 - End
%> 

<%= widgetFactory.createTextField( "acctNumEdit", "CorpCust.CustomerAccountNumber", "", "30", false, false, false,  " class=\"char30\" "+disabledFlag, "","" ) %>
<%= widgetFactory.createTextField( "acctNameEdit", "CorpCust.CustomerAccountName", "", "35", false, false, false,  " class=\"char30\" "+disabledFlag, "","" ) %>
<% options = Dropdown.createSortedCurrencyCodeOptions("", loginLocale); %>
<% out.print(widgetFactory.createSelectField("acctCurrEdit", "CorpCust.CustomerAccountCurrency"," ", options,  false, false, false, " class=\"char11\" "+disabledFlag, "", "inline"));%>
<%= widgetFactory.createTextField( "acctTypeEdit", "CorpCust.CustomerAccountType", "", "3", false, false, false,  " class=\"char11\" "+disabledFlag, "","inline" ) %>

    <div style="clear: both;"></div>

<% 
   options = Dropdown.createSortedBankCountryCodeOptions("", loginLocale);
   out.print(widgetFactory.createSelectField("bankCountryCodeEdit", "CorpCust.BankCountryCode","", options,  false, false, false, " class=\"char11\" "+disabledFlag, "", "inline"));
%>
<%= widgetFactory.createTextField( "sourceSystemBranchEdit", "CorpCust.SourceSystemOrBranch", "", "", false, false, false,  " class=\"char11\" "+disabledFlag, "maxLength:'6'","inline" ) %>

    <div style="clear: both;"></div>

<%String accOidInitVal = EncryptDecrypt.encryptStringUsingTripleDes("0", userSession.getSecretKey()); %>
<INPUT TYPE=HIDDEN NAME='acctOidEdit' id='acctOidEdit' VALUE="">

<%  
  //default to this corp org fxrate
  String fxRateGroup = corporateOrg.getAttribute("fx_rate_group");
  if (InstrumentServices.isBlank(otherAccountsHidden)) {
    out.print(widgetFactory.createTextField( "fxRateGroupFEdit", "CorpCust.fxRateGroup", fxRateGroup, "3", isReadOnlyAcc, false, false, " class=\"char11\" disabled=\"disabled\"" , "","inline" ) );
  }
  
  
 %> 
 <%--dpatra Rel-9.5 -added for regulatory account type CR-1051 --%>
 <% 
 	options = Dropdown.createSortedRegAccTypeCodeOptions("", loginLocale);
   out.print(widgetFactory.createSelectField("regulatoryAccountTypeEdit", "CorpCust.RegulatoryAccountType"," ", options,  false, false, false, " class=\"char11\" "+disabledFlag, "", "inline"));
%>

<%-- CR 821 - Rel 8.3 START --%>
    <div style="clear: both;"></div>
        <%
        out.print(widgetFactory.createCheckboxField("deactivateIndicatorEdit", "CorpCust.accountDeactIndex", false, isReadOnlyAcc, false,"","",""));
        %>
        <div id="panelRowsDefault" class="formItem">
        <table border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable" id="panelTable" style="width:90%">
    <thead>
      <tr>
        <th colspan="2" style="text-align: left" ><%=widgetFactory.createSubLabel("CorpCust.PanelGroups")%></th>
       </tr>
    </thead>
    <tbody>
<%
  int panelIndex = 0;
  int rowIndex = 0; 
%>
      <%@ include file="CorporateCustomerPanelRows.frag" %>   	
    </tbody>
  </table>
  </div>
  <%
  if (!isReadOnly) {
%>
  <div class="formItem">
    <button data-dojo-type="dijit.form.Button" type="button" id="add2MorePanel">
    <%=resMgr.getText("CorpCust.Add2Panels",TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        local.add2MorePanelsDefault ();
      </script>
    </button>
  </div>
<%
  }
%>    
<%-- CR 821 - Rel 8.3 END --%>
  </div>

  <div class="columnRight">
<%    
  //to this corp org customer id
  String tpsCustId = corporateOrg.getAttribute("Proponix_customer_id");
  //cquinton hide TP Customer ID if 'do not allow other corp account' is checked on profile
  if (InstrumentServices.isBlank(otherAccountsHidden)) {
    //this field is always for display only purpose
%>
    <%= widgetFactory.createTextField( "PropCustomerIdEdit", "CorpCust.otlCustId", tpsCustId, "20", 
          isReadOnlyAcc, false, false, " class=\"char30\" disabled=\"disabled\"", "","" ) %>
<%
  }
%>

    <input type="hidden" value="<%=corporateOrgOid%>" name="corporateOrgOid" id="corporateOrgOid"> 
    <input type="hidden" value="<%=corporateOrg.getAttribute("first_op_bank_org")%>" name="displayCOOBOid" id="displayCOOBOid">
    <input type="hidden" value="<%=isReadOnly%>" name="isReadOnly" id="isReadOnly">

<%
  //create a set of options for the account op bank orgs
  sqlQuery = new StringBuilder();
  sqlQuery.append("select organization_oid, name from operational_bank_org where organization_oid in (select a_first_op_bank_org from corporate_org where organization_oid = ?");
  sqlQuery.append(" and a_first_op_bank_org is not NULL union select a_second_op_bank_org from corporate_org where organization_oid=?");
  sqlQuery.append(" union select a_third_op_bank_org from corporate_org where organization_oid=?");
  sqlQuery.append(" union select a_fourth_op_bank_org from corporate_org where organization_oid=?)");
  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
  List<Object> sqlParams = new ArrayList();
  sqlParams.add(corporateOrgOid);
  sqlParams.add(corporateOrgOid);
  sqlParams.add(corporateOrgOid);
  sqlParams.add(corporateOrgOid);
  
  queryListView.setSQL(sqlQuery.toString(),sqlParams);
  queryListView.getRecords();
  operationalBankOrgsDoc = queryListView.getXmlResultSet();

  if (InstrumentServices.isBlank(otherAccountsHidden)) {
    //for the initial version, set the selected item to the corp's first op bank org
    dropdownOptions = "<option  value=\"\"></option>";
    dropdownOptions += ListBox.createOptionList(operationalBankOrgsDoc, "ORGANIZATION_OID", "NAME",corporateOrgOpBankOrg1Oid, userSession.getSecretKey());

    //create a dropdown and a hidden disabled text box for displaying the value for accounts belong to subsidiaries
%>
    <div class='formItem <%=isReadOnly?"readOnly":""%>'>
      <%=widgetFactory.createInlineLabel("opBankOrgIdEdit", "CorpCust.opBankOrgId")%>
      <br/>
      <%=widgetFactory.createSelectField("opBankOrgIdEdit", "", "", dropdownOptions, 
          false, false, false, " class=\"char30\" "+disabledFlag, "", "none")%>
      <%=widgetFactory.createTextField("opBankOrgIdOtherAcct", "", "", "",
          false, false, false, " class=\"char30\" disabled=\"disabled\" style=\"display:none\"", "", "none")%>
    </div>
<%
  }
%> 

<%
  out.print(widgetFactory.createLabel("", "CorpCust.AvailableFor", false, false, false, ""));
  out.print(widgetFactory.createCheckboxField("availableForDirectDebitEdit", "CorpCust.AvailableForDirectDebit", false, isReadOnlyAcc, false,"","",""));
  out.print(widgetFactory.createCheckboxField("availableForLoanReqEdit", "CorpCust.AvailableForLoanRequest", false, isReadOnlyAcc, false,"","",""));
  out.print(widgetFactory.createCheckboxField("availableForIntPmtEdit", "CorpCust.AvailableForIntPmt", false, isReadOnlyAcc, false,"","",""));
  out.print(widgetFactory.createCheckboxField("availableForTBAEdit", "CorpCust.AvailableForTBA", false, isReadOnlyAcc, false,"","",""));
  out.print(widgetFactory.createCheckboxField("availableForDomesticPymtEdit", "CorpCust.AvailableForDomesticPymts", false, isReadOnlyAcc, false,"","",""));
  //SSikhakolli - Rel-9.4 CR-818
  out.print(widgetFactory.createCheckboxField("availableForSettlementInstrEdit", "CorpCust.AvailableForSettlementInstructions", false, isReadOnlyAcc, false,"","",""));
%>

<%
  if (!isReadOnly) {
%>
    <div class="formActions" >
      <span class="formActions-right">
        <button data-dojo-type="dijit.form.Button" id="addNewAccount" 
                type="button" onClick="local.addNewAccount();">
          <%=resMgr.getText("CorpCust.SaveAccountText",TradePortalConstants.TEXT_BUNDLE)%>
        </button>
        <%=widgetFactory.createHoverHelp("addNewAccount","CorpCust.SaveAccount") %>
        <button data-dojo-type="dijit.form.Button" id="editAccount" style='display:none;'
                type="button" onClick="local.updateAccount();">
          <%=resMgr.getText("CorpCust.UpdateAccountText",TradePortalConstants.TEXT_BUNDLE)%>
        </button>
        <%=widgetFactory.createHoverHelp("editAccount","CorpCust.UpdateAccount") %>
        <button data-dojo-type="dijit.form.Button" id="cancelAccount" 
                type="button" onClick="local.prepareForNewAccount();">
          <%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE)%>
        </button>
        <%=widgetFactory.createHoverHelp("cancelAccount","CorpCust.CancelAccount") %>
      </span>
      <div style="clear:both;"></div>
    </div>
<%
  }
%>
  </div>
