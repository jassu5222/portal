<%--this fragment is included in CorporateCustomerDetail.jsp --%>
 
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable" id="marginTable">
    <thead>
      <tr>
        <th>&nbsp;</th>
        <th align="left"><%= resMgr.getText("CorpCust.Currency", TradePortalConstants.TEXT_BUNDLE) %></th>
        <th align="left"><%= resMgr.getText("CorpCust.InstrumentType", TradePortalConstants.TEXT_BUNDLE) %></th>
        <th align="left"><%= resMgr.getText("CorpCust.ThresholdAmt", TradePortalConstants.TEXT_BUNDLE) %></th>
        <th align="left"><%= resMgr.getText("CorpCust.RateType", TradePortalConstants.TEXT_BUNDLE) %></th>
        <th align="left"><%= resMgr.getText("CorpCust.Margin", TradePortalConstants.TEXT_BUNDLE) %></th>
      </tr>
      <input type=hidden value="<%=row2Count%>" name="numberOfMultipleObjects2" id="noOf2Rows">
    </thead>
    <tbody>

<%
  int marginIndex = 0;
%>
      <%@ include file="CorporateCustomerMarginRows.frag" %>   	

    </tbody>
  </table>
    
<%
  if (!isReadOnly) {
%>
  <div>
    <button data-dojo-type="dijit.form.Button" type="button" id="add4MoreRules">
    <%=resMgr.getText("CorpCust.Add4Rules",TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        local.add4MoreRules();
      </script>
    </button>
  </div>
<%
  }
%>
