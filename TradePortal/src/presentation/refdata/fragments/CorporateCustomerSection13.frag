<%--this fragment is included in CorporateCustomerDetail.jsp --%>
<% 
if(  corporateOrgSPInd != null && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgSPInd))){%>
  <%= widgetFactory.createTextField("SPPayProgName", "CorpCust.SPPayablesProgName", corporateOrgPayProgName, "60", isReadOnly, true, false, "style=\"width: 400px;\"", "", "" ) %>
<% }else{%>
 <%=  widgetFactory.createTextField("SPPayProgName", "CorpCust.SPPayablesProgName", corporateOrgPayProgName, "60", isReadOnly, false, false, "style=\"width: 400px;\"", "", "" ) %>
<%}%>

 <%= widgetFactory.createSubLabel("CorpCust.SPCalculateDiscountUsing")%>
 	<%
         if ((!insertModeFlag || errorFlag) && corporateOrgSPCalculateDiscountInd != null )
         	{
         	 if (TradePortalConstants.STRAIGHT_DISCOUNT .equals(corporateOrgSPCalculateDiscountInd))
             	{
                defaultRadioButtonValue1 = true;
                defaultRadioButtonValue2 = false;
             	}
        	 else if (TradePortalConstants.DISCOUNT_YIELD .equals(corporateOrgSPCalculateDiscountInd))
            	{
               defaultRadioButtonValue1 = false;
               defaultRadioButtonValue2 = true;
            	}
             else
            	{
           	   defaultRadioButtonValue1 = false;
                defaultRadioButtonValue2 = false;
   			 	}
             }
          else
             {
          	   defaultRadioButtonValue1 = false;
               defaultRadioButtonValue2 = false;
      		}
	%>
 <div style="clear: both;"></div>
 <div>
      <%= widgetFactory.createRadioButtonField("SPCalculateDiscountIndicator", "SPStraightDiscount", "CorpCust.StraightDiscount", TradePortalConstants.STRAIGHT_DISCOUNT, defaultRadioButtonValue1,
      	isReadOnly,
    	"onClick=\"local.setNettedDiscountFiled()\"", "")%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("SPCalculateDiscountIndicator", "SPDiscountToYield", "CorpCust.DiscountToYield", TradePortalConstants.DISCOUNT_YIELD, defaultRadioButtonValue2,
      	isReadOnly,
    	"onClick=\"local.setNettedDiscountFiled()\"", "")%>
</div>    	
<div>    
<div style="clear: both;"></div>  

<% 
 //jgadela IR# T36000020755 - [START] - separated title heading from  below table to eliminate the row count error when we click 4 more rows.
 %>
<table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
      <tr>
        <th colspan="5" style="text-align: left"><%= resMgr.getText("CorpCust.SupplierPortalMarginRules", TradePortalConstants.TEXT_BUNDLE) %></th>
      </tr>
 </table> 
 <% 
 //jgadela IR# T36000020755 - [END] 
 %>  
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable" id="SpMarginTable">
    <thead>
      <tr>
        <th>&nbsp;</th>
        <th><%= resMgr.getText("CorpCust.Currency", TradePortalConstants.TEXT_BUNDLE) %></th>
        <th><%= resMgr.getText("CorpCust.ThresholdAmt", TradePortalConstants.TEXT_BUNDLE) %></th>
        <th><%= resMgr.getText("CorpCust.RateType", TradePortalConstants.TEXT_BUNDLE) %></th>
        <th><%= resMgr.getText("CorpCust.Margin", TradePortalConstants.TEXT_BUNDLE) %></th>
      </tr>
    </thead>
    <input type=hidden value="<%=spMarginRulesCount%>" name="numberOfMultipleObjects1" id="noOfSPMarginRules">
    <tbody>

<%
  int spMarginIndex = 0;
%>
      <%@ include file="CorporateCustomerSPMarginRows.frag" %>   	

    </tbody>
  </table>
</div>    
<%
  if (!isReadOnly) {
%>
  <div>
    <button data-dojo-type="dijit.form.Button" type="button" id="add4MoreSPMarginRules">
			<%= resMgr.getText("common.Add4MoreRules", TradePortalConstants.TEXT_BUNDLE) %>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        local.add4MoreSPMarginRules();
      </script>
    </button>
  </div>
<%
  }
%>
