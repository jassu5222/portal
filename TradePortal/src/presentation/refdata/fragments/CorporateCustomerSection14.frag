
<%
  int rowCheck = 0;
  String operBank = null;
  String extBankOid = null;
  String corpOid = null;
  int extBankSize = externalBankList.size();
  Debug.debug("externalBankList.size() "+externalBankList.size());
  //Each row contains 3 External Banks. Incase of short: we have to create additional external banks to match the row count.
  while( (extBankSize % 3) != 0 ){
	  externalBankList.add(beanMgr.createBean(ExternalBankWebBean.class, "ExternalBank"));
	  extBankSize++;
  }
  %>
<input type=hidden value="<%=extBankSize%>" name="numberOfMultipleObjects3" id="numberOfMultipleObjects3">
 
<table id="ExternalBankTable"  border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable"> <%-- Table Start --%>
<thead>
     
    </thead>
    <tbody>
  <tr >
  <% 

  //Extract the webbean object from the list, display external bank values in screen.
  for( int index=0; index < extBankSize; index++ ){
	  extBankWebBean = (ExternalBankWebBean) externalBankList.get(index);
	
	  if(null != extBankWebBean.getAttribute("op_bank_org_oid")){
		  operBank = extBankWebBean.getAttribute("op_bank_org_oid");
		 
	  }else{
		  operBank = "";
	  }
	  if(null != extBankWebBean.getAttribute("external_bank_oid")){
		  extBankOid = extBankWebBean.getAttribute("external_bank_oid");
	  }else{
		  extBankOid = "";
	  }
	  if(null != extBankWebBean.getAttribute("corp_org_oid")){
		  corpOid = extBankWebBean.getAttribute("corp_org_oid");
	  }else{
		  corpOid = "";
	  }
  if( rowCheck==0 ){
  %>
  <tr >
  <% } %>
  	<td align="center"><%
  	
  	dropdownOptions = Dropdown.createSortedOptions(externalBankOrgsDoc, "ORGANIZATION_OID", "NAME", operBank, userSession.getSecretKey(), resMgr.getResourceLocale());
  	
  	out.print(widgetFactory.createSelectField( "OperationalBankOid"+index, "", defaultText, dropdownOptions, 
    isReadOnly, false, false, "","","none"));
  
  out.print("<INPUT TYPE=HIDDEN NAME='ExternalBankOid"+index+"' VALUE='"+extBankOid+"'>");
  out.print("<INPUT TYPE=HIDDEN NAME='CorporateOrgOid"+index+"' VALUE='" +corporateOrgOid+ "'>");
  rowCheck++;
  %>
  	</td>
  	<% if(rowCheck > 2){%>
  		</tr>
  	<% rowCheck = 0;
  	}%>
  	
  
   <%} %>
   </tr>
   </tbody>
  </table>
 
  <% if (!(isReadOnly)) {		%>
			<button data-dojo-type="dijit.form.Button" type="button" id="addMoreExternalBanks">
				<%=resMgr.getText("CorpCust.ExternalBankAddButton",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					addMoreExternalBanks();
				</script>
			</button>
		<% } else {		%>
					&nbsp;
		<% }		%>
