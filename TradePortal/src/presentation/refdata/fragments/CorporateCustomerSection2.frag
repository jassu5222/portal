<%--this fragment is included in CorporateCustomerDetail.jsp --%>

	<div class="columnLeft">
          <%
            defaultText = "";
			if (insertModeFlag)
             {
				defaultText = resMgr.getText("CorpCust.SelectACurrency", TradePortalConstants.TEXT_BUNDLE);
             }

             dropdownOptions = Dropdown.createSortedRefDataOptions("CURRENCY_CODE", corporateOrgBaseCurrency, userLocale);
             out.print(widgetFactory.createSelectField( "BaseCurrencyCode", "CorpCust.BaseCurrency", " ", dropdownOptions, isReadOnly,true,false,"class='char35'","",""));

		   if (hasCorporateOrgs)
           {
                   defaultText = " ";
                   dropdownOptions = ListBox.createOptionList(corporateOrgsDoc, "ORGANIZATION_OID", "NAME", corporateOrgParentOrgOid, userSession.getSecretKey());
                   out.print(widgetFactory.createSelectField( "ParentCorporationOid", "CorpCust.ParentCorp", defaultText, dropdownOptions, isReadOnly, false,false,"class='char35'","",""));
           } else {
              out.print("&nbsp;");
           }

           dropdownOptions = Dropdown.createSortedRefDataOptions("TIMEZONE", corporateOrgTimeZone, userLocale);
           out.print(widgetFactory.createSelectField( "TimeZone", "CorpCust.TimeZone", defaultText, dropdownOptions, isReadOnly,true,false,"class='char35'","",""));

           defaultCheckBoxValue = false;
		   if ((!insertModeFlag || errorFlag) && (corporateOrgBankTypePartiesInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;
           }
           out.print(widgetFactory.createCheckboxField("BankTypePartiesIndicator", "CorpCust.BankTypeParties", defaultCheckBoxValue, isReadOnly));

           defaultCheckBoxValue = false;
           if ((!insertModeFlag || errorFlag) && (corporateOrgDocPrepInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;
           }
           //defaultText = resMgr.getText("CorpCust.DocumentPreparation", TradePortalConstants.TEXT_BUNDLE);
           out.print(widgetFactory.createCheckboxField("DocPrepInd", "CorpCust.DocumentPreparation", defaultCheckBoxValue, isReadOnly));

           defaultCheckBoxValue = false;
           if ((!insertModeFlag || errorFlag) && (corporateOrgTimeoutAutoSaveInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;
           }
           out.print(widgetFactory.createCheckboxField("TimeoutAutoSaveInd", "CorpCust.TimeoutAutoSaveInd", defaultCheckBoxValue, isReadOnly));

           defaultCheckBoxValue = false;
           if ((!insertModeFlag || errorFlag) && (corporateOrgLastEntryUserInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;
           }
           out.print(widgetFactory.createCheckboxField("LastEntryUserIndicator","CorpCust.LastEntryUser",defaultCheckBoxValue, isReadOnly));
         
           defaultCheckBoxValue = false;
           if ((!insertModeFlag || errorFlag) && (corporateOrgPreDebitOptionInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;
           }
           out.print(widgetFactory.createCheckboxField("PreDebitOptionIndicator","CorpCust.PreDebitOptionInd",defaultCheckBoxValue, isReadOnly));
         
           if(TradePortalConstants.INDICATOR_YES.equals(cbMobileBankingInd)){
        	   defaultCheckBoxValue = false;
        	   if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgMobileBankingInd)) )
        	   {
        		   defaultCheckBoxValue = true;
        	   }
        	   out.print(widgetFactory.createCheckboxField("MobileBankingAccessInd","CorpCust.MobileBankingInd",defaultCheckBoxValue, isReadOnly));
           
           }
        %>
       <%--Rel 9.5 CR-927B - Moving the reporting type field to this section from Section3-Notification and Email --%> 
        <%
    		sqlQuery = new StringBuilder();
        	queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
        	sqlQuery.append("select report_categories_ind from client_bank");
        	sqlQuery.append(" where organization_oid = ?");
      		Debug.debug(sqlQuery.toString());
      		queryListView.setSQL(sqlQuery.toString(),new Object[]{clientBankOid});
      		queryListView.getRecords();
      		String reportCategInd = queryListView.getRecordValue("report_categories_ind");
			
      		if (!("Y".equals(reportCategInd))) {
     	 %>
			<div class="formItem">
				<%=resMgr.getText("UserDetail.ReportType", TradePortalConstants.TEXT_BUNDLE)%>

				<%
				// Setup the        type radio button options;
				// if the indicator is set
				// to the empty string or "N", make the first option (Cash Management) the default
				String reportingType = corporateOrg.getAttribute("reporting_type");

				out.println(widgetFactory.createRadioButtonField("ReportingType", "TradePortalConstants.REPORTING_TYPE_CASH_MGMT",
                        "UserDetail.CashManagement", TradePortalConstants.REPORTING_TYPE_CASH_MGMT, reportingType.equals(TradePortalConstants.REPORTING_TYPE_CASH_MGMT), isReadOnly, "", ""));

				out.println(widgetFactory.createRadioButtonField("ReportingType", "TradePortalConstants.REPORTING_TYPE_TRADE_SRV",
                              "UserDetail.TradePortal", TradePortalConstants.REPORTING_TYPE_TRADE_SRV, reportingType.equals(TradePortalConstants.REPORTING_TYPE_TRADE_SRV), isReadOnly, "", ""));
              %>
            </div>
            <%                    
			} else {
			%>
                  <input type=hidden  name=ReportingType value="T" >
            <% 
			} %>
       
       
	</div>
    
  <div class="columnRight">
<%
  String corporateCustomerType = corporateOrg.getAttribute("corporate_org_type_code");
  String corpCustTypeWidgetProps = "placeHolder:'" +
    resMgr.getText("CorpCust.SelectACorpCustType", TradePortalConstants.TEXT_BUNDLE) + "'";
  //cquinton 12/14/2012 ir#8895 use the widget 'value' to set default as its more accurate
  // than relying on options.
  if (!InstrumentServices.isBlank(corporateCustomerType)) {
    corpCustTypeWidgetProps += ", value:'" + corporateCustomerType + "'";
  }
  else {
    corpCustTypeWidgetProps += ", value:''"; //set initial value to blank rather than the first select item
  }
  dropdownOptions = Dropdown.createSortedRefDataOptions("CORP_ORG_TYPE", corporateCustomerType, userLocale);
  out.print(widgetFactory.createSelectField( "CorporateCustomerType", "CorpCust.CorporateCustomerType", "", 
              dropdownOptions, isReadOnly,true,false,"class='char35'",corpCustTypeWidgetProps,"inline"));
%>
    <div style="clear: both;"></div>

<%
  if (!insertModeFlag && hasCorporateOrgUsers) {
    defaultText = " ";
    dropdownOptions = ListBox.createOptionList(corporateOrgUsersDoc, "USER_OID", "USER_IDENTIFIER", corporateOrgDefaultUserOid, userSession.getSecretKey());
    out.print(widgetFactory.createSelectField( "DefaultUserOid", "CorpCust.DefaultUser", defaultText, dropdownOptions, isReadOnly, false, false,"class='char10'","",""));
  }
%>

    <%= widgetFactory.createTextField( "FXRateGroup", "CorpCust.FXRateGroup", corporateOrg.getAttribute("fx_rate_group"), "3", isReadOnly, false, false,"onChange=\"setAccountFxRateGroup("+corporateOrgOid+")\"class='char10'","","inline") %>
    <%= widgetFactory.createTextField( "InterestDiscRateGroup", "CorpCust.InterestDiscRateGroup", corporateOrg.getAttribute("interest_disc_rate_group"), "3", isReadOnly, false, false,"onblur=\"this.value=this.value.toUpperCase()\"class='char10'","","inline") %>

    <div style="clear: both;"></div>

    <div class="formItem"> 

      <%= widgetFactory.createSubLabel("CorpCust.ForRouting") %>

<%
  String routingSetting = corporateOrg.getAttribute("route_within_hierarchy");

  // Default the routing setting to being able to route within the entire hierarchy
  if(InstrumentServices.isBlank(routingSetting))
    routingSetting = TradePortalConstants.ROUTE_ALL;
%>
      <br>
      <%= widgetFactory.createRadioButtonField("RoutingSetting", "TradePortalConstants.ROUTE_ALL",
            "CorpCust.CanRouteThroughout", TradePortalConstants.ROUTE_ALL, routingSetting.equals(TradePortalConstants.ROUTE_ALL), isReadOnly, "", "") %>
      <br>
      <%= widgetFactory.createRadioButtonField("RoutingSetting", "TradePortalConstants.CHILDREN_PARENT_ONLY",
            "CorpCust.CanRouteOnlyChildrenParent", TradePortalConstants.CHILDREN_PARENT_ONLY, routingSetting.equals(TradePortalConstants.CHILDREN_PARENT_ONLY), isReadOnly, "", "") %>
      <br>
    </div>

    <div class="formItem">

      <%= widgetFactory.createSubLabel("CorpCust.ForExportLCs")%>

<%
  String dnldAdvTermsFormat = corporateOrg.getAttribute("dnld_adv_terms_format");

  // Default the format setting to XML Format
  if(InstrumentServices.isBlank(dnldAdvTermsFormat))
    dnldAdvTermsFormat = TradePortalConstants.DNLD_XML;
%>
      <br>
      <%= widgetFactory.createRadioButtonField("DnldAdvTermsFormat", "TradePortalConstants.DNLD_SWIFT",
            "CorpCust.SWIFTFormat", TradePortalConstants.DNLD_SWIFT, dnldAdvTermsFormat.equals(TradePortalConstants.DNLD_SWIFT), isReadOnly, "", "") %>
      <br>
      <%= widgetFactory.createRadioButtonField("DnldAdvTermsFormat", "TradePortalConstants.DNLD_XML",
            "CorpCust.XMLFormat", TradePortalConstants.DNLD_XML, dnldAdvTermsFormat.equals(TradePortalConstants.DNLD_XML), isReadOnly, "", "") %>
      <br>
    </div>
	<%--Rel 9.5 CR-927B - Moving the Email Language field to this section from Section3-Notification and Email --%>
    <%
    	dropdownOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.INSTRUMENT_LANGUAGE, corporateOrgEmailLanguage, userLocale);
        out.print(widgetFactory.createSelectField( "EmailLanguage", "CorpCust.SendEmailIn", "", dropdownOptions, isReadOnly));
    %>
    
  </div>

