<%--this fragment is included in CorporateCustomerDetail.jsp --%>

  <div>
    &nbsp;&nbsp;<%= widgetFactory.createSubLabel("CorpCust.UsersForThisCorporateCustomer") %>
  </div>
    
  <div class="columnLeft">
<%
  String authMethod = corporateOrg.getAttribute("authentication_method");

  //cquinton 11/20/2012
  //if this is a new corporate org, default to password
  boolean passwordChecked = false;
  if (!getDataFromXmlDocFlag && insertModeFlag) { //i.e. new
    passwordChecked = true;
  }
  else { //use the authMethod
    passwordChecked = TradePortalConstants.AUTH_PASSWORD.equals(authMethod);
  }
%>
    <%= widgetFactory.createRadioButtonField("AuthenticationMethod", "TradePortalConstants.AUTH_CERTIFICATE",
         "CorpCust.MustPresentACertificate", TradePortalConstants.AUTH_CERTIFICATE, authMethod.equals(TradePortalConstants.AUTH_CERTIFICATE), isReadOnly, "onClick=\"local.unCheckForceCertsForUserMaint()\"", "")%>
    <br>
    <%= widgetFactory.createRadioButtonField("AuthenticationMethod", "TradePortalConstants.AUTH_SSO",
          "CorpCust.MustUseSSO", TradePortalConstants.AUTH_SSO, authMethod.equals(TradePortalConstants.AUTH_SSO), isReadOnly, "onClick=\"local.unCheckForceCertsForUserMaint()\"", "")%>
    <br>
    <%= widgetFactory.createRadioButtonField("AuthenticationMethod", "TradePortalConstants.AUTH_2FA",
          "CorpCust.MustUse2FA", TradePortalConstants.AUTH_2FA, authMethod.equals(TradePortalConstants.AUTH_2FA), isReadOnly, "onClick=\"local.unCheckForceCertsForUserMaint()\"", "")%>
    <br>
    <br>
  </div>

  <div class="columnRight">
    <%= widgetFactory.createRadioButtonField("AuthenticationMethod", "TradePortalConstants.AUTH_PASSWORD",
          "CorpCust.MustEnterPasswords", TradePortalConstants.AUTH_PASSWORD, passwordChecked, isReadOnly, "onClick=\"local.unCheckForceCertsForUserMaint()\"", "")%>
    <br>
    <%= widgetFactory.createRadioButtonField("AuthenticationMethod", "TradePortalConstants.AUTH_REGISTERED_SSO",
          "CorpCust.MustUseSSOByRegistration", TradePortalConstants.AUTH_REGISTERED_SSO, authMethod.equals(TradePortalConstants.AUTH_REGISTERED_SSO), isReadOnly, "onClick=\"local.unCheckForceCertsForUserMaint()\"", "")%>
    <br>
    <%= widgetFactory.createRadioButtonField("AuthenticationMethod", "TradePortalConstants.AUTH_PERUSER",
          "CorpCust.ConfiguredForEither", TradePortalConstants.AUTH_PERUSER, authMethod.equals(TradePortalConstants.AUTH_PERUSER), isReadOnly, "onClick=\"local.unCheckForceCertsForUserMaint()\"", "")%>
    <br>
  </div>

  <div style="clear:both;"></div>

  <%= widgetFactory.createCheckboxField("ForceCertsForUserMaint", "CorpCust.CertUsersMaintain", TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("force_certs_for_user_maint")), isReadOnly, false, "labelClass=\"formWidth\"","", "none")%>
