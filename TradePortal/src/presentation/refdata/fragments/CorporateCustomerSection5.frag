<%--this fragment is included in CorporateCustomerDetail.jsp --%>

  <div class="columnLeft">

    <%= widgetFactory.createTextField( "ProponixCustomerId", "CorpCust.CustomerID", corporateOrg.getAttribute("Proponix_customer_id"), "20", isReadOnly, true) %>

<%
  if (isClientBankUser)   {
    String bankGrpWidgetProps = "placeHolder:'" +
      resMgr.getText("CorpCust.SelectABankGroup", TradePortalConstants.TEXT_BUNDLE) + "'";
    //cquinton 12/14/2012 ir#8895 use the widget 'value' to set default as its more accurate
    // than relying on options.
    if (!InstrumentServices.isBlank(corporateOrgBankOrgGroupOid)) {
      String encBankGroupOid = EncryptDecrypt.encryptStringUsingTripleDes(corporateOrgBankOrgGroupOid, userSession.getSecretKey());
      bankGrpWidgetProps += ", value:'" + encBankGroupOid + "'";
    }
    else {
      bankGrpWidgetProps += ", value:''"; //set initial value to blank rather than the first select item
    }
    dropdownOptions = ListBox.createOptionList(bankGroupsDoc, "ORGANIZATION_OID", "NAME", corporateOrgBankOrgGroupOid, userSession.getSecretKey());
    out.print(widgetFactory.createSelectField( "BankOrgGroupOid", "CorpCust.BankGroup", "",
      dropdownOptions, isReadOnly, true, false, "", bankGrpWidgetProps, "inline"));
  }
  else {
    out.print("&nbsp;");
  }

  dropdownOptions = ListBox.createOptionList(corporateOrgUsersDoc, "USER_OID", "USER_IDENTIFIER", h2hSystemUserOid, userSession.getSecretKey());
%>

         <%=widgetFactory.createSelectField( "H2HSystemUserOid", "CorpCust.H2HSystemUser", " ", dropdownOptions, isReadOnly, false, false, "", "", "inline") %>

         <div style="clear:both;"></div>

            <%
                  String allowLiveMarketRates = CBCResult.getAttribute("/ResultSetRecord(0)/ALLOW_LIVE_MARKET_RATES_REQ");
				  
                if(allowLiveMarketRates.equals("Y"))  {
            %>
					<%= widgetFactory.createTextField( "fxOnlineAcctId", "CorpCust.FxOnlineAcctId", corporateOrg.getAttribute("fx_online_acct_id"), "250", isReadOnly) %>
             <%
                }

                  dropdownOptions = ListBox.createOptionList(corporateOrgUsersDoc, "USER_OID", "USER_IDENTIFIER", uploadSystemUserOid, userSession.getSecretKey());
              %>
 
             <%=widgetFactory.createSelectField( "UploadSystemUserOid", "CorpCust.UploadSystemUser", " ", dropdownOptions, isReadOnly, false, false, "", "", "inline") %>

    </div>
	
    <div class="columnRight">

        <%= widgetFactory.createTextField( "DocPrepAccountName", "CorpCust.TradeBeamAccountName", corporateOrg.getAttribute("doc_prep_accountname"), "256", isReadOnly) %>

        <%= widgetFactory.createTextField( "DocPrepURL", "CorpCust.TradeBeamURL", corporateOrg.getAttribute("doc_prep_url"), "2000", isReadOnly) %>

        <%= widgetFactory.createTextField( "DocPrepDomainName", "CorpCust.TradeBeamDomainName", corporateOrg.getAttribute("doc_prep_domainname"), "2000", isReadOnly) %>
        
<%
	defaultCheckBoxValue = false;
	if(insertModeFlag || (TradePortalConstants.INDICATOR_YES.equals(tpsIntegCustomer)) ){
		defaultCheckBoxValue = true;
	}
	
	if(userSession.isEnableAdminUpdateInd()){
		if(userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_BOG)){
			BankOrganizationGroupWebBean bankOrgGrp  = beanMgr.createBean(BankOrganizationGroupWebBean.class, "BankOrganizationGroup");
			bankOrgGrp.getById(userSession.getOwnerOrgOid());
			if(TradePortalConstants.INDICATOR_YES.equals(bankOrgGrp.getAttribute("enable_admin_update_centre"))){
%> 	
					<%= widgetFactory.createCheckboxField("custIsNotIntgTps", "CorpCust.CustomerNOTIntegratedTPS", defaultCheckBoxValue, isReadOnly, false, "", "", "") %>
<%
			}
		}else{
%>
			<div id='custIsNotIntgTpsDiv'>
				<%= widgetFactory.createCheckboxField("custIsNotIntgTps", "CorpCust.CustomerNOTIntegratedTPS", defaultCheckBoxValue, isReadOnly, false, "", "", "") %>
			</div>
<%
		}
	}
%>
    </div>
