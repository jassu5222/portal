<%--this fragment is included in CorporateCustomerDetail.jsp --%>


  <%= widgetFactory.createSubLabel("CorpCust.SelectOpBankOrganisationTextUpdated") %>
  <br>

  <div class="columnLeft">
    <br/>
    
<%
  //keep a set of hidden inputs that map encrypted values to names
  String  value;
  String  text;
  // WUUK082559086 Check null inputDoc to avoid NullPointerException
  if (operationalBankOrgsDoc != null) {
    Vector ocv = operationalBankOrgsDoc.getFragments ("/ResultSetRecord");
    int items = ocv.size ();
    for (int i = 0; i < items; i++) {
      DocumentHandler docO = (DocumentHandler)ocv.elementAt (i);
      // Extract the value and text attributes from the document.
      try {
        value = docO.getAttribute ("/ORGANIZATION_OID");
      }
      catch (Exception e){
        value = "0";
      }
      try {
        text = docO.getAttribute ("/NAME");
        text = StringFunction.xssCharsToHtml(text); 
      } catch (Exception e){
        text = "";
      }
      // W Zhu 8/17/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.            
      value = EncryptDecrypt.encryptStringUsingTripleDes(value, userSession.getSecretKey());
      out.println("<input type=hidden name=\"" + value + "\" id=\"" + value + "\" value=\"" + text + "\">");
    }
  }

  //now create the actual dropdowns
  String opOrg1Props = "placeHolder:'" +
    resMgr.getText("CorpCust.SelectABankOrg", TradePortalConstants.TEXT_BUNDLE) + "'";
  //cquinton 12/14/2012 ir#8895 use the widget 'value' to set default as its more accurate
  // than relying on options.
  if (!InstrumentServices.isBlank(corporateOrgOpBankOrg1Oid)) {
    String encOpOrg1 = EncryptDecrypt.encryptStringUsingTripleDes(corporateOrgOpBankOrg1Oid, userSession.getSecretKey());
    opOrg1Props += ", value:'" + encOpOrg1 + "'";
  }
  else {
    opOrg1Props += ", value:''"; //set initial value to blank rather than the first select item
  }
  dropdownOptions = ListBox.createOptionList(operationalBankOrgsDoc, "ORGANIZATION_OID", "NAME", corporateOrgOpBankOrg1Oid, userSession.getSecretKey());
  out.print(widgetFactory.createSelectField( "OperationalBankOrg1", "", " ", dropdownOptions, 
    isReadOnly, false, false, "onChange=\"local.setAccountOpBankOrg("+corporateOrgOid+", '1')\"","",""));

  //the ...Hid inputs here allow us to have reference to the previous values when dropdown changes.
  //todo: there has got to be a better way just through the onChange function call...
  String encryptedOpBankOrg1 = "";
  if (InstrumentServices.isNotBlank(corporateOrgOpBankOrg1Oid)) {
    encryptedOpBankOrg1 = EncryptDecrypt.encryptStringUsingTripleDes(corporateOrgOpBankOrg1Oid, userSession.getSecretKey());
  }
  out.println("<input type=hidden name=OperationalBankOrgHid1 id=OperationalBankOrgHid1 value=\"" + encryptedOpBankOrg1 + "\">");
%>

    <div style="clear: both;"></div>

<%
  defaultText = " ";
  dropdownOptions = ListBox.createOptionList(operationalBankOrgsDoc, "ORGANIZATION_OID", "NAME", corporateOrgOpBankOrg3Oid, userSession.getSecretKey());
  out.print(widgetFactory.createSelectField( "OperationalBankOrg3", "", defaultText, dropdownOptions, 
    isReadOnly, false, false, "onChange=\"local.setAccountOpBankOrg("+corporateOrgOid+", '3')\"","",""));

  String encryptedOpBankOrg3 = "";
  if (InstrumentServices.isNotBlank(corporateOrgOpBankOrg3Oid)) {
    encryptedOpBankOrg3 = EncryptDecrypt.encryptStringUsingTripleDes(corporateOrgOpBankOrg3Oid, userSession.getSecretKey());
  }
  out.println("<input type=hidden name=OperationalBankOrgHid3 id=OperationalBankOrgHid3 value=\"" + encryptedOpBankOrg3 + "\">");
%>

  </div>

  <div class="columnRight">
  <br/>

<%
  //defaultText = " ";
  dropdownOptions = ListBox.createOptionList(operationalBankOrgsDoc, "ORGANIZATION_OID", "NAME", corporateOrgOpBankOrg2Oid, userSession.getSecretKey());
  out.print(widgetFactory.createSelectField( "OperationalBankOrg2", "", defaultText, dropdownOptions, 
    isReadOnly, false, false, "onChange=\"local.setAccountOpBankOrg("+corporateOrgOid+", '2')\"","",""));

  String encryptedOpBankOrg2 = "";
  if (InstrumentServices.isNotBlank(corporateOrgOpBankOrg2Oid)) {
    encryptedOpBankOrg2 = EncryptDecrypt.encryptStringUsingTripleDes(corporateOrgOpBankOrg2Oid, userSession.getSecretKey());
  }
  out.println("<input type=hidden name=OperationalBankOrgHid2 id=OperationalBankOrgHid2 value=\"" + encryptedOpBankOrg2 + "\">");
        
  //defaultText = " ";
  dropdownOptions = ListBox.createOptionList(operationalBankOrgsDoc, "ORGANIZATION_OID", "NAME", corporateOrgOpBankOrg4Oid, userSession.getSecretKey());
  out.print(widgetFactory.createSelectField( "OperationalBankOrg4", "", defaultText, dropdownOptions, 
    isReadOnly, false, false, "onChange=\"local.setAccountOpBankOrg("+corporateOrgOid+", '4')\"","",""));

  String encryptedOpBankOrg4 = "";
  if (InstrumentServices.isNotBlank(corporateOrgOpBankOrg4Oid)) {
    encryptedOpBankOrg4 = EncryptDecrypt.encryptStringUsingTripleDes(corporateOrgOpBankOrg4Oid, userSession.getSecretKey());
  }
  out.println("<input type=hidden name=OperationalBankOrgHid4 id=OperationalBankOrgHid4 value=\"" + encryptedOpBankOrg4 + "\">");
%>

  </div>
