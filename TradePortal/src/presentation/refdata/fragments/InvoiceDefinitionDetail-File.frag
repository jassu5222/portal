<%--
*******************************************************************************
             Invoice Upload Definition Detail - File Definition (Tab) Page

Description:   This jsp simply displays the html for the file tab portion of 
field Order.  This page is called from 
InvoiceDefinition.jsp  and is called by:
		<%@ include file="InvoiceDefinitionDetail-File.jsp" %>
Note: the values stored in secureParms in this page are done deliberately to 
avoid being overwritten as each tab is a submit action.  As a result, if this 
data does not make it into the xml document, then it may be overwritten with a 
null value in this tab or another.
*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%
Debug.debug("******************** IN FILE DEFINITION TAB JSP *************************");	

 String fieldINVOptionList        = invoiceDef.buildINVFieldOptionList(resMgr);
 String fieldINVGoodsOptionList   = invoiceDef.buildINVGoodsFieldOptionList(resMgr);
 String fieldItemDetailOptionList = invoiceDef.buildLineItemFieldOptionList(resMgr);
 //Nar IR-NEUM031959693 04/09/2012 Begin -set number of field acording to invoice definition required field
 final int NUMBER_OF_INV_SUMMARY_FIELDS      = invoiceDef.getReqInvSummaryCount();
 final int NUMBER_OF_INV_GOODS_FIELDS        = invoiceDef.getReqInvGoodsCount();
 final int NUMBER_OF_LNE_ITEM_DETAILS_FIELDS = invoiceDef.getReqInvLineitemCount();
 // in invoice summary data dropdown, total fields are 9 and out of that only 8 is required. user can select Buyer ID, or Buyer Name or both Name and ID.
 //if invoice definition is in read only mode and user seleted only either Buyer ID or Buyer Name, then value of 'buyerNameORID' variable will be 1. 
 int buyerNameORID = 0;
 //Nar IR-NEUM031959693 04/09/2012 End
 String defaultText = " ";
 String options;
 String lineDetail=invoiceDef.getAttribute("line_item_detail_provided");
 //This will ensure that the 'update message' will have the correct reference to this 
 //Invoice Definition.  Otherwise 'null' is substituted in for the defnintion name inthe message.

 secureParms.put("name", invoiceDef.getAttribute("name"));

 //Because these are check boxes, and the tabs are submit actions - we need to ensure that
 //These values make it to the mapfile otherwise the premediator will reset the values to 'N'

		secureParms.put ( "linked_instrument_ty_req", invoiceDef.getAttribute("linked_instrument_ty_req" ));
		secureParms.put ( "incoterm_req", invoiceDef.getAttribute("incoterm_req" ));
		secureParms.put ( "country_of_loading_req", invoiceDef.getAttribute("country_of_loading_req" ));
		secureParms.put ( "country_of_discharge_req", invoiceDef.getAttribute("country_of_discharge_req" ));
		secureParms.put ( "vessel_req", invoiceDef.getAttribute("vessel_req" ));
		secureParms.put ( "carrier_req", invoiceDef.getAttribute("carrier_req" ));
		secureParms.put ( "actual_ship_date_req", invoiceDef.getAttribute("actual_ship_date_req" ));
		secureParms.put ( "payment_date_req", invoiceDef.getAttribute("payment_date_req" ));
		secureParms.put ( "purchase_ord_id_req", invoiceDef.getAttribute("purchase_ord_id_req" ));
		secureParms.put ( "goods_description_req", invoiceDef.getAttribute("goods_description_req" ));
		secureParms.put ( "include_column_headers", invoiceDef.getAttribute("include_column_headers" )); // Nar IR-NNUM022937252 03/24/2012
		for(int x=1; x<=TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++){
			secureParms.put ( "buyer_users_def" + x + "_req", invoiceDef.getAttribute("buyer_users_def" + x + "_req"));
			secureParms.put ( "seller_users_def" + x + "_req", invoiceDef.getAttribute("seller_users_def" + x + "_req"));			
		}
		
		//Checking line item field required check box
		
		secureParms.put ( "line_item_id_req", invoiceDef.getAttribute("line_item_id_req" ));
		secureParms.put ( "unit_price_req", invoiceDef.getAttribute("unit_price_req" ));
		secureParms.put ( "unit_of_measure_price_req", invoiceDef.getAttribute("unit_of_measure_price_req" ));
		secureParms.put ( "inv_quantity_req", invoiceDef.getAttribute("inv_quantity_req" ));
		secureParms.put ( "unit_of_measure_quantity_req", invoiceDef.getAttribute("unit_of_measure_quantity_req" ));
		secureParms.put ( "line_item_detail_provided", invoiceDef.getAttribute("line_item_detail_provided" ));
		
		
		for(int x=1; x<=TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++){
			secureParms.put ( "prod_chars_ud" + x + "_type_req", invoiceDef.getAttribute("prod_chars_ud" + x + "_type_req"));
			//secureParms.put ( "prod_chars_ud" + x + "_val_req", invoiceDef.getAttribute("prod_chars_ud" + x + "_val_req" ));			
		}
%>


  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ColorBeige">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
   </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="15" nowrap>&nbsp;</td> 
      <td>
         <p class="ListText">
         <%= resMgr.getText("InvoiceDefinition.InvFieldOrderDesc", 
					TradePortalConstants.TEXT_BUNDLE) %>
	    </p>			 					
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="ColorGrey">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td>
         <p class="ControlLabel">
         <%= resMgr.getText("InvoiceDefinition.InvData", 
					TradePortalConstants.TEXT_BUNDLE) %>
	    </p>				
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
   <td>
   <table width="50%" border="0" cellspacing="0" cellpadding="0">
    <tr class="ColorGrey"> 
      <td>&nbsp;</td>     
      <td nowrap> 
        <p class="ControlLabel">
         <%= resMgr.getText("InvoiceDefinition.InvSummaryData", 
					TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td> 
        <p class="ControlLabel" align="center">
		<%= resMgr.getText("InvoiceDefinition.Order", 
					TradePortalConstants.TEXT_BUNDLE) %>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
		<%= resMgr.getText("InvoiceDefinition.FieldName", 
					TradePortalConstants.TEXT_BUNDLE) %>
	</p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
<%
   for(int x=1; x<=NUMBER_OF_INV_SUMMARY_FIELDS; x++) {
%>
    <tr> 
      <td>&nbsp;</td>
    <%
     /********************
      * Order XX Field
      ********************/ 
    %>
      <td> 
        <p class="ControlLabel" align="center"> 
       <%-- invoice summaty order _x is blank, it means user didn't select both Buyer Name and Buyer ID. so
             in read only mode don't display sequence number of both.  --%>
       <% if(isReadOnly && InstrumentServices.isBlank(invoiceDef.getAttribute("inv_summary_order" + x))) {
             buyerNameORID = 1;
          }else{
             out.println(x);      
          }
        %>  
	  <span class="AsterixText"></span>
	</p>
      </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText"> 
<%
            options = invoiceDef.selectFieldInList( fieldINVOptionList, 
					  	     invoiceDef.getAttribute("inv_summary_order" + x) );
            out.println(InputField.createSelectField("inv_summary_order" + x, "", 
                        defaultText, options, "ListText", isReadOnly));
%>
      </td>
      <td>&nbsp;</td>
    </tr>

<% }
%>
   <tr> 
      <td width="20" nowrap>&nbsp;</td>
   </tr>
    <tr class="ColorGrey"> 
      <td>&nbsp;</td>     
      <td nowrap> 
        <p class="ControlLabel">
         <%= resMgr.getText("InvoiceDefinition.InvGoodsData", 
					TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td> 
        <p class="ControlLabel" align="center">
		<%= resMgr.getText("InvoiceDefinition.Order", 
					TradePortalConstants.TEXT_BUNDLE) %>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
		<%= resMgr.getText("InvoiceDefinition.FieldName", 
					TradePortalConstants.TEXT_BUNDLE) %>
	</p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
<%
   for(int x=1; x<=NUMBER_OF_INV_GOODS_FIELDS; x++) {
%>
    <tr> 
      <td>&nbsp;</td>
    <%
     /********************
      * Order XX Field
      ********************/ 
    %>
      <td> 
        <p class="ControlLabel" align="center"> 
	  <%= x + NUMBER_OF_INV_SUMMARY_FIELDS - buyerNameORID %>
	  <span class="AsterixText"></span>
	</p>
      </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText"> 
<%	
            options = invoiceDef.selectFieldInList( fieldINVGoodsOptionList, 
					  	     invoiceDef.getAttribute("inv_goods_order" + x) );
            out.println(InputField.createSelectField("inv_goods_order" + x, "", 
                        defaultText, options, "ListText", isReadOnly));
%>
      </td>
      <td>&nbsp;</td>
    </tr>

<% }
%> 
  </table>
 </td>
 <%-- //Srinivasu_D IR-KUL122880629  01/24/2011 Begin --%>
 <td>
 <%
 if(lineDetail!=null && !lineDetail.equals(TradePortalConstants.INDICATOR_YES)){
 %>
 <%-- //Srinivasu_D IR-KUL122880629  01/24/2011 End --%>
  <table width="50%" border="0" cellspacing="0" cellpadding="0">
   <tr class="ColorGrey">     
      <td nowrap > 
        <p class="ControlLabel">
         <%= resMgr.getText("InvoiceDefinition.InvoiceLineItemDetail", 
					TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td> 
        <p class="ControlLabel" align="center">
		<%= resMgr.getText("InvoiceDefinition.Order", 
					TradePortalConstants.TEXT_BUNDLE) %>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
		<%= resMgr.getText("InvoiceDefinition.FieldName", 
					TradePortalConstants.TEXT_BUNDLE) %>
	</p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
<%


   for(int x=1; x<=NUMBER_OF_LNE_ITEM_DETAILS_FIELDS; x++) {
%>
     <tr>
       <td> 
        <p class="ControlLabel" align="center"> 
	     <%= x + NUMBER_OF_INV_SUMMARY_FIELDS + NUMBER_OF_INV_GOODS_FIELDS - buyerNameORID %>
	    <span class="AsterixText"></span>
	  </p>
      </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText"> 
<%
            options = invoiceDef.selectFieldInList( fieldItemDetailOptionList, 
					  	     invoiceDef.getAttribute("inv_line_item_order" + x) );
            out.println(InputField.createSelectField("inv_line_item_order" + x, "", 
                        defaultText, options, "ListText", isReadOnly));
%>
      </td>
      <td>&nbsp;</td>
    </tr>

<% }
}
%>
   </table>
  </td>
  </tr>
 </table>
