<%--
*******************************************************************************
             PO Upload Definition Detail - File Definition (Tab) Page

  Description:   This jsp simply displays the html for the file tab portion of
the PO Upload Definition.jsp.  This page is called from
PO UploadDefinition Detail.jsp  and is called by:
		<%@ include file="POUploadDefinitionDetail-File.jsp" %>
Note: the values stored in secureParms in this page are done deliberately to
avoid being overwritten as each tab is a submit action.  As a result, if this
data does not make it into the xml document, then it may be overwritten with a
null value in this tab or another.
*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%
	int x = 1;
	String toDisplay = "none";

	if (TradePortalConstants.INDICATOR_YES.equals(invoiceDef
			.getAttribute("line_item_detail_provided"))) {
		toDisplay = "";
	}
%>
<script language='JavaScript'>
	
	function setLineItemOption(){

		if(document.forms[0].line_item_detail_provided.checked == true){
			document.getElementById('lineItemDiv1').style.display  = '';
			document.getElementById('lineItemDiv2').style.display  = '';
		}
		else{
			document.getElementById('lineItemDiv1').style.display  = 'none';
			document.getElementById('lineItemDiv2').style.display  = 'none';
			}
		}
</script>
<div>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("line_item_detail_provided"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="line_item_detail_provided"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	<%=widgetFactory.createCheckboxField(
					"line_item_detail_provided",
					"InvoiceDefinition.LineItemDetaidNotReq",
					(TradePortalConstants.INDICATOR_YES.equals(invoiceDef
							.getAttribute("line_item_detail_provided"))),
					isReadOnly, false, "onClick=\"setLineItemOption()\"", "",
					"none")%>
</div>

<div id="lineItemDiv1" class = "columnLeft" style="display:<%=toDisplay%>">
	<%=widgetFactory.createLabel("",
					"INVUploadDefinitionDetail.DefSep1", false, false, false,
					"none")%>

  <table class="formDocumentsTable" id="poSysDefinitionLineTableId" style="width:98%;">
   <thead>
	<tr style="height:35px">
		<th colspan=4 style="text-align:left">
			<%=widgetFactory
					.createSubLabel("POStructureDefinition.SystemDefinedFields")%>
		</th>
	</tr>
   </thead>
   <tbody>
   <tr>
      	 <td nowrap width="15" >&nbsp;</td>
 		 <td nowrap width="130"  align="left" >
		  	<b><%=resMgr.getText("InvoiceDefinition.FieldName",
					TradePortalConstants.TEXT_BUNDLE)%></b>
 		  </td>
 		 <td  width="20" align="center" >
		  	<b><%=resMgr.getText("InvoiceDefinition.Size",
					TradePortalConstants.TEXT_BUNDLE)%></b>
 		 </td>
 		 <td width="30">
		  	<b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("InvoiceDefinition.DataReq",
					TradePortalConstants.TEXT_BUNDLE)%></b>
 		 </td>
      </tr>
   <tr>
      <td  align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("line_item_id_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="line_item_id_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
            <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.line_item_id",
					TradePortalConstants.TEXT_BUNDLE)%>" name="line_item_id" id="line_item_id">
			<%=widgetFactory.createCheckboxField(
					"line_item_id_req",
					"",
					invoiceDef.getAttribute("line_item_id_req").equals(
							TradePortalConstants.INDICATOR_YES), isReadOnly,
					false, "", "", "none")%>
	  </td>
	  <td >
			<%=widgetFactory
					.createLabel("", "InvoiceDefinition.LineItemNumber", false,
							false, false, "none")%>
	  </td>
	  <td align='right'>
	      		<%=widgetFactory.createLabel("", "70", false, false, false,
					"none")%>
	  </td>
	  <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("line_item_id_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="line_item_id_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
 <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.line_item_id",
					TradePortalConstants.TEXT_BUNDLE)%>" name="line_item_id" id="line_item_id">
 
              <%=widgetFactory.createCheckboxField(
					"line_item_id_data_req",
					"",
					invoiceDef.getAttribute("line_item_id_data_req").equals(
							TradePortalConstants.INDICATOR_YES), isReadOnly,
					false, "", "", "none")%>
       </td>
    </tr>
    <tr>
       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("unit_price_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="unit_price_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
           <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.unit_price",
					TradePortalConstants.TEXT_BUNDLE)%>" name="unit_price" id="unit_price">
		   <%=widgetFactory.createCheckboxField(
					"unit_price_req",
					"",
					invoiceDef.getAttribute("unit_price_req").equals(
							TradePortalConstants.INDICATOR_YES), isReadOnly,
					false, "", "", "none")%>
	   </td>
	    <td >
			<%=widgetFactory.createLabel("",
					"InvoiceDefinition.LineItemUnitPrice", false, false, false,
					"none")%>
	    </td>
	    <td align='right'>
	      		<%=widgetFactory.createLabel("", "15,3", false, false,
					false, "none")%>
	    </td>
	    <td align='center'>
 	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("unit_price_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="unit_price_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
              <%=widgetFactory.createCheckboxField(
					"unit_price_data_req",
					"",
					invoiceDef.getAttribute("unit_price_data_req").equals(
							TradePortalConstants.INDICATOR_YES), isReadOnly,
					false, "", "", "none")%>
       </td>
    </tr>
		<tr>
		
			<td align='center'>
 	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("unit_of_measure_price_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="unit_of_measure_price_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
			<input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.unit_of_measure_price",
					TradePortalConstants.TEXT_BUNDLE)%>" name="unit_of_measure_price" id="unit_of_measure_price">
			<%=widgetFactory.createCheckboxField(
					"unit_of_measure_price_req", "",
					invoiceDef.getAttribute("unit_of_measure_price_req")
							.equals(TradePortalConstants.INDICATOR_YES),
					isReadOnly, false, "", "", "none")%></td>
			<td><%=widgetFactory.createLabel("",
					"InvoiceDefinition.LineItemUnitMesrPrice", false, false,
					false, "none")%></td>
			<td align='right'><%=widgetFactory.createLabel("", "35", false, false, false,
					"none")%></td>
			<td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("unit_of_measure_price_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="unit_of_measure_price_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
			<%=widgetFactory.createCheckboxField(
					"unit_of_measure_price_data_req", "", invoiceDef
							.getAttribute("unit_of_measure_price_data_req")
							.equals(TradePortalConstants.INDICATOR_YES),
					isReadOnly, false, "", "", "none")%></td>
		</tr>
		<tr>
        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("inv_quantity_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="inv_quantity_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
            <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.inv_quantity",
					TradePortalConstants.TEXT_BUNDLE)%>" name="inv_quantity" id="inv_quantity">
			<%=widgetFactory.createCheckboxField(
					"inv_quantity_req",
					"",
					invoiceDef.getAttribute("inv_quantity_req").equals(
							TradePortalConstants.INDICATOR_YES), isReadOnly,
					false, "", "", "none")%>
	 	</td>
	    <td >
			<%=widgetFactory.createLabel("",
					"InvoiceDefinition.LineItemDetailInvQunt", false, false,
					false, "none")%>
	    </td>
	    <td  align='right'>
	        <%=widgetFactory.createLabel("", "13,5", false, false,
					false, "none")%>
	    </td>
	    <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("inv_quantity_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="inv_quantity_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
 	<input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.inv_quantity",
					TradePortalConstants.TEXT_BUNDLE)%>" name="inv_quantity" id="inv_quantity">
 
              <%=widgetFactory.createCheckboxField(
					"inv_quantity_data_req",
					"",
					invoiceDef.getAttribute("inv_quantity_data_req").equals(
							TradePortalConstants.INDICATOR_YES), isReadOnly,
					false, "", "", "none")%>
       </td>
    </tr>
    <tr>
       <td  align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("unit_of_measure_quantity_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="unit_of_measure_quantity_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
            <input type=hidden value="<%=resMgr.getText(
					"InvoiceUploadRequest.unit_of_measure_quantity",
					TradePortalConstants.TEXT_BUNDLE)%>" name="unit_of_measure_quantity" id="unit_of_measure_quantity">
			<%=widgetFactory.createCheckboxField(
					"unit_of_measure_quantity_req", "", invoiceDef
							.getAttribute("unit_of_measure_quantity_req")
							.equals(TradePortalConstants.INDICATOR_YES),
					isReadOnly, false, "", "", "none")%>
	 	</td>
	    <td >
			<%=widgetFactory.createLabel("",
					"InvoiceDefinition.LineItemDetailUnitMsrQunty", false,
					false, false, "none")%>
	    </td>
	    <td  align='right'>
	      		<%=widgetFactory.createLabel("", "35", false, false, false,
					"none")%>
	    </td>
	    <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("unit_of_measure_quantity_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="unit_of_measure_quantity_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
  	<input type=hidden value="<%=resMgr.getText(
					"InvoiceUploadRequest.unit_of_measure_quantity",
					TradePortalConstants.TEXT_BUNDLE)%>" name="unit_of_measure_quantity" id="unit_of_measure_quantity">
 
              <%=widgetFactory.createCheckboxField(
					"unit_of_measure_quantity_data_req", "", invoiceDef
							.getAttribute("unit_of_measure_quantity_data_req")
							.equals(TradePortalConstants.INDICATOR_YES),
					isReadOnly, false, "", "", "none")%>
       </td>
    </tr>
   </tbody>
  </table>
  <br><br><p>

 <table>
  <tr><td>
	&nbsp;
  </td></tr>
 </table>

 <table class="formDocumentsTable" id="InvUserDefinitionLineTableId" style="width:95%;">
 <thead>
	<tr style="height:35px">
		<th colspan="4" style="text-align:left;">
			<%=widgetFactory
					.createSubLabel("InvoiceUploadRequest.ProdCharUserDefinedFields")%>
		</th>
	</tr>
 </thead>
 <tbody>
      <tr>
      <td nowrap width="15" >&nbsp;</td>
 		 <td nowrap width="130"  >
		  	<b><%=resMgr.getText("InvoiceDefinition.FieldName",
					TradePortalConstants.TEXT_BUNDLE)%></b>
 		  </td>
 		 <td  width="20" align="center" >
		  	<b><%=resMgr.getText("InvoiceDefinition.Size",
					TradePortalConstants.TEXT_BUNDLE)%></b>
 		 </td>
 		 <td width="30">
		  	<b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("InvoiceDefinition.DataReq",
					TradePortalConstants.TEXT_BUNDLE)%></b>
 		 </td>
       </tr>

		<tr>
			<td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("prod_chars_ud1_type_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="prod_chars_ud1_type_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
			<%=widgetFactory.createCheckboxField(
					"prod_chars_ud1_type_req",
					"",
					invoiceDef.getAttribute("prod_chars_ud1_type_req").equals(
							TradePortalConstants.INDICATOR_YES), isReadOnly,
					false, "", "", "none")%></td>
			<td>
	 		  <% if (isReadOnly && StringFunction.isNotBlank(
								invoiceDef.getAttribute("prod_chars_ud1_type"))) { %>
								<%=widgetFactory.createTextField("prod_chars_ud1_type",
						"", invoiceDef.getAttribute("prod_chars_ud1_type"),
						"35", false, true, false, "width = '10' style='display:none;'", "", "none")%>
	 			 
	 		  <% } %>
			<%=widgetFactory.createTextField("prod_chars_ud1_type", "",
					invoiceDef.getAttribute("prod_chars_ud1_type"), "35",
					isReadOnly, false, false, "width = '10'", "", "none")%>
			</td>
			<td align="right"><%=widgetFactory.createLabel("", "35", false, false, false,
					"none")%>
			</td>
			<td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("prod_chars_ud1_type_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="prod_chars_ud1_type_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
			<%=widgetFactory.createCheckboxField(
					"prod_chars_ud1_type_data_req", "", invoiceDef
							.getAttribute("prod_chars_ud1_type_data_req")
							.equals(TradePortalConstants.INDICATOR_YES),
					isReadOnly, false, "", "", "none")%></td>
		</tr>

		<tr>
			<td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("prod_chars_ud2_type_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="prod_chars_ud2_type_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
			<%=widgetFactory.createCheckboxField(
					"prod_chars_ud2_type_req",
					"",
					invoiceDef.getAttribute("prod_chars_ud2_type_req").equals(
							TradePortalConstants.INDICATOR_YES), isReadOnly,
					false, "", "", "none")%></td>
			<td>
	 		  <% if (isReadOnly && StringFunction.isNotBlank(
								invoiceDef.getAttribute("prod_chars_ud2_type"))) { %>
								<%=widgetFactory.createTextField("prod_chars_ud2_type",
						"", invoiceDef.getAttribute("prod_chars_ud2_type"),
						"35", false, true, false, "width = '10' style='display:none;'", "", "none")%>
	 			 
	 		  <% } %>
			<%=widgetFactory.createTextField("prod_chars_ud2_type", "",
					invoiceDef.getAttribute("prod_chars_ud2_type"), "35",
					isReadOnly, false, false, "width = '10'", "", "none")%></td>
			<td align="right"><%=widgetFactory.createLabel("", "35", false, false, false,
					"none")%></td>
			<td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("prod_chars_ud2_type_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="prod_chars_ud2_type_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
			<%=widgetFactory.createCheckboxField(
					"prod_chars_ud2_type_data_req", "", invoiceDef
							.getAttribute("prod_chars_ud2_type_data_req")
							.equals(TradePortalConstants.INDICATOR_YES),
					isReadOnly, false, "", "", "none")%></td>
		</tr>

		<tr>
			<td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("prod_chars_ud3_type_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="prod_chars_ud3_type_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
			<%=widgetFactory.createCheckboxField(
					"prod_chars_ud3_type_req",
					"",
					invoiceDef.getAttribute("prod_chars_ud3_type_req").equals(
							TradePortalConstants.INDICATOR_YES), isReadOnly,
					false, "", "", "none")%></td>
			<td>
	 		  <% if (isReadOnly && StringFunction.isNotBlank(
								invoiceDef.getAttribute("prod_chars_ud3_type"))) { %>
								<%=widgetFactory.createTextField("prod_chars_ud3_type",
						"", invoiceDef.getAttribute("prod_chars_ud3_type"),
						"35", false, true, false, "width = '10' style='display:none;'", "", "none")%>
	 			 
	 		  <% } %>
			<%=widgetFactory.createTextField("prod_chars_ud3_type", "",
					invoiceDef.getAttribute("prod_chars_ud3_type"), "35",
					isReadOnly, true, false, "width = '10'", "", "none")%></td>
			<td align="right"><%=widgetFactory.createLabel("", "35", false, false, false,
					"none")%></td>
			<td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("prod_chars_ud3_type_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="prod_chars_ud3_type_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
			<%=widgetFactory.createCheckboxField(
					"prod_chars_ud3_type_data_req", "", invoiceDef
							.getAttribute("prod_chars_ud3_type_data_req")
							.equals(TradePortalConstants.INDICATOR_YES),
					isReadOnly, false, "", "", "none")%></td>
		</tr>
		<%
			int counterLineTmp = 4;
			String currentDefinedLineLabDataReq = "";
			String currentDefinedLineDataReq = "";
			while (counterLineTmp <= 7) {
				currentDefinedLab = "prod_chars_ud"
						+ (new Integer(counterLineTmp)).toString() + "_type";
				currentDefined = invoiceDef.getAttribute(currentDefinedLab);
				if (InstrumentServices.isBlank(currentDefined)) {
					break;
				}
				currentDefinedLabReq = "prod_chars_ud"
						+ (new Integer(counterLineTmp)).toString()
						+ "_type_req";
				currentDefinedReq = invoiceDef
						.getAttribute(currentDefinedLabReq);
				currentDefinedLabUpdt = currentDefinedLabReq + "Updt";
				currentDefinedLineLabDataReq = "prod_chars_ud"
						+ (new Integer(counterLineTmp)).toString()
						+ "_type_data_req";
				currentDefinedLineDataReq = invoiceDef
						.getAttribute(currentDefinedLineLabDataReq);
		%>
 	  <tr>
 	      <td align="center">
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								currentDefinedReq)) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="<%=currentDefinedLabReq%>"  value="Y"  checked style="display:none;" />
	 		  <% } %>
 	      
              <%=widgetFactory.createCheckboxField(
						currentDefinedLabReq, "",
						TradePortalConstants.INDICATOR_YES
								.equals(currentDefinedReq), isReadOnly, false,
						"", "", "none")%>
              </td>
              <td>
	 		  <% if (isReadOnly && StringFunction.isNotBlank(
								currentDefined)) { %>
								<%=widgetFactory.createTextField(currentDefinedLab,
						"", currentDefined,
						"35", false, true, false, "width = '10' style='display:none;'", "", "none")%>
	 			 
	 		  <% } %>
		<%=widgetFactory.createTextField(currentDefinedLab, "",
						currentDefined, "35", isReadOnly, true, false,
						"class = 'char20'", "", "none")%>
 		  </td>
 		  <td align="right">
			<%=widgetFactory.createLabel("", "35", false, false,
						false, "none")%>
 		  </td>
 		  <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								currentDefinedLineDataReq)) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="<%=currentDefinedLineLabDataReq%>"  value="Y"  checked style="display:none;" />
	 		  <% } %>
 	      
 
              <%=widgetFactory.createCheckboxField(
						currentDefinedLineLabDataReq, "",
						TradePortalConstants.INDICATOR_YES
								.equals(currentDefinedLineDataReq), isReadOnly,
						false, "", "", "none")%>
       </td>
 	  </tr>
<%
	counterLineTmp++;
	}
%>

 </tbody>
 </table>
 <%
 	if (!(isReadOnly)) {
 %>
 	<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreLineDefined">
 	<%=resMgr.getText("common.Add4MoreText",TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	add4MoreDefined('InvUserDefinitionLineTableId');
	</script>
	</button>
	<%=widgetFactory.createHoverHelp("add4MoreLineDefined","POStructure.Add4More") %>
<%
	} else {
%>
					&nbsp;
<%
	}
%>
</div>

<div id="lineItemDiv2" class = "columnRight" style="display:<%=toDisplay%>">
    <%=widgetFactory.createLabel("",
					"INVUploadDefinitionDetail.DefSep2", false, false, false,
					"none")%>

 <table id="poStructSumLineItemOrderTableId" class="formDocumentsTable">
   <thead>
	<tr style="height:35px">
		<th colspan=2 style="text-align:left">
			<%=widgetFactory
					.createSubLabel("INVUploadDefinitionDetail.InvLineItemOrder")%>
		</th>
	</tr>
	</thead>
	<tbody>
	<tr>      
 		 <td width="20">
		  	<%=widgetFactory.createLabel("",
					"POStructureDefinition.Order", false, false, false, "")%>
 		  </td>
 		  <td width="180">
		  	<%=widgetFactory.createLabel("",
					"POStructureDefinition.FieldName", false, false, false, "")%>
 		  </td>
    </tr>
<%
	invSumValue = null;
	invSumField = null;
	invSumReq = TradePortalConstants.INDICATOR_YES;
	reStartIndex = 1;

	while ((reStartIndex <= 12)
			&& InstrumentServices.isNotBlank(invoiceDef
					.getAttribute("inv_line_item_order"
							+ (new Integer(reStartIndex)).toString()))) {

		invSumField = invoiceDef.getAttribute("inv_line_item_order"
				+ (new Integer(reStartIndex)).toString());
		invSumValue = invoiceDef.getAttribute(invSumField);
		if ((InstrumentServices.isBlank(invSumValue))
				&& (InstrumentServices.isNotBlank(invSumField)))
			invSumValue = "InvoiceUploadRequest." + invSumField;

		if (InstrumentServices.isNotBlank(invSumValue)) {
%>
 	  <tr id= "<%=invSumField%>_req_order"> 
 		 <td align="center">
 		     <input type=hidden id="upload_userDefLine_order_<%=reStartIndex%>"
 		        name="inv_line_item_order<%=reStartIndex%>"
 		        value="<%=invSumField%>">
   			 <input type="text" name="" value='<%=counterTmp%>' data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
   				id='userDefLineText<%=reStartIndex%>'  style="width: 25px;" '<%=isReadOnly%>'>
 		  </td>
 		  <td nowrap id='userDefLineFileFieldName<%=reStartIndex%>' align="left">
		  	<%=widgetFactory.createLabel("", invSumValue, false,
							false, false,"labelClass=\"invOrderLabel\"", "none")%>
		  	<span class="deleteSelectedItem" style="margin:0.3em 1em" id='<%=invSumField%>'></span>
 		  </td>
 	  </tr>
<%
	counterTmp++;
		}
		reStartIndex++;
	}
%>
</tbody>
</table>
<div>
 <input type=hidden name=invSumFieldMax value=<%=counterTmp%>>
         <input type=hidden name=allFields     value=<%=allFields%>>
<%
	if (!(isReadOnly)) {
%>
<button data-dojo-type="dijit.form.Button" type="button" id="updateUserLineOrder"><%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        local.updateSumLineItemOrder(document.getElementById("poStructSumLineItemOrderTableId").rows.length , (document.getElementById("poStructSumDataOrderTableId").rows.length - 2) +
                              (document.getElementById("InvDefFileOrderTableId").rows.length - 2) + (document.getElementById("discountCodeDataOrderTableId").rows.length - 2),  'sumDataPos');
	</script>
</button>
<%
	} else {
%>
					&nbsp;
<%
	}
%>
</div>

</div>
