<%-- Srinivasu_D CR-599 Rel8.4 11/21/2013 - Added  --%>
<%	
	//jgadela 02/25/2013 R8.4 IR T36000026580 - Modified the logic to show 'Payment Beneficiary Details Default' by default.
	if(TradePortalConstants.PAYMENT_MGMT_FILE_DEF.equals(paymentFileFormat)){
%>

<table  width="100%" bgcolor=''  ><tr><td>
 <table bgcolor=''  width="100%" ><tr><td>
          <table id="panel2" class="formDocumentsTable" width="50%" border="1" bgcolor='' cellspacing="0" cellpadding="0">
			<thead>
			  <tr>
				<th align="text-align: center;"><%=resMgr.getText("OrgProfileDetail.PaymentFileDefHeader2", TradePortalConstants.TEXT_BUNDLE)%></th>
				
			  </tr>
			</thead>
			<tbody>
		</table>
		
	  <table width="75%" border="0" bgcolor='' cellspacing="0" cellpadding="0" class="formPaymentDocumentsTable" id="paymentAliasTable">		
			
			
		<%	
						
			//String fileFormat ="<option value='100001'>ABA File Format</option><option value='100002'>PNG File Format</option>";
			
			String  payDef = "";
		    int aliasIndex = 0;
			UserPreferencesPayDefWebBean paymentAlias = null;
			paymentAlias = (UserPreferencesPayDefWebBean) aliasList.get(aliasIndex);
		
		%>
			
			  <tr id="aliasIndex<%=aliasIndex%>">		
			   <td align="center" class="paymentCol1"><%=aliasIndex+1%>.</td>

				 <td align="center"><%=resMgr.getText("OrgProfileDetail.PaymentFileDefLabel",	TradePortalConstants.TEXT_BUNDLE)%></td>
				</td>
				<td align="center" bgcolor=''>
			<%
				String paymentDef0 = paymentAlias.getAttribute("a_pay_upload_definition_oid");
			    String payDefOid   = paymentAlias.getAttribute("user_pref_pay_def_oid");
				
				if (paymentDef0 == null) {
					paymentDef0 = "";
				}
				  payDef = ListBox.createOptionList(fullPayDefDetails, "PAY_UPLOAD_DEFINITION_OID", "DESCR", paymentDef0);				
				
				String pagOptions1 = payDef;
				
				out.println(widgetFactory.createSelectField( "PaymentDef"+(aliasIndex), "", " ", pagOptions1, isReadOnly, false, false, "style='width:160px'", "", "none"));

				String paymentAliasOid1 = "";
				
				if (InstrumentServices.isNotBlank(paymentAlias.getAttribute("user_pref_pay_def_oid"))) {
				  paymentAliasOid1 = EncryptDecrypt.encryptStringUsingTripleDes(paymentAlias.getAttribute("user_pref_pay_def_oid"), userSession.getSecretKey());
				}
				out.print("<INPUT TYPE=HIDDEN NAME='paymentAliasOid" + (aliasIndex) + "' VALUE='" +paymentAliasOid1+ "'>");

				
			%>	<%=widgetFactory.createHoverHelp("PaymentDef"+(aliasIndex),"MyOrgProfile.PayDefSelect") %>
				</td>
				<%	//payDefCheckboxIndicator = TradePortalConstants.INDICATOR_YES;
					
					payDefCheckboxIndicator = paymentAlias.getAttribute("pmt_file_default_ind");
					
					
					  if (TradePortalConstants.INDICATOR_YES.equals(payDefCheckboxIndicator)) {
							payDefCheckBoxValue = true;
					  }
					else if (TradePortalConstants.INDICATOR_NO.equals(payDefCheckboxIndicator) || "".equals(payDefCheckBoxValue)) {
						    payDefCheckBoxValue = false;
						 
					}
						
				%>
				<td width="60px" align='left' bgcolor=''>
				<%=widgetFactory.createCheckboxField("DefaultInd","OrgProfileDetail.PaymentFileDefDefault", payDefCheckBoxValue, isReadOnly, false,"","","none")%>
				
				</td>
			 </tr>	 			

			<%	
				
				
			  for ( int idx = 1; idx<aliasList.size(); idx++) {
				 
	
				 paymentAlias = (UserPreferencesPayDefWebBean) aliasList.get(idx);
			%>
			
			<tr id="aliasIndex<%=(aliasIndex+idx)%>">	
			 <td align="center" class="paymentCol1"><%=aliasIndex+idx+1%>.</td>
				 <td align="center"><%=resMgr.getText("OrgProfileDetail.PaymentFileDefLabel",	TradePortalConstants.TEXT_BUNDLE)%></td>
				</td>
				<td align="center">
			<%   
				 paymentDef0 = paymentAlias.getAttribute("a_pay_upload_definition_oid");				 
				 
				if (paymentDef0 == null)  {
					paymentDef0 = "";
				}
				  payDef = ListBox.createOptionList(fullPayDefDetails, "PAY_UPLOAD_DEFINITION_OID", "DESCR", paymentDef0);
				  pagOptions1 = payDef;

				  out.println(widgetFactory.createSelectField( "PaymentDef"+(aliasIndex+idx), "", " ", pagOptions1, isReadOnly, false, false, "style='width:160px'", "", "none"));

				 paymentAliasOid1 = "";
				if (InstrumentServices.isNotBlank(paymentAlias.getAttribute("user_pref_pay_def_oid"))) {
				  paymentAliasOid1 = EncryptDecrypt.encryptStringUsingTripleDes(paymentAlias.getAttribute("user_pref_pay_def_oid"), userSession.getSecretKey());
				}
				

				out.print("<INPUT TYPE=HIDDEN NAME='paymentAliasOid" + (aliasIndex+idx) + "' VALUE='" +paymentAliasOid1+ "'>");

				
			%>	
				</td>
				<td width="70px" align='left' bgcolor=''></td>
				 </tr>
			 
			<%
			  }
			%>				
				
	   		</table>	
				</td>
<%}%>	
	<td valign='top' >
<%
//jgadela 06/05/2014 R8.4 IR T36000028464 - Modified the logic to show 'Payment Beneficiary Details Default' by default.
if(TradePortalConstants.PAYMENT_MGMT_FILE_DEF.equals(paymentFileFormat) ||
				TradePortalConstants.PAYMENT_MGMT_PILE_FIXED_FORMAT.equals(paymentFileFormat)){ %>
				
		
	<%-- DK CR-886 Rel8.4 10/15/2013 start --%> 
			<%= widgetFactory.createLabel("", "OrgProfileDetail.PmtBeneView", false, false, false, "") %>
				<div class="formItemWithIndent1">
			<%=widgetFactory.createRadioButtonField("PaymentBeneSort","TradePortalConstants.PMT_BENE_ACCT_NUM",
						"OrgProfileDetail.PmtBeneAcctNum", TradePortalConstants.PMT_BENE_ACCT_NUM,
						pmtBeneSort.equals(TradePortalConstants.PMT_BENE_ACCT_NUM),isReadOnly, "", "") %>	
			<br>
			<%=widgetFactory.createRadioButtonField("PaymentBeneSort","TradePortalConstants.PMT_BENE_ORDER",
						"OrgProfileDetail.PmtBeneOrder", TradePortalConstants.PMT_BENE_ORDER,
						pmtBeneSort.equals(TradePortalConstants.PMT_BENE_ORDER),isReadOnly, "", "") %>	
						</div>
				<%-- DK CR-886 Rel8.4 10/15/2013 ends --%>
		
<%}%>

		<%--Rel9.5 CR 927B --%>
		<%String langOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.INSTRUMENT_LANGUAGE, corporateOrg.getAttribute("email_language"), loginLocale);%> 
    	<%= widgetFactory.createSelectField( "EmailLanguage", "CorpCust.SendEmailIn", "", langOptions, isReadOnly)%>
  		</td>
  					

<%	
	//jgadela 02/25/2013 R8.4 IR T36000026580 - Modified the logic to show 'Payment Beneficiary Details Default' by default.
	if(TradePortalConstants.PAYMENT_MGMT_FILE_DEF.equals(paymentFileFormat)){
%>		
		
		
		</tr>
		</table>
		 </td>
		 </tr>
		</table>
	
		<%
		  if (!isReadOnly) {
		%>
		  <div>
			<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreAlias">
			<%=resMgr.getText("OrgProfileDetail.PaymentFileDefButton1",TradePortalConstants.TEXT_BUNDLE)%>
			  <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				add4MoreAlias();
			  </script>
			</button>
		  </div>
		<%
		  }
	%>  		
<%}%>			