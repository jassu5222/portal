<%--
*******************************************************************************
       Notification Rule Detail - BILLING (Tab) Page
  
Description:    This jsp simply displays the html for the other 
		portion of the NotificationRuleDetail.jsp.  This page is called
		from NotificationRuleDetail.jsp  and is called by:

		<%@ include file="NotificationRuleDetail-Billing.jsp" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>

<%
String[] transaction7Types	= {TradePortalConstants.ACCOUNTS_RECEIVABLE_UPDATE, 
				               TradePortalConstants.ADJUSTMENT,
				               TransactionType.CHANGEBILL, 
    			               TradePortalConstants.COLLECTBILL, 
    			               TransactionType.PCS,
                               TransactionType.PCE,
				               TransactionType.DEACTIVATE,
				               TradePortalConstants.NEWBILL,
				               TransactionType.REACTIVATE,
				               TransactionType.REDUCE, 
				               TransactionType.REVOLVE
			                   };

String[] criterion7Types		= {TradePortalConstants.NOTIF_RULE_NOTIFICATION, TradePortalConstants.NOTIF_RULE_EMAIL};

secureParms.put("name", notificationRule.getAttribute("name"));

%>  
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th colspan="3"><%= resMgr.getText("NotificationRuleDetail.Notification", TradePortalConstants.TEXT_BUNDLE)%></th>
		    <th colspan="3"><%= resMgr.getText("NotificationRuleDetail.Email", TradePortalConstants.TEXT_BUNDLE)%></th>	
	   </tr>
	   
		 <tr>
		    <th>&nbsp;</th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.ChargesDocuments")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.ChargesDocuments")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>
		 </tr>
	 </thead>
	 <tbody>
	 <% 
	 for (int transCount=0; transCount<transaction7Types.length; transCount++) 
	 { 
	   String[] criterionRuleOid		= new String[2];
	   String[] criterionRuleSetting 	= {TradePortalConstants.NOTIF_RULE_NONE, TradePortalConstants.NOTIF_RULE_NONE};
	   String transactionTypeLabel           = transaction7Types[transCount];

	   if (transaction7Types[transCount].equals(TransactionType.ISSUE))
	     transactionTypeLabel = TransactionType.ISSUE + TransactionType.RELEASE;
  	%>
    <tr>
    	<td align="left">
    	<%=widgetFactory.createSubLabel("NotificationRuleDetail.TransactionType" + transactionTypeLabel)%> 	
    	</td>
<%

  // If the notification rule specifies that only daily emails are sent then do not display
  // email criterion settings. Daily emails are driven by messages/notifications already 
  // present in the portal, not the notification rule's email settings
  int criterionTypeCount = 2;
 
  for (int criterionCount=0; criterionCount<criterionTypeCount; criterionCount++)
  {
    String key;
    key = TradePortalConstants.BILLING;
    key = key.concat(transaction7Types[transCount]);
    key = key.concat(criterion7Types[criterionCount]);

    NotifyRuleCriterionWebBean notifyRuleCriterion = null;
    notifyRuleCriterion = (NotifyRuleCriterionWebBean)criterionRuleList.get(key);
     
    if (notifyRuleCriterion != null)
    {
      criterionRuleOid[criterionCount] = notifyRuleCriterion.getAttribute("criterion_oid");
      criterionRuleSetting[criterionCount] = notifyRuleCriterion.getAttribute("setting");
    }
    String criterionRuleType = criterion7Types[criterionCount];
    criterionRuleCount++;

%>

    <jsp:include page="/refdata/NotificationRuleDetailRow.jsp">
      <jsp:param name="transactionTypes" 	value='<%=transaction7Types[transCount]%>' />    
      <jsp:param name="instrCategory" 		value='<%=TradePortalConstants.BILLING%>' />
      <jsp:param name="criterionRuleOid"	value='<%=criterionRuleOid[criterionCount]%>' />
      <jsp:param name="criterionRuleSetting" 	value='<%=criterionRuleSetting[criterionCount]%>' />
      <jsp:param name="criterionRuleType" 	value='<%=criterionRuleType%>' />
      <jsp:param name="criterionRuleCount" 	value='<%=criterionRuleCount%>' />	
      <jsp:param name="isReadOnly" 		value='<%=isReadOnly%>' />
    </jsp:include>
        
<%
  }
}
%>
</tbody>
</table>