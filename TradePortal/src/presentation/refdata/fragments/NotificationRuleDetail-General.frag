<%--
*******************************************************************************
       Notification Rule Detail - General (Tab) Page
  
Description:   This jsp simply displays the html for the general portion of 
               the NotificationRuleDetail.jsp.  This page is called from 
               NotificationRuleDetail.jsp  and is called by:

	       <%@ include file="NotificationRuleDetail-General.jsp" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>
           <%--IR 3009 26/07/2012 by dillip --%>
  
         <%= widgetFactory.createTextField( "name", "NotificationRuleDetail.RuleName", notificationRule.getAttribute("name"), "35", isReadOnly, true, false, "", "", "" ) %>                               
        
         <%--UAT IR 6252 size of description field to be 25 - Pavani --%>
          <%= widgetFactory.createTextField( "description", "NotificationRuleDetail.Description", notificationRule.getAttribute("description"), "25", isReadOnly, true, false, "class='char35'", "", "" ) %>
          <%--Ended Here IR#6252 --%>  
           <%--Ended Here IR#3009 --%>