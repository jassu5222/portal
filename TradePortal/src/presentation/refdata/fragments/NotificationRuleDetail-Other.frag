<%--
*******************************************************************************
       Notification Rule Detail - Other (Tab) Page
  
Description:    This jsp simply displays the html for the other 
		portion of the NotificationRuleDetail.jsp.  This page is called
		from NotificationRuleDetail.jsp  and is called by:

		<%@ include file="NotificationRuleDetail-Other.jsp" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>

<%
//<Papia Dastidar IR#BKUF080335338 & BIUF080336121 25thAug 2005>
//Modified the constant name in TradePortalConstants.java from ACCOUNTS_REC to 
//ACCOUNTS_RECEIVABLE_UPDATE  as per Weian's suggestion
String[] transaction1Types	= {TradePortalConstants.ACCOUNTS_RECEIVABLE_UPDATE, 
				   TradePortalConstants.ADJUSTMENT,
				   TransactionType.AMEND, 
    				   TransactionType.BUY, 
                                   TransactionType.CHANGE, 
				   TransactionType.CREATE_USANCE,
				   TransactionType.DEACTIVATE,
				   TransactionType.EXPIRE,
				   TransactionType.ISSUE, 
				   TransactionType.LIQUIDATE,
			           TransactionType.PAYMENT,
				   TransactionType.REACTIVATE};

String[] criterion1Types		= {TradePortalConstants.NOTIF_RULE_NOTIFICATION, TradePortalConstants.NOTIF_RULE_EMAIL};

secureParms.put("name", notificationRule.getAttribute("name"));

%>  
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th colspan="3"><%= resMgr.getText("NotificationRuleDetail.Notification", TradePortalConstants.TEXT_BUNDLE)%></th>
			<%
      		/* if (!emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY) && !emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL))
      		{ */
			%>
			<th colspan="3"><%= resMgr.getText("NotificationRuleDetail.Email", TradePortalConstants.TEXT_BUNDLE)%></th>	
			<%
      		/* }  */    
			%>		
		 </tr>
		 <tr>
		    <th>&nbsp;</th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.ChargesDocuments")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.ChargesDocuments")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>
		 </tr>
	 </thead>
	 <tbody>
	 <% 
	 for (int transCount=0; transCount<transaction1Types.length; transCount++) 
	 { 
	   String[] criterionRuleOid		= new String[2];
	   String[] criterionRuleSetting 	= {TradePortalConstants.NOTIF_RULE_ALWAYS, TradePortalConstants.NOTIF_RULE_NONE};
	   String transactionTypeLabel           = transaction1Types[transCount];

	   if (transaction1Types[transCount].equals(TransactionType.ISSUE))
	     transactionTypeLabel = TransactionType.ISSUE + TransactionType.RELEASE;
  	%>
    <tr>
    	<td align="left">
    	<%=widgetFactory.createSubLabel("NotificationRuleDetail.TransactionType" + transactionTypeLabel)%> 	
    	</td>
<%

  // If the notification rule specifies that only daily emails are sent then do not display
  // email criterion settings. Daily emails are driven by messages/notifications already 
  // present in the portal, not the notification rule's email settings
  int criterionTypeCount = 2;
  /* if (emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY) || emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL))
  {
    criterionTypeCount = 1;
  } */

	
  for (int criterionCount=0; criterionCount<criterionTypeCount; criterionCount++)
  {
    String key;
    key = TradePortalConstants.OTHER;
    key = key.concat(transaction1Types[transCount]);
    key = key.concat(criterion1Types[criterionCount]);

    NotifyRuleCriterionWebBean notifyRuleCriterion = null;
    notifyRuleCriterion = (NotifyRuleCriterionWebBean)criterionRuleList.get(key);
     
    if (notifyRuleCriterion != null)
    {
      criterionRuleOid[criterionCount] = notifyRuleCriterion.getAttribute("criterion_oid");
      criterionRuleSetting[criterionCount] = notifyRuleCriterion.getAttribute("setting");
    }
    String criterionRuleType = criterion1Types[criterionCount];
    criterionRuleCount++;

%>

    <jsp:include page="/refdata/NotificationRuleDetailRow.jsp">
      <jsp:param name="transactionTypes" 	value='<%=transaction1Types[transCount]%>' />    
      <jsp:param name="instrCategory" 		value='<%=TradePortalConstants.OTHER%>' />
      <jsp:param name="criterionRuleOid"	value='<%=criterionRuleOid[criterionCount]%>' />
      <jsp:param name="criterionRuleSetting" 	value='<%=criterionRuleSetting[criterionCount]%>' />
      <jsp:param name="criterionRuleType" 	value='<%=criterionRuleType%>' />
      <jsp:param name="criterionRuleCount" 	value='<%=criterionRuleCount%>' />	
      <jsp:param name="isReadOnly" 		value='<%=isReadOnly%>' />
    </jsp:include>
        
<%
  }
}
%>
</tbody>
</table>