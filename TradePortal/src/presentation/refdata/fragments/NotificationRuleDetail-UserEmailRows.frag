<%--this fragment is included in  
    CorporateCustomerSection10 fragment for adding multiple Panel Groups to an Account
Added for CR 821 - Rel 8.3
  Passed variables:
    notifEmailIndex - an integer value, zero based of the 1st 
    rowIndex	
    isReadOnly
    
--%>
<%
  for (int idx = 0; idx<DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT; idx++) {
%>

  <tr id="<%=StringFunction.xssCharsToHtml(tableRowIdPrefix)%>_<%=rowIndex%>">
    <td align="left" width="15%">
    <%
    	defaultSelection = "";
	    if(idx < defUserIdArray.length && uidx < defUserIdArray.length){
			  if(null != defUserIdArray[uidx]){
				  defaultSelection = defUserIdArray[uidx];
				  uidx++;
			  }
		}
		String userEmailsOptionsDisplay = "<option  value=\"\"></option>";
		userEmailsOptionsDisplay += ListBox.createOptionList(notifyRuleUsersList, "USER_OID", "USER_IDENTIFIER", defaultSelection, null);
     	out.println(widgetFactory.createSelectField(StringFunction.xssCharsToHtml(notifyEmail)+notifEmailIndex, "", "", userEmailsOptionsDisplay, isReadOnly, false, false, " required=\"false\" class=\"char9\" onChange=\"local.prepareUserIds("+sectionNum+",'"+sectionCategory+"')\"" , "", "none"));
	    notifEmailIndex++; 
    %>
    </td>
    <td align="left" width="15%">
    <%
    	defaultSelection = "";
	    if(idx < defUserIdArray.length && uidx < defUserIdArray.length){
			  if(null != defUserIdArray[uidx]){
				  defaultSelection = defUserIdArray[uidx];
				  uidx++;
			  }
		}
		userEmailsOptionsDisplay = "<option  value=\"\"></option>";
     	userEmailsOptionsDisplay += ListBox.createOptionList(notifyRuleUsersList, "USER_OID", "USER_IDENTIFIER", defaultSelection, null);
		out.println(widgetFactory.createSelectField(StringFunction.xssCharsToHtml(notifyEmail)+notifEmailIndex, "", "", userEmailsOptionsDisplay, isReadOnly, false, false, " required=\"false\" class=\"char9\" onChange=\"local.prepareUserIds("+sectionNum+",'"+sectionCategory+"')\"" ,"", "none"));
		notifEmailIndex++;
	%>
    </td>
  </tr>
<%
rowIndex++;
  }
%>

