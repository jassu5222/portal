<%--
*******************************************************************************
       Notification Rule Detail - SupplierPortalInvoices setting section
  
Description:    This jsp simply displays the html for the SupplierPortalInvoices setting 
		of the NotificationRuleDetail.jsp.  This page is called
		from NotificationRuleDetail.jsp  and is called by:

		<%@ include file="NotificationRuleDetail-SupplierPortalInvoicesSection.frag" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>
<%--Supplier Portal Invoices sections begins here --%>
<%
			String defApplytoAllTransSPInvoices = "";
			String defClearAllTransSPInvoices ="";
			String defSendEmailSettingSPInvoices = "";
			transArray = instrTransArrays[32];
			String supValue = "";
    		for(int tLoop = 0; tLoop < transArray.length; tLoop++){
    			String key;
   			    key = instrumentCatagory[32]+"_"+transArray[tLoop];

   			    NotificationRuleCriterionWebBean notifyRuleCriterion = null;
   			    notifyRuleCriterion = (NotificationRuleCriterionWebBean)criterionRuleTransList.get(key);
   			    yy++;
   			 	if (notifyRuleCriterion != null) {
					defSendEmailSettingSPInvoices = notifyRuleCriterion.getAttribute("send_email_setting");
					defNotifEmailFerquency = notifyRuleCriterion.getAttribute("notify_email_freq");
					supValue = "Y";
   		      		//load using OID
 					out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + notifyRuleCriterion.getAttribute("instrument_type_or_category") + "'>");
					out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + notifyRuleCriterion.getAttribute("transaction_type_or_action") + "'>");
	   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='" + notifyRuleCriterion.getAttribute("criterion_oid") + "'>");
   		      		out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value='" + notifyRuleCriterion.getAttribute("send_email_setting") + "'>");
   		      		out.print("<input type=hidden name='notify_email_freq"+yy+"' id='notify_email_freq"+yy+"' value='" + notifyRuleCriterion.getAttribute("notify_email_freq") + "'>");
   		      		
	   		      	} else {//if it is a new form
	   		    	out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + instrumentCatagory[32] + "'>");
					out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + transArray[tLoop] + "'>");
	   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='0'>");		   		     	
	   		      	out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value=''>");
	   		      	out.print("<input type=hidden name='notify_email_freq"+yy+"' id='notify_email_freq"+yy+"' value=''>");
	   		     
	   		      	}
			}//ent tLoop

			if (getDataFromDoc){
				defSendEmailSettingSPInvoices = "";
				defNotifEmailFerquency = "";
				defApplytoAllTransSPInvoices = "";
				defClearAllTransSPInvoices = "";
				supValue = "Y";
				DocumentHandler notifyRuleDefInstrDoc = (DocumentHandler) defListVector.elementAt(32);
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/send_email_setting"))){
				defSendEmailSettingSPInvoices = notifyRuleDefInstrDoc.getAttribute("/send_email_setting");
				}
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/notify_email_freq"))){
				defNotifEmailFerquency = notifyRuleDefInstrDoc.getAttribute("/notify_email_freq");
				}
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran"))){
				defApplytoAllTransSPInvoices = notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran");
				}
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran"))){
				defClearAllTransSPInvoices = notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran");
				}
				
			}
			%>

<%=widgetFactory.createSectionHeader("34", "NotificationRuleDetail.SupplierInvoicesSection", null, true) %> 
	<%=widgetFactory.createNote("NotificationRuleDetail.SupplierPortalInvoicesNote","SupplierPortalInvoicesNote1")%>      
		<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.SupplierPortalInvoicesNote2") %>	
	<table width="100%">
		<tr>
			<td width="35%">
				<%=widgetFactory.createSubLabel("NotificationRuleDetail.ReceiveEmail") %>
			</td>
			<td width="8%">
				&nbsp;
			</td>
			<td width="11.5%">
			
			</td>
			<td width="38%">
			
			</td>
		</tr>
		<tr>
			<td width="35%" style="vertical-align: top;">
				<%=widgetFactory.createRadioButtonField("emailSetting33", "emailSettingYes33", "NotificationRuleDetail.Yes", TradePortalConstants.INDICATOR_YES, 
						TradePortalConstants.INDICATOR_YES.equals(defSendEmailSettingSPInvoices), isReadOnly,  "onClick=\"local.setSupValue()\"", "")%>
				<br>
				<%=widgetFactory.createRadioButtonField("emailSetting33", "emailSettingNo33", "NotificationRuleDetail.No", TradePortalConstants.INDICATOR_NO, 
						TradePortalConstants.INDICATOR_NO.equals(defSendEmailSettingSPInvoices), isReadOnly,  "onClick=\"local.setSupValue()\"", "")%>
				<br><br>
				<%=widgetFactory.createLabel("", "NotificationRuleDetail.EmailInterval", false, false, false, "inline") %>
				<%= widgetFactory.createTextField("notifyEmailInterval33", "", defNotifEmailFerquency, "5", 
						isReadOnly,false,false,"","regExp:'^[1-9][0-9]*0$',invalidMessage:'"+resMgr.getText("NotificationRuleDetail.InvalidNotificationEmailFreqValue",TradePortalConstants.TEXT_BUNDLE)+"'","none") %>								
			</td>
			<td width="8%" style="vertical-align: top;" bgcolor=''>
				&nbsp;
			</td>
			<td width="11.5%" style="vertical-align: top;">
			
			</td>
			<td width="38%" align='left' style="vertical-align: top;">
		
				<button data-dojo-type="dijit.form.Button"  name="ApplytoAllTransactions33" id="ApplytoAllTransactions33" type="button">
					<%=resMgr.getText("NotificationRuleDetail.Apply.Button",TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						local.applyToAllOtherTransactions(33, <%=instrTransCriteriaFieldStartCount[32]%>);
					</script>
				</button>
				<br>
				<button data-dojo-type="dijit.form.Button"  name="ClearAll33" id="ClearAll33" type="button">
					<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						local.clearAllOtherSection(33, <%=instrTransCriteriaFieldStartCount[32]%>);
					</script>
				</button>
			</td>
			<input type="hidden" name="instrumentType33" id="instrumentType33" value="LRQ_INV">
			<input type="hidden" name="applyInst33" id="applyInst33" value="<%=defApplytoAllTransSPInvoices%>">
			<input type="hidden" name="clearAll33" id="clearAll33" value="<%=defClearAllTransSPInvoices%>">
			<input type="hidden" name="supplierReadOnly" id="supplierReadOnly" value="<%=supValue%>">
		</tr>	
	</table> 
	
</div><%--Supplier Portal Invoices sections Ends here --%>