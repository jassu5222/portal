<%--
*******************************************************************************
       PO Upload Definition Detail - Goods Description Layout (Tab) Page

    Description:   This jsp simply displays the html for the goods description
portion of the PO Upload Definition.jsp.  This page is called from
PO UploadDefinition Detail.jsp  and is called by:
		<%@ include file="POUploadDefinitionDetail-Goods.jsp" %>
Note: the values stored in secureParms in this page are done deliberately to avoid
being overwritten as each tab is a submit action.  As a result, if this data does
not make it into the xml document, then it may be overwritten with a null value
in this tab or another.
*******************************************************************************
--%>
<%-- 
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>

<%
int rowCount = 0;
final int NUMBER_OF_BUYER_OR_SELLER_FIELD   = TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD;
final int NUMBER_OF_ITEM_DETAILS_PROD_FIELD =TradePortalConstants.PO_LINE_ITEM_TYPE_OR_VALUE_FIELD;

String del_options = "";
String po_field_data_type_options = "";		//List of refdata options for Dropdown(s)

int counterTmp = 3;
String currentDefinedLab = "";
String currentDefined = "";
String currentDefinedLabReq = "";
String currentDefinedReq = "";

%>

<%-- General Title Panel details start --%>

<%= widgetFactory.createSectionHeader("1", "POUploadDefinitionDetail.General") %>
		
<div class = "columnLeft" style="border-right: 0px">

	<%-- Name and Description --%>

	<%= widgetFactory.createSubsectionHeader("POUploadDefinitionDetail.NameAndDescription",false, false, false,"" ) %>
	
	<%=widgetFactory.createTextField("name","POUploadDefinitionDetail.DefinitionName",
			poDefinitionRef.getAttribute("name") ,"35",isReadOnly,true,false,"class = 'char25'", "", "") %>
			
	<%=widgetFactory.createTextField("description","POUploadDefinitionDetail.Description",
			poDefinitionRef.getAttribute("description") ,"35",isReadOnly,true,false,"class = 'char25'", "", "") %>
				
	<br>
	<%-- Default Checkbox --%>  
	
	<%=widgetFactory.createCheckboxField("default_flag", "POUploadDefinitionDetail.default", 
			poDefinitionRef.getAttribute("default_flag").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "")%>
			 			
</div>
<div class = "columnRight">
	
	<%-- Date Format --%>
	
	<%= widgetFactory.createSubsectionHeader("POUploadDefinitionDetail.DataFormat",false, false, false,"" ) %>
	
	<% String poDateFormat = poDefinitionRef.getAttribute("date_format"); %>
	
        <%del_options = Dropdown.createSortedRefDataOptions( TradePortalConstants.SPO_DATE_FORMAT, 
	 						 poDefinitionRef.getAttribute("date_format"), resMgr.getResourceLocale() );
        out.print(widgetFactory.createSelectField("date_format", "POStructureDefinition.DateFormat", " ", 
						  del_options, isReadOnly, true, false, "", "", ""));
	%>					  
			
	<%-- Data Format --%>
	
	<%	
	 del_options = Dropdown.createSortedRefDataOptions( TradePortalConstants.INV_DELIMITER_CHAR, 
							 poDefinitionRef.getAttribute("delimiter_char"), resMgr.getResourceLocale() );
         %>
          <%= widgetFactory.createSelectField("delimiter_char", "POUploadDefinitionDetail.DelimiterCharacter", 
      			" ", del_options,  isReadOnly, true, false, "", "", "") %>
      			
      	<br>

	<%=widgetFactory.createCheckboxField("include_column_headers", "POStructureDefinition.includeColumnHeading", 
		TradePortalConstants.INDICATOR_YES.equals(poDefinitionRef.getAttribute("include_column_headers")), isReadOnly, false,  "", "", "")%>

</div>

</div>

<%-- General Title Panel End --%>




<%-- Data Definition Title Panel details start --%>
<%= widgetFactory.createSectionHeader("2", "POStructureDefinition.POStructureSummary") %>

<div class = "columnLeft">
	<%=widgetFactory.createLabel("","POStructureDefinition.Step1Text", false, false, false, "none")%> 			
</div>
<div class = "columnRight">
	<%=widgetFactory.createLabel("","POStructureDefinition.Step2Text", false, false, false, "none")%> 			
</div>
<div style="clear:both;"></div>
<div class = "columnLeft">

<table class="formDocumentsTable" id="poStructDataDefinitionTableId" style="width: 96%">
<thead>
	<tr style="height:35px">
		<th colspan=3 style="text-align:left">
			<%= widgetFactory.createSubLabel("POStructureDefinition.SystemDefinedFields") %>
		</th>
	</tr>
</thead>
<tbody>
      <tr>
      	 <td nowrap width="4">&nbsp</td>
 		 <td nowrap width="190">
		  	<%=widgetFactory.createLabel("","POStructureDefinition.FieldName", false, false, false, "","none")%>
 		  </td> 
 		 <td nowrap width="20">
		  	<%=widgetFactory.createLabel("","POStructureDefinition.Size", false, false, false, "", "none")%>
 		  </td> 
       	</tr>
 	<tr>
 		 <td nowrap width="4">&nbsp</td>
 		 <td nowrap width="190">
 		 	<span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("","POStructureDefinition.PurchaseOrderNum", false, true, false, "","none")%> 			
 		  </td>
 		  <td width="20" align="center">
 		    <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
 		  </td>
 	  </tr>
 	  <tr>
 		 <td nowrap width="4">&nbsp</td>
 		 <td nowrap width="190">
 			<span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("","POStructureDefinition.OrderType", false, true, false, "none")%>
 		  </td>
 		  <td width="20" align="center">
 			<%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
 		  </td>
 	  </tr>
 	  <tr>
 		 <td nowrap width="4">&nbsp</td>
 		 <td nowrap width="190">
 		 	<span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("","POStructureDefinition.IssueDate", false, true, false, "none")%>
 		  </td>
 		  <td width="20" align="center">
 			<%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
 		  </td>
 	  </tr> 	  
          <tr >
 		 <td nowrap width="4">&nbsp</td>
 		 <td nowrap width="190">
 		 	<span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("","POStructureDefinition.Currency", false, true, false, "none")%>
 		  </td>		 
 		  <td width="20" align="center">
 			<%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
 		  </td>
 	  </tr>
 	   <tr>
 		 <td nowrap width="4">&nbsp</td>
 		 <td nowrap width="190">
 		 	<span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("","POStructureDefinition.Amount", false, true, false, "none")%>
 		  </td> 		 
 		  <td width="20" align="center">
 			<%= widgetFactory.createLabel("", "18,3", false, false, false, "none") %>
 		  </td>
 	   </tr> 
 	   <tr class>
 		 <td nowrap width="4">&nbsp</td>
 		 <td nowrap width="190">
 		 	<span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("","POStructureDefinition.SellerName", false, true, false, "none")%>
 		  </td> 		 
 		  <td width="20" align="center">
 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
 		  </td>
 	  </tr>   

       <tr>      
       <td class=ListText width="30" align='center'>
              <input type=hidden value="<%=resMgr.getText("POStructureDefinition.Incoterm", TradePortalConstants.TEXT_BUNDLE)%>" name="incoterm" id="incoterm">
              <%=widgetFactory.createCheckboxField("incoterm_req", "",
                            poDefinitionRef.getAttribute("incoterm_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
       </td>
 		<td nowrap width="190">
			<%=widgetFactory.createLabel("","POStructureDefinition.Incoterm", false, false, false, "none")%>
 		</td>            
       <td width="20" align="center">
           <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
        </td>                     
       </tr>
    
       <tr>
       <td nowrap class=ListText width="30" align='center'>       
              <input type=hidden value="<%=resMgr.getText("POStructureDefinition.IncotermLoc", TradePortalConstants.TEXT_BUNDLE)%>" name="incoterm_loc" id="incoterm_loc">
              <%=widgetFactory.createCheckboxField("incoterm_loc_req", "",
                            poDefinitionRef.getAttribute("incoterm_loc_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
       </td>       
 		<td nowrap width="190">
			<%=widgetFactory.createLabel("","POStructureDefinition.IncotermLoc", false, false, false, "none")%>
 		</td>        
        <td width="20" align="center">
          <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
        </td>                      
       </tr>
       <tr>
       <td nowrap class=ListText width="30" align='center'>
              <input type=hidden value="<%=resMgr.getText("POStructureDefinition.ShipmentDate", TradePortalConstants.TEXT_BUNDLE)%>" name="latest_shipment_date" id="latest_shipment_date">
              <%=widgetFactory.createCheckboxField("latest_shipment_date_req", "",
                            poDefinitionRef.getAttribute("latest_shipment_date_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
        </td>
 		<td nowrap width="190">
			<%=widgetFactory.createLabel("","POStructureDefinition.ShipmentDate", false, false, false, "none")%>
 		</td>               
       <td width="20" align="center">
          <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
        </td>                      
     </tr>          
     <tr>
       <td nowrap class=ListText width="30" align='center'>

              <input type=hidden value="<%=resMgr.getText("POStructureDefinition.PartialShip", TradePortalConstants.TEXT_BUNDLE)%>" name="partial_ship_allowed" id="partial_ship_allowed">                 
              <%=widgetFactory.createCheckboxField("partial_ship_allowed_req", "",
                            poDefinitionRef.getAttribute("partial_ship_allowed_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
       </td>     
 		<td nowrap width="190">
			<%=widgetFactory.createLabel("","POStructureDefinition.PartialShip", false, false, false, "none")%>
 		</td>        
       <td width="20" align="center">
          <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>        
	  </td>                      
     </tr>
 
 	<tr>
       <td nowrap class=ListText width="30" align='center'>
              <input type=hidden value="<%=resMgr.getText("POStructureDefinition.GoodsDesc", TradePortalConstants.TEXT_BUNDLE)%>" name="goods_desc" id="goods_desc">
              <%=widgetFactory.createCheckboxField("goods_desc_req", "",
                            poDefinitionRef.getAttribute("goods_desc_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
       </td>
 		<td nowrap width="190">
			<%=widgetFactory.createLabel("","POStructureDefinition.GoodsDesc", false, false, false, "none")%>
 		</td>              
       <td width="20" align="center">
          <%= widgetFactory.createLabel("", "70", false, false, false, "none") %>
        </td>
      </tr>                 
</tbody>
</table>

<table>
     <tr>
     <td>&nbsp;</td>
     </tr>
</table>

<table class="formDocumentsTable" id="poStructBuyerDefinedTableId" style="width: 96%">
<thead>
	<tr style="height:35px">
		<th colspan=3 style="text-align:left">
			<%= widgetFactory.createSubLabel("POStructureDefinition.BuyerDefinedFields") %>
		</th>
	</tr>
</thead>
<tbody>
      <tr>
      <td></td>
      <td width="190">
		<%= widgetFactory.createLabel("", "POStructureDefinition.Label", false, false, false, "none") %>
 	   
       </td>
       <td width="20">
		<%= widgetFactory.createLabel("", "POStructureDefinition.Size", false, false, false, "none") %>
       </td>
       </tr>
 	  <tr>
 	    <td nowrap width="30">
              
              <%=widgetFactory.createCheckboxField("buyer_users_def1_req", "",
                            poDefinitionRef.getAttribute("buyer_users_def1_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
        </td>              
 		<td width="190">
		<%=widgetFactory.createTextField("buyer_users_def1_label","",
			poDefinitionRef.getAttribute("buyer_users_def1_label") ,"35",
			isReadOnly,true,false,"class = 'char25'", "", "none") %>
 		</td>
 		<td width="20" align="center">
			<%= widgetFactory.createLabel("", "140", false, false, false, "none") %>
 		 </td>
 	  </tr>
 	  
 	  <tr>
 	    <td nowrap width="30" >              
              <%=widgetFactory.createCheckboxField("buyer_users_def2_req", "",
                            poDefinitionRef.getAttribute("buyer_users_def2_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
        </td>  		 
        <td width="190">
		<%=widgetFactory.createTextField("buyer_users_def2_label","",
			poDefinitionRef.getAttribute("buyer_users_def2_label") ,"35",
			isReadOnly,true,false,"class = 'char25'", "", "none") %>
 		  </td>
 		  <td width="20" align="center">
			<%= widgetFactory.createLabel("", "140", false, false, false, "none") %>
 		  </td>
 	  </tr>
<%
  int totalBuyerDefinedField = 0;
  for(int i=3; i<=10; i++){
	currentDefinedLab = "buyer_users_def" + (new Integer(i)).toString() + "_label";
	currentDefined = poDefinitionRef.getAttribute(currentDefinedLab);
	
	currentDefinedLabReq = "buyer_users_def" + (new Integer(i)).toString() + "_req";
	currentDefinedReq = poDefinitionRef.getAttribute(currentDefinedLabReq);
	
	if(InstrumentServices.isNotBlank(currentDefined)){
		totalBuyerDefinedField++;
	}else if("Y".equals(currentDefinedReq)){
		totalBuyerDefinedField++;
	}
  }

 counterTmp = 3;

while (counterTmp <= 10)
{	
	currentDefinedLab = "buyer_users_def" + (new Integer(counterTmp)).toString() + "_label";
	currentDefined = poDefinitionRef.getAttribute(currentDefinedLab);
	if (InstrumentServices.isBlank(currentDefined)) {
		if(totalBuyerDefinedField == 0){
			break;
		}
	}else{
		totalBuyerDefinedField = totalBuyerDefinedField -1;
	}
	currentDefinedLabReq = "buyer_users_def" + (new Integer(counterTmp)).toString() + "_req";
	currentDefinedReq = poDefinitionRef.getAttribute(currentDefinedLabReq);
%>
 	  <tr>
 	      <td width="30">           
              <%=widgetFactory.createCheckboxField(currentDefinedLabReq, "",
                            TradePortalConstants.INDICATOR_YES.equals(currentDefinedReq),
                            isReadOnly, false,  "", "", "none")%>
           </td> 
           <td width="190">
		<%=widgetFactory.createTextField(currentDefinedLab,"",
			currentDefined ,"35",
			isReadOnly,true,false,"class = 'char25'", "", "none") %>
 		  </td>
 		  <td width="20" align="center">
			<%= widgetFactory.createLabel("", "140", false, false, false, "none") %>
 		  </td>
 	  </tr>
<%
	counterTmp++;
}

%>
 	  
</tbody>
</table>
<br>
<%
	if (!(isReadOnly)) {
%>
	<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreDefined" name="add4MoreDefined">
	 <%=resMgr.getText("common.Add4MoreText",TradePortalConstants.TEXT_BUNDLE)%>
     <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	    add4MoreDefined('poStructBuyerDefinedTableId');
	</script>
	</button>
	<%=widgetFactory.createHoverHelp("add4MoreDefined","POStructure.Add4More") %>
<%
	} else {
%>
					&nbsp;
<%
	}
%>
<br><br><p>

<table class="formDocumentsTable" id="poStructSellerDefinedTableId" style="width: 96%">
<thead>
	<tr style="height:35px">
		<th colspan=3 style="text-align:left">
			<%= widgetFactory.createSubLabel("POStructureDefinition.SellerDefinedFields") %>
		</th>
	</tr>
</thead>
<tbody>
      <tr>
      <td></td>
      <td width="190">
		<%= widgetFactory.createLabel("", "POStructureDefinition.Label", false, false, false, "none") %>
 	   
       </td>
       <td width="20">
		<%= widgetFactory.createLabel("", "POStructureDefinition.Size", false, false, false, "none") %>
       </td>
       </tr>
 	 <tr>
 	      <td width="30">
                            
              <%=widgetFactory.createCheckboxField("seller_users_def1_req", "",
                            poDefinitionRef.getAttribute("seller_users_def1_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
              </td>              
 		<td width="190">
		<%=widgetFactory.createTextField("seller_users_def1_label","",
			poDefinitionRef.getAttribute("seller_users_def1_label") ,"35",
			isReadOnly,true,false,"class = 'char25'", "", "none") %>
 		</td>
 		<td width="20" align="center">
			<%= widgetFactory.createLabel("", "140", false, false, false, "none") %>
 		 </td>
 	  </tr>
 	  <tr >
 	      <td width="30">                            
              <%=widgetFactory.createCheckboxField("seller_users_def2_req", "",
                            poDefinitionRef.getAttribute("seller_users_def2_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
              </td>  		 
              <td width="190">
		<%=widgetFactory.createTextField("seller_users_def2_label","",
			poDefinitionRef.getAttribute("seller_users_def2_label") ,"35",
			isReadOnly,true,false,"class = 'char25'", "", "none") %>
 		  </td>
 		  <td width="20" align="center">
			<%= widgetFactory.createLabel("", "140", false, false, false, "none") %>
 		  </td>
 	  </tr>
<%

int totalSellerDefinedField = 0;
for(int i=3; i<=10; i++){
	currentDefinedLab = "seller_users_def" + (new Integer(i)).toString() + "_label";
	currentDefined = poDefinitionRef.getAttribute(currentDefinedLab);
	
	currentDefinedLabReq = "seller_users_def" + (new Integer(i)).toString() + "_req";
	currentDefinedReq = poDefinitionRef.getAttribute(currentDefinedLabReq);
	
	if(InstrumentServices.isNotBlank(currentDefined)){
		totalSellerDefinedField++;
	}else if("Y".equals(currentDefinedReq)){
		totalSellerDefinedField++;
	}
}

counterTmp = 3;

while (counterTmp <= 10)
{	
	currentDefinedLab = "seller_users_def" + (new Integer(counterTmp)).toString() + "_label";
	currentDefined = poDefinitionRef.getAttribute(currentDefinedLab);
	if (InstrumentServices.isBlank(currentDefined)) {
		if(totalSellerDefinedField == 0){
			break;
		}
	}else{
		totalSellerDefinedField = totalSellerDefinedField -1;
	}
	currentDefinedLabReq = "seller_users_def" + (new Integer(counterTmp)).toString() + "_req";
	currentDefinedReq = poDefinitionRef.getAttribute(currentDefinedLabReq);
%>
 	  <tr>
 	      <td width="30">
              <%=widgetFactory.createCheckboxField(currentDefinedLabReq, "",
                            TradePortalConstants.INDICATOR_YES.equals(currentDefinedReq),
                            isReadOnly, false,  "", "", "none")%>
              </td> 
              <td width="190">
		<%=widgetFactory.createTextField(currentDefinedLab,"",
			currentDefined ,"35",
			isReadOnly,true,false,"class = 'char25'", "", "none") %>
 		  </td>
 		  <td width="20" align="center">
			<%= widgetFactory.createLabel("", "140", false, false, false, "none") %>
 		  </td>
 	  </tr>
<%
	counterTmp++;
}
%>
 	  
</tbody>
</table>
<br>
<%
	if (!(isReadOnly)) {
%>
	<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreSellDefined" name="add4MoreSellDefined">
	<%=resMgr.getText("common.Add4MoreText",TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	add4MoreDefined('poStructSellerDefinedTableId');
	</script>
	</button>
	<%=widgetFactory.createHoverHelp("add4MoreSellDefined","POStructure.Add4More") %>
<%
	} else {
%>
					&nbsp;
<%
	}
%>
	
</div>

<div class = "columnRight">

 <table class="formDocumentsTable" id="poStructDataFileOrderTableId">
<thead>
	<tr style="height:35px">
		<th colspan=2 style="text-align:left">
			<%= widgetFactory.createSubLabel("POStructureDefinition.PODataOrder") %>
		</th>
	</tr>
</thead>
<tbody>

      <tr>
 		 <td width="50" >
		  	<%=widgetFactory.createLabel("","POStructureDefinition.Order", false, false, false, "none")%>
 		  </td> 		 
 		 <td >
		  	<%=widgetFactory.createLabel("","POStructureDefinition.FieldName", false, false, false, "none")%>
 		  </td> 
       </tr>
       
<%
   String [] initialVal = {"purchase_order_num", "purchase_order_type", "issue_date", "currency", "amount", "seller_name"}; 
   boolean initialFlag = false;
   if (InstrumentServices.isBlank(poDefinitionRef.getAttribute("po_data_field1")))
   	initialFlag = true;
   String currentValue;	
			
   for( counterTmp=1; counterTmp <= 6; counterTmp++ ) 
   {
   
   	if (initialFlag)
   		currentValue = initialVal[counterTmp-1];
   	else
   		currentValue = poDefinitionRef.getAttribute("po_data_field" + (new Integer(counterTmp )).toString());
   
%>

 	 <tr>
 		 <td align="center">
 		  	<input type=hidden id="upload_order_<%=counterTmp%>" name="po_data_field<%=(new Integer(counterTmp)).toString()%>" value="<%=StringFunction.xssCharsToHtml(currentValue)%>">
   			<input type="text" name="" value='<%=counterTmp %>' 
   				data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true" 
   				id='text<%=counterTmp %>'  style="width: 25px;">
 		  </td>
          <td id='fileFieldName<%=counterTmp %>' align="left">
			<%=widgetFactory.createLabel("", "PurchaseOrderUploadRequest."+currentValue, false, false, false, "","none")%>			
 		  </td>
	</tr>
	
<%
}
%>

</tbody>
</table>
<br>
<div>
<%
	if (!(isReadOnly)) {
%>
<button data-dojo-type="dijit.form.Button" type="button" id="updateSysDefOrder">
  <%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		updateDefOrder(document.getElementById("poStructDataFileOrderTableId").rows.length, 'text'); 
	</script>
</button>
<%
	} else {
%>
					&nbsp;
<%
	}
%>

</div>
<br/><br/>


<table class="formDocumentsTable" id="poStructSumDataOrderTableId">
<thead>
	<tr style="height:35px">
		<th colspan=2 style="text-align:left">
			<%= widgetFactory.createSubLabel("POStructureDefinition.POSummaryDataOrder") %>
		</th>
	</tr>
</thead>
<tbody>

      <tr>
 		 <td width="50" >
		  	<%=widgetFactory.createLabel("","POStructureDefinition.Order", false, false, false, "none")%>
 		  </td>
 		 <td >
		  	<%=widgetFactory.createLabel("","POStructureDefinition.FieldName", false, false, false, "none")%>
 		 </td> 
       </tr>
       
<%

//Draw theAssigment Ordre Table
String poSumValue = null;
String poSumField = null;
String poSumReq = TradePortalConstants.INDICATOR_YES;
int reStartIndex = 1;
String allFields="";
currentValue = "";
String indexLabel = "";
while (InstrumentServices.isNotBlank
	(poDefinitionRef.getAttribute("po_summary_field" + (new Integer(reStartIndex )).toString()))
      &&(reStartIndex  <= 24))
{

poSumField = poDefinitionRef.getAttribute("po_summary_field" + (new Integer(reStartIndex )).toString());

//Some of buyer/seeler labels assigned to summary fields have 'user_' as a part of their names 
//and must be corrected to have it as 'users_' before the values can be retrieved from corresponding label fileds
if(poSumField.contains("user_"))
{
	if((poSumField.length()>=10)&&(poSumField.substring(0,5).equals("buyer")))
	{	
		poSumField = poSumField.substring(0,10) + "s" + poSumField.substring(10);
		poSumValue = poDefinitionRef.getAttribute(poSumField);
		indexLabel = poSumField.substring(0,poSumField.indexOf("_label"));
	}	
	else if((poSumField.length()>=11)&&(poSumField.substring(0,6).equals("seller")))
	{
		poSumField = poSumField.substring(0,11) + "s" + poSumField.substring(11);
		poSumValue = poDefinitionRef.getAttribute(poSumField);
		indexLabel = poSumField.substring(0,poSumField.indexOf("_label"));
	}
	else{
		poSumValue = "PurchaseOrderUploadRequest."+poSumField;
		indexLabel = poSumField;
	}
}	
else if (poSumField.contains("users_"))
{
	poSumValue = poDefinitionRef.getAttribute(poSumField);
}
else
{
	poSumValue = "PurchaseOrderUploadRequest."+poSumField;
	indexLabel = poSumField;
	if("incoterm_location".equals(indexLabel)){
	     indexLabel = "incoterm_loc";
    }else if("partial_shipment_allowed".equals(indexLabel)){
    	indexLabel = "partial_ship_allowed";	
    }else if("goods_description".equals(indexLabel)){
    	indexLabel = "goods_desc";
    }
}
int indexOfLabel = poSumField.indexOf("_label");
if (indexOfLabel>-1)
{
	poSumReq = poDefinitionRef.getAttribute(poSumField.substring(0, indexOfLabel) + "_req");	
}
indexOfLabel = poSumField.indexOf("_req");
if (indexOfLabel>-1)
{
	poSumReq = poDefinitionRef.getAttribute(poSumField);	
}


if ((TradePortalConstants.INDICATOR_YES.equals(poSumReq))&&(InstrumentServices.isNotBlank(poSumValue)))
{
	currentValue = poDefinitionRef.getAttribute("po_summary_field" + (new Integer(reStartIndex )).toString());
%>
 	  <tr id= "<%=indexLabel%>_req_order">
 		 <td align="center">
 		      <input type=hidden id="upload_userdef_order_<%=reStartIndex%>" name="po_summary_field<%=reStartIndex%>" 
 		          value="<%=currentValue%>">
   			  <input type="text" name="" value='<%=counterTmp %>' data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true" 
   				id='userDefText<%=reStartIndex %>'  style="width: 25px;" >
 		  </td> 
 		  <td id='userDefFileFieldName<%=reStartIndex%>' align="left">
		  	    <%=widgetFactory.createLabel("", poSumValue, false, false, false, "none")%>	
		  	<%if(!isReadOnly){ %>		
			<span class="deleteSelectedPOStructureDefinitipnItem" id='<%=StringFunction.xssCharsToHtml(indexLabel)%>'></span>
			<%} %>
 		  </td>
 	  </tr>
<%
	counterTmp++;
}	
	reStartIndex++;
}
%>
</tbody>
</table>
<br>
<div>
<%
	if (!(isReadOnly)) {
%> 
<button data-dojo-type="dijit.form.Button" type="button" id="updateUserDefOrder">
    <%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		updateSumDefOrder(document.getElementById("poStructSumDataOrderTableId").rows.length, 
                              document.getElementById("poStructDataFileOrderTableId").rows.length, 'sumDataPos'); 
	</script>
</button>

<%
	} else {
%>
					&nbsp;
<%
	}
%>

</div>
<br/><br/>	

</div>

</div>
