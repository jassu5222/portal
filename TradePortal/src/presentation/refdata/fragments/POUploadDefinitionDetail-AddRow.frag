<%-- Frag used to create dynamic rows --%>

<tr>

	<td><%=resMgr.getText("Data Item "+StringFunction.xssCharsToHtml(k),TradePortalConstants.TEXT_BUNDLE)  %></td>



	<td><%=widgetFactory.createTextField("other" + poUploadTableIndex + "_field_name","",
				poUploadDef.getAttribute("other" + poUploadTableIndex + "_field_name") ,"15", isReadOnly,false,false, "onChange='populateDynamicRows(this);'", "", "") %>
	</td>

	<td>
		<%=widgetFactory.createTextField("other" + poUploadTableIndex + "_size","",
			poUploadDef.getAttribute("other" + poUploadTableIndex + "_size") ,"2",isReadOnly,false,false, "", "", "") %>
	</td>
	
	<td>
<%        // Display the datatype but exclude Large Text from drop down since it is only used for PO Text.
          Vector exclusion = new Vector();
          exclusion.add(TradePortalConstants.PO_FIELD_DATA_TYPE_LARGETEXT);
          po_fielddatatype_options = Dropdown.createSortedRefDataOptions( TradePortalConstants.PO_FIELD_DATA_TYPE,
								poUploadDef.getAttribute("other" + poUploadTableIndex + "_datatype"), resMgr.getResourceLocale(),
                                exclusion );
          
%>

		
		
		<%= widgetFactory.createSelectField("other" + poUploadTableIndex + "_datatype", "", 
					" ", po_fielddatatype_options, (isReadOnly), false, false, 
					"", "", "") %>
					
					
      </td>
      
      <td><%= widgetFactory.createLabel("", "35", false, false, false, "") %></td>

</tr>
