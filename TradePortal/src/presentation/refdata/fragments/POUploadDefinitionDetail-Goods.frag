<%--
*******************************************************************************
       PO Upload Definition Detail - Goods Description Layout (Tab) Page

    Description:   This jsp simply displays the html for the goods description 
portion of the PO Upload Definition.jsp.  This page is called from 
PO UploadDefinition Detail.jsp  and is called by:
		<%@ include file="POUploadDefinitionDetail-Goods.jsp" %>
Note: the values stored in secureParms in this page are done deliberately to 
avoid being overwritten as each tab is a submit action.  As a result, if this 
data does not make it into the xml document, then it may be overwritten with a 
null value in this tab or another.
*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<script language="javascript">

<%-- set values for goods desc order hidden fields --%>
	
	

function setGoodsDescValue(ind, val){
	
	calcFunc();
}


function setChgGoods(ind){
	
	if(document.getElementById("gchg_order_"+ind)){
		   var new_val = document.getElementById("gtext"+ind).value;
			if(parseInt(new_val)>parseInt(ind)){
			document.getElementById("gchg_order_"+ind).value="3";
			}
			else if(parseInt(new_val)<parseInt(ind)){
				document.getElementById("gchg_order_"+ind).value="1";
			}
		
		}	
}


<%-- Method will order goods tabledata, also will swap the rows/id's --%>


function poGoodsUpdateOrder()
			{
				 
				var data_goods = new Array();
				var table = document.getElementById( 'poGoodsOrderTable' );
				var tbl = document.getElementById( 'poGoodsOrderTable' );
				var rows = table.rows;
				var loop=0;
				var data_goods_inner = new Array();
				var dataLoop1=0;
				var arrTemp=new Array();
				var cnt=0;
				for ( var x = 2; x < rows.length; x++ ) 
				{
						var td = rows[x].getElementsByTagName( 'td' );
						data_goods_inner = new Array();
						loop = 0;
						num=x-1;
						for ( var y = 0; y < td.length; y++ ) 
						{
							var input = td[y].getElementsByTagName( 'input' );
							for ( var z = 0; z < input.length; z++ ) 
							{
								if(input[z].id.indexOf("goodsCheckbox")!=-1)
								{
									var nam = input[z].id;
									data_goods_inner[loop]= nam;
									loop= loop+1;
									data_goods_inner[loop] = ""+document.getElementById(nam).checked;
									loop= loop+1;
									
								}
								 else if(input[z].id.indexOf("gtext")!=-1 || input[z].id.indexOf("goods_descr_orderKey_")!=-1){
									 	data_goods_inner[loop]=input[z].value;
										loop= loop+1;
										data_goods_inner[loop]=input[z].id;
										loop= loop+1;

									  }
							}
							var c = td[y].innerText;
							c = c.replace(/^\s+|\s+$/g,'');
							if(c.length>0){
								var temp=td[y].id;
								if(temp.indexOf("goodsFieldName")!=-1){
									temp = temp.replace("goodsFieldName","");
									num=temp;
									arrTemp[cnt]=parseInt(num);
									cnt=cnt+1;
								}else if(temp.indexOf("goodsFieldName")==-1){
									continue;	
								}
								
								data_goods_inner[loop] = document.getElementById("goodsFieldName"+num).id;
								loop = loop+1;
								data_goods_inner[loop] = document.getElementById("goodsFieldName"+num).innerText;
								loop = loop+1;
								data_goods_inner[loop] = document.getElementById("goodsSize"+num).id;
								loop = loop+1;
								data_goods_inner[loop] = document.getElementById("goodsSize"+num).innerText;
								loop = loop+1;
								
							}
						}
						
						
						data_goods_inner[loop]= document.getElementById("goods_descr_orderKey_"+num).value;
						loop=loop+1;
						data_goods_inner[loop]=document.getElementById("goods_descr_orderKey_"+num).id;
						loop=loop+1;
						data_goods_inner[loop]= document.getElementById("goods_descr_order_"+num).value;
						loop=loop+1;
						data_goods_inner[loop]=document.getElementById("goods_descr_order_"+num).id;
						loop=loop+1;
						data_goods_inner[loop]=document.getElementById("gchg_order_"+num).value;
						data_goods[dataLoop1]=data_goods_inner;
						dataLoop1=dataLoop1+1;
						
				}
				data_goods.sort(mySorting);
				function mySorting(a, b) {
							  var o1 = parseInt(a[2]);
							  var o2 = parseInt(b[2]);
							
							  var p1 = parseInt(a[12]);
							  var p2 = parseInt(b[12]);
							
							  if (o1 != o2) {
							    if (o1 < o2) return -1;
							    if (o1 > o2) return 1;
							    return 0;
							  }
							  if (p1 < p2) return -1;
							  if (p1 > p2) return 1;
							  return 0;
							}
				
				
				
				    var innerHtml = "";
					var i=0;
					for(var ind=arrTemp.length-1; ind>=0;ind--)
					{
						
						i=arrTemp[ind];
						if(dijit.byId("goods_descr_orderKey_"+i))
							dijit.byId("goods_descr_orderKey_"+i).destroy(true);
						if(document.getElementById("goods_descr_orderKey_"+i))
							document.getElementById("goods_descr_orderKey_"+i).removeAttribute("id");
						
						if(dijit.byId("goods_descr_order_"+i))
							dijit.byId("goods_descr_order_"+i).destroy(true);
						if(document.getElementById("goods_descr_order_"+i))
							document.getElementById("goods_descr_order_"+i).removeAttribute("id");
						
						if(dijit.byId("goodsFieldName"+i))
							dijit.byId("goodsFieldName"+i).destroy(true);
						if(document.getElementById("goodsFieldName"+i))
							document.getElementById("goodsFieldName"+i).removeAttribute("id");
						
						if(dijit.byId("gtext"+i))
							dijit.byId("gtext"+i).destroy(true);
						if(document.getElementById("gtext"+i))
							document.getElementById("gtext"+i).removeAttribute("id");
						
						if(dijit.byId("goodsSize"+i))
							dijit.byId("goodsSize"+i).destroy(true);
						if(document.getElementById("goodsSize"+i))
							document.getElementById("goodsSize"+i).removeAttribute("id");
						
						if(dijit.byId("goodsCheckbox"+i))
							dijit.byId("goodsCheckbox"+i).destroy(true);
						if(document.getElementById("goodsCheckbox"+i))
							document.getElementById("goodsCheckbox"+i).removeAttribute("id");
						tbl.deleteRow(ind+2);
					}
					
				   
					for(var i=0;i<data_goods.length;i++)
					{
							
						   var rowCount = tbl.rows.length;	
							var newRow1 = tbl.insertRow(rowCount);
							var data_temp = data_goods[i];
							var cell00 = newRow1.insertCell(0);
							var cell1 = newRow1.insertCell(1);
							var cell2 = newRow1.insertCell(2);
							var cell3 = newRow1.insertCell(3);
							var num = i+1;
							var cell_data0_name = "goodsCheckbox"+num;
							var chk = false;
							var cell_data0_val = data_temp[1];
							if(cell_data0_val=='false'){
							    chk=false;
								}
							else{
								chk=true;
								cell_data0_val="checked";
							}		
							var cell_data1_name = "gtext"+num;
							var cell_data1_val = data_temp[2];
							newRow1.id = rowCount;	
							
							
							var gCheckBox = "";
							if(chk==false)
							 gCheckBox = new dijit.form.CheckBox({
						        id: "goodsCheckbox"+num,
						        checked:false
						    });
							else
								gCheckBox = new dijit.form.CheckBox({
							        id: "goodsCheckbox"+num,
							        checked: true
							    });
														
							require(["dojo/ready","dijit/registry", "dojo/on"], function(ready,registry, on){
								ready(function(){
							on(gCheckBox, "change", function(isChecked){
								if(isChecked){
									calcFunc();
								}
								
								else{
									calcFunc();
								}
							},true);
								})});
							cell00.setAttribute("align","center");
							cell00.appendChild(gCheckBox.domNode);
							
							var goodsTextBox = new dijit.form.TextBox({
						        name: "",
						        id: "gtext"+num,
						        value: num,
						        trim:true
						 	});
							
							
							require(["dojo/ready","dijit/registry", "dojo/on"], function(ready,registry, on){
								ready(function(){
							 dojo.connect(registry.byId('gtext'+num),'onChange',function(){
								 var temp = this.id;
								 temp = temp.replace("gtext","");
								 setChgGoods(temp);
								  
						    });})});
							
							dojo.addClass(goodsTextBox.domNode, "selectedItemOrderTextBox");
						    
							
							cell1.appendChild(goodsTextBox.domNode);
						    cell1.setAttribute("align","center");
							
							cell2.innerText=data_temp[5];
						    cell2.setAttribute("id","goodsFieldName"+num);
						    cell2.setAttribute("align","left");
						    	
						    cell3.innerText=data_temp[7];
						    cell3.setAttribute("id","goodsSize"+num);
						    cell3.setAttribute("align","center"); 
							
						    
						    
						    var input1 = document.createElement("input");
						    input1.setAttribute("type", "hidden");
							input1.setAttribute("name", "goods_descr_order_"+num);
							input1.setAttribute("id","goods_descr_order_"+num);
							input1.setAttribute("value", data_temp[10]);
							
							var input2 = document.createElement("input");
						    input2.setAttribute("type", "hidden");
							input2.setAttribute("name", "goods_descr_orderKey_"+num);
							input2.setAttribute("id","goods_descr_orderKey_"+num);
							input2.setAttribute("value", data_temp[8]);
							
							var input3 = document.createElement("input");
						    input3.setAttribute("type", "hidden");
							input3.setAttribute("name", "goods_"+data_temp[8]);
							input3.setAttribute("id","goods_"+data_temp[8]);
							input3.setAttribute("value", num);
							
							var input4 = document.createElement("input");
						    input4.setAttribute("type", "hidden");
							input4.setAttribute("name", "gchg_order_"+num);
							input4.setAttribute("id","gchg_order_"+num);
							input4.setAttribute("value", "2");
							newRow1.appendChild(input1);
							newRow1.appendChild(input2);
							newRow1.appendChild(input3);
							newRow1.appendChild(input4);
							require(["dojo/parser"], function(parser) {
						     	parser.parse(newRow1.id);     	
							 });
							
							
					}
				    
			}



function calcFunc(){
	
	var tb = document.getElementById("poGoodsOrderTable");
	var lineLen=0;
	var space = 0;
	for(var i=0;i<tb.rows.length;i++)
	{
		var ind = (i+1);
		
		if(ind>=tb.rows.length-1)
			continue;
		if(document.getElementById("goodsCheckbox"+ind).checked)
		{
			space=space+1;
			document.getElementById("goods_descr_order_"+ind).value =document.getElementById("goods_descr_orderKey_"+ind).value;
			lineLen=parseInt(document.getElementById("goodsSize"+ind).innerText)+lineLen;
			
		}
		else{
			document.getElementById("goods_descr_order_"+ind).value ="";
		}
	}

	
	lineLen = lineLen+space-1;
	
	if(lineLen<0 || isNaN(lineLen))
		lineLen=0;
		if(document.getElementById('tempLineLength'))
			document.getElementById('tempLineLength').value = lineLen;
			if(document.getElementById('total_line_length'))
			document.getElementById('total_line_length').innerHTML = lineLen;
}




function poUploadGoodsUpdateOrder(gIndex){
	
	poGoodsUpdateOrder();
	
	
		
	}
	


</script>
<%
Debug.debug("******************** IN GOODS DESCRIPTION LAYOUT TAB JSP *************************");

final int NUMBER_OF_FOOTER_LINES = 3;
final int NUMBER_OF_GOODS_DESC_ORDER_FIELDS = 30;

String goodsCheckedValue = "";
int iloop = 1;

// Handle the case where the user clicked the "Check Line Length" button after
// unchecking a checkbox.   When performing a Save, the premediator action takes
// care of this.   Since no mediator is called, this must be handled here.
if(getDataFromDoc && InstrumentServices.isBlank(defaultDoc.getAttribute("/In/POUploadDefinition/include_column_header")))
     poUploadDef.setAttribute("include_column_header", TradePortalConstants.INDICATOR_NO);

if(getDataFromDoc && InstrumentServices.isBlank(defaultDoc.getAttribute("/In/POUploadDefinition/include_footer")))
     poUploadDef.setAttribute("include_footer", TradePortalConstants.INDICATOR_NO);

if(getDataFromDoc && InstrumentServices.isBlank(defaultDoc.getAttribute("/In/POUploadDefinition/include_po_text")))
     poUploadDef.setAttribute("include_po_text", TradePortalConstants.INDICATOR_NO);

String lineLength = poUploadDef.getLineLengthString();

fieldNameOptionList = poUploadDef.buildFieldOptionList( true, false, resMgr );

//get parsed data as key:value
hashMap = poUploadDef.parseFieldOptionsList(fieldNameOptionList, "POUploadDefinitionDetail-Goods");

defaultText = " ";
options = "";

if( InstrumentServices.isBlank( lineLength ) ) {
	lineLength = "0";
}



//This will ensure that the 'update message' will have the correct reference to this 
//PO Upload Defn.  Otherwise 'null' is substituted in for the defnintion name inthe message.

secureParms.put("name", poUploadDef.getAttribute("name"));

//Because this is a checkbox, and the tabs are submit actions - we need to ensure that
//This value makes it to the mapfile otherwise the premediator will reset the value to 'N'

//secureParms.put("default_flag", poUploadDef.getAttribute("default_flag"));
%>

<div class = "columnLeft">

	<%= widgetFactory.createSubsectionHeader("POUploadDefinitionDetail.GoodsDesciptionFieldOrder",false, true, false,"" ) %>
	
	<%= widgetFactory.createLabel("", "POUploadDefinitionDetail.GoodsDesciptionUseCheckBoxText", false, false, false, "") %>
<div class="formItem">
	<table id="poGoodsOrderTable" class="formDocumentsTable" border="1" style="width:80%" cellspacing="0" >
	<thead>
	<tr style="height:35px">
		<th style="width:10%">&nbsp;</th>
		<th style="text-align:left;width:15%;"><%= resMgr.getText("POUploadDefinitionDetail.Order", TradePortalConstants.TEXT_BUNDLE) %></th>
		<th style="text-align:left;width:55%;"><%= resMgr.getText("POUploadDefinitionDetail.FieldName", TradePortalConstants.TEXT_BUNDLE) %></th>
		<th style="text-align:left;width:20%;"><%= resMgr.getText("POUploadDefinitionDetail.Size", TradePortalConstants.TEXT_BUNDLE) %></th>
	</tr>
	<tr>
			<td colspan="4" class="tableSubHeaderWithOutBold"><%= resMgr.getText("POUploadDefinitionDetail.TotalLineLength", TradePortalConstants.TEXT_BUNDLE) %> <span id='total_line_length'></span> 
			<%= resMgr.getText("POUploadDefinitionDetail.Of", TradePortalConstants.TEXT_BUNDLE) %>
			</td>
			
	</tr>
	</thead>
	<tbody>
	<%
	

    
    List goodsDescKeyList = new ArrayList();
    int i=0;
    int j=0;
   
    List tempList = null;
    for(i=1;i<= hashMap.size();i++){
    	if(poUploadDef.getAttribute("goods_descr_order_" + i)!=null && !"".equals(poUploadDef.getAttribute("goods_descr_order_" + i))){
    		
    		goodsDescKeyList.add(poUploadDef.getAttribute("goods_descr_order_" + i));
    		
    	}
    }
    
   
    for(i=0; i<goodsDescKeyList.size(); i++){
    	for(j=1;j<= hashMap.size();j++){
    		tempList = hashMap.get(j);
    		
    		if(tempList.contains(goodsDescKeyList.get(i))){
    			
    			%>
			    <tr>
					<td align="center"><input type="checkbox" id="goodsCheckbox<%=iloop %>" checked <%=(isReadOnly?"readonly":"")%>
				    	data-dojo-type="dijit.form.CheckBox" onClick="calcFunc();"></td>
					<td align="center">
						
			   		<input type="text" name="" value='<%=iloop %>' data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true" 
			   				id='gtext<%=iloop %>'  <%=(isReadOnly?"readonly":"")%> class="selectedItemOrderTextBox" onchange="setChgGoods(<%=iloop%>);">
					</td>
					<td id='goodsFieldName<%=iloop %>' align="left"><%= tempList.get(1) %></td>	
						
					<td id='goodsSize<%=iloop %>' align="center"><%= tempList.get(2) %></td>		
						
					<input type=hidden name='goods_descr_order_<%=iloop %>'  
					id='goods_descr_order_<%=iloop %>'
					value='<%= tempList.get(0)%>'>
					
					<input type=hidden name='goods_descr_orderKey_<%=iloop %>'  
							id='goods_descr_orderKey_<%=iloop %>'
					value='<%= tempList.get(0) %>'>	
					
					<input type=hidden name='goods_<%=tempList.get(0) %>'  
						id='goods_<%=tempList.get(0) %>'
					value='<%= iloop %>'>
					<input type=hidden name='gchg_order_<%=iloop%>'  
						id='gchg_order_<%=iloop%>'
					value='2'>
				</tr>    			
    			<%
    			iloop++;
    			break;
    		}
    	}
    }
    
    for(j=1;j<= hashMap.size();j++){
		tempList = hashMap.get(j);
		
		if(!goodsDescKeyList.contains(tempList.get(0))){
			
			%>
		    <tr>
				<td align="center"><input type="checkbox" id="goodsCheckbox<%=iloop %>" <%=(isReadOnly?"readonly":"")%>
			    	data-dojo-type="dijit.form.CheckBox" onClick="calcFunc();"></td>
				<td align="center">
					
		   		<input type="text" name="" value='<%=iloop %>' data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true" 
		   				id='gtext<%=iloop %>'  <%=(isReadOnly?"readonly":"")%> class="selectedItemOrderTextBox" onchange="setChgGoods(<%=iloop%>);">
				</td>
				<td id='goodsFieldName<%=iloop %>' align="left"><%= tempList.get(1) %></td>	
					
				<td id='goodsSize<%=iloop %>' align="center"><%= tempList.get(2) %></td>		
					
				<input type=hidden name='goods_descr_order_<%=iloop %>'  
					id='goods_descr_order_<%=iloop %>'
				value=''>
				
				<input type=hidden name='goods_descr_orderKey_<%=iloop %>'  
						id='goods_descr_orderKey_<%=iloop %>'
				value='<%= tempList.get(0) %>'>		
				
				<input type=hidden name='goods_<%=tempList.get(0) %>'  
						id='goods_<%=tempList.get(0) %>'
				value='<%= iloop %>'>
				<input type=hidden name='gchg_order_<%=iloop%>'  
						id='gchg_order_<%=iloop%>'
				value='2'>
				
				
				
			</tr>    			
			<%
			iloop++;			
		}
	}
		
		%>
	
	
	</tbody>	

	</table>
	<br>
	<button data-dojo-type="dijit.form.Button" id="updateGoodsOrder" <%=(isReadOnly?"disabled":"")%>
			onClick="poUploadGoodsUpdateOrder(<%=iloop %>)" type="button">
			<%=resMgr.getText("POUploadDefnSelector.UpdateOrder",TradePortalConstants.TEXT_BUNDLE)%>
	</button></div>
</div>

<div class = "columnRight">
	<%= widgetFactory.createSubsectionHeader("POUploadDefinitionDetail.GoodsDescriptionLayout",false, false, false,"" ) %>
		
	<%=widgetFactory.createCheckboxField("include_column_header", "POUploadDefinitionDetail.IncludeColumnHeaders", 
		poUploadDef.getAttribute("include_column_header").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "")%>
		
	<%=widgetFactory.createCheckboxField("include_footer", "POUploadDefinitionDetail.IncludeFooterShownBelow", 
		poUploadDef.getAttribute("include_footer").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "")%>
		
	<%
	   for( x=1; x<=NUMBER_OF_FOOTER_LINES; x++) {
	%>
	<div class="formItemButtoninline"><span class="formItemButtoninline"><span class="formItemButtoninline">
	   <%=widgetFactory.createTextField("footer_line_" + x,"",
    		  			poUploadDef.getAttribute("footer_line_" + x) ,"65", isReadOnly,false,false, "class='char35';", "", "none") %></span></span>
    </div>
	      
	<% }
	%>
	
	<% /******************* INCLUDE PO TEXT ***********************/ %>
    
        <div id="include_po_text_div">
        <%=widgetFactory.createCheckboxField("include_po_text", "POUploadDefinitionDetail.IncludePOText", 
		poUploadDef.getAttribute("include_po_text").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "")%>       
		</div>  

</div>
