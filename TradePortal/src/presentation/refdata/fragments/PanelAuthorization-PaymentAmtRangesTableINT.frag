<%
for(int tablenum = 0; tablenum < 6; tablenum++){
%>
<div id='<%="INTTable"+tablenum%>' >

		<table cellspacing="0" cellpadding="0" class="formDocumentsTable">
			<thead>
			  <tr>
			  		<th colspan="6" style="text-align: left;">
			  			<% 
		    				String panelAuthRangeMinAmtINT0=" ";
		    				String panelAuthRangeMaxAmtINT0=" ";
		    				String panelAuthRangeOidINT0=EncryptDecrypt.encryptStringUsingTripleDes( "", userSession.getSecretKey());
		    				String InstrumentTypeCodeINT0=InstrumentType.FUNDS_XFER;
		    				if(panelAuthorizationRangeINTList[tablenum]!=null){
		    					panelAuthRangeMinAmtINT0=panelAuthorizationRangeINTList[tablenum].getAttribute("panel_auth_range_min_amt");
		    					panelAuthRangeMaxAmtINT0=panelAuthorizationRangeINTList[tablenum].getAttribute("panel_auth_range_max_amt");
		    					panelAuthRangeOidINT0=EncryptDecrypt.encryptStringUsingTripleDes( panelAuthorizationRangeINTList[tablenum].getAttribute("panel_auth_range_oid"), userSession.getSecretKey());
		    					InstrumentTypeCodeINT0=panelAuthorizationRangeINTList[tablenum].getAttribute("instrument_type_code");
		    				}
		    				//out.println("panelAuthRangeOidINT0"+panelAuthRangeOidINT0);
		    			%>
		    			<input type=hidden value='<%=panelAuthRangeOidINT0%>' name='<%="panelAuthOidINT"+tablenum%>' >
		    			<input type=hidden value='<%=InstrumentTypeCodeINT0%>' name='<%="InstrumentTypeCodeINT"+tablenum%>' >
			  			<%=tablenum+1 %>. <%=resMgr.getText("PanelAuthorizationGroupDetail.PmntAmtRange", TradePortalConstants.TEXT_BUNDLE) %>
			  			<%=widgetFactory.createAmountField("panelAuthRangeMinAmtINT"+tablenum,"",panelAuthRangeMinAmtINT0, "", isReadOnly, false, false,"class='char10'", "", "none")%>
			  			<%=resMgr.getText("PanelAuthorizationGroupDetail.to", TradePortalConstants.TEXT_BUNDLE) %>
			  			<%=widgetFactory.createAmountField("panelAuthRangeMaxAmtINT"+tablenum,"",panelAuthRangeMaxAmtINT0, "", isReadOnly, false, false,"class='char10'", "", "none")%>
						
			  		</th>
			  </tr>
		      <tr>
		            <th width="5%">&nbsp;</th>
		            <th width="20%"><%=resMgr.getText("PanelAuthorizationGroupDetail.LevelA", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th width="20%"><%=resMgr.getText("PanelAuthorizationGroupDetail.LevelB", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th width="20%"><%=resMgr.getText("PanelAuthorizationGroupDetail.LevelC", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th width="20%"><%=resMgr.getText("PanelAuthorizationGroupDetail.TotalAuthorisers", TradePortalConstants.TEXT_BUNDLE)%></th>
		            <th width="15%"><%=resMgr.getText("PanelAuthorizationGroupDetail.ClearRow", TradePortalConstants.TEXT_BUNDLE)%></th>
		      </tr>
		     </thead>
		     <% for(int iLoop = 0; iLoop < 5; iLoop++) { %>
		     <tr>
		     	<td align="left"><%= (iLoop+1) %></td>
		     	<td>
		     		<% 
		     			String numOfPanelAUsersGroupINT=" ";
		     			if(panelAuthorizationRangeINTList[tablenum]!=null)
		     				numOfPanelAUsersGroupINT=panelAuthorizationRangeINTList[tablenum].getAttribute("num_of_panel_A_users_group"+(iLoop + 1));
		     		%>
		     		<%=widgetFactory.createTextField("numOfPanelAUsersGroupINT"+(iLoop+1)+tablenum, "", numOfPanelAUsersGroupINT, "15", isReadOnly, false, false,"", "", "none")%>
		     	</td>
		     	<td>
		     		<% 
		     			String numOfPanelBUsersGroupINT=" ";
		     			if(panelAuthorizationRangeINTList[tablenum]!=null)
		     				numOfPanelBUsersGroupINT=panelAuthorizationRangeINTList[tablenum].getAttribute("num_of_panel_B_users_group"+(iLoop + 1));
		     		%>
		     		<%=widgetFactory.createTextField("numOfPanelBUsersGroupINT"+(iLoop+1)+tablenum, "", numOfPanelBUsersGroupINT, "15", isReadOnly, false, false,"", "", "none")%>
		     	</td>
		     	<td>
		     		<% 
		     			String numOfPanelCUsersGroupINT=" ";
		     			if(panelAuthorizationRangeINTList[tablenum]!=null)
		     				numOfPanelCUsersGroupINT=panelAuthorizationRangeINTList[tablenum].getAttribute("num_of_panel_C_users_group"+(iLoop + 1));
		     		%>
		     		<%=widgetFactory.createTextField("numOfPanelCUsersGroupINT"+(iLoop+1)+tablenum, "",numOfPanelCUsersGroupINT , "15", isReadOnly, false, false,"", "", "none")%>
		     	</td>
		     	<td>
		     		<%
					    int panelA = 0;
					    int panelB = 0;
					    int panelC = 0;
					    int totalNumOfAuthGroup = 0;
					    if(panelAuthorizationRangePMTList[tablenum]!=null){
					    try {
					   	 	 
					    	panelA = Integer.parseInt(panelAuthorizationRangePMTList[tablenum].getAttribute("num_of_panel_A_users_group"+(iLoop + 1)));
					    } catch(NumberFormatException nfe) {
					    	panelA = 0;
					    }
					    try {
					   	     panelB = Integer.parseInt(panelAuthorizationRangePMTList[tablenum].getAttribute("num_of_panel_B_users_group"+(iLoop + 1)));
					    } catch(NumberFormatException nfe) {
					    	panelB = 0;
					    }
					    try {
					   	 	panelC = Integer.parseInt(panelAuthorizationRangePMTList[tablenum].getAttribute("num_of_panel_C_users_group"+(iLoop + 1)));
	
					    } catch(NumberFormatException nfe) {
					    	panelC = 0;
					    }
					    }
					    totalNumOfAuthGroup = panelA + panelB + panelC;
				    %>
				    <%=totalNumOfAuthGroup %>
		     	</td>
		     	<td>
		     		<%	if (!isReadOnly){ %>
	              		<a href="javascript:clearTextINT();" >Clear</a>
	              <% } %>
		     	</td>
		     </tr>
		     <%} %>
		     
		</table>
</div>
<%
}
%>