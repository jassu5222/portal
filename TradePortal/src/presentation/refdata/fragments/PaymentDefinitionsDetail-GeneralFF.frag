<%-- Data Definition Title Panel details start --%>
<%
Map sizeMap = payDefUtil.getSizeMap();
%>
<%=widgetFactory.createSectionHeader("2",
					"PaymentDefinitionDetail.FFF")%>

<div class = "columnLeft">
	<%=widgetFactory.createLabel("",
					"PaymentDefinitions.FFDefSep1", false, false, false,
					"none")%>


    <table class="formDocumentsTable" id="PmtFFDefHdrTableId" style="width:98%;">
    <thead>
    <tr style="height:35px;">
        <th colspan=4 style="text-align:left;">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitions.FFHdrFields")%>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr style="height:35px">
        <td width="15" align='center' >
            <b><%=resMgr.getText("PaymentDefinitions.ReqField", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td nowrap width="130"  >
            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td  width="20" align="center" >
            <b><%=resMgr.getText("PaymentDefinitions.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="30" >
            <b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("PaymentDefinitions.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
    </tr>
    <tr>
        <td >&nbsp;</td>
        <td>
            <span class="asterisk">*</span>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.HeaderIdentifier", false, true, false, "none")%>
        </td>
        <td align="right">
            <input type="hidden" name="header_identifier_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("header_identifier_size"))? paymentDef.getAttribute("header_identifier_size") : "3" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='header_identifier_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
        </td>

        <td >&nbsp;</td>
    </tr>
    <tr>
        <td >&nbsp;</td>
        <td>
            <span class="asterisk">*</span>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.DebitAcctNum", false, true, false, "none")%>
        </td>

        <td align="right">
            <input type="text" name="debit_account_number_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("debit_account_number_size"))? paymentDef.getAttribute("debit_account_number_size") : "30" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='debit_account_number_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
      
        </td>
        <td >&nbsp;</td>
    </tr>
    <tr>
        <td nowrap width="4">&nbsp</td>
        <td  >
            <span class="asterisk">*</span>
            <%=widgetFactory.createLabel("", "PaymentDefinitions.ExecutionDate",
                    false, true, false, "none")%>
        </td>
        <td align="right">
            <input type="hidden" name="execution_date_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("execution_date_size"))? paymentDef.getAttribute("execution_date_size") : "10" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='execution_date_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
        </td>
        <td >&nbsp;</td>
    </tr>
    <tr>
        <td nowrap width="4">&nbsp</td>
        <td  >
            <span class="asterisk">*</span>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.Cuurency", false, true, false, "none")%>
        </td>
        <td align="right">
            <input type="hidden" name="payment_currency_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("payment_currency_size"))? paymentDef.getAttribute("payment_currency_size") : "3" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='payment_currency_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
        </td>
        <td >&nbsp;</td>
    </tr>
    <tr>
        <td nowrap width="4">&nbsp</td>
        <td  >
            <span class="asterisk">*</span>
            <%=widgetFactory.createLabel("", "PaymentDefinitions.PaymentMethod",
                    false, true, false, "none")%>
        </td>
        <td align="right">
            <input type="hidden" name="payment_method_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("payment_method_size"))? paymentDef.getAttribute("payment_method_size") : "4" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='payment_method_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />

            <%= widgetFactory.createLabel("", "4", false, false, false, "none") %>
        </td>
        <td >&nbsp;</td>
    </tr>
    <tr>
        <td align='center'>
            <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.file_reference", TradePortalConstants.TEXT_BUNDLE)%>" name="hfile_reference" id="hfile_reference">

            <%=widgetFactory.createCheckboxField(
                    "hfile_reference_req",
                    "",
                    TradePortalConstants.INDICATOR_YES.equals(
                            paymentDef.getAttribute("file_reference_req")), isReadOnly,
                    false, "", "", "none")%>
        </td>
        <td  >
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.file_reference", false, false, false, "none")%>
        </td>
        <td align="right">
            <input type="text" name="file_reference_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("file_reference_size"))? paymentDef.getAttribute("file_reference_size") : "35" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='file_reference_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />

        
        </td>
        <td align='center'>
            <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.file_reference", TradePortalConstants.TEXT_BUNDLE)%>" name="hfile_reference" id="hfile_reference">

            <%=widgetFactory.createCheckboxField(
                    "hfile_reference_data_req",
                    "",
                    TradePortalConstants.INDICATOR_YES.equals(
                            paymentDef.getAttribute("file_reference_data_req")), isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td align='center'>
            <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.confidential_ind", TradePortalConstants.TEXT_BUNDLE)%>" name="hconfidential_ind" id="hconfidential_ind">

            <%=widgetFactory.createCheckboxField(
                    "hconfidential_ind_req",
                    "",
                    TradePortalConstants.INDICATOR_YES.equals(
                            paymentDef.getAttribute("confidential_ind_req")), isReadOnly,
                    false, "", "", "none")%>
        </td>
        <td  >
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.confidential_ind", false, false, false, "none")%>
        </td>
        <td align="right">
            <input type="hidden" name="confidential_ind_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("confidential_ind_size"))? paymentDef.getAttribute("confidential_ind_size") : "1" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='confidential_ind_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.confidential_ind", TradePortalConstants.TEXT_BUNDLE)%>" name="hconfidential_ind" id="hconfidential_ind">

            <%=widgetFactory.createCheckboxField(
                    "hconfidential_ind_data_req",
                    "",
                    TradePortalConstants.INDICATOR_YES.equals(
                            paymentDef.getAttribute("confidential_ind_data_req")), isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>

    <tr>
        <td align='center'>
            <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.indv_acct_entry_ind", TradePortalConstants.TEXT_BUNDLE)%>" name="hindv_acct_entry_ind" id="hindv_acct_entry_ind">

            <%=widgetFactory.createCheckboxField(
                    "hindv_acct_entry_ind_req",
                    "",
                    TradePortalConstants.INDICATOR_YES.equals(
                            paymentDef.getAttribute("indv_acct_entry_ind_req")), isReadOnly,
                    false, "", "", "none")%>
        </td>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.indv_acct_entry_ind", false, false, false, "none")%>
        </td>
        <td align="right">
            <input type="hidden" name="indv_acct_entry_ind_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("indv_acct_entry_ind_size"))? paymentDef.getAttribute("indv_acct_entry_ind_size") : "1" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='indv_acct_entry_ind_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.indv_acct_entry_ind", TradePortalConstants.TEXT_BUNDLE)%>" name="hindv_acct_entry_ind" id="hindv_acct_entry_ind">

            <%=widgetFactory.createCheckboxField(
                    "hindv_acct_entry_ind_data_req",
                    "",
                    TradePortalConstants.INDICATOR_YES.equals(
                            paymentDef.getAttribute("indv_acct_entry_ind_data_req")), isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
 
    
    <tr>
    <td align='center'>
        <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.fx_contract_number", TradePortalConstants.TEXT_BUNDLE)%>" name="hfx_contract_number" id="hfx_contract_number">

        <%=widgetFactory.createCheckboxField(
                "hfx_contract_number_req",
                "",
                TradePortalConstants.INDICATOR_YES.equals(
                        paymentDef.getAttribute("fx_contract_number_req")), isReadOnly,
                false, "", "", "none")%>
    </td>
    <td  >
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fx_contract_number", false, false, false, "none")%>
    </td>
    <td align="right">
        <input type="text" name="fx_contract_number_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fx_contract_number_size"))? paymentDef.getAttribute("fx_contract_number_size") : "14" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fx_contract_number_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
      
    </td>
    <td align='center'>
        <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.fx_contract_number", TradePortalConstants.TEXT_BUNDLE)%>" name="hfx_contract_number" id="hfx_contract_number">

        <%=widgetFactory.createCheckboxField(
                "hfx_contract_number_data_req",
                "",
                TradePortalConstants.INDICATOR_YES.equals(
                        paymentDef.getAttribute("fx_contract_number_data_req")), isReadOnly,
                false, "", "", "none")%>
    </td>
    </tr>
    
    <tr>
    <td align='center'>
        <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.fx_contract_rate", TradePortalConstants.TEXT_BUNDLE)%>" name="hfx_contract_rate" id="hfx_contract_rate">
  
        <%=widgetFactory.createCheckboxField(
                "hfx_contract_rate_req",
                "",
                TradePortalConstants.INDICATOR_YES.equals(
                        paymentDef.getAttribute("fx_contract_rate_req")), isReadOnly,
                false, "", "", "none")%>
    </td>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fx_contract_rate", false, false, false, "none")%>
    </td>
    <td align="right">
    
    <% String fxRateSize = paymentDef.getAttribute("fx_contract_rate_size");
    if (StringFunction.isNotBlank(fxRateSize)){
        int rateSize = new Integer(fxRateSize).intValue();
        fxRateSize  = (rateSize-9) + ",8";
    }
    %>
        <input type="text" name="fx_contract_rate_size" value='<%=StringFunction.isNotBlank(fxRateSize)? fxRateSize : "5,8" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fx_contract_rate_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
      
    </td>
    <td align='center'>
    <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.fx_contract_rate", TradePortalConstants.TEXT_BUNDLE)%>" name="hfx_contract_rate" id="hfx_contract_rate">

    <%=widgetFactory.createCheckboxField(
            "hfx_contract_rate_data_req",
            "",
            TradePortalConstants.INDICATOR_YES.equals(
                    paymentDef.getAttribute("fx_contract_rate_data_req")), isReadOnly,
            false, "", "", "none")%>
    </td>  
    </tr>


    </tbody>
    </table>


<table>

     <tr style="height:30px">
     <td>&nbsp</td>
     </tr>
</table>


<table class="formDocumentsTable" id="PmtFFDefTableId" style="width:98%;">
<thead>
	<tr style="height:35px;">
		<th colspan=4 style="text-align:left;">
			<%=widgetFactory
					.createSubLabel("PaymentDefinitions.FFPaymentFields")%>
		</th>
	</tr>
</thead>
<tbody>
      <tr style="height:35px">
      	 <td width="15" align='center' >
             <b><%=resMgr.getText("PaymentDefinitions.ReqField", TradePortalConstants.TEXT_BUNDLE)%></b>
         </td>
 		 <td nowrap width="130"  >
		  	<b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
 		  </td>
 		 <td  width="20" align="center" >
		  	<b><%=resMgr.getText("PaymentDefinitions.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
 		 </td>
 		 <td width="30" >
		  	<b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("PaymentDefinitions.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
 		 </td>
      </tr>
      <tr>
 		 <td >&nbsp;</td>
 		 <td>
 		 <span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("",
					"PaymentDefinitions.detail_identifier", false, true, false, "none")%>
 		  </td>
 		  <td align="right">
              <input type="hidden" name="detail_identifier_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("detail_identifier_size"))? paymentDef.getAttribute("detail_identifier_size") : "3" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='detail_identifier_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
              <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
 		  </td>

 		  <td >&nbsp;</td>
 	  </tr>
 	  <tr>
 		 <td nowrap width="4">&nbsp</td>
 		 <td  >
 		 <span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("", "PaymentDefinitions.Amount",
					false, true, false, "none")%>
 		  </td>
 		  <td align="right">
              <%
                  String amountSize = paymentDef.getAttribute("payment_amount_size");
                  if (StringFunction.isNotBlank(amountSize)){
                      int amtSize = new Integer(amountSize).intValue();
                      amountSize = (amtSize-4) + ",3";
                  }
              %>
              <input type="text" name="payment_amount_size" value='<%=StringFunction.isNotBlank(amountSize)? amountSize : "15,3" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='payment_amount_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />

 		  </td>
 		  <td >&nbsp;</td>
 	  </tr>
 	  <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.customer_reference", TradePortalConstants.TEXT_BUNDLE)%>" name="fcustomer_reference" id="fcustomer_reference">

              <%=widgetFactory.createCheckboxField(
					"fcustomer_reference_req",
					"",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("customer_reference_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td  >
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.CustomerReference", false, false, false, "none")%>
 		</td>
        <td align="right">
            <input type="text" name="customer_reference_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("customer_reference_size"))? paymentDef.getAttribute("customer_reference_size") : "20" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='customer_reference_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />

 		  </td>
        <td align='center'>
            <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.customer_reference", TradePortalConstants.TEXT_BUNDLE)%>" name="fcustomer_reference" id="fcustomer_reference">

              <%=widgetFactory.createCheckboxField(
					"fcustomer_reference_data_req",
					"",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("customer_reference_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>

      <tr>
          <td nowrap width="4">&nbsp</td>
          <td  >
              <span class="asterisk">*</span>
              <%=widgetFactory.createLabel("", "PaymentDefinitions.BeneficiaryName",
					false, true, false, "none")%>
          </td>
          <td align="right">
              <input type="text" name="beneficiary_name_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("beneficiary_name_size"))? paymentDef.getAttribute("beneficiary_name_size") : "120" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='beneficiary_name_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
          </td>
          <td >&nbsp;</td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_account", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_account_number" id="fbene_account_number">

              <%=widgetFactory.createCheckboxField(
					"fbene_account_number_req",
					"",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("bene_account_number_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <%--  KMehta IR T36000026012 @Rel8400 12 Mar 2014 added id --%> 
          <td  id="benAcct">          
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneficiaryAcct", false, false, false, "none")%>
          </td>
          <td align="right">
              <input type="text" name="bene_account_number_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_account_number_size"))? paymentDef.getAttribute("bene_account_number_size") : "34" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='bene_account_number_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_account", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_account_number" id="fbene_account_number">

              <%=widgetFactory.createCheckboxField(
					"fbene_account_number_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_account_number_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>

       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address_line1", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_address1" id="fbene_address1">

              <%=widgetFactory.createCheckboxField(
					"fbene_address1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address1_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td id="td_fbene_address1">
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneAddressLine1", false, false, false, "none")%>
 		</td>
       <td align="right">
           <input type="text" name="bene_address1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_address1_size"))? paymentDef.getAttribute("bene_address1_size") : "35" %>'
                  data-dojo-type="dijit.form.TextBox"
                  id='bene_address1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
 	   </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address_line1", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_address1" id="fbene_address1">

              <%=widgetFactory.createCheckboxField(
					"fbene_address1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address1_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address_line2", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_address2" id="fbene_address2">

              <%=widgetFactory.createCheckboxField(
					"fbene_address2_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address2_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneAddressLine2", false, false, false,
					"none")%>
 		</td>
       <td align="right">
           <input type="text" name="bene_address2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_address2_size"))? paymentDef.getAttribute("bene_address2_size") : "35" %>'
                  data-dojo-type="dijit.form.TextBox"
                  id='bene_address2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address_line2", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_address2" id="fbene_address2">

              <%=widgetFactory.createCheckboxField(
					"fbene_address2_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address2_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address_line3", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_address3" id="fbene_address3">

              <%=widgetFactory.createCheckboxField(
					"fbene_address3_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address3_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneAddressLine3", false, false,
					false, "none")%>
 		</td>
        <td align="right">
            <input type="text" name="bene_address3_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_address3_size"))? paymentDef.getAttribute("bene_address3_size") : "35" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='bene_address3_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address_line3", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_address3" id="fbene_address3">

              <%=widgetFactory.createCheckboxField(
					"fbene_address3_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address3_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address_line4", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_address4" id="fbene_address4">

              <%=widgetFactory.createCheckboxField(
					"fbene_address4_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address4_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("", "PaymentDefinitions.BeneAddressLine4",
					false, false, false, "none")%>
 		</td>
        <td align="right">
            <input type="text" name="bene_address4_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_address4_size"))? paymentDef.getAttribute("bene_address4_size") : "35" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='bene_address4_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
 			
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address_line4", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_address4" id="fbene_address4">

              <%=widgetFactory.createCheckboxField(
					"fbene_address4_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address4_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_country", TradePortalConstants.TEXT_BUNDLE)%>" name="fbeneficiary_country" id="fbeneficiary_country">

              <%=widgetFactory.createCheckboxField(
					"fbeneficiary_country_req",
					"",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("beneficiary_country_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td  id="td_fbeneficiary_country">
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneCountry", false, false, false, "none")%>
          </td>
          <td align="right">
              <input type="text" name="beneficiary_country_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("beneficiary_country_size"))? paymentDef.getAttribute("beneficiary_country_size") : "2" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='beneficiary_country_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
             
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_country", TradePortalConstants.TEXT_BUNDLE)%>" name="fbeneficiary_country" id="fbeneficiary_country">

              <%=widgetFactory.createCheckboxField(
					"fbeneficiary_country_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("beneficiary_country_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_fax_no", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_fax_no" id="fbene_fax_no">

              <%=widgetFactory.createCheckboxField(
					"fbene_fax_no_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_fax_no_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneFaxNo", false, false, false, "none")%>
 		</td>
        <td align="right">
            <input type="text" name="bene_fax_no_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_fax_no_size"))? paymentDef.getAttribute("bene_fax_no_size") : "15" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='bene_fax_no_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
 			
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_fax_no", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_fax_no" id="fbene_fax_no">

              <%=widgetFactory.createCheckboxField(
					"fbene_fax_no_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_fax_no_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_email_id", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_email_id" id="fbene_email_id">

              <%=widgetFactory.createCheckboxField(
					"fbene_email_id_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_email_id_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory
					.createLabel("", "PaymentDefinitions.BeneEmailId", false,
							false, false, "none")%>
 		</td>
        <td align="right">
            <input type="text" name="bene_email_id_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_email_id_size"))? paymentDef.getAttribute("bene_email_id_size") : "255" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='bene_email_id_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
 			
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_email_id", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_email_id" id="fbene_email_id">

              <%=widgetFactory.createCheckboxField(
					"fbene_email_id_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_email_id_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bank_branch_code_swift", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bank_branch_code" id="fbene_bank_branch_code">

              <%=widgetFactory.createCheckboxField(
                      "fbene_bank_branch_code_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("bene_bank_branch_code_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td id="td_fbene_bank_branch_code">
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.bene_bank_branch_code_swift", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="bene_bank_branch_code_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_bank_branch_code_size"))? paymentDef.getAttribute("bene_bank_branch_code_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='bene_bank_branch_code_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
             
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bank_branch_code_swift", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bank_branch_code" id="fbene_bank_branch_code">

              <%=widgetFactory.createCheckboxField(
                      "fbene_bank_branch_code_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("bene_bank_branch_code_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
 	    <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bank_name", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bank_name" id="fbene_bank_name">

              <%=widgetFactory.createCheckboxField(
					"fbene_bank_name_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bank_name_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneBankName", false, false, false,
					"none")%>
 		</td>
       <td align="right">
           <input type="text" name="bene_bank_name_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_bank_name_size"))? paymentDef.getAttribute("bene_bank_name_size") : "35" %>'
                  data-dojo-type="dijit.form.TextBox"
                  id='bene_bank_name_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
 			
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bank_name", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bank_name" id="fbene_bank_name">

              <%=widgetFactory.createCheckboxField(
					"fbene_bank_name_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bank_name_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_name" id="fbene_bnk_brnch_name">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_name_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_name_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_brnch", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="bene_bnk_brnch_name_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_bnk_brnch_name_size"))? paymentDef.getAttribute("bene_bnk_brnch_name_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='bene_bnk_brnch_name_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_name", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_name" id="fbene_bnk_brnch_name">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_name_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_name_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_addr_line1", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_addr1" id="fbene_bnk_brnch_addr1">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_addr1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_addr1_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_brnch_addr_line1", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="bene_bnk_brnch_addr1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_bnk_brnch_addr1_size"))? paymentDef.getAttribute("bene_bnk_brnch_addr1_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='bene_bnk_brnch_addr1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_addr_line1", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_addr1" id="fbene_bnk_brnch_addr1">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_addr1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_addr1_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_addr_line2", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_addr2" id="fbene_bnk_brnch_addr2">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_addr2_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_addr2_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_brnch_addr_line2", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="bene_bnk_brnch_addr2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_bnk_brnch_addr2_size"))? paymentDef.getAttribute("bene_bnk_brnch_addr2_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='bene_bnk_brnch_addr2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
             
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_addr_line2", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_addr2" id="fbene_bnk_brnch_addr2">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_addr2_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_addr2_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_city", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_city" id="fbene_bnk_brnch_city">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_city_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_city_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_city", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="bene_bnk_brnch_city_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_bnk_brnch_city_size"))? paymentDef.getAttribute("bene_bnk_brnch_city_size") : "31" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='bene_bnk_brnch_city_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
              
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_city", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_city" id="fbene_bnk_brnch_city">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_city_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_city_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_prvnc", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_prvnc" id="fbene_bnk_brnch_prvnc">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_prvnc_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_prvnc_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_prvnc", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="bene_bnk_brnch_prvnc_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_bnk_brnch_prvnc_size"))? paymentDef.getAttribute("bene_bnk_brnch_prvnc_size") : "8" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='bene_bnk_brnch_prvnc_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
              
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_prvnc", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_prvnc" id="fbene_bnk_brnch_prvnc">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_prvnc_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_prvnc_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_cntry", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_cntry" id="fbene_bnk_brnch_cntry">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_cntry_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_cntry_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_cntry", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="bene_bnk_brnch_cntry_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("bene_bnk_brnch_cntry_size"))? paymentDef.getAttribute("bene_bnk_brnch_cntry_size") : "2" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='bene_bnk_brnch_cntry_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
             
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_cntry", TradePortalConstants.TEXT_BUNDLE)%>" name="fbene_bnk_brnch_cntry" id="fbene_bnk_brnch_cntry">

              <%=widgetFactory.createCheckboxField(
					"fbene_bnk_brnch_cntry_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_cntry_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.charges", TradePortalConstants.TEXT_BUNDLE)%>" name="fcharges" id="fcharges">

              <%=widgetFactory.createCheckboxField(
                      "fcharges_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("charges_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.Charges", false, false, false, "none")%>
          </td>
          <td align="right">
              <input type="hidden" name="charges_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("charges_size"))? paymentDef.getAttribute("charges_size") : "3" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='charges_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
              <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.charges", TradePortalConstants.TEXT_BUNDLE)%>" name="fcharges" id="fcharges">

              <%=widgetFactory.createCheckboxField(
                      "fcharges_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("charges_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.payable_location", TradePortalConstants.TEXT_BUNDLE)%>" name="fpayable_location" id="fpayable_location">

              <%=widgetFactory.createCheckboxField(
					"fpayable_location_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("payable_location_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td id="td_fpayable_location">
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.payable_location", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="payable_location_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("payable_location_size"))? paymentDef.getAttribute("payable_location_size") : "20" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='payable_location_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
             
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.payable_location", TradePortalConstants.TEXT_BUNDLE)%>" name="fpayable_location" id="fpayable_location">

              <%=widgetFactory.createCheckboxField(
					"fpayable_location_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("payable_location_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.print_location", TradePortalConstants.TEXT_BUNDLE)%>" name="fprint_location" id="fprint_location">

              <%=widgetFactory.createCheckboxField(
					"fprint_location_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("print_location_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td id="td_fprint_location">
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.print_location", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="print_location_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("print_location_size"))? paymentDef.getAttribute("print_location_size") : "20" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='print_location_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
              
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.print_location", TradePortalConstants.TEXT_BUNDLE)%>" name="fprint_location" id="fprint_location">

              <%=widgetFactory.createCheckboxField(
					"fprint_location_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("print_location_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.delvry_mth", TradePortalConstants.TEXT_BUNDLE)%>" name="fdelvry_mth_n_delvrto" id="fdelvry_mth_n_delvrto">

              <%=widgetFactory.createCheckboxField(
					"fdelvry_mth_n_delvrto_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("delvry_mth_n_delvrto_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td id="td_fdelvry_mth_n_delvrto">
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.delvry_mth", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="hidden" name="delvry_mth_n_delvrto_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("delvry_mth_n_delvrto_size"))? paymentDef.getAttribute("delvry_mth_n_delvrto_size") : "2" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='delvry_mth_n_delvrto_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
              <%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.delvry_mth", TradePortalConstants.TEXT_BUNDLE)%>" name="fdelvry_mth_n_delvrto" id="fdelvry_mth_n_delvrto">

              <%=widgetFactory.createCheckboxField(
					"fdelvry_mth_n_delvrto_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("delvry_mth_n_delvrto_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address_line1", TradePortalConstants.TEXT_BUNDLE)%>" name="fmailing_address1" id="fmailing_address1">

              <%=widgetFactory.createCheckboxField(
					"fmailing_address1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address1_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.mailing_address_line1", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="mailing_address1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("mailing_address1_size"))? paymentDef.getAttribute("mailing_address1_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='mailing_address1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
             
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address_line1", TradePortalConstants.TEXT_BUNDLE)%>" name="fmailing_address1" id="fmailing_address1">

              <%=widgetFactory.createCheckboxField(
					"fmailing_address1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address1_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address_line2", TradePortalConstants.TEXT_BUNDLE)%>" name="fmailing_address2" id="fmailing_address2">

              <%=widgetFactory.createCheckboxField(
					"fmailing_address2_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address2_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.mailing_address_line2", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="mailing_address2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("mailing_address2_size"))? paymentDef.getAttribute("mailing_address2_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='mailing_address2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
           
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address_line2", TradePortalConstants.TEXT_BUNDLE)%>" name="fmailing_address2" id="fmailing_address2">

              <%=widgetFactory.createCheckboxField(
					"fmailing_address2_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address2_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address_line3", TradePortalConstants.TEXT_BUNDLE)%>" name="fmailing_address3" id="fmailing_address3">

              <%=widgetFactory.createCheckboxField(
					"fmailing_address3_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address3_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.mailing_address_line3", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="mailing_address3_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("mailing_address3_size"))? paymentDef.getAttribute("mailing_address3_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='mailing_address3_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
             
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address_line3", TradePortalConstants.TEXT_BUNDLE)%>" name="fmailing_address3" id="fmailing_address3">

              <%=widgetFactory.createCheckboxField(
					"fmailing_address3_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address3_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address_line4", TradePortalConstants.TEXT_BUNDLE)%>" name="fmailing_address4" id="fmailing_address4">

              <%=widgetFactory.createCheckboxField(
					"fmailing_address4_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address4_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.mailing_address_line4", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="mailing_address4_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("mailing_address4_size"))? paymentDef.getAttribute("mailing_address4_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='mailing_address4_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
          
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address_line4", TradePortalConstants.TEXT_BUNDLE)%>" name="fmailing_address4" id="fmailing_address4">

              <%=widgetFactory.createCheckboxField(
					"fmailing_address4_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address4_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.instruction_number", TradePortalConstants.TEXT_BUNDLE)%>" name="finstruction_number" id="finstruction_number">

              <%=widgetFactory.createCheckboxField(
					"finstruction_number_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("instruction_number_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.instruction_number", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="instruction_number_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("instruction_number_size"))? paymentDef.getAttribute("instruction_number_size") : "10" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='instruction_number_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
              
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.instruction_number", TradePortalConstants.TEXT_BUNDLE)%>" name="finstruction_number" id="finstruction_number">

              <%=widgetFactory.createCheckboxField(
					"finstruction_number_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("instruction_number_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.details_of_payment", TradePortalConstants.TEXT_BUNDLE)%>" name="fdetails_of_payment" id="fdetails_of_payment">

              <%=widgetFactory.createCheckboxField(
                      "fdetails_of_payment_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("details_of_payment_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.details_of_payment", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="details_of_payment_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("details_of_payment_size"))? paymentDef.getAttribute("details_of_payment_size") : "140" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='details_of_payment_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
          
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.details_of_payment", TradePortalConstants.TEXT_BUNDLE)%>" name="fdetails_of_payment" id="fdetails_of_payment">

              <%=widgetFactory.createCheckboxField(
                      "fdetails_of_payment_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("details_of_payment_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_cd", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_cd" id="ff_int_bnk_brnch_cd">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_cd_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_cd_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.int_bnk_brnch_cd", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="f_int_bnk_brnch_cd_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("f_int_bnk_brnch_cd_size"))? paymentDef.getAttribute("f_int_bnk_brnch_cd_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='f_int_bnk_brnch_cd_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
             
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_cd", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_cd" id="ff_int_bnk_brnch_cd">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_cd_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_cd_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_name", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_name" id="ff_int_bnk_name">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_name_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_name_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.int_bnk_name", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="f_int_bnk_name_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("f_int_bnk_name_size"))? paymentDef.getAttribute("f_int_bnk_name_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='f_int_bnk_name_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_name", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_name" id="ff_int_bnk_name">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_name_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_name_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_nm", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_nm" id="ff_int_bnk_brnch_nm">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_nm_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_nm_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.int_bnk_brnch_nm", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="f_int_bnk_brnch_nm_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("f_int_bnk_brnch_nm_size"))? paymentDef.getAttribute("f_int_bnk_brnch_nm_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='f_int_bnk_brnch_nm_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_nm", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_nm" id="ff_int_bnk_brnch_nm">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_nm_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_nm_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_addr1", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_addr1" id="ff_int_bnk_brnch_addr1">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_addr1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_addr1_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.int_bnk_brnch_addr1", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="f_int_bnk_brnch_addr1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("f_int_bnk_brnch_addr1_size"))? paymentDef.getAttribute("f_int_bnk_brnch_addr1_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='f_int_bnk_brnch_addr1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
              
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_addr1", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_addr1" id="ff_int_bnk_brnch_addr1">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_addr1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_addr1_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_addr2", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_addr2" id="ff_int_bnk_brnch_addr2">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_addr2_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_addr2_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.int_bnk_brnch_addr2", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="f_int_bnk_brnch_addr2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("f_int_bnk_brnch_addr2_size"))? paymentDef.getAttribute("f_int_bnk_brnch_addr2_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='f_int_bnk_brnch_addr2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
           
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_addr2", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_addr2" id="ff_int_bnk_brnch_addr2">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_addr2_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_addr2_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_city", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_city" id="ff_int_bnk_brnch_city">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_city_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_city_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.int_bnk_brnch_city", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="f_int_bnk_brnch_city_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("f_int_bnk_brnch_city_size"))? paymentDef.getAttribute("f_int_bnk_brnch_city_size") : "31" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='f_int_bnk_brnch_city_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
             
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_city", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_city" id="ff_int_bnk_brnch_city">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_city_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_city_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_prvnc", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_prvnc" id="ff_int_bnk_brnch_prvnc">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_prvnc_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_prvnc_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.int_bnk_brnch_prvnc", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="f_int_bnk_brnch_prvnc_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("f_int_bnk_brnch_prvnc_size"))? paymentDef.getAttribute("f_int_bnk_brnch_prvnc_size") : "8" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='f_int_bnk_brnch_prvnc_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_prvnc", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_prvnc" id="ff_int_bnk_brnch_prvnc">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_prvnc_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_prvnc_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_cntry", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_cntry" id="ff_int_bnk_brnch_cntry">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_cntry_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_cntry_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.int_bnk_brnch_cntry", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="f_int_bnk_brnch_cntry_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("f_int_bnk_brnch_cntry_size"))? paymentDef.getAttribute("f_int_bnk_brnch_cntry_size") : "2" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='f_int_bnk_brnch_cntry_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.int_bnk_brnch_cntry", TradePortalConstants.TEXT_BUNDLE)%>" name="ff_int_bnk_brnch_cntry" id="ff_int_bnk_brnch_cntry">

              <%=widgetFactory.createCheckboxField(
					"ff_int_bnk_brnch_cntry_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_cntry_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep_line1", TradePortalConstants.TEXT_BUNDLE)%>" name="fcentral_bank_rep1" id="fcentral_bank_rep1">

              <%=widgetFactory.createCheckboxField(
					"fcentral_bank_rep1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep1_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td id="td_fcentral_bank_rep1">
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.central_bank_rep_line1", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="central_bank_rep1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("central_bank_rep1_size"))? paymentDef.getAttribute("central_bank_rep1_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='central_bank_rep1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep_line1", TradePortalConstants.TEXT_BUNDLE)%>" name="fcentral_bank_rep1" id="fcentral_bank_rep1">

              <%=widgetFactory.createCheckboxField(
					"fcentral_bank_rep1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep1_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep_line2", TradePortalConstants.TEXT_BUNDLE)%>" name="fcentral_bank_rep2" id="fcentral_bank_rep2">

              <%=widgetFactory.createCheckboxField(
					"fcentral_bank_rep2_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep2_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.central_bank_rep_line2", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="central_bank_rep2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("central_bank_rep2_size"))? paymentDef.getAttribute("central_bank_rep2_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='central_bank_rep2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
           
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep_line2", TradePortalConstants.TEXT_BUNDLE)%>" name="fcentral_bank_rep2" id="fcentral_bank_rep2">

              <%=widgetFactory.createCheckboxField(
					"fcentral_bank_rep2_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep2_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep_line3", TradePortalConstants.TEXT_BUNDLE)%>" name="fcentral_bank_rep3" id="fcentral_bank_rep3">

              <%=widgetFactory.createCheckboxField(
					"fcentral_bank_rep3_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep3_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.central_bank_rep_line3", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="central_bank_rep3_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("central_bank_rep3_size"))? paymentDef.getAttribute("central_bank_rep3_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='central_bank_rep3_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep_line3", TradePortalConstants.TEXT_BUNDLE)%>" name="fcentral_bank_rep3" id="fcentral_bank_rep3">

              <%=widgetFactory.createCheckboxField(
					"fcentral_bank_rep3_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep3_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.reporting_code1", TradePortalConstants.TEXT_BUNDLE)%>" name="freporting_code11" id="freporting_code1">

              <%=widgetFactory.createCheckboxField(
                      "freporting_code1_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("reporting_code1_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td id="td_freporting_code1">
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.reporting_code1", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="reporting_code1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("reporting_code1_size"))? paymentDef.getAttribute("reporting_code1_size") : "3" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='reporting_code1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
           
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.reporting_code1", TradePortalConstants.TEXT_BUNDLE)%>" name="freporting_code1" id="freporting_code1">

              <%=widgetFactory.createCheckboxField(
                      "freporting_code1_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("reporting_code1_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.reporting_code2", TradePortalConstants.TEXT_BUNDLE)%>" name="freporting_code2" id="freporting_code2">

              <%=widgetFactory.createCheckboxField(
                      "freporting_code2_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("reporting_code2_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td id="td_freporting_code2">
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.reporting_code2", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="reporting_code2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("reporting_code2_size"))? paymentDef.getAttribute("reporting_code2_size") : "3" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='reporting_code2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
           
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.reporting_code2", TradePortalConstants.TEXT_BUNDLE)%>" name="freporting_code2" id="freporting_code2">

              <%=widgetFactory.createCheckboxField(
                      "freporting_code2_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("reporting_code2_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def1", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def1" id="fusers_def1">

              <%=widgetFactory.createCheckboxField(
					"fusers_def1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("users_def1_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.users_def1", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <input type="text" name="users_def1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("users_def1_size"))? paymentDef.getAttribute("users_def1_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='users_def1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def1", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def1" id="fusers_def1">

              <%=widgetFactory.createCheckboxField(
					"fusers_def1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("users_def1_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def2", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def2" id="fusers_def2">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def2_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def2_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.users_def2", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="users_def2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("users_def2_size"))? paymentDef.getAttribute("users_def2_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='users_def2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
           
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def2", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def2" id="fusers_def2">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def2_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def2_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def3", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def3" id="fusers_def3">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def3_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def3_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.users_def3", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="users_def3_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("users_def3_size"))? paymentDef.getAttribute("users_def3_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='users_def3_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
          
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def3", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def3" id="fusers_def3">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def3_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def3_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def4", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def4" id="fusers_def4">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def4_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def4_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.users_def4", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="users_def4_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("users_def4_size"))? paymentDef.getAttribute("users_def4_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='users_def4_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
           
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def4", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def4" id="fusers_def4">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def4_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def4_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def5", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def5" id="fusers_def5">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def5_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def5_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.users_def5", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="users_def5_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("users_def5_size"))? paymentDef.getAttribute("users_def5_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='users_def5_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
           
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def5", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def5" id="fusers_def5">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def5_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def5_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def6", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def6" id="fusers_def6">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def6_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def6_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.users_def6", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="users_def6_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("users_def6_size"))? paymentDef.getAttribute("users_def6_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='users_def6_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
           
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def6", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def6" id="fusers_def6">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def6_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def6_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def7", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def7" id="fusers_def7">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def7_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def7_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.users_def7", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="users_def7_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("users_def7_size"))? paymentDef.getAttribute("users_def7_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='users_def7_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def7", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def7" id="fusers_def7">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def7_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def7_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def8", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def8" id="fusers_def8">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def8_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def8_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.users_def8", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="users_def8_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("users_def8_size"))? paymentDef.getAttribute("users_def8_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='users_def8_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
             
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def8", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def8" id="fusers_def8">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def8_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def8_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def9", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def9" id="fusers_def9">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def9_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def9_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.users_def9", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="users_def9_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("users_def9_size"))? paymentDef.getAttribute("users_def9_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='users_def9_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
 
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def9", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def9" id="fusers_def9">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def9_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def9_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def10", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def10" id="fusers_def10">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def10_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def10_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.users_def10", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <input type="text" name="users_def10_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("users_def10_size"))? paymentDef.getAttribute("users_def10_size") : "35" %>'
                     data-dojo-type="dijit.form.TextBox"
                     id='users_def10_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
          
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.users_def10", TradePortalConstants.TEXT_BUNDLE)%>" name="fusers_def10" id="fusers_def10">

              <%=widgetFactory.createCheckboxField(
                      "fusers_def10_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("users_def10_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
</tbody>
</table>



<table>

     <tr style="height:30px">
     <td>&nbsp</td>
     </tr>
</table>

    <table class="formDocumentsTable" id="PmtFFDefInvTableId" style="width:98%;">
        <thead>
        <tr style="height:35px;">
            <th colspan=4 style="text-align:left;">
                <%=widgetFactory
                        .createSubLabel("PaymentDefinitions.FFInvoiceFields")%>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr style="height:35px">
            <td width="15" align='center' >
                <b><%=resMgr.getText("PaymentDefinitions.ReqField", TradePortalConstants.TEXT_BUNDLE)%></b>
            </td>
            <td nowrap width="130"  >
                <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
            </td>
            <td  width="20" align="center" >
                <b><%=resMgr.getText("PaymentDefinitions.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
            </td>
            <td width="30" >
                <b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("PaymentDefinitions.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
            </td>
        </tr>
        <tr>
            <td align='center'>
                <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.inv_details_header", TradePortalConstants.TEXT_BUNDLE)%>" name="iinv_details_header" id="iinv_details_header">

                <%=widgetFactory.createCheckboxField(
                        "iinv_details_header_req",
                        "",
                        TradePortalConstants.INDICATOR_YES.equals(
                                paymentDef.getAttribute("inv_details_header_req")), isReadOnly,
                        false, "", "", "none")%>
            </td>
            <td  >
                <%=widgetFactory.createLabel("",
                        "PaymentDefinitions.InvoiceHeader", false, false, false, "none")%>
            </td>
            <td align="right">
                <input type="hidden" name="inv_details_header_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("inv_details_header_size"))? paymentDef.getAttribute("inv_details_header_size") : "3" %>'
                       data-dojo-type="dijit.form.TextBox"
                       id='inv_details_header_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
                <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
            </td>
            <td align='center'>
                <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.inv_details_header", TradePortalConstants.TEXT_BUNDLE)%>" name="iinv_details_header" id="iinv_details_header">

                <%=widgetFactory.createCheckboxField(
                        "iinv_details_header_data_req",
                        "",
                        TradePortalConstants.INDICATOR_YES.equals(
                                paymentDef.getAttribute("inv_details_header_data_req")), isReadOnly,
                        false, "", "", "none")%>
            </td>
        </tr>
        <tr>
            <td align='center'>
                <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.inv_details_lineitm", TradePortalConstants.TEXT_BUNDLE)%>" name="iinv_details_lineitm" id="iinv_details_lineitm">

                <%=widgetFactory.createCheckboxField(
                        "iinv_details_lineitm_req",
                        "",
                        TradePortalConstants.INDICATOR_YES.equals(
                                paymentDef.getAttribute("inv_details_lineitm_req")), isReadOnly,
                        false, "", "", "none")%>
            </td>
            <td  >
                <%=widgetFactory.createLabel("",
                        "PaymentDefinitions.inv_details_lineitm", false, false, false, "none")%>
            </td>
            <td align="right">
         
                <input type="hidden" name="inv_details_lineitm_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("inv_details_lineitm_size"))? paymentDef.getAttribute("inv_details_lineitm_size") : "80" %>'
                       data-dojo-type="dijit.form.TextBox"
                       id='inv_details_lineitm_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
                <%= widgetFactory.createLabel("", "80", false, false, false, "none") %>
            </td>
            <td align='center'>
                <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.inv_details_lineitm", TradePortalConstants.TEXT_BUNDLE)%>" name="iinv_details_lineitm" id="iinv_details_lineitm">

                <%=widgetFactory.createCheckboxField(
                        "iinv_details_lineitm_data_req",
                        "",
                        TradePortalConstants.INDICATOR_YES.equals(
                                paymentDef.getAttribute("inv_details_lineitm_data_req")), isReadOnly,
                        false, "", "", "none")%>
            </td>
        </tr>
        </tbody>
    </table>


</div>

<div class = "columnRight">

<%=widgetFactory.createLabel("",
					"PaymentDefinitionDetail.DefSep2", false, false, false,
					"none")%>
<table>
    <tr style="height:57px;"><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
</table>
<table class="formDocumentsTable" id="PmtFFHdrDefFileOrderTableId" >
<thead>
	<tr style="height:35px">
		<th colspan=3 style="text-align:left">
			<%=widgetFactory
					.createSubLabel("PaymentDefinitionDetail.HdrOrder")%>
		</th>
	</tr>
</thead>

<tbody>

      <tr style="height:35px">
 		 <td width="20" >
		  	<%=widgetFactory.createLabel("",
					"POStructureDefinition.Order", false, false, false, "none")%>
 		  </td>
 		  <td width="160">
		  	<%=widgetFactory.createLabel("",
					"POStructureDefinition.FieldName", false, false, false, "none")%>
 		  </td>
          <td width="20">
              <%=widgetFactory.createLabel("",
					"POStructureDefinition.Position", false, false, false, "none")%>
          </td>
       </tr>

<%
//RKAZI Rel 8.2 T36000011727 - START
//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
//NOTE: please do not reformat the code.
	String[] initialValFFHdr = { "header_identifier", "debit_account_number", "execution_date", "payment_currency", "payment_method" };
	boolean initialFlagFFHdr = false;
	if (InstrumentServices.isBlank(paymentDef
			.getAttribute("pmt_hdr_summary_order1")))
		initialFlagFFHdr = true;
	String currentValueFFHdr;
    counterTmp = 1;
    int positionHdr = 1;
	if (initialFlagFFHdr) {
		for (counterTmp = 1; counterTmp <= initialValFFHdr.length; counterTmp++) {

			currentValueFFHdr = initialValFFHdr[counterTmp - 1];		
			if (counterTmp ==1){
			    positionHdr = 1;
			}else{
			    positionHdr = positionHdr + ((Integer)sizeMap.get(initialValFFHdr[counterTmp - 2]+"_size")).intValue() ;			
			}
%>

 	  <tr id= "h<%=currentValueFFHdr%>_req_order">
 		 <td  align="center">
   			<input type="text" name="" value='<%=counterTmp%>'
   				data-dojo-type="dijit.form.TextBox"
                    id='htext<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px;" <%= (isReadOnly ? "readOnly=true" : " ") %> /><input type="hidden" id="hupload_order_<%=counterTmp%>" name="hpmt_hdr_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFHdr%>" />
 		  </td>
          <td id='hfileFieldName<%=counterTmp%>' >
			<%=widgetFactory.createLabel("",
							"PaymentUploadRequest." + currentValueFFHdr, false,
							false, false, "none")%>

 		  </td>
          <td  align="right">
              <input type="text" name="" value='<%=positionHdr%>'
                     data-dojo-type="dijit.form.TextBox"
                     id='hposition<%=counterTmp%>'  style="width: 30px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField" /><input type="hidden" id="hsize<%=counterTmp%>" name="hsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFHdr%>" />
          </td>
	  </tr>
<%
	    }
	} else {	
		counterTmp = 1;
		positionHdr = 1;
		// invoice summary data in upload invoice file has maximum 28 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order9)
		while ((counterTmp <= 10)
				&& InstrumentServices
						.isNotBlank(paymentDef
								.getAttribute("pmt_hdr_summary_order"
										+ (new Integer(counterTmp))
												.toString()))) {

			currentValueFFHdr = paymentDef.getAttribute("pmt_hdr_summary_order"
					+ (new Integer(counterTmp)).toString());
            if (counterTmp == 1){
                positionHdr = 1;
            } else{
                String tmpSizeField = paymentDef.getAttribute("pmt_hdr_summary_order"
					+ (new Integer(counterTmp - 1)).toString());
                String fSize =   paymentDef.getAttribute(tmpSizeField+"_size");
                if(StringFunction.isBlank(fSize)) {
                    fSize = (String)sizeMap.get(tmpSizeField+"_size");
                }
                positionHdr = positionHdr + (new Integer(fSize)).intValue();
            }
%>

   	   <tr id= "h<%=currentValueFFHdr%>_req_order">
 		 <td  align="center">
   			<input type="text" name="" value='<%=counterTmp%>'
   				data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
   				id='htext<%=counterTmp%>'  style="width: 25px;" <%= (isReadOnly ? "readOnly=true" : " ") %> /><input type="hidden" id="hupload_order_<%=counterTmp%>" name="hpmt_hdr_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFHdr%>" />
 		  </td>
          <td id='hfileFieldName<%=counterTmp%>' align="left">
			<%=widgetFactory.createLabel("",
							"PaymentUploadRequest." + currentValueFFHdr, false,
							false, false,"labelClass=\"invOrderLabel\"", "none")%>
			<% if(pmtSummOptionalVal.contains(currentValueFFHdr) && !pmtSummReqVal.contains(currentValueFFHdr)){ %>
			<span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='h<%=currentValueFFHdr%>'></span>
           <%} %>
 		  </td>
           <td  align="right">
               <input type="text" name="" value='<%=positionHdr%>'
                      data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                      id='hposition<%=counterTmp%>'  style="width: 30px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type="hidden" id="hsize<%=counterTmp%>" name="hsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFHdr%>" />
           </td>
	   </tr>

<%
	counterTmp++;
		}
	}
%>
</tbody>
</table>
<br/>
<div>

<%
	invSummDataOrder = counterTmp;
	if (!(isReadOnly)) {
%>
<button data-dojo-type="dijit.form.Button" type="button" id="hupdateSysDefOrder"><%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		updateFFHdrDefOrder(document.getElementById("PmtFFHdrDefFileOrderTableId").rows.length, 'htext');
	</script>
</button>
<%
	} else {
%>
					&nbsp;
<%
	}
%>

</div>
<table>

     <tr style="height:30px">
     <td>&nbsp</td>
     </tr>
</table>


<table class="formDocumentsTable" id="PmtFFDefFileOrderTableId" >
    <thead>
    <tr style="height:35px">
        <th colspan=3 style="text-align:left">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitionDetail.DetailOrder")%>
        </th>
    </tr>
    </thead>

    <tbody>

    <tr style="height:35px">
        <td width="20" >
            <%=widgetFactory.createLabel("",
                    "POStructureDefinition.Order", false, false, false, "none")%>
        </td>
        <td width="160">
            <%=widgetFactory.createLabel("",
                    "POStructureDefinition.FieldName", false, false, false, "none")%>
        </td>
        <td width="20" >
            <%=widgetFactory.createLabel("",
                    "POStructureDefinition.Position", false, false, false, "none")%>
        </td>
    </tr>

    <%
        //RKAZI Rel 8.2 T36000011727 - START
//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
//NOTE: please do not reformat the code.
        String[] initialValFF = { "detail_identifier",  "payment_amount", "beneficiary_name" };
        boolean initialFlagFF = false;
        if (InstrumentServices.isBlank(paymentDef
                .getAttribute("pmt_pay_summary_order1")))
            initialFlagFF = true;
        String currentValueFF;
        counterTmp = 1;
        int positionPay = 1;
        if (initialFlagFF) {
            for (counterTmp = 1; counterTmp <= initialValFF.length; counterTmp++) {
                currentValueFF = initialValFF[counterTmp - 1];
                if (counterTmp ==1){
                    positionPay = 1;
                }else{
                    positionPay = positionPay + ((Integer)sizeMap.get(initialValFF[counterTmp - 2]+"_size")).intValue() ;
                }

    %>

    <tr id= "f<%=currentValueFF%>_req_order">
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='ftext<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px;" <%= (isReadOnly ? "readOnly=true" : " ") %> /><input type="hidden" id="fupload_order_<%=counterTmp%>" name="fpmt_pay_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFF%>" />
        </td>
        <td id='ffileFieldName<%=counterTmp%>' >
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + currentValueFF, false,
                    false, false, "none")%>

        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionPay%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fposition<%=counterTmp%>'  style="width: 30px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField" /><input type="hidden" id="fsize<%=counterTmp%>" name="fsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFF%>" />
        </td>
    </tr>
    <%
        }
    } else {
        counterTmp = 1;
        positionPay = 1;
        // invoice summary data in upload invoice file has maximum 28 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order9)
        while ((counterTmp <= 54)
                && InstrumentServices
                .isNotBlank(paymentDef
                        .getAttribute("pmt_pay_summary_order"
                                + (new Integer(counterTmp))
                                .toString()))) {

            currentValueFF = paymentDef.getAttribute("pmt_pay_summary_order" + (new Integer(counterTmp)).toString());
            if (counterTmp == 1){
                positionPay = 1;
            } else{
                String tmpSizeField = paymentDef.getAttribute("pmt_pay_summary_order"+ (new Integer(counterTmp - 1)).toString());
                String fSize =   paymentDef.getAttribute(tmpSizeField+"_size");
                if(StringFunction.isBlank(fSize)) {
                    fSize = (String)sizeMap.get(tmpSizeField+"_size");
                }

                positionPay = positionPay + (new Integer(fSize)).intValue();
            }

    %>

    <tr id= "f<%=currentValueFF%>_req_order">
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='ftext<%=counterTmp%>'  style="width: 25px;" <%= (isReadOnly ? "readOnly=true" : " ") %> /><input type="hidden" id="fupload_order_<%=counterTmp%>" name="fpmt_pay_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFF%>" />
        </td>
        <td id='ffileFieldName<%=counterTmp%>' align="left">
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + currentValueFF, false,
                    false, false,"labelClass=\"invOrderLabel\"", "none")%>
            <% if(pmtSummOptionalVal.contains(currentValueFF)){ %>
            <span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='f<%=currentValueFF%>'></span>
            <%} %>
        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionPay%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='fposition<%=counterTmp%>'  style="width: 30px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type="hidden" id="fsize<%=counterTmp%>" name="fsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFF%>" />
        </td>
    </tr>

    <%
                counterTmp++;
            }
        }
    %>
    </tbody>
</table>
<br/>
<div>

    <%
        invSummDataOrder = counterTmp;
        if (!(isReadOnly)) {
    %>
    <button data-dojo-type="dijit.form.Button" type="button" id="fupdateSysDefOrder"><%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		updateFFDefOrder(document.getElementById("PmtFFDefFileOrderTableId").rows.length, 'ftext');
	</script>
    </button>
    <%
    } else {
    %>
    &nbsp;
    <%
        }
    %>

</div>
<table>

     <tr style="height:30px">
     <td>&nbsp</td>
     </tr>
</table>

<table class="formDocumentsTable" id="PmtFFInvDefFileOrderTableId" >
    <thead>
    <tr style="height:35px">
        <th colspan=3 style="text-align:left">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitionDetail.InvDetailOrder")%>
        </th>
    </tr>
    </thead>

    <tbody>

    <tr style="height:35px">
        <td width="20" >
            <%=widgetFactory.createLabel("",
                    "POStructureDefinition.Order", false, false, false, "none")%>
        </td>
        <td width="160">
            <%=widgetFactory.createLabel("",
                    "POStructureDefinition.FieldName", false, false, false, "none")%>
        </td>
        <td width="20" >
            <%=widgetFactory.createLabel("",
                    "POStructureDefinition.Position", false, false, false, "none")%>
        </td>
    </tr>

    <%
        //RKAZI Rel 8.2 T36000011727 - START
//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
//NOTE: please do not reformat the code.
        String[] initialValFFInv = {  };
        boolean initialFlagFFInv = false;
        if (InstrumentServices.isBlank(paymentDef
                .getAttribute("pmt_inv_summary_order1")))
            initialFlagFFInv = true;
        String currentValueFFInv;
        counterTmp = 1;
        int positionInv = 1;
        if (initialFlagFFInv) {
            for (counterTmp = 1; counterTmp <= initialValFFInv.length; counterTmp++) {

                currentValueFFInv = initialValFFInv[counterTmp - 1];
                if (counterTmp ==1){
                    positionInv = 1;
                }else{
                    positionInv = positionInv + ((Integer)sizeMap.get(initialValFFInv[counterTmp - 2]+"_size")).intValue() ;
                }
    %>

    <tr id= "i<%=currentValueFFInv%>_req_order">
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='itext<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px;" <%= (isReadOnly ? "readOnly=true" : " ") %> /><input type="hidden" id="iupload_order_<%=counterTmp%>" name="ipmt_inv_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFInv%>" />
        </td>
        <td id='ifileFieldName<%=counterTmp%>' >
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + currentValueFFInv, false,
                    false, false, "none")%>

        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionInv%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='iposition<%=counterTmp%>'  style="width: 30px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff" readOnly=true class="ordField" /><input type="hidden" id="isize<%=counterTmp%>" name="isize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFInv%>" />
        </td>
    </tr>
    <%
        }
    } else {
        counterTmp = 1;
            positionInv = 1;
        // invoice summary data in upload invoice file has maximum 28 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order9)
        while ((counterTmp <= 2)
                && InstrumentServices
                .isNotBlank(paymentDef
                        .getAttribute("pmt_inv_summary_order"
                                + (new Integer(counterTmp))
                                .toString()))) {

            currentValueFFInv = paymentDef.getAttribute("pmt_inv_summary_order"
                    + (new Integer(counterTmp)).toString());
            if (counterTmp == 1){
                positionInv = 1;
            } else{
                String tmpSizeField = paymentDef.getAttribute("pmt_inv_summary_order"
                        + (new Integer(counterTmp - 1)).toString());
                String fSize =   paymentDef.getAttribute(tmpSizeField+"_size");
                if(StringFunction.isBlank(fSize)) {
                    fSize = (String)sizeMap.get(tmpSizeField+"_size");
                }

                positionInv = positionInv + (new Integer(fSize)).intValue();
            }
    %>

    <tr id= "i<%=currentValueFFInv%>_req_order">
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='itext<%=counterTmp%>'  style="width: 25px;" <%= (isReadOnly ? "readOnly=true" : " ") %> /><input type="hidden" id="iupload_order_<%=counterTmp%>" name="ipmt_inv_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFInv%>" />
        </td>
        <td id='ifileFieldName<%=counterTmp%>' align="left">
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + currentValueFFInv, false,
                    false, false,"labelClass=\"invOrderLabel\"", "none")%>
            <% if(pmtSummOptionalVal.contains(currentValueFFInv)){ %>
            <span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='i<%=currentValueFFInv%>'></span>
            <%} %>
        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionInv%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='iposition<%=counterTmp%>'  style="width: 30px; border-color: #ffffff; background-color: #ffffff; " readOnly=true class="ordField" /><input type="hidden" id="isize<%=counterTmp%>" name="isize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFInv%>" />
        </td>
    </tr>

    <%
                counterTmp++;
            }
        }
    %>
    </tbody>
</table>
<br/>
<div>

    <%
        invSummDataOrder = counterTmp;
        if (!(isReadOnly)) {
    %>
    <button data-dojo-type="dijit.form.Button" type="button" id="iupdateSysDefOrder"><%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		updateFFInvDefOrder(document.getElementById("PmtFFInvDefFileOrderTableId").rows.length, 'itext');
	</script>
    </button>
    <%
    } else {
    %>
    &nbsp;
    <%
        }
    %>

</div>
<br/><br/>


</div>


</div>
