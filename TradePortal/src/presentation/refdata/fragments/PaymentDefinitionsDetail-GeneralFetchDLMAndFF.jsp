<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, java.util.Vector, java.util.Hashtable" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<jsp:useBean id="payDefUtil" class="com.ams.tradeportal.common.PaymentDefinitionsUtil"
   scope="session">
</jsp:useBean>

<%

  //Initialize WidgetFactory.
    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
    Set pmtSummOptionalVal = payDefUtil.getPmtSummOtions();
    Set pmtSummReqVal = payDefUtil.getPmtSummReqVal();
    int counterTmp = 3;
    int invSummDataOrder;
    DocumentHandler defaultDoc     = formMgr.getFromDocCache();
    String paymentDefOid          = StringFunction.xssCharsToHtml(request.getParameter("oid"));
    String retrieveFromDB          = StringFunction.xssCharsToHtml(request.getParameter("retrieveFromDB"));  
    String getDataFromDoc          = StringFunction.xssCharsToHtml(request.getParameter("getDataFromDoc"));
    String isReadOnlyS          = StringFunction.xssCharsToHtml(request.getParameter("isReadOnly"));
    boolean isReadOnly = Boolean.valueOf(isReadOnlyS).booleanValue();
    
    if(paymentDefOid != null)
    {
       //paymentDefOid = EncryptDecrypt.decryptStringUsingTripleDes(paymentDefOid,userSession.getSecretKey());
    }
   else
    {
       paymentDefOid = "0";
    }
    PaymentDefinitionsWebBean paymentDef = beanMgr.createBean(PaymentDefinitionsWebBean.class, "PaymentDefinitions");
    if ("true".equals(retrieveFromDB)) {
    	 getDataFromDoc = "false";
    paymentDef.setAttribute("payment_definition_oid", paymentDefOid);
    paymentDef.getDataFromAppServer();
    }
    if ("true".equals(getDataFromDoc)) {
        // Populate the securityProfile bean with the output doc.
        try {
            paymentDef.populateFromXmlDoc(defaultDoc.getComponent("/In"));
        } catch (Exception e) {
           out.println("Contact Administrator: Unable to get Payment Method Definition "
                + "attributes for oid: " + paymentDefOid + " " + e.toString());
        }
    }
%>
<div id="DLMFFDiv">
    <%@ include file="PaymentDefinitionsDetail-GeneralDLM.frag" %>
</div>
<div id="FFFDiv">
    <%@ include file="PaymentDefinitionsDetail-GeneralFF.frag" %>
</div>