<%--
*******************************************************************************
       Invoice Definition Detail - General and Data Definition Layout (Tab) Page

    Description:   This jsp simply displays the html for the data description
portion of the Payment Method Definition.jsp.  This page is called from
Payment Method Definition Detail.jsp  and is called by:
		<%@ include file="PaymentDefinitionsDetail.jsp" %>
Note: the values stored in secureParms in this page are done deliberately to avoid
being overwritten as each tab is a submit action.  As a result, if this data does
not make it into the xml document, then it may be overwritten with a null value
in this tab or another.
*******************************************************************************
--%>

<%-- Data Definition Title Panel details start --%>
<%
    Map sizeMapPNG = new HashMap();
    sizeMapPNG.put("fd_record_type_size",1);
    sizeMapPNG.put("fd_bsb_size",7);
    sizeMapPNG.put("fd_account_size",9);
    sizeMapPNG.put("fd_reserved1_size",17);
    sizeMapPNG.put("fd_sequence_number_size",2);
    sizeMapPNG.put("fd_nm_of_usr_fin_inst_size",3);
    sizeMapPNG.put("fd_reserved2_size",7);
    sizeMapPNG.put("fd_nm_of_usr_sup_file_size",26);
    sizeMapPNG.put("fd_user_ident_num_size",6);
    sizeMapPNG.put("fd_desc_of_entr_on_file_size",12);
    sizeMapPNG.put("fd_date_to_be_process_size",8);
    sizeMapPNG.put("fd_time_size",4);
    sizeMapPNG.put("fd_reserved3_size",50);
    sizeMapPNG.put("dr_record_type_size",1);
    sizeMapPNG.put("dr_bsb_size",7);
    sizeMapPNG.put("dr_account_type_size",3);
    sizeMapPNG.put("dr_acct_num_to_be_cred_size",12);
    sizeMapPNG.put("dr_withhold_tax_ind_size",1);
    sizeMapPNG.put("dr_trans_code_size",2);
    sizeMapPNG.put("dr_nw_var_bsb_acct_det_size",1);
    sizeMapPNG.put("dr_title_acct_cred_size",32);
    sizeMapPNG.put("dr_amt_to_be_cred_size",10);
    sizeMapPNG.put("dr_lodge_ref_size",18);
    sizeMapPNG.put("dr_trace_bsb_num_size",7);
    sizeMapPNG.put("dr_trace_acct_num_size",9);
    sizeMapPNG.put("dr_nm_of_remitter_size",16);
    sizeMapPNG.put("dr_withhold_amt_size",8);
    sizeMapPNG.put("dr_lodge_ref_bsb_num_size",7);
    sizeMapPNG.put("dr_lodge_ref_acct_typ_size",3);
    sizeMapPNG.put("dr_lodeg_ref_acct_num_size",12);
    sizeMapPNG.put("dr_reserved_size",12);
    sizeMapPNG.put("br_record_type_size",1);
    sizeMapPNG.put("br_reserved1_size",7);
    sizeMapPNG.put("br_reserved2_size",12);
    sizeMapPNG.put("br_btch_net_tot_amt_size",10);
    sizeMapPNG.put("br_btch_cred_tot_amt_size",10);
    sizeMapPNG.put("br_btch_deb_tot_amt_size",10);
    sizeMapPNG.put("br_reserved3_size",24);
    sizeMapPNG.put("br_btch_tot_itm_cnt_size",8);
    sizeMapPNG.put("br_reserved4_size",50);
%>
<%=widgetFactory.createSectionHeader("2",
        "PaymentDefinitionDetail.PNGFF")%>

<div class = "columnLeft">
<%=widgetFactory.createLabel("",
        "PaymentDefinitions.PNGDefSep1", false, false, false,
        "none")%>


<table class="formDocumentsTable" id="PmtFFDefFDTableId" style="width:98%;">
<thead>
<tr style="height:35px;">
    <th colspan=3 style="text-align:left;">
        <%=widgetFactory
                .createSubLabel("PaymentDefinitions.PNGDescriptiveRecordFields")%>
    </th>
</tr>
</thead>
<tbody>
<tr style="height:35px">
    <td nowrap width="145"  >
        <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
    </td>
    <td  width="20" align="center" >
        <b><%=resMgr.getText("PaymentDefinitions.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
    </td>
    <td width="30" >
        <b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("PaymentDefinitions.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
    </td>
</tr>
<tr>
    <td>
        
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fd_record_type", false, true, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="fd_record_type_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_record_type_size"))? paymentDef.getAttribute("fd_record_type_size") : "1" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fd_record_type_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
    </td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td  >
        
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fd_reserved1", false, true, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="fd_reserved1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_reserved1_size"))? paymentDef.getAttribute("fd_reserved1_size") : "17" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fd_reserved1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "17", false, false, false, "none") %>
    </td>
    <td align='center'>
        <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.fd_reserved1", TradePortalConstants.TEXT_BUNDLE)%>" name="fd_reserved1" id="fd_reserved1">

        <%=widgetFactory.createCheckboxField(
                "fd_reserved1_data_req",
                "",
                true, isReadOnly,
                false, "", "", "none")%>
    </td>
</tr>
<tr>
    <td  >
        
        <%=widgetFactory.createLabel("", "PaymentDefinitions.fd_sequence_number",
                false, true, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="fd_sequence_number_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_sequence_number_size"))? paymentDef.getAttribute("fd_sequence_number_size") : "2" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fd_sequence_number_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
    </td>
    <td >&nbsp;</td>
</tr>
<tr>
    <td  >
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fd_nm_of_usr_fin_inst", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="fd_nm_of_usr_fin_inst_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_nm_of_usr_fin_inst_size"))? paymentDef.getAttribute("fd_nm_of_usr_fin_inst_size") : "3" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fd_nm_of_usr_fin_inst_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />

        <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td  >
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fd_reserved2", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="fd_reserved2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_reserved2_size"))? paymentDef.getAttribute("fd_reserved2_size") : "7" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fd_reserved2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "7", false, false, false, "none") %>
    </td>
    <td align='center'>
        <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.fd_reserved2", TradePortalConstants.TEXT_BUNDLE)%>" name="fd_reserved2" id="fd_reserved2">

        <%=widgetFactory.createCheckboxField(
                "fd_reserved2_data_req",
                "",
                true, isReadOnly,
                false, "", "", "none")%>
    </td>
</tr>

<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fd_nm_of_usr_sup_file", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="fd_nm_of_usr_sup_file_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_nm_of_usr_sup_file_size"))? paymentDef.getAttribute("fd_nm_of_usr_sup_file_size") : "26" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fd_nm_of_usr_sup_file_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "26", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fd_user_ident_num", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="fd_user_ident_num_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_user_ident_num_size"))? paymentDef.getAttribute("fd_user_ident_num_size") : "6" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fd_user_ident_num_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "6", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fd_desc_of_entr_on_file", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="fd_desc_of_entr_on_file_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_desc_of_entr_on_file_size"))? paymentDef.getAttribute("fd_desc_of_entr_on_file_size") : "12" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fd_desc_of_entr_on_file_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "12", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fd_date_to_be_process", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="fd_date_to_be_process_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_date_to_be_process_size"))? paymentDef.getAttribute("fd_date_to_be_process_size") : "8" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fd_date_to_be_process_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "8", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.fd_reserved3", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="fd_reserved3_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_reserved3_size"))? paymentDef.getAttribute("fd_reserved3_size") : "50" %>'
               data-dojo-type="dijit.form.TextBox"
               id='fd_reserved3_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "50", false, false, false, "none") %>
    </td>
    <td align='center'>
        <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.fd_reserved3", TradePortalConstants.TEXT_BUNDLE)%>" name="fd_reserved3" id="fd_reserved3">

        <%=widgetFactory.createCheckboxField(
                "fd_reserved3_data_req",
                "",
                true, isReadOnly,
                false, "", "", "none")%>

    </td>
</tr>
</tbody>
</table>
<table>

     <tr style="height:56px">
     <td>&nbsp</td>
     </tr>
</table>

<table class="formDocumentsTable" id="PmtFFDefDRTableId" style="width:98%;">
<thead>
<tr style="height:35px;">
    <th colspan=3 style="text-align:left;">
        <%=widgetFactory
                .createSubLabel("PaymentDefinitions.PNGDRFields")%>
    </th>
</tr>
</thead>
<tbody>
<tr style="height:35px">
    <td nowrap width="145"  >
        <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
    </td>
    <td  width="20" align="center" >
        <b><%=resMgr.getText("PaymentDefinitions.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
    </td>
    <td width="30" >
        <b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("PaymentDefinitions.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
    </td>
</tr>
<tr>
    <td>
        
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_record_type", false, true, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_record_type_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_record_type_size"))? paymentDef.getAttribute("dr_record_type_size") : "1" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_record_type_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
    </td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>
        
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_bsb", false, true, false, "none")%>
    </td>

    <td align="center">
        <input type="hidden" name="dr_bsb_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_bsb_size"))? paymentDef.getAttribute("dr_bsb_size") : "7" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_bsb_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "7", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td>
        
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_account_type", false, true, false, "none")%>
    </td>

    <td align="center">
        <input type="hidden" name="dr_account_type_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_account_type_size"))? paymentDef.getAttribute("dr_account_type_size") : "3" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_account_type_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td  >
        
        <%=widgetFactory.createLabel("", "PaymentDefinitions.dr_acct_num_to_be_cred",
                false, true, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_acct_num_to_be_cred_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_acct_num_to_be_cred_size"))? paymentDef.getAttribute("dr_acct_num_to_be_cred_size") : "12" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_acct_num_to_be_cred_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "12", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td  >
        
        <%=widgetFactory.createLabel("", "PaymentDefinitions.dr_nw_var_bsb_acct_det",
                false, true, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_nw_var_bsb_acct_det_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_nw_var_bsb_acct_det_size"))? paymentDef.getAttribute("dr_nw_var_bsb_acct_det_size") : "1" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_nw_var_bsb_acct_det_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td  >
        
        <%=widgetFactory.createLabel("", "PaymentDefinitions.dr_trans_code",
                false, true, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_trans_code_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_trans_code_size"))? paymentDef.getAttribute("dr_trans_code_size") : "2" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_trans_code_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
    </td>
    <td >&nbsp;</td>
</tr>
<tr>
    <td  >
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_amt_to_be_cred", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_amt_to_be_cred_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_amt_to_be_cred_size"))? paymentDef.getAttribute("dr_amt_to_be_cred_size") : "10" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_amt_to_be_cred_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />

        <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td  >
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_title_acct_cred", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_title_acct_cred_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_title_acct_cred_size"))? paymentDef.getAttribute("dr_title_acct_cred_size") : "32" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_title_acct_cred_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "32", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>

<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_lodge_ref", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_lodge_ref_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_lodge_ref_size"))? paymentDef.getAttribute("dr_lodge_ref_size") : "18" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_lodge_ref_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "18", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_lodge_ref_bsb_num", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_lodge_ref_bsb_num_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_lodge_ref_bsb_num_size"))? paymentDef.getAttribute("dr_lodge_ref_bsb_num_size") : "7" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_lodge_ref_bsb_num_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "7", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_lodge_ref_acct_typ", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_lodge_ref_acct_typ_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_lodge_ref_acct_typ_size"))? paymentDef.getAttribute("dr_lodge_ref_acct_typ_size") : "3" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_lodge_ref_acct_typ_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_lodeg_ref_acct_num", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_lodeg_ref_acct_num_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_lodeg_ref_acct_num_size"))? paymentDef.getAttribute("dr_lodeg_ref_acct_num_size") : "12" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_lodeg_ref_acct_num_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "12", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_nm_of_remitter", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_nm_of_remitter_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_nm_of_remitter_size"))? paymentDef.getAttribute("dr_nm_of_remitter_size") : "16" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_nm_of_remitter_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "16", false, false, false, "none") %>
    </td>
    <td align='center'>&nbsp;</td>
</tr>
<tr>
    <td>
        <%=widgetFactory.createLabel("",
                "PaymentDefinitions.dr_png_reserved", false, false, false, "none")%>
    </td>
    <td align="center">
        <input type="hidden" name="dr_reserved_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_reserved_size"))? paymentDef.getAttribute("dr_reserved_size") : "12" %>'
               data-dojo-type="dijit.form.TextBox"
               id='dr_reserved_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
        <%= widgetFactory.createLabel("", "12", false, false, false, "none") %>
    </td>
    <td align='center'>
        <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.dr_withhold_amt", TradePortalConstants.TEXT_BUNDLE)%>" name="dr_withhold_amt" id="dr_withhold_amt">

        <%=widgetFactory.createCheckboxField(
                "dr_withhold_amt_data_req",
                "",
                true, isReadOnly,
                false, "", "", "none")%>
    </td>
</tr>
</tbody>
</table>

<table>

     <tr style="height:68px">
     <td>&nbsp</td>
     </tr>
</table>


<table class="formDocumentsTable" id="PmtFFDefBRTableId" style="width:98%;">
    <thead>
    <tr style="height:35px;">
        <th colspan=3 style="text-align:left;">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitions.PNGBRFields")%>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr style="height:35px">
        <td nowrap width="145"  >
            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td  width="20" align="center" >
            <b><%=resMgr.getText("PaymentDefinitions.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="30" >
            <b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("PaymentDefinitions.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
    </tr>
    <tr>
        <td>
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_record_type", false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_record_type_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_record_type_size"))? paymentDef.getAttribute("br_record_type_size") : "1" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_record_type_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_reserved1", false, true, false, "none")%>
        </td>

        <td align="center">
            <input type="hidden" name="br_reserved1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_reserved1_size"))? paymentDef.getAttribute("br_reserved1_size") : "7" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_reserved1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "7", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("", "PaymentDefinitions.br_reserved2",
                    false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_reserved2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_reserved2_size"))? paymentDef.getAttribute("br_reserved2_size") : "12" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_reserved2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "12", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.br_reserved2", TradePortalConstants.TEXT_BUNDLE)%>" name="br_reserved2" id="br_reserved2">

            <%=widgetFactory.createCheckboxField(
                    "br_reserved2_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_btch_net_tot_amt", false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_btch_net_tot_amt_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_btch_net_tot_amt_size"))? paymentDef.getAttribute("br_btch_net_tot_amt_size") : "10" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_btch_net_tot_amt_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("", "PaymentDefinitions.br_btch_cred_tot_amt",
                    false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_btch_cred_tot_amt_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_btch_cred_tot_amt_size"))? paymentDef.getAttribute("br_btch_cred_tot_amt_size") : "10" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_btch_cred_tot_amt_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
        </td>
        <td align="center">
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.br_btch_cred_tot_amt", TradePortalConstants.TEXT_BUNDLE)%>" name="br_btch_cred_tot_amt" id="br_btch_cred_tot_amt">

            <%=widgetFactory.createCheckboxField(
                    "br_btch_cred_tot_amt_data_req",
                    "",
                   true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("", "PaymentDefinitions.br_btch_deb_tot_amt",
                    false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_btch_deb_tot_amt_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_btch_deb_tot_amt_size"))? paymentDef.getAttribute("br_btch_deb_tot_amt_size") : "10" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_btch_deb_tot_amt_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
        </td>
        <td align="center">
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.br_btch_deb_tot_amt", TradePortalConstants.TEXT_BUNDLE)%>" name="br_btch_deb_tot_amt" id="br_btch_deb_tot_amt">

            <%=widgetFactory.createCheckboxField(
                    "br_btch_deb_tot_amt_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_reserved3", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_reserved3_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_reserved3_size"))? paymentDef.getAttribute("br_reserved3_size") : "24" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_reserved3'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />

            <%= widgetFactory.createLabel("", "24", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.br_reserved3", TradePortalConstants.TEXT_BUNDLE)%>" name="br_reserved3" id="br_reserved3">

            <%=widgetFactory.createCheckboxField(
                    "br_reserved3_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_btch_tot_itm_cnt", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_btch_tot_itm_cnt_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_btch_tot_itm_cnt_size"))? paymentDef.getAttribute("br_btch_tot_itm_cnt_size") : "8" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_btch_tot_itm_cnt_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "8", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>

    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_reserved4", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_reserved4_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_reserved4_size"))? paymentDef.getAttribute("br_reserved4_size") : "50" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_reserved4_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "50", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.br_reserved4", TradePortalConstants.TEXT_BUNDLE)%>" name="br_reserved4" id="br_reserved4">

            <%=widgetFactory.createCheckboxField(
                    "br_reserved4_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    </tbody>
</table>



</div>

<div class = "columnRight">

<%=widgetFactory.createLabel("",
        "PaymentDefinitionDetail.PNGDefSep2", false, false, false,
        "none")%>
<table>

     <tr style="height:36px">
     <td>&nbsp</td>
     </tr>
</table>

<table class="formDocumentsTable" id="PmtFFFDDefFileOrderTableId" >
    <thead>
    <tr style="height: 35px;">
        <th colspan=3 style="text-align:left">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitionDetail.PNGDescriptiveRecordFieldsOrd")%>
        </th>
    </tr>
    </thead>

    <tbody>

    <tr style="height: 35px;">
        <td width="20" >
            <b><%=resMgr.getText("PaymentDefinitions.Order", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="160">
            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="20">
	        <b><%=resMgr.getText("PaymentDefinitions.Position", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
    </tr>

    <%
        //RKAZI Rel 8.2 T36000011727 - START
//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
//NOTE: please do not reformat the code.
        String[] initialValFFFDPNG = { "fd_record_type", "fd_reserved1", "fd_sequence_number", "fd_nm_of_usr_fin_inst", "fd_reserved2", "fd_nm_of_usr_sup_file", "fd_user_ident_num", "fd_desc_of_entr_on_file", "fd_date_to_be_process", "fd_reserved3" };
        boolean initialFlagFFFDPNG = false;
        if (InstrumentServices.isBlank(paymentDef
                .getAttribute("fd_summary_order1")))
            initialFlagFFFDPNG = true;
        String currentValueFFFDPNG;
        counterTmp = 1;
        int positionFDPNG = 1;
        if (initialFlagFFFDPNG) {
            for (counterTmp = 1; counterTmp <= initialValFFFDPNG.length; counterTmp++) {

                currentValueFFFDPNG = initialValFFFDPNG[counterTmp - 1];
                if (counterTmp ==1){
                    positionFDPNG = 1;
                }else{
                    positionFDPNG = positionFDPNG + ((Integer) sizeMapPNG.get(initialValFFFDPNG[counterTmp - 2]+"_size")).intValue() ;
                }
    %>

    <tr>
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='htext<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="hupload_order_<%=counterTmp%>" name="fd_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFFDPNG%>" />
        </td>
        <td id='hfileFieldName<%=counterTmp%>' >
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + currentValueFFFDPNG, false,
                    false, false, "none")%>

        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionFDPNG%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='hposition<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="hsize<%=counterTmp%>" name="hsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFFDPNG%>" />
        </td>
    </tr>
    <%
        }
    } else {
        counterTmp = 1;
        positionFDPNG = 1;
        // invoice summary data in upload invoice file has maximum 28 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order9)
        while ((counterTmp <= 10)
                && InstrumentServices
                .isNotBlank(paymentDef
                        .getAttribute("fd_summary_order"
                                + (new Integer(counterTmp))
                                .toString()))) {

            currentValueFFFDPNG = paymentDef.getAttribute("fd_summary_order"
                    + (new Integer(counterTmp)).toString());
            if (counterTmp == 1){
                positionFDPNG = 1;
            } else{
                String tmpSizeField = paymentDef.getAttribute("fd_summary_order"
                        + (new Integer(counterTmp - 1)).toString());
                String fSize =   paymentDef.getAttribute(tmpSizeField+"_size");
                if(StringFunction.isBlank(fSize)) {
                    fSize = (String) sizeMapPNG.get(tmpSizeField+"_size");
                }
                positionFDPNG = positionFDPNG + (new Integer(fSize)).intValue();
            }
    %>

    <tr id= "h<%=currentValueFFFDPNG%>_req_order">
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='htext<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="hupload_order_<%=counterTmp%>" name="fd_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFFDPNG%>" />
        </td>
        <td id='hfileFieldName<%=counterTmp%>'>
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + currentValueFFFDPNG, false,
                    false, false,"", "none")%>
            <% if(pmtSummOptionalVal.contains(currentValueFFFDPNG) && !pmtSummReqVal.contains(currentValueFFFDPNG)){ %>
            <span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='h<%=currentValueFFFDPNG%>'></span>
            <%} %>
        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionFDPNG%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='hposition<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="hsize<%=counterTmp%>" name="hsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFFDPNG%>" />
        </td>
    </tr>

    <%
                counterTmp++;
            }
        }
    %>
    </tbody>
</table>
<table>

     <tr style="height:36px">
     <td>&nbsp</td>
     </tr>
</table>

<table class="formDocumentsTable" id="PmtFFDefDRFileOrderTableId" >
    <thead>
    <tr style="height:35px">
        <th colspan=3 style="text-align:left">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitionDetail.PNGDRFieldsOrder")%>
        </th>
    </tr>
    </thead>

    <tbody>

    <tr style="height:35px">
        <td width="20" >
            <b><%=resMgr.getText("PaymentDefinitions.Order", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="160">
            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="20">
	        <b><%=resMgr.getText("PaymentDefinitions.Position", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
    </tr>

    <%
        //RKAZI Rel 8.2 T36000011727 - START
//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
//NOTE: please do not reformat the code.
        String[] initialValFFDRPNG = { "dr_record_type", "dr_bsb", "dr_account_type", "dr_acct_num_to_be_cred", "dr_nw_var_bsb_acct_det", "dr_trans_code", "dr_amt_to_be_cred", "dr_title_acct_cred", "dr_lodge_ref", "dr_lodge_ref_bsb_num", "dr_lodge_ref_acct_typ", "dr_lodeg_ref_acct_num", "dr_nm_of_remitter", "dr_reserved" };
        boolean initialFlagFFDRPNG = false;
        if (InstrumentServices.isBlank(paymentDef
                .getAttribute("dr_summary_order1")))
            initialFlagFFDRPNG = true;
        String currentValueFFDRPNG;
        counterTmp = 1;
        int positionDR = 1;
        if (initialFlagFFDRPNG) {
            for (counterTmp = 1; counterTmp <= initialValFFDRPNG.length; counterTmp++) {

                currentValueFFDRPNG = initialValFFDRPNG[counterTmp - 1];
                if (counterTmp ==1){
                    positionDR = 1;
                }else{
                    positionDR = positionDR + ((Integer) sizeMapPNG.get(initialValFFDRPNG[counterTmp - 2]+"_size")).intValue() ;
                }

    %>

    <tr>
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='ftext<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="fupload_order_<%=counterTmp%>" name="dr_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFDRPNG%>" />
        </td>
        <td id='ffileFieldName<%=counterTmp%>' >
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + ("dr_reserved".equals(currentValueFFDRPNG) ? "png_"+currentValueFFDRPNG : currentValueFFDRPNG), false,
                    false, false, "none")%>

        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionDR%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fposition<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="fsize<%=counterTmp%>" name="fsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFDRPNG%>" />
        </td>
    </tr>
    <%
        }
    } else {
        counterTmp = 1;
        positionDR = 1;
        // invoice summary data in upload invoice file has maximum 28 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order9)
        while ((counterTmp <= 14)
                && InstrumentServices
                .isNotBlank(paymentDef
                        .getAttribute("dr_summary_order"
                                + (new Integer(counterTmp))
                                .toString()))) {

            currentValueFFDRPNG = paymentDef.getAttribute("dr_summary_order"
                    + (new Integer(counterTmp)).toString());
            if (counterTmp == 1){
                positionDR = 1;
            } else{
                String tmpSizeField = paymentDef.getAttribute("dr_summary_order"
                        + (new Integer(counterTmp - 1)).toString());
                String fSize =   paymentDef.getAttribute(tmpSizeField+"_size");
                if(StringFunction.isBlank(fSize)) {
                    fSize = (String) sizeMapPNG.get(tmpSizeField+"_size");
                }

                positionDR = positionDR + (new Integer(fSize)).intValue();
            }

    %>

    <tr id= "f<%=currentValueFFDRPNG%>_req_order">
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='ftext<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="fupload_order_<%=counterTmp%>" name="dr_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFDRPNG%>" />
        </td>
        <td id='ffileFieldName<%=counterTmp%>'>
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + ("dr_reserved".equals(currentValueFFDRPNG) ? "png_"+currentValueFFDRPNG : currentValueFFDRPNG), false,
                    false, false,"", "none")%>
            <% if(pmtSummOptionalVal.contains(currentValueFFDRPNG)){ %>
            <span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='f<%=currentValueFFDRPNG%>'></span>
            <%} %>
        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionDR%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='fposition<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="fsize<%=counterTmp%>" name="fsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFDRPNG%>" />
        </td>
    </tr>

    <%
                counterTmp++;
            }
        }
    %>
    </tbody>
</table>
<table>

     <tr style="height:40px">
     <td>&nbsp</td>
     </tr>
</table>

<table class="formDocumentsTable" id="PmtFFDefBRFileOrderTableId" >
    <thead>
    <tr style="height:35px">
        <th colspan=3 style="text-align:left">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitionDetail.PNGBRFieldsOrder")%>
        </th>
    </tr>
    </thead>

    <tbody>

    <tr style="height:35px">
        <td width="20" >
            <b><%=resMgr.getText("PaymentDefinitions.Order", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="160">
            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="20">
	        <b><%=resMgr.getText("PaymentDefinitions.Position", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
    </tr>

    <%
        //RKAZI Rel 8.2 T36000011727 - START
//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
//NOTE: please do not reformat the code.
        String[] initialValFFBRPNG = { "br_record_type", "br_reserved1", "br_reserved2", "br_btch_net_tot_amt", "br_btch_cred_tot_amt", "br_btch_deb_tot_amt", "br_reserved3", "br_btch_tot_itm_cnt", "br_reserved4" };
        boolean initialFlagFFBRPNG = false;
        if (InstrumentServices.isBlank(paymentDef
                .getAttribute("br_summary_order1")))
            initialFlagFFBRPNG = true;
        String currentValueFFBRPNG;
        counterTmp = 1;
        int positionBRPNG = 1;
        if (initialFlagFFBRPNG) {
            for (counterTmp = 1; counterTmp <= initialValFFBRPNG.length; counterTmp++) {

                currentValueFFBRPNG = initialValFFBRPNG[counterTmp - 1];
                if (counterTmp ==1){
                    positionBRPNG = 1;
                }else{
                    positionBRPNG = positionBRPNG + ((Integer) sizeMapPNG.get(initialValFFBRPNG[counterTmp - 2]+"_size")).intValue() ;
                }

    %>

    <tr>
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='itext<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="iupload_order_<%=counterTmp%>" name="br_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFBRPNG%>" />
        </td>
        <td id='ifileFieldName<%=counterTmp%>' >
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + currentValueFFBRPNG, false,
                    false, false, "none")%>

        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionBRPNG%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='iposition<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="isize<%=counterTmp%>" name="isize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFBRPNG%>" />
        </td>
    </tr>
    <%
        }
    } else {
        counterTmp = 1;
        positionBRPNG = 1;
        // invoice summary data in upload invoice file has maximum 28 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order9)
        while ((counterTmp <= 9)
                && InstrumentServices
                .isNotBlank(paymentDef
                        .getAttribute("br_summary_order"
                                + (new Integer(counterTmp))
                                .toString()))) {

            currentValueFFBRPNG = paymentDef.getAttribute("br_summary_order"
                    + (new Integer(counterTmp)).toString());
            if (counterTmp == 1){
                positionBRPNG = 1;
            } else{
                String tmpSizeField = paymentDef.getAttribute("br_summary_order"
                        + (new Integer(counterTmp - 1)).toString());
                String fSize =   paymentDef.getAttribute(tmpSizeField+"_size");
                if(StringFunction.isBlank(fSize)) {
                    fSize = (String) sizeMapPNG.get(tmpSizeField+"_size");
                }

                positionBRPNG = positionBRPNG + (new Integer(fSize)).intValue();
            }

    %>

    <tr id= "f<%=currentValueFFBRPNG%>_req_order">
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='itext<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="iupload_order_<%=counterTmp%>" name="br_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFBRPNG%>" />
        </td>
        <td id='ifileFieldName<%=counterTmp%>'>
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + currentValueFFBRPNG, false,
                    false, false,"", "none")%>
            <% if(pmtSummOptionalVal.contains(currentValueFFBRPNG)){ %>
            <span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='f<%=currentValueFFBRPNG%>'></span>
            <%} %>
        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionBRPNG%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='iposition<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="isize<%=counterTmp%>" name="isize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFBRPNG%>" />
        </td>
    </tr>

    <%
                counterTmp++;
            }
        }
    %>
    </tbody>
</table>
<br/>

</div>


</div>
