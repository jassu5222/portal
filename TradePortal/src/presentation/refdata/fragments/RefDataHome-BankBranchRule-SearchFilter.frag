<%--
*******************************************************************************
                  Ref Data Home - Bank Branch Rule Filter include file

  Description:
   This is a JSP meant to be included using the <%@ include file="x.frag" %> 
  tag.  It creates the HTML for displaying the Search Filter fields for the 
  Bank Branch Rules.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
  
  This page is currently included in ReceivablesManagement_Notices.frag. 
    
  Each originating page should contain the following code:
  
    <input type="hidden" name="NewSearch" value="Y">
  
  - this will allow a new search criteria to be created.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>
<%--
*
*     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<script LANGUAGE="JavaScript">

         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }

</script>

<input type="hidden" name="NewSearch" value="Y">

<%--cquinton 10/19/2012 add search classes to standardize with other search pages--%>
<div class="searchDivider"></div>

<div class="searchDetail">
  <span class="searchCriteria">
    <span class="note formItem">
      <%= resMgr.getText("RefDataHome.BankBranchRules.InstructionalText", TradePortalConstants.TEXT_BUNDLE) %>
    </span>
  </span>
  <span class="searchActions">
    <button data-dojo-type="dijit.form.Button" type="button" id="newButton1"> 
    <%= resMgr.getText("common.FilterText", TradePortalConstants.TEXT_BUNDLE) %> 
    	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
    		searchBankBranchRules();
    	</script>
    </button>
     <%=widgetFactory.createHoverHelp("newButton1","SearchHoverText") %>
  </span>
  <div style="clear:both;"></div>
</div>

       <%
       Vector codesToExclude = new Vector();
       codesToExclude.add(TradePortalConstants.PAYMENT_METHOD_IACC);
          String dropdwnOptions = Dropdown.createSortedRefDataOptions("PAYMENT_METHOD",
                                                                        paymentMethod,
                                                                        userSession.getUserLocale(),
                                                                        codesToExclude);       
             dropdwnOptions = "<option value=\"\"></option>" +dropdwnOptions;

          String countryDropdwnOptions = Dropdown.createSortedRefDataOptions("COUNTRY", country, userSession.getUserLocale());       
             countryDropdwnOptions = "<option value=\"\"></option>" +countryDropdwnOptions;        

        %>        

<div class="searchDetail">
  <span class="searchCriteria">
    <span class="formItem">
      	<span class="asterisk" style="margin-left:-7px;">*</span>
 	    <%=widgetFactory.createSearchSelectField("PaymentMethod","RefDataBankBranchRulesSearch.PaymentMethod","", dropdwnOptions, "")%>
    </span>
     <span class="formItem">
      	<%=widgetFactory.createSearchTextField("BankBranchCode", "RefDataBankBranchRulesSearch.BankBranchCode", "35", "onKeydown='Javascript: filterBankBranchRuleOnEnter(\"BankBranchCode\");' class='char15'") %>
    </span>
     <span class="formItem">
      	<%=widgetFactory.createSearchTextField("BranchName", "RefDataBankBranchRulesSearch.BranchName", "70", "onKeydown='Javascript: filterBankBranchRuleOnEnter(\"BranchName\");' class='char15'") %>
    </span>     
  </span>
  
  <div style="clear:both;"></div>
</div>	

<div class="searchDetail">
  <span class="searchCriteria">
    <span class="formItem">
      	<%=widgetFactory.createSearchTextField("BankName", "RefDataBankBranchRulesSearch.BankName", "120", "onKeydown='Javascript: filterBankBranchRuleOnEnter(\"BankName\");' class='char15'") %>
    </span>

    <span class="formItem">
      	<%=widgetFactory.createSearchTextField("City", "RefDataBankBranchRulesSearch.City", "35", "onKeydown='Javascript: filterBankBranchRuleOnEnter(\"City\");' class='char15'") %>
    </span>
     <span class="formItem">
   <%=widgetFactory.createSearchSelectField("Country","RefDataBankBranchRulesSearch.Country","", countryDropdwnOptions, "")%>
    </span>   
  </span>
  
  <div style="clear:both;"></div>
</div>