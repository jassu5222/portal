<%--
**********************************************************************************
                        Receivables Instruments Tab

  Description:
    Build the where clause to be included in the Ref Data - Bank Branch page 

    This is not a standalone JSP.  It MUST be included in a jsp page.
*******************************************************************************
--%> 

<%       
      
  String paymentMethod = null;
  String bankBranchCode = null; //IR - NOUK020460094
  String bankName = null;
  String branchName = null;
  String city = null;
  String country = null;
  
  String newSearchInd = request.getParameter("NewSearch");
  String DELIMITER = "/";
  if (TradePortalConstants.INDICATOR_YES.equals(newSearchInd))
  {
     //session.removeAttribute(listViewName);
     paymentMethod = request.getParameter("PaymentMethod");
     bankBranchCode = request.getParameter("BankBranchCode");//IR - NOUK020460094 	
     bankName = request.getParameter("BankName");
     branchName = request.getParameter("BranchName");
     city = request.getParameter("City");
     country = request.getParameter("Country");
    
  }

   
%>
