<%--this fragment is included in TradeMarginRows.jsp
    for adding new rows via ajax.  
--%>

<%--TODO: this should iterate through the # of existing shipment terms, or create the correct number of new rows depending on the situation--%>
<%	
  for (int kg=0; kg<marginRuleList.size(); kg++) { 
	  TradePartnerMarginRulesWebBean tradeMarginRow = (TradePartnerMarginRulesWebBean)marginRuleList.get(kg);
	 
%>
  <tr id="marginIndex<%=marginIndex+kg%>">
    <td><%=marginIndex+kg+1%></td>
  		<td>
			 <% 			 			 
				String options11 = Dropdown.createSortedCurrencyCodeOptions(tradeMarginRow.getAttribute("currency_code"), resMgr.getResourceLocale()); 
	  		  	out.println(widgetFactory.createSelectField("CurrencyCode" + (marginIndex+kg), "", " ", options11, isReadOnly, false, false, "class='char5'onChange=\"enableMarginThresholdAmt("+(marginIndex+kg)+")\"", "", "none"));
	  		     String tradeMarginOid = tradeMarginRow.getAttribute("trade_margin_rule_oid");
	  		  	 if(InstrumentServices.isNotBlank(tradeMarginOid)){
	  		  		tradeMarginOid =  EncryptDecrypt.encryptStringUsingTripleDes(tradeMarginOid, userSession.getSecretKey());
	  		  	 }
	  		  	 out.print("<INPUT TYPE=HIDDEN NAME='TraderMarginAliasoid" + (marginIndex+kg) + "' VALUE='" + tradeMarginOid + "'>");
	  		  %>
		</td>
		<td>
			  	 <%
			  	Vector recInstrumentVector2a = new Vector();
				   recInstrumentVector2a.addElement(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT);
				  
				 String  options13 = Dropdown.createSortedRefDataIncludeOptions( TradePortalConstants.INSTRUMENT_TYPE, tradeMarginRow.getAttribute("margin_instrument_type"),resMgr, resMgr.getResourceLocale(), recInstrumentVector2a,true );
         		 
	       			//options11 = Dropdown.createSortedRefDataOptions(TradePortalConstants.UPLOAD_INV_LINKED_INSTR_TY, tradeMarginRow.getAttribute("margin_instrument_type"), resMgr.getResourceLocale());
           			out.print(widgetFactory.createSelectField("MarginInstrumentType" + (marginIndex+kg), "", " ", options13, isReadOnly, false, false, "class='char25'", "", "none"));
           		%>
		</td>
		<td>
				<% String thresholdAmt1="";
	    			 if(InstrumentServices.isNotBlank(tradeMarginRow.getAttribute("threshold_amount")) && InstrumentServices.isNotBlank(tradeMarginRow.getAttribute("currency_code"))) {
							thresholdAmt1 = TPCurrencyUtility.getDisplayAmount(
									tradeMarginRow.getAttribute("threshold_amount"), 
									tradeMarginRow.getAttribute("currency_code"), resMgr.getResourceLocale());			
			
		 			} else {
							thresholdAmt1 = tradeMarginRow.getAttribute("threshold_amount");
					}

	    			    String thresholdAmtHtmlProps1 = "";
	    			    if (InstrumentServices.isBlank(tradeMarginRow.getAttribute("currency_code"))) {
	    			      thresholdAmtHtmlProps1 = " disabled=\"disabled\"";
	    			    }
	    		 %>	 
	  		  <%= widgetFactory.createAmountField( "ThresholdAmount" + (marginIndex+kg), "", thresholdAmt1, "", isReadOnly, false, false, "class='char20'"+thresholdAmtHtmlProps1, "", "none") %>
	    </td>
	    <td>
	  		  <%
	  	   		String options12 = Dropdown.createSortedRefDataOptions("REFINANCE_RATE_TYPE", tradeMarginRow.getAttribute("rate_type"), resMgr.getResourceLocale());
           		out.print(widgetFactory.createSelectField("RateType" + (marginIndex+kg), "", " ", options12, isReadOnly, false, false, "class='char6'", "", "none"));
           		
           		%>
       </td>
       <td>
        	  <%= widgetFactory.createNumberField( "Margin" + (marginIndex+kg), "", tradeMarginRow.getAttribute("margin"), "", isReadOnly, false, false, "class='char2'", "constraints:{min:0,max:999,pattern:'###.######'}", "none") %>
        	 
       </td>      	 
 </tr>
 <%
  } 
%>