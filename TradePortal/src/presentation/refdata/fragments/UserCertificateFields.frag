<%--
*******************************************************************************
                         User Login Password Fields

  Description:
     This fragment is used to construct the login and password fields for both 
  the Admin and User Detail pages.
*******************************************************************************
--%>

<%--
 *
 *     Copyright  2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  if (!corpAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)
      && !corpAuthMethod.equals(TradePortalConstants.AUTH_PERUSER)) {
%>

<%//= widgetFactory.createLabel("", "UserDetail.TransitioningToCertificateAuth", false, false, false, "") %>
      
<%
  } else if ( corpAuthMethod.equals(TradePortalConstants.AUTH_PERUSER) && !isAdminUser ) {
%>

   
        <%
           boolean unableTOView = false;
         
      if (!(TradePortalConstants.INDICATOR_YES.equals(canViewIDFields) || isAdminUser ))
         {
           unableTOView = true;
         }
      %>
        
        <%= widgetFactory.createRadioButtonField("AuthenticationMethod",	"TradePortalConstants.AUTH_CERTIFICATE","UserDetail.UseCertificateID",
							TradePortalConstants.AUTH_CERTIFICATE,	TradePortalConstants.AUTH_CERTIFICATE.equals(userAuthMethod),	isReadOnly)%>
 
<%
  }
%>
      
<%
   String certificateType = user.getAttribute("certificate_type");
StringBuilder certificateOptions = new StringBuilder();
certificateOptions.append("<option value=\"\"");
if (!TradePortalConstants.CERTIFICATE_ID.equals(certificateType) 
&& !TradePortalConstants.GEMALTO.equals(certificateType)) certificateOptions.append("\" selected ");
certificateOptions.append("></option>");

certificateOptions.append("<option value=\"").append(TradePortalConstants.CERTIFICATE_ID).append("\"");
if (TradePortalConstants.CERTIFICATE_ID.equals(certificateType)) certificateOptions.append(" selected ");
certificateOptions.append(">")
                .append(resMgr.getText("UserDetail.CertificateID", TradePortalConstants.TEXT_BUNDLE))
                .append("</option>");

certificateOptions.append("<option value=\"").append(TradePortalConstants.GEMALTO).append("\"");
if (TradePortalConstants.GEMALTO.equals(certificateType)) certificateOptions.append(" selected ");
certificateOptions.append(">")
                .append(resMgr.getText("UserDetail.GEMALTO", TradePortalConstants.TEXT_BUNDLE))
                .append("</option>");
if ( isAdminUser ) {%>
    <%= widgetFactory.createSelectField("CertificateType", "UserDetail.CertificateType", "", certificateOptions.toString(),  isReadOnly, false, false, "onChange='displayCertificateID();'", "", "") %>
<%} else {%>
    <div class="formItemWithIndent4"> 
    <%= widgetFactory.createSelectField("CertificateType", "UserDetail.CertificateType", "", certificateOptions.toString(),  isReadOnly, false, false, "onChange='displayCertificateID();'", "", "none") %>
    </div>
<%}
String certificateIDHTML = "";
if ( TradePortalConstants.INDICATOR_YES.equals(canViewIDFields) || isAdminUser ){
    if ( isAdminUser ) {
       certificateIDHTML = widgetFactory.createTextField( "certificateID", "AdminUserDetail.CertificateID", user.getAttribute("certificate_id"), "32", isReadOnly, false, false,  "class=\"char20\"", "","" );
    }
    else {
       certificateIDHTML = widgetFactory.createTextField( "certificateID", "AdminUserDetail.CertificateID", user.getAttribute("certificate_id"), "32", isReadOnly, false, false,  "class=\"char20\"", "","none" );
    }
       if ( isAdminUser ) {%>
         <div id="certificateIDDiv"
        <%} else {%>
            <div class="formItemWithIndent4" id="certificateIDDiv" 
        <%}%>
          <%  if (!TradePortalConstants.CERTIFICATE_ID.equals(certificateType)) { %>
                style="visibility: hidden"                          
          <%  }  %>
          >
                <%= certificateIDHTML %>                          
        </div>
<script  type="text/javascript">
    function displayCertificateID() {
        require(["dijit/registry","dojo/dom-style"], function(registry,domStyle){
            var certificateTypeElement = dijit.byId("CertificateType");
            var certificateType = certificateTypeElement.value;
            if (certificateType == 'C') {
                domStyle.set('certificateIDDiv','visibility','visible');
            }
            else {
                domStyle.set ('certificateIDDiv','visibility','hidden'); 
            }
        });
    }
</script>
<%
}
%> 
  <%--cquinton 12/6/2012 end this section--%>                                   
  <div style="clear:both;"></div>
