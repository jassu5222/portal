<%--
*******************************************************************************
               User Detail - Account Access for Funds Transfer

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
	
	<table  id="accountsTable" class="formDocumentsTable" Style="width:75%" >
		<thead>
			<tr>
				<th>&nbsp;</th>
				<%--cquinton 11/16/2012 ir#6864 left align headers--%>
				<th class="headingLeft"><%=widgetFactory.createSubLabel("UserDetail.AccountNumberName")%></th>
				<th class="headingLeft"><%=widgetFactory.createSubLabel("UserDetail.AccountDescription")%></th>
			</tr>
		</thead>
		<tbody>		
			<input type=hidden value="<%=numTotalAccounts%>" name="numberOfMultipleObjects" id="numberOfMultipleObjects">
				<% for (int accIndex = 0; accIndex < numTotalAccounts; accIndex++) { %>
					<%//@ include  file="UserDetail-AcctAccessFundsXferRows.frag"%>
					<tr>	
			<td><%=(accIndex+1)+ "."%></td>
			<td>
				<%
				/*if (!isReadOnly) {
					defaultText = resMgr.getText("UserDetail.selectAccount",TradePortalConstants.TEXT_BUNDLE);
				} else {
					
				}*/
				defaultText = " ";
				options = ListBox.createOptionList(accountDoc, "ACCOUNT_OID","ACCOUNT_NUMBER",account[accIndex].getAttribute("account_oid"), userSession.getSecretKey());
				%>
				<%=widgetFactory.createSelectField("AccountOid" + accIndex,""," ", options, isReadOnly, false, false, "style='width: 26em;'","", "none")%> 
				<%out.print("<INPUT TYPE=HIDDEN NAME='UserAuthorizedAccountOid"+ accIndex + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes(account[accIndex].getAttribute("authorized_account_oid"), userSession.getSecretKey()) + "'>");%>
			</td>
			<td><%=widgetFactory.createTextField("AccountDescription"+ accIndex, "",account[accIndex].getAttribute("account_description"),"35", isReadOnly, false, false, "class='char30'", "", "none")%></td>
</tr>
				<%	} %>
		</tbody>
	</table>
		<% if (!(isReadOnly)) {		%>
			<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreAccounts">
				<%=resMgr.getText("UserDetail.Add4MoreAccounts",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					add4MoreAccounts();
			</script>
			</button>
		<% } else {		%>
					&nbsp;
		<% }		%>

<input type=hidden value='<%=accountDoc%>' name="accountDoc" id="accountDocID">
<input type=hidden value='<%=insertMode%>' name="insertMode" id="insertModeID">
				
<script language="JavaScript">


function add4MoreAccounts() {
	<%-- so that user can only add 4 rows --%>
	var tbl = document.getElementById('accountsTable');<%-- to identify the table in which the row will get insert --%>
	var lastElement = tbl.rows.length;
	var defaultText = '<%=defaultText%>'
	
	for(i=lastElement;i<lastElement+4;i++){
		
   	 	var newRow = tbl.insertRow(i);<%-- creation of new row --%>
   	 	var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
		var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
		var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
		newRow.id = "accountsRow_"+i;
		
		j = i-1;

        cell0.innerHTML =i;
        cell1.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"AccountOid'+(j)+'\" style=\"width: 26em;\" id=\"AccountOid'+(j)+'\" ></select><INPUT TYPE=HIDDEN NAME=\"UserAuthorizedAccountOid'+(j)+'\" VALUE=\"\">'
        cell2.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"AccountDescription'+j+'\" id=\"AccountDescription'+j+'\" onblur=\"this.value=this.value.toUpperCase()\" class=\'char30\'  maxLength=\"35\">'
      
		 require(["dojo/parser", "dijit/registry"], function(parser, registry) {
         	parser.parse(newRow.id);
         	var accOid = registry.byId("AccountOid0");
            if(accOid!=null){
              var theSelData = accOid.store.data;
              for(var i=0; i<theSelData.length;i++){
                      registry.byId('AccountOid'+(j)).store.data.push({name:theSelData[i].name,value:theSelData[i].value, id:theSelData[i].value});
              }
              registry.byId('AccountOid'+(j)).attr('displayedValue', "");
            }
    	 });
	}
	document.getElementById("numberOfMultipleObjects").value = eval(lastElement+3); 
}
</script>
