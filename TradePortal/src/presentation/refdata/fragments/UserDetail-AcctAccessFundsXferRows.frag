<%--this fragment is included in both the 
    Transaction-IMP_DLC-ISS-TransportDocsShipment fragment for existing
    rows as well as in the Transaction-IMP_DLC-ISS-ShipmentTermsRows.jsp
    for adding new rows via ajax.

  Passed variables:
    shipmentTermsIndex - an integer value, zero based
    isReadOnly
    isFromExpress
--%>

<%--TODO: this should iterate through the # of existing shipment terms, or create the correct number of new rows depending on the situation--%>

  <tr>	
			<td><%=(accIndex+1)+ "."%></td>
			<td>
				<%
				if (insertMode) {
					defaultText = resMgr.getText("UserDetail.selectAccount",
							TradePortalConstants.TEXT_BUNDLE);
				} else {
					defaultText = " ";
				}
				
					options = ListBox.createOptionList(accountDoc, "ACCOUNT_OID","ACCOUNT_NUMBER",
								account[accIndex].getAttribute("account_oid"), userSession.getSecretKey());
				%>
				<%=widgetFactory.createSelectField("AccountOid" + accIndex,
						"", defaultText, options, isReadOnly, false, false, "",
						"", "none")%> 
						<%out.print("<INPUT TYPE=HIDDEN NAME='UserAuthorizedAccountOid"
 				+ accIndex + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes(account[accIndex]
 						.getAttribute("authorized_account_oid"), userSession.getSecretKey()) + "'>");
 %>

			</td>
			<td><%=widgetFactory.createTextField("AccountDescription"
						+ accIndex, "",
						account[accIndex].getAttribute("account_description"),
						"32", isReadOnly, false, false, "class='char37'", "", "none")%></td>
			</tr>