<%--
*******************************************************************************
                                    User Detail - Assigned TO Section

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
<%--cquinton 11/16/2012 reshuffled so spacing is correct for the field--%>
  <div class='formItem<%=isReadOnly?" readOnly":""%>'>
    <%=widgetFactory.createInlineLabel("PanelAuthority","UserDetail.PanelAutority")%>
    <div style="clear:both;"></div>
    <%=widgetFactory.createNote("UserDetail.PanelGroupNote")%>
    <div style="clear:both;"></div>
<%
//CR 821 - Rel 8.3 PMitnala START
try{
	  //MEer Rel 8.3 IR-19699- Updated method call with locale as parameter
	  //MEer Rel 8.3 T36000021992 -Updated method call with a boolean to ignore default panel level
	panelAliasesMap = corpOrgBean.getPanelLevelAliases(userSession.getOwnerOrgOid(), userSession.getUserLocale(), ignoreDefaultPanelValue);
	panelAliasesOptions = widgetFactory.createSortedOptions("PANEL_AUTH_TYPE", panelAliasesMap, user.getAttribute("panel_authority_code"));
	
}catch(Exception e){
	Debug.debug("Exception caught in calling CorporateOrganizationBean::getPanelLevelAliases()-"+ e.toString());
}
//  options = Dropdown.createSortedRefDataOptions("PANEL_AUTH_TYPE",user.getAttribute("panel_authority_code"),loginLocale);
//CR 821 - Rel 8.3 END
                      
  if (isReadOnly) {
    defaultText = "";
  } 
  else {
    defaultText = resMgr.getText("UserDetail.selectPanelAutority",
      TradePortalConstants.TEXT_BUNDLE);
  }
%>
    <%=widgetFactory.createSelectField("PanelAuthority",
					"", " ", panelAliasesOptions, isReadOnly,
					false, false, "", "", "none")%>
  </div>
 
 
  <%=widgetFactory.createCheckboxField("AuthorizeOwnInputInd","UserDetail.AuthorizeOwnOutput",
       TradePortalConstants.INDICATOR_YES.equals(user.getAttribute("authorize_own_input_ind")),isReadOnly)%>
       
    <div class="formItemWithIndent2">
      <%=widgetFactory.createNote("UserDetail.AuthorizeOwnOutputNote")%>
    </div>
