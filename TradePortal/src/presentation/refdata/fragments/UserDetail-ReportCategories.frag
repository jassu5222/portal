<%--
*******************************************************************************
                                    User Detail - Template Groups Section

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
<div>
<%=widgetFactory.createSubLabel("UserDetail.SelectReptCategsText")%>
</div>
<br/>
<%
      sqlQuery = new StringBuffer();
  	  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
	  sqlQuery.append("select report_categories_ind from client_bank");
	  sqlQuery.append(" where organization_oid = ?");
      Debug.debug(sqlQuery.toString());
      queryListView.setSQL(sqlQuery.toString(),new Object[]{ userSession.getClientBankOid()});
      queryListView.getRecords();
      String reportCategInd = queryListView.getRecordValue("report_categories_ind");
      if ("Y".equals(reportCategInd)) {
      %>
      <%
      // jgadela Rel 8.4 CR-854 [Begin]- Adding reporting language option
      DocumentHandler reportCategsDoc = null;
      String repLan = user.getAttribute("reporting_language");
      if(repLan!= null && repLan.toLowerCase().contains("fr")){
			reportCategsDoc = reportCategsDocFr;
		}else{
			reportCategsDoc = reportCategsDocEn;
		}
      // jgadela Rel 8.4 CR-854 [END]- Adding reporting language option
      %>
     
<div>      
<div class="columnLeft">
			<div>
			<%  defaultText = " ";
           		dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   									   userDetailReptCateg1Oid, userSession.getSecretKey());	%>
        	<%=widgetFactory.createSelectField("ReportingCategory1", "", " ",
							dropdownOptions, isReadOnly, false, false, "class='char32'", "", "none")%> <br/>
			</div>
			<div>&nbsp;</div>
			<div>
			<% defaultText = " ";
           	   dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   										userDetailReptCateg2Oid, userSession.getSecretKey());  %>
			<%=widgetFactory.createSelectField("ReportingCategory2", "", " ",
							dropdownOptions, isReadOnly, false, false, "class='char32'", "", "none")%> <br/>        		   										
			</div>
		<div>&nbsp;</div>
			<div>
			<%  defaultText = " ";
           		dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   									   userDetailReptCateg3Oid, userSession.getSecretKey());	%>
        	<%=widgetFactory.createSelectField("ReportingCategory3", "", " ",
							dropdownOptions, isReadOnly, false, false, "class='char32'", "", "none")%> <br/>
			</div>
			<div>&nbsp;</div>
			<div>
			<% defaultText = " ";
           	   dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   										userDetailReptCateg4Oid, userSession.getSecretKey());  %>
			<%=widgetFactory.createSelectField("ReportingCategory4", "", " ",
							dropdownOptions, isReadOnly, false, false, "class='char32'", "", "none")%>         		   										
			</div><div>&nbsp;</div>
			<div>
			<%  defaultText = " ";
           		dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   									   userDetailReptCateg5Oid, userSession.getSecretKey());	%>
        	<%=widgetFactory.createSelectField("ReportingCategory5", "", " ",
							dropdownOptions, isReadOnly, false, false, "class='char32'", "", "none")%> 
			</div>
</div>						
<div class="columnRight">
			<div>
			<% defaultText = " ";
           	   dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   										userDetailReptCateg6Oid, userSession.getSecretKey());  %>
			<%=widgetFactory.createSelectField("ReportingCategory6", "", " ",
							dropdownOptions, isReadOnly, false, false, "class='char32'", "", "none")%>         		   										
			</div>
			<div>&nbsp;</div>
			<div>
			<% defaultText = " ";
           	   dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   									   userDetailReptCateg7Oid, userSession.getSecretKey());	%>
        	<%=widgetFactory.createSelectField("ReportingCategory7", "", " ",
							dropdownOptions, isReadOnly, false, false, "class='char32'", "", "none")%> 
			</div>
			<div>&nbsp;</div>
			<div>
			<% defaultText = " ";
           	   dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   										userDetailReptCateg8Oid, userSession.getSecretKey());  %>
			<%=widgetFactory.createSelectField("ReportingCategory8", "", " ",
							dropdownOptions, isReadOnly, false, false, "class='char32'", "", "none")%>         		   										
			</div>
			<div>&nbsp;</div>
			<div>
			<% defaultText = " ";
           	   dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   									   userDetailReptCateg9Oid, userSession.getSecretKey());	%>
        	<%=widgetFactory.createSelectField("ReportingCategory9", "", " ",
							dropdownOptions, isReadOnly, false, false, "class='char32'", "", "none")%> 
			</div>
			<div>&nbsp;</div>
			<div>
			<% defaultText = " ";
           	   dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR",
        		   										userDetailReptCateg10Oid, userSession.getSecretKey());  %>
			<%=widgetFactory.createSelectField("ReportingCategory10", "", " ",
							dropdownOptions, isReadOnly, false, false, "class='char32'", "", "none")%>         		   										
			</div>
</div>		
</div>
<% } %>