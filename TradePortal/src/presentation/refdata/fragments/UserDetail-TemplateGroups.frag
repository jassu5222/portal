<%--
*******************************************************************************
                                    User Detail - Template Groups Section

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
<div>
  <%=widgetFactory.createNote("UserDetail.TemplateGrouptobeUsedforUsers")%>
  <table id="templateGroupsTable" class="formDocumentsTable" style="width:55%">
    <colgroup>
      <col span="1" style="width: 5%;">
      <col span="1" style="width: 40%;">
      <col span="1" style="width: 5%;">
      <col span="1" style="width: 40%;">
    </colgroup>

	<thead>
		<tr>
			<th>&nbsp;</th>
			<%--cquinton 11/16/2012 ir#6864 left align headers--%>
			<th class="headingLeft"><%=widgetFactory.createSubLabel("UserDetail.TemplateGroupsName")%></th>
			<th>&nbsp;</th>
			<th class="headingLeft"><%=widgetFactory.createSubLabel("UserDetail.TemplateGroupsName")%></th>
		</tr>
	</thead>

	<tbody>
		<%
			if (TradePortalConstants.INDICATOR_YES.equals(canViewPaymentTemplateGroups)) {
		%>
		
		<input type=hidden value="<%=numTotalTemplateGroups%>" name="numberOfMultipleObjects1" id="noOfTemplateGroupRows">
			<%
			int iLoop = 0;
			while(iLoop < numTotalTemplateGroups){%>
				<%//@ include file="UserDetail-TemplateGroupsTermRows.frag"%>
				
				<tr>
					<%
					
					if (!isReadOnly) {
							defaultText = resMgr.getText("UserDetail.selectTemplateGroup",TradePortalConstants.TEXT_BUNDLE);
						} else {
							defaultText = " ";
						}	
						%>						
					<% for( int iColumnCount = 0; iColumnCount < 2; iColumnCount++){ %>
						<td><%=(iLoop + iColumnCount +1) + "."%></td>
						<td> <%try{%>
									<%options = ListBox.createOptionList(templateGroupDoc,"PAYMENT_TEMPLATE_GROUP_OID","NAME",templateGroup[iLoop+iColumnCount].getAttribute("payment_template_group_oid"),userSession.getSecretKey());
									%>
									<%=widgetFactory.createSelectField("PaymentTemplateGroupOid" + (iLoop+iColumnCount), "", " ",options, isReadOnly, false, false, "", "", "none")%> 
									<% 	out.print("<INPUT TYPE=HIDDEN NAME='UserAuthorizedTemplateGroupOid"+ (iLoop+iColumnCount) +"' VALUE='"+ EncryptDecrypt.encryptStringUsingTripleDes(templateGroup[iLoop+iColumnCount].getAttribute("authorized_template_group_oid"), userSession.getSecretKey())+ "'>"); %>
						     <%}catch(Exception e){
						    		 options = ListBox.createOptionList(templateGroupDoc,"PAYMENT_TEMPLATE_GROUP_OID","NAME","",userSession.getSecretKey());%>
									<%=widgetFactory.createSelectField("PaymentTemplateGroupOid" + (iLoop+iColumnCount), "", " ",options, isReadOnly, false, false, "", "", "none")%> 
									<%out.print("<INPUT TYPE=HIDDEN NAME='UserAuthorizedTemplateGroupOid"+ (iLoop+iColumnCount) +"' VALUE=''>"); 
						     }
						     %>
						</td>
					<% } %>			
				</tr>
					<%iLoop = iLoop+2; 		
				}
			}%>
	</tbody>
</table>
	<% if (!(isReadOnly)) {		%>
			<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreTemplateGroups">
				<%=resMgr.getText("UserDetail.Add4MoreTemplateGroup",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					add4MoreTemplateGroups();
				</script>
			</button>
		<% } else {		%>
					&nbsp;
		<% }		%>

<input type=hidden value='<%=templateGroupDoc%>' name="templateGroupDoc" id="templateGroupDocID">
<input type=hidden value='<%=insertMode%>' name="insertMode" id="insertModeID">
	</div>			
<script language="JavaScript">  


function add4MoreTemplateGroups() {
	<%-- so that user can only add 2 rows --%>
	var tbl = document.getElementById('templateGroupsTable');<%-- to identify the table in which the row will get insert --%>
	var lastElement = tbl.rows.length;
	var defaultText = '<%=defaultText%>'
	
	for(i=lastElement;i<lastElement+2;i++){
		
   	 	var newRow = tbl.insertRow(i);<%-- creation of new row --%>
   	 	var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
		var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
		var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
		var cell3 = newRow.insertCell(3);<%-- forth  cell in the row --%>
		newRow.id = "tempGroupRow_"+i;
		j = i-1;

        cell0.innerHTML =(j*2)+1;
        cell1.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"PaymentTemplateGroupOid'+(j*2)+'\" id=\"PaymentTemplateGroupOid'+(j*2)+'\"></select><INPUT TYPE=HIDDEN NAME=\"UserAuthorizedTemplateGroupOid'+(j*2)+'\" VALUE=\"\">'
        cell2.innerHTML =(j*2)+2;
        cell3.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"PaymentTemplateGroupOid'+(j*2+1)+'\" id=\"PaymentTemplateGroupOid'+(j*2+1)+'\"></select><INPUT TYPE=HIDDEN NAME=\"UserAuthorizedTemplateGroupOid'+(j*2+1)+'\" VALUE=\"\">'
		 require(["dojo/parser", "dijit/registry"], function(parser, registry) {
         	parser.parse(newRow.id);
         	var templateGroupOid = registry.byId("PaymentTemplateGroupOid0");
            if(templateGroupOid!=null){
              var theSelData = templateGroupOid.store.data;
              for(var i=0; i<theSelData.length;i++){
                      registry.byId('PaymentTemplateGroupOid'+(j*2)).store.data.push({name:theSelData[i].name,value:theSelData[i].value, id:theSelData[i].value});
                      registry.byId('PaymentTemplateGroupOid'+(j*2+1)).store.data.push({name:theSelData[i].name,value:theSelData[i].value, id:theSelData[i].value});
              }
              registry.byId('PaymentTemplateGroupOid'+(j*2)).attr('displayedValue', "");
              registry.byId('PaymentTemplateGroupOid'+(j*2+1)).attr('displayedValue', "");
            }
    	 });
	}
	document.getElementById("noOfTemplateGroupRows").value = eval((lastElement+1)*2); 
}
 
</script>
