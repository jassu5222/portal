<%--
*******************************************************************************
                         User Login Password Fields

  Description:
     This fragment is used to construct the login and password fields for both 
  the Admin and User Detail pages.

  Assumes the following variables are defined and valued:  
  reqdForLogin, reqdForPassword
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  //cquinton 2/19/2013 ir#11753
  //by default login is not required
  boolean loginIdRequired = false;
  if (TradePortalConstants.AUTH_PASSWORD.equals(corpAuthMethod) ||
      TradePortalConstants.AUTH_2FA.equals(corpAuthMethod) ) {
    //for corp auth method = password or 2fa, it is always required
    loginIdRequired = true;
  }
  else if (TradePortalConstants.AUTH_PERUSER.equals(corpAuthMethod) ) {
    //for peruser, we default to required so we get a validationtextbox, 
    // but removed requiredness via javascript as necessary
    loginIdRequired = true;
  }

  String passwordFieldLabel;
  String retypePasswordFieldLabel;

  if (insertMode)
  {
    passwordFieldLabel       = resMgr.getText("UserDetail.Password",
                                              TradePortalConstants.TEXT_BUNDLE);
    retypePasswordFieldLabel = resMgr.getText("UserDetail.RetypePassword", 
                                              TradePortalConstants.TEXT_BUNDLE);
  }
  else
  {
    passwordFieldLabel       = resMgr.getText("UserDetail.NewPassword",
                                              TradePortalConstants.TEXT_BUNDLE);
    retypePasswordFieldLabel = resMgr.getText("UserDetail.RetypeNewPassword", 
                                              TradePortalConstants.TEXT_BUNDLE);
  }


  if (corpAuthMethod.equals(TradePortalConstants.AUTH_PERUSER) && !isAdminUser) {
%>
 <%-- KMehta on 16 Dec 2015 @ Rel 9200 for IR T36000034128 Start --%>
  <%= widgetFactory.createRadioButtonField("AuthenticationMethod", "TradePortalConstants.AUTH_PASSWORD", "UserDetail.LoginIDPassword",
        TradePortalConstants.AUTH_PASSWORD, TradePortalConstants.AUTH_PASSWORD.equals(userAuthMethod), isReadOnly, " onClick=\"clear2FA()\"", "")%>
<%-- KMehta on 16 Dec 2015 @ Rel 9200 for IR T36000034128 End --%>

<%
  }
%>

  <%--cquinton 12/6/2012 restoring formItem usage so field data displayed correctly when readonly
      wrapping the whole thing with some indenting--%>
  <div id="UserLoginPwd" class="indentHalf">
    <%--cquinton 2/19/2013 ir#11753 set requiredness as above--%>
    <%= widgetFactory.createTextField( "loginID", "UserDetail.LoginID", user.getAttribute("login_id"), "32", 
        isReadOnly, loginIdRequired, false,  "class='char30'", "","" ) %>                          
    
<% 
	// [START] CR-482 Blank-out the password fields; pointless to return a hashed password to the user
	if (instanceHSMEnabled)
	{
		doc.setAttribute("/In/User/newPassword", "");
		doc.setAttribute("/In/User/retypePassword", "");
	}
	// [END] CR-482
	;
  // Peter Ng IR PKUJ012560867 2/5/2009 End
  if (doc.getAttribute("/In/User/newPassword") != null ) {
    //password1 = doc.getAttribute("/In/User/newPassword");
  }
  if (doc.getAttribute("/In/User/retypePassword") != null ) {
    //password2 = doc.getAttribute("/In/User/retypePassword");
  }
  // Peter Ng IR PKUJ012560867 2/5/2009 End

  // Do not show the password sections if in read only mode

  if (!isReadOnly) {
%>
    <%= widgetFactory.createTextField( "Password1", passwordFieldLabel, password1, "30", isReadOnly, false, false,  "type=\"password\" AutoComplete=\"off\" ", "","" ) %>
       
    <%= widgetFactory.createTextField( "Password2", retypePasswordFieldLabel, password2, "30", isReadOnly, false, false,  "type=\"password\" AutoComplete=\"off\" ", "","" ) %>
<%
  }
%>
  </div>

<%--cquinton 2/19/2013 ir#11753 remove inline javascript - its supposed to be at bottom of page (not just frag)--%>
