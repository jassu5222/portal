<%--
*******************************************************************************
                         User SSO Fields

  Description:
     This fragment is used to construct the SSO ID fields for both 
  the Admin and User Detail pages.
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  if (!corpAuthMethod.equals(TradePortalConstants.AUTH_SSO)
      && !corpAuthMethod.equals(TradePortalConstants.AUTH_PERUSER)) {
%>
          <%//=resMgr.getText("UserDetail.TransitioningToSSOAuth", TradePortalConstants.TEXT_BUNDLE)%>
          <%//= widgetFactory.createLabel("", "UserDetail.TransitioningToSSOAuth", false, false, false, "") %>
        
<%
  } else if (corpAuthMethod.equals(TradePortalConstants.AUTH_PERUSER) && !isAdminUser ) {
%>
        
        <%= widgetFactory.createRadioButtonField("AuthenticationMethod",	"TradePortalConstants.AUTH_SSO","UserDetail.UseSSO",
							TradePortalConstants.AUTH_SSO,	TradePortalConstants.AUTH_SSO.equals(userAuthMethod),	isReadOnly)%>
        
<%
  }
%>

  <% if ( isAdminUser ) { %>

    <%= widgetFactory.createTextField( "ssoID", "UserDetail.SSOID", user.getAttribute("sso_id"), "32", 
          isReadOnly, false, false,  "class='char20'", "","" ) %>
          
  <% } else { %>
       <%--cquinton 12/6/2012 restoring formItem usage so fields display as readonly correctly. wrapping all in an indent--%>
       <div class="indentHalf">
            <%= widgetFactory.createTextField( "ssoID", "UserDetail.SSOID", user.getAttribute("sso_id"), "32", 
                isReadOnly, false, false,  "class='char20'", "","" ) %>
            <%= widgetFactory.createCheckboxField("AllowSSOPasswordInd","UserDetail.AllowSSOPassword",
			user.getAttribute("allow_sso_password_ind").equals(TradePortalConstants.INDICATOR_YES),isReadOnly,false,"","","") %>

        </div>
   
  <% } %>
