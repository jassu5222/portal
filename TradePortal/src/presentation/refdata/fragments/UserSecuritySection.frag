<%--
*******************************************************************************
                             User Security Section

  Description:
     This page builds the Security section for both the Admin and User Detail
  pages.

*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  // To reduce the number of times we have to check corpAuthMethod, set up 
  // the required indicators to nothing or the standard asterisk.
  String reqdForLogin;
  String reqdForPassword;
  String reqdForCertificate;
  String reqdFor2FA;
  String reqdForSSOId;
  //BSL 08/31/11 CR663 Rel 7.1 Begin
  String reqdForSSOByRegistrationLogin = "";
  String reqdForSSOByRegistrationPassword = "";
  //BSL 08/31/11 CR663 Rel 7.1 End
  String defaultAdminAuthMethod = "";
	String defaultDisplayDiv = "";
	String defaultSelectValue = "";

  if (corpAuthMethod.equals(TradePortalConstants.AUTH_PASSWORD)) { 
     reqdForLogin = "<span class=Asterix>*</span>";
     reqdForPassword = "<span class=Asterix>*</span>";
     reqdForCertificate = "";
     reqdFor2FA = "";
     reqdForSSOId = "";

     // When an existing user has a non-blank password, the password is not
     // required to be entered.  We remember the current value.
     if (InstrumentServices.isNotBlank(user.getAttribute("password"))
        && (!insertMode)) {
        reqdForPassword = "";
     }
  } else if (corpAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)) {
     reqdForLogin = "";
     reqdForPassword = "";
     reqdForCertificate = "<span class=Asterix>*</span>";
     reqdFor2FA = "";
     reqdForSSOId = "";
  }
  else if (corpAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)) {
     reqdForLogin = "";
     reqdForPassword = "";
     reqdForCertificate = "";
     reqdFor2FA = "";
     reqdForSSOId = "<span class=Asterix>*</span>";
  }
  //BSL 08/31/11 CR663 Rel 7.1 Begin
  else if (corpAuthMethod.equals(TradePortalConstants.AUTH_REGISTERED_SSO)) {
     reqdForLogin = "";
     reqdForPassword = "";
     reqdForCertificate = "";
     reqdFor2FA = "";
     reqdForSSOId = "";
     reqdForSSOByRegistrationLogin = "<span class=Asterix>*</span>";

     // When an existing user has a non-blank password, the password is not
     // required to be entered.  We remember the current value.
     if (InstrumentServices.isNotBlank(user.getAttribute("registration_password"))
    		 && (!insertMode)) {
    	 reqdForSSOByRegistrationPassword = "";
     }
     else {
    	 reqdForSSOByRegistrationPassword = "<span class=Asterix>*</span>";
     }
  }
  //BSL 08/31/11 CR663 Rel 7.1 End
  else if (corpAuthMethod.equals(TradePortalConstants.AUTH_2FA)) {
      reqdForLogin = "";
      reqdForPassword = "";
      reqdForCertificate = "";
      reqdFor2FA = "<span class=Asterix>*</span>";
      reqdForSSOId = "";
   }
  else { //AUTH_PERUSER
     reqdForLogin = "";
     reqdForPassword = "";
     reqdForCertificate = "";
     reqdFor2FA = "";
     reqdForSSOId = "";
  }

  if(isAdminUser){
%>
<%@ include file="AdminUserSecuritySection.frag"%>  
<%
  }else{

  if (corpAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)) {
%>
  <div class="columnLeft"> 
    <%@ include file="UserCertificateFields.frag" %>

    <%@ include file="UserLoginPasswordFields.frag" %>
  </div> 
  <div class="columnRight"> 
    <%@ include file="User2FAFields.frag" %>

    <%@ include file="UserSSOFields.frag" %>

    <%@ include file="UserSSOByRegistrationFields.frag" %><%-- BSL 08/31/11 CR663 Rel 7.1 Add --%>
  </div>
<%
  } else if (corpAuthMethod.equals(TradePortalConstants.AUTH_SSO)) {
%>
  <div class="columnLeft"> 
    <%@ include file="UserSSOFields.frag" %>

    <%@ include file="UserLoginPasswordFields.frag" %>
  </div>
  <div class="columnRight"> 
    <%@ include file="UserCertificateFields.frag" %>

    <%@ include file="User2FAFields.frag" %>

    <%-- BSL 08/31/11 CR663 Rel 7.1 Begin --%>
    <%@ include file="UserSSOByRegistrationFields.frag" %>
  </div>
<%
  } else if (corpAuthMethod.equals(TradePortalConstants.AUTH_REGISTERED_SSO)) {
%>
  <div class="columnLeft"> 
    <%@ include file="UserSSOByRegistrationFields.frag" %>
	
    <%@ include file="UserLoginPasswordFields.frag" %>
  </div>
  <div class="columnRight"> 
    <%@ include file="UserCertificateFields.frag" %>

    <%@ include file="User2FAFields.frag" %>

    <%@ include file="UserSSOFields.frag" %>
  </div>
  <%-- BSL 08/31/11 CR663 Rel 7.1 End --%>
<%
  } else if (corpAuthMethod.equals(TradePortalConstants.AUTH_2FA)) {
%>
  <div class="columnLeft"> 
    <%@ include file="User2FAFields.frag" %>

    <%@ include file="UserLoginPasswordFields.frag" %>
  </div>
  <div class="columnRight"> 
    <%@ include file="UserCertificateFields.frag" %>

    <%@ include file="UserSSOFields.frag" %>

    <%@ include file="UserSSOByRegistrationFields.frag" %><%-- BSL 08/31/11 CR663 Rel 7.1 Add --%>
  </div>
<%
  } else { 
%>
  <div class="columnLeft"> 
    <%@ include file="UserLoginPasswordFields.frag" %>

    <%--cquinton 11/15/2012 moving 2fa fields below login/password to even out section
        note that when an admin user ssoregistration does not display, so can't move too much--%>
    <%@ include file="User2FAFields.frag" %>
  </div>
  <div class="columnRight"> 
    <%@ include file="UserCertificateFields.frag" %>

    <%@ include file="UserSSOFields.frag" %>

    <%@ include file="UserSSOByRegistrationFields.frag" %><%-- BSL 08/31/11 CR663 Rel 7.1 Add --%>
  </div>   
<%
  }
%>
  <div style="clear:both;"></div> 

<% 
}//else isAdminUser
  //cquinton 11/15/2012 - moved additional auth here so it it commonly used by user preferences
  if (!isAdminUser) { 	  
%>
  <div>
    <%=widgetFactory.createLabel("", "UserDetail.AdditionalAuthentication", false, false, false, "")%>
    <div class="formItem">
        
      <%=widgetFactory.createRadioButtonField("AdditionalAuthType","AdditionalAuthType-Y","UserDetail.AdditionalAuth-Certificate", TradePortalConstants.AUTH_CERTIFICATE,	TradePortalConstants.AUTH_CERTIFICATE.equals(additionalAuthType),isReadOnly, " onClick=\"setOptions()\"", "")%>
      <br/>
      <%=widgetFactory.createRadioButtonField("AdditionalAuthType","AdditionalAuthType-N","UserDetail.AdditionalAuth-2FA", TradePortalConstants.AUTH_2FA, TradePortalConstants.AUTH_2FA.equals(additionalAuthType),isReadOnly," onClick=\"setOptions()\"", "")%>
<%
    //CR711. Rel8.0. 4/17/2012 - Disable the fields if user can not view the id fields
    boolean unableTOView = false;
         
    if (!(TradePortalConstants.INDICATOR_YES.equals(canViewIDFields) || userSecurityType.equals(TradePortalConstants.ADMIN))) {
      unableTOView = true;
    }
%>	
      <br/>
      <div class="formItem">
        <%=widgetFactory.createRadioButtonField("Auth2FAType","TradePortalConstants.AUTH_2FA_SUBTYPE_OTP","UserDetail.OTP2FA", TradePortalConstants.AUTH_2FA_SUBTYPE_OTP,	TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equals(additionalAuth2FAType),unableTOView, "", "")%>
        <br/>
        <%=widgetFactory.createRadioButtonField("Auth2FAType","TradePortalConstants.AUTH_2FA_SUBTYPE_SIG","UserDetail.SigCode2FA", TradePortalConstants.AUTH_2FA_SUBTYPE_SIG, TradePortalConstants.AUTH_2FA_SUBTYPE_SIG.equals(additionalAuth2FAType), unableTOView, "", "")%>      
      </div>
    </div>
  </div>
  <div style="clear:both;"></div>   
<% 
  } 
%>
