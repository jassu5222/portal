<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%
Debug.debug("***START*********************CUSTOM*EDIT*REPORT****************************START***"); 
StringBuffer        link                        = null;
String              storageToken                = null;


storageToken = request.getParameter("entry");

link = new StringBuffer();
link.append(NavigationManager.getNavMan().getPhysicalPage("EditReportStep2", request));
link.append("?entry=");
link.append(storageToken);
link.append("&reportMode=");
link.append(TradePortalConstants.REPORT_EDIT_CUSTOM);


%>

<jsp:forward page='<%=link.toString()%>' />

<%
Debug.debug("***END*********************CUSTOM*EDIT*REPORT****************************END***"); 
%>