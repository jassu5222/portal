<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, com.ams.tradeportal.common.cache.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,com.crystaldecisions.sdk.framework.*,java.util.*,
                 com.ams.tradeportal.busobj.util.SecurityAccess, java.net.URLEncoder" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>
<%@ include file="fragments/reportVariables.frag" %>
<%
Debug.debug("***START*********************CUSTOM*REPORTS*PAGE***************************START***"); 
WidgetFactory widgetFactory = new WidgetFactory(resMgr);
StringBuffer      dropdownOptions              = new StringBuffer();
String options = ""; 
String buttonPressed = "";
Vector error = null;
String              userName                = "";
String              oID                     = "";
int                 intOID                  = 0;
int                 iRepoType               = 0;

String              urlParm                 = "";


boolean             gooddoc                 = false;
String              cell                    = "";

MediatorServices    mediatorServices        = null;
DocumentHandler     xmlDoc                  = null;
String              physicalPage            = "";
String 				newReport				= "";
int iPrompts = 0;

//BO XIR2 objects
DocumentInstance 	widDoc 		= null;
Prompts 		widPrompts 	= null;
Reports 		widReports 	= null;
Report 			widReport 	= null;
PaginationMode 		pgMode 		= null;
HTMLView		repView 	= null;

int 			totalNoPages 	= 0;
PageNavigation 		pgn 		= null;
String 			pageCall 	= ""; 
String 			pageNum 	= "";
String 			pageMode 	= "";
int 			prevPage 	= 0;
int 			nextPage 	= 0;
int 			goToPage 	= 0;

PropertyResourceBundle portalProperties = null;
portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");

//cquinton 2/14/2013 add a variable to see if on entry page is expanded or not
String expandedPage= request.getParameter("expandedPage");

if ( expandedPage == null ) {
  expandedPage = "N";
}

pageMode = portalProperties.getString("pageMode");
String securityRights = userSession.getSecurityRights();
if( pageMode == null)
{
    pageMode = "n";

}

if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
{
	//The pageCall variable determines if the request is coming from
	//the same page of a different page
	if (request.getParameter("pageCall")!=null)
	{
		pageCall = request.getParameter("pageCall");
	}
	else
	{
		pageCall ="N";
	}

	if (request.getParameter("goToPage")!=null) 
	{
		pageNum = request.getParameter("goToPage");
		goToPage = (new Integer(pageNum).intValue());
	}
	else 
	{
  	 	goToPage = 1;
	}
}
else
{
	pageCall ="N";
}
 
if(request.getParameter("Name")!=null)
{
	//repName = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("Name"), userSession.getSecretKey());
	repName = request.getParameter("Name");
	//repName = EncryptDecrypt.base64StringToString(repName);
}

if(request.getParameter("oid")!=null)
{
	//oID = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
	oID = request.getParameter("oid");
	//oID = EncryptDecrypt.base64StringToString(oID);
	intOID = (new Integer(oID).intValue());
}


if(request.getParameter("ReportType")!=null)
{
	//reportType = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("ReportType"), userSession.getSecretKey());
	reportType = request.getParameter("ReportType");
	//reportType = EncryptDecrypt.base64StringToString(reportType);
	iRepoType = (new Integer(reportType).intValue());
}
//Rel 9.3 XSS CID 11252, 1260
if(request.getParameter("sToken")!=null)
{
	//sToken = StringFunction.xssCharsToHtml(request.getParameter("sToken"));
	sToken = request.getParameter("sToken");
}
%>
<%@ include file="fragments/wistartpage.frag" %>
<%
try
{
 	if (reportEngine.isReady())
    {    
    	 if (null != pageCall && pageCall.equals("Y"))
		{
			  //If the request is coming from the current page then open the
			  //page from the storage token
			  widDoc = reportEngine.getDocumentFromStorageToken(sToken);
		}
		else
		{
			//Open the report using the report details for the first time
			//modified by kajal bhatt
			widDoc = reportEngine.openDocument(intOID);

			//This call will refresh the report widDoc.refresh();
			widDoc.refresh();
		}  
	//	sToken = StringFunction.xssCharsToHtml(widDoc.getStorageToken());
		sToken = widDoc.getStorageToken();
		widReports = widDoc.getReports();
		//need to change this code to add report item
		widReport = widReports.getItem(0);
		widReport.setPaginationMode(PaginationMode.QuickDisplay);
		pgn = widReport.getPageNavigation();
		pgn.last();
		totalNoPages = pgn.getCurrent();
		
		System.out.println("Custom Reports = "+ totalNoPages);

		if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
		{			//Set the page number
			pgn.setTo(goToPage);
			repView = (HTMLView) widReport.getView(OutputFormatType.HTML);
		}
	}
	else
	{
		System.out.println("XIR2 Reports Engine is not ready ######################");
		xmlDoc = formMgr.getFromDocCache();
		mediatorServices = new MediatorServices();
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
		mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		mediatorServices.addErrorInfo();
		xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
		xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
		formMgr.storeInDocCache("default.doc", xmlDoc);
		physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
		%>
		<jsp:forward page='<%= physicalPage %>' />
		<%
	}
}
catch (REException rex)
{
	  //Catch all exception
	  System.out.println("EXCEPTION in Custom Reports Page ######################"+rex.getCode()+" "+rex.getMessage());                 
	  xmlDoc = formMgr.getFromDocCache();
	  mediatorServices = new MediatorServices();
	  mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
	  mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
	  mediatorServices.addErrorInfo();
	  xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
	  xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES); 
	  formMgr.storeInDocCache("default.doc", xmlDoc);       
	  physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);

	  %>
	  <jsp:forward page='<%= physicalPage %>' />
	  <%
}
/* 
if (xmlDoc.getDocumentNode("/In/Update/ButtonPressed") != null) 
{
	buttonPressed = xmlDoc.getAttribute("/In/Update/ButtonPressed"); 	
	error = xmlDoc.getFragments("/Error/errorlist/error");
}
Debug.debug("doc from cache is " + xmlDoc.toString()); */

try
{
	link = new StringBuffer();
	widPrompts = widDoc.getPrompts();
    iPrompts = widPrompts.getCount();
    if (iPrompts > 0)
    { 
		//Navigation logic to prompt page
		link.append(NavigationManager.getNavMan().getPhysicalPage("CustomReportPrompt1", request));
		link.append("?storageToken=");
		link.append(sToken);      
		%>
		 <jsp:forward page='<%=link.toString()%>' />
		<%
    }
}catch (REException rex)
{     
	System.out.println( "Excepton in Standard Reports Page"+rex.getMessage());    
	physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
	%>
		<jsp:forward page='<%= physicalPage %>' />
	<%
}

repName = widDoc.getProperties().getProperty(PropertiesType.NAME);
reportAuthor = widDoc.getProperties().getProperty(PropertiesType.AUTHOR);

// Place the reporting user suffix on to the end of the OID
// The suffix is used to distinguish between organizations being processed
// by different ASPs that might be on the same reports server.  
userName = userSession.getOwnerOrgOid()+userSession.getReportingUserSuffix();

//jgadela R 8.4 CR-854 T36000024797   -[START] Reporting page labels. 
	String reportingPageLabelLan = null;
	String cbReportingLangOptionInd = null;

	Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
	DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
	if (!StringFunction.isBlank(userSession.getClientBankOid())){
	   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 
	}

	if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)){
		reportingPageLabelLan = userSession.getReportingLanguage();
	}else{
		reportingPageLabelLan = userSession.getUserLocale();
	}
//jgadela R 8.4 CR-854 T36000024797   -[END]
%>


<%--*******************************HTML for page begins here **************--%>


<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="expandedPage" value="<%=StringFunction.xssCharsToHtml(expandedPage)%>" />
</jsp:include>
	
<div class="pageMain">
	<div class="pageContent">
		<%-- End HTML header decleration --%>
	<%  String pageTitleKey;%>
  	<% String current2ndNav ="C"; %>
  	<%String helpLink = OnlineHelp.createContextSensitiveLink("customer/report.htm",resMgr,userSession); %>
	<%@ include file="/reports/fragments/Reports-SecondaryNavigation.frag" %>
	<input type=hidden value='<%=current2ndNav%>' name="current2ndNav" id="current2ndNav">
	
	

<%		
  	pageTitleKey = repName;
	// figures out how to return to the calling page
	StringBuffer parms = new StringBuffer();
	parms.append("&returnAction=");
	String selectedCategory = request.getParameter("selectedCategory");
	parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToReportsHome", userSession.getSecretKey()));
	//String returnLink = formMgr.getLinkAsUrl("goToReportsHome", parms.toString(), response);
	String returnLink = formMgr.getLinkAsUrl("goToReportsHome", "&reportTab=C&current2ndNav=C&selectedCategory="+selectedCategory, response);
	
	
	String report=request.getParameter("reportType");
	
%>
<%
         //Download report button
         newLink = new StringBuffer();
         newLink.append(request.getContextPath());
         // W Zhu 10/25/12 Rel 8.1 KJUM101652104 T36000006922 remove /.jsp
         newLink.append("/common/TradePortalDownloadServlet?");
         newLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
         newLink.append("=");
         newLink.append(TradePortalConstants.REPORT_DOWNLOAD);
         newLink.append("&storageToken=" + sToken);
         csvReport = newLink.toString();
      %>
  
  <%  //Save as Standard Reports button
//Create a copy of this report button
  newLink = new StringBuffer();
  newLink.append(formMgr.getLinkAsUrl("goToEditReportStep3", response));
  newLink.append("&storageToken=" + sToken);
  newLink.append("&reportMode=" + TradePortalConstants.REPORT_COPY_CUSTOM);
  saveAsNavPage = formMgr.getLinkAsUrl("goToEditReportStep3", response);      
  %>
  <%    //Reports delete button
  	reportTab = TradePortalConstants.CUSTOM_REPORTS;
  %>   
  
  <%
  	String 	repOwn  = reportAuthor.toString();;
  	
  	showDeleteButton = false;
    showEditButton = false;
    showSaveAsButton = false; // Nar IR-T36000014687
    // User has maintain rights then only 'Delete' and 'Edit Report Design' Button should be display
     if (SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_CUSTOM_REPORT)){	
      showSaveAsButton = true; // Nar IR-T36000014687
	  // 02/22/2014 R8.4 IR-T36000018491 - while doing save as, the other name is copied from the original report,
	  // So the user is not able to do edit/delete, Since the custom report is only visible to the user, below condition (if (userName.equals(repOwn))) is not required.
      //if (userName.equals(repOwn)){
		//Edit reports design method
		 editReportMode = TradePortalConstants.REPORT_EDIT_CUSTOM; 
		 editNavPage = formMgr.getLinkAsUrl("goToNewReportStep1", response); 
		 showEditButton = true;
		//Delete reports design method
		 reportTab = TradePortalConstants.CUSTOM_REPORTS;
		 deleteNavPage = formMgr.getLinkAsUrl("goToReportsDelete", "&reportTab=C&current2ndNav=C", response);
	    // deleteNavPage = formMgr.getLinkAsUrl("goToReportsDelete", response);
	     showDeleteButton = true;
       //}
     } 
%>
  
<%--cquinton 2/14/2013 do not display divider if already expanded--%>
<div id="divider1" class="subHeaderDivider"<%=("Y".equals(expandedPage))?" style=\"display:none;\"":""%>></div>
 
<%--cquinton 2/14/2013 add expandNode - everthing within this expands to the page size--%>
<div id="expandNode"<%=("Y".equals(expandedPage))?" class=\"expandedPage\"":""%> >
   
  <jsp:include page="/common/ReportsSubHeaderBar.jsp">
	<jsp:param name="returnLink" value="<%=returnLink%>" />
	<jsp:param name="pageTitleKey" value="<%=pageTitleKey%>" />
	<jsp:param name="showExpandCollapse" value="true"/>
	<jsp:param name="showEditButton" value="<%=showEditButton%>"/>
	<jsp:param name="showSaveAsButton" value="<%=showSaveAsButton%>"/>
	<jsp:param name="showDeleteButton" value="<%=showDeleteButton%>"/>	
	<jsp:param name="sToken" value="<%=StringFunction.xssCharsToHtml(sToken)%>"/>
	<jsp:param name="repName" value="<%=repName%>"/>
	<jsp:param name="reportDesc" value="<%=reportDesc%>"/>
	<jsp:param name="reportTab" value="<%=reportTab%>"/>
	<jsp:param name="editReportMode" value="<%=editReportMode%>"/>
	<jsp:param name="editNavPage" value="<%=editNavPage%>"/> 
	<jsp:param name="saveAsNavPage" value="<%=saveAsNavPage%>"/> 
	<jsp:param name="deleteNavPage" value="<%=deleteNavPage%>"/>  
	<jsp:param name="reportType"   value="<%=TradePortalConstants.CUSTOM_REPORTS%>" />
	<jsp:param name="current2ndNav"   value="<%=current2ndNav%>" />
	<jsp:param name="expandedPage"   value="<%=StringFunction.xssCharsToHtml(expandedPage)%>" />
  </jsp:include>

  <form id="customReportsPage" name="customReportsPage" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
    <input type=hidden value="" name=buttonName>			

    <%--cquinton 2/14/2013 what does error section do here?--%>
    <jsp:include page="/common/ErrorSection.jsp" />

    <%--cquinton 2/14/2013 move ReportsTypeSubHeaderBar.jsp to customReportHTML.frag--%>			
    <div id="customReportsPDFId">
      <%@ include file="fragments/customReportHTML.frag" %>
    </div>

    <%= formMgr.getFormInstanceAsInputField("saveReport") %>
			
  </form>

</div>


  </div>
</div>

<form name="saveReport" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
<input type=hidden value="" name=buttonName value="<%=TradePortalConstants.CUSTOM_REPORTS%>" />
<input type="hidden" name="ReportName" />
<input type="hidden" name="ReportDesc" />
<input type="hidden" name="StandardReport" id="StandardReport" />
<input type="hidden" name="StandardCategory" />
<input type="hidden" name="CustomCategory" />
<input type="hidden" name="storageToken" value="<%=StringFunction.xssCharsToHtml(sToken) %>" />
<input type="hidden" name="reportMode" value="<%=TradePortalConstants.REPORT_COPY_CUSTOM%>" />
<input type=hidden value='<%=current2ndNav%>' name="current2ndNav" id="current2ndNav1">
<input type="hidden" name="selectedCategory" /> 
<%= formMgr.getFormInstanceAsInputField("saveReport") %>
</form>

<jsp:include page="/common/Footer.jsp">
	<jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%--cquinton 2/14/2013 add expand/collapse functionality--%>
<script>
<%-- MEer Rel 9.2 Reverted XSS fix CID 11481 --%>
  var expandedPage = "<%=StringFunction.escapeQuotesforJS(expandedPage)%>";

  var local = {};

  require(["dojo/dom", "dojo/dom-style", "dojo/dom-class", "dojo/query", "dijit/registry"],
      function( dom, domStyle, domClass, query, registry ) {

    local.expandPage = function() {
      var expandNode = dom.byId("expandNode");
      if ( expandNode ) {
        <%-- hide everything else --%>
        query(".pageHeader").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".pageHeaderNavigation").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".secondaryNav").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query("#divider1").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".pageFooter").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query("#reportSection").forEach( function(entry, i) {
          domStyle.set(entry, "width", "auto");
          domStyle.set(entry, "height", "auto");
          domStyle.set(entry, "overflow", "visible");
        });
        domClass.add(expandNode, "expandedPage");

        var expandButton = registry.byId("ExpandButton");
        var collapseButton = registry.byId("CollapseButton");
        if (expandButton && collapseButton) {
          domStyle.set(expandButton.domNode, "display", "none");
          domStyle.set(collapseButton.domNode, "display", "inline-block");
        }

        expandedPage = "Y";
        local.setPageLinkExpandedPageParmVal( "firstPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "prefPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "nextPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "lastPageLink", "Y" );
      }
    }

    local.collapsePage = function() {
      var expandNode = dom.byId("expandNode");
      if ( expandNode ) {
        <%-- and remove the expand class --%>
        domClass.remove(expandNode, "expandedPage");
        query("#reportSection").forEach( function(entry, i) {
          domStyle.set(entry, "width", "100%");
          domStyle.set(entry, "height", "375px");
          domStyle.set(entry, "overflow", "auto");
        });
        <%-- show everything that was hidden above --%>
        query(".pageHeader").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".pageHeaderNavigation").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".secondaryNav").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query("#divider1").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".pageFooter").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });

        var expandButton = registry.byId("ExpandButton");
        var collapseButton = registry.byId("CollapseButton");
        if (expandButton && collapseButton) {
          domStyle.set(collapseButton.domNode, "display", "none");
          domStyle.set(expandButton.domNode, "display", "inline-block");
        }

        expandedPage = "N";
        local.setPageLinkExpandedPageParmVal( "firstPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "prefPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "nextPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "lastPageLink", "N" );
      }
    },

    <%-- this assumes expandedPage parm exists and is the last in the href url --%>
    local.setPageLinkExpandedPageParmVal = function( pageLinkId, parmVal ) {
      var pageLink = dom.byId(pageLinkId);
      if ( pageLink ) {
        var myHref = pageLink.href;
        var parmIdx = myHref.indexOf("expandedPage=");
        if ( parmIdx >= 0 ) {
          parmIdx = parmIdx + 13;
          myHref = myHref.substring(0, parmIdx) + parmVal;
        }
        pageLink.href = myHref;
      }
    }

  });

</script>

</body>
</html>



<%
//Garbage collection
//WebIntelligence variables
session.setAttribute("test",null);



widDoc 		= null;
widPrompts 	= null;
widReports 	= null;
widReport 	= null;
pgMode 		= null;
repView 	= null;
totalNoPages 	= 0;
pgn 		= null;
//getOwnershipLevel = userSession.getOwnershipLevel();

%>

<%
Debug.debug("***END*********************CUSTOM*REPORTS*PAGE***************************END***"); 
%>
