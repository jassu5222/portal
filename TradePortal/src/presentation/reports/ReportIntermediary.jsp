<%--
*******************************************************************************
  Report Intermediary

  Description:  When a report is selected from mylinks, we first need to ensure
                reporting is setup before going to the report detail page.

  Variables:

*******************************************************************************
--%>
<%--
 *	   Dev By Sandeep
 *     Copyright  � 2012                         
 *     CGI, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,java.util.*,java.beans.Beans,com.businessobjects.rebean.wi.*,
		 com.crystaldecisions.sdk.framework.*,
                 com.crystaldecisions.sdk.exception.*,com.ams.tradeportal.busobj.util.SecurityAccess, 
                 java.text.*, com.ams.tradeportal.busobj.util.*, com.crystaldecisions.sdk.occa.infostore.IInfoStore, net.sf.json.*, net.sf.json.xml.*
                 " %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>
	
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>
	
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>
	
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%
  Debug.debug(" *** Entering ReportIntermediary");

  //todo: this page just logs in to reporting, should check to see if its available, then login

  // THIS SECTION DETERMINES IF REPORTING IS AVAILABLE IN THIS ENVIRONMENT
  // IF IT IS NOT, THEN JUST DISPLAY A MESSAGE AND NOTHING ELSE
  boolean reportingAvailable = false;

  PropertyResourceBundle portalProperties = null; 
	
  IEnterpriseSession boSessionLocal = (IEnterpriseSession) request.getSession().getAttribute("CE_ENTERPRISESESSION");
	   
  Debug.debug(" In ReportIntermediary ");

  try
  {
    portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
    String reportsModuleAvailable = portalProperties.getString("reportsModuleAvailable");
	
    if(reportsModuleAvailable.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
      reportingAvailable = true;
  }
  catch(MissingResourceException mre)
  {
    // This will happen if the reportsModuleAvailable setting is not in the config file
    reportingAvailable = false;
  }

  if(!reportingAvailable) {
    out.println("<br><br><font color='red'>&nbsp;&nbsp;&nbsp;The reporting module is not available in this environment</font>");
    return;
  }
  Debug.debug(" Reporting is available ");

  String userName = null;
  String userPass = null;
  String physicalPage = null;
  MediatorServices mediatorServices = null; 
  DocumentHandler xmlDoc = null; 
  boolean reportLoginInd = false;
  IEnterpriseSession boSession = null; 
  String cmsName = null;
  String cmsAuth = null;	

  try { 
    //added by kajal bhatt on 25/06/07
    //starts here
    try {
      boSession = (IEnterpriseSession) request.getSession().getAttribute("CE_ENTERPRISESESSION");
      //out.println("boSession="+request.getSession().getAttribute("CE_ENTERPRISESESSION"));
    }
    catch (Exception e) {
      Debug.debug("*** <<Exception at the beginnning - printing stacktrace>>: ");
      e.printStackTrace();
    }
    //ends here

    // Determine the suffix to place in front of the organization OID
    String reportingUserSuffix = userSession.getReportingUserSuffix();
 
    if(reportingUserSuffix == null) {
      //rbhaduri - 30th Nov 09 - IR KAUJ112336153 Modify Begin - M And T Bank Change - get reporting 
      //suffix based on the level of the user logged in. For ASP level user get the
      //get the suffix from global organization table. But for all other levels, get
      //the suffix from the client bank table
	   
      String userOwnerShipLevel = userSession.getOwnershipLevel();
      if (userOwnerShipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) { 
        // Get the reporting user suffix from the Global Org to which this org belongs

	     //jgadela R90 IR T36000026319 - SQL INJECTION FIX
        String reportingUserSuffixSql = "select reporting_user_suffix" 
		                     + " from global_organization"
		                     + " where organization_oid = ? ";
	
        DocumentHandler result = DatabaseQueryBean.getXmlResultSet(reportingUserSuffixSql, false, new Object[]{userSession.getGlobalOrgOid()});
		
        reportingUserSuffix = result.getAttribute("/ResultSetRecord(0)/REPORTING_USER_SUFFIX");    
		  
        // Put it on the session so that later accesses of this page don't have to look it up  
        userSession.setReportingUserSuffix(reportingUserSuffix);
      }
      else {	    	  
        String clientBankOID = null;
        clientBankOID = userSession.getClientBankOid();
	    	  
		  		// Get the reporting user suffix from the Client Bank of the logged in user
		  		String reportingUserSuffixSql = "select reporting_user_suffix" 
		                     + " from client_bank"
		                     + " where organization_oid = ? ";
	
		  		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(reportingUserSuffixSql, false, new Object[]{clientBankOID});
		
		  		reportingUserSuffix = result.getAttribute("/ResultSetRecord(0)/REPORTING_USER_SUFFIX");
		  
		  		// Put it on the session so that later accesses of this page don't have to look it up
	      		userSession.setReportingUserSuffix(reportingUserSuffix);

	    	}  //rbhaduri - 30th Nov 09 - IR KAUJ112336153 Modify End - M And T Bank Change 
	 }
	
	//rbhaduri - 30th Nov 09 - IR KAUJ112336153 - added system out
	Debug.debug("*** <<Reporting Suffix>>:" + reportingUserSuffix );
	// Place the reporting user suffix on to the beginning of the OID
	// The suffix is used to distinguish between organizations being processed
	// by different ASPs that might be on the same reports server.  
	userName = userSession.getOwnerOrgOid()+reportingUserSuffix;
	Debug.debug("*** <<User Name>>: " + userName );

	// get password from the property file
	userPass = userSession.getOwnerOrgOid() + reportingUserSuffix.toLowerCase(); 
	//initialize config file
    //ConfigFileReader.init(application.getRealPath("WEB-INF"));
	Debug.debug("*** <<User Name in Reports Home is>>: "+userName);
    try //try-2
    {
      Debug.debug("*** Inside webiServer is running loop in ReportsHome"); 
      Debug.debug("\n*** webiServer is running - checking from ReportsHome");

      //Retrieve report login indicator
      reportLoginInd = userSession.getReportLoginInd(); 
	
      //modified by Kajal Bhatt on 25/06/07
      //starts here
      if((boSession == null) || (reportLoginInd == false)) //if-3
      {
        // Attempt logon. Create an Enterprise session manager object.
        ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
			
        // Log on to BusinessObjects Enterprise
        cmsName = portalProperties.getString("CMSName");
        cmsAuth = portalProperties.getString("Authentication");	

        Debug.debug("*** <<CMS Name>>: "+ cmsName);
        Debug.debug("*** <<CMS Auth>>: "+ cmsAuth );

        Debug.debug(" user Credentials " + userName + userPass);
        boSession = sessionMgr.logon(userName, userPass, cmsName, cmsAuth);

        Debug.debug("*** <<boSession>>: " + boSession );
			
        //put the enterprisesession in to session
        request.getSession().setAttribute("CE_ENTERPRISESESSION", boSession);
			
        //get the reportengines object and store it into session
        ReportEngines reportEngines = null;
        reportEngines = (ReportEngines)boSession.getService("ReportEngines");
        request.getSession().setAttribute("ReportEngines", reportEngines);
        //ends here
			
        //get the default storage token and store it into session
        String strToken = boSession.getLogonTokenMgr().getDefaultToken();
        userSession.setReportLoginInd(true); // Nar IR-T36000031433 Rel 9.2.0.0
        Debug.debug("*** Successfully created webiSession");       
      }//end of if-3
    } //end of try-2
    catch (REException rex) //catch-2
    {
      rex.printStackTrace();
      //Catch all exceptions
      Debug.debug("*** REException in Reports Home>>: "+rex.getErrorCode()+" "+rex.getMessage());
		
      xmlDoc = formMgr.getFromDocCache();
      mediatorServices = new MediatorServices();
      mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
      mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
      mediatorServices.addErrorInfo();
      xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
      xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
      formMgr.storeInDocCache("default.doc", xmlDoc);
      physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
%>
  <jsp:forward page='<%= physicalPage %>'/>
<%
    }//end of catch-2
    catch(SDKException sdk) {
      sdk.printStackTrace();
      StringTokenizer stk = new StringTokenizer(sdk.toString(),"OCA_Abuse");
      Debug.debug("*** <<SDKException in Reports Home>>: " + sdk.getMessage());
		
      //BO User is not setup in CMS
      if( stk.hasMoreTokens()) {
        Debug.debug("*** <<BO user is not serup in CMS>>: " + userName);
   
        xmlDoc = formMgr.getFromDocCache();
        mediatorServices = new MediatorServices();
        mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORT_USER_NOT_SETUP);
        mediatorServices.addErrorInfo();
        xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
        xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
        formMgr.storeInDocCache("default.doc", xmlDoc);
        physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
%>
  <jsp:forward page='<%= physicalPage %>'/>
<% 
      }
      else {
        //Catch all exceptions
        xmlDoc = formMgr.getFromDocCache();
        mediatorServices = new MediatorServices();
        mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
        mediatorServices.addErrorInfo();
        xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
        xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
        formMgr.storeInDocCache("default.doc", xmlDoc);
        physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
%>
  <jsp:forward page='<%= physicalPage %>'/>
<%
      }
    }
  }
  catch (Exception e) { 
    // In this block, catch exceptions from anywhere during the process of attempting to connect
    // to the reports server

    Debug.debug("*** Exception occured while in ReportIntermediary.jsp while attempting to connect to reports server: "); 
    e.printStackTrace();
    xmlDoc = formMgr.getFromDocCache();
    mediatorServices = new MediatorServices();
    mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());        
    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 	TradePortalConstants.WEBI_SERVER_ERROR);
    mediatorServices.addErrorInfo();
    xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
    xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
    formMgr.storeInDocCache("default.doc", xmlDoc);
    physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request); 
%>
  <jsp:forward page='<%= physicalPage %>' />
<%
  } 

  //if we got to here we logged in

  //get the intermediary page report type - it is used to determine which page to go to, standard or custom
  String encIntReportType = request.getParameter("IntReportType");
  String intReportType = TradePortalConstants.CUSTOM_REPORTS; //default
  try {
    if(encIntReportType!=null) {
      intReportType = EncryptDecrypt.decryptStringUsingTripleDes(encIntReportType, userSession.getSecretKey());
    }
  }
  catch(Exception ex) {
    //don't really care if we have an error, will default to custom below
  }
  if ( TradePortalConstants.STANDARD_REPORTS.equals(intReportType) ) {
    physicalPage = NavigationManager.getNavMan().getPhysicalPage("StandardReportsDetail", request);
  }
  else {
    physicalPage = NavigationManager.getNavMan().getPhysicalPage("CustomReportsDetail", request);
  }
%>

<jsp:forward page='<%= physicalPage %>' />
