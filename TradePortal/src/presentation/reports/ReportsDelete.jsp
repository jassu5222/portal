<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,
				 com.crystaldecisions.sdk.occa.infostore.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%--
commented by kajal bhatt
we require EnterpriseSession instead of WISession
<%WISession webiSession = null; %>
--%>

<%
Debug.debug("***START*********************REPORTS*DELETE***************************START***"); 

//commented by kajal bhatt
//WIDocument          webiDocument                = null;
//ends here
String              storageToken                = null;
String              reportTab                   = null;
StringBuffer        link                        = null;
String              reportName                  = null;
String				strDocId                    = null;
  
storageToken = request.getParameter("storageToken");
reportTab = request.getParameter("reportTab");


%>
<%@ include file="fragments/wistartpage.frag" %>
<%

//KM E6.5
DocumentInstance widDoc = null;
String documentToken = null;
IInfoStore iStore = null;
IInfoObjects webiReports = null;
IInfoObject webiReport = null;

widDoc = reportEngine.getDocumentFromStorageToken(storageToken);
documentToken = widDoc.getStorageToken();

//Retrieve report from repository using storage token
if (storageToken != null)
{
    //get the name of the document
    reportName = widDoc.getProperties().getProperty(PropertiesType.NAME);

	//get the document id to delete the document
	strDocId = widDoc.getProperties().getProperty(PropertiesType.DOCUMENT_ID);
    
	//prepare the query to delete the document
	String strQuery = "SELECT * FROM CI_INFOOBJECTS Where SI_ID = '"+strDocId+"'";

	//get the infostore object from the usersession
	//iStore = userSession.getInfoStoreObject();
	 iStore = (IInfoStore) boSession.getService("InfoStore");

	//get the collection of InfoObjects
	webiReports = iStore.query(strQuery);

	if(webiReports.size() > 0)
	{
		//get the infoobject to delete
		webiReport = (IInfoObject) webiReports.get(0);
		
		//delete the infoobject
		webiReports.delete(webiReport);
	}
   iStore.commit(webiReports);
}

link = new StringBuffer();
DocumentHandler     xmlDoc                      = null;
MediatorServices    mediatorServices            = null;

if (reportTab.equals(TradePortalConstants.STANDARD_REPORTS))
{
    xmlDoc = formMgr.getFromDocCache();
    mediatorServices = new MediatorServices();
    mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.DELETE_SUCCESSFUL, reportName);
    mediatorServices.addErrorInfo();
    xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
    xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
    formMgr.storeInDocCache("default.doc", xmlDoc);
    
	//Navigation logic to source page
    link.append(NavigationManager.getNavMan().getPhysicalPage("ReportsHome", request));
    link.append("?reportTab=");
    link.append(TradePortalConstants.STANDARD_REPORTS);
}
else
{
    xmlDoc = formMgr.getFromDocCache();
    mediatorServices = new MediatorServices();
    mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DELETE_SUCCESSFUL, reportName);
    mediatorServices.addErrorInfo();
    xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
    xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
    formMgr.storeInDocCache("default.doc", xmlDoc);
    
    //Navigation logic to source page
    link.append(NavigationManager.getNavMan().getPhysicalPage("ReportsHome", request));
    link.append("?reportTab=");
    link.append(TradePortalConstants.CUSTOM_REPORTS);
}

%>

<jsp:forward page='<%=link.toString()%>' />

<%
Debug.debug("***END*********************REPORTS*DELETE***************************END***"); 
%>