<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.frame.*,com.businessobjects.rebean.wi.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%
//commented by kajal bhatt
//WISession webiSession = null;
//ends here
MediatorServices    mediatorServices        = null;
DocumentHandler     xmlDoc                  = null;
String              physicalPage            = null;
StringBuffer        link = new StringBuffer();
String              storageToken                = null;     
String              cell                        = null;
int                 pageCount                   = 0;
boolean             promptDoc                   = false;
DocumentInstance    webiDocument                = null;
//commented by kajal bhatt
//WIDocument          webiDocument                = null;
//WIOutput            webiOutput                  = null;
//ends here
HTMLView            webiOutput                  = null;
storageToken = request.getParameter("storageToken");
%>

<%@ include file="fragments/wistartpage.frag" %>

<%

//Retrieve report from repository using storage token

if (reportEngine != null)
{
    webiDocument = reportEngine.getDocumentFromStorageToken(storageToken);
}

try
{
    //Following method retrieved report in HTML format
        if (webiDocument.getReports().getCount() == 1)

    {
                Reports reports = webiDocument.getReports();
                Report report = reports.getItem(0);
                webiOutput = (HTMLView) report.getView(OutputFormatType.HTML);
        cell = webiOutput.getStringPart("body",true);
        out.println(cell);
    }
    else
    {
       Reports reports = webiDocument.getReports();
        for ( int i = 0;i <= reports.getCount() ; i++)
        {
                        Report report = reports.getItem(i);
                        webiOutput = (HTMLView) report.getView(OutputFormatType.HTML);        
                        cell = webiOutput.getStringPart("body",true);
            out.println(cell);
        }
    }
}catch (REException wie)
{
	//commented by kajal, there is no such subclass in REException
    /*if ((wie.getNumber() == WIException.WIERR_DOC_NEEDPROMPT_01))
    {*/
        link = new StringBuffer();
                
        //Navigation logic to prompt page
        link.append(formMgr.getLinkAsUrl("PromptPage1", response));
        link.append("&storageToken=");
        link.append(storageToken);
        %>
         <jsp:forward page='<%=link.toString()%>' />
        <%
        
	//}
}
%>
