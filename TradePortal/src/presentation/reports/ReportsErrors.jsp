<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>

<%--
commented by kajal bhatt
we require EnterpriseSession instead of WISession
<%WISession webiSession = null; %>
--%>
<%@ include file="fragments/wistartpage.frag"%>

<%--*******************************HTML for page begins here **************--%>


<%-- Begin HTML header decleration --%>

<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeErrorSectionFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%-- End HTML header decleration --%>

<%
formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
<% 

// 01/24 - T36000009952 TP refresh issue
String error = resMgr.getText("E198", "ErrorMessages");
%>
<div class="pageMainNoSidebar">
<div class="pageContent">

	<div class="secondaryNav"><%-- Komal ANZ Issue No.359 BEGINS --%>
		<div class="secondaryNavTitleCamel"><%=resMgr.getText("SecondaryNavigation.Reports",TradePortalConstants.TEXT_BUNDLE).toUpperCase()%></div></div>
		<br>
		<br>
		<br>
		<div class="formContentNoSidebar">
			<div class="errorsAndWarnings">
				<div class="errorText"><%= error%></div>
			</div>
	</div>
</div>
</div>
<%--cquinton 3/16/2012 Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
	<jsp:param name="displayFooter"
		value="<%=TradePortalConstants.INDICATOR_NO%>" />
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton 3/16/2012 Portal Refresh - end--%>