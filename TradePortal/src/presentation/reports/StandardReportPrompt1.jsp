<%--
 *
 *     Copyright  ? 2012                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,java.util.*, com.ams.tradeportal.common.cache.*,com.ams.tradeportal.busobj.util.SecurityAccess"%>
                


<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>
<%@ include file="fragments/reportVariables.frag"%>

<%
Debug.debug("***START********************STANDARD*REPORTS*PROMPT*PAGE1**************************START***");
 
// first try to retrieve the session by using the WebIntelligence Cookie
// if not switch back to default page
 //T36000023438
WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
StringBuffer      dropdownOptions              = new StringBuffer();
String options = ""; 

String viewURLLink = "";
String                        buttonPressed                 = "";
String                        divID                         = ""; 
String                        reportdivID                   = "";
String              redirectURI             = null;
String              storageToken            = null;
Prompts                       webiPromptList                = null;
String []           listOfValues            = null;
MediatorServices    mediatorServices        = null;
DocumentHandler     xmlDoc                  = null;
String                    physicalPage                = null;
String previousValue = null;
String                        repOwn                              = null;
DocumentInstance widDoc = null;
Prompts widPromptsList = null;
Prompt webiPrompt = null;
Reports widReports = null;
PaginationMode pgMode = null;
int iPrompts = 0;
int                     totalNoPages                  = 0;
PageNavigation          pgn                           = null;
String                        pageToken                     = null;
String                        formString                    = null;
String []               LOV                           = null;
boolean                 end;
int                     iPromptCount                  = 0;
HTMLView                repView                       = null;
Report                        widReport                     = null;
String                        pageMode                      = null;
int                     prevPage                      = 0;
int                     nextPage                      = 0;
String                        pageCall                      = "";
String                        pageNum                       = null;
int                     goToPage                      = 0;
String                        locale                              = null;
String                        userName                      = "";
PromptType        widPromptType           = null;
String isRequiredMsg = null;

//cquinton 2/14/2013 add a variable to see if on entry page is expanded or not
String expandedPage=request.getParameter("expandedPage");
if ( expandedPage == null ) {
  expandedPage = "N";
}

PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");

storageToken = request.getParameter("storageToken");
if(request.getParameter("pageToken")!=null)
{
      pageToken = request.getParameter("pageToken");
}

if(request.getParameter("repOwn")!=null)
{
      repOwn = request.getParameter("repOwn");
}

pageMode = portalProperties.getString("pageMode");
if( pageMode == null) {
	  pageMode = "n";
}
String securityRights = userSession.getSecurityRights();
userName = userSession.getOwnerOrgOid()+userSession.getReportingUserSuffix();


if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
{
	if (request.getParameter("pageCall")!=null)
	{
		pageCall = request.getParameter("pageCall");
	}
	else
	{
		pageCall ="N";
	}

	if (request.getParameter("goToPage")!=null) 
	{
   		pageNum = request.getParameter("goToPage");
	   	goToPage = (new Integer(pageNum).intValue());
	}
	else 
	{
   		goToPage = 1;
	}
}
else
{
	pageCall ="N";
}


//Locale to set the date prompt
locale = portalProperties.getString("locale");

repName=(String)session.getAttribute("repName"); 
%>


<%@ include file="fragments/wistartpage.frag"%>
<%

try
{
    
	if (pageCall.equals("Y"))
	{
		//Request coming from the same page
		widDoc = reportEngine.getDocumentFromStorageToken(pageToken);
	}
	else
	{
		//Request coming for the first time
		widDoc =  reportEngine.getDocumentFromStorageToken(storageToken);
	}

     //Refresh the report     
     widDoc.refresh();

       //get the report
     webiPromptList = widDoc.getPrompts();
}
catch (REException rex)
{
     //Catch all exception
     System.out.println("EXCEPTION in Standard Reports Prompt Page1 ######################"+rex.getCode()+" "+rex.getMessage());
                        
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);       
     %>
<jsp:forward page='<%= physicalPage %>' />
<%
}

//T36000016123 - R 8.1.0.6 ER   jgadela  -   handling validation exceptions  [START]
Vector            errorList          = null;
int               severityLevel      = 0;
int               errorListSize      = 0;
boolean fromException = false;
xmlDoc             = formMgr.getFromDocCache();
errorList     = xmlDoc.getFragments("/Error/errorlist/error");
if(errorList != null){
	errorListSize = errorList.size();
}
if(errorListSize>0){
	fromException = true;
}
xmlDoc = null;

//T36000016123 - R 8.1.0.6 ER   jgadela  -   handling validation exceptions [END]

buttonPressed = request.getParameter("buttonPressed");
//T36000016123 - R 8.1.0.6 ER   jgadela - added condition fromException
if(("SpecifyReportButton").equals(buttonPressed) && !pageCall.equals("Y")  && fromException != true) 
{
	try
	{
		//Prompts  webiPromptList = widDoc.getPrompts();
		webiPromptList = widDoc.getPrompts();
		iPromptCount = webiPromptList.getCount();
		for (int i=0;i<iPromptCount;i++)
		{
			webiPrompt = webiPromptList.getItem(i);
			// the Request.Form contains the values selected in previous page
			// it can contained multiple values for a single variable in a string separated by commas
			formString = "ListOfValues"+i;
			widPromptType = webiPrompt.getType();
			if (webiPrompt.getType() == PromptType.Mono)
			{
				//case for an input text
				String  repPrompt = request.getParameter(formString);
				// DK IR T36000017385 Rel8.2 06/18/2013 starts
				if(repPrompt!=null){
						session.setAttribute("formString"+i,repPrompt);
				}
				// DK IR T36000017385 Rel8.2 06/18/2013 ends
				StringTokenizer strTokenizer = new StringTokenizer(repPrompt, ";");
				int iValueCount = strTokenizer.countTokens();
				String[] astrValues = new String[iValueCount];
				PromptType pt = webiPrompt.getType();
				int iValueIndex = 0;
				for (iValueIndex = 0; iValueIndex < iValueCount; iValueIndex++)
				{
					astrValues[iValueIndex] = strTokenizer.nextToken();
					//Date parsing logic				
					if (webiPrompt.getObjectType() == ObjectType.DATE)
					{
						//jgadela 01/16/2014 Rel 8.4 IR T36000023438 -[START] Modified to take date format as per selected user date format
						String  retDt = astrValues[iValueIndex];
						int intFirstVal = retDt.indexOf("-");
						String dtFirsrVal = retDt.substring(0,intFirstVal);
						int intSecVal = retDt.lastIndexOf("-");
						String dtSecVal = retDt.substring(intFirstVal+1,intSecVal);
						int dtlen = retDt.length();
						String dtThirdVal = retDt.substring(intSecVal+1,dtlen);
						
						if(locale.equals("en_GB"))
						{ 
							if((intSecVal == 7 ) && (intFirstVal == 4) )
							{
								astrValues[iValueIndex] =  dtSecVal + "/"+  dtThirdVal + "/" + dtFirsrVal;
							}	
						}else{
							if((intSecVal == 7 ) && (intFirstVal == 4) )
							{
								astrValues[iValueIndex] =   dtThirdVal + "/"+   dtSecVal+ "/" + dtFirsrVal;
							}	
						}
						//jgadela 01/16/2014 Rel 8.4 IR T36000023438 -[END]
					}
					webiPrompt.enterValues(astrValues);
				}
			}
			else
			{
				LOV = request.getParameterValues(formString);
				webiPrompt.enterValues(LOV);
			}
		}

		widDoc.setPrompts();
		widDoc.applyFormat();

		storageToken = widDoc.getStorageToken();
		pageToken = widDoc.getStorageToken();
		
	}
	catch (REException rex) // have changed the REException class
	{
		 //Catch all exception
		 System.out.println("EXCEPTION in Standard Reports Prompt Page1 ######################"+rex.getErrorCode()+" "+rex.getMessage());
		 xmlDoc = formMgr.getFromDocCache();
		 mediatorServices = new MediatorServices();
		 mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_INVALID_DATE_ERROR);
		 mediatorServices.addErrorInfo();
		 xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
		 xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
		 formMgr.storeInDocCache("default.doc", xmlDoc);
		 physicalPage = NavigationManager.getNavMan().getPhysicalPage("StandardReportPrompt1", request);
		 %>
		 <jsp:forward page='<%= physicalPage %>' />
		 <%
	}
	catch (IndexOutOfBoundsException ex) // Check for null dates
	{
		 //Catch all exception
		 System.out.println("EXCEPTION in Standard Reports Prompt Page3 ######################");
		 xmlDoc = formMgr.getFromDocCache();
		 mediatorServices = new MediatorServices();
		 mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_INVALID_DATE_ERROR);
		 mediatorServices.addErrorInfo();
		 xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
		 xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
		 formMgr.storeInDocCache("default.doc", xmlDoc);
		 physicalPage = NavigationManager.getNavMan().getPhysicalPage("StandardReportPrompt1", request);
		 %>
		 <jsp:forward page='<%= physicalPage %>' />
		 <%
	}	
}
//DK IR T36000017385 Rel8.2 06/18/2013 starts
else if(goToPage==1 && pageCall.equals("Y")){
	webiPromptList = widDoc.getPrompts();
	iPromptCount = webiPromptList.getCount();
	for (int i=0;i<iPromptCount;i++)
	{
		webiPrompt = webiPromptList.getItem(i);
		widPromptType = webiPrompt.getType();
		if (webiPrompt.getType() == PromptType.Mono)
		{	
			String repPrompt = 	(String)session.getAttribute("formString"+i);
			StringTokenizer strTokenizer = new StringTokenizer(repPrompt, ";");
			int iValueCount = strTokenizer.countTokens();
			String[] astrValues = new String[iValueCount];
			PromptType pt = webiPrompt.getType();
			
			int iValueIndex = 0;
			for (iValueIndex = 0; iValueIndex < iValueCount; iValueIndex++)
			{
				astrValues[iValueIndex] = strTokenizer.nextToken();
				//Date parsing logic				
				if (webiPrompt.getObjectType() == ObjectType.DATE)
				{
					//jgadela 01/16/2014 Rel 8.4 IR T36000023438 -[START] Modified to take date format as per selected user date format
					String  retDt = astrValues[iValueIndex];
					int intFirstVal = retDt.indexOf("-");
					String dtFirsrVal = retDt.substring(0,intFirstVal);
					int intSecVal = retDt.lastIndexOf("-");
					String dtSecVal = retDt.substring(intFirstVal+1,intSecVal);
					int dtlen = retDt.length();
					String dtThirdVal = retDt.substring(intSecVal+1,dtlen);
					
					if(locale.equals("en_GB"))
					{ 
						if((intSecVal == 7 ) && (intFirstVal == 4) )
						{
							astrValues[iValueIndex] =  dtSecVal + "/"+  dtThirdVal + "/" + dtFirsrVal;
						}	
					}else{
						if((intSecVal == 7 ) && (intFirstVal == 4) )
						{
							astrValues[iValueIndex] =   dtThirdVal + "/"+   dtSecVal+ "/" + dtFirsrVal;
						}	
					}
					//jgadela 01/16/2014 Rel 8.4 IR T36000023438 -[END]
				}
				webiPrompt.enterValues(astrValues);
			}
		}
	}	
	widDoc.setPrompts();
	widDoc.applyFormat();
}
//DK IR T36000017385 Rel8.2 06/18/2013 ends

//Retrieve the reports
widReports = widDoc.getReports();
widReport = widReports.getItem(0);
        
//Set the page navigation mode to page mode
widReport.setPaginationMode(PaginationMode.QuickDisplay);
pgn = widReport.getPageNavigation();
         
//Set the report navigation to the last page for computing total number of pages
pgn.last();
totalNoPages = pgn.getCurrent();
pgn.setTo(goToPage);


// jgadela R 8.4 CR-854 T36000024797   -[START] Reporting page labels. 
String reportingPageLabelLan = null;
String cbReportingLangOptionInd = null;

Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
if (!StringFunction.isBlank(userSession.getClientBankOid())){
	   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 
}

if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)){
	reportingPageLabelLan = userSession.getReportingLanguage();
}else{
	reportingPageLabelLan = userSession.getUserLocale();
}
//jgadela R 8.4 CR-854 T36000024797   -[END]

%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag"
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag"
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="expandedPage" value="<%=expandedPage%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">
<%  
  String pageTitleKey;
  String current2ndNav ="S";
  String helpLink = OnlineHelp.createContextSensitiveLink("customer/specify_report_criteria.htm",resMgr,userSession); 
%>
    <%@ include file="/reports/fragments/Reports-SecondaryNavigation.frag"%>
    <input type=hidden value='<%=current2ndNav%>' name="current2ndNav" id="current2ndNav">
<%          
  pageTitleKey = repName;
  // figures out how to return to the calling page
  StringBuffer parms = new StringBuffer();
  parms.append("&returnAction=");
  String selectedCategory = request.getParameter("selectedCategory");
  String selectedCategoryUrlParam = "";
  if(StringFunction.isNotBlank(selectedCategory)){
  selectedCategory = EncryptDecrypt.base64StringToString(selectedCategory);
  selectedCategoryUrlParam = EncryptDecrypt.stringToBase64String(selectedCategory);
  }
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToReportsHome", userSession.getSecretKey()));
  String returnLink = formMgr.getLinkAsUrl("goToReportsHome", "&reportTab=S&current2ndNav=S&selectedCategory="+selectedCategoryUrlParam, response);
  showSaveAsButton = false; // Nar IR-T36000014687  
  //T36000016123 - R 8.1.0.6 ER   jgadela - added condition fromException
  if(("SpecifyReportButton").equals(buttonPressed) && fromException != true) {
    
	    // DK IR T36000026070 Rel8.4 - display Save As button if user has view rights
	    if (SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_OR_MAINTAIN_STANDARD_REPORT)) {
	  	  showSaveAsButton = true;
	    }
    // User has maintain rights then only 'Delete' and 'Edit Report Design' Button should be display
    if (SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_STANDARD_REPORT)){  
       // Nar IR-T36000014687 show Save as Standard Reports button if user has maintain rights
       saveAsNavPage = formMgr.getLinkAsUrl("goToEditReportStep3", response);
      if (userName.equals(repOwn)) {    
        //Edit reports design method
        editReportMode = TradePortalConstants.REPORT_EDIT_STANDARD; 
        editNavPage = formMgr.getLinkAsUrl("goToNewReportStep1", response); 
        showEditButton = true;
                 
        //delete button
        reportTab = TradePortalConstants.STANDARD_REPORTS;
        deleteNavPage = formMgr.getLinkAsUrl("goToReportsDelete", response);
        showDeleteButton = true;
      }
    }
  }else{
    showSaveAsButton = false;
    showEditButton = false;
    showDeleteButton = false;
  }
     
%>
  
    <%--cquinton 2/14/2013 do not display divider if already expanded--%>
    <div id="divider1" class="subHeaderDivider"<%=("Y".equals(expandedPage))?" style=\"display:none;\"":""%>></div>
 
    <%--cquinton 2/14/2013 add expandNode - everthing within this expands to the page size--%>
    <div id="expandNode"<%=("Y".equals(expandedPage))?" class=\"expandedPage\"":""%> >
   
      <jsp:include page="/common/ReportsSubHeaderBar.jsp">
        <jsp:param name="returnLink" value="<%=returnLink%>" />
        <jsp:param name="pageTitleKey" value="<%=pageTitleKey%>" />
        <jsp:param name="showExpandCollapse" value="true" />
        <jsp:param name="showEditButton" value="<%=showEditButton%>" />
        <jsp:param name="showSaveAsButton" value="<%=showSaveAsButton%>" />
        <jsp:param name="showDeleteButton" value="<%=showDeleteButton%>" />
        <jsp:param name="sToken" value="<%=storageToken%>" />
        <jsp:param name="repName" value="<%=repName%>" />
        <jsp:param name="reportTab" value="<%=reportTab%>" />
        <jsp:param name="editReportMode" value="<%=editReportMode%>" />
        <jsp:param name="editNavPage" value="<%=editNavPage%>" />
        <jsp:param name="saveAsNavPage" value="<%=saveAsNavPage%>" />
        <jsp:param name="deleteNavPage" value="<%=deleteNavPage%>" />
        <jsp:param name="reportType"
                   value="<%=TradePortalConstants.STANDARD_REPORTS%>" />
        <jsp:param name="expandedPage"   value="<%=expandedPage%>" />
      </jsp:include>

<%


      reportdivID = "standardReportPromptPDFId";
      divID = "SpecifyReportCriteriaStandard";
%>

      <form id="standardReportPrompt" name="standardReportPrompt" method="post" data-dojo-type="dijit.form.Form"
            action="<%=formMgr.getSubmitAction(response)%>">
        <input type=hidden value="<%=StringFunction.xssCharsToHtml(buttonPressed)%>" name="buttonPressed" id="buttonPressed" /> 
        <input type=hidden value="<%=StringFunction.xssCharsToHtml(storageToken)%>" name=storageToken> 
        <input type=hidden value="<%=StringFunction.xssCharsToHtml(repOwn)%>" name=repOwn>
        <input type=hidden value="false" name="showSaveAsButton" id="showSaveAsButton">
<% if("Y".equals(pageCall)) {%>
        <input type=hidden value="Y" name="pageCall" id="pageCall" /> 
<%}else{ %>
        <input type=hidden value="N" name="pageCall" id="pageCall" /> 
<%}%>

<% 
//T36000016123 - R 8.1.0.6 ER   jgadela -hidden variables to validate expand/collapse of criteria toggle pane  
if(fromException == true) {%>
        <input type=hidden value="Y" name="fromExceptionH" id="fromExceptionH" /> 
<%}else{ %>
        <input type=hidden value="N" name="fromExceptionH" id="fromExceptionH" /> 
<%}%>



        <jsp:include page="/common/ErrorSection.jsp" />
        <div class="errorsAndWarnings" id="errorsAndWarnings1"
             style="display: none; width:900px;">
          <div class="errorText" id="errorText1"></div>
        </div>

        <div id="SpecifyReportCriteriaStandard">
       <%  //jgadela 12-19-2013 Rel8.4 CR-854 [BEGIN]- Modified to get the label using locale from page.
		  String specifyReportCr = resMgr.getText("SpecifyReport.SpecifyReportCriteria",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan); %>
          <%=widgetFactory.createSectionHeader("",specifyReportCr)%>
          <%@ include file="fragments/SpecifyReportCriteriaStandard.frag"%>
          </div>
        </div>
        <div id="standardReportPromptPDFId">
<% 
//T36000016123 - R 8.1.0.6 ER   jgadela - added condition fromException
if((("SpecifyReportButton").equals(buttonPressed) || "Y".equals(pageCall)) && fromException != true) {
%>
          <%@ include file="fragments/StandardReportPromptHTML.frag"%>
<% 
    } 
%>

        </div>
        <div style="clear: both;"></div>
					
        <%= formMgr.getFormInstanceAsInputField("standardReportPrompt") %>
        <input type=hidden value="<%=StringFunction.xssCharsToHtml(storageToken)%>" name=storageToken>
			
      </form>

    </div> <%--end expandNode--%>

  </div>
</div>


<form name="saveReport" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
	<input type=hidden value="" name=buttonName value="<%=TradePortalConstants.STANDARD_REPORTS%>" /> 
	<input type="hidden" name="ReportName" /> <input type="hidden" name="ReportDesc" /> 
	<input type="hidden" name="StandardReport" id="StandardReport" /> 
	<input type="hidden" name="StandardCategory" />
	<input type="hidden" name="CustomCategory" /> 
	<input type="hidden" name="storageToken" value="<%=StringFunction.xssCharsToHtml(storageToken)%>" />
	<input type="hidden" name="reportMode" value="<%=TradePortalConstants.REPORT_COPY_STANDARD%>" /> 
	<input type=hidden value='<%=current2ndNav%>' name="current2ndNav" id="current2ndNav1">
	<input type="hidden" name="selectedCategory" /> 
	<%= formMgr.getFormInstanceAsInputField("saveReport") %>
</form>

<%--cquinton 3/16/2012 Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter"
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag"
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton 3/16/2012 Portal Refresh - end--%>


<script type="text/javascript">

  var expandedPage = "<%=StringFunction.escapeQuotesforJS(expandedPage)%>";

  var local = {};

  function viewReports(buttonName){
      var isRequiredMsg = '<%=isRequiredMsg%>'
      var returnFlag = true;
      var noOfControls = document.getElementById('noOfControls').value;
      var errorMsgs = "";
      var buttonClicked = document.getElementById('buttonPressed');
    
      buttonClicked.value = buttonName;
      var showSaveAsButton = document.getElementById('showSaveAsButton');
      showSaveAsButton.value = "true";
      document.forms[0].submit();
    }
    
    require(["dijit/registry", "dojo/on", "dojo/ready"],
            function(registry, on, ready) {
            ready( function(){
            	
            var buttonClicked = document.getElementById('buttonPressed');
            var pageCall = document.getElementById('pageCall');
            
          <%-- T36000016123 - R 8.1.0.6 ER   jgadela - added condition fromException --%>
            var fromException = document.getElementById('fromExceptionH');
            if((buttonClicked.value == 'SpecifyReportButton' || pageCall.value == 'Y') && fromException.value!='Y' ){
            var section=registry.byId("section");
            section.set("open",false);
            }
    });
  });

  require(["dojo/dom", "dojo/dom-style", "dojo/dom-class", "dojo/query", "dijit/registry"],
      function( dom, domStyle, domClass, query, registry ) {

    local.expandPage = function() {
      var expandNode = dom.byId("expandNode");
      if ( expandNode ) {
        <%-- hide everything else --%>
        query(".pageHeader").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".pageHeaderNavigation").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".secondaryNav").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query("#divider1").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".pageFooter").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query("#reportSection").forEach( function(entry, i) {
          domStyle.set(entry, "width", "auto");
          domStyle.set(entry, "height", "auto");
          domStyle.set(entry, "overflow", "visible");
        });
        domClass.add(expandNode, "expandedPage");

        var expandButton = registry.byId("ExpandButton");
        var collapseButton = registry.byId("CollapseButton");
        if (expandButton && collapseButton) {
          domStyle.set(expandButton.domNode, "display", "none");
          domStyle.set(collapseButton.domNode, "display", "inline-block");
        }

        expandedPage = "Y";
        local.setPageLinkExpandedPageParmVal( "firstPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "prefPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "nextPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "lastPageLink", "Y" );
      }
    }

    local.collapsePage = function() {
      var expandNode = dom.byId("expandNode");
      if ( expandNode ) {
        <%-- and remove the expand class --%>
        domClass.remove(expandNode, "expandedPage");
        query("#reportSection").forEach( function(entry, i) {
          domStyle.set(entry, "width", "100%");
          domStyle.set(entry, "height", "375px");
          domStyle.set(entry, "overflow", "auto");
        });
        <%-- show everything that was hidden above --%>
        query(".pageHeader").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".pageHeaderNavigation").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".secondaryNav").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query("#divider1").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".pageFooter").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });

        var expandButton = registry.byId("ExpandButton");
        var collapseButton = registry.byId("CollapseButton");
        if (expandButton && collapseButton) {
          domStyle.set(collapseButton.domNode, "display", "none");
          domStyle.set(expandButton.domNode, "display", "inline-block");
        }

        expandedPage = "N";
        local.setPageLinkExpandedPageParmVal( "firstPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "prefPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "nextPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "lastPageLink", "N" );
      }
    },

    <%-- this assumes expandedPage parm exists and is the last in the href url --%>
    local.setPageLinkExpandedPageParmVal = function( pageLinkId, parmVal ) {
      var pageLink = dom.byId(pageLinkId);
      if ( pageLink ) {
        var myHref = pageLink.href;
        var parmIdx = myHref.indexOf("expandedPage=");
        if ( parmIdx >= 0 ) {
          parmIdx = parmIdx + 13;
          myHref = myHref.substring(0, parmIdx) + parmVal;
        }
        pageLink.href = myHref;
      }
    }

  });

</script>

</body>
</html>

<%
formMgr.storeInDocCache("default.doc", new DocumentHandler());
Debug.debug("***END********************STANDARD*REPORTS*PROMPT*PAGE1**************************END***");
%>
