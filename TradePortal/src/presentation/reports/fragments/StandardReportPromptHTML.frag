<%--cquinton 2/14/2013 ir#11445 fix formatting of page subheaders so they can be expanded/collapsed--%>
<div class="pageSubHeader">

<%
	if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
	{
		//Retrieve the reports in HTML format and print the body
		if(pgn.getCurrent()==1)
		{
			prevPage = 1;
		}
		else
		{
			prevPage = pgn.getCurrent()-1;
		}


		if(pgn.getCurrent() == totalNoPages)
		{
		  nextPage = totalNoPages ;
		}
		else
		{
		  nextPage = pgn.getCurrent()+1;
		}

		if (totalNoPages != 1)
		{
		  for(int iLoop=0; iLoop < totalNoPages; iLoop++){
	        dropdownOptions.append("<option value=\"");
	        dropdownOptions.append(iLoop+1);
	        dropdownOptions.append("\"");
	        if ((Integer.valueOf(iLoop).compareTo(Integer.valueOf(pgn.getCurrent()-1))) == 0)	
	        {
	       	 dropdownOptions.append("selected");
	        }
	        dropdownOptions.append(">");
	        dropdownOptions.append(iLoop+1);    
	        dropdownOptions.append("</option>");
	        
	        }
%>
<input type=hidden value='<%=StringFunction.xssCharsToHtml(storageToken)%>' name="storageToken" id="storageToken">
<input type=hidden value='<%=StringFunction.xssCharsToHtml(pageToken)%>' name="pageToken" id="pageToken">

	<Table align="left" style="margin-top: -4px; margin-bottom: -6px;" >
	
			<TR align="center">
			<%//01-14-2014 Rel 8.4 CR-854 T36000023777 [BEGIN]- label translations
			String pageLabel =  resMgr.getText("listview.page",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);
			String ofLabel =  resMgr.getText("listview.of",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);%>
					<TD><%=widgetFactory.createSubLabel(pageLabel)%></TD>					
					<TD><%=widgetFactory.createSelectField("PaginationDropdown","", "", dropdownOptions.toString(), false,false, false, "onChange='filterPages();' style=\"width: 50px;\"", "", "none")%></TD>
					<TD><%=widgetFactory.createSubLabel(ofLabel)%>	 
						<%=widgetFactory.createSubLabel(Integer.toString(totalNoPages))%>	
							
					</TD>
					<%-- Komal ANZ Issue No.359 BEGINS --%>
					<%if(pgn.getCurrent() != 1){%>
						<TD>
						<span class="paginationFirst">
						<a id="firstPageLink" href="/portal/reports/StandardReportPrompt1.jsp?goToPage=1&pageCall=Y&pageToken=<%=StringFunction.xssCharsToHtml(pageToken)%>&storageToken=<%=StringFunction.xssCharsToHtml(storageToken)%>&oid=<%=StringFunction.xssCharsToHtml(request.getParameter("oid"))%>&expandedPage=<%=StringFunction.xssCharsToHtml(expandedPage)%>"></a>
						</span>
						</TD>				
						<TD>
						<span class="paginationPrev">
						<a id="prevPageLink" href="/portal/reports/StandardReportPrompt1.jsp?goToPage=<%=prevPage%>&pageCall=Y&pageToken=<%=StringFunction.xssCharsToHtml(pageToken)%>&storageToken=<%=StringFunction.xssCharsToHtml(storageToken)%>&oid=<%=StringFunction.xssCharsToHtml(request.getParameter("oid"))%>&expandedPage=<%=StringFunction.xssCharsToHtml(expandedPage)%>"></a>
						</span>
						</TD>
						<%} else {%>
						<TD>
						<span class="paginationFirstDisabled">
						<a href="#"  onclick="return false;"></a>
						</span>
						</TD>
						<TD>
						<span class="paginationPrevDisabled">
						<a href="#" onclick="return false;"></a>
						</span>
						</TD>
			 	<%}%>	
						
					<%if(pgn.getCurrent() != totalNoPages){%>
						<TD>
						<span class="paginationNext">
						<a id="nextPageLink" href="/portal/reports/StandardReportPrompt1.jsp?goToPage=<%=nextPage%>&pageCall=Y&pageToken=<%=StringFunction.xssCharsToHtml(pageToken)%>&storageToken=<%=StringFunction.xssCharsToHtml(storageToken)%>&oid=<%=StringFunction.xssCharsToHtml(request.getParameter("oid"))%>&expandedPage=<%=StringFunction.xssCharsToHtml(expandedPage)%>"></a>
						</span>
						</TD>
						<TD>
						<span class="paginationLast">
						<a id="lastPageLink" href="/portal/reports/StandardReportPrompt1.jsp?goToPage=<%=totalNoPages%>&pageCall=Y&pageToken=<%=StringFunction.xssCharsToHtml(pageToken)%>&storageToken=<%=StringFunction.xssCharsToHtml(storageToken)%>&oid=<%=StringFunction.xssCharsToHtml(request.getParameter("oid"))%>&expandedPage=<%=StringFunction.xssCharsToHtml(expandedPage)%>"></a>
		                         	</span>
						</TD>
					<%} else {%>
						<TD>
						<span class="paginationNextDisabled">
					<a href="#"  onclick="return false;"></a>
						</span>
						</TD>
						<TD>
						<span class="paginationLastDisabled">
					<a href="#" onclick="return false;"></a>
						</span>
						</TD>	
				<%}%>
					<%-- Komal ANZ Issue No.359 ENDS --%>
				</TR>
			</Table>
<%
		}
%>
<%// 20/02/2013 jgadela CQ T36000012078 - corrected the storageToken%>
  <jsp:include page="/common/ReportsTypesSubHeaderBar.jsp">
    <jsp:param name="sToken" value="<%=storageToken%>"/> 	
    <jsp:param name="divID" value="standardReportsPDFId"/>	
  </jsp:include>

  <div style="clear:both;"></div>

</div>

<%
  //cquinton 2/14/2013 ir#11445 on first entry to page, set explicit height of the report viewport.
  // also add ability to expand/collapse page.  on subsequent entry if page if expanded
  // ensure we set the expanded styles correctly.  see expandPage() function on main page for
  // more details
  String reportSectionStyle = "";
  if ("Y".equals(expandedPage)) {
    reportSectionStyle="position: relative; width:auto; height: auto; overflow: visible; *zoom: 1;";
  }
  else {
    reportSectionStyle="position: relative; width:100% height: 375px; overflow: auto; *zoom: 1;";
  }
%>
<div id="reportSection" style="<%=reportSectionStyle%>">

<%
	repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 
	
	if(repView!=null)
	{
			
		String strHeight = "height:50%";
		String strWidth = "width:100%";
		String strDivStart = "<DIV id = WebiContent style=\"postion:relative";
		String strDivEnd = "\">";
		String strHead = repView.getStringPart("head",false);
		out.println(strHead);
		String strBody = repView.getStringPart("body",false);
		int iDiv1 = strBody.indexOf("<div");
		int iDiv2 = strBody.indexOf("<div",iDiv1+1);
		String strDiv = strBody.substring(iDiv1,iDiv2);
		StringTokenizer stk = new StringTokenizer(strDiv,";");

		while(stk.hasMoreTokens())
		{
			String strToken = stk.nextToken();
			if(strToken.startsWith("width"))
			{
				strWidth = strToken;
			}
			else if(strToken.startsWith("height"))
			{
				strHeight = strToken;
			}
		}
		while(strBody.indexOf("overflow:hidden;")>-1){
			int istrBodylength = strBody.length();
			int istrBodybeginOverFlow = strBody.indexOf("overflow:hidden;");
			String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);
			String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+16 ,istrBodylength);
			strBody = strBodyPart1 + strBodyPart2;
		}

		while(strBody.indexOf("overflow: hidden;")>-1){
			int istrBodylength = strBody.length();
			int istrBodybeginOverFlow = strBody.indexOf("overflow: hidden;");
			String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);
			String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+17 ,istrBodylength);
			strBody = strBodyPart1 + strBodyPart2;
		}
		
		while(strBody.indexOf("ro:style")>-1)
		{
			strBody = strBody.replaceAll("ro:style","style");
		}
		
		while(strBody.indexOf("pxpadding")>-1)
		{
			strBody = strBody.replaceAll("pxpadding","px;padding");
		}
		
		if(strBody.indexOf("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;")> -1){			
			strBody = strBody.replace("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;","<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:48px;");
		}

		//02/05/2014 R 8.4 T36000013921 - Corrected image display.
		if(strHead.indexOf("ap{position: absolute;}")>-1){
			strBody = strBody.replaceAll("class=\"ap\"","");
		}
		
     %>
	 <DIV ID="webIContent" style=" position:relative;<%=strWidth%>;<%=strHeight%>">  
    <%
	out.println(strBody);
	%>
	</DIV>
	<%
		out.flush();
	}
	%>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td>&nbsp;</td>
	    </tr>
	  </table>
	<%
	}
	else
	{

		for(int pgcount = 1;pgcount<=totalNoPages;pgcount++){
			pgn.setTo(pgcount);
			repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 

			String strHeight = "height:50%";
			String strWidth = "width:100%";
			String strDivStart = "<DIV id = WebiContent style=\"postion:relative";
			String strDivEnd = "\">";
			String strHead = repView.getStringPart("head",false);
			out.println(strHead);
			String strBody = repView.getStringPart("body",false);
			int iDiv1 = strBody.indexOf("<div");
			int iDiv2 = strBody.indexOf("<div",iDiv1+1);
			String strDiv = strBody.substring(iDiv1,iDiv2);
			StringTokenizer stk = new StringTokenizer(strDiv,";");

			while(stk.hasMoreTokens())
			{
				String strToken = stk.nextToken();

				if(strToken.startsWith("width"))
				{
					strWidth = strToken;
				}
				else if(strToken.startsWith("height"))
				{
					strHeight = strToken;
				}
			}


			while(strBody.indexOf("overflow:hidden;")>-1){
		 		
		 				 		
				int istrBodylength = strBody.length();
				int istrBodybeginOverFlow = strBody.indexOf("overflow:hidden;");

				String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);

				String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+16 ,istrBodylength);

				strBody = strBodyPart1 + strBodyPart2;
				
		
 			}


			while(strBody.indexOf("overflow: hidden;")>-1){
		 		
		 				 		
				int istrBodylength = strBody.length();
				int istrBodybeginOverFlow = strBody.indexOf("overflow: hidden;");

				String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);

				String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+17 ,istrBodylength);

				strBody = strBodyPart1 + strBodyPart2;
				
		
 			}

			while(strBody.indexOf("ro:style")>-1)
			{
				strBody = strBody.replaceAll("ro:style","style");
			}
			
			while(strBody.indexOf("pxpadding")>-1)
			{
				strBody = strBody.replaceAll("pxpadding","px;padding");
			}
			
		    if(strBody.indexOf("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;")> -1){			
				strBody = strBody.replace("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;","<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:48px;");
			}
			
			//02/05/2014 R 8.4 T36000013921 - Corrected image display.
	        if(strHead.indexOf("ap{position: absolute;}")>-1){
				strBody = strBody.replaceAll("class=\"ap\"","");
			}


		       %>
		       <DIV ID="webIContent" style=" position:relative;<%=strWidth%>;<%=strHeight%>">  



		       <%

			out.println(strBody);


			%>

			</DIV>

			<%

			out.flush();


		}

	}
%>
</div>
 <script  type="text/javascript">
 var oidRep = '<%=StringFunction.escapeQuotesforJS(request.getParameter("oid"))%>';
	function filterPages() {
	    require(["dojo/dom"],
	      function(dom){
			var storageToken = document.getElementById("storageToken").value;
			var pageToken = document.getElementById("pageToken").value;
	    	var pagingSelect = dijit.byId("PaginationDropdown").attr('value');
	    	location.href="/portal/reports/StandardReportPrompt1.jsp?goToPage="+pagingSelect+"&pageCall=Y&pageToken="+pageToken+"&storageToken="+storageToken+"&oid="+oidRep+"&expandedPage="+expandedPage; 	    	
		});
	}  
  
  </script>
