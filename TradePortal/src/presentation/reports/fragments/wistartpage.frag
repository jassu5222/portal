 <%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" 
						 import="com.businessobjects.rebean.wi.*,com.crystaldecisions.sdk.framework.*,com.crystaldecisions.sdk.exception.*"%>

<jsp:useBean id="objUtils" class="com.businessobjects.adv_ivcdzview.Utils" scope="application" />
<jsp:useBean id="objUserSettings" class="com.businessobjects.adv_ivcdzview.UserSettings" scope="session" />
<jsp:useBean id="requestWrapper" class="com.businessobjects.adv_ivcdzview.RequestWrapper" scope="page" />

<%
ReportEngines reportEngines = null;
ReportEngine reportEngine =  null;
IEnterpriseSession boSession = null;
try
{
	boSession = (IEnterpriseSession) request.getSession().getAttribute("CE_ENTERPRISESESSION");
	
	if (boSession != null)
	{
		reportEngines = (ReportEngines)request.getSession().getAttribute("ReportEngines");
		if (reportEngines == null)
		{
			reportEngines = (ReportEngines)boSession.getService("ReportEngines");
			request.getSession().setAttribute("ReportEngines", reportEngines);
		}
		//get the webi report engine
		reportEngine = reportEngines.getService(ReportEngines.ReportEngineType.WI_REPORT_ENGINE);
		request.getSession().setAttribute("ReportEngine", reportEngine);
	}
	else
	{
		out.clearBuffer();
		%>
		<%-- <jsp:forward page='<%= wiStartPagePhysicalPage %>' /> --%>
		<%
	}
	/*if (objUtils.objConfig == null)
	objUtils.loadViewerConfig();*/
}
catch(Exception e)
{
		System.out.println("Exception while attempting to get webiSession in wistartpage."); 
		e.printStackTrace();
	   /* DocumentHandler wiStartPageXmlDoc = formMgr.getFromDocCache();
		MediatorServices wiStartPageMediatorServices = new MediatorServices();
		wiStartPageMediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());        
		wiStartPageMediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 	TradePortalConstants.WEBI_SERVER_ERROR);
		wiStartPageMediatorServices.addErrorInfo();
		wiStartPageXmlDoc.setComponent("/Error",         wiStartPageMediatorServices.getErrorDoc());
		wiStartPageXmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
		formMgr.storeInDocCache("default.doc", wiStartPageXmlDoc);
		String wiStartPagePhysicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);*/
%>
		<%--<jsp:forward page='<%= wiStartPagePhysicalPage %>' />--%>
<%        
}    
%>
