<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,java.io.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%
Debug.debug("***START********************VIEW*EXCEL*PAGE**************************START***");
String                  reportName          = null;
DocumentInstance 	widDoc = null;
Reports 		widReports = null;
Report 			widReportEXCEL = null;
BinaryView 		docBinaryView = null;
ServletOutputStream outputStream = null;  
StringBuffer            defaultFileName     = null;
%>

<%@ include file="fragments/wistartpage.frag" %>

<%
String storageToken = request.getParameter("storageToken");
response.reset();
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragma","no-cache");
//NARAYAN-IR-PSUL011859834  Begin
// commented the below code because while downloading excel file this is preventing to chahce on local system and then system doesn't find  the excel file to open
//response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
//response.setHeader("Pragma","no-cache"); //HTTP 1.0   
//NARAYAN-IR-PSUL011859834  End 
response.setDateHeader("expires", 0);
request.setCharacterEncoding("UTF-8");

try
{
	//Request coming for the first time
	widDoc = reportEngine.getDocumentFromStorageToken(storageToken);
	
	widReports = widDoc.getReports();
	widReportEXCEL = widReports.getItem(0);
	
	//Set the pagination mode
	widReportEXCEL.setPaginationMode(PaginationMode.Listing);
	// Set the default file name to use for the report data file to be downloaded
	defaultFileName = new StringBuffer();

	//Retrieve the report name from the report
	reportName = widDoc.getProperties().getProperty(PropertiesType.NAME, "");

	defaultFileName.append((reportName+".xls"));
	docBinaryView = (BinaryView) widReportEXCEL.getView(OutputFormatType.XLS);
	byte[] abyBinaryContent = docBinaryView.getContent();
	int iLength = docBinaryView.getContentLength();
	
	response.setContentLength(iLength);
	response.setContentType("application/vnd.ms-excel");
    String disposition = "attachment; fileName="+defaultFileName;
	response.setHeader("Content-Disposition", disposition);
	outputStream = response.getOutputStream();
	outputStream.write(abyBinaryContent);

}catch (REException rex)
{
   //Catch all exception
     System.out.println("EXCEPTION in View Excel Page ######################"+rex.getErrorCode()+" "+rex.getMessage());
}
finally
{
//         try
//         {
//                 outputStream.close();
//         }
//         catch(IOException e){}
        outputStream = null;
}
//Garbage Collection
widDoc = null;
widReports = null;
widReportEXCEL = null;
docBinaryView = null;

%>


<%
Debug.debug("***END********************VIEW EXCEL *REPORTS*PAGE**************************END***");
%>