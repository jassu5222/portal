<%--
 *    This JSP provides the border and controls for the system overview pages
 *    Based on the "page" URL parameter that is passed in, the system overview
 *    includes the appropriate fragment that contains the actual content.
 *
 *     Copyright   2002
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*"%>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>

<%
   // Total number of pages in the system overview
   int TOTAL_PAGES = 6;

   // Determine name of the CSS style sheet
   StringBuffer cssFilename = new StringBuffer(TradePortalConstants.IMAGES_PATH);
   cssFilename.append(userSession.getBrandingDirectory());
   cssFilename.append("/");
   cssFilename.append(resMgr.getText("brandedImage.stylesheet", TradePortalConstants.TEXT_BUNDLE));

   // Determine path to the bank logo
   StringBuffer logoFilename = new StringBuffer(TradePortalConstants.IMAGES_PATH);
   logoFilename.append(userSession.getBrandingDirectory());
   logoFilename.append(resMgr.getText("brandedImage.logo", TradePortalConstants.TEXT_BUNDLE));

   // Path to branded images
   String brandedImagePath = StringFunction.xssCharsToHtml(request.getContextPath()) + "/images/" + userSession.getBrandingDirectory() + "/";


	 //GGAYLE - 2007/02/26 - IR DEUH020561518
	 // Retrieve the images from the resource manager and check them for a leading "/".  Since the brandedImagePath
	 // contains a trailing "/", we will end up with "//" if the image location start with a "/".  The "/" from the image
	 // location will be removed.
	 String slashSeparator = "/";

	 // For Overview.jsp
	 String leftFrameFill = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.SystemOverviewLeftFrameFill", TradePortalConstants.TEXT_BUNDLE));
	 if(leftFrameFill.startsWith(slashSeparator))
	 {
		 leftFrameFill = leftFrameFill.substring(1);
	 }

	 String leftUpperCorner = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.SystemOverviewLeftUpperCorner", TradePortalConstants.TEXT_BUNDLE));
	 if(leftUpperCorner.startsWith(slashSeparator))
	 {
		 leftUpperCorner = leftUpperCorner.substring(1);
	 }

	 String leftLowerCorner =StringFunction.xssCharsToHtml( resMgr.getText("brandedImage.SystemOverviewLeftLowerCorner", TradePortalConstants.TEXT_BUNDLE));
	 if(leftLowerCorner.startsWith(slashSeparator))
	 {
		 leftLowerCorner = leftLowerCorner.substring(1);
	 }

	 String rightFrameFill =StringFunction.xssCharsToHtml( resMgr.getText("brandedImage.SystemOverviewRightFrameFill", TradePortalConstants.TEXT_BUNDLE));
	 if(rightFrameFill.startsWith(slashSeparator))
	 {
		 rightFrameFill = rightFrameFill.substring(1);
	 }

	 String rightUpperCorner = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.SystemOverviewRightUpperCorner", TradePortalConstants.TEXT_BUNDLE));
	 if(rightUpperCorner.startsWith(slashSeparator))
	 {
		 rightUpperCorner = rightUpperCorner.substring(1);
	 }

	 String rightLowerCorner = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.SystemOverviewRightLowerCorner", TradePortalConstants.TEXT_BUNDLE));
	 if(rightLowerCorner.startsWith(slashSeparator))
	 {
		 rightLowerCorner = rightLowerCorner.substring(1);
	 }

	 //////////////////////////////////////////////////
	 // For OverviewPage1.frag and OverviewPage5.frag
	 //////////////////////////////////////////////////
	 String logoutImgRO = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.LogoutImgRO", TradePortalConstants.TEXT_BUNDLE));
	 if(logoutImgRO.startsWith(slashSeparator))
	 {
		 logoutImgRO = logoutImgRO.substring(1);
	 }

	 String logoutImg = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.LogoutImg", TradePortalConstants.TEXT_BUNDLE));
	 if(logoutImg.startsWith(slashSeparator))
	 {
		 logoutImg = logoutImg.substring(1);
	 }

	 String homeImgRO = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.HomeImgRO", TradePortalConstants.TEXT_BUNDLE));
	 if(homeImgRO.startsWith(slashSeparator))
	 {
		 homeImgRO = homeImgRO.substring(1);
	 }

	 String homeImg = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.HomeImg", TradePortalConstants.TEXT_BUNDLE));
	 if(homeImg.startsWith(slashSeparator))
	 {
		 homeImg = homeImg.substring(1);
	 }

	 String helpImgRO = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.HelpImgRO", TradePortalConstants.TEXT_BUNDLE));
	 if(helpImgRO.startsWith(slashSeparator))
	 {
		 helpImgRO = helpImgRO.substring(1);
	 }

	 String helpImg = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.HelpImg", TradePortalConstants.TEXT_BUNDLE));
	 if(helpImg.startsWith(slashSeparator))
	 {
		 helpImg = helpImg.substring(1);
	 }

	 String leftCap = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.LeftCap", TradePortalConstants.TEXT_BUNDLE));
	 if(leftCap.startsWith(slashSeparator))
	 {
		 leftCap = leftCap.substring(1);
	 }

	 String messagesImgRO = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.MessagesImgRO", TradePortalConstants.TEXT_BUNDLE));
	 if(messagesImgRO.startsWith(slashSeparator))
	 {
		 messagesImgRO = messagesImgRO.substring(1);
	 }

	 String messagesImg = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.MessagesImg", TradePortalConstants.TEXT_BUNDLE));
	 if(messagesImg.startsWith(slashSeparator))
	 {
		 messagesImg = messagesImg.substring(1);
	 }

	 String transactionsImgRO = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.TransactionsImgRO", TradePortalConstants.TEXT_BUNDLE));
	 if(transactionsImgRO.startsWith(slashSeparator))
	 {
		 transactionsImgRO = transactionsImgRO.substring(1);
	 }

	 String transactionsImg = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.TransactionsImg", TradePortalConstants.TEXT_BUNDLE));
	 if(transactionsImg.startsWith(slashSeparator))
	 {
		 transactionsImg = transactionsImg.substring(1);
	 }

	 String reportsImgRO = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.ReportsImgRO", TradePortalConstants.TEXT_BUNDLE));
	 if(reportsImgRO.startsWith(slashSeparator))
	 {
		 reportsImgRO = reportsImgRO.substring(1);
	 }

	 String reportsImg = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.ReportsImg", TradePortalConstants.TEXT_BUNDLE));
	 if(reportsImg.startsWith(slashSeparator))
	 {
		 reportsImg = reportsImg.substring(1);
	 }

	 String refdataImgRO = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.RefdataImgRO", TradePortalConstants.TEXT_BUNDLE));
	 if(refdataImgRO.startsWith(slashSeparator))
	 {
		 refdataImgRO = refdataImgRO.substring(1);
	 }

	 String refdataImg = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.RefdataImg", TradePortalConstants.TEXT_BUNDLE));
	 if(refdataImg.startsWith(slashSeparator))
	 {
		 refdataImg = refdataImg.substring(1);
	 }

	 String barFill = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.BarFill", TradePortalConstants.TEXT_BUNDLE));
	 if(barFill.startsWith(slashSeparator))
	 {
		 barFill = barFill.substring(1);
	 }

	 String rightCap = StringFunction.xssCharsToHtml(resMgr.getText("brandedImage.RightCap", TradePortalConstants.TEXT_BUNDLE));
	 if(rightCap.startsWith(slashSeparator))
	 {
		 rightCap = rightCap.substring(1);
	 }
	 //GGAYLE - 2007/02/26 - IR DEUH020561518


   // Determine which page we're looking at... Default to 1
   String pageNumberStr = request.getParameter("page");
   int pageNumber;

   if(pageNumberStr == null)
     pageNumber = 1;
   else
     pageNumber = Integer.parseInt(pageNumberStr);
%>


<html>
<head>
<title><%=resMgr.getText("SystemOverview.Welcome", TradePortalConstants.TEXT_BUNDLE) %> </title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 17:00:00 GMT"></meta>

<link rel="stylesheet" href="<%= cssFilename %>">

<script language="JavaScript">

function MM_swapImgRestore() { <%-- v3.0 --%>
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { <%-- v3.0 --%>
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { <%-- v4.01 --%>
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}


function MM_swapImage() { <%-- v3.0 --%>
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_showHideLayers() { <%-- v3.0 --%>
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }

}
	<%--
	// If window is currently in a frame, close the frame's owner
	// otherwise, just close this window
	--%>
	function closeButtonAction()    {
		if(top.location == self.location)
			window.close();
		else
			window.parent.close();
}

</script>
</head>

<body bgcolor="#FFFFFF" onLoad="MM_preloadImages('<%=TradePortalConstants.IMAGES_PATH%>exit_ro.gif','<%=  resMgr.getText("SystemOverview.nextRO", TradePortalConstants.TEXT_BUNDLE)%>', '<%=  resMgr.getText("SystemOverview.previous", TradePortalConstants.TEXT_BUNDLE)%>', '<%=TradePortalConstants.IMAGES_PATH%>exit_ro.gif','/portal/images/1_CTro.gif','/portal/images/1_3VTro.gif','/portal/images/1_5ATro.gif','/portal/images/1_7BPro.gif','/portal/images/3_2Nro.gif')">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td background="<%= brandedImagePath + leftFrameFill%>" valign=top><img src="<%= brandedImagePath + leftUpperCorner%>" width="15" height="22"></td>
    <td width="100%" class="BankColor">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td width="100%" align="right">
            <p class="Links"><a href="javascript:closeButtonAction()" class="Links"><%=resMgr.getText("SystemOverview.ExitSystemOverview", TradePortalConstants.TEXT_BUNDLE) %></a></p>
          </td>
          <td width="10" nowrap class="BankColor">&nbsp;</td>
          <td nowrap class="BankColor"><a href="javascript:closeButtonAction()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image18','','<%=TradePortalConstants.IMAGES_PATH%>exit_ro.gif',1)"><img name="Image18" border="0" src="<%=TradePortalConstants.IMAGES_PATH%>exit_off.gif" width="22" height="22"></a></td>
        </tr>
      </table>
    </td>
    <td background="<%= brandedImagePath + rightFrameFill%>" valign=top><img src="<%= brandedImagePath + rightUpperCorner%>" width="15" height="22"></td>
  </tr>
  <tr>
    <td background="<%= brandedImagePath + leftFrameFill%>">&nbsp;</td>
    <td>&nbsp;</td>
    <td background="<%= brandedImagePath + rightFrameFill%>">&nbsp;</td>
  </tr>
  <tr>
    <td background="<%= brandedImagePath + leftFrameFill%>">&nbsp;</td>
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
             <td width="20" nowrap>&nbsp;</td>
             <td width="100%">
              <%
                  // Determine which page content fragment to include and include it
                  switch(pageNumber)
                   {
                       case 1:
               %>
                        <%@ include file="fragments/OverviewPage1.frag" %>
               <%
                          break;

                       case 2:
               %>
                        <%@ include file="fragments/OverviewPage2.frag" %>
               <%
                          break;

                       case 3:
               %>
                        <%@ include file="fragments/OverviewPage3.frag" %>
               <%
                          break;

                       case 4:
               %>
                        <%@ include file="fragments/OverviewPage4.frag" %>
               <%
                          break;

                       case 5:
               %>
                        <%@ include file="fragments/OverviewPage5.frag" %>
               <%
                          break;

                       case 6:
               %>
                        <%@ include file="fragments/OverviewPage6.frag" %>
               <%
                          break;

                   }
               %>
             </td>
          </tr>
        </table>
    </td>
    <td background="<%= brandedImagePath + rightFrameFill%>">&nbsp;</td>
  </tr>
  <tr>
    <td background="<%= brandedImagePath + leftFrameFill%>">&nbsp;</td>
    <td>&nbsp;</td>
    <td background="<%= brandedImagePath + rightFrameFill%>">&nbsp;</td>
  </tr>
  <tr>
    <td background="<%= brandedImagePath + leftFrameFill%>" align="left" valign="bottom"><img src="<%= brandedImagePath + leftLowerCorner%>" width="15" height="22"></td>
    <td class="BankColor">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" align="right">
            <div align="left">
              <p class="BodyTextwhite"><%=resMgr.getText("SystemOverview.Page", TradePortalConstants.TEXT_BUNDLE) %>&nbsp;<%=pageNumber%></p>
            </div>
          </td>
          <td width="10" nowrap>&nbsp;</td>
<%
    // Include the 'Previous' button on all pages except the first
    if(pageNumber != 1)
     {
%>
          <td nowrap align="right"><a href="<%=request.getContextPath()+"/sysoverview/Overview.jsp?page="+(pageNumber-1) %>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image20','','<%=  resMgr.getText("SystemOverview.previousRO", TradePortalConstants.TEXT_BUNDLE)%>',1)"><img name="Image20" border="0" src="<%=  resMgr.getText("SystemOverview.previous", TradePortalConstants.TEXT_BUNDLE)%>" width="80" height="17"></a></td>
<%
     }

    // Include the 'Next' button on all pages except the last
    if(pageNumber != TOTAL_PAGES)
     {
%>
          <td width="10" nowrap>&nbsp;</td>
          <td><a href="<%=request.getContextPath()+"/sysoverview/Overview.jsp?page="+(pageNumber+1) %>"  onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image19','','<%=  resMgr.getText("SystemOverview.nextRO", TradePortalConstants.TEXT_BUNDLE)%>',1)"><img name="Image19" border="0" src="<%=  resMgr.getText("SystemOverview.next", TradePortalConstants.TEXT_BUNDLE)%>" width="71" height="17"></a></td>
<%
     }
%>
        </tr>
      </table>
    </td>
    <td valign=bottom background="<%= brandedImagePath + rightFrameFill%>"><img src="<%= brandedImagePath + rightLowerCorner%>" width="15" height="22"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20" nowrap>&nbsp;</td>
    <td>
&nbsp;
    </td>
    <td width="20" nowrap>&nbsp;</td>
  </tr>
</table>
<span class="ControlLabel"></span>
</body>
</html>
