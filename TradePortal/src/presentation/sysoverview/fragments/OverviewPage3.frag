<%--
 *    System Overview Content, page 3
 *
 *     Copyright  � 2002                      
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<div id="verifytransaction" style="position:absolute; left:45px; top:278px; width:250px; height:63px; z-index:1; visibility: hidden" class="BankColor"> 
  <table width="250" border="0" cellspacing="0" cellpadding="0" class="BankColor" height="63">
    <tr> 
      <td width="5" nowrap>&nbsp;</td>
      <td width="100%" align="left" valign="top"> 
        <p class="BodyTextwhite"><span class="BodyTextBoldwhite"><%= resMgr.getText("SystemOverview.VerifyTransaction", TradePortalConstants.TEXT_BUNDLE) %></span> 
          - <span class="BodyText"><%= resMgr.getText("SystemOverview.UserInitiates", TradePortalConstants.TEXT_BUNDLE) %></span></p>
      </td>
      <td width="5" nowrap>&nbsp;</td>
    </tr>
  </table>
</div>
<div id="notification" style="position:absolute; left:45px; top:278px; width:250px; height:63px; z-index:2; visibility: hidden" class="BankColor"> 
  <table width="250" border="0" cellspacing="0" cellpadding="0" class="BankColor" height="63">
    <tr> 
      <td width="5" nowrap>&nbsp;</td>
      <td width="100%" align="left" valign="top"> 
        <p class="BodyTextwhite"><span class="BodyTextBoldwhite"><%= resMgr.getText("SystemOverview.Notification", TradePortalConstants.TEXT_BUNDLE) %></span> 
          - <span class="BodyText"><%= resMgr.getText("SystemOverview.AfterTheBankProcesses", TradePortalConstants.TEXT_BUNDLE) %></span></p>
      </td>
      <td width="5" nowrap>&nbsp;</td>
    </tr>
  </table>
</div>
<div id="bankprocessing" style="position:absolute; left:45px; top:278px; width:250px; height:63px; z-index:4; visibility: hidden" class="BankColor"> 
  <table width="250" border="0" cellspacing="0" cellpadding="0" class="BankColor" height="63">
    <tr> 
      <td width="5" nowrap>&nbsp;</td>
      <td width="100%" class="BodyTextwhite" align="left" valign="top"><span class="BodyTextBoldwhite"><%= resMgr.getText("SystemOverview.BankProcessing", TradePortalConstants.TEXT_BUNDLE) %></span> -<span class="BodyText"> <%= resMgr.getText("SystemOverview.AfterSuccessfulAuthorization", TradePortalConstants.TEXT_BUNDLE) %></span></td>
      <td width="5" nowrap>&nbsp;</td>
    </tr>
  </table>
</div>
<div id="authorisetransaction" style="position:absolute; left:45px; top:278px; width:250px; height:63px; z-index:5; visibility: hidden" class="BankColor"> 
  <table width="250" border="0" cellspacing="0" cellpadding="0" class="BankColor" height="63">
    <tr> 
      <td width="5" nowrap>&nbsp;</td>
      <td width="100%" align="left" valign="top"> 
        <p class="BodyTextwhite"><span class="BodyTextBoldwhite"><%= resMgr.getText("SystemOverview.AuthorizeTransaction", TradePortalConstants.TEXT_BUNDLE) %></span> 
          - <span class="BodyText"><%= resMgr.getText("SystemOverview.AfterTheTransaction", TradePortalConstants.TEXT_BUNDLE) %></span></p>
      </td>
      <td width="5" nowrap>&nbsp;</td>
    </tr>
  </table>
</div>
<div id="createtransaction" style="position:absolute; left:45px; top:278px; width:250px; height:63px; z-index:6; visibility: hidden" class="BankColor"> 
  <table width="250" border="0" cellspacing="0" cellpadding="0" class="BankColor" height="63">
    <tr> 
      <td width="5" nowrap>&nbsp;</td>
      <td width="100%" align="left" valign="top"> 
        <p class="BodyTextwhite"><span class="BodyTextBoldwhite"><%= resMgr.getText("SystemOverview.CreateTransaction", TradePortalConstants.TEXT_BUNDLE) %></span> 
          - <span class="BodyText"><%= resMgr.getText("SystemOverview.AUserCreates", TradePortalConstants.TEXT_BUNDLE) %></span></p>
      </td>
      <td width="5" nowrap>&nbsp;</td>
    </tr>
  </table>
</div>

            <p class="ControlLabel">
                 <%= resMgr.getText("SystemOverview.WhatIsTransactionWorkFlow", TradePortalConstants.TEXT_BUNDLE) %>
            </p>
            <p class="BodyText">
                 <%= resMgr.getText("SystemOverview.TransactionWorkFlow", TradePortalConstants.TEXT_BUNDLE) %>
            </p>

            <p class="ControlLabel"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image9','','<%= resMgr.getText("SystemOverview.CreateTransactionRO", TradePortalConstants.TEXT_BUNDLE) %>',1);MM_showHideLayers('verifytransaction','','hide','notification','','hide','bankprocessing','','hide','authorisetransaction','','hide','createtransaction','','show')"><img name="Image9" border="0" src="<%= resMgr.getText("SystemOverview.CreateTransactionImg", TradePortalConstants.TEXT_BUNDLE) %>" width="137" height="96"></a><img src="/portal/images/1_2gap.gif" width="18" height="96"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11','','<%= resMgr.getText("SystemOverview.VerifyTransactionRO", TradePortalConstants.TEXT_BUNDLE) %>',1);MM_showHideLayers('verifytransaction','','show','notification','','hide','bankprocessing','','hide','authorisetransaction','','hide','createtransaction','','hide')"><img name="Image11" border="0" src="<%= resMgr.getText("SystemOverview.VerifyTransactionImg", TradePortalConstants.TEXT_BUNDLE) %>" width="131" height="96"></a><img src="/portal/images/1_4gap.gif" width="19" height="96"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image13','','<%= resMgr.getText("SystemOverview.AuthorizeTransactionRO", TradePortalConstants.TEXT_BUNDLE) %>',1);MM_showHideLayers('verifytransaction','','hide','notification','','hide','bankprocessing','','hide','authorisetransaction','','show','createtransaction','','hide')"><img name="Image13" border="0" src="<%= resMgr.getText("SystemOverview.AuthorizeTransactionImg", TradePortalConstants.TEXT_BUNDLE) %>" width="130" height="96"></a><a href="#" onMouseOver="MM_showHideLayers('verifytransaction','','hide','notification','','hide','bankprocessing','','hide','authorisetransaction','','hide','createtransaction','','hide')"><img src="/portal/images/1_6gap.gif" width="25" height="96" border="0"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image15','','<%= resMgr.getText("SystemOverview.BankProcessingRO", TradePortalConstants.TEXT_BUNDLE) %>',1);MM_showHideLayers('verifytransaction','','hide','notification','','hide','bankprocessing','','show','authorisetransaction','','hide','createtransaction','','hide')"><img name="Image15" border="0" src="<%= resMgr.getText("SystemOverview.BankProcessingImg", TradePortalConstants.TEXT_BUNDLE) %>" width="133" height="96"></a><br>
              <a href="#" onMouseOver="MM_showHideLayers('verifytransaction','','hide','notification','','hide','bankprocessing','','hide','authorisetransaction','','hide','createtransaction','','hide')"><img src="<%= resMgr.getText("SystemOverview.FullLine", TradePortalConstants.TEXT_BUNDLE) %>" width="593" height="29" border="0"></a><br>
              <a href="#" onMouseOver="MM_showHideLayers('verifytransaction','','hide','notification','','hide','bankprocessing','','hide','authorisetransaction','','hide','createtransaction','','hide')"><img src="<%= resMgr.getText("SystemOverview.TradeServicesSystem", TradePortalConstants.TEXT_BUNDLE) %>" width="300" height="102" border="0"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image21','','<%= resMgr.getText("SystemOverview.NotificationRO", TradePortalConstants.TEXT_BUNDLE) %>',1);MM_showHideLayers('verifytransaction','','hide','notification','','show','bankprocessing','','hide','authorisetransaction','','hide','createtransaction','','hide')"><img name="Image21" border="0" src="<%= resMgr.getText("SystemOverview.NotificationImg", TradePortalConstants.TEXT_BUNDLE) %>" width="135" height="102"></a><img src="/portal/images/3_3gap.gif" width="25" height="102" onMouseDown="MM_showHideLayers('verifytransaction','','hide','notification','','hide','bankprocessing','','hide','authorisetransaction','','hide','createtransaction','','hide')"><a href="#" onMouseOver="MM_showHideLayers('verifytransaction','','hide','notification','','hide','bankprocessing','','hide','authorisetransaction','','hide','createtransaction','','hide')"><img src="/portal/images/3_4lastgap.gif" width="133" height="34" align="top" border="0"></a><br>
              <br>
            </p>
            <p class="BodyText">
               <%= resMgr.getText("SystemOverview.AcrossTheSystem", TradePortalConstants.TEXT_BUNDLE) %>
            </p>
