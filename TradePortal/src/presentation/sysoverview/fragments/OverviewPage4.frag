<%--
 *    System Overview Content, page 4
 *
 *     Copyright  � 2002                      
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  // Because of the way that the HTML for this page was designed, 
  // the <div> tags need to appear in different places depending on how
  // tall the bank's logo is.
  String brandingDirectory = userSession.getBrandingDirectory();

  // This value will work with most bank logos
  int topOfScreenShot = 127; 

%>

<div id="homepage" style="position:absolute; left:47; top:<%= topOfScreenShot %>px; width:521px; height:320px; z-index:1; visibility: hidden"> 
  <img src="<%=  resMgr.getText("SystemOverview.homepage", TradePortalConstants.TEXT_BUNDLE)%>" width="569" height="372"></div>
<div id="listingpage" style="position:absolute; left:47; top:<%= topOfScreenShot %>px; width:521; height:320; z-index:2; visibility: hidden"> 
  <img src="<%=  resMgr.getText("SystemOverview.listview", TradePortalConstants.TEXT_BUNDLE)%>" width="569" height="295"></div>
<div id="proceduralpage" style="position:absolute; left:47; top:<%= topOfScreenShot %>px; width:521; height:320; z-index:3; visibility: hidden"> 
  <img src="<%=  resMgr.getText("SystemOverview.transaction", TradePortalConstants.TEXT_BUNDLE)%>" width="569" height="372"></div>
<div id="detailpage" style="position:absolute; left:47; top:<%= topOfScreenShot %>px; width:521; height:320; z-index:4; visibility: hidden"> 
  <img src="<%=  resMgr.getText("SystemOverview.details", TradePortalConstants.TEXT_BUNDLE)%>" width="569" height="372"> </div>
<div id="homepagetext" style="position:absolute; left:542; top:57; width:172px; height:71px; z-index:7; visibility: hidden; background-color: #FFFFCC; layer-background-color: #FFFFCC; border: 1px none #000000"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="25">
    <tr> 
      <td width="10" nowrap>&nbsp;</td>
      <td width="100%" align="left" valign="middle"> 
        <p class="BodyTextBlue">
            <%= resMgr.getText("SystemOverview.YourHomePage", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
    </tr>
  </table>
</div>
<div id="screenoff" style="position:absolute; left:47px; top:<%= topOfScreenShot %>px; width:521px; height:320px; z-index:6; visibility: hidden"><img src="<%=  resMgr.getText("SystemOverview.homepageOff", TradePortalConstants.TEXT_BUNDLE)%>" width="569" height="372"></div>
<div id="pptext" style="position:absolute; left:542; top:57; width:172px; height:104px; z-index:8; visibility: hidden; background-color: #FFFFCC; layer-background-color: #FFFFCC; border: 1px none #000000"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="25">
    <tr> 
      <td width="10" nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="BodyTextBlue">
            <%= resMgr.getText("SystemOverview.ProceduralPages", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
    </tr>
  </table>
</div>
<div id="lptext" style="position:absolute; left:542px; top:57px; width:172px; height:89px; z-index:9; visibility: hidden; background-color: #FFFFCC; layer-background-color: #FFFFCC; border: 1px none #000000" class="ColorBeige"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="25" bgcolor="#FFFFCC">
    <tr> 
      <td width="10" nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="BodyTextBlue">
            <%= resMgr.getText("SystemOverview.ListingPages", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
    </tr>
  </table>
</div>
<div id="dptext" style="position:absolute; left:542; top:57; width:172px; height:75px; z-index:10; visibility: hidden; background-color: #FFFFCC; layer-background-color: #FFFFCC; border: 1px none #000000"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="25">
    <tr> 
      <td width="10" nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="BodyTextBlue">
            <%= resMgr.getText("SystemOverview.DetailPages", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
    </tr>
  </table>
</div>
            <p class="BodyText"><span class="ControlLabel">
                <%= resMgr.getText("SystemOverview.WhatDoScreensLookLike", TradePortalConstants.TEXT_BUNDLE) %>
            </span><br>
              <span class="BodyText"><br>
                <%= resMgr.getText("SystemOverview.RollYourCursor", TradePortalConstants.TEXT_BUNDLE) %>
              </span><br>
              <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image16','','<%=  resMgr.getText("SystemOverview.homepagePage", TradePortalConstants.TEXT_BUNDLE)%>',1);MM_showHideLayers('homepage','','show','listingpage','','hide','proceduralpage','','hide','detailpage','','hide','homepagetext','','show','screenoff','','hide','pptext','','hide','lptext','','hide','dptext','','hide')"><img name="Image16" border="0" src="<%=  resMgr.getText("SystemOverview.homepagePageRO", TradePortalConstants.TEXT_BUNDLE)%>" ></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image17','','<%=  resMgr.getText("SystemOverview.proceduralPage", TradePortalConstants.TEXT_BUNDLE)%>',1);MM_showHideLayers('homepage','','hide','listingpage','','hide','proceduralpage','','show','detailpage','','hide','homepagetext','','hide','screenoff','','hide','pptext','','show','lptext','','hide','dptext','','hide')"><img name="Image17" border="0" src="<%=  resMgr.getText("SystemOverview.proceduralPageRO", TradePortalConstants.TEXT_BUNDLE)%>" ></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image21','','<%=  resMgr.getText("SystemOverview.listingPage", TradePortalConstants.TEXT_BUNDLE)%>',1);MM_showHideLayers('homepage','','hide','listingpage','','show','proceduralpage','','hide','detailpage','','hide','homepagetext','','hide','screenoff','','hide','pptext','','hide','lptext','','show','dptext','','hide')"><img name="Image21" border="0" src="<%=  resMgr.getText("SystemOverview.listingPageRO", TradePortalConstants.TEXT_BUNDLE)%>" ></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image22','','<%=  resMgr.getText("SystemOverview.detailPage", TradePortalConstants.TEXT_BUNDLE)%>',1);MM_showHideLayers('homepage','','hide','listingpage','','hide','proceduralpage','','hide','detailpage','','show','homepagetext','','hide','screenoff','','hide','pptext','','hide','lptext','','hide','dptext','','show')"><img name="Image22" border="0" src="<%=  resMgr.getText("SystemOverview.detailPageRO", TradePortalConstants.TEXT_BUNDLE)%>" ></a><a href="#" onMouseOver="MM_showHideLayers('homepage','','hide','listingpage','','hide','proceduralpage','','hide','detailpage','','hide','homepagetext','','hide','screenoff','','show','pptext','','hide','lptext','','hide','dptext','','hide')"><img src="/portal/images/2x2_blank.gif"  border="0"></a><br>
              <a href="#" onMouseOver="MM_showHideLayers('homepage','','hide','listingpage','','hide','proceduralpage','','hide','detailpage','','hide','homepagetext','','hide','screenoff','','show','pptext','','hide','lptext','','hide','dptext','','hide')"><img src="/portal/images/2x2_blank.gif" width="700" height="345" border="0"></a> 
            </p>

