<%--
 *    System Overview Content, page 5
 *
 *     Copyright  � 2002
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
            <p class="ControlLabel">
               <%= resMgr.getText("SystemOverview.HowDoIMove", TradePortalConstants.TEXT_BUNDLE) %>
            </p>
            <p class="BodyText">
               <%= resMgr.getText("SystemOverview.MoveBetween", TradePortalConstants.TEXT_BUNDLE) %>
              <br>
              <br>
            </p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" nowrap class="Subnav" height="35"><%= resMgr.getText("SystemOverview.NavigationBar", TradePortalConstants.TEXT_BUNDLE) %></td>
                <td align="center" valign="middle" nowrap class="Subnav" width="10" height="35">&nbsp;</td>
                <td align="center" valign="middle" height="35" nowrap>
                  <p class="BodyText"><%= resMgr.getText("SystemOverview.YouUse", TradePortalConstants.TEXT_BUNDLE) %></p>
                </td>
                <td width="100%" align="center" valign="bottom" nowrap height="35">&nbsp;</td>
                <td align="center" valign="bottom" height="35">&nbsp;</td>
                <td align="center" valign="bottom" height="35">&nbsp;</td>
                <td width="10" height="35">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" nowrap class="Subnav" colspan="3"><img src="<%= brandedImagePath + leftCap%>" width="12" height="34"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image101','','<%= brandedImagePath + messagesImgRO%>',1)"><img name="Image101" border="0" src="<%= brandedImagePath + messagesImg%>"  alt="Messages"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image151','','<%= brandedImagePath + transactionsImgRO%>',1)"><img name="Image151" border="0" src="<%= brandedImagePath + transactionsImg%>" ></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image161','','<%= brandedImagePath + reportsImgRO%>',1)"><img name="Image161" border="0" src="<%= brandedImagePath + reportsImg%>" ></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image171','','<%= brandedImagePath + refdataImgRO%>',1)"><img name="Image171" border="0" src="<%= brandedImagePath + refdataImg%>" ></a></td>
                <td width="100%" background="<%= brandedImagePath + barFill%>" align="center" valign="bottom" nowrap>&nbsp;</td>
                <td align="center" valign="bottom"><img src="<%= brandedImagePath + rightCap%>" width="18" height="34"></td>
                <td width="10">&nbsp;</td>
              </tr>
            </table>
            <br>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td nowrap>
                  <p class="Subnav">
                    <%= resMgr.getText("SystemOverview.LinksHeading", TradePortalConstants.TEXT_BUNDLE) %>
                  </p>
                </td>
                <td width="10" nowrap>&nbsp;</td>
                <td width="100%">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td width="350" nowrap align="left" valign="top">
                  <p class="BodyText">
                     <%= resMgr.getText("SystemOverview.LinkAppears", TradePortalConstants.TEXT_BUNDLE) %>
                  </p>
                </td>
                <td>&nbsp;</td>
                <td align="left" valign="top">
                  <p class="BodyText"><img src="<%= resMgr.getText("SystemOverview.links", TradePortalConstants.TEXT_BUNDLE)%>" width="309" height="111"></p>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <br>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td nowrap>
                  <p class="Subnav">
                     <%= resMgr.getText("SystemOverview.ActionButtons", TradePortalConstants.TEXT_BUNDLE) %>
                  </p>
                </td>
                <td width="10" nowrap>&nbsp;</td>
                <td width="100%" class="ColorGrey" height="30">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10" nowrap>&nbsp;</td>
                      <td><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image22','','/portal/images/<%= resMgr.getText("common.AuthorizeGreyImgRO", TradePortalConstants.TEXT_BUNDLE)%>',1)"><img name="Image22" border="0" src="/portal/images/<%= resMgr.getText("common.AuthorizeGreyImg", TradePortalConstants.TEXT_BUNDLE)%>" ></a></td>
                      <td width="10" nowrap>&nbsp;</td>
                      <td><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image23','','/portal/images/<%=  resMgr.getText("common.RouteGreyImgRO", TradePortalConstants.TEXT_BUNDLE)%>',1)"><img name="Image23" border="0" src="/portal/images/<%=  resMgr.getText("common.RouteGreyImg", TradePortalConstants.TEXT_BUNDLE)%>" ></a></td>
                      <td width="10" nowrap>&nbsp;</td>
                      <td><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image24','','/portal/images/<%= resMgr.getText("common.DeleteImgRO", TradePortalConstants.TEXT_BUNDLE)%>',1)"><img name="Image24" border="0" src="/portal/images/<%= resMgr.getText("common.DeleteImg", TradePortalConstants.TEXT_BUNDLE)%>" ></a></td>
                      <td width="10" nowrap>&nbsp;</td>
                      <td><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image21','','/portal/images/<%=  resMgr.getText("common.SaveCloseImgRO", TradePortalConstants.TEXT_BUNDLE)%>',1)"><img name="Image21" border="0" src="/portal/images/<%=  resMgr.getText("common.SaveCloseImg", TradePortalConstants.TEXT_BUNDLE)%>" ></a></td>
                      <td width="100%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="left" height="40" valign="bottom">
                  <p class="BodyText">
                     <%= resMgr.getText("SystemOverview.ClickingActionButton", TradePortalConstants.TEXT_BUNDLE) %>
                  </p>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>


