<%--
*******************************************************************************
                            Add Invoice Page

  Description:  Test page to test Portal Session Sync.


*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>

<%
String version = resMgr.getText("version", "TradePortalVersion");
if (version.equals("version")) {
  version = resMgr.getText("Footer.UnknownVersion", 
                           TradePortalConstants.TEXT_BUNDLE);
}
//M Eerupula 03/21/2013 Rel 8.2 T3600001495 version is encrypted consistently to fix the URL difference for cache busting i.e.,the version is same on all included pages 
version = EncryptDecrypt.bytesToBase64String(version.getBytes());
//RPasupulati geting first 4 char's from encripted version String IR no T36000017311 Starts.
//version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey());
version = version.substring(0, 4);
String loginUserLocale = resMgr.getResourceLocale() != null ? resMgr.getResourceLocale() : "en_US";
%>

<%-- ********************* HTML for page begins here *********************  --%>

<input type=button id = "testAjaxCall" value="Click Me" />			
<%--configure dojo. must be done before dojo loaded. in separate file to avoid loading every time--%>
<%--note our simple cache busting mechanism of including version info to the url--%>
<%-- RKAZI RELEASE 9.1 - DOJO - MINIFICATION - 08/29/2014 - Start - Added new js files as src and moved dojoparse.js to end --%>
<script src="<%=userSession.getdojoJsPath()%>/dojoconfig.js?<%= version %>"></script>
<!-- 01/27/2011 Prateep Gedupudi Load dojo with login locale from user session. Added locale:loginUserLocale --> 
<script src='<%=userSession.getdojoJsPath()%>/dojo/dojo.js?<%= version %>' data-dojo-config="async: 1, locale: '<%=loginUserLocale %>', cacheBust: '<%=version%>'" ></script>
<script src="<%=userSession.getdojoJsPath()%>/dojo/dojox.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/t360/t360.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/t360/t360-widgets.js?<%= version %>"></script>

<%-- include common javascript libraries --%>
<%--!!!!!!include these temporarily. they are pass throughs to actual functions--%>
<script src="<%=userSession.getdojoJsPath()%>/common.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/dialog.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/datagrid.js?<%= version %>"></script>
<%-- define datagrid formatters.
     this is temporary until all grids are loaded with t360/datagrid rather than js/datagrid--%>
<script src="<%=userSession.getdojoJsPath()%>/datagridformatters.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/t360/partySearch.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/t360/checkSessionTimeOut.js?<%= version %>"></script>

<%-- parse the page first thing. in separate file to avoid loading every time--%>
 <script src="<%=userSession.getdojoJsPath()%>/dojoparse.js?<%= version %>"></script>   
 <script lang="text/javscript">
 require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
         function(registry, on, common, ready) {
 	on(document.getElementById("testAjaxCall"),"click" , function(evt){
 		common.callAjaxToUpdate("newTimeOut", 
	    "/portal/logon/SessionSync.jsp",extendTimeOut1);
 		//var newTimeOut = document.getElementById("newTimeOut");
 	});
 	 function extendTimeOut1(){
 		console.log("Session extended");
 	}
 });
 
</script>	
</body>
</html>

