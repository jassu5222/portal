<%--
**********************************************************************************
  All Transactions Home

  Description:
     This page is used as the main Transactions page.

**********************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>




<%

	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, response);
	String gridHtml = "";
	String gridLayout="";

	final String ALL = "common.all";
	final String ALL_WORK = "common.allWork";
	final String MY_WORK = "common.myWork";

	String  userOid        = userSession.getUserOid();
	String  userOrgOid	 = userSession.getOwnerOrgOid();
	String  loginLocale    = userSession.getUserLocale();
	String  loginRights    = userSession.getSecurityRights();
	String  defaultWIPView = userSession.getDefaultWipView();
	String  currentSecurityType = userSession.getSecurityType();
	//build alltran options
	String currentAllTranWork = MY_WORK;
	String currentAllTranGroup = TradePortalConstants.INSTR_GROUP__ALL;
	String currentAllTranInstrType = ALL;
	String currentAllTranStatus = TradePortalConstants.STATUS_ALL;
	DocumentHandler hierarchyDoc = null;
	int totalOrganizations = 0;
	ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
	boolean hasFTBARights = false;
	boolean hasFTDPRights = false;
	session.setAttribute("fromPage", TradePortalConstants.ALL_TRANS_HOME); // DK IR T36000024626 Rel9.0 05/05/2014
	//IR T36000026794 Rel9.0 07/24/2014 starts
	 session.setAttribute("startHomePage", "AllTransactionsHome");
	 session.removeAttribute("fromTPHomePage");
	//IR T36000026794 Rel9.0 07/24/2014 End

	Map searchQueryMap =  userSession.getSavedSearchQueryData();
	Map savedSearchQueryData =null;
	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("AllTransactionsDataView");
	}
	//MEer Rel 8.4 IR-23915

	//Variables needed for the sql statements and the returned number of rows.
	  CorporateOrganizationWebBean org1  = null;
	  try {
	    org1 = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	    org1.getById(userOrgOid);

	  }
	  catch(Exception e){
	    System.out.println("************** Unable to build the user object because" +
	      " the userOid on the UserSession is null, which caused the" +
	      " getDataFromAppserver to fail!  ***************");
	  }

		// Changes by Pavani - BMO Issue 80 - IR T36000011363 BEGIN. For users having rights to Payment Transactions, certain statuses
		// in All Transactions Status dropdown should be displayed, otherwise not.
	  	hasFTBARights = (TradePortalConstants.INDICATOR_YES.equals(org1.getAttribute("allow_xfer_btwn_accts")) ||
	          TradePortalConstants.INDICATOR_YES.equals(org1.getAttribute("allow_xfer_btwn_accts_panel")) ) &&
	         SecurityAccess.hasRights(loginRights,SecurityAccess.TRANSFER_CREATE_MODIFY);
		hasFTDPRights = (TradePortalConstants.INDICATOR_YES.equals(org1.getAttribute("allow_domestic_payments")) ||
	          TradePortalConstants.INDICATOR_YES.equals(org1.getAttribute("allow_domestic_payments_panel")) ) &&
	         SecurityAccess.hasRights(loginRights,SecurityAccess.DOMESTIC_CREATE_MODIFY);

		// Changes by Pavani - BMO Issue 80 - IR T36000011363 END.


        //cquinton 1/18/2013 remove old close action behavior

	if (!TradePortalConstants.ADMIN.equals(currentSecurityType)) {
		  StringBuffer sqlQuery = new StringBuffer();
		    // This is the query used for populating the option drop down list;
		    // it retrieves all active child organizations that belong to the user's
		    // org in addition to the user's org itself.  It is used by both the mail
		    // and notification.
		    sqlQuery.append("select organization_oid, name");
		    sqlQuery.append(" from corporate_org");
		    sqlQuery.append(" where activation_status = ?");
		    sqlQuery.append(" start with organization_oid = ?");
		    sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
		    sqlQuery.append(" order by ");
		    sqlQuery.append(resMgr.localizeOrderBy("name"));
		    // jgadela 04/11/2014 R90 IR T36000026319 - SQL FIX
		    Object sqlParams[] = new Object[2];
		    sqlParams[0] = TradePortalConstants.ACTIVE;
		    sqlParams[1] = userOrgOid;
		    hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams);
		    totalOrganizations = hierarchyDoc.getFragments("/ResultSetRecord").size();
	}

	 // IR NIUN012952035
	 	String selectedTranWork = "";

	 StringBuffer dropdownOptions = new StringBuffer();
	 StringBuffer prependText = new StringBuffer();
		prependText.append(resMgr.getText("PendingTransactions.WorkFor", TradePortalConstants.TEXT_BUNDLE));
		prependText.append(" ");

	 	String defaultValue = "";
	    String defaultText  = "";
	    String selectedWorkflow = "";
	 
	    // KMehta IR-T36000041657 Rel 9500 on 17 Mar 2016 Change Start

	    if (selectedWorkflow == null){
	     selectedWorkflow = (String) session.getAttribute("workflow");	
	          }
	 
	    if (selectedWorkflow != null)
	      {
	        
		            if (defaultWIPView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
		            {
		               selectedWorkflow = userOrgOid;
		               
		            }
		            else if (defaultWIPView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
		            {
		                  if(totalOrganizations > 1){
		            	      selectedWorkflow = ALL_WORK;
		                     }else{
		                	selectedWorkflow = userOrgOid;
		            	     }
		            }
		            else
		            {
		                 selectedWorkflow = MY_WORK;
		             }
		        selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
		          
		      }
    // KMehta IR-T36000041657 Rel 9500 on 17 Mar 2016 Change 
	      session.setAttribute("workflow", selectedWorkflow);
          selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
	    if(!userSession.hasSavedUserSession())
	     {
	    defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
	       defaultText = MY_WORK;

	       dropdownOptions.append("<option value=\"");
	       dropdownOptions.append(defaultValue);
	       dropdownOptions.append("\"");

	       if (selectedWorkflow.equals(defaultText))
	       {
	          dropdownOptions.append(" selected");
	       }

	       dropdownOptions.append(">");
	       dropdownOptions.append(resMgr.getText( defaultText, TradePortalConstants.TEXT_BUNDLE));
	       dropdownOptions.append("</option>");
	       selectedTranWork = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
	     }

        // If the user has rights to view child organization work, retrieve the
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(loginRights,
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
           dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc,
                                                               "ORGANIZATION_OID", "NAME",
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));
           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
              dropdownOptions.append("<option value=\"");
              dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              dropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 dropdownOptions.append(" selected");
              }

              dropdownOptions.append(">");
              dropdownOptions.append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
              dropdownOptions.append("</option>");
              selectedTranWork = EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey());
           }
        }
        else
        {  dropdownOptions.append("<option value=\"");
           dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
           dropdownOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
              dropdownOptions.append(" selected");
           }

           dropdownOptions.append(">");
           dropdownOptions.append(prependText.toString());
           dropdownOptions.append(userSession.getOrganizationName());
           dropdownOptions.append("</option>");
           selectedTranWork = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
        }
	 //IR NIUN012952035

	StringBuffer allTranGroups = new StringBuffer();
	  allTranGroups.append("<option value='").
	    append(TradePortalConstants.INSTR_GROUP__ALL).
	    append("'>").
	    append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__ALL, loginLocale)).
	    append("</option>");

	  //RPasupulati #IR T36000014543 Starts.
	  //adding condition,If user have access to this instrument group then only it will display
      if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_TRADE_INSTRUMENTS)){
  		 allTranGroups.append("<option value='").
         append(TradePortalConstants.INSTR_GROUP__TRADE).
         append("'>").
         append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__TRADE, loginLocale)).
         append("</option>");
      }

      if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_PAYMENT_INSTRUMENTS)){
         allTranGroups.append("<option value='").
         append(TradePortalConstants.INSTR_GROUP__PAYMENT).
         append("'>").
	     append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__PAYMENT, loginLocale)).
         append("</option>");
      }

      if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DIRECT_DEBIT_INSTRUMENTS)){
         allTranGroups.append("<option value='").
         append(TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT).
         append("'>").
         append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT, loginLocale)).
         append("</option>");
      }

      if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS)){
         allTranGroups.append("<option value='").
         append(TradePortalConstants.INSTR_GROUP__RECEIVABLES).
         append("'>").
         append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__RECEIVABLES, loginLocale)).
         append("</option>");
                  }
    //RPasupulati #IR T36000014543 Ends.
    
    //IR 29337 start
	if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYABLES_TRANSACTIONS)){
         allTranGroups.append("<option value='").
         append(TradePortalConstants.INSTR_GROUP__PAYABLES).
         append("'>").
         append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__PAYABLES, loginLocale)).
         append("</option>");
      }
	 //IR 29337 start
	 
	  StringBuffer allTranInstrTypes = new StringBuffer();
	  allTranInstrTypes.append("<option value='").append(ALL).append("'").
	    append(" selected >").
	    append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
	    append("</option>");
	  //popupate the transaction type drop down with all instrument types based on what the user can see
	  Vector viewableInstruments = Dropdown.getViewableInstrumentTypes(formMgr,
	    new Long(userOrgOid), loginRights, userSession.getOwnershipLevel(), new Long(userOid), "allTranInstrTypes");
	  String viewableInstrTypes = Dropdown.getInstrumentList("",  //selected instr type
	    loginLocale, viewableInstruments );
	  allTranInstrTypes.append(viewableInstrTypes);





	  StringBuffer allTranStatus = new StringBuffer();
	  allTranStatus.append("<option value='").append(ALL).append("'").
	    append(" selected >").
	    append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
	    append("</option>");
	  allTranStatus.append("<option value='").
	    append(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED).
	    append("'>").
	    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED, loginLocale)).
	    append("</option>");
	  allTranStatus.append("<option value='").
	    append(TradePortalConstants.TRANS_STATUS_AUTHORIZED).
	    append("'>").
	    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZED, loginLocale)).
	    append("</option>");
	  allTranStatus.append("<option value='").
	    append(TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK).
	    append("'>").
	    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK, loginLocale)).
	    append("</option>");

	  //Sandeep 02/28/2013 Comment -Start
      //Removing Delete option from Status dropdown box. This Code change is Per Kim/Clay as it is a BMO Issue
      //allTranStatus.append("<option value='").
	    //append(TradePortalConstants.TRANS_STATUS_DELETED).
	    //append("'>").
	    //append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_DELETED, loginLocale)).
	    //append("</option>");
      //Sandeep 02/28/2013 Comment -End

	  if(hasFTBARights || hasFTDPRights){
		  allTranStatus.append("<option value='").
		    append(TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT).
		    append("'>").
		    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT, loginLocale)).
		    append("</option>");
	  }
	  allTranStatus.append("<option value='").
	    append(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK).
	    append("'>").
	    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK, loginLocale)).
	    append("</option>");
	  allTranStatus.append("<option value='").
	    append(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE).
	    append("'>").
	    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE, loginLocale)).
	    append("</option>");
	  allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED, loginLocale)).
        append("</option>");
	  allTranStatus.append("<option value='").
	    append(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK).
	    append("'>").
	    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK, loginLocale)).
	    append("</option>");
	//jgadela Rel-8.3 CR-821 Dev 06/13/2013 - Start
      allTranStatus.append("<option value='").
        append(TradePortalConstants.STATUS_READY_TO_CHECK).
        append("' >").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.STATUS_READY_TO_CHECK, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.STATUS_REPAIR).
        append("' >").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.STATUS_REPAIR, loginLocale)).
        append("</option>");
     //jgadela Rel-8.3 CR-821 Dev 06/13/2013 - End

	  if(hasFTBARights || hasFTDPRights){
		  allTranStatus.append("<option value='").
		    append(TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT).
		    append("'>").
		    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT, loginLocale)).
		    append("</option>");
		 allTranStatus.append("<option value='").
	     	append(TradePortalConstants.TRANS_STATUS_VERIFIED).
	     	append("'>").
	     	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_VERIFIED, loginLocale)).
	     	append("</option>");
	     allTranStatus.append("<option value='").
	     	append(TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX).
	     	append("'>").
	     	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX, loginLocale)).
	     	append("</option>");
	     allTranStatus.append("<option value='").
	     	append(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED).
	     	append("'>").
	     	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED, loginLocale)).
	     	append("</option>");
	     allTranStatus.append("<option value='").
	     	append(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE).
	     	append("'>").
	     	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE, loginLocale)).
	    	append("</option>");
	  }
	  allTranStatus.append("<option value='").
	    append(TradePortalConstants.TRANS_STATUS_STARTED).
	    append("'>").
	    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_STARTED, loginLocale)).
	    append("</option>");
	  userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

  //cquinton 1/18/2013 set return page
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); //to keep it correct
  }
  else {
    userSession.addPage("goToAllTransactionsHome");
  }
%>


<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/transactions/TransactionCleanup.jsp"/>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="allTradeTranHome"/>
</jsp:include>

<div class="pageMain">
<div class="pageContent">
			<div class="secondaryNav">
              <div class="secondaryNavTitle">
                <%=resMgr.getText("SecondaryNavigation.AllTransactions", TradePortalConstants.TEXT_BUNDLE)%>
              </div>
              <div class="secondaryNavHelp" id="secondaryNavHelp">
                <%=OnlineHelp.createContextSensitiveLink("customer/all_transactions.htm",  resMgr, userSession)%>
              </div>
              <%=widgetFactory.createHoverHelp("secondaryNavHelp", "HelpImageLinkHoverText") %>
              <div style="clear:both;"></div>
            </div>

<div class="formContentNoSidebar">

<%-- Added by Sandeep 12/06/2012 - Start --%>
<%--As per the design change a search button and a new field Instrument Id is added--%>
   <div class="gridSearch">
    <div class="searchHeader">
      <span class="searchHeaderCriteria">
        <%= widgetFactory.createSearchSelectField("AllTranWorkDialog","AllTransactions.Show", "",
        		dropdownOptions.toString(), " onChange='searchAllTran()'" )%>

        <%=widgetFactory.createSearchSelectField( "AllTranStatusDialog", "AllTransactions.criteria.Status", "",
          allTranStatus.toString(), " class=\"char12\" onChange='searchAllTran()'" )%>
     
     <%if(userSession.isCustNotIntgTPS()){ //IR-45225 Rel 935 10/30/2015 %>    
       	<%= widgetFactory.createSearchTextField("AllTranBankInstrumentId","AllTransactions.BankInstrumentID","30",
	      		"class=\"char11\" onKeydown='searchAllTranOnEnter(event, \"AllTranBankInstrumentId\");'")%>   
      <%}%>
      </span>
      <span class="searchHeaderActions">
        <jsp:include page="/common/gridShowCount.jsp">
          <jsp:param name="gridId" value="allTranGrid" />
        </jsp:include>

        <span>
	      	<button data-dojo-type="dijit.form.Button" type="button" id="AllTransSearchButton">
	           	<%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	           	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	        		searchAllTran();return false;
	       		</script>
		    </button>
		</span>
	    <%=widgetFactory.createHoverHelp("AllTransSearchButton","SearchHoverText") %>

        <span id="allTranRefresh" class="searchHeaderRefresh"></span>
        <%=widgetFactory.createHoverHelp("allTranRefresh", "RefreshImageLinkHoverText") %>

        <span id="allTranGridEdit" class="searchHeaderEdit"></span>
        <%=widgetFactory.createHoverHelp("allTranGridEdit", "CustomizeListImageLinkHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>
  </div>

    <div class="gridSearch">
    	<div class="searchHeader">
	      <span class="searchHeaderCriteria">
	      	<%-- SSikhakolli - Rel-8.3 IR#T36000021245 on 10/01/2013 - Instrument Type has been moved from the above div to here--%>		
	        <%= widgetFactory.createSearchSelectField( "AllTranInstrTypeDialog", "AllTransactions.criteria.InstrumentType", "",
          			allTranInstrTypes.toString(), " class=\"char16\" onChange='searchAllTran()'") %>
          			
	        <%= widgetFactory.createSearchSelectField( "AllTranGroupDialog", "AllTransactions.criteria.InstrumentGroup", "",
	          		allTranGroups.toString(), " class=\"char10\" onChange='searchAllTran()'" )%>

	        <%= widgetFactory.createSearchTextField("AllTranInstrumentId","AllTransactions.InstrumentID","30",
	        		"class=\"char11\" onKeydown='searchAllTranOnEnter(event, \"AllTranInstrumentId\");'")%>
	      </span>
      <div style="clear:both;"></div>
    </div>
   </div>
<%--    Added by Sandeep 12/06/2012 - End --%>
<%
  gridHtml = dgFactory.createDataGrid("allTranGrid","AllTransactionsDataGrid",null);
  gridLayout = dgFactory.createGridLayout("allTranGrid", "AllTransactionsDataGrid");
%>
  <%= gridHtml %>


</div>
</div>
</div>


<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>

<%--the following script loads the datagrid.  note that this dialog
    is a DialogSimple to allow scripts to execute after it loads on the page--%>
<script type='text/javascript'>
	require(["dojo/aspect","dijit/registry","dojo/ready","t360/OnDemandGrid", "dojo/dom-construct","dojo/dom"], function(aspect,registry,ready, onDemandGrid, domConstruct, dom){
		ready(function(){
			var gridLayout = <%=gridLayout%>;
			var work = "<%=selectedTranWork%>";
		 	<%--
		if(work==''){
				work = registry.byId("AllTranWorkDialog").get('value');
			}
		--%>
			var savedSort = savedSearchQueryData["sort"];
		   	if("" == savedSort || null == savedSort || "undefined" == savedSort){
		        savedSort = '0';
		     }
		  	var allTranWorkSelect = savedSearchQueryData["work"];
         	var allTranGroupSelect = savedSearchQueryData["grp"];
         	var allTranInstrTypeSelect = savedSearchQueryData["iType"];
         	var allTranStatusSelect = savedSearchQueryData["status"];
         	var instrumentId = savedSearchQueryData["instrumentId"];
         	
         	var bankInstrumentId = "";
         	<% if(userSession.isCustNotIntgTPS()){ %>
         		bankInstrumentId = savedSearchQueryData["bankInstrumentId"];
         	<%}%>
         	
           	if("" == allTranWorkSelect || null == allTranWorkSelect || "undefined" == allTranWorkSelect){
           		<%--allTranWorkSelect="<%=selectedTranWork%>";--%>
           		allTranWorkSelect= registry.byId("AllTranWorkDialog").get('value');
           	}
      		 else{
      			<%-- PMitnala IR#T36000020839 Rel 8.3. Checking if the store has the value that is being set --%>
      			if(registry.byId("AllTranWorkDialog").store.query({value:allTranWorkSelect}).length>0){
      	  			registry.byId("AllTranWorkDialog").set('value',allTranWorkSelect);
      		    }
      		 }
      			
           	if("" == allTranWorkSelect || null == allTranWorkSelect)
           		allTranWorkSelect = dijit.byId("AllTranWorkDialog").value;

           	if("" == allTranGroupSelect || null == allTranGroupSelect || "undefined" == allTranGroupSelect){
        		allTranGroupSelect=dijit.byId("AllTranGroupDialog").value;
           	}
      		 else{
      			registry.byId("AllTranGroupDialog").set('value',allTranGroupSelect);
      		 }
           	if("" == allTranInstrTypeSelect || null == allTranInstrTypeSelect || "undefined" == allTranInstrTypeSelect){
           		allTranInstrTypeSelect=dijit.byId("AllTranInstrTypeDialog").value;
           	}
      		 else{
      			registry.byId("AllTranInstrTypeDialog").set('value',allTranInstrTypeSelect);
      			}
        	if("" == allTranStatusSelect || null == allTranStatusSelect || "undefined" == allTranStatusSelect){
        		allTranStatusSelect=dijit.byId("AllTranStatusDialog").value;
        	}
      		 else{
      			registry.byId("AllTranStatusDialog").set('value',allTranStatusSelect);
      		 }
        	if("" == allTranStatusSelect || null == allTranStatusSelect){
        		allTranStatusSelect = "common.all";
        	}
        	<% if(userSession.isCustNotIntgTPS()){ %>
        		if("" == bankInstrumentId || null == bankInstrumentId || "undefined" == bankInstrumentId){
        			bankInstrumentId=dom.byId("AllTranBankInstrumentId").value;   
        		}
        		else{
           			dom.byId("AllTranBankInstrumentId").value = bankInstrumentId;
           		 }
        	<%}%>
        
        	if("" == instrumentId || null == instrumentId || "undefined" == instrumentId){
        		instrumentId=dom.byId("AllTranInstrumentId").value;
        	}
      		 else{
      			dom.byId("AllTranInstrumentId").value = instrumentId;
      		 }

			<%--  Prateep Gedupudi 24/02/2013 Added confInd param for loading instruments based on confInd for loggedin user  --%>
			<%--MEer Rel 8.4 IR-23915--%>
		<%-- 	var initSearchParms = "work="+work+"&confInd=<%=confInd%>&status=common.all"; --%>
			<%--var initSearchParms = "confInd=<%=confInd%>"; --%>
			var initSearchParms ;
			<% if(userSession.isCustNotIntgTPS()){ %>
			 	initSearchParms = "bankInstrumentId="+bankInstrumentId+"&instrumentId="+instrumentId+"&work="+allTranWorkSelect+"&grp="+allTranGroupSelect+"&iType="+allTranInstrTypeSelect+"&status="+allTranStatusSelect;
			<%}else{%>
			 	initSearchParms = "instrumentId="+instrumentId+"&work="+allTranWorkSelect+"&grp="+allTranGroupSelect+"&iType="+allTranInstrTypeSelect+"&status="+allTranStatusSelect;
			<%}%>
			 
			<%--  MEerupula Rel 8.3 T36000021433/20624 Made Party column in the grid non-sortable for sql tuning.  STARTS HERE  --%>
			var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("AllTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
			var allTranGrid = onDemandGrid.createOnDemandGrid("allTranGrid", viewName,gridLayout, initSearchParms,savedSort); 
			
			
			<%-- by Sandeep - 01/11/2013 - IR T36000009976 - Start --%>
			<%-- setting All_Work/My_Work as a selected option of the 'show' box. --%>
			<%-- registry.byId("AllTranWorkDialog").set('value', work);  //commented for IR NIUN012952035 --%>
			<%-- by Sandeep - 01/11/2013 - IR T36000009976 - End --%>
			<%--  making party column sortable IR T36000034285 RPasupulati
			var fieldName="Party";
			
		    var index = -1;        
		    allTranGrid.canSort = function(col){    
		        index =t360grid.getColumnIndexWithName(allTranGrid, fieldName); 
		              if(Math.abs(col) === index) { return false; }
		                else{return true; }
		        };  --%>
		  <%-- MEerupula Rel 8.3 T36000021433/20624 Made Party column in the grid non-sortable for sql tuning. ENDS HERE --%>
			
			
		});
	});

	 <%-- Added by Sandeep 12/06/2012 - Start --%>
	function searchAllTranOnEnter(event, fieldId){
		<%-- 
	  	 *The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
	  	  --%>
	  	<%-- By Sandeep - 01/11/2013 - Start --%>

	  	<%-- 
	  	 *	Commented the below code and added two lines below this commented code
	  	 *	As Event is triggering multiple times I removed the 'on' function and
	  	 *	handeling the event directly by passing 'event' as a perameter from calling place
	  	  --%>

	  	<%-- 
	  	require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldId), "keypress", function(event) {
		        if (event && event.keyCode == 13) {
		        	dojo.stopEvent(event);
		        	searchAllTran();
		        }
		    });
		});
		 --%>

	  	if (event && event.keyCode == 13) {
        	searchAllTran();
        }
		<%-- By Sandeep - 01/11/2013 - End --%>
	}
	 <%-- Added by Sandeep 12/06/2012 - End --%>

	function searchAllTran() {
	  require(["dijit/registry","dojo/dom","t360/OnDemandGrid", "dojo/dom-construct","dojo/domReady!"],
	      function(registry,dom, onDemandGrid, domConstruct){
	      var searchParms = "";
	      var allTranWorkSelect = dijit.byId("AllTranWorkDialog");
	      var allTranWorkOid = allTranWorkSelect.get('value');
	      if ( allTranWorkOid && allTranWorkOid.length > 0 ) {
	        searchParms += "work="+allTranWorkOid;
	      }
	      var allTranGroupSelect = dijit.byId("AllTranGroupDialog");
	      var allTranGroup = allTranGroupSelect.get('value');
	      if ( allTranGroup && allTranGroup.length > 0 ) {
	        if ( searchParms && searchParms.length > 0 ) {
	          searchParms += "&";
	        }
	        searchParms += "grp="+allTranGroup;
	      }
	      var allTranInstrTypeSelect = dijit.byId("AllTranInstrTypeDialog");
	      var allTranInstrType = allTranInstrTypeSelect.get('value');
	      if ( allTranInstrType && allTranInstrType.length > 0 ) {
	        if ( searchParms && searchParms.length > 0 ) {
	          searchParms += "&";
	        }
	        searchParms += "iType="+allTranInstrType;
	      }
	      var allTranStatusSelect = dijit.byId("AllTranStatusDialog");
	      var allTranStatus = allTranStatusSelect.get('value');
	      if ( allTranStatus && allTranStatus.length > 0 ) {
	        if ( searchParms && searchParms.length > 0 ) {
	          searchParms += "&";
	        }
	        searchParms += "status="+allTranStatus;
	      }

	     <%-- Added by Sandeep 12/06/2012 - Start --%>
	     <%-- As per the design change a new field Instrument Id is added, --%>
	     <%-- so appending another paramenter to filter data based on Instrument ID also. --%>
	      var instrumentId = dom.byId("AllTranInstrumentId").value;
	      <%--  Prateep Gedupudi 24/02/2013 Added confInd param for loading instruments based on confInd for loggedin user  --%>
	      searchParms += "&instrumentId="+instrumentId;
	      
	      
	      <%-- Added by Sandeep 12/06/2012 - End --%>
	      <% if(userSession.isCustNotIntgTPS()){ %>
	      		var bankInstrumentId = dom.byId("AllTranBankInstrumentId").value;
	      		searchParms += "&bankInstrumentId="+bankInstrumentId;	
	      <%}%>
	      <%--  Prateep Gedupudi 24/02/2013 Added confInd param for loading instruments based on confInd for loggedin user  --%>
	      
	      
	      onDemandGrid.searchDataGrid("allTranGrid", "AllTransactionsDataView", searchParms);
	    });
	}

	require(["dojo/query", "dojo/on", "t360/popup","t360/OnDemandGrid", "dojo/dom-construct", "dojo/domReady!"],
	    function(query, on, t360popup, onDemandGrid, domConstruct){
	  query('#allTranRefresh').on("click", function() {
	    searchAllTran();
	  });
	  query('#allTranGridEdit').on("click", function() {
	    var columns = onDemandGrid.getColumnsForCustomization('allTranGrid');
	    var parmNames = [ "gridId", "gridName", "columns" ];
	    var parmVals = [ "allTranGrid", "AllTransactionsDataGrid", columns ];
	    t360popup.open(
	      'allTranGridEdit', 'gridCustomizationPopup.jsp',
	      parmNames, parmVals,
	      null, null);  <%-- callbacks --%>
	  });
	});
</script>

<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="allTranGrid" />
</jsp:include>

</body>
</html>
