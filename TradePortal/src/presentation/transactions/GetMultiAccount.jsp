<%--
**********************************************************************************
  Trade Transactions Home

  Description:  
     This page is used as the main Transactions page for trade instruments.

**********************************************************************************
--%> 

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>


 
<%
	DomesticPaymentWebBean currentDomesticPayment = beanMgr.createBean(DomesticPaymentWebBean.class, "DomesticPayment");
	String payeeAcctNum = currentDomesticPayment.getAttribute("payee_account_number");

	if (payeeAcctNum == null) {
		payeeAcctNum = "";
	}

	WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
	String partyName = request.getParameter("partyName");
	
	Set accountSet = null;
	boolean displayMultipleAccounts = false;
	String extTag=""; 
	String acctNo = "";
	DocumentHandler acct = null;
	Vector accounts = null;
	boolean isReadOnly = false;
	boolean isTemplate = false;

	  String isTemplateStr = request.getParameter("isTemplate");
	  if ( "true".equals(isTemplateStr) ) {
	    isTemplate = true; 
	  } 
	  
	  String isisReadOnly = request.getParameter("isReadOnly");
	  if ( "true".equals(isisReadOnly) ) {
	    isReadOnly = true;
	  }

	StringBuffer invDetSql = new StringBuffer();
	invDetSql.append("select account_oid, account_number, currency, a_op_bank_org_oid from account WHERE  p_owner_oid = (SELECT PARTY_OID FROM PARTY WHERE NAME= ? )");
	DocumentHandler accountDoc = DatabaseQueryBean.getXmlResultSet(invDetSql.toString(), false, new Object[]{partyName});
	
	if(accountDoc!=null){
		accounts = accountDoc.getFragments("/ResultSetRecord");
		int numAccts = accounts.size();
		
		Debug.debug("Account Choices for Party: " + accountDoc.getFragments("/ResultSetRecord"));
		Debug.debug("Number of account in list is " + numAccts);

		  // Set the showAccts flag that determines if a list of accounts with radio buttons
		  // appears.  They appear if we have a set of accounts (i.e., it's been preloaded into
		  // a acctList variable).  However, in readonly mode, we do not display the list.
		if (numAccts > 0) displayMultipleAccounts = true;
		//if (isReadOnly) displayMultipleAccounts = false;
	}
	
	
	if(displayMultipleAccounts){
		accountSet = new HashSet();
	    for (int i = 0; i < accounts.size(); i++) {
	    	acct = (DocumentHandler) accounts.elementAt(i);
	        accountSet.add(acct.getAttribute("ACCOUNT_NUMBER"));
	    } 
	}
	
	out.println(widgetFactory.createLabel("","DirectDebitInstruction.PayerAccountNumber",false,true,false,"",""));
	if(displayMultipleAccounts){
		out.print("<span class='formItem inline'>");
		out.print(widgetFactory.createRadioButtonField( "AccountSelectType", "AccountSelectType1", "", 
    			"U", !accountSet.contains(payeeAcctNum), isReadOnly, 
    			"onclick=\"javascript:setAccount('EnteredAccount');\"", ""));
		out.print("</span>");
		
		extTag = "onchange=\"javascript:setAccountSelectType('EnteredAccount','0');\"";	
	}else {
 	   extTag = "onChange=\"javascript:setAccount('EnteredAccount');\"\"";
    }
    
    if (accountSet != null && accountSet.contains(payeeAcctNum)) {
 	   acctNo = "";
    }
    else {
 	   acctNo = payeeAcctNum;
    } 
    
    out.println(widgetFactory.createTextField( "EnteredAccount", "", 
    		acctNo, "30", isReadOnly, false, false, extTag, "", ""));
    
    if (displayMultipleAccounts) {
    	out.print("<span class='formItem inline'>");
    	out.print(widgetFactory.createRadioButtonField( "AccountSelectType", "AccountSelectType2", "", 
    			"NU", accountSet.contains(payeeAcctNum), isReadOnly, 
    			"onclick=\"javscript:setAccount('SelectedAccount');\"", ""));
    	out.print("</span>");
    	
		acctNo = (accountSet.contains(payeeAcctNum))? payeeAcctNum : "";
		              
		String options = ListBox.createOptionList(accountDoc, "ACCOUNT_NUMBER", "ACCOUNT_NUMBER",acctNo,userSession.getSecretKey());
		out.println(widgetFactory.createSelectField( "SelectedAccount", "", 
		"--Select Account Number--", options, isReadOnly, false, false, "onchange=\"javascript:setAccountSelectType('SelectedAccount','1');\"", "","" ));
    }
%>
