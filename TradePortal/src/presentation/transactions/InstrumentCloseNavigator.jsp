<%--
*******************************************************************************
                         Instrument Close Navigator Page

  Description:
    Handles cleanup after working with a transaction.  The following actions
  are performed:

  1.  Remove all cached phrase lists that have been created.
  2.  Unlock the instrument.  (Although templates are not locked, we'll
      do an unlock anyway.  No harm done.)
  3.  Unregister the beans we created in the Instrument Navigator page.
  4.  Forward to some page.  This is based on
    After closing the instrument/transaction page, we need to return somewhere.
  We will get a value from the session that will tell us where to go.  If the
  session parameter does not exist we'll go to some home page.

  It is assumed that any page with a link to a transaction page has set the
  "InstrumentCloseAction" value in the session.  This is the only way to
  know which page to return to.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

  // The transaction page has been closed.  Clean up all cached phrase lists.
  formMgr.removeFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);

  // Unlock the instrument.  (Templates are not locked but we attempt to
  // unlock anyway -- no harm done.).
  InstrumentWebBean instrument   = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");

  StringBuffer link = null;
  String instrumentOid = null;
  boolean passInstrumentOid = false;

  String oid = userSession.getUserOid();
  long userOid = 0;
  try {
     userOid = Long.valueOf(oid).longValue();

     Debug.debug("Attempting to unlock instrument for user " + userOid);
     LockingManager.unlockBusinessObjectByUser(userOid, false);
     userSession.setInstrumentLock(false);
  } catch (NumberFormatException e) {
     // unable to convert the instrument oid into a number.
     // skip over the unlock step.
     System.out.println("Error converting instrument " + oid + " into a number.");
  } catch (InstrumentLockException e) {
     System.out.println("Error unlocking instrument for user " + userOid);
     System.out.println("  The error is: " + e.toString());
  } catch (AmsException e) {
     System.out.println("Error unlocking instrument for user " + userOid);
     System.out.println("  The error is: " + e.toString());
  }

  // Unregister the beans we used for this transaction/template. Not all pages coming to
  // this jsp will have a Template web bean registered, so check for its existence first.
  Debug.debug("Unregistering the webbeans");
//Added by dillip NullpointerException
  if(beanMgr.getBean("Transaction")!=null){
	   	beanMgr.unregisterBean("Transaction");
	  }else if(beanMgr.getBean("Instrument")!=null){
	  	beanMgr.unregisterBean("Instrument");
	  }else if (beanMgr.getBean("Instrument")!=null){
	      beanMgr.unregisterBean("Terms");
	  }

  if ((TemplateWebBean) beanMgr.getBean("Template") != null)
  {
     beanMgr.unregisterBean("Template");
  }

  //cquinton 1/18/2013 remove old returnAction stuff

  //cquinton 1/18/2013 add back page logic
  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String returnAction = backPage.getLinkAction();
  //todo: set all the parms not just explicitly instrument_oid...
  SessionWebBean.PageFlowParameter[] parms = backPage.getLinkParameters();
  if ( parms!=null ) {
    for(int i=0; i<parms.length; i++ ) {
      SessionWebBean.PageFlowParameter parm = parms[i];
      if ( parm!=null ) {
        if ( "instrument_oid".equals(parm.getName()) ) {
          instrumentOid = parm.getValue();
          passInstrumentOid = true;
          break;
        }
      }
    }
  }

  NavigationManager navMgr = NavigationManager.getNavMan();
  String nextPage = "";
  nextPage = navMgr.getNextPage(null, returnAction);
  String fromPage = (String)session.getAttribute("fromPage");
  String msgHome = "MessagesHome";  // DK IR T36000019521 Rel8.3 08/13/2013
  // DK IR T36000026794 Rel9.0 05/29/2014 starts
  String fromTPHomePage = (String)session.getAttribute("fromTPHomePage");
  String startHomePage = (String)session.getAttribute("startHomePage");
   if ("TradePortalHome".equals(startHomePage) && TradePortalConstants.TRADE_PORTAL_HOME.equals(fromTPHomePage)){
	  	nextPage = fromTPHomePage;
	  	session.removeAttribute("fromPage");
	  	session.removeAttribute("fromTPHomePage");
	} 
  // DK IR T36000026794 Rel9.0 05/29/2014 ends
  	//Sandeep Rel-8.3 IR#-T36000016484 07/09/2013 - Begin
  	else if("MessagesHome".equals(startHomePage)){ // DK IR T36000019521 Rel8.3 08/13/2013
	  	nextPage = fromPage;
	  	session.removeAttribute("fromPage");
  	}else if (fromPage != null && !"".equals(fromPage) && ("ReportsHome".equals(fromPage) || "AdminReportsHome".equals(fromPage)) ){
	  	nextPage = fromPage;
	  	session.removeAttribute("fromPage");
  	}//Sandeep Rel-8.3 IR#-T36000016484 07/09/2013 - End
  	//Rel 9.3.5 - CR 1029 - assign next page as BankTransactionUpdates
	else if ( fromPage != null && !"".equals(fromPage) && ("BankTransactionUpdatesGrid".equals(fromPage)) ){
	  	nextPage = fromPage;
	  	session.removeAttribute("fromPage");
  	}

  String physicalPage = navMgr.getPhysicalPage(nextPage, request);

  //cquinton 1/18/2013 remove old 'new transaction' stuff

  //NSX CR-564 Rel6.1 01/12/11 Begin
  //clear listview parameter before leaving PaymentPage so it will start new when entering PaymentPage
  session.removeAttribute("DomesticPaymentListView.xml");
  //NSX CR-564 Rel6.1 01/12/11 End

  formMgr.setCurrPage(nextPage);

  // If we should pass the instrument oid as a URL parameter, use the URL that was constructed
  // above so that this oid gets passed to the next jsp
  if (passInstrumentOid == false)
  {
    Debug.debug("ReturnAction=" + returnAction);
    Debug.debug("About to forward to " + physicalPage);
%>
     <jsp:forward page='<%=physicalPage%>' >
        <jsp:param name="returning" value="true" />
     </jsp:forward>

<%
  }
  else
  {
    Debug.debug("ReturnAction=" + returnAction + " with instrument " + instrumentOid);
    Debug.debug("About to forward to " + physicalPage);
%>

     <jsp:forward page='<%=physicalPage%>' >
        <jsp:param name="instrument_oid" value="<%= instrumentOid %>" />
        <jsp:param name="returning" value="true" />
     </jsp:forward>

<%
  }
%>
