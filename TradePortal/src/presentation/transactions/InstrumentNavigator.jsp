<%--
*******************************************************************************
                              Instrument Navigator Page

  Description:
    This is the entry point for ALL transaction pages.  It provides a single
  common, and consistent entry point.  It is used when opening a transaction
  or template, when returning to a transaction page after doing a phrase
  lookup or a party search, or aftr a save or verify.

  This page creates web beans for the instrument, transaction (and the
  template).

  If the page is being entered for initial display of a transaction (or after
  a successful save), a transaction oid is passed in.  If coming from the
  "create a transaction" process, the transaction_oid exists in the
  TRANS_DATA_DOC document.  If coming from one of the instrument listview
  pages, the transaction oid is received as a secure parm.

  We also look at the default doc's error section to determine if an error
  exists.  If so, we will not retrieve from the database but rather use the
  data in the default doc to populate the web beans.  Otherwise we do retrieve
  from the database and populate the web beans that way.

  Next we may need to lock the instrument and make an attempt to do so.  If
  we need to lock and cannot, an error is placed in the error section of the
  default doc (which then gets displayed on the transaction page).

  Finally we use the instrument type and transaction type to redirect to the
  appropriate transaction page.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.*,org.slf4j.Logger,
org.slf4j.LoggerFactory" %>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  boolean getFromDB = false;
  boolean multiPartTabPressed = false;

  String instrumentOid = null;
  String transactionOid;
  String templateOid = "";

  String instrumentType;
  String instrumentStatus;
  String transactionType;
  String transactionStatus;
  boolean isTemplate = false;
  boolean isFromTransTermsFlag = false;
  String transTermsFlag;
  String newTransaction;

  TransactionWebBean     transaction;
  InstrumentWebBean      instrument;
  TemplateWebBean        template;
  HttpSession	theSession    = request.getSession(false);
   	String fromDetailPage = (String)theSession.getAttribute("isFromTransDetailPage");

  //Vshah - 17/07/2012 - Portal Refresh - <BEGIN>
  DocumentHandler doc = formMgr.getFromDocCache();
  newTransaction = doc.getAttribute("/Out/newTransaction");  
  if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(newTransaction)) {
%>
  <%-- including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
  <%-- this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
  	<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>
  	  
<%
  	  	}
  	    //Vshah - 17/07/2012 - Portal Refresh - <END>

  	    transaction    = (TransactionWebBean) beanMgr.getBean("Transaction");
  	    instrument     = (InstrumentWebBean) beanMgr.getBean("Instrument");
  	    template       = (TemplateWebBean) beanMgr.getBean("Template");

  	    if (transaction == null) {
  	       Debug.debug("No transaction bean found, creating");
  	       beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.TransactionWebBean",
  	                             "Transaction", resMgr.getCSDB());
  	       transaction = (TransactionWebBean) beanMgr.getBean("Transaction");
  	       getFromDB = true;
  	    }

  	    if (instrument == null) {
  	       Debug.debug("No instrument bean found, creating");
  	       beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.InstrumentWebBean",
  	                             "Instrument");
  	       instrument = (InstrumentWebBean) beanMgr.getBean("Instrument");
  	       getFromDB = true;
  	    }

  	    if (template == null) {
  	       Debug.debug("No template bean found, creating");
  	       beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.TemplateWebBean",
  	                             "Template");
  	       template = (TemplateWebBean) beanMgr.getBean("Template");
  	       // do not set getFromDB to true!!!
  	    }

  	    //First we need to determine if this flag was passed in,
  	    //Then if it was and it is 'true' then we need to update our flag for use...
  	    //Otherwise it will default to 'false'.
  	    if( request.getParameter("isFromTransTermsFlag") != null ){

  	  	transTermsFlag = EncryptDecrypt.decryptStringUsingTripleDes( request.getParameter("isFromTransTermsFlag"), userSession.getSecretKey() );

  	          if ((!InstrumentServices.isBlank(transTermsFlag)) && (transTermsFlag.equals("true")))
  	  	      isFromTransTermsFlag = true;
  	    }

  	    // If the user is not coming from the terms detail page set the part pressed; otherwise
  	    // clear out the part pressed because if they're coming from the terms detail page
  	    // we always want to display the first tab
  	    if (isFromTransTermsFlag == false)
  	    {
  	      if(request.getParameter("part") != null)
  	      {
  	        session.setAttribute(TradePortalConstants.READONLY_PART, request.getParameter("part")); 
  	        multiPartTabPressed = true;   
  	      }
  	    }
  	    else
  	    {
  	      session.setAttribute(TradePortalConstants.READONLY_PART, ""); 
  	    }

  	// Jaya Mamidala CR-49930 Start
      
    String encryptedtranOid = request.getParameter("notifTransactionOid");
	  String tranOid  = EncryptDecrypt.decryptStringUsingTripleDes(encryptedtranOid, userSession.getSecretKey());	  
	  String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
	    ClientServerDataBridge csdb = resMgr.getCSDB();
        Transaction notifTransaction       = null;
     
       if( StringFunction.isNotBlank(tranOid) )
       {
    	    long ltransOid = Long.parseLong(tranOid.trim());
    		notifTransaction = (Transaction)EJBObjectFactory.createClientEJB(serverLocation, 
 		      "Transaction", ltransOid, csdb);

    		if(!TradePortalConstants.INDICATOR_NO.equals(notifTransaction.getAttribute("unread_flag")) ) 
    		{
				notifTransaction.setAttribute("unread_flag",TradePortalConstants.INDICATOR_NO);
				notifTransaction.save();                                     
		    }   
		 }
   
  	    
  	  // Jaya Mamidala CR-49930 End

  	    // There are two ways we could be passed a transaction oid.  One is from
  	    // the create transaction/template process.  In this case the oid is
  	    // available in the doc cache ( ).  The other way is via a secure page
  	    // parm called oid (as when coming from  Pending Transactions page).
  	    // First check the doc cache and then if not there try the secure parm

  	    // If we are in the transaction area the parameters will be passed in via
  	    // this document
  	    //DocumentHandler doc = formMgr.getFromDocCache();
  	    transactionOid = doc.getAttribute("/Out/Transaction/oid");
  	    
  	    boolean toTransactionSummary = false;
  	    if (!InstrumentServices.isBlank(transactionOid)) {
  	       Debug.debug("doc follows on next line\n" + doc.toString());
  	       templateOid = doc.getAttribute("/Out/Template/oid");
  	       Debug.debug("Found transaction oid in TransactionDataDoc: " + transactionOid);
  	    } else {
  	       String oidValue = null;
  	       if (request.getParameter("instrument_oid") != null) {
  	          toTransactionSummary = true;
  	       }

  	       if(request.getParameter("oid") != null)
  	  	     oidValue = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
  	       int position = -1;
  	       if (oidValue != null) position = oidValue.indexOf("/");
  	       if (position >= 0) {
  	           transactionOid = oidValue.substring(0,position);
  	           templateOid = oidValue.substring(position+1, oidValue.length());
  	       } else {
  	           transactionOid = oidValue;
  	       }
  	       Debug.debug("Found transaction oid from secure parm: " + transactionOid);
  	    }
  	    Debug.debug("The transaction web bean oid is "
  	          + transaction.getAttribute("transaction_oid"));

  	    if (transactionOid != null) {
  	       if (!transactionOid.equals(transaction.getAttribute("transaction_oid"))) {
  	          // If for any reason we've been passed an oid and it doesn't match the
  	          // transaction web bean oid, we will repopulate.
  	          getFromDB = true;
  	          Debug.debug("No match between passed in oid and bean oid so repopulate");
  	       }
  	    } else {
  	       transactionOid = transaction.getAttribute("transaction_oid");
  	    }
  	    Debug.debug("The transaction oid to use is " + transactionOid);

  	    if (!getFromDB) {
  	       // We're still not sure if we should get the data from the database.
  	       // Look at the document: if errors exist or we have a phrase lookup
  	       // or party search section, we don't populate.  Otherwise we've done
  	       // a good update and should repopulate from the database

  	       String maxError = doc.getAttribute("/Error/maxerrorseverity");
  	       if (maxError != null && maxError.compareTo(
  	                String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
  	          Debug.debug("no errors were found");
  	          if ((doc.getDocumentNode("/Out/PhraseLookupInfo") == null)
  	             && (doc.getDocumentNode("/In/PartySearchInfo/PartyOid") == null)) {
  	             Debug.debug("Didn't find phrase or party info");
  	             getFromDB = true;
  	          }
  	       }
  	    }

  	    // We now know the transaction oid to use.

  	    Debug.debug("getFromDB is " + getFromDB);

  	    if (getFromDB) {
  	      Debug.debug("Populating from database");

  	      transaction.setAttribute("transaction_oid", transactionOid);
  	      transaction.getDataFromAppServer();

  	      // test for successful retrieve

  	      instrumentOid = transaction.getAttribute("instrument_oid");
  	      instrument.setAttribute("instrument_oid", instrumentOid);
  	      instrument.getDataFromAppServer();

  	      // test for successful retrieve

  	      String value = instrument.getAttribute("template_flag");
  	      if (value != null && value.equals(TradePortalConstants.INDICATOR_YES)) {
  	         Debug.debug("Retrieving template");
  	         isTemplate = true;

  	         // If we still have a blank template oid and we're dealing with templates, we did not come from 
  	         // the create template process and we did not get one passed in as a URL parameter; the template
  	         // oid should already be set in the template web bean
  	         if (InstrumentServices.isBlank(templateOid))
  	         {
  	            templateOid = template.getAttribute("template_oid");
  	         }

  	         template.setAttribute("template_oid", templateOid);
  	         template.getDataFromAppServer();
  	         // test that data was successfully retrieved
  	      }
  	    }

  	    transactionType = transaction.getAttribute("transaction_type_code");
  	    transactionStatus = transaction.getAttribute("transaction_status");

  	    instrumentType = instrument.getAttribute("instrument_type_code");
  	    instrumentStatus = instrument.getAttribute("instrument_status");

  	    Debug.debug("Instrument Type " + instrumentType);
  	    Debug.debug("Instrument Status " + instrumentStatus);
  	    Debug.debug("Transaction Type " + transactionType);
  	    Debug.debug("Transaction Status " + transactionStatus);

  	    String standbyUsingGuarantee = 
  	                   transaction.getAttribute("standby_using_guarantee_form");
  	    Debug.debug("standby using guarantee flag " + standbyUsingGuarantee);

  	    // Now we need to lock the instrument.
  	    if (!isTemplate) {
  	       // The instrument is not a template, attempt to lock it.
  	       if (InstrumentServices.isEditableTransStatus(transactionStatus)) {
  	          Debug.debug("Transaction is editable, attempting to lock...");
  	          instrumentOid = instrument.getAttribute("instrument_oid");
  	          long theOid = Long.valueOf(instrumentOid).longValue();
  	          long userOid = Long.valueOf(userSession.getUserOid()).longValue();
  	          try {
  	             LockingManager.lockBusinessObject(theOid, userOid, false);
  	             userSession.setInstrumentLock(true);
  	             Debug.debug("Instrument sucessfully locked for this user.");
  	          } catch (InstrumentLockException e) {
  	             // instrument locked by someone else, issue an error that will show
  	             // up on the transaction page
  	             if (e.getUserOid() != userOid) {
  	               Debug.debug("Instrument locked by another user: " + e.getLastName());
  	               MediatorServices medService = new MediatorServices();
  	               medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
  	               medService.getErrorManager().issueError(
  	                                      TradePortalConstants.ERR_CAT_1,
  	                                      TradePortalConstants.INSTRUMENT_LOCKED,
  	                                      e.getFirstName() + " " + e.getLastName() );
  	               medService.addErrorInfo();
  	               doc.setComponent("/Error", medService.getErrorDoc());
  	               formMgr.storeInDocCache("default.doc", doc);
  	             } else {
  	               Debug.debug("Instrument is already locked by this user.");
  	             }
  	          } catch (AmsException e) {
  	             System.out.println("Exception found trying to lock instrument "
  	                                + instrumentOid);
  	             System.out.println("   " + e.toString());
  	          }
  	       }
  	    }

  	    String logicalPage = "";

  	    boolean allowAccess = true;

  	    // By the time we've gotten here, any new transactions have already been created
  	    // so it is safe to reset this session variable to No.
  	     theSession = request.getSession(false);
  	    //ctq - use inNewTransactionArea for transaction sub header
  	    //theSession.setAttribute("inNewTransactionArea", TradePortalConstants.INDICATOR_NO);

  	    // Finally, the instrument and transaction web beans have been populated and we
  	    // are ready to forward to the correct instrument page. First determine which 
  	    // logical page to use. 

  	    // If a mulit-part tab was pressed then forward the user to the appropriate instrument
  	    // transaction page
  	    if (allowAccess) {
  	  	  //rkazi IR RSUL020973906 02/11/2011 START added 2 Transaction statuses to condition
  	    if(( transactionStatus.equals( TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK ) | 
  	  		  transactionStatus.equals( TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT ) |
  	  		  transactionStatus.equals( TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT )) &&
  	       ( isFromTransTermsFlag == false) && (multiPartTabPressed == false)) {
  	  	//rkazi IR RSUL020973906 02/11/2011 END
  	         logicalPage = "Transaction-TermsDetails";

  	         // If the shipmentTabNumber parameter exists then the user has selected a shipment tab
  	         // on an Import DLC Issue Transaction, so set this as the logical page
  	         if (!InstrumentServices.isBlank(request.getParameter("shipmentTabNumber")))
  	           logicalPage = "Transaction-IMP_DLC-ISS";
  	    }
  	    else if (instrumentType.equals(InstrumentType.IMPORT_DLC))
  	    {
  	       if (transactionType.equals(TransactionType.ISSUE)) {
  	          logicalPage = "Transaction-IMP_DLC-ISS";
  	       } 
  	       else if (transactionType.equals(TransactionType.AMEND))
  	       {
  	          logicalPage = "Transaction-IMP_DLC-AMD";
  	       }
  	       else if (transactionType.equals(TransactionType.DISCREPANCY))
  	       {
  	          logicalPage = "Transaction-IMP_DLC-DCR";
  	       }
  	       else if (transactionType.equals(TransactionType.TRANSFER))
  	       {
  	          logicalPage = "Transaction-EXP_DLC-TRN";
  	       }
  	       else if (transactionType.equals(TransactionType.SIR))
	       {
	          logicalPage = "Transaction-IMC-SIR-DBA-SIR-DFP-SIR";
	       }
  	    }
  	    //rkrishna CR 375-D ATP 07/19/2007 Begin
  	    else if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY))
  	    {
  	       if (transactionType.equals(TransactionType.ISSUE)) {
  	  		 Debug.debug(" Inside almost");
  	          logicalPage = "Transaction-ATP-ISS";
  	       } 
  	       else if (transactionType.equals(TransactionType.AMEND))
  	       {
  	          logicalPage = "Transaction-ATP-AMD";
  	       }
  	       else if (transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE))
  	       {
  	          logicalPage = "Transaction-ATP-APR";
  	       }
  	    }
  	    //rkrishna CR 375-D ATP 07/19/2007 End
  	    else if (instrumentType.equals(InstrumentType.EXPORT_COL))
  	    {
  	       if (transactionType.equals(TransactionType.ISSUE))
  	       {
  	  	logicalPage = "Transaction-EXP_COL-ISS";
  	       }
  	       else if ((transactionType.equals(TransactionType.AMEND)) ||
  	   	      (transactionType.equals(TransactionType.TRACER)))
  	       {
  	          logicalPage = "Transaction-EXP_COL-AMD_TRC";
  	       }
  	    }
  	    //Vasavi CR 524 Begin
  	    else if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL))
  	    {
  	       if (transactionType.equals(TransactionType.ISSUE))
  	       {
  	  	logicalPage = "Transaction-EXP_OCO-ISS";
  	       }
  	       else if ((transactionType.equals(TransactionType.AMEND)) ||
  	   	      (transactionType.equals(TransactionType.TRACER)))
  	       {
  	          logicalPage = "Transaction-EXP_OCO-AMD_TRC";
  	       }
  	    }
  	    //Vasavi CR 524 End
  	    else if (instrumentType.equals(InstrumentType.INCOMING_SLC))
  	    {
  	       if (transactionType.equals(TransactionType.DISCREPANCY))
  	       {
  	          logicalPage = "Transaction-INC_SLC-DCR";
  	       }
  	       else if (transactionType.equals(TransactionType.AMEND))
  	       {
  	          logicalPage = "Transaction-SLC-AMD";
  	       }
  	       else if (transactionType.equals(TransactionType.DISCREPANCY))
  	       {
  	          logicalPage = "Transaction-SLC-DCR";
  	       }
  	    }
  	    else if (instrumentType.equals(InstrumentType.EXPORT_DLC))
  	    {
  	       if (transactionType.equals(TransactionType.TRANSFER))
  	       {
  	          logicalPage = "Transaction-EXP_DLC-TRN";
  	       }
  	       else if (transactionType.equals(TransactionType.DISCREPANCY))
  	       {
  	          logicalPage = "Transaction-EXP_DLC-DCR";
  	       }
  	       else if (transactionType.equals(TransactionType.AMEND_TRANSFER))
  	       {
  	          logicalPage = "Transaction-EXP_DLC-ATR";
  	       }
  	       else if (transactionType.equals(TransactionType.ASSIGNMENT))
  	       {
  	          logicalPage = "Transaction-EXP_DLC-ASN";
  	       }
  	    }
  	    else if ((instrumentType.equals(InstrumentType.AIR_WAYBILL)) &&
  	             (transactionType.equals(TransactionType.RELEASE)))
  	    {
  	       logicalPage = "Transaction-AIR-REL_SHP-ISS";
  	    }
  	    else if ((instrumentType.equals(InstrumentType.SHIP_GUAR)) &&
  	             (transactionType.equals(TransactionType.ISSUE)))
  	    {
  	       logicalPage = "Transaction-AIR-REL_SHP-ISS";
  	    }
  	    else if (instrumentType.equals(InstrumentType.GUARANTEE))
  	    {
  	       if (transactionType.equals(TransactionType.ISSUE))
  	       {
  	          logicalPage = "Transaction-GUAR-ISS";
  	       }
  	       else if (transactionType.equals(TransactionType.AMEND))
  	       {
  	          logicalPage = "Transaction-GUAR-AMD";
  	       }
  	       else if (transactionType.equals(TradePortalConstants.CONVERTED_TRANSACTION_PAYMENT))
  	       {
  	          logicalPage = "Transaction-GUAR-PAY";
  	       }
  	    }
  	    else if (instrumentType.equals(InstrumentType.STANDBY_LC))
  	    {
  	       // For SLC, if the standby using guarantee flag is yes, we actually want
  	       // to use the guarantee form to open this up.
  	       if (standbyUsingGuarantee.equals(TradePortalConstants.INDICATOR_YES)) {
  	          if (transactionType.equals(TransactionType.ISSUE))
  	          {
  	             logicalPage = "Transaction-GUAR-ISS";
  	          }
  	          else if (transactionType.equals(TransactionType.AMEND))
  	          {
  	             logicalPage = "Transaction-GUAR-AMD";
  	          }
  	       } 
  	       else if (transactionType.equals(TransactionType.ISSUE))
  	       {
  	          logicalPage = "Transaction-SLC-ISS";
  	       }
  	       else if (transactionType.equals(TransactionType.AMEND))
  	       {
  	          logicalPage = "Transaction-SLC-AMD";
  	       }
  	       else if (transactionType.equals(TransactionType.DISCREPANCY))
  	       {
  	          logicalPage = "Transaction-SLC-DCR";
  	       }
  	    }
  	    else if (instrumentType.equals(InstrumentType.LOAN_RQST))  	             
  	    {
  	          if ( transactionType.equals(TransactionType.ISSUE) ) {
  	        	logicalPage = "Transaction-LRQ-ISS";
  	          }
  	          else if ( transactionType.equals(TransactionType.SIM))
  	          {
  	        	logicalPage = "Transaction-LRQ-SIM"; 
  	          }
  	          else if ( transactionType.equals(TransactionType.SIR))
	          {
	        	logicalPage = "Transaction-LRQ-SIR"; 
	          }
  	       
  	    }
  	    else if ((instrumentType.equals(InstrumentType.FUNDS_XFER)) &&
  	             (transactionType.equals(TransactionType.ISSUE)))
  	    {
  	       if (toTransactionSummary) {
  	  	logicalPage = "InstrumentSummary";
  	       } else {
  	  	logicalPage = "Transaction-FTRQ-ISS";
  	       }
  	    }
  	    //IAZ CM 10/29/08 Starts
  	    //else if ((instrumentType.equals(InstrumentType.DOM_PMT)) &&
  	    else if ((instrumentType.equals("FTDP")) &&
  	             (transactionType.equals(TransactionType.ISSUE)))  
  	    {
  	      if (toTransactionSummary) {
  	       logicalPage = "InstrumentSummary";
  	      } else {
  	       logicalPage = "Transaction-FTDP-ISS";
  	      }
  	    }
  	    //IAZ CM 10/29/08 Ends
  	    else if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
  	    {
  	       if (transactionType.equals(TransactionType.ISSUE)) {
  	          logicalPage = "Transaction-RQA-ISS";
  	       } 
  	       else if (transactionType.equals(TransactionType.AMEND))
  	       {
  	          logicalPage = "Transaction-RQA-AMD";
  	       }
  	       else if (transactionType.equals(TransactionType.DISCREPANCY))
  	       {
  	          logicalPage = "Transaction-RQA-DCR";
  	       }
  	    }
  	  //-- CR-451 NShrestha 10/27/2008 Begin --
  	    else if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS))
  	    {
  	       if (toTransactionSummary) {
  	          logicalPage = "InstrumentSummary";
  	       } else {        
  	  	if (transactionType.equals(TransactionType.ISSUE)) {
  	  	   logicalPage = "Transaction-FTBA-ISS";
  	  	} 
  	       }
  	    }
  	  //-- CR-451 NShrestha 10/27/2008 End --
  	  //NSX CR-509 12/15/2009 Begin 
  	    else if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))
  	    {
  	       if (toTransactionSummary) {
  	          logicalPage = "InstrumentSummary";
  	       } else {        
  	  	       if (transactionType.equals(TransactionType.ISSUE)) {
  	  	            logicalPage = "Transaction-DDI-ISS";
  	  	       } 
  	       }
  	    } else if(instrumentType.equals(InstrumentType.DOCUMENTARY_BA) && !TransactionType.SIM.equals(transactionType) 
  	    		&& !TransactionType.SIR.equals(transactionType) ){
  	  	      logicalPage = "TradeTransactionsHome";
  	    } else if ( InstrumentType.IMPORT_COL.equals(instrumentType) || InstrumentType.DOCUMENTARY_BA.equals(instrumentType) 
  			  || InstrumentType.DEFERRED_PAY.equals(instrumentType) )  {
  			 if (TransactionType.SIM.equals(transactionType) ) {
  		        logicalPage = "Transaction-IMC-SIM-DBA-SIM-DFP-SIM";
  			 } else if (TransactionType.SIR.equals(transactionType) ) {
   		        logicalPage = "Transaction-IMC-SIR-DBA-SIR-DFP-SIR";
   			 }
  	    } else {
  	       out.println("Page for " + instrumentType + ": " + transactionType + " not yet built.");
  	    }
  	   }

  	    Debug.debug("Next Logical page is " + logicalPage);

  	    // Now, if the logical page is not blank, forward to the physical page for the
  	    // logical page.
  	    if (!logicalPage.equals("")) {
  	      String physicalPage = NavigationManager.getNavMan().getPhysicalPage(logicalPage, request);
  	      Debug.debug("...and the physical page is " + physicalPage);
  	    String logicalPageAndInstrumentId = logicalPage ;	
  	      // Add context so that we will know which page the user is actually looking at
  	      PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

  	      if((perfStat != null) && (perfStat.isLinkAction()))
  	          perfStat.setContext(logicalPage);

  	      // When the isFromTransTermsFalg is true, we need to pass this through as
  	      // a parameter to the transaction page.  It gets passed on to the 
  	      // PartNavigation bar to include in the parts tab links.
  	      String extraPartTags="";
  	      if (isFromTransTermsFlag) {
  	         extraPartTags = "%26isFromTransTermsFlag="
  	                       + EncryptDecrypt.encryptStringUsingTripleDes( "true", userSession.getSecretKey() );
  	      }
  	      //IValavala Rel 8.2 IR T36000016319. Applying the below for handling returning pages.
  	      //cquinton 5/23/2013 handle returning action
  	      if ( "true".equals( request.getParameter("returning") ) ) {
  	    	
  	        SessionWebBean.PageFlowRef thisPage = userSession.pageBack(); //to keep it correct
  	      } 
  	      else {
  	        //cquinton 1/17/2013 add the page - if its a new transaction, clear existing page flow and
  	        //insert home page and instrument summary for back behavior
//  	        if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(newTransaction)) {
//  	          userSession.clearPageFlow();
//  	          userSession.addPage("goToTradePortalHome");
//  	          String iOid = instrument.getAttribute("instrument_oid");
//  	          String encIOid = EncryptDecrypt.encryptStringUsingTripleDes( iOid, userSession.getSecretKey() );
//  	          userSession.addPage("goToInstrumentSummary", userSession.new PageFlowParameter("instrument_oid", encIOid));
//  	        }
  	        //add to page flow, include all request parms
  	        
  	        if ( toTransactionSummary ) {
 	        	  userSession.addPage("goToInstrumentSummary", request); //bit of a correction!
 	        }
  	        else {
  	        	
	  	        if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(newTransaction) && TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(fromDetailPage)) {
	  	        	
		          userSession.clearPageFlow();
		          userSession.addPage("goToTradePortalHome");
		          String iOid = instrument.getAttribute("instrument_oid");
		          String encIOid = EncryptDecrypt.encryptStringUsingTripleDes( iOid, userSession.getSecretKey() );
		          userSession.addPage("goToInstrumentSummary", userSession.new PageFlowParameter("instrument_oid", encIOid));
		        }
  	        	
  	          	userSession.addPage("updateTransaction", request);
  	          theSession.removeAttribute("newTransaction");
	          theSession.removeAttribute("isFromTransDetailPage"); 
	  	      
  	        }
  	      }
  	  %>

    <jsp:forward page='<%=physicalPage%>'>
       <jsp:param name="extraPartTags" value="<%=extraPartTags%>" />
       <jsp:param name="logicalPageAndInstrumentId" value="<%=logicalPageAndInstrumentId%>" />
       <jsp:param name="transOid" value="<%=transactionOid%>" />
    </jsp:forward>
<%
  }
  // else we've fallen through to an unknown type, what should we do?
%>

</body>
</html>




<%!
   // Various methods are declared here (in alphabetical order).

   public String getAnchorTag(DocumentHandler doc) {
     if (doc.getDocumentNode("/In/PartySearchInfo/PartyOid") != null) {
       // We have returned from the party search page.  Based on the returned 
       // party type, create an anchor link that can be appended to the
       // page.  This causes the page to scroll to that link (effectively putting
       // focus near the returned party fields.

       String termsPartyType = doc.getAttribute("/In/PartySearchInfo/Type");

      if (InstrumentServices.isNotBlank(termsPartyType)) {
          return ("#" + termsPartyType);
       }
     }

     return "";
   }

%>