<%--
*******************************************************************************
                         Instrument Route Navigator Page

  Description:
	Directs the routing to the necessary page.
	0. Determine if the routing originates from listview or detail page
	1. If RouteSelection errors exist, display the route selection page
	2. If other errors exist, display the originating page (listview or detail)
	3. If no errors, return to the listview, either directly, or through
	   InstrumentCloseNavigator.

*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

  // Chandrakanth CR 451 11/27/2008 Begin
  String currentPrimaryNavigation = userSession.getCurrentPrimaryNavigation();
  // Chandrakanth CR 451 11/27/2008 End.
  DocumentHandler doc = null;
  String newCurrPage = null;
  String forwardPage = null;

  // Get the document from the cache to determine what page
  // to forward to:
  // route selection related errors - return to Route Selection page
  // from listview - return to transactions listview
  // from detail page - if there are errors, return to detail page
  // otherwise, return to InstrumentCloseNavigator page
  doc = formMgr.getFromDocCache();

  Debug.debug("doc from cache is " + doc.toString());

  // determine if originating page is from listview or from detail
  
  boolean fromListView = doc.getAttribute("/In/Route/fromListView") != null &&
  						 doc.getAttribute("/In/Route/fromListView").equals
						 (TradePortalConstants.INDICATOR_YES);
		
  // Page navigation is as follows:
  //   - if any route errors exists, return to route items page
  //   - if we've come from the transactions list view, return to the 
  //     Transactions Home page
  //   - if there are non-route errors, return to the instrument
  //   - otherwise close the instrument (and forward on based on the
  //     session parameter INSTRUMENT_CLOSE_ACTION).  Whatever page
  //     sent us here should have set that parameter.
 

    if (fromListView )
  {

	  if ("NavigationBar.CashManagement".equals(currentPrimaryNavigation)) {
		  newCurrPage = "PaymentTransactionsHome";
      } else  if ("NavigationBar.ReceivablesManagement".equals(currentPrimaryNavigation)) {
	      newCurrPage = "ReceivablesManagementHome";
      } else  if ("NavigationBar.DirectDebitTransaction".equals(currentPrimaryNavigation)) {
	      newCurrPage = "DirectDebitTransactionsHome";
      } else {
		  newCurrPage = "TradeTransactionsHome";  
	  }
  }
  else 
  {
	newCurrPage = "InstrumentNavigator";
	if("NavigationBar.Transactions".equalsIgnoreCase(currentPrimaryNavigation) 
			|| "NavigationBar.NewInstruments".equalsIgnoreCase(currentPrimaryNavigation) ) {
		  newCurrPage = "InstrumentCloseNavigator";		
	} else if("NewInstrumentsMenu.Trade.AirWaybill".equalsIgnoreCase(currentPrimaryNavigation)){
		newCurrPage = "InstrumentNavigator";	
	}
  }
  formMgr.setCurrPage(newCurrPage);

%>
  <jsp:forward page='<%=NavigationManager.getNavMan().getPhysicalPage(newCurrPage, request)%>' />