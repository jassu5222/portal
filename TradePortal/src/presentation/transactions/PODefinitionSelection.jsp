<%--
*******************************************************************************
                       PO Definition Selection Page

  Description: When a user wants to add manually entered POs to a transaction, 
  they must first select a PO definition.  This page allows the user to select 
  one PO Definition and then continue to the PO Details Entry page where they 
  may manually enter PO Line Items.

  Clicking the Continue button forwards the user to the PO Details Entry Page.  
  The Cancel button returns the user to the Transaction Page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2003                      
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*, com.ams.tradeportal.busobj.util.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval/page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  // Declare variables
  DocumentHandler doc            = null;
  String          onLoad         = null;
  String          link           = null;
  String          physicalPage   = null;
  String          options        = "";
  String          defaultText    = null;
  boolean         hasPOLineItems = false;

  // Declare variables for retrieving PO Definitions
  DocumentHandler poDefinitionsDoc = null;
  DocumentHandler resultDoc        = null;
  StringBuffer    sqlQuery         = null;
  String          poDefinitionOid  = null;
  String          defaultDefnOid   = "";
  String          definition       = null;
  Vector          poDefinitionList = new Vector();

  doc = formMgr.getFromDocCache();

  // If a definition oid already exists then forward user to the PODetailsEntry
  // page; a definition oid would exist if manually entered PO Line Items have 
  // been added to the transaction. Only use the definition oid if po line items 
  // are assigned to this shipment, definition oid could have a value if uploaded 
  // po line items are assigned to other shipments of this transaction, in which 
  // case the user should be prompted to select a definition

  hasPOLineItems  = Boolean.valueOf(doc.getAttribute("/In/SearchForPO/hasPOLineItems")).booleanValue();
  if (hasPOLineItems)
  {
    poDefinitionOid  = doc.getAttribute("/In/SearchForPO/uploadDefinitionOid");
  }

  if (!InstrumentServices.isBlank(poDefinitionOid))
  {
    poDefinitionOid = EncryptDecrypt.encryptStringUsingTripleDes(poDefinitionOid, userSession.getSecretKey());
    formMgr.setCurrPage("PODetailsEntry");
    physicalPage = NavigationManager.getNavMan().getPhysicalPage("PODetailsEntry", request);
%>
    <jsp:forward page='<%= physicalPage %>'>
      <jsp:param name="PODefinition" value="<%=poDefinitionOid%>" />
    </jsp:forward>
<%
  }

  // Build query to retrieve the Corporate Customer's PO Definitions
  poDefinitionsDoc = new DocumentHandler();
  sqlQuery         = new StringBuffer();
  List<Object> sqlParams = new ArrayList<Object>();

  sqlQuery.append("select po_upload_definition_oid, name ");
  sqlQuery.append(" from po_upload_definition where a_owner_org_oid in (?");
  sqlParams.add(userSession.getOwnerOrgOid());
  // Also include PO defs from the user's actual organization if using subsidiary access
  if(userSession.showOrgDataUnderSubAccess())
   {
      sqlQuery.append(",?");
      sqlParams.add(userSession.getSavedUserSession().getOwnerOrgOid());
   }

  sqlQuery.append(") and definition_type in (");
  sqlQuery.append("?,");
  sqlQuery.append("?)"); 
  sqlParams.add(TradePortalConstants.PO_DEFINITION_TYPE_MANUAL);
  sqlParams.add(TradePortalConstants.PO_DEFINITION_TYPE_BOTH);
  sqlQuery.append(" order by ");
  sqlQuery.append(resMgr.localizeOrderBy("name"));

  // Retrieve any PO Definitions
  poDefinitionsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams);
  if (poDefinitionsDoc != null)
  {
    poDefinitionList = poDefinitionsDoc.getFragments("/ResultSetRecord");
    // Special navigation logic: if there is only one definition, pass this 
    // definition oid to the PODetailsEntry page.
    if (poDefinitionList.size() == 1)
    {
      poDefinitionsDoc = (DocumentHandler) poDefinitionList.elementAt(0);
      poDefinitionOid  = poDefinitionsDoc.getAttribute("/PO_UPLOAD_DEFINITION_OID");
      poDefinitionOid = EncryptDecrypt.encryptStringUsingTripleDes(poDefinitionOid, userSession.getSecretKey());
      formMgr.setCurrPage("PODetailsEntry");
      physicalPage = NavigationManager.getNavMan().getPhysicalPage("PODetailsEntry", request);
%>
      <jsp:forward page='<%= physicalPage %>'>
        <jsp:param name="PODefinition" value="<%=poDefinitionOid%>" />
      </jsp:forward>
<%
    } 
  }

  // Determine if any PO Definition is the default.  If so, we'll use
  // that as the definition to select in the dropdown list.
  sqlQuery = new StringBuffer("select po_upload_definition_oid");
  sqlQuery.append(" from po_upload_definition ");
  sqlQuery.append(" where a_owner_org_oid = ?" );
  sqlQuery.append(" and definition_type in (");
  sqlQuery.append("?,");
  sqlQuery.append("?)");
  sqlQuery.append(" and default_flag='Y'"); 
  
  sqlParams = new ArrayList<Object>();
  sqlParams.add(userSession.getOwnerOrgOid()); 
  sqlParams.add(TradePortalConstants.PO_DEFINITION_TYPE_MANUAL);
  sqlParams.add(TradePortalConstants.PO_DEFINITION_TYPE_BOTH);
  resultDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams);
  if (resultDoc != null) 
  {
    defaultDefnOid = resultDoc.getAttribute("/ResultSetRecord(0)/PO_UPLOAD_DEFINITION_OID");
  }

%>

<%-- ========================== HTML for page begins here ==============================  --%>

<%
  // onLoad is set to default the cursor to the dropdown field
  onLoad = "document.PODefinitionSelectionForm.PODefinition.focus();";
%>

  <jsp:include page="/common/Header.jsp">
    <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
    <jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
    <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
  </jsp:include>

<%-- Note the non-standard ACTION on this form.  Rather than going through the
     AMS Servlet continue to a validation page (which validates the selection) --%>

	<div class="pageMain">     
		<div class="pageContent"> 
			<jsp:include page="/common/PageHeader.jsp">
		    <jsp:param name="titleKey" value="PODefinitionSelection.PODefinitionTitle" /> 
		    <jsp:param name="helpUrl" value="/customer/select_po_definition.htm" />
		  	</jsp:include>
		  
			
			<form id="PODefinitionSelectionForm" name="PODefinitionSelectionForm" method="POST" data-dojo-type="dijit.form.Form"
            	action="<%= request.getContextPath()+NavigationManager.getNavMan().getPhysicalPage("ValidateDefinitionSelection", request) %>">
				<div class="formArea">
				<jsp:include page="/common/ErrorSection.jsp" />
					<div class="formContent">
					</br>
					<div class="formItem">
						<%=widgetFactory.createNote("PODefinitionSelection.SelectPODefinition")%>
						</div>
						
					<%
	          			// Build the list of definitions (with encrypted oids)
	          			if (poDefinitionsDoc != null){
	            			options = ListBox.createOptionList(poDefinitionsDoc, "PO_UPLOAD_DEFINITION_OID", "NAME", defaultDefnOid, userSession.getSecretKey());
	          			}
	         	 		defaultText = resMgr.getText("PODefinitionSelection.SelectDefinition", TradePortalConstants.TEXT_BUNDLE);
					%>
	      				<%=widgetFactory.createSelectField("PODefinition", "PODefinitionSelection.PODefinition", defaultText, options, false, true, false, "", "", "")%>
	      				<br><br>
	  				</div><%-- end of formContent div --%>  
	  			</div><%--formArea--%>
	  			
	  			<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props=" form: 'PODefinitionSelectionForm'">
                <%= widgetFactory.createSidebarQuickLinks(false, true, false, userSession) %>
				<ul class="sidebarButtons">	
						<button data-dojo-type="dijit.form.Button"  name="Continue" id="Continue" type="submit" data-dojo-props="iconClass:'verify'" >
				    		<%=resMgr.getText("common.continueText",TradePortalConstants.TEXT_BUNDLE)%>
				    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								setButtonPressed('<%=TradePortalConstants.BUTTON_CONTINUE%>', '0');
								document.forms[0].submit();       					
	 						</script>
						</button> 
						<%=widgetFactory.createHoverHelp("Continue", "ContinueHoverText") %>
						<button data-dojo-type="dijit.form.Button"  name="Cancel" id="Cancel" type="button" data-dojo-props="iconClass:'close'" >
					    	<%=resMgr.getText("common.CancelText",TradePortalConstants.TEXT_BUNDLE)%>
					    	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								openURL("<%=formMgr.getLinkAsUrl("goToInstrumentNavigator", response)%>");        					
	 						</script>
						</button> 
						<%=widgetFactory.createHoverHelp("Cancel", "CloseHoverText") %>
					</ul>
				</div> <%--closes sidebar area--%>
			</form>
  		</div><%-- end of pageContent div --%>
	</div><%-- end of pageMainNoSidebar div --%>
	
	<jsp:include page="/common/Footer.jsp">
	    <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	    <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
    </jsp:include>	
    
    <script language="JavaScript">
		function openURL(URL){
		    if (isActive =='Y') {
		    	if (URL != '' && URL.indexOf("javascript:") == -1) {    
		    		var cTime = (new Date()).getTime();
			        URL = URL + "&cTime=" + cTime;
			        URL = URL + "&prevPage=" + context;
		    	}
		    }
	 		document.location.href  = URL;
	 		return false;
 		}
	</script>
</body>
</html>

<%
  	doc.removeComponent("/Error");
  	formMgr.storeInDocCache("default.doc", doc);
%>
