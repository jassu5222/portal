<%--
*******************************************************************************
                            PO Details Entry Page

  Description:  The PO Details Entry page allows the user to manually add
                PO line items to an Import DLC Issue transaction. If the user's
                corporate org allows PO's to be manually created then a button
                is displayed on the transaction page which will open this page.

                The elements of this page are created dynamically. This page
                uses a PO Definition to determine which fields to display, the
                names of those fields, and the sizes of those fields.  Therefore,
                the user's corporate org must have at least one PO Definition
                defined in order to access this page and manually create PO's.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2003                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*, com.ams.tradeportal.busobj.util.*, java.math.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
Debug.debug("***START******************** PO Entry Details **************************START***");
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   // Variables used to gather info about the shipment, transaction, po line items
   DocumentHandler    doc                  = null;
   DocumentHandler    poLineItemDoc        = null;
   Hashtable          secureParms          = null;
   boolean            isReadOnly           = false;
   boolean            getDataFromDoc       = false;
   boolean            hasPOLineItems       = false;
   boolean            defaultCheckBoxValue = false;
   String             userSecurityType     = null;
   String             userOrgOid           = null;
   String             instrumentOid        = null;
   String             poDateFormat         = null;
   String             dateFormat           = null;
   String             transactionOid       = null;
   String             transactionCurrency  = null;
   String             shipmentOid          = null;
   String             definitionOid        = null;
   String             poLineItemOid        = null;
   String             shipmentNumber       = null;
   String             link                 = null;
   String             buttonPressed        = "";
   String             maxError             = "";
   String             onLoad               = "";

   // Variables used to construct the html for the PO Line Items
   boolean            createNewRow         = false;
   boolean            newPOLineItem        = false;
   String             colorTag             = null;
   String             extraTags            = "";
   String             fieldType            = null;
   String             fieldName            = null;
   String             fieldSize            = null;
   String             fieldDataType        = null;
   String             fieldValue           = null;
   int                fieldIndex           = 0;
   int                maxRowSize           = 0;
   int                currentRowSize       = 0;
   int                numberOfPOFields     = 0;
   int                numPOLineItems       = 0;
   int                iLoop                = 0;
   int                poLineItemCount      = 0;
   Vector             poLineItemFieldList  = new Vector();
   Vector             poLineItemList       = new Vector();
   POLineItemField    poLineItemField      = null;

   // Variables used for populating the PO Text dropdown
   DocumentHandler    phraseLists          = null;
   DocumentHandler    poTextDocList        = null;
   String             options;
   String             defaultText;
   
   // <Amit - IR EEUE032659500 -05/06/2005 >
   int 		      numberOfCurrencyPlaces = 0;
   //</Amit - IR EEUE032659500 -05/06/2005 >
   boolean enableEncode = false; //Rel 9.3 XSS CID 11429
   boolean isValidDate = false;

   // Get the user's security type and organization oid
   userSecurityType = userSession.getSecurityType();
   userOrgOid       = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
  
   // Determine the user's screen resolution; if extended listviews are displayed
   // then the user's screen resolution is 1024x768 or higher else its 800x600.
   // This setting determines how many po line item fields should be displayed
   // on each row; a lower sreeen resolution restricts the number of po line fields
   // that can be displayed per row.
   maxRowSize = 500;
   if (userSession.getDisplayExtendedListview() == true)
     maxRowSize = 500;

   // Get and create web beans
   TransactionWebBean        transaction   = (TransactionWebBean) beanMgr.getBean("Transaction");
   InstrumentWebBean         instrument    = (InstrumentWebBean) beanMgr.getBean("Instrument");
   TermsWebBean terms                      = (TermsWebBean) beanMgr.getBean("Terms");
   ShipmentTermsWebBean      shipmentTerms = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
   POUploadDefinitionWebBean poDefinition  = beanMgr.createBean(POUploadDefinitionWebBean.class, "POUploadDefinition");

   doc = formMgr.getFromDocCache();

   // Get the instrument oid from instrument
   instrumentOid = instrument.getAttribute("instrument_oid");
  
   // Get the transaction oid from transaction
   transactionOid      = transaction.getAttribute("transaction_oid");
   transactionCurrency = transaction.getAttribute("copy_of_currency_code");

   // Get information about the shipment that these PO Details are associated with
   if (doc.getDocumentNode("/In/SearchForPO") != null)
   {
     shipmentOid    = doc.getAttribute("/In/SearchForPO/shipment_oid");
     shipmentNumber = doc.getAttribute("/In/SearchForPO/shipmentNumber");
     definitionOid  = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("PODefinition"), userSession.getSecretKey());
   }
   else
   {
     shipmentOid    = doc.getAttribute("/In/ShipmentTerms/shipment_oid");
     shipmentNumber = doc.getAttribute("/In/shipmentNumber");
     definitionOid  = doc.getAttribute("/In/definitionOid");
   }

	 

   // Shipment oid will be empty if we're adding PO's to a new shipment; shipment exists in
   // database but the oid is not in the xml document.  Attempt to retrieve shipment using
   // the shipment number
   if(InstrumentServices.isBlank(shipmentOid) || shipmentOid.equals("0"))
   {
     if(shipmentNumber != null)
     {
       shipmentTerms.loadShipmentTerms(terms.getAttribute("terms_oid"), Integer.parseInt(shipmentNumber));
       shipmentOid = shipmentTerms.getAttribute("shipment_oid");
       doc.setAttribute("/In/SearchForPO/shipment_oid", shipmentOid);
     }
   }

   shipmentTerms.setAttribute("shipment_oid", shipmentOid);
   shipmentTerms.getDataFromAppServer();

   // Load the PO Definiton Web Bean and retrieve the PO Line Item Fields
   if (!InstrumentServices.isBlank(definitionOid))
   {
     poDefinition.setAttribute("po_upload_definition_oid", definitionOid);
     poDefinition.getDataFromAppServer();
     poLineItemFieldList = poDefinition.loadPOLineItemFields();
     numberOfPOFields = poLineItemFieldList.size();
     
     // Get the date format used by this PO Definition
     poDateFormat = poDefinition.getAttribute("date_format");
     if (poDateFormat.equals(TradePortalConstants.PO_DATE_FORMAT_EURO))
     {
       dateFormat = TradePortalConstants.PO_FORMATTED_EURO_DATE;
     }
     else
     {
       dateFormat = TradePortalConstants.PO_FORMATTED_US_DATE;
     }
   }

   // Determine if the user pressed a button
   if (doc.getDocumentNode("/In/Update/ButtonPressed") != null) 
   {
     buttonPressed  = doc.getAttribute("/In/Update/ButtonPressed");
   }

   // Now check to see if we came back with errors
   maxError = doc.getAttribute("/Error/maxerrorseverity");
   if ((maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) > 0) ||
       (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_PO_LINE_ITEM))       || 
       (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_FIVE_PO_LINE_ITEMS)) ||
       (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_SELECTED_PO_ITEMS)))
   {
     getDataFromDoc = true;
   }

   // If a phrase was just retrieved then retrieve phrase from the output document,
   // set the value in the input document and get data from doc
   if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) 
   {
     // If a Looked up phrase exists then replace it in the /In document
     getDataFromDoc = true;

     String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
     if (result.equals(TradePortalConstants.INDICATOR_YES)) 
     {
       // Take the looked up and appended phrase text from the /Out section
       // and copy it to the /In section.  This allows the beans to be 
       // properly populated.
       String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
       xmlPath = "/In" + xmlPath;
       doc.setAttribute(xmlPath, doc.getAttribute("/Out/PhraseLookupInfo/NewText"));
       
       // If we returned from the phrase lookup without errors, set the focus
       // to the correct field.  Otherwise, don't set the focus so the user can
       // see the error.
       if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) 
       {
         String focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
         onLoad += "document.forms[0]." + focusField + ".focus();";
       }
     }
   }

   // Retrieve the shipment's PO Line Items
   if (getDataFromDoc)
   {
     poLineItemList = doc.getFragments("/In/ShipmentTerms/POLineItemList");
     numPOLineItems = poLineItemList.size();
   }
   else
   {
     poLineItemList = shipmentTerms.retrieveAssignedPOLineItems();
   }

   // Get the number of PO Line Items if any exist
   if (poLineItemList != null)
   {
     hasPOLineItems = true;
     numPOLineItems = poLineItemList.size();
   }

   // Page should display at least 10 PO Line Items
   if (numPOLineItems < 10)
   {
     numPOLineItems = 10;
   }

   // If the user pressed one of the buttons to add PO Line Items then increment
   // the number of available PO Line Items accordingly
   if (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_PO_LINE_ITEM))
   {
     numPOLineItems++;
     onLoad += "location.hash='#BottomOfPage" + "';";
   }
   else if (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_FIVE_PO_LINE_ITEMS))
   {
     numPOLineItems += 5;
     onLoad += "location.hash='#BottomOfPage" + "';";
   }

   // Create a PO Line Item Web Bean for the number of PO Line Items
   POLineItemWebBean poLineItem[] = new POLineItemWebBean[numPOLineItems];
   for (iLoop=0;iLoop<numPOLineItems;iLoop++)
   {
     poLineItem[iLoop] = beanMgr.createBean(POLineItemWebBean.class, "POLineItem");

     // Only load the Web Bean's data if it exists in the database or in the document
     if ((hasPOLineItems) && (iLoop < poLineItemList.size()))
     {       
       poLineItemDoc = (DocumentHandler) poLineItemList.elementAt(iLoop);
       if (getDataFromDoc)
       {
         // Load the Web Bean's data from the document
         poLineItem[iLoop].loadManualPOLineItemsFromDoc(poLineItemDoc, poLineItemFieldList);
       }
       else
       {
         // If we're loading the Web Bean from the database set the oid and retrieve the data
         poLineItemOid = poLineItemDoc.getAttribute("/PO_LINE_ITEM_OID");
         poLineItem[iLoop].setAttribute("po_line_item_oid", poLineItemOid);
         poLineItem[iLoop].getDataFromAppServer();
       }
     }
   }

   // Create document for the po text phrase dropdown. First check the cache (no need
   // to recreate if they already exist).  After creating, place in the cache.
   phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
   if (phraseLists == null) phraseLists = new DocumentHandler();

   poTextDocList = phraseLists.getFragment("/POText");
   if (poTextDocList == null) 
   {
     poTextDocList = PhraseUtility.createPhraseList(TradePortalConstants.PHRASE_CAT_POTEXT, 
                                                   userSession, formMgr, resMgr);
     phraseLists.addComponent("/POText", poTextDocList);
   }
   formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);

   // Store attribute data in secure parms
   secureParms = new Hashtable();
   secureParms.put("shipment_oid", shipmentOid);
   secureParms.put("shipmentNumber", shipmentNumber);
   secureParms.put("definitionOid", StringFunction.xssCharsToHtml(definitionOid));
   secureParms.put("transaction_oid", transactionOid);
   secureParms.put("transactionCurrency", transactionCurrency);
   secureParms.put("corpOrgOid", userOrgOid);
   secureParms.put("instrument_oid", instrumentOid);
   secureParms.put("partNumber", TradePortalConstants.PART_THREE);

%>

<%-- ========================== HTML for page begins here ==============================  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>

	<div class="pageMain">
		<div class="pageContent">
		 	<jsp:include page="/common/PageHeader.jsp">
		    <jsp:param name="titleKey" value="PODetailsEntry.PODetails" /> 
		    <jsp:param name="helpUrl" value="/customer/po_details_entry.htm" />
		  </jsp:include>
 
			
		<%
			StringBuffer parms = new StringBuffer();
			parms.append("&returnAction=");
			parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToInstrumentSummary",  userSession.getSecretKey()));
			parms.append("&instrument_oid=");
			parms.append(EncryptDecrypt.encryptStringUsingTripleDes(instrument.getAttribute("instrument_oid"),  userSession.getSecretKey()));
			
			String loginLocale = userSession.getUserLocale();
			ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
			
			StringBuffer breadCrumb = new StringBuffer();
			breadCrumb.append(instrument.getAttribute("complete_instrument_id"));
			breadCrumb.append("&nbsp;&nbsp;");
			breadCrumb.append(refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrument.getAttribute("instrument_type_code"),loginLocale));
			
			String returnLink = formMgr.getLinkAsUrl("goToInstrumentNavigator", parms.toString(), response);
			String typeCode = refData.getDescr(TradePortalConstants.TRANSACTION_TYPE,transaction.getAttribute("transaction_type_code"), loginLocale);
			String statusCode = refData.getDescr(TradePortalConstants.TRANSACTION_STATUS, transaction.getAttribute("transaction_status"), loginLocale);
			String shipmentCode = resMgr.getText("PODetailsEntry.Shipment", TradePortalConstants.TEXT_BUNDLE);
			String poDetailCode = resMgr.getText("PODetailsEntry.PODetails", TradePortalConstants.TEXT_BUNDLE);
			String transactionSubHeaderTitle = breadCrumb+" - "+typeCode+"("+statusCode+")"+" - "+shipmentCode+" "+ shipmentNumber;//+" - "+poDetailCode;
		%>

			<jsp:include page="/common/PageSubHeader.jsp">
			  <jsp:param name="titleKey" value="<%=transactionSubHeaderTitle%>" />
			  <jsp:param name="returnUrl" value="<%=returnLink%>" />
			</jsp:include>		
			
			<form id="PODetailsEntryForm" name="PODetailsEntryForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">		

			<%
				// Set parameters used by the transaction page to determine that the user is coming
				// from this page
				parms = new StringBuffer();
				parms.append("&updatedPODetails=true");
				parms.append("&shipmentTabNumber=");
				parms.append(shipmentNumber);
				parms.append("&partNumber=");
				parms.append(TradePortalConstants.PART_THREE);
				
				link = formMgr.getLinkAsUrl("goToInstrumentNavigator", parms.toString(), response);
			%>
				<div class="formArea">
					<jsp:include page="/common/ErrorSection.jsp" />
					<div class="formContent">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" >
		<tr>
							<br>
							<div class="formItem">
							<%= widgetFactory.createNote("PODetailsEntry.EnterPOLineItemData")%>
							<%= widgetFactory.createNote("PODetailsEntry.PODateFormat")%>&nbsp;<i class ="note"><%= "   " + dateFormat.toUpperCase() + "."%></i>
							</div>
							
	</tr>
</table>					
							<%-- <i>
								<%=resMgr.getText("PODetailsEntry.EnterPOLineItemData", TradePortalConstants.TEXT_BUNDLE)%>
						        <%=resMgr.getText("PODetailsEntry.PODateFormat", TradePortalConstants.TEXT_BUNDLE)%>
						        <%= " " + dateFormat.toUpperCase() + "."%>
					        </i> --%>

					<%
						// Alternate the background color for each PO Line Item
						for (poLineItemCount = 0; poLineItemCount<numPOLineItems; poLineItemCount++){
						  	colorTag = "";
					  		if (poLineItemCount % 2 == 0){
						    	//colorTag = "class=ColorGrey";
						    	//background-color: #C0C0C0;
						    	colorTag = "bgcolor=#D8D8D8";
						  	} 
					%>
   <table width="100%" border="0" cellspacing="0" cellpadding="0" <%=colorTag%>>
     <tr> 
      <td width="10" nowrap>&nbsp;</td>
      <td width="10" class="ColorBeige" nowrap>&nbsp;</td>
      <td nowrap>&nbsp;</td>
     </tr>
   </table>
  <%-- ============================ Begin - Create po line item field ================================ --%>
<%
						    // Create a data entry field for each PO Line Item Field
    for (fieldIndex = 0; fieldIndex < numberOfPOFields; fieldIndex++)
    {
								poLineItemField = (POLineItemField) poLineItemFieldList.elementAt(fieldIndex);
								fieldType       = poLineItemField.getFieldType();
								fieldName       = poLineItemField.getFieldName();
								fieldSize       = String.valueOf(poLineItemField.getFieldSize());
								fieldDataType   = poLineItemField.getFieldDataType();
	      
	      						// Get the value of the po line item field
      if (fieldType.equals(TradePortalConstants.PO_FIELD_TYPE_LAST_SHIPMENT_DATE))
      {
        fieldValue = poLineItem[poLineItemCount].getAttribute(fieldType);
        if (fieldValue.length() > 18)
        {
          fieldValue = DateTimeUtility.convertTimestampToDateString(fieldValue, dateFormat);         
        }
        if(TPDateTimeUtility.isValidDateFormat(fieldValue, dateFormat))
        	isValidDate = true;     
      }   						
      // <Amit - IR EEUE032659500 -05/06/2005 >
      else if(fieldType.equals(TradePortalConstants.PO_FIELD_TYPE_AMOUNT))
      {

	fieldValue = poLineItem[poLineItemCount].getAttribute(fieldType);

	if(transactionCurrency.equals("") || (transactionCurrency == null))
	{
		numberOfCurrencyPlaces = 2;
	}
	else
	{	
		numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, transactionCurrency));
	}

	fieldValue = TPCurrencyUtility.getDecimalAmountWithoutThousandSeparator(fieldValue, numberOfCurrencyPlaces, loginLocale);	
      }
      // </Amit - IR EEUE032659500 -05/06/2005 >
      else 
      {
        fieldValue = poLineItem[poLineItemCount].getAttribute(fieldType);
      }
	  // Rel 9.3 XSS CID-11429 For 'date' field type, check if the fieldValue is in valid date format.Enable encoding on fieldValue, if either the date is not valid or the field type is other than DATE.  
	  if(!fieldType.equals(TradePortalConstants.PO_FIELD_TYPE_LAST_SHIPMENT_DATE) || !isValidDate )    						
		  enableEncode = true;
      // Determine the size of the field and it's label in pixels
      // Each character in the field's label is approx 8 pixels
      // The field's size in pixels can be determine by multiplying the size
      // by 6 and adding 22
      int labelLength = fieldName.length() * 8;
      int fieldLength = 22 + (Integer.parseInt(fieldSize) * 6);

      // Need to create a new row in the table if this is the first field of the 
      // PO Line Item or if we've reached the max row size
      createNewRow  = false;
      newPOLineItem = false;

      if (fieldIndex == 0)
      {
        createNewRow  = true;
        newPOLineItem = true;
      }
      else
      {
        // Determine the row size by adding the field's size or the label's size (whichever is larger)
        // plus 15 pixels for the space inserted between the fields to the current row size
        // Create a new row if the current row size will be greater than the max row size or
        // if the current field is po text
        if (labelLength > fieldLength)
          currentRowSize += labelLength + 15;
        else      
          currentRowSize += fieldLength + 15;

        if (currentRowSize > maxRowSize || fieldType.equals("po_text"))
        {
          createNewRow = true;
          // If this is not the first or last PO Line Item field and we're creating a new row
          // then we need to add the closing tag for the previous row
          if (fieldIndex < numberOfPOFields)
          {
%>
                <td width="100%" nowrap>&nbsp;</td>
              </tr>
            </table>
<% 
            if (fieldType.equals("po_text"))
            {
%> 
              <table border="0" cellspacing="0" cellpadding="0" <%=colorTag%>>
                <tr> 
                  <td width="20" nowrap>&nbsp;</td>
                  <td class="ColorBeige" width="20" nowrap>&nbsp;</td>
                  <td width="100%" <%=colorTag%> nowrap>&nbsp;</td>
                </tr>
              </table>
<%
            }
          }
        }
      }
      
      // If a new row is created reset the current row size and create the new row; if the current
      // field is po text then set the current row size to the max size because po text should 
      // be displayed on it's own row
      if (createNewRow)
      {
        if (fieldType.equals("po_text"))
        {
          currentRowSize = maxRowSize;
        }
        else
        {
          if (labelLength > fieldLength)
            currentRowSize = labelLength;
          else      
            currentRowSize = fieldLength;
        }
%>
        <table width="100%" border="0" cellspacing="0" cellpadding="0"  <%=colorTag%> >
          <tr> 
            <td><div style="width:10px"></div></td>  
<%    

            if (newPOLineItem)
            {
%>
              <td  width="20" align="left" valign="bottom">
<%
              defaultCheckBoxValue = false;
              if (poLineItem[poLineItemCount].getAttribute("selected_indicator").equals(TradePortalConstants.INDICATOR_YES))
              {
                defaultCheckBoxValue = true;
              }
              
              // Set tab index for check box field to ensure its not part of the normal tab sequence when a user
              // navigates between the different po line item fields  
              extraTags = "tabindex=1";
%>
				<%= widgetFactory.createCheckboxField("selected_indicator" + poLineItemCount, "", defaultCheckBoxValue,isReadOnly, 
                        								false,extraTags, "","none", "") %>
          
            
              </td>
              <%if(poLineItemCount<9){ %>
              <td ><div style="width:5px"></div></td>
              <%} %>
              <td width="20" align="right" valign="bottom">
                <%= widgetFactory.createSubLabel((poLineItemCount + 1) + ".")%>
              </td>
<%
            }  
            else
            {
            	%>
              <td nowrap>
             <div style="width:40px"></div>
              </td>
              
              
              
<%
            }
     }
	     						
					%>
						<td><div style="width:15px"></div></td>
							<td nowrap align="left">
							<%-- <%=widgetFactory.createLabel("", fieldName, true, false, false, "")%> --%>
							
							<%if (!fieldType.equals(TradePortalConstants.PO_FIELD_PO_TEXT)){ 
							
							System.out.println("fieldvalue "+ fieldValue);%>
      						
      						<%= widgetFactory.createTextField(fieldType + poLineItemCount, fieldName, enableEncode?StringFunction.xssCharsToHtml(fieldValue): StringFunction.xssCharsToHtml(fieldValue), fieldSize, isReadOnly, false, false, enableEncode?StringFunction.xssCharsToHtml(fieldValue):fieldValue, "", "none")%>
      						
					<%
	      						}else{
        if (isReadOnly)
        {
          out.println("&nbsp;");
        }
        else
        {
	          							options = ListBox.createOptionList(poTextDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
	          							defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);
					%>
					
					
					<span class="alignRight">
					<%=widgetFactory.createSelectField("POPhraseItem" + poLineItemCount, "", defaultText, options, isReadOnly, false, false, 
							"onChange=" + PhraseUtility.getPhraseChange("/ShipmentTerms/POLineItemList(" + poLineItemCount + ")/po_text",fieldType + poLineItemCount, "6500","document.forms[0]."+fieldType + poLineItemCount),
							"", "none")%>
					</span>
          			<%
										}
					%>
					
					<%= widgetFactory.createTextArea(fieldType + poLineItemCount,fieldName, enableEncode?StringFunction.xssCharsToHtml(fieldValue):fieldValue, isReadOnly, false, false, "cols='110'", "", "none") %>
					
					
<%
      }
%>
      </td>
<%
	     			
      if (fieldIndex == (numberOfPOFields - 1))
      {
%>
					<td width="100%" nowrap>&nbsp;</td>
          </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" <%=colorTag%>>
          <tr> 
            <td width="20" nowrap>&nbsp;</td>
            <td class="ColorBeige" width="20" nowrap>&nbsp;</td>
             <td width="100%" <%=colorTag%> nowrap>&nbsp;</td>
					          
              			</tr>
              			
      	
			          <input type=hidden name="<%="po_line_item_oid" + poLineItemCount%>" value="<%=EncryptDecrypt.encryptStringUsingTripleDes(poLineItem[poLineItemCount].getAttribute("po_line_item_oid"), userSession.getSecretKey())%>">  
			          <input type=hidden name="<%="active_for_instrument" + poLineItemCount%>" value="<%=instrumentOid%>">        
			          <input type=hidden name="<%="assigned_to_trans_oid" + poLineItemCount%>" value="<%=transactionOid%>">
			          <input type=hidden name="<%="source_upload_definition_oid" + poLineItemCount%>" value="<%=StringFunction.xssCharsToHtml(definitionOid)%>">
			          <input type=hidden name="<%="owner_org_oid" + poLineItemCount%>" value="<%=userOrgOid%>">
			          <input type=hidden name="<%="source_type" + poLineItemCount%>" value="<%=TradePortalConstants.PO_SOURCE_MANUAL%>">
</table>
					<%
      							}
    						}
  						}
					%>
  			<%@ include file="fragments/PhraseLookupPrep.frag" %>
  			<input type=hidden name="buttonName" value="">
 			<%= formMgr.getFormInstanceAsInputField("PODetailsEntryForm", secureParms) %>
<%
   doc.removeComponent("/Error");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>
					</div><%-- end of formContent div --%>  
				</div><%--formArea--%>
				
				<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props=" form: 'PODetailsEntryForm'">
                <%= widgetFactory.createSidebarQuickLinks(false, true, false, userSession) %>
					<ul class="sidebarButtons">	
						<button data-dojo-type="dijit.form.Button"  name="SaveAndClose" id="SaveAndClose" type="submit" data-dojo-props="iconClass:'saveclose'" >
				    		<%=resMgr.getText("common.SaveCloseText",TradePortalConstants.TEXT_BUNDLE)%>
				    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								setButtonPressed('SaveAndClose', '0');
								document.forms[0].submit();       					
	 						</script>
						</button> 
						<%=widgetFactory.createHoverHelp("SaveAndClose","SaveCloseHoverText") %>
						<button data-dojo-type="dijit.form.Button"  name="AddPOLineItem" id="AddPOLineItem" type="submit" data-dojo-props="iconClass:'add'" >
				    		<%=resMgr.getText("PODetailsEntry.AddPOLineItem",TradePortalConstants.TEXT_BUNDLE)%>
				    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								setButtonPressed('<%=TradePortalConstants.BUTTON_ADD_PO_LINE_ITEM%>', '0');
								document.forms[0].submit();       					
	 						</script>
						</button> 
						<%=widgetFactory.createHoverHelp("AddPOLineItem","PODetailsEntry.AddPOLineItem") %>
						<button data-dojo-type="dijit.form.Button"  name="Add5POLineItems" id="Add5POLineItems" type="submit" data-dojo-props="iconClass:'add'" >
				    		<%=resMgr.getText("PODetailsEntry.Add5POLineItems",TradePortalConstants.TEXT_BUNDLE)%>
				    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								setButtonPressed('<%=TradePortalConstants.BUTTON_ADD_FIVE_PO_LINE_ITEMS%>', '0');
								document.forms[0].submit();       					
	 						</script>
						</button>
						<%=widgetFactory.createHoverHelp("Add5POLineItems","PODetailsEntry.Add5POLineItem") %> 
						<button data-dojo-type="dijit.form.Button"  name="DeleteSelectedPOLineItems" id="DeleteSelectedPOLineItems" type="submit" data-dojo-props="iconClass:'delete'" >
				    		<%=resMgr.getText("PODetailsEntry.DeletePOLineItems",TradePortalConstants.TEXT_BUNDLE)%>
				    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE_SELECTED_PO_ITEMS%>', '0');
								document.forms[0].submit();       					
	 						</script>
						</button> 
						<%=widgetFactory.createHoverHelp("DeleteSelectedPOLineItems","PODetailsEntry.DeletePOLineItems") %>
						<button data-dojo-type="dijit.form.Button"  name="Close" id="Close" type="button" data-dojo-props="iconClass:'close'" >
				    		<%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
				    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								openURL("<%=link%>");        					
	 						</script>
						</button> 
						<%=widgetFactory.createHoverHelp("Close","CloseHoverText") %>
					</ul>
				</div> <%--closes sidebar area--%>
 			</form>
 		</div><%-- end of pageContent div --%>
	</div><%-- end of pageMain div --%>
	
	<jsp:include page="/common/Footer.jsp">
	    <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	    <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
    </jsp:include>	
    
    <script language="JavaScript">

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

		function openURL(URL){
		    if (isActive =='Y') {
		    	if (URL != '' && URL.indexOf("javascript:") == -1) {
			    	var cTime = (new Date()).getTime();
			        URL = URL + "&cTime=" + cTime;
			        URL = URL + "&prevPage=" + context;
		    	}
		    }
	 		document.location.href  = URL;
	 		return false;
 		}

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

	</script>
</body>
</html>
