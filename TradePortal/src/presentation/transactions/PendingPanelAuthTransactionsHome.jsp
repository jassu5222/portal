<%--
**********************************************************************************
  Pending Panel Authorisation Transactions Home

  Description:  
     This page is used to view the Pending Panel Authorisation Transactions

**********************************************************************************
--%> 

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
  



<%
	
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
	String gridHtml = "";
	String gridLayout="";
	
	final String ALL = "common.all";
	final String ALL_WORK = "common.allWork";
	final String MY_WORK = "common.myWork";
	
	String  userOid        = userSession.getUserOid();
	String  userOrgOid	 = userSession.getOwnerOrgOid();
	String  loginLocale    = userSession.getUserLocale(); 	
	String  loginRights    = userSession.getSecurityRights();
	String  defaultWIPView = userSession.getDefaultWipView();
	String  currentSecurityType = userSession.getSecurityType();
	//build alltran options
	String currentAllTranWork = MY_WORK;
	String currentAllTranGroup = TradePortalConstants.INSTR_GROUP__ALL;
	String currentAllTranInstrType = ALL;
	String currentAllTranStatus = TradePortalConstants.STATUS_ALL;
	DocumentHandler hierarchyDoc = null;
	int totalOrganizations = 0;
	ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
	
	
	/* Prateep Gedupudi 24/02/2013 getting confidential indicatior for the logged in user Start*/ 
	UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
	   thisUser.setAttribute("user_oid", userOid);
	   thisUser.getDataFromAppServer();
	   
	   String confInd = "";
	   if (userSession.getSavedUserSession() == null)
	       confInd = thisUser.getAttribute("confidential_indicator");
	   else
	   {
	       if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
	           confInd = TradePortalConstants.INDICATOR_YES;
	       else
	           confInd = thisUser.getAttribute("subsid_confidential_indicator");  
	   }
	   if (InstrumentServices.isBlank(confInd))
	      confInd = TradePortalConstants.INDICATOR_NO;
	   
	/* Prateep Gedupudi 24/02/2013 getting confidential indicatior for the logged in user End*/
	  
	//Variables needed for the sql statements and the returned number of rows.
	  CorporateOrganizationWebBean org1  = null;
	  try {
	     org1 = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	    org1.getById(userOrgOid);
	    
	   }
	  catch(Exception e){
	    System.out.println("************** Unable to build the user object because" +
	      " the userOid on the UserSession is null, which caused the" +
	      " getDataFromAppserver to fail!  ***************");
	  }

		
	
	if (!TradePortalConstants.ADMIN.equals(currentSecurityType)) {
		  StringBuffer sqlQuery = new StringBuffer();
		    // This is the query used for populating the option drop down list; 
		    // it retrieves all active child organizations that belong to the user's 
		    // org in addition to the user's org itself.  It is used by both the mail
		    // and notification.
		    sqlQuery.append("select organization_oid, name");
		    sqlQuery.append(" from corporate_org");
		    sqlQuery.append(" where activation_status = '");
		    sqlQuery.append(TradePortalConstants.ACTIVE);
		    sqlQuery.append("' start with organization_oid = ?");
		    sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
		    sqlQuery.append(" order by ");
		    sqlQuery.append(resMgr.localizeOrderBy("name"));
		    hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{userOrgOid});
		    totalOrganizations = hierarchyDoc.getFragments("/ResultSetRecord").size();
	}

	 String currentpanelAuthTranWork = MY_WORK;
     String currentpanelAuthTranGroup = TradePortalConstants.INSTR_GROUP__ALL;
     String currentpanelAuthTranInstrType = ALL;
     String currentpanelAuthTranStatus = TradePortalConstants.STATUS_ALL;

     StringBuffer panelAuthTranWork = new StringBuffer();
     // If the user has rights to view child organization work, retrieve the
     // list of dropdown options from the query listview results doc (containing
     // an option for each child org) otherwise, simply use the user's org
     // option to the dropdown list (in addition to the default 'My Work' option).
//IR NIUN012952035
	 StringBuffer panelAuthdropdownOptions = new StringBuffer();
	 StringBuffer prependText = new StringBuffer();
		prependText.append(resMgr.getText("PendingTransactions.WorkFor", TradePortalConstants.TEXT_BUNDLE));
		prependText.append(" ");
	
	 	String defaultValue = "";
	    String defaultText  = "";
	    String selectedWorkflow="";
	    selectedWorkflow = request.getParameter("workflow");
	      if (selectedWorkflow == null)
	      {
	         selectedWorkflow = (String) session.getAttribute("workflow");
	         if (selectedWorkflow == null)
	         {
	            if (defaultWIPView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
	            {
	               selectedWorkflow = userOrgOid;
	            }
	            else if (defaultWIPView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
	            {
	               selectedWorkflow = ALL_WORK;
	            }
	            else
	            {
	               selectedWorkflow = MY_WORK;
	            }
	            selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
	         }
	      }

	      session.setAttribute("workflow", selectedWorkflow);

	      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());   
	    if(!userSession.hasSavedUserSession())
	     { 
	      defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
	       defaultText = MY_WORK;
	       
	       panelAuthdropdownOptions.append("<option value=\"");
	       panelAuthdropdownOptions.append(defaultValue);
	       panelAuthdropdownOptions.append("\"");
	       
	       if (selectedWorkflow.equals(defaultText))
	       {
	    	   panelAuthdropdownOptions.append(" selected");
	       }
	
	       panelAuthdropdownOptions.append(">");
	       panelAuthdropdownOptions.append(resMgr.getText( defaultText, TradePortalConstants.TEXT_BUNDLE));
	       panelAuthdropdownOptions.append("</option>");
	     }

       // If the user has rights to view child organization work, retrieve the 
       // list of dropdown options from the query listview results doc (containing
       // an option for each child org) otherwise, simply use the user's org 
       // option to the dropdown list (in addition to the default 'My Work' option).

       if (SecurityAccess.hasRights(loginRights, 
                                    SecurityAccess.VIEW_CHILD_ORG_WORK))
       {
       	panelAuthdropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                              "ORGANIZATION_OID", "NAME", 
                                                              prependText.toString(),
                                                              selectedWorkflow, userSession.getSecretKey()));
          // Only display the 'All Work' option if the user's org has child organizations
          if (totalOrganizations > 1)
          {
       	   panelAuthdropdownOptions.append("<option value=\"");
       	   panelAuthdropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
       	   panelAuthdropdownOptions.append("\"");

             if (selectedWorkflow.equals(ALL_WORK))
             {
                panelAuthdropdownOptions.append(" selected");
             }

             panelAuthdropdownOptions.append(">");
             panelAuthdropdownOptions.append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
             panelAuthdropdownOptions.append("</option>");
          }
       }
       else
       {   panelAuthdropdownOptions.append("<option value=\"");
       	panelAuthdropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
       	panelAuthdropdownOptions.append("\"");

          if (selectedWorkflow.equals(userOrgOid))
          {
       	   panelAuthdropdownOptions.append(" selected");
          }
          panelAuthdropdownOptions.append(">");
          panelAuthdropdownOptions.append(prependText.toString());
          panelAuthdropdownOptions.append(userSession.getOrganizationName());
          panelAuthdropdownOptions.append("</option>");
       }
	    String panelAuthTranGroups =null;
 	  panelAuthTranGroups = Dropdown.createSortedRefDataOptions("PANEL_INST_GROUP",TradePortalConstants.PANEL_INSTR_GROUP__ALL, loginLocale);
 	  

     StringBuffer panelAuthTranInstrTypes = new StringBuffer();
     panelAuthTranInstrTypes.append("<option value='").append(ALL).append("'").
       append(" selected >").
       append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
       append("</option>");
     //popupate the transaction type drop down with all instrument types based on what the user can see
     Vector viewableInstruments = Dropdown.getViewableInstrumentTypes(formMgr,
       new Long(userOrgOid), loginRights, userSession.getOwnershipLevel(), new Long(userOid));
     String viewableInstrTypes = Dropdown.getInstrumentList("",  //selected instr type
       loginLocale, viewableInstruments );
     panelAuthTranInstrTypes.append(viewableInstrTypes);

     StringBuffer panelAuthTranStatus = new StringBuffer();
     panelAuthTranStatus.append("<option value='").append(ALL).append("'").
       append(">").
       append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
       append("</option>");
     panelAuthTranStatus.append("<option value='").
       append(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED).
       append("'>").
       append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED, loginLocale)).
       append("</option>");
     panelAuthTranStatus.append("<option value='").
       append(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED).
       append("'>").
       append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED, loginLocale)).
       append("</option>");
     panelAuthTranStatus.append("<option value='").
       append(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE).
       append("'>").
       append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE, loginLocale)).
       append("</option>");
     
     StringBuffer panelAuthTransactions = new StringBuffer();
     panelAuthTransactions.append("<option value='").append(ALL_WORK).append("'").
       append(">").
       append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE)).
       append("</option>");
     panelAuthTransactions.append("<option value='").append(MY_WORK).
       append("'>").
       append(resMgr.getText(MY_WORK, TradePortalConstants.TEXT_BUNDLE)).
       append("</option>");
     
     
	  userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

  //cquinton 1/18/2013 set return page
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); //to keep it correct
  }
  else {
    userSession.addPage("goToAllTransactionsHome");
  }
%>


<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<div class="pageMain">
<div class="pageContent">
			<div class="secondaryNav">	
              <div class="secondaryNavTitle">
                <%=resMgr.getText("SecondaryNavigation.PendingPanelAuthorisationTransactions", TradePortalConstants.TEXT_BUNDLE)%>
              </div>
              <div class="secondaryNavHelp" id="secondaryNavHelp">
                <%=OnlineHelp.createContextSensitiveLink("customer/all_transactions.htm",  resMgr, userSession)%>
              </div>
              <%=widgetFactory.createHoverHelp("secondaryNavHelp", "HelpImageLinkHoverText") %>
              <div style="clear:both;"></div>
            </div>

<div class="formContentNoSidebar">

<%-- Added by Sandeep 12/06/2012 - Start --%>
<%--As per the design change a search button and a new field Instrument Id is added--%>
   <div class="gridSearch">
    <div class="searchHeader">
      <span class="searchHeaderCriteria">
        <%= widgetFactory.createSearchSelectField("PanelAuthTranWork","AllTransactions.Show", "",
        		panelAuthdropdownOptions.toString(), " onChange='searchAllTran()'" )%>
          
        <%=widgetFactory.createSearchSelectField( "PanelAuthTransactions", "Home.PanelAuthTransactions.criteria.MyTransactions", "",
             panelAuthTransactions.toString(), " onChange='searchAllTran()'" )%>
          
        <%= widgetFactory.createSearchSelectField( "PanelAuthTranInstrType", "AllTransactions.criteria.InstrumentType", "", 
        		panelAuthTranInstrTypes.toString(), " class=\"char16\" onChange='searchAllTran()'") %>         
      </span>
      <span class="searchHeaderActions">
        <jsp:include page="/common/dGridShowCount.jsp">
          <jsp:param name="gridId" value="PanelAuthTranGrid" />
        </jsp:include>
        
        <span>
	      	<button data-dojo-type="dijit.form.Button" type="button" id="AllTransSearchButton">
	           	<%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	           	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	        		searchAllTran();return false;
	       		</script>
		    </button>
		</span>
	    <%=widgetFactory.createHoverHelp("AllTransSearchButton","SearchHoverText") %>
	    
        <span id="allTranRefresh" class="searchHeaderRefresh"></span>
        <%=widgetFactory.createHoverHelp("allTranRefresh", "RefreshImageLinkHoverText") %>
        
        <span id="allTranGridEdit" class="searchHeaderEdit"></span>
        <%=widgetFactory.createHoverHelp("allTranGridEdit", "CustomizeListImageLinkHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>
  </div>

    <div class="gridSearch">
    	<div class="searchHeader">
	      <span class="searchHeaderCriteria">
	        <%= widgetFactory.createSearchSelectField( "PanelAuthTranGroup", "AllTransactions.criteria.InstrumentGroup", "", 
	        		panelAuthTranGroups, " class=\"char10\" onChange='searchAllTran()'" )%>
	        
	        <%= widgetFactory.createSearchTextField("AllTranInstrumentId","AllTransactions.InstrumentID","30",
	        		"class=\"char11\" onKeydown='searchAllTranOnEnter(event, \"AllTranInstrumentId\");'")%>
	      </span>
      <div style="clear:both;"></div>
    </div>
   </div>
<%--    Added by Sandeep 12/06/2012 - End --%>
<%
  gridHtml = dGridFactory.createDataGrid("PanelAuthTranGrid","HomePanelAuthTransactionsDataGrid",null);
  gridLayout = dGridFactory.createGridLayout("PanelAuthTranGrid", "HomePanelAuthTransactionsDataGrid"); 
%>
  <%= gridHtml %>


</div>
</div>
</div>
<%
Hashtable secureParms = new Hashtable();
secureParms.put("login_oid", userSession.getUserOid());
secureParms.put("UserOid",userSession.getUserOid());
secureParms.put("login_rights", loginRights);
secureParms.put("SecurityRights", userSession.getSecurityRights());
%>

<form method="post" id="Transaction-NotifyPanelUserForm" name="Transaction-NotifyPanelUserForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden name="buttonName" value="">
<input type=hidden name="transaction_oid" value=""> 
<input type=hidden name="instrument_oid" value="">  
<%= formMgr.getFormInstanceAsInputField("Transaction-NotifyPanelUserForm", secureParms) %>
 </form>
 
 <jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%--the following script loads the datagrid.  note that this dialog
    is a DialogSimple to allow scripts to execute after it loads on the page--%>
<script type='text/javascript'>
	require(["t360/OnDemandGrid", "dojo/dom-construct", "dojo/aspect","dijit/registry","dojo/ready", "t360/datagrid"], 
			function(onDemandGrid, domConstruct, aspect,registry,ready,t360grid){
		ready(function(){
			<%--  Moving page layout to XML  --%>
			var gridLayout = <%=gridLayout%>;<%---[
			      		{name:"Instrument ID", field:"InstrumentID", fields:["InstrumentID","InstrumentID_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"100px"} ,
			      		{name:"Instrument Type", field:"InstrumentType", width:"150px"} ,
			      		{name:"Transaction", field:"Transaction", fields:["Transaction","Transaction_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"120px"} ,
			      		{name:"CCY", field:"Currency", width:"40px"} ,
			      		{name:"Amount", field:"Amount", width:"70px", cellClasses:"gridColumnRight"} ,
			      		{name:"Status", field:"Status", width:"170px"} ,
			      		{name:"Party", field:"Party", width:"180px"} ,
			      		{name:"Primary Reference", field:"ApplicantsRefNo", width:"150px"} ,
			      		{name:"Date Started", field:"DateStarted", width:"85px"} ,
			      		{name:"Vendor ID", field:"VendorId", width:"80px"} ,
			      		{name:"Action", field:"Action", fields:["Action","InstrumentID"], formatter:viewAuthorisationsFormatter, width:"150px"} ]; --%>

			           	
			var iType=registry.byId("PanelAuthTranInstrType").get('value');            
			var grp=registry.byId("PanelAuthTranGroup").get('value');
			var initSearchParms = "work=<%=EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey())%>&confInd=<%=confInd%>&myTransactions=<%=ALL_WORK%>&status=<%=ALL%>&iType="+iType+"&grp="+grp;
			var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PanelAuthTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
			var allTransGrid = onDemandGrid.createOnDemandGrid("PanelAuthTranGrid", viewName, gridLayout, initSearchParms,"0");
			
			 <%-- MEerupula Rel 8.3 T36000021939 Disable sorting on Action column in the grid as sorting is not required. STARTS HERE --%>  
		       var fieldNamePanelAuth="Action";
		       var index = -1;         
		       allTransGrid.canSort = function(col){    
		       		index =t360grid.getColumnIndexWithName(allTransGrid, fieldNamePanelAuth); 
		           	if(Math.abs(col) === index) { return false; }
		               else{return true; }
		       };
		       
		     <%-- MEerupula Rel 8.3 T36000021939 Made Action column in the grid non-sortable. ENDS HERE --%>
			
		});
	});
	
	 <%-- Added by Sandeep 12/06/2012 - Start  --%>
	function searchAllTranOnEnter(event, fieldId){
	
	  	if (event && event.keyCode == 13) {
        	searchAllTran();
        }
		<%-- By Sandeep - 01/11/2013 - End --%>
	}
	 <%-- Added by Sandeep 12/06/2012 - End  --%>

	function searchAllTran() {
	  require(["t360/OnDemandGrid", "t360/datagrid","dijit/registry","dojo/dom","dojo/ready", "dojo/domReady!"],
	      function(onDemandGridSearch, datagrid,registry,dom,ready){
		  ready(function (){
			  var searchParms = "";
		         var panelAuthTranWorkSelect = registry.byId("PanelAuthTranWork");
		         var panelAuthTranWorkOid = panelAuthTranWorkSelect.get('value');
		         if ( panelAuthTranWorkOid && panelAuthTranWorkOid.length > 0 ) {
		           searchParms += "work="+panelAuthTranWorkOid;
		         }
		         var panelAuthTranGroupSelect = registry.byId("PanelAuthTranGroup");
		         var panelAuthTranGroup = panelAuthTranGroupSelect.get('value');
		         if ( panelAuthTranGroup && panelAuthTranGroup.length > 0 ) {
		           if ( searchParms && searchParms.length > 0 ) {
		             searchParms += "&";
		           }
		           searchParms += "grp="+panelAuthTranGroup;
		         }
		         var panelAuthTranInstrTypeSelect = registry.byId("PanelAuthTranInstrType");
		         var panelAuthTranInstrType = panelAuthTranInstrTypeSelect.get('value');
		         if ( panelAuthTranInstrType && panelAuthTranInstrType.length > 0 ) {
		           if ( searchParms && searchParms.length > 0 ) {
		             searchParms += "&";
		           }
		           searchParms += "iType="+panelAuthTranInstrType;
		         }
		         var panelAuthTransactionsSelect = registry.byId("PanelAuthTransactions");
		         var panelAuthTransactions = panelAuthTransactionsSelect.get('value');
		         if ( panelAuthTransactions && panelAuthTransactions.length > 0 ) {
		           if ( searchParms && searchParms.length > 0 ) {
		             searchParms += "&";
		           }
		           searchParms += "myTransactions="+panelAuthTransactions;
		         }
		      
		      var instrumentId = dom.byId("AllTranInstrumentId").value;
		      searchParms += "&instrumentId="+instrumentId+"&confInd=<%=confInd%>";
		      onDemandGridSearch.searchDataGrid("PanelAuthTranGrid", "PanelAuthTransactionsDataView", searchParms);
		  });
		  
	    });
	}
  
	require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	    function(query, on, t360grid, t360popup){
	  query('#allTranRefresh').on("click", function() {
	    searchAllTran();
	  });
	  query('#allTranGridEdit').on("click", function() {
	    var columns = t360grid.getColumnsForCustomization('PanelAuthTranGrid');
	    var parmNames = [ "gridId", "gridName", "columns" ];
	    var parmVals = [ "PanelAuthTranGrid", "AllTransactionsDataGrid", columns ];
	    t360popup.open(
	      'allTranGridEdit', 'dGridCustomizationPopup.jsp',
	      parmNames, parmVals,
	      null, null);  <%-- callbacks --%>
	  });
	});
	
	var viewAuthorisationsFormatter=function(columnValues) {
		var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
		var gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"PanelAuthTranGrid\",\"TradeCash\");'>"+"<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisations", TradePortalConstants.TEXT_BUNDLE)%>"+"</a>";
	    return gridLink;
	}

	

</script>

<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="PanelAuthTranGrid" />
</jsp:include>

</body>
</html>
