<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, java.util.*, 
                 com.ams.tradeportal.common.*,java.io.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<%
	
   String nextPhysicalPage = null;

   try
   {
      ServletContext context = this.getServletConfig().getServletContext();

      response.setContentType("text/html");

      RequestDispatcher requestDispatcher = context.getRequestDispatcher("/PurchaseOrderFileUploadServlet");

      requestDispatcher.include(request, response);

      NavigationManager nif = NavigationManager.getNavMan();

      nextPhysicalPage = nif.getPhysicalPage(formMgr.getCurrPage(), request);
      Debug.debug("nextPhysicalPage===="+nextPhysicalPage);
   }
   catch (Exception e)
   {
      System.out.println("Exception caught in PurchaseOrderFileUploadServlet.jsp");
      ByteArrayOutputStream bytes = new ByteArrayOutputStream();
      PrintWriter writer = new PrintWriter(bytes, true);
      e.printStackTrace(writer);
      System.out.println(bytes.toString());
   }
%>
 
<jsp:forward page='<%= nextPhysicalPage %>' />
<%--<jsp:forward page='/uploads/UploadsHome.jsp' />
--%>