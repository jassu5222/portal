<%--

**********************************************************************************

  Description:
     

This page is called by one Ajax Call from the Transaction Pending list view and 
Cash Management transaction detail pages.
**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,
	com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,
	com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.*,com.ams.tradeportal.mediator.premediator.*,com.ams.tradeportal.agents.*,com.ams.tradeportal.busobj.util.*,java.util.*"%>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session"></jsp:useBean>
<% 




	String selectedCount = request.getParameter("selectedCount") == null ? "0" : request.getParameter("selectedCount");	
    String selected = request.getParameter("selected") == null ? "" : request.getParameter("selected");	

	String transactionOid = null;
	String instrumentOid = null;
	String transactionType = null;	
	String userOid = userSession.getUserOid();
	String client_bankOID = userSession.getClientBankOid();
    String corp_oid = userSession.getOwnerOrgOid();
	boolean errorOccured = false;
	
	MediatorServices medService = new MediatorServices();
	IssuedError issuedError = null;
	IssuedError timedOutError =  medService.getErrorManager().findErrorCode(TradePortalConstants.FX_GET_RATE_NO_RESPONSE,userSession.getUserLocale());
	
	DocumentHandler doc = null;
		   try {
		    int selCount = Integer.parseInt(selectedCount);
		    if (selCount < 1) {
			   throw new AmsException(medService.getErrorManager().findErrorCode(TradePortalConstants.FX_GET_RATE_NO_ITEM_SELECTED,userSession.getUserLocale()));
			}
			if (selCount > 1) {
			   throw new AmsException(medService.getErrorManager().findErrorCode(TradePortalConstants.FX_GET_RATE_MULTIPLE_ITEMS_SELECTED,userSession.getUserLocale()));
			}
			
			selected=EncryptDecrypt.decryptStringUsingTripleDes(selected, userSession.getSecretKey());

			String[] arr=selected.split("/");
			transactionOid = arr[0];
			instrumentOid = arr[1];
	        transactionType = arr[2];

			
		    ClientBank clientBank = (ClientBank)EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "ClientBank");
			clientBank.setClientServerDataBridge(resMgr.getCSDB());
			clientBank.getDataFromReferenceCache(Long.parseLong(client_bankOID));
			
          
			CorporateOrganization corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "CorporateOrganization",Long.parseLong(corp_oid));
			
			TransactionWebBean transaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");
	        transaction.getById(transactionOid);
			
			
			if (!transaction.isValid()) {
			  throw new AmsException("error occured retrieving transaction, transactionid: " + transactionOid);
			}
			
                        //validate transaction status
			if (!(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED.equals(transaction.getAttribute("transaction_status")) ||
			    TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE.equals(transaction.getAttribute("transaction_status")))) {
                           throw new AmsException(medService.getErrorManager().findErrorCode(TradePortalConstants.FX_GET_RATE_INVALID_TRANS_STATUS,userSession.getUserLocale()));				
			}
			
			
			boolean canAuthorize = SecurityAccess.canAuthorizeInstrument(userSession.getSecurityRights(), transaction.getAttribute("copy_of_instr_type_code"), transactionType);
        
    
        if (!canAuthorize) {
		         throw new AmsException(medService.getErrorManager().findErrorCode(TradePortalConstants.NO_PERMISSION_TO_AUTH,userSession.getUserLocale()));
		         
        }
		else {
		
		   boolean adminCustAccess = false;
          if (userSession.hasSavedUserSession())       {
			if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType())) 
				adminCustAccess = true;
			else if (TradePortalConstants.INDICATOR_YES.equals((String)request.getAttribute("panelEnabled")))
				adminCustAccess = true;

    	   }
           if  (adminCustAccess) {
		        //  throw new AmsException(medService.getErrorManager().findErrorCode(TradePortalConstants.FX_GET_RATE_INVALID_TRANS_STATUS,userSession.getUserLocale()));				
		   }
		}	

			   doc=(new MarketRateRequest()).queryMarketRate(userOid, transactionOid, clientBank, corpOrg, beanMgr);

		   }
		   catch (Exception ex) {
		       errorOccured = true;
			   String errorCode = TradePortalConstants.FX_GET_RATE_GENERAL_ERR;
		       if (ex instanceof AmsException) {
			       AmsException aex = (AmsException) ex;
			      IssuedError err = aex.getIssuedError();
			      if (err != null) {
				   errorCode = err.getErrorCode();
			      }
			     else {
				   ex.printStackTrace();
			     }
		      }else {
			    ex.printStackTrace();
		      }
		      issuedError = medService.getErrorManager().findErrorCode(errorCode,userSession.getUserLocale());
		   }
		
	   session.setAttribute(MarketRateRequest.FX_GETRATE_RESPONSE,doc);

%>

<% 
if (errorOccured) {
%>
<div id="errorSectionResp">
<div class="errorsAndWarnings">
		<div class="errorText"><%=issuedError.getErrorText()%></div>
		</div>
<%--		
<table width="100%" border="0" cellpadding="0" cellspacing="0" height="30">
      
      <tr>
        
      <td >
						<img src="/portal/images/Error.gif" alt="<%= resMgr.getText("ErrorSection.ErrorMessage", TradePortalConstants.TEXT_BUNDLE)%>"/> 
						</td> <td>
						
			<img src="/portal/images/Blank_15.gif" />
 	  </td>
	  <td width="100%" class="ErrorText"> <%=issuedError.getErrorText()%></td>
               
      </tr>
  </table>
  --%>
  </div>
<% 
} else {
%>

<table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
<tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
      </tr>
      <tr>
         <td colspan="2" style="padding-left: 15px;">
            <span class="ControlLabel"><%= resMgr.getText("MarketRate.selection",TradePortalConstants.TEXT_BUNDLE) %> </span>
         </td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
      </tr>
      <tr>
         <td colspan="2" style="padding-left: 15px;">
                           <span class="ControlLabel"><%= resMgr.getText("MarketRate.fxRateQuot",TradePortalConstants.TEXT_BUNDLE) %></span> <span class="ListText"> <%=doc.getAttribute("/FXRate")%>  </span>
         </td>
      </tr>
      <tr>    
         <td colspan="2" style="padding-left: 15px;">
      		<%-- MDB PKUL120231929 Rel 7.1 12/6/11 --%>
		<span class="ControlLabel"><%= resMgr.getText("MarketRate.paymentAmt",TradePortalConstants.TEXT_BUNDLE) %></span>  <span class="ListText"><%=TPCurrencyUtility.getDisplayAmountWithoutCommaSeperator(doc.getAttribute("/payAmount"),doc.getAttribute("/payCurrencyCode"),resMgr.getResourceLocale())%> &nbsp; <%=doc.getAttribute("/payCurrencyCode")%>   </span>
         </td>
      </tr>
      <tr>
      <td colspan="2" style="padding-left: 15px;">
	                <span class="ControlLabel"><%= resMgr.getText("MarketRate.equivalentAmt",TradePortalConstants.TEXT_BUNDLE) %></span>  <span class="ListText"><%=doc.getAttribute("/equivalentAmount")%> &nbsp; <%=doc.getAttribute("/equivalentCurrencyCode")%></span>
         </td>
      </tr>
      <tr>
      <td colspan="2" style="padding-left: 15px;">
      	                <span class="ControlLabel"><%= resMgr.getText("MarketRate.timeout",TradePortalConstants.TEXT_BUNDLE) %></span>  <span class="ListText" style="color:red"><span id="countDown"></span> &nbsp; <%= resMgr.getText("MarketRate.seconds",TradePortalConstants.TEXT_BUNDLE) %></span>
               </td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
      </tr>
      <tr>
         <td  style="text-align: center;">
			<button data-dojo-type="dijit.form.Button"  name="fxConfirmButton" id="fxConfirmButton" type="submit" data-dojo-props="" >
				<%=resMgr.getText("common.AcceptFXText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					return fxConfirm();
			 	</script>
			</button>
         </td>
         
         <td  style="text-align: center;">
			<button data-dojo-type="dijit.form.Button"  name="fxRejectButton" id="fxRejectButton" type="button" data-dojo-props="" >
				<%=resMgr.getText("common.RejectFXText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					return dclose();
			 	</script>
			</button>
         </td>
      </tr>
	   <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
      </tr>
       </table>
	   
	   <div id="timedOutResp" style="display:none">
	   <div class="errorsAndWarnings">
		<div class="errorText"><%=timedOutError.getErrorText()%></div>
		</div>
<%-- <table >
      
      <tr>
        
      <td >
						<img src="/portal/images/Error.gif" alt="<%= resMgr.getText("ErrorSection.ErrorMessage", TradePortalConstants.TEXT_BUNDLE) %>"/> 
						</td> 
	  <td>
						
			<img src="/portal/images/Blank_15.gif" />
 	  </td>
	  <td width="100%" class="ErrorText"> <%=timedOutError.getErrorText()%></td>
              
      </tr>
  </table>
--%>
  </div>
	   
<% 
}
%>