
<%--
**********************************************************************************
  Trade Transactions Home

  Description:  
     This page is used as the main Transactions page for trade instruments.

**********************************************************************************
--%> 

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,java.util.Map,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.busobj.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>
 
<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  final String ALL_WORK = "common.allWork";
  final String MY_WORK = "common.myWork";
  
   String            current2ndNav                = null;
   DocumentHandler   hierarchyDoc                 = null;
   StringBuffer      dynamicWhereClause           = new StringBuffer();
   StringBuffer      dropdownOptions              = new StringBuffer();
   StringBuffer      extraTags                    = new StringBuffer();
   StringBuffer      newLink                      = new StringBuffer();
   StringBuffer      onLoad                       = new StringBuffer();
   Hashtable         secureParms                  = new Hashtable();
   String            userOrgAutoLCCreateIndicator = null;
   String            userOrgManualPOIndicator     = null;
   String            userSecurityRights           = null;
   String            userDefaultWipView           = null;
   String 			 userOwnerShipLevel			  = null;
   String            helpSensitiveLink            = null;
   String            userSecurityType             = null;
   String            selectedWorkflow             = null;
   String            selectedStatus               = "";
   String            selectedOrg                  = null;
   String            searchType                   = null;
   String            userOrgOid                   = null;
   String            userOid                      = null;
   String            tabPendingOn                 = TradePortalConstants.INDICATOR_NO;
   String            tabAuthorizedOn              = TradePortalConstants.INDICATOR_NO;
   String            tabHistoryOn                 = TradePortalConstants.INDICATOR_NO;
   String            tabPurchaseOrderOn           = TradePortalConstants.INDICATOR_NO;
   String            formName                     = "TransactionListForm";
   int               totalOrganizations           = 0;
   String            userOrgAutoATPCreateIndicator = null;
   String            userOrgATPManualPOIndicator   = null;
   String authorizeLink = "";
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
   String gridHtml = "";

   // Create a link to the basic or advanced instrument search
   String linkParms   = null;
   String linkText    = null;
   String link        = null;
   String loginLocale = userSession.getUserLocale();
   String loginRights = loginRights = userSecurityRights;

   // These variables are used for Instrument History for the 
   // Advanced and Basic Search logic
   String       options;
   StringBuffer dynamicWhere = dynamicWhereClause;
   StringBuffer orgList = new StringBuffer();

   String instrumentId = "";
   String refNum = "";
   String bankRefNum = "";
   String instrumentType = "";
   String vendorId = "";
   String currency = "";
   String amountFrom = "";
   String amountTo = "";
   String otherParty = "";
   String dayFrom = "";
   String monthFrom = "";
   String yearFrom = "";
   String dayTo = "";
   String monthTo = "";
   String yearTo = "";
   String searchCondition 	= TradePortalConstants.SEARCH_ALL_INSTRUMENTS;  
   Vector instrumentTypes = null;
   boolean showInstrumentDropDown = false;
   StringBuffer newSearchCriteria = new StringBuffer();
   //Trudden IR PYUJ031764918 Add Begin
    String mode = "";
    //Trudden IR PYUJ031764918 End

   //TLE - 09/28/06 - CR381 - Add Begin 
   String poNumber = "";
   String lineItemNumber = "";
   String beneName = "";
   //TLE - 09/28/06 - CR381 - Add End 
   String bankInstrumentId = ""; //CR-1026 Rel 9.3.5

   
   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

   //cquinton 1/18/2013 set return page
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToTradeTransactionsHome");
   }
   
 //IR T36000026794 Rel9.0 07/24/2014 starts
   session.setAttribute("startHomePage", "TradeTransactionHome");
   session.removeAttribute("fromTPHomePage");
  //IR T36000026794 Rel9.0 07/24/2014 End
  
   Map searchQueryMap =  userSession.getSavedSearchQueryData();
   String sort ="";
   String instrStatusType ="";
   Map savedSearchQueryData =null;
   //cquinton 1/18/2013 remove old close action stuff
   
   userOrgAutoLCCreateIndicator = userSession.getOrgAutoLCCreateIndicator();
   userOrgManualPOIndicator     = userSession.getOrgManualPOIndicator();
   userOrgAutoATPCreateIndicator = userSession.getOrgAutoATPCreateIndicator();
   userOrgATPManualPOIndicator     = userSession.getOrgATPManualPOIndicator();
   
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();
   userDefaultWipView = userSession.getDefaultWipView();
   userOwnerShipLevel = userSession.getOwnershipLevel();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";
   
//MEerupula Rel 8.4 IR-23915 Moved it to dataview
 

   current2ndNav = request.getParameter("current2ndNav");
   if (current2ndNav == null)
   {
      current2ndNav = (String) session.getAttribute("tradeTransactions2ndNav");

      if (current2ndNav == null)
      {
         current2ndNav = TradePortalConstants.NAV__PENDING_TRANSACTIONS;
      }
   }


   // This is the query used for populating the workflow drop down list; it retrieves
   // all active child organizations that belong to the user's current org.
   StringBuffer sqlQuery = new StringBuffer();
   sqlQuery.append("select organization_oid, name");
   sqlQuery.append(" from corporate_org");
   sqlQuery.append(" where activation_status = ? start with organization_oid = ? ");
   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
   sqlQuery.append(" order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));
	
 //jgadela  R90 IR T36000026319 - SQL FIX
   Object sqlParamsHierac[] = new Object[2];
   sqlParamsHierac[0] = TradePortalConstants.ACTIVE;
   sqlParamsHierac[1] = userOrgOid;
   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParamsHierac);
	  
   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
   totalOrganizations = orgListDoc.size();

   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current tab as its context
   PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(current2ndNav);

   // ***********************************************************************
   // Data setup for the Pending tab
   // ***********************************************************************
   if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav))
   {
      tabPendingOn = TradePortalConstants.INDICATOR_YES;
      formName     = "TransactionListForm";

      // Based on the statusType dropdown, build the where clause to include one or more
      // statuses.
      selectedStatus = request.getParameter("transStatusType");

      if (selectedStatus == null) 
      {
         selectedStatus = (String) session.getAttribute("transStatusType");
      }
     
      if(!TradePortalConstants.STATUS_READY.equals(selectedStatus) &&
			  !TradePortalConstants.STATUS_PARTIAL.equals(selectedStatus) &&
			  !TradePortalConstants.STATUS_AUTH_FAILED.equals(selectedStatus) &&
			  !TradePortalConstants.STATUS_STARTED.equals(selectedStatus) &&
			  !TradePortalConstants.STATUS_REJECTED_BY_BANK.equals(selectedStatus) &&
				/*Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - Start*/
			  !TradePortalConstants.STATUS_READY_TO_CHECK.equals(selectedStatus) &&
			  !TradePortalConstants.STATUS_REPAIR.equals(selectedStatus)
			  	/*Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - End*/
		){
		  selectedStatus = TradePortalConstants.STATUS_ALL;
	  }
      session.setAttribute("transStatusType", selectedStatus);

      // Based on the workflow dropdown, build the where clause to include transactions at
      // various levels of the organization.
      selectedWorkflow = request.getParameter("workflow");

      if (selectedWorkflow == null)
      {
         if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
         {
            selectedWorkflow = userOrgOid;
         }
         else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
         {
        	 if( totalOrganizations > 1){
        		 selectedWorkflow = ALL_WORK;
        	 }else{
        		 selectedWorkflow = userOrgOid;
        	 }            
         }
         else
         {
            selectedWorkflow = MY_WORK;
         }
         selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
      }

      session.setAttribute("workflow", selectedWorkflow);

      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());      

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/instruments_pend_trans.htm",  resMgr, userSession);
    
      onLoad.append("document.TransactionListForm.Workflow.focus();");

   } // End data setup for Pending tab

   // ***********************************************************************
   // Data setup for the Authorized tab
   // ***********************************************************************
   else if (TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav))
   {
      tabAuthorizedOn = TradePortalConstants.INDICATOR_YES;
      formName        = "TransactionListForm";

      // The organization dropdown displays dynamically based on security and the
      // number of orgs.  If it will display, set focus to that field.
      if ((SecurityAccess.hasRights(userSecurityRights, 
                                    SecurityAccess.VIEW_CHILD_ORG_WORK)) 
           && (totalOrganizations > 1)) {
        onLoad.append("document.TransactionListForm.Organization.focus();");
      }

      // Determine the organization to select in the dropdown
      selectedOrg = request.getParameter("org");
      if (selectedOrg == null)
      {
         selectedOrg = (String) session.getAttribute("org");
         if (selectedOrg == null)
         {
            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
         }
      }

      session.setAttribute("org", selectedOrg);

	//PR ER - T36000015639. selectedOrg should be decrypted.
	//Reason - while creating options for the Organization, the selectedOption being sent to the function should always be decrypted
      selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
	  
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/auth_trans.htm",  resMgr, userSession);
   }  // End data setup for Authorized tab.

   // ***********************************************************************
   // Data setup for the History tab
   // ***********************************************************************
   else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav))
   {
      tabHistoryOn = TradePortalConstants.INDICATOR_YES;
      formName     = "TransactionListForm";

      // Determine the organization to select in the dropdown
      selectedOrg = request.getParameter("historyOrg");
      if (selectedOrg == null)
      {
         selectedOrg = (String) session.getAttribute("historyOrg");
         if (selectedOrg == null)
         {
            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
         }
      }

      session.setAttribute("historyOrg", selectedOrg);

    //PR ER - T36000015639. selectedOrg should be decrypted.
  	//Reason - while creating options for the Organization, the selectedOption being sent to the function should always be decrypted
      selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());

      // Check if this is a new Search (based on passed in NewSearch parameter)
      // If so, clear the listview information so it does not
      // retain any page/sort order/ sort column/ search type information 
      // resulting from previous searches
	
      // A new search can also result from the corpo org or status type dropdown 
      // being reselected. In this case, NewDropdownSearch parameter indicates a 
      // pseudo new search -- the search type is retained but the rest of the 
      // search clause is not

      String newSearch = request.getParameter("NewSearch");
      String newDropdownSearch = request.getParameter("NewDropdownSearch");
      Debug.debug("New Search is " + newSearch);
      Debug.debug("New Dropdown Search is " + newDropdownSearch);

      if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
      {
         // Default search type for instrument status on the transactions history page is ACTIVE.
         session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
      }

      searchType   = request.getParameter("SearchType");

      if (searchType == null) 
      {
         if (searchType == null)
         {
            searchType = TradePortalConstants.SIMPLE;
         }

       }

      // depending on whether the search type is Advanced or Basic, display
      // the link, link parameters, link text and help links.
      linkParms = "";
      linkText  = "";

      if (searchType.equals(TradePortalConstants.ADVANCED))
      {
         linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
         link       = formMgr.getLinkAsUrl("goToTransactionsHome", linkParms, response);
         linkText   = resMgr.getText("InstSearch.BasicSearch", TradePortalConstants.TEXT_BUNDLE);

         helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "advanced", resMgr, userSession);
      }
      else
      {
         linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
         link       = formMgr.getLinkAsUrl("goToTransactionsHome", linkParms, response);
         linkText   = resMgr.getText("InstSearch.AdvancedSearch", TradePortalConstants.TEXT_BUNDLE);

         helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "basic", resMgr, userSession);
      }


      // Based on the statusType dropdown, build the where clause to include one or more
      // instrument statuses.
      Vector statuses = new Vector();
      int numStatuses = 0;

      selectedStatus = request.getParameter("instrStatusType");

      if (selectedStatus == null) 
      {
         selectedStatus = (String) session.getAttribute("instrStatusType");
      }

      if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
         statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
         statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
      }

      else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
         statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
         statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
         statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
         statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
         statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
      }

      else // default is ALL (actually not all since DELeted instruments
           // never show up (handled by the SQL in the listview XML)
      {
         selectedStatus = TradePortalConstants.STATUS_ALL;
      }
	
      Debug.debug("statuses --"+statuses);
      session.setAttribute("instrStatusType", selectedStatus);

      // If the desired status is ALL, there's no point in building the where 
      // clause for instrument status.  Otherwise, build it using the vector
      // of statuses we just set up.

      if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus))
      {
         dynamicWhere.append(" and i.instrument_status in (");

         numStatuses = statuses.size();

         for (int i=0; i<numStatuses; i++) {
             dynamicWhere.append("'");
             dynamicWhere.append( (String) statuses.elementAt(i) );
             dynamicWhere.append("'");

             if (i < numStatuses - 1) {
               dynamicWhere.append(", ");
             }
         }

         dynamicWhere.append(") ");
      }
      
      

Debug.debug("JSP QUERY --"+dynamicWhere);

      // Now include a file which reads the search criteria and build the
      // dynamic where clause.
%>
      <%@ include file="/transactions/fragments/InstSearch-InstType.frag" %>
      <%@ include file="/transactions/fragments/InstSearch-SearchParms.frag" %>

     
<%
   } // End data setup for History Tab

//MEer Rel8.4 IR-23915
   
String searchNav = "tradeTranHome."+current2ndNav;
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>
 
<%-- Vshah - 08/02/2012 - Rel8.1 - Portal-refresh --%>
<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

   <%String certAuthURL = ""; //needed for the frag %>
        
<div class="pageMain">
<div class="pageContent">

<%
//IR T36000014931 REL ER 8.1.0.4 BEGIN
boolean anyRequireAuth = false;
	    if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.INSTRUMENT_AUTHORIZE))
        {
            //cr498 setup openReauthenticationWindow.frag
            //cquinton 1/18/2011 Rel 6.1.0 ir#stul011761874 start
            //only include reauthentication stuff if there is some transaction type that requires it
            Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
            DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
            String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
            anyRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
                requireTranAuth, InstrumentAuthentication.ANY_TRAN_AUTH);
            if (anyRequireAuth) { 
                
%>
              <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
         
        <%
        } 
        }
//IR T36000014931 REL ER 8.1.0.4 END	
        %>
<div class="secondaryNav">

  <div class="secondaryNavTitle">
    <%=resMgr.getText("SecondaryNavigation.Instruments",
                      TradePortalConstants.TEXT_BUNDLE)%>
  </div>

  <%
    String pendingSelected    = TradePortalConstants.INDICATOR_NO;
    String authorizedSelected = TradePortalConstants.INDICATOR_NO;
    String inquiriesSelected    = TradePortalConstants.INDICATOR_NO;
    if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav))
    {
      pendingSelected = TradePortalConstants.INDICATOR_YES;
    }
    else if (TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav))
    {
      authorizedSelected = TradePortalConstants.INDICATOR_YES;
    }
    else
    {
      inquiriesSelected = TradePortalConstants.INDICATOR_YES;
    }

    String navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__PENDING_TRANSACTIONS;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Instruments.PendingTransactions" />
    <jsp:param name="linkSelected" value="<%= pendingSelected %>" />
    <jsp:param name="action"       value="goToTradeTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
    <jsp:param name="hoverText"       value="PendingTransHover" />
  </jsp:include>
  <%
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Instruments.AuthorizedTransactions" />
    <jsp:param name="linkSelected" value="<%= authorizedSelected %>" />
    <jsp:param name="action"       value="goToTradeTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
    <jsp:param name="hoverText"       value="AuthTransHover" />
  </jsp:include>
  <%
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__INQUIRIES; 
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Instruments.Inquiries" />
    <jsp:param name="linkSelected" value="<%= inquiriesSelected %>" />
    <jsp:param name="action"       value="goToTradeTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
    <jsp:param name="hoverText"       value="HistoryHover" />
  </jsp:include>

  <div id="secondaryNavHelp" class="secondaryNavHelp">
    <%=helpSensitiveLink%>
  </div>
<%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %> 
  <div style="clear:both;"></div>

</div>
<%  //include a hidden form for new instrument submission
//this is used for all of the new instrument types available from the menu
Hashtable newInstrSecParms = new Hashtable();
newInstrSecParms.put("UserOid", userSession.getUserOid());
newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
%>



<form name="<%=formName%>" method="POST" 
      action="<%= formMgr.getSubmitAction(response) %>">
  <input type=hidden value="" name=buttonName>
 <jsp:include page="/common/ErrorSection.jsp" />
 
 <% //cr498 begin
  if (anyRequireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%>
  <div class="formContentNoSidebar">
<%
	
   if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav)) {
      // Store the user's oid and security rights in a secure hashtable for the form
      secureParms.put("SecurityRights", userSecurityRights);
      secureParms.put("UserOid",        userOid);
      //VS RVU062382912 Adding New parameter value for pending list transactions page
      if (userSession.getSavedUserSession() == null) {
        secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_YES); 
      }
   }

%>

<%
  // Based on the current secondary navigation, display the appropriate HTML

  if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav))
  {
	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){   
		   savedSearchQueryData = (Map)searchQueryMap.get("InstrumentsPendingTransactionsDataView");
	   } 
%>
    <%@ include file="/transactions/fragments/Transactions-PendingListView.frag" %>
<%
  }
  else if (TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav))
  {
	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		   savedSearchQueryData = (Map)searchQueryMap.get("InstrumentsAuthorizedTransactionsDataView");
	   
	   } 
%>
    <%@ include file="/transactions/fragments/Transactions-AuthorizedListView.frag" %>
<%
  }
  else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) 
  {
	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	   savedSearchQueryData = (Map)searchQueryMap.get("InstrumentsInquiriesDataView");
	   
  } 
%>
    <%@ include file="/transactions/fragments/Transactions-HistoryListView.frag" %>
<%
  }  
%>

  </div>
<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>

</form>

<%-- IR T36000014931 REL ER 8.1.0.4  ALWAYS PLACE THESE FORMS AFTER THE PRIMARY FORM. DO NOT CHANGE THE ORDER OF THESE FORMS AS OTHER FUNCTIONALITY WILL BE AFFECTED. --%>
<form method="post" name="NewInstrumentForm1" action="<%=formMgr.getSubmitAction(response)%>">
<input type="hidden" name="bankBranch" />
<input type="hidden" name="transactionType"/>
<input type="hidden" name="instrumentType" />
<input type="hidden" name="copyInstrumentOid" />
<input type="hidden" name="mode" value="CREATE_NEW_INSTRUMENT" />
<input type="hidden" name="copyType" value="Instr" />
  
<%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
</form>
<%
Hashtable newTempSecParms = new Hashtable();
newTempSecParms.put("userOid", userSession.getUserOid());
newTempSecParms.put("securityRights", userSession.getSecurityRights());
newTempSecParms.put("clientBankOid", userSession.getClientBankOid());
newTempSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
newTempSecParms.put("ownerLevel", userSession.getOwnershipLevel());
%>
<form method="post" name="NewTemplateForm1" action="<%=formMgr.getSubmitAction(response)%>">
<input type="hidden" name="name" />
<input type="hidden" name="bankBranch" />
<input type="hidden" name="transactionType"/>
<input type="hidden" name="InstrumentType" />
<input type="hidden" name="copyInstrumentOid" />
<input type="hidden" name="mode"  />
<input type="hidden" name="CopyType" />
<input type="hidden" name="expressFlag"  />
<input type="hidden" name="fixedFlag" />
<input type="hidden" name="PaymentTemplGrp" />
<input type="hidden" name="validationState" />

<%= formMgr.getFormInstanceAsInputField("NewTemplateForm",newTempSecParms) %>
</form>
</div>
</div>

<%--cquinton 3/16/2012 Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton 3/16/2012 Portal Refresh - end--%>
<%--IR 16481--%>
<%@ include file="/common/GetSavedSearchQueryData.frag" %> 
<%
  // load the footer fragments - this is where individual page javascript should be placed
  if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav)) {
	 
%>
    <%@ include file="/transactions/fragments/InstrumentsPendingTransactionFooter.frag" %>
<%
  }
  if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) {
	  
%>

    <%@ include file="/transactions/fragments/Transactions-HistoryListViewFooter.frag" %>
<%
  }
  if (TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav)) {
	  
%>
    <%@ include file="/transactions/fragments/InstrumentsAuthorizedTransactionFooter.frag" %>
<%
  }
%>
 <div id="copySelectedInstrument"></div>
<script type="text/javascript">

<%-- Method added to get the grid selected row details --%>
function getSelectedGridRow(){ 
	require(["dijit/registry","t360/dialog","dojo/on", "t360/datagrid", "dojo/ready","dojo/_base/array"], 
			function(registry,dialog,on,datagrid, ready, baseArray) {
			
			var myGrid1 = registry.byId("instrumentsSearchGridId");
			var myGrid2 = registry.byId("tradePendGrid");
			console.log(myGrid1+"-"+myGrid2);
			if(myGrid1){				
				var items = myGrid1.selection.getSelected();
				baseArray.forEach(items, function(item){
					instrType = item.i.InstrumentTypeCode;
					console.log(instrType);
				});
				
			}else if(myGrid2){				
					var items = myGrid2.selection.getSelected();
					baseArray.forEach(items, function(item){
						if(item && item != null){
							instrType = item.i.InstrumentTypeCode;
							console.log(instrType);
						}
					});
					
			}
			
	});
}

function openCopySelectedDialogHelper(callBackFunction){
    require(["t360/dialog","dijit/registry"], function(dialog,registry) {
    	var selectedRow;
    	if(registry.byId("instrumentsSearchGridId") != undefined){
      		 selectedRow = getSelectedGridRowKeys("instrumentsSearchGridId");
    	}else if(registry.byId("tradePendGrid") != undefined){
    		selectedRow = getSelectedGridRowKeys("tradePendGrid");
    	}
      if(selectedRow.length == 0){
    	<%--  IR T36000017020 -use StringFunction.asciiToUnicode --%>
    	  alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("CopySelected.PromptMessage4",TradePortalConstants.TEXT_BUNDLE)) %>');
      }else if(selectedRow.length >1){
    	<%--  IR T36000017020 -use StringFunction.asciiToUnicode --%>
    	  alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("CopySelected.PromptMessage5",TradePortalConstants.TEXT_BUNDLE)) %>');
      }else {
    	  dialog.open('copySelectedInstrument', '<%=resMgr.getTextEscapedJS("CopySelected.Title",TradePortalConstants.TEXT_BUNDLE) %>',
                  'copySelectedInstrumentDialog.jsp',
                  "instrumentType", instrType, <%-- parameters --%>
                  'select', callBackFunction);
      }	  
    });
}

function copySelectedToInstrument(bankBranchOid, copyType, templateName, templateGroup, flagExpressFixed){
	var rowKeys ;
	 require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
		        function(registry, query, on, dialog ) {
		      <%--get array of rowkeys from the grid--%>
		      if(registry.byId("instrumentsSearchGridId") != undefined){
		    	  	rowKeys = getSelectedGridRowKeys("instrumentsSearchGridId");
		      }else if(registry.byId("tradePendGrid") != undefined){
		    		rowKeys = getSelectedGridRowKeys("tradePendGrid");
		      }
	 });
	if(copyType == 'I'){ 
		if ( document.getElementsByName('NewInstrumentForm1').length > 0 ) {
		    var theForm = document.getElementsByName('NewInstrumentForm1')[0];
		    theForm.bankBranch.value=bankBranchOid;
		    theForm.copyInstrumentOid.value=rowKeys;
		    theForm.submit();
		  }
	}	
	if(copyType == 'T'){ 
		if ( document.getElementsByName('NewTemplateForm1').length > 0 ) {
			var flag = flagExpressFixed.split('/');
			var expressFlag = flag[0];
			var fixedFlag = flag[1];
			var theForm = document.getElementsByName('NewTemplateForm1')[0];
		    theForm.name.value=templateName;
		    theForm.mode.value="<%=TradePortalConstants.NEW_TEMPLATE%>";
		    theForm.CopyType.value="<%=TradePortalConstants.FROM_INSTR%>";
		    theForm.fixedFlag.value=fixedFlag;
		    theForm.expressFlag.value=expressFlag;
		    theForm.PaymentTemplGrp.value=templateGroup;
		    theForm.copyInstrumentOid.value=rowKeys;
		    theForm.validationState.value = "";
		    theForm.submit();
		  }
	}
}

require(["dijit/registry","t360/dialog","dojo/on", "t360/datagrid", "dojo/ready","dojo/_base/array"], 
		function(registry,dialog,on,datagrid, ready, baseArray) {
		ready(function(){
		var myGrid1 = registry.byId("instrumentsSearchGridId");
		console.log(myGrid1);
		if(myGrid1 != undefined){
			myGrid1.on("SelectionChanged", function(){
			var items = this.selection.getSelected();
			baseArray.forEach(items, function(item){
				instrType = item.i.InstrumentTypeCode;
				console.log(instrType);
			});
			});
		}
		});	
});

require(["dijit/registry","t360/dialog","dojo/on", "t360/datagrid", "dojo/ready","dojo/_base/array"], 
		function(registry,dialog,on,datagrid, ready, baseArray) {
		ready(function(){
		var myGrid = registry.byId("instrumentsSearchGridId");
		if(myGrid){
			myGrid.on("SelectionChanged", function(){
			var items = this.selection.getSelected();
			baseArray.forEach(items, function(item){
				instrType = item.i.InstrumentTypeCode; 
			});
			});
			
			myGrid.onCanSelect=function(idx){
				var selectedItem = this.getItem(idx).i;
				var isChild = (selectedItem.CHILDREN == "false");
				if (!isChild)  {
					if (selectedItem.TPSGen == "T") 	{
						registry.byId("CopySelected").setAttribute("disabled",true);
					} else {
						registry.byId("CopySelected").setAttribute("disabled",false);
					}		
				}
			
			   return (!isChild);
			};
		}
		});	
});
</script>

<%-- **************** JavaScript for Pending tab only *********************  --%>

      <script language="JavaScript">

      
         <%--
         // This function is used to display a popup message confirming
         // that the user wishes to delete the selected items in the
         // listview.
         --%>
         function confirmDelete()
         {
        	 <%--  IR T36000017020 -use StringFunction.asciiToUnicode --%>
           var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("PendingTransactions.PopupMessage", 
                                                     TradePortalConstants.TEXT_BUNDLE)) %>";

            if (!confirm(confirmMessage)) <%--  Modified by LOGANATHAN - 10/10/2005 - IR LNUF092749769 --%>
             {
                formSubmitted = false;
                return false;
             }
           else
           {
              return true;
           }
        }

    

      </script>
	  
	   <%-- JavaScript for Instrument History tab to enable form submission by enter.
           event.which works for NetScape and event.keyCode works for IE  --%>
      <script LANGUAGE="JavaScript">
     
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  
   
      </script>
</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   session.setAttribute("tradeTransactions2ndNav", current2ndNav);
%>
