
<%--
*******************************************************************************
                              Approval to Pay Issue Page

  Description:
    This is the main driver for the Approval to Pay Issue page.  It handles data
  retrieval of terms and terms parties (or retrieval from the input document)
  and creates the html page to display all the data for an Approval to Pay.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String onLoad = "";
  String focusField = "BenName";   // Default focus field
  boolean focusSet = false;

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType;
  String transactionStatus;

  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database
  boolean phraseSelected = false;  // Indicates if a phrase was selected.

  DocumentHandler doc;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  // These are local variables used to display the Transport Docs/Shipment section
  // of the transaction page
  String            termsOid                      = "";
  String      shipmentOid                   = "";
  String            bButtonPressed                = "";
  StringBuffer      linkParameters                = null;
  int       numShipments            = 1;
  int               shipmentNumber      = 1;
  int               origShipmentNumber            = 1;
  int iLoop;
  // These are local variables used in the Shipment section for the PO line item
  // buttons and text area box
  DocumentHandler   poLineItemsDoc                = null;
  DocumentHandler   poLineItemDoc                 = null;
  DocumentHandler   poDefinitionsDoc              = null;
  DocumentHandler   poDefinitionDoc               = null;
  StringBuffer      sqlQuery                      = null;
  StringBuffer      link                          = null;
  boolean           hasPOLineItems                = false;
  boolean           hasStructuredPOs              = false;
  boolean           amountUpdatedFromPOs          = false;
  boolean           userCanUploadPOs              = false;
  boolean           userCanAddManualPOs           = false;
  boolean           corpOrgHasMultipleAddresses   = false;
  String            corpOrgOid                    = null;
  String            poLineItemUploadDefinitionOid = "";
  String            userOrgAutoLCCreateIndicator  = null;
  String            userOrgManualPOIndicator      = null;
  String            userCustomerAccessIndicator   = null;
  String            poLineItemBeneficiaryName     = "";
  String            poLineItemCurrency            = "";
  String            poLineItemSourceType          = "";
  Vector            poLineItems                   = null;
  Vector            poDefinitionList              = null;
  String      rejectionIndicator            = "";
  String      rejectionReasonText           = "";

  //TAILE
  String            userOrgAutoATPCreateIndicator  = null;
  String            userOrgATPManualPOIndicator      = null;
  String displayAmount;
  boolean canDisplayPDFLinks = false;

  String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
	String parentOrgID      = userSession.getOwnerOrgOid();
	String bogID            = userSession.getBogOid();
	String clientBankID     = userSession.getClientBankOid();
	String globalID         = userSession.getGlobalOrgOid();
	String ownershipLevel   = userSession.getOwnershipLevel();
  //TAILE

  //cquinton 3/21/2012 portal refresh
  String expiryDate = "";

   String links = "";

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;
  String invoiceDetails ="";
  String invoiceOnlyInd ="";

  // These are the beans used on the page.

  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");
  TermsWebBean terms              = null;

  ShipmentTermsWebBean shipmentTerms   = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
  ShipmentTermsWebBean shipmentTermsOrig = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
  TermsPartyWebBean termsPartySel    = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyBuy    = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyIns    = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyNot    = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyOth    = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
  int ADD_REQ_DOC_DEFAULT_ROW_COUNT = 0;
  List addReqDocList = new ArrayList(ADD_REQ_DOC_DEFAULT_ROW_COUNT);
  AdditionalReqDocWebBean addReqDoc=null;
  Vector vVector;
  int addReqDocRowCount=0;
  if(request.getParameter("numberOfMultipleObjects2")!=null)
	  addReqDocRowCount = Integer.parseInt(request.getParameter("numberOfMultipleObjects2"));
  else
	  addReqDocRowCount = ADD_REQ_DOC_DEFAULT_ROW_COUNT;
//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

  // Get the document from the cache.  We'll may use it to repopulate the
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();

 
/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
  Party Search     exists                       SearchInfo to lookup, and
                                                populate a specific TermsParty
                                                web bean

  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
  Phrase Lookup                                 LookupInfo text (from /Out) to
                                                replace a specific phrase text
                                                in the /In document before
                                                populating

  return from    /In/AddressSearchInfo/Address  doc cache (/In); use /In/Address
  Address Search   SearchPartyType exists       SearchInfo to populate a specific
                                                TermsParty web bean

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction
    mediator
    (error)
******************************************************************************/

  // Assume we get the data from the doc.
  getDataFromDoc = true;



  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }

//if newTransaction, set session variable so it persists
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  }
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }

  //cquinton 1/18/2013 remove old close action behavior

  if (doc.getDocumentNode("/Out/ChangePOs") != null) {

     doc.setAttribute("/In/Terms/ShipmentTermsList/po_line_items",doc.getAttribute("/Out/ChangePOs/po_line_items"));
     doc.setAttribute("/In/Terms/amount", doc.getAttribute("/Out/ChangePOs/amount"));
     doc.setAttribute("/In/Terms/amount_currency_code", doc.getAttribute("/Out/ChangePOs/amount_currency_code"));
     doc.setAttribute("/In/Terms/ShipmentTermsList/goods_description", doc.getAttribute("/Out/ChangePOs/goods_description"));
     //SHR  708 screens start
     //IR T36000018915 start - Party Name is set if its populated as part of adding structured PO/Invoice
     if(StringFunction.isNotBlank(doc.getAttribute("/Out/ChangePOs/name"))){
     doc.setAttribute("/In/Terms/FirstTermsParty/name",doc.getAttribute("/Out/ChangePOs/name"));//SHR CR708 Rel8.1.1
     }
     // Party details are set if its populated as part of adding Invoices
     if(TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/Out/ChangePOs/invoice_only_ind"))){
     //IR T36000018915 end

     doc.setAttribute("/In/Terms/invoice_due_date", doc.getAttribute("/Out/ChangePOs/invoice_due_date"));
     doc.setAttribute("/In/Terms/invoice_details",doc.getAttribute("/Out/ChangePOs/invoice_details"));//SHR CR708 Rel8.1.1

     doc.setAttribute("/In/Terms/invoice_only_ind",doc.getAttribute("/Out/ChangePOs/invoice_only_ind"));//SHR CR708 Rel8.1.1
 	 // IR T36000018148 -when POs are added do not reset party info
     
     }
     else{
	      doc.setAttribute("/In/Terms/invoice_due_date", "");
	      doc.setAttribute("/In/Terms/invoice_details","");
	      doc.setAttribute("/In/Terms/invoice_only_ind",TradePortalConstants.INDICATOR_NO);
     }

     doc.setAttribute("/In/Terms/FirstTermsParty/OTL_customer_id",doc.getAttribute("/Out/ChangePOs/BenOTLCustomerId"));
     //SHR  708 screens end
     //pmitnala 2/27/2013 Changes Begin for IR-T36000014341, PR BMO Issue-IMPORT LC - Expiry Date or the Latest Ship Date were not derived from uploaded PO
     doc.setAttribute("/In/Terms/expiry_date", doc.getAttribute("/Out/ChangePOs/expiry_date"));
     doc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", doc.getAttribute("/Out/ChangePOs/latest_shipment_date"));
     //pmitnala 2/27/2013 Changes End for IR-T36000014341
     getDataFromDoc = true;
     amountUpdatedFromPOs = true;
   }
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     // We have returned from the address search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;
     phraseSelected = true;

     String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
     if (result.equals(TradePortalConstants.INDICATOR_YES)) {
        // Take the looked up and appended phrase text from the /Out section
        // and copy it to the /In section.  This allows the beans to be
        // properly populated.
        String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
        xmlPath = "/In" + xmlPath;

        doc.setAttribute(xmlPath,
                      doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

        // If we returned from the phrase lookup without errors, set the focus
        // to the correct field.  Otherwise, don't set the focus so the user can
        // see the error.
        if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
           focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
           focusSet = true;
        }
     } else {
        // We do nothing because nothing was looked up.  We stil want to get
        // the data from the doc.
     }
  }

  // Get the total number of shipments which helps us determine how many shipment
  // tabs to display
  if (doc.getDocumentNode("/In/numberOfShipments") != null)
  {
    numShipments = Integer.parseInt(doc.getAttribute("/In/numberOfShipments"));
    if (numShipments == 0)
    {
      numShipments = 1;
    }
  }

  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        String termsPartyOid;

        termsPartyOid = terms.getAttribute("c_FirstTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartySel.setAttribute("terms_party_oid", termsPartyOid);
           termsPartySel.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_SecondTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyBuy.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyBuy.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyIns.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyIns.getDataFromAppServer();
        }

        DocumentHandler termsPartyDoc;

        termsPartyDoc = doc.getFragment("/In/Terms/FirstTermsParty");
        termsPartySel.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
        termsPartyBuy.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/ThirdTermsParty");
        termsPartyIns.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        //cquinton 3/21/2012 portal refresh
		expiryDate = doc.getAttribute("/In/Terms/expiry_date");

		invoiceDetails = doc.getAttribute("/In/Terms/invoice_details");
        invoiceOnlyInd = doc.getAttribute("/In/Terms/invoice_only_ind");


        // Get the number of shipment terms associated with this transaction and
        // load the shipment term's web bean with the data contained in the
        // document.  Additionally, retrieve the parties associated with the
        // shipment terms and load the party web beans data.

        // If data is retrieved from the document use the original shipment number which
        // is the shipment tab number before the shipment number was updated (save, delete, etc)
        //shipmentNumber = Integer.parseInt(doc.getAttribute("/In/origShipmentNumber"));
         if (doc.getAttribute("/In/origShipmentNumber") != null)
         {
        	shipmentNumber = Integer.parseInt(doc.getAttribute("/In/origShipmentNumber"));
          if (shipmentNumber == 0)
           {
        	  shipmentNumber = 1;
          }
        }

        shipmentTerms.populateFromXmlDoc(doc, "/In/Terms/ShipmentTermsList", false);
        shipmentOid = shipmentTerms.getAttribute("shipment_oid");

        // Set the page focus to the Transport Docs/Shipment section if the
        // user attempted to add, delete, or change shipments
        bButtonPressed = doc.getAttribute("/In/Update/ButtonPressed");
        if ((bButtonPressed != null) && (
            bButtonPressed.equals(TradePortalConstants.BUTTON_ADD_SHIPMENT)              ||
            bButtonPressed.equals(TradePortalConstants.BUTTON_DELETE_SHIPMENT)           ||
            bButtonPressed.equals(TradePortalConstants.SHIPMENT_TAB)                     ||
            bButtonPressed.equals(TradePortalConstants.BUTTON_ADD_PO_LINE_ITEMS_MANUAL)  ||
            bButtonPressed.equals(TradePortalConstants.BUTTON_EDIT_PO_LINE_ITEMS)))
        {
          focusField = "description";
          focusSet = true;
          onLoad += "location.hash='#TransportDocsShipment" + "';";
        }

        // Set the page focus to the Goods Description section if the
        // user performed an action with POs
        if ((bButtonPressed != null) && (
            bButtonPressed.equals("AddUploadedPOLineItemsButton")              ||
            bButtonPressed.equals("AddPOLineItemsManuallyButton")           ||
            bButtonPressed.equals("AddPOLineItems")                     ||
            bButtonPressed.equals("EditPOLineItemsButton")                     ||
            bButtonPressed.equals("RemovePOLineItemsButton") ))
        {
          focusField = "GoodsDescText";
          focusSet = true;
          onLoad += "location.hash='#GoodsDescrAnchor" + "';";
        }

        // GGAYLE - 03/06/2007 - Back out IR FEUH010745960 because it is causing Shipment Phrases to not work properly.
        // IR AUUH021942084 is for this backout and for finding a solution that will allow both features to work correctly.
        //
        // TLE - 01/08/07 - IR-FEUH010745960 - Add Begin
        // Retrieve shipment terms
        //if (!InstrumentServices.isBlank(shipmentOid))
        //{
          //shipmentTerms.getDataFromAppServer();
        //}
        // TLE - 01/08/07 - IR-FEUH010745960 - Add End
        // GGAYLE - 03/06/2007

        // Retrieve the parties associated with Shipment Terms
        termsPartyOid = shipmentTerms.getAttribute("c_NotifyParty");
        if (!InstrumentServices.isBlank(termsPartyOid))
        {
          termsPartyNot.setAttribute("terms_party_oid", termsPartyOid);
          termsPartyNot.getDataFromAppServer();
        }

        termsPartyOid = shipmentTerms.getAttribute("c_OtherConsigneeParty");
        if (!InstrumentServices.isBlank(termsPartyOid))
        {
          termsPartyOth.setAttribute("terms_party_oid", termsPartyOid);
          termsPartyOth.getDataFromAppServer();
        }

        termsPartyDoc = doc.getFragment("/In/Terms/ShipmentTermsList/NotifyParty");
        termsPartyNot.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/ShipmentTermsList/OtherConsigneeParty");
        termsPartyOth.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

      //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
 		vVector = doc.getFragments("/In/Terms/AdditionalReqDocList");
 		for (iLoop=0; iLoop<vVector.size(); iLoop++)
		   {
 			addReqDoc= beanMgr.createBean(AdditionalReqDocWebBean.class,"AdditionalReqDoc");
		    DocumentHandler additionalReqDocDoc = (DocumentHandler) vVector.elementAt(iLoop);
		    addReqDoc.populateFromXmlDoc(additionalReqDocDoc,"/");
		    addReqDocList.add(addReqDoc);
		   }
		if(vVector.size() < ADD_REQ_DOC_DEFAULT_ROW_COUNT) {
		  for(iLoop = vVector.size(); iLoop < ADD_REQ_DOC_DEFAULT_ROW_COUNT; iLoop++) {
			  addReqDocList.add(beanMgr.createBean(AdditionalReqDocWebBean.class,"AdditionalReqDoc"));
			  }
		}
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

     } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

     String termsPartyOid;

     termsPartyOid = terms.getAttribute("c_FirstTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartySel.setAttribute("terms_party_oid", termsPartyOid);
        termsPartySel.getDataFromAppServer();
     }
     termsPartyOid = terms.getAttribute("c_SecondTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyBuy.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyBuy.getDataFromAppServer();
     }
     termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyIns.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyIns.getDataFromAppServer();
     }

     // Get the number of shipment terms associated with this transaction and
     // load the shipment term's web bean from the database.  Additionally,
     // retrieve the parties associated with the shipment terms and load the
     // party web beans data.
     String shipmentTabParm = request.getParameter("shipmentTabNumber");
     if (!InstrumentServices.isBlank(shipmentTabParm))
     {
       shipmentNumber = Integer.parseInt(shipmentTabParm);
       bButtonPressed = TradePortalConstants.SHIPMENT_TAB;
     }
     else
     {
       if (doc.getDocumentNode("/In/shipmentNumber") != null)
       {
         shipmentNumber = Integer.parseInt(doc.getAttribute("/In/shipmentNumber"));
         if (shipmentNumber == 0)
         {
           shipmentNumber = 1;
         }
       }
     }

     // Determine if the user selected the add, delete, or shipment tab buttons
     termsOid = terms.getAttribute("terms_oid");

     if (doc.getDocumentNode("/In/Update/ButtonPressed") != null)
     {
       bButtonPressed = doc.getAttribute("/In/Update/ButtonPressed");
     }

     if (bButtonPressed.equals(TradePortalConstants.BUTTON_ADD_SHIPMENT))
     {
       // If the user adds a shipment, then we create the new tab's shipment terms
       // by copying the data from the previously selected tab.  Therefore the first
       // step is to retrieve the previous tab's shipment terms.
       numShipments = shipmentTermsOrig.loadShipmentTerms(termsOid, shipmentNumber);

       // Increment the number of shipments since the user has just added one which
       // doesn't exist in the database yet.  The current shipment number is set equal
       // to the total number of shipments; the shipment number will become the
       // selected tab.
       numShipments++;
       shipmentNumber = numShipments;

       shipmentOid = shipmentTermsOrig.getAttribute("shipment_oid");
       if (!InstrumentServices.isBlank(shipmentOid))
       {
         shipmentTermsOrig.getDataFromAppServer();
         shipmentTerms.copy(shipmentTermsOrig);
         shipmentOid = "";
       }
     }
     else
     {
       // If the user deleted a shipment, the current shipment number should
       // be one less than the previous shipment number; the shipment number
       // will become the selected tab.
       if (bButtonPressed.equals(TradePortalConstants.BUTTON_DELETE_SHIPMENT))
       {
         if (shipmentNumber != 1)
         {
           shipmentNumber--;
         }
       }
       // Populate the shipment terms
       numShipments = shipmentTerms.loadShipmentTerms(termsOid, shipmentNumber);
       shipmentOid = shipmentTerms.getAttribute("shipment_oid");
       if (!InstrumentServices.isBlank(shipmentOid))
       {
         shipmentTerms.getDataFromAppServer();
       }
     }

     // Set the page focus to the Transport Docs/Shipment section if the
     // user has added, deleted, or changed shipments
     if (bButtonPressed.equals(TradePortalConstants.BUTTON_ADD_SHIPMENT)    ||
           bButtonPressed.equals(TradePortalConstants.BUTTON_DELETE_SHIPMENT) ||
           bButtonPressed.equals(TradePortalConstants.SHIPMENT_TAB))
     {
       focusField = "description";
       focusSet = true;
       onLoad += "location.hash='#TransportDocsShipment" + "';";
     }

     // If the user is returning from the Manual PO Details Entry Page
     // set the focus to the Goods Description
     if (!InstrumentServices.isBlank(request.getParameter("updatedPODetails")) ||
          doc.getDocumentNode("/Out/updatedPODetails") != null)
     {
       focusField = "GoodsDescText";
       focusSet = true;
       onLoad += "location.hash='#GoodsDescrAnchor" + "';";
     }

     termsPartyOid = shipmentTerms.getAttribute("c_NotifyParty");
     if (!InstrumentServices.isBlank(termsPartyOid)) {
        termsPartyNot.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyNot.getDataFromAppServer();
     }

     termsPartyOid = shipmentTerms.getAttribute("c_OtherConsigneeParty");
     if (!InstrumentServices.isBlank(termsPartyOid)) {
        termsPartyOth.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyOth.getDataFromAppServer();
     }

     //cquinton 3/21/2012 portal refresh
     expiryDate = terms.getAttribute("expiry_date");
     invoiceDetails = terms.getAttribute("invoice_details");//SHR PR CR708
     invoiceOnlyInd = terms.getAttribute("invoice_only_ind");//SHR PR CR708
   //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
	 vVector=null;
	 QueryListView queryListView = null;
	try{
			StringBuffer sql = new StringBuffer();
			queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
            sql.append("select addl_req_doc_oid, addl_req_doc_ind, addl_req_doc_name, addl_req_doc_originals, addl_req_doc_copies, addl_req_doc_text, p_terms_oid  ");
            sql.append(" from ADDTIONAL_REQ_DOCS ");
            sql.append(" where p_terms_oid = ? ");
          //Leelavathi IR#T36000011235 Rel-8.2 08/03/2013 Begin
            sql.append(" order by ");
            sql.append(resMgr.localizeOrderBy("addl_req_doc_oid"));
          //Leelavathi IR#T36000011235 Rel-8.2 08/03/2013 End
            queryListView.setSQL(sql.toString(), new Object[]{termsOid});
            queryListView.getRecords();

            DocumentHandler addReqDocOidList = new DocumentHandler();
            addReqDocOidList = queryListView.getXmlResultSet();
          	vVector = addReqDocOidList.getFragments("/ResultSetRecord");
			} catch (Exception e) {
	            e.printStackTrace();
	      } finally {
	            try {
	                  if (queryListView != null) {
	                        queryListView.remove();
	                  }
	            } catch (Exception e) {
	                  System.out.println("Error removing querylistview in BankGroupDetail.jsp!");
	            }
	      }
        for (iLoop=0; iLoop<vVector.size(); iLoop++)
		   {
        	addReqDoc= beanMgr.createBean(AdditionalReqDocWebBean.class,"AdditionalReqDoc");
        	DocumentHandler addReqDocDoc = (DocumentHandler) vVector.elementAt(iLoop);
        	addReqDoc.setAttribute("addl_req_doc_oid", addReqDocDoc.getAttribute("/ADDL_REQ_DOC_OID"));
        	addReqDoc.setAttribute("addl_req_doc_ind", addReqDocDoc.getAttribute("/ADDL_REQ_DOC_IND"));
        	addReqDoc.setAttribute("addl_req_doc_name", addReqDocDoc.getAttribute("/ADDL_REQ_DOC_NAME"));
        	addReqDoc.setAttribute("addl_req_doc_originals", addReqDocDoc.getAttribute("/ADDL_REQ_DOC_ORIGINALS"));
        	addReqDoc.setAttribute("addl_req_doc_copies", addReqDocDoc.getAttribute("/ADDL_REQ_DOC_COPIES"));
        	addReqDoc.setAttribute("addl_req_doc_text", addReqDocDoc.getAttribute("/ADDL_REQ_DOC_TEXT"));
        	addReqDoc.setAttribute("terms_oid", addReqDocDoc.getAttribute("/P_TERMS_OID"));
        	addReqDocList.add(addReqDoc);
		   }

		if(vVector.size() < ADD_REQ_DOC_DEFAULT_ROW_COUNT) {
		  for(iLoop = vVector.size(); iLoop < ADD_REQ_DOC_DEFAULT_ROW_COUNT; iLoop++) {
			  addReqDocList.add(beanMgr.createBean(PmtTermsTenorDtlWebBean.class,"PmtTermsTenorDtl"));
			  }
		}
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

  }

  /*************************************************
  * Load New Party
  **************************************************/
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
      doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null ) {

     // We have returned from the PartyDetailNew page.  If the PartyOid is not null,
     // then based on the returned
     // data, RELOAD one of the terms party beans with the selected party.
     // Set focus of the page regardless whether a party was selected.

     String termsPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
     String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");

     Debug.debug("Returning from party search with " + termsPartyType
           + "/" + partyOid);

     // Use a Party web bean to get the party data for the selected oid.
     DocumentHandler partyDoc = null;
     if (partyOid != null) {
        PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");
        party.setAttribute("party_oid", partyOid);
        party.getDataFromAppServer();

        partyDoc = new DocumentHandler();
        party.populateXmlDoc(partyDoc);

        partyDoc = partyDoc.getFragment("/Party");
     }

     // Based on the party type being returned (which we previously set)
     // reload one of the terms party web beans with the data from the
     // doc.
     if (termsPartyType.equals(TradePortalConstants.ATP_SELLER)) {
        if (partyDoc != null) {
           termsPartySel.loadTermsPartyFromDoc(partyDoc);
           //Krishna IR NOUH080959585 10/04/2007 Begin
           // For a Seller selection, "Insured by Party" should not be populated with the
           //designated bank party  even if there is one.
           //loadDesignatedParty(doc, termsPartyIns, beanMgr); //commented this line
           //Krishna IR NOUH080959585 10/04/2007 End
        }
        focusField = "BenName";
        focusSet = true;
     }
     if (termsPartyType.equals(TradePortalConstants.ATP_BUYER)) {
        if (partyDoc != null) {
           termsPartyBuy.loadTermsPartyFromDoc(partyDoc);
     //rbhaduri - 13th June 06 - IR SAUG051361872
     termsPartyBuy.setAttribute("address_search_indicator", "N");
        }
        focusField = "";
        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";
     }
     if (termsPartyType.equals(TradePortalConstants.INSURING_PARTY)) {
        if (partyDoc != null) {
           termsPartyIns.loadTermsPartyFromDoc(partyDoc);
        }
        focusField = "";
        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";
     }
     if (termsPartyType.equals(TradePortalConstants.NOTIFY_PARTY)) {
        if (partyDoc != null) {
           termsPartyNot.loadTermsPartyFromDoc(partyDoc);
           termsPartyNot.createAddressLine3(resMgr);
        }
        focusField = "NotName";
        focusSet = true;
     }
     if (termsPartyType.equals(TradePortalConstants.OTHER_CONSIGNEE)) {
        if (partyDoc != null) {
           termsPartyOth.loadTermsPartyFromDoc(partyDoc);
           termsPartyOth.createAddressLine3(resMgr);
        }
        focusField = "OthName";
        focusSet = true;
     }

  }

//rbhaduri - 8th August 06 - IR AOUG100368306 - Moved it here from below
BigInteger requestedSecurityRight = SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY;

%>

<%@ include file="fragments/Transaction-PageMode.frag" %>

<%

    //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved the code here from below
    if (isTemplate)
    corpOrgOid = template.getAttribute("owner_org_oid");
    else
    corpOrgOid = instrument.getAttribute("corp_org_oid");

	//jgadela  R90 IR T36000026319 - SQL FIX
	Object sqlParams[] = new Object[1];
	sqlParams[0] = corpOrgOid;

     if (DatabaseQueryBean.getCount("address_oid", "address", "p_corp_org_oid = ?", true, sqlParams) > 0 )
     {
   		 corpOrgHasMultipleAddresses = true;
     }

	//MEer CR-1027
     CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
     corporateOrg.getById(corpOrgOid);	
     String bankOrgGroupOid = corporateOrg.getAttribute("bank_org_group_oid");
     
     BankOrganizationGroupWebBean bankOrganizationGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class,    "BankOrganizationGroup");
     bankOrganizationGroup.getById(bankOrgGroupOid);  
     String selectedPDFType =  bankOrganizationGroup.getAttribute("atp_pdf_type");
     
     if(TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(selectedPDFType)){
  			canDisplayPDFLinks = true;
     }
  /*************************************************
  * Load address search info if we come back from address search page.
  * Reload applicant terms party web beans with the data from
  * the address
  **************************************************/
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     String termsPartyType = doc.getAttribute("/In/AddressSearchInfo/AddressSearchPartyType");
     String addressOid = doc.getAttribute("/In/AddressSearchInfo/AddressOid");

     //rbhaduri - 9th Oct 06 - IR AOUG100368306
     String primarySelected = doc.getAttribute("/In/AddressSearchInfo/ChoosePrimary");

     Debug.debug("Returning from address search with " + termsPartyType
           + "/" + addressOid + ":" + primarySelected );

     // Use a Party web bean to get the party data for the selected oid.
     AddressWebBean address = null;

     //rbhaduri - 9th Oct 06 - IR AOUG100368306 - added PRIMARY case for primary address selection
     if ((addressOid != null||primarySelected != null) && termsPartyType.equals(TradePortalConstants.ATP_BUYER)) {

  if (primarySelected.equals("PRIMARY")) {
    termsPartyBuy.setAttribute("name",corporateOrg.getAttribute("name"));
            termsPartyBuy.setAttribute("address_line_1",corporateOrg.getAttribute("address_line_1"));
            termsPartyBuy.setAttribute("address_line_2",corporateOrg.getAttribute("address_line_2"));
          termsPartyBuy.setAttribute("address_city",corporateOrg.getAttribute("address_city"));
            termsPartyBuy.setAttribute("address_state_province",corporateOrg.getAttribute("address_state_province"));
            termsPartyBuy.setAttribute("address_country",corporateOrg.getAttribute("address_country"));
            termsPartyBuy.setAttribute("address_postal_code",corporateOrg.getAttribute("address_postal_code"));
            termsPartyBuy.setAttribute("address_seq_num","1");
          //Rel9.5 CR1132 Populate userdefinedfields from corporate
			termsPartyBuy.setAttribute("user_defined_field_1", corporateOrg.getAttribute("user_defined_field_1"));
			termsPartyBuy.setAttribute("user_defined_field_2", corporateOrg.getAttribute("user_defined_field_2"));
			termsPartyBuy.setAttribute("user_defined_field_3", corporateOrg.getAttribute("user_defined_field_3"));
			termsPartyBuy.setAttribute("user_defined_field_4", corporateOrg.getAttribute("user_defined_field_4"));
  }
  else { address = beanMgr.createBean(AddressWebBean.class, "Address");
          address.setAttribute("address_oid", addressOid);
          address.getDataFromAppServer();

          termsPartyBuy.setAttribute("name",address.getAttribute("name"));
          termsPartyBuy.setAttribute("address_line_1",address.getAttribute("address_line_1"));
          termsPartyBuy.setAttribute("address_line_2",address.getAttribute("address_line_2"));
        termsPartyBuy.setAttribute("address_city",address.getAttribute("city"));
          termsPartyBuy.setAttribute("address_state_province",address.getAttribute("state_province"));
          termsPartyBuy.setAttribute("address_country",address.getAttribute("country"));
          termsPartyBuy.setAttribute("address_postal_code",address.getAttribute("postal_code"));
          termsPartyBuy.setAttribute("address_seq_num",address.getAttribute("address_seq_num"));
        //Rel9.5 CR1132 Populate userdefinedfields from address
			termsPartyBuy.setAttribute("user_defined_field_1", address.getAttribute("user_defined_field_1"));
			termsPartyBuy.setAttribute("user_defined_field_2", address.getAttribute("user_defined_field_2"));
			termsPartyBuy.setAttribute("user_defined_field_3", address.getAttribute("user_defined_field_3"));
			termsPartyBuy.setAttribute("user_defined_field_4", address.getAttribute("user_defined_field_4"));
        }
  }
     focusField = "";
     focusSet = true;
     onLoad += "location.hash='#" + termsPartyType + "';";

  }


  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");
  transactionOid = transaction.getAttribute("transaction_oid");

  // Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");

  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

  // Now determine the mode for how the page operates (readonly, etc.)
  //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved above
  //BigInteger requestedSecurityRight = SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY;
%>

<%


  // Create documents for the phrase dropdowns. First check the cache (no need
  // to recreate if they already exist).  After creating, place in the cache.
  // (InstrumentCloseNavigator.jsp cleans these up.)

  DocumentHandler phraseLists = formMgr.getFromDocCache(
                                             TradePortalConstants.PHRASE_LIST_DOC);
  if (phraseLists == null) phraseLists = new DocumentHandler();

  DocumentHandler reqdDocList = phraseLists.getFragment("/Required");
  if (reqdDocList == null) {
     reqdDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_DOCREQD,
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/Required", reqdDocList);
  }

  DocumentHandler transportDocList = phraseLists.getFragment("/Transport");
  if (transportDocList == null) {
     transportDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_TRANSPORT,
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/Transport", transportDocList);
  }

  DocumentHandler goodsDocList = phraseLists.getFragment("/Goods");
  if (goodsDocList == null) {
     goodsDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_GOODS,
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/Goods", goodsDocList);
  }

  DocumentHandler addlCondDocList = phraseLists.getFragment("/AddlCond");
  if (addlCondDocList == null) {
     addlCondDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_ADDL_COND,
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/AddlCond", addlCondDocList);
  }

  DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
  if (spclInstrDocList == null) {
     spclInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SPCL_INST,
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/SpclInstr", spclInstrDocList);
  }

  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);

  // Retrieve the currency code  of the transaction

  String currency    = terms.getAttribute("amount_currency_code");

%>


<%
  // The navigation bar is only shown when editing templates.  For transactions
  // it is not shown ti minimize the chance of leaving the page without properly
  // unlocking the transaction.
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  // Auto save the form when time-out if not readonly.
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
%>


<%
String pageTitleKey;
if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
  pageTitleKey = "SecondaryNavigation.NewInstruments";
  if (isTemplate){
       if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
       userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
  }else{
     userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
  }
  }else
       userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
} else {
  pageTitleKey = "SecondaryNavigation.Instruments";
  if (isTemplate){
       if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
       userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
  }else{
     userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
  }
  }else
       userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
}
  String helpUrl = "customer/issue_approval_to_pay.htm";

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<div class="pageMain">
<div class="pageContent">

<%//cr498 begin
  //Include ReAuthentication frag in case re-authentication is required for
  //authorization of transactions for this client
  String certAuthURL = "";
  Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());
  String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
  boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
    requireTranAuth,InstrumentAuthentication.TRAN_AUTH__ATP_ISS);
  if (requireAuth) {
%>
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
  }
  //cr498 end
%>


<%
		  if(isTemplate) {
		  String pageTitle;
		  String titleName;
		  String returnAction;
		  pageTitle = resMgr.getText("common.template", TradePortalConstants.TEXT_BUNDLE);
		  titleName = template.getAttribute("name");
		  StringBuffer title = new StringBuffer();
		  String itemKey = "";
		  title.append(pageTitle);
		  title.append( " : " );
		  if(instrument.getAttribute("instrument_type_code").equals("SLC")){
		  if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingSTandbyLCSimple", TradePortalConstants.TEXT_BUNDLE);
		  }else{
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingStandbyLCDetailed", TradePortalConstants.TEXT_BUNDLE);
		  }
		  title.append(itemKey);
		  }
		  else
		  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE,
		                 instrument.getAttribute("instrument_type_code"),
		                 loginLocale) );
		  title.append( " - " );
		  title.append(titleName );
		  returnAction = "goToInstrumentCloseNavigator";

		  String transactionSubHeader = title.toString();

  		%>
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
				<jsp:param name="helpUrl"  value="<%=helpUrl%>" />
  		</jsp:include>

  		<jsp:include page="/common/PageSubHeader.jsp">
 				 <jsp:param name="titleKey" value="<%= transactionSubHeader%>" />
  				 <jsp:param name="returnUrl" value="<%= formMgr.getLinkAsUrl(returnAction, response) %>" />
		</jsp:include>
		 <%}else{ %>
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="SecondaryNavigation.Instruments.ApprovalToPay" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />

<%} %>
<form id="TransactionATP" name="TransactionATP" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

<input type=hidden value="" name=buttonName>

<%-- error section goes above form content --%>
<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">

<% //cr498 begin
  if (requireAuth) {
%>

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%>

<%
  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

  secureParms.put("instrument_oid",        instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code",  instrumentType);
  secureParms.put("instrument_status",     instrumentStatus);
  secureParms.put("corp_org_oid",  corpOrgOid);

  secureParms.put("transaction_oid",       transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status",    transactionStatus);

  secureParms.put("transaction_instrument_info",
    transaction.getAttribute("transaction_oid") + "/" +
  instrument.getAttribute("instrument_oid") + "/" +
  transactionType);

  // If the terms record doesn't exist, set its oid to 0.
  termsOid = terms.getAttribute("terms_oid");
  if (termsOid == null)
  {
     termsOid = "0";
  }
  secureParms.put("terms_oid", termsOid);

  // Store shipment attributes in secure parms
  secureParms.put("shipment_oid",shipmentOid);
  secureParms.put("numberOfShipments", String.valueOf(numShipments));
  secureParms.put("origShipmentNumber", String.valueOf(shipmentNumber));

  if (isTemplate)
  {
     secureParms.put("template_oid", template.getAttribute("template_oid"));
     secureParms.put("opt_lock",     template.getAttribute("opt_lock"));
  }
%>



<%
  if (isTemplate)
  {
%>

    <%@ include file="fragments/TemplateHeader.frag" %>
<%

  }

%>


<% // [BEGIN] IR-YVUH032343792 - jkok %>
  <%@ include file="fragments/Instruments-AttachDocuments.frag" %>
<% // [END] IR-YVUH032343792 - jkok %>

<%	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
    || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
	<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
     </div>
<%
  }
%>

	<% //CR 821 Added Repair Reason Section
	  StringBuffer repairReasonWhereClause = new StringBuffer();
	  int  repairReasonCount = 0;

		/*Get all repair reason's count from transaction history table*/
		repairReasonWhereClause.append("p_transaction_oid = ?");
		repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
		
		//jgadela  R90 IR T36000026319 - SQL FIX
		Object sqlParams1[] = new Object[1];
		sqlParams1[0] = transaction.getAttribute("transaction_oid");

		Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
		repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParams1);

	if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
			<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
	<%} %>

	<%=widgetFactory.createSectionHeader("1", "ApprovalToPayIssue.Terms") %>
     <%@ include file="fragments/Transaction-ATP-ISS-General.frag" %>
	</div>

  	<%= widgetFactory.createSectionHeader("2", "ApprovalToPayIssue.DocsRequired") %>
		<%@ include file="fragments/Transaction-ATP-ISS-DocsReqd.frag" %>
	</div>

 	<%= widgetFactory.createSectionHeader("3", "ApprovalToPayIssue.TransportDocsShipment") %>
     	<%@ include file="fragments/Transaction-ATP-ISS-TransportDocsShipment.frag" %>
	</div>

  	<%= widgetFactory.createSectionHeader("4", "ApprovalToPayIssue.OtherConditions") %>
     <%@ include file="fragments/Transaction-ATP-ISS-OtherConditions.frag" %>
   	</div>

	<%= widgetFactory.createSectionHeader("5", "ApprovalToPayIssue.ClientBankInstructions") %>
     <%@ include file="fragments/Transaction-ATP-ISS-BankInstructions.frag" %>
    </div>

	<%= widgetFactory.createSectionHeader("6", "ApprovalToPayIssue.InternalInstructions") %>
     <%@ include file="fragments/Transaction-ATP-ISS-InternalInstructions.frag" %>
	</div>
<%
     // Only display the bank defined section if the user is an admin user
     // If not, send the values for the checkbox fields in this section as
     // secure parms (ensure the values are accidentally reset).
     // IR-42689 REL 9.4 SURREWSH Start
     if (userSession.getSecurityType().equals(TradePortalConstants.ADMIN) || userSession.hasSavedUserSession())
     {
%>
	<%= widgetFactory.createSectionHeader("7", "ApprovalToPayIssue.BankDefined") %>
       <%@ include file="fragments/Transaction-ATP-ISS-BankDefined.frag" %>
	</div>
<%
     }


%>
	<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){
		 if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))   {%>
						<%=widgetFactory.createSectionHeader("8", "TransactionHistory.RepairReason", null,true) %>
							<%@ include file="fragments/Transaction-RepairReason.frag" %>
						</div>
					<%}else{%>
						<%=widgetFactory.createSectionHeader("7", "TransactionHistory.RepairReason", null, true) %>
						<%@ include file="fragments/Transaction-RepairReason.frag" %>
					</div>
				<% 	}
	 }%>
</div><%--formContent--%>
</div><%--formArea--%>

			<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="title: '', form: 'TransactionATP'">
						<jsp:include page="/common/Sidebar.jsp">
							<jsp:param name="links" value="<%=links%>" />
							<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
							<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
							<jsp:param name="buttonPressed" value="<%=bButtonPressed%>" />
							<jsp:param name="error" value="<%= error%>" />
							<jsp:param name="formName" value="0" />
							<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
							 <jsp:param name="showLinks" value="true" />
							 <jsp:param name="showApplnForm" value="<%=canDisplayPDFLinks%>"/>
							 <jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
						</jsp:include>
					</div>



  <%@ include file="fragments/PhraseLookupPrep.frag" %>
  <%@ include file="fragments/PartySearchPrep.frag" %>

<%= formMgr.getFormInstanceAsInputField("Transaction-ATPForm", secureParms) %>
<div id="PartySearchDialog"></div>
<div id="AddStructurePODialog"></div>
<div id="RemoveStructurePODialog"></div>
<div id="ViewStructurePODialog"></div>
<input type="hidden" name="selection" />
<input type="hidden" name="ChoosePrimary" />
</form>
</div>
</div>

<jsp:include page="/common/Footer.jsp">
    <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/common/SidebarFooter.jsp"/>

<script>

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
<%
  }
%>
</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>



<%@include file="fragments/DesignatedPartyLookup.frag" %>
<script type="text/javascript">

  var itemid;
  var section;

  function SearchParty(identifierStr, sectionName ,partyType){

    itemid = String(identifierStr);
    section = String(sectionName);
    partyType=String(partyType);
    <%-- itemid = 'OrderingPartyName,OrderingPartyAddress1,OrderingPartyAddress2,OrderingPartyAddress3,OrderingPartyAddress4,OrderingPartyCountry'; --%>

    require(["dijit/registry", "dojo/ready"],
      function(registry, ready ) {
      ready(function() {
        var focusField = registry.byId("BenName");
        if (partyType == "SEL") {
            focusField = registry.byId("BenName");
        }
        if (partyType == "INS") {
            focusField = registry.byId("TransactionCurrency");
        }
        if (partyType == "NOT") {
            focusField = registry.byId("NotName");
        }
        if (partyType == "CSN") {
            focusField = registry.byId("OthName");
        }

        focusField.focus();
      });
    });

    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- cquinton 2/7/2013 --%>
      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
	'PartySearch.jsp',
	['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
        ['selectTransactionParty','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section]); <%-- parameters --%>
    });
  }
<%-- IR T36000017820 -modified to pass currency/BeneName and uploadDefOid to the AddStructuredPO.jsp --%>
function AddPOStructure(){
	  require(["dojo/dom","t360/dialog"], function(dom,dialog) {
		var Currency = dom.byId("TransactionCurrency").value;
	    destroyPurchaseOrderSearchId();
		<%-- IR T36000016949 start --%>
		var benName = '<%=StringFunction.escapeQuotesforJS(poLineItemBeneficiaryName)%>';
		<%if(StringFunction.isBlank(poLineItemBeneficiaryName)){%>
		require(["dijit/registry"], function(registry) {
		benName= registry.byId("BenName").get('value');
		});
		<%}%>
		
		<%-- IR T36000016949 ends --%>
		var dialogName = '<%=resMgr.getText("PurchaseOrder.SearchStructuredPO", TradePortalConstants.TEXT_BUNDLE)%>';
		var parmNames = [ "ins_type", "poLineItemCurrency", "poLineItemBeneficiaryName", "poLineItemUploadDefinitionOid" ];
		<%-- PMitnala Rel 8.3 IR#T36000021934 - Passing poLineItemBeneficiaryName to the method in StringFucntion, to replace special characters --%>
	        var parmVals = [ "ATP", Currency,benName, '<%= poLineItemUploadDefinitionOid%>' ];
			 dialog.open('AddStructurePODialog', dialogName, 'AddStructuredPO.jsp', parmNames,parmVals, <%-- parameters --%>
	                 'select', null);
	  });
	}

	function ViewPOStructure(){
	 require(["t360/dialog"], function(dialog) {

		 destroyPurchaseOrderSearchId();

		 var dialogName = '<%=resMgr.getText("PurchaseOrder.AssignedStructuredPO", TradePortalConstants.TEXT_BUNDLE)%>';
		 dialog.open('ViewStructurePODialog', dialogName, 'ViewStructuredPO.jsp', ['shipmentOid'],['<%= shipmentOid%>'], <%-- parameters --%>
	                'select', null);
	  });
	}

	function RemovePOStructure(){
	  require(["t360/dialog"], function(dialog) {

		destroyPurchaseOrderSearchId();

		var dialogName = '<%=resMgr.getText("PurchaseOrder.AssignedStructuredPO", TradePortalConstants.TEXT_BUNDLE)%>';
		dialog.open('RemoveStructurePODialog', dialogName, 'RemoveStructuredPO.jsp', ['shipmentOid','ins_type'],['<%= shipmentOid%>','ATP'], <%-- parameters --%>
	                 'select', null);
	 });
	}

	function destroyPurchaseOrderSearchId(){

		require(["dijit/registry"], function(registry) {
			var PONum = registry.byId("PONum");
			if(PONum) PONum.destroy();

			var BeneName = registry.byId("BeneName");
			if(BeneName) BeneName.destroy();

			var AmountFrom = registry.byId("AmountFrom");
			if(AmountFrom) AmountFrom.destroy();

			var AmountTo = registry.byId("AmountTo");
			if(AmountTo) AmountTo.destroy();

			var Currency = registry.byId("Currency");
			if(Currency) Currency.destroy();

			var Search = registry.byId("PurchaseOrderSearch");
			if(Search) Search.destroy();

			var Close = registry.byId("AddStructurePODialogCloseButton");
			if(Close) Close.destroy();
		});

	}

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

</script>
