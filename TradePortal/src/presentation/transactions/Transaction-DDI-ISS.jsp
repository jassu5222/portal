<%--
*******************************************************************************
                        Direct Debit 

  Description:    
    This is the main driver for the Direct Debit Instruction page.  It handles 
  data retrieval of terms and parties (or retrieval from the input document)
  and creates the html page to display all the data for a Direct Debit Instruction.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,java.util.*,
	com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
	com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);  // RONC March-14
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

	String onLoad = "";
	String focusField = "CreditAccount"; // Default focus field
	boolean focusSet = true;
	boolean radioSelectSection = false;
	boolean payeeAutoReload = true;
	String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
	String BankSearchAddressTitle =resMgr.getTextEscapedJS("RefDataHome.BankBranchRules.Search",TradePortalConstants.TEXT_BUNDLE);
	
	String parentOrgID      = userSession.getOwnerOrgOid();
	String bogID            = userSession.getBogOid();
	String clientBankID     = userSession.getClientBankOid();
	String globalID         = userSession.getGlobalOrgOid();
	String ownershipLevel   = userSession.getOwnershipLevel();
	
	// Various oid and status info from transaction and instruments used in
	// several places.
	String instrumentOid = "";
	String transactionOid;
	String instrumentType;
	String instrumentStatus;
	String transactionType;
	String transactionStatus;
	String rejectionIndicator = "";
	String rejectionReasonText = "";
	String prevTransStatusFromDoc = null;
	String links = null;
	boolean corpOrgHasMultipleAddresses = false;
	String credit_account_oid="";
	String corpOrgOid = null;
	//cr498 replace certNewLink with certAuthURL
	//String certNewLink = "";
	DocumentHandler accountDoc = null;
	boolean displayMultipleAccounts = false;
	Set accountSet = null;
	String acctNo = "";
    StringBuffer accountSql = new StringBuffer();
    String buttonClicked = request.getParameter("buttonName");

	boolean getDataFromDoc; // Indicates if data is retrieved from the
	// input doc cache or from the database
	boolean instrumentSelected = false; // Indicates if an instrument was selected.
	boolean phraseSelected = false; // Indicates if a phrase was selected.
	boolean isStatusVerifiedPendingFX 	= false;	//VShah - CR564

	DocumentHandler doc;

	String loginLocale = userSession.getUserLocale();
	String loginRights = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

	// Variables used for dealing with a terms party's accounts.
	boolean showPayeeAccts = false;
	String payeeAcctList = "";
	String payerAcctList = "";
	String acctOid = "";
	String acctNum = "";
	String acctCcy = "";
	String payeeAcctNum = "";
	String payeeAcctCcy = "";
	String paymentDateDay = null;
	String paymentDateMonth = null;
	String paymentDateYear = null;
	String displayDPDate = null;
	String formatableDate = null; 

	// Variables used for populating ref data dropdowns.
	String options;
	String defaultText;

	// These are the beans used on the page.
	TransactionWebBean transaction = (TransactionWebBean) beanMgr
			.getBean("Transaction");
	InstrumentWebBean instrument = (InstrumentWebBean) beanMgr
			.getBean("Instrument");
	TemplateWebBean template = (TemplateWebBean) beanMgr
			.getBean("Template");
	TermsWebBean terms = null;

	TermsPartyWebBean payerParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

	TermsPartyWebBean applicantParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

	DomesticPaymentWebBean currentDomesticPayment = beanMgr.createBean(DomesticPaymentWebBean.class, "DomesticPayment");
	

	PaymentPartyWebBean firstIntBank = beanMgr.createBean(PaymentPartyWebBean.class, "PaymentParty");

	PaymentPartyWebBean secondIntBank = beanMgr.createBean(PaymentPartyWebBean.class, "PaymentParty");
					
  //Vshah CR509 Begin					
  PaymentPartyWebBean orderingParty = beanMgr.createBean(PaymentPartyWebBean.class, "PaymentParty");
                               
  PaymentPartyWebBean orderingPartyBank = beanMgr.createBean(PaymentPartyWebBean.class, "PaymentParty");
  //Vshah CR509 End

	// Get the document from the cache.  We'll may use it to repopulate the 
	// screen if returning from another page, a save, validation, or any other
	// mediator called from this page.  Otherwise, we assume an instrument oid
	// and transaction oid was passed in.

	doc = formMgr.getFromDocCache();

	String currentDPOid = null;

	Debug.debug("IAZDebug: Dom Pmt Page returns back from Action: "
			+ doc.getAttribute("/In/Update/ButtonPressed"));
	Debug.debug("IAZDebug: Dom Pmt Oid is: "
					+ doc.getAttribute("/In/DomesticPayment/domestic_payment_oid"));

	//IAZ: The only time we want to load data to the Credit section from the cashed document 
	//     is when it comes from the list at the bottom, not when it returns from the mediator.

	if ((doc != null)
			&& (doc.getAttribute("/In/Update/ButtonPressed") == null)) {
		currentDPOid = doc
				.getAttribute("/In/DomesticPayment/domestic_payment_oid");
	}

	Debug.debug("doc from cache is " + doc.toString());

	/******************************************************************************
	 We are either entering the page or returning from somewhere.  These are the 
	 conditions for how to populate the web beans.  Data comes from either the
	 database or the doc cache (/In section) with some variation.

	 Mode           Condition                      Populate Beans From
	 -------------  ----------------------------   --------------------------------
	 Enter Page     no /In/Transaction in doc      Instrument and Template web
	 beans already populated, get
	 data for Terms and TermsParty
	 web beans from database

	 return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
	 Transaction                                   retrieved from database)
	 mediator
	 (no error)

	 return from    /Error/maxerrorseverity > 0    doc cache (/In)
	 Transaction    
	 mediator
	 (error)

	 ******************************************************************************/

	// Assume we get the data from the doc.
	getDataFromDoc = true;

	 Vector error = null;
	  error = doc.getFragments("/Error/errorlist/error");

	  String maxError = doc.getAttribute("/Error/maxerrorseverity");
	if (maxError != null
			&& maxError.compareTo(String
					.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
		// No errors, so don't get the data from doc.
		getDataFromDoc = false;
	}

	//In the case where reCertification/Authentication req'd on authorized, the page is reloaded with error
	//but the status of the transaction actually changed.
	else {
		String errorSectionText = null;
		if (doc.getComponent("/Error") != null) {
			errorSectionText = doc.getComponent("/Error").toString();
		}
		if (errorSectionText != null) {
			if ((errorSectionText
					.indexOf(TradePortalConstants.CERTIFICATE_AUTH_REQ_ERR) != -1)
			) {

				prevTransStatusFromDoc = doc
						.getAttribute("/In/Transaction/transaction_status"); 
				getDataFromDoc = false;
				transaction.getDataFromAppServer();

			}
		}
	}
        //ir cnuk113043991 - check to see if transaction needs to be refreshed
        // if so, refresh it and do not get data from doc as it is wrong
        if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
           transaction.getDataFromAppServer();
           getDataFromDoc = false;
        }

	if (doc.getDocumentNode("/In/Transaction") == null) {
		// No /In/Transaction means we've never looked up the data.
		Debug.debug("No /In/Transaction section - get data from database");
		getDataFromDoc = false;
	}
	if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
	     // We have returned from the party search page.  Get data from doc cache
	     getDataFromDoc = true;
	}
	//Vshah CR509 Begin
	if (doc.getDocumentNode("/In/BankSearchInfo/Type") != null) {
     // We have returned from the Bank search page.  Get data from doc cache
     getDataFromDoc = true;
    }
    //Vshah CR509 End
	
    //ctq - if newTransaction, set session variable so it persists
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }

  //cquinton 1/18/2013 remove old close action behavior

	if (getDataFromDoc) {

		Debug.debug("Populating beans from doc cache");

		// Populate the beans from the input doc.
		try {
			instrument.populateFromXmlDoc(doc.getComponent("/In"));
			transaction.populateFromXmlDoc(doc.getComponent("/In"));
			template.populateFromXmlDoc(doc.getComponent("/In"));

			terms = (TermsWebBean) beanMgr.getBean("Terms");
			terms.populateFromXmlDoc(doc, "/In");
			credit_account_oid= terms.getAttribute("credit_account_oid");
			//out.println("credit_account_oid="+credit_account_oid);
			String termsPartyOid, paymentPartyOid;

			termsPartyOid = terms.getAttribute("c_FirstTermsParty");
			if (termsPartyOid != null && !termsPartyOid.equals("")) {
				payerParty.setAttribute("terms_party_oid",
						termsPartyOid);
				payerParty.getDataFromAppServer();
			}

			termsPartyOid = terms.getAttribute("c_SecondTermsParty");
			if (termsPartyOid != null && !termsPartyOid.equals("")) {
				applicantParty.setAttribute("terms_party_oid",
						termsPartyOid);
				applicantParty.getDataFromAppServer();
			}
			
			//Vshah CR509 Begin
			paymentPartyOid = terms.getAttribute("c_OrderingParty");
	        if (paymentPartyOid != null && !paymentPartyOid.equals(""))
    	    {
        	   orderingParty.setAttribute("payment_party_oid", paymentPartyOid);
           	   orderingParty.getDataFromAppServer();
        	}
        
       	 	paymentPartyOid = terms.getAttribute("c_OrderingPartyBank");
        	if (paymentPartyOid != null && !paymentPartyOid.equals(""))
       		{
           		orderingPartyBank.setAttribute("payment_party_oid", paymentPartyOid);
           		orderingPartyBank.getDataFromAppServer();
       		 }
			//Vshah CR509 End
			

			/*
			termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
			if (termsPartyOid != null && !termsPartyOid.equals("")) {
				termsPartyPayeeBank.setAttribute("terms_party_oid",
						termsPartyOid);
				termsPartyPayeeBank.getDataFromAppServer();
			}
*/
			DocumentHandler termsPartyDoc, paymentPartyDoc;
			
		    //Vshah CR509 Begin
        	paymentPartyDoc = doc.getFragment("/In/DomesticPayment/OrderingParty");
        	orderingParty.loadPaymentPartyFromDocTagsOnly(paymentPartyDoc);  
        
     	    paymentPartyDoc = doc.getFragment("/In/DomesticPayment/OrderingPartyBank");
        	//orderingPartyBank.loadPaymentPartyBankFromDocTagsOnly(paymentPartyDoc); 	//IAZ IR-PMUK012737723 02/03/10 CHF
        	orderingPartyBank.loadPaymentPartyFromDocTagsOnly(paymentPartyDoc);  		//IAZ IR-PMUK012737723 02/03/10	CHT
			//Vshah CR509 End     
		
/*
			termsPartyDoc = doc
					.getFragment("/In/Terms/FirstTermsParty");
			payerParty
					.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

			termsPartyDoc = doc
					.getFragment("/In/Terms/SecondTermsParty");
			payerPartyBank
					.loadTermsPartyFromDocTagsOnly(termsPartyDoc);
			*/
/*
			termsPartyDoc = doc
					.getFragment("/In/Terms/ThirdTermsParty");
			termsPartyPayeeBank
					.loadTermsPartyFromDocTagsOnly(termsPartyDoc);
*/
			if (doc.getAttribute("/Out/DomesticPayment/domestic_payment_oid") == null) {
				currentDomesticPayment.populateFromXmlDoc(doc.getComponent("/In"));
				String curCharges = curCharges = doc.getAttribute("/In/Terms/bank_charges_type");
				if (curCharges != null) {
					currentDomesticPayment.setAttribute("bank_charges_type", curCharges);
				}

				//IAZ CM-451 01/29/09 Begin: PartyName was moved to Domestic Payment
				//Tirms First Party's Name will be used separately
				//currentDomesticPayment.setAttribute("payee_name", 
				//doc.getAttribute("/In/Terms/FirstTermsParty/name"));
				//IAZ CM-451 01/29/09 End

			} else {
				currentDomesticPayment.setAttribute("domestic_payment_oid",	doc.getAttribute("/Out/DomesticPayment/domestic_payment_oid"));
				currentDomesticPayment.getDataFromAppServer();

			}

			displayDPDate = transaction.getAttribute("payment_date");
			if ((displayDPDate != null) && (!displayDPDate.equals(""))) {
				if ((displayDPDate.indexOf("//") != -1)
						|| (displayDPDate.indexOf("-1") != -1)) {
					paymentDateDay = paymentDateMonth = paymentDateYear = "-1";
				} else {
					paymentDateDay = displayDPDate.substring(3, 5);
					paymentDateMonth = displayDPDate.substring(0, 2);
					paymentDateYear = displayDPDate.substring(6, 10);
				}
			} else {
				paymentDateDay = "-1";
				paymentDateMonth = "-1";
				paymentDateYear = "-1";
				displayDPDate = paymentDateMonth + "/" + paymentDateDay
						+ "/" + paymentDateYear + " 00:00:00";
			}
			formatableDate = paymentDateYear + '-' + paymentDateMonth
					+ '-' + paymentDateDay + " 00:00:00.0";
		} catch (Exception e) {
			out.println("Contact Administrator: "
					+ "Unable to reload data after returning to page. "
					+ "Error is " + e.toString());
		}
	} else {
		Debug.debug("populating beans from database");

		// We will perform a retrieval from the database.
		// Instrument and Transaction were already retrieved.  Get
		// the rest of the data
		//terms = (TermsWebBean) beanMgr.getBean("Terms");
		//out.println("terms " + terms);
		//if (terms == null)
		terms = transaction.registerCustomerEnteredTerms();
		credit_account_oid= terms.getAttribute("credit_account_oid");
		//out.println("credit_account_oid="+credit_account_oid);
		Debug.debug("IAZDebug: Dom Pmt Page obtained terms object handler complete");

		String termsPartyOid;
		termsPartyOid = terms.getAttribute("c_FirstTermsParty");
		if (termsPartyOid != null && !termsPartyOid.equals("")) {
			payerParty.setAttribute("terms_party_oid",
					termsPartyOid);
			payerParty.getDataFromAppServer();
		}

		Debug.debug("IAZDebug: Dom Pmt Page Terms Payee Oid is "
				+ termsPartyOid + "<br>");
		Debug.debug("IAZDebug: Dom Pmt Page Terms Payee Name is "
				+ payerParty.getAttribute("name") + "<br>");

		termsPartyOid = terms.getAttribute("c_SecondTermsParty");
		if (termsPartyOid != null && !termsPartyOid.equals("")) {
			applicantParty.setAttribute("terms_party_oid",
					termsPartyOid);
			applicantParty.getDataFromAppServer();
		}

		//out.println("IAZDebug: Dom Pmt Page Terms Payer Oid is "
		//		+ termsPartyOid);
		//out.println("IAZDebug: Dom Pmt Page Terms Payer Name is "
		//		+ applicantParty.getAttribute("name") + "<br>");
/*
		termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
		if (termsPartyOid != null && !termsPartyOid.equals("")) {
			termsPartyPayeeBank.setAttribute("terms_party_oid",
					termsPartyOid);
			termsPartyPayeeBank.getDataFromAppServer();
		}
*/
		 //Vshah CR509 Begin
	 	if (InstrumentServices.isNotBlank(terms.getAttribute("c_OrderingParty")))
	   	 {
   			orderingParty.setAttribute("payment_party_oid", terms.getAttribute("c_OrderingParty"));
   			orderingParty.getDataFromAppServer();
	   	 }
   
   		 if (InstrumentServices.isNotBlank(terms.getAttribute("c_OrderingPartyBank")))
   	 	{
   			orderingPartyBank.setAttribute("payment_party_oid", terms.getAttribute("c_OrderingPartyBank"));
   			orderingPartyBank.getDataFromAppServer();
   		 }
		 //Vshah CR509 End	
		
		String dpDate = transaction.getAttribute("payment_date");

		if ((dpDate == null) || (dpDate.equals(""))) 
		{
			paymentDateDay = "-1";
			paymentDateMonth = "-1";
			paymentDateYear = "-1";
		} else {

			if (dpDate.indexOf("/") != -1) {
				transaction.getDataFromAppServer();
				dpDate = transaction.getAttribute("payment_date");
			}

			paymentDateDay = TPDateTimeUtility.parseDayFromDate(dpDate);
			paymentDateMonth = TPDateTimeUtility
					.parseMonthFromDate(dpDate);
			paymentDateYear = TPDateTimeUtility
					.parseYearFromDate(dpDate);
		}
		displayDPDate = paymentDateMonth + '/' + paymentDateDay + '/'
				+ paymentDateYear + " 00:00:00";
	}

    //Vshah CR509 Begin
    boolean returnFromSearch = false;
  
    /*************************************************
    * Load New Party
    **************************************************/
    if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
        doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null ) {
 
     returnFromSearch = true;
     
     String paymentPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
     String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");

     Debug.debug("Returning from party search with " + paymentPartyType + "/" + partyOid);

     // Use a Party web bean to get the party data for the selected oid.
     PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");
     party.setAttribute("party_oid", partyOid);
     party.getDataFromAppServer();

     DocumentHandler partyDoc = new DocumentHandler();
     party.populateXmlDoc(partyDoc);

     partyDoc = partyDoc.getFragment("/Party");
     
      if (paymentPartyType.equals(TradePortalConstants.PAYER)) {
	     if (partyDoc != null) {
    		 currentDomesticPayment.loadPaymentPartyFromDoc(partyDoc,paymentPartyType); 
    	 }	 	
     	 focusField = "PayeeName";
         focusSet = true;
     	}
     	
       if (paymentPartyType.equals(TradePortalConstants.ORDERING_PARTY)) {
	     if (partyDoc != null) {
    		orderingParty.loadPaymentPartyFromDoc(partyDoc);
    	 }	 	
     	 focusField = "OrderingPartyName";
         focusSet = true;
       }
       
     // IAZ IR-PMUK012737723 02/03/10 Begin
     if (paymentPartyType.equals(TradePortalConstants.ORDERING_PARTY_BANK)) {
	     if (partyDoc != null) {
    		orderingPartyBank.loadPaymentPartyFromDoc(partyDoc);
    	 }	 	
     	 focusField = "OrderingPartyBankCode";
         focusSet = true;
     }
     // IAZ IR-PMUK012737723 02/03/10 End       
     }	
    
	if (doc.getDocumentNode("/In/BankSearchInfo/BankOid") != null) {
  	 returnFromSearch = true;
  	 String bankType = doc.getAttribute("/In/BankSearchInfo/Type");
     String bankOid = doc.getAttribute("/In/BankSearchInfo/BankOid");
     
     BankBranchRuleWebBean bankBranchAddr = beanMgr.createBean(BankBranchRuleWebBean.class, "BankBranchRule");
     bankBranchAddr.setAttribute("bank_branch_rule_oid", bankOid);
     bankBranchAddr.getDataFromAppServer();

     DocumentHandler bankBranchAddrDoc = new DocumentHandler();
     bankBranchAddr.populateXmlDoc(bankBranchAddrDoc);

     bankBranchAddrDoc = bankBranchAddrDoc.getFragment("/BankBranchRule");
     
         	
	 if (bankType.equals(TradePortalConstants.PAYER_BANK)) {
		     currentDomesticPayment.loadPaymentPartyBankFromDoc(bankBranchAddrDoc);
		}	
		
	if (bankType.equals(TradePortalConstants.ORDERING_PARTY_BANK)) {
		     orderingPartyBank.loadPaymentPartyBankFromDoc(bankBranchAddrDoc);
		}		
					
     
  }
  //Vshah CR509 End 
  
  // Vshah ClearParty Start
  if (doc.getDocumentNode("/In/BankToClearInfo/ClearBank") != null) {
  
     //out.println("cleaning... " + doc.getAttribute("/In/BankToClearInfo/Type"));
     returnFromSearch = true;
    
	 String bankType = doc.getAttribute("/In/BankToClearInfo/Type"); 
	  if (bankType.equals(TradePortalConstants.ORDERING_PARTY_BANK)) {
		     orderingPartyBank.clearPaymentPartyBank();
		}
		
	 if (bankType.equals(TradePortalConstants.PAYER_BANK)) {
		     currentDomesticPayment.clearPaymentPartyBank();
		}	
		
  }  
 // Vshah ClearParty End


	//IAZ: When user picks a payee form the table, it must reload to the upper part
	//                 of the screen.  We must handle this differently from loading data from db
	//                 or regular document's reload
    if (TradePortalConstants.BUTTON_MODIFY_PAYEE.equals(buttonClicked)) { 
	if ((currentDPOid != null) && (!returnFromSearch) && (!currentDPOid.equals(""))) {
		currentDomesticPayment.setAttribute("domestic_payment_oid",
				currentDPOid);
		currentDomesticPayment.getDataFromAppServer();
	   } 
    }
  
	if (InstrumentServices.isNotBlank(currentDomesticPayment.getAttribute("beneficiary_party_oid"))) {
	      accountSql.append("SELECT ACCOUNT_NUMBER FROM ACCOUNT WHERE P_OWNER_OID = ?");
	     //jgadela  R90 IR T36000026319 - SQL FIX
		 Object sqlParamsAcct[] = new Object[1];
		 sqlParamsAcct[0] = currentDomesticPayment.getAttribute("beneficiary_party_oid");
	      accountDoc = DatabaseQueryBean.getXmlResultSet(accountSql.toString(), false, sqlParamsAcct);
		  if (accountDoc != null) {
			     Vector v = accountDoc.getFragments ("/ResultSetRecord");
		         displayMultipleAccounts = v != null && v.size() > 0;

			     if (displayMultipleAccounts) {
			         DocumentHandler acct = null;
			         accountSet = new HashSet();
			         for (int i = 0; i < v.size(); i++) {
		                acct = (DocumentHandler) v.elementAt(i);
		                accountSet.add(acct.getAttribute("ACCOUNT_NUMBER"));
			        }     
			    }
			  }
		  }
	
	
	BigInteger requestedSecurityRight = SecurityAccess.DDI_CREATE_MODIFY;
%>




<%@ include file="fragments/Transaction-PageMode.frag"%>

<%
	if (isTemplate)
		corpOrgOid = template.getAttribute("owner_org_oid");
	else
		corpOrgOid = instrument.getAttribute("corp_org_oid");
	
   //jgadela  R90 IR T36000026319 - SQL FIX
	Object sqlParamsMultiAdd[] = new Object[1];
	sqlParamsMultiAdd[0] = corpOrgOid;
	if (DatabaseQueryBean.getCount("address_oid", "address","p_corp_org_oid = ? ", false, sqlParamsMultiAdd ) > 0) {
		corpOrgHasMultipleAddresses = true;
	}

	transactionType = transaction.getAttribute("transaction_type_code");
	transactionStatus = transaction.getAttribute("transaction_status");

	rejectionIndicator = transaction
			.getAttribute("rejection_indicator");
	rejectionReasonText = transaction
			.getAttribute("rejection_reason_text");

	instrumentType = instrument.getAttribute("instrument_type_code");
	instrumentStatus = instrument.getAttribute("instrument_status");

	Debug.debug("Instrument Type " + instrumentType);
	Debug.debug("Instrument Status " + instrumentStatus);
	Debug.debug("Transaction Type " + transactionType);
	Debug.debug("Transaction Status " + transactionStatus);

	//Template Dropdown Data Retrive
	QueryListView queryTemplateListView = null;

	Vector templateList = null;
	int numberOfTemplates = 0;
	boolean isBankUser = true;
	try {
		queryTemplateListView = (QueryListView) EJBObjectFactory
				.createClientEJB(formMgr.getServerLocation(),
						"QueryListView");

		String sqlString = "select i.original_transaction_oid, i.original_transaction_oid || '/' || t.template_oid as TransactionOid, t.name as TemplateName, t.ownership_level as ownershipLevel, t.ownership_type as ownershipType, i.instrument_type_code as instrumentType, p.name as partyName from template t, instrument i, terms_party p where t.c_template_instr_oid = i.instrument_oid and t.default_template_flag = 'N' and i.a_counter_party_oid = p.terms_party_oid (+)  and i.instrument_type_code = 'DDI' and p_owner_org_oid in ("
				+ userSession.getOwnerOrgOid()
				+ ","
				+ userSession.getClientBankOid()
				+ ","
				+ userSession.getGlobalOrgOid()
				+ ","
				+ userSession.getBogOid()
				+ ") order by upper(TemplateName)";

		queryTemplateListView.setSQL(sqlString);
		queryTemplateListView.getRecords();

		DocumentHandler templatesDocHandler = queryTemplateListView
				.getXmlResultSet();
		numberOfTemplates = queryTemplateListView.getRecordCount();
		templateList = templatesDocHandler
				.getFragments("/ResultSetRecord");

		sqlString = "select name from client_bank, users where user_oid = "
				+ userSession.getUserOid()
				+ " and p_owner_org_oid = organization_oid";

		queryTemplateListView.setSQL(sqlString);
		queryTemplateListView.getRecords();
		if (queryTemplateListView.getRecordCount() == 0) {
			isBankUser = false;
		}

		if (userSession.hasSavedUserSession()) {
			isBankUser = true;
		}

	} catch (Exception e) {
		out.println("Exception");
		e.printStackTrace();

	} finally {
		try {
			if (queryTemplateListView != null) {
				queryTemplateListView.remove();
			}
		} catch (Exception e) {
			System.out
					.println("Error removing QueryListView in -CreditDebitDetails.frag!");

		}
	}
%>

<%-- ********************* HTML for page begins here ********************* --%>

<%
	if (isTemplate) {
%>
<%-- <jsp:include page="/common/ButtonPrep.jsp" /> --%>
<%
	}
%>

<%@include file="fragments/MultiPartMode.frag"%>

<%
	if (!isReadOnly) {
// 		onLoad += "document.forms[0]." + focusField + ".focus();";
// 		onLoad += "updateTotalAmount();"; 
	}
%>

<%
	// The navigation bar is only shown when editing templates.  For transactions
	// it is not shown ti minimize the chance of leaving the page without properly
	// unlocking the transaction.
	String showNavBar = TradePortalConstants.INDICATOR_NO;
	if (isTemplate) {
		showNavBar = TradePortalConstants.INDICATOR_YES;
	}

	// Auto save the form when time-out if not readonly.  
	// (Header.jsp will check for auto-save setting of the corporate org).
	String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO
			: TradePortalConstants.INDICATOR_YES;
	String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES
			: TradePortalConstants.INDICATOR_NO;
%>

<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__DDI_ISS);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
%>
<%
  String pageTitleKey;
if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
    pageTitleKey = "SecondaryNavigation.NewInstruments";
    
    if (isTemplate){
         if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
         userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
    }else{
       userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
    }
    }else	         
    	userSession.setCurrentPrimaryNavigation("NewInstrumentsMenu.Trade.AirWaybill");
} else {
    pageTitleKey = "SecondaryNavigation.Instruments";
    if (isTemplate){
         if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
         userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
    }else{
       userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
    }
    }else 
         userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
}
  String helpUrl ="customer/direct_debit.htm";

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">

<%
		  if(isTemplate) {
		  String pageTitle;
		  String titleName;
		  String returnAction;
		  pageTitle = resMgr.getText("common.template", TradePortalConstants.TEXT_BUNDLE);
		  titleName = template.getAttribute("name");
		  StringBuffer title = new StringBuffer();
		  String itemKey = "";
		  title.append(pageTitle);
		  title.append( " : " );
		  if(instrument.getAttribute("instrument_type_code").equals("SLC")){
		  if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){     
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingSTandbyLCSimple", TradePortalConstants.TEXT_BUNDLE); 
		  }else{
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingStandbyLCDetailed", TradePortalConstants.TEXT_BUNDLE);
		  }
		  title.append(itemKey);
		  }
		  else
		  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
		                 instrument.getAttribute("instrument_type_code"), 
		                 loginLocale) );
		  title.append( " - " );
		  title.append(titleName );
		  returnAction = "goToInstrumentCloseNavigator";
		
		  String transactionSubHeader = title.toString();
  		
  		%>
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
				<jsp:param name="helpUrl"  value="<%=helpUrl%>" />
  		</jsp:include>
  		
  		<jsp:include page="/common/PageSubHeader.jsp">
 				 <jsp:param name="titleKey" value="<%= transactionSubHeader%>" />
  				 <jsp:param name="returnUrl" value="<%= formMgr.getLinkAsUrl(returnAction, response) %>" />
		</jsp:include> 		
		 <%}else{ %>
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="Direct Debit" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />

<%} %>
<form id="TransactionDDI" name="TransactionDDI" method="post" action="<%=formMgr.getSubmitAction(response)%>" data-dojo-type="dijit.form.Form"> 
	<input
	type="hidden" value="" name="buttonName"> 
 <%-- Main Content Area starts here --%>

<%-- error section goes above form content --%>
<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent"> <%-- Form Content Area starts here --%>	
<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 
	
<%
 	// Store values such as the userid, security rights, and org in a
 	// secure hashtable for the form.  Also store instrument and transaction
 	// data that must be secured.
 	Hashtable secureParms = new Hashtable();
 	secureParms.put("login_oid", userSession.getUserOid());
 	secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
 	secureParms.put("login_rights", loginRights);

 	secureParms.put("instrument_oid", instrument
 			.getAttribute("instrument_oid"));
 	secureParms.put("instrument_type_code", instrumentType);
 	secureParms.put("instrument_status", instrumentStatus);
 	secureParms.put("corp_org_oid", corpOrgOid);

 	secureParms.put("transaction_oid", transaction
 			.getAttribute("transaction_oid"));
 	secureParms.put("transaction_type_code", transactionType);
 	secureParms.put("transaction_status", transactionStatus);

 	secureParms.put("transaction_instrument_info", transaction
 			.getAttribute("transaction_oid")
 			+ "/"
 			+ instrument.getAttribute("instrument_oid")
 			+ "/"
 			+ transactionType);

 	secureParms.put("userOid", userSession.getUserOid());
 	secureParms.put("securityRights", userSession.getSecurityRights());
 	secureParms.put("clientBankOid", userSession.getClientBankOid());
 	secureParms.put("ownerOrg", userSession.getOwnerOrgOid());

 	// If the terms record doesn't exist, set its oid to 0.
 	String termsOid = terms.getAttribute("terms_oid");

 	if (termsOid == null) {
 		termsOid = "0";
 	}
 	secureParms.put("terms_oid", termsOid);

 	if (isTemplate) {
 		secureParms.put("template_oid", template
 				.getAttribute("template_oid"));
 		secureParms.put("opt_lock", template.getAttribute("opt_lock"));
 	}
 %> <%@include file="fragments/MultiPartModeFormElements.frag"%>

<%
	if (isTemplate) {
%> <%@ include file="fragments/TemplateHeader.frag"%>
<%
	} else {
%>


		       
<%
 	}

 	String extraPartTags = request.getParameter("extraPartTags");
%> 
 <%@ include
 			file="fragments/Instruments-AttachDocuments.frag"%>
 			

		
		<%
		

		links="DirectDebitInstruction.CreditDetailsText,"+
				"DirectDebitInstruction.DebitDetails";


 	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
 			|| rejectionIndicator
 					.equals(TradePortalConstants.INDICATOR_X)) {
 %> 
		 <%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
 		 <%@ include file="fragments/Transaction-RejectionReason.frag"%>
 		 </div>
		<%
			}

			if (!isTemplate) {
		%> <%-- %@ include file="fragments/Transaction-TemplateDropDown.frag" % --%>
		<%
			}
			if (!multiPartMode
					|| whichPart.equals(TradePortalConstants.PART_ONE)) {
		%> 
		
		
		<div id="section1" class="section" data-dojo-type="dijit.TitlePane"							 data-dojo-props="title: '1. Credit Details', open: true">
         <%@ include file="fragments/Transaction-DDI-ISS-CreditDetails.frag"%>           
	 </div>
	 
		<div id="section2" class="section" data-dojo-type="dijit.TitlePane"						 data-dojo-props="title: '2. Debit Parties', open: true">
 			<%@ include  	file="fragments/Transaction-DDI-ISS-DebitDetails.frag"%> 
			</div>
		

		<%
			}

			if (!isReadOnly) {
				radioSelectSection = true;
			}
         %>
		<%@ include file="fragments/PartySearchPrep.frag" %>
		<%-- Vshah CR509 Begin --%>
		<%@ include file="fragments/BankSearchPrep.frag" %>
		<%-- Vshah CR509 End --%>
		<%-- Vshah ClearParty Start--%>
 		 <%@ include file="fragments/BankClearPrep.frag" %>
		<%--Vshah ClearParty End--%>
		<%=formMgr.getFormInstanceAsInputField("Transaction-DDIForm", secureParms)%>

</div> <%-- end of formContent --%>
</div> <%--formArea--%>
<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="title: 'Sidebar', form: 'TransactionDDI'">
	<jsp:include page="/common/Sidebar.jsp">
		<jsp:param value="<%=links %>" name="links"/>
        <jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
		<jsp:param name="buttonPressed" value="<%=buttonClicked%>" />
		<jsp:param name="copyInstrument" value="true" />
		<jsp:param name="error" value="<%=error%>" />		
		<jsp:param name="formName" value="0" />
		<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
		<jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
		<jsp:param name="showLinks" value="true" /> 
		<jsp:param name="saveOnClick" value="none" />
        <jsp:param name="saveCloseOnClick" value="none" />
    </jsp:include>
</div>
								 
<div id="PartySearchDialog"></div>
<div id="BankSearchDialog"></div>
<div id="bankSearchSelectorDialog"></div>
<input type="hidden" name="selection" />
<input type="hidden" name="ChoosePrimary" />
</form>
</div> <%--closes page content area --%>
</div> <%-- pageMain --%>


<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/common/SidebarFooter.jsp"/>

<script >
require(["dojo/ready"], function(ready){
	console.log('inside dojo/ready');
    ready(function(){
    	console.log('inside dojo/ready');
    	onloadPayer();
    });
});
</script>
<%-- IR4505 ends--%>

<script type="text/javascript">


function setAccount(id) {
	var arr = dijit.byId(id);
	console.log('setAccount arr value='+arr.value);
	if (arr != null) {
     	document.forms[0].PayeeAccount.value = arr.value;
    }
}

function setAccountSelectType(id,idx) {
	var arr = dijit.byId(id);
	if (arr != null) {
	console.log('setAccountSelectType arr value='+arr.value);
		var val = arr.value;
		if (val.length > 0) {
			<%-- document.forms[0].AccountSelectType[idx].checked='true'; --%>
			document.forms[0].PayeeAccount.value = val;
	    }
	}
}


</script> 
</body>
</html>

<%
	// Finally, reset the cached document to eliminate carryover of
	// information to the next visit of this page.
	formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<%!// Various methods are declared here (in alphabetical order).

	public String getExpressIndicator(boolean showIndicator) {
		if (showIndicator) {
			return "<span class=Express>#</span>";
		} else {
			return "";
		}
	}

	public String getRequiredIndicator(boolean showIndicator) {
		if (showIndicator) {
			return "<span class=Asterix>*</span>";
		} else {
			return "";
		}
	}
   public String buildPaymentBankDisplayData(String bankName, String branchName, String addr1, String addr2, String addr3, boolean readOnly)
   {
		return buildPaymentBankDisplayData(bankName, branchName, addr1, addr2, addr3, null, readOnly);
   }
   
   public String buildPaymentBankDisplayData(String bankName, String branchName, String addr1, String addr2, String addr3, String country, boolean readOnly)
   {
		if (InstrumentServices.isBlank(bankName))
			return "";
		String eol = "\n";
		//if (readOnly)
		//	eol = "<br>";
		StringBuffer bankDData = new StringBuffer(bankName);
		if (InstrumentServices.isNotBlank(branchName))
			bankDData.append(eol + branchName);
		if (InstrumentServices.isNotBlank(addr1))
			bankDData.append(eol + addr1);
		if (InstrumentServices.isNotBlank(addr2))
			bankDData.append(eol + addr2);
		if (InstrumentServices.isNotBlank(addr3))
			bankDData.append(eol + addr3);
   	    if (InstrumentServices.isNotBlank(country))
   		    bankDData.append(" " + country);				
		return bankDData.toString();
	}%>
	
<%
String encryptedInstrOid = EncryptDecrypt.encryptStringUsingTripleDes(instrument.getAttribute("instrument_oid"), userSession.getSecretKey());
%>	
	
<script type="text/javascript">



require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready", "t360/dialog"],
	      function(registry, on, common, ready, dialog) {
	    ready(function() {
	      <%--register event handlers--%>
	      <%--todo: only execute when necessary--%>
	      <%--todo: use event propagation to reduce registry associations--%>
	      
	      updateTotalAmount();
	      var myButton = registry.byId("CopyInstrumentButton");
	      if (myButton) {
	        myButton.on("click", function() {
	        	dialog.open('bankSearchSelectorDialog', bankBranchSelectorDialogTitle,
	                    'bankBranchSelectorDialog.jsp',
	                    null, null, <%-- parameters --%>
	                    'select', this.createNewInstrumentBankBranchSelectedValue);

	    		
	        });
	      }
	    });
	});


function updateTotalAmount() {
	require(["dijit/registry"], 
		function(registry) {
			var calculatedTranAmt = document.forms[0].DisplayTotalPaymentsAmount.value;
			var displayTranAmtfromDB = registry.byId("displayTransactionAmount").value;
		    
		    if (displayTranAmtfromDB != calculatedTranAmt)
		    {
		    	document.forms[0].TransactionAmount.value = calculatedTranAmt;
		    	registry.byId("displayTransactionAmount").set("value",calculatedTranAmt);
		    	document.forms[0].FundingAmount.value = calculatedTranAmt;
		    }
});
}
function openBankSelectorPopup () {
	
}
function createNewInstrumentBankBranchSelectedValue(id, type, bankBranchOid) {

	console.log("inside createNewInstrumentBankBranchSelectedValue() - bankBranchOid: "+bankBranchOid);
	if ( document.getElementsByName('NewInstrumentForm1').length > 0 ) {
        var theForm = document.getElementsByName('NewInstrumentForm1')[0];
      	
      	theForm.bankBranch.value=bankBranchOid;
      	theForm.copyInstrumentOid.value='<%=encryptedInstrOid%>';
 	    	
    	console.log("theForm.copyInstrumentOid.value: "+theForm.copyInstrumentOid.value);
  		console.log("theForm.bankBranch.value: "+theForm.bankBranch.value);
    		  
      	theForm.submit();
      } else {
        	alert('Problem in createNewInstrument: cannot find ExistingInstrumentsForm');
      }
}

  var itemid;
  var section;

  function SearchParty(identifierStr, sectionName,partyType){
	
    itemid = String(identifierStr);
    section = String(sectionName);
    partyType = String(partyType);
    <%-- itemid = 'OrderingPartyName,OrderingPartyAddress1,OrderingPartyAddress2,OrderingPartyAddress3,OrderingPartyAddress4,OrderingPartyCountry'; --%>

    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- cquinton 2/7/2013 --%>
      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
        'PartySearch.jsp',
        ['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
        ['selectTransactionParty','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section]); <%-- parameters --%>
    });
  }

function BankSearch(identifierStr, sectionName,bankType){
	
	itemid = String(identifierStr);
	section = String(sectionName);
	bankType= String(bankType);
	var credit_account_oid = dijit.byId('CreditAccount').value;
	if (credit_account_oid == '') {
		alert('Credit Account Number and Currency is empty');
		return;
	}
	
	require(["t360/dialog"], function(dialog ) {
	dialog.open('BankSearchDialog', '<%=BankSearchAddressTitle%>',
	'BankSearch.jsp',
	['returnAction','paymentMethodCode','bankType','itemid','section','account_oid'],
	['selectTransactionParty','DDI',bankType,itemid,section, credit_account_oid], <%-- parameters --%>
	'select', null);
	});
}

</script>
