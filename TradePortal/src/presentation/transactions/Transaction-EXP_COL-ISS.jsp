
<%--
*******************************************************************************
                              Export Colleection Issue Page

  Description:
    This is the main driver for the Export Collection Issue page.  It handles data
  retrieval of terms and terms parties (or retrieval from the input document)
  and creates the html page to display all the data for an Import DLC.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String  onLoad = "";
  String  focusField = "DraweeName";   // Default focus field
  boolean focusSet = false;
  boolean isExportCollection 	= true;
  boolean hasPDFLinks 		= false;
  String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);  
  String parentOrgID      = userSession.getOwnerOrgOid();
  String bogID            = userSession.getBogOid();
  String clientBankID     = userSession.getClientBankOid();
  String globalID         = userSession.getGlobalOrgOid();
  String ownershipLevel   = userSession.getOwnershipLevel();	

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;

  //Vshah - 09/29/2010 - IR#KUUK092841654 - [BEGIN]
  String rejectionIndicator   = "";
  String rejectionReasonText  = "";
  //Vshah - 09/29/2010 - IR#KUUK092841654 - [END]

  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database
  boolean phraseSelected = false;  // Indicates if a phrase was selected.
  				   // Indicates if Payment Instructions needs
  String paymtInstr = TradePortalConstants.PMT_INSTR_NO;	    
				   // to be displayed due to an error occurring...
  DocumentHandler doc;
  boolean           corpOrgHasMultipleAddresses   = false;
  String            corpOrgOid                    = null;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  // Dates are stored in the database as one field but displayed on the
  // page as 3.  These variables hold the pieces for each date.
  String date = "";
  String atFixedDay = "";
  String atFixedMonth = "";
  String atFixedYear = "";
  String maturityFecDay = "";
  String maturityFecMonth = "";
  String maturityFecYear = "";

  // This array of string is used when creating the Collection Schedule
  // and Bill of Exchanges pdf links.
  String linkArgs[] = {"","","",""};

  //Tenor related variables
  String label;
  String descName;
  String descAttName;
  String amountName;
  String amountAttName;
  String draftName;
  String draftAttName;
  int NUMBER_OF_TENORS = 6;

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;
  String termsPartyOid;
  String buttonPressed;
  String links ="";
  boolean  canDisplayPDFLinks   = false;

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  // These are the beans used on the page.

  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");
  TermsWebBean terms              = null;

  TermsPartyWebBean termsPartyDrawee = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyDrawer = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyBank = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyCaseOfNeed = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  ShipmentTermsWebBean shipmentTerms = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();
  buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");

  Debug.debug("doc from cache is " + doc.toString());

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
  Party Search     exists                       SearchInfo to lookup, and 
                                                populate a specific TermsParty
                                                web bean

  return from    /In/AddressSearchInfo/Address  doc cache (/In); use /In/Address
  Address Search   SearchPartyType exists       SearchInfo to populate a specific
                                                TermsParty web bean

  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
  Phrase Lookup                                 LookupInfo text (from /Out) to
                                                replace a specific phrase text
                                                in the /In document before 
                                                populating

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction    
    mediator
    (error)
******************************************************************************/
  
  // Assume we get the data from the doc.
  getDataFromDoc = true;

  String maxError = doc.getAttribute("/Error/maxerrorseverity");

  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;

  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     // We have returned from the address search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;
     phraseSelected = true;

     String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
     if (result.equals(TradePortalConstants.INDICATOR_YES)) {
        // Take the looked up and appended phrase text from the /Out section
        // and copy it to the /In section.  This allows the beans to be 
        // properly populated.
        String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
        xmlPath = "/In" + xmlPath;

        doc.setAttribute(xmlPath, 
                      doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

        // If we returned from the phrase lookup without errors, set the focus
        // to the correct field.  Otherwise, don't set the focus so the user can
        // see the error.
        if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
           focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
           focusSet = true;
        }
     } else {
        // We do nothing because nothing was looked up.  We stil want to get
        // the data from the doc.
     }
  }

  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }
  
  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

         // Populate the beans from the input doc.
     try {

           instrument.populateFromXmlDoc(doc.getComponent("/In"));
           transaction.populateFromXmlDoc(doc.getComponent("/In"));
           template.populateFromXmlDoc(doc.getComponent("/In"));

           terms = (TermsWebBean) beanMgr.getBean("Terms");
           terms.populateFromXmlDoc(doc, "/In");

        termsPartyOid = terms.getAttribute("c_FirstTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals("")) {
           termsPartyDrawee.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyDrawee.getDataFromAppServer();
        }
        termsPartyOid = terms.getAttribute("c_SecondTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals("")) {
           termsPartyDrawer.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyDrawer.getDataFromAppServer();
        }
        termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals("")) {
           termsPartyBank.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyBank.getDataFromAppServer();
        }
        termsPartyOid = terms.getAttribute("c_FourthTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals("")) {
           termsPartyCaseOfNeed.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyCaseOfNeed.getDataFromAppServer();
        }

     	paymtInstr = terms.getAttribute("pmt_instr_multiple_phrase");

        DocumentHandler termsPartyDoc;

        termsPartyDoc = doc.getFragment("/In/Terms/FirstTermsParty");
        termsPartyDrawee.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
        termsPartyDrawer.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/ThirdTermsParty");
        termsPartyBank.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

		termsPartyDoc = doc.getFragment("/In/Terms/FourthTermsParty");
		termsPartyCaseOfNeed.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        // Some of the data for this transaction is stored as part of the shipment terms
        terms.populateFirstShipmentFromXml(doc);

        atFixedDay = doc.getAttribute("/In/Terms/pmt_terms_fixed_maturity_day");
        atFixedMonth = doc.getAttribute("/In/Terms/pmt_terms_fixed_maturity_month");
        atFixedYear = doc.getAttribute("/In/Terms/pmt_terms_fixed_maturity_year");

        maturityFecDay = doc.getAttribute("/In/Terms/fec_maturity_day");
        maturityFecMonth = doc.getAttribute("/In/Terms/fec_maturity_month");
        maturityFecYear = doc.getAttribute("/In/Terms/fec_maturity_year");

	//Need to set up the Payment Instructions Flag.  This will help determine
 	//Whether we need to display the Payment Instructions Section as well
	//as keep the terms.pmt_instr_multiple_phrase value accurate since the
	//values is in the secure parms.
  	if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) > 0)
  	{
	   DocumentHandler errorDoc = null;
	   String errors = null;
	   int errLocation = 0;
	
	   errorDoc = doc.getFragment("/Error");
	   errors = errorDoc.toString();
	   errLocation = errors.indexOf(TradePortalConstants.SELECT_PMT_INSTR);

	   if( errLocation > 0 ) 	{		//'SELECT_PMT_INSTR' error was found
	         paymtInstr = TradePortalConstants.PMT_INSTR_YES;
	         terms.setAttribute("payment_instructions", "");
	   }
	   else  paymtInstr = TradePortalConstants.PMT_INSTR_NO;
  	}


     } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

     DocumentHandler myDoc = new DocumentHandler();
     terms.populateXmlDoc( myDoc );
     Debug.debug("The terms object is: " + myDoc.toString() );

     termsPartyOid = terms.getAttribute("c_FirstTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyDrawee.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyDrawee.getDataFromAppServer();
     }
     termsPartyOid = terms.getAttribute("c_SecondTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyDrawer.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyDrawer.getDataFromAppServer();
     }
     termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyBank.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyBank.getDataFromAppServer();
     }
     termsPartyOid = terms.getAttribute("c_FourthTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyCaseOfNeed.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyCaseOfNeed.getDataFromAppServer();
     }

     // Some of the data for this transaction is stored as part of the shipment terms
     terms.populateFirstShipmentFromDatabase();

     // Because date fields are represented as three fields on the page,
     // we need to parse out the date values into three fields.  Pre-mediator
     // logic puts these three fields back together for the terms bean.

     date = terms.getAttribute("pmt_terms_fixed_maturity_date");
     atFixedDay = TPDateTimeUtility.parseDayFromDate(date);
     atFixedMonth = TPDateTimeUtility.parseMonthFromDate(date);
     atFixedYear = TPDateTimeUtility.parseYearFromDate(date);

     date = terms.getAttribute("fec_maturity_date");
     maturityFecDay = TPDateTimeUtility.parseDayFromDate(date);
     maturityFecMonth = TPDateTimeUtility.parseMonthFromDate(date);
     maturityFecYear = TPDateTimeUtility.parseYearFromDate(date);

     
     paymtInstr = terms.getAttribute("pmt_instr_multiple_phrase");

  }

  /*************************************************
  * Load New Party
  **************************************************/
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
      doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null ) {

     // We have returned from the PartyDetailNew page.  Based on the returned 
     // data, RELOAD one of the terms party beans with the selected party

     String termsPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
     String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");

     Debug.debug("Returning from party search with " + termsPartyType 
           + "/" + partyOid);

     // Use a Party web bean to get the party data for the selected oid.
     PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");

     party.setAttribute("party_oid", partyOid);
     party.getDataFromAppServer();

     DocumentHandler partyDoc = new DocumentHandler();
     party.populateXmlDoc(partyDoc);

     partyDoc = partyDoc.getFragment("/Party");

     // Based on the party type being returned (which we previously set)
     // reload one of the terms party web beans with the data from the
     // doc.
     if (termsPartyType.equals(TradePortalConstants.DRAWEE_BUYER))
      {
        termsPartyDrawee.loadTermsPartyFromDoc(partyDoc);
        //cquinton 2/7/2013 pass designated party as part of partyDoc
	loadDesignatedParty(partyDoc, termsPartyBank, beanMgr);
        focusField = "DraweeName";
        focusSet = true;
     }
     if (termsPartyType.equals(TradePortalConstants.DRAWER_SELLER)) {
        termsPartyDrawer.loadTermsPartyFromDoc(partyDoc);
        //rbhaduri - 13th June 06 - IR SAUG051361872
	termsPartyDrawer.setAttribute("address_search_indicator", "N");
        focusField = "";
        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";
     }
     if (termsPartyType.equals(TradePortalConstants.COLLECTING_BANK)) {
        termsPartyBank.loadTermsPartyFromDoc(partyDoc);
        focusField = "";
        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";
     }
     if (termsPartyType.equals(TradePortalConstants.CASE_OF_NEED)) {
	termsPartyCaseOfNeed.loadTermsPartyFromDoc(partyDoc);
        termsPartyCaseOfNeed.createAddressLine3(resMgr);
        focusField = "CaseOfNeedName";
        focusSet = true;
     }

  }
  
   //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved it here from below
   BigInteger requestedSecurityRight = SecurityAccess.EXPORT_COLL_CREATE_MODIFY; 
%>
     
<%-- //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved it here from below --%>
<%@ include file="fragments/Transaction-PageMode.frag" %>

<%

    //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved the code here from below
    if (isTemplate) 
   	corpOrgOid = template.getAttribute("owner_org_oid");
    else
   	corpOrgOid = instrument.getAttribute("corp_org_oid");

	//MEer  CR-1027  
	CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	corporateOrg.getById(corpOrgOid);
	String bankOrgGroupOid = corporateOrg.getAttribute("bank_org_group_oid");
   
    BankOrganizationGroupWebBean bankOrganizationGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class, "BankOrganizationGroup");
    bankOrganizationGroup.getById(bankOrgGroupOid);   
    String selectedPDFType =  bankOrganizationGroup.getAttribute("exp_col_pdf_type");
    boolean pdfTypeSelected = false;
  
    if(TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(selectedPDFType)){
 			//canDisplayPDFLinks = true;
 			pdfTypeSelected = true;
    }
    
  //	if(!userSession.isCustNotIntgTPS()){
    	String suppressDocLinkInd = instrument.getAttribute("suppress_doc_link_ind");
    	if (InstrumentServices.isBlank(suppressDocLinkInd))
       		suppressDocLinkInd = TradePortalConstants.INDICATOR_NO; 
    		//canDisplayPDFLinks =  (suppressDocLinkInd.equals(TradePortalConstants.INDICATOR_NO)) ? true : false ;
  //	}
  	//Rel 9.3.5 IR-43051
  	if(pdfTypeSelected && TradePortalConstants.INDICATOR_NO.equals(suppressDocLinkInd))
  		canDisplayPDFLinks = true;
    
   
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object sqlParamsAddCount[] = new Object[1];
	sqlParamsAddCount[0] =  corpOrgOid ;
    if (DatabaseQueryBean.getCount("address_oid", "address", "p_corp_org_oid = ?", true, sqlParamsAddCount) > 0 )
    {
   		corpOrgHasMultipleAddresses = true;
    }    
  /*************************************************
  * Load address search info if we come back from address search page.
  * Reload drawer terms party web beans with the data from 
  * the address
  **************************************************/
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     String termsPartyType = doc.getAttribute("/In/AddressSearchInfo/AddressSearchPartyType");
     String addressOid = doc.getAttribute("/In/AddressSearchInfo/AddressOid");
     
     //rbhaduri - 9th Oct 06 - IR AOUG100368306
     String primarySelected = doc.getAttribute("/In/AddressSearchInfo/ChoosePrimary");
     
     Debug.debug("Returning from address search with " + termsPartyType 
           + "/" + addressOid);

     // Use a Party web bean to get the party data for the selected oid.
     AddressWebBean address = null;  
     //rbhaduri - 9th Oct 06 - IR AOUG100368306 - added PRIMARY case for primary address selection
     if ((addressOid != null||primarySelected != null) && termsPartyType.equals(TradePortalConstants.DRAWER_SELLER)) {
        
        if (primarySelected.equals("PRIMARY")) {
			termsPartyDrawer.setAttribute("name",corporateOrg.getAttribute("name"));
           	termsPartyDrawer.setAttribute("address_line_1",corporateOrg.getAttribute("address_line_1"));
           	termsPartyDrawer.setAttribute("address_line_2",corporateOrg.getAttribute("address_line_2"));
     	   	termsPartyDrawer.setAttribute("address_city",corporateOrg.getAttribute("address_city"));
       	   	termsPartyDrawer.setAttribute("address_state_province",corporateOrg.getAttribute("address_state_province"));
       	   	termsPartyDrawer.setAttribute("address_country",corporateOrg.getAttribute("address_country"));
       	   	termsPartyDrawer.setAttribute("address_postal_code",corporateOrg.getAttribute("address_postal_code"));
       	   	termsPartyDrawer.setAttribute("address_seq_num","1");
       	//Rel9.5 CR1132 Populate userdefinedfields from corporate
			termsPartyDrawer.setAttribute("user_defined_field_1", corporateOrg.getAttribute("user_defined_field_1"));
			termsPartyDrawer.setAttribute("user_defined_field_2", corporateOrg.getAttribute("user_defined_field_2"));
			termsPartyDrawer.setAttribute("user_defined_field_3", corporateOrg.getAttribute("user_defined_field_3"));
			termsPartyDrawer.setAttribute("user_defined_field_4", corporateOrg.getAttribute("user_defined_field_4"));
	}
	else { address = beanMgr.createBean(AddressWebBean.class, "Address");
        address.setAttribute("address_oid", addressOid);
        address.getDataFromAppServer();
        termsPartyDrawer.setAttribute("name",address.getAttribute("name"));
        termsPartyDrawer.setAttribute("address_line_1",address.getAttribute("address_line_1"));
        termsPartyDrawer.setAttribute("address_line_2",address.getAttribute("address_line_2"));
     	termsPartyDrawer.setAttribute("address_city",address.getAttribute("city"));
       	termsPartyDrawer.setAttribute("address_state_province",address.getAttribute("state_province"));
       	termsPartyDrawer.setAttribute("address_country",address.getAttribute("country"));
       	termsPartyDrawer.setAttribute("address_postal_code",address.getAttribute("postal_code"));
       	termsPartyDrawer.setAttribute("address_seq_num",address.getAttribute("address_seq_num"));
      //Rel9.5 CR1132 Populate userdefinedfields from address
		termsPartyDrawer.setAttribute("user_defined_field_1", address.getAttribute("user_defined_field_1"));
		termsPartyDrawer.setAttribute("user_defined_field_2", address.getAttribute("user_defined_field_2"));
		termsPartyDrawer.setAttribute("user_defined_field_3", address.getAttribute("user_defined_field_3"));
		termsPartyDrawer.setAttribute("user_defined_field_4", address.getAttribute("user_defined_field_4"));
       	}
     }  
     focusField = "";
     focusSet = true;
     onLoad += "location.hash='#" + termsPartyType + "';";
     
  }

  

  transactionOid = transaction.getAttribute("transaction_oid");

  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");

  //Vshah - 09/29/2010 - IR#KUUK092841654 - [BEGIN]
  //Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");
  //Vshah - 09/29/2010 - IR#KUUK092841654 - [END]

  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

  //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved it here from here and placed above
  // Now determine the mode for how the page operates (readonly, etc.)
  //BigInteger requestedSecurityRight = SecurityAccess.EXPORT_COLL_CREATE_MODIFY;     
%>
  
	<%-- rbhaduri - 8th August 06 - IR AOUG100368306 - Moved above
		<%@ include file="fragments/Transaction-PageMode.frag" %>
	--%>


<%  
  // Create documents for the phrase dropdowns. First check the cache (no need
  // to recreate if they already exist).  After creating, place in the cache.
  // (InstrumentCloseNavigator.jsp cleans these up.)

  DocumentHandler phraseLists = formMgr.getFromDocCache(
                                             TradePortalConstants.PHRASE_LIST_DOC);
  if (phraseLists == null) phraseLists = new DocumentHandler();
			// In General Section
  DocumentHandler goodsDocList = phraseLists.getFragment("/GoodsDescription");  
  if (goodsDocList == null) {
     goodsDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_GOODS, 
                                    userSession, formMgr, resMgr);			
     phraseLists.addComponent("/GoodsDescription", goodsDocList);
  }
			// In Documents Presented Section
  DocumentHandler presentDocList = phraseLists.getFragment("/PresentOtherDocuments"); 	
  if (presentDocList == null) {
     presentDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_PRES_DOC, 
                                    userSession, formMgr, resMgr);			
     phraseLists.addComponent("/PresentOtherDocuments", presentDocList);
  }	
			// In Collection Instructions Section
  DocumentHandler otherInstrDocList = phraseLists.getFragment("/OtherInstructions");
  if (otherInstrDocList == null) {
     otherInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_COLL_INSTR, 
                                    userSession, formMgr, resMgr);			
     phraseLists.addComponent("/OtherInstructions", otherInstrDocList);
  }
			// In Collection Instructions Section
  DocumentHandler caseOfNeedDocList = phraseLists.getFragment("/Need");			
  if (caseOfNeedDocList == null) {
     caseOfNeedDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_NEED, 
                                    userSession, formMgr, resMgr);			
     phraseLists.addComponent("/Need", caseOfNeedDocList);
  }
			// In Collection Instructions Section
  DocumentHandler collChargesDocList = phraseLists.getFragment("/Charges");
  if (collChargesDocList == null) {
     collChargesDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_COLL_CHARGE, 
                                    userSession, formMgr, resMgr);			
     phraseLists.addComponent("/Charges", collChargesDocList);
  }
			// In Collection Instructions Section
  DocumentHandler collInterestDocList = phraseLists.getFragment("/Interest");
  if (collInterestDocList == null) {
     collInterestDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_COLL_INTEREST, 
                                    userSession, formMgr, resMgr);			
     phraseLists.addComponent("/Interest", collInterestDocList);
  }
			// In Bank Instructions Section
  DocumentHandler SpclInstrDocList = phraseLists.getFragment("/SpclInstr");
  if (SpclInstrDocList == null) {
     SpclInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SPCL_INST, 
                                    userSession, formMgr, resMgr);			
     phraseLists.addComponent("/SpclInstr", SpclInstrDocList);
  }
			// In Bank Instructions Section
  DocumentHandler SettlementInstrDocList = phraseLists.getFragment("/Settlement");
  if (SettlementInstrDocList == null) {
     SettlementInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SETTLEMENT, 
                                    userSession, formMgr, resMgr);			
     phraseLists.addComponent("/Settlement", SettlementInstrDocList);
  }
			// In Payment Instructions Section
  DocumentHandler paymentInstrDocList = phraseLists.getFragment("/Payment");
  //if (paymentInstrDocList == null) {
     paymentInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_PMT_INST, 
                                    userSession, formMgr, resMgr, terms.getAttribute("amount_currency_code"));			
     phraseLists.addComponent("/Payment", paymentInstrDocList);
  //}

  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);

  //-- This sets up the collection Date info. --
  String collectionDt = terms.getAttribute("collection_date");	//Get what's stored first
	  
  String gmtDateTimeNow = DateTimeUtility.getGMTDateTime();	//Get today's date
  Date nowCollectionDate = TPDateTimeUtility.getLocalDateTime( gmtDateTimeNow, userSession.getTimeZone() );

  GregorianCalendar myCalendar = new GregorianCalendar();
  myCalendar.setTime( nowCollectionDate );    //******* Based on TODAY'S DATE *******
					      //Build a saveable version of the date
  //** of Note ** For some reason the Month constant is zero based so you have to add 1 for the correct month int...

  String jPylonDate = TPDateTimeUtility.buildJPylonDateTime( String.valueOf( myCalendar.get(myCalendar.DATE) ),
							     String.valueOf( myCalendar.get(myCalendar.MONTH) + 1 ),
							     String.valueOf( myCalendar.get(myCalendar.YEAR) ));

%>

<%-- ********************* HTML for page begins here ********************* --%>
<%
  // The navigation bar is only shown when editing templates.  For transactions
  // it is not shown ti minimize the chance of leaving the page without properly
  // unlocking the transaction.
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate) {
    showNavBar = TradePortalConstants.INDICATOR_YES;
  }
%>
<% 
  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;

%>

<%
  String pageTitleKey;
if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
    pageTitleKey = "SecondaryNavigation.NewInstruments";
    
    if (isTemplate){
         if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
         userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
    }else{
       userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
    }
    }else
         userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
} else {
    pageTitleKey = "SecondaryNavigation.Instruments";
    if (isTemplate){
         if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
         userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
    }else{
       userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
    }
    }else 
         userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
}
    
  String helpUrl = "customer/issue_direct_send_coll.htm";
%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" 
              value="<%=showNavBar%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag" value="<%=templateFlag%>" />
</jsp:include>

<%
  if (isTemplate) {
%>
    <jsp:include page="/common/ButtonPrep.jsp" />
<%
  }
%>

<%-- Body tag included as part of common header --%>

<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__EXP_COL_ISS);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
%>



<div class="pageMain">
<div class="pageContent">


<%
		  if(isTemplate) {
		  String pageTitle;
		  String titleName;
		  String returnAction;
		  pageTitle = resMgr.getText("common.template", TradePortalConstants.TEXT_BUNDLE);
		  titleName = template.getAttribute("name");
		  StringBuffer title = new StringBuffer();
		  String itemKey = "";
		  title.append(pageTitle);
		  title.append( " : " );
		  if(instrument.getAttribute("instrument_type_code").equals("SLC")){
		  if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){     
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingSTandbyLCSimple", TradePortalConstants.TEXT_BUNDLE); 
		  }else{
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingStandbyLCDetailed", TradePortalConstants.TEXT_BUNDLE);
		  }
		  title.append(itemKey);
		  }
		  else
		  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
		                 instrument.getAttribute("instrument_type_code"), 
		                 loginLocale) );
		  title.append( " - " );
		  title.append(titleName );
		  returnAction = "goToInstrumentCloseNavigator";
		
		  String transactionSubHeader = title.toString();
  		
  		%>
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
				<jsp:param name="helpUrl"  value="<%=helpUrl%>" />
  		</jsp:include>
  		
  		<jsp:include page="/common/PageSubHeader.jsp">
 				 <jsp:param name="titleKey" value="<%= transactionSubHeader%>" />
  				 <jsp:param name="returnUrl" value="<%= formMgr.getLinkAsUrl(returnAction, response) %>" />
		</jsp:include> 		
		 <%}else{ %>
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="SecondaryNavigation.Instruments.DirectSendCollection" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />

	<%} %>		
<form name="TransactionEXP_COL" id="TransactionEXP_COL" method="post"
      data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>

<%-- error section goes above form content --%>
<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">
<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 

<%-- Keeps track of whether to display the Payment Instructions Section or not... --%>
  <input type=hidden value='<%= paymtInstr %>' name="pmt_instr_multiple_phrase">
<%
  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

  //If the user Save's or Verify's then this secureParm will be saved...
  //ie: the collection date will be updated with todays date.
  secureParms.put("CollectionDate", jPylonDate);

  secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code", instrumentType);
  secureParms.put("instrument_status", instrumentStatus);
  secureParms.put("corp_org_oid",  corpOrgOid);

  secureParms.put("transaction_oid", transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status", transactionStatus);
  secureParms.put("ShipmentOid", terms.getFirstShipment().getAttribute("shipment_oid"));

  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");
  if (termsOid == null) termsOid = "0";
  secureParms.put("terms_oid", termsOid);

  // To support the Route Trans/Authorize Trans buttons - this secureParm is required.
  secureParms.put("transaction_instrument_info", 
		   transaction.getAttribute("transaction_oid") + "/" + 
		   instrument.getAttribute("instrument_oid") + "/" +
		   transactionType);

  // Add to the secureParms the Multipart Navigation values...via a include file call
%>
  
<%
  if (isTemplate) {
    secureParms.put("template_oid", template.getAttribute("template_oid"));
    secureParms.put("opt_lock", template.getAttribute("opt_lock"));
  }

  if (isTemplate) {
%>
    <%@ include file="fragments/TemplateHeader.frag" %>
<%
  }
%>
<% // CR-186 - jkok - BEGIN %>











  <%--@ include file="fragments/Transaction-Documents.frag" --%>
  <%@ include file="fragments/Instruments-AttachDocuments.frag" %>
<% // CR-186 - jkok - END %>  
 
<%
  if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
  	|| rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>   
 <%@ include file="fragments/Transaction-RejectionReason.frag" %>
 </div>
<%
  }
%>    
<%-- Vshah - 09/29/2010 - IR#KUUK092841654 - [END] --%>   	
<% //CR 821 Added Repair Reason Section  
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object sqlParamsRepCount[] = new Object[1];
	sqlParamsRepCount[0] =  transaction.getAttribute("transaction_oid");
	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsRepCount);
	
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 


		<%= widgetFactory.createSectionHeader("1", "ExportCollectionIssue.Terms") %>
	  		<%@ include file="fragments/Transaction-EXP_COL-ISS-General.frag" %>

	  		<%@ include file="fragments/Transaction-EXP_COL-ISS-MultiTenors.frag" %>
	    </div>
	    
	    <%= widgetFactory.createSectionHeader("2", "ExportCollectionIssue.2DocumentsPresented") %>
	  		<%@ include file="fragments/Transaction-EXP_COL-ISS-DocsPresented.frag" %>
	    </div>
	    
	    <%= widgetFactory.createSectionHeader("3", "ExportCollectionIssue.3CollectionInstructions") %>
	  		<%@ include file="fragments/Transaction-EXP_COL-ISS-CollInstructions.frag" %>
	  	</div>	
	  	
	  	<%= widgetFactory.createSectionHeader("4", "ExportCollectionIssue.4InstructionstoBank") %>
	  		<%@ include file="fragments/Transaction-EXP_COL-ISS-BankInstructions.frag" %>
	  	</div>	
	  	
	  
<%
		    //Determine if we need to display the Payment Instructions
  	if (paymtInstr.equals( TradePortalConstants.PMT_INSTR_YES ) ) {
%>
	    <%= widgetFactory.createSectionHeader("5", "ExportCollectionIssue.5PaymentInstructions") %>
	  		<%@ include file="fragments/Transaction-EXP_COL-ISS-PaymentInstructions.frag" %>
	    </div>
<%
  	}
String sectionHeader;
if (paymtInstr.equals( TradePortalConstants.PMT_INSTR_YES ) ) {
	sectionHeader="ArMatchingRuleDetail.BankDef";
	
}else{
	sectionHeader="ArMatchingRuleDetail.BankDef5";
}
  
	//Only display the Bank defined section if the current user is an admin user...
	// IR-42689 REL 9.4 SURREWSH Start
   if (userSession.getSecurityType().equals(TradePortalConstants.ADMIN) || userSession.hasSavedUserSession()) {
%>
		<%= widgetFactory.createSectionHeader("6", sectionHeader) %>
	     		<%@ include file="fragments/Transaction-EXP_COL-ISS-BankDefined.frag" %>
	    </div>
<%
  	}
%>
 <%-- MEer Rel 8.3 IR-19700 Repair reason section UI issue for DSC --%>   
	<%
	if (paymtInstr.equals( TradePortalConstants.PMT_INSTR_YES ) ) {
		 if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))   {
				sectionHeader="7" ; 
		 }	else{
			 sectionHeader="6" ; 
		 }
	}else{
		sectionHeader="5";
	}
	  
	 if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
 						<%=widgetFactory.createSectionHeader(sectionHeader, "TransactionHistory.RepairReason", null,true) %>
							<%@ include file="fragments/Transaction-RepairReason.frag" %>
						</div>
		<%} %> 

	</div><%--formContent--%>
	</div><%--formArea--%>
	<%if(!isReadOnly) {%>
<%= widgetFactory.createHoverHelp("drawee","PartySearchIconHoverHelp")%>

<%= widgetFactory.createHoverHelp("direct_send_collecting_bank","PartySearchIconHoverHelp")%>
<%= widgetFactory.createHoverHelp("direct_send_collecting_bank_two","PartySearchIconHoverHelp")%>
<%= widgetFactory.createHoverHelp("need_of_contact","PartySearchIconHoverHelp")%>

<%} %>

	<div class="formSidebar" data-dojo-type="widget.FormSidebar"
                                    data-dojo-props="form: 'TransactionEXP_COL'">
          <jsp:include page="/common/Sidebar.jsp">
            <jsp:param name="links" value="<%=links%>" />
        	<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
			<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
			<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
            <jsp:param name="error" value="<%= error%>" />
            <jsp:param name="formName" value="0" />
            <jsp:param name="isTemplate" value="<%=isTemplate%>"/>
            <jsp:param name="showLinks" value="true" />  
			<jsp:param name="showApplnForm" value="<%=canDisplayPDFLinks%>"/>
			<jsp:param name="showBillOfExchangeForm" value="<%=canDisplayPDFLinks%>"/>
    	  </jsp:include>
   </div>
    	  
<%-- Vshah - 09/29/2010 - IR#KUUK092841654 - [BEGIN] --%>
<%--Commenting below code as it is repeting IR T36000016662 RPasupulati	        
<%
  if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
  	|| rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
<%
  }
%>  --%>
<%-- Vshah - 09/29/2010 - IR#KUUK092841654 - [END] --%>   

  <%@ include file="fragments/PhraseLookupPrep.frag" %>
  <%@ include file="fragments/PartySearchPrep.frag" %>

<%
	Debug.debug("*********************** IN THE ISSUE FILE ****************************");
	Debug.debug("*** This is the Terms_Oid: " + terms.getAttribute("terms_oid"));
	Debug.debug("*** This is the Transaction_oid: " + transaction.getAttribute("transaction_oid"));
	Debug.debug("*** This is the Terms Party 1 oid: " + terms.getAttribute("c_FirstTermsParty"));
	Debug.debug("*** This is the Terms Party 2 oid: " + terms.getAttribute("c_SecondTermsParty"));
	Debug.debug("*** This is the Terms Party 3 oid: " + terms.getAttribute("c_ThirdTermsParty"));

%>
<input type="hidden" name="selection" />
<input type="hidden" name="ChoosePrimary" />
<%= formMgr.getFormInstanceAsInputField("Transaction-EXP_COLForm", secureParms) %> 
<div id="PartySearchDialog"></div>
</form>
<%  //include a hidden form for new instrument submission
			//this is used for all of the new instrument types available from the menu
			Hashtable newInstrSecParms = new Hashtable();
			newInstrSecParms.put("UserOid", userSession.getUserOid());
			newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
			newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
			newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
			%>
			<form method="post" name="NewInstrumentForm1" action="<%=formMgr.getSubmitAction(response)%>">
			<input type="hidden" name="bankBranch" />
			<input type="hidden" name="transactionType"/>
			<input type="hidden" name="instrumentType" />
			<input type="hidden" name="copyInstrumentOid" />
			<input type="hidden" name="mode" value="CREATE_NEW_INSTRUMENT" />
			<input type="hidden" name="copyType" value="Instr" />
			
			<%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
			</form>
			<%
			Hashtable newTempSecParms = new Hashtable();
			newTempSecParms.put("userOid", userSession.getUserOid());
			newTempSecParms.put("securityRights", userSession.getSecurityRights());
			newTempSecParms.put("clientBankOid", userSession.getClientBankOid());
			newTempSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
			newTempSecParms.put("ownerLevel", userSession.getOwnershipLevel());
			%>
			<form method="post" name="NewTemplateForm1" action="<%=formMgr.getSubmitAction(response)%>">
			<input type="hidden" name="name" />
			<input type="hidden" name="bankBranch" />
			<input type="hidden" name="transactionType"/>
			<input type="hidden" name="InstrumentType" />
			<input type="hidden" name="copyInstrumentOid" />
			<input type="hidden" name="mode"  />
			<input type="hidden" name="CopyType" />
			<input type="hidden" name="expressFlag"  />
			<input type="hidden" name="fixedFlag" />
			<input type="hidden" name="PaymentTemplGrp" />
			<input type="hidden" name="validationState" />
			
			<%= formMgr.getFormInstanceAsInputField("NewTemplateForm",newTempSecParms) %>
			</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp"/>

<script>

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
<%
  }
%>
</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<%@ include file="fragments/DesignatedPartyLookup.frag" %>
<script type="text/javascript">

  hideTenorTable();

  var itemid;
  var section;

  function SearchParty(identifierStr, sectionName,partyType){

    itemid = String(identifierStr);
    section = String(sectionName);
    partyType=String(partyType);
    <%-- itemid = 'OrderingPartyName,OrderingPartyAddress1,OrderingPartyAddress2,OrderingPartyAddress3,OrderingPartyAddress4,OrderingPartyCountry'; --%>

    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- cquinton 2/7/2013 --%>
      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
        'PartySearch.jsp',
        ['returnAction','partyType','unicodeIndicator','itemid','section'],
        ['selectTransactionParty',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section]); <%-- parameters --%>
    });
  }

function openCopySelectedDialogHelper(callBackFunction){
    require(["t360/dialog"], function(dialog) {
          dialog.open('copySelectedInstrument', '<%=resMgr.getTextEscapedJS("CopySelected.Title",TradePortalConstants.TEXT_BUNDLE) %>',
                    'copySelectedInstrumentDialog.jsp',
                    "instrumentType", '<%=instrumentType%>', <%-- parameters --%>
                    'select', callBackFunction);
        	  
      });
  }
function copySelectedToInstrument(bankBranchOid, copyType, templateName, templateGroup, flagExpressFixed){
	var rowKeys ;
	 require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
		        function(registry, query, on, dialog ) {
		      <%--get array of rowkeys from the grid--%>
		      rowKeys = getSelectedGridRowKeys("instrumentsSearchGridId");
	 });
	if(copyType == 'I'){ 
		if ( document.getElementsByName('NewInstrumentForm1').length > 0 ) {
		    var theForm = document.getElementsByName('NewInstrumentForm1')[0];
		    theForm.bankBranch.value=bankBranchOid;
		    theForm.copyInstrumentOid.value=rowKeys;
		    theForm.submit();
		  }
	}	
	if(copyType == 'T'){ 
		if ( document.getElementsByName('NewTemplateForm1').length > 0 ) {
			var flag = flagExpressFixed.split('/');
			var expressFlag = flag[0];
			var fixedFlag = flag[1];
			var theForm = document.getElementsByName('NewTemplateForm1')[0];
		    theForm.name.value=templateName;
		    theForm.mode.value="<%=TradePortalConstants.NEW_TEMPLATE%>";
		    theForm.CopyType.value="<%=TradePortalConstants.FROM_INSTR%>";
		    theForm.fixedFlag.value=fixedFlag;
		    theForm.expressFlag.value=expressFlag;
		    theForm.PaymentTemplGrp.value=templateGroup;
		    theForm.copyInstrumentOid.value=rowKeys;
		    theForm.validationState.value = "";
		    theForm.submit();
		  }
	}
}



function pickCurrencyChanged() {
   document.forms[0].pmt_instr_multiple_phrase.value = '<%= TradePortalConstants.PMT_INSTR_CURR %>';
   paymtInstr = '<%= TradePortalConstants.PMT_INSTR_CURR%>';
  }



</script>
<script>
var isReadOnly = <%=isReadOnly%>;<%-- Checks done in case of readOnly mode, so that the On functions are not trigerred which are leading to js errors --%>
function showTenorTable() {	
	  if(document.getElementById("MultiTenorLabel"))
	  	document.getElementById("MultiTenorLabel").style.display = 'block' ;
	  if(document.getElementById("MultTenor"))
	  	document.getElementById("MultTenor").style.display = 'block' ;
	  if(document.getElementById("tenorButtons"))
	  	document.getElementById("tenorButtons").style.display = 'block' ;	  
}
	  
function hideTenorTable() {	
	  if(document.getElementById("MultiTenorLabel"))
		document.getElementById("MultiTenorLabel").style.display = 'none' ;
	  if(document.getElementById("MultTenor"))
		document.getElementById("MultTenor").style.display = 'none' ;
	  if(document.getElementById("tenorButtons"))
		document.getElementById("tenorButtons").style.display = 'none' ;
	deleteTenor(); <%-- IR T36000018006 --%>
}	 
<%-----deleteTenor() function is already defined in 'Transaction-EXP_COL-ISS-MultiTenors.frag' IR #T36000023868 Commenting Starts
//IR T36000018006 start- delete multi tenor details when it is unselected
function deleteTenor() {
	var tbl = document.getElementById('MultTenor');//to identify the table in which the row will get insert
	var multiTenorIndex = tbl.rows.length;
	for(var i=1; i< multiTenorIndex; i++){
		
	if(document.getElementById('MultTenor').rows[i].style.display=='none'){
	//	if( dijit.byId('TenorInd'+i) && dijit.byId('TenorInd'+i).getValue()=='Y'|| dijit.byId('TenorInd'+i).getValue()=='on' ){
		if( dijit.byId('TenorInd'+i)){
				dijit.byId('TenorInd'+i).set('checked',false);
				document.getElementById('TenorInd'+i).checked = false;
				dijit.byId('Tenor'+i).set('value','');
				document.getElementById('Tenor'+i).value = "";
				dijit.byId('Tenor'+i+'Amount').set('value','');
				document.getElementById('Tenor'+i+'Amount').value = "";
				dijit.byId('Tenor'+i+'DraftNumber').set('value','');
				document.getElementById('Tenor'+i+'DraftNumber').value = "";
				
				
				dijit.byId('TenorInd'+i).destroy( true );
				dijit.byId('Tenor'+i).destroy( true );
				dijit.byId('Tenor'+i+'Amount').destroy( true );
				dijit.byId('Tenor'+i+'DraftNumber').destroy( true );
				
				document.getElementById('TenorInd'+i).removeAttribute('id'); 
				document.getElementById('Tenor'+i).removeAttribute("id"); 
				document.getElementById('Tenor'+i+'Amount').removeAttribute("id"); 
				document.getElementById('Tenor'+i+'DraftNumber').removeAttribute("id"); 
			}
		}//end of if checking style.
	}
	
	return false;
}
//IR T36000018006 end---IR #T36000023868 Ends----%>

function disableAddTenorButton(){	  
	require(["dojo/dom","dojo/domReady!" ], function(dom) {
  var table = dom.byId('MultTenor');
  var insertRowIdx = table.rows.length;
	if(insertRowIdx=6){	  
	 dijit.byId("AddTenorButton").disabled = true;
	}
	});
}

  require(["dijit/registry", "dojo/on", "dojo/ready"],
      function(registry, on, ready) {
    ready( function(dom){
    	if(dijit.byId("TradePortalConstants.PMT_SIGHT").checked == true){
    		hideTenorTable();
    	}else if(dijit.byId("TradePortalConstants.PMT_CASH_AGAINST").checked == true){
    		hideTenorTable();
    	}else if(dijit.byId("TradePortalConstants.PMT_DAYS_AFTER").checked == true){
    		hideTenorTable();
    	}else if(dijit.byId("TradePortalConstants.PMT_FIXED_MATURITY").checked == true){
    		hideTenorTable();
    	}else if(dijit.byId("TradePortalConstants.PMT_OTHER")){
    		dijit.byId("TradePortalConstants.PMT_OTHER").set('checked',true);
    		showTenorTable();
    	}
    });
  });
  
  
  
<%--   Added for on change data field respected radio needs to be selected Start --%>
  require(["dijit/registry", "dojo/on", "dojo/ready"],
	      function(registry, on, ready) {
	  	ready(function(){
	  		var date_Maturity=registry.byId("AtFixedMaturityDate");
			var dateFiled=registry.byId("TradePortalConstants.PMT_FIXED_MATURITY");
			if(!isReadOnly){
				on(date_Maturity, "change", function(){
					dateFiled.set("checked",true);
				},true);
			}
	  	});
	  
  });
<%-- Added for on change data field respected radio needs to be selected End --%>

require(["dijit/registry", "dojo/on", "dojo/ready"],
	      function(registry, on, ready) {
	  	ready(function(){
	  		var CaseOfNeedInd= registry.byId("CaseOfNeedInd");
			var CaseOfNeedName=registry.byId("CaseOfNeedName");
			if(!isReadOnly){
				on(CaseOfNeedName, "change", function(){
					CaseOfNeedInd.set("isChecked",true);
				},true);
			}
	  	});
	  
  });
function pickCaseOfNeedCheckbox() {
    require(["dijit/registry"],
      function(registry) {
  if (document.forms[0].InCaseOfNeedText.value > ''
     || document.forms[0].CaseOfNeedName.value > ''
     || document.forms[0].CaseOfNeedAddressLine1.value > ''
     || document.forms[0].CaseOfNeedAddressLine2.value > ''
     || document.forms[0].CaseOfNeedAddressLine3.value > ''
     || document.forms[0].CaseOfNeedType.value > '' ) {
    
      <%-- document.forms[0].CaseOfNeedInd.checked = true; --%>
      registry.byId('CaseOfNeedInd').set("checked",true);
  } 
  else {
 
		<%-- document.forms[0].CaseOfNeedInd.checked = false; --%>
		registry.byId('CaseOfNeedInd').set("checked",false);
	}
  });
} 
require(["dijit/registry", "dojo/on", "dojo/ready"],
	      function(registry, on, ready) {
	  	ready(function(){
	  		var CollectionChargesText= registry.byId("CollectionChargesText");
			var COLLECTION_CHARGES_TYPE_ALL=registry.byId("TradePortalConstants.COLLECTION_CHARGES_TYPE_ALL");
			if(!isReadOnly){ 
				on(CollectionChargesText, "change", function(){
				COLLECTION_CHARGES_TYPE_ALL.set("checked",true);
				},true);
			}
	  	});
	  
});
require(["dijit/registry", "dojo/on", "dojo/ready"],
	      function(registry, on, ready) {
	  	ready(function(){
	  		var COLLECTION_CHARGES_TYPE_ALL=registry.byId("TradePortalConstants.COLLECTION_CHARGES_TYPE_ALL");
			var ChargesPhraseItem= registry.byId("ChargesPhraseItem");
			if(!isReadOnly){
				on(ChargesPhraseItem, "change", function(){
					COLLECTION_CHARGES_TYPE_ALL.set("checked",true);
				},true);
			}
	  	});
	  
});

require(["dijit/registry", "dojo/on", "dojo/ready"],
    	      function(registry, on, ready) {
    	  	ready(function(){
    	  		var InterestPhraseItem= registry.byId("InterestPhraseItem");
    			var CollectInterestInd=registry.byId("CollectInterestInd");
    			if(!isReadOnly){		
	    			on(InterestPhraseItem, "change", function(){
	    				CollectInterestInd.set("checked",true);
	    			},true);
    			}
    	  	});
    	  
      });

require(["dijit/registry", "dojo/on", "dojo/ready"],
    	      function(registry, on, ready) {
    	  	ready(function(){
    			var CollectInterestInd=registry.byId("CollectInterestInd");
				var CollectInterestAtText=registry.byId("CollectInterestAtText");
				if(!isReadOnly){		
					on(CollectInterestAtText, "change", function(){
	    				CollectInterestInd.set("checked",true);
	    			},true);
				}
    	  	});
    	  
      });

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

</script>
