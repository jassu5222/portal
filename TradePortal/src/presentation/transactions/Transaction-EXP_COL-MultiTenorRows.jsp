<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.ArrayList, java.util.Hashtable" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 
  //parameters -
  String parmValue = "";
String frmCcyCode  = null;
String toCcyCode  = null;
String frmCcyOptions = null;
String toCcyOptions = null;
String frmCcyDefaultText = null;
String toCcyDefaultText = null;
String multiplyIndicator = null;
String dropdownOptions = null;
String multiDivValue= null;
String loginLocale;
String loginRights;
  boolean isReadOnly = false; //request.getParameter("isReadOnly");
  boolean isFromExpress = false; //request.getParameter("isFromExpress");
  boolean isExpressTemplate = false; //request.getParameter("isExpressTemplate");
  boolean isTemplate = false; //request.getParameter("isExpressTemplate");
  parmValue = request.getParameter("multiTenorIndex");
  int multiTenorIndex = 0;
  if ( parmValue != null ) {
    try {
    	multiTenorIndex = (new Integer(parmValue)).intValue();
    } 
    catch (Exception ex ) {
    }
  }

  //other necessaries
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();
  
  frmCcyDefaultText = resMgr.getText("FXRatesDetail.SelectCurrency", TradePortalConstants.TEXT_BUNDLE);
  toCcyDefaultText = resMgr.getText("FXRatesDetail.SelectCurrency", TradePortalConstants.TEXT_BUNDLE);
  DocumentHandler doc = formMgr.getFromDocCache();
  
  TermsWebBean terms              = null;
  terms = (TermsWebBean) beanMgr.getBean("Terms");
  try{
	  terms.populateFromXmlDoc(doc, "/In");
  } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
%>
<%--Naveen 02-August-2012 IR-T36000003243. Uncommented below line and updated the path--%>
<%-- Transaction-EXP_COL-MultiTenorRows.jsp file is no more in use.
The below frag file is used in many places, disconnecting it from current jsp --%>
<%--@ include file="fragments/Transaction-EXP_COL-AMD-Tenors.frag" --%>
