<%--
*******************************************************************************
                        Funds Transfer Between Accounts Page

  Description:    
    This is the main driver for the Funds Transfer Between Accounts page.  It handles 
  data retrieval of terms (or retrieval from the input document)
  and creates the html page to display all the data for a Funds Transfer Request.
*******************************************************************************
--%>
      
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated  
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,com.amsinc.ecsg.util.DateTimeUtility, java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"     scope="session"> </jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"> </jsp:useBean> 
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"> </jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"     scope="session"> </jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  String onLoad = "";
  String focusField = "amount_currency_code";   // Default focus field
  
  // Various oid and status info from transaction and instruments used in
  // several places.
  String transferDay = "";
  String transferMonth = "";
  String transferYear = "";
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String rejectionIndicator = "";	
  String rejectionReasonText = ""; 	
  String prevTransStatusFromDoc = null;	
  
  String corpOrgOid = null;

  String links= "";
  
  boolean getDataFromDoc;              // Indicates if data is retrieved from the input doc cache or from the database
  boolean instrumentSelected = false;  // Indicates if an instrument was selected.
  boolean isStatusVerifiedPendingFX 	= false;   

  
  
  //WidgetFactory object is creating here
  WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession); 

  
  DocumentHandler doc;
  DocumentHandler fromAccountDoc = new DocumentHandler();
  DocumentHandler toAccountDoc = new DocumentHandler();
  DocumentHandler currencyIDDoc = new DocumentHandler();
  DocumentHandler templateList = new DocumentHandler();
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();
  
  DocumentHandler CBCResult = null;
  Cache reCertCache = null;
  
  // Variables used for dealing with a terms party's accounts.
  String acctNum = "";
  String acctCcy = "";

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;

  // These are the beans used on the page.
  TransactionWebBean transaction  = (TransactionWebBean)beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)beanMgr.getBean("Template");
  TermsWebBean terms              = null;

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();
  Debug.debug("doc from cache is " + doc.toString());

  // Assume we get the data from the doc.
  getDataFromDoc = true;
  

  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
  
  String bButtonPressed = doc.getAttribute("/In/Update/ButtonPressed");


  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }

  //In the case where reCertification/Authentication req'd on authorized, the page is reloaded with error
  //but the status of the transaction actually chnaged.
  else
  {
          String errorSectionText = null;
          if (doc.getComponent("/Error") != null)
    	{
    		errorSectionText = doc.getComponent("/Error").toString();
    	}
    	if (errorSectionText != null)
    	{
    		if (errorSectionText.indexOf(TradePortalConstants.CERTIFICATE_AUTH_REQ_ERR) != -1)
    		{
  				prevTransStatusFromDoc = doc.getAttribute("/In/Transaction/transaction_status"); //IAZ CM-451 03/24/09 Add    			
    			getDataFromDoc = false;
    			transaction.getDataFromAppServer();
    		}
    	}
  }

  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }

  
  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  
  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        transferDay = doc.getAttribute("/In/Transaction/transfer_day");
        transferMonth = doc.getAttribute("/In/Transaction/transfer_month");
        transferYear = doc.getAttribute("/In/Transaction/transfer_year");
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to reload data after returning to page. Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database. 
     // Instrument and Transaction were already retrieved.  Get the rest of the data
     
     terms = transaction.registerCustomerEnteredTerms();

     // Because date fields are represented as three fields on the page,
     // we need to parse out the date values into three fields.  Pre-mediator
     // logic puts these three fields back together for the terms bean.
     
     String date = transaction.getAttribute("payment_date");
     transferDay = TPDateTimeUtility.parseDayFromDate(date);
     transferMonth = TPDateTimeUtility.parseMonthFromDate(date);
     transferYear = TPDateTimeUtility.parseYearFromDate(date);
  }

  //ctq - if newTransaction, set session variable so it persists
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }

  //cquinton 1/18/2013 remove old close action behavior

  boolean defaultCheckBoxValue = false;
  
  // Don't format the amount if we are reading data from the doc
  String amount = terms.getAttribute("amount");
  String currency = terms.getAttribute("amount_currency_code");
  String displayAmount;
  String displayExchAmount;
  
  if(!getDataFromDoc) {
     displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currency, loginLocale);
     displayExchAmount= TPCurrencyUtility.getDisplayAmount(terms.getAttribute("equivalent_exch_amount"), terms.getAttribute("equivalent_exch_amt_ccy"), loginLocale);
	 }
  else {
     displayAmount = amount;
     displayExchAmount= terms.getAttribute("equivalent_exch_amount");
	 }

  
  BigInteger requestedSecurityRight = SecurityAccess.TRANSFER_CREATE_MODIFY;
%>

<%@ include file="fragments/Transaction-PageMode.frag" %>

<%!   
   /* IAZ/VS CR-511 01/12/10 Begin-Delete
   public DocumentHandler filterResults(String includeTag, DocumentHandler doc) { 
	  DocumentHandler newDoc = new DocumentHandler(doc.toString(),false);
	  DocumentNode node =newDoc.getRootNode();
	  node = node.getLeftChildNode();
      while (node != null) {
       	  if (!check(includeTag, node)) {
    		node.getParentNode().removeChild(node);  
    	  }
    	  node = node.getRightSiblingNode(); 
       }
      return newDoc;
    }

    public boolean check(String tag, DocumentNode node) {
	  node = node.getLeftChildNode();
      while (node != null) {
    	  if (tag.equals(node.getTagName()) && TradePortalConstants.INDICATOR_YES.equals(node.getNodeValue(true))) {
    		  return true;
    	  }
    	  node = node.getRightSiblingNode();
      }
      return false;
    } IAZ/VS CR-511 01/12/10 End-Delete*/
%>

<%
    if (isTemplate) 
   	   corpOrgOid = template.getAttribute("owner_org_oid");
    else
   	   corpOrgOid = instrument.getAttribute("corp_org_oid");
 
  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");
  
  rejectionIndicator = transaction.getAttribute("rejection_indicator");		
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");	

  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);
  
  boolean isFXeditable = (TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED.equals(transaction.getAttribute("transaction_status")) || TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE.equals(transaction.getAttribute("transaction_status")));
  
  // Retrieve the data used to populate some of the dropdowns.


      //check if this is a bank user
      String sqlString = "select name from client_bank, users where user_oid = ? and p_owner_org_oid = organization_oid";
      DocumentHandler bankUser = DatabaseQueryBean.getXmlResultSet(sqlString, false, new Object[]{userSession.getUserOid()});
      

      boolean isSubsUser = false;
      if (userSession.hasSavedUserSession())
      		 isSubsUser = true;

		 

      //if bankUser or subsidiary access, bring up all accounts for this customer
      //otherwise, only display accounts for this user
      StringBuffer sql = new StringBuffer();
      List<Object> sqlParam = new ArrayList<Object>();
      
      if ((bankUser == null) && (!isSubsUser))
      {
      	// Get all the Account defined in Account Access for Fund Transfer section in user detail  
      	sql.append("select a.P_OWNER_OID, a.account_oid, a.account_number || '-' || u.account_description || ' (' || a.currency || ')' as account,");
      	sql.append("a.currency as currency, a.OTHER_ACCOUNT_OWNER_OID");								
      	sql.append(" from account a, user_authorized_acct u");
      	sql.append(" where a.account_oid = u.a_account_oid and u.p_user_oid = ?");
      	sqlParam.add(userSession.getUserOid());
      }
      else
      {
      	sql.append("select a.P_OWNER_OID, a.account_oid, a.account_number || ' (' || a.currency || ')' as account,");
      	sql.append("a.currency as currency, a.OTHER_ACCOUNT_OWNER_OID");											
      	sql.append(" from account a");
      	sql.append(" where a.p_owner_oid = ?"); 
      	sqlParam.add(userSession.getOwnerOrgOid());
      }
      /*Rel 8.3 IR T36000022821 - show deactivated account for instruments which are in "processed by bank" status*/
      if(!isReadOnly){
      	sql.append(" and a.deactivate_indicator != 'Y'");
      }
      sql.append(" and a.available_for_xfer_btwn_accts = 'Y'"); 

      if (reCertCache == null){
    	  reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
      }
      if (CBCResult == null){
    	  CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());
      }
      String allowPayByAnotherAccount = CBCResult.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
      if (TradePortalConstants.INDICATOR_NO.equals(allowPayByAnotherAccount)){
    	  sql.append(" and a.othercorp_customer_indicator != 'Y'"); 
      }


	  sql.append(" order by ");
	  sql.append(resMgr.localizeOrderBy("currency"));

      currencyIDDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParam);

      if (currencyIDDoc != null) {
      	  fromAccountDoc = currencyIDDoc;
      	  toAccountDoc = currencyIDDoc;												
      }

      
    //Template Dropdown Data Retrive
  	%>
  	<%@include file="fragments/TransactionGetUser.frag" %>
  	<%
    
    String confidentialClause = "";
    if (userSession.getSavedUserSession() == null)
    {
      if (TradePortalConstants.INDICATOR_NO.equals(loggedUser.getAttribute("confidential_indicator")))
          confidentialClause = "and t.confidential_indicator = 'N' ";
    }   	
    else
    {
      if (!TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))	
         if (TradePortalConstants.INDICATOR_NO.equals(loggedUser.getAttribute("subsid_confidential_indicator")))
            confidentialClause = "and t.confidential_indicator = 'N' ";
    }


    if (!isTemplate) {
      boolean useTemplateGroup = false;
      if (userSession.getSavedUserSession() == null)
      {
      	QueryListView queryTemplateListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 
      
      	String checkTemplateGroupSql = "select tg.authorized_template_group_oid from USER_AUTHORIZED_TEMPLATE_GROUP tg where tg.p_user_oid = ? ";
        queryTemplateListView.setSQL(checkTemplateGroupSql, new Object[]{userSession.getUserOid()});
        queryTemplateListView.getRecords();

        if (queryTemplateListView.getRecordCount() != 0)
        	useTemplateGroup = true;
      }

      String sqlStr;
      List<Object> sqlParams = new ArrayList<Object>();
      if (useTemplateGroup)
      {
      	sqlStr = 
      "select i.original_transaction_oid, i.original_transaction_oid || '/' || t.template_oid as TransactionOid, t.name as TemplateName, t.ownership_level as ownershipLevel, t.ownership_type as ownershipType, i.instrument_type_code as instrumentType, p.name as partyName from template t, instrument i, terms_party p, USER_AUTHORIZED_TEMPLATE_GROUP tg where t.c_template_instr_oid = i.instrument_oid and t.default_template_flag = 'N' and i.a_counter_party_oid = p.terms_party_oid (+)  and i.instrument_type_code = 'FTBA' and t.payment_templ_grp_oid = tg.A_TEMPLATE_GROUP_OID and tg.p_user_oid = ? "
      + confidentialClause + " order by upper(TemplateName)";
		sqlParams.add(userSession.getUserOid());
      }
      else
      {
      	sql = new StringBuffer("select i.original_transaction_oid, t.name as TemplateName from template t, instrument i, terms_party p where t.c_template_instr_oid = i.instrument_oid and t.default_template_flag = 'N' and i.a_counter_party_oid = p.terms_party_oid (+)  and i.instrument_type_code = 'FTBA' and p_owner_org_oid in ("); 
      	sql.append("?,?,");
      	sql.append("?,?) " + confidentialClause + " order by upper(TemplateName)");
      	sqlStr = sql.toString();
      	sqlParams.add(userSession.getOwnerOrgOid());
      	sqlParams.add(userSession.getClientBankOid());
      	sqlParams.add(userSession.getGlobalOrgOid());
      	sqlParams.add(userSession.getBogOid());
      }
	  Debug.debug(sqlStr);
      templateList = DatabaseQueryBean.getXmlResultSet(sqlStr, false, sqlParams);

    }
    String requestMarketRateInd = terms.getAttribute("request_market_rate_ind"); 
	String selectedInstrument = transaction.getAttribute("transaction_oid") + "/" +  instrument.getAttribute("instrument_oid") + "/" + transactionType;
	selectedInstrument=EncryptDecrypt.encryptStringUsingTripleDes(selectedInstrument, userSession.getSecretKey());
 %>
 
<%
String pageTitleKey="";
if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
  pageTitleKey = "SecondaryNavigation.NewInstruments";
  
  if (isTemplate){
       if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
       userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
  }else{
     userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
  }
  }else
       userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
} else {
  pageTitleKey = "SecondaryNavigation.Instruments";
  if (isTemplate){
       if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
       userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
  }else{
     userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
  }
  }else 
       userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
}

  
String helpUrl = "customer/issue_transfer_betw_accts.htm";

  // The navigation bar is only shown when editing templates.  For transactions
  // it is not shown ti minimize the chance of leaving the page without properly
  // unlocking the transaction.
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
 
   //Include ReAuthentication frag in case re-authentication is required for complete authorization of transactions for this client
   //cr498 - change certNewLink for certAuthURL
   String certAuthURL = "";
   
   if (reCertCache == null || CBCResult == null){ 
   	reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   	CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());
   }

   
   //cr498 - any instrument/transaction could require reauthentication
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth,InstrumentAuthentication.TRAN_AUTH__FTBA_ISS);
   if (requireAuth) {
      // If Transaction just got successfully authroized (so Tran status just got changed to REQ_AUTHENTICTN 
      // from somnething else, open the authentication window once Transaction Page reloads.
      if (((InstrumentServices.isNotBlank(prevTransStatusFromDoc)))&&(!TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN.equals(prevTransStatusFromDoc)))
      {
      		onLoad = onLoad + "openReauthenticationWindow('" + certAuthURL + "')";
      }
      
   }
   

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<%
  
  if (!isReadOnly) {
	 onLoad += "updateCurr();";
  }
  
%>
 
 

 <script>
  
 function getCurArr() {
     <%
     StringBuffer sb = new StringBuffer("new Array()");
     if (currencyIDDoc != null) {
     Vector v = currencyIDDoc.getFragments ("/ResultSetRecord");
     int numItems = v.size ();
     
     boolean firstLoop = true;
     if (numItems > 0) {
     String  cur="",id="",tempCur="";
     sb.delete(0,sb.length());
     for (int i = 0; i < numItems; i++)
     {
        DocumentHandler tdoc = (DocumentHandler)v.elementAt(i);
        cur = tdoc.getAttribute ("/CURRENCY");
        id = tdoc.getAttribute ("/ACCOUNT_OID");
        if (!cur.equals(tempCur)) {
        	tempCur = cur;
        	if (firstLoop) {
        		sb.append("[{");
        		firstLoop = false;
        	}
        	else { 	
        		sb.append("\"]},{");
        	}
        	sb.append("value:\"" + cur + "\",ids:[\"" + id);	
        }
        else {
        	sb.append("\",\"" + id);	
        }
        }
     }
       sb.append("\"]}]");
     }
     
   
     out.println("var curFields =" + sb.toString() + ";");
     //var curFields = [{value:"USD",ids:["11","22"]},{value:"CAN",ids:["33","44"]}];
	 %>
	 
     return curFields;
 } 


  function getAllCurr() {
	  var arr=getCurArr();
	  var newarr = new Array();
	  newarr[0]="";
	  for (i=0;i<arr.length;i++) {
		  newarr[i+1] = arr[i].value;
	   }
      return newarr;
  }

  function getCurForID(id) {
	  var arr=getCurArr();
	  for (i=0;i<arr.length;i++) {
		  for (j=0; j<arr[i].ids.length; j++) {
			  if (arr[i].ids[j] == id) {
				  return arr[i].value;
			  }	
		  }
	  }	  	    
      return "";
  }   
  
  function getfilteredCurr(from, to) {
	  var newarr = new Array();
	  var h=0;
	  var fromCur = getCurForID(from);
	  var toCur = getCurForID(to);
	  newarr[h++]="";
	  newarr[h++]=fromCur;
	  if (fromCur != toCur) {
		  newarr[h++]=toCur;
	  }	  	    
      return newarr;
  }
  
 
  function updateCurr() {

  require(["dijit/registry", "dojo/ready"],
		function(registry, ready ) {
    <%-- var from = document.TransactionFTBA.from_account_oid.value; --%>
	var from = dijit.byId('from_account_oid').value;
    <%-- var to = document.TransactionFTBA.to_account_oid.value; --%>
	var to = dijit.byId('to_account_oid').value;
	
    <%-- var selected=document.TransactionFTBA.amount_currency_code.value; --%>
	var selected = dijit.byId('amount_currency_code').value;
    <%-- var acctx  = document.forms[0].from_account_oid.options[document.forms[0].from_account_oid.selectedIndex].value; --%>


	var selectedIndexFrom = document.forms[0].from_account_oid.selectedIndex;
	var selectedIndexTo = document.forms[0].to_account_oid.selectedIndex;
    if ((from ) && (to)) {
        populateCurrency(getfilteredCurr(from,to),selected);
     } 
    <%--//else it will just display all dropdown values by default. 
        	  
    //AAlubala CR610 Rel7.0.0.0 02/28/2011
    //Obtaining the account owner name that corresponds
    //to the selected account from the drop-down list
    //then displaying this information to the user
    //--%>
    var accntOwnerFrom = document.getElementById("accntOwnerFrom");
    var accntOwnerTo = document.getElementById("accntOwnerTo");
    
    var orderingPartyFrom = "";
    var orderingPartyTo = "";
   
    <%-- example --%>
    <%-- <input type=hidden name="1174628" id="from1174628" value="CMRM1" /> --%>
    if(from)
    orderingPartyFrom = document.getElementById("from"+from).value;

    if(to)
    orderingPartyTo = document.getElementById("to"+to).value;
   
    if(accntOwnerFrom != null){ 
  		if(orderingPartyFrom != null){
  			accntOwnerFrom.innerHTML = orderingPartyFrom;
  		}else
       		accntOwnerFrom.innerHTML = "";
    }    
    if(accntOwnerTo != null){ 
  		if(orderingPartyTo != null){
  			accntOwnerTo.innerHTML = orderingPartyTo;
  		}else
       		accntOwnerTo.innerHTML = "";
    }    
    
    });
  }



  function populateCurrency(cArr,selected)
  {
     require(["dijit/registry", "dojo/ready"],
		function(registry, ready ) {
 
			var curr= dijit.byId('amount_currency_code');
			
			curr.store.fetch({
			query: {name: '*'},
			onComplete: function (items) {
				dojo.forEach(items, function (item, i) {
					curr.store.remove(item.id);
				});

			}
			
		}); 
			
			<%-- curr.dropDown.destroy() --%>
			var selVal=null;

			for (i=0;i<cArr.length;i++) {
     
				if (cArr[i] == selected) {
					curr.store.add({disabled:false,id:cArr[i],name:cArr[i],label:cArr[i],selected:true,value:cArr[i]});
					selVal=cArr[i];
				}
				else {
					curr.store.add({disabled:false,id:cArr[i],name:cArr[i],label:cArr[i],selected:false,value:cArr[i]});
				}
			}
   				curr.reset();
				curr.set("value",selVal);
		});
   }
   
   	
</script>


<% 
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
%>

<div class="pageMain">
<div class="pageContent">



<%
	if(isTemplate) {
		  String pageTitle = resMgr.getText("common.template", TradePortalConstants.TEXT_BUNDLE);
		  StringBuffer title = new StringBuffer();
		  String itemKey = "";
		  title.append(pageTitle);
		  title.append( " : " );
		  title.append( ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE, instrument.getAttribute("instrument_type_code"), loginLocale) );
		  title.append( " - " );
		  title.append(template.getAttribute("name") );
		
		  String transactionSubHeader = title.toString();
		  String returnUrl=	formMgr.getLinkAsUrl("goToInstrumentCloseNavigator", response);
  		
  		%>
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
				<jsp:param name="helpUrl"  value="customer/issue_transfer_betw_accts.htm" />
  		</jsp:include>
  		
  		<jsp:include page="/common/PageSubHeader.jsp">
 				 <jsp:param name="titleKey" value="<%= transactionSubHeader%>" />
  				 <jsp:param name="returnUrl" value="<%= returnUrl %>" />
		</jsp:include> 		
	<%} else { %>
	<jsp:include page="/common/PageHeader.jsp">
	   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
	   <jsp:param name="item1Key" value="SecondaryNavigation.Instruments.TransferBetweenAccounts" />
	   <jsp:param name="helpUrl"  value="customer/issue_transfer_betw_accts.htm" />
	</jsp:include>


	<jsp:include page="/common/TransactionSubHeader.jsp" />

<%} %>

<form name="TransactionFTBA" id="TransactionFTBA" method="post"   data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name=buttonName>

<%-- error section goes above form content --%>
<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">

<%
 
  if (requireAuth) {
%> 

 <input type="hidden" name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
 <input type="hidden" name="reCertOK">
 <input type="hidden" name="logonResponse">
 <input type="hidden" name="logonCertificate">
  <%--AAlubala CR711 Rel 8.0 01/27/2012
  Add proxy value but do not set a value to it.
  The value will be set on the fly if the transaction requires to be 
  signed by 2FA token --%>
 <input type="hidden" name="proxy"> 
<%
 }
%> 

 
<%
  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

  secureParms.put("instrument_oid",        instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code",  instrumentType);
  secureParms.put("instrument_status",     instrumentStatus);
  secureParms.put("corp_org_oid",  corpOrgOid);

  secureParms.put("transaction_oid",       transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status",    transactionStatus);
  
  secureParms.put("transaction_instrument_info", transaction.getAttribute("transaction_oid") + "/" + 	instrument.getAttribute("instrument_oid") + "/" + 	transactionType);

    secureParms.put("userOid",userSession.getUserOid());
    secureParms.put("securityRights",userSession.getSecurityRights()); 
    secureParms.put("clientBankOid",userSession.getClientBankOid()); 
    secureParms.put("ownerOrg",userSession.getOwnerOrgOid());

  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");

  if (termsOid == null)
  {
     termsOid = "0";
  }
  secureParms.put("terms_oid", termsOid);

  if (isTemplate)
  {
     secureParms.put("template_oid", template.getAttribute("template_oid"));
     secureParms.put("opt_lock",     template.getAttribute("opt_lock"));
  }
%>

<%
  if (isTemplate)
  {
%>
    <%@ include file="fragments/TemplateHeader.frag" %>
<%
  }
  else
  {
%>
  
<%
  }
%>

   <%@ include file="fragments/Instruments-AttachDocuments.frag" %>

<%
  
  // Rejection Text Section
  if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES) || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
     </div>
<%
  }
  
%>  

<% //CR 821 Added Repair Reason Section 
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?"  );
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");

	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), false, new Object[]{transaction.getAttribute("transaction_oid")});
	
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
<%} %> 
	
 <%@ include file="fragments/Transaction-FTBA.frag" %>

 
 <% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
<%} %> 
 
<%= formMgr.getFormInstanceAsInputField("Transaction-FTBAForm", secureParms) %>


</div>    <%-- formContent Closing Div --%>
<div style="clear:both";></div> 
</div>
<div class="formSidebar" data-dojo-type="widget.FormSidebar"    data-dojo-props="form: 'TransactionFTBA'">
	<jsp:include page="/common/Sidebar.jsp">
	<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
	<jsp:param value="<%=certAuthURL%>" name="certAuthURL" />
	<jsp:param name="buttonPressed" value="<%=bButtonPressed%>" />
	<jsp:param name="error" value="<%=error%>" />  
	<jsp:param name="formName" value="0" />
	<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
	<jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
	</jsp:include>

</div>

</form>
</div>
</div>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SidebarFooter.jsp"/>


<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
	require(["dijit/registry", "dojo/on", "dojo/ready"],
        function(registry, on, ready ) {
      ready(function() {
          updateCurr();
      	  
      	  var display;
      	  if('<%=requestMarketRateVisibility%>'=='hidden') display='none';
      	  if('<%=requestMarketRateVisibility%>'=='visible') display='block';
      	  
      	dojo.style("requestMarketRate", "visibility", "<%=requestMarketRateVisibility%>");
      	dojo.style("requestMarketRate", "display", display);
      	  
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
</script>
<%
  }
%>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
