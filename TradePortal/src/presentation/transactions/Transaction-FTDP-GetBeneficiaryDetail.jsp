  
<%--  
*******************************************************************************
                        Payment - Get Bene detail info

*******************************************************************************
--%>
      
<%-- 
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*, net.sf.json.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"    scope="session"> </jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"> </jsp:useBean> 
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"> </jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"     scope="session"> </jsp:useBean>
   
<%

response.setContentType("application/json");
response.setCharacterEncoding("UTF-8");


//HashMap 		rMap				 = new HashMap();
JSONObject rMap = new JSONObject();
//String payeeInvoiceDetails 			 = "";
 
String          userSecurityRights   = userSession.getSecurityRights();
String          userOid              = userSession.getUserOid();
String          domPmtOid         	 = request.getParameter("rowKey");
String          bankGroupOid         	 = request.getParameter("bankgOid");
String	        readOnly			= request.getParameter("readOnly");
String	        debitAcc			= request.getParameter("accountOID");
domPmtOid =EncryptDecrypt.decryptStringUsingTripleDes(domPmtOid, userSession.getSecretKey());
if (InstrumentServices.isNotBlank(bankGroupOid)) {
	bankGroupOid =EncryptDecrypt.decryptStringUsingTripleDes(bankGroupOid, userSession.getSecretKey());
}


DomesticPaymentWebBean currentDomesticPayment = beanMgr.createBean(DomesticPaymentWebBean.class, "DomesticPayment");  


currentDomesticPayment.setAttribute("domestic_payment_oid", domPmtOid);
currentDomesticPayment.getDataFromAppServer();
//rMap.put("domPmtOid",domPmtOid);
//rMap.put("bankGroupOid",bankGroupOid);
//rMap.put("readOnly",readOnly);


 String defaultCntry = currentDomesticPayment.getAttribute("country");
 if (InstrumentServices.isNotBlank(defaultCntry)) {
    defaultCntry=ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY, defaultCntry);
 }
 rMap.put("Country",defaultCntry);
 
 String defaultDeliveryMethod = currentDomesticPayment.getAttribute("delivery_method");
 if (InstrumentServices.isNotBlank(defaultDeliveryMethod)) {
     defaultDeliveryMethod=ReferenceDataManager.getRefDataMgr().getDescr("DELIVERY_METHOD_DELIVER_TO", defaultDeliveryMethod);
 }
 rMap.put("DeliveryMethod",defaultDeliveryMethod);

	// Get invoice details from DB if cached doc does not have correct details for current payment
	StringBuffer invDetSql = new StringBuffer("SELECT INVOICE_DETAIL_OID FROM INVOICE_DETAILS WHERE DOMESTIC_PAYMENT_OID = ?");

	DocumentHandler invDetOidDoc = DatabaseQueryBean.getXmlResultSet(invDetSql.toString(), false, new Object[]{domPmtOid});
	if (invDetOidDoc != null) {
		String invoiceDetailOid = invDetOidDoc.getAttribute("/ResultSetRecord/INVOICE_DETAIL_OID");
		if (InstrumentServices.isNotBlank(invoiceDetailOid)) {
			InvoiceDetailsWebBean paymentInvoiceDetails = beanMgr.createBean(InvoiceDetailsWebBean.class, "InvoiceDetails"); 
			paymentInvoiceDetails.setAttribute("invoice_detail_oid", invoiceDetailOid);
			paymentInvoiceDetails.getDataFromAppServer();
			
			String payeeInvoiceDetails = paymentInvoiceDetails.getAttribute("payee_invoice_details");
			String invoiceDetailOID = paymentInvoiceDetails.getAttribute("invoice_detail_oid");
			
			rMap.put("payeeInvoiceDetails",payeeInvoiceDetails);
			rMap.put("InvoiceDetailOID",invoiceDetailOID);
			
		}
	}
	
  
   		if (InstrumentServices.isNotBlank(currentDomesticPayment.getAttribute("c_FirstIntermediaryBank")))
   		{
			PaymentPartyWebBean firstIntBank = beanMgr.createBean(PaymentPartyWebBean.class, "PaymentParty");
   			firstIntBank.setAttribute("payment_party_oid", currentDomesticPayment.getAttribute("c_FirstIntermediaryBank"));
   			firstIntBank.getDataFromAppServer();
			
			String countryCode = firstIntBank.getAttribute("country");
			String countryDesc = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY, countryCode, 	resMgr.getCSDB().getLocaleName());
			String c_FirstIntermediaryBank = currentDomesticPayment.getAttribute("c_FirstIntermediaryBank");
			
			//rMap.put("c_FirstIntermediaryBank",EncryptDecrypt.encryptStringUsingTripleDes(c_FirstIntermediaryBank, userSession.getSecretKey()));
			rMap.put("FirstIntBankBranchCode",firstIntBank.getAttribute("bank_branch_code"));
			rMap.put("FirstIntBankData", buildPaymentBankDisplayData(firstIntBank.getAttribute("bank_name"), 
																	firstIntBank.getAttribute("branch_name"), 
																	firstIntBank.getAttribute("address_line_1"), 
																	firstIntBank.getAttribute("address_line_2"), 
																	firstIntBank.getAttribute("address_line_3"), 
																	countryDesc));
			rMap.put("FirstIntBankName",firstIntBank.getAttribute("bank_name"));
			rMap.put("FirstIntBankBranchName",firstIntBank.getAttribute("branch_name"));
			rMap.put("FirstIntBankAddressLine1",firstIntBank.getAttribute("address_line_1"));
			rMap.put("FirstIntBankAddressLine2",firstIntBank.getAttribute("address_line_2"));
			rMap.put("FirstIntBankAddressLine3",firstIntBank.getAttribute("address_line_3"));
			rMap.put("FirstIntBankCountry",firstIntBank.getAttribute("country"));
			
	
   		}
   	   //RPasupulati IR T36000032489
       rMap.put("REPORTING_CODE_1",getReportingCode(currentDomesticPayment.getAttribute("reporting_code_1"),bankGroupOid,"payment_reporting_code_1"));
	   rMap.put("REPORTING_CODE_2",getReportingCode(currentDomesticPayment.getAttribute("reporting_code_2"),bankGroupOid,"payment_reporting_code_2"));


//out.println(getResults(rMap));
out.println(rMap.toString());

%>

<%!

private String getReportingCode(String code,String bankGroupOid, String table ) {

	String val="";
	
	if (InstrumentServices.isBlank(bankGroupOid) || InstrumentServices.isBlank(code)) return val;
	
	StringBuffer sql = new StringBuffer("select CODE, DESCRIPTION");
	sql.append(" from ").append(table);
	sql.append(" where p_bank_group_oid = ? and code = ?");

	try {
			DocumentHandler xmlDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, bankGroupOid, code);

			if (xmlDoc != null) {
				val = xmlDoc.getAttribute("/ResultSetRecord/DESCRIPTION");
			}
	}
	catch (Exception e) {
		e.printStackTrace();
	}
	return val; 

}

private String  getResults(HashMap rMap) {
	int ctr =0;
	StringBuffer buf = new StringBuffer("{");
    Iterator it = rMap.entrySet().iterator();
    while (it.hasNext()) {
        Map.Entry pairs = (Map.Entry)it.next();
		if (ctr!=0) buf.append(",");
		buf.append(pairs.getKey()).append("'").append(pairs.getValue()).append("'");
		ctr++;
    }
	buf.append("}");
	return buf.toString();
}



   
    public void appendPaymentBankDisplayData(StringBuffer bankDData, String separator, String data) {
      	if (InstrumentServices.isNotBlank(data)) {
          	if (bankDData.length()>0) bankDData.append(separator);
          	bankDData.append(data);
      	}
   }
 private String buildPaymentBankDisplayData(String bankName, String branchName, String addr1, String addr2, String addr3, String country)    {

    String eol = "\n";
   	StringBuffer bankDData = new StringBuffer();
   	appendPaymentBankDisplayData(bankDData, eol, bankName);
   	appendPaymentBankDisplayData(bankDData, eol, branchName);
   	appendPaymentBankDisplayData(bankDData, eol, addr1);
   	appendPaymentBankDisplayData(bankDData, eol, addr2);
   	appendPaymentBankDisplayData(bankDData, eol, addr3);
   	appendPaymentBankDisplayData(bankDData, " ", country);
   	return bankDData.toString();	
   }

%>