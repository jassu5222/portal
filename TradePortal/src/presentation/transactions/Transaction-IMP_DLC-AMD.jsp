
<%--
*******************************************************************************
                              Import DLC Amend Page

  Description:
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String focusField = "TransactionAmount";   // Default focus field
  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType;
  String transactionStatus;
  String corpOrgOid = "";

  boolean           amountUpdatedFromPOs          = false;
  String links = "";  

  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database

  DocumentHandler doc;
  String buttonPressed;
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  // Applicant name and expiry date from the issue transaction.  Displayed in General section
  String applicantName = "";
  String currentExpiryDate = null;

  // These are local variables used in the Shipment section for the PO line item
  // buttons and text area box
  DocumentHandler   poLineItemsDoc                = null;
  DocumentHandler   poLineItemDoc                 = null;
  StringBuffer      sqlQuery                      = null;
  StringBuffer      link                          = null;
  boolean           hasPOLineItems                = false;
  String            poLineItemUploadDefinitionOid = "";
  String            userOrgAutoLCCreateIndicator  = null;
  String            userCustomerAccessIndicator   = null;
  String            poLineItemBeneficiaryName     = "";
  String            poLineItemCurrency            = "";
  boolean 		  isProcessedByBank		  = false;
  Vector           poLineItems                   = null;
  String			rejectionIndicator            = "";
  String			rejectionReasonText	          = "";
  StringBuffer         LCAmount                 = new StringBuffer();
  StringBuffer         newLCAmount              = new StringBuffer();
  StringBuffer         availableAmount          = new StringBuffer();
  boolean canDisplayPDFLinks = false;

  // Dates are stored in the database as one field but displayed on the
  // page as 3.  These variables hold the pieces for each date.
  String expiryDay = "";
  String expiryMonth = "";
  String expiryYear = "";
  String shipDay = "";
  String shipMonth = "";
  String shipYear = "";

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;

  // This array of string is used when creating the LC Amendment Application Form links.
  String linkArgs[] = {"","","",""};
  
  
  
  // These are the beans used on the page.

  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");
  TermsWebBean terms              = null;

  ShipmentTermsWebBean shipmentTerms = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");

  // We need the amount of the original transaction to display.  Create a webbean
  // to hold this data.
  TransactionWebBean activeTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");

  // Get the document from the cache.  We'll may use it to repopulate the
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();

  	buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
	Vector error = null;
	error = doc.getFragments("/Error/errorlist/error");
	//KMehta Rel 9400 IR T36000042634 on 23 Sep 2015 Add-Begin 
	/* KMehta Rel 9500 IR T36000034805 on 16 Mar 2016 Updtaed Values */
	  String textAreaMaxlen = "3300"; //IR T36000034805
	  String textAreaMaxlength2 = "5000";
	  //KMehta Rel 9400 IR T36000042634 on 23 Sep 2015 Add-End 
  //Debug.debug("doc from cache is " + doc.toString());

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
  Party Search     exists                       SearchInfo to lookup, and
                                                populate a specific TermsParty
                                                web bean

  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
  Phrase Lookup                                 LookupInfo text (from /Out) to
                                                replace a specific phrase text
                                                in the /In document before
                                                populating

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction
    mediator
    (error)
******************************************************************************/

  // Assume we get the data from the doc.

  // W Zhu 7/18/07 EGUH071657459 & FUH011755336 Reverse the previous fix of FUH011755336
  // TLE - 01/18/07 IR-FUH011755336 - Add Begin
  getDataFromDoc = true;
  //getDataFromDoc = false;
  // TLE - 01/18/07 IR-FUH011755336 - Add End
  // W Zhu 7/18/07 EGUH071657459 & FUH011755336 END

  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;

  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache

     getDataFromDoc = true;

  }

  if (doc.getDocumentNode("/Out/ChangePOs") != null) {

     // Some data was updated by the add/remove POs process.
     // Update the data in the XML so that the page will relect the changes
     doc.setAttribute("/In/Terms/ShipmentTermsList/po_line_items",doc.getAttribute("/Out/ChangePOs/po_line_items"));
     doc.setAttribute("/In/Terms/amount", doc.getAttribute("/Out/ChangePOs/amount"));
     doc.setAttribute("/In/Terms/ShipmentTermsList/goods_description", doc.getAttribute("/Out/ChangePOs/goods_description"));

     //pmitnala 3/05/2013 Changes Begin for IR-T36000014541, PR BMO Issue-123 - Expiry Date or the Latest Ship Date were not derived from uploaded PO
     doc.setAttribute("/In/Terms/expiry_date", doc.getAttribute("/Out/ChangePOs/expiry_date"));
     doc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", doc.getAttribute("/Out/ChangePOs/latest_shipment_date"));
     //pmitnala 3/05/2013 Changes End for IR-T36000014541

     getDataFromDoc = true;
     amountUpdatedFromPOs = true;

   }

  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;

  }

 if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");

     getDataFromDoc = true;

     // Take the looked up and appended phrase text from the /Out section
     // and copy it to the /In section.  This allows the beans to be
     // properly populated.
     String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
     xmlPath = "/In" + xmlPath;

     doc.setAttribute(xmlPath,
                      doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

     // If we returned from the phrase lookup without errors, set the focus
     // to the correct field.  Otherwise, don't set the focus so the user can
     // see the error.
     if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
       focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
     }

  }



  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        expiryDay = doc.getAttribute("/In/Terms/expiry_day");
        expiryMonth = doc.getAttribute("/In/Terms/expiry_month");
        expiryYear = doc.getAttribute("/In/Terms/expiry_year");

        shipDay = doc.getAttribute("/In/Terms/shipment_day");
        shipMonth = doc.getAttribute("/In/Terms/shipment_month");
        shipYear = doc.getAttribute("/In/Terms/shipment_year");

        String activeTransOid = instrument.getAttribute("active_transaction_oid");
        if (!InstrumentServices.isBlank(activeTransOid)) {
           activeTransaction.setAttribute("transaction_oid", activeTransOid);
           activeTransaction.getDataFromAppServer();
        }

        // Some of the data for this transaction is stored as part of the shipment terms
        terms.populateFirstShipmentFromXml(doc);
     } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }


  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

        String activeTransOid = instrument.getAttribute("active_transaction_oid");
        if (!InstrumentServices.isBlank(activeTransOid)) {
           activeTransaction.setAttribute("transaction_oid", activeTransOid);
           activeTransaction.getDataFromAppServer();
        }


     // Because date fields are represented as three fields on the page,
     // we need to parse out the date values into three fields.  Pre-mediator
     // logic puts these three fields back together for the terms bean.
     String date = terms.getAttribute("expiry_date");
     expiryDay = TPDateTimeUtility.parseDayFromDate(date);
     expiryMonth = TPDateTimeUtility.parseMonthFromDate(date);
     expiryYear = TPDateTimeUtility.parseYearFromDate(date);

     // Some of the data for this transaction is stored as part of the shipment terms
     terms.populateFirstShipmentFromDatabase();

     date = terms.getFirstShipment().getAttribute("latest_shipment_date");
     shipDay = TPDateTimeUtility.parseDayFromDate(date);
     shipMonth = TPDateTimeUtility.parseMonthFromDate(date);
     shipYear = TPDateTimeUtility.parseYearFromDate(date);

  }

  
//if newTransaction, set session variable so it persists
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }

  // Now determine the mode for how the page operates (readonly, etc.)
  BigInteger requestedSecurityRight = SecurityAccess.DLC_CREATE_MODIFY;
%>
  <%@ include file="fragments/Transaction-PageMode.frag" %>


<%
  // Now we need to calculate the computed new lc amount total.  This involves
  // using the instument amount of the active transaction, the amount entered by the user,
  // performing some validation and then doing the calculation.

  MediatorServices medService = new MediatorServices();
  medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
  BigDecimal originalAmount = BigDecimal.ZERO;
  BigDecimal offsetAmount = BigDecimal.ZERO;
  BigDecimal calculatedTotal = BigDecimal.ZERO;
  String amountValue = null;
  String displayAmount;
  String incrDecrValue = TradePortalConstants.NOAMOUNT;
  String currencyCode = activeTransaction.getAttribute("copy_of_currency_code");
  boolean badAmountFound = false;

  // Convert the original transaction amount to a double
  try {
	  
     amountValue = activeTransaction.getAttribute("instrument_amount");
     originalAmount = new BigDecimal(amountValue);
  } catch (Exception e) {
     originalAmount = BigDecimal.ZERO;
  }
	
  if((currencyCode != null)
	      && (currencyCode.trim().length() > 0)
	      && (activeTransaction.getAttribute("available_amount") != null)
	      && (activeTransaction.getAttribute("available_amount").trim().length() > 0))
	   {
	      availableAmount.append(currencyCode);
	      availableAmount.append(" ");
	      availableAmount.append(TPCurrencyUtility.getDisplayAmount(activeTransaction.getAttribute("available_amount"),
	    		  currencyCode, loginLocale));
	   }


  //TLE - 01/29/07 - IR#FUH011755336 - Add Begin

 // jgadela 10/21/2014 R 9.1 - T36000033573 [START]- Re-factored the amendment amount logic to fix radio button and amout feild issues.
 if (doc.getAttribute("/In/Terms/amount") != null) {
     // Convert the amount for this terms record to a double.  If we're getting
     // data from the doc, use the value in the document and attempt to convert
     // to a double.  It may fail due to invalid number.  If so, we'll post 
     // an error and set a flag to prevent the calculation from being 
     // displayed.
     //
     // Furthermore, since the displayed terms amount is always displayed as a 
     // positive number (the Increase/Decrease dropdown represents the sign).
     // Therefore, we may need to set the double amount negative.
     amountValue = doc.getAttribute("/In/Terms/amount");
     incrDecrValue = doc.getAttribute("/In/Terms/IncreaseDecreaseFlag");
     Debug.debug("amountvalue " + amountValue);
     

     try {
        amountValue = NumberValidator.getNonInternationalizedValue(amountValue,loginLocale, false);
        try {
           offsetAmount = new BigDecimal(amountValue);;
        } catch (Exception e) {
           offsetAmount = BigDecimal.ZERO;
        }
        if (incrDecrValue.equals(TradePortalConstants.DECREASE)) {
           offsetAmount = offsetAmount.negate();
        }
        
        if(amountUpdatedFromPOs) {
            displayAmount = terms.getAttribute("amount");
            if( InstrumentServices.isBlank(displayAmount) && !isReadOnly ) {
               displayAmount = "";
            }
         }
         else {
        	// Don't format the number because we only came from the doc
             displayAmount = doc.getAttribute("/In/Terms/amount");
         }
        
     } catch (Exception e) {

        // Unable to unformat number so the display value is the save value
        // the user typed in.
        displayAmount = amountValue;
        badAmountFound = true;
        medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                  TradePortalConstants.INVALID_CURRENCY_FORMAT,
                                  amountValue);
        medService.addErrorInfo();
        doc.addComponent("/Error", medService.getErrorDoc());
        formMgr.storeInDocCache("default.doc", doc);
     }
  } else {	
     // We're getting the amount from the database so convert to a positive
     // number if necessary and set the Increase/Decrease dropdown accordingly.
     try {
        amountValue = terms.getAttribute("amount");
        offsetAmount = new BigDecimal(amountValue);
     } catch (Exception e) {
        offsetAmount = BigDecimal.ZERO;
     }
     if (offsetAmount.doubleValue() < 0){ incrDecrValue = TradePortalConstants.DECREASE;}
	 else if (offsetAmount.doubleValue() > 0){ incrDecrValue = TradePortalConstants.INCREASE;}
	 if( InstrumentServices.isBlank( amountValue )  && !isReadOnly) {
   	    displayAmount = "";
     }
     else{
       displayAmount = offsetAmount.abs().toString();
     }
	 //Srinivasu_D IR#T36000035537 ER9.1.0.2 01/01/2015 - handling minus
	 if(StringFunction.isNotBlank(amountValue)){
	 char amtChar[] = amountValue.toCharArray();
		if(amtChar[0] == '-'){
			amountValue = amountValue.substring(1,amountValue.length());
		}
	 }
	 //Srinivasu_D IR#T36000035537 ER9.1.0.2 01/01/2015 - End 
  }
//jgadela 10/21/2014 R 9.1 - T36000033573 [END]
if (!TradePortalConstants.NOAMOUNT.equals(incrDecrValue)){
 calculatedTotal = originalAmount.add(offsetAmount);
}
  LCAmount.append(currencyCode);
  LCAmount.append(" ");
  LCAmount.append(TPCurrencyUtility.getDisplayAmount(originalAmount.toString(),currencyCode, loginLocale));
  
  newLCAmount.append(currencyCode);
  newLCAmount.append(" ");  
  newLCAmount.append(TPCurrencyUtility.getDisplayAmount(calculatedTotal.toString(),currencyCode, loginLocale));
  

  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");
  transactionOid = transaction.getAttribute("transaction_oid");

  // Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");

  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

  // Create documents for the phrase dropdowns. First check the cache (no need
  // to recreate if they already exist).  After creating, place in the cache.
  // (InstrumentCloseNavigator.jsp cleans these up.)

  DocumentHandler phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
  if (phraseLists == null) phraseLists = new DocumentHandler();

  DocumentHandler goodsDocList = phraseLists.getFragment("/Goods");
  if (goodsDocList == null) {
     goodsDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_GOODS,
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/Goods", goodsDocList);
  }

  DocumentHandler addlCondDocList = phraseLists.getFragment("/AddlCond");
  if (addlCondDocList == null) {
     addlCondDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_ADDL_COND,
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/AddlCond", addlCondDocList);
  }

  DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
  if (spclInstrDocList == null) {
     spclInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SPCL_INST,
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/SpclInstr", spclInstrDocList);
  }

  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);


  ////////////////////////////////////////////////////////////////////////
  // Retrieve the latest information of applicatnt name and expiry date.
  // They are displayed in general section.
  ////////////////////////////////////////////////////////////////////////

  // Retrieve the latest information of expiry date from instrument.
  currentExpiryDate = instrument.getAttribute("copy_of_expiry_date");
  
  // Retrieve the applicant name from the bank release terms of the latest issuance or amendment transaction.
  //    (The latest transaction is the one that has the latest transaction status date and in case several transaction
  //     have the same transaction status date, the largest transaction_oid.)
  String instrument_oid = instrument.getAttribute("instrument_oid");
  StringBuffer issueTransactionSQL = new StringBuffer();
  issueTransactionSQL.append("select p.name")
                     .append(" from terms_party p, terms t, transaction tr")
                     .append(" where tr.transaction_oid =")
                     .append("       (select max(transaction_oid)")
                     .append("          from transaction")
                     .append("         where transaction_status_date = (select max(transaction_status_date)")
                     .append("                                            from transaction")
                     .append("                                           where p_instrument_oid = ?")
                     .append("                                             and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)")
                     .append("                                             and transaction_status = ?)")
                     .append("           and p_instrument_oid = ?")
                     .append("           and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)")
                     .append("           and transaction_status = ?)")
                     .append(" and tr.c_bank_release_terms_oid = t.terms_oid")
                     .append(" and (p.terms_party_oid = t.c_first_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_second_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_third_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_fourth_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_fifth_terms_party_oid)")
                     .append(" and p.terms_party_type = ?");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object[] sqlParamsTrans = new Object[11];
	sqlParamsTrans[0] =  instrument_oid;
	sqlParamsTrans[1] =  TransactionType.ISSUE;
	sqlParamsTrans[2] =  TransactionType.AMEND;
	sqlParamsTrans[3] =  TransactionType.CHANGE;
	sqlParamsTrans[4] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
	sqlParamsTrans[5] =  instrument_oid;
	sqlParamsTrans[6] =  TransactionType.ISSUE;
	sqlParamsTrans[7] =  TransactionType.AMEND;
	sqlParamsTrans[8] =  TransactionType.CHANGE;
	sqlParamsTrans[9] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
	sqlParamsTrans[10] =  TradePortalConstants.APPLICANT;
  DocumentHandler issueTransactionResult = DatabaseQueryBean.getXmlResultSet(issueTransactionSQL.toString(), false, sqlParamsTrans);
  if (issueTransactionResult!=null) {
     applicantName = issueTransactionResult.getAttribute("/ResultSetRecord(0)/NAME");
  }
  
  String onlineHelpFileName="";

	if (instrumentType.equals(TransactionType.AMEND))
		onlineHelpFileName = "customer/amend_import_dlc.htm";
%>


<%-- ********************* HTML for page begins here ********************* --%>

<%
  // Some of the retrieval logic above may have set a focus field.  Otherwise,
  // we'll use the initial value for focus.
  String onLoad = "";

	//The navigation bar is only shown when editing templates.  For transactions
	 // it is not shown ti minimize the chance of leaving the page without properly
	 // unlocking the transaction.
	 String showNavBar = TradePortalConstants.INDICATOR_NO;
	 if (isTemplate)
	 {
	    showNavBar = TradePortalConstants.INDICATOR_YES;
	 }
  // Auto save the form when time-out if not readonly.
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
%>



<%-- Body tag included as part of common header --%>
<%
  String pageTitleKey;
  String item1Key;
  if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) && !(TransactionType.AMEND).equals(transaction.getAttribute("transaction_type_code"))) {
	    pageTitleKey = "SecondaryNavigation.NewInstruments";
	    userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
	  } else {
	    pageTitleKey = "SecondaryNavigation.Instruments";
	    userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
	  }
  String helpUrl = "customer/amend_import_dlc.htm";
  item1Key = resMgr.getText("SecondaryNavigation.Instruments.ImportLC",TradePortalConstants.TEXT_BUNDLE) + ": "+resMgr.getText("common.amendButton",TradePortalConstants.TEXT_BUNDLE);

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<div class="pageMain">
<div class="pageContent">
<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());
   // KMehta Rel 9400 IR T36000042634 on 22 Sep 2015
   /* KMehta Rel 9500 IR T36000034805 on 16 Mar 2016 Commented Below Code as per updated email from Ann C
    	The setting - �Allow more characters than SWIFT recommended field size limitations for Goods Description,
    	Documents Required and Additional Conditions fields� applies to Issue transactions only. */
   /* String swiftLengthInd = "Y"; 
   swiftLengthInd = CBCResult.getAttribute("/ResultSetRecord(0)/OVERRIDE_SWIFT_LENGTH_IND");	   
   if(TradePortalConstants.INDICATOR_NO.equals(swiftLengthInd)){
	   textAreaMaxlen = "1000";
	   textAreaMaxlength2 = "100";
   } */	   
   // KMehta Rel 9400 IR T36000042634 on 22 Sep 2015
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__IMP_DLC_AMD);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
    //MEer Rel 9.3.5  CR-1027  
    corpOrgOid = instrument.getAttribute("corp_org_oid");
    CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
    corporateOrg.getById(corpOrgOid);	
    String bankOrgGroupOid = corporateOrg.getAttribute("bank_org_group_oid");
    
    BankOrganizationGroupWebBean bankOrganizationGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class, "BankOrganizationGroup");
     bankOrganizationGroup.getById(bankOrgGroupOid);
  
     String selectedPDFType =  bankOrganizationGroup.getAttribute("imp_dlc_pdf_type");
     
     if(TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(selectedPDFType)  || TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(selectedPDFType)){
  			canDisplayPDFLinks = true;
     }
%>

<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="<%=item1Key%>" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />


<form id="TransactionIMP_DLC" name="TransactionIMP_DLC"
	method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name=buttonName>
 <%-- error section goes above form content --%>
    <div class="formArea">
    <jsp:include page="/common/ErrorSection.jsp" />
 
       <div class="formContent"> <%-- Form Content Area starts here --%>

<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 

<%
  if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK))
	  isProcessedByBank = true;

  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

  secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code", instrumentType);
  secureParms.put("instrument_status", instrumentStatus);

  secureParms.put("transaction_oid", transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status", transactionStatus);

  secureParms.put("shipment_oid", terms.getFirstShipment().getAttribute("shipment_oid"));
  secureParms.put("shipmentOid", terms.getFirstShipment().getAttribute("shipment_oid"));

  secureParms.put("transaction_instrument_info",
  	transaction.getAttribute("transaction_oid") + "/" +
	instrument.getAttribute("instrument_oid") + "/" +
	transactionType);

  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");
  if (termsOid == null) termsOid = "0";
  secureParms.put("terms_oid", termsOid);
  secureParms.put("TransactionCurrency", currencyCode);
  //IR - PAUK042248441 [BEGIN]
  //secureParms.put("ApplRefNum", terms.getAttribute("reference_number"));
  secureParms.put("ApplRefNum", StringFunction.xssHtmlToChars(terms.getAttribute("reference_number")));
  //IR - PAUK042248441 [END]		 

%>

 

<% // [BEGIN] IR-YVUH032343792 - jkok %>
  <%@ include file="fragments/Instruments-AttachDocuments.frag" %>
<% // [END] IR-YVUH032343792 - jkok %>

 <%	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
    || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
	  {
	%>
		<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
	     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
	     </div>
	<%
	  }
	%>	
	
	<% //CR 821 Added Repair Reason Section  
	  StringBuffer repairReasonWhereClause = new StringBuffer();
	  int  repairReasonCount = 0;
		
		/*Get all repair reason's count from transaction history table*/
		repairReasonWhereClause.append("p_transaction_oid = ?");
		repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
		//jgadela  R90 IR T36000026319 - SQL FIX
		Object[] sqlParamsCnt = new Object[1];
		sqlParamsCnt[0] =  transaction.getAttribute("transaction_oid");
		Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
		repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsCnt);
				
	if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
			<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
	<%} %> 


			     
	<%= widgetFactory.createSectionHeader("1", "ImportDLCAmend.General") %>
         <%@ include file="fragments/Transaction-IMP_DLC-AMD-General.frag"%> 			
	</div> <%-- General Tab Ends Here--%>
	
	<%= widgetFactory.createSectionHeader("2", "ImportDLCAmend.Shipment") %>
		<%@ include file="fragments/Transaction-IMP_DLC-AMD-Shipment.frag"%>
	 </div> <%-- Shipment Tab Ends Here --%>
	
	<%= widgetFactory.createSectionHeader("3", "ImportDLCAmend.OtherConditions") %>
		<%@ include  file="fragments/Transaction-IMP_DLC-AMD-OtherConditions.frag"%>
	</div> <%-- Other Conditions Tab Ends Here --%>
	
	<%= widgetFactory.createSectionHeader("4", "ImportDLCAmend.ClientBankInstructions") %>
		<%@ include file="fragments/Transaction-IMP_DLC-AMD-ClientBankInstructions.frag"%>
	</div> <%-- Bank Instructions Tab Ends Here --%>
	
	
 <% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("5", "TransactionHistory.RepairReason", null, true) %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div><%-- Repair Reason Section Ends Here --%>
<%} %> 
	
		</div> <%-- Form Content End Here --%>
		</div><%--formArea--%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar"
	data-dojo-props="form: 'TransactionIMP_DLC'">

	<jsp:include page="/common/Sidebar.jsp">
		<jsp:param name="links" value="<%=links%>" />
		<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
		<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
		<jsp:param name="error" value="<%= error%>" />
		<jsp:param name="formName" value="0" />
		<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
		 <jsp:param name="showLinks" value="true" />  
		 <jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
		 <jsp:param name="showApplnForm" value="<%=canDisplayPDFLinks%>"/>
	</jsp:include>
</div>


<%@ include file="fragments/PhraseLookupPrep.frag"%>

<%= formMgr.getFormInstanceAsInputField("Transaction-IMP_DLCForm", secureParms) %>

</form>
</div>
</div>
<div id="AddStructurePODialog"></div>
<div id="RemoveStructurePODialog"></div>
<div id="ViewStructurePODialog"></div>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp"/>

<script type="text/javascript">

  <%-- cquinton 3/3/2013 add local var --%>
<%-- Srinivasu_D IR#T36000018060 Rel8.2 06/19/2013 - ViewPOStructure() moved before the isReadOnly condition below. --%>
  var local = {};
function ViewPOStructure(){	
		  			 require(["t360/dialog"], function(dialog) {
		  				
		  				 destroyPurchaseOrderSearchId();
		  					
		  				 var dialogName = '<%=resMgr.getText("PurchaseOrder.AssignedStructuredPO", TradePortalConstants.TEXT_BUNDLE)%>';	 
		  				 dialog.open('ViewStructurePODialog', dialogName, 'ViewStructuredPO.jsp', ['shipmentOid'],['<%=terms.getFirstShipment().getAttribute("shipment_oid")%>'], <%-- parameters --%>
		  			                'select', null);
		  			  });
		  			}
		  			function destroyPurchaseOrderSearchId(){
		  				
		  				require(["dijit/registry"], function(registry) {
		  					var PONum = registry.byId("PONum");
		  					if(PONum) PONum.destroy();
		  					
		  					var BeneName = registry.byId("BeneName");
		  					if(BeneName) BeneName.destroy();
		  					
		  					var AmountFrom = registry.byId("AmountFrom");
		  					if(AmountFrom) AmountFrom.destroy();
		  					
		  					var AmountTo = registry.byId("AmountTo");
		  					if(AmountTo) AmountTo.destroy();
		  					
		  					var Currency = registry.byId("Currency");
		  					if(Currency) Currency.destroy();
		  					
		  					var Search = registry.byId("PurchaseOrderSearch");
		  					if(Search) Search.destroy();
		  					
		  					var Close = registry.byId("AddStructurePODialogCloseButton");
		  					if(Close) Close.destroy();
		  				});		
		  			
		  			}
<% 
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dijit/registry", "dojo/ready", "dojo/dom"],
    function(registry, ready, dom ) {
      ready(function() {
    	<%-- KMehta Rel 9400 IR T36000042634 on 23 Sep 2015 Add-Begin  --%>
    	<%-- /* KMehta Rel 9500 IR T36000034805 on 16 Mar 2016 Commented Below Code as per updated email from Ann C
    	The setting - �Allow more characters than SWIFT recommended field size limitations for Goods Description,
    	Documents Required and Additional Conditions fields� applies to Issue transactions only.  */ --%>
    	   <%-- if(TradePortalConstants.INDICATOR_NO.equals(swiftLengthInd)){ %>
	    	  var textAreaMaxLength = 1000;
	    	  var textAreaMaxLength2 = 100;
	    	  var addtnlCond = '<%=terms.getAttribute("additional_conditions")%>';
	    	  addtnlCond = amend_det.substring(0,textAreaMaxLength);
	    	  dom.byId("AddlConditionsText").value = addtnlCond; --%>
	    	  /* Below field not applicable for swiftLengthInd validation */
	    	  <%-- var spclBankInst = '<%=terms.getAttribute("special_bank_instructions")%>';
	    	  spclBankInst = spclBankInst.substring(0,textAreaMaxLength);
	    	  dom.byId("SpclBankInstructions").value = spclBankInst; --%>
	    	  <%-- var goodsDescText = '<%=terms.getAttribute("goods_description")%>';
	    	  goodsDescText = goodsDescText.substring(0,textAreaMaxLength);
	    	  dom.byId("GoodsDescText").value = goodsDescText;
	    	  --%>
    	<%-- KMehta Rel 9400 IR T36000042634 on 23 Sep 2015 Add-Begin  --%>
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
            <%-- var focusField = registry.byId(focusFieldId); --%>
            <%-- focusField.focus(); --%>
          }
        <%-- registry.byId("TransactionAmount2").attr('disabled', 'true'); --%>
      });
      
  });
  var xhReq;
  function setNewAmount(){
	 <%--  SureshL IR-T36000034257 04/10/2015 --%>
	   require(["dijit/registry"],
			          function(registry) {
		   
		  			var existingLCAmount = <%=originalAmount%>;
	  				var ccyCode = '<%=currencyCode%>';
	  				var flag;
	  				
	  				var obj=document.getElementById("formItem1");
	  				
	  				   if(registry.byId("Increase").checked){
	  					   flag='Increase';
	  	                   obj.style.display='block';
	  				   }else if(registry.byId("Decrease").checked){
	  					   flag='Decrease';
	  					   obj.style.display='block';
	  				   }else{
	  			           flag='NoAmount';
	  			           obj.style.display='none';
	  					   registry.byId('TransactionAmount').setAttribute('value', '');
	  				   }
	  				var incDecValue = registry.byId("TransactionAmount").value;
	  				var newLCAmount = 0;	  				
	  				
		  				if(flag == 'Increase'){
		  					newLCAmount = Number(existingLCAmount) + Number(incDecValue);
		  				}else if(flag == 'Decrease'){
		  					newLCAmount = Number(existingLCAmount) - Number(incDecValue);
		  				}	  				
		  				if(isNaN(newLCAmount)){
		  					newLCAmount = existingLCAmount;
		  				}
	  				if (!xhReq)
	  					xhReq = getXmlHttpRequest();
	  				var req = "Amount=" + newLCAmount + "&currencyCode="+ccyCode;
	  				xhReq.open("GET", '/portal/transactions/AmountFormatter.jsp?'+req, true);
	  				xhReq.onreadystatechange = updateAmount;
	  				xhReq.send(null);
	  			  });}

  

  function getXmlHttpRequest()
  {
    var xmlHttpObj;
    if (window.XMLHttpRequest) {
      xmlHttpObj = new XMLHttpRequest();
    }
    else {
      try {
        xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
          xmlHttpObj = false;
        }
      }
    }
    console.log(xmlHttpObj);
    return xmlHttpObj;
  }
  		<%--  KMehta Rel 8400 IR-T36000016434 on 18/03/2013 Start --%>
	  			function updateAmount(){
	  				require(["dijit/registry","dojo/dom","dojo/domReady!"], function(registry,dom){ <%--  KMehta - do not merge with two comma  --%>
	  				 if (xhReq.readyState == 4 && xhReq.status == 200) {	 
	  					var ccyCode = '<%=currencyCode%>';
	  					 var formattedAmt = ccyCode + " " + xhReq.responseText;
	  					dom.byId("TransactionAmount2").innerHTML = formattedAmt;
	  					<%-- Commented this line for IR T36000016434 on 18/03/2013  --%>
	  			    	<%-- registry.byId("TransactionAmount2").set('value',xhReq.responseText); --%>
	  			      } 
	  				});
	  			  }
	 	<%--  KMehta Rel 8400 IR-T36000016434 on 18/03/2013 End --%>
	  		<%-- IR T36000017820 -modified to pass currency/BeneName and uploadDefOid to the AddStructuredPO.jsp --%>
	  			function AddPOStructure(){
		  			  require(["dojo/dom","t360/dialog"], function(dom,dialog) {
		  				destroyPurchaseOrderSearchId();
		  			    
		  				var dialogName = '<%=resMgr.getText("PurchaseOrder.SearchStructuredPO", TradePortalConstants.TEXT_BUNDLE)%>';	 
		  				var parmNames = [ "ins_type", "poLineItemCurrency", "poLineItemBeneficiaryName", "poLineItemUploadDefinitionOid" ];
		  			<%-- PMitnala Rel 8.3 IR#T36000021934 - Passing poLineItemBeneficiaryName to the method in StringFucntion, to replace special characters --%>
		  			    var parmVals = [ "IMPORTLC", '<%= currencyCode%>', '<%= StringFunction.escapeQuotesforJS(poLineItemBeneficiaryName)%>', '<%= poLineItemUploadDefinitionOid%>' ];
		  					
		  					 dialog.open('AddStructurePODialog', dialogName, 'AddStructuredPO.jsp',parmNames,parmVals, <%-- parameters --%>
		  			                 'select', null);
		  			  });
		  			}

		  			

		  			function RemovePOStructure(){
		  			  require(["t360/dialog"], function(dialog) {
		  					
		  				destroyPurchaseOrderSearchId();
		  				  
		  				var dialogName = '<%=resMgr.getText("PurchaseOrder.AssignedStructuredPO", TradePortalConstants.TEXT_BUNDLE)%>';	 
		  				dialog.open('RemoveStructurePODialog', dialogName, 'RemoveStructuredPO.jsp', ['shipmentOid','ins_type'],['<%=terms.getFirstShipment().getAttribute("shipment_oid")%>','IMPORTLC'], <%-- parameters --%>
		  			                 'select', null);
		  			 });
		  			}

					
				
	<%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });		

<%
  }
%>
</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
