<%--
**********************************************************************************
// DK IR PBUL103152840/PIUL103154077 Rel7.1
**********************************************************************************
--%> 

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, java.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,com.ams.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"                   scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"                   scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"              scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<% 
String          userSecurityRights   = userSession.getSecurityRights();
String          userOid              = userSession.getUserOid();
String          bankGroupOid         = request.getParameter("BankGroupOID");


if (InstrumentServices.isBlank(userOid)) {
%>
	<jsp:forward page='<%= NavigationManager.getNavMan().getPhysicalPage("PageNotAvailable", request) %>'></jsp:forward>
<%
}
%>

<jsp:include page="fragments/Transaction-ISS-GetFXOnlineAvailable.jsp">
    <jsp:param name="BankGroupOID" value="<%=bankGroupOid%>" />
</jsp:include>