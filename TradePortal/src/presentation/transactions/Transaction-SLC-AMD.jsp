<%--
*******************************************************************************
                              Standby LC Amend Page

  Description:
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
	
	<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
	                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
	                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
	                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,com.amsinc.ecsg.util.DateTimeUtility" %>
	
	<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	    scope="session">
	</jsp:useBean>
	
	<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
	</jsp:useBean>
	
	<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
	</jsp:useBean>
	
	<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
	   scope="session">  
	</jsp:useBean>
	
	
	<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);	
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String links="";
  //links="GuaranteeAmend.General,GuaranteeAmend.Instructions";
	
  String focusField = "TransactionAmount";   // Default focus field
	
  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String rejectionIndicator = "";
  String rejectionReasonText = "";
  
  boolean isProcessedByBank = false;
	
  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database
	
  DocumentHandler doc;
	
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

	  // Applicant name and expiry date from the issue transaction.  Displayed in General section
	  String applicantName = null;
	  String currentExpiryDate = null;
	
	  // Variables used for populating ref data dropdowns.
	  String options;
	  String defaultText;
	
	  // These are the beans used on the page.
	
	  TransactionWebBean transaction  = (TransactionWebBean)beanMgr.getBean("Transaction");
	  InstrumentWebBean instrument    = (InstrumentWebBean)beanMgr.getBean("Instrument");
	  TemplateWebBean template        = (TemplateWebBean)beanMgr.getBean("Template");
	  TermsWebBean terms              = null;
	
	    //PR CMA Issue No 5 # IR T36000010961
	    StringBuffer         availableAmount          = new StringBuffer();
	  // We need the amount of the active transaction to display.  Create a webbean
	  // to hold this data.
	  TransactionWebBean activeTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");
	
	  // Get the document from the cache.  We'll may use it to repopulate the 
	  // screen if returning from another page, a save, validation, or any other
	  // mediator called from this page.  Otherwise, we assume an instrument oid
	  // and transaction oid was passed in.
	
	  doc = formMgr.getFromDocCache();
	
	  Vector error = null;
	  error = doc.getFragments("/Error/errorlist/error");
	  
	  String buttonClicked = request.getParameter("buttonName");
	  //KMehta Rel 9400 IR T36000042634 on 23 Sep 2015 Add-Begin 
	  String textAreaMaxlen = "2147483646";	  
	  //KMehta Rel 9400 IR T36000042634 on 23 Sep 2015 Add-End  
	/******************************************************************************
	  We are either entering the page or returning from somewhere.  These are the 
	  conditions for how to populate the web beans.  Data comes from either the
	  database or the doc cache (/In section) with some variation.
	
	  Mode           Condition                      Populate Beans From
	  -------------  ----------------------------   --------------------------------
	  Enter Page     no /In/Transaction in doc      Instrument and Template web
	                                                beans already populated, get
	                                                data for Terms and TermsParty
	                                                web beans from database
	
	  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
	  Phrase Lookup                                 LookupInfo text (from /Out) to
	                                                replace a specific phrase text
	                                                in the /In document before 
	                                                populating
	
	  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
	  Transaction                                   retrieved from database)
	    mediator
	    (no error)
	
	  return from    /Error/maxerrorseverity > 0    doc cache (/In)
	  Transaction    
	    mediator
	    (error)
	******************************************************************************/
	
	  // Assume we get the data from the doc.
	  getDataFromDoc = true;
	
	  String maxError = doc.getAttribute("/Error/maxerrorseverity");
	  if (maxError != null && maxError.compareTo(
	              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
	     // No errors, so don't get the data from doc.
	     getDataFromDoc = false;
	  }
	  //ir cnuk113043991 - check to see if transaction needs to be refreshed
	  // if so, refresh it and do not get data from doc as it is wrong
	  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
	     transaction.getDataFromAppServer();
	     getDataFromDoc = false;
	  }
	  
	  String newTransaction = null;
	    if (doc.getDocumentNode("/Out/newTransaction")!=null) {
	      newTransaction = doc.getAttribute("/Out/newTransaction");
	      session.setAttribute("newTransaction", newTransaction);
	      System.out.println("found newTransaction = "+ newTransaction);
	    } 
	    else {
	      newTransaction = (String) session.getAttribute("newTransaction");
	      if ( newTransaction==null ) {
	        newTransaction = TradePortalConstants.INDICATOR_NO;
	      }
	      System.out.println("used newTransaction from session = " + newTransaction);
  }
	  if (doc.getDocumentNode("/In/Transaction") == null) {
	     // No /In/Transaction means we've never looked up the data.
	     Debug.debug("No /In/Transaction section - get data from database");
	     getDataFromDoc = false;
	  }
	  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
	     // A Looked up phrase exists.  Replace it in the /In document.
	     Debug.debug("Found a looked-up phrase");
	     getDataFromDoc = true;
	
	     // Take the looked up and appended phrase text from the /Out section
	     // and copy it to the /In section.  This allows the beans to be 
	     // properly populated.
	     String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
	     xmlPath = "/In" + xmlPath;
	
	     doc.setAttribute(xmlPath, 
	                      doc.getAttribute("/Out/PhraseLookupInfo/NewText"));
	
	     // If we returned from the phrase lookup without errors, set the focus
	     // to the correct field.  Otherwise, don't set the focus so the user can
	     // see the error.
	     if (maxError != null && maxError.compareTo(
	              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
	       focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
	     }
	  }
	
	  if (getDataFromDoc) {
	     Debug.debug("Populating beans from doc cache");
	
	     // Populate the beans from the input doc.
	     try {
	        instrument.populateFromXmlDoc(doc.getComponent("/In"));
	        transaction.populateFromXmlDoc(doc.getComponent("/In"));
	        template.populateFromXmlDoc(doc.getComponent("/In"));
	
	        terms = (TermsWebBean) beanMgr.getBean("Terms");
	        terms.populateFromXmlDoc(doc, "/In");
	
	        String activeTransOid = instrument.getAttribute("active_transaction_oid");
	        if (!InstrumentServices.isBlank(activeTransOid)) {
	           activeTransaction.setAttribute("transaction_oid", activeTransOid);
	           activeTransaction.getDataFromAppServer();
	        }
	
	     } catch (Exception e) {
	        out.println("Contact Administrator: "
	              + "Unable to reload data after returning to page. "
	              + "Error is " + e.toString());
	     }
	  } else {
	     Debug.debug("populating beans from database");
	     // We will perform a retrieval from the database.
	     // Instrument and Transaction were already retrieved.  Get
	     // the rest of the data
	
	     terms = transaction.registerCustomerEnteredTerms();
	
	     String activeTransOid = instrument.getAttribute("active_transaction_oid");
	     if (!InstrumentServices.isBlank(activeTransOid)) {
	        activeTransaction.setAttribute("transaction_oid", activeTransOid);
	        activeTransaction.getDataFromAppServer();
	     }

	  }
	
	  // Copy the reference number from the instrument to
	  // this transaction's terms.
	     terms.setAttribute("reference_number", 
		 	instrument.getAttribute("copy_of_ref_num"));	
	     Debug.debug("INSTRUMENT REFERENCE NUMBER: " + instrument.getAttribute("copy_of_ref_num"));
			
	     Debug.debug("REFERENCE NUMBER: " + terms.getAttribute("reference_number"));
	  
	  // Now determine the mode for how the page operates (readonly, etc.)
	  BigInteger requestedSecurityRight = SecurityAccess.SLC_CREATE_MODIFY;
	%>
  	<%@ include file="fragments/Transaction-PageMode.frag" %>

	<%
	  // Now we need to calculate the computed new lc amount total.  This involves 
	  // using the active transaction amount, the amount entered by the user,
	  // performing some validation and then doing the calculation.
	
	  MediatorServices medService = new MediatorServices();
	  medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
	  BigDecimal originalAmount = BigDecimal.ZERO;
	  BigDecimal offsetAmount = BigDecimal.ZERO;
	  BigDecimal calculatedTotal = BigDecimal.ZERO;
	  String amountValue = null;
	  String displayAmount;
	  String incrDecrValue = TradePortalConstants.INCREASE;
	  String currencyCode = activeTransaction.getAttribute("copy_of_currency_code");
	  Debug.debug("##### Amount currency code " + currencyCode);
	  boolean badAmountFound = false;
	
	  try {
	     amountValue = activeTransaction.getAttribute("instrument_amount");
	     originalAmount = new BigDecimal(amountValue);
	  } catch (Exception e) {
	     originalAmount = BigDecimal.ZERO;
	  }
	
	//PR CMA Issue No 5 # IR T36000010961
	  if((currencyCode != null)
		      && (currencyCode.trim().length() > 0)
		      && (activeTransaction.getAttribute("available_amount") != null)
		      && (activeTransaction.getAttribute("available_amount").trim().length() > 0))
		   {
		      availableAmount.append(currencyCode);
		      availableAmount.append(" ");
		      availableAmount.append(TPCurrencyUtility.getDisplayAmount(activeTransaction.getAttribute("available_amount"),
		    		  currencyCode, loginLocale));
		   }
	  
	// DK IR T36000027615 Rel9.1 09/12/2014
	  if (doc.getAttribute("/In/Terms/amount") != null) {
	  Debug.debug("Get Data From Doc");
	
	  	//
	   	// Furthermore, since the displayed terms amount is always displayed as a 
	   	// positive number (the Increase/Decrease dropdown represents the sign).
	   	// Therefore, we may need to set the double amount negative.
	
	
	     amountValue = doc.getAttribute("/In/Terms/amount");
	     incrDecrValue = doc.getAttribute("/In/Terms/IncreaseDecreaseFlag");
	
	     try {
	        amountValue = NumberValidator.getNonInternationalizedValue(amountValue, 
	                                                          loginLocale, false);
	        try {
	           offsetAmount = new BigDecimal(amountValue);
	        } catch (Exception e) {
	           offsetAmount = BigDecimal.ZERO;
	        }
	        if (incrDecrValue.equals(TradePortalConstants.DECREASE)) {
	           offsetAmount = offsetAmount.negate();
	        }
	
	        // Don't format the number because we only came from the doc
	        displayAmount = doc.getAttribute("/In/Terms/amount");
	
	     } catch (Exception e) {
	        // Unable to unformat number so the display value is the save value
	        // the user typed in.
	        displayAmount = amountValue;
	        badAmountFound = true;
	        medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	                                  TradePortalConstants.INVALID_CURRENCY_FORMAT,
	                                  amountValue);
	        medService.addErrorInfo();
	        doc.addComponent("/Error", medService.getErrorDoc());
	        formMgr.storeInDocCache("default.doc", doc);
		}
	  }
	  else 
	  {
	  Debug.debug("Get data from Database");
	     // We're getting the amount from the database so convert to a positive
	     // number if necessary and set the Increase/Decrease dropdown accordingly.
	     try {
	        amountValue = terms.getAttribute("amount");
	        offsetAmount = new BigDecimal(amountValue);
	     } catch (Exception e) {
	        offsetAmount = BigDecimal.ZERO;
	     }
	     if (offsetAmount.doubleValue() < 0) incrDecrValue = TradePortalConstants.DECREASE;
	     if( InstrumentServices.isBlank( amountValue )  && !isReadOnly) {
	   	    displayAmount = "";
	     }
	     else{
	        displayAmount = TPCurrencyUtility.getDisplayAmount(
	                            offsetAmount.abs().toString(), currencyCode, loginLocale);
	     }
	  }
	//Srinivasu_D IR#T36000035537 ER9.1.0.2 01/01/2015 - handling minus
		 if(StringFunction.isNotBlank(amountValue)){
		 char amtChar[] = amountValue.toCharArray();
			if(amtChar[0] == '-'){
				amountValue = amountValue.substring(1,amountValue.length());
			}
		 }
		 //Srinivasu_D IR#T36000035537 ER9.1.0.2 01/01/2015 - End 
	  calculatedTotal = originalAmount.add(offsetAmount);
	 
	 
	  transactionType = transaction.getAttribute("transaction_type_code");
	  transactionStatus = transaction.getAttribute("transaction_status");
	  transactionOid = transaction.getAttribute("transaction_oid");
	
	  // Get the transaction rejection indicator to determine whether to show the rejection reason text
	  rejectionIndicator = transaction.getAttribute("rejection_indicator");
	  rejectionReasonText = transaction.getAttribute("rejection_reason_text");
	  
	  instrumentType = instrument.getAttribute("instrument_type_code");
	  instrumentStatus = instrument.getAttribute("instrument_status");
	
	  Debug.debug("Instrument Type " + instrumentType);
	  Debug.debug("Instrument Status " + instrumentStatus);
	  Debug.debug("Transaction Type " + transactionType);
	  Debug.debug("Transaction Status " + transactionStatus);
	
	  // Create documents for the phrase dropdowns. First check the cache (no need
	  // to recreate if they already exist).  After creating, place in the cache.
	  // (InstrumentCloseNavigator.jsp cleans these up.)
	
	  DocumentHandler phraseLists = formMgr.getFromDocCache("PhraseLists");
	  if (phraseLists == null) phraseLists = new DocumentHandler();
	
	  DocumentHandler addlCondDocList = phraseLists.getFragment("/AddlCond");
	  if (addlCondDocList == null) {
	     addlCondDocList = PhraseUtility.createPhraseList(
	                                    TradePortalConstants.PHRASE_CAT_ADDL_COND, 
	                                    userSession, formMgr, resMgr);
	     phraseLists.addComponent("/AddlCond", addlCondDocList);
	  }
	
	  DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
	  if (spclInstrDocList == null) {
	     spclInstrDocList = PhraseUtility.createPhraseList(
	                                    TradePortalConstants.PHRASE_CAT_SPCL_INST, 
	                                    userSession, formMgr, resMgr);
	     phraseLists.addComponent("/SpclInstr", spclInstrDocList);
	  }
	
	  formMgr.storeInDocCache("PhraseLists", phraseLists);
	

	
	  ////////////////////////////////////////////////////////////////////////
	  // Retrieve the latest information of applicatnt name and expiry date.  
	  // They are displayed in general section.
	  ////////////////////////////////////////////////////////////////////////
	
	  // Retrieve the latest information of expiry date from instrument.
	  currentExpiryDate = instrument.getAttribute("copy_of_expiry_date");
	  // Retrieve the applicant name from the bank release terms of the latest issuance or amendment transaction.
	  //    (The latest transaction is the one that has the latest transaction status date and in case several transaction
	  //     have the same transaction status date, the largest transaction_oid.)
	  String instrument_oid = instrument.getAttribute("instrument_oid");
	  StringBuffer issueTransactionSQL = new StringBuffer();
	  issueTransactionSQL.append("select p.name")
	                     .append(" from terms_party p, terms t, transaction tr")
	                     .append(" where tr.transaction_oid =")
	                     .append("       (select max(transaction_oid)")
	                     .append("          from transaction")
	                     .append("         where transaction_status_date = (select max(transaction_status_date)")
	                     .append("                                            from transaction")
	                     .append("                                           where p_instrument_oid = ?")
	                     .append("                                             and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)")
	                     .append("                                             and transaction_status = ?)")
	                     .append("           and p_instrument_oid = ?")
	                     .append("           and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)")
	                     .append("           and transaction_status = ?)")
	                     .append(" and tr.c_bank_release_terms_oid = t.terms_oid")
	                     .append(" and (p.terms_party_oid = t.c_first_terms_party_oid")
	                     .append("      or p.terms_party_oid = t.c_second_terms_party_oid")
	                     .append("      or p.terms_party_oid = t.c_third_terms_party_oid")
	                     .append("      or p.terms_party_oid = t.c_fourth_terms_party_oid")
	                     .append("      or p.terms_party_oid = t.c_fifth_terms_party_oid)")
	                     .append(" and p.terms_party_type = ?");
	//jgadela  R90 IR T36000026319 - SQL FIX
	  Object[] sqlParamsTran = new Object[11];
	  sqlParamsTran[0] =  instrument_oid;
	  sqlParamsTran[1] =  TransactionType.ISSUE;
	  sqlParamsTran[2] =  TransactionType.AMEND;
	  sqlParamsTran[3] =  TransactionType.CHANGE;
	  sqlParamsTran[4] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
	  sqlParamsTran[5] =  instrument_oid;
	  sqlParamsTran[6] =  TransactionType.ISSUE;
	  sqlParamsTran[7] =  TransactionType.AMEND;
	  sqlParamsTran[8] =  TransactionType.CHANGE;
	  sqlParamsTran[9] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
	  sqlParamsTran[10] =  TradePortalConstants.APPLICANT;
	  DocumentHandler issueTransactionResult = DatabaseQueryBean.getXmlResultSet(issueTransactionSQL.toString(), false, sqlParamsTran);
	  if (issueTransactionResult!=null) {
	     applicantName = issueTransactionResult.getAttribute("/ResultSetRecord(0)/NAME");
	  }
	%>

	<%
	  // Some of the retrieval logic above may have set a focus field.  Otherwise,
	  // we'll use the initial value for focus.
	  String onLoad = "";
	
	  // Auto save the form when time-out if not readonly.  
	  // (Header.jsp will check for auto-save setting of the corporate org).
	  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
	  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
	  
	%>
	
<%
  String pageTitleKey;
  if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) && !(TransactionType.AMEND).equals(transaction.getAttribute("transaction_type_code"))) {
    pageTitleKey = "SecondaryNavigation.NewInstruments";
    userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
  } else {
    pageTitleKey = "SecondaryNavigation.Instruments";
    userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
  }
  String item1Key = resMgr.getText("SecondaryNavigation.Instruments.OutgoingSTandbyLC",TradePortalConstants.TEXT_BUNDLE) + ": "+resMgr.getText("common.amendButton",TradePortalConstants.TEXT_BUNDLE);
  String helpUrl = "customer/amend_standby_lc.htm";

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

	<div class="pageMain">
		<div class="pageContent">
		
			<%-- ********************* HTML for page begins here ********************* --%>
	
	
	
	<% //cr498 begin
	   //Include ReAuthentication frag in case re-authentication is required for
	   //authorization of transactions for this client
	   String certAuthURL = "";
	   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
	   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	   
	   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
	// KMehta Rel 9400 IR T36000042634 on 22 Sep 2015
	   String swiftLengthInd = "Y"; 
	   swiftLengthInd = CBCResult.getAttribute("/ResultSetRecord(0)/OVERRIDE_SWIFT_LENGTH_IND");	   
	   if(TradePortalConstants.INDICATOR_NO.equals(swiftLengthInd)){
		   textAreaMaxlen = "1000"; // IR T36000048753 Rel9.5
	   }	   
	   // KMehta Rel 9400 IR T36000042634 on 22 Sep 2015
	   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
	       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__SLC_AMD);
	   if (requireAuth) {
	%>   
	      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
	<%
	   }
	   //cr498 end
	%>
			<form id="TransactionSLC" name="TransactionSLC" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
				<input type=hidden value="" name=buttonName>

				<% //cr498 begin
				  if (requireAuth) {
				%> 
				
				  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
				  <input type=hidden name="reCertOK">
				  <input type=hidden name="logonResponse">
				  <input type=hidden name="logonCertificate">
				
				<%
				  } //cr498 end
				%> 

				<%
				  if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK))
					  isProcessedByBank = true;
				
				  // Store values such as the userid, security rights, and org in a
				  // secure hashtable for the form.  Also store instrument and transaction
				  // data that must be secured.
				  Hashtable secureParms = new Hashtable();
				  secureParms.put("login_oid", userSession.getUserOid());
				  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
				  secureParms.put("login_rights", loginRights);
				
				  secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
				  secureParms.put("instrument_type_code", instrumentType);
				  secureParms.put("instrument_status", instrumentStatus);
				
				  secureParms.put("transaction_oid", transaction.getAttribute("transaction_oid"));
				  secureParms.put("transaction_type_code", transactionType);
				  secureParms.put("transaction_status", transactionStatus);
				  
				  secureParms.put("transaction_instrument_info", 
				  	transaction.getAttribute("transaction_oid") + "/" + 
					instrument.getAttribute("instrument_oid") + "/" + 
					transactionType);
				
				  // If the terms record doesn't exist, set its oid to 0.
				  String termsOid = terms.getAttribute("terms_oid");
				  if (termsOid == null) termsOid = "0";
				  secureParms.put("terms_oid", termsOid);
				  secureParms.put("TransactionCurrency", currencyCode);
				  //IR - PAUK042248441 [BEGIN]
				  //secureParms.put("ApplRefNum", terms.getAttribute("reference_number"));
				  secureParms.put("ApplRefNum", StringFunction.xssHtmlToChars(terms.getAttribute("reference_number")));
				  //IR - PAUK042248441 [END]
				%>
				
				
				<jsp:include page="/common/PageHeader.jsp">
				   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
				   <jsp:param name="item1Key" value="<%=item1Key %>" />
				   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
				</jsp:include>
				
				<jsp:include page="/common/TransactionSubHeader.jsp" />
	
    
			  	<%//@ include file="fragments/Transaction-SLC-AMD-Links.frag" %>  
			
        <%-- error section goes above form content --%>
        <div class="formArea">
         <div class="formContent">
          <jsp:include page="/common/ErrorSection.jsp" />
		   <%@ include file="fragments/Instruments-AttachDocuments.frag" %>
	
				 
				  <%	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
					    || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
					  {
					%>
						<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
					     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
					     </div>
					<%
					  }
					%>
<% //CR 821 Added Repair Reason Section
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object[] sqlParamsRepCnt1 = new Object[1];
	sqlParamsRepCnt1[0] =  transaction.getAttribute("transaction_oid");

	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsRepCnt1);
		
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 					
				  	<%@ include file="fragments/Transaction-SLC-AMD-html.frag" %>
				  	
				  <% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
						<%=widgetFactory.createSectionHeader("3", "TransactionHistory.RepairReason", null, true) %>
						<%@ include file="fragments/Transaction-RepairReason.frag" %>
						</div><%-- Repair Reason Section Ends Here --%>
				<%} %> 
				  </div><%--formContent--%>
				</div><%--formArea--%>
				
			  	<%//@ include file="fragments/Transaction-Footer.frag" %>

					<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'TransactionSLC'">
							<jsp:include page="/common/Sidebar.jsp">
								<jsp:param name="links" value="<%=links%>" />
								<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
								<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
								<jsp:param name="buttonPressed" value="<%=buttonClicked%>" />
								<jsp:param name="error" value="<%= error%>" />
								<jsp:param name="formName" value="0" />
								<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
								 <jsp:param name="showLinks" value="true" />  
								 <jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
							</jsp:include>
						</div> <%--closes sidebar area--%>	

				<%@ include file="fragments/PhraseLookupPrep.frag" %>

				<%= formMgr.getFormInstanceAsInputField("Transaction-SLCForm", secureParms) %>
			</form>
		</div>
	</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp"/>

<script>

  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dijit/registry", "dojo/ready", "dojo/dom"],
    function(registry, ready, dom ) {
      ready(function() {    	
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
        
        
       <%--  registry.byId("TransactionAmount2").attr('disabled', 'true'); --%>
      });
  });
  var xhReq;
  function setNewAmount(){
	  require(["dijit/registry"],
			    function(registry ) {
		  			var existingLCAmount = <%=originalAmount%>;
	  				var ccyCode = '<%=currencyCode%>';
	  				var flag;
	  				if(registry.byId("Increase").checked){
	  					flag='Increase';
	  				}else{
	  					flag='Decrease';
	  				}
	  				var incDecValue = registry.byId("TransactionAmount").value;
	  				var newLCAmount = 0;	  				
	  				
		  				if(flag == 'Increase'){
		  					newLCAmount = Number(existingLCAmount) + Number(incDecValue);
		  				}else if(flag == 'Decrease'){
		  					newLCAmount = Number(existingLCAmount) - Number(incDecValue);
		  				}		  				
		  				if(isNaN(newLCAmount)){
		  					newLCAmount = existingLCAmount;
		  				}
	  				if (!xhReq)
	  					xhReq = getXmlHttpRequest();
	  				var req = "Amount=" + newLCAmount + "&currencyCode="+ccyCode;
	  				xhReq.open("GET", '/portal/transactions/AmountFormatter.jsp?'+req, true);
	  				xhReq.onreadystatechange = updateAmount;
	  				xhReq.send(null);
	  			  });}

  function getXmlHttpRequest()
  {
    var xmlHttpObj;
    if (window.XMLHttpRequest) {
      xmlHttpObj = new XMLHttpRequest();
    }
    else {
      try {
        xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
          xmlHttpObj = false;
        }
      }
    }
    console.log(xmlHttpObj);
    return xmlHttpObj;
  }
  
  <%--  KMehta Rel 8400 IR-T36000016434 on 18/03/2013 Start --%>
	function updateAmount(){
		require(["dijit/registry","dojo/dom","dojo/domReady!"], function(registry,dom){ <%--  KMehta - do not merge with two comma  --%>
		 if (xhReq.readyState == 4 && xhReq.status == 200) {	 
			var ccyCode = '<%=currencyCode%>';
			 var formattedAmt = ccyCode + " " + xhReq.responseText;
			dom.byId("TransactionAmount2").innerHTML = formattedAmt;
			<%-- Commented this line for IR T36000016434 on 18/03/2013  --%>
	    	<%-- registry.byId("TransactionAmount2").set('value',xhReq.responseText); --%>
	      } 
		});
	  }
<%--  KMehta Rel 8400 IR-T36000016434 on 18/03/2013 End --%>
  
			

<%
  }
%>

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

</script>

</body>
</html>

	<%
	   // Finally, reset the cached document to eliminate carryover of
	   // information to the next visit of this page.
	   formMgr.storeInDocCache("default.doc", new DocumentHandler());
	%>
