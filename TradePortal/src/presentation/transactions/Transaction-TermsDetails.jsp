<%--
*******************************************************************************
                              Transaction Terms Detail Page

  Description: 
    This is the main driver for the Transaction Terms Detail page.  It handles data
  retrieval of terms and terms parties (or retrieval from the input document)
  and creates the html page to display all the data for the transaction.
  This Jsp will call 3 jsp(s) each of which represents a different html section
  of this document.  This Jsp is designed to be static in that no data is 
  submitted for storage.  The primary action here is to display data for read-
  only.  Of Note: the TermsSummary.jsp is also shared by the CurrentTerms.jsp.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   Debug.debug("***START******************** Transaction - Terms Detail **************************START***");
    /*********\
    * GLOBALS *
    \*********/
    WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
    boolean isReadOnly         = true;   //is the page read only
    boolean showSave           = false;  //do we show the save button
    boolean showDelete         = false;  //do we show the delete button
    boolean showTemplateSave   = false;
    boolean showTemplateDelete = false;
    boolean isTemplate	       = false;
    boolean isFromCurrentTerms = false;  //****** should be defaulted to 'false'
    String showViewTerms = null;

    boolean showTermsSummary   = false;
    boolean isSummaryPage = false;
    String date 	       = "";   	 //universal variable to display a date.
    String currencyCode;
    String attribute;			 //generic attribute used to get data from db
    String displayText;			 //after formating the attribute - display the result

    String goToInstrumentNavigator = "goToInstrumentNavigator";
    String goToInstrumentCloseNavigator;

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String termsPartyOid;

  //Variables used to pass parameters in the url - encrypted.
  String encryptVal1;
  String encryptVal2;
  String encryptVal3;
  String urlParm;
  String links = "";
  //Variables used to build the list of Document Images and Listview data for the Charges Section.
  DocumentHandler dbQuerydoc = null;
  StringBuffer query = null;
  Vector listviewVector;

  String loginLocale = userSession.getUserLocale(); 	//need this for the formatting of dates
  String loginRights = userSession.getSecurityRights();

  DocumentHandler myDoc;				//used in the Document / Charges loops
							//when scrolling through a list of data.
							
  String linkArgs[] = {"","","",""};//Vshah CR-452							

  // These are the beans used on the page.

  TransactionWebBean transaction  	  = (TransactionWebBean) beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    	  = (InstrumentWebBean) beanMgr.getBean("Instrument");
  TermsWebBean terms              	  = null;
  TermsWebBean termsForLoan        	  = null; //PUUH062751147

  TermsPartyWebBean termsPartyApplicant   = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyBeneficiary = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsBillCustomer = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  String customerEnteredTerms = transaction.getAttribute("c_CustomerEnteredTerms");
  boolean hasCustEnteredTerms = InstrumentServices.isNotBlank( customerEnteredTerms );

  String bankReleasedTerms	= transaction.getAttribute("c_BankReleasedTerms");
  boolean hasBankReleasedTerms  = InstrumentServices.isNotBlank( bankReleasedTerms );

  transactionOid = transaction.getAttribute("transaction_oid");
  instrumentOid  = instrument.getAttribute("instrument_oid");
  encryptVal1 = EncryptDecrypt.encryptStringUsingTripleDes( transactionOid, userSession.getSecretKey() ); 	//Transaction_Oid
  encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( instrumentOid, userSession.getSecretKey() );	//Instrument_Oid
  encryptVal3 = EncryptDecrypt.encryptStringUsingTripleDes( "true", userSession.getSecretKey() );		//From Transaction-TermsDetails.jsp flag

  urlParm = "&oid=" + encryptVal1 + "&instrumentOid=" + encryptVal2 + "&isFromTransTermsFlag=" + encryptVal3;

  Debug.debug("populating beans from database");
  // We will perform a retrieval from the database.
  // Instrument and Transaction were already retrieved.  Get
  // the rest of the data

  if( hasBankReleasedTerms )
  {
	terms = transaction.registerBankReleasedTerms();
	termsForLoan = terms; //PUUH062751147
  }else{
	Debug.debug("Error: transaction is processed by bank by doesn't have bank released terms");
	Debug.debug("   Forwarding to the standard customer entered terms page.");
      String physicalPage = NavigationManager.getNavMan().getPhysicalPage("InstrumentNavigator", request);

%>	<jsp:forward page='<%=physicalPage%>'>
       	    <jsp:param name="isFromTransTermsFlag" value="<%=encryptVal3%>" />
	    <jsp:param name="oid" value="<%=encryptVal1%>" />
	    <jsp:param name="instrumentOid" value="<%=encryptVal2%>" />
    	</jsp:forward>
<% }
  
  // Now load the termsPartyApplicant and termsPartyBeneficiary 
  // webbeans.
%>
  <%@ include file="fragments/LoadTermsDetailsParties.frag" %>

<%
  transactionType   = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");

  instrumentType    = instrument.getAttribute("instrument_type_code");
  instrumentStatus  = instrument.getAttribute("instrument_status");
   boolean isARMInstrument = false;
   boolean isRFInstrument = false; //IR - RIUJ021980931 Vshah
  //Pramey - 11/22/2008 CR-434 ARM Add Begin
  	if (instrumentType.equals(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT))
       isARMInstrument = true;    
	//Pramey - 11/22/2008 CR-434 ARM Add End 
	//08/05/2015 Rel 94 CR 932 start
   boolean isBILInstrument = false;
   if (instrumentType.equals(TradePortalConstants.BILLING))
     isBILInstrument = true;
   // 08/05/2015 Rel 94 CR 932 end

  //IR - RIUJ021980931 Vshah
    if (instrumentType.equals(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT)) 
       isRFInstrument = true;    

  Debug.debug("Instrument Type "    + instrumentType);
  Debug.debug("Instrument Status "  + instrumentStatus);
  Debug.debug("Transaction Type "   + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);


  // The Terms Summary section of this page only displays under certain
  // conditions.  1.  The instrument type must be IMP_DLC, EXP_DLC, SLC,
  // GUAR.  2.  The transaction is the original transaction OR the 
  // transaction type is Amend or Advise.

  showTermsSummary = false;

  if (InstrumentServices.isNotBlank(instrumentType)) {
    if ((instrumentType.equals(InstrumentType.IMPORT_DLC)) ||
	(instrumentType.equals(InstrumentType.EXPORT_DLC)) ||
	(instrumentType.equals(InstrumentType.LOAN_RQST)) ||
	(instrumentType.equals(InstrumentType.REQUEST_ADVISE)) ||
	(instrumentType.equals(InstrumentType.INCOMING_SLC)) ||
	(instrumentType.equals(InstrumentType.INCOMING_GUA)) ||
	(instrumentType.equals(InstrumentType.STANDBY_LC)) ||
	(instrumentType.equals(InstrumentType.GUARANTEE)) ||
	(isARMInstrument)||
        (isRFInstrument) || //IR - RIUJ021980931 Vshah
	(instrumentType.equals(InstrumentType.FUNDS_XFER)) || // pcutrone CR-375-D FTR 09/28/2007
	(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) || //rkrishna CR 375-D ATP 08/22/2007
	(instrumentType.equals(InstrumentType.DOMESTIC_PMT)) || // Peter Ng IR INUJ012743663
	(instrumentType.equals(InstrumentType.XFER_BET_ACCTS)) || // 08/10/2015 R 94 CR 932
	(instrumentType.equals(TradePortalConstants.BILLING))
    		) { // Peter Ng PSUJ011957828 2/27/2009
      if (InstrumentServices.isNotBlank(transactionType)) {
         if ((transactionType.equals(TransactionType.AMEND)) ||
             (transactionType.equals(TransactionType.ADVISE)) ||
             (transactionType.equals(TradePortalConstants.NEWBILL)) || // 08/10/2015 R 94 CR 932
             (transactionType.equals(TradePortalConstants.CHANGEBILL)) 
        		 ) {
           showTermsSummary = true;
         }
         else
         {
           String origTrans = instrument.getAttribute("original_transaction_oid");
           if (transactionOid.equals(origTrans)) {
             showTermsSummary = true;
           }
        }
      }
    }
  }

  boolean isFundsXfer = false;
  boolean isLoanRqst = false;
 
  
  // Certain fields display based on instrument types of funds transfer or loan request.
  if (instrumentType.equals(InstrumentType.FUNDS_XFER))
       isFundsXfer = true;
  if (instrumentType.equals(InstrumentType.LOAN_RQST)) 
       isLoanRqst = true;
       
  //Vshah CR-452 Add Begin
  boolean showPDFLinks = false;
  if ( (instrumentType.equals(InstrumentType.EXPORT_COL)) &&
       (transactionType.equals(TransactionType.ISSUE) || transactionType.equals(TransactionType.AMEND)) &&
       (instrument.getAttribute("suppress_doc_link_ind").equals(TradePortalConstants.INDICATOR_YES)) &&
       (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK)) &&
       (hasCustEnteredTerms) )
		showPDFLinks = true;
  
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  String onLoad = "";
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;


 String certAuthURL = "";
 if((isFromCurrentTerms == false) && hasCustEnteredTerms)
 {
	 showViewTerms = "true";
 }
  //Vshah CR-452 Add End     

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
  int sectionHeaderNum =0;
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>
	
<div class="pageMain">
<div class="pageContent">

<jsp:include page="/common/TransactionSubHeader.jsp" />

<form id="TransactionTermsDetail" name="TransactionTermsDetail" method="post" data-dojo-type="dijit.form.Form" >



<div class="formArea">
<div class="formContent">
	

	<%=widgetFactory.createSectionHeader(++sectionHeaderNum + "", "TransactionTerms.TransactionSummary", null, true) %>
         <%@ include file="fragments/Transaction-TermsDetails-TransactionSummary.frag" %>        
	 </div>
	 
	 <%=widgetFactory.createSectionHeader(++sectionHeaderNum + "", "TransactionTerms.Documents", null, true) %>
         <%@ include file="fragments/Transaction-TermsDetails-Documents.frag" %>        
	 </div>
	 <% if(!isARMInstrument){ %>
	 
		 <%=widgetFactory.createSectionHeader(++sectionHeaderNum + "", "TransactionTerms.CommAndChgs", null, true) %>
	         <%@ include file="fragments/Transaction-TermsDetails-Charges.frag" %>        
		 </div>
		 
		 <% if (showTermsSummary) { %>
		 	<%=widgetFactory.createSectionHeader(++sectionHeaderNum + "", "TransactionTerms.TermsSummary", null, true) %>
	         <%@ include file="fragments/Transaction-Terms-Summary.frag" %>        
		 	</div>
		 <% } 
	 }else {%>
		 	<%=widgetFactory.createSectionHeader(++sectionHeaderNum + "", "TransactionTerms.TermsSummaryNew", null, true) %>
	         <%@ include file="fragments/Transaction-Terms-SummaryARM.frag" %>        
		 	</div>
    <%} %>
    
         <%//jgadela 10/30/2013 Rel 8.3 IR T36000022473  CR 821 - [Start] - Removed  the below Transaction Log section - section 4%>
 </div><%--formContent--%>
</div><%--formArea--%>
<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'TransactionTermsDetail'">
	<jsp:include page="/common/Sidebar.jsp">
		<jsp:param value="<%=links %>" name="links"/>
        <jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
		<jsp:param name="showViewTerms" value="<%=showViewTerms %>"/>
		<jsp:param name="showApplnForm" value="false"/>
		<jsp:param name="showBillOfExchangeForm" value="false"/>
		<jsp:param name="showLinks" value="true" />
    </jsp:include>
</div> <%--closes sidebar area--%>
 
  

</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp"/>

</body>
</html>
<%
  /**********
   * CLEAN UP
   **********/ 

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   //doc.removeAllChildren("/DocRoot");
   //formMgr.storeInDocCache("default.doc", doc);
   Debug.debug("***END******************** Transaction - Terms Detail **************************END***");
%>