<%--
*******************************************************************************
                         Transaction Cleanup Page

  Description:
    Handles cleanup after working with a transaction.  The following actions
  are performed:

  1.  Remove all cached phrase lists that have been created.
  2.  Unlock the instrument.  (Although templates are not locked, we'll
      do an unlock anyway.  No harm done.)
  3.  Unregister the beans we created in the Instrument Navigator page.
  
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>



<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.crystaldecisions.sdk.framework.IEnterpriseSession" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%
if (userSession.hasInstrumentLock()) 
{
  // The transaction page has been closed.  Clean up all cached phrase lists.
  formMgr.removeFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);

  // Unlock the instrument.  (Templates are not locked but we attempt to
  // unlock anyway -- no harm done.
  
  String oid = userSession.getUserOid();
  long userOid = 0;
  try {
     userOid = Long.valueOf(oid).longValue();

     Debug.debug("Attempting to unlock instrument for user " + userOid);
     LockingManager.unlockBusinessObjectByUser(userOid, false);
     userSession.setInstrumentLock(false);
  } catch (NumberFormatException e) {
     // unable to convert the instrument oid into a number.
     // skip over the unlock step.
     System.out.println("Error converting instrument " + oid + " into a number.");
  } catch (InstrumentLockException e) {
     System.out.println("Error unlocking instrument for user " + userOid);
     System.out.println("  The error is: " + e.toString());
  } catch (AmsException e) {
     System.out.println("Error unlocking instrument for user " + userOid);
     System.out.println("  The error is: " + e.toString());
  }

  // Unregister the beans we used for this transaction/template. Not all pages coming to 
  // this jsp will have a Template web bean registered, so check for its existence first.
  Debug.debug("Unregistering the webbeans");
  beanMgr.unregisterBean("Transaction");
  beanMgr.unregisterBean("Instrument");
  beanMgr.unregisterBean("Terms");

  if ((TemplateWebBean) beanMgr.getBean("Template") != null)
  {
     beanMgr.unregisterBean("Template");
  }
  HttpSession theSession    = request.getSession(false);
  theSession.setAttribute("inNewTransactionArea", TradePortalConstants.INDICATOR_NO);

  //clear the newTransaction flag in the session
  theSession.removeAttribute("newTransaction");

  theSession.removeAttribute(TradePortalConstants.READONLY_PART);

  session.removeAttribute("DomesticPaymentListView.xml"); 
}  
String fromPage = (String)session.getAttribute("fromPage");
String startHomePage = (String)session.getAttribute("startHomePage");
String currPage = formMgr.getCurrPage();
Debug.debug("from Page -- > "+ fromPage);
Debug.debug("curr Page -- > "+ currPage);
if ("ReportsHome".equals(fromPage) && (!"ReportsHome".equals(currPage) && !"InstrumentNavigator".equals(currPage))){
	session.removeAttribute("fromPage");
}

// Nar IR-T36000031433 Rel 9.2.0.0 02/09/2015 Begin
if ( !( "ReportsHome".equals(fromPage) && "ReportsHome".equals(startHomePage) ) ) {
	 boolean isThereAReportingSession = userSession.getReportLoginInd();
	 if(isThereAReportingSession) {		   
	      try {
	         IEnterpriseSession enterpriseSession =(IEnterpriseSession) session.getAttribute("CE_ENTERPRISESESSION");
	         session.removeAttribute("CE_ENTERPRISESESSION");	                
	         if(enterpriseSession != null) {
	            enterpriseSession.logoff();
	            enterpriseSession = null;
	            Debug.debug("Successfully closed BO XI Session - Logout");
	         }
	      }
	      catch(Exception e) {
	    	  Debug.debug("Exception removing BO XI session - logout");
	          e.printStackTrace();	         
	      }
	      finally {
	    	  userSession.setReportLoginInd(false); 
	      }
	   }
}
// Nar IR-T36000031433 Rel 9.2.0.0 02/09/2015 End



%>