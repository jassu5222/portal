<%--
*******************************************************************************
                            Add PO Line Items Page

  Description:  The Add PO Line Items page is used to present the user with a 
                list of PO line items from which he/she can select and add to 
                the transaction that is currently being edited. This page can 
                only be accessed in edit mode and only by organizations that 
                support purchase order processing and by users having the 
                necessary security right to do the same. Currently, this page 
                is accessible only to Import LC - Issue and Import LC - Amend
                transaction types.

                This page contains a listview of PO line items, each having 
                its own checkbox. The actual PO line items that appear in this 
                listview depend on whether or not the transaction currently 
                being edited has PO line items assigned to it. If PO line 
                items *are* currently assigned to it, the po upload definition
                oid, beneficiary name, and currency for the assigned PO line 
                items are passed into the page. The listview will then display
                only those unassigned PO line items that match all three of the 
                fields that were passed in. If the transaction currently 
                doesn't have any PO line items assigned to it, the listview 
                will display all unassigned PO line items belonging to the 
                user's organization.  

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,
                 com.ams.tradeportal.html.*, com.ams.util.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
	DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	String gridLayout = null,
		   gridHtml = null,
		   dataViewJava	= null;


   TransactionWebBean              transaction         = null;
   DocumentHandler                 xmlDoc              = null;
   StringBuffer                    dynamicWhereClause  = null;
   Hashtable                       secureParms         = null;
   boolean                         hasPOLineItems      = false;
   String                          uploadDefinitionOid = null;
   String                          userSecurityType    = null;
   String                          beneficiaryName     = null;
   String                          transactionOid      = null;
   String                          currency            = null;
   String                          link                = null;
   String                          shipmentOid         = null;
   String                          sourceType          = null;
   int                             numberOfRows        = 0;
   String financeType = null;
   String buyerBacked = null;

    String poNumber = "";
   String lineItemNumber = "";
   String beneName = "";
  	String poCurrency = request.getParameter("Currency") != null ? StringFunction.xssCharsToHtml(request.getParameter("Currency").toUpperCase()) : "";
	String loginLocale = userSession.getUserLocale();
	String amountFrom = request.getParameter("AmountFrom") != null ? StringFunction.xssCharsToHtml(request.getParameter("AmountFrom")) : "";	
	String amountTo = request.getParameter("AmountTo") != null ? StringFunction.xssCharsToHtml(request.getParameter("AmountTo")) : "";	
	String dueDateFrom = request.getParameter("dueDateFrom") != null ? StringFunction.xssCharsToHtml(request.getParameter("dueDateFrom")) : "";
	String dueDateTo = request.getParameter("dueDateTo") != null ? StringFunction.xssCharsToHtml(request.getParameter("dueDateTo")) : "";
	String datePattern = (userSession.getDatePattern()).toLowerCase();
	datePattern = "placeHolder:'" + datePattern + "'"; 
	String buttonPressed = null;
   // Get the user's security type and organization oid
   userSecurityType = userSession.getSecurityType();

   // Get the current transaction web bean so that we can pass its oid to the Add PO Line Items mediator
   transaction = (TransactionWebBean) beanMgr.getBean("Transaction");

   transactionOid = transaction.getAttribute("transaction_oid");

   xmlDoc = formMgr.getFromDocCache();
	
   // If there are PO line items currently assigned to the transaction, then the PO upload definition
   // oid, beneficiary name, and currency will be passed into this page in the XML; otherwise,
   // check to see if they were passed back into the page from the Add PO Line Items mediator.
   if (!StringFunction.isBlank(xmlDoc.getAttribute("/In/SearchForINV/uploadDefinitionOid")))
   {
      uploadDefinitionOid = xmlDoc.getAttribute("/In/SearchForINV/uploadDefinitionOid");
      beneficiaryName     = xmlDoc.getAttribute("/In/SearchForINV/beneficiaryName");
      currency            = xmlDoc.getAttribute("/In/SearchForINV/currency");
      shipmentOid         = xmlDoc.getAttribute("/In/SearchForINV/shipment_oid");
      amountFrom		  = xmlDoc.getAttribute("/In/SearchForINV/amountFrom");
      amountTo		  = xmlDoc.getAttribute("/In/SearchForINV/amountTo");
      dueDateFrom		  = xmlDoc.getAttribute("/In/SearchForINV/dueDateFrom");
      dueDateTo		  = xmlDoc.getAttribute("/In/SearchForINV/dueDateTo");

      if(!ConvenienceServices.isBlank(uploadDefinitionOid))
           hasPOLineItems      = true;
   }
   else
   {
      // If we're coming back to this page as a result of the transaction previously having
      // PO line items, get the PO upload definition oid, beneficiary name, and currency from 
      // the /In section of the xml doc; otherwise, try to get it from the /Out section (in 
      // the case where the transaction did *not* previously have PO line items assigned to it).
      uploadDefinitionOid = xmlDoc.getAttribute("/In/Transaction/uploadDefinitionOid");
      shipmentOid = xmlDoc.getAttribute("/In/Terms/ShipmentTermsList/shipment_oid");

      if (xmlDoc.getAttribute("/Out/Transaction") != null)
      {
         uploadDefinitionOid = xmlDoc.getAttribute("/Out/Transaction/uploadDefinitionOid");
         beneficiaryName     = xmlDoc.getAttribute("/Out/Transaction/beneficiaryName");
         currency            = xmlDoc.getAttribute("/Out/Transaction/currency");
         hasPOLineItems      = true;
      }
   }

  // If this is a new shipment, the shipment OID will be in the out section of the XML
  if( (shipmentOid == null) || shipmentOid.equals("0") || shipmentOid.equals(""))
   {
      String numShipmentsString = xmlDoc.getAttribute("/In/numberOfShipments");

      int numShipments = 1;

      if(numShipmentsString != null)
         numShipments = Integer.parseInt(numShipmentsString);

      ShipmentTermsWebBean shipmentTerms	 = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
      TermsWebBean terms = (TermsWebBean) beanMgr.getBean("Terms");

      shipmentTerms.loadShipmentTerms(terms.getAttribute("terms_oid"), numShipments);

      shipmentOid = shipmentTerms.getAttribute("shipment_oid");
      xmlDoc.setAttribute("/In/SearchForINV/shipment_oid", shipmentOid);
      xmlDoc.setAttribute("/In/Terms/ShipmentTermsList/shipment_oid", shipmentOid);
   }
   buttonPressed = xmlDoc.getAttribute("/In//Update/ButtonPressed");
   financeType = xmlDoc.getAttribute("/In/Terms/finance_type");
   buyerBacked = xmlDoc.getAttribute("/In/Terms/financing_backed_by_buyer_ind");
   if (StringFunction.isBlank(buyerBacked)){
	   buyerBacked = TradePortalConstants.INDICATOR_NO;
   }

   String importIndicator = xmlDoc.getAttribute("/In/Instrument/import_indicator");
   if (currency == null || StringFunction.isBlank(currency)){
	   currency = xmlDoc.getAttribute("/In/Terms/amount_currency_code");
	   if (StringFunction.isBlank(poCurrency)){
		   poCurrency = currency;
	   }
   }
   
  String invClassification = null; 
	  if (TradePortalConstants.TRADE_LOAN_REC.equals(financeType) | TradePortalConstants.INDICATOR_NO.equals(importIndicator) |
			  TradePortalConstants.SELLER_REQUESTED_FINANCING.equals(financeType)){
		  invClassification = "R";
	  }
	  else if (TradePortalConstants.TRADE_LOAN_PAY.equals(financeType) | TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(financeType)){
		  invClassification = "P";
	  }
   StringBuffer groupingOptions              = new StringBuffer();
   String       selectedOption               = "";
   StringBuffer optionsExtraTags             = new StringBuffer();
   String		multiplePOLineItems 		 = TradePortalConstants.INDICATOR_NO;   
     
   selectedOption = request.getParameter("selectPOListType");
   
   if (selectedOption == null) {
      selectedOption = (String) session.getAttribute("selectPOListType");
   }
   groupingOptions.append("<option value='");
   groupingOptions.append(TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER);
   groupingOptions.append("' ");
   if (TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER.equals(selectedOption) || selectedOption == null) {
      groupingOptions.append(" selected ");
	 }
   groupingOptions.append(">");
   groupingOptions.append(resMgr.getText("ViewTransInvoices.ShowPOAndLineItem", TradePortalConstants.TEXT_BUNDLE));
   groupingOptions.append("</option>");

   groupingOptions.append("<option value='");
   groupingOptions.append(TradePortalConstants.BY_PO_NUMBER);
   groupingOptions.append("' ");
   if (TradePortalConstants.BY_PO_NUMBER.equals(selectedOption)) {
      groupingOptions.append(" selected ");
	 }
   groupingOptions.append(">");
   groupingOptions.append(resMgr.getText("ViewTransInvoices.ShowPONumber", TradePortalConstants.TEXT_BUNDLE));
   groupingOptions.append("</option>");
   
   session.setAttribute("selectPOListType", selectedOption);
   
   // Set a bunch of secure parameters necessary for the Add PO Line Items mediator
   secureParms = new Hashtable();
   secureParms.put("transactionOid", transactionOid);
   //secureParms.put("shipmentOid", shipmentOid);
   secureParms.put("hasPOLineItems", String.valueOf(hasPOLineItems));

   if (hasPOLineItems)
   {
      secureParms.put("uploadDefinitionOid", uploadDefinitionOid);
      secureParms.put("currency",            currency);
      secureParms.put("beneficiaryName",     beneficiaryName);
   }
	
   if (!hasPOLineItems)currency = xmlDoc.getAttribute("/In/SearchForINV/currency");
  
%>


<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

	<div class="pageMainNoSidebar">     
		<div class="pageContent"> 
			<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="ViewTransInvoices.Header"/>
				<jsp:param name="helpUrl"  value="customer/add_invoice.htm" />
  			</jsp:include>
  			
  			<jsp:include page="/common/ErrorSection.jsp" />
			<form id="ViewTransInvoicesForm" name="ViewTransInvoicesForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
                <div class="formContentNoSidebar">
					<%= formMgr.getFormInstanceAsInputField("ViewTransInvoicesForm", secureParms) %>
					<input type=hidden name="buttonName"     value="">
					<input type=hidden name="hasPOLineItems" value="<%=hasPOLineItems%>">
					<div class="gridSearch">
						<div class="searchHeader">
							<span class="searchHeaderCriteria">
								<i><%=widgetFactory.createSubLabel("ViewTransInvoices.NoteFilterText")%></i>
							</span>
							
							<span class="searchHeaderActions">
								<jsp:include page="/common/gridShowCount.jsp">
		        					<jsp:param name="gridId" value="viewTransInvoicesDataGridId" />
		      					</jsp:include>
		      				</span>
		      				<div style="clear: both;"></div>
				</div>
				</div>
		      				
				</br>		
					
				<div class="gridSearch">
					<div class="searchHeader">
					<span class="searchHeaderCriteria">
						<%=widgetFactory.createSearchTextField("PONumber","ViewTransInvoices.InvoiceID", "14")%>
						
						<%=widgetFactory.createSearchTextField("BeneName","ViewTransInvoices.BeneficiaryName","35", " class=\"char20\"")%>
					</span>	
					

			<%
			String options = Dropdown.createSortedRefDataOptions(
						TradePortalConstants.CURRENCY_CODE, poCurrency,
						loginLocale);
			%>

			<span class="searchHeaderCriteria">				
			<%=widgetFactory.createLabel("","ViewTransInvoices.AmountRange",false, false, false, " inline searchLabel")%>	
			<%=widgetFactory.createLabel("","ViewTransInvoices.From",false, false, false, "inline searchLabel")%>									
			<%=widgetFactory.createTextField("AmountFrom","",amountFrom, "27", false, false, false, "class='char6'",  "", " inline searchItem")%>
			
			<%=widgetFactory.createLabel("","InstSearch.To",false, false, false, " inline searchLabel")%>									
			<%=widgetFactory.createTextField("AmountTo","",amountTo, "27", false, false, false, "class='char6'",  "", " inline searchItem")%>						
		</span>						
		<span class="searchHeaderCriteria">
		<%=widgetFactory.createLabel("","ViewTransInvoices.DateRange",false, false, false, " inline searchLabel")%>
		<%--  DK IR T36000016877 Rel8.2 05/12/2013 starts --%>
		<%=widgetFactory.createSearchDateField("DueDateFrom","ViewTransInvoices.From","class='char6'",datePattern) %>
		<%=widgetFactory.createSearchDateField("DueDateTo","InstSearch.To","class='char6'",datePattern) %>
		<%--  DK IR T36000016877 Rel8.2 05/12/2013 ends --%>
		</span>
				
					<span class="searchHeaderActions">
					
						<button data-dojo-type="dijit.form.Button" type="button" id="SearchButton">
							<%=resMgr.getText("common.FilterText", TradePortalConstants.TEXT_BUNDLE)%>
						    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  			filterInvs();return false;
	      					</script>
						</button>
						<%=widgetFactory.createHoverHelp("SearchButton", "SearchHoverText")%>
					</span>
					<div style="clear: both;"></div>
				</div>
				</div>
					
					<%
						//}//end of hasPOLineItems if
					%>
 					<input type=hidden name="multiplePOLineItems" value="<%=multiplePOLineItems%>">
					
					<%

	                    	gridLayout = dgFactory.createGridLayout("viewTransInvoicesDataGridId", "ViewTransInvoicesDataGrid");
	                    	gridHtml = dgFactory.createDataGrid("viewTransInvoicesDataGridId","ViewTransInvoicesDataGrid",null);
	                    	dataViewJava = "ViewTransInvoicesDataView";
	                    	
                    %>
						<%=gridHtml%>
						                              
				</div><%-- end of formContentNoSidebar div --%>  
  			</form>
  		</div><%-- end of pageContent div --%>
	</div><%-- end of pageMainNoSidebar div --%>
	
	<jsp:include page="/common/Footer.jsp">
	    <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	    <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
    </jsp:include>
    
    
    <script type='text/javascript'>
		var gridLayout = <%= gridLayout %>;	
		
		var uploadDefinitionOid = <%=uploadDefinitionOid%>;
		var beneficiaryName = "<%=beneficiaryName%>";
		var dataView = "<%=EncryptDecrypt.encryptStringUsingTripleDes(dataViewJava,userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
		var transactionOid="<%=transactionOid%>";
		
		if(uploadDefinitionOid == null || uploadDefinitionOid == "null") uploadDefinitionOid = "";
		if(beneficiaryName == null || beneficiaryName == "null") beneficiaryName = "";
		var benName = '<%=beneName%>';
		var poNumber = '<%=poNumber%>';
		var currency = '<%=StringFunction.escapeQuotesforJS(poCurrency)%>';
		var amountFrom = '<%=StringFunction.escapeQuotesforJS(amountFrom)%>';
		var amountTo = '<%=StringFunction.escapeQuotesforJS(amountTo)%>';
		var dueDateFrom = '<%=StringFunction.escapeQuotesforJS(dueDateFrom)%>';
		var dueDateTo = '<%=StringFunction.escapeQuotesforJS(dueDateTo)%>';
	
		var initSearchParms="uploadDefinitionOid="+uploadDefinitionOid;
		   	initSearchParms+="&beneficiaryName="+beneficiaryName;
			initSearchParms+="&sourceType=<%=TradePortalConstants.PO_SOURCE_UPLOAD%>";
			initSearchParms+="&hasPOLineItems=<%=hasPOLineItems%>";
			initSearchParms+="&transactionOid=<%=transactionOid%>";
			initSearchParms+="&encrypted=true";
			initSearchParms+="&beneName="+benName;
			initSearchParms+="&poNumber="+poNumber;
			initSearchParms+="&amountFrom="+amountFrom;
			initSearchParms+="&amountTo="+amountTo;
			initSearchParms+="&dueDateFrom="+dueDateFrom;
			initSearchParms+="&dueDateTo="+dueDateTo;
			initSearchParms+="&invClassification=<%=invClassification%>";
			initSearchParms+="&transactionOid=<%=transactionOid%>";
			initSearchParms+="&importIndicator=<%=importIndicator%>";
			initSearchParms+="&financeType=<%=financeType%>";
			initSearchParms+="&buyerBacked=<%=buyerBacked%>";
			
		console.log("initSearchParms="+initSearchParms);
		console.log("dataViewJava-"+dataView);
		createDataGrid("viewTransInvoicesDataGridId", dataView,gridLayout, initSearchParms);
		
		function filterInvs(){
			require(["dojo/dom","dojo/domReady!" ], function(dom) {
			console.log("Inside filterPOs()");
			
			var benName = dom.byId("BeneName").value;
			var poNumber = dom.byId("PONumber").value;
			var amountFrom = dom.byId("AmountFrom").value;
			var amountTo = dom.byId("AmountTo").value;
			var dueDateFrom = dom.byId("DueDateFrom").value;
			var dueDateTo = dom.byId("DueDateTo").value;
			var uploadDefinitionOid = <%=uploadDefinitionOid%>;
			var transactionOid="<%=transactionOid%>";
			
			if(uploadDefinitionOid == null || uploadDefinitionOid == "null") uploadDefinitionOid = "";
			
			var searchParms="uploadDefinitionOid=<%=uploadDefinitionOid%>";
			searchParms+="&sourceType=<%=TradePortalConstants.PO_SOURCE_UPLOAD%>";
			searchParms+="&hasPOLineItems=<%=hasPOLineItems%>";
			searchParms+="&transactionOid=<%=transactionOid%>";
			searchParms+="&encrypted=true";
			
			searchParms+="&beneName="+benName;
			searchParms+="&poNumber="+poNumber;
			searchParms+="&amountFrom="+amountFrom;
			searchParms+="&amountTo="+amountTo;
			searchParms+="&dueDateFrom="+dueDateFrom;
			searchParms+="&dueDateTo="+dueDateTo;
			searchParms+="&invClassification=<%=invClassification%>";
			searchParms+="&transactionOid=<%=transactionOid%>";
			searchParms+="&buttonPressed=<%=buttonPressed%>";
			searchParms+="&importIndicator=<%=importIndicator%>";
			searchParms+="&financeType=<%=financeType%>";
			searchParms+="&buyerBacked=<%=buyerBacked%>";
			
			console.log("searchParms="+searchParms);

			searchDataGrid("viewTransInvoicesDataGridId", "<%=dataViewJava%>", searchParms);
		});
		}
		
		function addTransInvoices(){
			
			var rowKeys = getSelectedGridRowKeys("viewTransInvoicesDataGridId");
			
			if(rowKeys == ""){
				alert("Please select record(s).");
				return;
			}
			
			setButtonPressed('AssignTransInvoices', 0); 
			submitFormWithParms("AddTransInvoicesForm", 'AssignTransInvoices', "checkbox", rowKeys);
		}
		
		function closePOLineItems(){
			var URL = '<%=formMgr.getLinkAsUrl("goToInstrumentNavigator", response)%>';
			document.location.href  = URL;
	 		<%-- return false; --%>
		}
		function shufflePODataView(){
			var selectedValue;
			require(["dijit/registry"],
			        function(registry) { 
						selectedValue = registry.byId("selectPOListType").value;
			});
			 var URL;
			 if(selectedValue == "<%=TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER%>"){
				 URL = '<%=formMgr.getLinkAsUrl("goToAddPOLineItems", response)+"&selectPOListType="+TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER %>';
			 }else if(selectedValue == "<%=TradePortalConstants.BY_PO_NUMBER%>"){
				 URL = '<%=formMgr.getLinkAsUrl("goToAddPOLineItems", response)+"&selectPOListType="+TradePortalConstants.BY_PO_NUMBER%>';
			 }
			 document.location.href  = URL;
		}
		
		require(["dojo/aspect","dijit/registry","dojo/ready"], function(aspect,registry,ready){
			ready(function(){
				var bgrid=registry.byId("viewTransInvoicesDataGridId");<%-- Add your grid id instead mailmessagedatagridid --%>
				aspect.after(bgrid, "_onFetchComplete", function(){
					var invClassification='<%=invClassification%>';
					if ('P' == invClassification){
						var columnHeaderNodes = dojo.query(
					              '.dojoxGridHeader table th',
					              bgrid.viewsHeaderNode)
						var nthColumn = 7;
						<%--  if has child and its not a textnode - this may happen --%>
						<%--  when there is a listener (dnd, click) attached for sorting etc. --%>
						if(columnHeaderNodes[nthColumn].firstChild && columnHeaderNodes[nthColumn].firstChild.nodeType != 3) 
						    tgt = columnHeaderNodes[nthColumn].firstChild;
						else tgt = columnHeaderNodes[nthColumn];
							tgt.innerHTML = "Seller Name";
					}
					if(!bgrid.store._numRows){
						registry.byId("ViewTransInvoices_AddSelectedPOLineItemsText").set('disabled',true);<%-- Add your button id to byId --%>
					}

				});
			});
		});
	</script>
</body>
</html>
<jsp:include page="/common/gridShowCountFooter.jsp">
	<jsp:param name="gridId" value="viewTransInvoicesDataGridId" />
</jsp:include>
<%
   DocumentHandler xml = formMgr.getFromDocCache();

   xml.removeComponent("/In/POLineItemList");
   xml.removeComponent("/Error");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", xml);
%>
