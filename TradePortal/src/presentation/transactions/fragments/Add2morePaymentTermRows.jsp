<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 -Initial Version --%>
<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<% 
//Initialize WidgetFactory.
WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession); 
String loginLocale = userSession.getUserLocale();

int pmtTermsIndex = 0;
int iLoop=0;
String percentOrAmountInd = "";
String parmValue = request.getParameter("pmtTermsIndex");
String termsPmtType = request.getParameter("TermsPmtType");
boolean isReadOnly = false;
boolean isFromExpress = false;
int newRecCount=0;
String defaultText1 = "";
String options="";
//Leelavathi IR#T36000015041 CR-737 Rel-8.2 06/05/2013 Begin
String currency="";
//Leelavathi IR#T36000015041 CR-737 Rel-8.2 06/05/2013 End
//Leelavathi IR#T36000011655 Rel-8.2 02/20/2013 Begin
String accpDefpDisabled ="";
if("ACCP".equals(termsPmtType) || "DEFP".equals(termsPmtType)){
	accpDefpDisabled = " READONLY ";
}
//Leelavathi IR#T36000011655 Rel-8.2 02/20/2013 End

DocumentHandler reqdDocList=null;
if ( parmValue != null ) {
	try {
		pmtTermsIndex = Integer.parseInt(parmValue);
		
		newRecCount = Integer.parseInt(request.getParameter("count"));
	}
	catch (Exception ex ) {
	}
}


List<PmtTermsTenorDtlWebBean> pmtTermsList = new ArrayList<PmtTermsTenorDtlWebBean>();
PmtTermsTenorDtlWebBean pmtTerms = null;
for(int i=0; i<newRecCount ; i++){
	//when included per ajax, the business objects will be blank      
	 pmtTerms=beanMgr.createBean(PmtTermsTenorDtlWebBean.class,"PmtTermsTenorDtl");

	if("PAYM".equals(termsPmtType)){
		pmtTerms.setAttribute("percent","100");
		pmtTerms.setAttribute("tenor_type","SGT");
	}else if("ACCP".equals(termsPmtType)){
		pmtTerms.setAttribute("percent","100");
		pmtTerms.setAttribute("tenor_type","ACC");
	}else if("DEFP".equals(termsPmtType)){
		pmtTerms.setAttribute("percent","100");
		pmtTerms.setAttribute("tenor_type","DFP");
		//Leelavathi IR#T36000011802 Rel-8.2 14/02/2013 Begin
	}else if("MIXP".equals(termsPmtType)){
		pmtTerms.setAttribute("percent","0");
	}else if("NEGO".equals(termsPmtType)){
		pmtTerms.setAttribute("percent","0");
		//Leelavathi IR#T36000011802 Rel-8.2 14/02/2013 End
	}
	pmtTermsList.add(pmtTerms);
	
}

%>
<%-- Leelavathi 30thJan2013 - Rel8200 CR-737 - start --%>
<%@ include file="TransactionPaymentTermRows.frag" %>
<%-- Leelavathi 30thJan2013 - Rel8200 CR-737 - End --%>
