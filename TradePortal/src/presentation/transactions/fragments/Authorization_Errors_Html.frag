<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%   	for (int j = 0; j < errorMessageList.size(); j++)
   	{
%>		     <tr>
          		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td valign="top">
			    <img src="/portal/images/Error.gif" alt="<%= resMgr.getText("ErrorSection.ErrorMessage", TradePortalConstants.TEXT_BUNDLE) %>" >
			</td>
          		<td>&nbsp;&nbsp;&nbsp;</td>
			<td width="100%" class="ErrorText">
            		   <p class="ControlLabel">
				<%= errorMessageList.elementAt(j) %>
			   </p>
			</td>
          		<td>&nbsp;</td>
		      </tr>
    		      <tr> 
      	  		<td>&nbsp;</td>
       	  		<td>&nbsp;</td>
      	  		<td>&nbsp;</td>
      	  		<td>&nbsp;</td>
      	  		<td>&nbsp;</td>
    		      </tr>
<%   	}

   	for (int k = 0; k < warningMessageList.size(); k++)
   	{
%>      	     <tr>
          		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td valign="top">
			    <img src="/portal/images/warning.gif" alt="<%= resMgr.getText("ErrorSection.WarningMessage", TradePortalConstants.TEXT_BUNDLE) %>" >
			</td>
          		<td>&nbsp;&nbsp;&nbsp;</td>
			<td width="100%" class="WarningText">
            		   <p class="ControlLabel">
				<%= warningMessageList.elementAt(k) %>
			   </p>
			</td>
          		<td>&nbsp;</td>
		      </tr>
    		      <tr> 
      	  		<td>&nbsp;</td>
       	  		<td>&nbsp;</td>
      	  		<td>&nbsp;</td>
      	  		<td>&nbsp;</td>
      	  		<td>&nbsp;</td>
    		      </tr>
<%   	}

   	for (int n = 0; n < infoMessageList.size(); n++)
   	{
%>      	     <tr>
          		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td valign="top">
			    <img src="/portal/images/Information.gif" alt="<%= resMgr.getText("ErrorSection.InfoMessage", TradePortalConstants.TEXT_BUNDLE) %>" >
			</td>
          		<td>&nbsp;&nbsp;&nbsp;</td>
			<td width="100%" class="InfoText">
            		   <p class="ControlLabel">
			 	<%= infoMessageList.elementAt(n) %>
	    		   </p>
			</td>
          		<td>&nbsp;</td>
		      </tr>
    		      <tr> 
      	  		<td>&nbsp;</td>
       	  		<td>&nbsp;</td>
      	  		<td>&nbsp;</td>
      	  		<td>&nbsp;</td>
      	  		<td>&nbsp;</td>
    		      </tr>
<%   	}
%>