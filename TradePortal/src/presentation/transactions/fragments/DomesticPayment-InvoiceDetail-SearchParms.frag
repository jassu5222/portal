<%--
*********************************************************************************************
                  Domestic Payment InvoiceDetail - Search Parms include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %>
  tag.  It extracts search criteria from the request and builds a  where clause
  suitable for use in a listview (use DomesticPaymentListView as a model).
  As it is included with <%@ include %> rather than with the <jsp:include>
  directive, it is not a standalone servlet.  This JSP was created to allow
  a degree of reusability in multiple pages.

  This JSP assumes that searchType, dynamicWhereClause, searchListViewName and
  newSearchCriteria has previously been declared and assigned a
  value.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*********************************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%
  //SHILPAR CR-597 start
  // Choose a character for the delimiter that will never appear in any data
  // The data must be base64ed first so that we can guarantee that the delimiter
  // will not appear
  String DELIMITER = "@";

  benificiaryName = request.getParameter("BenificiaryName");
       if (benificiaryName != null) {
        	benificiaryName = benificiaryName.trim().toUpperCase();
        } else {
        	benificiaryName = "";
        }

        newSearchCriteria.append("BenificiaryName=" + EncryptDecrypt.stringToBase64String(benificiaryName) + DELIMITER);
        benificiaryBankName = request.getParameter("BenificiaryBankName");
        if (benificiaryBankName != null) {
        	benificiaryBankName = benificiaryBankName.trim().toUpperCase();
        } else {
        	benificiaryBankName = "";
        }
        newSearchCriteria.append("BenificiaryBankName=" + EncryptDecrypt.stringToBase64String(benificiaryBankName) + DELIMITER);

        paymentStatusSearch = request.getParameter("PaymentStatusSearch");
        if (paymentStatusSearch != null) {
        	paymentStatusSearch = paymentStatusSearch.trim();
        } else {
        	paymentStatusSearch = "";
        }
        //BSL IR# SBUL040440564 Begin
        //newSearchCriteria.append("PaymentStatusSearch=" + EncryptDecrypt.stringToBase64String(paymentStatusSearch));
        newSearchCriteria.append("PaymentStatusSearch=" + EncryptDecrypt.stringToBase64String(paymentStatusSearch) + DELIMITER);
        
        amount = request.getParameter("Amount");
        if (amount != null) {
        	amount = amount.trim();
        } else {
        	amount = "";
        }
        newSearchCriteria.append("Amount=" + EncryptDecrypt.stringToBase64String(amount));

        dynamicWhereClause.append(" and (1=1");

        if (InstrumentServices.isNotBlank(benificiaryName)) {
           dynamicWhereClause.append(" and ");
           dynamicWhereClause.append("upper(dp.payee_name) like '%"); 
           dynamicWhereClause.append(SQLParamFilter.filter(benificiaryName));
           dynamicWhereClause.append("%'"); //"%'" is safe for URLdecoding.
        }

        if (InstrumentServices.isNotBlank(benificiaryBankName)) {
           dynamicWhereClause.append(" and ");
           //dynamicWhereClause.append("upper(dp.payee_bank_name) like '");//BSL IR RSUL060252944 06/30/11 DELETE
           dynamicWhereClause.append("upper(dp.payee_bank_name) like '%"); //BSL IR RSUL060252944 06/30/11 ADD
           dynamicWhereClause.append(SQLParamFilter.filter(benificiaryBankName));
           dynamicWhereClause.append("%'");
        }

        if (InstrumentServices.isNotBlank(paymentStatusSearch)) {
           dynamicWhereClause.append(" and ");
           //BSL IR LMUL052437851 05/31/11 BEGIN - paymentStatusSearch value 'REJECTED' should also match 'REJECTED_UNPAID'
           //dynamicWhereClause.append("dp.payment_status='");
           dynamicWhereClause.append("dp.payment_status like '");
           dynamicWhereClause.append(paymentStatusSearch);
           //dynamicWhereClause.append("'");
           dynamicWhereClause.append("%'");
           //BSL IR LMUL052437851 05/31/11 END
        }

        if (InstrumentServices.isNotBlank(amount)) {
           dynamicWhereClause.append(" and ");
           dynamicWhereClause.append("dp.amount='");
           dynamicWhereClause.append(amount);
           dynamicWhereClause.append("'");
        }

        dynamicWhereClause.append(")");
        //BSL IR# SBUL040440564 End changes  

  Debug.debug("The search criteria is " + newSearchCriteria.toString());
  Debug.debug("The search criteria is " + dynamicWhereClause);
//SHILPAR CR-597 end
%>
