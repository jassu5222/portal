<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 
 *******************************************************************************
--%>
            		
            <%=widgetFactory.createCheckboxField(
              "IndividualDebitsIndicator", 
              "DomesticPaymentRequest.IndividualDebit",
              TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("individual_debit_ind")),
              isReadOnly,
              false,
              "",
              "",
              ""
              )%>