<%--
*******************************************************************************
                    Instrument Search Basic Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Basic Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<div id="basicInstrumentFilter" style="padding-top: 5px;"> 
  <input type="hidden" name="NewSearch" value="Y">

  <div class="searchDetail">
    <span class="searchCriteria">
	
	<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
	<%-- changed event to window.event to all the fields in order for enter key to work in Firefox --%>
      <%=widgetFactory.createSearchTextField("InstrumentId","InstSearch.InstrumentID","16", " class='char15' onKeydown=' filterOnEnter(window.event, \"InstrumentId\", \"Basic\");'")%>
    <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>
<% 

  if (showInstrumentDropDown)
  {
	  options = Dropdown.getInstrumentList(instrumentType,loginLocale, instrumentTypes );
%>
	<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
      <%=widgetFactory.createSearchSelectField("InstrumentTypeBasic","InstSearch.InstrumentType"," ", options, "onKeydown='filterOnEnter(window.event, \"InstrumentTypeBasic\", \"Basic\");'")%>
    <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>  
  <% 
  } else
  {
      // Based on search condition (for specific instrument type searches only),
      // print the name of the instrument type.  Also store this value so it is 
      // picked up on return to the page.
      String instrTypeText = "";
      if (searchCondition.equals(TradePortalConstants.SEARCH_EXP_DLC_ACTIVE))
         instrTypeText = "CorpCust.ExportLC";
      else if (searchCondition.equals(InstrumentType.LOAN_RQST))
         instrTypeText = "CorpCust.LoanRequests";
      else
         instrTypeText = instrumentType;
   %>
   <%=widgetFactory.createSubLabel("InstSearch.InstrumentType")%>
   <b> <%=resMgr.getText(StringFunction.xssCharsToHtml(instrTypeText), TradePortalConstants.TEXT_BUNDLE)%> </b>
   <%
      out.println("<input type=hidden name=InstrumentType value='");
 	//Validation to check Cross Site Scripting
	if(instrumentType != null){
		instrumentType = StringFunction.xssCharsToHtml(instrumentType);
	}
      out.println(instrumentType);
      out.println("'>");
   }
  %>
  	 <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>	 
      <%=widgetFactory.createSearchTextField("RefNum","InstSearch.ApplRefNumber","30", " width=\"25\" onKeydown='filterOnEnter(window.event, \"RefNum\", \"Basic\");'")%>
     <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%> 
     <%
     if (userSession.isCustNotIntgTPS()){
     %>
     	 <%=widgetFactory.createSearchTextField("BankInstrumentId","InstSearch.BankInstrumentID","16", " class='char15' onKeydown='filterOnEnter(window.event, \"BankInstrumentId\", \"Basic\");'")%>
     <% } %>
    </span>
    <span class="searchActions">
      <button id="basicSearchId" class="gridSearchButton" data-dojo-type="dijit.form.Button" type="button" >
       <%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          local.searchInstruments("Basic");return false;
        </script>
      </button>
      <%-- <a class="searchTypeToggle" href="<%=linkHref%>" ><%=linkName%></a>--%>
      <a id="advanceSearchType" class="searchTypeToggle" href="javascript:local.shuffleFilter('Advanced');"><%=resMgr.getText("ExistingDirectDebitSearch.Advanced", TradePortalConstants.TEXT_BUNDLE)%></a>      
    </span>
     <%=widgetFactory.createHoverHelp("advanceSearchType","AdvancedSearchHypertextLinkHoverText") %>
    <%=widgetFactory.createHoverHelp("basicSearchId","SearchHoverText") %>
    <div style="clear:both"></div>
  </div>
   
  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">

	<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
      <%=widgetFactory.createSearchTextField("BankRefNum","InstSearch.OrigBanksRefNo","30", " class='char25' onKeydown='filterOnEnter(window.event, \"BankRefNum\", \"Basic\");'")%>
      <%=widgetFactory.createSearchTextField("VendorIdBasic","InstSearch.VendorId","15", " class='char10' onKeydown='filterOnEnter(window.event, \"VendorIdBasic\", \"Basic\");'")%>
    <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>	
    </span>
    <div style="clear:both"></div>
  </div>
 </div>