<%--
*******************************************************************************
                  Instrument Search - Search Parms include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %>
  tag.  It extracts search criteria from the request and builds a  where clause
  suitable for use in a listview (use InstrumentSearchListView as a model).
  As it is included with <%@ include %> rather than with the <jsp:include>
  directive, it is not a standalone servlet.  This JSP was created to allow
  a degree of reusability in multiple pages.

  This JSP assumes that searchType, dynamicWhere, searchListViewName and
  newSearchCriteria has previously been declared and assigned a
  value.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%

  // This boolean and MediatorServices provides error handling for invalid
  // search criteria fields.
  boolean errorsFound = false;
  MediatorServices medService = new MediatorServices();
  medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());

  // Choose a character for the delimiter that will never appear in any data
  // The data must be base64ed first so that we can guarantee that the delimiter
  // will not appear
  String DELIMITER = "@";

  // determine if this is a redisplay of the listview coming from either
  // the a page link (page number or prev, next link) or the sort column
  // link

     // Determine the possible instrument type combinations to search on
     // and any additional search conditions based on where the previous
     // page's actions.  The conditions are as follows:
     // for Export DLC searches coming from Export DLC Transfer, only search
     // on instruments where the transaction type where the original transaction
     // is Advise
     // otherwise, only search on instrument types that are available
     // to the user, based on user's rights

     if (searchCondition.equals(TradePortalConstants.SEARCH_EXP_DLC_ACTIVE))
     {
        instrumentType = InstrumentType.EXPORT_DLC;
     }else if (!searchCondition.equals(TradePortalConstants.SEARCH_ALL_INSTRUMENTS))
     {
        instrumentType = "";
     }
     
     
     if (searchType.equals(TradePortalConstants.ADVANCED)) {

        dayFrom   = request.getParameter("DayFrom");
        monthFrom = request.getParameter("MonthFrom");
        yearFrom  = request.getParameter("YearFrom");

        if (dayFrom != null) newSearchCriteria.append("DayFrom=" + EncryptDecrypt.stringToBase64String(dayFrom) + DELIMITER);
        if (monthFrom != null) newSearchCriteria.append("MonthFrom=" + EncryptDecrypt.stringToBase64String(monthFrom) + DELIMITER);
        if (yearFrom != null) newSearchCriteria.append("YearFrom=" + EncryptDecrypt.stringToBase64String(yearFrom) + DELIMITER);

        if (TPDateTimeUtility.isGoodDate(yearFrom, monthFrom, dayFrom)) {
           String fromDate = TPDateTimeUtility.formatOracleDate(yearFrom,
                                                           monthFrom, dayFrom);
           if (!fromDate.equals("")) {
              dynamicWhere.append(" and i.copy_of_expiry_date >= '");
              dynamicWhere.append(fromDate);
              dynamicWhere.append("'");
           }
        } else {
           medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                   TradePortalConstants.INVALID_FROM_DATE);
           errorsFound = true;
        }

        dayTo   = request.getParameter("DayTo");
        monthTo = request.getParameter("MonthTo");
        yearTo  = request.getParameter("YearTo");

        if (dayTo != null) newSearchCriteria.append("DayTo=" + EncryptDecrypt.stringToBase64String(dayTo) + DELIMITER);
        if (monthTo != null) newSearchCriteria.append("MonthTo=" + EncryptDecrypt.stringToBase64String(monthTo) + DELIMITER);
        if (yearTo != null) newSearchCriteria.append("YearTo=" + EncryptDecrypt.stringToBase64String(yearTo) + DELIMITER);

        if (TPDateTimeUtility.isGoodDate(yearTo, monthTo, dayTo)) {
           String toDate = TPDateTimeUtility.formatOracleDate(yearTo,
                                                        monthTo, dayTo);
           if (!toDate.equals("")) {
             dynamicWhere.append(" and i.copy_of_expiry_date <= '");
             dynamicWhere.append(toDate);
             dynamicWhere.append("'");
           }
        } else {
           medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                   TradePortalConstants.INVALID_TO_DATE);
           errorsFound = true;
        }

        instrumentType = request.getParameter("InstrumentType");
      //Validation to check Cross Site Scripting	  
    	if(instrumentType != null){
    		instrumentType = StringFunction.xssCharsToHtml(instrumentType);
    	  }

        otherParty = request.getParameter("OtherParty");
        if (otherParty != null) {
           otherParty = otherParty.trim().toUpperCase();
        } else {
           otherParty = "";
        }

        currency = request.getParameter("Currency");
        if (currency == null) {
           currency = "";
        }
        
        amountFrom = request.getParameter("AmountFrom");
        if (amountFrom != null) {
           amountFrom = amountFrom.trim();
        } else {
          amountFrom = "";
        }
        amountTo = request.getParameter("AmountTo");
        if (amountTo != null) {
           amountTo = amountTo.trim();
        } else {
           amountTo = "";
        }
        
        vendorId = request.getParameter("VendorId");
        if (vendorId != null) {
           vendorId = vendorId.trim().toUpperCase();
           if (vendorId.equals("")) {
              vendorId = "";
           }
        }
     } else {

        // Unlike the ADVANCED search, the SIMPLE search values are ORed together.
        // To handle this we'll add an 'AND (simple conditions ored together)'
        // clause.  This is only added if at least one of the search fields has a
        // value.

        instrumentId = request.getParameter("InstrumentId");

        if (instrumentId != null) {
          instrumentId = instrumentId.trim().toUpperCase();
        } else {
          instrumentId = "";
        }

        refNum = request.getParameter("RefNum");
        if (refNum != null) {
           refNum = refNum.trim().toUpperCase();
        } else {
           refNum = "";
        }
        
        bankRefNum = request.getParameter("BankRefNum");

        if (bankRefNum != null) {
           bankRefNum = bankRefNum.trim().toUpperCase();
        } else {
           bankRefNum = "";
        }
        
        instrumentType = request.getParameter("InstrumentType");
        if (instrumentType != null) {
           instrumentType = instrumentType.trim();
        } else {
           instrumentType = "";
        }
        
        vendorId = request.getParameter("VendorId");
        if (vendorId != null) {
           vendorId = vendorId.trim().toUpperCase();
        } else {
           vendorId = "";
        }

     }  // end if-else (oldSearchCriteria != null)
  
  Debug.debug("The search criteria is " + newSearchCriteria.toString());
  Debug.debug("The search criteria is " + dynamicWhere);

  if (errorsFound) {
    // Any errors found while editing the date and amount fields need to be
    // placed in the doc cache.
    DocumentHandler errDoc = formMgr.getFromDocCache();
    medService.addErrorInfo();
    errDoc.setComponent("/Error", medService.getErrorDoc());
    formMgr.storeInDocCache("default.doc", errDoc);
  }
%>
