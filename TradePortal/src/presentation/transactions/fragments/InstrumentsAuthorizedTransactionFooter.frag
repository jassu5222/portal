<%--
*******************************************************************************
  Payment Authorized Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>

<%
  DGridFactory dgridFactory = new DGridFactory(resMgr, userSession, formMgr, response);
  String gridLayout = dgridFactory.createGridLayout("tradeAuthGrid", "InstrumentsAuthorizedTransactionsDataGrid");
%>

<%--the route messages dialog--%>
<div id="routeDialog" style="display:none;"></div>

<script type="text/javascript">

<%--IR T36000015639 REL ER 8.1.0.4 BEGIN --%>
  var local = {};
  var selectedOrg="";
  var userOrgOid = '<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>';

  var savedSort = savedSearchQueryData["sort"];
  selectedOrg = savedSearchQueryData["selectedOrg"];
  
  require(["dojo/query", "dojo/on", "t360/OnDemandGrid", "dojo/dom-construct", "t360/popup", "dojo/ready","dijit/registry","dojo/domReady!"],
	      function(query, on, onDemandGrid, domConstruct, t360popup, ready, registry){
		  ready(function(){
	  if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg){
	  selectedOrg = ( registry.byId('Organization') == null ?"":registry.byId('Organization').attr('value'));	
	  }
	<%-- IR 16481 start --%>
	  else if(registry.byId('Organization')){
			  registry.byId("Organization").set('value',selectedOrg);
	  }
	    if("" == selectedOrg || null == selectedOrg){
	      selectedOrg = userOrgOid;
	    }
	    if("" == savedSort || null == savedSort || "undefined" == savedSort)
  		  savedSort = '0';
	  <%-- IR 16481 end    --%>
<%--IR T36000015639 REL ER 8.1.0.4 END --%>	    
    <%--get grid layout from the data grid factory--%>
    var gridLayout = <%= gridLayout %>;

    <%--set the initial search parms--%>
    <%-- cquinton 3/26/2013 Rel PR ir#15207 org must always be encrypted for security --%>
    <%--IR T36000015639 REL ER 8.1.0.4  --%>
    <%--MEer Rel 8.4 IR-23915--%>
  var initSearchParms = "selectedOrg="+selectedOrg;
    <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InstrumentsAuthorizedTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    onDemandGrid.createOnDemandGrid("tradeAuthGrid", viewName,gridLayout, initSearchParms,savedSort); 
	  });
    
    local.filterAuthListView = function() {
      var searchParmss = "";
    	 
      if (<%=SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK)%> == true && <%=totalOrganizations%> > 1){
        var org=registry.byId('Organization').value;
        searchParmss += "selectedOrg="+org;
      }
      else{
        searchParmss += "selectedOrg=<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>";
      }
    	 
    <%--  searchParmss += "&filterAllCashMgmtTrans=<%=filterAllCashMgmtTrans%>"; --%>
  
    	 
      onDemandGrid.searchDataGrid("tradeAuthGrid", "InstrumentsAuthorizedTransactionsDataView", searchParmss);
    }

    query('#tradeAuthRefresh').on("click", function() {
      local.filterAuthListView();
    });
    query('#tradeAuthGridEdit').on("click", function() {
      var columns = onDemandGrid.getColumnsForCustomization('tradeAuthGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "tradeAuthGrid", "InstrumentsAuthorizedTransactionsDataGrid", columns ];
      t360popup.open(
        'tradeAuthGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
	  
  });


</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="tradeAuthGrid" />
</jsp:include>
