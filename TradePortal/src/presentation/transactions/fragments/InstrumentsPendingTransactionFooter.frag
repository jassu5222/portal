<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridLayout = dgFactory.createGridLayout("tradePendGrid", "InstrumentsPendingTransactionsDataGrid");
%>

<%--the route messages dialog--%>


<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  var savedSort = savedSearchQueryData["sort"];
 var selectedOrg = savedSearchQueryData["selectedWorkflow"];
 var selectedStatus = savedSearchQueryData["selectedStatus"];
  <%--set the initial search parms--%>
 
<%
  //cquinton 3/26/2013 Rel PR ir#15207 always send encrypted workflow for security!
  String encryptedSelectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
%>
 
	<%-- IR 16481 start --%>
	function initializePenDataGrid(){
  require(["dojo/dom", "dojo/query", "dijit/registry", "t360/datagrid"], 
	      function(dom, query, registry, t360grid) {
		  if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg){
		 <%--  selectedOrg = ( registry.byId('Workflow') == null ?"":registry.byId('Workflow').attr('value'));	 --%>
		selectedOrg= '<%=encryptedSelectedWorkflow%>';
		  }
		  else{
			  registry.byId("Workflow").set('value',selectedOrg);
			  	}
		    if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg){
		      selectedOrg = userOrgOid;
		    }
		    if("" == savedSort || null == savedSort || "undefined" == savedSort)
	  		  savedSort = '0';
		    if("" == selectedStatus || null == selectedStatus || "undefined" == selectedStatus){
		    	selectedStatus = dom.byId("transStatusType").value;
			 }
		    else{
		    	  registry.byId("transStatusType").set('value',selectedStatus);
  		  	}
		    <%--MEer Rel 8.4 IR-23915--%>	    
		    <%--var initSearchParms = "selectedStatus="+selectedStatus+"&selectedWorkflow="+selectedOrg+"&filterAllCashMgmtTrans=<%=filterAllCashMgmtTrans%>&confInd=<%=confInd%>";--%>
		    var initSearchParms = "selectedStatus="+selectedStatus+"&selectedWorkflow="+selectedOrg; 
 <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
 var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InstrumentsPendingTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var instruPendTransGrid = 
    createDataGrid("tradePendGrid", viewName,
                    gridLayout, initSearchParms,savedSort);

  });
	}
	<%-- IR 16481 end --%>
  <%--provide a search function that collects the necessary search parameters--%>
  function searchPendTrans() {
    console.log("Inside searchPendTrans()");
    require(["dojo/dom"], function(dom){
      var workflow = dijit.byId("Workflow").attr('value');
      var status = dijit.byId("transStatusType").attr('value');
      var searchParms = null;
      if (status != "")
        searchParms="&selectedStatus="+status;
      if (workflow != "")
        searchParms+="&selectedWorkflow="+workflow;
    
      <%--cquinton 3/26/2013 remove encrypted parameter as no longer used--%>
      <%-- searchParms+="&filterAllCashMgmtTrans=<%=filterAllCashMgmtTrans%>&confInd=<%=confInd%>";--%>
      searchDataGrid("tradePendGrid","InstrumentsPendingTransactionsDataView",searchParms);
    });
  }

  <%--authorize the selected records.
      NOTE: this does not currently do any of the authentication checking!
      authentication checking needs to be done first and then
      this is called to actually submit the form!
      Due to change in way form is submitted, this requires changes
      to authentication - which submits form via javascript.
      Probably need to have that just call this when the time comes instead. --%>
  function authorizePaymentTransactions() {
	<%
	  if (InstrumentServices.isNotBlank(certAuthURL)){
		  //String authLink = "javascript:openReauthenticationWindow("+ "'" + certAuthURL + "','Authorize')";
		  //IR T36000014931 REL ER 8.1.0.4 BEGIN
		  String authLink = "javascript:openReauthenticationWindowForList("+ "'" + certAuthURL + "','"+formName+"','"+TradePortalConstants.BUTTON_AUTHORIZE+"','Transaction','tradePendGrid')";
		  //IR T36000014931 REL ER 8.1.0.4 END
	  %>
	openURL("<%=authLink%>");
	  <%}else{%>
    var formName = 'TransactionListForm';   
    
    var buttonName = '<%=TradePortalConstants.BUTTON_AUTHORIZE%>';

    <%--get array of rowkeys from the grid--%>
    var rowKeys = getSelectedGridRowKeys("tradePendGrid");

    <%--submit the form with rowkeys
        becuase rowKeys is an array, the parameter name for
        each will be checkbox + an iteration number -
        i.e. checkbox0, checkbox1, checkbox2, etc --%>
    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
    
    <% }%>
  }


  function openURL(URL){
    if (isActive =='Y') {
    	if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
	    }
    }
		document.location.href  = URL;
		return false;
 }

  <%--delete the selected records--%>
  function deletePaymentTransactions() {
    var formName = 'TransactionListForm';
    var buttonName = '<%=TradePortalConstants.BUTTON_DELETE%>';

    <%--get array of rowkeys from the grid--%>
    var rowKeys = getSelectedGridRowKeys("tradePendGrid");

    <%--submit the form with rowkeys
        becuase rowKeys is an array, the parameter name for
        each will be checkbox + an iteration number -
        i.e. checkbox0, checkbox1, checkbox2, etc --%>
    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
  }

 
  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/ready","dojo/domReady!"],
      function(query, on, t360grid, t360popup,ready){
	  ready(function(){
		  initializePenDataGrid();
    query('#tradePendRefresh').on("click", function() {
      searchPendTrans();
    });
    query('#tradePendGridEdit').on("click", function() {
      var columns = t360grid.getColumnsForCustomization('tradePendGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "tradePendGrid", "InstrumentsPendingTransactionsDataGrid", columns ];
      t360popup.open(
        'tradePendGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
	  });
  });

  
  function routeItems(){
	  openRouteDialog('routeItemDialog', routeItemDialogTitle, 
			  							'tradePendGrid', 
			  							'<%=TradePortalConstants.INDICATOR_YES%>', 
			  							'<%=TradePortalConstants.FROM_TRADE%>','') ;
  }

</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="tradePendGrid" />
</jsp:include>
