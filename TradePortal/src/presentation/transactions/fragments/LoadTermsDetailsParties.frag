<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                              Load Terms Details Parties

  Description: 
    This is common logic used by both the CurrentTermsDetails and the 
  TransactionTermsDetails pages.

  It loads the termsPartyApplicant and termsPartyBeneficiary webbeans with the 
  appropriate data for display in the applicant and beneficiary fields.  In
  general these will be the applicant and the beneficiary from the bank released
  terms record.  (On that record, the applicant is in the c_FirstTermsParty 
  attribute and the beneficiary is in the c_SecondTermsParty attribute.)

  We will test the third, fourth, and fifth terms parties to see if any of them
  are the Transferee type.  If so, then the termsPartyApplicant is replaced 
  with the Beneficiary and termsPartyBeneficiary is replaced with the 
  Transferee party.

  Thus, even through termsPartyApplicant may or may not be an applicant, it 
  always contains the terms party to display in the applicant field on the
  pages.

  This is not a standalone JSP.
*******************************************************************************
--%>
<%
  // First retrieve the applicant and beneficiary from the first and second
  // terms parties.
if(terms != null) {
	//Getting applicant start
	if(isSummaryPage){
		termsPartyOid = InstrumentServices.getLatestPartyOid(instrument.getAttribute("instrument_oid"),"c_FIRST_TERMS_PARTY_OID");
		if(StringFunction.isBlank(termsPartyOid)){
			termsPartyOid = terms.getAttribute("c_FirstTermsParty");
		}
	}
	else{
	  termsPartyOid = terms.getAttribute("c_FirstTermsParty");
	}

  	if (termsPartyOid != null && !termsPartyOid.equals("")) {
     	termsPartyApplicant.setAttribute("terms_party_oid", termsPartyOid);
     	termsPartyApplicant.getDataFromAppServer();
  	}
  //Getting applicant end
  
  //Getting beneficiary start
  	if(isSummaryPage){

		termsPartyOid = InstrumentServices.getLatestPartyOid(instrument.getAttribute("instrument_oid"),"c_SECOND_TERMS_PARTY_OID");
		if(StringFunction.isBlank(termsPartyOid)){
			termsPartyOid = terms.getAttribute("c_SecondTermsParty");
		}
	}
	else{
	  termsPartyOid = terms.getAttribute("c_SecondTermsParty");
	}

  if (termsPartyOid != null && !termsPartyOid.equals("")) {
     	termsPartyBeneficiary.setAttribute("terms_party_oid", termsPartyOid);
     	termsPartyBeneficiary.getDataFromAppServer();
  }
  //Getting beneficiary end
  
  //R 94 CR 932  - Getting Bill customer start
  	if(isSummaryPage){

		termsPartyOid = InstrumentServices.getLatestPartyOid(instrument.getAttribute("instrument_oid"),"c_THIRD_TERMS_PARTY_OID");
		if(StringFunction.isBlank(termsPartyOid)){
			termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
		}
	}
	else{
	  termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
	}

  if (termsPartyOid != null && !termsPartyOid.equals("")) {
     	termsBillCustomer.setAttribute("terms_party_oid", termsPartyOid);
     	termsBillCustomer.getDataFromAppServer();
  }
  //R 94 CR 932  - Getting Bill customer end

  TermsPartyWebBean termsPartyTemp = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  // Now test each of the remaining terms parties to see if any are the
  // transferee type.  If so, replace the applicant webbean with the 
  // beneficiary webbean.  Then replace the beneficiary webbean with the
  // transferee.

  for (int i = 2; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
  {

	  //termsPartyOid = terms.getAttribute(TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);
	  termsPartyOid = InstrumentServices.getLatestPartyOid(instrument.getAttribute("instrument_oid"),TradePortalConstants.TERMS_PARTY_COLUMN_NAMES[i]);
	  if (termsPartyOid != null && !termsPartyOid.equals("")) {
    	 	termsPartyTemp.setAttribute("terms_party_oid", termsPartyOid);
     		termsPartyTemp.getDataFromAppServer();
	        if (termsPartyTemp.getAttribute("terms_party_type").equals(TradePortalConstants.TRANSFEREE)) {
    	       termsPartyApplicant = termsPartyBeneficiary;
        	   termsPartyBeneficiary = termsPartyTemp;
	        } 
	        //Krishna IR-NLUI012860811 Begin 02/05/2008
	        /* Following one is commented out
	        else if (termsPartyTemp.getAttribute("terms_party_type").equals(TradePortalConstants.PAYER)) {
				termsPartyApplicant = termsPartyTemp;
			}
			*/
			//IR T36000020564 Rel 8.3 - Added condition when terms_party_type is 'Borrower'
		    else if (termsPartyTemp.getAttribute("terms_party_type").equals(TradePortalConstants.PAYER) ||
		    		termsPartyTemp.getAttribute("terms_party_type").equals(TradePortalConstants.BORROWER)) {
     	        termsPartyApplicant.setAttribute("terms_party_oid", termsPartyOid);
     	        termsPartyApplicant.getDataFromAppServer();
			}
		    else if (termsPartyTemp.getAttribute("terms_party_type").equals(TradePortalConstants.PAYEE) ||
		    	     termsPartyTemp.getAttribute("terms_party_type").equals(TradePortalConstants.MULTIPLE)) {
		    	     // Peter Ng IR INUJ012743663
				termsPartyBeneficiary.setAttribute("terms_party_oid", termsPartyOid);
     	        termsPartyBeneficiary.getDataFromAppServer();
			}			
			//Krishna IR-NLUI012860811 End 02/05/2008
  	  }
  }
}

%>