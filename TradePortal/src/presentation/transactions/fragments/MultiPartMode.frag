<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%
   /* This JSP handles all logic for the following page functionality:
 		1.) Determining if the page should be displayed as many parts or all as one part
		2.) If in multi-part mode, determining which part to display
		3.) Keeping the errors and warnings displayed from previous actions while viewing    
		    other parts.

       Two important variables are set in this JSP that are used after the inclusion of this
       JSP into the transaction page:
		whichPart - this is set to a constant that indicates the part that is selected
            errorsToStoreInSecureParms - a String of XML that is the /Error section of the
                                         last mediator call (in some situations).  This is
						     used to hold on to old errors.

       This JSP should be "<@include"d into the transaction pages JSPs.  By using the "whichPart"
       and "multiPartMode" variables later in the transaction page, the inclusion of other JSPs
       can be turned on or off.  

       In order for this JSP to work, the "MultiPartModeFormElements.jsp" must be "<@include"d later
       in the transaction page.  See that JSP to see where it must be included.

       To make the part navigation bar appear, jsp:include the file "PartNavigationBar.jsp" into
       the transaction page. 

       Because the part navigation links are actually submitting the page, several entries must be
       added as alternate forms in FormManager.xml.  For each part ("Part1", "Part2", etc), an entry 
       must be included.
   */

    // Determine if the user wants to view the page in multi page mode   
    boolean multiPartMode = userSession.getMultiPartMode();
    if ((instrumentType.equals(InstrumentType.DOM_PMT))||
    	 instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))	//IAZ IR-SRUK011902013 01/24/10
       multiPartMode = false;
    
    // Variable to hold the part that the user wishes to view
    String whichPart = "";

    // Indicator of wheter any errors or warnings exist in the 
    // doc cache from the last form submission
    boolean errorsOrWarningsExist = false;

    // Holds the errors that must be placed into the form
    // submission so that they will be kept around
    String errorsToStoreInSecureParms = "";

    // If the user doesn't want to use tab mode, there is nothing further to do
    if(multiPartMode)
     {
	    // True if the user just clicked on a new part
	    boolean justClickedNewPart;

	    // Get max error severity from the doc cache
	    String maxErrorSeverity = doc.getAttribute("/Error/maxerrorseverity");

	    // Determine if there are errors or warnings
	    errorsOrWarningsExist = (maxErrorSeverity != null) &&
	                            (maxErrorSeverity.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) > 0);

	    String buttonPressed;

	    if(isReadOnly)
           {
		    // If we are in read only mode, pressing the button links back to the
		    // same page, passing a parameter in the URL.  InstrumentNavigator takes
		    // this parameter and puts it on the session.
		    buttonPressed = (String) session.getAttribute(TradePortalConstants.READONLY_PART);
	     }
          else
           {
		    // If not in readonly mode, the button pressed is submitted as part of the form
		    buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
	     }

	    // If the button pressed was a part navigation...
	    if( (buttonPressed != null) &&
	        ( buttonPressed.equals(TradePortalConstants.PART_ONE) ||
	          buttonPressed.equals(TradePortalConstants.PART_TWO) ||
	          buttonPressed.equals(TradePortalConstants.PART_THREE) ||
	          buttonPressed.equals(TradePortalConstants.PART_FOUR) ) )
	     {
		  justClickedNewPart = true;
	
	        // User has selected a new part.  This is the part that now should be displayed.
	        // However, if there are any errors on trying to save the data(this could result from bad amount
	        // formats), then the page should go to the previous tab
	        if(!errorsOrWarningsExist)
	         {
	           // There are no errors from the last part navigation
	           whichPart = buttonPressed;
	         }
	        else
	         {
	           // There were errors from the last part navigation, so stay on the same part
		     whichPart = doc.getAttribute("/In/partNumber");
	         }
	     }
	    else
	     {
                // User is still in the same part they were in last time
                String partNumberFromXml = doc.getAttribute("/In/partNumber"); 
                String partNumberParm = request.getParameter("partNumber"); 
                justClickedNewPart = false;
                
                // Use the partNumber that was sent as part of the last submit if it exists 
                // in the xml. The part number will not exist the first time the user comes 
                // into the page or if the user is returning from the PO Details Entry Page
		if(partNumberFromXml == null)
	        {
                  // First check if the part number was passed in as a parameter. This parameter
                  // will only exist when the user has returned from the PO Details Entry Page
                  // on an Import DLC Issue transaction. Otherwise just default to Part1
                  if (!InstrumentServices.isBlank(partNumberParm))
                    whichPart = partNumberParm;
                  else
                    whichPart = TradePortalConstants.PART_ONE;
	        }
		else
	        {
                  // Set the part to the current part that the user is on.
                  whichPart = partNumberFromXml;
	        }
	     }

	    // If the user performed an action which generated error or warning messages, 
	    // then clicked on a different part, they should still see the errors and warnings
	    // from their previous action. 
	    // The exception to this is when errors are generated as a result of the user
	    // clicking the new part (because the system does a save when they click on a new part).  
	
	    if(justClickedNewPart && !errorsOrWarningsExist)
	     {
			// User clicked a new part and there were no errors as a result of clicking on the new part
			// If there are any errors from previous actions, we must display them     

			// Populate the error section of the XML with errors submitted in secure parameters
	      	// when the user clicked the new part
	 		String oldErrorString = doc.getAttribute("/In/lastActionErrors");
			if(oldErrorString != null)
			 {
	 			// Place the errors into default.doc
				DocumentHandler oldErrors = new DocumentHandler(oldErrorString, true);
	                  String maxOldErrorSeverity = oldErrors.getAttribute("/Error/maxerrorseverity");
	                  if ((maxOldErrorSeverity != null) &&
	                      (maxOldErrorSeverity.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) > 0) )
	                     {
					doc.setComponent("/", oldErrors);
					formMgr.storeInDocCache("default.doc", doc);
					errorsToStoreInSecureParms = oldErrorString;
				   }
			 }
	     }

	    // User did something other than clicking a new part
          // If there are errors or warnings, we need to store them off
          // For now, place them into a String variable.
          // MultiPartModeFormElements.jsp places these into secureParms
	    if(!justClickedNewPart && errorsOrWarningsExist)
	     {
	  	  errorsToStoreInSecureParms = doc.getFragment("/Error").toString();
	     }
     }
  %>
