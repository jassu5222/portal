<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
            Air Waybill Release Page - Shipment Details section

  Description:
    Contains HTML to create the Air Waybill Release Shipment Details section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-AIR-REL-ShipmentDetails.jsp" %>
*******************************************************************************
--%>

<%=widgetFactory.createTextField("CarrierNameFlightNumber",
					"AWB_SGTEE.CarrierNameFlight", terms.getFirstShipment()
							.getAttribute("carrier_name_flight_num"), "35",
					isReadOnly, false, false,"class='char30'", "", "inline")%>

<%=widgetFactory.createTextField("AirWaybillNumber",
					"AWB_SGTEE.AirWaybillNumber", terms.getFirstShipment()
							.getAttribute("air_waybill_num"), "40", isReadOnly,
					false, false,"class='char30'", "", "formItemWithIndent5 inline")%>

<div style="clear: both;"></div>
<%=widgetFactory.createWideSubsectionHeader(
					"AWB_SGTEE.GoodsDescription", isReadOnly, false, false, "")%>
<%
	options = ListBox.createOptionList(goodsDocList, "PHRASE_OID",
			"NAME", "", userSession.getSecretKey());
	defaultText = resMgr.getText("transaction.AddAPhrase",
			TradePortalConstants.TEXT_BUNDLE);
%>
<%=widgetFactory.createSelectField(
							"GoodsPhraseItem",
							"",
							defaultText,
							options,
							isReadOnly,
							false,
							false,
							"onChange="
									+ PhraseUtility
											.getPhraseChange(
													"/Terms/ShipmentTermsList/goods_description",
													"GoodsDescText",
													"6500",
													"document.forms[0].GoodsDescText"),
							"", "")%>
<%if(!isReadOnly){ %>
<%=widgetFactory.createTextArea("GoodsDescText", "", terms
					.getFirstShipment().getAttribute("goods_description"),
					isReadOnly, false, false, "style='min-height:125px ;' cols='128'", "", "")%>

	<%=widgetFactory.createHoverHelp("GoodsDescText", "AirwayBill.GoodsDescToolTip") %>	
<%}else{%>

<%=widgetFactory.createAutoResizeTextArea("GoodsDescText", "", terms
					.getFirstShipment().getAttribute("goods_description"),
					isReadOnly, false, false, "maxlength=6500 style='width: 600px;min-height:140px;'cols='128'", "", "")%>
<%}%>					