<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Approval to Pay Issue Page - Internal Instructions section

  Description:
    Contains HTML to create the Approval to Pay Issue Internal Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-ATP-ISS-InternalInstructions.jsp" %>
*******************************************************************************
--%>
<div class = "formItem">
<%= widgetFactory.createNote("ApprovalToPayIssue.IntInstrText") %>
</div>
<%= widgetFactory.createTextArea( "InternalInstructions", "", terms.getAttribute("internal_instructions"), isReadOnly, false,false, "rows='10' cols='100'","","") %>
