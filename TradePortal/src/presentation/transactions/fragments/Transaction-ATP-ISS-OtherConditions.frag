<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Approval to Pay Issue Page - Other Conditions section

  Description:
    Contains HTML to create the Approval to Pay Issue Other Conditions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-ATP-ISS-OtherConditions.jsp" %>
*******************************************************************************
--%>

	<%=widgetFactory.createWideSubsectionHeader("ApprovalToPayIssue.Invoices")%>
	
	<%= widgetFactory.createCheckboxField( "InvoiceOnlyInd", "ApprovalToPayIssue.AprovedInvOnly", terms.getAttribute("invoice_only_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", ""  ) %>
	
	<%= widgetFactory.createDateField( "InvoiceDueDate", "ApprovalToPayIssue.InvoiceDueDate", terms.getAttribute("invoice_due_date").toString(), isReadOnly, false, false,  "", dateWidgetOptions, "" ) %>
	
	<%-- SHR PR CR708 --%>
	<%-- %=widgetFactory.createTextArea("InvoiceDetailsText","",invoiceDetails,isReadOnly || isFromExpress, false, false, 
	"style='width:auto;' rows='10' cols='100'", "", "") %--%>
	<%-- DK IR T36000016040 Rel8.2 04/21/2013 Starts --%>
	<div class="formItem">
		<%=widgetFactory.createPOTextArea("InvoiceDetailsText",invoiceDetails,"78","10", "POLineItems", isReadOnly || isFromExpress, 
			"", "OFF", true) %>
			</div>
			<%-- DK IR T36000016040 Rel8.2 04/21/2013 Ends --%>
	<%
       //     userOrgATPInvoiceIndicator     = userSession.getOrgATPInvoiceIndicator();
           
      // String userSecurityRights  = userSession.getSecurityRights();
	   boolean canProcessInvoiceForATP = SecurityAccess.canProcessATPInvoice(userSecurityRights, InstrumentType.APPROVAL_TO_PAY);
           transactionOid  = transaction.getAttribute("transaction_oid");
           String            invSellerName     = "";
           String            invCurrency            = "";
           String            invOid          = "";
           String invDueDate          = "";
           boolean hasInvoiceItems =false;
           Vector invoiceItems = null;
           if ((canProcessInvoiceForATP ||
                (userSession.hasSavedUserSession() && 
                (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)))))
           {
 
        	 //check if already Invs are associated
          		    StringBuffer invSqlQuery = new StringBuffer();
          		 	invSqlQuery.append("select A_UPLOAD_DEFINITION_OID, seller_name, currency,(case when payment_date is null then due_date else payment_date end) as pay_date");
          			invSqlQuery.append(" from invoices_summary_data where a_terms_oid = ?");	 
          			//jgadela  R90 IR T36000026319 - SQL FIX
					Object sqlParamsTerms[] = new Object[1];
					sqlParamsTerms[0] = termsOid;					
                    DocumentHandler   invDoc = DatabaseQueryBean.getXmlResultSet(invSqlQuery.toString(), true, sqlParamsTerms);
                   if (invDoc != null)
                     {
                	   hasInvoiceItems = true;
                	   invoiceItems                  = invDoc.getFragments("/ResultSetRecord");
                	   DocumentHandler invItemDoc    = (DocumentHandler) invoiceItems.elementAt(0);
                       invOid = invItemDoc.getAttribute("/A_UPLOAD_DEFINITION_OID");
                       invSellerName     = invItemDoc.getAttribute("/SELLER_NAME");
                       invCurrency            = invItemDoc.getAttribute("/CURRENCY");
                       invDueDate            = invItemDoc.getAttribute("/PAY_DATE");
                      }
          	     }

              // Use the transaction's currency if a po line item currency does not exist
              if (InstrumentServices.isBlank(invCurrency))
                invCurrency = currency;

              // Store po line item data in secure parms which will be used by the pages
              // which add/remove po line items
              secureParms.put("addInv-uploadInvDefOid", invOid);
              secureParms.put("addInv-sellerName", invSellerName);
              secureParms.put("addInv-invCurrency", invCurrency);
              secureParms.put("addInv-termsOid", terms.getAttribute("terms_oid"));
              secureParms.put("addInv-hasInvoiceItems", String.valueOf(hasInvoiceItems));
              secureParms.put("addInv-dueDate", invDueDate); //SHR IR T36000005856
              if (canProcessInvoiceForATP  && !hasInvoiceItems && !isTemplate)
              {   
        %>
       
			<div class = "formItem">
				<button data-dojo-type="dijit.form.Button" type="submit" name="AddInvoiceButton" id="AddInvoiceButton">
		          <%=resMgr.getText("Atp.AssignInvoices",TradePortalConstants.TEXT_BUNDLE)%>	
		          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			      setButtonPressed('AddInvoiceButton', '0');
                  document.forms[0].submit();
				  </script>	            
		       </button>
		     </div>
		      <% } 
              if (canProcessInvoiceForATP && !isTemplate && hasInvoiceItems)
              {
		           %>
		   <div class = "formItem">                   
		    <% if(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE.equals(transactionStatus) ||
		    		TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equals(transactionStatus) ||
		    		TradePortalConstants.TRANS_STATUS_AUTHORIZED.equals(transactionStatus)){ %>
           		<button data-dojo-type="dijit.form.Button" type="submit" name="ViewInvoiceButton" id="ViewInvoiceButton" >
		 		 <%=resMgr.getText("Atp.ViewAssignInvoices",TradePortalConstants.TEXT_BUNDLE)%>	
		 		 <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			      setButtonPressed('ViewInvoiceButton', '0');
                  document.forms[0].submit();
				  </script>	 	 		 
		   		</button>
				<% } else { %>
				<button data-dojo-type="dijit.form.Button" type="submit" name="ViewInvoiceButton" id="ViewInvoiceButton" >
		 		 <%=resMgr.getText("Atp.ViewAssignInvoices",TradePortalConstants.TEXT_BUNDLE)%>	
		 		 <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			      setButtonPressed('AddInvoicesButton', '0');
                  document.forms[0].submit();
				  </script>	 	 		 
		   		</button>
				<% } %>
		   		<%= widgetFactory.createHoverHelp("ViewInvoiceButton","Atp.ViewAssignInvoices")%>
		   		 <%
  			    if (canProcessInvoiceForATP && 
                        (!(isReadOnly || isFromExpress)) && !isTemplate)
          		{										
                               %>
           		 <button data-dojo-type="dijit.form.Button" type="submit" name="AddMoreInvoiceButton" id="AddMoreInvoiceButton" >
		 		 <%=resMgr.getText("Atp.AssignInvoices",TradePortalConstants.TEXT_BUNDLE)%>
		 		  <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			      setButtonPressed('AddInvoiceButton', '0');
                  document.forms[0].submit();
				  </script>	
		    	</button>
		    	<%= widgetFactory.createHoverHelp("AddMoreInvoiceButton","Atp.AssignInvoices")%>
            
            	<button data-dojo-type="dijit.form.Button" type="submit" name="RemoveInvoiceButton" id="RemoveInvoiceButton" >
		 		 <%=resMgr.getText("Atp.RemoveInvoices",TradePortalConstants.TEXT_BUNDLE)%>
		 		  <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			      setButtonPressed('RemoveInvoiceButton', '0');
                  document.forms[0].submit();
				  </script>	
		   		</button>  
		   		<%= widgetFactory.createHoverHelp("RemoveInvoiceButton","Atp.RemoveInvoices")%>   
		   		<%-- Leelavathi IR#T36000015029 Rel-8.2 21/03/2013 Begin --%>          
            <%--</div>                   
                               
          <%
          		 }
		    }
	      %> --%>
	      <%
          		 }
		   		 %>
		   		  </div> 
		   	<%
		    }
	      %>
	<%-- Leelavathi IR#T36000015029 Rel-8.2 21/03/2013 End --%>  
	<%-- SHR PR CR708 --%>
	<%=widgetFactory.createWideSubsectionHeader("ApprovalToPayIssue.AdditionalConditions")%>
	
	<%
	if (isReadOnly || isFromExpress) {
        out.println("&nbsp;");
      } else {
          options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        defaultText1 = resMgr.getText("transaction.SelectPhrase",
                                     TradePortalConstants.TEXT_BUNDLE);
	%>

	<% if(isExpressTemplate) {%>
		<span class="hashMark">#</span>
	&nbsp;&nbsp;
	<%=widgetFactory.createSelectField("AddlDocsPhraseItem1","", defaultText1, options,isReadOnly || isFromExpress, false, false,  
										"onChange=" + PhraseUtility.getPhraseChange("/Terms/additional_conditions","AddlConditionsText", "5000","document.forms[0].AddlConditionsText"), "", "none")%>
	<%}else{%>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%=widgetFactory.createSelectField("AddlDocsPhraseItem1","", defaultText1, options,isReadOnly || isFromExpress, false, false,  
										"onChange=" + PhraseUtility.getPhraseChange("/Terms/additional_conditions","AddlConditionsText", "5000","document.forms[0].AddlConditionsText"), "", "none")%>
	<%}%>																			
	<br>
	<br>																			
	<%
      }
      if(!isReadOnly){
	%>
	<%=widgetFactory.createTextArea("AddlConditionsText","",terms.getAttribute("additional_conditions"),isReadOnly || isFromExpress, false, false, "rows='10' cols='100'", "", "") %>
	<%= widgetFactory.createHoverHelp("AddlConditionsText","InstrumentIssue.PlaceHolderOtherAddlConditions")%>
	<%}else{%>
	<%=widgetFactory.createAutoResizeTextArea("AddlConditionsText","",terms.getAttribute("additional_conditions"),isReadOnly || isFromExpress, false, false, "style='width:600px;min-height:140px;' cols='100'", "", "") %>
	<%}%>
