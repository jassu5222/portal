<%-- IAZ CM-451 11/25/08 Add/Modify/Delete Payee should be available in !Read-Only Mode --%>
<%
	BigDecimal totalPaymentsAmount = BigDecimal.ZERO; 
	String totalPaymentsAmountStr = "0.00";
	
	
	//IAZ: get payee accout number from current payee, if loaded     
	payeeAcctNum = currentDomesticPayment
			.getAttribute("payee_account_number");
	if (payeeAcctNum == null) {
		payeeAcctNum = "";
	}
%>

<div>
<script>

function setBankChargesType(type) {
console.log('entered setBankChargesType');
document.forms[0].BankChargesType.value = type;

console.log('BankChargesType='+document.forms[0].BankChargesType.value);
}

function setPayerType(payerType) {
	<%-- var DomesticPaymentchkBox = registry.byId('DomesticPaymentOID'); --%>
	
	dijit.byId("numOfPayer").value = payerType;
	console.log('payer type set to ' + payerType);
}
<%-- Jyoti 05-09-2012 IR4505 --%>
var BankChargesType1chk=false;
var BankChargesType2chk=false;
var BankChargesType3chk=false;
function onloadPayer(){
 BankChargesType1chk= dijit.byId('BankChargesType1').checked; 
 BankChargesType2chk= dijit.byId('BankChargesType2').checked;
 BankChargesType3chk= dijit.byId('BankChargesType3').checked;
 	 if(BankChargesType1chk==false && BankChargesType2chk==false && BankChargesType3chk==false  ){
		 dijit.byId('BankChargesType1').set('checked',true);
		 setBankChargesType('U');
	 }
 
}
function clearAllDebitFields () {
		clearPayerBank();		
		dijit.byId("addPayerButton").set('disabled',true);				
		require(["dijit/registry"],function(registry){
		var btn=dijit.byId('UpdatePayerButton');
		btn.setLabel('Save Payer');
	});
	}

<%-- IR4505 ends --%>
</script>
</div>
<%! String domestic_payment_oid=""; %>	 		
<%	
	domestic_payment_oid= currentDomesticPayment.getAttribute("domestic_payment_oid");		
	if (domestic_payment_oid.equals("") || numberOfDomPmts > 0) {
	if (!isReadOnly){%>   <%-- A List of All the Payees --%>
	
			<%if (!domestic_payment_oid.equals("")) {%>
			<%--	Naveen 07-Sep-2012 IR-T36000004772. Added the below Div tag and CSS class for right alignment of Add Payer button	--%>
					<div class="customizationAction">	
						<button data-dojo-type="dijit.form.Button" onClick='clearAllDebitFields();' id='addPayerButton'>Add Payer</button>
						<%= widgetFactory.createHoverHelp("addPayerButton","AddPayerButton") %>
					</div>
			<%}
			%>
	<%}
	%>
	
<%if (numberOfDomPmts ==0) { %>
		<div><table>
	<%}
	%>

<%if (numberOfDomPmts > 0) { %>
<div class="scrollableTable">

	<table border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable" >
	<tr>
		<tr>
		<%-- Jyoti 30-08-2012 IR4828 Removed  <%if (!isReadOnly){ %>--%>
		<th width="1%" nowrap>&nbsp;</th>
	
		<th width="6%%" nowrap align="left"><%=resMgr.getText("FundsTransferRequest.AccountNumber",
								TradePortalConstants.TEXT_BUNDLE)%></th>
		<th width="6%%" nowrap ><%=resMgr.getText("DirectDebitInstruction.PayerName",
								TradePortalConstants.TEXT_BUNDLE)%></th>
		<%-- IAZ CR-483B 08/13/09 Begin: New Currency and Payment Method Columns per CR-483 design --%>
		<th width="4%" nowrap><%=resMgr.getText("FundsTransferRequest.Currency",
								TradePortalConstants.TEXT_BUNDLE)%></th>
		<th width="4%" nowrap ><%=resMgr.getText("FundsTransferRequest.Amount",
								TradePortalConstants.TEXT_BUNDLE)%></th>
		<th width="10%" nowrap><%=resMgr.getText(
								"DirectDebitInstruction.PayerBankName",
								TradePortalConstants.TEXT_BUNDLE)%></th>
		<th width="5%" nowrap><%=resMgr.getText(
								"DirectDebitInstruction.PayerBankBranch",
								TradePortalConstants.TEXT_BUNDLE)%></th>
		<th width="5%" nowrap><%=resMgr.getText("DirectDebitInstruction.ValueDate",
								TradePortalConstants.TEXT_BUNDLE)%></th>
		<%
		if (!isReadOnly) {
		%>
		<th width="5%" nowrap>Action</th>
		<%
		}	
		%>
	</tr>

	<%
		for (int j = 0; j < numberOfDomPmts; j++) {
				enteredDomPmtDoc = (DocumentHandler) enteredDomPmtList
						.elementAt(j);						
				boolean isPayeeSelected = enteredDomPmtDoc.getAttribute(
						"/DOMESTIC_PAYMENT_OID").equals(
						currentDomesticPayment
								.getAttribute("domestic_payment_oid"));
				if ((j % 2) == 0) {
					classAttribute = "";
				} else {
					classAttribute = "class='ColorGrey'";
				}
	%>
	<tr>
<%-- Jyoti 30-08-2012 IR4828 Removed <% if (!isReadOnly) {%> and modified parameter from redonly to false --%>
		<td width="5%"><%= widgetFactory.createRadioButtonField(
			"DomesticPaymentOID",
			"DomesticPaymentOID"+j,	
			"",
			enteredDomPmtDoc.getAttribute("/DOMESTIC_PAYMENT_OID"),
			isPayeeSelected,
			false,
			"onClick='setButtonPressed(\"ModifyPayee\", 0);document.forms[0].submit()'",
			"")%>
			</td>		
		<td <%=classAttribute%> align="left" valign="middle">
		<p class="ListText">
		<%
					defaultValue = enteredDomPmtDoc
							.getAttribute("/PAYEE_ACCOUNT");
					defaultValue = StringFunction.xssCharsToHtml(defaultValue);
					out.println(defaultValue);
		%>
		</p>
		</td>

		<td <%=classAttribute%> align="left" valign="middle"><span
			class="ListText"> <%
 	{
 				defaultValue = enteredDomPmtDoc
 						.getAttribute("/PAYEE_NAME");
 				defaultValue = StringFunction
 						.xssCharsToHtml(defaultValue);
 			}
 			out.println(defaultValue);
 %> </span></td>

		<%-- IAZ CR-483B 08/13/09 Begin: New Currency and Pmt Method Columns per CR-483 design --%>
		<td <%=classAttribute%> align="left" valign="middle"><span
			class="ListText"> <%
 	out.println(transactionCCY);
 %> </span></td>

		<td <%=classAttribute%> align="right" valign="middle"><span
			class="ListText"> <%
 	{
 				String currentCurrency = enteredDomPmtDoc
 						.getAttribute("/CURRENCY_CODE");
 				// 17-02--2013 jgadela,Fixed Showing numbers after decimal by passing correct currency code, issue found by Ann 
 				// defaultValue = TPCurrencyUtility.getDisplayAmount(enteredDomPmtDoc.getAttribute("/PAYEE_AMOUNT"), currentCurrency, loginLocale);
 				defaultValue = TPCurrencyUtility.getDisplayAmount(enteredDomPmtDoc.getAttribute("/PAYEE_AMOUNT"), transactionCCY, loginLocale);
 			}

 			out.print(defaultValue);

 			try {
 				BigDecimal checkPmtValue = new BigDecimal(
 						enteredDomPmtDoc.getAttribute("/PAYEE_AMOUNT"));
 				totalPaymentsAmount = totalPaymentsAmount
 						.add(checkPmtValue);
 			} catch (Exception any_e) {
 				//skip this till next
 			}
 %> </span></td>
		<td <%=classAttribute%> align="left" valign="middle"><span
			class="ListText"> <%
 	{
 				defaultValue = enteredDomPmtDoc
 						.getAttribute("/PAYEE_BANK_NAME");
 				defaultValue = StringFunction
 						.xssCharsToHtml(defaultValue);

 				out.print(defaultValue); 			
 	}

 %> </span></td>
		<%-- IAZ CR-483B 08/13/09 End: New Currency and Pmt Method Columns per CR-483 design --%>
		<td <%=classAttribute%> align="left" valign="middle"><span
			class="ListText"> <%
 	{
 				defaultValue = enteredDomPmtDoc
 						.getAttribute("/PAYEE_BANK_CODE");
 				defaultValue = StringFunction
 						.xssCharsToHtml(defaultValue);
 			}

 			out.print(defaultValue);
 %> </span></td>
		<td <%=classAttribute%> align="left" valign="middle"><span
			class="ListText"> <%

 	{
 				//Add Value date
 				defaultValue = TPDateTimeUtility.formatDate(enteredDomPmtDoc.getAttribute("/VALUE_DATE"), 
                             									TPDateTimeUtility.LONG,
                             									userLocale);
 				defaultValue = StringFunction
 						.xssCharsToHtml(defaultValue);
 			}

 			out.print(defaultValue);			
 %> </span></td>
    <% 
	if (!isReadOnly) {
	%>
	<td>
	<%--	Naveen 07-Sep-2012 IR-T36000004772. Modified the link value as- Delete from the earlier value of- Del	--%>
    <a href="javascript:document.forms[0].submit()" name="DeletePayee" 
    onClick="setButtonPressed('DeletePayee', 0); setButtonPressed('Delete', 0); setDeletionId ('DomesticPaymentOID'+<%=j%>); return confirmDelete()">
	Delete</a>
    </td>
	<%
	}
	%>
	</tr>
	<%
		}

			totalPaymentsAmountStr = TPCurrencyUtility.getDisplayAmount(
					totalPaymentsAmount.toString(),
					transactionCCY, loginLocale); 

			DocumentHandler checkError = doc.getComponent("/Error");
			if (checkError != null) {
				String checkErrorStr = checkError.toString();
				if ((checkErrorStr != null)
						&& (checkErrorStr
								.indexOf(TradePortalConstants.INVALID_CURRENCY_FORMAT)) != -1) {
					totalPaymentsAmountStr = totalPaymentsAmount.toString();
				}
			}
		} else {
		}
	%>
	</table>
	</div>
<%}%>





<%-- IAZ: Store various hidden fields to be send to the server --%> 
<%--      Some of these fields are needed by both Terms and Domestic Payments --%>

<input type="hidden" name="DisplayTotalPaymentsAmount"
	value="<%=totalPaymentsAmountStr%>"> 
<input type="hidden"
	name="TotalPaymentsAmount" value="<%=totalPaymentsAmount%>"> 
	
<input type="hidden" name="PayerRefNum"
	value='<%=terms.getAttribute("reference_number")%>'> 
				   
<input type="hidden"
	name="TransactionCurrency" value="<%=transactionCCY%>">  

<input
	type="hidden" name="TransactionAmount"
	value="<%=totalPaymentsAmount.toString()%>"> 

<%-- IAZ CM-451 11/25/08 Regulary, Transaction Detail Page does not have a delete
                         button so confimrDelete() is not there.  Add for our specific
                         Domenstic Payment Payee case --%> 
<script	LANGUAGE="JavaScript" type="text/javascript">
<%if (!isTemplate) 
{
	out.println("function confirmDelete()");
	out.println("{ var msg = 'Are you sure you want to delete this item?'");
	out.println("if(!confirm(msg)) {formSubmitted = false; return false;}");
	out.println("return true;}");

}%>
</script> 

<script LANGUAGE="JavaScript" type="text/javascript">
  
 function setDeletionId (deletionId) {
 dijit.byId(deletionId).set('checked','true');
 console.log(deletionId)
 }


var domestic_payment_oid ='<%=domestic_payment_oid%>';

<%-- Jyoti 09-Oct-2012 IR 4505 --%>
  function previousPayeeValue() {
  	  require(["dijit/registry"],function(registry){
	  if(!domestic_payment_oid==""){
		  dijit.byId('EnteredAccount').set('value','<%=payeeAcctNum%>');
		  dijit.byId('PayeeName').set('value','<%=currentDomesticPayment.getAttribute("payee_name")%>');
		  dijit.byId('PayeeAddrL1').set('value','<%=currentDomesticPayment.getAttribute("payee_address_line_1")%>');
		  dijit.byId('PayeeAddrL2').set('value','<%=currentDomesticPayment.getAttribute("payee_address_line_2")%>');
		  dijit.byId('PayeeAddrL3').set('value','<%=currentDomesticPayment.getAttribute("payee_address_line_3")%>');
		  dijit.byId('PayeeAddrL4').set('value','<%=currentDomesticPayment.getAttribute("payee_address_line_4")%>');
		  dijit.byId('Country').set('value','<%=currentDomesticPayment.getAttribute("country")%>');
		  dijit.byId('payeeFaxNumber').set('value','<%=currentDomesticPayment.getAttribute("payee_fax_number")%>');
		  dijit.byId('payeeEmail').set('value','<%=currentDomesticPayment.getAttribute("payee_email")%>');
		  dijit.byId('PayeeBankBranchCode').set('value','<%=currentDomesticPayment.getAttribute("payee_bank_code")%>');
		  var payee_bank_name= '<%=currentDomesticPayment.getAttribute("payee_bank_name")%>';
		  var payee_branch_name = '<%=currentDomesticPayment.getAttribute("payee_branch_name")%>';
		  var address_line_1 = '<%=currentDomesticPayment.getAttribute("address_line_1")%>';
		  var address_line_2 = '<%=currentDomesticPayment.getAttribute("address_line_2")%>';
		  var address_line_3 = '<%=currentDomesticPayment.getAttribute("address_line_3")%>';
		  var countryCode='<%=countryCode%>';
		  var abc ='<%=currentDomesticPayment.getAttribute("payee_bank_country")%>';
		  
		  dijit.byId('DomesticPaymentAmount').set('value','<%=displayPaymentAmount%>');
		  dijit.byId('PayerBankData').set('value',payee_bank_name+'\n'+payee_branch_name+'\n'+address_line_1 +'\n'+address_line_2+'\n'+address_line_3+'\n'+countryCode);
		  
		  if(BankChargesType1chk==true)
		  dijit.byId('BankChargesType1').set('checked',true);
		  else if(BankChargesType2chk==true)
		  dijit.byId('BankChargesType2').set('checked',true);
		  else if(BankChargesType3chk==true)
		  dijit.byId('BankChargesType3').set('checked',true);
		  <%-- dijit.byId('ValueDate').set('value','<%=currentDomesticPayment.getAttribute("value_date")%>');		   --%>
		  dijit.byId('PayerReference').set('value','<%=currentDomesticPayment.getAttribute("customer_reference")%>');
		  dijit.byId('PaymentDetail').set('value','<%=currentDomesticPayment.getAttribute("payee_description")%>');
		   
	  }else{
		clearPayerBank();
		}
	  });
	
  }
  
  <%-- IR4505 Ends --%>

<%-- Jyoti 05-09-2012 IR 4505 --%>
  function clearPayerBank() {
  require(["dijit/registry"],function(registry){
  var enteredAccount =dijit.byId('EnteredAccount');
  enteredAccount.set("value","");	
  var PayeeName =dijit.byId('PayeeName');
  PayeeName.set("value","");
  dijit.byId('PayeeAddrL1').set('value','');
  dijit.byId('PayeeAddrL2').set('value','');
  dijit.byId('PayeeAddrL3').set('value','');
  dijit.byId('PayeeAddrL4').set('value','');
  
  var Country =dijit.byId('Country');
  Country.set("value","");
  dijit.byId('payeeFaxNumber').set('value','');
  dijit.byId('payeeEmail').set('value','');
  dijit.byId('PayeeBankBranchCode').set('value','');
  dijit.byId('PayerBankData').set('value','');
  dijit.byId('BankChargesType1').set('checked',true);
  
  dijit.byId('BankChargesType2').set('checked',false);
  dijit.byId('BankChargesType3').set('checked',false);
  dijit.byId('DomesticPaymentAmount').set('value','');
  dijit.byId('PaymentDetail').set('value','');
  dijit.byId('PayerReference').set('value','');
  dijit.byId('ValueDate').set('value','');
  var DomesticPaymentOID="DomesticPaymentOID";
  for(var i=0;i< <%=numberOfDomPmts%>;i++){
  dijit.byId('DomesticPaymentOID'+i).set('checked',false);
  }   
  domestic_payment_oid = "";
  console.log('payer bank cleared');
  });
  }
  
  <%-- IR4505 Ends --%>
  
  function clearOrderingPartyBank() {
  <%--  jgadela - R92 IR T36000036104. --%>
  document.forms[0].OrderingPartyBankName.value='';
  document.forms[0].OrderingPartyBankCode.value='';
  document.forms[0].PayeeBankData.value='';
  }
  
	<%-- Komal M 15-02-2013 PR ANZ Incident Report_Issue No.803 --%>
	  function erasePayerBank() {
	  require(["dijit/registry"],function(registry){
	  
	  	dijit.byId('PayeeBankBranchCode').set('value','');
	  	dijit.byId('PayerBankData').set('value','');
	  });
	  }
	<%-- Komal M 15-02-2013 PR ANZ Incident Report_Issue No.803   --%>

  
   function updateDMPaymentDate() {

	var index = document.forms[0].PaymentDateDay.selectedIndex;
	var day = document.forms[0].PaymentDateDay.options[index].value;
	
	index = document.forms[0].PaymentDateMonth.selectedIndex;
	var month = document.forms[0].PaymentDateMonth.options[index].value;
	
	index = document.forms[0].PaymentDateYear.selectedIndex;
	var year = document.forms[0].PaymentDateYear.options[index].value;
		    
	if (day != '-1' || month != '-1' || year != '-1') 
	{
             document.forms[0].payment_date.value = month + '/' + day + '/' + year + ' 00:00:00';
	}
	document.getElementById("ValueDate").innerHTML = ""; 
  }
	
 function updatePaymentCCY() {
	  require(["dojo/ready","dojo/dom","dijit/registry", "dojo/on"], function(ready,dom,registry, on){
		ready(function(){
		var TransactionCurrencyDD = registry.byId('TransactionCurrencyDD');
	  	registry.byId("TransactionCurrencyDisplay").set('value',TransactionCurrencyDD);
		  	if(registry.byId("TransactionCurrency")){
		  		registry.byId("TransactionCurrency").set('value',TransactionCurrencyDD);
		  	}
		 document.forms[0].TransactionCurrency.value = TransactionCurrencyDD; 
			
	    });
	    });
  }
  
  function updatePayerAccCurrency() {
 	 
	  require(["dojo/ready","dojo/dom","dijit/registry", "dojo/on"], function(ready,dom,registry,  on){
		ready(function(){
	    var account = dijit.byId('CreditAccount').attr('displayedValue');
	    var i = 0;
	    var currency ="";
	    var aindex = account.indexOf("(");
	       if (aindex != -1)
	    	currency = account.substring(aindex+1, aindex+4);
			registry.byId("TransactionCurrencyDD").set('value',currency);
			registry.byId("TransactionCurrencyDisplay").set('value',currency);
				if(registry.byId("TransactionCurrency")){
					registry.byId("TransactionCurrency").set('value',currency);
				}
			<%-- dojo.byId("ValueDate").innerHTML="";  --%>
			document.forms[0].TransactionCurrency.value = currency;
	    });
	    });
  }    
    
    
  function updateBankCode() {
	  
	 
	  document.forms[0].PayeeBankCode.value =    	  
	    	  document.forms[0].PayeeBankBranchCode.value;
	  }

  
    
  function updateTermsDescription() {
  
    document.forms[0].PayerRefNum.value =    	  
    	  document.forms[0].PayerReference.value;
  }

function updatePayeePartyName()
{
	document.forms[0].PayeeTermsPartyName.value = 
          dijit.byId('PayeeName').value.substring(0,35);
        document.forms[0].PayerPartyContactName.value = 
          dijit.byId('PayeeName').value.substring(0,35);
}
function updatePayeeAddressLine()
{
	document.forms[0].FirstTermsPartyAddress1.value = 
          dijit.byId('PayeeAddrL1').value;
    document.forms[0].FirstTermsPartyAddress2.value = 
          dijit.byId('PayeeAddrL2').value;
    document.forms[0].FirstTermsPartyAddress3.value = 
          dijit.byId('PayeeAddrL3').value;
    document.forms[0].FirstTermsPartyAddress4.value = 
          dijit.byId('PayeeAddrL4').value;
    document.forms[0].FirstTermsPartyCountry.value = 
          dijit.byId('Country').value;

}

function selectOtherCharges()
{
  var size = document.forms[0].BankChargesType.length;
  var counter = size - 1
  while(counter >=0)
  {
  	if (document.forms[0].BankChargesType[counter].value == '<%=TradePortalConstants.CHARGE_OTHER%>')
  	{
  		document.forms[0].BankChargesType[counter].checked = true;
  	    break;
  	}
  	counter--;	
  }
}
function updateValueDate()
{
	  document.getElementById("ValueDate").innerHTML = "";        
}
</script></p>
</p>

	<%= widgetFactory.createWideSubsectionHeader("DirectDebitInstruction.details") %>
	
	<div class="columnLeft">
	<%
	String initialBankCharges = "";
	if (bankChargesType.equals(TradePortalConstants.CHARGE_OUR)) {
	initialBankCharges = "U";
	} else if (bankChargesType.equals(TradePortalConstants.CHARGE_PAYER)) {
	initialBankCharges = "P";
	} else if (bankChargesType.equals(TradePortalConstants.CHARGE_SHARED)) {
	initialBankCharges = "S";
	}
	%>
	
	<input type=hidden name="BankChargesType" value='<%=initialBankCharges%>'>
	<span class="formItem">
	<%if(!isTemplate){%>
	<span class="asterisk">*</span><%}%>
<%=resMgr.getText("DirectDebitInstruction.Charges",TradePortalConstants.TEXT_BUNDLE)%></span><br/>
	<%--= widgetFactory.createLabel( "", "DirectDebitInstruction.Charges", false,!isTemplate,false,"")---%>
		<div class="formItem">
		<%= widgetFactory.createRadioButtonField( "Charges", "BankChargesType1", "DirectDebitInstruction.OursCharges", 
		TradePortalConstants.CHARGE_OUR, bankChargesType.equals(TradePortalConstants.CHARGE_OUR), isReadOnly,
		 "onClick='setBankChargesType(\"U\");'", "") %>
		<br>
		<%= widgetFactory.createRadioButtonField( "Charges", "BankChargesType2", "DirectDebitInstruction.Payer", 
		TradePortalConstants.CHARGE_PAYER, bankChargesType.equals(TradePortalConstants.CHARGE_PAYER),  isReadOnly, 
		"onClick='setBankChargesType(\"P\");'", "") %>
		<br>
		<%= widgetFactory.createRadioButtonField( "Charges", "BankChargesType3", "DirectDebitInstruction.SharedCharges", 
		TradePortalConstants.CHARGE_SHARED, bankChargesType.equals(TradePortalConstants.CHARGE_SHARED), isReadOnly, 
		"onClick='setBankChargesType(\"S\");'", "") %>
	
		</div> <%-- close formItem --%>
	<input type=hidden name=PaymentMethodType value='<%=InstrumentType.DIRECT_DEBIT_INSTRUCTION %>'>
<%--	Naveen 13-August-2012 IR-T36000004481. Modified the height of the below TextArea using style attribute		--%>	
				<%
					String paymentDetailText = currentDomesticPayment
							.getAttribute("payee_description");
					if (!InstrumentServices.isBlank(paymentDetailText)
							&& (paymentDetailText.equals("null")))
						paymentDetailText = "";
						out.println(widgetFactory.createTextArea( "PaymentDetail", "DomesticPaymentRequest.PaymentDetails", paymentDetailText,
		isReadOnly || isFromExpress, false, isFromExpress, "style=\"min-height:40px;\"","",""));
				%>
		</div> <%-- closes leftColumn --%>
 <div class="columnRight">		
				<input type="hidden"
					name='PayeeBankTermsPartyType'
					value="<%=TradePortalConstants.PAYEE_BANK%>">
				<div>	
				
<div class="formItem inline" >
<label for="TransactionCurrencyDisplay">Currency</label><br>
<input data-dojo-type="dijit.form.TextBox" name="TransactionCurrencyDisplay" readonly="true" 
id="TransactionCurrencyDisplay" maxLength="5" class="char5" required="false" value="<%=transactionCCYDisplay%>">
</input>
</div>

    <%= widgetFactory.createAmountField( "DomesticPaymentAmount", "FundsTransferRequest.Amount", displayPaymentAmount, 
  	 transactionCCY, isReadOnly, !isTemplate, isFromExpress, "style=\"width:10em;\"", "", "inline") %>						

<div class="formItem inline" >
<%
String valuedDate=currentDomesticPayment.getAttribute("value_date");

SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

if (!valuedDate.equals(null) && !valuedDate.equals("")){
Date date = fmt.parse(valuedDate);
valuedDate = sdf.format(date);
}

%>
<label for="ValueDate">Value Date</label><br>
<input data-dojo-type="dijit.form.TextBox" name="ValueDate" readonly="true" 
<%-- Jyoti 11-09-2012 IR4748 below added --%>
id="ValueDate" maxLength="5" class="char10" required="false" 
value="<%=valuedDate%>">
</input>
</div>
<div style="clear:both"></div>
			</div>
<%--	Naveen 13-August-2012 IR-T36000004481. Modified the width of the below TextField		--%>			
<%= widgetFactory.createTextField( "PayerReference","DirectDebitInstruction.BOReference",
			currentDomesticPayment.getAttribute("customer_reference"), "20", isReadOnly, false, isFromExpress, 
									"onChange='updateTermsDescription();'",  "", "")%>
	</div> <%-- closes cloumnRight --%>
	<div style="clear:both"></div>		
	<%= widgetFactory.createWideSubsectionHeader("DirectDebitInstruction.PayerDetails") %>	
	<%
			String ident = "EnteredAccount,PayeeName,PayeeAddrL1,PayeeAddrL2,PayeeAddrL3,PayeeAddrL4,Country,payeeFaxNumber,payeeEmail";
		    String sect = "orderPayer";

			String payerSearchHtml = widgetFactory.createPartySearchButton(ident,sect,false,TradePortalConstants.PAYER,false);
	%>
<div class="columnLeft">
<%= widgetFactory.createSubsectionHeader( "DirectDebitInstruction.Payer",
isReadOnly, false, false, payerSearchHtml) %>

    <div id='payerAccountsData'>
                
				<%
				  String extTag="";  
				  if (displayMultipleAccounts) {
				   // TODO: Fix this radiobutton- Bhaskar
				  
				  out.println(widgetFactory.createRadioButtonField( "AccountSelectType", "AccountSelectType1", "", 
		"U", !accountSet.contains(payeeAcctNum), isReadOnly, 
		"onclick=\"javascript:setAccount('EnteredAccount');\"", ""));
					
					/*
					out.println(InputField.createRadioButtonField("AccountSelectType", TradePortalConstants.USER_ENTERED_ACCT, "",!accountSet.contains(payeeAcctNum), "ListText", "", isReadOnly,"onClick=\"javascript:setAccount('EnteredAccount');\"\""));
					*/
					extTag = "onchange=\"javascript:setAccountSelectType('EnteredAccount','0');\"";			
				  } 
   	              else {
		        	   extTag = "onChange=\"javascript:setAccount('EnteredAccount');\"\"";
		           }
		           
		           if (accountSet != null && accountSet.contains(payeeAcctNum)) {
		        	   acctNo = "";
		           }
		           else {
		        	   acctNo = payeeAcctNum;
		           }  
                 %>
<%--	Naveen 13-August-2012 IR-T36000004481. Modified the width of the below Text field		--%>                 
                 <%= widgetFactory.createTextField( "EnteredAccount", "DirectDebitInstruction.PayerAccountNumber", 
acctNo, "30", isReadOnly, !isTemplate, isFromExpress, extTag, "", "") %>
				</div>
				  
				  <%if (displayMultipleAccounts) {
				  // TODO: Fix this radiobutton- Bhaskar
				   
	              //  defaultText = resMgr.getText("DomesticPaymentRequest.selectAccountNumber",  TradePortalConstants.TEXT_BUNDLE);
	                out.println(widgetFactory.createRadioButtonField( "AccountSelectType", "", "", 
		"NU", accountSet.contains(payeeAcctNum), isReadOnly, 
		"onclick=\"javscript:setAccount('SelectedAccount');\"", ""));
					
					/*
				     out.println(InputField.createRadioButtonField("AccountSelectType", "NOT"+TradePortalConstants.USER_ENTERED_ACCT, "",accountSet.contains(payeeAcctNum), "ListText", "", isReadOnly,"onClick=\"javascript:setAccount('SelectedAccount');\"\""));
					 */
	        	    acctNo = (accountSet.contains(payeeAcctNum))? payeeAcctNum : "";
                   
					options = ListBox.createOptionList(accountDoc, "ACCOUNT_NUMBER", "ACCOUNT_NUMBER",acctNo,userSession.getSecretKey());
                    out.println(widgetFactory.createSelectField( "SelectedAccount", "", 
					defaultText, options, isReadOnly, false, isFromExpress, "onchange=\"javascript:setAccountSelectType('SelectedAccount','1');\"", "","" ));
		           
                }
		        %>
<%--	Naveen 13-August-2012 IR-T36000004481. Modified the width of the below five Text fields		--%>		        
		        <%= widgetFactory.createTextField( "PayeeName", "DirectDebitInstruction.Name", 
currentDomesticPayment.getAttribute("payee_name"), "30", isReadOnly, !isTemplate, isFromExpress,
"onChange='updatePayeePartyName();'", "", "") %>

				<%= widgetFactory.createTextField( "PayeeAddrL1", "DirectDebitInstruction.PayerAddress", 
currentDomesticPayment.getAttribute("payee_address_line_1"), "30", isReadOnly, false, isFromExpress, "onChange='updatePayeeAddressLine();'", "","" ) %>
<%= widgetFactory.createTextField( "PayeeAddrL2", "", 
currentDomesticPayment.getAttribute("payee_address_line_2"), "30", isReadOnly, false, isFromExpress, "onChange='updatePayeeAddressLine();'", "","" ) %>
<%= widgetFactory.createTextField( "PayeeAddrL3", "", 
currentDomesticPayment.getAttribute("payee_address_line_3"), "30", isReadOnly, false, isFromExpress, "onChange='updatePayeeAddressLine();'", "","" ) %>
<%= widgetFactory.createTextField( "PayeeAddrL4", "", 
currentDomesticPayment.getAttribute("payee_address_line_4"), "30", isReadOnly, false, isFromExpress, "onChange='updatePayeeAddressLine();'", "","" ) %>

<%--	Naveen 13-August-2012 IR-T36000004481. Modified the width of the below Select field and 2 Text Fields		--%>
<%
					String defaultCntry = currentDomesticPayment
							.getAttribute("country");
					options = Dropdown.createSortedRefDataOptions("COUNTRY",
							defaultCntry, loginLocale);
				out.println(widgetFactory.createSelectField( "Country", "DirectDebitInstruction.Country", 
	" ", options, isReadOnly, false, isFromExpress, "class='char30'", "","" ));
				%> 
				
				<%= widgetFactory.createTextField( "payeeFaxNumber", "DirectDebitInstruction.PayerFax", 
	currentDomesticPayment.getAttribute("payee_fax_number"), "30", isReadOnly, false, isFromExpress, "","","")%>
	
	<%= widgetFactory.createTextField( "payeeEmail", "DirectDebitInstruction.PayerEmail", 
	currentDomesticPayment.getAttribute("payee_email"), "30", isReadOnly, false, isFromExpress, "","","")%>
			        
		         <input type=hidden name=beneficiaryPartyOid value=<%=currentDomesticPayment.getAttribute("beneficiary_party_oid")%>>
		         <input type=hidden name=PayeeAccount value=<%=currentDomesticPayment.getAttribute("payee_account_number")%>>
</div> <%-- closes columnLeft --%>

<div class="columnRight">
				<%
				String payerBankSearchHtml ="";
				String clearPayerBankSearchHtml ="";
				
				if (TradePortalConstants.NON_ADMIN.equals(userSession.getSecurityType())) {
 			     
		String itemid="PayeeBankName,PayeeBankAddressLine1,PayeeBankAddressLine2,PayeeBankAddressLine3,PayeeBankCountry,PayeeBankBranchName,PayeeBankBranchCode,PayerBankData,PayeeBankCode";
			 String section="banksection_ddi";
			payerBankSearchHtml = widgetFactory.createBankSearchButton(itemid,section, isReadOnly, TradePortalConstants.PAYER_BANK);
			//String payerBankSearchHtml = widgetFactory.createBankSearchButton(TradePortalConstants.BUTTON_BANKSEARCH,
           // "", isReadOnly, TradePortalConstants.PAYER_BANK);

			// String payerBankSearchHtml = widgetFactory.createPartySearchButton(TradePortalConstants.BUTTON_BANKSEARCH,"",
			 // false,TradePortalConstants.PAYER_BANK,false);
			 
			  //String clearPayerFunction = "setButtonPressed('Clear', 0); return setBankPartyToClear('PRB')";
			 //String clearPayerBankSearchHtml = widgetFactory.createPartyClearButton("ClearPayerBankButton",
			 //clearPayerFunction, false, TradePortalConstants.PAYER_BANK);
			 clearPayerBankSearchHtml = widgetFactory.createPartyClearButton( "ClearPayerBankButton", "erasePayerBank();",false,"");
			}
			 %>
			 <%-- Below code added by jyoti on 30-11-2012 for adding Hovertext  as per DDI doc--%>
			  <%=widgetFactory.createHoverHelp("banksection_ddi","SearchHoverText") %>
			  <%=widgetFactory.createHoverHelp("ClearPayerBankButton","ClearHoverText") %>
			  

<%= widgetFactory.createSubsectionHeader( "DirectDebitInstruction.PayerBank", isReadOnly, false, isFromExpress, payerBankSearchHtml+clearPayerBankSearchHtml) %>
 <%-- @Komal M PR ANZ Incident Report_Issue No.796 Begins--%>
<div class="formItem  required" >
<label for="ValueDate"><%=resMgr.getText("DomesticPaymentRequest.BenBankBranchCode", TradePortalConstants.TEXT_BUNDLE)%></label>
</div>

<%-- <input data-dojo-type="dijit.form.TextBox" name="PayeeBankBranchCode" readonly="true" 
id="PayeeBankBranchCode" maxLength="5" class="char16" required="false" 
value="<%=currentDomesticPayment.getAttribute("payee_bank_code")%>"> --%>

<%= widgetFactory.createTextField( "PayeeBankBranchCode", "", 
	currentDomesticPayment.getAttribute("payee_bank_code"), "30", isReadOnly, false, isFromExpress, "class='char16' onChange='updateBankCode();'","","")%>
	
<%-- @Komal M PR ANZ Incident Report_Issue No.796 Ends--%>


				

				<input type="hidden" name="PayeeBankCode" value="<%=currentDomesticPayment.getAttribute("payee_bank_code")%>">	<%-- Vshah CR509 --%>		
				<%-- Second Line: Payee Name and Address, Bank Address and Central Bank Label only --%>
				<%-- plus Payee Terms and OTL Payee --%>
			   <input type="hidden"
					name='PayeeTermsPartyType' value="<%=TradePortalConstants.PAYEE%>">
				
			<%
        		countryCode = "";
        		countryDesc = "";
        		try
        		{
        			countryCode = currentDomesticPayment.getAttribute("payee_bank_country");
        			if (InstrumentServices.isNotBlank(countryCode))
        				countryDesc = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY, countryCode, 
        															resMgr.getCSDB().getLocaleName());
        			if (InstrumentServices.isBlank(countryDesc))
        			{
        				countryDesc = countryCode;
        				countryCode = "";
        			}
        		}
        		catch (Exception e)
        		{
        			countryCode = "";
        			countryDesc = "";
        		}
        		%>          							

<%--	Naveen 21-August-2012 IR-T36000004481/T36000004555. Modified the height of the below Text Area using style attribute	--%>				
				<%= widgetFactory.createTextArea(
			"PayerBankData",
			"DirectDebitInstruction.PayerBankAddress",
			buildPaymentBankDisplayData(
					currentDomesticPayment
											.getAttribute("payee_bank_name"),
									currentDomesticPayment
											.getAttribute("payee_branch_name"),
									currentDomesticPayment
											.getAttribute("address_line_1"),
									currentDomesticPayment
											.getAttribute("address_line_2"),
									currentDomesticPayment
											.getAttribute("address_line_3"),
									countryDesc, isReadOnly),
			true, false, isFromExpress,
			"style=\"min-height: 80px;\"",
			"",
			"") %>
				<%
					String clearReqExtraTag = "return setBankPartyToClear('" + TradePortalConstants.PAYER_BANK + "');"; //IAZ 01/30/10 Add
					if (TradePortalConstants.NON_ADMIN.equals(userSession.getSecurityType())) {
				%>				
				 <%}%>
				<input type="hidden" name="PayeeBankName"	value='<%=currentDomesticPayment.getAttribute("payee_bank_name")%>'>
				<input type="hidden" name="PayeeBankAddressLine1" value='<%=currentDomesticPayment.getAttribute("address_line_1")%>'>
				<input type="hidden" name="PayeeBankAddressLine2" value='<%=currentDomesticPayment.getAttribute("address_line_2")%>'>
				<input type="hidden" name="PayeeBankAddressLine3" value='<%=currentDomesticPayment.getAttribute("address_line_3")%>'>
				<input type="hidden" name="PayeeBankCountry" value='<%=countryCode%>'>
				<input type=hidden name="PayeeBankBranchName" value="<%=currentDomesticPayment.getAttribute("payee_branch_name")%>"> 	<%-- Vshah CR509 --%>
				<%-- Third Line: Country, Fax, Email, Instructions Number and Centrtal Bank Data 121009 --%>
				<%-- plus Payee Terms and OTL Payee --%>
				<input type="hidden"
					name='PayeeTermsPartyType' value="<%=TradePortalConstants.PAYEE%>">
				<input type="hidden" name="PayeeOTLCustomerId"
					value='<%=payerParty.getAttribute("OTL_customer_id")%>'>
				
				
				

				</p>
				<%if (!domestic_payment_oid.equals("")) {%>
				<div class="formItem" id='UpdatePayerButtondiv'>&nbsp;&nbsp;
				<button data-dojo-type="dijit.form.Button"  name="UpdatePayer" id="UpdatePayerButton" data-dojo-props="iconClass:'cancel'" >
				<%=resMgr.getText("common.UpdatePayerText",TradePortalConstants.TEXT_BUNDLE)%>				
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					setButtonPressed('SaveTrans', '0');
					document.forms[0].submit();
			 	</script>
			</button>	
			<%= widgetFactory.createHoverHelp("UpdatePayerButton","common.UpdatePayerText") %>
			
			<button data-dojo-type="dijit.form.Button"  name="CancelButton" id="CancelButton" type="cancel" data-dojo-props="iconClass:'cancel'" >
				<%=resMgr.getText("common.CancelText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					previousPayeeValue();
					<%-- document.forms[0].submit(); --%>
			 	</script>
			 	</button>
			 	<%= widgetFactory.createHoverHelp("CancelButton","common.Cancel") %>
			</div>
			<% }else{
			%>
			
			
			<div id='SavePayerButtondiv'>&nbsp;&nbsp;
			<button data-dojo-type="dijit.form.Button"  name="SavePayer" id="SavePayerButton"  data-dojo-props="iconClass:'cancel'" >
				<%=resMgr.getText("common.SavePayerText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					setButtonPressed('SaveTrans', '0');
					document.forms[0].submit();
			 	</script> 	
			</button>	
			<%= widgetFactory.createHoverHelp("SavePayerButton","common.SavePayerText") %>		
			<button data-dojo-type="dijit.form.Button" onClick='clearAllDebitFields();' name="CancelButton" id="CancelButton" data-dojo-props="iconClass:'cancel'" >
				<%=resMgr.getText("common.CancelText",TradePortalConstants.TEXT_BUNDLE)%>								
			 	</button>
			 	<%= widgetFactory.createHoverHelp("CancelButton","common.Cancel") %>
			</div><% }%>
</div> <%-- closes columnRight --%>
<% if (!isReadOnly){ %> 
				<%=widgetFactory.createHoverHelp("orderPayer", "PartySearchIconHoverHelp") %>			
			<%} %>