<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Export Collection Amend Page - all sections

  Description:
    Contains HTML to create the Export Collection Amend page.  All data retrieval 
  is handled in the Transaction-EXP_COL-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_COL-AMD-html.jsp" %>
*******************************************************************************
--%>
<%
  String bankActionRequired = terms.getAttribute("bank_action_required");
%>
  <%
  /*********************************************************************************************************
  * Start of General Section - beige bar
  **********************************************************************************************************/
  %>
  
  <%= widgetFactory.createSectionHeader("1", "ExportCollectionAmend.General") %> 

  <%
  /***********************************************************
  * Reference Number - static display of stored data
  ***********************************************************/
  %>
  <div>
  
   	 <div class="inline formItem readOnly" >
		<label for="exportRefNoLabel"><%=resMgr.getText("ExportCollectionAmend.YourRefNo", TradePortalConstants.TEXT_BUNDLE)%></label><br>
		<span class="fieldValue"><%= (terms.getAttribute("reference_number")==null
				||terms.getAttribute("reference_number").trim().equals(""))?"&nbsp;":terms.getAttribute("reference_number") %></span>
	</div>
	<div class="inline formItem readOnly" >
		<label for="exportDrawNameLabel"><%=resMgr.getText("ExportCollectionAmend.DrawerName", TradePortalConstants.TEXT_BUNDLE)%></label><br>
		<span class="fieldValue"><%= (drawerName==null||drawerName.trim().equals(""))?"&nbsp;":drawerName %></span>
	</div>     
    		
      <div style="clear: both;"></div>
  
  <%
  /***********************************************************
  * Collection Amount Row - static display of stored data
  ***********************************************************/
  %>
  <%
            if (!isProcessedByBank) {
      %>
      
      <%String tempCollAmt = TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), currencyCode,loginLocale); %>
	<div class="formItem inline readOnly">
		<label for="exportColAmtLabel"><%=resMgr.getText("ExportCollectionAmend.CollectionAmount", TradePortalConstants.TEXT_BUNDLE)%></label><br>
			<%=currencyCode%>
    	<span class="fieldValue"><%= (tempCollAmt==null||tempCollAmt.trim().equals(""))?"&nbsp;":tempCollAmt %></span>
    </div>
    
      <div style="clear: both;"></div>
      <%
            }
      %>
    
       <div class="formItem readOnly">
      	<div class="inline">
      		<label for="exportNewAmtLabel"><%=resMgr.getText("ExportCollectionAmend.NewAmount", TradePortalConstants.TEXT_BUNDLE)%></label><br>
      			<%=currencyCode%>
	      <%= widgetFactory.createTextField( "TransactionAmount", "", displayAmount, "30", isReadOnly, false,false, "class='char8'", "", "none") %>                      
	    </div>
	   </div>
     <br/>
     
 
      <%=widgetFactory.createTextArea("AmendmentDetails",
                              "",
                              terms.getAttribute("amendment_details"), isReadOnly, false,
                              false, "rows='10';", "", "")%>
      <%if(!isReadOnly){ %>
      	<%=widgetFactory.createHoverHelp("AmendmentDetails", "DirectDebitCollection.AdditionalToolTip") %>
      <%} %>
	</div>
	</div>  

  <%
  /*********************************************************************************************************
  * Start of Instructions To Bank Section - beige bar
  **********************************************************************************************************/
  %>
  <%= widgetFactory.createSectionHeader("2", "ExportCollectionAmend.InsttoBank") %> 

  <%
  /*************************************************
  *  Action Radio buttons action is/not required
  *************************************************/
  %>
  <%=resMgr.getText("ExportCollectionAmend.InstsenttoBank", 
                             TradePortalConstants.TEXT_BUNDLE)%>                   
      <br/>  <br/>   
      <%=widgetFactory.createRadioButtonField("BankActionRequired","TradePortalConstants.INDICATOR_YES","",
    		  TradePortalConstants.INDICATOR_YES,bankActionRequired.equals(TradePortalConstants.INDICATOR_YES), isReadOnly) %>         
      
      <%=resMgr.getText("ExportCollectionAmend.YourAction", 
                             TradePortalConstants.TEXT_BUNDLE)%>
                             
	  
	   <b><%=resMgr.getText("ExportCollectionAmend.actionrequired", 
                             TradePortalConstants.TEXT_BUNDLE)%></b>
	   
	   <%=resMgr.getText("ExportCollectionAmend.OnThisAmend", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	
       <br> 
       <%=widgetFactory.createRadioButtonField("BankActionRequired","TradePortalConstants.INDICATOR_NO","",TradePortalConstants.INDICATOR_NO,bankActionRequired.equals(TradePortalConstants.INDICATOR_NO), isReadOnly) %>           
      
	   <%=resMgr.getText("ExportCollectionAmend.YourAction", 
                             TradePortalConstants.TEXT_BUNDLE)%>
                             
	   <b><%=resMgr.getText("ExportCollectionAmend.actionnotrequired", 
                             TradePortalConstants.TEXT_BUNDLE)%></b>

	   <%=resMgr.getText("ExportCollectionAmend.OnThisAmend", 
                             TradePortalConstants.TEXT_BUNDLE)%>
       <br/>  <br/>                        

  <%
  /*************************************************
  *  Special Instructions Dropdown
  *************************************************/
  %>
  
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          
          out.println(widgetFactory.createSelectField( "SpecInstrPhraseItem", "",defaultText, options, isReadOnly, false, false, "onChange=" + 
                  PhraseUtility.getPhraseChange("/Terms/special_bank_instructions",
                          "SpclBankInstructions", "1000","document.forms[0].SpclBankInstructions"),"", ""));
  /*************
  * Add Button
  **************/
  %>
          
  <%
        }
  %>
	 	  
  <%
  /*************************************************
  *  Special Instructions TextArea Box
  *************************************************/
  %>
   
   	<%=widgetFactory.createTextArea("SpclBankInstructions", "", terms.getAttribute("special_bank_instructions"),
					isReadOnly, false, false, "rows='10';", "", "")%>
	<%if(!isReadOnly){ %>
		<%=widgetFactory.createHoverHelp("SpclBankInstructions", "DirectDebitCollection.SpecialInstToolTip") %>	  
	<%} %>                      
      </div>

  <%
  /*********************************************************************************************************
  * Start of New Payment Terms Section - beige bar
  **********************************************************************************************************/
  %>
  <%= widgetFactory.createSectionHeader("3", "ExportCollectionAmend.NewPaymentTerms") %>                       
  
  <%
  /************
  *  Tenor #1
  ************/
  %>
  
  
	<%--=widgetFactory.createStackedLabel("",
					"ExportCollectionAmend.tenor")--%>
	<table class="formDocumentsTable" width="100%">
		<thead>
			<tr>
				<th width="15%">&nbsp;</th>
				<th style="text-align:left;font-weight: bold;"><%=resMgr.getText("ExportCollectionAmend.TenorColumn", TradePortalConstants.TEXT_BUNDLE)%></th>
				<th style="text-align:left;font-weight: bold;"><%=resMgr.getText("ExportCollectionAmend.Amount", TradePortalConstants.TEXT_BUNDLE)%></th>
				<th style="text-align:left;font-weight: bold;"><%=resMgr.getText("ExportCollectionAmend.DraftNumber", TradePortalConstants.TEXT_BUNDLE)%></th>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td><%=resMgr.getText("ExportCollectionAmend.Tenor1",TradePortalConstants.TEXT_BUNDLE)%></td>
				<td><%=widgetFactory.createTextField("Tenor1", "",
					terms.getAttribute("tenor_1"), "35", isReadOnly, false, false,
					"class='char35'", "", "none")%></td>
				<%
					String amount;
					amount = terms.getAttribute("tenor_1_amount");

					if (!getDataFromDoc)
						displayAmount = TPCurrencyUtility.getDisplayAmount(amount,
								currencyCode, loginLocale);
					else
						displayAmount = amount;
				%>

				<td><%=widgetFactory.createTextField("Tenor1Amount", "",
					displayAmount, "15", isReadOnly, false, false, "class='char10'", "",
					"none")%></td>
				<td><%=widgetFactory.createTextField("Tenor1DraftNumber", "",
					terms.getAttribute("tenor_1_draft_number"), "15", isReadOnly,
					false, false, "class='char20'", "", "none")%></td>
					
			</tr>
			<%
				for (int x = 2; x <= NUMBER_OF_TENORS; x++) {

					/****************
					 * Tenor #X Row
					 ****************/

					label = "ExportCollectionAmend.Tenor" + x;
					descName = "Tenor" + x;
					descAttName = "tenor_" + x;
					amountName = "Tenor" + x + "Amount";
					amountAttName = "tenor_" + x + "_amount";
					draftName = "Tenor" + x + "DraftNumber";
					draftAttName = "tenor_" + x + "_draft_number";
			%>

			<%@ include file="Transaction-EXP_OCO-AMD-Tenors.frag"%>
			<%
				}
			%>
		</tbody>
	</table>

</div>