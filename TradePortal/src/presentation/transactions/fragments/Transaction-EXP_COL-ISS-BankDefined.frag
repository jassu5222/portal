<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Export Collection Issue Page - Bank Defined section

  Description:
    Contains HTML to create the Export COL Issue Bank Defined section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_COL-ISS-BankDefined.jsp" %>
*******************************************************************************
--%>

  
  <%
  /*********************************************************
  * Start of Bank Defined Section - beige bar
  **********************************************************/
  %>
 <%--    
<%
	if (paymtInstr.equals("Y") ){
%>	      <%=resMgr.getText("ExportCollectionIssue.7BankDefined", 
                                TradePortalConstants.TEXT_BUNDLE)%>
<%  
	}else{
%>            <%=resMgr.getText("ExportCollectionIssue.6BankDefined", 
                                TradePortalConstants.TEXT_BUNDLE)%>
<%      }
%>
--%>	
<div class="columnLeft">
<%
  /********************************
  * Purpose Type
  ********************************/
  %>
    
          <%= widgetFactory.createTextField("PurposeType", "ExportCollectionIssue.PurposeType",  terms.getAttribute("purpose_type"), "3",isReadOnly) %>    
  </div>
  
  <div class="columnRight">
  <%
  /********************************
  * Uniform Rules for Collection
  ********************************/
  %>
 	<%-- <div class="indent">
    
        <%=resMgr.getText("ExportCollectionIssue.UniformRulesForCollection",TradePortalConstants.TEXT_BUNDLE)%></div>--%>
        <%=widgetFactory.createSubsectionHeader("ExportCollectionIssue.UniformRulesForCollection",isReadOnly,false,false,"")%>
  <%
  /********************************
  * Revision Year
  ********************************/
  %><span class="formItem inline">
      
          <%= widgetFactory.createTextField("RevisionYear", "ExportCollectionIssue.Year",  terms.getAttribute("urc_year"), "4",isReadOnly,false, false, "", "", "none") %>                              
       </span><span class="formItem inline">
  <%
  /********************************
  * Publication Number
  ********************************/
  %>
      
          <%= widgetFactory.createTextField("PublicationNumber", "ExportCollectionIssue.PublicationNumber",  terms.getAttribute("urc_pub_num"), "10",isReadOnly,false, false, "", "", "none") %>                              
  </span>
   </div>