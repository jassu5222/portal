<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Export Col Issue Page - Collection Instructions section

  Description:
    Contains HTML to create the Export COL Issue Collection Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_COL-ISS-CollInstructions.jsp" %>

  Note: There has been a change in the Case of Need display due to a request
	From the Business Analyst group - instead of displaying this in a 
 	multi-line textbox, the data is displayed in a couple of text boxes.
	This change will affect how the generated PDF will look as well.
	6/22/01 -RAS
*******************************************************************************
--%>

<%
  // -- For the radial buttons --
  String caseOfNeedType = terms.getAttribute("in_case_of_need_use_instruct");
  String chargesType = terms.getAttribute("collection_charges_type");
%>

  <%= widgetFactory.createWideSubsectionHeader( "ExportCollectionIssue.CollectionInstructionOpts",isReadOnly, false, false, "" ) %>	
 <div class="columnLeft" style="border-right: 0px;">
  <%
  /************************
  * Release Doc on Payment
  ************************/
  %>
      
      <%= widgetFactory.createCheckboxField("ReleaseOnPayInd", "ExportCollectionIssue.ReleaseDocumentsOnPayment",terms.getAttribute("instr_release_doc_on_pmt").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %>
    
  <%
  /********************************
  * Advise payment By Teleco
  ********************************/
  %>
      <%= widgetFactory.createCheckboxField("AdvisePmtByTelInd", "ExportCollectionIssue.AdvisePaymentByTelecommunication",terms.getAttribute("instr_advise_pmt_by_telco").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %> 
   
  <%
  /********************************
  * Advise Non-Payment By Teleco
  ********************************/
  %>
       <%= widgetFactory.createCheckboxField("AdviseNonPayByTelInd", "ExportCollectionIssue.AdviseNonPaymentByTelecommunication",terms.getAttribute("instr_advise_non_pmt_by_telco").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %> 
  
  <%
  /********************************
  * Note for Non-Payment
  ********************************/
  %>
       <%= widgetFactory.createCheckboxField("NoteForNonPayInd", "ExportCollectionIssue.NoteForNonPayment",terms.getAttribute("instr_note_for_non_pmt").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %> 
  
  <%
  /********************************
  * Protest for Non-Payment
  ********************************/
  %>
       <%= widgetFactory.createCheckboxField("ProtestNonPayInd", "ExportCollectionIssue.ProtestForNonPayment",terms.getAttribute("instr_protest_for_non_pmt").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %> 
              
 </div>
 <div class="columnRight">
 <%
  /****************************
  * Release Doc on Acceptance
  ****************************/
  %>
  	  <%= widgetFactory.createCheckboxField("ReleaseOnAcceptInd", "ExportCollectionIssue.ReleaseDodumentsOnAcceptance",terms.getAttribute("instr_release_doc_on_accept").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %>  
  <%
  /********************************
  * Advise Acceptance By Teleco
  ********************************/
  %>
      <%= widgetFactory.createCheckboxField("AdviseAcceptByTelInd", "ExportCollectionIssue.AdviseAcceptanceByTelecommunication",terms.getAttribute("instr_advise_accept_by_telco").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %>  
   <%
  /********************************
  * Advise Non-Acceptance By Teleco
  ********************************/
  %>
       <%= widgetFactory.createCheckboxField("AdviseNonAcceptByTelInd", "ExportCollectionIssue.AdviseNonAcceptanceByTelecommunication",terms.getAttribute("instr_advise_non_accept_by_tel").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %>
  <%
  /********************************
  * Note for Non-Acceptance
  ********************************/
  %>
       <%= widgetFactory.createCheckboxField("NoteForNonAcceptInd", "ExportCollectionIssue.NoteForNonAcceptance",terms.getAttribute("instr_note_for_non_accept").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %>  
  <%
  /********************************
  * Protest For Non-Acceptance
  ********************************/
  %>
        <%= widgetFactory.createCheckboxField("ProtestNonAcceptInd", "ExportCollectionIssue.ProtestForNonAcceptance",terms.getAttribute("instr_protest_for_non_accept").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %>                                            
 </div>
  <%
  /********************************
  * Instructions-Other TextArea
  ********************************/
  %>
  <div class="formItemWithIndent4">
  		<%= widgetFactory.createLabel( "ExportCollectionIssue.EnterAnyAdditionalInstHere", "ExportCollectionIssue.EnterAnyAdditionalInstHere", false, false, false, "none") %>  		
</div>
<div class="formItemWithIndent4">
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(otherInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          
          out.println(widgetFactory.createSelectField( "InstrOtherPhraseItem", "", defaultText, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                  "/Terms/instr_other",
                  "InstrOtherText", "500","document.forms[0].InstrOtherText"), "", "none"));
%>
    
<%
        }
%>
</div>  
	<%if (!isReadOnly) {%>
		<%= widgetFactory.createTextArea( "InstrOtherText", "", terms.getAttribute("instr_other"), isReadOnly,false, false, "rows='10',col='128'","", "" ) %>
		<%= widgetFactory.createHoverHelp("InstrOtherText","InstrOtherText")%>
        <%}else{%>
        <%=widgetFactory.createAutoResizeTextArea( "InstrOtherText", "", terms.getAttribute("instr_other"), isReadOnly,false, false, "style='width:600px;;min-height:140px;' class='formItemWithIndent3'","", "" ) %>
        <%}%>

  <%
  /***************************************
  * In Case of Need Contact -  TextArea
  ***************************************/
  %>
  <%--IR-3679 26 July,2012 by Jyoti --%>
  <%!String SearchCheckBoxButton ="";%>
  <%
    if (!isReadOnly) {
      // Only display this row (and button) if not in readonly mode
 %>
     <% 
     String item_id="CaseOfNeedName,CaseOfNeedAddressLine1,CaseOfNeedAddressLine2,CaseOfNeedAddressLine3,CaseOfUserDefinedField1,CaseOfUserDefinedField2,CaseOfUserDefinedField3,CaseOfUserDefinedField4";
     String section_name="need_of_contact";
      SearchCheckBoxButton= widgetFactory.createPartySearchButton(item_id,section_name,isReadOnly,TradePortalConstants.CASE_OF_NEED,false); %>
    <%
    }
    %> <%
    if (!isReadOnly) {
      // Only display this row (and button) if not in readonly mode
 %>
  		<%= widgetFactory.createCheckboxField("CaseOfNeedInd", "ExportCollectionIssue.InCaseOfNeed",terms.getAttribute("in_case_of_need_contact").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly,false,"","","inline",SearchCheckBoxButton) %> 
              <%}else{%>
              <%= widgetFactory.createCheckboxField("CaseOfNeedInd", "ExportCollectionIssue.InCaseOfNeed",terms.getAttribute("in_case_of_need_contact").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly,false,"","","inline") %>
              <%}%>
  <%--IR-3679 Ended --%>

	<div style="clear:both;"></div>
	<div class="formItemWithIndent4">
	 <%= widgetFactory.createTextField( "CaseOfNeedName", "", termsPartyCaseOfNeed.getAttribute("name"), "35", isReadOnly, false, false, "onChange=\"pickCaseOfNeedCheckbox();\"" + 
		      " onBlur='checkCaseOfNeedName(\"" +
				StringFunction.escapeQuotesforJS(termsPartyCaseOfNeed.getAttribute("name")) + "\")'", "placeHolder:'"+resMgr.getTextEscapedJS("PartyDetail.PartyName",TradePortalConstants.TEXT_BUNDLE)+"'", "none" ) %></div>
				<div class="formItemWithIndent4">
     <%= widgetFactory.createTextField( "CaseOfNeedAddressLine1", "", termsPartyCaseOfNeed.getAttribute("address_line_1"), "35", isReadOnly, false, false, "onChange=\"pickCaseOfNeedCheckbox();\"", "placeHolder:'"+resMgr.getTextEscapedJS("ExportCollectionIssue.AddressLine1",TradePortalConstants.TEXT_BUNDLE)+"'", "none" ) %>
     </div><div class="formItemWithIndent4">
     <%= widgetFactory.createTextField( "CaseOfNeedAddressLine2", "", termsPartyCaseOfNeed.getAttribute("address_line_2"), "35", isReadOnly, false, false, "onChange=\"pickCaseOfNeedCheckbox();\"", "placeHolder:'"+resMgr.getTextEscapedJS("ExportCollectionIssue.AddressLine2",TradePortalConstants.TEXT_BUNDLE)+"'", "none" ) %>
          </div><div class="formItemWithIndent4">
     <%= widgetFactory.createTextField( "CaseOfNeedAddressLine3", "", termsPartyCaseOfNeed.getAttribute("address_line_3"), "35", isReadOnly, false, false, "onChange=\"pickCaseOfNeedCheckbox();\"", "placeHolder:'"+resMgr.getTextEscapedJS("common.cityStateAddressPlaceHolder",TradePortalConstants.TEXT_BUNDLE)+"'", "none" ) %>
          </div>
  <%
  /***************************************
  * In Case of Need Contact -  Option List
  ***************************************/
  %>
  <div class="formItem">
  <%=widgetFactory.createRadioButtonField("CaseOfNeedType","TradePortalConstants.IN_CASE_OF_NEED_ACCEPT","ExportCollectionIssue.AcceptInstructions",TradePortalConstants.IN_CASE_OF_NEED_ACCEPT,caseOfNeedType.equals(TradePortalConstants.IN_CASE_OF_NEED_ACCEPT), isReadOnly, "onChange=\"pickCaseOfNeedCheckbox();\"","") %>
  <br>
  <%=widgetFactory.createRadioButtonField("CaseOfNeedType","TradePortalConstants.IN_CASE_OF_NEED_GUIDANCE","ExportCollectionIssue.InstructionsForGuidanceOnly",TradePortalConstants.IN_CASE_OF_NEED_GUIDANCE,caseOfNeedType.equals(TradePortalConstants.IN_CASE_OF_NEED_GUIDANCE), isReadOnly, "onChange=\"pickCaseOfNeedCheckbox();\"","") %>
  <br>
  </div>
  <br>
  <div class="formItemWithIndent4">
  <%= widgetFactory.createLabel( "ExportCollectionIssue.CaseOfNeedAdditionalInstructions", "ExportCollectionIssue.CaseOfNeedAdditionalInstructions", false, false, false, "none") %>         
     </div>   <div class="formItemWithIndent4">    
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(caseOfNeedDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          
          out.println(widgetFactory.createSelectField( "CaseOfNeedPhraseItem", "", defaultText, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                  "/Terms/in_case_of_need_contact_text",
                  "InCaseOfNeedText", "500", "document.forms[0].InCaseOfNeedText"), "", "none"));
%>
        
<%
        }
%></div>
	<%if (!isReadOnly) {%>
	<%= widgetFactory.createTextArea( "InCaseOfNeedText", "", terms.getAttribute("in_case_of_need_contact_text"), isReadOnly,false, false, "rows='10' cols='128' onChange=\"pickCaseOfNeedCheckbox();\"","", "" ) %>
    <%= widgetFactory.createHoverHelp("InCaseOfNeedText","InCaseOfNeedText")%>    
    <%}else{%>
    <%=widgetFactory.createAutoResizeTextArea( "InCaseOfNeedText", "", terms.getAttribute("in_case_of_need_contact_text"), isReadOnly,false, false, "style='width:600px;min-height:140px;' class='formItemWithIndent3' onChange=\"pickCaseOfNeedCheckbox();\"","", "" ) %>
    <%}
          secureParms.put("case_of_need_terms_party_oid", 
                          termsPartyCaseOfNeed.getAttribute("terms_party_oid"));
%>
        <input type=hidden name="CaseOfNeedTermsPartyType" 
               value="<%=TradePortalConstants.CASE_OF_NEED%>">
        <input type=hidden name="CaseOfNeedOTLCustomerId"
               value="<%=termsPartyCaseOfNeed.getAttribute("OTL_customer_id")%>">
               
                <input type=hidden name="CaseOfUserDefinedField1"
		     value="<%=termsPartyCaseOfNeed.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="CaseOfUserDefinedField2"
		     value="<%=termsPartyCaseOfNeed.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="CaseOfUserDefinedField3"
		     value="<%=termsPartyCaseOfNeed.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="CaseOfUserDefinedField4"
		     value="<%=termsPartyCaseOfNeed.getAttribute("user_defined_field_4")%>">
    
  <%
  /**********************
  * Charges
  **********************/
  %>
    	<%= widgetFactory.createWideSubsectionHeader( "ExportCollectionIssue.Charges",isReadOnly, !isTemplate, false, "" ) %>
          
  <%
  /**********************
  * Charges OptionList
  **********************/
  %>
    	<div class="formItem">
    	<%=widgetFactory.createRadioButtonField("ChargesType","TradePortalConstants.COLLECTION_CHARGES_TYPE_ALL","ExportCollectionIssue.ChargesForDrawee",TradePortalConstants.COLLECTION_CHARGES_TYPE_ALL,chargesType.equals(TradePortalConstants.COLLECTION_CHARGES_TYPE_ALL), isReadOnly, "labelClass=\"formWidth\"","") %>
          <%--=resMgr.getText("ExportCollectionIssue.ChargesForDrawee",TradePortalConstants.TEXT_BUNDLE)--%>
         <div class="formItemWithIndent4"> <%=resMgr.getText("ExportCollectionIssue.ChargesForDrawee1",TradePortalConstants.TEXT_BUNDLE)%></div>
          <br/>
          <div class="formItemWithIndent4">
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(collChargesDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          
          out.println(widgetFactory.createSelectField( "ChargesPhraseItem", "", defaultText, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                  "/Terms/collection_charges_includ_txt",
                  "CollectionChargesText", "500","document.forms[0].CollectionChargesText"), "", "none"));
%>
      
<%
        }
%>
</div>
	
		<%= widgetFactory.createTextArea( "CollectionChargesText", "", terms.getAttribute("collection_charges_includ_txt"), isReadOnly,false, false, "maxlength=300 rows='10' onChange=\"pickAllBankChargesRadioButton();\"","", "" ) %>
        <%if (!isReadOnly) {%>
        <%= widgetFactory.createHoverHelp("CollectionChargesText","InstrOtherText")%>
        <%}%>
        <%=widgetFactory.createRadioButtonField("ChargesType","TradePortalConstants.COLLECTION_CHARGES_TYPE_DRAWEE","ExportCollectionIssue.OverseasChargesForDrawee",TradePortalConstants.COLLECTION_CHARGES_TYPE_DRAWEE,chargesType.equals(TradePortalConstants.COLLECTION_CHARGES_TYPE_DRAWEE), isReadOnly, 
        		"labelClass=\"formWidth\"","") %>
        <%--=resMgr.getText("ExportCollectionIssue.OverseasChargesForDrawee",TradePortalConstants.TEXT_BUNDLE)--%>
        <br/>
        <%=widgetFactory.createRadioButtonField("ChargesType","TradePortalConstants.COLLECTION_CHARGES_TYPE_DRAWER","ExportCollectionIssue.ChargesForDrawer",TradePortalConstants.COLLECTION_CHARGES_TYPE_DRAWER,chargesType.equals(TradePortalConstants.COLLECTION_CHARGES_TYPE_DRAWER), isReadOnly) %>
        <br>
        </div>
  <%
  /**********************
  * Charges CheckBoxes
  **********************/
  %>
        <%= widgetFactory.createCheckboxField("DontWaiveChargesInd", "ExportCollectionIssue.DoNotWaiveCharges1",terms.getAttribute("do_not_waive_refused_charges").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %> 
       
	 <%--  <span class="ControlLabel">
	     <%=resMgr.getText("ExportCollectionIssue.DoNotWaiveCharges2", 
                                TradePortalConstants.TEXT_BUNDLE)%>
	  </span> 
	  <%=resMgr.getText("ExportCollectionIssue.DoNotWaiveCharges3", 
                             TradePortalConstants.TEXT_BUNDLE)%>--%>
		<%= widgetFactory.createCheckboxField("DontWaiveInterestInd", "ExportCollectionIssue.DoNotWaiveInterest1",terms.getAttribute("do_not_waive_refused_interest").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %> 
       
	  <%--<span class="ControlLabel"> 
          <%=resMgr.getText("ExportCollectionIssue.DoNotWaiveInterest2", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	  </span> 
          <%=resMgr.getText("ExportCollectionIssue.DoNotWaiveInterest3", 
                             TradePortalConstants.TEXT_BUNDLE)%>--%>
	
  <%
  /************************************
  * Charges - Collect Ineterst At:
  ************************************/
  %>
  	<%= widgetFactory.createCheckboxField("CollectInterestInd", "ExportCollectionIssue.CollectInterestAt",terms.getAttribute("collect_interest_at_indicator").
              equals(TradePortalConstants.INDICATOR_YES),isReadOnly) %> 
   <div class="formItem"> 
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(collInterestDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          
          out.println(widgetFactory.createSelectField( "InterestPhraseItem", "", defaultText, options, isReadOnly, false, false,  "onChange=" + PhraseUtility.getPhraseChange(
                  "/Terms/collect_interest_at",
                  "CollectInterestAtText", "500","document.forms[0].CollectInterestAtText"), "", ""));
%>
      
<%
        }
        if(!isReadOnly){
%>
	<%= widgetFactory.createTextArea( "CollectInterestAtText", "", terms.getAttribute("collect_interest_at"), isReadOnly,false, false, "rows='10' onChange=\"pickCollectInterestCheckbox();\"","", "" ) %>
        <%}else{%>
    <%=widgetFactory.createAutoResizeTextArea( "CollectInterestAtText", "", terms.getAttribute("collect_interest_at"), isReadOnly,false, false, "style='width:600px;min-height:140px;' onChange=\"pickCollectInterestCheckbox();\"","", "" ) %>    
        <%}%>
</div>

<script LANGUAGE="JavaScript">

   function pickAllBankChargesRadioButton() {
   if (document.forms[0].CollectionChargesText.value > '')
	document.forms[0].ChargesType[0].click();
  }

  function pickCollectInterestCheckbox() {
    if (document.forms[0].CollectInterestAtText.value > '')
        document.forms[0].CollectInterestInd.checked = true;
    else
	document.forms[0].CollectInterestInd.checked = false;
  }

<%--
//This function will reset the OTL Customer ID (to "") for the 
//Drawee Terms party if the user changes the name in the text field.
--%>

  function checkCaseOfNeedName(originalName) {
   if( document.forms[0].CaseOfNeedName.value != originalName )
   {
	document.forms[0].CaseOfNeedOTLCustomerId.value = "";
   }
  }

</script>

<%
  if (phraseSelected) {
     // To ensure that the checkbox gets turned on when the user selects
     // a phrase for a previously blank text area, call these methods to
     // re-evaluate all the fields.
     // Note: this calls must be done AFTER the definition of the functions.
%>
     <script language="Javascript">
	pickCaseOfNeedCheckbox();
	pickAllBankChargesRadioButton();
	pickCollectInterestCheckbox();
     </script>
<%
  }
%>
