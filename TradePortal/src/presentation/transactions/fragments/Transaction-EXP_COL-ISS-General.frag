<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Export Collection Issue Page - General section

  Description:
    Contains HTML to create the Export Collection Issue General section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_COL-ISS-General.jsp" %>
*******************************************************************************
--%>
<div class="columnLeft">

<%
//Get some attributes for radio button fields.  We need to refer to this
// value frequently so do getAttribute once.

String amount = terms.getAttribute("amount");
String curcy = terms.getAttribute("amount_currency_code");
String currencyCode = curcy;
String pmtTermsType = terms.getAttribute("pmt_terms_type");
String displayAmount;

//02-feb-2013. IR-T36000010953. added extra params to populate designated bank info into collecting bank

String ide="DraweeName,DraweeAddressLine1,DraweeAddressLine2,DraweeCity,DraweeStateProvince,DraweePostalCode,DraweeCountry,DraweePhoneNumber,DraweeOTLCustomerId,bank_terms_party_oid,BankName,BankAddressLine1,BankAddressLine2,BankAddressLine3,BankCity,BankStateProvince,BankCountry,BankPostalCode,BankPhoneNumber,CollectingBank,BankOTLCustomerId,DraweeUserDefinedField1,DraweeUserDefinedField2,DraweeUserDefinedField3,DraweeUserDefinedField4";
String sect="drawee";


// Don't format the amount if we are reading data from the doc
if(!getDataFromDoc)
   displayAmount = TPCurrencyUtility.getDisplayAmount(amount, curcy, loginLocale);
else
   displayAmount = amount;
%>
<%
     String draweeSearchHtml = widgetFactory.createPartySearchButton(ide,sect,isReadOnly,TradePortalConstants.DRAWEE_BUYER,false);
     out.print(widgetFactory.createSubsectionHeader( "ExportCollectionIssue.Drawee",isReadOnly, false,false,draweeSearchHtml));

    if (!(isReadOnly)) {
%>
    <div style="display:none">
	<input type=hidden name='DraweeTermsPartyType'
               		   value=<%=TradePortalConstants.DRAWEE_BUYER%>>

    <input type=hidden name="DraweeOTLCustomerId"
               		   value="<%=termsPartyDrawee.getAttribute("OTL_customer_id")%>">
    <input type=hidden name="VendorId"
               		   value="<%=termsPartyDrawee.getAttribute("vendor_id")%>">
               		   
	<input type=hidden name="DraweeUserDefinedField1"
	value="<%=termsPartyDrawee.getAttribute("user_defined_field_1")%>">
	<input type=hidden name="DraweeUserDefinedField2"
	     value="<%=termsPartyDrawee.getAttribute("user_defined_field_2")%>">
	<input type=hidden name="DraweeUserDefinedField3"
	     value="<%=termsPartyDrawee.getAttribute("user_defined_field_3")%>">
	<input type=hidden name="DraweeUserDefinedField4"
	     value="<%=termsPartyDrawee.getAttribute("user_defined_field_4")%>">
     </div>
<%
    }
    secureParms.put("drawee_terms_party_oid",
                    termsPartyDrawee.getAttribute("terms_party_oid"));
%>
  <%
  /*************************
  * Drawee Name Textbox
  **************************/
  %>
  <%= widgetFactory.createTextField( "DraweeName", "ExportCollectionIssue.DraweeName", termsPartyDrawee.getAttribute("name"), "35", isReadOnly, !isTemplate, isExpressTemplate, "onBlur='checkDraweeName(\"" +
			StringFunction.escapeQuotesforJS(termsPartyDrawee.getAttribute("name")) + "\")' class='char30'","", "" ) %>
  <%
  /********************************
   * Drawee Address Line 1 Textbox
   *********************************/
   %>
   <%= widgetFactory.createTextField( "DraweeAddressLine1", "ExportCollectionIssue.AddressLine1", termsPartyDrawee.getAttribute("address_line_1"), "35", isReadOnly, !isTemplate, isExpressTemplate, "class='char30'", "", "" ) %>
   <%
   /********************************
  * Drawee Address Line 2 Textbox
  *********************************/
  %>
  <%= widgetFactory.createTextField( "DraweeAddressLine2", "ExportCollectionIssue.AddressLine2", termsPartyDrawee.getAttribute("address_line_2"), "35", isReadOnly, false, isExpressTemplate, "class='char30'", "", "" ) %>
      <div style="display:none">
      	<input type=hidden name="DraweeAddressLine3"
               value="<%=termsPartyDrawee.getAttribute("address_line_3")%>">
      </div>

   <%
  /********************************
  * Drawee City Textbox
  *********************************/
  %>
  <%= widgetFactory.createTextField( "DraweeCity", "ExportCollectionIssue.City", termsPartyDrawee.getAttribute("address_city"), "23", isReadOnly, !isTemplate, isExpressTemplate, "class='char30'", "", "" ) %>

   <%
  /********************************
  * Drawee State Textbox
  *********************************/
  %>
  <%= widgetFactory.createTextField( "DraweeStateProvince", "ExportCollectionIssue.ProvinceState", termsPartyDrawee.getAttribute("address_state_province"), "8", isReadOnly, false, isExpressTemplate, "", "", "inline" ) %>

  <%
  /********************************
  * Drawee Postal Code Textbox
  *********************************/
  %>
  <%= widgetFactory.createTextField( "DraweePostalCode", "PartyDetail.PostalCode", termsPartyDrawee.getAttribute("address_postal_code"), "8", isReadOnly, false, isExpressTemplate, "", "", "inline" ) %>
  <br/>
  <%
  /********************************
  * Drawee Country Dropdown box
  *********************************/
  %>
   <%
      options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY,
            		 									   termsPartyDrawee.getAttribute("address_country"),
                                                           loginLocale);
   %>
   <div style="clear:both;"></div>
   <%= widgetFactory.createSelectField( "DraweeCountry", "ExportCollectionIssue.Country"," ", options, isReadOnly, !isTemplate, isExpressTemplate, "class='char30'", "", "") %>

   <%
  /*************************
  * Drawee Phone # Textbox
  **************************/
  %>
  <%= widgetFactory.createTextField( "DraweePhoneNumber", "ExportCollectionIssue.PhoneNumber", termsPartyDrawee.getAttribute("phone_number"), "20", isReadOnly, false, isExpressTemplate, "", "regExp:'^[-0-9+;]+$'", "" ) %>
  <br>
  </div>
  <div class="columnRight">
  <%
  /***************************************************
  * Drawer/Collecting Bank - Search button(s)
  ****************************************************/
  %>
  <%
  String addressSearchIndicator = null;
  //DK IR T36000022581 Rel9.0 04/30/2014 - remove clear drawer icon
  //String addressDrawerClearHtml = "";
  addressSearchIndicator = termsPartyDrawer.getAttribute("address_search_indicator");
  String itemId="drawer_terms_party_oid,DrawerName,DrawerAddressLine1,DrawerAddressLine2,DrawerAddressLine3,DrawerCity,DrawerStateProvince,DrawerCountry,DrawerPostalCode,DrawerPhoneNumber,Drawer,DrawerOTLCustomerId,DrawerUserDefinedField1,DrawerUserDefinedField2,DrawerUserDefinedField3,DrawerUserDefinedField4";
  String Section="direct_send_collecting_bank";
  String addressSearchHtml = widgetFactory.createPartySearchButton(itemId,Section,isReadOnly,TradePortalConstants.DRAWER_SELLER,false);
  //addressDrawerClearHtml=widgetFactory.createPartyClearButton( "ClearDrawButton", "clearDrawer();",false,"");
  %>
	<%=widgetFactory.createSubsectionHeader( "ExportCollectionIssue.Drawer",isReadOnly, !isTemplate,isExpressTemplate,addressSearchHtml)%>

<%
	if (!(isReadOnly || isFromExpress) && !addressSearchIndicator.equals("N"))
  {
  	if(!isReadOnly && corpOrgHasMultipleAddresses)
  	{ %>
  	<div class="formItemButtoninline">
  	   <button data-dojo-type="dijit.form.Button"  name="AddressSearch" id="AddressSearch" type="button">
         <%=resMgr.getText("common.searchAddressButton",TradePortalConstants.TEXT_BUNDLE)%>
         <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	         var corpOrgOid = <%=corpOrgOid%>;
			require(["t360/common", "t360/partySearch"], function(common, partySearch){
				 partySearch.setPartySearchFields('<%=TradePortalConstants.DRAWER_SELLER%>');
	  			common.openCorpCustAddressSearchDialog(corpOrgOid);
	  		});
         </script>
         </button>

		 </div>
  	<%
  	}
  }
	%>
  <%
	  /*****************
  	  * Drawer TextArea
  	  ******************/
  	  %>
	   <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>	
	   <%--
  	  <%= widgetFactory.createTextArea("Drawer","",termsPartyDrawer.buildAddress(false, resMgr),true,false,false,"onFocus='this.blur();' style='width:auto;' rows='4' cols='45'","","") %>
		--%>	 
	  
		<%= widgetFactory.createAutoResizeTextArea("Drawer", "", termsPartyDrawer.buildAddress(false, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	
		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>

<%
         secureParms.put("drawer_terms_party_oid",termsPartyDrawer.getAttribute("terms_party_oid"));
%>
        <input type=hidden name="drawer_terms_party_oid"
               value="<%=termsPartyDrawer.getAttribute("terms_party_oid")%>">
        <input type=hidden name="DrawerTermsPartyType"
            value="<%=TradePortalConstants.DRAWER_SELLER%>">
        <input type=hidden name="DrawerName"
               value="<%=termsPartyDrawer.getAttribute("name")%>">
        <input type=hidden name="DrawerAddressLine1"
               value="<%=termsPartyDrawer.getAttribute("address_line_1")%>">
        <input type=hidden name="DrawerAddressLine2"
               value="<%=termsPartyDrawer.getAttribute("address_line_2")%>">
        <input type=hidden name="DrawerAddressLine3"
               value="<%=termsPartyDrawer.getAttribute("address_line_3")%>">
        <input type=hidden name="DrawerCity"
               value="<%=termsPartyDrawer.getAttribute("address_city")%>">
        <input type=hidden name="DrawerStateProvince"
               value="<%=termsPartyDrawer.getAttribute("address_state_province")%>">
        <input type=hidden name="DrawerCountry"
               value="<%=termsPartyDrawer.getAttribute("address_country")%>">
        <input type=hidden name="DrawerPostalCode"
               value="<%=termsPartyDrawer.getAttribute("address_postal_code")%>">
        <input type=hidden name="DrawerPhoneNumber"
               value="<%=termsPartyDrawer.getAttribute("phone_number")%>">
        <input type=hidden name="DrawerOTLCustomerId"
               value="<%=termsPartyDrawer.getAttribute("OTL_customer_id")%>">
        <input type=hidden name="DrawerAddressSeqNum"
               value="<%=termsPartyDrawer.getAttribute("address_seq_num")%>">
 <input type=hidden name="DrawerUserDefinedField1"
		     value="<%=termsPartyDrawer.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="DrawerUserDefinedField2"
		     value="<%=termsPartyDrawer.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="DrawerUserDefinedField3"
		     value="<%=termsPartyDrawer.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="DrawerUserDefinedField4"
		     value="<%=termsPartyDrawer.getAttribute("user_defined_field_4")%>">

	<%--rbhaduri - 14th Jun 06 - IR SAUG051361872--%>
	<input type=hidden name="DrawerAddressSearchIndicator"
               value="<%=termsPartyDrawer.getAttribute("address_search_indicator")%>">
   <%
  /**************************************************
  * Your Reference Number - Display/entry
  ***************************************************/
  %>
   <%= widgetFactory.createTextField( "RefNum", "ExportCollectionIssue.DrawersReferenceNumber", terms.getAttribute("reference_number"), "30", isReadOnly, false, false, "class='char20'", "", "") %>

   <%=widgetFactory.createSubsectionHeader("ExportCollectionIssue.DetailedInformation",isReadOnly,false,false,"")%>
   <%
  /*****************************
  * Currency Dropdown box
  *****************************/
  %>
      <div style="display:none">
		<input type=hidden name='DraweeTermsPartyType'
               		   	   value=<%=TradePortalConstants.DRAWEE_BUYER%>>
    </div>

<%
          options = Dropdown.createSortedCurrencyCodeOptions(
                      terms.getAttribute("amount_currency_code"), loginLocale);


          out.println(widgetFactory.createSelectField("TransactionCurrency","ExportCollectionIssue.Currency"," ",options,isReadOnly,!isTemplate,false,"class='char5' onChange=\"pickCurrencyChanged();\"","","inline"));
%>

  <%
  /*****************************
  * Amount Textbox
  *****************************/
  /*This is an temp fix, need to do it in common file.
  if(null != displayAmount && !isReadOnly){
	  displayAmount = displayAmount.trim().replaceAll(",", "");
  }
  */
  %>
  <%= widgetFactory.createAmountField( "TransactionAmount", "ExportCollectionIssue.Amount", displayAmount, currencyCode, isReadOnly, !isTemplate, false, "class='char15'", "readOnly:" +isReadOnly, "inline") %>
   <div style="clear: both;"></div>
   <%
  /*******************************************************
  * Covering Shipment of Dropdown and Multi-line textbox
  ********************************************************/
  %>
	<div class="formItem">
		<%=resMgr.getText("ExportCollectionIssue.CoveringShipmentOf", TradePortalConstants.TEXT_BUNDLE)%>
	</div>
  <%
  /*****************************
  * Covering Shipment Dropdown
  *****************************/

        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(goodsDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          /* KMehta IR-T36000040135 Rel 9500 on 07-Jan-2015 Change  - Begin*/
          out.println(widgetFactory.createSelectField( "GoodsDescPhraseItem", "",defaultText, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                  "/Terms/ShipmentTermsList/goods_description",
                  "GoodsDescText", "4000","document.forms[0].GoodsDescText"), "",""));
          /* KMehta IR-T36000040135 Rel 9500 on 07-Jan-2015 Change  - End*/
  /*************
  * Add Button
  **************/
  %>

  <%
        }
  %>

  <%
  /*****************************
  * Covering Shipment TextArea
  *****************************/
  %>

        <%= widgetFactory.createTextArea("GoodsDescText","",terms.getFirstShipment().getAttribute("goods_description"),isReadOnly,false, false,"rows='4' cols='45'","","") %>

	    </div> <%--Column Right End --%>
	    <div style="clear:both;"></div>
<div class="columnLeft">
   <%
  /**************************************
  * Payment Terms Table
  **************************************/
  %>
  <%=widgetFactory.createSubsectionHeader("ExportCollectionIssue.PaymentTerms",isReadOnly,!isTemplate,false,"")%>
    <%
    /*****************************
    * Sight Radio Button
    *****************************/
    %>
    <div class="formItem">
    <%--	Naveen 09-August-2012 IR-T36000003243. Commented below line and added alternate line	--%>
    <%-- <%=widgetFactory.createRadioButtonField("PmtTermsType","TradePortalConstants.PMT_SIGHT","ExportCollectionIssue.Sight",TradePortalConstants.PMT_SIGHT,pmtTermsType.equals(TradePortalConstants.PMT_SIGHT), isReadOnly) %>	--%>
    <%=widgetFactory.createRadioButtonField("PmtTermsType","TradePortalConstants.PMT_SIGHT","ExportCollectionIssue.Sight",TradePortalConstants.PMT_SIGHT,pmtTermsType.equals(TradePortalConstants.PMT_SIGHT), isReadOnly, "onClick=\"hideTenorTable()\"", "")%>
    <br><br>
    <%
    /**********************************
    * Cash Against Docs Radial Button
    **********************************/
    %>
    <%--	Naveen 09-August-2012 IR-T36000003243. Commented below line and added alternate line	--%>
    <%-- <%=widgetFactory.createRadioButtonField("PmtTermsType","TradePortalConstants.PMT_CASH_AGAINST","ExportCollectionIssue.CashAgainstDocuments",TradePortalConstants.PMT_CASH_AGAINST,pmtTermsType.equals(TradePortalConstants.PMT_CASH_AGAINST), isReadOnly) %>	--%>
    <%=widgetFactory.createRadioButtonField("PmtTermsType","TradePortalConstants.PMT_CASH_AGAINST","ExportCollectionIssue.CashAgainstDocuments",TradePortalConstants.PMT_CASH_AGAINST,pmtTermsType.equals(TradePortalConstants.PMT_CASH_AGAINST), isReadOnly, "onClick=\"hideTenorTable()\"", "") %>
    </div>
    <%
    /**********************************
    * # of Days After... Radial Button
    **********************************/
    %>
    <div class="formItem">
    <%--	Naveen 09-August-2012 IR-T36000003243. Commented below line and added alternate line	--%>
    <%-- <%=widgetFactory.createRadioButtonField("PmtTermsType","TradePortalConstants.PMT_DAYS_AFTER","",TradePortalConstants.PMT_DAYS_AFTER,pmtTermsType.equals(TradePortalConstants.PMT_DAYS_AFTER), isReadOnly) %>	--%>
    <%=widgetFactory.createRadioButtonField("PmtTermsType","TradePortalConstants.PMT_DAYS_AFTER","",TradePortalConstants.PMT_DAYS_AFTER,pmtTermsType.equals(TradePortalConstants.PMT_DAYS_AFTER), isReadOnly, "onClick=\"hideTenorTable()\"", "") %>
     <%if(!isReadOnly){ %>
     <%= widgetFactory.createHoverHelp("TradePortalConstants.PMT_DAYS_AFTER","ExportCollectionIssue.DaysAfter")%>
     <%} %>
    <%= widgetFactory.createNumberField( "PmtTermsNumDaysAfter", "", terms.getAttribute("pmt_terms_num_days_after"), "3", isReadOnly, false, false, "onChange=\"pickDaysAfterRadioButton();\" class='char0'","", "none" ) %>
    <%if(!isReadOnly){ %>
    <%= widgetFactory.createHoverHelp("PmtTermsNumDaysAfter","ExportCollectionIssue.DaysAfter")%>
    <%} %>
    <%--= widgetFactory.createSubLabel("ExportCollectionIssue.DaysAfter")--%>
	<%
          options = Dropdown.createSortedRefDataOptions(TradePortalConstants.PMT_TERMS_DAYS_AFTER,
                                 terms.getAttribute("pmt_terms_days_after_type"),
                                 loginLocale);
	%>
	<%=widgetFactory.createSearchSelectField("PmtTermsDaysAfterType","ExportCollectionIssue.DaysAfter"," ", options, isReadOnly, "class='char14'; onChange=\"pickDaysAfterRadioButton();\"")%>

	</div>
    <%
    /**********************************
    * At Fixed Maturity Radial Button
    **********************************/
    %>
    <div class="formItem">
    <%-- Naveen 01-August-2012 IR-T36000003243. Commented below line and added new overloaded method which takes 8 parameters --%>
    <%-- <%=widgetFactory.createRadioButtonField("PmtTermsType","TradePortalConstants.PMT_FIXED_MATURITY","ExportCollectionIssue.AtFixedMaturity",TradePortalConstants.PMT_FIXED_MATURITY,pmtTermsType.equals(TradePortalConstants.PMT_FIXED_MATURITY), isReadOnly) %>	--%>
    <%=widgetFactory.createRadioButtonField("PmtTermsType","TradePortalConstants.PMT_FIXED_MATURITY","",TradePortalConstants.PMT_FIXED_MATURITY,pmtTermsType.equals(TradePortalConstants.PMT_FIXED_MATURITY), isReadOnly, "onClick=\"hideTenorTable()\"", "") %>
    <%=widgetFactory.createSubLabel("ExportCollectionIssue.AtFixedMaturity")%>
    <%if(!isReadOnly){ %>
     <%= widgetFactory.createHoverHelp("TradePortalConstants.PMT_FIXED_MATURITY","ExportCollectionIssue.AtFixedMaturity")%>
     <%} %>
     <%if(isReadOnly) {
    	 String tempD = terms.getAttribute("pmt_terms_fixed_maturity_date");
    	 if(null != tempD){
    	 int tempPos = tempD.indexOf(" ");
			if (tempPos > 0) {
				tempD = tempD.substring(0, tempPos);
				tempD = TPDateTimeUtility.convertJPylonDateToISODate(tempD);
			}
			else if (tempD.length() == 10 && tempPos == -1) {
				tempD = TPDateTimeUtility.convertJPylonDateToISODate(tempD);
			}
			%>
     <span class="radioButtonLabel ReadOnly"><b><%=tempD %></b></span>
    	<%} %>


    <%} else{%>
    <span><%=widgetFactory.createDateField("AtFixedMaturityDate", "", terms.getAttribute("pmt_terms_fixed_maturity_date"), isReadOnly,
    false, false, "class='char10'; ", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'", "none", "") %></span>
    <%} %>
    <br/><br/>
    <%
    /****************************************
    * Other - Multiple Tenors Radial Button
    ****************************************/
    %>
    <%-- Naveen 01-August-2012 IR-T36000003243. Commented below line and added new overloaded method which takes 8 parameters --%>
    <%-- <%=widgetFactory.createRadioButtonField("PmtTermsType","TradePortalConstants.PMT_OTHER","ExportCollectionIssue.PaymentTermsOther1",TradePortalConstants.PMT_OTHER,pmtTermsType.equals(TradePortalConstants.PMT_OTHER), isReadOnly) %> --%>
    <%= widgetFactory.createRadioButtonField("PmtTermsType","TradePortalConstants.PMT_OTHER","ExportCollectionIssue.PaymentTermsOther1",TradePortalConstants.PMT_OTHER,pmtTermsType.equals(TradePortalConstants.PMT_OTHER), isReadOnly, "onClick=\"showTenorTable()\"", "")%>

    </div>
</div><%--Column left End --%>
<div class="columnRight">

  <%
  /********************************
  * Collecting Bank Search button
  *********************************/
  %>
  <%
    ide = "bank_terms_party_oid,BankName,BankAddressLine1,BankAddressLine2,BankAddressLine3,BankCity,BankStateProvince,BankCountry,BankPostalCode,BankPhoneNumber,CollectingBank,BankOTLCustomerId,BankUserDefinedField1,BankUserDefinedField2,BankUserDefinedField3,BankUserDefinedField4";
    sect = "direct_send_collecting_bank_two";
     String collectSearchHtml = widgetFactory.createPartySearchButton(ide,sect,isReadOnly,TradePortalConstants.COLLECTING_BANK,false);
     out.print(widgetFactory.createSubsectionHeader( "ExportCollectionIssue.CollectingBank",isReadOnly, !isTemplate,isExpressTemplate,collectSearchHtml));
     %>

  <%


  /**************************************************
  * Drawer / Collecting Bank Text box data display
  ***************************************************/
  %>

      <%
	  /*************************
  	  * Colecting Bank TextArea
  	  *************************/
  	  %>
	  <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>	
	  <%--
  	  <%= widgetFactory.createTextArea("CollectingBank","",termsPartyBank.buildAddress(false, resMgr),true,false,false,"rows='4' cols='45'","","") %> --%>

	<%= widgetFactory.createAutoResizeTextArea("CollectingBank", "", termsPartyBank.buildAddress(false, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	
	<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
<%
        //  secureParms.put("bank_terms_party_oid",termsPartyBank.getAttribute("terms_party_oid"));
%>
		<input type=hidden name="BankTermsPartyType"
				value="<%=TradePortalConstants.COLLECTING_BANK%>">
        <input type=hidden name="bank_terms_party_oid"
               value="<%=termsPartyBank.getAttribute("terms_party_oid")%>">
        <input type=hidden name="BankName"
               value="<%=termsPartyBank.getAttribute("name")%>">
        <input type=hidden name="BankAddressLine1"
               value="<%=termsPartyBank.getAttribute("address_line_1")%>">
        <input type=hidden name="BankAddressLine2"
               value="<%=termsPartyBank.getAttribute("address_line_2")%>">
        <input type=hidden name="BankAddressLine3"
               value="<%=termsPartyBank.getAttribute("address_line_3")%>">
        <input type=hidden name="BankCity"
               value="<%=termsPartyBank.getAttribute("address_city")%>">
        <input type=hidden name="BankStateProvince"
             value="<%=termsPartyBank.getAttribute("address_state_province")%>">
        <input type=hidden name="BankCountry"
               value="<%=termsPartyBank.getAttribute("address_country")%>">
        <input type=hidden name="BankPostalCode"
               value="<%=termsPartyBank.getAttribute("address_postal_code")%>">
        <input type=hidden name="BankPhoneNumber"
               value="<%=termsPartyBank.getAttribute("phone_number")%>">
        <input type=hidden name="BankOTLCustomerId"
               value="<%=termsPartyBank.getAttribute("OTL_customer_id")%>">
               
                <input type=hidden name="BankUserDefinedField1"
		     value="<%=termsPartyBank.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="BankUserDefinedField2"
		     value="<%=termsPartyBank.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="BankUserDefinedField3"
		     value="<%=termsPartyBank.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="BankUserDefinedField4"
		     value="<%=termsPartyBank.getAttribute("user_defined_field_4")%>">
          <%

	  %>

 </div>   <%--ColumnRight End --%>
 <div style="clear:both"></div>
 <% if (!isReadOnly){ %>
<%=widgetFactory.createHoverHelp("ClearDrawButton", "PartyClearIconHoverHelp") %>
 <%} %>
 <div style="clear:both;"></div>
<script LANGUAGE="JavaScript">
<%--
//If the associated textbox or dropdown box is selected then
//this radio button is selected..
--%>
  function pickDaysAfterRadioButton() {
   if ((document.forms[0].PmtTermsNumDaysAfter.value > '') ||
       (document.forms[0].PmtTermsDaysAfterType.selectedIndex != 0))
	document.forms[0].PmtTermsType[2].click();
  }

<%--
//When a part of the date is selected in the dropdown boxes,
//this will turn on the appropriate radio button.
--%>
  function pickFixedMaturityRadioButton() {
   if ((document.forms[0].AtFixedMaturityDay.selectedIndex != 0) ||
       (document.forms[0].AtFixedMaturityMonth.selectedIndex != 0) ||
       (document.forms[0].AtFixedMaturityYear.selectedIndex != 0))
	document.forms[0].PmtTermsType[3].click();
  }


<%--
//This function will reset the OTL Customer ID (to "") for the
//Drawee Terms party if the user changes the name in the text field.
--%>
  function checkDraweeName(originalName) {
   if( document.forms[0].DraweeName.value != originalName )
   {
	document.forms[0].DraweeOTLCustomerId.value = "";
   }
  }

<%--
//This function will clear the Drawer textarea.
//Drawer is not always needed on a template.
//GGAYLE - 1/12/04 - IR RBUD090458041
--%>
<%--DK IR T36000022581 Rel9.0 04/30/2014 - removed clearDrawer function as it is no longer used --%>
  
<%--
//Naveen 01-August-2012. IR-T36000003243. This function will show the Tenor table
--%>
  function showTenorTable() {
	  if(document.getElementById("MultTenorLabel"))
	  	document.getElementById("MultTenorLabel").style.display = 'block' ;
	  if(document.getElementById("MultTenor"))
	  	document.getElementById("MultTenor").style.display = 'block' ;
	  if(document.getElementById("tenorButtons"))
	  	document.getElementById("tenorButtons").style.display = 'block' ;
  }

<%--
//Naveen 01-August-2012. IR-T36000003243. This function will hide the Tenor table
--%>
  function hideTenorTable() {
	  if(document.getElementById("MultTenorLabel"))
	  	document.getElementById("MultTenorLabel").style.display = 'none' ;
	  if(document.getElementById("MultTenor"))
	  	document.getElementById("MultTenor").style.display = 'none' ;
	  if(document.getElementById("tenorButtons"))
	  	document.getElementById("tenorButtons").style.display = 'none' ;
  }


</script>
