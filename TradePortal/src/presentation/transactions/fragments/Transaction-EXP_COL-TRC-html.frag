<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Export Collection Amend Page - all sections

  Description:
    Contains HTML to create the Export Collection Amend page.  All data retrieval 
  is handled in the Transaction-EXP_COL-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_COL-AMD-html.jsp" %>
*******************************************************************************
--%>

  <%
  String tracerSendType = terms.getAttribute("tracer_send_type");
  if(tracerSendType==null) tracerSendType ="Y";
%>
  <%
  /*********************************************************************************************************
  * Start of General Section - beige bar
  **********************************************************************************************************/
  %>
  
      <%= widgetFactory.createSectionHeader("1", "ExportCollectionAmend.General") %>                       	    

  <%
  /***********************************************************
  * Reference Number - static display of stored data
  ***********************************************************/
  %>
  <div>
      <%= widgetFactory.createTextField( "", "ExportCollectionAmend.YourRefNo", terms.getAttribute("reference_number"), "20", true, false, false, "", "", "inline" ) %>                       
	<%-- DK IR T36000017442 Rel8.3 08/12/2013 starts - Remove Drawer Name field and move Collection Amount --%>
      <%--= widgetFactory.createTextField( "", "ExportCollectionAmend.DrawerName", drawerName, "20", true, false, false, "", "", "inline" ) --%>   
      <%
	  /***********************************************************
	  * Collection Amount Row - static display of stored data
	  ***********************************************************/
	  %>
	   <%--MEer Rel 8.3 IR-17442 Collection Amount should be visible in all the statuses --%>
      	 
  	 <%= widgetFactory.createTextField("", "ExportCollectionAmend.CollectionAmount", currencyCode + "   " + originalAmount.toString(), "25", true, false,false, "", "", "inline")%>	
  	 	
                     
  </div>
  <div style="clear:both"></div>
 <%--DK IR T36000017442 Rel8.3 08/12/2013 ends --%>
  <%
  /*************************************************
  * NEW Amount of Collection Row - 
  *************************************************/
  %>
   <%--MEer Rel 8.3 IR-17442 New Collection Amount is not applicable for Tracer --%>
  <%
  /*************************************************
  * Amendment Details Text Area 
  *************************************************/
  %>
      <%= widgetFactory.createTextArea("TracerDetails","ExportCollectionTrace.AdditionalTracertext",terms.getAttribute("tracer_details"),isReadOnly,false,false,"rows=5","","") %>                              
      <%if(!isReadOnly){%>
		<%=widgetFactory.createHoverHelp("TracerDetails", "ExportCollectionTrace.TracerDetailToolTip") %>
	  <%} %>
      </div>

  <%
  /*********************************************************************************************************
  * Start of Instructions To Bank Section - beige bar
  **********************************************************************************************************/
  %>
  
       <%= widgetFactory.createSectionHeader("2", "ExportCollectionAmend.InsttoBank") %>                      
	  

  <%
  /*************************************************
  *  Action Radio buttons action is/not required
  *************************************************/
  %>
  <div class="formItem">
  <%=widgetFactory.createSubLabel("ExportCollectionAmend.InstsenttoBank") %>                     
     <br>
  
	<%=widgetFactory.createRadioButtonField("TracerSendType","","",TradePortalConstants.NO_ACTION_REQUIRED,tracerSendType.equals(TradePortalConstants.NO_ACTION_REQUIRED), isReadOnly) %>
		<%=resMgr.getText("ExportCollectionTrace.CopyofTracer", TradePortalConstants.TEXT_BUNDLE)%>
   <br>

    
	<%=widgetFactory.createRadioButtonField("TracerSendType","","", TradePortalConstants.BANK_TO_SEND,tracerSendType.equals(TradePortalConstants.BANK_TO_SEND), isReadOnly) %>           
		<%=resMgr.getText("ExportCollectionTrace.SendTracer", TradePortalConstants.TEXT_BUNDLE)%>		 
<br/>&nbsp;<br/>
</div>
  <%
  /*************************************************
  *  Special Instructions Dropdown
  *************************************************/
  %>
  
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          
          out.println(widgetFactory.createSelectField( "SpecInstrPhraseItem", "ExportCollectionAmend.SpecialInstrucionsText",defaultText, options, isReadOnly, false, false, "onChange=" + 
                  PhraseUtility.getPhraseChange("/Terms/special_bank_instructions",
                          "SpclBankInstructions", "1000","document.forms[0].SpclBankInstructions"),"", ""));
  /*************
  * Add Button
  **************/
  %>
          
  <%
        }
  %>
	  <br>
  <%
  /*************************************************
  *  Special Instructions TextArea Box
  *************************************************/
  %>
        
        <%= widgetFactory.createTextArea("SpclBankInstructions","",terms.getAttribute("special_bank_instructions"),isReadOnly,false,false,"rows=5","","") %>                             
      </div>
<%-- DK IR T36000017442 Rel8.3 08/12/2013 starts - Remove New Payment Terms Section --%>