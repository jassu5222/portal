<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
    Export Letter of Credit Assignment Page - Other Conditions section

  Description:
    Contains HTML to create the Export Letter of Credit Assignment - Other 
    Conditions section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_DLC-ASN-OtherConditions.jsp" %>
*******************************************************************************
--%>

  
         
          
       
        <%
           if (isReadOnly)
           {
              out.println("&nbsp;");
           }
           else
           {
              options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());

              defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);

        out.println(widgetFactory.createSelectField( "AddlConditionsPhraseItem", "",defaultText , options, isReadOnly || isFromExpress,
					              !isTemplate,false,  "onChange=" + PhraseUtility.getPhraseChange( "/Terms/additional_conditions", "AddlConditionsText","1000","document.forms[0].AddlConditionsText"), "", "")); 
        
           }
        %>
       
	
 	<%= widgetFactory.createTextArea( "AddlConditionsText","",terms.getAttribute("additional_conditions"), isReadOnly || isFromExpress, false, false, "maxlength=1000 rows=10 cols='128'", "", "") %>                       
    <%if(!isReadOnly){%>
    	<%=widgetFactory.createHoverHelp("AddlConditionsText", "AssignmentHoverHelp") %>     
	<%}%>