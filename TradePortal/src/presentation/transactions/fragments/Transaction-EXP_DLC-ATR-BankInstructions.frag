<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
    Export Letter of Credit Amend Transfer Page - Bank Instructions section

  Description:
    Contains HTML to create the Export Letter of Credit Amend Transfer Page - 
    Bank Instructions section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_DLC-ATR-BankInstructions.jsp" %>
*******************************************************************************
--%>
	
<%
if (isReadOnly) {
	out.println("&nbsp;");
} else {
%>
<%
options = ListBox.createOptionList(spclInstrDocList,
			"PHRASE_OID", "NAME", "", userSession.getSecretKey());
	defaultText = resMgr.getText("transaction.AddAPhrase",
			TradePortalConstants.TEXT_BUNDLE);
%>
<%=widgetFactory.createSelectField(
							"SpecialInstructionsPhraseItem",
							"",
							defaultText,
							options,
							isReadOnly,
							false,
							false,
							"onChange="
									+ PhraseUtility
											.getPhraseChange(
													"/Terms/special_bank_instructions",
													"SpecialInstructionsText",
													"100",
													"document.forms[0].SpecialInstructionsText"),
							"", "")%>
<%
}
%>
<%=widgetFactory.createTextArea("SpecialInstructionsText", "",
				terms.getAttribute("special_bank_instructions"),
				isReadOnly, false, false, "rows='10' cols='128'", "", "")%>
<%if(!isReadOnly) {%>
<%=widgetFactory.createHoverHelp("SpecialInstructionsText","SpclBankInstructions") %>
<% }%>
