<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
    Export Letter of Credit Issue Transfer Page - Other Conditions section

  Description:
    Contains HTML to create the Export Letter of Credit Issue Transfer - Other 
    Conditions section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_DLC-TRN-OtherConditions.jsp" %>
*******************************************************************************
--%>

  
        <%
           if (isReadOnly)
           {
              out.println("&nbsp;");
           }
           else
           {
              options = ListBox.createOptionList(goodsDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());

              defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);

              
              out.println(widgetFactory.createSelectField( "GoodsPhraseItem", "TransferELC.GoodsDescription",defaultText , options, isReadOnly,
		              false,false,  "onChange=" + PhraseUtility.getPhraseChange("/Terms/ShipmentTermsList/goods_description", "GoodsDescText", 
                              "6500","document.forms[0].GoodsDescText"), "", ""));      
           }
        %>
        
        <%= widgetFactory.createTextArea( "GoodsDescText","",terms.getFirstShipment().getAttribute("goods_description"), isReadOnly) %>                             
       
        <%
           if (isReadOnly)
           {
              out.println("&nbsp;");
           }
           else
           {
              options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());

              defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);

              
              out.println(widgetFactory.createSelectField( "AddlConditionsPhraseItem", "TransferELC.OtherConditions",defaultText , options, isReadOnly,
		              false,false,  "onChange=" + PhraseUtility.getPhraseChange("/Terms/additional_conditions", 
                              "AddlConditionsText", "1000","document.forms[0].AddlConditionsText"), "", "")); 
        
           }
        %>
        
        <%= widgetFactory.createTextArea( "AddlConditionsText","",terms.getAttribute("additional_conditions"), isReadOnly) %>                                  
