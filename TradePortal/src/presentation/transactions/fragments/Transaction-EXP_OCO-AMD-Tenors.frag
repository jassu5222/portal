<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *  Vasavi CR 524 03/31/2010
--%>
<%--
*******************************************************************************
                     Export Collection Amend Page - Tenors section

  Description:
    Contains HTML to display the multiple tenors for the Export Collection 
    Amend Page.  This was localized because it gets called 5 times.  This
    should help the code in the Transaction-EXP_OCO-AMD-html.jsp file be a 
    little easier to read.

  This is not a standalone JSP.  It MUST be included using the following tag:

  label         = "ExportCollectionAmend.Tenor2";
  descName      = "Tenor2";
  descAttName   = "tenor_2";
  amountName    = "Tenor2Amount";
  amountAttName = "tenor_2_amount";
  draftName     = "Tenor2DraftNumber";
  draftAttName  = "tenor_2_draft_number";

    <%@ include file="Transaction-EXP_OCO-AMD-Tenors.frag" %>

*******************************************************************************
--%>
<tr>
	<td><%=resMgr.getText(label,TradePortalConstants.TEXT_BUNDLE)%></td>
					<%-- <%=resMgr.getText(label, TradePortalConstants.TEXT_BUNDLE)%> --%>
	<td><%=widgetFactory.createTextField(descName, "",
					terms.getAttribute(descAttName), "35", isReadOnly, false, false,
					"class='char35'", "", "none")%></td>
	<%
		amount = terms.getAttribute(amountAttName);

		if (!getDataFromDoc)
			displayAmount = TPCurrencyUtility.getDisplayAmount(amount,
					currencyCode, loginLocale);
		else
			displayAmount = amount;
	%>

	<td><%=widgetFactory.createTextField(amountName, "",
					displayAmount, "15", isReadOnly, false, false, "class='char10'", "",
					"none")%></td>
	<td><%=widgetFactory.createTextField(draftName, "",
					terms.getAttribute(draftAttName), "15", isReadOnly, false,
					false, "class='char20'", "", "none")%></td>
</tr>