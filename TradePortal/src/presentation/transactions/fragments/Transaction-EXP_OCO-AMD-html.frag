<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *  Vasavi CR 524 03/31/2010 
--%>
<%--
*******************************************************************************
                 Export Collection Amend Page - all sections

  Description:
    Contains HTML to create the Export Collection Amend page.  All data retrieval 
  is handled in the Transaction-EXP_OCO-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_OCO-AMD-html.jsp" %>
*******************************************************************************
--%>
<%
  String bankActionRequired = terms.getAttribute("bank_action_required");
%>
  <%
  /*********************************************************************************************************
  * Start of General Section - beige bar
  **********************************************************************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34" class="BankColor">
    <tr class="BankColor"> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
	  <%=resMgr.getText("ExportCollectionAmend.General", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	  <a name="General"></a>
	</p>
      </td>
      <td width="90%" class="BankColor" height="35" valign="top">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td class="BankColor" align="right" valign="middle" width="15" height="35" nowrap>

<%    // -- Online Help button goes here -- (fileName, anchor, resMgr)
	out.print( OnlineHelp.createContextSensitiveLink( "customer/amend_exp_coll.htm", 
							  "general", resMgr, userSession) );
%>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /***********************************************************
  * Reference Number - static display of stored data
  ***********************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap rowspan="2">&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
	  <%=resMgr.getText("ExportCollectionAmend.YourRefNo", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td nowrap width="50">&nbsp;</td>
      <td nowrap>
         <p class="ControlLabel">
          <%=resMgr.getText("ExportCollectionAmend.DrawerName", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	     </p>
      </td>
      <td width=100%>&nbsp;</td>
    </tr>
    <tr>
      <td nowrap> 
        <p class="ListText">
          <%=terms.getAttribute("reference_number")%>
	</p>
      </td>
      <td>&nbsp;</td>
      <td nowrap>
         <p class="ListText">
          <%=drawerName%>
	     </p>
      </td>
      <td></td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /***********************************************************
  * Collection Amount Row - static display of stored data
  ***********************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <%
     if(!isProcessedByBank)
      {
   %>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
	  <%=resMgr.getText("ExportCollectionAmend.CollectionAmount", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="25" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">
          <%=currencyCode%>
	</p>
      </td>
      <td width="5" nowrap>&nbsp;</td>
      <td nowrap align="right"> 
        <p class="ListText">
          <%=TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), 
                                              currencyCode, loginLocale)%>
	</p>
      </td>
      <td width="25" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="100%">&nbsp;</td>
    </tr>
   <%
	}
   %>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp; </td>
      <td>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">&nbsp;</p>
      </td>
      <td>&nbsp;</td>
      <td class="ListText" align="right">&nbsp; </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

  <%
  /*************************************************
  * NEW Amount of Collection Row - 
  *************************************************/
  %>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
	  <%=resMgr.getText("ExportCollectionAmend.NewAmount", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">
          <%=currencyCode%>
	</p>
      </td>
      <td>&nbsp;</td>
      <td align="right"> 
        <p class="ListText">
          <%=InputField.createTextField("TransactionAmount", displayAmount,
                                        "22", "22", "ListTextRight", isReadOnly)%>
        </p>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td width=100%>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /*************************************************
  * Amendment Details Text Area 
  *************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap class="ListText" colspan="5"> 
        <p class="ListText">
	  <%=resMgr.getText("ExportCollectionAmend.AdditionalAmendmenttext", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td class="ListText">&nbsp; </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" align="left" valign="bottom" nowrap width="500"> 
        <%=InputField.createTextArea("AmendmentDetails", 
                                     terms.getAttribute("amendment_details"), 
                                     "100", "3", "ListText", isReadOnly )%>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /*********************************************************************************************************
  * Start of Instructions To Bank Section - beige bar
  **********************************************************************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
	   <%=resMgr.getText("ExportCollectionAmend.InsttoBank", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	   <a name="InstructionstoBank"></a> 
        </p>
      </td>
      <td width="100%" class="BankColor" height="35" valign="top">&nbsp; </td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="90" class="BankColor" height="35" nowrap> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
          <tr> 
            <td align="center" valign="middle" height="17"> 
              <p class="ButtonLink">
	 	<a href="#top" class="LinksSmall">
                  <%=resMgr.getText("common.TopOfPage", 
                                    TradePortalConstants.TEXT_BUNDLE)%>
		</a>
	      </p>
            </td>
          </tr>
        </table>
      </td>
      <td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" align="right" valign="middle" width="15" height="35" nowrap>

<%    // -- Online Help button goes here -- (fileName, anchor, resMgr)
	out.print( OnlineHelp.createContextSensitiveLink( "customer/amend_exp_coll.htm", 
							  "instr_to_bank", resMgr, userSession) );
%>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /*************************************************
  *  Action Radio buttons action is/not required
  *************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ListText">
	   <%=resMgr.getText("ExportCollectionAmend.InstsenttoBank", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="30" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
          <%=InputField.createRadioButtonField("BankActionRequired", 
                     TradePortalConstants.INDICATOR_YES, "", 
                     bankActionRequired.equals(TradePortalConstants.INDICATOR_YES),
                     "ListText", "", isReadOnly )%>
      </td>
      <td width="5" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ListText">
	   <%=resMgr.getText("ExportCollectionAmend.YourAction", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	  <span class="ControlLabel">
	   <%=resMgr.getText("ExportCollectionAmend.actionrequired", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	  </span> 
	   <%=resMgr.getText("ExportCollectionAmend.OnThisAmend", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
          <%=InputField.createRadioButtonField("BankActionRequired", 
                     TradePortalConstants.INDICATOR_NO, "", 
                     bankActionRequired.equals(TradePortalConstants.INDICATOR_NO),
                     "ListText", "", isReadOnly )%>
      </td>
      <td>&nbsp;</td>
      <td nowrap>
        <p class="ListText">
	   <%=resMgr.getText("ExportCollectionAmend.YourAction", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	  <span class="ControlLabel">
	   <%=resMgr.getText("ExportCollectionAmend.actionnotrequired", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	  </span> 
	   <%=resMgr.getText("ExportCollectionAmend.OnThisAmend", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /*************************************************
  *  Special Instructions Dropdown
  *************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap class="ListText" colspan="5"> 
        <p class="ListText">
	   <%=resMgr.getText("ExportCollectionAmend.SpecialInstrucionsText", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td class="ListText">&nbsp; </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" align="left" valign="bottom" nowrap width="500"> 
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          out.print(InputField.createSelectField("SpecInstrPhraseItem", "", 
                   defaultText, options, "ListText", isReadOnly,
                   "onChange=" + PhraseUtility.getPhraseChange(
                                            "/Terms/special_bank_instructions",
                                            "SpclBankInstructions", "1000")));
  /*************
  * Add Button
  **************/
  %>
          <jsp:include page="/common/PhraseLookupButton.jsp">
            <jsp:param name="imageName" value='GoodsDescButton' />
          </jsp:include>
  <%
        }
  %>
	  <br>
  <%
  /*************************************************
  *  Special Instructions TextArea Box
  *************************************************/
  %>
        <%=InputField.createTextArea("SpclBankInstructions", 
                                     terms.getAttribute("special_bank_instructions"), 
                                     "100", "3", "ListText", isReadOnly)%>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /*********************************************************************************************************
  * Start of New Payment Terms Section - beige bar
  **********************************************************************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
	  <span class="Tabtext">
	    <span class="Links">
	    </span>
	    <span class="ControlLabelWhite">
	    <%=resMgr.getText("ExportCollectionAmend.NewPaymentTerms", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	    <a name="NewPaymentTerms"></a>
	    </span>
	  </span>
	</p>
      </td>
      <td width="90%" class="BankColor" height="35" valign="top">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="90" class="BankColor" height="35" nowrap> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
          <tr> 
            <td align="right" valign="middle" width="8" height="17">&nbsp;</td>
            <td align="center" valign="middle" height="17"> 
              <p class="ButtonLink">
	 	<a href="#top" class="LinksSmall">
                  <%=resMgr.getText("common.TopOfPage", 
                                    TradePortalConstants.TEXT_BUNDLE)%>
		</a>
	      </p>
            </td>
            <td align="left" valign="middle" width="8" height="17">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" align="right" valign="middle" width="15" height="35" nowrap>

<%    // -- Online Help button goes here -- (fileName, anchor, resMgr)
	out.print( OnlineHelp.createContextSensitiveLink( "customer/amend_exp_coll.htm", 
							  "new_terms", resMgr, userSession) );
%>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /************
  *  Tenor #1
  ************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">
	    <%=resMgr.getText("ExportCollectionAmend.tenor", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="bottom">&nbsp;</td>
      <td width="5" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="bottom">&nbsp;</td>
      <td width="20" nowrap>&nbsp;</td>
      <td align="left" nowrap valign="bottom">&nbsp;</td>
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="bottom">&nbsp;</td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="bottom"> 
        <p class="ControlLabel">
	    <%=resMgr.getText("ExportCollectionAmend.Tenor1", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="5" nowrap>&nbsp; </td>
      <td nowrap align="left" valign="bottom"> 
     <%
     /********************
     * Tenor 1 description
     ********************/
     %>
          <%=InputField.createTextField("Tenor1",
                                        terms.getAttribute("tenor_1"),
                                        "35", "35", "ListText", isReadOnly)%>
      </td>
      <td width="20" nowrap>&nbsp; </td>
      <td align="left" nowrap valign="bottom" class="ListText"> 
     <%
     /*******************
     * Tenor 1 Amount
     *******************/
     %>
        <p class="ListText">
	  <span class="ControlLabel">
	    <%=resMgr.getText("ExportCollectionAmend.Amount", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </span>
	  <br>
<%
	  String amount;
	  amount = terms.getAttribute( "tenor_1_amount" );

  	  if(!getDataFromDoc)
     		displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);
  	  else
     		displayAmount = amount;
%>
          <%=InputField.createTextField("Tenor1Amount", displayAmount,
                                        "22", "22", "ListText", isReadOnly)%>
        </p>
      </td>
      <td width="20" nowrap>&nbsp; </td>
      <td nowrap align="left" valign="bottom" class="ListText"> 
     <%
     /*********************
     * Tenor 1 Draft Number
     *********************/
     %>
        <p class="ListText">
	  <span class="ControlLabel">
	    <%=resMgr.getText("ExportCollectionAmend.DraftNumber", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </span>
	  <br>
          <%=InputField.createTextField("Tenor1DraftNumber",
                                        terms.getAttribute("tenor_1_draft_number"),
                                        "15", "15", "ListText", isReadOnly)%>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>

<%-- ******************************************************************************************* --%>
<%-- Tenors 2 - 6 are coded with the include directive tags because to use the jsp:include tags  --%>
<%-- I would have needed to create another Bean for the ResMgr so that the getText method could  --%>
<%-- be called.  This seemed a little expensive since I'm only really calling that method once   --%>
<%-- per jsp:include call.  Esentially the use of variables and the include directive tags       --%>
<%-- is the same exact function, but I don't have to create new objects in memory nor include    --%>
<%-- the packages again.									 --%>
<%-- ******************************************************************************************* --%>
<%
  for(int x=2; x<=NUMBER_OF_TENORS; x++)
  {


  /****************
  * Tenor #X Row
  ****************/

  label         = "ExportCollectionAmend.Tenor" + x;
  descName      = "Tenor" + x;
  descAttName   = "tenor_" + x;
  amountName    = "Tenor" + x + "Amount";
  amountAttName = "tenor_" + x + "_amount";
  draftName     = "Tenor" + x + "DraftNumber";
  draftAttName  = "tenor_" + x + "_draft_number";
%>

    <%@ include file="Transaction-EXP_OCO-AMD-Tenors.frag" %>
<%
  }
%>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
