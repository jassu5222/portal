<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *  Vasavi CR 524 03/31/2010
--%>
<%--
*******************************************************************************
                 Export Collection Trace Page - all sections

  Description:
    Contains HTML to create the Export Collection Trace page.  All data retrieval 
  is handled in the Transaction-EXP_OCO-TRC.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_OCO-TRC-html.jsp" %>
*******************************************************************************
--%>
<%
	String bankActionRequired = terms
			.getAttribute("bank_action_required");
%>
<div>
	
	 <%=resMgr.getText("ExportCollectionAmend.InstsenttoBank", 
                             TradePortalConstants.TEXT_BUNDLE)%>                   
      <br/> <br/>   
      <%=widgetFactory.createRadioButtonField("BankActionRequired","TradePortalConstants.INDICATOR_YES","",
    		  TradePortalConstants.INDICATOR_YES,bankActionRequired.equals(TradePortalConstants.INDICATOR_YES), isReadOnly) %>         
      
      <%=resMgr.getText("ExportCollectionAmend.YourAction", 
                             TradePortalConstants.TEXT_BUNDLE)%>
                             
	  
	   <b><%=resMgr.getText("ExportCollectionAmend.actionrequired", 
                             TradePortalConstants.TEXT_BUNDLE)%></b>
	   
	   <%=resMgr.getText("ExportCollectionAmend.OnThisAmend", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	
       <br> 
       <%=widgetFactory.createRadioButtonField("BankActionRequired","TradePortalConstants.INDICATOR_NO","",TradePortalConstants.INDICATOR_NO,bankActionRequired.equals(TradePortalConstants.INDICATOR_NO), isReadOnly) %>           
      
	   <%=resMgr.getText("ExportCollectionAmend.YourAction", 
                             TradePortalConstants.TEXT_BUNDLE)%>
                             
	   <B><%=resMgr.getText("ExportCollectionAmend.actionnotrequired", 
                             TradePortalConstants.TEXT_BUNDLE)%></B>

	   <%=resMgr.getText("ExportCollectionAmend.OnThisAmend", 
                             TradePortalConstants.TEXT_BUNDLE)%>
       
	
	<%
		if (isReadOnly) {
			out.println("&nbsp;");
		} else {
			options = ListBox.createOptionList(spclInstrDocList,
					"PHRASE_OID", "NAME", "", userSession.getSecretKey());
			defaultText = resMgr.getText("transaction.SelectPhrase",
					TradePortalConstants.TEXT_BUNDLE);
	%>
	<br/><br/>  
	<%=widgetFactory.createSelectField(
								"SpecInstrPhraseItem",
								"",
								defaultText,
								options,
								isReadOnly,
								false,
								false,
								"onChange="
										+ PhraseUtility
												.getPhraseChange(
														"/Terms/special_bank_instructions",
														"SpclBankInstructions",
														"5000",
														"document.forms[0].SpclBankInstructions"),
								"", "")%>
	<%
		}
	%>
	
	
	<%=widgetFactory.createTextArea("SpclBankInstructions", "", terms.getAttribute("special_bank_instructions"),
					isReadOnly, false, false, "rows='10';", "", "")%>
	<%if(!isReadOnly) {%>
		<%=widgetFactory.createHoverHelp("SpclBankInstructions", "DirectDebitCollection.SpecialInstToolTip") %>	
	<%} %>
</div>