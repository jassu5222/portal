<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *  Vasavi CR 524 03/31/2010 
--%>
<%--
*******************************************************************************
                 Export Collection Amend Page - all sections

  Description:
    Contains HTML to create the Export Collection Amend page.  All data retrieval 
  is handled in the Transaction-EXP_OCO-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_OCO-AMD-html.jsp" %>
*******************************************************************************
--%>

<div>
	
	<table class="formDocumentsTable" width="100%">
		<thead>
			<tr>
				<th width="15%">&nbsp;</th>
				<th style="text-align:left;font-weight: bold;"><%=resMgr.getText("ExportCollectionAmend.TenorColumn", TradePortalConstants.TEXT_BUNDLE)%></th>
				<th style="text-align:left;font-weight: bold;"><%=resMgr.getText("ExportCollectionAmend.Amount", TradePortalConstants.TEXT_BUNDLE)%></th>
				<th style="text-align:left;font-weight: bold;"><%=resMgr.getText("ExportCollectionAmend.DraftNumber", TradePortalConstants.TEXT_BUNDLE)%></th>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td><%=resMgr.getText("ExportCollectionAmend.Tenor1",TradePortalConstants.TEXT_BUNDLE)%></td>
				<td><%=widgetFactory.createTextField("Tenor1", "",
					terms.getAttribute("tenor_1"), "35", isReadOnly, false, false,
					"class='char35'", "", "none")%></td>
				<%
					String amount;
					amount = terms.getAttribute("tenor_1_amount");

					if (!getDataFromDoc)
						displayAmount = TPCurrencyUtility.getDisplayAmount(amount,
								currencyCode, loginLocale);
					else
						displayAmount = amount;
				%>

				<td><%=widgetFactory.createTextField("Tenor1Amount", "",
					displayAmount, "15", isReadOnly, false, false, "class='char10'", "",
					"none")%></td>
				<td><%=widgetFactory.createTextField("Tenor1DraftNumber", "",
					terms.getAttribute("tenor_1_draft_number"), "15", isReadOnly,
					false, false, "class='char20'", "", "none")%></td>
			</tr>
			<%
				for (int x = 2; x <= NUMBER_OF_TENORS; x++) {

					/****************
					 * Tenor #X Row
					 ****************/

					label = "ExportCollectionAmend.Tenor" + x;
					descName = "Tenor" + x;
					descAttName = "tenor_" + x;
					amountName = "Tenor" + x + "Amount";
					amountAttName = "tenor_" + x + "_amount";
					draftName = "Tenor" + x + "DraftNumber";
					draftAttName = "tenor_" + x + "_draft_number";
			%>

			<%@ include file="Transaction-EXP_OCO-AMD-Tenors.frag"%>
			<%
				}
			%>
		</tbody>
	</table>
</div>