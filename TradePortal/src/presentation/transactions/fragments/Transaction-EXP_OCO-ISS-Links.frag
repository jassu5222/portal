<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Export Collection Issue Page - Link section

  Description:
    Contains HTML to display links to the other sections.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_OCO-ISS-Links.jsp" %>
*******************************************************************************
--%>
<%
	boolean pdfFlag = true;
%>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	  <a href="#General">
		<%=resMgr.getText("ExportCollectionIssue.1General", 
                              	   TradePortalConstants.TEXT_BUNDLE)%>
	  </a>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	  <a href="#DocumentsPresented">
		<%=resMgr.getText("ExportCollectionIssue.2DocumentsPresented", 
                              	   TradePortalConstants.TEXT_BUNDLE)%>
	  </a>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td nowrap height="30" width="15">&nbsp;</td>
      <td nowrap height="30"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	  <a href="#CollectionInstructions">
		<%=resMgr.getText("ExportCollectionIssue.3CollectionInstructions", 
                              	   TradePortalConstants.TEXT_BUNDLE)%>
	  </a>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	  <a href="#InstructionstoBank"> 
		<%=resMgr.getText("ExportCollectionIssue.4InstructionstoBank", 
                              	   TradePortalConstants.TEXT_BUNDLE)%>
	  </a>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td height="30">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	  <a href="#MultipleTenor">
		<%=resMgr.getText("ExportCollectionIssue.5MultipleTenor", 
                              	   TradePortalConstants.TEXT_BUNDLE)%>
	  </a>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> <%-- If We need to display the Payment Instructions screen   --%>
        <p class="ControlLabel">               <%-- (based on a found error) Then we need to show it's Link --%>
<%
      //*******************************************************************************************************
      //This layout is a little misleading-
      //Either the user is an Admin user which means they'll get the Bank Defined Link-in this specific case
      //they will only be able to get to the Export Collection page via a template and wont be able to see the
      //Payment Instructions link since that validation will never get tripped.  
      //OR - they might (not always the case) have failed validation for the payment Instructions section...
      //in this event it might be necessary to display the Payment Instructions section based on the flags 
      //value.  
      //** Lastly ** It may be necessary to display the PDF document links.  If neither of the prior two links
      //were displayed already then we'll need to start with the PDF Links here - otherwise we'll need to start
      //a new line with the 2 links.  The flag 'pdfFlag' is used to track whether or not we've display the PDF
      //Links in the event that neither the Payment Instructions link nor the Bank Defined link was displayed.
      //*******************************************************************************************************

      if (paymtInstr.equals( TradePortalConstants.PMT_INSTR_YES ) ){
%>
	  <a href="#PaymentInstructions">
		<%=resMgr.getText("ExportCollectionIssue.6PaymentInstructions", 
                              	   TradePortalConstants.TEXT_BUNDLE)%>
	  </a>

<%    }else if (userSession.getSecurityType().equals(TradePortalConstants.ADMIN) ){
%>
	  <a href="#BankDefined">
		<%=resMgr.getText("ExportCollectionIssue.6BankDefined", 
                              	   TradePortalConstants.TEXT_BUNDLE)%>
	  </a>

<%    }else if ( (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_STARTED)) &&
	         (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_DELETED)) && 
			 (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK)) ){

          // Create the "6. Export Collection" pdf link
          linkArgs[0] = TradePortalConstants.PDF_NEW_EXPORT_COLLECTION;
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
          linkArgs[2] = "en_US";
          linkArgs[3] = userSession.getBrandingDirectory();
%>
  	  <%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("ExportCollectionIssue.ExportCollection1", 
                              	   	 	                    TradePortalConstants.TEXT_BUNDLE), 
					             "CollectionSchedule",
                                                     linkArgs,
                                                     response) %>
      </p>
    </td>
    <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
    <tr> 					
      <td width="14" nowrap>&nbsp;</td>		
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
<%
          // Create the "7. Bill of Exchanges pdf link
          linkArgs[0] = TradePortalConstants.PDF_BILL_OF_EXCHANGES;
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
          linkArgs[2] = "en_US";
          linkArgs[3] = null;
%>
  	  <%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("ExportCollectionIssue.BillOfExchanges1", 
                              	   	 	                    TradePortalConstants.TEXT_BUNDLE), 
					             TradePortalConstants.PDF_BILL_OF_EXCHANGES,
                                                     linkArgs,
                                                     response) %>
<%
     	pdfFlag = false;   //Already displayed the PDF Links - dont do it again
      }
%>
      </p>
    </td>
    <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
<%                                   //Need to re-review this logic - I think there's a bug here
				     //Because paymtInstr might be 'N' yet the status is not Started/deleted
   if ( (paymtInstr.equals("Y")) || 
        ((!InstrumentServices.isBlank( terms.getAttribute("payment_instructions"))) &&
         (pdfFlag == true) ) ) {
      				//under this condition then we need to display a PDF links...
      if( (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_STARTED)) &&
	  (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_DELETED)) ) {
%>
    <tr> 					
      <td width="14" nowrap>&nbsp;</td>		
      <td nowrap align="left" valign="middle">  
	<p class="ControlLabel">
<%
          // Create the "7. Export Collection pdf link
          linkArgs[0] = TradePortalConstants.PDF_NEW_EXPORT_COLLECTION;
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
          linkArgs[2] = "en_US";
          linkArgs[3] = userSession.getBrandingDirectory();
%>
  	  <%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("ExportCollectionIssue.ExportCollection2", 
                              	   	 	                    TradePortalConstants.TEXT_BUNDLE), 
					             TradePortalConstants.PDF_NEW_EXPORT_COLLECTION,
                                                     linkArgs,
                                                     response) %>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">
        <p class="ControlLabel">
<%
          // Create the "8. Bill of Exchanges pdf link
          linkArgs[0] = TradePortalConstants.PDF_BILL_OF_EXCHANGES;
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
          linkArgs[2] = "en_US";
          linkArgs[3] = null;
%>
  	  <%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("ExportCollectionIssue.BillOfExchanges2", 
                              	   	 	                    TradePortalConstants.TEXT_BUNDLE), 
					             TradePortalConstants.PDF_BILL_OF_EXCHANGES,
                                                     linkArgs,
                                                     response) %>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
<%
   }
  }
%>
  </table>
