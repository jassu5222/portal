<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Export Collection Issue Page - Payment Instructions section

  Description:
    Contains HTML to create the Export COL Issue Payment Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_COL-ISS-PaymentInstructions.jsp" %>
*******************************************************************************
--%>

  <%
  /*********************************************************
  * Start of Payment Instructions Section - beige bar
  **********************************************************/
  %>
  <%
  /********************************
  * Enter Payment Instructions
  ********************************/
  %>
     <span class="formItem">
              <%=widgetFactory.createSubLabel("ExportCollectionIssue.EnterPaymentInstructions") %></span>
	
  <%
  /************************************
  * Payment Instructions- Dropdown box
  *************************************/
  %>
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(paymentInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          
          out.println(widgetFactory.createSelectField("PaymentInstrPhraseItem", "", defaultText, options, isReadOnly, false, false, 
        		  "onChange=" + PhraseUtility.getPhraseChange(
                          "/Terms/payment_instructions",
                          "PaymentInstructionsText", "1000","document.forms[0].PaymentInstructionsText"), "", ""));
        }
%>
  <%
  /************************************
  * Payment Instructions- TextArea box
  *************************************/
  %>
	<br>
	    <%= widgetFactory.createTextArea("PaymentInstructionsText", "", terms.getAttribute("payment_instructions"), true, false, false, "", "","" ) %>
                                    
              
  <%--
   As per IR GOUF012571384, Payment Instruction phrase texts should be displayed
   as normal editable text area instead of present method of display as table
   so as to enable users to edit phrase text like other text area fields.
   Following 2 code modifications done to accomplish.
   1. In above terms.getAttribute method call, changed last parameter to 'isReadOnly' 
   instead of  'true'.
   2. Below call to secureParms is comment out
   Developer :  KS        Date: 22nd Dec 2006                                   
  --%>
  	<%  //Have to put the value explicitly into the Hashtable since the
	    //Text area is read only - it creates a table and not an input 
	    //textbox...This has to be put into secureParms since this not a 
            //normal control rather - a read only text display.
	  // secureParms.put("PaymentInstructionsText", terms.getAttribute("payment_instructions")); 
	%>
