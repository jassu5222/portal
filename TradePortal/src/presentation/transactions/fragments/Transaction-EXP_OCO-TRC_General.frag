<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *  Vasavi CR 524 03/31/2010
--%>
<%--
*******************************************************************************
                 Export Collection Trace Page - all sections

  Description:
    Contains HTML to create the Export Collection Trace page.  All data retrieval 
  is handled in the Transaction-EXP_OCO-TRC.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_OCO-TRC-html.jsp" %>
*******************************************************************************
--%>
<%
	String tracerSendType = terms.getAttribute("tracer_send_type");
%>
<div>
	<div>		
	<div class="inline formItem readOnly" >
		<label for="exportRefNoLabel"><%=resMgr.getText("ExportCollectionAmend.YourRefNo", TradePortalConstants.TEXT_BUNDLE)%></label><br>
		<span class="fieldValue"><%= (terms.getAttribute("reference_number")==null
				||terms.getAttribute("reference_number").trim().equals(""))?"&nbsp;":terms.getAttribute("reference_number") %></span>
	</div>
	
	<%String tempCollAmt = TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), currencyCode, loginLocale); %>
	<div class="inline formItem readOnly" >
	<div class="formItem inline">
	<div class="formItem inline">
		<label for="exportDrawNameLabel"><%=resMgr.getText("ExportCollectionTrace.CollectionAmount", TradePortalConstants.TEXT_BUNDLE)%></label><br>
			<%=currencyCode%>
    	<span class="fieldValue"><%= (tempCollAmt==null||tempCollAmt.trim().equals(""))?"&nbsp;":tempCollAmt %></span>
    </div></div>
	</div>
		
	</div>
	<div style="clear: both;"></div>

	
	<%=widgetFactory.createTextArea("TracerDetails",
					"ExportCollectionTrace.AdditionalTracertext",
					terms.getAttribute("tracer_details"), isReadOnly, false,
					false, "rows='10';", "", "")%>
	<%if(!isReadOnly){%>
		<%=widgetFactory.createHoverHelp("TracerDetails", "ExportCollectionTrace.TracerDetailToolTip") %>
	<%} %>
</div>
