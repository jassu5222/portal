<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
    Fund Transfer Request Page - Bank Instructions section

  Description:
    Contains HTML to create the Funds Transfer Request Bank Instructions section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTRQ-ISS-BankInstructions.jsp" %>
*******************************************************************************
--%>

	<div> 
			<%  options = Dropdown.createSortedRefDataOptions(
                      TradePortalConstants.INSTRUMENT_LANGUAGE, 
                      instrument.getAttribute("language"), 
                      loginLocale);
         		out.println(widgetFactory.createSelectField( "InstrumentLanguage", "FundsTransferRequest.IssueInstrumentIn","" , options, isReadOnly,true,false, "", "", ""));                      
			%>
	
			
		<%
	        if (isReadOnly || isFromExpress) {
	          out.println("&nbsp;");
	        } else {
				options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
				defaultText = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);
	            out.println(widgetFactory.createSelectField( "CommInvPhraseItem", "FundsTransferRequest.SpecialInstructionsForBank",defaultText , options, isReadOnly || isFromExpress,
					              false,false,  "onChange=" + PhraseUtility.getPhraseChange(
	                                              "/Terms/special_bank_instructions",
	                                              "SpclBankInstructions",
	                                              "1000","document.forms[0].SpclBankInstructions"), "", ""));  
       } %>
        
       <%= widgetFactory.createTextArea( "SpclBankInstructions","",terms.getAttribute("special_bank_instructions"), isReadOnly || isFromExpress, false, false, 
						"maxlength='1000'", "", "") %>   
						<%= widgetFactory.createHoverHelp("SpclBankInstructions","SpclBankInstructions.PlaceHolderAddlConditions")%>                    
	</div>		

<div style="clear:both;"></div>