<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
*******************************************************************************
  Funds Transfer Request Page - General section

  Description:
    Contains HTML to create the Funds Transfer Request General section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTRQ-ISS-General.jsp" %>
*******************************************************************************
--%>

<%if(isTemplate){ %>
		<%
      if ((InstrumentType.DOM_PMT.equals(instrument.getAttribute("instrument_type_code")))||
          (InstrumentType.XFER_BET_ACCTS.equals(instrument.getAttribute("instrument_type_code")))||
          (InstrumentType.FUNDS_XFER.equals(instrument.getAttribute("instrument_type_code"))))
      {
         UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
         thisUser.setAttribute("user_oid", userSession.getUserOid());
         thisUser.getDataFromAppServer();
         String confInd = "";
         if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))
         	confInd = TradePortalConstants.INDICATOR_YES;
         else if (userSession.getSavedUserSession() == null)
            confInd = thisUser.getAttribute("confidential_indicator");
         else
         {
            if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
                confInd = TradePortalConstants.INDICATOR_YES;
            else
                confInd = thisUser.getAttribute("subsid_confidential_indicator");
         }
         if (TradePortalConstants.INDICATOR_YES.equals(confInd))
         {
            //IAZ CR-586 IR-PRUK092452162 09/29/10: CONF IND for TEMPLATES is T (vs Y).
            //           This fix is for templates created from Instruments (so Y is copied over and needs to be replaced by N in Terms)
            if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("confidential_indicator")))
               terms.setAttribute("confidential_indicator", TradePortalConstants.INDICATOR_NO);
      %>
        <%=widgetFactory.createCheckboxField("ConfidentialIndicatorBox", "TemplateDetail.ConfidentialPmtLabel",
        		terms.getAttribute("confidential_indicator").equals(TradePortalConstants.CONF_IND_FRM_TMPLT),
				isReadOnly,false,"onClick=setConfidentialIndicator()","","")%>

            <input type=hidden name = ConfidentialIndicator value=<%=terms.getAttribute("confidential_indicator")%>>
            <div id="Confidential Payment"><input type=hidden name = ConfidentialIndicatorTemplate value=<%=terms.getAttribute("confidential_indicator")%>>

            </div>
      <%
         }
      }
      %>
    <%} %>
<%
  if (!isTemplate) {
    String confType = resMgr.getText("TemplateDetail.ConfidentialPmtLabel", TradePortalConstants.TEXT_BUNDLE);
%>
  <%@ include file="TransactionConfInd.frag" %>
<%
  }
%>

<div class="columnLeft">    <%--  GENERAL LEFT CONTENT  MAIN DIV --%>

  <%= widgetFactory.createSubsectionHeader( "FundsTransferRequest.Payer",isReadOnly,false, isFromExpress,"" ) %>

 <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
 <%--
  <%= widgetFactory.createTextArea("Payer", "FundsTransferRequest.ApplicantDetails",
        termsPartyPayer.buildAddress(false, resMgr), true,false,false,"onFocus='this.blur();' rows='4'","","")  %>
--%>
	 <%= widgetFactory.createAutoResizeTextArea("Payer", "FundsTransferRequest.ApplicantDetails", termsPartyPayer.buildAddress(false, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	

<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
<%
  secureParms.put("payer_terms_party_oid", termsPartyPayer.getAttribute("terms_party_oid"));
%>
  <%--cquinton 1/3/2012 move hidden inputs to end of section to help with ie7 spacing--%>

  <%= widgetFactory.createTextField( "PayerRefNum", "FundsTransferRequest.PayerRefNumber",
        terms.getAttribute("reference_number"), "30", isReadOnly, !isTemplate, false,  "class='char32'", "","" ) %>

<%
  String identifierStr ="PayeeName,PayeeAddressLine1,PayeeAddressLine2,PayeeCity,PayeeStateProvince,PayeeCountry,PayeePostalCode,PayeeOTLCustomerId";
  String sectionName="beneficiary";
  String benSearchHtml = "";
  String benClearHtml = "";
  if (!(isReadOnly)) {
    benSearchHtml = widgetFactory.createPartySearchButton(identifierStr,sectionName,false,TradePortalConstants.PAYEE,false);
    benClearHtml = widgetFactory.createPartyClearButton( "ClearBenButton", "clearPayee()",false,"");
  }
%>

  <%= widgetFactory.createSubsectionHeader( "FundsTransferRequest.Payee",isReadOnly,!isTemplate, isFromExpress,benSearchHtml+benClearHtml ) %>

  <%--cquinton 1/3/2012 move hidden inputs to end of section to help with ie7 spacing--%>

<%
  // Always send the payee terms party oid
  secureParms.put("payee_terms_party_oid", termsPartyPayee.getAttribute("terms_party_oid"));
%>

  <%= widgetFactory.createTextField( "PayeeName", "FundsTransferRequest.PayeeName",
        termsPartyPayee.getAttribute("name"), "20", isReadOnly || isFromExpress, !isTemplate, false,  "class='char35' onBlur='checkPayeeName(\"" +
          StringFunction.escapeQuotesforJS(termsPartyPayee.getAttribute("name")) + "\")'", "","" ) %>

  <%= widgetFactory.createTextField( "PayeeAddressLine1", "FundsTransferRequest.AddressLine1", termsPartyPayee.getAttribute("address_line_1"), "35", isReadOnly, !isTemplate, false,  "", "","" ) %>
  <%= widgetFactory.createTextField( "PayeeAddressLine2", "FundsTransferRequest.AddressLine2", termsPartyPayee.getAttribute("address_line_2"), "35", isReadOnly, false, false,  "", "","" ) %>
  <%= widgetFactory.createTextField( "PayeeCity", "FundsTransferRequest.City", termsPartyPayee.getAttribute("address_city"), "16", isReadOnly, !isTemplate, false,  "class='char32'", "","" ) %>
  <%= widgetFactory.createTextField( "PayeeStateProvince", "FundsTransferRequest.ProvinceState", termsPartyPayee.getAttribute("address_state_province"), "8", isReadOnly, false, false,  "class='char32'", "","" ) %>

<%
  options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY, termsPartyPayee.getAttribute("address_country"), loginLocale);
%>

  <%= widgetFactory.createSelectField( "PayeeCountry", "FundsTransferRequest.Country"," " , options, isReadOnly,  !isTemplate,false,  "class='char32'", "", "") %>
  <%= widgetFactory.createTextField( "PayeePostalCode", "FundsTransferRequest.PostalCode", termsPartyPayee.getAttribute("address_postal_code"), "15", isReadOnly, false, false,  "class='char15'", "","" ) %>

  <%--cquinton 1/4/2013 the beneficiary account number field can be either a text box or a set of radio buttons.
      we set the label here and put content in special div that is replaced via ajax as necessary.
      We use a partial page loading section to give a visual indicator--%>
  <div class='formItem<%=(!isTemplate)?" required":""%>'>

    <%= widgetFactory.createInlineLabel("SelectedAccount", "FundsTransferRequest.PayeeAccountNumber") %>
    <div style="clear:both;"></div>

    <div id="beneAccountsDataLoading" style="display:none;"> <%--hide it to start--%>
      <span class='dijitInline dijitIconLoading'></span>
        <%=resMgr.getText("common.Loading", TradePortalConstants.TEXT_BUNDLE)%>
    </div>
    <div id="beneAccountsData">
<%
  //setup the include. we pass following variables
  TermsPartyWebBean termsParty = termsPartyPayee;
%>
      <%@ include file="Transaction-FTRQ-ISS-BeneAcctData.frag" %>
    </div>
  </div>

 <%= widgetFactory.createTextArea( "PayeeMessage","FundsTransferRequest.PayeeMessageText",terms.getAttribute("message_to_beneficiary"), isReadOnly,false, false, 
			"maxlength='210'", "", "") %>     

</div>  <%-- GENERAL LEFT CONTENT  MAIN DIV CLOSE --%>


<div class="columnRight">  <%-- GENERAL LEFT CONTENT  MAIN DIV START --%>

<%
  String identifierStr1 ="PayeeBankName,PayeeBankAddressLine1,PayeeBankAddressLine2,PayeeBankCity,PayeeBankStateProvince,PayeeBankCountry,PayeeBankPostalCode,PayeeBankOTLCustomerId";
  String sectionName1="beneficiary_bank";
  String benBankSearchHtml = "";
  String benBankClearHtml = "";
  if (!(isReadOnly)) {
    benBankSearchHtml = widgetFactory.createPartySearchButton(identifierStr1,sectionName1,false,TradePortalConstants.PAYEE_BANK,false);
    benBankClearHtml = widgetFactory.createPartyClearButton( "ClearBenBankButton", "clearPayeeBankBank()",false,"");
  }
%>

  <%= widgetFactory.createSubsectionHeader( "FundsTransferRequest.BeneBank",isReadOnly,false, isFromExpress,benBankSearchHtml+benBankClearHtml) %>
  <%-- <div class="subsectionShortHeader"><h3>Beneficiary</h3><a name="PartySearch" onClick="SearchParty('<%=identifierStr1%>', '<%=sectionName1%>');"><img class="searchButton" src="/portal/themes/default/images/search_icon.png"></a></div>---%>

<%
  // Always send the payee_bank terms party oid
  secureParms.put("payee_bank_terms_party_oid",  termsPartyPayeeBank.getAttribute("terms_party_oid"));
%>

  <%--cquinton 1/3/2012 move hidden inputs to end of section to help with ie7 spacing--%>

  <%= widgetFactory.createTextField( "PayeeBankName", "FundsTransferRequest.PayeeBankName", termsPartyPayeeBank.getAttribute("name"), "20", isReadOnly || isFromExpress, false, false,  "class='char35' onBlur='checkPayeeBankName(\"" +
										  StringFunction.escapeQuotesforJS(termsPartyPayeeBank.getAttribute("name")) + "\")'", "","" ) %>
  <%= widgetFactory.createTextField( "PayeeBankAddressLine1", "FundsTransferRequest.AddressLine1", termsPartyPayeeBank.getAttribute("address_line_1"), "35", isReadOnly, false, false,  "", "","" ) %>
  <%= widgetFactory.createTextField( "PayeeBankAddressLine2", "FundsTransferRequest.AddressLine2", termsPartyPayeeBank.getAttribute("address_line_2"), "35", isReadOnly, false, false,  "", "","" ) %>
  <%= widgetFactory.createTextField( "PayeeBankCity", "FundsTransferRequest.City", termsPartyPayeeBank.getAttribute("address_city"), "16", isReadOnly, false, false,  "class='char32'", "","" ) %>
  <%= widgetFactory.createTextField( "PayeeBankStateProvince", "FundsTransferRequest.ProvinceState", termsPartyPayeeBank.getAttribute("address_state_province"), "8", isReadOnly, false, false,  "class='char32'", "","" ) %>

<%
  options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY, termsPartyPayeeBank.getAttribute("address_country"),  loginLocale);
%>
  <%= widgetFactory.createSelectField( "PayeeBankCountry", "FundsTransferRequest.Country"," " , options, isReadOnly,false,false, "class='char32'", "", "") %>
  <%= widgetFactory.createTextField( "PayeeBankPostalCode", "FundsTransferRequest.PostalCode", termsPartyPayeeBank.getAttribute("address_postal_code"), "15", isReadOnly, false, false,  "class='char15'", "","" ) %>

</div>
<div style="clear:both;"></div>
	<% if (!isReadOnly){ %>
<%=widgetFactory.createHoverHelp("beneficiary", "InternationalPayment.Beneficiary") %>
<%=widgetFactory.createHoverHelp("beneficiary_bank", "InternationalPayment.BeneficiaryBank") %>
<%=widgetFactory.createHoverHelp("ClearBenButton", "PartyClearIconHoverHelp") %>
<%=widgetFactory.createHoverHelp("ClearBenBankButton", "PartyClearIconHoverHelp") %>
 <%} %>
<div style="clear:both;"></div>
<%--cquinton 1/3/2012 move hidden inputs to end of section to help with ie7 spacing--%>
<input type="hidden" name="PayerTermsPartyType"  value="<%=TradePortalConstants.PAYER%>">
<input type="hidden" name="PayerName"  value="<%=termsPartyPayer.getAttribute("name")%>">
<input type="hidden" name="PayerAddressLine1"  value="<%=termsPartyPayer.getAttribute("address_line_1")%>">
<input type="hidden" name="PayerAddressLine2"   value="<%=termsPartyPayer.getAttribute("address_line_2")%>">
<input type="hidden" name="PayerCity"   value="<%=termsPartyPayer.getAttribute("address_city")%>">
<input type="hidden" name="PayerStateProvince"   value="<%=termsPartyPayer.getAttribute("address_state_province")%>">
<input type="hidden" name="PayerCountry"  value="<%=termsPartyPayer.getAttribute("address_country")%>">
<input type="hidden" name="PayerPostalCode"   value="<%=termsPartyPayer.getAttribute("address_postal_code")%>">
<input type="hidden" name="PayerOTLCustomerId"   value="<%=termsPartyPayer.getAttribute("OTL_customer_id")%>">
<input type="hidden" name="PayerAddressSeqNum"  value="<%=termsPartyPayer.getAttribute("address_seq_num")%>">

<input type="hidden" name='PayeeTermsPartyType'     value=<%=TradePortalConstants.PAYEE%>>
<input type="hidden" name="PayeeOTLCustomerId"        value="<%=termsPartyPayee.getAttribute("OTL_customer_id")%>">
<input type="hidden" name="VendorId"               value="<%=termsPartyPayee.getAttribute("vendor_id")%>">

<input type="hidden" name='PayeeBankTermsPartyType' value=<%=TradePortalConstants.PAYEE_BANK%>>
<input type="hidden" name="PayeeBankOTLCustomerId"  value="<%=termsPartyPayeeBank.getAttribute("OTL_customer_id")%>">


<script LANGUAGE="JavaScript">

  function clearPayeeBankBank() {
    document.TransactionFTRQ.PayeeBankName.value = "";
    document.TransactionFTRQ.PayeeBankAddressLine1.value = "";
    document.TransactionFTRQ.PayeeBankAddressLine2.value = "";
    document.TransactionFTRQ.PayeeBankCity.value = "";
    document.TransactionFTRQ.PayeeBankStateProvince.value = "";
    <%-- document.TransactionFTRQ.PayeeBankCountry.value = ""; --%>
    document.TransactionFTRQ.PayeeBankPostalCode.value = "";
    <%-- document.TransactionFTRQ.PayeeBankPhoneNumber.value = ""; --%>
    document.TransactionFTRQ.PayeeBankOTLCustomerId.value = "";
    document.getElementById('PayeeBankCountry').value="";
  }

  function clearPayee() {
    document.TransactionFTRQ.PayeeName.value = "";
    document.TransactionFTRQ.PayeeAddressLine1.value = "";
    document.TransactionFTRQ.PayeeAddressLine2.value = "";
    document.TransactionFTRQ.PayeeCity.value = "";
    document.TransactionFTRQ.PayeeStateProvince.value = "";
    <%-- document.TransactionFTRQ.PayeeCountry.value = ""; --%>
    document.TransactionFTRQ.PayeePostalCode.value = "";
    <%-- document.TransactionFTRQ.PayeePhoneNumber.value = ""; --%>
    document.TransactionFTRQ.PayeeOTLCustomerId.value = "";
    document.getElementById('PayeeCountry').value="";
  }


  function checkPayeeName(originalName)
  {
     if (document.forms[0].PayeeName.value != originalName)
     {
        document.forms[0].PayeeOTLCustomerId.value = "";
     }
  }

  function checkPayeeBankName(originalName)
  {
	  <%-- 
     if (document.forms[0].PayeeBankName.value != originalName)
     {
        document.forms[0].PayeeBankOTLCustomerId.value = "";
     }
	 --%>
	  if(dijit.byId('PayeeBankName')){
    	  		if(dijit.byId('PayeeBankName').value != originalName){
    	  			if(null != document.getElementById('PayeeBankOTLCustomerId'))
    	  				document.getElementById('PayeeBankOTLCustomerId').value = "";
    	  		}
    	  	}
  }

  function clearPayer()
  {
    document.TransactionFTRQ.PayerName.value = "";
    document.TransactionFTRQ.PayerAddressLine1.value = "";
    document.TransactionFTRQ.PayerAddressLine2.value = "";
    document.TransactionFTRQ.PayerCity.value = "";
    document.TransactionFTRQ.PayerStateProvince.value = "";
    document.TransactionFTRQ.PayerCountry.value = "";
    document.TransactionFTRQ.PayerPostalCode.value = "";
    document.TransactionFTRQ.PayerOTLCustomerId.value = "";
    document.TransactionFTRQ.PayerPhoneNumber.value = "";
  }

</script>
