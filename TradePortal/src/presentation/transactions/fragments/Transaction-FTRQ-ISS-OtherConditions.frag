<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Funds Transfer Request Issue Page - Other Conditions section

  Description:
    Contains HTML to create the Funds Transfer Request Issue Other Conditions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTRQ-ISS-OtherConditions.jsp" %>
*******************************************************************************
--%>

	<div>  
			
		<%
			if (isReadOnly || isFromExpress) {
			          out.println("&nbsp;");
			} else {
				options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
			    defaultText = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);
			         
			    out.println(widgetFactory.createSelectField( "AddlDocsPhraseItem", "",defaultText , options, isReadOnly || isFromExpress,
				              false,false,  "onChange=" + PhraseUtility.getPhraseChange("/Terms/additional_conditions","AddlConditionsText", "5000","document.forms[0].AddlConditionsText"), "", ""));                  
				              
		    }
		%>
			
		<%= widgetFactory.createTextArea( "AddlConditionsText","",terms.getAttribute("additional_conditions"), isReadOnly || isFromExpress, false, false, 
						"maxlength='5000'", "", "") %>  
			<%= widgetFactory.createHoverHelp("AddlConditionsText","InternationalPayments.PlaceHolderAddlConditions")%>         
			
	</div>
	<div style="clear:both;"></div>