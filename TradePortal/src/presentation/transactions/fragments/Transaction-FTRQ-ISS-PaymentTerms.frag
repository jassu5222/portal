<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Funds Transfer Request Page - Payment Terms section

  Description:
    Contains HTML to create the Funds Transfer Request Payment Terms section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTRQ-ISS-PaymentTerms.jsp" %>
*******************************************************************************
--%>

<%
  // Get some attributes for radio button fields.  We need to refer to this
  // value frequently so do getAttribute once.

  String exchangeRateType = terms.getAttribute("ex_rt_variation_adjust");
  String bankChargesType = terms.getAttribute("bank_charges_type");

%>

<div class="columnLeft">  <%-- START GENERAL LEFT CONTENT  MAIN DIV --%>
<% 
  options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("amount_currency_code"), loginLocale);  
%>

  <div>
    <%= widgetFactory.createSelectField("TransactionCurrency", "FundsTransferRequest.CCY", " ",options, isReadOnly, !isTemplate, isExpressTemplate,"class='char4'","", "inline")%>
    <%= widgetFactory.createAmountField( "TransactionAmount", "FundsTransferRequest.Amount", displayAmount, currency, isReadOnly,!isTemplate, false, "onChange='updateFundingAmount();' class=char16 maxlength='22'", "readOnly:" +isReadOnly, "inline") %>
    <div style="clear: both;"></div>
  </div>
  <%-- Nar CR 966 Rel 9.2 09/16/2014 modified widget to make it read-Only if it is template --%>
  <%= widgetFactory.createDateField( "ReleaseDate", "FundsTransferRequest.ForReleaseByBankOn", date, 
        isReadOnly || isTemplate, !isTemplate, false,  "", dateWidgetOptions, "")%>

</div> <%-- END GENERAL LEFT CONTENT  MAIN DIV --%>

<div class="columnRight"> <%--  START GENERAL RIGHT CONTENT  MAIN DIV --%>
		<%=widgetFactory.createLabel("","FundsTransferRequest.BankCharges",  false,!isTemplate, isFromExpress,"" )%>
		&nbsp; &nbsp;&nbsp;
		<%= widgetFactory.createRadioButtonField("BankChargesType",	TradePortalConstants.CHARGE_OUR_ACCT,	"FundsTransferRequest.AllOurAcct",	TradePortalConstants.CHARGE_OUR_ACCT, bankChargesType.equals(TradePortalConstants.CHARGE_OUR_ACCT),	isReadOnly)%>
		<br>&nbsp; &nbsp;&nbsp;
		<%= widgetFactory.createRadioButtonField("BankChargesType",	TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE,	"FundsTransferRequest.PayeeAccount",	TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE, bankChargesType.equals(TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE),	isReadOnly)%>
		<br> &nbsp; &nbsp;&nbsp;
		<%= widgetFactory.createRadioButtonField("BankChargesType",	"chargeOther",	"FTRQ.OtherDetails",	TradePortalConstants.CHARGE_OTHER,	bankChargesType.equals(TradePortalConstants.CHARGE_OTHER),	isReadOnly || isFromExpress)%>
        <span class="dijitInline">
        <%if(!isReadOnly){ %>
		<a href='javascript:openOtherConditionsDialog("AddlConditionsText","SaveTrans","<%=resMgr.getText("FTRQ.OtherCondLink", TradePortalConstants.TEXT_BUNDLE)%>");' onclick="setChargeOther()">
			<span id="OtherCond">
				<%=resMgr.getText("FTRQ.OtherCondLink", TradePortalConstants.TEXT_BUNDLE)%>
			</span>
		</a>
		<%}else{ %>
			<%=resMgr.getText("FTRQ.OtherCondLink", TradePortalConstants.TEXT_BUNDLE)%>
		<%} %>
		<%//=resMgr.getText("ImportDLCIssue.Below", TradePortalConstants.TEXT_BUNDLE)%>
		</span>
</div> <%-- END GENERAL RIGHT CONTENT  MAIN DIV --%>

<div style="clear:both;"></div>

<script LANGUAGE="JavaScript">

    function updateFundingAmount() {
     <%--  if (document.forms[0].TransactionAmount.value > '' && document.forms[0].FundsXferSettleType[0].checked) {
	  	if ((document.forms[0].FundingAmount.value.length==0) || (document.forms[0].FundingAmount.value==null)) {
	      document.forms[0].FundingAmount.value = document.forms[0].TransactionAmount.value;
		}
	  } --%>
    }

	function updateFundingCurrency() {
	   if (document.forms[0].FundsXferSettleType[0].checked) {
	      document.getElementById('FundingCurrency').innerHTML = document.forms[0].TransactionCurrency.value;
	   }
    }
</script>
