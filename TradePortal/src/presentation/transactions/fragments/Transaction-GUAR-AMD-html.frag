<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Guarantee Amend Page - all sections

  Description:
    Contains HTML to create the Guarantee Amend page.  All data retrieval 
  is handled in the Transaction-GUAR-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-GUAR-AMD-html.jsp" %>
*******************************************************************************
--%>
<%--
*******************************************************************************
               Guarantee Amend Page - General section

*******************************************************************************
--%>
  
  		  <%= widgetFactory.createSectionHeader("1", "ImportDLCAmend.General") %>
  		  <%--
  		  	RKAZI PPX-269 REL 8.4 Kyriba 09/06/2013 - Add - START
  		  --%>
  		  <% if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind"))){ %>
          	 	<div class="formItem">  
                  <%= widgetFactory.createCheckboxField("ConvTransInd", "GuaranteeIssue.ConvTransInd",  TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind")), true,false,"","","none") %><br/>
                </div>
          <%}%>
  		  <%--
  		  	RKAZI PPX-269 REL 8.4 Kyriba 09/06/2013 - Add - END
  		  --%>
          <div class="columnLeft">
          
          <%= widgetFactory.createTextField("transaction.YourRefNumber","transaction.YourRefNumber",terms.getAttribute("reference_number"),"30",true) %>
          <%= widgetFactory.createTextField( "", "ApprovalToPayAmend.BuyerName", applicantName, "30", true ) %>
          <br/><br/><br/><br/>                  
	      </div>

  <div class="columnRight">
   <%
     if(!isProcessedByBank)
      {
    	 String gteeAmt = currencyCode +"  "+ TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), currencyCode, loginLocale);
   %>
   <%= widgetFactory.createTextField("GuaranteeAmend.GteeAmount","GuaranteeAmend.GteeAmount",gteeAmt,"30",true) %>
 <%--PR CMA Issue No 5 # IR T36000010961 --%>
  <%-- CR-1026 MEer --%>
 	<%if (!userSession.isCustNotIntgTPS()){%>
   		<%= widgetFactory.createTextField("AvailableAmount", "ImportDLCAmend.CurrentAvailableAmount", availableAmount.toString(), "25", true, false,false, "", "", "")%>   
<%
		}
	}
%>
   
				&nbsp;&nbsp;&nbsp;<%= widgetFactory.createRadioButtonField("IncreaseDecreaseFlag", "Increase", 
					"SLCAmend.IncreaseAmount", TradePortalConstants.INCREASE, incrDecrValue.equals(TradePortalConstants.INCREASE), isReadOnly," onChange='setNewAmount();'", "") %>
				<br/>
				&nbsp;&nbsp;&nbsp;<%= widgetFactory.createRadioButtonField("IncreaseDecreaseFlag", "Decrease", 
					"SLCAmend.DecreaseAmount", TradePortalConstants.DECREASE, incrDecrValue.equals(TradePortalConstants.DECREASE), isReadOnly, " onChange='setNewAmount();'", "") %>
				
	
	<%if(!isReadOnly){ %>
	<br/><br/>
	<div class="formItem">
	<%=widgetFactory.createInlineLabel("",currencyCode)%>
	<%= widgetFactory.createAmountField( "amount", "", displayAmount, currencyCode, isReadOnly, false, false, "onChange='setNewAmount();'", "", "none") %> 				
	</div>	
	<%}else{
				String displayAmount1 = TPCurrencyUtility.getDisplayAmount(displayAmount, currencyCode, loginLocale);
				String tempDisplayAmt = currencyCode +"  "+ displayAmount1;
				%>
				<%= widgetFactory.createAmountField( "amount", " ", tempDisplayAmt, "22", true, false, false, "onChange='setNewAmount();'", "", "") %>
				<%---  Rpasupulati IR T36000032448 07/11/2014 start --%>
				
				<input type="hidden" name="amount"
			               value="<%=amountValue%>">
			     <%--Rpasupulati 07/11/2014 End --%>
			<%}%>
	<%
	StringBuffer newGteeAmt= new StringBuffer();
	 if(!isProcessedByBank)
	  {
		 if(!badAmountFound) {
			 newGteeAmt.append(currencyCode);
			 newGteeAmt.append(" ");  
			 newGteeAmt.append(TPCurrencyUtility.getDisplayAmount(calculatedTotal.toString(),currencyCode, loginLocale));			 
		 }
	%>
	
	<%= widgetFactory.createTextField( "TransactionAmount2", "GuaranteeAmend.NewGteeAmount", newGteeAmt.toString(), "22", true, false, false, "", "", "") %> 				
	

	<%
	}
	%>
</div>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <th align="left"> 
      <%=widgetFactory.createWideSubsectionHeader("GuaranteeAmend.NewValidity") %>
    </th>
  </table>
  <div class="columnLeft">
<%
  String validFromType = terms.getAttribute("guar_valid_from_date_type");
  String validToType = terms.getAttribute("guar_expiry_date_type");
%>
  
          <%= widgetFactory.createSubsectionHeader( "GuaranteeIssue.ValidFrom",isReadOnly, false,false, "")  %>                 
         <div class="formItem">
          <%=widgetFactory.createRadioButtonField("guar_valid_from_date_type","TradePortalConstants.CURRENT_VALIDITY_FROM_DATE","GuaranteeAmend.CurrentValidityFromDate",TradePortalConstants.CURRENT_VALIDITY_FROM_DATE,validFromType.equals(TradePortalConstants.CURRENT_VALIDITY_FROM_DATE), isReadOnly) %>
          <br>
          <%=widgetFactory.createRadioButtonField("guar_valid_from_date_type","TradePortalConstants.DATE_OF_ISSUE","GuaranteeIssue.DateofIssue",TradePortalConstants.DATE_OF_ISSUE,validFromType.equals(TradePortalConstants.DATE_OF_ISSUE), isReadOnly) %>
          <br>
          <%=widgetFactory.createRadioButtonField("guar_valid_from_date_type","TradePortalConstants.OTHER_VALID_FROM_DATE","GuaranteeIssue.OtherDate",TradePortalConstants.OTHER_VALID_FROM_DATE,validFromType.equals(TradePortalConstants.OTHER_VALID_FROM_DATE), isReadOnly) %>
          
		  <% if(!isReadOnly){ %>
		  <div class="formItem">
          &nbsp;<%=widgetFactory.createDateField("guar_valid_from_date","",StringFunction.xssHtmlToChars(terms.getAttribute("guar_valid_from_date")),isReadOnly, false, false, "class='char6' onChange=\"setOtherDate()\"", dateWidgetOptions, "none") %>
          </div>
          <%}else{ %>
			<div class="formItemWithIndent3"><%= widgetFactory.createDateField("guar_valid_from_date","",StringFunction.xssHtmlToChars(terms.getAttribute("guar_valid_from_date")),true) %></div>
          <%} %>
          
          </div>
  </div>
  <div class="columnRight">        
          <%= widgetFactory.createSubsectionHeader( "GuaranteeIssue.ValidTo",isReadOnly, false,false, "")  %> 
          <div class="formItem">
          <%=widgetFactory.createRadioButtonField("guar_expiry_date_type","TradePortalConstants.CURRENT_VALIDITY_TO_DATE","GuaranteeAmend.CurrentValidityToDate",TradePortalConstants.CURRENT_VALIDITY_TO_DATE,validToType.equals(TradePortalConstants.CURRENT_VALIDITY_TO_DATE), isReadOnly) %>                 
          <div class="formItemWithIndent3">
          <%= widgetFactory.createDateField("currentValidToDate","",currentValidToDate,true) %>
          </div>
          <%=widgetFactory.createRadioButtonField("guar_expiry_date_type","TradePortalConstants.VALIDITY_DATE","GuaranteeAmend.NewValidityDate",TradePortalConstants.VALIDITY_DATE,validToType.equals(TradePortalConstants.VALIDITY_DATE), isReadOnly) %>
		  
		  <% if(!isReadOnly){ %>
		  <div class="formItem">
		  &nbsp;<%=widgetFactory.createDateField("expiry_date","",StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")),isReadOnly, false, false, "class='char6' onChange=\"setValidityDate()\"", dateWidgetOptions, "none") %>
		 </div>		  
		   <%}else{ %>
			<div class="formItemWithIndent3"><%= widgetFactory.createDateField("expiry_date","",StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")),true) %></div>
          <%} %>
		 
		  <%-- <%=widgetFactory.createRadioButtonField("guar_expiry_date_type","TradePortalConstants.OTHER_EXPIRY_DATE","GuaranteeAmend.Other1",
				  TradePortalConstants.OTHER_EXPIRY_DATE,validToType.equals(TradePortalConstants.OTHER_EXPIRY_DATE), isReadOnly) %> --%>	   
		  <%-- Komal Start --%>
		    <%= widgetFactory.createRadioButtonField("guar_expiry_date_type", "TradePortalConstants.OTHER_EXPIRY_DATE", 
                              "GuaranteeIssue.Other1", TradePortalConstants.OTHER_EXPIRY_DATE, validToType.equals(TradePortalConstants.OTHER_EXPIRY_DATE), isReadOnly, "", "") %>
                         <%if(!isReadOnly){ %>
                              <a href='javascript:openOtherConditionsDialog("special_bank_instructions","SaveTrans", "<%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>");' onclick="setOtherInDelivery()">
                               <%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>
                              </a>
                         <%}else{ %>			
						 	<%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>
				 		 <% } %>
				 		 <%=resMgr.getText("common.ExpiryPlaceOtherEnd",TradePortalConstants.TEXT_BUNDLE)%>
		  <%-- Komal End --%>				 		 
		  </div>
 </div>
<%--
*******************************************************************************
               Guarantee Amend Page - Terms section

*******************************************************************************
--%>
</div>
  <%= widgetFactory.createSectionHeader("2", "GuaranteeAmend.Terms") %>
  

  <br>

 
<%
        //Details Phrase Dropdown
        if (isReadOnly)
          out.println("&nbsp;");
        else {
          options = ListBox.createOptionList(customerTextList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          
          out.println(widgetFactory.createSelectField( "customerTextPhraseDropDown", "",defaultText, options, isReadOnly, false, false, "onChange=" + 
                  PhraseUtility.getPhraseChange("/Terms/guar_customer_text",
                          "guar_customer_text", "3000","document.forms[0].guar_customer_text"),"", ""));
          // Add Button
%>
          
<%
        }
%>
        <br>
        <%-- SSikhakolli - Rel-8.3 SysTest IR# T36000021260 on 09/30/2013 - Removing "style='width: auto'" attribute from HTML props. --%>
        <%= widgetFactory.createTextArea("guar_customer_text","",terms.getAttribute("guar_customer_text"),isReadOnly,false,false,"rows='10' cols='128'","","") %>
	<%if(!isReadOnly) {%>
        <%=widgetFactory.createHoverHelp("guar_customer_text","guar_customer_text") %>                            
       	<%} %>
  <br>
</div>
<%--
*******************************************************************************
               Guarantee Amend Page - Bank Instructions section

*******************************************************************************
--%>
  
            <%= widgetFactory.createSectionHeader("3", "GuaranteeAmend.Instructions") %>                  
          
  <br> 
<%
        //Bank Instructions Phrase Dropdown
        if (isReadOnly)
          out.println("&nbsp;");
        else {
          options = ListBox.createOptionList(specialInstructionsList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          
          out.println(widgetFactory.createSelectField( "bankInstructionsPhraseDropDown", "",defaultText, options, isReadOnly, false, false, "onChange=" + 
                  PhraseUtility.getPhraseChange("/Terms/special_bank_instructions",
                          "special_bank_instructions", "1000","document.forms[0].special_bank_instructions"),"", ""));
%>
          
<%
        }
%>
        <br>
        <%-- SSikhakolli - Rel-8.3 SysTest IR# T36000021260 on 09/30/2013 - Removing "style:'width: auto'" attribute from HTML props. --%>
       <%= widgetFactory.createTextArea("special_bank_instructions","",terms.getAttribute("special_bank_instructions"),isReadOnly,false,false,"rows='10' cols='128'","","") %>
	<%if(!isReadOnly) {%>
       <%=widgetFactory.createHoverHelp("special_bank_instructions","SpclBankInstructions") %>
 	<%} %>
</div>
  		  <%--
  		  	RKAZI PPX-269 REL 8.4 Kyriba 09/06/2013 - Add - START
  		  --%>
<%
						 if (TradePortalConstants.INDICATOR_YES.equals(instrument.getAttribute("converted_transaction_ind"))){ 

%>
	<%= widgetFactory.createSectionHeader("4", "GuaranteeAmend.ConversionDetails") %>
			
	<tr><td>
      <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.ChargesIncurred") %>
	  </td></tr>
	  <tr><td>
&nbsp;&nbsp;&nbsp;<%= widgetFactory.createNote("GuaranteeIssue.ChargeDetails") %><br>
</td></tr>
<input type=hidden value=<%=chrgIncRowCount %> name="numberOfMultipleObjects"   id="noOfChrgIncRows"> 
<table><tr><td>&nbsp;&nbsp;</td><td width=""><table id="chrgIncTable" class="formDocumentsTable">
<thead>
	<tr>
		<th class="genericCol"><%=resMgr.getText("GuaranteeIssue.ChargeType",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol"><%=resMgr.getText("GuaranteeIssue.ChrgIncCurrency",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol"><%=resMgr.getText("GuaranteeIssue.ChrgIncAmount",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol">&nbsp;</th>
	</tr>
</thead>

<tbody>
					 <%--passes in pmtTermsList, isReadOnly, isFromExpress, loginLocale,
			          plus new pmtTermsIndex below--%>
					<%
					  int chrgIncIndex = 0;
					%>
			      	<%@ include file="TransactionChrgIncRows.frag" %> 
	
</tbody>
</table>
</td></tr></table>
<br>
<div>
  <% if (!(isReadOnly)) 
		{     %>	 
				&nbsp;&nbsp;&nbsp;&nbsp;<button data-dojo-type="dijit.form.Button" type="button" id="add3moreChargesIncurred">
				<%= resMgr.getText("common.Add3MoreChargesIncurred", TradePortalConstants.TEXT_BUNDLE) %>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					
				</script>
			</button>	
	<% 
		}else{  %>
			&nbsp;
		<% 
		} %>  		
  	</div>

  
  <br>

	</div>	
	<%}%>
	  		  <%--
  		  	RKAZI PPX-269 REL 8.4 Kyriba 09/06/2013 - Add - END
  		  --%>