<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
/********************************************************************************
 *                    Guarantee Issue Page - Instruction to Bank section
 *
 * Description:
 *    Contains HTML to create the Guarantee Issue Instruction to Bank section.
 *
 * Note:
 *    This is not a standalone JSP.  It MUST be included using the following tag:
 *        <%@ include file="Transaction-GUAR-ISS-BankInstructions.jsp" %>
 ********************************************************************************/
--%>

		<%
        	options = Dropdown.createSortedRefDataOptions(TradePortalConstants.INSTRUMENT_LANGUAGE, instrument.getAttribute("language"), loginLocale);
		%>
		<%= widgetFactory.createSelectField("InstrumentLanguage", "GuaranteeIssue.IssueInstrumentIn", " ", options, isReadOnly,true,false,"", "", "")%>

        <%             
            //Bank Instructions Phrase Dropdown
            if (isReadOnly){ %>
              <span class="formItem">
            	<%=widgetFactory.createStackedLabel("GuaranteeIssue.AdditionalInstructions", "GuaranteeIssue.AdditionalInstructions") %>
            </span>
            <% } else{
                options = ListBox.createOptionList(specialInstructionsList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        %>
        
        <%= widgetFactory.createSelectField("bankInstructionsPhraseDropDown", "GuaranteeIssue.AdditionalInstructions", defaultText, options, isReadOnly, false, false, 
				"onChange=" + PhraseUtility.getPhraseChange("/Terms/special_bank_instructions","special_bank_instructions", "500","document.forms[0].special_bank_instructions"), "", "")%>

        <%
            }
        %>
	<% if (!isReadOnly){ %>   
		<%=widgetFactory.createTextArea("special_bank_instructions","",
                        terms.getAttribute("special_bank_instructions"), 
                        isReadOnly, false, false, "rows='10' cols='128'", "", "") %>
		<%= widgetFactory.createHoverHelp("guar_customer_text","InstrumentIssue.PlaceHolderEnterText")%>
		<%= widgetFactory.createHoverHelp("special_bank_instructions","InstrumentIssue.PlaceHolderSpecialBankInstr")%>
		<%}else{%>
		 <%=widgetFactory.createAutoResizeTextArea("special_bank_instructions","",terms.getAttribute("special_bank_instructions"),isReadOnly, false, false, "style='width:600px;min-height:140px;' cols='128'", "", "") %>
		<%} %>
		<br/>
		
		<div class="columnLeft">
			<%= widgetFactory.createSubsectionHeader("GuaranteeIssue.SettlementInstructions") %>
			<%= widgetFactory.createTextField("settlement_our_account_num", "GuaranteeIssue.DebitAccount", terms.getAttribute("settlement_our_account_num").toString(),
					"30", isReadOnly, false, false, "class='char20'", "", "") %>					
			<%= widgetFactory.createTextField("branch_code", "GuaranteeIssue.BranchCode", terms.getAttribute("branch_code").toString(),
					"30", isReadOnly, false, false, "class='char20'", "", "") %>
			<%= widgetFactory.createTextField("settlement_foreign_acct_num", "GuaranteeIssue.DebitFCA", terms.getAttribute("settlement_foreign_acct_num").toString(),
					"30", isReadOnly, false, false, "class='char20'", "", "") %>

          	<%
            	currencies = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("settlement_foreign_acct_curr"),loginLocale);
         	%>
          					
			<%-- <%= widgetFactory.createCurrencySelectField("settlement_foreign_acct_curr", "GuaranteeIssue.CcyofAccount", currencies, " ",isReadOnly, false, false)%> --%>
			<%=widgetFactory.createSelectField("settlement_foreign_acct_curr","GuaranteeIssue.CcyofAccount", " ", currencies, isReadOnly, false,false, "style=\"width: 50px;\"", "", "inline")%>
			<div style="clear:both;"></div>
		</div>
		
		<div class="columnRight">
			<%= widgetFactory.createSubsectionHeader("GuaranteeIssue.CommissionsCharges") %>			
			<%= widgetFactory.createTextField("coms_chrgs_our_account_num", "ImportDLCIssue.DebitOurAcctCommChrg", terms.getAttribute("coms_chrgs_our_account_num").toString(),
					"30", isReadOnly, false, false, "class='char20'", "", "") %>					
			<%= widgetFactory.createTextField("coms_chrgs_foreign_acct_num", "ImportDLCIssue.DebitForeignAcctCommChrg", terms.getAttribute("coms_chrgs_foreign_acct_num").toString(),
					"30", isReadOnly, false, false, "class='char20'", "", "") %>			
			<%
          		currencies = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("coms_chrgs_foreign_acct_curr"),loginLocale);
          	%>         			
			<%-- <%= widgetFactory.createCurrencySelectField("coms_chrgs_foreign_acct_curr", "GuaranteeIssue.CcyofAccount", currencies, " ", isReadOnly, true, false)%> --%>
			<%=widgetFactory.createSelectField("coms_chrgs_foreign_acct_curr","GuaranteeIssue.CcyofAccount", " ", currencies, isReadOnly, false,false, "style=\"width: 50px;\"", "", "inline")%>
			
			<div style="clear: both"></div>
		</div>
		
		<div style="clear: both"></div><br/>
        <%
            //Bank Instructions Phrase Dropdown
            if (isReadOnly){ %>
              <span class="formItem">
            	<%=widgetFactory.createStackedLabel("GuaranteeIssue.AdditionalInstructions", "GuaranteeIssue.AdditionalInstructions") %>
            </span>
            <% }else 
            {
                options = ListBox.createOptionList(specialInstructionsList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        %>
      	<%= widgetFactory.createSelectField("chargesPhraseDropdown", "GuaranteeIssue.AdditionalInstructions", defaultText, options, isReadOnly, false, false, 
			"onChange=" + PhraseUtility.getPhraseChange("/Terms/coms_chrgs_other_text","coms_chrgs_other_text", "500","document.forms[0].coms_chrgs_other_text"), "", "")%>
        <%
            }
        if (!isReadOnly){ %>  	
		
		<%=widgetFactory.createTextArea("coms_chrgs_other_text","",
                        terms.getAttribute("coms_chrgs_other_text"), 
                        isReadOnly, false, false, "rows='10' cols='128'", "", "") %>
                 
		<%= widgetFactory.createHoverHelp("coms_chrgs_other_text","InstrumentIssue.PlaceHolderCommissionsChargesInstr")%>
		<%}else{%>
		<%=widgetFactory.createAutoResizeTextArea("coms_chrgs_other_text","",
                        terms.getAttribute("coms_chrgs_other_text"), 
                        isReadOnly, false, false, "style='width:600px;min-height:140px;' cols='128'", "", "") %>
		<%}%>