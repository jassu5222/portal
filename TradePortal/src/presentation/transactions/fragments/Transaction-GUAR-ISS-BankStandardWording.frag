<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
/********************************************************************************
                 Guarantee Issue Page - Bank Standard Wording section
 *
 * Description:
 *    Contains HTML to create the Guarantee Bank Standard Wording fields (part of
 * the bank instructions section).  Based on if a phrase was selected for this
 * field and the type of phrase, the phrase dropdown may be suppressed.  Also, if
 * the phrase is partially modifiable, several input fields are displayed after 
 * the text area to allow input of the modifiable fields.  Otherwise, the field
 * displays as read-only.  Note that unlike other text area fields on this page
 * which are all only read-only or enterable, it is possible for this field to
 * be read-only when the others are not.
 *
 * Note:
 *    This is not a standalone JSP.  It MUST be included using the following tag:
 *        <%@ include file="Transaction-GUAR-ISS-BankStandardWording.jsp" %>
 ********************************************************************************/
--%>
		<%
		  // Declare a few variables that are used in this section.
		
		  boolean phraseIsReadOnly = isReadOnly;
		  String  bankStandardText = terms.getAttribute("guar_bank_standard_text");
		  String  bankStdTextType  = terms.getAttribute("guar_bank_std_phrase_type");
		
		  // This tag is used in both the clear text and submit buttons to set parameters
		  // needed by the PhraseLookupMediator
		  String clicktag= PhraseUtility.setPhraseParameters("/Terms/guar_bank_standard_text",  "guar_bank_standard_text", "5000");
		
		%>
	  <%-- Anchor tag for Bank Standard Wording (BSW) is for focus setting in readonly mode --%>
	  <a name=BSW></a>

        <input type=hidden name=guar_bank_standard_texts value="<%=bankStandardText%>" >
        <input type=hidden name=guar_bank_std_phrase_types value="<%=bankStdTextType%>" >
        
		<%
	        // When the text is blank, display the phrase list dropdown box and
	        // lookup button.  Otherwise, display nothing.
	        //Commented by dillip for defect 2844
	       // if (InstrumentServices.isBlank(bankStandardText) && !isReadOnly) {
           options = ListBox.createOptionList(bankStandardTextList, "PHRASE_OID", 
                                              "NAME", "", userSession.getSecretKey());
		%>
			
		
		
 		<div class="formItem">
        <%= widgetFactory.createSelectField("bankStandardTextPhraseDropDown", "GuaranteeIssue.BankStandardWording", defaultText, options, isReadOnly,false, false, 
              "onChange=" +PhraseUtility.getPhraseChangeNew("/Terms/guar_bank_standard_text", "guar_bank_standard_text", "5000","document.forms[0].guar_bank_standard_text") , "", "none") %>
       <%=widgetFactory.createPartyClearButton("ClearText", "clearText()",isReadOnly,"") %>
       </div>
 		
 		
		<%	
		//Commented by dillip for defect 2844
		/*	
	        } else {
	           out.println("&nbsp;");
	        }*/
	        //Ended Here
		%>
		<%-- Modified by dillip for defect 3330--%>
 		<%--<%= widgetFactory.createTextArea("guar_bank_standard_text", "", terms.getAttribute("guar_bank_standard_text"), true) %>--%> 
 		<%=widgetFactory.createTextArea("guar_bank_standard_text","",
                        terms.getAttribute("guar_bank_standard_text"), 
                        true, false, false, "rows='10' cols='128'", "", "") %>
		<% if (!isReadOnly){ %>
			<%= widgetFactory.createHoverHelp("guar_bank_standard_text","InstrumentIssue.PlaceHolderEnterText")%>
			<%= widgetFactory.createHoverHelp("ClearText","ClearBankStandardWording")%>
		<%} %>
 		<%--<%= widgetFactory.createHoverHelp("guar_bank_standard_text","InstrumentIssue.PlaceHolderEnterText")%>--%>
 		<%-- Ended by dillip for defect 3330--%>		
 		<%
   		 // Display a Clear Text button when there is text (to allow the user to change it)
    	if (!isReadOnly && InstrumentServices.isNotBlank(bankStandardText)) {
		%>
		<%-- <%= widgetFactory.createPartyClearButton("ClearText", clicktag, isReadOnly,"TradePortalConstants.BUTTON_PHRASECLEAR") %> --%>
	<%--	<%= widgetFactory.createPartyClearButton("ClearText","resetWording()",false,"") %> --%>
		<%} %>
		
        <%-- The text area is ALWAYS readonly. --%>
		<%
		  // If the phrase is partially modifiable (and not already filled in), parse through
		  // the text looking for fields (contained within []).  Each one of these fields 
		  // becomes an enterable text field.
		  
		  if (bankStdTextType.equals(TradePortalConstants.PHRASE_PARTLY_MODIFIABLE)
		     && (bankStandardText.indexOf("[") > -1)) {
		%>   	
    	<%= widgetFactory.createLabel("", "GuaranteeIssue.SubmitTextArea", isReadOnly, false, false, "") %>
    	<%= widgetFactory.createLabel("", "GuaranteeIssue.InfoRequired", isReadOnly, false, false, "") %>

		<%
		      PhraseParser fieldParser = new PhraseParser(bankStandardText);
		      PhraseField field;
		      String priorUserValue;
		
		      int fieldCount = 0;
		
		      field = fieldParser.getNextField();
		      while (field != null) {
		         // Check if the user already tried to enter a value for this field.
		         priorUserValue = doc.getAttribute("/In/PhraseFields("+fieldCount+")/FieldValue");
		         if (priorUserValue == null) priorUserValue = "";
		%>

       	<%=field.getLabel()%>
       	<input type=hidden name=PhraseLabel<%=fieldCount%> value="<%=field.getLabel()%>">

		<%= widgetFactory.createTextField("PhraseField" + fieldCount++, "SLCIssue.BeneficiaryName", priorUserValue, field.getLength(), isReadOnly) %>

		<%
		         // Get the next field and loop again if not null
		         field = fieldParser.getNextField();
		      } // end while
		%>		
		<a href="javascript:document.forms[0].submit()" name="PhraseFillinSubmit" onClick="setButtonPressed('<%=TradePortalConstants.BUTTON_PHRASEFILLIN%>', '0'); <%=clicktag%>">
			<button data-dojo-type="dijit.form.Button" class="sidebarButtons">Attach a Document</button>
		</a>
		<%
		  }  // End-if phrase partially modifiable
		%>
		
		<%-- <%= widgetFactory.createHoverHelp("guar_bank_standard_text","BankStandardWordingPhraseDropDown")%> --%>