<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
/********************************************************************************
 *                    Guarantee Issue Page - Internal Instructions section
 *
 * Description:
 *    Contains HTML to create the Guarantee Issue Internal Instructions section.
 *
 * Note:
 *    This is not a standalone JSP.  It MUST be included using the following tag:
 *        <%@ include file="Transaction-GUAR-ISS-ExternalBank.frag" %>
 ********************************************************************************/
--%>

	
	 	
	 
<%
	beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.OperationalBankOrganizationWebBean", "OperationalBankOrganization");
    OperationalBankOrganizationWebBean opBankOrg = (OperationalBankOrganizationWebBean)beanMgr.getBean("OperationalBankOrganization");

	String externalBankOid = instrument.getAttribute("op_bank_org_oid");
	opBankOrg.getById(externalBankOid);
	String bankData =""; 
	String bankName = opBankOrg.getAttribute("name");
	if (StringFunction.isBlank(bankName)){
		bankName = "";
	}
	int bankNameLen = (bankName.length());
	bankName = StringFunction.padRight(bankName, 40-bankNameLen);
	
	bankData+=bankName;
	
	bankData+=opBankOrg.getAttribute("phone_number");
	
	bankData+="\n";
	bankData+=opBankOrg.getAttribute("address_line_1");

	bankData+="\n";
	String addrLine2 = opBankOrg.getAttribute("address_line_2");
	if (StringFunction.isBlank(addrLine2)){
		addrLine2 = "";
	}
	int addrLine2Len = (addrLine2.length());
	addrLine2 = StringFunction.padRight(addrLine2, 40-addrLine2Len);
	bankData+=addrLine2;
	bankData+=opBankOrg.getAttribute("swift_address_part1");

	bankData+="\t";
	bankData+=opBankOrg.getAttribute("swift_address_part2");
	
	bankData+="\n";
	bankData+=opBankOrg.getAttribute("address_city");

	bankData+="\n";
	bankData+=opBankOrg.getAttribute("address_state_province");
	
	bankData+="\n";
	bankData+=refData.getDescr(TradePortalConstants.COUNTRY, 
		                 opBankOrg.getAttribute("address_country"), 
		                 loginLocale) ;

%>
	<span class="formItem">
	 	<%=widgetFactory.createTextArea("external_bank","",
                        bankData, 
                        true, false, false, "rows='6' cols='50'", "", " inline ") %>

	
	</span>
