<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">
        <p class="ControlLabel"><a href="#general">
            <%=resMgr.getText("GuaranteeIssue.General", TradePortalConstants.TEXT_BUNDLE)%></a></p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">
        <p class="ControlLabel"><a href="#terms">
          <%=resMgr.getText("GuaranteeIssue.Terms", TradePortalConstants.TEXT_BUNDLE)%></a></p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td nowrap height="30" width="15">&nbsp;</td>
      <td nowrap height="30">
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
    <tr>
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">
        <p class="ControlLabel"><a href="#instructionstobank">
                      <%=resMgr.getText("GuaranteeIssue.InstructionstoBank", TradePortalConstants.TEXT_BUNDLE)%></a></p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">
        <p class="ControlLabel"><a href="#internalinstructions"><%=resMgr.getText("GuaranteeIssue.InternalInstructions", TradePortalConstants.TEXT_BUNDLE)%></a></p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td height="30">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
<%
    // Only display the bank defined section if the user is an admin user
    if (userSession.getSecurityType().equals(TradePortalConstants.ADMIN) 
        && isTemplate) {
%>    
      <tr> 
        <td width="14" nowrap>&nbsp;</td>
        <td nowrap align="left" valign="middle"> 
          <p class="ControlLabel">
            <a href="#BankDefined">
              <%=resMgr.getText("GuaranteeIssue.BankDefined", 
                                TradePortalConstants.TEXT_BUNDLE)%>
            </a>
          </p>
        </td>
        <td width="15" nowrap>&nbsp;</td>
        <td>&nbsp;</td>
        <td width="100%" height="30">&nbsp;</td>
      </tr>
<% 
   }
	//Add Issue Application Form PDF Link if not in admin template mode
	//and the transaction is not started and not deleted...
	if ((!userSession.getSecurityType().equals(TradePortalConstants.ADMIN)) &&
		(!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_STARTED)) &&
		(!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_DELETED)) &&
		(!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK)) && // Change this to an || later
		(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_YES))) {
%>
	  <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	       <%
	         
	         linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
	         linkArgs[2] = "en_US";
	         //loginLocale;
	         linkArgs[3] = userSession.getBrandingDirectory(); 
	       %>
<%
			if (transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_YES)) {
				linkArgs[0] = TradePortalConstants.PDF_SLC_ISSUE_APPLICATION_D;
%>
	       		<%= formMgr.getDocumentLinkAsHrefInFrame(resMgr.getText("SLCIssue.IssueApplicationForm",
	      							TradePortalConstants.TEXT_BUNDLE),
	      							TradePortalConstants.PDF_SLC_ISSUE_APPLICATION_D,
	      							linkArgs,
	      							response) %>
<%			} else {
				// Uncomment later -- linkArgs[0] = TradePortalConstants.PDF_GUAR_ISSUE_APPLICATION;
%>
				<%= formMgr.getDocumentLinkAsHrefInFrame(resMgr.getText("GuaranteeIssue.IssueApplicationForm",
	      							TradePortalConstants.TEXT_BUNDLE),
	      							TradePortalConstants.PDF_SLC_ISSUE_APPLICATION_D, // Change to -- TradePortalConstants.PDF_GUAR_ISSUE_APPLICATION,
	      							linkArgs,
	      							response) %>
<%
			}
%>
	    </p>
	  </td>
        <td width="15" nowrap>&nbsp;</td>
        <td>&nbsp;</td>
        <td width="100%" height="30">&nbsp;</td>
<%
   }
%>
  </table>
