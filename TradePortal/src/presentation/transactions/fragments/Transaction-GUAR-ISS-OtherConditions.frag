<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Standby LC Issue Page - Other Conditions section

  Description:
    Contains HTML to create the Standby LC Issue Other Conditions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-SLC-ISS-OtherConditions.jsp" %>
*******************************************************************************
--%>
<div class="columnLeft">
		<%=widgetFactory.createSubsectionHeader("GuaranteeIssue.AutoExtensionTerms")%>
		<%-- Leelavathi IR#T36000010273 25th Jan 2013 added onClick event Start --%>
		<%-- Leelavathi IR#T36000014700 Rel-8.2 13/03/2013 Begin --%>
		<%-- <% 
			String disabled="DISABLED";
		if(isReadOnly) disabled="";
		else disabled="DISABLED";
		%> --%>
		
		<% 
		String disabled=" disabled=\"disabled\"";
		String maxAutoHtmlPros =" disabled=\"disabled\"";
		String finalExpiryDateHtmlProps =" disabled=\"disabled\"";
		if(TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("auto_extension_ind"))){
			disabled="";
			if(StringFunction.isBlank(terms.getAttribute("final_expiry_date")) && StringFunction.isBlank(terms.getAttribute("max_auto_extension_allowed"))){
				maxAutoHtmlPros ="";
				finalExpiryDateHtmlProps = "";
			}else if(StringFunction.isNotBlank(terms.getAttribute("final_expiry_date"))){
				maxAutoHtmlPros =" disabled=\"disabled\"";
				finalExpiryDateHtmlProps = "";
			}else{
				finalExpiryDateHtmlProps =" disabled=\"disabled\"";
				maxAutoHtmlPros ="";
			}
		}
		
 	   String isNumDaysReadOnly = "";
 	   if(isFromExpress || !"DAY".equals(terms.getAttribute("auto_extension_period"))){
 		isNumDaysReadOnly =" disabled=\"disabled\"";
 	  }
     %>
			<div>	
				<%=widgetFactory.createCheckboxField("AutoExtensionIndicator","GuaranteeIssue.AutoExtension",terms.getAttribute("auto_extension_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, isFromExpress, "onClick='maxNumDisable();'", "",  "inline")%>
			    <div style='clear: both;'></div>
			</div>
			<div>
			    <%-- Narayan CR 831 12-Jan-2014 Rel9.0 Begin --%>	
			    <%=widgetFactory.createDateField("FinalExpiryDate", "TermsBeanAlias.final_expiry_date",StringFunction.xssHtmlToChars(terms.getAttribute("final_expiry_date")),isReadOnly || isFromExpress, false, isFromExpress, "onChange='extensionType(\"FinalExpiryDate\");'"+finalExpiryDateHtmlProps,"", "inline")%>
				<%-- Narayan CR 831 12-Jan-2014 Rel9.0 End --%>
				<%= widgetFactory.createNumberField( "MaxAutoExtensionAllowed", "GuaranteeIssue.MaxNumber", terms.getAttribute("max_auto_extension_allowed"), "3", isReadOnly || isFromExpress, false, isFromExpress, "onChange='extensionType(\"MaxAutoExtensionAllowed\");'"+maxAutoHtmlPros, "", "inline" ) %>
				<%-- Leelavathi IR#T36000010273 25th Jan 2013 End --%>
				<div style='clear: both;'></div>
			</div>
			<div>
				<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AUTO_EXTEND_PERIOD_TYPE, terms.getAttribute("auto_extension_period"), loginLocale);%>
				<%-- Leelavathi IR#T36000014949,IR#T36000016435 CR-737 Rel-8.2 03/04/2013 added onChange function Begin --%>
		        <%=widgetFactory.createSelectField("AutoExtensionPeriod","GuaranteeIssue.ExtensionPeriod", " ", options, isReadOnly || isFromExpress, false, isFromExpress, "onChange='numDaysDisable();'"+disabled, "", "inline")%>
		        <%-- Leelavathi IR#T36000014949,IR#T36000016435 CR-737 Rel-8.2 03/04/2013 End --%>
		        <%= widgetFactory.createNumberField( "AutoExtensionDays", "GuaranteeIssue.NumOfDays", terms.getAttribute("auto_extension_days"), "3", isReadOnly || isFromExpress, false, isFromExpress, isNumDaysReadOnly, "", "inline" ) %>
				<div style='clear: both;'></div>
			</div>
			<%-- Narayan CR 831 12-Jan-2014 Rel9.0 Begin --%>			
			<div>
				<%= widgetFactory.createNumberField( "NotifyBeneDays", "TermsBeanAlias.notify_bene_days", terms.getAttribute("notify_bene_days"), "4", isReadOnly || isFromExpress, false, isFromExpress, disabled, "", "inline" ) %>
				<div style='clear: both;'></div>
			</div>
			<%-- Narayan CR 831 12-Jan-2014 Rel9.0 End --%>		
	</div>
	
	<%--Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - Begin--%>
	<div class="columnRight">
		<%//jgadela - 29th Aug 2013 - Rel8.3 IR-T36000020389 - corrected the lable common.ICCApplicableRules %>
		<%= widgetFactory.createCheckboxField("UCPVersionDetailInd", "common.ICCApplicableRules", terms.getAttribute("ucp_version_details_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false, "onClick=\"clearICCPublication('UCPVersionDetailInd')\"", "", "") %>
		<%
			//jgadela -23 Oct 2013 REL 8.3 IR T36000022056 & T36000022110 -ICC rules population for GUA and DLC based on page. 
			if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){
              		options = Dropdown.createSortedRefDataOptions(TradePortalConstants.ICC_GUIDELINES_GUA, terms.getAttribute("ucp_version"),loginLocale);
			}else{
			        options = Dropdown.createSortedRefDataOptions(TradePortalConstants.ICC_GUIDELINES_SLC, terms.getAttribute("ucp_version"),loginLocale);
			}
			
			if(isReadOnly && StringFunction.isNotBlank(terms.getAttribute("ucp_version")) && 
					(ReferenceDataManager.getRefDataMgr().checkCode(TradePortalConstants.ICC_GUIDELINES_GUA, terms.getAttribute("ucp_version"), loginLocale) == false
					&& ReferenceDataManager.getRefDataMgr().checkCode(TradePortalConstants.ICC_GUIDELINES_SLC, terms.getAttribute("ucp_version"), loginLocale) == false )){
				
				options = Dropdown.createSortedRefDataOptions(TradePortalConstants.ICC_GUIDELINES, terms.getAttribute("ucp_version"), loginLocale);
			}
			//jgadela  REL 8.3 IR T36000022056 & T36000022110  [END]
		%>
                <% //cquinton 10/16/2013 ir#21872 Rel 8.3 start %>
		<%= widgetFactory.createSelectField("UCPVersion", "common.UCPVersion", " ", options, isReadOnly, false, false, "class='char11' onChange=\"changeUCPVersion()\"", "", "formItemWithIndent1") %>
                <%
                   //if version is something other than 'OTHER' the details field should be disabled
                   //also remove setting ucp checkbox onclick - this is done in UCPVersion onchange above
                   String ucpDetailsHtmlProps = "class='char21'";
                   if ( !TradePortalConstants.GUA_OTHER.equals(terms.getAttribute("ucp_version")) ) {
                       ucpDetailsHtmlProps += " disabled=\"disabled\"";
                   }
                %>
		<%= widgetFactory.createTextField("UCPDetails", "common.UCPDetails", terms.getAttribute("ucp_details").toString(), "30", isReadOnly, false, false, ucpDetailsHtmlProps, "", "formItemWithIndent1") %>
                <% //cquinton 10/16/2013 ir#21872 Rel 8.3 end %>
	</div>
	<%--Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - End--%>
	<div style="clear:both;"></div>
