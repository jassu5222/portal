<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
/********************************************************************************
 *                    Guarantee Issue Page - Terms section
 *
 * Description:
 *    Contains HTML to create the Guarantee Issue Terms section.
 *
 * Note:
 *    This is not a standalone JSP.  It MUST be included using the following tag:
 *        <%@ include file="Transaction-GUAR-ISS-Terms.jsp" %>
 ********************************************************************************/
--%>

		<%= widgetFactory.createLabel("", "GuaranteeIssue.Wording", isReadOnly, !isTemplate, false, "") %>		
        <%             
            //Bank Instructions Phrase Dropdown
            if (isReadOnly){ %>
              <span class="formItem">
            	<%=widgetFactory.createStackedLabel("GuaranteeIssue.CustomerText", "GuaranteeIssue.CustomerText") %>
            </span>
            <% } else{
                options = ListBox.createOptionList(customerTextList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        %>		
		
		<%= widgetFactory.createSelectField("customerTextPhraseDropDown", "GuaranteeIssue.CustomerText", defaultText, options, isReadOnly, false, false, 
 			"onChange=" + PhraseUtility.getPhraseChange("/Terms/guar_customer_text", "guar_customer_text", "5000","document.forms[0].guar_customer_text"), "", "inline") %>
        <%
            }
        %>
		<br/>
		<% if (!isReadOnly){ %> 
		<%=widgetFactory.createTextArea("guar_customer_text","",
                        terms.getAttribute("guar_customer_text"), 
                        isReadOnly, false, false, "rows='10' cols='128'", "", "") %>
				<%= widgetFactory.createHoverHelp("guar_customer_text","InstrumentIssue.PlaceHolderEnterText")%>
		<%}else{%>
		<%=widgetFactory.createAutoResizeTextArea("guar_customer_text","",
                        terms.getAttribute("guar_customer_text"), 
                        isReadOnly, false, false, "style='width:600px;min-height:140px;' cols='128'", "", "") %>
		<%}%>
		<br/>