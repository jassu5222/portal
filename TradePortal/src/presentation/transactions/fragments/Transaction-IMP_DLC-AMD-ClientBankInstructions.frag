<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Import DLC Amend Page -  Bank Instructions section

  Description:
    Contains HTML to create the Import DLC Amend page.  All data retrieval 
  is handled in the Transaction-IMP_DLC-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-ISS-BankDefined.jsp" %>
*******************************************************************************

--%>
<%
	if (isReadOnly) {
		out.println("&nbsp;");
	} else {
%>
<%
	options = ListBox.createOptionList(spclInstrDocList,
				"PHRASE_OID", "NAME", "", userSession.getSecretKey());
		defaultText = resMgr.getText("transaction.SelectPhrase",
				TradePortalConstants.TEXT_BUNDLE);
%>
<%=widgetFactory
						.createSelectField(
								"CommInvPhraseItem",
								"",
								defaultText,
								options,
								isReadOnly,
								false,
								false,
								"onChange="
										+ PhraseUtility
												.getPhraseChange(
														"/Terms/special_bank_instructions",
														"SpclBankInstructions",
														textAreaMaxlength2,
														"document.forms[0].SpclBankInstructions"),
								"", "")%>
<%
	}
%>
<%=widgetFactory.createTextArea("SpclBankInstructions", "",
					terms.getAttribute("special_bank_instructions"),
					isReadOnly, false, false, "rows='10' cols='128'", "", "","")%>
<%if(!isReadOnly) {%>
<%=widgetFactory.createHoverHelp("SpclBankInstructions","SpclBankInstructions") %>
<% }%>