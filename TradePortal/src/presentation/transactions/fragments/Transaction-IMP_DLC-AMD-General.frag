<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Import DLC Amend Page - General sections

  Description:
    Contains HTML to create the Import DLC Amend page.  All data retrieval 
  is handled in the Transaction-IMP_DLC-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="fragments/Transaction-IMP_DLC-AMD-General.frag"%> 
*******************************************************************************

--%>
<%
	//Add Issue Application Form PDF Link if not in admin template mode
	//and the transaction is not started and not deleted...
	if ((!userSession.getSecurityType().equals(
			TradePortalConstants.ADMIN))
			&& (!transaction.getAttribute("transaction_status").equals(
					TradePortalConstants.TRANS_STATUS_STARTED))
			&& (!transaction.getAttribute("transaction_status").equals(
					TradePortalConstants.TRANS_STATUS_DELETED))
			&& (!transaction.getAttribute("transaction_status").equals(
					TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK))) {
%>
<%--
	linkArgs[0] = TradePortalConstants.PDF_DLC_AMEND_APPLICATION;
		linkArgs[1] = EncryptDecrypt
				.encryptStringUsingTripleDes(transactionOid, userSession.getSecretKey());
		linkArgs[2] = "en_US";
		//loginLocale;
		linkArgs[3] = userSession.getBrandingDirectory();
--%>
<%--=formMgr.getDocumentLinkAsHrefInFrame(resMgr.getText(
						"ImportDLCAmend.AmendApplicationForm",
						TradePortalConstants.TEXT_BUNDLE),
						TradePortalConstants.PDF_DLC_AMEND_APPLICATION,
						linkArgs, response)--%>
<%
	}
%>

<%--
*******************************************************************************
                     Import DLC Issue Page - General section
*******************************************************************************
--%>

<div class="columnLeftNoBorder">

	<%=widgetFactory.createTextField("refnum",
					"transaction.YourRefNumber",
					instrument.getAttribute("copy_of_ref_num"), "30", true,
					false, false, "", "", "")%>
	<%=widgetFactory.createTextField("appname",
					"ImportDLCAmend.ApplicantName", applicantName, "30", true,
					false, false, "", "", "")%>
	<%=widgetFactory.createDateField("",
					"ImportDLCAmend.CurrentExpiryDate", currentExpiryDate,
					true, false, false, "class='char6'", dateWidgetOptions, "")%>

	
	<%=widgetFactory.createDateField("ExpiryDate",
						"ImportDLCAmend.NewExpiryDate", 
								StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")),
						isReadOnly, false, isExpressTemplate, "class='char6'", dateWidgetOptions, "")%>
	
</div>
<%-- Left Column Ends.... General Section --%>
<div class="columnRight">
	
	<%= widgetFactory.createTextField( "TransactionAmount", "ImportDLCAmend.LCAmount", LCAmount.toString(), "22", true, false, false, "", "", "") %> 				
	<%-- CR-1026 MEer --%>
	<%if (!userSession.isCustNotIntgTPS()){%>
		<%= widgetFactory.createTextField("AvailableAmount", "ImportDLCAmend.CurrentAvailableAmount", availableAmount.toString(), "25", true, false,false, "", "", "")%>
	<%}%>
	 <%-- SureshL IR-T36000034257 04/10/2015 Start --%> 

		&nbsp;&nbsp;&nbsp;<%=widgetFactory.createRadioButtonField(
					"IncreaseDecreaseFlag", "NoAmount",
					"ImportDLCAmend.NoAmount",
					TradePortalConstants.NOAMOUNT,
					incrDecrValue.equals(TradePortalConstants.NOAMOUNT),
					isReadOnly || isFromExpress, " onChange='setNewAmount();'", "")%>
		<br>
		&nbsp;&nbsp;&nbsp;<%=widgetFactory.createRadioButtonField(
					"IncreaseDecreaseFlag", "Increase",
					"ImportDLCAmend.IncreaseAmount",
					TradePortalConstants.INCREASE,
					incrDecrValue.equals(TradePortalConstants.INCREASE),
					isReadOnly || isFromExpress, " onChange='setNewAmount();'", "")%>
		<br>
		&nbsp;&nbsp;&nbsp;<%=widgetFactory.createRadioButtonField(
					"IncreaseDecreaseFlag", "Decrease",
					"ImportDLCAmend.DecreaseAmount",
					TradePortalConstants.DECREASE,
					incrDecrValue.equals(TradePortalConstants.DECREASE),
					isReadOnly || isFromExpress," onChange='setNewAmount();'", "")%>
					
<%if (incrDecrValue.equals(TradePortalConstants.NOAMOUNT)){ %>
	<div class="formItem1"	id="formItem1" style="display:none;">
  <%}else{%>
  <div class="formItem1"	id="formItem1">
  <%}%>
    <%if(!isReadOnly){ %>
	<br>
	<div class="formItem">
	<%=widgetFactory.createInlineLabel("",currencyCode)%>
	<%= widgetFactory.createAmountField( "TransactionAmount", "", displayAmount, currencyCode, isReadOnly, false, false, "onChange='setNewAmount();'", "", "none") %>
	</div>
	<%} if(isReadOnly){ 
	if (!incrDecrValue.equals(TradePortalConstants.NOAMOUNT)){
	// jgadela 21/10/2014 REL 9.1 IR T36000033573 - Fixed the format issue.
		String displayAmount1 = TPCurrencyUtility.getDisplayAmount(displayAmount, currencyCode, loginLocale);		
		StringBuffer         tempDisplayAmt              = new StringBuffer();
		tempDisplayAmt.append(currencyCode);
		tempDisplayAmt.append(" ");
		tempDisplayAmt.append(displayAmount1);
		%>
		<%= widgetFactory.createAmountField( "TransactionAmount", " ", tempDisplayAmt.toString(), "", true, false, false, "onChange='setNewAmount();'", "", "") %>
		
		<%// jgadela 21/10/2014 REL 9.1 IR T36000033573 - Fixed the data saving issue.%>
		<input type="hidden" name="TransactionAmount" value="<%=amountValue%>">
        <%}}%>
	<%
		if (!isProcessedByBank) {
	%>
	<%
		if (!badAmountFound) {
	%>

	
	<%= widgetFactory.createTextField( "TransactionAmount2", "ImportDLCAmend.NewLCAmount", newLCAmount.toString(), "22", true, false, false, "", "", "") %>				
	
	<%
	
		}
		}
	%>
	

	<div class="formItem">
		<%=widgetFactory.createStackedLabel("",
					"ApprovalToPayIssue.NewAmtTolerance")%>
		
		<%= widgetFactory.createSubLabel( "transaction.Plus") %>
		<%if(!isReadOnly){ %>
		<%= widgetFactory.createPercentField( "AmountTolerancePlus", "", terms.getAttribute("amt_tolerance_pos"), isReadOnly,
									false, false, "", "", "none") %>
		<%}else {%>
			<b><%=terms.getAttribute("amt_tolerance_pos") %></b>
		<%} %>							
		
		
		<%= widgetFactory.createSubLabel( "transaction.Percent") %>
		&nbsp;&nbsp;
		<%= widgetFactory.createSubLabel( "transaction.Minus") %>
		<%if(!isReadOnly){ %>
		<%= widgetFactory.createPercentField( "AmountToleranceMinus", "", terms.getAttribute("amt_tolerance_neg"), isReadOnly,
									false, false, "", "", "none") %>
		<%}else {%>
			<b><%=terms.getAttribute("amt_tolerance_neg") %></b>
		<%} %>	
		
		<%= widgetFactory.createSubLabel( "transaction.Percent") %>
		<div style="clear:both;"></div>
	</div>
	</div>
</div>
<%-- Right Column Ends.... General Section --%>
