<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Import DLC Issue Page - Bank Defined section

  Description:
    Contains HTML to create the Import DLC Issue Bank Defined section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-ISS-BankDefined.jsp" %>
*******************************************************************************
--%>

<div class = "columnLeft"> 
		<%= widgetFactory.createTextField( "PurposeType", "ImportDLCIssue.PurposeType", terms.getAttribute("purpose_type"), "3", isReadOnly ) %>
		<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start --%>
		<%-- 
		<%
				options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AVAILABLE_BY,
		                       terms.getAttribute("available_by"),
		                       loginLocale);
				defaultText = " ";
		%>
		<%= widgetFactory.createSelectField( "AvailableBy", "ImportDLCIssue.AvailBy", defaultText, options, isReadOnly ) %>
		--%>
		<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - End --%>
		<%
				options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AVAILABLE_WITH_PARTY,
		                       terms.getAttribute("available_with_party"),
		                       loginLocale);
				defaultText = " ";
		%>
		<%= widgetFactory.createSelectField( "AvailableWithParty", "ImportDLCIssue.AvailWithPty", defaultText, options, isReadOnly ) %>
</div>
<div class = "columnRight">
		<%= widgetFactory.createCheckboxField( "DraftsRequired", "ImportDLCIssue.DraftReqd", terms.getAttribute("drafts_required").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false, "onClick='enableField();'","","") %>
		<div class = "formItemWithIndent2">
			<%
					options = Dropdown.createSortedRefDataOptions(TradePortalConstants.DRAWN_ON_PARTY,
					                       terms.getAttribute("drawn_on_party"),
					                       loginLocale);
					defaultText = " ";
			%>
			<%= widgetFactory.createSelectField( "DrawnOnParty", "ImportDLCIssue.DrawnOnPty", defaultText, options, isReadOnly ) %>  
		</div>
		<%= widgetFactory.createCheckboxField( "Irrevocable", "ImportDLCIssue.Irrevocable", terms.getAttribute("irrevocable").equals(TradePortalConstants.INDICATOR_YES), isReadOnly) %>
		<%= widgetFactory.createCheckboxField( "Operative", "ImportDLCIssue.Operative", terms.getAttribute("operative").equals(TradePortalConstants.INDICATOR_YES), isReadOnly) %> 
</div>		