<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Import DLC Issue Page - General section

  Description:
    Contains HTML to create the Import DLC Issue General section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-ISS-General.jsp" %>
*******************************************************************************
--%>

<%
  // Get some attributes for radio button fields.  We need to refer to this
  // value frequently so do getAttribute once.

 
  System.out.println ("GC Collection");
  
  String pmtTermsType = terms.getAttribute("pmt_terms_type");
  String bankChargesType = terms.getAttribute("bank_charges_type");

  String amount = terms.getAttribute("amount");
  String displayAmount;

  // Don't format the amount if we are reading data from the doc
  if(!getDataFromDoc || amountUpdatedFromPOs)
     displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currency, loginLocale);
  else
     displayAmount = amount;

/*     if(!isReadOnly)
         displayAmount = amount;
      else
    	  displayAmount = TPCurrencyUtility.getDisplayAmount(amount.toString(), currency, loginLocale);
 */
%>


		<div class="columnLeft">
		 <%			  String advisingBankStr = ",ben_terms_party_oid,AdvName,AdvAddressLine1,AdvAddressLine2,AdvCity,AdvStateProvince,AdvCountry,AdvPostalCode,AdvisingBank,BenOTLCustomerId,BenUserDefinedField1,BenUserDefinedField2,BenUserDefinedField3,BenUserDefinedField4";
				      String identifierStr1 ="BenName,BenAddressLine1,BenAddressLine2,BenCity,BenStateProvince,BenPostalCode,BenCountry,BenPhoneNumber"+advisingBankStr;
				      String sectionName1="importben";
		%>

			 <%
			 String benSearchHtml = widgetFactory.createPartySearchButton(identifierStr1,sectionName1,false,TradePortalConstants.BENEFICIARY,false);
			 %>
		     <%= widgetFactory.createSubsectionHeader( "ImportDLCIssue.Beneficiary",(isReadOnly || isFromExpress), false, false, benSearchHtml ) %>


		 <%--
		     <div class="subsectionShortHeader"><h3>Beneficiary</h3><a name="PartySearch" onClick="SearchParty('<%=identifierStr1%>', '<%=sectionName1%>');"><img class="searchButton" src="/portal/themes/default/images/search_icon.png"></a></div>
		    --%>
		     <%
			 // Always send the ben terms party oid
		     secureParms.put("ben_terms_party_oid", termsPartyBen.getAttribute("terms_party_oid"));
		     %>
		     <div style="display:none">
	        <input type=hidden name="ben_terms_party_oid" value="<%=termsPartyBen.getAttribute("terms_party_oid")%>">
		     <input type=hidden name='BenTermsPartyType' value=<%=TradePortalConstants.BENEFICIARY%>>
		     <input type=hidden name="BenOTLCustomerId" value="<%=termsPartyBen.getAttribute("OTL_customer_id")%>">
		     <input type=hidden name="VendorId" value="<%=termsPartyBen.getAttribute("vendor_id")%>">
	       	 <input type=hidden name="BenUserDefinedField1"
                  value="<%=termsPartyBen.getAttribute("user_defined_field_1")%>">
             <input type=hidden name="BenUserDefinedField2"
                  value="<%=termsPartyBen.getAttribute("user_defined_field_2")%>">
             <input type=hidden name="BenUserDefinedField3"
                  value="<%=termsPartyBen.getAttribute("user_defined_field_3")%>">
             <input type=hidden name="BenUserDefinedField4"
                  value="<%=termsPartyBen.getAttribute("user_defined_field_4")%>">      
		     </div>
		     <%Debug.debug("KOMAL ++++++++ " +termsPartyBen.getAttribute("OTL_customer_id")); %>
		     <%
		     	if (isFromExpress || isReadOnly) {
		           // We are in a transaction created from express.  The beneficiary
		           // fields are express fields.  This means they display as readonly
		           // when the user is editing an instrument created from an express
		           // template.  However, we still need to send the ben data to the
		           // mediator.  Put the ben data in a bunch of hidden fields to
		           // ensure this happens.
		     %>
		     	   <div style="display:none">
		           <input type=hidden name="BenName"
		                  value="<%=termsPartyBen.getAttribute("name")%>">
		           <input type=hidden name="BenPhoneNumber"
		                  value="<%=termsPartyBen.getAttribute("phone_number")%>">
		           <input type=hidden name="BenAddressLine1"
		                  value="<%=termsPartyBen.getAttribute("address_line_1")%>">
		           <input type=hidden name="BenAddressLine2"
		                  value="<%=termsPartyBen.getAttribute("address_line_2")%>">
		           <input type=hidden name="BenCity"
		                  value="<%=termsPartyBen.getAttribute("address_city")%>">
		           <input type=hidden name="BenStateProvince"
		                  value="<%=termsPartyBen.getAttribute("address_state_province")%>">
		           <input type=hidden name="BenCountry"
		                  value="<%=termsPartyBen.getAttribute("address_country")%>">
		           <input type=hidden name="BenPostalCode"
		                  value="<%=termsPartyBen.getAttribute("address_postal_code")%>">
		         </div>
		     <%
				       }
		     %>

			 <%-- 	KMehta IR-T36000039924 Rel 9400 on 01-July-2015 Change - Begin	--%>
			 	<%= widgetFactory.createTextField( "BenName", "ImportDLCIssue.BeneficiaryName", termsPartyBen.getAttribute("name"), "35", isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "onChange='checkBeneficiaryName(\"" + StringFunction.escapeQuotesforJS(termsPartyBen.getAttribute("name")) + "\")'", "","" ) %>
			 <%-- 	KMehta IR-T36000039924 Rel 9400 on 01-July-2015 Change - End	--%>			
		     <%= widgetFactory.createTextField( "BenAddressLine1", "ImportDLCIssue.AddressLine1", termsPartyBen.getAttribute("address_line_1"), "35", isReadOnly || isFromExpress, !isTemplate, isExpressTemplate,  "", "","" ) %>

		     <%= widgetFactory.createTextField( "BenAddressLine2", "ImportDLCIssue.AddressLine2", termsPartyBen.getAttribute("address_line_2"), "35", isReadOnly || isFromExpress, false, isExpressTemplate, "", "",""  ) %>

		     <%= widgetFactory.createTextField( "BenCity", "ImportDLCIssue.City", termsPartyBen.getAttribute("address_city"), "23", isReadOnly || isFromExpress, !isTemplate, isExpressTemplate,  "class='char35'", "",""  ) %>

		     <div>
		     <%= widgetFactory.createTextField( "BenStateProvince", "ImportDLCIssue.ProvinceState", termsPartyBen.getAttribute("address_state_province").toString(), "8", isReadOnly || isFromExpress, false, isExpressTemplate, "class='char10'", "","inline"  ) %>
		     <%= widgetFactory.createTextField( "BenPostalCode", "ImportDLCIssue.PostalCode", termsPartyBen.getAttribute("address_postal_code").toString(), "15", isReadOnly || isFromExpress, false, isExpressTemplate, "class='char10'", "","inline" ) %>
		     <div style="clear:both;"></div>
		     </div>

			 <%
		     options = Dropdown.createSortedRefDataOptions(
		                 TradePortalConstants.COUNTRY,
		                 termsPartyBen.getAttribute("address_country"),
		                 loginLocale);
		     %>
		     <%= widgetFactory.createSelectField( "BenCountry", "ImportDLCIssue.Country", " ", options, isReadOnly || isFromExpress, !isTemplate, isExpressTemplate,  "class='char35'", "", "") %>

		     <%= widgetFactory.createTextField( "BenPhoneNumber", "ImportDLCIssue.PhoneNumber", termsPartyBen.getAttribute("phone_number").toString(), "20", isReadOnly || isFromExpress, false, isExpressTemplate, "", "regExp:'[0-9]+'","") %>
		     
		     <%= widgetFactory.createSubsectionHeader( "ImportDLCIssue.DetailedInformation",true, false, false, "" ) %>

		     <%
		     options = Dropdown.createSortedCurrencyCodeOptions(
                     terms.getAttribute("amount_currency_code"), loginLocale);
		     %>

		     <div>
		     <%-- <%= widgetFactory.createCurrencySelectField( "TransactionCurrency", "transaction.Currency", options,"", isReadOnly, !isTemplate, false) %> --%>
		     <br>
		     <%=widgetFactory.createSelectField("TransactionCurrency","transaction.Currency", " ", options, isReadOnly, !isTemplate,false, "style=\"width: 50px;\"", "", "inline")%>

		     <%= widgetFactory.createAmountField( "TransactionAmount", "transaction.Amount", displayAmount, currency, isReadOnly, !isTemplate, false, "class='char15'", "readOnly:" +isReadOnly, "inline") %>
		     <div style="clear:both;"></div>
		     </div>


		     <div style="margin-left: 17px;"><%= widgetFactory.createSubLabel("ImportDLCIssue.AmtTolerance") %></div>


		     <div class = "formItem">
		     <%= widgetFactory.createSubLabel( "transaction.Plus") %>
			<%if(!isReadOnly){ %>
			<%= widgetFactory.createPercentField( "AmountTolerancePlus", "", terms.getAttribute("amt_tolerance_pos"), isReadOnly,
										false, false, "", "", "none") %>

			<%}else {%>
				<b><%=terms.getAttribute("amt_tolerance_pos") %></b>
			<%} %>


			<%= widgetFactory.createSubLabel( "transaction.Percent") %>
			&nbsp;&nbsp;
			<%= widgetFactory.createSubLabel( "transaction.Minus") %>
			<%if(!isReadOnly){ %>
			<%= widgetFactory.createPercentField( "AmountToleranceMinus", "", terms.getAttribute("amt_tolerance_neg"), isReadOnly,
										false, false, "", "", "none") %>

			<%}else {%>
				<b><%=terms.getAttribute("amt_tolerance_neg") %></b>
			<%} %>

			<%= widgetFactory.createSubLabel( "transaction.Percent") %>
			<div style="clear:both;"></div>
			</div>
			<div>
				<table>
		            <tr>
			            <td>
				     		<%= widgetFactory.createLabel("ExpiryDate","ImportDLCIssue.ExpiryDate",isReadOnly, !isTemplate, false,"inline")%>
				     		<br/>
				     	</td>
				     	<td>
				     		<div class="formItem">
					     	 <%= widgetFactory.createLabel("","common.ExpiryPlace",false,false,false,"none")%>
					     	 <%= widgetFactory.createNote("ImportDLCIssue.ExpiryPlace1")%>
					     	 <div  style="margin-top: -7px;">
					     	 <%= widgetFactory.createNote("ImportDLCIssue.ExpiryPlace2")%>
						     	 <%if(!isReadOnly){ %>
								 <%--Srinivasu_D IR#T36000048130 Rel9.5 04/25/2016 changing href from note to normal href --%>
									 <%--<%= widgetFactory.createNote("<a href='javascript:openOtherConditionsDialog(\"AddlConditionsText\",\"SaveTrans\",\"Other Conditions\");' >"+resMgr.getText("ImportDLCIssue.OtherCondLink", TradePortalConstants.TEXT_BUNDLE)+"</a>")%>--%>
									 <%-- IR T36000048415 Rel9.5 05/03/2016 - Passing text resource key --%>
									<a href='javascript:openOtherConditionsDialog("AddlConditionsText","SaveTrans","<%=resMgr.getText("ImportDLCIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>");'> <%=resMgr.getText("ImportDLCIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>  </a>
									 <%--Srinivasu_D IR#T36000048130 Rel9.5 04/25/2016 End --%>

									 <%}else{ %>
									 	<%= widgetFactory.createNote("RequestAdviseIssue.OtherCondLink")%>
									 <%} %>
								 <%= widgetFactory.createInlineLabel("","common.ExpiryPlaceOtherEnd")%>
								</div>
							</div>
				     	 </td>

				     </tr>
				     <tr>
				     	<td><div style="margin-top: -2px;"><%= widgetFactory.createDateField( "ExpiryDate", "", StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")), isReadOnly, !isTemplate, false,  "class='char6'", dateWidgetOptions, "inline") %></div></td>
						 <%
						 Vector codesToExclude=new Vector();
				         codesToExclude.addElement(TradePortalConstants.COUNTRY_OF_SELLER);
						 options = Dropdown.createSortedRefDataOptions(
			 					  TradePortalConstants.PLACE_OF_EXPIRY_TYPE,
			 					  terms.getAttribute("place_of_expiry"),
			 					  loginLocale,codesToExclude);
			             codesToExclude=null;
					     %>
					     <td><div  style="margin-top: -2px;"><%= widgetFactory.createSelectField( "PlaceOfExpiry", "", " ", options, isReadOnly, !isTemplate, false,  "class='char12'", "", "inline" ) %></div></td>
					 </tr>
				</table><div style="clear: both;"></div>
			</div>

		</div>
		<div class="columnRight">

			<%
			String applicantSearchHtml = "";
			String applicantClearHtml = "";
			String addressSearchIndicator = "";
			/*KMehta IR-23711 Rel 8400 on 20th Feb 2014 Started*/
			String identifierStr= "app_terms_party_oid,AppName,AppAddressLine1,AppAddressLine2,AppCity,AppStateProvince,AppCountry,AppPostalCode,Applicant,AppOTLCustomerId,AppUserDefinedField1,AppUserDefinedField2,AppUserDefinedField3,AppUserDefinedField4";
			String sectionName="applicant_dlc";
			/*KMehta IR-23711 Rel 8400 on 20th Feb 2014 End*/
			if (!(isReadOnly || isFromExpress)) {
				applicantSearchHtml = widgetFactory.createPartySearchButton(identifierStr,sectionName,false,TradePortalConstants.APPLICANT,false);
				if (!isReadOnly && isTemplate) {
					applicantClearHtml = widgetFactory.createPartyClearButton( "ClearApplicantButton", "clearApplicant()",false,"");
				}

				addressSearchIndicator = termsPartyApp.getAttribute("address_search_indicator");
			}
			 %>
			<%= widgetFactory.createSubsectionHeader( "ImportDLCIssue.Applicant",false, false, isExpressTemplate, (applicantSearchHtml+applicantClearHtml) ) %>
			<%--<div class="subsectionShortHeader"><h3>Applicant</h3><a name="PartySearch" onClick="SearchParty('<%=identifierStr%>', '<%=sectionName%>');"><img class="searchButton" src="/portal/themes/default/images/search_icon.png"></a></div>--%>

			<%
			if (!(isReadOnly || isFromExpress) && !addressSearchIndicator.equals("N"))
		    {
		    	if(!isReadOnly && corpOrgHasMultipleAddresses)
		    	{ %>
		    	   <div class = "formItem">
						<button data-dojo-type="dijit.form.Button" name="AddressSearch" id="AddressSearch" type="button">
							<%=resMgr.getText("common.searchAddressButton",TradePortalConstants.TEXT_BUNDLE)%>
							<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								var corpOrgOid = <%=corpOrgOid%>;
								// JGADELA R92 IR T36000035674
								require(["t360/common", "t360/partySearch"], function(common, partySearch){
								partySearch.setPartySearchFields('<%=TradePortalConstants.APPLICANT%>');
								common.openCorpCustAddressSearchDialog(corpOrgOid);
							});
						</script>
						</button>
				 </div>
		    	<%
		    	}
		    }
			%>
			<% String applicantAddress = termsPartyApp.buildAddress(false, resMgr); %>
			<% if (isTemplate)
				{%>
			<table style="margin-left: 16px;" bgcolor='' border='0' width="97%"><tr>

                  <td>
				  <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
				<%--  <%= widgetFactory.createTextArea( "Applicant", "", applicantAddress, true,!isTemplate, false, "style='width:auto;' rows='4' cols='43'", "","none" ) %>
				--%>
				  <%= widgetFactory.createAutoResizeTextArea("Applicant", "",applicantAddress, true, !isTemplate,false, "class='buyerAutoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	
				  </td>
            </tr></table>

			<%}else{ %>
			<table style="margin-left: 7px;"><tr>
                  <td style="vertical-align:text-top;"><span class="asterisk">*</span></td>
                  <td>
				<%--
				  <%= widgetFactory.createTextArea( "Applicant", "", applicantAddress, true,!isTemplate, false, "rows='4' cols='43'", "","none" ) %>
				  --%>
				   <%= widgetFactory.createAutoResizeTextArea("Applicant", "",applicantAddress, true, !isTemplate,false, "class='buyerAutoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	
				  </td>
            </tr></table>
            <%} %>
			<%
	        // secureParms.put("app_terms_party_oid", termsPartyApp.getAttribute("terms_party_oid"));

			%>

			        <input type=hidden name="app_terms_party_oid"
			               value="<%=termsPartyApp.getAttribute("terms_party_oid")%>">
			        <input type=hidden name="AppTermsPartyType"
			               value="<%=TradePortalConstants.APPLICANT%>">
			        <input type=hidden name="AppName"
			               value="<%=termsPartyApp.getAttribute("name")%>">
			        <input type=hidden name="AppAddressLine1"
			               value="<%=termsPartyApp.getAttribute("address_line_1")%>">
			        <input type=hidden name="AppAddressLine2"
			               value="<%=termsPartyApp.getAttribute("address_line_2")%>">
			        <input type=hidden name="AppCity"
			               value="<%=termsPartyApp.getAttribute("address_city")%>">
			        <input type=hidden name="AppStateProvince"
			             value="<%=termsPartyApp.getAttribute("address_state_province")%>">
			        <input type=hidden name="AppCountry"
			               value="<%=termsPartyApp.getAttribute("address_country")%>">
			        <input type=hidden name="AppPostalCode"
			               value="<%=termsPartyApp.getAttribute("address_postal_code")%>">
			        <input type=hidden name="AppOTLCustomerId"
			               value="<%=termsPartyApp.getAttribute("OTL_customer_id")%>">
			        <input type=hidden name="AppAddressSeqNum"
			               value="<%=termsPartyApp.getAttribute("address_seq_num")%>">
	               <input type=hidden name="AppUserDefinedField1"
	               value="<%=termsPartyApp.getAttribute("user_defined_field_1")%>">
	               <input type=hidden name="AppUserDefinedField2"
	               value="<%=termsPartyApp.getAttribute("user_defined_field_2")%>">
	               <input type=hidden name="AppUserDefinedField3"
	               value="<%=termsPartyApp.getAttribute("user_defined_field_3")%>">
	               <input type=hidden name="AppUserDefinedField4"
	               value="<%=termsPartyApp.getAttribute("user_defined_field_4")%>">

				<%--rbhaduri - 14th Jun 06 - IR SAUG051361872--%>
				<input type=hidden name="AppAddressSearchIndicator"
			               value="<%=termsPartyApp.getAttribute("address_search_indicator")%>">


			 
			<%= widgetFactory.createTextField( "ApplRefNum", "transaction.ApplRefNumber", terms.getAttribute("reference_number"), "30", isReadOnly , false, false, "class='char30'", "", "") %>

			<%
			String advisingBankSearchHtml = "";
			String advisingBankClearHtml = "";
			String identifierStr2 = "adv_terms_party_oid,AdvName,AdvAddressLine1,AdvAddressLine2,AdvCity,AdvStateProvince,AdvCountry,AdvPostalCode,AdvisingBank,AdvOTLCustomerId,AdvUserDefinedField1,AdvUserDefinedField2,AdvUserDefinedField3,AdvUserDefinedField4";
			String sectionName2="advisingbank";
			if (!(isReadOnly)) {
				advisingBankSearchHtml = widgetFactory.createPartySearchButton(identifierStr2,sectionName2,false,TradePortalConstants.ADVISING_BANK,false);
				advisingBankClearHtml = widgetFactory.createPartyClearButton( "ClearAdvButton", "clearAdvisingBank()",false,"");

				System.out.println ("advisingBankSearchHtml " + advisingBankSearchHtml);
				System.out.println ("advisingBankClearHtml " + advisingBankClearHtml);
				
			}
			 %>
			
			<%= widgetFactory.createSubsectionHeader( "ImportDLCIssue.AdvisingBank",isReadOnly, false, isExpressTemplate, (advisingBankSearchHtml+advisingBankClearHtml) ) %>
			<%--<div class="subsectionShortHeader"><h3>Advising Bank</h3><a name="PartySearch" onClick="SearchParty('<%=identifierStr2%>', '<%=sectionName2%>');"><img class="searchButton" src="/portal/themes/default/images/search_icon.png"></a></div>--%>
			<br>


			<% String advBankAddress = termsPartyAdv.buildAddress(false, resMgr); 
			
			System.out.println ("OTL_customer_id 1" + termsPartyAdv.getAttribute("OTL_customer_id"));
			
			%>
			 <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>	
	   <%--
			<%= widgetFactory.createTextArea( "AdvisingBank", "", advBankAddress, true, false, false, "rows='4' cols='43'", "","" ) %>
		--%>
	 
  			<%= widgetFactory.createAutoResizeTextArea("AdvisingBank", "", advBankAddress, true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	
		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>

			<%
	        //secureParms.put("adv_terms_party_oid",termsPartyAdv.getAttribute("terms_party_oid"));
			%>
					<input type=hidden name="AdvTermsPartyType"
			               value="<%=TradePortalConstants.ADVISING_BANK%>">
			        <input type=hidden name="adv_terms_party_oid"
			               value="<%=termsPartyAdv.getAttribute("terms_party_oid")%>">
			        <input type=hidden name="AdvName"
			               value="<%=termsPartyAdv.getAttribute("name")%>">
			        <input type=hidden name="AdvAddressLine1"
			               value="<%=termsPartyAdv.getAttribute("address_line_1")%>">
			        <input type=hidden name="AdvAddressLine2"
			               value="<%=termsPartyAdv.getAttribute("address_line_2")%>">
			        <input type=hidden name="AdvCity"
			               value="<%=termsPartyAdv.getAttribute("address_city")%>">
			        <input type=hidden name="AdvStateProvince"
			             value="<%=termsPartyAdv.getAttribute("address_state_province")%>">
			        <input type=hidden name="AdvCountry"
			               value="<%=termsPartyAdv.getAttribute("address_country")%>">
			        <input type=hidden name="AdvPostalCode"
			               value="<%=termsPartyAdv.getAttribute("address_postal_code")%>">
			        <input type=hidden name="AdvOTLCustomerId"
			               value="<%=termsPartyAdv.getAttribute("OTL_customer_id")%>">
	                <input type=hidden name="AdvUserDefinedField1"
	               value="<%=termsPartyAdv.getAttribute("user_defined_field_1")%>">
	                <input type=hidden name="AdvUserDefinedField2"
	               value="<%=termsPartyAdv.getAttribute("user_defined_field_2")%>">
	                <input type=hidden name="AdvUserDefinedField3"
	               value="<%=termsPartyAdv.getAttribute("user_defined_field_3")%>">
	                <input type=hidden name="AdvUserDefinedField4"
	               value="<%=termsPartyAdv.getAttribute("user_defined_field_4")%>">
			               
			               
			        

			<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start --%>
			<%--
			<%= widgetFactory.createSubsectionHeader( "ImportDLCIssue.PmtTerms",isReadOnly, !isTemplate, isExpressTemplate, "" ) %>
			<div class = "formItem">
			<%= widgetFactory.createRadioButtonField( "PmtTermsType", "PMT_SIGHT", "ImportDLCIssue.Sight", TradePortalConstants.PMT_SIGHT,pmtTermsType.equals(TradePortalConstants.PMT_SIGHT), isReadOnly || isFromExpress,"onClick=\"clearPMTType();\"","" ) %>
			<br/><br/>

			<div>
			<%= widgetFactory.createRadioButtonField( "PmtTermsType", "PMT_DAYS_AFTER", " ", TradePortalConstants.PMT_DAYS_AFTER,pmtTermsType.equals(TradePortalConstants.PMT_DAYS_AFTER), isReadOnly || isFromExpress ) %>

			<%= widgetFactory.createNumberField( "PmtTermsNumDaysAfter", "", terms.getAttribute("pmt_terms_num_days_after"), "3", isReadOnly || isFromExpress, false, true, "onChange=\"enablePMTType();\"", "", "none" ) %>
			<%= widgetFactory.createSubLabel( "ImportDLCIssue.DaysAfter") %>
			<%
			options = Dropdown.createSortedRefDataOptions( TradePortalConstants.PMT_TERMS_DAYS_AFTER,
                    terms.getAttribute("pmt_terms_days_after_type"),
                    loginLocale);
			%>
			<%= widgetFactory.createSelectField( "PmtTermsDaysAfterType", "", " ", options, isReadOnly || isFromExpress, false, true, "class='char10' onChange=\"enablePMTType();\"", "", "none" ) %>
			<div style="clear:both;"></div>
			</div>
			<br/>

			<%= widgetFactory.createRadioButtonField( "PmtTermsType", "pmtOther", "ImportDLCIssue.OtherDetails", TradePortalConstants.PMT_OTHER, pmtTermsType.equals(TradePortalConstants.PMT_OTHER), isReadOnly || isFromExpress,"onClick=\"clearPMTType();\"","" ) %>
			<span class="label">
			<%if(!isReadOnly){ %>
			<a href='javascript:openOtherConditionsDialog("AddlConditionsText","SaveTrans","Other Conditions");' onclick="javascript:setPmtOther()"> <%=resMgr.getText("ImportDLCIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%></a>
			 <%}else{ %>
			 	<%=resMgr.getText("ImportDLCIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>
			 <%} %>
			</span>
			<%= widgetFactory.createSubLabel("ImportDLCIssue.Below") %>
			</div>

			<div class = "formItemWithIndent4">
			<%if(isReadOnly){%>
			<%=resMgr.getText("ImportDLCIssue.For",TradePortalConstants.TEXT_BUNDLE)%>
			<%}else{%>
			<%= widgetFactory.createLabel("PmtTermsPercentInvoiceLabel", "ImportDLCIssue.For", isReadOnly,!isTemplate, isExpressTemplate,"inline" ) %>
			<%}%>

			<%= widgetFactory.createPercentField( "PmtTermsPercentInvoice", "", terms.getAttribute("pmt_terms_percent_invoice"), isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "", "", "none" ) %>
			<%= widgetFactory.createSubLabel( "ImportDLCIssue.InvValuePercent") %>
			<div style="clear:both;"></div>
			</div>
			--%>
			<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - End --%>

			<%= widgetFactory.createSubsectionHeader( "ImportDLCIssue.BankCharges",isReadOnly, !isTemplate, isExpressTemplate, "" ) %>

			<div class = "formItem">
			<%= widgetFactory.createRadioButtonField( "BankChargesType", TradePortalConstants.CHARGE_OUR_ACCT, "ImportDLCIssue.AllOurAcct", TradePortalConstants.CHARGE_OUR_ACCT, bankChargesType.equals(TradePortalConstants.CHARGE_OUR_ACCT), isReadOnly || isFromExpress ) %>
			<br/>
			<br/>
			<%= widgetFactory.createRadioButtonField( "BankChargesType", TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE, "ImportDLCIssue.AllOutside", TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE, bankChargesType.equals(TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE), isReadOnly || isFromExpress ) %>
 			<%-- 		<div style="margin-left: 7px;"><%= widgetFactory.createLabel("","ImportDLCIssue.AllOutside2",false,false,false,"")%></div> --%>
			<br/>
			<%= widgetFactory.createRadioButtonField( "BankChargesType", "chargeOther", "ImportDLCIssue.OtherDetails", TradePortalConstants.CHARGE_OTHER, bankChargesType.equals(TradePortalConstants.CHARGE_OTHER), isReadOnly || isFromExpress ) %>
			<span class="label">
			<%if(!isReadOnly){ %>
			<%-- IR T36000048415 Rel9.5 05/03/2016 - Passing text resource key --%>
			<a href='javascript:openOtherConditionsDialog("AddlConditionsText","SaveTrans","<%=resMgr.getText("ImportDLCIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>");' onclick="javascript:setChargeOther()"> <%=resMgr.getText("ImportDLCIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%></a>
			 <%}else{ %>
			 	<%=resMgr.getText("ImportDLCIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>
			 <%} %>
			</span>
			<%= widgetFactory.createSubLabel("ImportDLCIssue.Below") %>
			</div>

</div>
<div style="clear:both;"></div>
<%-- Leelavathi 10thDec2012 - Rel8200 CR-737 - Start --%>
		<%=widgetFactory.createWideSubsectionHeader("ImportDLCIssue.PmtTerms", false,!isTemplate, false, "") %>
		<div class = "formItem">
			<div class="columnLeftNoBorder">
			<%= widgetFactory.createSubLabel("ImportDLCIssue.PmtType") %>
		<%
			if(isReadOnly){
		%>
			<%= widgetFactory.createBoldSubLabel(ReferenceDataManager.getRefDataMgr().getDescr(
					TradePortalConstants.AVAILABLE_BY_TYPE, terms.getAttribute("payment_type"), loginLocale)) %>
		<%
			}else{
				options = Dropdown.createSortedRefDataOptions( TradePortalConstants.AVAILABLE_BY_TYPE, terms.getAttribute("payment_type"), loginLocale);
		%>
			<%=widgetFactory.createSelectField("TermsPmtType","", " ", options,isFromExpress, false, false, "", "", "none")%>
		<%
			}
		%>
			</div>
			<div class="columnRight">
				<%= widgetFactory.createSubLabel("ImportDLCIssue.For") %>
			<%
				if(isReadOnly){
			%>
				<%= widgetFactory.createBoldSubLabel(terms.getAttribute("pmt_terms_percent_invoice")) %>
			<%
				}else{
			%>	
				<%= widgetFactory.createPercentField( "PmtTermsPercentInvoice", "", terms.getAttribute("pmt_terms_percent_invoice"), isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "", "", "none" ) %>
			<%
				}
			%>
				<%= widgetFactory.createSubLabel( "ImportDLCIssue.InvValuePercent") %>
		     </div>
		     <div style='clear: both;'>
		</div>
		<%-- Leelavathi IR#T36000015097 Rel-8.2 03/22/2013 End --%>
			<br>
			<% String displayStyle = "display: none;";
			String displayStyle1 = "display: block;";
			// Kiran IR#T36000015047 Rel-8.2 05/31/2013 Start 
			// Added a new string in order to hide the Add 2 more button  
			String disAdd2MrLnsStl = "display: block;";
			 // Kiran IR#T36000015047 Rel-8.2 05/31/2013 End 
			// Leelavathi IR#T36000011655,IR#T36000011742 Rel-8.2 02/20/2013 Begin
			String accpDefpDisabled ="";
			String accpDefpDisabled1 ="";
			if("ACCP".equals(terms.getAttribute("payment_type")) || "DEFP".equals(terms.getAttribute("payment_type"))){
				//Leelavathi IR#T36000014888 CR-737 Rel-8.2 11/04/2013 Begin
				//Leelavathi IR#T36000016659 CR-737 Rel-8.2 06/05/2013 Begin
				accpDefpDisabled = " READONLY ";
			// Kiran IR#T36000015047 Rel-8.2 05/31/2013 Start 
			// Used that new string in order to hide the Add 2 more button 
				disAdd2MrLnsStl= "display: none;";
			// Kiran IR#T36000015047 Rel-8.2 05/31/2013 Start 
				//accpDefpDisabled = " DISABLED ";
				//Leelavathi IR#T36000016659 CR-737 Rel-8.2 06/05/2013 End
				//Leelavathi IR#T36000014888 CR-737 Rel-8.2 11/04/2013 End
			}
			//Leelavathi IR#T36000011655,IR#T36000011742 Rel-8.2 02/20/2013 End
			if("SPEC".equals(terms.getAttribute("payment_type")) ){
					displayStyle = "display: block;";
					displayStyle1 = "display: none;";
			// Kiran IR#T36000015047 Rel-8.2 05/31/2013 Start 
			// Used that new string in order to hide the Add 2 more button 
					disAdd2MrLnsStl= "display: none;";
			// Kiran IR#T36000015047 Rel-8.2 05/31/2013 Start 
			}
			//Leelavathi IR#T36000011303 Rel-8.2 02/20/2013 Begin
			if("PAYM".equals(terms.getAttribute("payment_type")) ){
					displayStyle1 = "display: none;";
			// Kiran IR#T36000015047 Rel-8.2 05/31/2013 Start 
			// Used that new string in order to hide the Add 2 more button 
					disAdd2MrLnsStl= "display: none;";
			// Kiran IR#T36000015047 Rel-8.2 05/31/2013 Start 
			}
			//Leelavathi IR#T36000011303 Rel-8.2 02/20/2013 End
			%>

			<div id='PmtTermsSpecialTenorTextDiv' style='<%=displayStyle%>'>
			<%= widgetFactory.createTextArea( "PmtTermsSpecialTenorText", "ImportDLCIssue.SpecialTenorText", terms.getAttribute("special_tenor_text"), isReadOnly || isFromExpress, false, false, "", "", "none" ) %>
			</div>

			<div id='PmtTermsInPercentAmountIndDiv' style='<%=displayStyle1%>'>
			<%-- Leelavathi IR#T36000011742 Rel-8.2 02/20/2013 Begin  --%>
			<%--<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "ImportDLCIssue.Percentage","ImportDLCIssue.Percentage",TradePortalConstants.PMT_IN_PERCENT,TradePortalConstants.PMT_IN_PERCENT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress )
			<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "ImportDLCIssue.Amount","ImportDLCIssue.Amount",TradePortalConstants.PMT_IN_AMOUNT,  TradePortalConstants.PMT_IN_AMOUNT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress ) %>--%>
			<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "ImportDLCIssue.Percentage","ImportDLCIssue.Percentage",TradePortalConstants.PMT_IN_PERCENT,TradePortalConstants.PMT_IN_PERCENT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress,""+accpDefpDisabled,"" ) %>
			<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "ImportDLCIssue.Amount","ImportDLCIssue.Amount",TradePortalConstants.PMT_IN_AMOUNT,  TradePortalConstants.PMT_IN_AMOUNT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress ,""+accpDefpDisabled,"") %>
			<%-- Leelavathi IR#T36000011742 Rel-8.2 02/20/2013 End  --%>
			</div>
	<div id='pymtTermsDiv' style='<%=displayStyle1%>'>
	<input type=hidden value=<%=pmtTermsRowCount %> name="numberOfMultipleObjects1"   id="noOfPmtTermsRows">
<table id="pymtTerms" class="formDocumentsTable" width="100%" border="1" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<%if(TradePortalConstants.PMT_IN_AMOUNT.equals(terms.getAttribute("percent_amount_dis_ind"))){ %>
			<%-- Jyothikumari.G 16/05/2013 Rel8.2 IR-T36000016903 Start --%>
			<%-- I added the below class to move the column header to the left --%>
				<th width="8%" class="genericCol"><%=resMgr.getText("ImportDLCIssue.PaymentInAmount",TradePortalConstants.TEXT_BUNDLE)%></th>
			<%}else{%>
				<th width="8%" class="genericCol"><%=resMgr.getText("ImportDLCIssue.PaymentInPercent",TradePortalConstants.TEXT_BUNDLE)%></th>
			<%}%>
			<th width="18%" class="genericCol"><%=resMgr.getText("ImportDLCIssue.TenorType",TradePortalConstants.TEXT_BUNDLE)%></th>
			<th width="45%" class="genericCol"><%=resMgr.getText("ImportDLCIssue.TenorDetails",TradePortalConstants.TEXT_BUNDLE)%></th>
			<th width="28%" class="genericCol"><%=resMgr.getText("ImportDLCIssue.MaturityDate",TradePortalConstants.TEXT_BUNDLE)%></th>
			<%-- Jyothikumari.G 16/05/2013 Rel8.2 IR-T36000016903 End --%>
		</tr>
	</thead>
	<tbody>
	<%-- Leelavathi 30th Jan 2012 - Rel8200 CR-737 - start --%>
		 <%--passes in pmtTermsList, isReadOnly, isFromExpress, loginLocale,
          plus new pmtTermsIndex below--%>
		<%
		  int pmtTermsIndex = 0;
		String percentOrAmountInd = terms.getAttribute("percent_amount_dis_ind");

		%>
      <%@ include file="TransactionPaymentTermRows.frag" %>
      <%-- Leelavathi 30th Jan 2013 - Rel8200 CR-737 - End --%>
	</tbody>
</table>
<div style="clear:both;"></div>
			</div>
			</div>

<%-- Kiran IR#T36000015047 Rel-8.2 05/31/2013 Start --%>
<%-- Added a new style and replaced  displayStyle1 with disAdd2MrLnsStl --%>
<div id='add2MoreLinesDiv' style='<%=disAdd2MrLnsStl%>'>
<%-- Kiran IR#T36000015047 Rel-8.2 05/31/2013 End --%>
	<% if (!(isReadOnly))
	{     %>
    <button data-dojo-type="dijit.form.Button" type="button" id="add2MoreLines" name="add2MoreLines">
    	<%= resMgr.getText("common.Add2MoreLines", TradePortalConstants.TEXT_BUNDLE) %>
    	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				//add2MoreLinesTab(2);
		</script>
	</button>
			<%
		}else{  %>
			&nbsp;
		<%
		} %>
		<%-- Leelavathi IR#T36000011669 Rel-8.2 13/02/2013 End --%>
</div>


<%-- Leelavathi 10thDec2013 - Rel8200 CR-737 - End --%>
<div style="clear:both;"></div>
		<% if (!isReadOnly){ %>
				<%=widgetFactory.createHoverHelp("applicant", "PartySearchIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("AddressSearch", "PartySearchIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("importben", "PartySearchIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("advisingbank", "PartySearchIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("ClearAdvButton", "PartyClearIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("ClearApplicantButton", "PartyClearIconHoverHelp") %>
				<%= widgetFactory.createHoverHelp("AmountToleranceMinus", "ImportDLCIssue.Minus")%>
				<%= widgetFactory.createHoverHelp("AmountTolerancePlus", "ImportDLCIssue.Plus")%>
			 <%} %>
<div style="clear:both;"></div>


<script LANGUAGE="JavaScript">

function clearAdvisingBank() {
  document.TransactionIMP_DLC.AdvName.value = "";
  document.TransactionIMP_DLC.AdvAddressLine1.value = "";
  document.TransactionIMP_DLC.AdvAddressLine2.value = "";
  document.TransactionIMP_DLC.AdvCity.value = "";
  document.TransactionIMP_DLC.AdvStateProvince.value = "";
  document.TransactionIMP_DLC.AdvCountry.value = "";
  document.TransactionIMP_DLC.AdvPostalCode.value = "";
  document.TransactionIMP_DLC.AdvOTLCustomerId.value = "";
  document.TransactionIMP_DLC.AdvisingBank.value = "";
  document.TransactionIMP_DLC.AdvUserDefinedField1.value = "";
  document.TransactionIMP_DLC.AdvUserDefinedField2.value = "";
  document.TransactionIMP_DLC.AdvUserDefinedField3.value = "";
  document.TransactionIMP_DLC.AdvUserDefinedField4.value = "";

}

function checkBeneficiaryName(originalName)
{
	<%--  	KMehta IR-T36000039924 Rel 9400 on 07-August-2015 Change - Begin  --%>	
	if(dijit.byId('BenName')){
    	  		if(dijit.byId('BenName').value != originalName){    	  			
    	  			if(null != document.TransactionIMP_DLC.BenOTLCustomerId.value){	
    	  				document.TransactionIMP_DLC.BenOTLCustomerId.value = "";	
    	  			}
    	  		}
    	  	}
	<%--  	KMehta IR-T36000039924 Rel 9400 on 07-August-2015 Change - End  --%>
	<%-- 
	if (document.forms[0].BenName.value != originalName)
   {
      document.forms[0].BenOTLCustomerId.value = "";
   } --%>
}

function clearApplicant()
{
  document.TransactionIMP_DLC.AppName.value = "";
  document.TransactionIMP_DLC.AppAddressLine1.value = "";
  document.TransactionIMP_DLC.AppAddressLine2.value = "";
  document.TransactionIMP_DLC.AppCity.value = "";
  document.TransactionIMP_DLC.AppStateProvince.value = "";
  document.TransactionIMP_DLC.AppCountry.value = "";
  document.TransactionIMP_DLC.AppPostalCode.value = "";
  document.TransactionIMP_DLC.AppOTLCustomerId.value = "";
  document.TransactionIMP_DLC.Applicant.value = "";
  document.TransactionIMP_DLC.ben_terms_party_oid.value = "";  
  document.TransactionIMP_DLC.AppUserDefinedField1.value = "";
  document.TransactionIMP_DLC.AppUserDefinedField2.value = "";
  document.TransactionIMP_DLC.AppUserDefinedField3.value = "";
  document.TransactionIMP_DLC.AppUserDefinedField4.value = "";
}

function enablePMTType(){
	if(dijit.byId("PmtTermsNumDaysAfter").value > '' ||
		dijit.byId("PmtTermsDaysAfterType").value > ''){
		dijit.byId("PMT_DAYS_AFTER").set("checked", true);
	}
}

function clearPMTType(){
	dijit.byId("PmtTermsNumDaysAfter").set('value',"");
	dijit.byId("PmtTermsDaysAfterType").set('value',"");
}
</script>
