<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Import DLC Issue Page - Other Conditions section

  Description:
    Contains HTML to create the Import DLC Issue Other Conditions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-ISS-OtherConditions.jsp" %>
*******************************************************************************
--%>


	<div class="columnLeft">
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.AdditionalTerms")%>
		<% 
			String divclass="";
			if(isExpressTemplate){
			divclass = "none";
		%>
			<span class='hashMark'>#</span>
		<%}%>		
		<%=widgetFactory.createCheckboxField("Transferable","ImportDLCIssue.Transferable",terms.getAttribute("transferrable").equals(TradePortalConstants.INDICATOR_YES),isReadOnly || isFromExpress, isExpressTemplate, "", "", divclass)%>
		
		<% if(isExpressTemplate){%>
			<br><br>
			<span class='hashMark'>#</span>
		<%}else{%>
			<div class='formItem'>
		<%}%>
		
		<%=widgetFactory.createCheckboxField("Revolve","RequestAdviseIssue.Revolve",terms.getAttribute("revolve").equals(TradePortalConstants.INDICATOR_YES),isReadOnly || isFromExpress, false, "", "", "none")%>
		<%=widgetFactory.createNote("RequestAdviseIssue.RevolveConditions") %>
		
		<% if(!isExpressTemplate){%>
			</div>
		<%}%>
		<%--Sandeep - Rel-8.3 CR-752 Dev 06/19/2013 - Begin--%>
		<%-- PMitnala Rel 8.3 IR#T36000021051 Correcting the label to ICC Applicable Rules --%>
		<%=widgetFactory.createCheckboxField( "UCPVersionDetailInd", "common.ICCApplicableRules", terms.getAttribute("ucp_version_details_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false, "onClick=\"clearICCPublication('UCPVersionDetailInd')\"", "", "") %>
		<%
			options = Dropdown.createSortedRefDataOptions(TradePortalConstants.ICC_GUIDELINES_DLC, terms.getAttribute("ucp_version"), loginLocale);
			
			if(isReadOnly && StringFunction.isNotBlank(terms.getAttribute("ucp_version")) && 
					ReferenceDataManager.getRefDataMgr().checkCode(TradePortalConstants.ICC_GUIDELINES_DLC, terms.getAttribute("ucp_version"), loginLocale) == false){
				
				options = Dropdown.createSortedRefDataOptions(TradePortalConstants.ICC_GUIDELINES, terms.getAttribute("ucp_version"), loginLocale);
			}
			defaultText = " ";
		%>
                <% //cquinton 10/16/2013 ir#21872 Rel 8.3 start %>
		<%= widgetFactory.createSelectField( "UCPVersion", "common.UCPVersion", defaultText, options, isReadOnly, false, false, "onChange=\"changeUCPVersion()\"", "", "formItemWithIndent1" ) %>
                <%
                   //if version is something other than 'OTHER' the details field should be disabled
                   //also remove setting ucp checkbox onclick - this is done in UCPVersion onchange above
                   String ucpDetailsHtmlProps = "class='char21'";
                   if ( !TradePortalConstants.IMP_OTHER.equals(terms.getAttribute("ucp_version")) ) {
                       ucpDetailsHtmlProps += " disabled=\"disabled\"";
                   }
                %>
		<%= widgetFactory.createTextField( "UCPDetails", "common.UCPDetails", terms.getAttribute("ucp_details"), "30", isReadOnly, false, false, ucpDetailsHtmlProps, "", "formItemWithIndent1" ) %>
                <% //cquinton 10/16/2013 ir#21872 Rel 8.3 end %>
		<%--Sandeep - Rel-8.3 CR-752 Dev 06/19/2013 - End--%>
	</div>
	
	<div class="columnRight">
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.Confirmation",isReadOnly, false, isExpressTemplate, "")%>
		<div class="formItem">
		<%= widgetFactory.createRadioButtonField( "ConfirmationType", "TradePortalConstants.CONFIRM_NOT_REQD", "ImportDLCIssue.ConfirmNotReqd1", TradePortalConstants.CONFIRM_NOT_REQD, confirmType.equals(TradePortalConstants.CONFIRM_NOT_REQD), isReadOnly || isFromExpress) %>
		<br>
		<%= widgetFactory.createRadioButtonField( "ConfirmationType", "TradePortalConstants.BANK_TO_ADD", "ImportDLCIssue.BankToAdd1", TradePortalConstants.BANK_TO_ADD, confirmType.equals(TradePortalConstants.BANK_TO_ADD), isReadOnly || isFromExpress ) %>
		<br>
		<%= widgetFactory.createRadioButtonField( "ConfirmationType", "TradePortalConstants.BANK_MAY_ADD", "ImportDLCIssue.BankMayAdd1", TradePortalConstants.BANK_MAY_ADD, confirmType.equals(TradePortalConstants.BANK_MAY_ADD), isReadOnly || isFromExpress ) %>
		<br>
		</div>
		<%-- MEer CR-1026 Rel 9.3.5 --%>
		<% if (TradePortalConstants.INDICATOR_YES.equals(includeTTReimburseAllowInd)){ %>
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.Reimbursement",isReadOnly, false, isExpressTemplate, "")%>
		<div class="formItem">
		<%=widgetFactory.createCheckboxField("TTReimbursementAllowInd","ImportLCIssue.TTReimbursementAllowInd", TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("tt_reimbursement_allow_ind")),isReadOnly || isFromExpress, false, "", "", "none")%>
		<%=widgetFactory.createHoverHelp("TTReimbursementAllowInd","TTReimbursementAllowed.HoverText") %>
		<br>
		<br>
		<%=widgetFactory.createNote("ImportLCIssue.TTReimburseIndNote") %>
		</div>
		<% } %>
		</div>
	<div style="clear:both;"></div>
	<%
	if (isReadOnly || isFromExpress) {
        out.println("&nbsp;");
      } else {
          options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        defaultText1 = resMgr.getText("transaction.SelectPhrase",
                                     TradePortalConstants.TEXT_BUNDLE);
	%>
	<%=widgetFactory.createSelectField("AddlDocsPhraseItem1","ImportDLCIssue.AddlConditions", defaultText1, options,false, false, isExpressTemplate,  
										"onChange=" + PhraseUtility.getPhraseChange("/Terms/additional_conditions","AddlConditionsText", "5000","document.forms[0].AddlConditionsText"), "", "")%>
	<%
      }
	if (!isReadOnly){ %> 
	<%=widgetFactory.createTextArea("AddlConditionsText","",terms.getAttribute("additional_conditions"),isReadOnly || isFromExpress,false,false, "rows='10' cols='128'","","","") %>
	<%= widgetFactory.createHoverHelp("AddlConditionsText","InstrumentIssue.PlaceHolderOtherAddlConditions")%>
	<%}else{ %>
	<%=widgetFactory.createAutoResizeTextArea("AddlConditionsText","",terms.getAttribute("additional_conditions"),isReadOnly || isFromExpress,false,false, "style='width:600px;min-height:140px;' cols='128'","","") %>
	<%}%>
