<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.ArrayList, java.util.Hashtable" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 
  //parameters -
  String parmValue = "";
  boolean isReadOnly = false; //request.getParameter("isReadOnly");
  boolean isFromExpress = false; //request.getParameter("isFromExpress");
  boolean isExpressTemplate = false; //request.getParameter("isExpressTemplate");
  boolean isTemplate = false; //request.getParameter("isExpressTemplate");
  parmValue = request.getParameter("shipmentTermsIndex");
  int shipmentTermsIndex = 0;
  if ( parmValue != null ) {
    try {
      shipmentTermsIndex = (new Integer(parmValue)).intValue();
    } 
    catch (Exception ex ) {
    }
  }

  //other necessaries
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  //when included per ajax, the business objects will be blank
  ShipmentTermsWebBean shipmentTerms   = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
%>

<%@ include file="Transaction-IMP_DLC-ISS-ShipmentTermsRows.frag" %>
