<%
		String itemid1="NotName,NotAddressLine1,NotAddressLine2,NotAddressLine3,NotUserDefinedField1,NotUserDefinedField2,NotUserDefinedField3,NotUserDefinedField4";
		String section1="notifyparty_imp";
		String itemid2="OthName,OthAddressLine1,OthAddressLine2,OthAddressLine3,OthUserDefinedField1,OthUserDefinedField2,OthUserDefinedField3,OthUserDefinedField4";
		String section2="consigneeparty_imp";
		String selDoc = resMgr.getText("transaction.SelectDocument",TradePortalConstants.TEXT_BUNDLE);
		%>
<%= widgetFactory.createSectionHeader("transportdoc", "ImportDLCIssue.TransportDocs") %>
<%-- DK IR T36000023993 REl8.4 01/17/2014 --%>
<%= widgetFactory.createTextField( "description", "ImportDLCIssue.TranshipDocDesc", shipmentTerms.getAttribute("description"), "60", isReadOnly, false, false,  "style='width:55em;'", "placeHolder:'"+resMgr.getTextEscapedJS("ImportDLCIssue.TransportDocsShipmentDesc",TradePortalConstants.TEXT_BUNDLE)+"'", "" ) %>
<br>
<table class="formDocumentsTable">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th class="genericCol"><% if(isExpressTemplate){%>
				<span class='hashMark'>#</span>
				<%}%><%=resMgr.getText("ImportDLCIssue.DocType",TradePortalConstants.TEXT_BUNDLE)%></th>
				<th class="genericCol"><% if(isExpressTemplate){%>
				<span class='hashMark'>#</span>
				<%}%><%=resMgr.getText("ImportDLCIssue.Originals",TradePortalConstants.TEXT_BUNDLE)%></th>
				<th class="genericCol"><% if(isExpressTemplate){%>
				<span class='hashMark'>#</span>
				<%}%><%=resMgr.getText("ImportDLCIssue.Copies",TradePortalConstants.TEXT_BUNDLE)%></th>
				<th class="genericCol"><% if(isExpressTemplate){%>
				<span class='hashMark'>#</span>
				<%}%><%=resMgr.getText("ImportDLCIssue.Description",TradePortalConstants.TEXT_BUNDLE)%></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><%= widgetFactory.createCheckboxField( "TransDocInd", "", shipmentTerms.getAttribute("trans_doc_included").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none") %></td>
				<%
		          options = Dropdown.createSortedRefDataOptions(
		                      TradePortalConstants.TRANS_DOC_TYPE, 
		                      shipmentTerms.getAttribute("trans_doc_type"), 
		                      loginLocale);
				%>
				<td><%= widgetFactory.createSelectField( "TransDocType", "", selDoc, options, isReadOnly || isFromExpress, false, false,  "class='char21' onChange=\"pickTransDocCheckbox();\"", "", "none") %></td>
				<%
		          options = Dropdown.createSortedRefDataOptions(
		                      TradePortalConstants.TRANS_DOC_ORIGINALS_TYPE, 
		                      shipmentTerms.getAttribute("trans_doc_originals"), 
		                      loginLocale);
				%>
				<td><%= widgetFactory.createSelectField( "TransDocOrigs", "", " ", options, isReadOnly || isFromExpress, false, false,  "class='char7em' onChange=\"pickTransDocCheckbox();\"", "", "none") %> </td>
				<td><%= widgetFactory.createNumberField( "TransDocCopies", "", shipmentTerms.getAttribute("trans_doc_copies"), "3", isReadOnly || isFromExpress, false, false,  "onChange=\"pickTransDocCheckbox();\"", "", "none" ) %> </td>
				<td>
				<%
					if (!(isReadOnly || isFromExpress)) {
						options = ListBox.createOptionList(transportDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
						defaultText1 = resMgr.getText("transaction.SelectPhrase",
				                                   TradePortalConstants.TEXT_BUNDLE);
				%> 
			    	<%= widgetFactory.createSelectField( "TransDocItem", "", defaultText1, options, isReadOnly || isFromExpress, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                            "/Terms/ShipmentTermsList/trans_doc_text","TransDocText", textAreaMaxlength2,"document.forms[0].TransDocText"), "", "none") %>
			    	    
				<% }
			    %>
			    <br>
			    <br>
			    <%= widgetFactory.createTextArea( "TransDocText", "", shipmentTerms.getAttribute("trans_doc_text"), isReadOnly || isFromExpress,false, false, "class='resizeNoneTextArea' rows='2' cols='35'", "","none",textAreaMaxlength2 ) %>
			    			    
				</td>
			</tr>
			
			<tr>
				<td><%=widgetFactory.createCheckboxField("TransAddlDocInd", "",shipmentTerms.getAttribute("trans_addl_doc_included").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
				<td><% if(isExpressTemplate){%>
				<span class='hashMark'>#</span>
				<%}%><%=resMgr.getText("ImportDLCIssue.AddlTransportDocs",TradePortalConstants.TEXT_BUNDLE)%></td>
				<td colspan="3">
				<%
					if (isReadOnly || isFromExpress) {
				      out.println("&nbsp;");
				    } else {
				      options = ListBox.createOptionList(transportDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
				      defaultText1 = resMgr.getText("transaction.SelectPhrase",
				                                   TradePortalConstants.TEXT_BUNDLE); %>
				      <%= widgetFactory.createSelectField( "TransAddlDocItem", "", defaultText1, options, isReadOnly, false, false, "style='width:11em;' onChange=" + PhraseUtility.getPhraseChange(
		                        "/Terms/ShipmentTermsList/trans_addl_doc_text","TransAddlDocText", textAreaMaxlen,"document.forms[0].TransAddlDocText"), "", "none") %>
					    
				<%    }  
			    %>  
			
			    <%= widgetFactory.createNote("ImportDLCIssue.SpecifyNumber","")%>
			 <br>
			    <br>
			    <%= widgetFactory.createTextArea( "TransAddlDocText", "", shipmentTerms.getAttribute("trans_addl_doc_text"), isReadOnly || isFromExpress,false, false, "class='resizeNoneTextArea' rows='2' cols='62'", "","none",textAreaMaxlen ) %>
			    
				</td>
			</tr> 
		</tbody>	
		</table>
		
		<div class="columnLeft">
		<div style="margin-top:7px;">
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.Consignment",isReadOnly, false, isExpressTemplate, "" ) %>
		</div>
		<div>
			<div class = "formItem">
				<%=widgetFactory.createRadioButtonField( "ConsignmentType", "CONSIGN_ORDER", "ImportDLCIssue.ConsignedToOrderOf", TradePortalConstants.CONSIGN_ORDER, consignType.equals(TradePortalConstants.CONSIGN_ORDER), isReadOnly || isFromExpress) %>
			</div>		
			<%
			codesToExclude2 = new Vector(); 
	        codesToExclude2.addElement(TradePortalConstants.CONSIGN_BUYER);
	        codesToExclude2.addElement(TradePortalConstants.CONSIGN_BUYERS_BANK);
	        options = Dropdown.createSortedRefDataOptions( TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
	                                 shipmentTerms.getAttribute("trans_doc_consign_order_of_pty"),
	                                 loginLocale,codesToExclude2);  
	        %>
	        
			<%= widgetFactory.createSelectField( "ConsignToOrder", "", " ", options, isReadOnly || isFromExpress, false, false,  "class='formItemWithIndent4 formItemWithIndent8' onChange=\"pickConsignToOrderRadio();\" style='width:15em;'", "", "" ) %>
			 
		<div style="clear:both;"></div>
		</div>
		
		<div>
			<div class = "formItem inline">
			<%=widgetFactory.createRadioButtonField( "ConsignmentType", "CONSIGN_PARTY", "ImportDLCIssue.ConsignedTo", TradePortalConstants.CONSIGN_PARTY, consignType.equals(TradePortalConstants.CONSIGN_PARTY), isReadOnly || isFromExpress) %>
			</div>
			<%
			codesToExclude2.addElement(TradePortalConstants.CONSIGN_SHIPPER);  // DK IR SHUM071870717 Rel8.2 01/16/2013
			options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
                    shipmentTerms.getAttribute("trans_doc_consign_to_pty"),
                    loginLocale,codesToExclude2);
                    codesToExclude2=null;   
	        %>
			<%= widgetFactory.createSelectField( "ConsignToParty", "", " ", options, isReadOnly || isFromExpress, false, false, "onChange=\"pickConsignToPartyRadio();\" style='width:11em;'", "", "inline" ) %> 
		<div style="clear:both;"></div>
		</div>
		
		<br>
		<%
		 notifySearchHtml = widgetFactory.createPartySearchButton(itemid1,section1,isReadOnly,TradePortalConstants.NOTIFY_PARTY,false);
		 %>
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.NotifyParty",isReadOnly, false, isExpressTemplate, notifySearchHtml ) %>	    
		
		<%secureParms.put("not_terms_party_oid", termsPartyNot.getAttribute("terms_party_oid")); %>
		<input type=hidden name='NotTermsPartyType' value="<%=TradePortalConstants.NOTIFY_PARTY%>">
		<input type=hidden name="NotOTLCustomerId"  value="<%=termsPartyNot.getAttribute("OTL_customer_id")%>">
		<input type=hidden name="NotUserDefinedField1"
              value="<%=termsPartyNot.getAttribute("user_defined_field_1")%>">
               <input type=hidden name="NotUserDefinedField2"
              value="<%=termsPartyNot.getAttribute("user_defined_field_2")%>">
               <input type=hidden name="NotUserDefinedField3"
              value="<%=termsPartyNot.getAttribute("user_defined_field_3")%>">
               <input type=hidden name="NotUserDefinedField4"
              value="<%=termsPartyNot.getAttribute("user_defined_field_4")%>">
		<%
		    if (isFromExpress) {
		      // We are in a transaction created from express.  The notify party  
		      // fields are express fields.  This means they display as readonly 
		      // when the user is editing an instrument created from an express 
		      // template.  However, we still need to send the notify data to the 
		      // mediator.  Put the notify data in a bunch of hidden fields to 
		      // ensure this happens.
		%>
		      <input type=hidden name="NotName" 
		             value="<%=termsPartyNot.getAttribute("name")%>">
		      <input type=hidden name="NotAddressLine1"
		             value="<%=termsPartyNot.getAttribute("address_line_1")%>">
		      <input type=hidden name="NotAddressLine2"
		             value="<%=termsPartyNot.getAttribute("address_line_2")%>">
		      <input type=hidden name="NotAddressLine3"
		             value="<%=termsPartyNot.getAttribute("address_line_3")%>">
		      
		<%
		  }
		%>
		
		<%= widgetFactory.createTextField( "NotName", "", termsPartyNot.getAttribute("name"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "style='width:24em;' onBlur='checkNotifyPartyName(\"" + 
                StringFunction.escapeQuotesforJS(termsPartyNot.getAttribute("name")) + "\")'", "",""  ) %>
		<%= widgetFactory.createTextField( "NotAddressLine1", "", termsPartyNot.getAttribute("address_line_1"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "style='width:24em;'", "",""  ) %>
		<%= widgetFactory.createTextField( "NotAddressLine2", "", termsPartyNot.getAttribute("address_line_2"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "style='width:24em;'", "",""  ) %>
		<%= widgetFactory.createTextField( "NotAddressLine3", "", termsPartyNot.getAttribute("address_line_3"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "style='width:24em;'", "",""  ) %>
		</div>
		
		<div class="columnRight">
		<div style="margin-top:7px;">
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.MarkedFreight",isReadOnly, false, isExpressTemplate, "" ) %>
		</div>
		<div class = "formItem">
		<%=widgetFactory.createRadioButtonField( "MarkedFreightType", TradePortalConstants.PREPAID, "ImportDLCIssue.Prepaid", TradePortalConstants.PREPAID, markedFreight.equals(TradePortalConstants.PREPAID), isReadOnly || isFromExpress) %>
		<br/>
		<%=widgetFactory.createRadioButtonField( "MarkedFreightType", TradePortalConstants.COLLECT, "ImportDLCIssue.Collect", TradePortalConstants.COLLECT, markedFreight.equals(TradePortalConstants.COLLECT), isReadOnly || isFromExpress) %>
		</div>
		<br/><br/>
		<%
		
		consigneeSearchHtml = widgetFactory.createPartySearchButton(itemid2,section2,isReadOnly,TradePortalConstants.OTHER_CONSIGNEE,false);
		 %>
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.OtherConsignee",isReadOnly, false, isExpressTemplate, consigneeSearchHtml ) %>
		    
		<%secureParms.put("oth_terms_party_oid", 
                termsPartyOth.getAttribute("terms_party_oid"));
		%>
		    <input type=hidden name='OthTermsPartyType' 
		           value=<%=TradePortalConstants.OTHER_CONSIGNEE%>>
		    <input type=hidden name="OthOTLCustomerId"
		           value="<%=termsPartyOth.getAttribute("OTL_customer_id")%>">
		           <input type=hidden name="OthUserDefinedField1"
            value="<%=termsPartyOth.getAttribute("user_defined_field_1")%>">
             <input type=hidden name="OthUserDefinedField2"
            value="<%=termsPartyOth.getAttribute("user_defined_field_2")%>">
             <input type=hidden name="OthUserDefinedField3"
            value="<%=termsPartyOth.getAttribute("user_defined_field_3")%>">
             <input type=hidden name="OthUserDefinedField4"
            value="<%=termsPartyOth.getAttribute("user_defined_field_4")%>">
		<%
		    if (isFromExpress) {
		      // We are in a transaction created from express.  The other party  
		      // fields are express fields.  This means they display as readonly 
		      // when the user is editing an instrument created from an express 
		      // template.  However, we still need to send the otherdata to the 
		      // mediator.  Put the other data in a bunch of hidden fields to 
		      // ensure this happens.
		%>
		      <input type=hidden name="OthName" 
		             value="<%=termsPartyOth.getAttribute("name")%>">
		      <input type=hidden name="OthAddressLine1"
		             value="<%=termsPartyOth.getAttribute("address_line_1")%>">
		      <input type=hidden name="OthAddressLine2"
		             value="<%=termsPartyOth.getAttribute("address_line_2")%>">
		      <input type=hidden name="OthAddressLine3"
		             value="<%=termsPartyOth.getAttribute("address_line_3")%>">
             
		<%
		  }
		%>
		
		<%= widgetFactory.createTextField( "OthName", "", termsPartyOth.getAttribute("name"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "style='width:24em;' onBlur='checkOtherConsigneePartyName(\"" + 
                StringFunction.escapeQuotesforJS(termsPartyOth.getAttribute("name")) + "\")'", "",""  ) %>
		<%= widgetFactory.createTextField( "OthAddressLine1", "", termsPartyOth.getAttribute("address_line_1"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "style='width:24em;'", "",""  ) %>
		<%= widgetFactory.createTextField( "OthAddressLine2", "", termsPartyOth.getAttribute("address_line_2"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "style='width:24em;'", "",""  ) %>
		<%= widgetFactory.createTextField( "OthAddressLine3", "", termsPartyOth.getAttribute("address_line_3"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "style='width:24em;'", "",""  ) %>
		</div>
		
		<div style = "clear:both;"></div>
		
		<%=widgetFactory.createWideSubsectionHeader("ImportDLCIssue.Shipment") %>
		
		<div>
		
		<%= widgetFactory.createDateField( "ShipmentDate", "ImportDLCIssue.LastestShipDate", StringFunction.xssHtmlToChars(shipmentTerms.getAttribute("latest_shipment_date")), isReadOnly, false, isExpressTemplate,  "class='char6'", dateWidgetOptions, "inline" ) %>
        &nbsp;&nbsp;&nbsp;&nbsp;        
        <%=widgetFactory.createCheckboxField("TranshipAllowed", "ImportDLCIssue.TranshipAllowed",shipmentTerms.getAttribute("transshipment_allowed").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "inline" ) %>
        
		<div style = "clear:both;"></div>
		</div>
		
		<div>
		<%
        options = Dropdown.createSortedRefDataOptions( TradePortalConstants.INCOTERM,
                               shipmentTerms.getAttribute("incoterm"),
                               loginLocale);
		%>
		<%= widgetFactory.createSelectField( "Incoterm", "ImportDLCIssue.ShippingTerm", " ", options, isReadOnly || isFromExpress, false, isExpressTemplate,  "", "", "inline" ) %>
		<%= widgetFactory.createTextField( "IncotermLocation", "ImportDLCIssue.ShipTermLocation", shipmentTerms.getAttribute("incoterm_location"), "30", isReadOnly || isFromExpress, false, isExpressTemplate,  "", "","inline"  ) %>
		<div style="clear:both;"></div>
		</div>
        
		<div class="columnLeftNoBorder">		
		<span class="formItem"><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%><%= widgetFactory.createStackedLabel("From", "ImportDLCIssue.From") %></span>
		<%if(!(isReadOnly || isFromExpress)){ %>
			<%= widgetFactory.createTextField( "ShipFromPort", "ImportDLCIssue.FromPort", shipmentTerms.getAttribute("shipment_from"), "65", isReadOnly || isFromExpress, false, false,  "class='char30'", "",""  ) %>
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipFromPort", "ImportDLCIssue.FromPort", shipmentTerms.getAttribute("shipment_from"), isReadOnly || isFromExpress,false,false,"rows='2' cols='44'","","") %>
		<%} %>
		<%if(!(isReadOnly || isFromExpress)){ %>
			<%= widgetFactory.createTextField( "ShipFromLoad", "ImportDLCIssue.FromLoad", shipmentTerms.getAttribute("shipment_from_loading"), "65", isReadOnly || isFromExpress, false, false,  "class='char30'", "",""  ) %>
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipFromLoad", "ImportDLCIssue.FromLoad",shipmentTerms.getAttribute("shipment_from_loading"), isReadOnly || isFromExpress,false,false,"rows='2' cols='44'","","") %>
		<%} %>
		</div>
		
		<div class="columnRight">
		<span class="formItem"><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%><%= widgetFactory.createStackedLabel("To", "ImportDLCIssue.To") %></span>
		<%if(!(isReadOnly || isFromExpress)){ %>
			<%= widgetFactory.createTextField( "ShipToPort", "ImportDLCIssue.ToPort", shipmentTerms.getAttribute("shipment_to"), "65", isReadOnly || isFromExpress, false, false,  "class='char30'", "",""  ) %>
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipToPort", "ImportDLCIssue.ToPort", shipmentTerms.getAttribute("shipment_to"), isReadOnly || isFromExpress,false,false,"rows='2' cols='44'","","") %>
		<%} %>
		<%if(!(isReadOnly || isFromExpress)){ %>
			<%= widgetFactory.createTextField( "ShipToDischarge", "ImportDLCIssue.ToDischarge", shipmentTerms.getAttribute("shipment_to_discharge"), "65", isReadOnly || isFromExpress, false, false,  "class='char30'", "",""  ) %>
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipToDischarge", "ImportDLCIssue.ToDischarge",shipmentTerms.getAttribute("shipment_to_discharge"), isReadOnly || isFromExpress,false,false,"rows='2' cols='44'","","") %>
		<%} %>
		</div>
		
		<div style="clear:both;"></div>
		
		<%=widgetFactory.createWideSubsectionHeader("ImportDLCIssue.GoodsDesc", false,!isTemplate,false,"") %>
		<div>
		<%
        if (isReadOnly ) {%>
        	<div class="indent"><%=widgetFactory.createNote("common.RemindPOChanged")%></div>
        <%} else {
        	options = ListBox.createOptionList(goodsDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        	defaultText1 = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);
		%>
		<%-- KMehta - 26 Nov 2014 - Rel9.2 IR-T36000031194 - Change  - Begin --%>
		<%= widgetFactory.createSelectField( "GoodsPhraseItem", "", defaultText1, options, isReadOnly, false, false, "class='char15' onChange=" + PhraseUtility.getPhraseChange(
                "/Terms/ShipmentTermsList/goods_description",
                "GoodsDescText",
                "6500","document.forms[0].GoodsDescText"), "", "inline") %>
        <%-- KMehta - 26 Nov 2014 - Rel9.2 IR-T36000031194 - Change  - End --%>
		<div class="formItem inline"><%= widgetFactory.createNote("ImportDLCIssue.RemindPOAdded")%></div>
		<%
        }
		%>
		
		
				
		</div>
		<%if(!isReadOnly){ %>
		<%= widgetFactory.createTextArea( "GoodsDescText", "", shipmentTerms.getAttribute("goods_description").toString(), isReadOnly,false,false, "rows='10' cols='124'","","","") %>
			<%= widgetFactory.createHoverHelp("TransDocText","InstrumentIssue.PlaceHolder")%>
			<%= widgetFactory.createHoverHelp("TransAddlDocText","InstrumentIssue.PlaceHolder")%>
			<%=widgetFactory.createHoverHelp("notifyparty_imp", "PartySearchIconHoverHelp") %>
			<%=widgetFactory.createHoverHelp("consigneeparty_imp", "PartySearchIconHoverHelp") %>
		<%}else{%>
				<%= widgetFactory.createAutoResizeTextArea( "GoodsDescText", "", shipmentTerms.getAttribute("goods_description").toString(), isReadOnly,false,false, "style='width:600px;min-height:140px;' cols='100'","","") %>
		<%}%>
		<%-- =================================== Begin - No PO Line Items Assigned ===================================== --%>
		
		<%
        userOrgAutoLCCreateIndicator = userSession.getOrgAutoLCCreateIndicator();
        userOrgManualPOIndicator     = userSession.getOrgManualPOIndicator();

        transactionOid               = transaction.getAttribute("transaction_oid");

        // Determine if user can upload PO's
        if ((userOrgAutoLCCreateIndicator != null) && 
             (userOrgAutoLCCreateIndicator.equals(TradePortalConstants.INDICATOR_YES)))
        {
          userCanUploadPOs = true;
        }

        // Determine if user can manually enter PO's
        if ((userOrgManualPOIndicator != null) &&
              (userOrgManualPOIndicator.equals(TradePortalConstants.INDICATOR_YES)))
        {
          userCanAddManualPOs = true;
        }

        if (((userCanUploadPOs) || (userCanAddManualPOs) || isStructuredPO) && 
            ((SecurityAccess.hasRights(loginRights, SecurityAccess.DLC_PROCESS_PO)) ||
             (userSession.hasSavedUserSession() && 
             (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)))))
        {
         sqlQuery = new StringBuffer();
         if(!isStructuredPO){ 
          if (!InstrumentServices.isBlank(shipmentOid))
           {
             // First compose the SQL to retrieve PO Line Items that are currently assigned to 
             // this shipment
             sqlQuery = new StringBuffer();
             sqlQuery.append("select a_source_upload_definition_oid, ben_name, currency, source_type");
             sqlQuery.append(" from po_line_item where p_shipment_oid = ?");
			 //jgadela  R90 IR T36000026319 - SQL FIX
			 Object[] sqlParamsRepCont = new Object[1];
			 sqlParamsRepCont[0] =  shipmentOid;
             // Retrieve any PO line items that satisfy the SQL query that was just constructed
             poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParamsRepCont);

             if (poLineItemsDoc != null)
             {
               hasPOLineItems = true;

               poLineItems                   = poLineItemsDoc.getFragments("/ResultSetRecord");
               poLineItemDoc                 = (DocumentHandler) poLineItems.elementAt(0);
               poLineItemUploadDefinitionOid = poLineItemDoc.getAttribute("/A_SOURCE_UPLOAD_DEFINITION_OID");
               poLineItemBeneficiaryName     = poLineItemDoc.getAttribute("/BEN_NAME");
               poLineItemCurrency            = poLineItemDoc.getAttribute("/CURRENCY");
               poLineItemSourceType          = poLineItemDoc.getAttribute("/SOURCE_TYPE");
             }
           }

           // Next compose the SQL to retrieve PO Line Items that are currently assigned to 
           // this transaction if none are assigned to this shipment
           if (!hasPOLineItems)
           {
             sqlQuery = new StringBuffer();
             sqlQuery.append("select a_source_upload_definition_oid, ben_name, currency, source_type");
             sqlQuery.append(" from po_line_item where a_assigned_to_trans_oid = ?");
			 //jgadela  R90 IR T36000026319 - SQL FIX
			 Object[] sqlParamsRepCnt1 = new Object[1];
			 sqlParamsRepCnt1[0] =  transactionOid;
             // Retrieve any PO line items that satisfy the SQL query that was just constructed
             poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParamsRepCnt1);

             if (poLineItemsDoc != null)
             {
               poLineItems = poLineItemsDoc.getFragments("/ResultSetRecord");

               for (int poCount=0;poCount<poLineItems.size();poCount++)
               {
                 poLineItemDoc                 = (DocumentHandler) poLineItems.elementAt(poCount);
                 poLineItemUploadDefinitionOid = poLineItemDoc.getAttribute("/A_SOURCE_UPLOAD_DEFINITION_OID");
                 poLineItemBeneficiaryName     = poLineItemDoc.getAttribute("/BEN_NAME");
                 poLineItemCurrency            = poLineItemDoc.getAttribute("/CURRENCY");
                 poLineItemSourceType          = poLineItemDoc.getAttribute("/SOURCE_TYPE");

                 if (poLineItemSourceType.equals(TradePortalConstants.PO_SOURCE_UPLOAD))
                   break;

                 poLineItemUploadDefinitionOid = "";
                 poLineItemBeneficiaryName     = "";
               }
             }
           }
         }
         
         else if(isStructuredPO){
    		 sqlQuery.append("select a_upload_definition_oid, seller_name, currency");
             sqlQuery.append(" from purchase_order where a_shipment_oid = ?");	 
             //jgadela  R90 IR T36000026319 - SQL FIX
			Object[] sqlParamsPOLine = new Object[1];
			sqlParamsPOLine[0] =  shipmentOid;
             poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParamsPOLine);
             if (poLineItemsDoc != null)
               {
            	 hasStructuredPOs = true;
            	 poLineItems                   = poLineItemsDoc.getFragments("/ResultSetRecord");
                 poLineItemDoc                 = (DocumentHandler) poLineItems.elementAt(0);
                 poLineItemUploadDefinitionOid = poLineItemDoc.getAttribute("/A_UPLOAD_DEFINITION_OID");
                 poLineItemBeneficiaryName     = poLineItemDoc.getAttribute("/SELLER_NAME");
                 poLineItemCurrency            = poLineItemDoc.getAttribute("/CURRENCY");
                }
    	  }
           // Use the transaction's currency if a po line item currency does not exist
           if (InstrumentServices.isBlank(poLineItemCurrency))
             poLineItemCurrency = currency;

           // Store po line item data in secure parms which will be used by the pages
           // which add/remove po line items
           secureParms.put("addPO-uploadDefinitionOid", poLineItemUploadDefinitionOid);
           secureParms.put("addPO-beneficiaryName", poLineItemBeneficiaryName);
           secureParms.put("addPO-currency", poLineItemCurrency);
           secureParms.put("addPO-shipment_oid", shipmentOid);
           secureParms.put("addPO-shipmentNumber", String.valueOf(shipmentNumber));
           secureParms.put("addPO-searchType", TradePortalConstants.PO_DEFINITION_TYPE_MANUAL);
           secureParms.put("addPO-hasPOLineItems", String.valueOf(hasPOLineItems));
           secureParms.put("addPO-hasStructuredPO", String.valueOf(hasStructuredPOs));
     %>

		  <%
		  if (!hasStructuredPOs && !hasPOLineItems && (!(isReadOnly || isFromExpress)))
          {
         %>
         <div class = "formItem">
               <%
               
                 if (userCanUploadPOs && !isTemplate && !isStructuredPO)
                 {
               %>
                   
                 <button data-dojo-type="dijit.form.Button"  name="AddUploadedPO" id="AddUploadedPO" type="submit">
	                   <%=resMgr.getText("common.AddUploadedPOLineItemsText",TradePortalConstants.TEXT_BUNDLE)%>
	                   <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                        setButtonPressed('AddUploadedPOLineItemsButton', '0');
	                        document.forms[0].submit();
	                   </script>
	                </button>
               <%
                 }

                 if (userCanUploadPOs && userCanAddManualPOs && !isStructuredPO)
                 {
               %>
                 &nbsp;
               <%
                 }

                 if (userCanAddManualPOs && !isTemplate && !isStructuredPO)
                 {
               %>
               
                  <button data-dojo-type="dijit.form.Button"  name="AddManuallyPO" id="AddManuallyPO" type="submit">
	                   <%=resMgr.getText("common.AddPOLineItemsManuallyText",TradePortalConstants.TEXT_BUNDLE)%>
	                   <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                        setButtonPressed('AddPOLineItemsManuallyButton', '0');
	                        document.forms[0].submit();
	                   </script>
	                </button>
               <%
                 }
               %>
               <%
               if (isStructuredPO && !hasStructuredPOs)
               {
             %>
		         	<button data-dojo-type="dijit.form.Button"  name="AddStructuredPOButton" id="AddStructuredPOButton" onclick="AddPOStructure();" type="button">
		            <%=resMgr.getText("common.AddStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>
		         </button>
                
             <%
               }
             %>
               </div>
    <%
          }
        }
    %>


<%-- =================================== End - No PO Line Items Assigned ===================================== --%>

<%-- =================================== Begin - PO Line Items Assigned ===================================== --%>

<%
 // It is possible that after a transaction has been processed by bank that it has
 // no POs assigned to it because these POs have been moved to amendments.  However,
 // we still want to display the PO information in the PO line items field.
 if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK) &&
       !InstrumentServices.isBlank(shipmentTerms.getAttribute("po_line_items")))
           hasPOLineItems = true;
 if ((((userCanUploadPOs) || (userCanAddManualPOs)) && hasPOLineItems) 
 		|| (userSession.isEnableAdminUpdateInd() && userSession.getSecurityType().equals(TradePortalConstants.ADMIN))) //Added condition of ADMIN so that Admin can view POs from Update Centre - Rel9.3.5 IR#45181
 {
%>
<%--PR ER-T36000015639 BEGIN. Changes done to enable right click, text highlighting, Copy options.--%>
<%if(!isReadOnly){%>
<div class="formItem">
<%=widgetFactory.createPOTextArea("POLineItems", shipmentTerms.getAttribute("po_line_items"), "78", "10", 
        "POLineItems", false, "", "OFF",true)%>
        </div>
        <%}else{%>
        <%=widgetFactory.createAutoResizeTextArea("POLineItems","", shipmentTerms.getAttribute("po_line_items"), isReadOnly, false, false, "style='width:600px;min-height:140px;' cols='100'","","")%>
        <%}%>
        
<%--PR ER-T36000015639 END. Changes done to enable right click, text highlighting, Copy options.--%>

          <%
             if ((SecurityAccess.hasRights(loginRights, SecurityAccess.DLC_PROCESS_PO)) && 
                 (!(isReadOnly || isFromExpress)))
             {
          %>
          			<div class = "formItem">
                      <%
                        if (poLineItemSourceType.equals(TradePortalConstants.PO_SOURCE_UPLOAD))
                        {
                      %>
                      	
	      	       		<button data-dojo-type="dijit.form.Button"  name="AddPOLineItems" id="AddPOLineItems" type="submit">
		                   <%=resMgr.getText("common.AddPOLineItemsText",TradePortalConstants.TEXT_BUNDLE)%>
		                   <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		                        setButtonPressed('AddPOLineItems', '0');
		                        document.forms[0].submit();
		                   </script>
		                </button>
		                
		                <button data-dojo-type="dijit.form.Button"  name="RemovePOLineItemsButton" id="RemovePOLineItemsButton" type="submit">
		                   <%=resMgr.getText("common.RemovePOLineItemsText",TradePortalConstants.TEXT_BUNDLE)%>
		                   <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		                        setButtonPressed('RemovePOLineItemsButton', '0');
		                        document.forms[0].submit();
		                   </script>
		                </button>
	      	       			
                            
                      <%
                        }
                        else
                        {
                      %>
                      		<button data-dojo-type="dijit.form.Button"  name="EditPOLineItemsButton" id="EditPOLineItemsButton" type="submit">
	 		                   <%=resMgr.getText("common.EditPOLineItemsText",TradePortalConstants.TEXT_BUNDLE)%>
	 		                   <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	 		                        setButtonPressed('EditPOLineItemsButton', '0');
	 		                        document.forms[0].submit();
	 		                   </script>
	 		                </button>
                      <%
                        }
                      %>
                 </div>
          <%
             }
          %>
   
<%
 }
%>

<%-- =================================== End - PO Line Items Assigned ===================================== --%>
<%-- =================================== start of Structured PO Assigned=================================== --%>

<% 

     if (isStructuredPO && hasStructuredPOs)
      {
%>
        <div class = "formItem">                   
           <button data-dojo-type="dijit.form.Button"  name="ViewStructuredPOButton" id="ViewStructuredPOButton" onclick="ViewPOStructure();" type="button">
		 		 <%=resMgr.getText("common.ViewStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>
		   </button>		        
	        
<%
		  if ((SecurityAccess.hasRights(loginRights, SecurityAccess.DLC_PROCESS_PO)) &&
			(!(isReadOnly || isFromExpress)))
                {
%>

            <button data-dojo-type="dijit.form.Button"  name="AddMoreStructuredPOButton" id="AddMoreStructuredPOButton" onclick="AddPOStructure();" type="button">
		 		 <%=resMgr.getText("common.AddMoreStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>
		   </button>
            
            <button data-dojo-type="dijit.form.Button"  name="RemoveStructuredPOButton" id="RemoveStructuredPOButton" onclick="RemovePOStructure();" type="button">
		 		 <%=resMgr.getText("common.RemoveStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>
		   </button>
  <%
           }
  %> 
        </div>
  <%		
     }
  %>
<%if(!isReadOnly){ %>
			<%= widgetFactory.createHoverHelp("AddManuallyPO","common.AddPOLineItemsManuallyText")%>
			<%= widgetFactory.createHoverHelp("AddUploadedPO","common.AddUploadedPOLineItemsText")%>
			<%=widgetFactory.createHoverHelp("AddPOLineItems", "common.AddPOLineItemsText") %>
			<%=widgetFactory.createHoverHelp("RemovePOLineItemsButton", "common.RemovePOLineItemsText") %>
			<%=widgetFactory.createHoverHelp("EditPOLineItemsButton", "common.EditPOLineItemsText") %>
			<%= widgetFactory.createHoverHelp("AddStructuredPOButton", "common.AddUploadedPurchaseOrderText")%>
			<%=widgetFactory.createHoverHelp("ViewStructuredPOButton", "common.ViewUploadedPurchaseOrderText") %>
			<%=widgetFactory.createHoverHelp("AddMoreStructuredPOButton", "common.AddUploadedPurchaseOrderText") %>
			<%=widgetFactory.createHoverHelp("RemoveStructuredPOButton", "common.RemoveUploadedPurchaseOrderText") %>
		<%} %>
  
  <%--============================================end of Structured PO Assigned ========================================== --%>
</div>
