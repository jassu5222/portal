<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Loan Request Page - Export Loan Instructions section

  Description:
    Contains HTML to create the Loan Request Export Payment Instructions section.  
    It shows the payment instruction when the loan type is Export (Export_indicator = "N").

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-ExportLoanInstructions.frag" %>
*******************************************************************************
--%>

<div>
      <%=widgetFactory.createWideSubsectionHeader("LoanRequest.LoanProceeds")%>
      <div>
            <%    options = Dropdown.createSortedCurrencyCodeOptions(isExport? loanProceedsPmtCurrency: "", loginLocale);            %>
            <%=widgetFactory.createSelectField("LoanProceedsPmtCurrExp","LoanRequest.CcyOfPayment", defaultCurrencyVal, options, isReadOnly,!isTemplate, false, "style=\"width: 50px;\"", "", "inline")%>
            <%=widgetFactory.createAmountField("LoanProceedsPmtAmountExp","LoanRequest.LoanProceedsAmount", isExport ? displayLoanProceedsPmtAmount: "", loanProceedsPmtCurrency, isReadOnly, false, false,"style=\"width: 126px;\"", "", "inline")%>
            <div style="clear: both;"></div>
      </div>
</div>
<div>
      <%=widgetFactory.createWideSubsectionHeader("LoanRequest.TransactionFinanced", isReadOnly, !isTemplate,false, "")%>
      <%
            if (InstrumentServices.isBlank(linkedInstrumentID)) {
                  linkedInstrumentID = terms.getAttribute("linked_instrument_id");
            }
            String linkedDLCInstrumentID = "", linkedCOLInstrumentID = "";
            if (searchInstrumentType
                        .equals(TradePortalConstants.ALL_EXPORT_DLC)
                        || financeType.equals(InstrumentType.EXPORT_DLC)) {
                  linkedDLCInstrumentID = linkedInstrumentID;
            } else if (searchInstrumentType
                        .equals(InstrumentType.EXPORT_COL)
                        || financeType.equals(InstrumentType.EXPORT_COL)) {
                  linkedCOLInstrumentID = linkedInstrumentID;
            }

            //TLE - 03/30/07 - IR-AYUH021051356 - Add Begin
            // Set value for these below boolean that will be used
            // when creating radio button/tex field/button for
            // related instrument LC/CO/Other. Please note that these
            // below variables are used for that purpose only.
            boolean lb_ReadOnlyExportLC = false;
            boolean lb_ReadOnlyExportCO = false;
            boolean lb_ReadOnlyOther = false;

            if ((ib_rejectedByBankReadOnly) && (!isReadOnly)) {
                  // W Zhu 4/19/07 AUH031462056 Check financetype instead of linked 
                  // instrument id, which could be empty
                  //if (!InstrumentServices.isBlank(linkedDLCInstrumentID))
                  if (financeType.equals(InstrumentType.EXPORT_DLC)) {
                        lb_ReadOnlyExportLC = false;
                        lb_ReadOnlyExportCO = true;
                        lb_ReadOnlyOther = true;
                  }
                  //else if (!InstrumentServices.isBlank(linkedCOLInstrumentID))
                  else if (financeType.equals(InstrumentType.EXPORT_COL)) {
                        lb_ReadOnlyExportCO = false;
                        lb_ReadOnlyExportLC = true;
                        lb_ReadOnlyOther = true;
                  } else {
                        lb_ReadOnlyOther = false;
                        lb_ReadOnlyExportCO = true;
                        lb_ReadOnlyExportLC = true;
                  }

                  // W Zhu 4/19/07 AUH031462056 add hidden field 
                  // Although FinanceType is read-only when the transaction is rejected,
                  // We need set the value of this field so that the back-end can use it 
                  // to validate the other fields correctly.
      %>
      <input type="hidden" name="FinanceType" value="<%=financeType%>">
      <%
            } else {
                  if (isReadOnly) {
                        lb_ReadOnlyExportLC = true;
                        lb_ReadOnlyExportCO = true;
                        lb_ReadOnlyOther = true;
                  }
            }
            //TLE - 03/30/07 - IR-AYUH021051356 - Add End
      %>
      <input type="hidden" name="SearchInstrumentType" value="">
                  <%    String instruButton = "";
                        String ItemID1="LinkedDLCInstrumentID";
                        String section1="InstrumentId";
                        String section2="InstrumentId_1";
                        if (!isReadOnly) {String searchValue1 = TradePortalConstants.ALL_EXPORT_DLC;
                        clickTag1 = "setButtonPressed('"+TradePortalConstants.BUTTON_INSTRUMENT_SEARCH+"','0');return setInstrumentSearchType('" + searchValue1+ "');";     
                        //cquinton 9/19/2012 use createSearchLink method instead, which does not hardcode img tags
                        
                        instruButton = widgetFactory.createInstrumentSearchButton(ItemID1,section1,isReadOnly,searchValue1, false);%>
                  <% } %>

      <div>
            <div>
                  <div class="formItem inline">
                  	<%=widgetFactory.createRadioButtonField("FinanceType",InstrumentType.EXPORT_DLC,"",InstrumentType.EXPORT_DLC,searchInstrumentType.equals(TradePortalConstants.ALL_EXPORT_DLC)|| financeType.equals(InstrumentType.EXPORT_DLC),ib_rejectedByBankReadOnly, "onClick=\"handleAddInvoiceButton('enable')\"", "")%>  
                  	<%=widgetFactory.createInlineLabel("","LoanRequest.ExportLC")%>
                  </div>                  
            	  <div class="formItem inline">
            	  	<span style="margin-left: 79px"><%=widgetFactory.createInlineLabel("","LoanRequest.RelatedInstrumentID")%></span>	
            	  </div>
            	  <div class="formItem inline"> 
                  	<%=widgetFactory.createTextField("LinkedDLCInstrumentID","",linkedDLCInstrumentID, "16", lb_ReadOnlyExportLC,false, false,"onChange=\"pickRelatedInstExp('EXP_DLC');\"", "","", instruButton)%>
                  </div>
                  <div style="clear: both;"></div>                
            </div>
            <div>
                  <div class="formItem inline">
                        <%=widgetFactory.createRadioButtonField("FinanceType",InstrumentType.EXPORT_COL,"",InstrumentType.EXPORT_COL,searchInstrumentType.equals(InstrumentType.EXPORT_COL)|| financeType.equals(InstrumentType.EXPORT_COL),ib_rejectedByBankReadOnly, "onClick=\"handleAddInvoiceButton('enable')\"", "")%>
                  		<%=widgetFactory.createInlineLabel("","LoanRequest.ExportCOL")%>
                  </div>
				  <div class="formItem inline">
				  <span class="formItemWithIndent2"><%=widgetFactory.createInlineLabel("","LoanRequest.RelatedInstrumentID")%></span>   
				  </div>
				  <div class="formItem inline">               
                  <%    String instruButtonHTML2 = "";
                         ItemID1="LinkedCOLInstrumentID";
                        if (!isReadOnly) {String searchValue2 = InstrumentType.EXPORT_COL;
                        clickTag2 = "setButtonPressed('"+TradePortalConstants.BUTTON_INSTRUMENT_SEARCH+"','0');return setInstrumentSearchType('" + searchValue2 + "');";                            
                        //cquinton 9/19/2012 use createSearchLink method instead, which does not hardcode img tags
                      instruButtonHTML2 = widgetFactory.createInstrumentSearchButton(ItemID1,section2,isReadOnly, searchValue2, false);%>
                  <%    } %>
                  <%=widgetFactory.createTextField("LinkedCOLInstrumentID","",linkedCOLInstrumentID, "16", lb_ReadOnlyExportCO,false, false,"onChange=\"pickRelatedInstExp('EXP_COL');\"", "","inline",instruButtonHTML2)%>
                  </div>                      
                  <div style="clear: both;"></div>                
            </div>
            <div class="formItem inline">
            		<%--IR 21233- hide the Add Invoice button --%>
                  <%=widgetFactory.createRadioButtonField("FinanceType","PRESHIPMENT_FINANCING","",TradePortalConstants.PRESHIPMENT_FINANCING,financeType.equals(TradePortalConstants.PRESHIPMENT_FINANCING),ib_rejectedByBankReadOnly||areInvoicesAttached, "onClick=\"handleAddInvoiceButton('hide')\"", "")%>
                  <%=widgetFactory.createInlineLabel("","LoanRequest.StandalonePreshipmentFinanced")%>
                  <div>
                  <br/>
                        <%=widgetFactory.createTextArea("FinancedPOsDetailsText","", terms.getAttribute("financed_pos_details_text"),lb_ReadOnlyOther || areInvoicesAttached, false, false,"onChange=\"pickPreshipmentFinancingRadio();\" rows='10' cols='250'class='formItemWithIndent3'", "", "")%>
                  </div>
            </div>
            <div style="clear: both;"></div>
            <div class="formItem inline">
                  <%=widgetFactory.createRadioButtonField("FinanceType","LoanRequestOTHER","LoanRequest.OtherTransacionFinanced",TradePortalConstants.OTHER,financeType.equals(TradePortalConstants.OTHER),ib_rejectedByBankReadOnly||areInvoicesAttached, "onClick=\"handleAddInvoiceButton('disable')\"", "")%>
                  <div><br/>
                        <%=widgetFactory.createTextArea("FinanceTypeTextExp", "",isExport ? financeTypeText : "", lb_ReadOnlyOther || areInvoicesAttached, false,false, "onChange=\"pickOtherRadioExp();\" rows='10' cols='250'class='formItemWithIndent3' ", "", "")%>
                  </div>
            </div>
            <div style="clear: both;"></div>
      </div>
</div>
<%-- Radio Section Ends --%>
<div>
      <div class="columnLeft">
            <%=widgetFactory.createSubsectionHeader("LoanRequest.ApplyLoanProceedsTo",(isReadOnly || isFromExpress), !isTemplate, false, "")%>
            <div>
                  <div>             
                        <div class="formItem inline">
                              <%=widgetFactory.createRadioButtonField("LoanProceedsCreditTypeExp",TradePortalConstants.CREDIT_OUR_ACCT,"LoanRequest.OurAccount",TradePortalConstants.CREDIT_OUR_ACCT, (isExport? loanProceedsCreditType: "").equals(TradePortalConstants.CREDIT_OUR_ACCT),isReadOnly,"onClick=handleBeneficiaryRequirdness('ExportLoan',false)","")%>          
                              <%    // Using the acct_choices xml from the app party,build the dropdown and 
                                    // select the one matching loan_proceeds_credit_acct
                                    appAcctList = StringFunction.xssHtmlToChars(termsPartyApp.getAttribute("acct_choices"));
                                    DocumentHandler acctOptions = new DocumentHandler(appAcctList, true);
                                    options = Dropdown.createSortedAcctOptions(acctOptions,isExport ? StringFunction.xssHtmlToChars(terms.getAttribute("loan_proceeds_credit_acct")) : "",loginLocale);
                              %>
                        </div>
                        <%=widgetFactory.createSelectField("LoanProceedsCreditAccountExp", "", " ", options,isReadOnly, false, false,"onChange=\"pickOurAccountRadioExp();\""+"style='width:17em'", "", "inline")%>
                        <div style="clear: both;"></div>
                  </div>
                  <div style="clear: both;"></div>                
                  <br/>
                  <div class="formItem">
                        <%=widgetFactory.createRadioButtonField("LoanProceedsCreditTypeExp",TradePortalConstants.CREDIT_BEN_ACCT,"",TradePortalConstants.CREDIT_BEN_ACCT, (isExport ? loanProceedsCreditType: "").equals(TradePortalConstants.CREDIT_BEN_ACCT),isReadOnly, "onClick=handleBeneficiaryRequirdness('ExportLoan',true)", "")%>
                  <%=resMgr.getText("LoanRequest.TheFollowingBeneficiary",TradePortalConstants.TEXT_BUNDLE)%>
                  <%=widgetFactory.createNote("LoanRequest.TheFollowingBeneficiaryitalic")%>
                  </div>
                  <br/>
                  <div class="formItem">
                        <%=widgetFactory.createRadioButtonField("LoanProceedsCreditTypeExp","creditOtherAcct","LoanRequest.OtherRadio",TradePortalConstants.CREDIT_OTHER_ACCT, (isExport? loanProceedsCreditType: "").equals(TradePortalConstants.CREDIT_OTHER_ACCT),isReadOnly, "onClick=handleBeneficiaryRequirdness('ExportLoan',false)", "")%>
                  <%if(!isReadOnly){ %>(<a href='javascript:openOtherConditionsDialog("SpclBankInstructions","SaveTrans", "<%=resMgr.getText("common.OtherDetails",TradePortalConstants.TEXT_BUNDLE)%>");'  onclick="setCreditOtherAcct()"><%=resMgr.getText("common.OtherDetails",TradePortalConstants.TEXT_BUNDLE)%></a>)
                   <%}else{ %>                  
                        <%=resMgr.getText("common.OtherDetails",TradePortalConstants.TEXT_BUNDLE)%>
                   <%} %>
                  </div>
                  
                  <br/>
                        <div style="clear: both;"></div>
                  
            </div>
            <%
                  String benExpSearchHtml = "";
                  String benExpClearHtml = "";
                  String exportBankidentifierStr1 = ",BbkUserDefinedField1,BbkUserDefinedField2,BbkUserDefinedField3,BbkUserDefinedField4";
                  String exportBankidentifierStr = ",BbkNameExp,BbkAddressLine1Exp,BbkAddressLine2Exp,BbkCityExp,BbkStateProvinceExp,BbkPostalCodeExp,BbkCountryExp,BbkOTLCustomerIdExp,BenUserDefinedField1,BenUserDefinedField2,BenUserDefinedField3,BenUserDefinedField4";
                  String identifierStr2 = "BenNameExp,BenAddressLine1Exp,BenAddressLine2Exp,BenCityExp,BenStateProvinceExp,BenPostalCodeExp,BenCountryExp,BenOTLCustomerIdExp"+exportBankidentifierStr+exportBankidentifierStr1;
                  String sectionName2 = "benexp";
                  if (!(isReadOnly)) {
            %>
            <%
                  benExpSearchHtml = widgetFactory.createPartySearchButton(identifierStr2, sectionName2, false,TradePortalConstants.BENEFICIARY, false);
                        /* extra Tags : clearBeneficiaryExp();*/
                        benExpClearHtml = widgetFactory.createPartyClearButton("ClearBenButton", "clearBeneficiaryExp()", false, "");
            %>
            <%
                  }// Always send the ben terms party oid
                  secureParms.put("ben_terms_party_oid_exp",termsPartyBen.getAttribute("terms_party_oid"));
            %>
            <%=widgetFactory.createSubsectionHeader("LoanRequest.Beneficiary", isReadOnly, false, false,(benExpSearchHtml + benExpClearHtml))%>
            
                  

            <input type=hidden name='BenTermsPartyTypeExp' value=<%=TradePortalConstants.BENEFICIARY%>> 
            
            <input type=hidden name="BenUserDefinedField1"
                  value="<%=termsPartyBen.getAttribute("user_defined_field_1")%>">
             <input type=hidden name="BenUserDefinedField2"
                  value="<%=termsPartyBen.getAttribute("user_defined_field_2")%>">
             <input type=hidden name="BenUserDefinedField3"
                  value="<%=termsPartyBen.getAttribute("user_defined_field_3")%>">
             <input type=hidden name="BenUserDefinedField4"
                  value="<%=termsPartyBen.getAttribute("user_defined_field_4")%>"> 
             
            <%=widgetFactory.createTextField("BenOTLCustomerIdExp","",termsPartyBen.getAttribute("OTL_customer_id"),"35",false,false,false," type=hidden ","", "")%>
            <input type=hidden name="VendorIdExp" value="<%=termsPartyBen.getAttribute("vendor_id")%>">
            <%=widgetFactory.createTextField("BenNameExp","LoanRequest.BeneName",isExport ? (termsPartyBen.getAttribute("name")) : "","35",isReadOnly,false,false,"onBlur='checkBeneficiaryNameExp(\""+ StringFunction.escapeQuotesforJS(termsPartyBen.getAttribute("name"))+ "\"); pickTheFollowingBeneficiaryRadioExp();'","", "")%>

            <%=widgetFactory.createTextField("BenAddressLine1Exp","LoanRequest.BeneAddressLine1",isExport? (termsPartyBen.getAttribute("address_line_1")): "", "35", isReadOnly, false, false,"onBlur='pickTheFollowingBeneficiaryRadioExp();'", "", "")%>

            <%=widgetFactory.createTextField("BenAddressLine2Exp","LoanRequest.BeneAddressLine2",isExport? (termsPartyBen.getAttribute("address_line_2")): "", "35", isReadOnly, false, false, "", "", "")%>
			<%-- KMehta - 17 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - Begin --%>
            <%=widgetFactory.createTextField("BenCityExp","LoanRequest.BeneCity",isExport? (termsPartyBen.getAttribute("address_city")): "", "23", isReadOnly, false, false,"onBlur='pickTheFollowingBeneficiaryRadioExp();'", "", "")%>
            <%-- KMehta - 17 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - End --%>
            <div>
                  <%=widgetFactory.createTextField("BenStateProvinceExp","LoanRequest.BeneProvinceState",isExport ? (termsPartyBen.getAttribute("address_state_province")) : "", "8",isReadOnly, false, false, "", "", "inline")%>


                  <%=widgetFactory.createTextField("BenPostalCodeExp","LoanRequest.BenePostalCode",isExport ? (termsPartyBen.getAttribute("address_postal_code")) : "", "15",isReadOnly, false, false, "class='char10'", "", "inline")%>
                  <div style="clear: both;"></div>
            </div>
            <%
                  options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY,isExport? (termsPartyBen.getAttribute("address_country")): "", loginLocale);
            %>
            <%=widgetFactory.createSelectField("BenCountryExp","LoanRequest.BeneCountry", " ", options, isReadOnly, false,false, "class='char35'", "", "")%>
            <%-- Benificiary Account Number --%>
            
			<%--Beneficiary account number field can be either a text box or a set of radio buttons.
				we set the label here and put content in special div that is replaced via ajax as necessary.
				We use a partial page loading section to give a visual indicator--%>
			<div class='formItem<%=(!isTemplate) ? " required" : ""%>'><%=widgetFactory.createInlineLabel("SelectedAccount",
						"LoanRequest.BeneficiaryAccountNumber")%>
				<div style="clear: both;"></div>
				<div id="beneAccountsDataLoading" style="display: none;"><%--hide it to start--%>
				<span class='dijitInline dijitIconLoading'></span> <%=resMgr.getText("common.Loading",
									TradePortalConstants.TEXT_BUNDLE)%>
				</div>
				
				<div id="exportBeneAccountsData">
				<%
					//setup the include. we pass following variables
					TermsPartyWebBean termsParty = termsPartyBen;
				%> <%@ include file="Transaction-LRQ-ISS-ExportLoan-BeneAcctData.frag"%>
				</div>
			</div>
		
            
      </div>
      <%-- Left Col --%>
      <div class="columnRight">
            <%
                  String benBankExpSearchHtml = "";
                  String identifierStr3 = "BbkNameExp,BbkAddressLine1Exp,BbkAddressLine2Exp,BbkCityExp,BbkStateProvinceExp,BbkPostalCodeExp,BbkCountryExp,BbkOTLCustomerIdExp,BbkUserDefinedField1,BbkUserDefinedField2,BbkUserDefinedField3,BbkUserDefinedField4";
                  String sectionName3 = "bbkexp";
                  if (!(isReadOnly)) {
                  benBankExpSearchHtml = widgetFactory.createPartySearchButton(identifierStr3,sectionName3, false,TradePortalConstants.BENEFICIARY_BANK, false);
                  }

                  // Always send the bbk terms party oid
                  secureParms.put("bbk_terms_party_oid_exp",termsPartyBbk.getAttribute("terms_party_oid"));
            %>
            <%=widgetFactory.createSubsectionHeader("LoanRequest.BankBeneficiary", isReadOnly, false, false,benBankExpSearchHtml)%>

            <input type=hidden name='BbkTermsPartyTypeExp'value=<%=TradePortalConstants.BENEFICIARY_BANK%>> 
            
            <input type=hidden name="BbkUserDefinedField1"
                  value="<%=termsPartyBbk.getAttribute("user_defined_field_1")%>">
             <input type=hidden name="BbkUserDefinedField2"
                  value="<%=termsPartyBbk.getAttribute("user_defined_field_2")%>">
             <input type=hidden name="BbkUserDefinedField3"
                  value="<%=termsPartyBbk.getAttribute("user_defined_field_3")%>">
             <input type=hidden name="BbkUserDefinedField4"
                  value="<%=termsPartyBbk.getAttribute("user_defined_field_4")%>">
                  
            <%=widgetFactory.createTextField("BbkOTLCustomerIdExp","",termsPartyBbk.getAttribute("OTL_customer_id"),"35",false,false,false," type=hidden ","", "")%>
            <%=widgetFactory.createTextField("BbkNameExp","LoanRequest.BeneBankName",isExport ? termsPartyBbk.getAttribute("name") : "","35",isReadOnly,false,false,"onBlur='checkBeneficiaryBankNameExp(\""+ termsPartyBbk.getAttribute("name") + "\")'", "","")%>

            <%=widgetFactory.createTextField("BbkAddressLine1Exp","LoanRequest.BeneBankAddressLine1", isExport? (termsPartyBbk.getAttribute("address_line_1")): "", "35", isReadOnly, false, false,"onBlur='pickTheFollowingBeneficiaryRadioExp();'", "", "")%>

            <%=widgetFactory.createTextField("BbkAddressLine2Exp","LoanRequest.BeneBankAddressLine2", isExport? (termsPartyBbk.getAttribute("address_line_2")): "", "35", isReadOnly, false, false, "", "", "")%>
			<%-- KMehta - 17 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - Begin --%>
            <%=widgetFactory.createTextField("BbkCityExp","LoanRequest.BeneBankCity",isExport? (termsPartyBbk.getAttribute("address_city")): "", "23", isReadOnly, false, false,"onBlur='pickTheFollowingBeneficiaryRadioExp();'", "", "")%>
            <%-- KMehta - 17 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - End --%>
            <div>
                  <%=widgetFactory.createTextField("BbkStateProvinceExp","LoanRequest.BeneBankProvinceState",isExport ? (termsPartyBbk.getAttribute("address_state_province")) : "", "8",isReadOnly, false, false, "", "", "inline")%>


                  <%=widgetFactory.createTextField("BbkPostalCodeExp","LoanRequest.BeneBankPostalCode",isExport ? (termsPartyBbk.getAttribute("address_postal_code")) : "", "15",isReadOnly, false, false, "class='char10'", "", "inline")%>
                  <div style="clear: both;"></div>
            </div>
            <%
                  options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY,isExport? (termsPartyBbk.getAttribute("address_country")): "", loginLocale);
            %>
            <%=widgetFactory.createSelectField("BbkCountryExp","LoanRequest.BeneBankCountry", " ", options, isReadOnly,false, false, "class='char35'", "", "")%>
            <div style="clear: both;"></div>
      </div>
      <div style="clear: both;"></div>
      <%--  Right Column Ends --%>

</div>
<div>
<%@ include file="Transaction-LRQ-ISS-ExportLoan-InvoiceDetails.frag" %>
<div style="clear:both;"></div>  
      <%=widgetFactory.createWideSubsectionHeader("LoanRequest.LoanMaturity",false, !isTemplate, false,"")%>
      <div class="columnLeft">
            <%
                  String loanIndicator = isExport ? loanMaturityDebitType : "";
                  boolean debitApppInd = loanIndicator.equals(TradePortalConstants.DEBIT_OTHER);
                  boolean debitExpInd = loanIndicator.equals(TradePortalConstants.DEBIT_EXP);
            %>
            <div class="formItem">
                  <%=widgetFactory.createRadioButtonField("LoanMaturityDebitTypeExp", TradePortalConstants.DEBIT_EXP,"LoanRequest.OffsetExpLCAndCollection",TradePortalConstants.DEBIT_EXP, (isExport?loanMaturityDebitType:"").equals(TradePortalConstants.DEBIT_EXP),isReadOnly, "onClick=\"clearLoanMaturityDebitOtherText();\"", "")%>           
            </div>            
      </div>
      <div class="columnRight">
      <%=widgetFactory.createRadioButtonField("LoanMaturityDebitTypeExp", TradePortalConstants.DEBIT_OTHER,"LoanRequest.OtherRadio",TradePortalConstants.DEBIT_OTHER, (isExport?loanMaturityDebitType:"").equals(TradePortalConstants.DEBIT_OTHER),isReadOnly, "", "")%>

<br/>
                  <%=widgetFactory.createTextArea("LoanMaturityDebitOtherTextExp","",isExport? (terms.getAttribute("loan_maturity_debit_other_txt")): "", isReadOnly, false, false,"onChange='pickDebitOtherExpRadio();' class='formItemWithIndent3'", "", "")%>
                <%=widgetFactory.createHoverHelp("LoanMaturityDebitOtherTextExp", "LoanMaturityHoverText")%>
            <div style="clear: both;"></div>
      </div>
</div>

<script LANGUAGE="JavaScript">

      <%-- PICK  DLC INSTRUMENT RADIO IF RELATED INSTRUMENT ID IS ENTERED --%>
      function pickLinkedDLCInstrumentRadioExp() {
          
            if (document.forms[0].LinkedDLCnstrumentIDExp.value > '' || document.forms[0].FinanceTypeText.value > '' ) {
                  document.forms[0].FinanceType[2].checked = true;
            }
      }
      
      function pickLinkedCOLInstrumentRadioExp() {
          
            if (document.forms[0].LinkedCOLnstrumentIDExp.value > '' || document.forms[0].FinanceTypeText.value > '' ) {
                  document.forms[0].FinanceType[3].checked = true;
            }
      }
      
      function pickLinkedOTHInstrumentRadioExp() {
          
            if (document.forms[0].LinkedDLCnstrumentIDExp.value > '' || document.forms[0].FinanceTypeText.value > '' ) {
                  document.forms[0].FinanceType[4].checked = true;
            }
      }
   <%-- PICK OUR ACCOUNT RADIO IF AN ACCOUNT NUMBER IS SELECTED --%>
      function pickOurAccountRadioExp() {
      
      
      
          
            <%----var index = document.forms[0].LoanProceedsCreditAccountExp.selectedIndex;
      var acct  = document.forms[0].LoanProceedsCreditAccountExp.options[index].value;

            if (acct > '') {
                  document.forms[0].LoanProceedsCreditTypeExp[0].checked = true;
            }-----%>
      }

      <%-- PICK THE FOLLOWING BENEFICIARY RADIO IF A BENEFICIARY IS SELECTED --%>
      function pickTheFollowingBeneficiaryRadioExp() {
          
            if (document.TransactionLRQ.BenNameExp.value > ''                 ||
                  document.TransactionLRQ.BenAddressLine1Exp.value > '' ||
                  document.TransactionLRQ.BenAddressLine2Exp.value > '' ||
                  document.TransactionLRQ.BenStateProvinceExp.value > ''      ||
                  document.TransactionLRQ.BenCityExp.value > ''               ||
                  document.TransactionLRQ.BenPostalCodeExp.value > ''   ||
                  <%-- rkrishna IR-PAUH101945718 11/01/2007 Commented Below line --%>
                  <%-- document.TransactionLRQ.BenPhoneNumberExp.value > ''      || --%>
                  <%-- rkrishna IR-PAUH101945718 11/01/2007 End --%>
                  document.TransactionLRQ.BenOTLCustomerIdExp.value > ''      ||
                  document.TransactionLRQ.BenCountryExp.value > '') {
                        document.forms[0].LoanProceedsCreditTypeExp[1].checked = true;
            }
      }

      <%-- PICK OTHER (ENTER ...) RADIO IF APPLY LOAN PROCEEDS TO OTHER TEXT BOX IS FILLED OUT --%>
      function pickApplyProceedsToOtherRadioExp() {
          
            if (document.forms[0].LoanProceedsCreditOtherTextExp.value > '') {
                  document.forms[0].LoanProceedsCreditTypeExp[2].checked = true;
            }
      }
      
      function checkBeneficiaryNameExp(originalName) {
      
            if (document.forms[0].BenNameExp.value != originalName)
            {
                  document.forms[0].BenOTLCustomerIdExp.value = "";
            }
      }
      
      function checkBeneficiaryBankNameExp(originalName) {
      
            if (document.forms[0].BbkNameExp.value != originalName)
            {
                  document.forms[0].BbkOTLCustomerIdExp.value = "";
            }
      }

    function pickPreshipmentFinancingRadio() {           
        if (dijit.byId("FinancedPOsDetailsText").getValue() != null ) { 
             dijit.byId("PRESHIPMENT_FINANCING").set('checked',true);
          }
    }
            
    function pickOtherRadioExp() {
        if (dijit.byId("FinanceTypeTextExp").getValue() != null ) { 
             dijit.byId("LoanRequestOTHER").set('checked',true);
          }
    }
    
     function pickDebitOtherExpRadio() {
        if (dijit.byId("LoanMaturityDebitOtherTextExp").getValue() != null ) { 
             dijit.byId("OAD").set('checked',true);
          }
    }
      function pickRelatedInstExp(rdBtn){
    	  dijit.byId(rdBtn).set('checked',true);
      }
    
    function clearLoanMaturityDebitOtherText() {
        document.TransactionLRQ.LoanMaturityDebitOtherTextExp.value = "";
    }
    
    <%-- rkrishna IR-PAUH101945718 11/01/2007 Add Begin --%>
    function clearBeneficiaryExp() {
    
    document.TransactionLRQ.BenNameExp.value = "";
    document.TransactionLRQ.BenAddressLine1Exp.value = "";
    document.TransactionLRQ.BenAddressLine2Exp.value = "";
    document.TransactionLRQ.BenCityExp.value = "";
    document.TransactionLRQ.BenStateProvinceExp.value = "";
    dijit.byId("BenCountryExp").set('value',"");
    document.TransactionLRQ.BenPostalCodeExp.value = "";
    document.TransactionLRQ.BenOTLCustomerIdExp.value = "";
    document.TransactionLRQ.VendorIdExp.value = "";                     
    }
    <%-- rkrishna IR-PAUH101945718 11/01/2007  End --%>
</script>
