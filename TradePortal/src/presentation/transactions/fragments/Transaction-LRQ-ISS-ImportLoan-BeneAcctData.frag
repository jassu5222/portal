<%--
*******************************************************************************
  
  Description:
  A fragment for displaying terms party imp_accounts.
  Fairly generic, but should be copied for specific instrument behavior.

  The following variables are passed:

    termsParty - a TermsPartyWebBean
    isReadOnly
    isTemplate
    widgetFactory

*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<%
  // Logic to handle account list.  If no payee has been searched, there is
  // only an enterable account number field.  Otherwise, we display the set of imp_accounts defined
  // for the party (when the party is selected, we get the set of imp_accounts for that party 
  // and save it on the TermsParty record -- if the party's imp_accounts change later, we do not update
  // the set on the TermsParty record.) as well as an enterable account number field.  The account 
  // number selected by the user will be one of the payee's predefined imp_accounts or the 
  // entered account number.
  
  		  String imp_benAcctList = StringFunction.xssHtmlToChars(termsParty.getAttribute("acct_choices"));
		  DocumentHandler imp_AcctData = new DocumentHandler(imp_benAcctList, true);
		  DocumentHandler imp_acct;
		  boolean imp_FoundAcctMatch = false; // indicates if ANY predefined imp_acct num matches ben. imp_acct
		  //     number from business object
		  boolean imp_foundMatch = false; // indicates if a given imp_acct num matches ben. imp_acct
		  //     number from business object
		  int impNumAccts, ii;
		  Vector imp_accounts = imp_AcctData.getFragments("/DocRoot/ResultSetRecord");
		  impNumAccts = imp_accounts.size();
		  
		  String imp_AcctNum = null;
		  String imp_AcctCcy = null;

		  Debug.debug("Account Choices for Ben Party: " + imp_benAcctList);
		  Debug.debug("Number of account in list is " + impNumAccts);

		  // Set the imp_showBenAccts flag that determines if a list of imp_accounts with radio buttons
		  // appears.  They appear if we have a set of imp_accounts (i.e., it's been preloaded into
		  // a imp_benAcctList variable).  However, in readonly mode, we do not display the list.

		  // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
		  //if (impNumAccts > 0) imp_showBenAccts = true;
		  boolean imp_showBenAccts = false;
		  if (impNumAccts > 0)
				imp_showBenAccts = true;
		  // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
		  if (isReadOnly)
				imp_showBenAccts = false;

		  Debug.debug("imp_showBenAccts flag is " + imp_showBenAccts);

		  // Store the set of imp_accounts for the beneficiary in a hidden field.  This gets written to the
		  // TermsParty record so we always have the set of imp_accounts at the time the party was selected.
		  if(null != imp_benAcctList && !"".equals(imp_benAcctList)){
			%>
			<input type=hidden value='<%=imp_benAcctList%>' name=BenAcctChoices>
			<%
		  }
		  // Peform selective logic throughout that determines how this section is displayed -- as
		  // a set of radio buttons and imp_accounts, or just a single enterable account.
		  if (imp_showBenAccts) {
	%>
	<%--=widgetFactory.createTextField("","LoanRequest.BeneficiaryAccountNumber", "", "30", true, false, false, "width", "", "")--%>

	<%
		  }
	%>
	<%
		  if (imp_showBenAccts) {
				// This loop prints the set of imp_accounts previously determined when the party was
				// selected.
				for (ii = 0; ii < impNumAccts; ii++) {
					  imp_acct = (DocumentHandler) imp_accounts.elementAt(ii);
	%>
	<%
		  // Display a radio button using the "account number/currency" from the
					  // account list (for whichever row we're on).  Select the correct
					  // radio button if we match on the current value of "acct_num" on the
					  // beneficiary party.
					  out.println("<td width=24 nowrap>");

					  // (Don't know why, but the first "get" from the doc must be without the /,
					  // succeeding ones must be with a /.  This is different from similar logic
					  // elsewhere.  Believed to be a symptom of flattening a DocHandler to a string
					  // and then creating another DocHandler from the string.)

					  imp_AcctNum = imp_acct.getAttribute("ACCOUNT_NUMBER"); // no "slash"
					  imp_AcctCcy = imp_acct.getAttribute("/CURRENCY"); // get with "slash"

					  imp_foundMatch = imp_AcctNum.equals(termsParty.getAttribute("acct_num"));

					  if (!imp_FoundAcctMatch) {
							imp_FoundAcctMatch = imp_FoundAcctMatch | imp_foundMatch;
					  }
	
		String radioLabel = "";
		  if (imp_AcctCcy.equals("")) {
			  radioLabel = imp_AcctNum;
	
		  } else {
			  radioLabel = imp_AcctNum + " (" + imp_AcctCcy + ")";
	
		  }
	%>
	<%=widgetFactory.createRadioButtonField("SelectedAccountImp", 
			"BenAccountNum"+imp_AcctNum, radioLabel, 
			imp_AcctNum+ TradePortalConstants.DELIMITER_CHAR+ imp_AcctCcy, 
			imp_foundMatch, isReadOnly,	"onClick='pickTheFollowingBeneficiaryRadioImp();'","")%>
	<div style=\"clear:both;\"></div>
	<%
		  // End of for loop
				}
		  } // end if imp_showBenAccts
	%>
	<%
		  // This IF sets up imp_AcctNum and imp_AcctCcy fields with values if necessary and uses them
		  // for displaying the enterable account field.

		  if (imp_showBenAccts) {
				// Display radio button for selection of the enterable account.  If we haven't found
				// an account match yet, then we might match on the enterable value.  If they match,
				// turn on the radio button.
				if (imp_FoundAcctMatch) {
					  imp_AcctNum = "";
					  imp_AcctCcy = "";
					  imp_foundMatch = false;
				} else {
					  imp_AcctNum = termsParty.getAttribute("acct_num");
					  imp_AcctCcy = termsParty.getAttribute("acct_currency");
					  if (imp_AcctNum == null)
							imp_AcctNum = "";
							imp_foundMatch = imp_AcctNum.equals(termsParty.getAttribute("acct_num"));
				}
	%>
	<%=widgetFactory.createRadioButtonField("SelectedAccountImp","BenAccountNum","",
			TradePortalConstants.USER_ENTERED_ACCT, imp_foundMatch, isReadOnly,"onClick='pickTheFollowingBeneficiaryRadioImp();'", "")%>
	<%
		  } else {
				// We're not displaying the account list, so populate the account field with the
				// account from the beneficiary party record.
				imp_AcctNum = termsParty.getAttribute("acct_num");
				imp_AcctCcy = termsParty.getAttribute("acct_currency");
				if (imp_AcctNum == null)
					  imp_AcctNum = "";
		  }
	%>
	<%-- KMEhta IR T36000022557 on 15/1/2014 changed required indicator to !isTemplate --%>
	<%=widgetFactory.createTextField("EnteredAccountImp","",imp_AcctNum,"30",isReadOnly,!isTemplate,false,"","", "none")%>
