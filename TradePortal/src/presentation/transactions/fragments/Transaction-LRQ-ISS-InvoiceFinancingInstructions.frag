<%--
*
*     Copyright   2001-2007                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Loan Request Page - Receivables Financing Instructions section

  Description:
    Contains HTML to create the Receivables Financing Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-InvoiceFinancingInstructions.frag" %>
*******************************************************************************
--%>

<%
      // Do some upfront coding for variables in here...
%>
<div id="InvoiceFinancingInstructions" style="display: block;">
      <div id="loanProceedSection" >
            <%=widgetFactory.createWideSubsectionHeader("LoanRequest.LoanProceeds")%>
            <div>
                  <%
                        options = Dropdown.createSortedCurrencyCodeOptions(isReceivables ? loanProceedsPmtCurrency: "", loginLocale);
                  %>
                  <%=widgetFactory.createSelectField("LoanProceedsPmtCurrRec",
                              "LoanRequest.Currency", defaultCurrencyVal, options, isReadOnly,
                              !isTemplate, false, "style=\"width: 50px;\"", "", "inline")%>
                  <%=widgetFactory.createAmountField("LoanProceedsPmtAmountRec",
                              "LoanRequest.LoanProceedsAmount", isReceivables
                                          ? displayLoanProceedsPmtAmount
                                          : "", loanProceedsPmtCurrency, isReadOnly, false, false,
                              "style=\"width: 126px;\"", "", "inline")%>
            </div>
            <div style="clear: both;"></div>
      </div>
      <div>
            <%=widgetFactory.createWideSubsectionHeader(
                              "LoanRequest.TransactionFinanced", isReadOnly, !isTemplate,
                              false, "")%>
            <div class="formItem">

                  <%=widgetFactory
                              .createRadioButtonField(
                                          "FinanceType",
                                          "ReceivablesFinancing",
                                          "LoanRequest.ReceivablesFinancing",
                                          TradePortalConstants.SELLER_REQUESTED_FINANCING,
                                          financeType
                                                      .equals(TradePortalConstants.SELLER_REQUESTED_FINANCING),
                                          ib_rejectedByBankReadOnly, "", "")%>
                  <br>
                  <div id="BuyerFinance" style="display: block;">
                  <%=widgetFactory.createRadioButtonField(
                              "FinanceType",
                              "BuyerRequestedFinancing",
                              "LoanRequest.BuyerRequestedFinancing",
                              TradePortalConstants.BUYER_REQUESTED_FINANCING,
                              financeType
                                          .equals(TradePortalConstants.BUYER_REQUESTED_FINANCING),
                              ib_rejectedByBankReadOnly, "", "")%><br>
                  <%
                        if (financingBackedByBuyer
                                    .equals(TradePortalConstants.INDICATOR_YES)) {
                              defaultCheckBoxValue = true;
                              financingBackedByBuyerVal = "Y";
                        } else {
                              defaultCheckBoxValue = false;
                        }
                  %>
                  <%=widgetFactory.createCheckboxField(
                              "FinancingBackedByBuyer",
                              "LoanRequest.FinancingBackedByBuyer", defaultCheckBoxValue,
                              ib_rejectedByBankReadOnly, false,
                              "onClick='pickBuyerRequestedFinancingRadio();'", "", "")%>
            </div>
        </div>
      </div>
      <div id="SellerDetails" style="display: block;">
            <%=widgetFactory.createWideSubsectionHeader(
                              "LoanRequest.SellerInvolvedText", isReadOnly, false, false,
                              "")%>
            <div class="columnLeft">

                  <%=widgetFactory.createSubsectionHeader("LoanRequest.SellerDetails")%>
                  <input type=hidden name='BenTermsPartyTypeRec'
                        value=<%=TradePortalConstants.BENEFICIARY%>>
				  <%=widgetFactory.createTextField("BenOTLCustomerIdRec","",termsPartyBen.getAttribute("OTL_customer_id"),"35",false,false,false," type=hidden ","", "")%>
				  		<input
        type=hidden name="VendorIdRec"
        value="<%=termsPartyBen.getAttribute("vendor_id")%>">
				  
                  <div>
                        <%=widgetFactory.createTextField(
                              "BenNameRec",
                              "LoanRequest.SellerName",
                              isReceivables ? (termsPartyBen.getAttribute("name")) : "",
                              "35",
                              isReadOnly,
                              false,
                              false,
                              "onBlur='checkBeneficiaryNameRec(\""
                                          + StringFunction.escapeQuotesforJS(termsPartyBen.getAttribute("name"))
                                          + "\");'; class='char30'",
                              "", "inline")%>
                              
                               <%
                         String invoiceBankStr=",BbkNameRec,BbkAddressLine1Rec,BbkAddressLine2Rec,BbkCityRec,BbkStateProvinceRec,BbkPostalCodeRec,BbkCountryRec,BbkOTLCustomerIdRec";
                          String ItemIdinvoice="BenNameRec,BenAddressLine1Rec,BenAddressLine2Rec,BenCityRec,BenStateProvinceRec,BenPostalCodeRec,BenCountryRec,BenOTLCustomerIdRec"+invoiceBankStr;
                          String SectionInvoice="sellerdetails";
                          %>

                        <%
                              if (!(isReadOnly)) {
                        %>
                        <div class="inline">
                          <br/> <%--account for label--%>
                         
                          <%=widgetFactory.createPartySearchButton(
                                    ItemIdinvoice,SectionInvoice, false,
                                    TradePortalConstants.BENEFICIARY, false)%>
                          <%-- /* extra Tags : clearBeneficiaryRec();*/ --%>
                          <%=widgetFactory
                                    .createPartyClearButton("ClearSellerButton",
                                                "clearBeneficiaryRec()", false, "")
                                                %>
                        </div>
                        <%
                              }

                              // Always send the ben terms party oid
                              secureParms.put("ben_terms_party_oid_rec",
                                          termsPartyBen.getAttribute("terms_party_oid"));
                        %>
                        <div style="clear: both;"></div>
                  </div>
                  <%=widgetFactory.createTextField(
                              "BenAddressLine1Rec",
                              "LoanRequest.SellerAddressLine1",
                              isReceivables ? (termsPartyBen
                                          .getAttribute("address_line_1")) : "",
                              "35",
                              isReadOnly,
                              false,
                              false,
                              "",
                              "", "")%>

                  <%=widgetFactory.createTextField(
                              "BenAddressLine2Rec",
                              "LoanRequest.SellerAddressLine2",
                              isReceivables ? (termsPartyBen
                                          .getAttribute("address_line_2")) : "",
                              "35",
                              isReadOnly,
                              false,
                              false,
                              "",
                              "", "")%>

                  <%=widgetFactory.createTextField(
                              "BenCityRec",
                              "LoanRequest.SellerCity",
                              isReceivables
                                          ? (termsPartyBen.getAttribute("address_city"))
                                          : "",
                              "23",
                              isReadOnly,
                              false,
                              false,
                              "class='char35'",
                              "", "")%>
                  <div>
                        <%=widgetFactory.createTextField(
                              "BenStateProvinceRec",
                              "LoanRequest.SellerProvinceState",
                              isReceivables ? (termsPartyBen
                                          .getAttribute("address_state_province")) : "",
                              "8",
                              isReadOnly,
                              false,
                              false,
                              "class='char10'",
                              "", "inline")%>


                        <%=widgetFactory.createTextField(
                              "BenPostalCodeRec",
                              "LoanRequest.SellerPostalCode",
                              isReceivables ? (termsPartyBen
                                          .getAttribute("address_postal_code")) : "",
                              "15",
                              isReadOnly,
                              false,
                              false,
                              "class='char10'",
                              "", "inline")%>
                        <div style="clear: both;"></div>
                  </div>
                  <%
                        options = Dropdown.createSortedRefDataOptions(
                                    TradePortalConstants.COUNTRY, isReceivables
                                                ? (termsPartyBen.getAttribute("address_country"))
                                                : "", loginLocale);
                  %>
                  <%=widgetFactory
                              .createSelectField(
                                          "BenCountryRec",
                                          "LoanRequest.SellerCountry",
                                          " ",
                                          options,
                                          isReadOnly,
                                          false,
                                          false,
                                          "class='char35'",
                                          "", "")%>
<%-- Account number --%>
                  
            </div>
            <%-- Left Column Ends Here --%>




            <div class="columnRight">
            <%
            String ItemIdInvoice2="BbkNameRec,BbkAddressLine1Rec,BbkAddressLine2Rec,BbkCityRec,BbkStateProvinceRec,BbkPostalCodeRec,BbkCountryRec,BbkOTLCustomerIdRec";
            
            %>
                  <%=widgetFactory.createSubsectionHeader(
                              "LoanRequest.SellerBank", isReadOnly, false, false, "")%>
                  <input type=hidden name='BbkTermsPartyTypeRec'
                        value=<%=TradePortalConstants.BENEFICIARY_BANK%>>
                        <%=widgetFactory.createTextField("BbkOTLCustomerIdRec","",termsPartyBbk.getAttribute("OTL_customer_id"),"35",false,false,false," type=hidden ","", "")%>

                  <div>
                        <%=widgetFactory.createTextField(
                              "BbkNameRec",
                              "LoanRequest.SellerBankName",
                              isReceivables ? termsPartyBbk.getAttribute("name") : "",
                              "35",
                              isReadOnly || isFromExpress,
                              false,
                              false,
                              "onBlur='checkBeneficiaryBankNameRec(\""
                                          + termsPartyBbk.getAttribute("name")
                                          + "\");'; class='char30'",
                              "", "inline")%>
                        <%
                              if (!(isReadOnly)) {
                        %>
                        <div class="inline">
                          <br/> <%--account for label--%>
                          <%
                          String SectionInvoice_2="releasetopartySection";
                          %>
                          <%=widgetFactory.createPartySearchButton(ItemIdInvoice2,SectionInvoice_2, false,
                                    TradePortalConstants.BENEFICIARY_BANK, false)%>
                        </div>
                        <%
                              }

                              // Always send the bbk terms party oid
                              secureParms.put("bbk_terms_party_oid_rec",
                                          termsPartyBbk.getAttribute("terms_party_oid"));
                        %>

                        <div style="clear: both;"></div>
                  </div>
                  <%=widgetFactory.createTextField("BbkAddressLine1Rec",
                              "LoanRequest.SellerBankAddressLine1", isReceivables
                                          ? (termsPartyBbk.getAttribute("address_line_1"))
                                          : "", "35", isReadOnly, false, false,
                              "", "", "")%>

                  <%=widgetFactory.createTextField(
                              "BbkAddressLine2Rec",
                              "LoanRequest.SellerBankAddressLine2",
                              isReceivables ? (termsPartyBbk
                                          .getAttribute("address_line_2")) : "",
                              "35",
                              isReadOnly,
                              false,
                              false,
                              "",
                              "", "")%>
					<%-- KMehta - 17 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - Begin --%>
                  <%=widgetFactory.createTextField(
                              "BbkCityRec",
                              "LoanRequest.SellerBankCity",
                              isReceivables
                                          ? (termsPartyBbk.getAttribute("address_city"))
                                          : "",
                              "23",
                              isReadOnly,
                              false,
                              false,
                              "",
                              "", "")%>
                    <%-- KMehta - 17 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - End --%>
                  <div>
                        <%=widgetFactory.createTextField(
                              "BbkStateProvinceRec",
                              "LoanRequest.SellerBankProvinceState",
                              isReceivables ? (termsPartyBbk
                                          .getAttribute("address_state_province")) : "",
                              "8",
                              isReadOnly,
                              false,
                              false,
                              "class='char10'",
                              "", "inline")%>


                        <%=widgetFactory.createTextField(
                              "BbkPostalCodeRec",
                              "LoanRequest.SellerBankPostalCode",
                              isReceivables ? (termsPartyBbk
                                          .getAttribute("address_postal_code")) : "",
                              "15",
                              isReadOnly,
                              false,
                              false,
                              "class='char10'",
                              "", "inline")%>
                        <div style="clear: both;"></div>
                  </div>
                  <%
                        options = Dropdown.createSortedRefDataOptions(
                                    TradePortalConstants.COUNTRY, isReceivables
                                                ? (termsPartyBbk.getAttribute("address_country"))
                                                : "", loginLocale);
                  %>
                  <%=widgetFactory
                              .createSelectField(
                                          "BbkCountryRec",
                                          "LoanRequest.SellerBankCountry",
                                          " ",
                                          options,
                                          isReadOnly,
                                          false,
                                          false,
                                          "class='char35'",
                                          "", "")%>
                                     </div>
                                     <div style="clear:both"></div>
                                     </div>
                                     
                        <div class="columnLeft">              
                                     

                          </div>           
            <%-- Left Column Ends Here --%>




            <div class="columnRight">     
                            
            </div>
            <%-- Right Column Ends Here --%>
 <div style="clear:both;"></div>   
<%@ include file="Transaction-LRQ-ISS-InvoiceFinancingLoan-InvoiceDetails.frag" %>   
                  <%=widgetFactory.createSubsectionHeader(
                              "LoanRequest.LoanMaturity", (isReadOnly || isFromExpress),
                              !isTemplate, false, "")%>
                  <div class="formItem">
                        <%=widgetFactory.createRadioButtonField(
                              "LoanMaturityDebitTypeRec", "DebitApp1",
                              "LoanRequest.DebitTheBorrowersAccount",
                              TradePortalConstants.DEBIT_APP, (isReceivables
                                          ? loanMaturityDebitType
                                          : "").equals(TradePortalConstants.DEBIT_APP),
                              isReadOnly, "", "")%>

                        <%
                              // Using the acct_choices xml from the app party,build the dropdown and 
                              // select the one matching loan_maturity_debit_acct
                              acctNum = StringFunction.xssHtmlToChars(terms
                                          .getAttribute("loan_maturity_debit_acct"));

                              appAcctList = StringFunction.xssHtmlToChars(termsPartyApp
                                          .getAttribute("acct_choices"));

                              //DocumentHandler acctOptions = new DocumentHandler(appAcctList, true);
                              acctOptions = new DocumentHandler(appAcctList, true);
                              options = Dropdown.createSortedAcctOptions(acctOptions,
                                          isReceivables ? acctNum : "", loginLocale);
                        %>
                        <br>
                        <%=widgetFactory.createSelectField(
                              "LoanMaturityDebitAccountRec", "", " ", options, isReadOnly,
                              false, false,
                              "class='char25'", "", "")%>
                        <br>
                        <%=widgetFactory.createRadioButtonField(
                              "LoanMaturityDebitTypeRec", "DebitOther1",
                              "LoanRequest.OtherRadio",
                              TradePortalConstants.DEBIT_OTHER, (isReceivables
                                          ? loanMaturityDebitType
                                          : "").equals(TradePortalConstants.DEBIT_OTHER),
                              isReadOnly, "", "")%>

                        <div style="clear: both;"></div>
                  </div>
                  <div class="formItem">
                  <%=widgetFactory.createTextArea(
                              "LoanMaturityDebitOtherTextRec",
                              "",
                              isReceivables
                                          ? (terms.getAttribute("loan_maturity_debit_other_txt"))
                                          : "", isReadOnly, !isTemplate, false,
                              "style=\"min-height: 60px;\"", "", "")%>
                              <%=widgetFactory.createHoverHelp("LoanMaturityDebitOtherTextRec", "LoanMaturityHoverText")%>
                  </div>


</div>

<script LANGUAGE="JavaScript">
      
<%-- PICK BUYER REQUESTED FINANCING RADIO BUTTON --%>
      function pickBuyerRequestedFinancingRadio() {
            if (document.forms[0].FinancingBackedByBuyer.checked) {
                  document.forms[0].FinanceType[7].checked = true;
            }
      }
<%-- PICK DEBIT OTHER RADIO IF TEXT IS ENTERED INTO THE OTHER (ADVISE ...) TEXT BOX --%>
      function pickDebitOtherRadioRec() {
            if (document.forms[0].LoanMaturityDebitOtherTextRec.value > '') {
                  document.forms[0].LoanMaturityDebitTypeRec[1].checked = true;
            }
      }
<%-- PICK DEBIT THE APPLICANTS ACCOUNT RADIO IF AN ACCOUNT NUMBER IS SELECTED --%>
      function pickDebitApplicantsAccountRadioRec() {
            var index = document.forms[0].LoanMaturityDebitAccountRec.selectedIndex;
            var acct = document.forms[0].LoanMaturityDebitAccountRec.options[index].value;
            if (acct > '') {
                  document.forms[0].LoanMaturityDebitTypeRec[0].checked = true;
            }
      }

      function checkBeneficiaryNameRec(originalName) {

            if (document.forms[0].BenNameRec.value != originalName) {
                  document.forms[0].BenOTLCustomerIdRec.value = "";
            }
      }

      function checkBeneficiaryBankNameRec(originalName) {

            if (document.forms[0].BbkNameRec.value != originalName) {
<%-- rkrishna IR-PAUH101945718 11/01/2007 Begin: 'rec' changed to 'Rec'  Javascript typo--%>
      
<%--document.forms[0].BbkOTLCustomerIdrec.value = "";--%>
      document.forms[0].BbkOTLCustomerIdRec.value = "";
<%-- rkrishna IR-PAUH101945718 11/01/2007 End --%>
      }
      }
<%-- rkrishna IR-PAUH101945718 11/01/2007 Add Begin --%>
      function clearBeneficiaryRec() {

            document.TransactionLRQ.BenNameRec.value = "";
            document.TransactionLRQ.BenAddressLine1Rec.value = "";
            document.TransactionLRQ.BenAddressLine2Rec.value = "";
            document.TransactionLRQ.BenCityRec.value = "";
            document.TransactionLRQ.BenStateProvinceRec.value = "";
            dijit.byId("BenCountryRec").set('value',"");
            document.TransactionLRQ.BenPostalCodeRec.value = "";
            document.TransactionLRQ.VendorIdRec.value = "";
            document.TransactionLRQ.BenOTLCustomerIdRec.value = "";
      }
<%-- rkrishna IR-PAUH101945718 11/01/2007 End --%>
      
</script>
