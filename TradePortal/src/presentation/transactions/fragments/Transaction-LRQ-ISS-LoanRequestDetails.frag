<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Loan Request Page - Details section

  Description:
    Contains HTML to create the Loan Request Details section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-LoanRequestDetails.jsp" %>
*******************************************************************************
--%>

<div>
	<div id="section2.1">
		<div id="sec2.1Left" class="columnLeft">
			<%=widgetFactory.createSubsectionHeader("LoanRequest.LoanDetails", (isReadOnly || isFromExpress),false, false, "")%>
			<%
			 /* KMehta Rel 8400 IR-T36000022478 on 11/11/2013 Start*/
			String sTodayDate = DateTimeUtility.getGMTDateTime();
			
			// DK IR T36000019816 Rel8.3 08/29/2013 - updated logical condition to AND
  			 String loanStrtDate = (TradePortalConstants.INDICATOR_YES.equals(newTransaction) && StringFunction.isBlank(StringFunction.xssHtmlToChars(terms.getAttribute("loan_start_date")))) ? DateTimeUtility.convertDateToDateString(TPDateTimeUtility.getLocalDateTime(sTodayDate,timeZone)) : StringFunction.xssHtmlToChars(terms.getAttribute("loan_start_date"));  			
  			 //RKazi T36000021568 Rel 8.3 10/08/2013 - Start
  			
  			 if (StringFunction.isBlank(loanStrtDate) && TradePortalConstants.TRANS_STATUS_STARTED.equals(transaction.getAttribute("transaction_status"))){
			 	//loanStrtDate = DateTimeUtility.convertDateToDateString(GMTUtility.getGMTDateTime()); ///* commmented by KMehta Rel 8400 IR-T36000022478 on 11/11/2013 */
  				
  			     loanStrtDate = DateTimeUtility.convertDateToDateString(TPDateTimeUtility.getLocalDateTime(sTodayDate,timeZone));
  			 }
  			/* KMehta Rel 8400 IR-T36000022478 on 11/11/2013 End*/
			 //RKazi T36000021568 Rel 8.3 10/08/2013 -End
			 //IR T36000019026 -if template then dont display current date
  			 if (isTemplate) {%>
				<%=widgetFactory.createDateField("LoanStartDate","LoanRequest.LoanStartDate", StringFunction.xssHtmlToChars(terms.getAttribute("loan_start_date")) ,isReadOnly,!isTemplate, false, "class='char10'", dateWidgetOptions, "")%>
			<% } else { %>
				<%=widgetFactory.createDateField("LoanStartDate","LoanRequest.LoanStartDate", loanStrtDate ,isReadOnly, !isTemplate, false, "class='char10'", dateWidgetOptions, "")%>
			<% } %>
			<% options = Dropdown.createSortedCurrencyCodeOptions(currency,loginLocale); %>
			<div>
				<%=widgetFactory.createSelectField("TransactionCurrency","transaction.Currency", defaultCurrencyVal, options, isReadOnly, !isTemplate,false, "style=\"width: 50px;\"", "", "inline")%>
			    <%if(userSession.isCustNotIntgTPS()){%>
			      <%= widgetFactory.createAmountField( "TransactionAmount", "LoanRequest.Amount", displayAmount, currency, isReadOnly, true, false,"style=\"width: 126px;\"", "", "inline") %>
				<%} else {%>
				  <%= widgetFactory.createAmountField( "TransactionAmount", "LoanRequest.AmountIfKnown", displayAmount, currency, isReadOnly, false, false,"style=\"width: 126px;\"", "", "inline") %>
				<% } %>
				<div style="clear: both;"></div>
			</div></div>
			
		<div id="sec1Right" class="columnRight">
			<%=widgetFactory.createSubsectionHeader("LoanRequest.InterestToBePaid",(isReadOnly || isFromExpress), !isTemplate, false, "")%>
			<div class="formItem">
				<%=widgetFactory.createRadioButtonField("InterestToBePaid","InAdvance", "LoanRequest.InAdvance",TradePortalConstants.IN_ADVANCE,interestToBePaid.equals(TradePortalConstants.IN_ADVANCE),isReadOnly || isFromExpress)%>
				<br/>
				<%=widgetFactory.createRadioButtonField("InterestToBePaid","InArrears", "LoanRequest.InArrears",TradePortalConstants.IN_ARREARS,interestToBePaid.equals(TradePortalConstants.IN_ARREARS),isReadOnly || isFromExpress)%>
				<br/>
				<div style="clear: both;"></div>
			</div></div></div><div><div style="clear: both;"></div>
			
			<div class="columnLeft">
			<%=widgetFactory.createSubsectionHeader("LoanRequest.LoanType", (isReadOnly || isFromExpress),!isTemplate, false, "")%>
			<% if(selectedLoanTypes.size()==1){ 
				String fieldName = "";
				String fieldLabel = "";
				String valueIfSelected = "";
				boolean isChecked = false;
				String loanType = "";
				for (Map.Entry<String, String> entry : selectedLoanTypes.entrySet()) {
					loanType = entry.getKey();
					System.out.println("The only one (default) loan type selected is: [" + loanType + "]");
				}
				if ("ImportLoanType".equals(loanType)) {  
					fieldName = "ImportLoanRadio";
					fieldLabel = "LoanRequest.ImportLoan";
					valueIfSelected  = TradePortalConstants.INDICATOR_YES;
					isChecked = true;
				 
				}else if("ExportLoanType".equals(loanType)){
					fieldName = "ExportLoanRadio";
					fieldLabel = "LoanRequest.ExportLoan";
					valueIfSelected  = TradePortalConstants.INDICATOR_NO;
					isChecked = true;
				  
				}else if("InvoiceFinancingType".equals(loanType)){
					fieldName = "InvoiceFinancingRadio";
					fieldLabel = "LoanRequest.InvoiceFinancing";
					valueIfSelected  = TradePortalConstants.INDICATOR_X;
					isChecked = true;
				  
				}else if("TradeLoanType".equals(loanType)){
					fieldName = "TradeLoanRadio";
					fieldLabel = "LoanRequest.TradeLoan";
					valueIfSelected  = TradePortalConstants.INDICATOR_T;
					isChecked = true;
				  
				}	%>
			  
				<div class="formItem">
					<%=widgetFactory.createRadioButtonField("ImportIndicator",fieldName, fieldLabel,valueIfSelected, isChecked ,ib_rejectedByBankReadOnly, "", "")%>
					<%if("TradeLoanType".equals(loanType)){%>
				 		<div class="formItem">	               
				 			<%=widgetFactory.createRadioButtonField("FinanceType","TradeLoanReceivablesRadio", "LoanRequest.TradeLoanReceivables",TradePortalConstants.TRADE_LOAN_REC, isTradeReceivables,ib_rejectedByBankReadOnly, "onClick=\"validateMandatoryChargesField();\"", "")%>
				 			<%=widgetFactory.createRadioButtonField("FinanceType","TradeLoanPaybalesRadio", "LoanRequest.TradeLoanPaybales",TradePortalConstants.TRADE_LOAN_PAY, isTradePayables,ib_rejectedByBankReadOnly, "onClick=\"validateMandatoryChargesField();\"", "")%>	                
				 		</div>
				 	<%	} %>
				</div>
			<% }else{ %>
			<div class="formItem">
			<% if (TradePortalConstants.INDICATOR_YES.equals(orgTradeLoanType)) { %>
				<%=widgetFactory.createRadioButtonField("ImportIndicator","TradeLoanRadio", "LoanRequest.TradeLoan",TradePortalConstants.INDICATOR_T, isTradeLoan,ib_rejectedByBankReadOnly, "onClick=\"showHideDivs('TradeLoanRadio'); return false;\"", "")%>
		                <div class="formItem">
		                <%--Start IR#T36000031463 Vsarkary Rel 9.2- onClick calling a function to validate mandatory field for financeType --%>
		                    <%=widgetFactory.createRadioButtonField("FinanceType","TradeLoanReceivablesRadio", "LoanRequest.TradeLoanReceivables",TradePortalConstants.TRADE_LOAN_REC, isTradeReceivables,ib_rejectedByBankReadOnly, "onClick=\"validateMandatoryChargesField();\"", "")%>
		                    <%=widgetFactory.createRadioButtonField("FinanceType","TradeLoanPaybalesRadio", "LoanRequest.TradeLoanPaybales",TradePortalConstants.TRADE_LOAN_PAY, isTradePayables,ib_rejectedByBankReadOnly, "onClick=\"validateMandatoryChargesField();\"", "")%>
		                    <%-- End IR#T36000031463 Vsarkary Rel 9.2 --%>
		                </div>
		     <% } %>   
		     <% if (TradePortalConstants.INDICATOR_YES.equals(exportLoanType)) { %>        
				<%=widgetFactory.createRadioButtonField("ImportIndicator","ExportLoanRadio", "LoanRequest.ExportLoan",TradePortalConstants.INDICATOR_NO, isExport,ib_rejectedByBankReadOnly, "onClick=\"showHideDivs('ExportLoanRadio');\"", "")%>
				<br>
			<% } %> 
			<% if (TradePortalConstants.INDICATOR_YES.equals(importLoanType)) { %>   
				<%=widgetFactory.createRadioButtonField("ImportIndicator","ImportLoanRadio", "LoanRequest.ImportLoan",TradePortalConstants.INDICATOR_YES, isImport,ib_rejectedByBankReadOnly, "onClick=\"showHideDivs('ImportLoanRadio');\"", "")%>
				<br>
				<% } %>
				<% if (TradePortalConstants.INDICATOR_YES.equals(invoiceFinancingType)) { %>   
				<%=widgetFactory.createRadioButtonField("ImportIndicator","InvoiceFinancingRadio", "LoanRequest.InvoiceFinancing",TradePortalConstants.INDICATOR_X, isReceivables,ib_rejectedByBankReadOnly, "onClick=\"showHideDivs('InvoiceFinancingRadio');\"", "")%>
				<% } %>
				<div style="clear: both;"></div>
			</div>
			<% }
			 if (ib_rejectedByBankReadOnly) { %>
				<input type=hidden value=<%=importIndicator%> name="ImportIndicator">
			<% }%>
		</div>
			<div id="sec2.1Right" class="columnRight">
			<%=widgetFactory.createSubsectionHeader("LoanRequest.LoanTerms", (isReadOnly || isFromExpress),!isTemplate, false, "")%>
			<div>
				<div class="formItem inline">
					<%=widgetFactory.createRadioButtonField("LoanTermsType","TradePortalConstants.LOAN_FIXED_MATURITY","LoanRequest.AtFixedMaturityDate",TradePortalConstants.LOAN_FIXED_MATURITY, loanTermsType.equals(TradePortalConstants.LOAN_FIXED_MATURITY),isReadOnly || isFromExpress)%>
				</div>
				<% if (isReadOnly) {%>
					<%=widgetFactory.createDateField("LoanTermsFixedMaturityDate", "",StringFunction.xssHtmlToChars(terms.getAttribute("loan_terms_fixed_maturity_dt")),isReadOnly, false, false, "class='char10'",dateWidgetOptions, "inline")%>
				<%} else {%>
					<%=widgetFactory.createDateField("LoanTermsFixedMaturityDate","",StringFunction.xssHtmlToChars(terms.getAttribute("loan_terms_fixed_maturity_dt")),isReadOnly,false,false,"class='char10'",dateWidgetOptions, "inline")%>
				<%}%>
				<div style="clear: both;"></div>
			</div>
			<div class="formItem inline">
				<%=widgetFactory.createRadioButtonField("LoanTermsType","TradePortalConstants.LOAN_DAYS_AFTER", "  ",TradePortalConstants.LOAN_DAYS_AFTER,loanTermsType.equals(TradePortalConstants.LOAN_DAYS_AFTER),isReadOnly || isFromExpress)%>
				<%=widgetFactory.createNumberField("DaysFrom", "",terms.getAttribute("loan_terms_number_of_days"), "3",isReadOnly, false, false,"onChange='pickDaysFromLoanStartRadio();' constraints='{min:0,max:999,places:0}'", "", "none")%>
				<%=widgetFactory.createInlineLabel("","LoanRequest.DaysFrom")%>
			</div>
			<div style="clear: both;"></div>
			<div id="interestDiscountSection" style="visibility: hidden;">
				<div id="interestDiscountButtonSection">
				   <button data-dojo-type="dijit.form.Button"  name="CalcDistAndIntrst" id="CalcDistAndIntrst" type="button">
				    <%=resMgr.getText("UploadInvoices.CalcDistAndInrst",TradePortalConstants.TEXT_BUNDLE)%>
				    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							setButtonPressed('<%=TradePortalConstants.BUTTON_CALC_DIST_AND_INTRST%>', '0');
							document.forms[0].submit();
		 			</script>
				  </button>	 
			    </div>
			   <%=widgetFactory.createTextField("CalcCurrency","", terms.getAttribute("amount_currency_code"), "3", true, false, false, "", "", "")%>
			   <%if(TradePortalConstants.IN_ADVANCE.equals(interestToBePaid)){ 
			   		String netFinDisplayAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("net_finance_amount"), currency, loginLocale);%>
			   <%=widgetFactory.createNumberField("NetFinanceAmount", "UploadInvoiceDetail.NetFinanceAmount",netFinDisplayAmount, "18",true, false, false,"", "", "")%>
			   <%}if(TradePortalConstants.IN_ARREARS.equals(interestToBePaid)){ 
			   		String estInterestDisplayAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("estimated_interest_amount"), currency, loginLocale);%>
			   <%=widgetFactory.createNumberField("EstimatedInterestAmount", "UploadInvoiceDetail.EstimatedInterest",estInterestDisplayAmount, "18",true, false, false,"", "", "")%>	
			   <%}%>		  
			 </div>
		   <div style="clear: both;"></div>
		 </div>
		<div style="clear: both;"></div>	
	</div>
	<div id="section2.2">
		<%=widgetFactory.createSectionHeader("","LoanRequest.ShippingGoodsDetails")%>
	<div id="sec2.2Left" class="columnLeft">
		<%=widgetFactory.createLabel("","LoanRequest.ShippingFrom",false,false,false,"","")%>
		<%=widgetFactory.createTextField("ShipmentFrom","LoanRequest.FromPort", terms.getFirstShipment().getAttribute("shipment_from"), "65", isReadOnly,false, false, "class = 'char35'", "", "")%>
		<%=widgetFactory.createTextField("ShipmentFromLoading","LoanRequest.FromLoad", terms.getFirstShipment().getAttribute("shipment_from_loading"), "65",isReadOnly, false, false, "class = 'char35'", "", "")%>
		
		<%=widgetFactory.createTextField("VesselName","LoanRequest.VesselNameCarrier", terms.getFirstShipment().getAttribute("vessel_name_voyage_num"), "40",isReadOnly, false, false, "class = 'char35'", "", "")%>
	</div>
	<div id="sec2.2Right" class="columnRight">
		<%=widgetFactory.createLabel("","LoanRequest.ShippingTo",false,false,false,"","")%>
		<%=widgetFactory.createTextField("ShipmentToDischarge","LoanRequest.ToDischarge", terms.getFirstShipment().getAttribute("shipment_to_discharge"), "65",isReadOnly, false, false, "class = 'char35'", "", "")%>
		<%=widgetFactory.createTextField("ShipmentTo","LoanRequest.ToPort", terms.getFirstShipment().getAttribute("shipment_to"), "65", isReadOnly,false, false, "class = 'char35'", "", "")%>
		<%=widgetFactory.createTextField("AirWaybill","LoanRequest.BillOfLadingAirWaybill",terms.getFirstShipment().getAttribute("air_waybill_num"),"40", isReadOnly, false, false, "class = 'char35'", "", "")%>
	</div>	
	<%  options = ListBox.createOptionList(goodsDocList, "PHRASE_OID","NAME", "", userSession.getSecretKey());
		defaultText = resMgr.getText("transaction.SelectPhrase",TradePortalConstants.TEXT_BUNDLE);%>
	<%=widgetFactory.createSelectField("GoodsDescriptionPhraseItem","",defaultText,options,isReadOnly,false,false,"onChange="+ PhraseUtility.getPhraseChange("/Terms/ShipmentTermsList/goods_description","GoodsDescription", "6500","document.forms[0].GoodsDescription"),"", "")%>
	<%if(!isReadOnly){%>
	<%=widgetFactory.createTextArea("GoodsDescription", "",terms.getFirstShipment().getAttribute("goods_description"),isReadOnly, false, false, "rows='10'", "", "")%>
	<%}else{%>
	<%=widgetFactory.createAutoResizeTextArea("GoodsDescription", "",terms.getFirstShipment().getAttribute("goods_description"),isReadOnly, false, false, "style='width:600px;min-height:140px;'", "", "")%>
	<%}%>
	<%-- Main DIV --%>
	</div>
	<div style="clear: both;"></div>
	</div>
</div>

<script LANGUAGE="JavaScript">

<%-- PICK AT FIXED MATURITY DATE RADIO IF DATE IS SELECTED --%>
	function pickAtFixedMaturityDateRadio() {

		var index = document.forms[0].LoanTermsFixedMaturityDay.selectedIndex;
		var day = document.forms[0].LoanTermsFixedMaturityDay.options[index].value;

		index = document.forms[0].LoanTermsFixedMaturityMonth.selectedIndex;
		var month = document.forms[0].LoanTermsFixedMaturityMonth.options[index].value;

		index = document.forms[0].LoanTermsFixedMaturityYear.selectedIndex;
		var year = document.forms[0].LoanTermsFixedMaturityYear.options[index].value;

		if (day != '-1' || month != '-1' || year != '-1') {
			document.forms[0].LoanTermsType[0].checked = true;
		}

	}
<%-- PICK DAYS FROM LOAN START RADIO IF START DATE IS ENTERED --%>
	function pickDaysFromLoanStartRadio() {

		if (document.forms[0].DaysFrom.value > '') {
			document.forms[0].LoanTermsType[1].checked = true;
		}

	}
<%-- hides Import and Receivables and shows Export fragment on the fly based on ImportIndicator radio button --%>
	function showExport() {
		if (document.getElementById) {
<%-- DOM3 = IE5, NS6 --%>
	 return;

	 document.getElementById('ImportLoanInstructions').style.visibility = 'hidden';
			document.getElementById('InvoiceFinancingInstructions').style.visibility = 'hidden';
			document.getElementById('ExportLoanInstructions').style.visibility = 'visible';
			document.getElementById('ImportLoanInstructions').style.display = 'none';
			document.getElementById('InvoiceFinancingInstructions').style.display = 'none';
			document.getElementById('ExportLoanInstructions').style.display = 'block';

			document.getElementById('ImportLoanLink').style.visibility = 'hidden';
			document.getElementById('InvoiceFinancingLink').style.visibility = 'hidden';
			document.getElementById('ExportLoanLink').style.visibility = 'visible';
			document.getElementById('ImportLoanLink').style.display = 'none';
			document.getElementById('InvoiceFinancingLink').style.display = 'none';
			document.getElementById('ExportLoanLink').style.display = 'block';

			document.getElementById('LoanLink').style.visibility = 'hidden';
			document.getElementById('LoanLink').style.display = 'none';

		} else {
			if (document.layers) {
<%-- Netscape 4 --%>
	document.ImportLoanInstructions.visibility = 'hidden';
				document.InvoiceFinancingInstructions.visibility = 'hidden';
				document.ExportLoanInstructions.visibility = 'visible';
				document.ImportLoanInstructions.display = 'none';
				document.InvoiceFinancingInstructions.display = 'none';
				document.ExportLoanInstructions.display = 'block';

				document.ImportLoanLink.visibility = 'hidden'; //
				document.InvoiceFinancingLink.visibility = 'hidden'; //
				document.ExportLoanLink.visibility = 'visible'; //
				document.ImportLoanLink.display = 'none';
				document.InvoiceFinancingLink.display = 'none';
				document.ExportLoanLink.display = 'block';

				document.LoanLink.visibility = 'hidden'; //
				document.LoanLink.display = 'none';

			} else {
<%-- IE 4 --%>
	document.all.ImportLoanInstructions.style.visibility = 'hidden';
				document.all.InvoiceFinancingInstructions.style.visibility = 'hidden';
				document.all.ExportLoanInstructions.style.visibility = 'visible';
				document.all.ImportLoanInstructions.style.display = 'none';
				document.all.InvoiceFinancingInstructions.style.display = 'none';
				document.all.ExportLoanInstructions.style.display = 'block';

				document.all.ImportLoanLink.style.visibility = 'hidden';
				document.all.InvoiceFinancingLink.style.visibility = 'hidden';
				document.all.ExportLoanLink.style.visibility = 'visible';
				document.all.ImportLoanLink.style.display = 'none';
				document.all.InvoiceFinancingLink.style.display = 'none';
				document.all.ExportLoanLink.style.display = 'block';

				document.all.LoanLink.style.visibility = 'hidden';
				document.all.LoanLink.style.display = 'none';

			}
		}
	}
<%-- hides Export and Receivables and shows Import fragment on the fly based on ImportIndicator radio button --%>
	function showImport() {
		if (document.getElementById) {
<%-- DOM3 = IE5, NS6 --%>
	document.getElementById('ExportLoanInstructions').style.visibility = 'hidden';
			document.getElementById('InvoiceFinancingInstructions').style.visibility = 'hidden';
			document.getElementById('ImportLoanInstructions').style.visibility = 'visible';
			document.getElementById('ExportLoanInstructions').style.display = 'none';
			document.getElementById('InvoiceFinancingInstructions').style.display = 'none';
			document.getElementById('ImportLoanInstructions').style.display = 'block';

			document.getElementById('ExportLoanLink').style.visibility = 'hidden';
			document.getElementById('InvoiceFinancingLink').style.visibility = 'hidden';
			document.getElementById('ImportLoanLink').style.visibility = 'visible';
			document.getElementById('ExportLoanLink').style.display = 'none';
			document.getElementById('InvoiceFinancingLink').style.display = 'none';
			document.getElementById('ImportLoanLink').style.display = 'block';

			document.getElementById('LoanLink').style.visibility = 'hidden';
			document.getElementById('LoanLink').style.display = 'none';

		} else {
			if (document.layers) {
<%-- Netscape 4 --%>
	document.ExportLoanInstructions.visibility = 'hidden';
				document.InvoiceFinancingInstructions.visibility = 'hidden';
				document.ImportLoanInstructions.visibility = 'visible';
				document.ExportLoanInstructions.display = 'none';
				document.InvoiceFinancingInstructions.display = 'none';
				document.ImportLoanInstructions.display = 'block';

				document.ExportLoanLink.visibility = 'hidden';
				document.InvoiceFinancingLink.visibility = 'hidden';
				document.ImportLoanLink.visibility = 'visible';
				document.ExportLoanLink.display = 'none';
				document.InvoiceFinancingLink.display = 'none';
				document.ImportLoanLink.display = 'block';

				document.LoanLink.visibility = 'hidden'; //
				document.LoanLink.display = 'none';

			} else {
<%-- IE 4 --%>
	document.all.ExportLoanInstructions.style.visibility = 'hidden';
				document.all.InvoiceFinancingInstructions.style.visibility = 'hidden';
				document.all.ImportLoanInstructions.style.visibility = 'visible';
				document.all.ExportLoanInstructions.style.display = 'none';
				document.all.InvoiceFinancingInstructions.style.display = 'none';
				document.all.ImportLoanInstructions.style.display = 'block';

				document.all.ExportLoanLink.style.visibility = 'hidden';
				document.all.InvoiceFinancingLink.style.visibility = 'hidden';
				document.all.ImportLoanLink.style.visibility = 'visible';
				document.all.ExportLoanLink.style.display = 'none';
				document.all.InvoiceFinancingLink.style.display = 'none';
				document.all.ImportLoanLink.style.display = 'block';

				document.all.LoanLink.style.visibility = 'hidden';
				document.all.LoanLink.style.display = 'none';

			}
		}
	}
<%-- hides Export and Import and shows Receivables fragment on the fly based on ImportIndicator radio button --%>
	function showReceivables() {
		if (document.getElementById) {
<%-- DOM3 = IE5, NS6 --%>
	document.getElementById('ExportLoanInstructions').style.visibility = 'hidden';
			document.getElementById('InvoiceFinancingInstructions').style.visibility = 'visible';
			document.getElementById('ImportLoanInstructions').style.visibility = 'hidden';
			document.getElementById('ExportLoanInstructions').style.display = 'none';
			document.getElementById('InvoiceFinancingInstructions').style.display = 'block';
			document.getElementById('ImportLoanInstructions').style.display = 'none';

			document.getElementById('ExportLoanLink').style.visibility = 'hidden';
			document.getElementById('InvoiceFinancingLink').style.visibility = 'visible';
			document.getElementById('ImportLoanLink').style.visibility = 'hidden';
			document.getElementById('ExportLoanLink').style.display = 'none';
			document.getElementById('InvoiceFinancingLink').style.display = 'block';
			document.getElementById('ImportLoanLink').style.display = 'none';

			document.getElementById('LoanLink').style.visibility = 'hidden';
			document.getElementById('LoanLink').style.display = 'none';

		} else {
			if (document.layers) {
<%-- Netscape 4 --%>
	document.ExportLoanInstructions.visibility = 'hidden';
				document.InvoiceFinancingInstructions.visibility = 'visible';
				document.ImportLoanInstructions.visibility = 'hidden';
				document.ExportLoanInstructions.display = 'none';
				document.InvoiceFinancingInstructions.display = 'block';
				document.ImportLoanInstructions.display = 'none';

				document.ExportLoanLink.visibility = 'hidden';
				document.InvoiceFinancingLink.visibility = 'visible';
				document.ImportLoanLink.visibility = 'hidden';
				document.ExportLoanLink.display = 'none';
				document.InvoiceFinancingLink.display = 'block';
				document.ImportLoanLink.display = 'none';

				document.LoanLink.visibility = 'hidden'; //
				document.LoanLink.display = 'none';

			} else {
<%-- IE 4 --%>
	document.all.ExportLoanInstructions.style.visibility = 'hidden';
				document.all.InvoiceFinancingInstructions.style.visibility = 'visible';
				document.all.ImportLoanInstructions.style.visibility = 'hidden';
				document.all.ExportLoanInstructions.style.display = 'none';
				document.all.InvoiceFinancingInstructions.style.display = 'block';
				document.all.ImportLoanInstructions.style.display = 'none';

				document.all.ExportLoanLink.style.visibility = 'hidden';
				document.all.InvoiceFinancingLink.style.visibility = 'visible';
				document.all.ImportLoanLink.style.visibility = 'hidden';
				document.all.ExportLoanLink.style.display = 'none';
				document.all.InvoiceFinancingLink.style.display = 'block';
				document.all.ImportLoanLink.style.display = 'none';

				document.all.LoanLink.style.visibility = 'hidden';
				document.all.LoanLink.style.display = 'none';

			}
		}
	}

	function hideExportImportReceivables() {
		if (document.getElementById) {
<%-- DOM3 = IE5, NS6 --%>
	console.log("hideExportImportReceivables");
	document.getElementById('ExportLoanInstructions').style.visibility = 'visible';
			document.getElementById('InvoiceFinancingInstructions').style.visibility = 'visible';
			document.getElementById('ImportLoanInstructions').style.visibility = 'visible';
			document.getElementById('ExportLoanInstructions').style.display = 'block';
			document.getElementById('InvoiceFinancingInstructions').style.display = 'block';
			document.getElementById('ImportLoanInstructions').style.display = 'block';

			document.getElementById('ExportLoanLink').style.visibility = 'hidden';
			document.getElementById('InvoiceFinancingLink').style.visibility = 'hidden';
			document.getElementById('ImportLoanLink').style.visibility = 'hidden';
			document.getElementById('ExportLoanLink').style.display = 'none';
			document.getElementById('InvoiceFinancingLink').style.display = 'none';
			document.getElementById('ImportLoanLink').style.display = 'none';

			document.getElementById('LoanLink').style.visibility = 'visible';
			document.getElementById('LoanLink').style.display = 'block';

		} else {
			if (document.layers) {
<%-- Netscape 4 --%>
	document.ExportLoanInstructions.visibility = 'visible';
				document.InvoiceFinancingInstructions.visibility = 'visible';
				document.ImportLoanInstructions.visibility = 'visible';
				document.ExportLoanInstructions.display = 'block';
				document.InvoiceFinancingInstructions.display = 'block';
				document.ImportLoanInstructions.display = 'block';

				document.ExportLoanLink.visibility = 'hidden';
				document.InvoiceFinancingLink.visibility = 'hidden';
				document.ImportLoanLink.visibility = 'hidden';
				document.ExportLoanLink.display = 'none';
				document.InvoiceFinancingLink.display = 'none';
				document.ImportLoanLink.display = 'none';

				document.LoanLink.visibility = 'visible';
				document.LoanLink.display = 'block';

			} else {
<%-- IE 4 --%>
	document.all.ExportLoanInstructions.style.visibility = 'hidden';
				document.all.InvoiceFinancingInstructions.style.visibility = 'hidden';
				document.all.ImportLoanInstructions.style.visibility = 'hidden';
				document.all.ExportLoanInstructions.style.display = 'none';
				document.all.InvoiceFinancingInstructions.style.display = 'none';
				document.all.ImportLoanInstructions.style.display = 'block';

				document.all.ExportLoanLink.style.visibility = 'hidden';
				document.all.InvoiceFinancingLink.style.visibility = 'hidden';
				document.all.ImportLoanLink.style.visibility = 'hidden';
				document.all.ExportLoanLink.style.display = 'none';
				document.all.InvoiceFinancingLink.style.display = 'none';
				document.all.ImportLoanLink.style.display = 'none';

				document.all.LoanLink.style.visibility = 'visible';
				document.all.LoanLink.style.display = 'block';

			}
		}
	}
</script>
