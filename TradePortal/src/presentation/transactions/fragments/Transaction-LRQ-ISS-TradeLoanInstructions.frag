<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Loan Request Page - Import Payment Instructions section

  Description:
    Contains HTML to create the Loan Request Import Payment Instructions section.  
    It shows the payment instruction when the loan type is Export (import_indicator = "N").

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-TradeLoanInstructions.frag" %>
*******************************************************************************
--%>
<script type="text/javascript" src="/portal/js/datagrid.js"></script>
<%
  String			 benificiaryName 	   = "";					
  String			 benificiaryBankName   = "";					
  String	         paymentStatusSearch   = "";	
  String benNameTrd = "";
  String benAcctNumTrd = "";
  String benAdd1Trd ="";
  String benAdd2Trd ="";
  String benAdd3Trd ="";
  String benCityTrd ="";
  String benCountryTrd ="";
  String paymentMethodTrd ="";
  String paymentChargesTrd ="";
  String benOtlCusotmerOIDTrd = "";
  String bbkNameTrd	="";
  String bbkBranchCodeTrd = "";
  String bbkSortCodeTrd = "";
  String bbkAddr1Trd = "";
  String bbkAddr2Trd = "";
  String bbkCityTrd = "";
  String bbkStatPrvncTrd = "";
  String bbkCountryTrd	="";
  String bbkOTLCustOIDTrd ="";		
  String benReferenceTrd ="";
  String centralBankRep1 = "";
  String centralBankRep2 = "";
  String centralBankRep3 = "";
  String selectedRptgCodes1 = "";//CR1001 Rpasupulati adding reporting codes
  String selectedRptgCodes2 = "";//CR1001 Rpasupulati adding reporting codes
  String selectedRptgCodeDesc1 = "";//CR1001 Rpasupulati adding reporting codes
  String selectedRptgCodeDesc2 = "";//CR1001 Rpasupulati adding reporting codes
  //jgadela  R90 IR T36000026319 - SQL FIX
  Object[] sqlParam = null;
  				
  StringBuffer 	 newSearchCriteria     = new StringBuffer(); 
    rowCount = 8;
    String currentInvPaymentInstOid=""; 
    	if (isMultiBenInvoicesAttached)	{
			StringBuffer multiBenInvQuery = new StringBuffer();
			multiBenInvQuery.append("select isd.upload_invoice_oid from invoices_summary_data isd");
			//build where clause differently depending on whether a DP is already selected
			if (StringFunction.isBlank(currentInvPaymentInstOid)) {
				//get all DPs associated with current Transaction pick the first one in the list
				multiBenInvQuery.append(" where rownum = 1 and isd.a_transaction_oid = ?");
				//jgadela  R90 IR T36000026319 - SQL FIX
			    sqlParam = new Object[1];
				sqlParam[0] =  transaction.getAttribute("transaction_oid");
			}
			else {
				//use the OID of the selected DP
				multiBenInvQuery.append(" where isd.upload_invoice_oid = ?");
				//jgadela  R90 IR T36000026319 - SQL FIX
			    sqlParam = new Object[1];
				sqlParam[0] =  currentInvPaymentInstOid;
			}
			multiBenInvQuery.append(" order by upper(ben_acct_num)");
			
			DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(multiBenInvQuery.toString(), false, sqlParam);
	    	if (resultsDoc != null) {
	    		currentInvPaymentInstOid = resultsDoc.getAttribute("/ResultSetRecord(0)/UPLOAD_INVOICE_OID");
	    		

	    	}
  		
    	}else if (isTradeLoan){
	  
	   
	   if ( numberOfDomPmts > 0) {
			//BSL IR# PMUL031604858 Rel7.0 03/15/11 Begin
			//DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet("select inv_pay_inst_oid from invoice_payment_instructions d where rownum = 1 and d.p_transaction_oid =" + transaction.getAttribute("transaction_oid") + " order by upper(AccountNumber)",false);
			StringBuffer domPmtQuery = new StringBuffer();
			domPmtQuery.append("select ipi.inv_pay_inst_oid from invoice_payment_instructions ipi");
			//build where clause differently depending on whether a DP is already selected
			if (InstrumentServices.isBlank(currentInvPaymentInstOid)) {
				//get all DPs associated with current Transaction pick the first one in the list
				domPmtQuery.append(" where rownum = 1 and ipi.p_transaction_oid = ?");
				//jgadela  R90 IR T36000026319 - SQL FIX
			    sqlParam = new Object[1];
			    sqlParam[0] = transaction.getAttribute("transaction_oid");
			}
			else {
				//use the OID of the selected DP
				domPmtQuery.append(" where ipi.inv_pay_inst_oid = ?");
				//jgadela  R90 IR T36000026319 - SQL FIX
			    sqlParam = new Object[1];
			    sqlParam[0] = currentInvPaymentInstOid;
			}
			domPmtQuery.append(" order by upper(ben_acct_num)");
			DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(domPmtQuery.toString(), false, sqlParam);
			//BSL IR# PMUL031604858 Rel7.0 03/15/11 End
	    	if (resultsDoc != null) {
	    		currentInvPaymentInstOid = resultsDoc.getAttribute("/ResultSetRecord(0)/INV_PAY_INST_OID");
	    		

	    	}
	   }else if (numberOfDomPmts == 1 && !getDataFromDoc){
	   	StringBuffer domPmtQuery = new StringBuffer();
			domPmtQuery.append("select ipi.inv_pay_inst_oid from invoice_payment_instructions ipi ");
			//build where clause differently depending on whether a DP is already selected
			if (InstrumentServices.isBlank(currentInvPaymentInstOid)) {
				//get all DPs associated with current Transaction pick the first one in the list
				domPmtQuery.append(" where rownum = 1 and ipi.p_transaction_oid = ?");
				//jgadela  R90 IR T36000026319 - SQL FIX
				sqlParam = new Object[1];
			    sqlParam[0] = transaction.getAttribute("transaction_oid");
			}
			else {
				//use the OID of the selected DP
				domPmtQuery.append(" where ipi.inv_pay_inst_oid =?");
				//jgadela  R90 IR T36000026319 - SQL FIX
				sqlParam = new Object[1];
			    sqlParam[0] = currentInvPaymentInstOid;
			}
			domPmtQuery.append(" order by upper(ben_acct_num)");
			DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(domPmtQuery.toString(), false, sqlParam);
			//BSL IR# PMUL031604858 Rel7.0 03/15/11 End
	    	if (resultsDoc != null) {
	    		currentInvPaymentInstOid = resultsDoc.getAttribute("/ResultSetRecord(0)/INV_PAY_INST_OID");
	    		

	    	}
	   }
	  if (StringFunction.isNotBlank(currentInvPaymentInstOid)){
		  invoicePaymentInstructions.setAttribute("inv_pay_inst_oid", currentInvPaymentInstOid);
		  

	loanProceedsPmtCurrency = "";
	displayLoanProceedsPmtAmount ="";

		  //invoicePaymentInstructions.getDataFromAppServer();
	  }
  }
%>
<%
if( currentInvPaymentInstOid != null && !"".equalsIgnoreCase(currentInvPaymentInstOid) ){
	//currentInvPaymentInstOid = EncryptDecrypt.encryptStringUsingTripleDes(currentInvPaymentInstOid, userSession.getSecretKey());
	currentInvPaymentInstOid = "";
}else{
	currentInvPaymentInstOid = "";
}
%>
<input dojoType="dijit.form.TextBox" type="hidden" name="InvoicePaymentInstructionsOID" id="InvoicePaymentInstructionsOID" value="<%=currentInvPaymentInstOid%>"/>
<input type=hidden name=numberOfPayees value="<%=numberOfDomPmts%>">
<div id="add_ben_div">
<% 
String gridId = "";
String gridName ="";
String gridDataView = "";
DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
String gridHtml = "";
StringBuffer dynamicWhereClause = new StringBuffer();
if (TradePortalConstants.CREDIT_MULTI_BEN_ACCT.equals(loanProceedsCreditType) && !isMultiBenInvoicesAttached){

     		gridId = "TransactionLRQTradeLoanISSDataGridId";
     		gridName = "TransactionLRQTradeLoanISSDataGrid";
     		gridDataView = "TransactionLRQTradeLoanISSDataView";
     		dynamicWhereClause.append (" ipi.p_transaction_oid = ");
     		dynamicWhereClause.append(transaction.getAttribute("transaction_oid"));
     		currentInvPaymentInstOid=invoicePaymentInstructions.getAttribute("inv_pay_inst_oid");
     		
     		%>
	<%@ include file="LoanRequest-TradeLoan-MultiBen-SearchParms.frag" %>
		<%@ include file="LoanRequest-TradeLoan-MultiBen-Search.frag" %>

	<%
	
			gridHtml = dgFactory.createDataGrid(gridId,gridName,null);
		%>
		<%=gridHtml%>
		
<%} else if (TradePortalConstants.CREDIT_MULTI_BEN_ACCT.equals(loanProceedsCreditType) && 
				isMultiBenInvoicesAttached && TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invClassification)){
		gridId = "TransactionLRQTradeLoanISSMultiBenINVsDataGridId";
 		gridName = "TransactionLRQTradeLoanISSMultiBenINVsDataGrid";
 		gridDataView = "TransactionLRQTradeLoanISSMultiBenINVsDataView";
     		dynamicWhereClause.append (" isd.a_transaction_oid = ");
     		dynamicWhereClause.append(transaction.getAttribute("transaction_oid"));
     		currentInvPaymentInstOid="";
     		
  
%>
	<%@ include file="LoanRequest-TradeLoan-MultiBen-SearchParms.frag" %>
		<%@ include file="LoanRequest-TradeLoan-MultiBen-Search.frag" %>

	<%
			gridHtml = dgFactory.createDataGrid(gridId,gridName,null);
		%>
		<%=gridHtml%>
	<%}%>



</div>
<%
boolean isError = false;
boolean isSinglBeneReadOnly = false;
  maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.ERROR_SEVERITY)) == 0) {
     // errors, so  get the data from doc.
     	isError = true;
     	loanProceedsPmtCurrency = doc.getAttribute("/In/Terms/loan_proceeds_pmt_curr_trd");
     	displayLoanProceedsPmtAmount = doc.getAttribute("/In/Terms/loan_proceeds_pmt_amount_trd");
     	paymentMethodTrd = doc.getAttribute("/In/InvoicePaymentInstructions/pay_method");
		paymentChargesTrd = doc.getAttribute("/In/InvoicePaymentInstructions/payment_charges");
		benNameTrd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_name");
		benAcctNumTrd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_acct_num");
		benAdd1Trd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_address_one");
		benAdd2Trd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_address_two");
		benAdd3Trd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_address_three");
		benCityTrd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_city");
		benCountryTrd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_country");
		benOtlCusotmerOIDTrd = doc.getAttribute("/In/InvoicePaymentInstructions/Ben_OTL_customer_id_trd");
		
		bbkNameTrd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_bank_name");
		bbkBranchCodeTrd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_branch_code");
		bbkSortCodeTrd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_bank_sort_code");
		bbkAddr1Trd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_branch_address1");
		bbkAddr2Trd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_branch_address2");
		bbkCityTrd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_bank_city");
		bbkStatPrvncTrd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_bank_prvnce");
		bbkCountryTrd = doc.getAttribute("/In/InvoicePaymentInstructions/ben_branch_country");
		bbkOTLCustOIDTrd = doc.getAttribute("/In/InvoicePaymentInstructions/Bbk_OTL_customer_id_trd");
		centralBankRep1 = doc.getAttribute("/In/InvoicePaymentInstructions/central_bank_rep1");
		centralBankRep2 = doc.getAttribute("/In/InvoicePaymentInstructions/central_bank_rep2");
		centralBankRep3 = doc.getAttribute("/In/InvoicePaymentInstructions/central_bank_rep3");
		benReferenceTrd = doc.getAttribute("/In/InvoicePaymentInstructions/customer_reference");
		selectedRptgCodes1 = doc.getAttribute("/In/InvoicePaymentInstructions/reporting_code_1");
		selectedRptgCodes2 = doc.getAttribute("/In/InvoicePaymentInstructions/reporting_code_2");

  }else{

	if ( !TradePortalConstants.CREDIT_MULTI_BEN_ACCT.equals(loanProceedsCreditType)){
		isSinglBeneReadOnly = true;
		paymentMethodTrd = terms.getAttribute("payment_method");
		paymentChargesTrd = terms.getAttribute("bank_charges_type");
		benNameTrd = termsPartyBen.getAttribute("name");
		benAcctNumTrd = termsPartyBen.getAttribute("acct_num");
		benAdd1Trd = termsPartyBen.getAttribute("address_line_1");
		benAdd2Trd = termsPartyBen.getAttribute("address_line_2");
		benAdd3Trd = termsPartyBen.getAttribute("address_line_3");
		benCityTrd = termsPartyBen.getAttribute("address_city");
		benCountryTrd = termsPartyBen.getAttribute("address_country");
		benOtlCusotmerOIDTrd = termsPartyBen.getAttribute("OTL_customer_id");
		centralBankRep1 = termsPartyBen.getAttribute("central_bank_rep1");
		centralBankRep2 = termsPartyBen.getAttribute("central_bank_rep2");
		centralBankRep3 = termsPartyBen.getAttribute("central_bank_rep3");
		selectedRptgCodes1 =  termsPartyBen.getAttribute("reporting_code_1");//CR 1001 RP
		selectedRptgCodes2 =  termsPartyBen.getAttribute("reporting_code_2");//CR 1001 RP
		if (numberOfInvoicesAttached > 1){
			benReferenceTrd = "Multiple Invoices";
		}else{
			benReferenceTrd = termsPartyBen.getAttribute("customer_reference");
		}
		bbkNameTrd = termsPartyBbk.getAttribute("name");
		bbkBranchCodeTrd = termsPartyBbk.getAttribute("branch_code");
		bbkSortCodeTrd = termsPartyBbk.getAttribute("ben_bank_sort_code");
		bbkAddr1Trd = termsPartyBbk.getAttribute("address_line_1");
		bbkAddr2Trd = termsPartyBbk.getAttribute("address_line_2");
		bbkCityTrd = termsPartyBbk.getAttribute("address_city");
		bbkStatPrvncTrd = termsPartyBbk.getAttribute("address_state_province");
		bbkCountryTrd = termsPartyBbk.getAttribute("address_country");
		bbkOTLCustOIDTrd = termsPartyBbk.getAttribute("OTL_customer_id");
		
	
		
	}else if (TradePortalConstants.CREDIT_MULTI_BEN_ACCT.equals(loanProceedsCreditType)){
		paymentMethodTrd = invoicePaymentInstructions.getAttribute("pay_method");
		paymentChargesTrd = invoicePaymentInstructions.getAttribute("payment_charges");
		benNameTrd = invoicePaymentInstructions.getAttribute("ben_name");
		benAcctNumTrd = invoicePaymentInstructions.getAttribute("ben_acct_num");
		benAdd1Trd = invoicePaymentInstructions.getAttribute("ben_address_one");
		benAdd2Trd = invoicePaymentInstructions.getAttribute("ben_address_two");
		benAdd3Trd = invoicePaymentInstructions.getAttribute("ben_address_three");
		benCityTrd = invoicePaymentInstructions.getAttribute("ben_city");
		benCountryTrd = invoicePaymentInstructions.getAttribute("ben_country");
		//benOtlCusotmerOIDTrd = termsPartyBen.getAttribute("OTL_customer_id");
		
		bbkNameTrd = invoicePaymentInstructions.getAttribute("ben_bank_name");
		bbkBranchCodeTrd = invoicePaymentInstructions.getAttribute("ben_branch_code");
		bbkSortCodeTrd = invoicePaymentInstructions.getAttribute("ben_bank_sort_code");
		bbkAddr1Trd = invoicePaymentInstructions.getAttribute("ben_branch_address1");
		bbkAddr2Trd = invoicePaymentInstructions.getAttribute("ben_branch_address2");
		bbkCityTrd = invoicePaymentInstructions.getAttribute("ben_bank_city");
		bbkStatPrvncTrd = invoicePaymentInstructions.getAttribute("ben_bank_prvnce");
		bbkCountryTrd = invoicePaymentInstructions.getAttribute("ben_branch_country");
		//bbkOTLCustOIDTrd = termsPartyBbk.getAttribute("OTL_customer_id");
		centralBankRep1 = invoicePaymentInstructions.getAttribute("central_bank_rep1");
		centralBankRep2 = invoicePaymentInstructions.getAttribute("central_bank_rep2");
		centralBankRep3 = invoicePaymentInstructions.getAttribute("central_bank_rep3");
		benReferenceTrd = invoicePaymentInstructions.getAttribute("customer_reference");
		selectedRptgCodes1 = invoicePaymentInstructions.getAttribute("reporting_code_1");
		selectedRptgCodes2 = invoicePaymentInstructions.getAttribute("reporting_code_2");
	}
}

%>
<div class="columnLeft">
<%=widgetFactory.createSubsectionHeader("LoanRequest.LoanProceeds", (isReadOnly || isFromExpress),false, false, "")%>
<div>
    <%
        options = Dropdown.createSortedCurrencyCodeOptions(isTradeLoan? loanProceedsPmtCurrency: "", loginLocale);
    %>
    	<%=widgetFactory.createTextField("BenAmountTrd","",isTradeLoan? (displayLoanProceedsPmtAmount): "","35",false,false,false," type=hidden ","", "")%>
    		<%=widgetFactory.createTextField("BenNameAmountCCYTrd","",isTradeLoan? (loanProceedsPmtCurrency): "","35",false,false,false," type=hidden ","", "")%>
    <%=widgetFactory.createSelectField("LoanProceedsPmtCurrTrd","LoanRequest.CcyOfPayment", defaultCurrencyVal, options, isReadOnly,!isTemplate, false, "onBlur=\"document.getElementById('BenNameAmountCCYTrd').value=this.value\";onChange=\"document.getElementById('BenNameAmountCCYTrd').value=this.value\" style=\"width: 50px;\"", "", "inline")%>
    <%=widgetFactory.createTextField("LoanProceedsPmtAmountTrd","LoanRequest.LoanProceedsAmount", isTradeLoan		? displayLoanProceedsPmtAmount: "", "22", isReadOnly, false, false,"onBlur=\"document.getElementById('BenAmountTrd').value=this.value\";onChange=\"document.getElementById('BenAmountTrd').value=this.value\" style=\"width: 136px;\"", "", "inline")%>

    <div style="clear: both;"></div>
</div>
<%=widgetFactory.createSubsectionHeader("LoanRequest.ApplyLoanProceedsTo",(isReadOnly || isFromExpress), !isTemplate, false, "")%>

<div>
<div>
    <div class="formItem inline">
        <%=widgetFactory.createRadioButtonField("LoanProceedsCreditTypeTrd","CreditOurAcctTrd","LoanRequest.OurAccount", TradePortalConstants.CREDIT_OUR_ACCT, (isTradeLoan ? loanProceedsCreditType		: "").equals(TradePortalConstants.CREDIT_OUR_ACCT),isReadOnly, "onClick=handleBeneficiaryRequirdness('TradeLoan',false,'A')", "")%>
    </div>
    <%
        // Using the acct_choices xml from the app party,build the dropdown and
        // select the one matching loan_proceeds_credit_acct
        appAcctList = StringFunction.xssHtmlToChars(termsPartyApp.getAttribute("acct_choices"));

        acctOptions1 = new DocumentHandler(appAcctList, true);
		
		//IValavala May 31 2013. IR T36000017670. Save the acct num instead of oid
        options = Dropdown.createSortedAcctOptions(acctOptions1,	isTradeLoan ? StringFunction.xssHtmlToChars(terms.getAttribute("loan_proceeds_credit_acct")) : "",loginLocale);
        //options = Dropdown.createSortedAcctOptionsOid(acctOptions1,	isTradeLoan ? StringFunction.xssHtmlToChars(terms.getAttribute("loan_proceeds_credit_acct")) : "",loginLocale);
        
    %>
    <%=widgetFactory.createSelectField("LoanProceedsCreditAccountTrd",""," ",options,isReadOnly,false,false,"onChange=pickOurAccountRadioTrd()"+" class='char16'","", "inline")%>
    <div style="clear: both;"></div>
</div>
<div id="payablesBenRadioBtns">
<div class="formItem">
    <%=widgetFactory.createRadioButtonField("LoanProceedsCreditTypeTrd","CreditBenAccountTrd","LoanRequest.TheFollowingBeneficiary", TradePortalConstants.CREDIT_BEN_ACCT, (isTradeLoan? loanProceedsCreditType: "").equals(TradePortalConstants.CREDIT_BEN_ACCT),isReadOnly, "onClick=handleBeneficiaryRequirdness('TradeLoan',true,'B')", "")%>
</div>
<div class="formItem">
    <%=widgetFactory.createRadioButtonField("LoanProceedsCreditTypeTrd","CreditMultiBenAccountTrd","LoanRequest.MultipleBeneficiaries", TradePortalConstants.CREDIT_MULTI_BEN_ACCT, (isTradeLoan ? loanProceedsCreditType : "").equals(TradePortalConstants.CREDIT_MULTI_BEN_ACCT),isReadOnly, "onClick=handleBeneficiaryRequirdness('TradeLoan',false,'M')", "")%>
  </div>
</div>
<div style="clear: both;"></div>
</div>
<div id="ben_details">
<%
    String defaultPM = paymentMethodTrd;
     String pLocaleSQL = " locale_name = ? ";
    if (InstrumentServices.isBlank(defaultPM))
        defaultPM = "";
        
	//jgadela  R90 IR T36000026319 - SQL FIX
	List<Object> sqlParamss = new ArrayList();
	sqlParamss.add("PAYMENT_METHOD");
	if (pLocaleSQL.indexOf("fr") != -1){
		sqlParamss.add(loginLocale);
	}
	sqlParamss.add("RTGS");
	sqlParamss.add("ACH");
	sqlParamss.add("CBFT");
	
   
    if (pLocaleSQL.indexOf("fr") == -1) pLocaleSQL = " locale_name is null";
    String pSql = "select CODE, DESCR from refdata where table_type = ? and " + pLocaleSQL +" and CODE in (?, ?, ?)";
         
    
    DocumentHandler pList = DatabaseQueryBean.getXmlResultSet(pSql, false, sqlParamss);

    String pOptions = ListBox.createOptionList(pList, "CODE", "DESCR", defaultPM, null);



%>
<%= widgetFactory.createSelectField("PaymentMethodType", "Payment Method",
       " ", pOptions, (isReadOnly), (!isTemplate?true:false), false,
        "", "", "") %>
<%
     benSearchHtml = "";
     benClearHtml = "";
     identifierStr5=",BbkBranchCodeTrd,BbkNameTrd,BbkAddressLine1Trd,BbkAddressLine2Trd,BbkCityTrd,BbkStateProvinceTrd,BbkCountryTrd,BbkOTLCustomerIdTrd";
     identifierStr4="BenNameTrd,BenAddressLine1Trd,BenAddressLine2Trd,BenAddressLine3Trd,BenCityTrd,BenCountryTrd,BenOTLCustomerIdTrd"+identifierStr5;

     sectionName4="bentrd";
    if (!(isReadOnly)) {
%>
<%
    benSearchHtml = widgetFactory.createPartySearchButton(
            identifierStr4,sectionName4, false,
            TradePortalConstants.BENEFICIARY, false);
			/* extra Tags : clearBeneficiaryImp();*/
    benClearHtml = widgetFactory.createPartyClearButton("ClearBenButton", "clearBeneficiaryTrd()", false, "");
%>
<%
    }// Always send the ben terms party oid
    secureParms.put("ben_terms_party_oid_trd",
            termsPartyBen.getAttribute("terms_party_oid"));
%>

<%=widgetFactory.createSubsectionHeader("LoanRequest.Beneficiary", isReadOnly, false, false,
        (benSearchHtml + benClearHtml))%>


<input type=hidden name='BenTermsPartyTypeTrd'
       value=<%=TradePortalConstants.BENEFICIARY%>> 
           <input type=hidden name='ben_terms_party_oid_trd'
           value=<%=termsPartyBen.getAttribute("terms_party_oid")%>> 
	<%=widgetFactory.createTextField("BenOTLCustomerIdTrd","",benOtlCusotmerOIDTrd,"35",false,false,false," type=hidden ","", "")%>
		<input
        type=hidden name="VendorIdTrd"
        value="<%=termsPartyBen.getAttribute("vendor_id")%>">
        
    <%-- Benificiary Account Number --%>
    
    <%--Beneficiary account number field can be either a text box or a set of radio buttons.
		we set the label here and put content in special div that is replaced via ajax as necessary.
		We use a partial page loading section to give a visual indicator--%>
		<div class='formItem<%=(!isTemplate) ? " required" : ""%>'>
		<%=widgetFactory.createInlineLabel("SelectedAccount",
							"LoanRequest.BeneficiaryAccountNumber")%>
			<div style="clear: both;"></div>
				<div id="beneAccountsDataLoading" style="display: none;"><%--hide it to start--%>
				<span class='dijitInline dijitIconLoading'></span> <%=resMgr.getText("common.Loading",
									TradePortalConstants.TEXT_BUNDLE)%>
				</div>
				
				<div id="tradeLoanBeneAccountsData">
				<%
					//setup the include. we pass following variables
					termsParty = termsPartyBen;
				%> <%@ include file="Transaction-LRQ-ISS-TradeLoanLoan-BeneAcctData.frag"%>
				</div>		
		</div>


<%=widgetFactory.createTextField("BenNameTrd","LoanRequest.BeneName",isTradeLoan ? (StringFunction.xssCharsToHtml(benNameTrd)) : "","35",
        isReadOnly,(!isTemplate?true:false),
        false,"onBlur='checkBeneficiaryNameTrd(\""+ StringFunction.escapeQuotesforJS(benNameTrd)+ "\"); pickTheFollowingBeneficiaryRadioTrd();'","", "")%>

<%=widgetFactory.createTextField("BenAddressLine1Trd","LoanRequest.BeneAddressLine1",isTradeLoan? (StringFunction.xssCharsToHtml(benAdd1Trd)): "", "35", isReadOnly, false, false,"onBlur='pickTheFollowingBeneficiaryRadioTrd();'", "", "")%>

<%=widgetFactory.createTextField("BenAddressLine2Trd","",isTradeLoan? (StringFunction.xssCharsToHtml(benAdd2Trd)): "", "35", isReadOnly, false, false, "", "", "")%>

<%=widgetFactory.createTextField("BenAddressLine3Trd","",isTradeLoan? (StringFunction.xssCharsToHtml(benAdd3Trd)): "", "35", isReadOnly, false, false, "", "", "")%>
<%-- KMehta - 17 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - Begin --%>
<%=widgetFactory.createTextField("BenCityTrd","",isTradeLoan? (StringFunction.xssCharsToHtml(benCityTrd)): "", "23", isReadOnly, false, false, " type=hidden ", "", "")%>
<%-- KMehta - 17 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - End --%>      
<%
    options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY,
            isTradeLoan? (benCountryTrd): "", loginLocale);
%>
<%=widgetFactory.createSelectField("BenCountryTrd","LoanRequest.BeneCountry", " ", options, isReadOnly, (!isTemplate?true:false),false, "", "", "")%>
<%=widgetFactory.createTextField("BenReferenceTrd","LoanRequest.BeneBenReference",isTradeLoan? (StringFunction.xssCharsToHtml(benReferenceTrd)): "", "35", isReadOnly, false, false, "", "", "")%>
</div>
</div>

<div class="columnRight">
    <% bankChargesType = paymentChargesTrd;%>

    					<%--Start IR#T36000031463 Vsarkary Rel 9.2 As the label need to be validate for required(Mandatory) attribute on change of 
	Radio buttons dynamically we require Id to validate so commenting out WF Code and creating Static Div with ID. --%>
  <%-- <%= widgetFactory.createLabel("", "DomesticPaymentRequest.BankCharges", false,(!isTemplate?true:false), false, "") %> --%>
    
    <div id= "chargesDivId" class="formItem required">
		<label><%=resMgr.getText("DomesticPaymentRequest.BankCharges",TradePortalConstants.TEXT_BUNDLE)%></label>
	</div>
	
	<%--End IR#T36000031463 Vsarkary Rel 9.2 --%>
	

    <div class="formItem">
        <%=widgetFactory.createRadioButtonField("BankChargesType","TradePortalConstants.CHARGE_UPLOAD_FW_OURS",
					"DomesticPaymentRequest.AllOurAcct",TradePortalConstants.CHARGE_UPLOAD_FW_OURS,
					TradePortalConstants.CHARGE_UPLOAD_FW_OURS.equals(bankChargesType),  isReadOnly,
					"","") %>
        <br>
        <%=widgetFactory.createRadioButtonField("BankChargesType","TradePortalConstants.CHARGE_UPLOAD_FW_BEN",
					"DomesticPaymentRequest.PayeeAccount",TradePortalConstants.CHARGE_UPLOAD_FW_BEN,
					TradePortalConstants.CHARGE_UPLOAD_FW_BEN.equals(bankChargesType),  isReadOnly,
					"","") %>
        <br>
        <%=widgetFactory.createRadioButtonField("BankChargesType","TradePortalConstants.CHARGE_UPLOAD_FW_SHARE",
					"DomesticPaymentRequest.Share",TradePortalConstants.CHARGE_UPLOAD_FW_SHARE,
					 TradePortalConstants.CHARGE_UPLOAD_FW_SHARE.equals(bankChargesType),
					 isReadOnly, "","") %>

    </div>
    <div style="clear: both;"></div>
<div id="benBank_details">
    <%
       benBankSearchHtml = "";
       identifierStr5="BbkBranchCodeTrd,BbkNameTrd,BbkAddressLine1Trd,BbkAddressLine2Trd,BbkCityTrd,BbkStateProvinceTrd,BbkCountryTrd,BbkOTLCustomerIdTrd";
       sectionName5="bbktrd";
        if (!(isReadOnly)) {
            benBankSearchHtml = widgetFactory.createBankSearchButton(
                    identifierStr5,sectionName5, false,
                    TradePortalConstants.BENEFICIARY_BANK);
        }
        // Always send the bbk terms party oid
        secureParms.put("bbk_terms_party_oid_trd",termsPartyBbk.getAttribute("terms_party_oid"));
    %>

    <%=widgetFactory.createSubsectionHeader("LoanRequest.BankBeneficiary", isReadOnly, (!isTemplate?true:false), false,benBankSearchHtml)%>
    <input type=hidden name='BbkTermsPartyTypeTrd'
           value=<%=TradePortalConstants.BENEFICIARY_BANK%>> 
    <input type=hidden name='bbk_terms_party_oid_trd'
           value=<%=termsPartyBbk.getAttribute("terms_party_oid")%>> 
	<%=widgetFactory.createTextField("BbkOTLCustomerIdTrd","",bbkOTLCustOIDTrd,"35",false,false,false," type=hidden ","", "")%>
   <%=widgetFactory.createTextField("BbkBranchCodeTrd","LoanRequest.BeneBankBranchCode",isTradeLoan ? StringFunction.xssCharsToHtml(bbkBranchCodeTrd) : "","35",
            isReadOnly,false,
            false,"onBlur='checkBeneficiaryBankBranchCodeTrd(\""+ StringFunction.escapeQuotesforJS(bbkNameTrd)+ "\")'", "","")%>
            
   <%=widgetFactory.createTextField("BbkSortCodeTrd","LoanRequest.BeneBankSortCode",isTradeLoan ? StringFunction.xssCharsToHtml(bbkSortCodeTrd) : "","8",
            isReadOnly,false,
            false,"", "","")%>


    <%=widgetFactory.createTextField("BbkNameTrd","LoanRequest.BeneBankName",isTradeLoan ? StringFunction.xssCharsToHtml(bbkNameTrd) : "","35",
            isReadOnly,false,
            false,"onBlur='checkBeneficiaryBankNameTrd(\""+ StringFunction.escapeQuotesforJS(bbkNameTrd) + "\")'", "","")%>

    <%=widgetFactory.createTextField("BbkAddressLine1Trd","LoanRequest.BeneBankAddressLine1", isTradeLoan? (StringFunction.xssCharsToHtml(bbkAddr1Trd)): "", "35", isReadOnly, false, false,"onBlur='pickTheFollowingBeneficiaryRadioTrd();'", "", "")%>

    <%=widgetFactory.createTextField("BbkAddressLine2Trd","", isTradeLoan? (StringFunction.xssCharsToHtml(bbkAddr2Trd)): "", "35", isReadOnly, false, false, "", "", "")%>
	<%-- KMehta - 17 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - Begin --%>
    <%=widgetFactory.createTextField("BbkCityTrdTemp","",isTradeLoan? (StringFunction.xssCharsToHtml(bbkCityTrd) + " " +StringFunction.xssCharsToHtml(bbkStatPrvncTrd)): "", "23", isReadOnly, false, false," onBlur='pickTheFollowingBeneficiaryRadioTrd();' onChange='setbbkCityTrd();' ", "", "")%>
    <%--KMehta - 17 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - End --%>
    <div>
         <%=widgetFactory.createTextField("BbkCityTrd","",isTradeLoan ? (StringFunction.xssCharsToHtml(bbkCityTrd)) : "", "8",
                false, false, false, " type=hidden ", "", "inline")%>
        <%=widgetFactory.createTextField("BbkStateProvinceTrd","",isTradeLoan ? (StringFunction.xssCharsToHtml(bbkStatPrvncTrd)) : "", "8",
                false, false, false, " type=hidden ", "", "inline")%>



        <div style="clear: both;"></div>
    </div>
    <%
        options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY,
                isTradeLoan? (bbkCountryTrd): "", loginLocale);
    %>
    <%=widgetFactory.createSelectField("BbkCountryTrd","LoanRequest.BeneBankCountry", " ", options, isReadOnly,false, false, "", "", "")%>
</div>
	<div id="central_bank_reporting_div">
    <%= widgetFactory.createSubsectionHeader("DomesticPaymentRequest.CentraBankReportingLabel",false, false, false,"" ) %>

	
    <%=widgetFactory.createTextField("central_bank_reporting_1","DomesticPaymentRequest.CentraBankReporting",
						isTradeLoan ? StringFunction.xssCharsToHtml(centralBankRep1) :"","35",
						(isReadOnly),
						false,false, "", "", "") %>


    <%=widgetFactory.createTextField("central_bank_reporting_2","",
						isTradeLoan ? StringFunction.xssCharsToHtml(centralBankRep2) :"","35",
						(isReadOnly ),
						false,false, "", "", "") %>

    <%=widgetFactory.createTextField("central_bank_reporting_3","",
						isTradeLoan ? StringFunction.xssCharsToHtml(centralBankRep3) :"","35",
						(isReadOnly ),
						false,false, "", "", "") %>

	</div>
	  <%-- RPasupulati CR1001 REL 9.4 Start--%>
	<div id="reporting_code_div">
	<%
	
	
	StringBuffer reportingCode1 = new StringBuffer();
	StringBuffer reportingCode2 = new StringBuffer();
	reportingCode1.append("select code, description from payment_reporting_code_1");
	reportingCode2.append("select code, description from payment_reporting_code_2");
	reportingCode1.append(" where p_bank_group_oid = ? ");
	reportingCode2.append(" where p_bank_group_oid = ? ");
	
	DocumentHandler xmlDocRep1 = DatabaseQueryBean.getXmlResultSet(reportingCode1.toString(), false, new Object[]{bankGroupOid});
	DocumentHandler xmlDocRep2 = DatabaseQueryBean.getXmlResultSet(reportingCode2.toString(), false, new Object[]{bankGroupOid});
	String codeOptions1 = "";
	String codeOptions2 = "";
	if (xmlDocRep1 != null) {
		codeOptions1 = Dropdown.createMultiTextOptions(xmlDocRep1,"CODE", "CODE,DESCRIPTION", selectedRptgCodes1,loginLocale);
			
	}

	if (xmlDocRep2 != null) {
	   codeOptions2 = Dropdown.createMultiTextOptions(xmlDocRep2, "CODE", "CODE,DESCRIPTION", selectedRptgCodes2,loginLocale);
	  
	}
	
	StringBuffer reportingCodeDesc1 = new StringBuffer();
	StringBuffer reportingCodeDesc2 = new StringBuffer();
	DocumentHandler xmlDocDesc = null;
	reportingCodeDesc1.append("select description from payment_reporting_code_1 where code= ? and rownum=1");
	xmlDocDesc = DatabaseQueryBean.getXmlResultSet(reportingCodeDesc1.toString(), false, new Object[]{selectedRptgCodes1});
	if(xmlDocDesc != null){
		selectedRptgCodeDesc1 = selectedRptgCodes1 + "-" + xmlDocDesc.getAttribute("/ResultSetRecord(0)/DESCRIPTION");
	} 
	
	reportingCodeDesc2.append("select description from payment_reporting_code_2 where code= ? and rownum=1");
	xmlDocDesc = DatabaseQueryBean.getXmlResultSet(reportingCodeDesc2.toString(), false, new Object[]{selectedRptgCodes2});
	if(xmlDocDesc != null){
		selectedRptgCodeDesc2 = selectedRptgCodes2 + "-" + xmlDocDesc.getAttribute("/ResultSetRecord(0)/DESCRIPTION");
	} 
	
	%>
	<%=widgetFactory.createSelectField("ReportingCode1",
			"LoanRequest.ReportingCode1",
			" ", codeOptions1, isReadOnly, false, false, "", "", "")%>
	
	<%=widgetFactory.createSelectField("ReportingCode2",
			"LoanRequest.ReportingCode2",
			" ", codeOptions2, isReadOnly, false, false, "", "", "")%>
 </div>
 <%-- RPasupulati CR1001 REL 9.4 End--%>
  <div id="save_ben_button_div" class='formItem inline'>
			<button data-dojo-type="dijit.form.Button" type="button" name="saveBenButton" id="saveBenButton" class="SearchButton">
		            <%=resMgr.getText("Save Beneficiary", TradePortalConstants.TEXT_BUNDLE)%>
		            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		        	
					dijit.byId("InvoicePaymentInstructionsOID").set("value",null);		        	
					dijit.byId('InvoicePaymentInstructionsOID').set('checked', false);
		  			setButtonPressed('<%=TradePortalConstants.BUTTON_SAVETRANS%>', '0');
					document.forms[0].submit();
					
				</script>
			 	</button>
	 
	 		
	 	</div>

		<div id="update_ben_button_div" class='formItem inline'>
			<button data-dojo-type="dijit.form.Button" type="button" name="updateBenButton" id="updateBenButton" class="SearchButton">
	            <%=resMgr.getText("common.UpdateBeneficiaryText", TradePortalConstants.TEXT_BUNDLE)%>
	            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		        	    
		        	//dijit.byId("InvoicePaymentInstructionsOID").set("value",'<%=EncryptDecrypt.encryptStringUsingTripleDes(currentInvPaymentInstOid, userSession.getSecretKey())%>');
		  			setButtonPressed('<%=TradePortalConstants.BUTTON_SAVETRANS%>', '0');
					document.forms[0].submit();
					
				</script>
		 	</button>
	 
	 		
	 	</div>
	 	<div class='formItem inline' id="cancel_ben_link_div"><a href="Javascript: reloadBeneficiaryFormDetails();" name="cancel" ><%=resMgr.getText("common.cancel", TradePortalConstants.TEXT_BUNDLE)%></a></div>	    
</div>

<%@ include file="Transaction-LRQ-ISS-TradeLoan-InvoiceDetails.frag" %>
<div style="clear:both;"></div>
    <%=widgetFactory.createSubsectionHeader("LoanRequest.LoanMaturity", (isReadOnly || isFromExpress),!isTemplate, false, "")%>
    <div class="formItem">
        <%=widgetFactory.createRadioButtonField("LoanMaturityDebitTypeTrd","DebitAppTrd","LoanRequest.DebitTheBorrowersAccount", TradePortalConstants.DEBIT_APP, (isTradeLoan? loanMaturityDebitType: "").equals(TradePortalConstants.DEBIT_APP),isReadOnly, "", "")%>
        <%
            // Using the acct_choices xml from the app party,build the dropdown and
            // select the one matching loan_maturity_debit_acct
            acctNum = StringFunction.xssHtmlToChars(terms.getAttribute("loan_maturity_debit_acct"));

            appAcctList = StringFunction.xssHtmlToChars(termsPartyApp.getAttribute("acct_choices"));

            //DocumentHandler acctOptions = new DocumentHandler(appAcctList, true);
            acctOptions1 = new DocumentHandler(appAcctList, true);
            options = Dropdown.createSortedAcctOptions(acctOptions1, isTradeLoan? acctNum
                    : "", loginLocale);
        %>
        <br/>
        <%=widgetFactory.createSelectField("LoanMaturityDebitAccountTrd", "", " ", options, isReadOnly,false, false,"onChange='pickDebitApplicantsAccountTrdRadio();'"+" class='char16'", "", "")%>
        <br/>
        <%=widgetFactory.createRadioButtonField("LoanMaturityDebitTypeTrd","DebitOtherTrd","LoanRequest.OtherRadio", TradePortalConstants.DEBIT_OTHER, (isTradeLoan? loanMaturityDebitType: "").equals(TradePortalConstants.DEBIT_OTHER),isReadOnly, "", "")%>
        <div style="clear: both;"></div>
    </div>
    <div class="formItem">
        <%=widgetFactory.createTextArea("LoanMaturityDebitOtherTextTrd","",isTradeLoan? (terms.getAttribute("loan_maturity_debit_other_txt")): "", isReadOnly, !isTemplate, false,"onChange=\"pickDebitOtherTrdRadio();\" style=\"min-height: 60px;\"", "", "")%>
    </div>
 

<script LANGUAGE="JavaScript">

    <%-- PICK RELATED INSTRUMENT RADIO IF RELATED INSTRUMENT ID IS ENTERED --%>
    function pickRelatedInstrumentRadio() {

        if (document.forms[0].RelatedInstrumentID.value > '' || document.forms[0].MultipleRelatedInstrumentsTrd.value > '' ) {
            document.forms[0].LoanProceedsCreditTypeTrd[0].checked = true;
        }
    }

    <%-- PICK OUR ACCOUNT RADIO IF AN ACCOUNT NUMBER IS SELECTED --%>

    <%-- PICK THE FOLLOWING BENEFICIARY RADIO IF A BENEFICIARY IS SELECTED --%>
    <%-- PICK OTHER (ENTER ...) RADIO IF APPLY LOAN PROCEEDS TO OTHER TEXT BOX IS FILLED OUT --%>
    function pickApplyProceedsToOtherRadioTrd() {

        if (document.forms[0].LoanProceedsCreditOtherTextTrd.value > '') {
            document.forms[0].LoanProceedsCreditTypeTrd[2].checked = true;
        }
    }

    function checkBeneficiaryNameTrd(originalName) {

        if (document.forms[0].BenNameTrd.value != originalName)
        {
            document.forms[0].BenOTLCustomerIdTrd.value = "";
        }
    }

    function checkBeneficiaryBankNameTrd(originalName) {

        if (document.forms[0].BbkNameTrd.value != originalName)
        {
            document.forms[0].BbkOTLCustomerIdTrd.value = "";
        }
    }
    <%-- rkrishna IR-PAUH101945718 11/01/2007 Add Begin --%>
    function clearBeneficiaryTrd() {

        document.TransactionLRQ.BenNameTrd.value = "";
        document.TransactionLRQ.BenAddressLine1Trd.value = "";
        document.TransactionLRQ.BenAddressLine2Trd.value = "";
        document.TransactionLRQ.BenAddressLine3Trd.value = "";
        document.TransactionLRQ.BenCountryTrd.value = "";
        document.TransactionLRQ.BenOTLCustomerIdTrd.value = "";
        document.TransactionLRQ.VendorIdTrd.value = "";

    }
    <%-- rkrishna IR-PAUH101945718  11/01/2007 End --%>

function pickDebitOtherTrdRadio() {
        if (dijit.byId("LoanMaturityDebitOtherTextTrd").getValue() != null ) { 
             dijit.byId("DebitOtherTrd").set('checked',true);
          }
    }			
function pickDebitApplicantsAccountTrdRadio(){
        if (dijit.byId("LoanMaturityDebitAccountTrd").getValue() != null ) { 
             dijit.byId("DebitAppTrd").set('checked',true);
             dijit.byId("LoanMaturityDebitOtherTextTrd").set("value","");
          }
}
function checkBeneficiaryBankBranchCodeTrd(bbkNameTrd){
}
</script>
