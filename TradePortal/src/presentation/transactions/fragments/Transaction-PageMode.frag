<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                       Transaction Page - Page Mode
  Description:
    Contains logic to declare and set several indicators that help determine
  the mode of the page.  The following indicators are defined and may be used
  by the JSP after including this file:
     isReadOnly        - indicates if page should display in readonly mode
     isTemplate        - indicates if the instrument is a template
     isExpressTemplate - indicates if the instrument is an express template
     isFromExpress     - indicates if the transaction was created from an
                         express template

  NOTE: You must declare and assign the variable "requestedSecurityRight".
  Because this JSP is <% @ included %>, no parameters can be passed into this
  JSP.  Therefore you must define the variable and assign it a value.  This
  variable represents the create/modify security right for the specific
  instrument type.  For example:

     String requestedSecurityRight = SecurityAccess.DLC_CREATE_MODIFY;

  This JSP assumes the existence of several variables.  You have declared and
  populated webbeans for instrument, transaction, template, and userSession 
  (with those names).  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-ISS-General.jsp" %>
*******************************************************************************
--%>
<%
  boolean isReadOnly = true;	     // Indicates if JSP displays as read only
                                     //   (assume readonly as default)
  boolean isTemplate = false;        // Indicates if we're editing a template
  boolean isExpressTemplate = false; // Indicates if we're editing an express
                                     //    template.
  boolean isFromExpress = false;     // Indicates if transaction is from an 
                                     //    express template.

  boolean showTemplateSave = false;  // Indicates if save button is shown for
                                     //    template maintenance
  boolean showTemplateDelete = false;// Indicates if delete button is shown for
                                     //    template maintenance
  boolean isFixedPayment = false;//Rapsupulati IR #T36000019732
  
  boolean isFixedTemplate = false;
  
  String value;
  
  value = instrument.getAttribute("from_express_template");
  if (value != null && value.equals(TradePortalConstants.INDICATOR_YES)) {
     isFromExpress = true;
  }
	
  value = instrument.getAttribute("template_flag");
  if (value != null && value.equals(TradePortalConstants.INDICATOR_YES)) {
     isTemplate = true;
  }

  if (isTemplate) {
     value = template.getAttribute("express_flag");
     if (TradePortalConstants.INDICATOR_YES.equals(value)) {
        isExpressTemplate = true;
     }     
     value = template.getAttribute("fixed_flag");
     if (TradePortalConstants.INDICATOR_YES.equals(value)) {
    	 isFixedTemplate = true;
     }
  }
  //Rpasupulati IR #T36000019732 start.
  if(!isTemplate){
		value = instrument.getAttribute("fixed_payment_flag");
  	if (value != null && value.equals(TradePortalConstants.INDICATOR_YES)) {
     	isFixedPayment = true;
		}
	}
	//Rpasupulati IR #T36000019732 end.
  // Determine readonly access. 

  if (isTemplate)
  {
    // We're working with a template.

    // User can edit template if he has MAINTAIN rights.  He can view is he
    // has view rights.  For neither right, send him home.

    // Template must appear in read only if it is not owned by the user's org
    if(!userSession.getOwnerOrgOid().equals(template.getAttribute("owner_org_oid")))
    {
 	 isReadOnly = true;
    }
    else if (SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_TEMPLATE))
    {
       isReadOnly = false;
    }
    else if (SecurityAccess.hasRights(loginRights, SecurityAccess.VIEW_TEMPLATE))
    {
       isReadOnly = true;
    }
    else
    {
       // Don't have maintain or view access, send the user back.
       String returnAction = "goToRefDataHome";

       NavigationManager navMgr = NavigationManager.getNavMan();
       String nextPage = navMgr.getNextPage(null, returnAction);
       String physicalPage = navMgr.getPhysicalPage(nextPage, request);

       formMgr.setCurrPage(nextPage);
%>
       <jsp:forward page='<%=physicalPage%>' /> 
<%
       return;
    }

    // Corporate customers can never edit Express templates.
    // Admin users utilizing the customer access feature can edit the 
    // express templates owned by a corporate customer
    if (userSession.getSecurityType().equals(TradePortalConstants.NON_ADMIN)) 
    {
     if(!userSession.hasSavedUserSession() ||
        userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.NON_ADMIN) )
      {
         if (isExpressTemplate) {
           isReadOnly = true;
         }
      }
    }

  } else {
    // We're working with a transaction.  To get out of readonly mode, the
    // transaction must be in an editable state and the user must have
    // the correct access right (requestedSecurityRight was set earlier by
    // the includer of this page.  Finally, is must have been locked by
    // the user.

    String status = transaction.getAttribute("transaction_status");
    String oid = instrument.getAttribute("instrument_oid");
    long instOid = Long.valueOf(oid).longValue();
    long userOid = Long.valueOf(userSession.getUserOid()).longValue();

    if (InstrumentServices.isEditableTransStatus(status))
    {
       if (SecurityAccess.hasRights(userSession.getSecurityRights(), requestedSecurityRight))
       {
          // Set the read-only status to true if a parent org user is accessing a child org instrument and don't 
          // lock the instrument if one is; otherwise, try to lock the instrument for the user and set the 
          // read-only status to false if successful.
          if (!userSession.getOwnerOrgOid().equals(instrument.getAttribute("corp_org_oid")))
          {
             isReadOnly = true;
          }
          else if (LockingManager.isLocked(instOid, userOid, false))
          {
             isReadOnly = false;
          }
       }
    }
  }

  Debug.debug("isReadonly " + isReadOnly);
  Debug.debug("isTemplate " + isTemplate);
  Debug.debug("isExpressTemplate " + isExpressTemplate);
  Debug.debug("isFromExpress " + isFromExpress);
%>