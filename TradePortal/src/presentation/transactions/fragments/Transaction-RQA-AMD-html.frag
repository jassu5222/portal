<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Request to Advise Amend Page - all sections

  Description:
    Contains HTML to create the Request to Advise Amend page.  All data retrieval 
  is handled in the Transaction-RQA-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-RQA-ISS-BankDefined.jsp" %>
*******************************************************************************

--%>

<%--
*******************************************************************************
                     Import DLC Issue Page - General section
*******************************************************************************
--%>
	<%= widgetFactory.createSectionHeader("1", "RequestAdviseAmend.General") %>
	<div class="columnLeftNoBorder">
		<%= widgetFactory.createTextField("IssuerReferenceNumber","RequestAdviseAmend.IssuerReferenceNumber",issuerRefNum,"30",true) %>
		<%= widgetFactory.createTextField("ApplRefNumber","transaction.ApplRefNumber",instrument.getAttribute("copy_of_ref_num"),"30",true) %>
		<%= widgetFactory.createTextField("ApplicantName","ApprovalToPayAmend.BuyerName",applicantName,"30",true) %>
		<%= widgetFactory.createDateField("CurrentExpiryDate","RequestAdviseAmend.CurrentExpiryDate",currentExpiryDate,true) %>
		<%= widgetFactory.createDateField( "ExpiryDate", "SLCAmend.NewExpiryDate", StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")), isReadOnly, false, false, "class='char6'", dateWidgetOptions, "")%>
         
	</div>		
	
	<div class="columnRight">
   <%
     if(!isProcessedByBank)
      {
    	 String lcAmt = currencyCode +"  "+ TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), currencyCode, loginLocale);
  		%>
			
			<%= widgetFactory.createTextField("LCAmount","RequestAdviseAmend.LCAmount",lcAmt,"30",true) %>
			
			<%
				}
			%>
	
		&nbsp;&nbsp;&nbsp;<%= widgetFactory.createRadioButtonField("IncreaseDecreaseFlag", "Increase", 
			"SLCAmend.IncreaseAmount", TradePortalConstants.INCREASE, incrDecrValue.equals(TradePortalConstants.INCREASE), isReadOnly, " onChange='setNewAmount();'", "") %>
		<br/>
		&nbsp;&nbsp;&nbsp;<%= widgetFactory.createRadioButtonField("IncreaseDecreaseFlag", "Decrease", 
			"SLCAmend.DecreaseAmount", TradePortalConstants.DECREASE, incrDecrValue.equals(TradePortalConstants.DECREASE), isReadOnly, " onChange='setNewAmount();'", "") %>
			
	
	<%if (!isReadOnly) {%>
	<br><br>
	<div class="formItem">
	<%=widgetFactory.createInlineLabel("",currencyCode)%>
	<%= widgetFactory.createAmountField( "TransactionAmount", "", displayAmount, currencyCode, isReadOnly, false, false, " onChange='setNewAmount();'", "", "none") %> 				
	</div>
	<%}else{
		displayAmount = TPCurrencyUtility.getDisplayAmount(displayAmount, currencyCode, loginLocale);
		StringBuffer         tempDisplayAmt              = new StringBuffer();
		tempDisplayAmt.append(currencyCode);
		tempDisplayAmt.append(" ");
		tempDisplayAmt.append(displayAmount);
		%>
		<%= widgetFactory.createAmountField( "TransactionAmount", " ", tempDisplayAmt.toString(), "", true, false, false, " onChange='setNewAmount();'", "", "") %>
				
							
	<%}%>
	     
	    
	<%
	String newLCAmount= "";
	 if(!isProcessedByBank)
	  {
		 newLCAmount = currencyCode + " " + TPCurrencyUtility.getDisplayAmount(calculatedTotal.toString(),currencyCode, loginLocale);
		
	%>
	
	<%= widgetFactory.createTextField( "TransactionAmount2", "SLCAmend.NewLCAmount", newLCAmount.toString(), "22", true, false, false, "", "", "") %> 				
	

	<%
	}
	%>
	
	

	&nbsp;&nbsp;&nbsp;<%= widgetFactory.createRadioButtonField("AmountToleranceType", "newAmtTol", 
		"RequestAdviseAmend.NewAmtTolerance", TradePortalConstants.AMOUNT_TOLERANCE, amountToleranceType.equals(TradePortalConstants.AMOUNT_TOLERANCE), isReadOnly || isFromExpress) %>
	<br>
		<div class="formItem">
			<%= widgetFactory.createSubLabel("transaction.Plus") %>
			<%if(!isReadOnly){ %>
			<%= widgetFactory.createPercentField("AmountTolerancePlus", "", terms.getAttribute("amt_tolerance_pos").toString(),isReadOnly,
							false, false, "onChange=\"setNewAmtTol();\"", "", "none") %>
			<%}else {%>
				<b><%=terms.getAttribute("amt_tolerance_pos") %></b>
			<%} %>
			<%= widgetFactory.createSubLabel( "transaction.Percent") %>
			&nbsp;&nbsp;

			<%= widgetFactory.createSubLabel("transaction.Minus") %>				
			<%if(!isReadOnly){ %>
			<%= widgetFactory.createPercentField("AmountToleranceMinus", "", terms.getAttribute("amt_tolerance_neg").toString(),isReadOnly,
							false, false, "onChange=\"setNewAmtTol();\"", "", "none") %>	
			<%}else {%>
				<b><%=terms.getAttribute("amt_tolerance_neg") %></b>
			<%} %>	
			<%= widgetFactory.createSubLabel( "transaction.Percent") %>
			<div style="clear:both;"></div>
		</div>	
		
	<%= widgetFactory.createLabel("", "RequestAdviseAmend.NewMaximumCreditAmount", false, false, false, "") %>	
	&nbsp;&nbsp;&nbsp;<%= widgetFactory.createRadioButtonField("AmountToleranceType", "amtTolExceed", 
			"RequestAdviseIssue.NotExceeding", TradePortalConstants.NOT_EXCEEDING, amountToleranceType.equals(TradePortalConstants.NOT_EXCEEDING), isReadOnly || isFromExpress) %>
	<br>


<%= widgetFactory.createTextArea("AddlAmountsCovered", "RequestAdviseAmend.NewAdditionalAmountsCovered", terms.getAttribute("addl_amounts_covered"), isReadOnly || isFromExpress, false, false, "onChange=\"setAmtTolExceed();\" style='width:auto;' rows='4' cols='50'", "", "")%>

<div style="clear: both"></div>
    
 </div>  

</div>
 

<%--
*******************************************************************************
                  Import DLC Amend Page - Shipment section

*******************************************************************************
--%>
<%= widgetFactory.createSectionHeader("2", "RequestAdviseAmend.Shipment") %>
		<%=widgetFactory.createWideSubsectionHeader("ApprovalToPayIssue.Shipment")%>
  		<div class="columnLeftNoBorder">
		<%= widgetFactory.createDateField( "ShipmentDate", "ApprovalToPayIssue.NewLastestShipDate", terms.getFirstShipment().getAttribute("latest_shipment_date"), isReadOnly, false, isExpressTemplate,  "class='char6'", dateWidgetOptions, "")%>
		
		<%
        options = Dropdown.createSortedRefDataOptions( TradePortalConstants.INCOTERM,
        						terms.getFirstShipment().getAttribute("incoterm"),
                               loginLocale);
		%>
				
		<%= widgetFactory.createSelectField( "Incoterm", "ImportDLCAmend.NewShippingTerm", " ", options, isReadOnly, false, isExpressTemplate,  "class='char30'", "", "" ) %>
		</div>
		

		<div class="columnRight">
		<%= widgetFactory.createTextField( "IncotermLocation", "ImportDLCAmend.NewShipTermLocation", terms.getFirstShipment().getAttribute("incoterm_location"), "30", isReadOnly , false, false,  "", "",""  ) %>
          	
		</div>
		<div style="clear:both;"></div>
	
         <div class="columnLeftNoBorder">
         <span class="formItem">
			<%= widgetFactory.createStackedLabel("From", "ImportDLCIssue.From") %>
		 </span>
	<%if(!isReadOnly){ %>
		<%= widgetFactory.createTextField( "ShipFromPort", "ImportDLCAmend.NewFromPort", terms.getFirstShipment().getAttribute("shipment_from"), "65", isReadOnly, false, false,  "style='width:310px;'", "",""  ) %>
	<%}else{ %>
		<%= widgetFactory.createTextArea( "ShipFromPort", "ImportDLCAmend.NewFromPort", terms.getFirstShipment().getAttribute("shipment_from"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
	<%} %>
	<%if(!isReadOnly){ %>
		<%= widgetFactory.createTextField( "ShipToPort", "ImportDLCAmend.NewToPort", terms.getFirstShipment().getAttribute("shipment_to"), "65", isReadOnly, false, false,  "style='width:310px;'", "",""  ) %>
	<%}else{ %>
		<%= widgetFactory.createTextArea( "ShipToPort", "ImportDLCAmend.NewToPort", terms.getFirstShipment().getAttribute("shipment_to"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
	<%} %>	
		</div>
		<div class="columnRight">
		<span class="formItem">
		<%=widgetFactory.createStackedLabel("To", "ImportDLCIssue.To") %>
		</span>		
	<%if(!isReadOnly){ %>
	<%= widgetFactory.createTextField( "ShipToDischarge", "ImportDLCAmend.NewToDischarge", terms.getFirstShipment().getAttribute("shipment_to_discharge"), "65", isReadOnly, false, false,  "style='width:310px;'", "",""  ) %>
	<%}else{ %>
		<%= widgetFactory.createTextArea( "ShipToDischarge", "ImportDLCAmend.NewToDischarge", terms.getFirstShipment().getAttribute("shipment_to_discharge"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
	<%} %>
	<%if(!isReadOnly){ %>
	<%= widgetFactory.createTextField( "ShipFromLoad", "ImportDLCAmend.NewFromLoad", terms.getFirstShipment().getAttribute("shipment_from_loading"), "65", isReadOnly, false, false,  "style='width:310px;'", "",""  ) %>
	<%}else{ %>
		<%= widgetFactory.createTextArea( "ShipFromLoad", "ImportDLCAmend.NewFromLoad", terms.getFirstShipment().getAttribute("shipment_from_loading"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
	<%} %>
		</div>
		<div style="clear:both;"></div>
<%=widgetFactory.createWideSubsectionHeader("ImportDLCAmend.NewGoodsDescription")%>

	<%
		options = ListBox.createOptionList(goodsDocList, "PHRASE_OID",
					"NAME", "", userSession.getSecretKey());
			defaultText = resMgr.getText("transaction.SelectPhrase",
					TradePortalConstants.TEXT_BUNDLE);
	%>
	<%=widgetFactory.createSelectField("GoodsPhraseItem","",defaultText,options,isReadOnly,false,false,
			"onChange="+ PhraseUtility.getPhraseChange(
											"/Terms/ShipmentTermsList/goods_description",
											"GoodsDescText", "6500",
											"document.forms[0].GoodsDescText"),
					"", "inline")%>
	    
	<%=widgetFactory.createTextArea("GoodsDescText", "", terms.getFirstShipment().getAttribute("goods_description").toString(),isReadOnly, false, false, "rows='10' cols='128'", "", "")%>
	<%if(!isReadOnly) {%>
		<%=widgetFactory.createHoverHelp("GoodsDescText","InstrumentIssuePlaceHolder.GoodsDesc") %>
		<%} %>
	<%
		userOrgAutoLCCreateIndicator = userSession
				.getOrgAutoLCCreateIndicator();
		transactionOid = transaction.getAttribute("transaction_oid");

		if (((userOrgAutoLCCreateIndicator != null) && (userOrgAutoLCCreateIndicator
				.equals(TradePortalConstants.INDICATOR_YES)))
				&& ((SecurityAccess.hasRights(loginRights,
						SecurityAccess.DLC_PROCESS_PO)) || (userSession
						.hasSavedUserSession() && (userSession
						.getSavedUserSessionSecurityType()
						.equals(TradePortalConstants.ADMIN))))) {
			// Compose the SQL to retrieve PO Line Items that are currently assigned to 
			// this transaction
			sqlQuery = new StringBuffer();
			sqlQuery.append("select a_source_upload_definition_oid, ben_name, currency");
			sqlQuery.append(" from po_line_item where a_assigned_to_trans_oid = ?");
			//jgadela  R90 IR T36000026319 - SQL FIX
			Object[] sqlParamsPOLine= new Object[1];
			sqlParamsPOLine[0] =  transactionOid;

			// Retrieve any PO line items that satisfy the SQL query that was just constructed
			poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), true, sqlParamsPOLine);

			if (poLineItemsDoc != null) {
				hasPOLineItems = true;

				poLineItems = poLineItemsDoc
						.getFragments("/ResultSetRecord");
				poLineItemDoc = (DocumentHandler) poLineItems.elementAt(0);
				poLineItemUploadDefinitionOid = poLineItemDoc
						.getAttribute("/A_SOURCE_UPLOAD_DEFINITION_OID");
				poLineItemBeneficiaryName = poLineItemDoc
						.getAttribute("/BEN_NAME");
				poLineItemCurrency = poLineItemDoc
						.getAttribute("/CURRENCY");
			}

			if (!hasPOLineItems && (!(isReadOnly || isFromExpress))) {
	%>
		<%-- Removed since RQA doesn't support  --%>
		<%-- <div class="formItem">
		<button data-dojo-type="dijit.form.Button"  name="AddPOLineItems" id="AddPOLineItems" type="submit">
	    <%=resMgr.getText("AddAtpAmdPOLineItems.Header",TradePortalConstants.TEXT_BUNDLE)%>
		    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		         setButtonPressed('AddPOLineItems', '0');
		         document.forms[0].submit();
		    </script>
	    </button>
		</div> --%>
	
	
	<%
		// Include currency code so that POs will be filtered
				secureParms.put("addPO-currency", currencyCode);
	%>

	<%
		}
		}
	%>

	<%
		// It is possible that after a transaction has been processed by bank that it has
		// no POs assigned to it because these POs have been moved to amendments.  However,
		// we still want to display the PO information in the PO line items field.
		if (transaction.getAttribute("transaction_status").equals(
				TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK)
				&& !InstrumentServices.isBlank(terms.getFirstShipment()
						.getAttribute("po_line_items")))
			hasPOLineItems = true;

		if (((userOrgAutoLCCreateIndicator != null) && (userOrgAutoLCCreateIndicator
				.equals(TradePortalConstants.INDICATOR_YES)))
				&& hasPOLineItems) {
	%>
	<%=widgetFactory
					.createTextArea(
							"POLineItems",
							"ImportDLCAmend.POLineItems",
							terms.getFirstShipment().getAttribute(
									"po_line_items"),
							isReadOnly,
							false,
							false,
							"onFocus='this.blur();' oncontextmenu='return false;' onselectstart='return false;' rows='10' cols='128'",
							"", "")%>



	
	<%
		if ((SecurityAccess.hasRights(loginRights,
					SecurityAccess.DLC_PROCESS_PO))
					&& (!(isReadOnly || isFromExpress))) {
				secureParms.put("addPO-uploadDefinitionOid",
						poLineItemUploadDefinitionOid);
				secureParms.put("addPO-beneficiaryName",
						poLineItemBeneficiaryName);
				secureParms.put("addPO-currency", poLineItemCurrency);
				secureParms.put("addPO-shipment_oid", terms
						.getFirstShipment().getAttribute("shipment_oid"));
	%>

	<%-- Commented since it is not supported for RQA --%>
	<%-- 	<div class="formItem">
		<button data-dojo-type="dijit.form.Button"  name="AddPOLineItems" id="AddPOLineItems" type="submit">
	    <%=resMgr.getText("AddPOLineItems.Header",TradePortalConstants.TEXT_BUNDLE)%>
		    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		         setButtonPressed('AddPOLineItems', '0');
		         document.forms[0].submit();
		    </script>
	    </button>
	    
	    <button data-dojo-type="dijit.form.Button"  name="RemovePOLineItemsButton" id="RemovePOLineItemsButton" type="submit">
	    <%=resMgr.getText("common.RemovePOLineItemsText",TradePortalConstants.TEXT_BUNDLE)%>
		    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		         setButtonPressed('RemovePOLineItemsButton', '0');
		         document.forms[0].submit();
		    </script>
	    </button>
	    </div>  --%>
	
	
	<%
		}
	%>
	<%
		}
	%>

</div>


<%--
*******************************************************************************
	RQA Amend Page - Other Conditions section

*******************************************************************************
--%>

<%= widgetFactory.createSectionHeader("3", "RequestAdviseAmend.OtherConditions") %>

<%
    
    	 options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
         defaultText = resMgr.getText("transaction.SelectPhrase",
                                      TradePortalConstants.TEXT_BUNDLE);
%>

<%= widgetFactory.createSelectField("AddlDocsPhraseItem", "", defaultText, options, isReadOnly, false, false, 
		"onChange=" + PhraseUtility.getPhraseChange("/Terms/additional_conditions","AddlConditionsText","100","document.forms[0].AddlConditionsText"), "", "") %>
<%= widgetFactory.createTextArea("AddlConditionsText", "", terms.getAttribute("additional_conditions"), isReadOnly,false,false,"rows='10' cols='128'","","") %>
<%if(!isReadOnly) {%>
	<%=widgetFactory.createHoverHelp("AddlConditionsText","AddlConditionsText") %>
 	<%} %>

 </div>      
<%--
*******************************************************************************
               RQA Amend Page - Bank Instructions section

*******************************************************************************
--%>
<%= widgetFactory.createSectionHeader("4", "RequestAdviseAmend.ClientBankInstructions") %>

<%
    
    	 options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
         defaultText = resMgr.getText("transaction.SelectPhrase",
                                      TradePortalConstants.TEXT_BUNDLE);
%>

<%= widgetFactory.createSelectField("CommInvPhraseItem", "", defaultText, options, isReadOnly, false, false, 
		"onChange=" + PhraseUtility.getPhraseChange("/Terms/special_bank_instructions","SpclBankInstructions","1000","document.forms[0].SpclBankInstructions"), "", "") %>
<%= widgetFactory.createTextArea("SpclBankInstructions", "", terms.getAttribute("special_bank_instructions"), isReadOnly,false,false,"rows='10' cols='128'","","") %>
<%if(!isReadOnly) {%>
<%=widgetFactory.createHoverHelp("SpclBankInstructions","SpclBankInstructions") %>
<%} %>
 </div>   
        
<script LANGUAGE="JavaScript">

  function clearAmountTolerances()
  {
    document.forms[0].AmountTolerancePlus.value = "";
	document.forms[0].AmountToleranceMinus.value = "";
  }
  
</script>
