<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Request to Advise Issue Page - Bank Instructions section

  Description:
    Contains HTML to create the Request to Advise Issue Bank Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-RQA-ISS-BankInstructions.jsp" %>
*******************************************************************************
--%>
<script LANGUAGE="JavaScript">
function enableFinanceDrawing(){
		var FinanceDrawingNumDays= dijit.byId("FinanceDrawingNumDays").value;
		if((isNaN(FinanceDrawingNumDays)==false) ){
		dijit.byId("FinanceDrawing").set('checked',true);	
				}
}
</script>

<%
// Get the formatted FEC amount and Rate 

String displayFECAmount;
String displayFECRate;

if(!getDataFromDoc)
   {
     displayFECAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_amount"), 
                                                terms.getAttribute("amount_currency_code"), loginLocale);
     displayFECRate   = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_rate"), 
                                                "", loginLocale);
   }
  else
   {
     displayFECAmount = terms.getAttribute("fec_amount");
     displayFECRate   = terms.getAttribute("fec_rate");
   }

  String regExpRange = "";
  regExpRange = "[0-9]{0,5}([.][0-9]{0,8})?";
  regExpRange = "regExp:'" + regExpRange + "'"; 
%>


	 
		
		<%
		options = Dropdown.createSortedRefDataOptions(
              TradePortalConstants.INSTRUMENT_LANGUAGE, 
              instrument.getAttribute("language"), 
              loginLocale);
		%>

		<%= widgetFactory.createSelectField( "InstrumentLanguage", "RequestAdviseIssue.IssueInstrumentIn", "", options, isReadOnly, true, false, "", "", "") %>
		
		
		<%=widgetFactory.createLabel("","RequestAdviseIssue.InstructionsText",false, false,isExpressTemplate,"","")%>
		<%
      if (isReadOnly || isFromExpress) {
      	out.println("&nbsp;");
      } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
      	defaultText1 = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);
		%>

		<%= widgetFactory.createSelectField( "CommInvPhraseItem1", "", defaultText1, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                "/Terms/special_bank_instructions",
                "SpclBankInstructions",
                "1000","document.forms[0].SpclBankInstructions"), "", "") %>
		
		<%
        }
		%>
		<%= widgetFactory.createTextArea( "SpclBankInstructions", "", terms.getAttribute("special_bank_instructions").toString(), isReadOnly || isFromExpress,false,false, "rows='10' cols='128'","","") %>
		<%if(!isReadOnly){ %>					
			<%= widgetFactory.createHoverHelp("SpclBankInstructions","InstrumentIssue.PlaceHolderSpecialBankInstr2")%> 
		<%} %>
		<div class="columnLeft"> 
		
				<%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.SettleInstructions",true, false, false,"" ) %>
			
				<%= widgetFactory.createTextField( "SettleOurAcct", "RequestAdviseIssue.DebitOurAcct", terms.getAttribute("settlement_our_account_num").toString(), "30", isReadOnly ) %>
				
				<%= widgetFactory.createTextField( "BranchCode", "RequestAdviseIssue.BranchCode", terms.getAttribute("branch_code").toString(), "30", isReadOnly) %>
				
				<%= widgetFactory.createTextField( "SettleForeignAcct", "RequestAdviseIssue.DebitForeignAcct", terms.getAttribute("settlement_foreign_acct_num").toString(), "30", isReadOnly) %>
				
				<% options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("settlement_foreign_acct_curr"), loginLocale);
				%>
      

		  		<%=widgetFactory.createSelectField("SettleForeignCcy","RequestAdviseIssue.AcctCcy", " ", options, isReadOnly, false,false, "style=\"width: 50px;\"", "", "inline")%>		  				
  				<div style="clear:both;"></div>
		</div>
		
		<div class="columnRight"> 
		
				<%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.CommAndChrg",true, false, false, "" ) %>
				
				<%= widgetFactory.createTextField( "CandCOurAcct", "RequestAdviseIssue.DebitOurAcct", terms.getAttribute("coms_chrgs_our_account_num").toString(), "30", isReadOnly) %>
				
				<%= widgetFactory.createTextField( "CandCForeignAcct", "RequestAdviseIssue.DebitForeignAcct", terms.getAttribute("coms_chrgs_foreign_acct_num").toString(), "30", isReadOnly ) %>
				
				<% options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("coms_chrgs_foreign_acct_curr"), loginLocale);
				%>
       
				<%=widgetFactory.createSelectField("CandCForeignCcy","RequestAdviseIssue.AcctCcy", " ", options, isReadOnly, false,false, "style=\"width: 50px;\"", "", "inline")%>		
  				
  				<div style="clear:both;"></div>
				
				
		</div>
		
		<div style="clear:both;"></div>
		<div class="columnLeft">
		<%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.FEC",true, false, false, "" ) %>
				
				<div>
				<%= widgetFactory.createTextField( "FECNumber", "RequestAdviseIssue.FECCovered", terms.getAttribute("covered_by_fec_number").toString(), "14", isReadOnly, false, false,  "class='char15'", "", "inline"  ) %>
				<%= widgetFactory.createTextField( "FECRate", "RequestAdviseIssue.FECRate", displayFECRate, "14", isReadOnly, false, false,  "class='char10'",regExpRange, "inline"  ) %>
				<%-- <%= widgetFactory.createNumberField( "FECRate", "RequestAdviseIssue.FECRate", displayFECRate, "14", isReadOnly, false, false,  "class='char10'", "", "inline"  ) %> --%>
				<div style="clear:both;"></div>
			     </div>
				
			     <div>
				<%= widgetFactory.createAmountField( "FECAmount", "RequestAdviseIssue.FECAmt", displayFECAmount, currency, isReadOnly, false, false, "class='char15'", "readOnly:" +isReadOnly, "inline") %>
				<%= widgetFactory.createDateField( "FECMaturityDate", "RequestAdviseIssue.FECMaturity", StringFunction.xssHtmlToChars(terms.getAttribute("fec_maturity_date").toString()), isReadOnly, false, false,  "class='char10'", dateWidgetOptions, "inline" ) %>
				<div style="clear:both;"></div>
			     </div>
		</div>
		<div class="columnRight">
		
				<%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.FinancingInstructions",true, false, false,"" ) %>
				
				<div class  = "formItem">
				<%= widgetFactory.createCheckboxField( "FinanceDrawing", "RequestAdviseIssue.FinanceDrawing", terms.getAttribute("finance_drawing").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false, "", "", "none"  ) %>
				<%= widgetFactory.createNumberField( "FinanceDrawingNumDays", "", terms.getAttribute("finance_drawing_num_days"), "3", isReadOnly, false, true,  "onChange=\"enableFinanceDrawing();\"", "", "none" ) %>
				<%= widgetFactory.createSubLabel( "RequestAdviseIssue.DaysIn") %>	
				</div>
				<br/>
				<% options =Dropdown.createSortedRefDataOptions(TradePortalConstants.DRAWING_TYPE,
                        terms.getAttribute("finance_drawing_type"),
                        loginLocale);
				%>
				<div  style="margin-left: 50px; margin-top:-2px">
				<%= widgetFactory.createSubLabel( "RequestAdviseIssue.In") %>
				<%= widgetFactory.createSelectField( "FinanceDrawingType", "", "", options, isReadOnly, false, false,  "", "", "none" ) %>
				<%= widgetFactory.createSubLabel( "RequestAdviseIssue.DrawingAt") %>	
				</div>
		</div>
		<div style="clear:both;"></div>
		<% if (isReadOnly || isFromExpress) {
	          out.println("&nbsp;");
	        } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
	          defaultText1 = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE); 
	     %>
		
		<%= widgetFactory.createSelectField( "CommInvPhraseItem2", "RequestAdviseIssue.InstructionsText", defaultText1, options, isReadOnly, false, false,"onChange=" + PhraseUtility.getPhraseChange(
                "/Terms/coms_chrgs_other_text",
                "CandCOtherText",
                "1000","document.forms[0].CandCOtherText"), "", "") %>
		<% 
	        }
		%>
		<%= widgetFactory.createTextArea( "CandCOtherText", "", terms.getAttribute("coms_chrgs_other_text").toString(), isReadOnly,false,false, "rows='10' cols='128'","","") %>
		<%if(!isReadOnly){ %>
			<%= widgetFactory.createHoverHelp("CandCOtherText","InstrumentIssue.PlaceHolderCommissionsChargesInstr")%> 
		<%} %>
