<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Request to Advise Issue Page - Documents Required section

  Description:
    Contains HTML to create the Request to Advise Issue Documents Required section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-RQA-ISS-DocsReqd.jsp" %>
*******************************************************************************
--%>
<%-- Jyoti 23-08-2012 IR2914--%>
<script type="text/javascript">
	<%-- Leelavathi - CR-737 - Start --%>
	function add4moreDocuments(){
		require(["dojo/dom","dojo/domReady!" ], function(dom) {
		  var table = dom.byId('docsReqTable');
		  <%-- get index# for field naming --%>
		  var lastElement = table.rows.length;
		  <%-- Leelavathi IR#T36000015469 CR-737 Rel-8.2 05/14/2013 Begin --%>
		  for (i=lastElement-1;i<lastElement+3;i++) {
			  <%-- Leelavathi IR#T36000015469 CR-737 Rel-8.2 05/14/2013 End --%>
			  <%-- add the table row --%>
			  var newRow = table.insertRow(i);
			  newRow.id = "AddReqDoc"+i;
			  var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
			  var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
			  var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
			  var cell3 = newRow.insertCell(3);<%-- Fourth  cell in the row --%>
			  var cell4 = newRow.insertCell(4);<%-- Fifth  cell in the row --%>
			  j = i-1;
			  var name = "AdditionalReqDocOid"+j;
			  cell0.innerHTML ='<input data-dojo-type=\"dijit.form.CheckBox\" name=\"AdditionalReqDocInd'+j+'\" id=\"AdditionalReqDocInd'+j+'\" value=\'Y\'"\">'
			<%--  Leelavathi IR#T36000014355 Rel-8.2 03/05/2013 Begin   --%>
			  <%-- Leelavathi Rel-8.2 IR#T36000010994 03/08/2013 Begin --%>
		  <%-- cell1.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"AdditionalReqDocName'+j+'\" id=\"AdditionalReqDocName'+j+'\" class=\'char12\' onChange=\"enableAddReqDocs('+j+')\" maxLength=\"15\">' --%>
		  cell1.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"AdditionalReqDocName'+j+'\" id=\"AdditionalReqDocName'+j+'\" class=\'char12\' onChange=\"enableAddReqDocs('+j+')\" maxLength=\"30\">'
		<%-- Leelavathi Rel-8.2 IR#T36000010994 03/08/2013 End --%>
			<%-- Leelavathi Rel-8.2 IR#T36000014307 05/02/2013 Begin --%>
			<%-- cell2.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"AdditionalReqDocOriginals'+j+'\" id=\"AdditionalReqDocOriginals'+j+'\" class=\'char3\'  >' --%>
				<%--   cell3.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"AdditionalReqDocCopies'+j+'\" id=\"AdditionalReqDocCopies'+j+'\" class=\'char3\'  >'  --%>
			  cell2.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"AdditionalReqDocOriginals'+j+'\" id=\"AdditionalReqDocOriginals'+j+'\" class=\'char3\' onChange=\"enableAddReqDocs('+j+')\" style=\'width: 46px;\' >'
			  cell3.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"AdditionalReqDocCopies'+j+'\" id=\"AdditionalReqDocCopies'+j+'\" class=\'char3\' onChange=\"enableAddReqDocs('+j+')\" style=\'width: 46px;\' >'
			<%-- Leelavathi Rel-8.2 IR#T36000014307 05/02/2013 End --%>
					<%-- Leelavathi Rel-8.2 IR#T36000010992 11/03/2013 Begin				 --%>
				<%--  var html="<select data-dojo-type='t360.widget.FilteringSelect' name='AdditionalReqDocPhraseItem"+j+"' id='AdditionalReqDocPhraseItem"+j+"' class='formItem' style='width: 150px;' onChange='setPhraseLookupFields(this,\"/Terms/AdditionalReqDocList/addl_req_doc_text"+j+"\", \"AdditionalReqDocText"+j+"\",\"1000\",document.forms[0].AdditionalReqDocText"+j+");'></select>"
			+ '<textarea data-dojo-type=\"dijit.form.SimpleTextarea\" name=\"AdditionalReqDocText'+j+'\" id=\"AdditionalReqDocText'+j+'\" class=\'char1000 onChange=\"enableAddReqDocs('+j+')\" formItem\' style=\'width:auto;\' rows=\'2\' cols=\'60\' >'
			+ '</textarea> <INPUT TYPE=HIDDEN NAME=\"'+ name + '\" VALUE=\"\">' --%>
			<%--   Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin  --%>
				var html="<select data-dojo-type='t360.widget.FilteringSelect' name='AdditionalReqDocPhraseItem"+j+"' id='AdditionalReqDocPhraseItem"+j+"' class='formItem' style=\'width: 165px;\' onChange='setPhraseLookupFields(this,\"/Terms/AdditionalReqDocList/addl_req_doc_text"+j+"\", \"AdditionalReqDocText"+j+"\",\"1000\",document.forms[0].AdditionalReqDocText"+j+");'></select>"
				+ '<textarea data-dojo-type=\"dijit.form.SimpleTextarea\" name=\"AdditionalReqDocText'+j+'\" id=\"AdditionalReqDocText'+j+'\" class=\'char1000 onChange=\"enableAddReqDocs('+j+')\" formItem\' style=\'width:auto;\' rows=\'3\' cols=\'60\' >'
				+ '</textarea> <INPUT TYPE=HIDDEN NAME=\"'+ name + '\" VALUE=\"\">'
				<%--   Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin  --%>
				<%--  Leelavathi IR#T36000014355 Rel-8.2 03/05/2013 End --%>
				<%-- Leelavathi Rel-8.2 IR#T36000010992 11/03/2013 End	 --%>
			  cell4.innerHTML	= html;						
		      require(["dojo/parser", "dijit/registry"], function(parser, registry) {
		             parser.parse(newRow.id);
		             
		           	 var selAddReqDocPhrase = registry.byId("OtherDoc4PhraseItem");
		             if(selAddReqDocPhrase!=null){
		               var addReqDocPhrase = selAddReqDocPhrase.store.data;
		               for(var i=0; i<addReqDocPhrase.length;i++){
		                      registry.byId('AdditionalReqDocPhraseItem'+(j)).store.data.push({name:addReqDocPhrase[i].name,value:addReqDocPhrase[i].value, id:addReqDocPhrase[i].value});
		               } 
		               registry.byId('AdditionalReqDocPhraseItem'+(j)).attr('displayedValue', "<Select a Phrase>");
		           	}
			    });
		  }
		  <%-- Leelavathi IR#T36000015469 CR-737 Rel-8.2 05/14/2013 Begin --%>
		  document.getElementById("noOfAddDocsReqRows").value = eval(lastElement+3);
		  <%-- Leelavathi IR#T36000015469 CR-737 Rel-8.2 05/14/2013 End --%>
	});
	}
	<%-- Leelavathi - CR-737 - End --%>
	function enableCommInv(){
		var CommInvOrigs1 = dijit.byId("CommInvOrigs").value;
		var CommInvCopies1 = dijit.byId("CommInvCopies").value;
		var CommInvText1 = dijit.byId("CommInvText").value;
		if((CommInvOrigs1 > '') || (CommInvCopies1 > '') || CommInvText1 > ''){
		dijit.byId("CommInvInd").set('checked',true);
		}
	}	
	function enablePackList(){
		var PackListOrigs1 = dijit.byId("PackListOrigs").value;
		var PackListCopies1 = dijit.byId("PackListCopies").value;
		var PackListText1 = dijit.byId("PackListText").value;
		if((PackListOrigs1 > '') || (PackListCopies1 > '') || PackListText1 > ''){
		dijit.byId("PackListInd").set('checked',true);
		}		
	}
	function enableCertOrigin(){
		var CertOriginOrigs1 = dijit.byId("CertOriginOrigs").value;
		var CertOriginCopies1 = dijit.byId("CertOriginCopies").value;
		var CertOriginText1 = dijit.byId("CertOriginText").value;		
		if((CertOriginOrigs1 > '') || (CertOriginCopies1 > '') || CertOriginText1 > ''){
		dijit.byId("CertOriginInd").set('checked',true);
		}
	}
	function enableInsPolicy(){
		var InsPolicyOrigs1= dijit.byId("InsPolicyOrigs").value;
		var InsPolicyCopies1= dijit.byId("InsPolicyCopies").value;
		var InvoicePlusValue1= dijit.byId("InvoicePlusValue").value;
		var InsPolicyText1= dijit.byId("InsPolicyText").value;
		if((InsPolicyOrigs1 > '') || (InsPolicyCopies1 > '') || InvoicePlusValue1 > ''){
		dijit.byId("InsPolicyInd").set('checked',true);	
		}
	}
	function enableOtherDoc1(){
		var OtherDoc1Name1 = dijit.byId("OtherDoc1Name").value;
		var OtherDoc1Origs1= dijit.byId("OtherDoc1Origs").value;
		var OtherDoc1Copies1= dijit.byId("OtherDoc1Copies").value;
		var OtherDoc1Text1= dijit.byId("OtherDoc1Text").value;
		if((OtherDoc1Origs1 > '') || (OtherDoc1Copies1 > '') || OtherDoc1Text1 > '' || OtherDoc1Name1 > ''){
		dijit.byId("OtherDoc1Ind").set('checked',true);	
		}
	}
	function enableOtherDoc2(){
			var OtherDoc2Name2 = dijit.byId("OtherDoc2Name").value;
			var OtherDoc2Origs2= dijit.byId("OtherDoc2Origs").value;
			var OtherDoc2Copies2= dijit.byId("OtherDoc2Copies").value;
			var OtherDoc2Text2= dijit.byId("OtherDoc2Text").value;
			if((OtherDoc2Origs2 > '') || (OtherDoc2Copies2 > '') || OtherDoc2Text2 > '' || OtherDoc2Name2 > ''){
			dijit.byId("OtherDoc2Ind").set('checked',true);	
			}
	}	
	function enableOtherDoc3(){
			var OtherDoc3Name3 = dijit.byId("OtherDoc3Name").value;
			var OtherDoc3Origs3= dijit.byId("OtherDoc3Origs").value;
			var OtherDoc3Copies3= dijit.byId("OtherDoc3Copies").value;
			var OtherDoc3Text3= dijit.byId("OtherDoc3Text").value;
			if((OtherDoc3Origs3 > '') || (OtherDoc3Copies3 > '') || OtherDoc3Text3 > '' || OtherDoc3Name3 > ''){
			dijit.byId("OtherDoc3Ind").set('checked',true);	
			}
	}	
	function enableOtherDoc4(){
			var OtherDoc4Name4 = dijit.byId("OtherDoc4Name").value;
			var OtherDoc4Origs4= dijit.byId("OtherDoc4Origs").value;
			var OtherDoc4Copies4= dijit.byId("OtherDoc4Copies").value;
			var OtherDoc4Text4= dijit.byId("OtherDoc4Text").value;
			if((OtherDoc4Origs4 > '') || (OtherDoc4Copies4 > '') || OtherDoc4Text4>'' || OtherDoc4Name4 > ''){
			dijit.byId("OtherDoc4Ind").set('checked',true);	
			}
	}
	<%--  Leelavathi IR#T36000014355 Rel-8.2 03/05/2013 Begin  --%>
	function enableAddReqDocs(index){
		var AddReqDocName = dijit.byId("AdditionalReqDocName"+index).value;
		var AddReqDocOrigs = dijit.byId("AdditionalReqDocOriginals"+index).value;
		var AddReqDocCopies= dijit.byId("AdditionalReqDocCopies"+index).value;
		var AddReqDocText = dijit.byId("AdditionalReqDocText"+index).value;
		if((AddReqDocOrigs > '') || (AddReqDocCopies > '') || AddReqDocText>'' || AddReqDocName > ''){
			dijit.byId("AdditionalReqDocInd"+index).set('checked',true);	
		}
}	
	<%--  Leelavathi IR#T36000014355 Rel-8.2 03/05/2013 End   --%>
	function enableAddlDocs(){
			var AddlDocsText= dijit.byId("AddlDocsText").value;
			if(AddlDocsText > ''){
			dijit.byId("AddlDocsInd").set('checked',true);	
			}
	}
	
	
</script>
<%-- IR2914 Ends--%>
<%
String displayInsuranceEndorseValuePlus;

// Don't format the amount if we are reading data from the doc
if(!getDataFromDoc)
   displayInsuranceEndorseValuePlus = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("insurance_endorse_value_plus"), "", loginLocale);
else
   displayInsuranceEndorseValuePlus = terms.getAttribute("insurance_endorse_value_plus");
%>


<%--Leelavathi - 10thDec2012 - Rel8200 CR-737 -Start--%>
<input type=hidden value=<%=addReqDocRowCount%> name="numberOfMultipleObjects2"   id="noOfAddDocsReqRows"> 
<table id="docsReqTable" class="formDocumentsTable" >
<%--Leelavathi - 10thDec2012 - Rel8200 CR-737 - End --%>
<thead>
	<tr>
		<th>&nbsp;</th>
		<th class="genericCol"><%=resMgr.getText("RequestAdviseIssue.DocType",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol"><%=resMgr.getText("RequestAdviseIssue.Originals",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol"><%=resMgr.getText("RequestAdviseIssue.Copies",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol"><%=resMgr.getText("RequestAdviseIssue.Description",TradePortalConstants.TEXT_BUNDLE)%></th>
	</tr>
</thead>

<tbody>
	<tr>
	<td><%=widgetFactory.createCheckboxField("CommInvInd", "",terms.getAttribute("comm_invoice_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
	<td><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%>
	<%= widgetFactory.createSubLabel("RequestAdviseIssue.CommercialInv")%></td>
	<td><%= widgetFactory.createNumberField( "CommInvOrigs", "", terms.getAttribute("comm_invoice_originals"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enableCommInv();\"", "","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "CommInvCopies", "", terms.getAttribute("comm_invoice_copies"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enableCommInv();\"", "","none" ) %></td>
	<td>
	<%
		String defaultText1 = "";
      if (!(isReadOnly || isFromExpress)) {
        options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        defaultText1 = resMgr.getText("transaction.SelectPhrase",
                                     TradePortalConstants.TEXT_BUNDLE);
  %>
	<div class="formItem">
  	<%= widgetFactory.createSelectField( "CommInvPhraseItem", "", defaultText1, options, isReadOnly || isFromExpress, false, false,  "class='char12' onChange=" + PhraseUtility.getPhraseChange(
            "/Terms/comm_invoice_text","CommInvText", "1000","document.forms[0].CommInvText"), "", "none") %>
	    <%= widgetFactory.createNote("ImportDLCIssue.Includes","normalFontLabel")%>
    </div>
  <% }
  %>	
  <%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin --%>
  <%= widgetFactory.createTextArea( "CommInvText", "", terms.getAttribute("comm_invoice_text"), isReadOnly || isFromExpress,false,false,  "rows='3' cols='60'  onBlur=\"enableCommInv();\"","", "" ) %>
 <%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 End --%>
	</td>
	</tr>
	
	
	<tr>
	<td><%=widgetFactory.createCheckboxField("PackListInd", "",terms.getAttribute("packing_list_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
	<td><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%>
	<%= widgetFactory.createSubLabel("RequestAdviseIssue.PackingList")%></td>
	<td><%= widgetFactory.createNumberField( "PackListOrigs", "", terms.getAttribute("packing_list_originals"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enablePackList();\"", "","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "PackListCopies", "", terms.getAttribute("packing_list_copies"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enablePackList();\"", "","none" ) %></td>
	<td>
	<%
		if (!(isReadOnly || isFromExpress)) {
			options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
			defaultText1 = resMgr.getText("transaction.SelectPhrase",
	                                   TradePortalConstants.TEXT_BUNDLE);
	%> 
    	<%= widgetFactory.createSelectField( "PackListPhraseItem", "", defaultText1, options, isReadOnly || isFromExpress, false, false,  "onChange=" + PhraseUtility.getPhraseChange(
                "/Terms/packing_list_text","PackListText", "1000","document.forms[0].PackListText"), "", "") %>
	<% }
    %>
    <%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin --%>
    <%= widgetFactory.createTextArea( "PackListText", "", terms.getAttribute("packing_list_text"), isReadOnly || isFromExpress,false,false,  "rows='3' cols='60' onBlur=\"enablePackList();\"", "","" ) %>
	<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 End --%>
	</td>
	</tr>
	
	
	<tr>
	<td><%=widgetFactory.createCheckboxField("CertOriginInd", "",terms.getAttribute("cert_origin_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
	<td><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%>
	<%= widgetFactory.createSubLabel("RequestAdviseIssue.OriginCertificate")%></td>
	<td><%= widgetFactory.createNumberField( "CertOriginOrigs", "", terms.getAttribute("cert_origin_originals"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enableCertOrigin();\"", "","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "CertOriginCopies", "", terms.getAttribute("cert_origin_copies"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enableCertOrigin();\"", "","none" ) %></td>
	<td>
	<%
		if (!(isReadOnly || isFromExpress)) {
			options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
			defaultText1 = resMgr.getText("transaction.SelectPhrase",
	                                   TradePortalConstants.TEXT_BUNDLE);
	   
     %> 
     	<%= widgetFactory.createSelectField( "CertOriginPhraseItem", "", defaultText1, options, isReadOnly || isFromExpress, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                "/Terms/cert_origin_text","CertOriginText", "1000","document.forms[0].CertOriginText"), "", "") %>
 	<% }
    %>	
    <%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin --%>
    <%= widgetFactory.createTextArea( "CertOriginText", "", terms.getAttribute("cert_origin_text"), isReadOnly || isFromExpress,false, false, "rows='3' cols='60' onBlur=\"enableCertOrigin();\"", "","" ) %>
	<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 End --%>
	</td>
	</tr>
	
	
	<tr>
	<td><%=widgetFactory.createCheckboxField("InsPolicyInd", "",terms.getAttribute("ins_policy_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
	<td><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%>
	<%= widgetFactory.createSubLabel("RequestAdviseIssue.InsPolicyCert")%></td>
	<td><%= widgetFactory.createNumberField( "InsPolicyOrigs", "", terms.getAttribute("ins_policy_originals"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enableInsPolicy();\"", "","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "InsPolicyCopies", "", terms.getAttribute("ins_policy_copies"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enableInsPolicy();\"", "","none" ) %></td>
	<td>
	<div class = "formItem">
	<% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%>
	<%= widgetFactory.createNote( "RequestAdviseIssue.EndorsedInBlank","normalFontLabel") %>
	<%= widgetFactory.createPercentField( "InvoicePlusValue", "", displayInsuranceEndorseValuePlus, isReadOnly || isFromExpress, false, false,  "", "","none" ) %>
	<%if(!isReadOnly){ %>
	<%= widgetFactory.createHoverHelp("InvoicePlusValue", "RequestAdviseIssue.EndorsedInBlank" )%>
	<%} %>
	<%= widgetFactory.createNote( "RequestAdviseIssue.Covering","normalFontLabel") %>
	<div style="clear:both;"></div>
	</div>
	<% options = Dropdown.createSortedRefDataOptions(
          TradePortalConstants.INSURANCE_RISK_TYPE, 
          terms.getAttribute("insurance_risk_type"), 
          loginLocale);
	%>
	<%= widgetFactory.createSelectField( "InsRiskType", "", " ", options, isReadOnly || isFromExpress, false, isExpressTemplate,  "class='char35'", "", "") %>
	<%-- Leelavathi IR#IR#T36000011070 Rel-8.2 03/18/2013 Begin --%>
	<%--<%
	if (isReadOnly || isFromExpress) {
      out.println("&nbsp;");
    } else {
      options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
      defaultText1 = resMgr.getText("transaction.SelectPhrase",
                                   TradePortalConstants.TEXT_BUNDLE);
    }  
    %>
	--%>
	<%
	if (isReadOnly || isFromExpress) {
		options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
    } else if (!(isReadOnly || isFromExpress)) {
      options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
      defaultText1 = resMgr.getText("transaction.SelectPhrase",
                                   TradePortalConstants.TEXT_BUNDLE);
      %> 
	<%= widgetFactory.createSelectField( "InsPolicyPhraseItem", "", defaultText1, options, isReadOnly || isFromExpress, false, false,  "style='width:11em;' onChange=" + PhraseUtility.getPhraseChange(
            "/Terms/ins_policy_text","InsPolicyText", "1000","document.forms[0].InsPolicyText"), "", "inline") %>
        <%}
    %> 
    <%-- Leelavathi IR#IR#T36000011070 Rel-8.2 03/18/2013 End--%>
    <%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin --%>
    <%= widgetFactory.createTextArea( "InsPolicyText", "", terms.getAttribute("ins_policy_text"), isReadOnly || isFromExpress,false, false, "rows='3' cols='27' onBlur=\"enableInsPolicy();\"", "","inline" ) %>
    <%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 End --%>
    <div style="clear:both;"></div>
    
    </td>
	</tr>
	
	<% 
	String hashClass = "";
	if(isExpressTemplate){
			hashClass="style='width: 12em;'";
			}
			%>
	<tr>
	<td><%=widgetFactory.createCheckboxField("OtherDoc1Ind", "",terms.getAttribute("other_req_doc_1_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
	<td><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%><%= widgetFactory.createTextField( "OtherDoc1Name", "", terms.getAttribute("other_req_doc_1_name"), "30", isReadOnly || isFromExpress, false, false,  hashClass+" onChange=\"enableOtherDoc1();\" class='char15'", "placeHolder:'"+resMgr.getTextEscapedJS("ImportDLCIssue.DocName",TradePortalConstants.TEXT_BUNDLE)+"'","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "OtherDoc1Origs", "", terms.getAttribute("other_req_doc_1_originals"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enableOtherDoc1();\"", "","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "OtherDoc1Copies", "", terms.getAttribute("other_req_doc_1_copies"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enableOtherDoc1();\"", "","none" ) %></td>
	<td>
	<%-- Leelavathi IR#T36000011070 Rel-8.2 03/18/2013 Begin --%>
	<%--
	<%
	if (isReadOnly || isFromExpress) {
      out.println("&nbsp;");
    } else {
      options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
      defaultText1 = resMgr.getText("transaction.SelectPhrase",
                                   TradePortalConstants.TEXT_BUNDLE);
    }  
	%> --%>
	<%
		if (isReadOnly || isFromExpress) {
			options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
		}else if (!(isReadOnly || isFromExpress)) {
		
			options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
			defaultText1 = resMgr.getText("transaction.SelectPhrase",
	                                   TradePortalConstants.TEXT_BUNDLE);
	%>  
    <%= widgetFactory.createSelectField( "OtherDoc1PhraseItem", "", defaultText1, options, isReadOnly || isFromExpress, false, false,  "onChange=" + PhraseUtility.getPhraseChange(
            "/Terms/other_req_doc_1_text","OtherDoc1Text", "1000","document.forms[0].OtherDoc1Text"), "", "") %>
    <% }
	%>
	<%-- Leelavathi IR#T36000011070 Rel-8.2 03/18/2013 End --%>
	<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin --%>
    <%= widgetFactory.createTextArea( "OtherDoc1Text", "", terms.getAttribute("other_req_doc_1_text"), isReadOnly || isFromExpress,false, false, "rows='3' cols='60' onBlur=\"enableOtherDoc1();\"", "","" ) %>
	<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 End --%>
	</td>
	</tr>
	
	<tr>
	<td><%=widgetFactory.createCheckboxField("OtherDoc2Ind", "",terms.getAttribute("other_req_doc_2_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
	<td><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%><%= widgetFactory.createTextField( "OtherDoc2Name", "", terms.getAttribute("other_req_doc_2_name"), "30", isReadOnly || isFromExpress, false, false, hashClass+" onChange=\"enableOtherDoc2();\" class='char15'", "placeHolder:'"+resMgr.getTextEscapedJS("ImportDLCIssue.DocName",TradePortalConstants.TEXT_BUNDLE)+"'","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "OtherDoc2Origs", "", terms.getAttribute("other_req_doc_2_originals"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enableOtherDoc2();\"", "","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "OtherDoc2Copies", "", terms.getAttribute("other_req_doc_2_copies"), "3", isReadOnly || isFromExpress, false, false,  "style='width:46px;' onChange=\"enableOtherDoc2();\"", "","none" ) %></td>
	<td>
	<%-- Leelavathi IR#T36000011070 Rel-8.2 03/18/2013 Begin --%>
	<%--
	<%
	if (isReadOnly || isFromExpress) {
      out.println("&nbsp;");
    } else {
      options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
      defaultText1 = resMgr.getText("transaction.SelectPhrase",
                                   TradePortalConstants.TEXT_BUNDLE);
    }  
	%> --%>
	<%
		if (isReadOnly || isFromExpress) {
			options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
		}else if (!(isReadOnly || isFromExpress)) {
		
			options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
			defaultText1 = resMgr.getText("transaction.SelectPhrase",
	                                   TradePortalConstants.TEXT_BUNDLE);
	%>  
    <%= widgetFactory.createSelectField( "OtherDoc2PhraseItem", "", defaultText1, options, isReadOnly || isFromExpress, false, false,  "onChange=" + PhraseUtility.getPhraseChange(
            "/Terms/other_req_doc_2_text","OtherDoc2Text", "1000","document.forms[0].OtherDoc2Text"), "", "") %>
    <% }
	%>
	<%-- Leelavathi IR#T36000011070 Rel-8.2 03/18/2013 End --%>
	<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin --%>
    <%= widgetFactory.createTextArea( "OtherDoc2Text", "", terms.getAttribute("other_req_doc_2_text"), isReadOnly || isFromExpress,false, false, "rows='3' cols='60' onBlur=\"enableOtherDoc2();\"", "","" ) %>
	<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 End --%>
	</td>
	</tr>
	
	<tr>
	<td><%=widgetFactory.createCheckboxField("OtherDoc3Ind", "",terms.getAttribute("other_req_doc_3_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
	<td><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%><%= widgetFactory.createTextField( "OtherDoc3Name", "", terms.getAttribute("other_req_doc_3_name"), "30", isReadOnly || isFromExpress, false, false, hashClass+" onChange=\"enableOtherDoc3();\" class='char15'", "placeHolder:'"+resMgr.getTextEscapedJS("ImportDLCIssue.DocName",TradePortalConstants.TEXT_BUNDLE)+"'","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "OtherDoc3Origs", "", terms.getAttribute("other_req_doc_3_originals"), "3", isReadOnly || isFromExpress, false, false, "style='width:46px;' onChange=\"enableOtherDoc3();\"", "","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "OtherDoc3Copies", "", terms.getAttribute("other_req_doc_3_copies"), "3", isReadOnly || isFromExpress, false, false,"style='width:46px;' onChange=\"enableOtherDoc3();\"", "","none" ) %></td>
	<td>
	<%-- Leelavathi IR#T36000011070 Rel-8.2 03/18/2013 Begin --%>
	<%--
	<%
	if (isReadOnly || isFromExpress) {
      out.println("&nbsp;");
    } else {
      options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
      defaultText1 = resMgr.getText("transaction.SelectPhrase",
                                   TradePortalConstants.TEXT_BUNDLE);
    }  
	%> --%>
	<%
		if (isReadOnly || isFromExpress) {
			options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
		}else if (!(isReadOnly || isFromExpress)) {
		
			options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
			defaultText1 = resMgr.getText("transaction.SelectPhrase",
	                                   TradePortalConstants.TEXT_BUNDLE);
	%>  
    <%= widgetFactory.createSelectField( "OtherDoc3PhraseItem", "", defaultText1, options, isReadOnly || isFromExpress, false, false,  "onChange=" + PhraseUtility.getPhraseChange(
            "/Terms/other_req_doc_3_text","OtherDoc3Text", "1000","document.forms[0].OtherDoc3Text"), "", "") %>
    <% }
	%>
	<%-- Leelavathi IR#T36000011070 Rel-8.2 03/18/2013 End --%>
	<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin --%>
    <%= widgetFactory.createTextArea( "OtherDoc3Text", "", terms.getAttribute("other_req_doc_3_text"), isReadOnly || isFromExpress,false, false, "rows='3' cols='60' onBlur=\"enableOtherDoc3();\"", "","" ) %>
	<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 End --%>
	</td>
	</tr>
	
	<tr>
	<td><%=widgetFactory.createCheckboxField("OtherDoc4Ind", "",terms.getAttribute("other_req_doc_4_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
	<td><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%><%= widgetFactory.createTextField( "OtherDoc4Name", "", terms.getAttribute("other_req_doc_4_name"), "30", isReadOnly || isFromExpress, false, false,hashClass+" onChange=\"enableOtherDoc4();\" class='char15'", "placeHolder:'"+resMgr.getTextEscapedJS("ImportDLCIssue.DocName",TradePortalConstants.TEXT_BUNDLE)+"'","none" ) %></td> 
	<td><%= widgetFactory.createNumberField( "OtherDoc4Origs", "", terms.getAttribute("other_req_doc_4_originals"), "3", isReadOnly || isFromExpress, false, false,"style='width:46px;' onChange=\"enableOtherDoc4();\"", "","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "OtherDoc4Copies", "", terms.getAttribute("other_req_doc_4_copies"), "3", isReadOnly || isFromExpress, false, false, "style='width:46px;' onChange=\"enableOtherDoc4();\"", "","none" ) %></td>
	<td>
	<%-- Leelavathi IR#T36000011070 Rel-8.2 03/18/2013 Begin --%>
	<%--
	<%
	if (isReadOnly || isFromExpress) {
      out.println("&nbsp;");
    } else {
      options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
      defaultText1 = resMgr.getText("transaction.SelectPhrase",
                                   TradePortalConstants.TEXT_BUNDLE);
    }  
	%> --%>
	<%
		if (isReadOnly || isFromExpress) {
			options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
		}else if (!(isReadOnly || isFromExpress)) {
		
			options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
			defaultText1 = resMgr.getText("transaction.SelectPhrase",
	                                   TradePortalConstants.TEXT_BUNDLE);
	%>  
    <%= widgetFactory.createSelectField( "OtherDoc4PhraseItem", "", defaultText1, options, isReadOnly || isFromExpress, false, false,  "onChange=" + PhraseUtility.getPhraseChange(
            "/Terms/other_req_doc_4_text","OtherDoc4Text", "1000","document.forms[0].OtherDoc4Text"), "", "") %>
    <% }
	%>
	<%-- Leelavathi IR#T36000011070 Rel-8.2 03/18/2013 End --%>
	<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin --%>
    <%= widgetFactory.createTextArea( "OtherDoc4Text", "", terms.getAttribute("other_req_doc_4_text"), isReadOnly || isFromExpress,false, false, "rows='3' cols='60' onBlur=\"enableOtherDoc4();\"", "","" ) %>
	<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 End --%>
	</td>
	</tr>
	
	
	<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start --%>
	<%
			for (iLoop = 0; iLoop < addReqDocList.size(); iLoop++) {
				addReqDoc = (AdditionalReqDocWebBean) addReqDocList.get(iLoop);
		%>
		<tr>
			<td><%=widgetFactory.createCheckboxField("AdditionalReqDocInd"+iLoop, "",TradePortalConstants.INDICATOR_YES.equals(addReqDoc.getAttribute("addl_req_doc_ind")), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
			<%-- Leelavathi IR#T36000010992 Rel-8.2 02/05/2013 Begin  --%>
			<%-- Leelavathi IR#T36000014355 Rel-8.2 03/05/2013 Begin  --%>
			<%-- <td><%= widgetFactory.createTextField( "AdditionalReqDocName"+iLoop, "", addReqDoc.getAttribute("addl_req_doc_name"), "15", isReadOnly || isFromExpress, false, false,"", "","none" ) %></td>  --%>
			<%-- Leelavathi IR#T36000015099 Rel-8.2 03/22/2013 Begin  --%>
			<%-- <td><%= widgetFactory.createTextField( "AdditionalReqDocName"+iLoop, "", addReqDoc.getAttribute("addl_req_doc_name"), "12", isReadOnly || isFromExpress, false, false, hashClass+" onChange=\"enableAddReqDocs("+iLoop+");\" class='char15'", "","none" ) %></td>--%>
			<td><%= widgetFactory.createTextField( "AdditionalReqDocName"+iLoop, "", addReqDoc.getAttribute("addl_req_doc_name"), "30", isReadOnly || isFromExpress, false, false, hashClass+" onChange=\"enableAddReqDocs("+iLoop+");\" class='char15'", "","none" ) %></td>
			<%-- Leelavathi IR#T36000015099 Rel-8.2 03/22/2013 End  --%>
			<%-- Leelavathi IR#T36000010992 Rel-8.2 02/05/2013 Begin --%>
			<%-- Leelavathi Rel-8.2 IR#T36000014307 05/02/2013 Begin --%>
			<td><%= widgetFactory.createNumberField( "AdditionalReqDocOriginals"+iLoop, "", addReqDoc.getAttribute("addl_req_doc_originals"), "3", isReadOnly || isFromExpress, false, false, "style='width:46px;' onChange=\"enableAddReqDocs("+iLoop+");\"", "","none" ) %></td>
			<td><%= widgetFactory.createNumberField( "AdditionalReqDocCopies"+iLoop, "",addReqDoc.getAttribute("addl_req_doc_copies"), "3", isReadOnly || isFromExpress, false, false, "style='width:46px;' onChange=\"enableAddReqDocs("+iLoop+");\"", "","none" ) %></td>
			<%-- Leelavathi Rel-8.2 IR#T36000014307 05/02/2013 End --%>
			<%-- Leelavathi IR#T36000010992 Rel-8.2 03/11/2013 Begin  --%>
			<%-- <td>
			<%
				if (isReadOnly || isFromExpress) {
			      out.println("&nbsp;");
			    } else {
			    	phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
					if (phraseLists == null) phraseLists = new DocumentHandler();
		
				  reqdDocList = phraseLists.getFragment("/Required");
			      options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "");
			      defaultText1 = resMgr.getText("transaction.SelectPhrase",loginLocale);
			      %>
			        Leelavathi IR#T36000010992 Rel-8.2 02/05/2013 Begin  
			      
			       }  
		   
			<%= widgetFactory.createSelectField( "AdditionalReqDocPhraseItem"+iLoop, "", defaultText1, options, false, false, false, "", "", "") %>
		    <%= widgetFactory.createTextArea( "AdditionalReqDocText"+iLoop, "", addReqDoc.getAttribute("addl_req_doc_text"), false,false, false, "", "","" ) %>
			</td> 
		
		 
			      
			      <%= widgetFactory.createSelectField( "AdditionalReqDocPhraseItem"+iLoop, "", defaultText1, options, false, false, false, "", "", "") %>
			   <%  }  
		    %> 
			
		    <%= widgetFactory.createTextArea( "AdditionalReqDocText"+iLoop, "", addReqDoc.getAttribute("addl_req_doc_text"), false,false, false,  "rows='2' cols='60' onChange=\"enableAddReqDocs("+iLoop+");\"", "","" ) %>
			</td>--%>
			<td>
			<%
			  phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
			  if (phraseLists == null) phraseLists = new DocumentHandler();
			  reqdDocList = phraseLists.getFragment("/Required");
		      options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "");
		      defaultText1 = resMgr.getText("transaction.SelectPhrase",TradePortalConstants.TEXT_BUNDLE);
			%>
			<%= widgetFactory.createSelectField( "AdditionalReqDocPhraseItem"+iLoop, "", defaultText1, options, isReadOnly || isFromExpress, false, false, "", "", "") %>
			<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 Begin --%>
		    <%= widgetFactory.createTextArea( "AdditionalReqDocText"+iLoop, "", addReqDoc.getAttribute("addl_req_doc_text"), false,false, false,  "rows='3' cols='60' onChange=\"enableAddReqDocs("+iLoop+");\"", "","" ) %>
			<%--  Leelavathi IR#T36000017390 Rel-8.2 31/05/2013 End --%>
			</td>
			<%-- Leelavathi IR#T36000014355 Rel-8.2 03/05/2013 End  --%>
			<%-- Leelavathi IR#T36000010992 Rel-8.2 03/11/2013 End  --%>
			</tr>
	<%
			String additionalReqDocOid = addReqDoc.getAttribute("addl_req_doc_oid");
			out.print("<INPUT TYPE=HIDDEN NAME='AdditionalReqDocOid"+iLoop+"' VALUE='" +additionalReqDocOid+ "'>"); 
		}
	%>
	
	<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - End --%>
	<%-- Leelavathi IR#T36000011015 Rel-8.2 03/22/2013 Begin --%>
	<tr>
	<td><%=widgetFactory.createCheckboxField("AddlDocsInd", "",terms.getAttribute("addl_doc_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
	<td><% if(isExpressTemplate){%>
		<span class='hashMark'>#</span>
	<%}%><%= widgetFactory.createSubLabel("RequestAdviseIssue.AddlDocs1")%></td>
	<td colspan="3">
	<%
		if (isReadOnly || isFromExpress) {
	      out.println("&nbsp;");
	    } else {
	      options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
	      defaultText1 = resMgr.getText("transaction.SelectPhrase",
	                                   TradePortalConstants.TEXT_BUNDLE);
	    }  
    %>  
    <div  class = "formItem">
    <%= widgetFactory.createSelectField( "AddlDocsPhraseItem", "", defaultText1, options, isReadOnly || isFromExpress, false, false,  "style='width:11em;' onChange=" + PhraseUtility.getPhraseChange(
            "/Terms/addl_doc_text","AddlDocsText", "1000","document.forms[0].AddlDocsText"), "", "none") %>
 <%= widgetFactory.createNote("RequestAdviseIssue.SpecifyNumber","normalFontLabel")%>
    </div>
    <%-- Leelavathi IR#T36000017390 05/23/2013 Begin --%>
    <%= widgetFactory.createTextArea( "AddlDocsText", "", terms.getAttribute("addl_doc_text"), isReadOnly || isFromExpress,false, false, "rows='3' cols='82' onBlur=\"enableAddlDocs();\"", "","" ) %>
    <%-- Leelavathi IR#T36000017390 05/23/2013 End --%>
	</td>
	</tr>
	<%-- Leelavathi IR#T36000011015 Rel-8.2 03/22/2013 End --%>
</tbody>
</table>
<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start --%>
	<div>
	  <% if (!(isReadOnly)) {     %>	 
			<button data-dojo-type="dijit.form.Button" type="button" id="add4moreDocuments">
			<%-- Jyothikumari.G 16/05/2013 Rel8.2 IR-T36000016903 Start --%>
			<%-- I added the below line for the button Add 4 More Documents as per IR-T36000016903 --%>
			<%= resMgr.getText("common.Add4MoreDocuments", TradePortalConstants.TEXT_BUNDLE) %>
			<%-- Jyothikumari.G 16/05/2013 Rel8.2 IR-T36000016903 End --%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt"> add4moreDocuments();	</script>
			</button>	
		<% }else{  %>
				&nbsp;
		<% } %>  		
	 </div>
<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - End --%>	



<br>
