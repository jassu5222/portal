<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Request to Advise Issue Page - General section

  Description:
    Contains HTML to create the Request to Advise Issue General section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-RQA-ISS-General.jsp" %>
*******************************************************************************
--%>

<%
  // Get some attributes for radio button fields.  We need to refer to this
  // value frequently so do getAttribute once.

  String pmtTermsType = terms.getAttribute("pmt_terms_type");
  String draftsRequired = terms.getAttribute("drafts_required");
  //System.out.println("Drafts Required [" + draftsRequired.toString() + "]");
  
  // Setup Amount Tolerance and Maximum Credit Amount radio buttons.
  String amountToleranceType;
  String maximumCreditAmount = terms.getAttribute("maximum_credit_amount");
  String amountTolerancePlus = terms.getAttribute("amt_tolerance_pos");
  String amountToleranceMinus = terms.getAttribute("amt_tolerance_neg");
  if (!InstrumentServices.isBlank(maximumCreditAmount)) {
  	amountToleranceType = TradePortalConstants.NOT_EXCEEDING;
  } else if (!InstrumentServices.isBlank(amountTolerancePlus) ||
  	!InstrumentServices.isBlank(amountToleranceMinus)) {
	amountToleranceType = TradePortalConstants.AMOUNT_TOLERANCE;
  } else { // niether radio button should be checked
  	amountToleranceType = "";
  }
  
  String bankChargesType = terms.getAttribute("bank_charges_type");

  String amount = terms.getAttribute("amount");
  
  String displayAmount;

  // Don't format the amount if we are reading data from the doc
  if(!getDataFromDoc || amountUpdatedFromPOs)
     displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currency, loginLocale);
  else 
     displayAmount = amount;
/*     if(!isReadOnly)
         displayAmount = amount;
      else
    	  displayAmount = TPCurrencyUtility.getDisplayAmount(amount.toString(), currency, loginLocale);
 */   	  
%>

  
		<div class="columnLeft">  
		
		<%
		String designatedIdentStr = ",nab_terms_party_oid,NabName,NabAddressLine1,NabAddressLine2,NabCity,NabStateProvince,NabCountry,NabPostalCode,AdviseThroughBank,BenOTLCustomerId,NabOTLCustomerId,BenUserDefinedField1,BenUserDefinedField2,BenUserDefinedField3,BenUserDefinedField4";
		String identifierStr1="BenName,BenAddressLine1,BenAddressLine2,BenCity,BenStateProvince,BenPostalCode,BenCountry,BenPhoneNumber"+designatedIdentStr;
		String sectionName1="ben_rta";
		String benSearchHtml = widgetFactory.createPartySearchButton(identifierStr1,sectionName1,false,TradePortalConstants.BENEFICIARY,false);
		
		%>
		<% if (!isReadOnly){ %> 
				<%=widgetFactory.createHoverHelp("ben_rta", "PartySearchIconHoverHelp") %>
		<%} %>	
			
		<%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.Beneficiary",(isReadOnly || isFromExpress), !isTemplate, false, benSearchHtml ) %>
		
	<%--	<div class="subsectionShortHeader"><h3>Beneficiary</h3><a name="PartySearch" onClick="SearchParty('<%=identifierStr1%>', '<%=sectionName1%>');"><img class="searchButton" src="/portal/themes/default/images/search_icon.png"></a></div>--%>
		<%			 
		// Always send the ben terms party oid
		secureParms.put("ben_terms_party_oid", termsPartyBen.getAttribute("terms_party_oid"));
		%>
		<div style="display:none">
		<input type=hidden name='BenTermsPartyType' value="<%=TradePortalConstants.BENEFICIARY%>">
		<input type=hidden name="BenOTLCustomerId" value="<%=termsPartyBen.getAttribute("OTL_customer_id")%>">
		<input type=hidden name="VendorId" value="<%=termsPartyBen.getAttribute("vendor_id")%>">
		<input type=hidden name="BenUserDefinedField1"
		     value="<%=termsPartyBen.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="BenUserDefinedField2"
		     value="<%=termsPartyBen.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="BenUserDefinedField3"
		     value="<%=termsPartyBen.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="BenUserDefinedField4"
		     value="<%=termsPartyBen.getAttribute("user_defined_field_4")%>">
		</div>
		<%
		    if (isFromExpress) {
		      // We are in a transaction created from express.  The beneficiary 
		      // fields are express fields.  This means they display as readonly 
		      // when the user is editing an instrument created from an express 
		      // template.  However, we still need to send the ben data to the 
		      // mediator.  Put the ben data in a bunch of hidden fields to 
		      // ensure this happens.
		%>
			 <div style="display:none">
		      <input type=hidden name="BenName" 
		             value="<%=termsPartyBen.getAttribute("name")%>">
		      <input type=hidden name="BenPhoneNumber" 
		             value="<%=termsPartyBen.getAttribute("phone_number")%>">
		      <input type=hidden name="BenAddressLine1"
		             value="<%=termsPartyBen.getAttribute("address_line_1")%>">
		      <input type=hidden name="BenAddressLine2"
		             value="<%=termsPartyBen.getAttribute("address_line_2")%>">
		      <input type=hidden name="BenCity"
		             value="<%=termsPartyBen.getAttribute("address_city")%>">
		      <input type=hidden name="BenStateProvince"
		             value="<%=termsPartyBen.getAttribute("address_state_province")%>">
		      <input type=hidden name="BenCountry"
		             value="<%=termsPartyBen.getAttribute("address_country")%>">
		      <input type=hidden name="BenPostalCode"
		             value="<%=termsPartyBen.getAttribute("address_postal_code")%>">
		      </div>
		<%
			       }
		%>
		
		<%= widgetFactory.createTextField( "BenName", "RequestAdviseIssue.BeneficiaryName", termsPartyBen.getAttribute("name"), "35", isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "onBlur='checkBeneficiaryName(\"" + StringFunction.escapeQuotesforJS(termsPartyBen.getAttribute("name")) + "\")'", "","" ) %>
		
		<%= widgetFactory.createTextField( "BenAddressLine1", "RequestAdviseIssue.AddressLine1", termsPartyBen.getAttribute("address_line_1"), "35", isReadOnly || isFromExpress, !isTemplate, isExpressTemplate,  "", "","" ) %>
		
		<%= widgetFactory.createTextField( "BenAddressLine2", "RequestAdviseIssue.AddressLine2", termsPartyBen.getAttribute("address_line_2"), "35", isReadOnly || isFromExpress, false, isExpressTemplate, "", "",""  ) %>
		
		<%= widgetFactory.createTextField( "BenCity", "RequestAdviseIssue.City", termsPartyBen.getAttribute("address_city"), "23", isReadOnly || isFromExpress, !isTemplate, isExpressTemplate,  "class='char35'", "",""  ) %>
		
		<div>
		<%= widgetFactory.createTextField( "BenStateProvince", "RequestAdviseIssue.ProvinceState", termsPartyBen.getAttribute("address_state_province").toString(), "8", isReadOnly || isFromExpress, false, isExpressTemplate, "class='char10'", "","inline"  ) %>
		<%= widgetFactory.createTextField( "BenPostalCode", "RequestAdviseIssue.PostalCode", termsPartyBen.getAttribute("address_postal_code").toString(), "15", isReadOnly || isFromExpress, false, isExpressTemplate, "class='char10'", "","inline" ) %>
		<div style="clear:both;"></div>
		</div>
		<%
		options = Dropdown.createSortedRefDataOptions(
		            TradePortalConstants.COUNTRY, 
		            termsPartyBen.getAttribute("address_country"), 
		            loginLocale);
		%>
		<%= widgetFactory.createSelectField( "BenCountry", "RequestAdviseIssue.Country", " ", options, isReadOnly || isFromExpress, !isTemplate, isExpressTemplate,  "class='char35'", "", "") %>
					
		<%= widgetFactory.createTextField( "BenPhoneNumber", "RequestAdviseIssue.PhoneNumber", termsPartyBen.getAttribute("phone_number").toString(), "20", isReadOnly || isFromExpress, false, isExpressTemplate, "", "regExp:'[0-9]+'","") %>

		<%
		String applicantSearchHtml = "";
		String applicantClearHtml = "";
		String addressSearchIndicator = "";
		String identifierStr2="app_terms_party_oid,AppName,AppAddressLine1,AppAddressLine2,AppCity,AppStateProvince,AppCountry,AppPostalCode,Applicant,AppOTLCustomerId,AppUserDefinedField1,AppUserDefinedField2,AppUserDefinedField3,AppUserDefinedField4";
		String sectionName2="applicant_rta";
		if (!(isReadOnly || isFromExpress)) {
			applicantSearchHtml = widgetFactory.createPartySearchButton(identifierStr2,sectionName2,false,TradePortalConstants.APPLICANT,false);
			if (!isReadOnly && isTemplate) {
				applicantClearHtml = widgetFactory.createPartyClearButton( "ClearApplicantButton", "clearApplicant()",false,"");
			}
			addressSearchIndicator = termsPartyApp.getAttribute("address_search_indicator");
		}   
		 %>
		 <% if (!isReadOnly){ %> 
			<%=widgetFactory.createHoverHelp("applicant_rta", "PartySearchIconHoverHelp") %>
			<%=widgetFactory.createHoverHelp("ClearApplicantButton", "PartyClearIconHoverHelp") %>
			<%} %>
		<%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.Applicant",false,false, isExpressTemplate, (applicantSearchHtml+applicantClearHtml) ) %>
	<%--	<div class="subsectionShortHeader"><h3>Applicant</h3><a name="PartySearch" onClick="SearchParty('<%=identifierStr2%>', '<%=sectionName2%>');"><img class="searchButton" src="/portal/themes/default/images/search_icon.png"></a></div>--%>
		<%
		if (!(isReadOnly || isFromExpress) && !addressSearchIndicator.equals("N"))
	    {
	    	if(!isReadOnly && corpOrgHasMultipleAddresses)
	    	{ %>
	  <%--	Naveen 07-August-2012 IR-T36000004277. Commented the below div tag as the "Search Address" Button is not required- START	--%>	 
	  <%--   <div class = "formItem">
	    	   <a href="javascript:document.forms[0].submit()" name="AddressSearch" onClick="setButtonPressed('<%=TradePortalConstants.BUTTON_ADDRESSSEARCH%>', 0); return setPartySearchFields('<%=TradePortalConstants.APPLICANT%>');">
					<button data-dojo-type="dijit.form.Button" data-dojo-props="iconClass:'search'">Search Address</button>
			   </a>
			 </div>	--%>  	
	  <%--	Naveen 07-August-2012 IR-T36000004277 - END	--%>
	    	<% 
	    	}
	    }
		%>
		<% String applicantAddress = termsPartyApp.buildAddress(false, resMgr); %>
		<%if(isTemplate){ %>
		<table style="margin-left: 7px;" bgcolor='' border='0' width="97%"><tr>
            <td style="vertical-align:text-top;"><span class="asterisk">*</span></td>
            <td>
			<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
			<%--	<%= widgetFactory.createTextArea( "Applicant", "", applicantAddress, true,false, false, "style='overflow-x: hidden;' rows='4' cols='45'", "","none" ) %> --%>
				<%= widgetFactory.createAutoResizeTextArea("Applicant", "",applicantAddress, true, false,false, "class='buyerAutoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	
			</td>
            </tr>
        </table>
			
			
		<%}else{ %>
		<%--	<%= widgetFactory.createTextArea( "Applicant", "", applicantAddress, true,false, false, "style='width:auto;' rows='4' cols='43'", "","" ) %> --%>
		<table style="margin-left: 15px;" bgcolor='' border='0' width="97%"><tr>
		<td>
			<%= widgetFactory.createAutoResizeTextArea("Applicant", "",applicantAddress, true, false,false, "class='buyerAutoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	
		</td>
            </tr>
        </table>
		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
			<%} %>
		<%
       // secureParms.put("app_terms_party_oid",  termsPartyApp.getAttribute("terms_party_oid"));
		%>
				<div style="display:none">
				<input type=hidden name="AppTermsPartyType" 
					value="<%=TradePortalConstants.APPLICANT%>">
		        <input type=hidden name="app_terms_party_oid" 
		               value="<%=termsPartyApp.getAttribute("terms_party_oid")%>"">
		        <input type=hidden name="AppName" 
		               value="<%=termsPartyApp.getAttribute("name")%>">
		        <input type=hidden name="AppAddressLine1"
		               value="<%=termsPartyApp.getAttribute("address_line_1")%>">
		        <input type=hidden name="AppAddressLine2"
		               value="<%=termsPartyApp.getAttribute("address_line_2")%>">
		        <input type=hidden name="AppCity"
		               value="<%=termsPartyApp.getAttribute("address_city")%>">
		        <input type=hidden name="AppStateProvince"
		             value="<%=termsPartyApp.getAttribute("address_state_province")%>">
		        <input type=hidden name="AppCountry"
		               value="<%=termsPartyApp.getAttribute("address_country")%>">
		        <input type=hidden name="AppPostalCode"
		               value="<%=termsPartyApp.getAttribute("address_postal_code")%>">
		        <input type=hidden name="AppOTLCustomerId"
		               value="<%=termsPartyApp.getAttribute("OTL_customer_id")%>">
		        <input type=hidden name="AppAddressSeqNum"
		               value="<%=termsPartyApp.getAttribute("address_seq_num")%>">
		               <input type=hidden name="AppUserDefinedField1"
		     value="<%=termsPartyApp.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="AppUserDefinedField2"
		     value="<%=termsPartyApp.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="AppUserDefinedField3"
		     value="<%=termsPartyApp.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="AppUserDefinedField4"
		     value="<%=termsPartyApp.getAttribute("user_defined_field_4")%>">
	
			<%--rbhaduri - 14th Jun 06 - IR SAUG051361872--%>
			<input type=hidden name="AppAddressSearchIndicator"
		               value="<%=termsPartyApp.getAttribute("address_search_indicator")%>">
			</div>

						
		<%= widgetFactory.createTextField( "ApplRefNum", "transaction.ApplRefNumber", terms.getAttribute("reference_number"), "30", isReadOnly , false, false, "class='char30'", "", "") %>
		
		<%
		String applicantBankSearchHtml = "";
		String applicantBankClearHtml = "";
		String identifierStr3="apb_terms_party_oid,ApbName,ApbAddressLine1,ApbAddressLine2,ApbCity,ApbStateProvince,ApbCountry,ApbPostalCode,ApplicantBank,ApbOTLCustomerId,ApbUserDefinedField1,ApbUserDefinedField2,ApbUserDefinedField3,ApbUserDefinedField4";
		String sectionName3="app_bank_rta";
		if (!(isReadOnly)) {
			applicantBankSearchHtml = widgetFactory.createPartySearchButton(identifierStr3,sectionName3,false,TradePortalConstants.APPLICANT_BANK,false);
			if (!isTemplate) {
				applicantBankClearHtml = widgetFactory.createPartyClearButton( "ClearApbButton", "clearApplicantBank()",false,"");
			}	
		}   
		 %>
		 <% if (!isReadOnly){ %> 
				<%=widgetFactory.createHoverHelp("app_bank_rta", "PartySearchIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("ClearApbButton", "PartyClearIconHoverHelp") %>
		<%} %>	
		<% String applicantBankAddress = termsPartyApb.buildAddress(false, resMgr); %>
		<%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.ApplicantBank",false, false, isExpressTemplate, (applicantBankSearchHtml+applicantBankClearHtml) ) %>
		<%---<div class="subsectionShortHeader"><h3>Applicant Bank</h3><a name="PartySearch" onClick="SearchParty('<%=identifierStr3%>', '<%=sectionName3%>');"><img class="searchButton" src="/portal/themes/default/images/search_icon.png"></a></div>---%>
		
		 <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
		 <%--
		<%= widgetFactory.createTextArea( "ApplicantBank", "", applicantBankAddress, true,false, false, "rows='4' cols='43'", "","" ) %>
		 --%>

		 <%= widgetFactory.createAutoResizeTextArea("ApplicantBank", "", applicantBankAddress, true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	

		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
		<%
       // secureParms.put("apb_terms_party_oid", termsPartyApb.getAttribute("terms_party_oid"));
%>
		<div style="display:none">
		<input type=hidden name="ApbTermsPartyType" 
            value="<%=TradePortalConstants.APPLICANT_BANK%>">
      <input type=hidden name="apb_terms_party_oid"" 
               value="<%=termsPartyApb.getAttribute("terms_party_oid")%>"> 
               <input type=hidden name="ApbName" 
               value="<%=termsPartyApb.getAttribute("name")%>">
                <input type=hidden name="ApbAddressLine1"
               value="<%=termsPartyApb.getAttribute("address_line_1")%>"> 
               <input type=hidden name="ApbAddressLine2"
               value="<%=termsPartyApb.getAttribute("address_line_2")%>">
                <input type=hidden name="ApbCity"
               value="<%=termsPartyApb.getAttribute("address_city")%>">
                <input type=hidden name="ApbStateProvince"
               value="<%=termsPartyApb.getAttribute("address_state_province")%>"> 
      			<input type=hidden name="ApbCountry"
               value="<%=termsPartyApb.getAttribute("address_country")%>"> 
               <input type=hidden name="ApbPostalCode"
               value="<%=termsPartyApb.getAttribute("address_postal_code")%>"> 
     		 <input type=hidden name="ApbOTLCustomerId"
               value="<%=termsPartyApb.getAttribute("OTL_customer_id")%>">
               <input type=hidden name="ApbUserDefinedField1"
		     value="<%=termsPartyApb.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="ApbUserDefinedField2"
		     value="<%=termsPartyApb.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="ApbUserDefinedField3"
		     value="<%=termsPartyApb.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="ApbUserDefinedField4"
		     value="<%=termsPartyApb.getAttribute("user_defined_field_4")%>">
      </div>
      
      <%
		String advisingBankSearchHtml = "";
		String advisingBankClearHtml = "";
		String identifierStr4 = "nab_terms_party_oid,NabName,NabAddressLine1,NabAddressLine2,NabCity,NabStateProvince,NabCountry,NabPostalCode,AdviseThroughBank,NabOTLCustomerId,NabUserDefinedField1,NabUserDefinedField2,NabUserDefinedField3,NabUserDefinedField4";
		String sectionName4 = "advisethroughbank";
		if (!(isReadOnly || isFromExpress)) {
			advisingBankSearchHtml = widgetFactory.createPartySearchButton(identifierStr4,sectionName4,false,TradePortalConstants.ADVISE_THROUGH_BANK,false);
			if (!(isReadOnly || isFromExpress )) {
				advisingBankClearHtml = widgetFactory.createPartyClearButton( "ClearNabButton", "clearAdviseThroughBank()",false,"");
			}
		}   
		 %>
		 <% if (!isReadOnly){ %> 
				<%=widgetFactory.createHoverHelp("advisethroughbank", "PartySearchIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("ClearNabButton", "PartyClearIconHoverHelp") %>				
		<%} %>
		 <% String advisingBankAddress = termsPartyNab.buildAddress(false, resMgr); %>
		 <%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.AdviseThroughBank",false, false, isExpressTemplate, (advisingBankSearchHtml+advisingBankClearHtml) ) %>
		<%-- <div class="subsectionShortHeader"><h3>Advise Through Bank</h3><a name="PartySearch" onClick="SearchParty('<%=identifierStr4%>', '<%=sectionName4%>');"><img class="searchButton" src="/portal/themes/default/images/search_icon.png"></a></div>--%>
		
		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
		<%--
		<%= widgetFactory.createTextArea( "AdviseThroughBank", "", advisingBankAddress, true,false, false, "rows='4' cols='43'", "","" ) %>
		--%>

		 <%= widgetFactory.createAutoResizeTextArea("AdviseThroughBank", "", advisingBankAddress, true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	

		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
		<%
      //  secureParms.put("nab_terms_party_oid", termsPartyNab.getAttribute("terms_party_oid"));
%>
		<div style="display:none">
		 <input type=hidden name="NabTermsPartyType" 
             value="<%=TradePortalConstants.ADVISE_THROUGH_BANK%>">
        <input type=hidden name="nab_terms_party_oid" 
               value="<%=termsPartyNab.getAttribute("terms_party_oid")%>">
      <input type=hidden name="NabName" 
               value="<%=termsPartyNab.getAttribute("name")%>"> 
      <input type=hidden name="NabAddressLine1"
               value="<%=termsPartyNab.getAttribute("address_line_1")%>">
        <input type=hidden name="NabAddressLine2"
               value="<%=termsPartyNab.getAttribute("address_line_2")%>">
        <input type=hidden name="NabCity"
               value="<%=termsPartyNab.getAttribute("address_city")%>">
        <input type=hidden name="NabStateProvince"
               value="<%=termsPartyNab.getAttribute("address_state_province")%>">
        <input type=hidden name="NabCountry"
               value="<%=termsPartyNab.getAttribute("address_country")%>">
        <input type=hidden name="NabPostalCode"
               value="<%=termsPartyNab.getAttribute("address_postal_code")%>">
        <input type=hidden name="NabOTLCustomerId"
               value="<%=termsPartyNab.getAttribute("OTL_customer_id")%>">
               <input type=hidden name="NabUserDefinedField1"
		     value="<%=termsPartyNab.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="NabUserDefinedField2"
		     value="<%=termsPartyNab.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="NabUserDefinedField3"
		     value="<%=termsPartyNab.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="NabUserDefinedField4"
		     value="<%=termsPartyNab.getAttribute("user_defined_field_4")%>">
        </div>
      
      <%
		String reimbursingBankSearchHtml = "";
		String reimbursingBankClearHtml = "";
		String identifierStr5 ="rmb_terms_party_oid,RmbName,RmbAddressLine1,RmbAddressLine2,RmbCity,RmbStateProvince,RmbCountry,RmbPostalCode,ReimbursingBank,RmbOTLCustomerId,RmbUserDefinedField1,RmbUserDefinedField2,RmbUserDefinedField3,RmbUserDefinedField4";
		String sectionName5 ="reimbursing";
		if (!(isReadOnly || isFromExpress )) {
			reimbursingBankSearchHtml = widgetFactory.createPartySearchButton(identifierStr5,sectionName5,false,TradePortalConstants.REIMBURSING_BANK,false);
			if (!(isReadOnly || isFromExpress )) {
				reimbursingBankClearHtml = widgetFactory.createPartyClearButton( "ClearRmbButton", "clearReimbursingBank()",false,"");
			}
		}   
		 %>
		 <% if (!isReadOnly){ %> 				
				<%=widgetFactory.createHoverHelp("reimbursing", "PartySearchIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("ClearRmbButton", "PartyClearIconHoverHelp") %>
		 <%} %>
		 <% String reimbursingBankAddress = termsPartyRmb.buildAddress(false, resMgr); %>
		 <%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.ReimbursingBank",false, false, isExpressTemplate, (reimbursingBankSearchHtml+reimbursingBankClearHtml) ) %>
		<%-- <div class="subsectionShortHeader"><h3>Reimbursing Bank</h3><a name="PartySearch" onClick="SearchParty('<%=identifierStr5%>', '<%=sectionName5%>');"><img class="searchButton" src="/portal/themes/default/images/search_icon.png"></a></div>---%>
		 
		 <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
		 <%--
		 <%= widgetFactory.createTextArea( "ReimbursingBank", "", reimbursingBankAddress, true,false, false, "rows='4' cols='43'", "","" ) %>
		 --%>

			<%= widgetFactory.createAutoResizeTextArea("ReimbursingBank", "",reimbursingBankAddress, true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	

		 <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
		 <%
	   //     secureParms.put("rmb_terms_party_oid",   termsPartyRmb.getAttribute("terms_party_oid"));
	%>
		<div style="display:none">
		 <input type=hidden name="RmbTermsPartyType" 
             value="<%=TradePortalConstants.REIMBURSING_BANK%>">
	        <input type=hidden name="rmb_terms_party_oid" 
	               value="<%=termsPartyRmb.getAttribute("terms_party_oid")%>">
	        <input type=hidden name="RmbName" 
	               value="<%=termsPartyRmb.getAttribute("name")%>">
	        <input type=hidden name="RmbAddressLine1"
	               value="<%=termsPartyRmb.getAttribute("address_line_1")%>">
	        <input type=hidden name="RmbAddressLine2"
	               value="<%=termsPartyRmb.getAttribute("address_line_2")%>">
	        <input type=hidden name="RmbCity"
	               value="<%=termsPartyRmb.getAttribute("address_city")%>">
	        <input type=hidden name="RmbStateProvince"
	               value="<%=termsPartyRmb.getAttribute("address_state_province")%>">
	        <input type=hidden name="RmbCountry"
	               value="<%=termsPartyRmb.getAttribute("address_country")%>">
	        <input type=hidden name="RmbPostalCode"
	               value="<%=termsPartyRmb.getAttribute("address_postal_code")%>">
	        <input type=hidden name="RmbOTLCustomerId"
	               value="<%=termsPartyRmb.getAttribute("OTL_customer_id")%>">
	               <input type=hidden name="RmbUserDefinedField1"
		     value="<%=termsPartyRmb.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="RmbUserDefinedField2"
		     value="<%=termsPartyRmb.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="RmbUserDefinedField3"
		     value="<%=termsPartyRmb.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="RmbUserDefinedField4"
		     value="<%=termsPartyRmb.getAttribute("user_defined_field_4")%>">
	        </div>
	        
			 <% String issuerAddress = termsPartyIsb.buildAddress(false, resMgr); %>
			 <%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.Issuer",false, !isTemplate, isExpressTemplate, "" ) %>
			 <%
				if (!(isReadOnly || isFromExpress))
			    {
			    	if(!isReadOnly && corpOrgHasMultipleAddresses)
			    	{ %>
			    	<div class = "formItem">
			    	   <button data-dojo-type="dijit.form.Button"  name="IssuerAddressSearch" id="IssuerAddressSearch" type="button">
		                   <%=resMgr.getText("common.searchAddressButton",TradePortalConstants.TEXT_BUNDLE)%>
		                   <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			                   var corpOrgOid = <%=corpOrgOid%>;
								require(["t360/common", "t360/partySearch"], function(common, partySearch){
				 					partySearch.setPartySearchFields('<%=TradePortalConstants.ISSUING_BANK%>');
				       	  			common.openCorpCustAddressSearchDialog(corpOrgOid);
				       	  		});
		                   </script>
		                </button>
					 </div>  
			    	<% 
			    	}
			    }
				%>
			  <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
			  <%--
			 <%= widgetFactory.createTextArea( "textarea", "", issuerAddress, true,false, false, "rows='4' cols='43'", "","" ) %>
			 --%>
			 <%= widgetFactory.createAutoResizeTextArea("textarea", "", issuerAddress, true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	

			<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
			 <%= widgetFactory.createTextField( "IssuerRefNum", "RequestAdviseIssue.IssuerReferenceNumber", terms.getAttribute("issuer_ref_num"), "30", isReadOnly ) %>
			 <%
		        secureParms.put("isb_terms_party_oid", 
		                        termsPartyIsb.getAttribute("terms_party_oid"));
		%>
			<div style="display:none">
		        <input type=hidden name="IsbTermsPartyType" 
		               value="<%=TradePortalConstants.ISSUING_BANK%>">
		        <input type=hidden name="IsbName" 
		               value="<%=termsPartyIsb.getAttribute("name")%>"> 
		        <input type=hidden name="IsbAddressLine1"
		               value="<%=termsPartyIsb.getAttribute("address_line_1")%>">
		        <input type=hidden name="IsbAddressLine2"
		               value="<%=termsPartyIsb.getAttribute("address_line_2")%>">
		        <input type=hidden name="IsbCity"
		               value="<%=termsPartyIsb.getAttribute("address_city")%>">
		        <input type=hidden name="IsbStateProvince"
		               value="<%=termsPartyIsb.getAttribute("address_state_province")%>">
		        <input type=hidden name="IsbCountry"
		               value="<%=termsPartyIsb.getAttribute("address_country")%>">
		        <input type=hidden name="IsbPostalCode"
		               value="<%=termsPartyIsb.getAttribute("address_postal_code")%>">
		        <input type=hidden name="IsbOTLCustomerId"
		               value="<%=termsPartyIsb.getAttribute("OTL_customer_id")%>">

			<%-- rbhaduri - 1st Aug 06 - IR LRUG070634941 --%>
			<input type=hidden name="IsbAddressSeqNum"
		               value="<%=termsPartyIsb.getAttribute("address_seq_num")%>">   
			</div>
		</div>
		
		<div class="columnRight">
		
		<%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.DetailedInformation", false, !isTemplate, false, "")%>
		<%-- pgedupudi - IR#T36000041901 --%>
		<%= widgetFactory.createDateField( "IssueDate", "RequestAdviseIssue.IssueDate", StringFunction.xssHtmlToChars(instrument.getAttribute("issue_date")), isReadOnly, false, false,  "class='char10'", dateWidgetOptions, "" ) %>
		<div>
		<%= widgetFactory.createDateField( "ExpiryDate", "RequestAdviseIssue.ExpiryDate", StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")), isReadOnly, false, false,  "class='char10'",dateWidgetOptions, "inline" ) %>
		 
		<%= widgetFactory.createTextField( "PlaceOfExpiry", "RequestAdviseIssue.ExpiryPlace", terms.getAttribute("place_of_expiry"), "29", isReadOnly, false, false,  "class='char15'", "", "inline") %>
		<div style="clear:both;"></div>
	     </div>
	     <div class="formItem " >
		<%= widgetFactory.createNote("RequestAdviseIssue.ExpiryDetails")%>
		<%if(!isReadOnly){ %>
		<a href='javascript:openOtherConditionsDialog("AddlConditionsText","SaveTrans","<%=resMgr.getText("RequestAdviseIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>");' onclick="setChargeOther()"> <%=resMgr.getText("RequestAdviseIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>  </a>
		<%--<%= widgetFactory.createNote("<a href='javascript:openOtherConditionsDialog(\"AddlConditionsText\",\"SaveTrans\",\"Other Conditions\");' >Other Conditions</a>")%>--%>
		<%}else{ %>
			<%= widgetFactory.createNote("RequestAdviseIssue.OtherCondLink")%>
		<% } %>
		</div>
		
		<%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.AmountDetails", false, !isTemplate, false, "")%>
		
		<%
        options = Dropdown.createSortedCurrencyCodeOptions(
                    terms.getAttribute("amount_currency_code"), loginLocale);
		%>
		<div>
	     <%-- <%= widgetFactory.createCurrencySelectField( "TransactionCurrency", "transaction.Currency", options,"", isReadOnly, false, false) %> --%>
	     <%=widgetFactory.createSelectField("TransactionCurrency","transaction.Currency", " ", options, isReadOnly, !isTemplate,false, "style=\"width: 50px;\"", "", "inline")%>
	     <%= widgetFactory.createAmountField( "TransactionAmount", "transaction.Amount", displayAmount, currency, isReadOnly,false, false, 
	    		 "class='char15'", "readOnly:" +isReadOnly, "inline") %>
	     <div style="clear:both;"></div>
	     </div>
	     
	     <div class = "formItemWithIndent3">
	     <% 
			if(isExpressTemplate){
		%>
			<span class='hashMark'>#</span>
		<%}%>
	     <%= widgetFactory.createRadioButtonField( "AmountToleranceType", "TradePortalConstants.AmtTolerance", "RequestAdviseIssue.AmtTolerance", TradePortalConstants.AMOUNT_TOLERANCE,amountToleranceType.equals(TradePortalConstants.AMOUNT_TOLERANCE), isReadOnly || isFromExpress ) %>
	     </div>
	     
	     <div class = "formItem">
	     <%= widgetFactory.createSubLabel( "transaction.Plus") %>
		<%if(!isReadOnly){ %>
		<%= widgetFactory.createPercentField( "AmountTolerancePlus", "", terms.getAttribute("amt_tolerance_pos"), isReadOnly || isFromExpress,
									false, false, "", "", "none") %>											
		<%}else {%>
			<b><%=terms.getAttribute("amt_tolerance_pos") %></b>
		<%} %>							
		<% if (!isReadOnly){ %>	     		
		 		<%= widgetFactory.createHoverHelp("AmountTolerancePlus", "RequestAdviseIssue.Plus")%>     
			 <%} %>	
		<%= widgetFactory.createSubLabel( "transaction.Percent") %>
		&nbsp;&nbsp;
		<%= widgetFactory.createSubLabel( "transaction.Minus") %>
		<%if(!isReadOnly){ %>
		<%= widgetFactory.createPercentField( "AmountToleranceMinus", "", terms.getAttribute("amt_tolerance_neg"), isReadOnly || isFromExpress,
									false, false, "", "", "none") %>		
		<%}else {%>
			<b><%=terms.getAttribute("amt_tolerance_neg") %></b>
		<%} %>	
		<% if (!isReadOnly){ %>
	     		<%= widgetFactory.createHoverHelp("AmountToleranceMinus", "RequestAdviseIssue.Minus")%>
	    <%} %>
		<%= widgetFactory.createSubLabel( "transaction.Percent") %>
		<div style="clear:both;"></div>
	     
		 </div>
		 
		 <div class = "formItemWithIndent3">
		 <% 
			if(isExpressTemplate){
		%>
			<span class='hashMark'>#</span>
		<%}%>
	     <%= widgetFactory.createRadioButtonField( "AmountToleranceType", "MaximumCreditAmount", "RequestAdviseIssue.MaximumCreditAmount", TradePortalConstants.NOT_EXCEEDING,amountToleranceType.equals(TradePortalConstants.NOT_EXCEEDING), isReadOnly || isFromExpress ) %>
	     <%= widgetFactory.createInlineLabel("","RequestAdviseIssue.NotExceeding") %>
	     </div>
	     
	     <%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.AdditionalAmountsCovered")%>
	     <%= widgetFactory.createTextArea( "AddlAmountsCovered", "", terms.getAttribute("addl_amounts_covered"), isReadOnly || isFromExpress ,isReadOnly || isFromExpress, false, "", "","" ) %>
	     <% if (!isReadOnly){ %> 				
				<%= widgetFactory.createHoverHelp("AddlAmountsCovered", "RequestAdviseIssue.AddlAmountsCovered")%>
	     <%} %>	
	     <%-- Leelavathi IR#T36000015492 CR-737 Rel-8.2 04/04/2013 Begin --%>
	     <%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.Availability", false, !isTemplate, isExpressTemplate, "")%>
	     <div>
	      <%-- Leelavathi IR#T36000016452 Rel-8.2 06/10/2013 Begin --%>
	     <%-- <%
         options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AVAILABLE_BY,
                                terms.getAttribute("available_by"),
                                loginLocale);
         defaultText = " ";
         %>
         
	     <%= widgetFactory.createSelectField( "AvailableBy", "RequestAdviseIssue.AvailBy", defaultText, options, isReadOnly || isFromExpress, false, false, "class='char16'", "", ""  ) %> --%>
	     <%-- Leelavathi IR#T36000015492 CR-737 Rel-8.2 04/04/2013 End --%>
	      <%-- Leelavathi IR#T36000016452 Rel-8.2 06/10/2013 End --%>
	    <%
         options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AVAILABLE_WITH_PARTY,
                                terms.getAttribute("available_with_party"),
                                loginLocale);
         defaultText = " ";
         %>
	     <%= widgetFactory.createSelectField( "AvailableWithParty", "RequestAdviseIssue.AvailWithPty", defaultText, options, isReadOnly || isFromExpress, false, false, "class='char16'", "", "inline" ) %>
	     <div style="clear:both;"></div>
	     </div>
	     
	     <%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.DraftDetails", false, !isTemplate, isExpressTemplate, "")%>
	     <div class = "formItem">
	     <%= widgetFactory.createRadioButtonField( "DraftsRequired", "DraftsRequired1", "RequestAdviseIssue.DraftsRequiredNo", TradePortalConstants.INDICATOR_NO,draftsRequired.equals(TradePortalConstants.INDICATOR_NO), isReadOnly || isFromExpress ) %>
	     <br>
	     <%= widgetFactory.createRadioButtonField( "DraftsRequired", "DraftsRequired2", "RequestAdviseIssue.DraftsRequiredYes", TradePortalConstants.INDICATOR_YES,draftsRequired.equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress ) %>
	     <div style="clear:both;"></div>
	     </div>
	     
	     <%
         options = Dropdown.createSortedRefDataOptions(TradePortalConstants.DRAWN_ON_PARTY,
                                terms.getAttribute("drawn_on_party"),
                                loginLocale);
         defaultText = " ";
         %>
         <div class="formItemWithIndent8">
         	<%= widgetFactory.createSelectField( "DrawnOnParty", "RequestAdviseIssue.DrawnOnPty", defaultText, options, isReadOnly || isFromExpress ) %>
         </div>
         <%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start --%>
         <%--
         <%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.PmtTerms",isReadOnly, !isTemplate, isExpressTemplate, "" ) %>
			<div class = "formItem">
			<%= widgetFactory.createRadioButtonField( "PmtTermsType", "PMT_SIGHT", "RequestAdviseIssue.Sight", TradePortalConstants.PMT_SIGHT,pmtTermsType.equals(TradePortalConstants.PMT_SIGHT), isReadOnly || isFromExpress ) %>
			<br>
			
			<div>			
			<%= widgetFactory.createRadioButtonField( "PmtTermsType", "PMT_DAYS_AFTER", " ", TradePortalConstants.PMT_DAYS_AFTER,pmtTermsType.equals(TradePortalConstants.PMT_DAYS_AFTER), isReadOnly || isFromExpress ) %>
			<%= widgetFactory.createNumberField( "PmtTermsNumDaysAfter", "", terms.getAttribute("pmt_terms_num_days_after"), "3", isReadOnly || isFromExpress, false, true, "onChange=\"enablePMTType();\"", "", "none" ) %>
			<%= widgetFactory.createSubLabel( "RequestAdviseIssue.DaysAfter") %>
			<%
			options = Dropdown.createSortedRefDataOptions( TradePortalConstants.PMT_TERMS_DAYS_AFTER,
                 terms.getAttribute("pmt_terms_days_after_type"),
                 loginLocale);
			%>
			<%= widgetFactory.createSelectField( "PmtTermsDaysAfterType", "", " ", options, isReadOnly || isFromExpress, false, true, "onChange=\"enablePMTType();\"", "", "none" ) %> 
			<div style="clear:both;"></div>
			</div> 
			
			
			<%= widgetFactory.createRadioButtonField( "PmtTermsType", "pmtOther", "RequestAdviseIssue.OtherDetails", TradePortalConstants.PMT_OTHER, pmtTermsType.equals(TradePortalConstants.PMT_OTHER), isReadOnly || isFromExpress ) %>
			<%if(!isReadOnly){ %>
			<a href='javascript:openOtherConditionsDialog("AddlConditionsText","SaveTrans","<%=resMgr.getText("RequestAdviseIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%> ");' onclick="setPMTOther()"> <%=resMgr.getText("RequestAdviseIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>  </a>
			<%}else{ %>
				<%=resMgr.getText("RequestAdviseIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>
			<%} %>
			<%= widgetFactory.createSubLabel("ImportDLCIssue.Below") %>

			</div>
			
			<div class = "formItemWithIndent4">
			<%= widgetFactory.createLabel("PmtTermsPercentInvoice", "RequestAdviseIssue.For", isReadOnly,!isTemplate, isExpressTemplate,"inline" ) %>
			<%= widgetFactory.createPercentField( "PmtTermsPercentInvoice", "", terms.getAttribute("pmt_terms_percent_invoice"), isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "", "", "none" ) %>
			<%= widgetFactory.createSubLabel( "RequestAdviseIssue.InvValuePercent") %>
			<div style="clear:both;"></div>
			</div>
			--%>
			<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - End --%>
			
			<%= widgetFactory.createSubsectionHeader( "RequestAdviseIssue.BankCharges",isReadOnly, !isTemplate, isExpressTemplate, "" ) %>
			
			<div class = "formItem">
			<%= widgetFactory.createRadioButtonField( "BankChargesType", TradePortalConstants.CHARGE_OUR_ACCT, "RequestAdviseIssue.AllOurAcct", TradePortalConstants.CHARGE_OUR_ACCT, bankChargesType.equals(TradePortalConstants.CHARGE_OUR_ACCT), isReadOnly || isFromExpress ) %>
			<br>
			
			<%= widgetFactory.createRadioButtonField( "BankChargesType", TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE, "RequestAdviseIssue.AllOutside", TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE, bankChargesType.equals(TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE), isReadOnly || isFromExpress ) %>
			<br>
			
			<%= widgetFactory.createRadioButtonField( "BankChargesType", "chargeOther", "RequestAdviseIssue.OtherDetails", TradePortalConstants.CHARGE_OTHER, bankChargesType.equals(TradePortalConstants.CHARGE_OTHER), isReadOnly || isFromExpress ) %>
				<span class="label">
			<%if(!isReadOnly){ %>
			<a href='javascript:openOtherConditionsDialog("AddlConditionsText","SaveTrans","<%=resMgr.getText("RequestAdviseIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>");' onclick="setChargeOther()"> <%=resMgr.getText("RequestAdviseIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>  </a>
			<%}else{ %>
				<%=resMgr.getText("RequestAdviseIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>
			<%} %>
		 	</span>	
			<%= widgetFactory.createSubLabel("ImportDLCIssue.Below") %>
			 </div>        
	       
         
		</div>
	<div style="clear:both;"></div>	
		<%-- Leelavathi 10thDec2012 - Rel8200 CR-737 - Start --%>
		<div style="clear:both;"></div>
		<%--
		   -==========================================
		   -========= Payment Tenor Terms  ==========
		   -==========================================
		--%>	
		
		<%=widgetFactory.createWideSubsectionHeader("RAQIssue.PmtTerms", false,!isTemplate, false, "") %>
		<div class = "formItem">
			<div style="clear:both;"></div>
			<%-- Leelavathi IR#T36000015097 Rel-8.2 03/22/2013 Start --%>
		<%--<%= widgetFactory.createLabel( "ImportDLCIssue.PmtType","ImportDLCIssue.PmtType",isReadOnly,false,isFromExpress,"inline")
			<%
			options = Dropdown.createSortedRefDataOptions( TradePortalConstants.AVAILABLE_BY_TYPE, terms.getAttribute("payment_type"), loginLocale);
			%>
			<%=widgetFactory.createSelectField("TermsPmtType","", " ", options,isReadOnly, false, false, "", "", "")%> --%>
			<div>
			<%=resMgr.getText("ImportDLCIssue.PmtType",TradePortalConstants.TEXT_BUNDLE)%>
			&nbsp;&nbsp;
			<%options = Dropdown.createSortedRefDataOptions( TradePortalConstants.AVAILABLE_BY_TYPE, terms.getAttribute("payment_type"), loginLocale);%>
			<b><%=widgetFactory.createSelectField("TermsPmtType","", " ", options,isReadOnly||isFromExpress, false, false, "", "", "none")%></b>
			<div style='clear: both;'></div>
		</div>
		<%-- Leelavathi IR#T36000015097 Rel-8.2 03/22/2013 End --%>
			<br>
			<% String displayStyle = "display: none;";
			String displayStyle1 = "display: block;";
			// Kiran IR#T36000015047 Rel-8.2 06/03/2013 Start 
			// Added a new string in order to hide the Add 2 more button  
					String disAdd2MrLnsStl = "display: block;";
			 // Kiran IR#T36000015047 Rel-8.2 06/03/2013 End 
			if("SPEC".equals(terms.getAttribute("payment_type")) ){ 
					displayStyle = "display: block;";	
					displayStyle1 = "display: none;";
			}
			//Leelavathi IR#T36000011303 Rel-8.2 02/20/2013 Begin
			if("PAYM".equals(terms.getAttribute("payment_type")) ){ 
				displayStyle1 = "display: none;";
				// Kiran IR#T36000015047 Rel-8.2 06/03/2013 Start 
			// Used that new string in order to hide the Add 2 more button 
				disAdd2MrLnsStl= "display: none;";
			// Kiran IR#T36000015047 Rel-8.2 06/03/2013 End
			}
			//Leelavathi IR#T36000011303 Rel-8.2 02/20/2013 End 
			// Leelavathi IR#T36000011655,IR#T36000011742 Rel-8.2 02/20/2013 Begin
			String accpDefpDisabled ="";
			if("ACCP".equals(terms.getAttribute("payment_type")) || "DEFP".equals(terms.getAttribute("payment_type"))){
				//Leelavathi IR#T36000014888, IR#T36000016659 CR-737 Rel-8.2 11/04/2013 Begin
				//Leelavathi IR#T36000016659 CR-737 Rel-8.2 06/05/2013 Begin
				accpDefpDisabled = " READONLY ";
				// Kiran IR#T36000015047 Rel-8.2 06/03/2013 Start 
			// Used that new string in order to hide the Add 2 more button 
				disAdd2MrLnsStl= "display: none;";
			// Kiran IR#T36000015047 Rel-8.2 06/03/2013 End
				//accpDefpDisabled = " DISABLED ";
				//Leelavathi IR#T36000016659 CR-737 Rel-8.2 06/05/2013 End
				//Leelavathi IR#T36000014888 CR-737 Rel-8.2 11/04/2013 End
			}
			//Leelavathi IR#T36000011655,IR#T36000011742 Rel-8.2 02/20/2013 End
			%> 
			
			<div id='PmtTermsSpecialTenorTextDiv' style='<%=displayStyle%>'>
			<%= widgetFactory.createTextArea( "PmtTermsSpecialTenorText", "RAQIssue.SpecialTenorText", terms.getAttribute("special_tenor_text"), isReadOnly || isFromExpress, false, true, "", "", "none" ) %>
			</div>
			<br>
			<div id='PmtTermsInPercentAmountIndDiv' style='<%=displayStyle1%>'>		
			<%-- Leelavathi IR#T36000011742 Rel-8.2 02/20/2013 Begin  --%>	
			<%--<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "RAQIssue.Percentage","RAQIssue.Percentage",TradePortalConstants.PMT_IN_PERCENT,TradePortalConstants.PMT_IN_PERCENT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress ) 
			<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "RAQIssue.Amount","RAQIssue.Amount",TradePortalConstants.PMT_IN_AMOUNT,  TradePortalConstants.PMT_IN_AMOUNT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress ) %>--%>
			<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "RAQIssue.Percentage","RAQIssue.Percentage",TradePortalConstants.PMT_IN_PERCENT,TradePortalConstants.PMT_IN_PERCENT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress,""+accpDefpDisabled,"" ) %>
			<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "RAQIssue.Amount","RAQIssue.Amount",TradePortalConstants.PMT_IN_AMOUNT,  TradePortalConstants.PMT_IN_AMOUNT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress ,""+accpDefpDisabled,"") %>
			<%-- Leelavathi IR#T36000011742 Rel-8.2 02/20/2013 End  --%>
			</div>

			<input type=hidden value=<%=pmtTermsRowCount %> name="numberOfMultipleObjects1"   id="noOfPmtTermsRows"> 
			<div id='pymtTermsDiv' style='<%=displayStyle1%>'>
			<table id="pymtTerms" class="formDocumentsTable" width="100%" border="1" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<%if(TradePortalConstants.PMT_IN_AMOUNT.equals(terms.getAttribute("percent_amount_dis_ind"))){ %>
						<%-- Jyothikumari.G 16/05/2013 Rel8.2 IR-T36000016903 Start --%>
						<%-- I added the below class to move the column header to the left --%>
							<th width="8%" class="genericCol"><%=resMgr.getText("RAQIssue.PaymentInAmount",TradePortalConstants.TEXT_BUNDLE)%></th>	
						<%}else{%>
							<th width="8%" class="genericCol"><%=resMgr.getText("RAQIssue.PaymentInPercent",TradePortalConstants.TEXT_BUNDLE)%></th>
						<%}%>
						<th width="18%" class="genericCol"><%=resMgr.getText("RAQIssue.TenorType",TradePortalConstants.TEXT_BUNDLE)%></th>
						<th width="45%" class="genericCol"><%=resMgr.getText("RAQIssue.TenorDetails",TradePortalConstants.TEXT_BUNDLE)%></th>
						<th width="28%" class="genericCol"><%=resMgr.getText("RAQIssue.MaturityDate",TradePortalConstants.TEXT_BUNDLE)%></th>
						<%-- Jyothikumari.G 16/05/2013 Rel8.2 IR-T36000016903 End --%>
					</tr>
				</thead>
				<tbody>
					 <%--passes in pmtTermsList, isReadOnly, isFromExpress, loginLocale,
			          plus new pmtTermsIndex below--%>
					<%
					  int pmtTermsIndex = 0;
					String percentOrAmountInd = terms.getAttribute("percent_amount_dis_ind");
					%>
			      	<%@ include file="TransactionPaymentTermRows.frag" %> 
				</tbody>
			</table>
		</div>
<%-- Kiran IR#T36000015047 Rel-8.2 06/03/2013 Start --%>
<%-- Added a new style and replaced  displayStyle1 with disAdd2MrLnsStl --%>
<div id='add2MoreLinesDiv' style='<%=disAdd2MrLnsStl%>'>
<%-- Kiran IR#T36000015047 Rel-8.2 05/31/2013 End --%>
	<% if (!(isReadOnly))
	{     %>
    <button data-dojo-type="dijit.form.Button" type="button" id="add2MoreLines" name="add2MoreLines">
    	<%= resMgr.getText("common.Add2MoreLines", TradePortalConstants.TEXT_BUNDLE) %>
    	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				//add2MoreLinesTab(2);
		</script>
	</button>
			<%
		}else{  %>
			&nbsp;
		<%
		} %>
		<%-- Leelavathi IR#T36000011669 Rel-8.2 13/02/2013 End --%>
</div>
		
		<div style="clear:both;"></div>
			</div>
			<%-- Leelavathi 10thDec2012 - Rel8200 CR-737 - End --%>
		<script LANGUAGE="JavaScript">

		  function clearReimbursingBank() {
		    document.TransactionRQA.RmbName.value = "";
		    document.TransactionRQA.RmbAddressLine1.value = "";
		    document.TransactionRQA.RmbAddressLine2.value = "";
		    document.TransactionRQA.RmbCity.value = "";
		    document.TransactionRQA.RmbStateProvince.value = "";
		    document.TransactionRQA.RmbCountry.value = "";
		    document.TransactionRQA.RmbPostalCode.value = "";
		    document.TransactionRQA.RmbOTLCustomerId.value = "";
		    document.TransactionRQA.ReimbursingBank.value = "";
		    document.TransactionRQA.RmbUserDefinedField1.value = "";	
		    document.TransactionRQA.RmbUserDefinedField2.value = "";	
		    document.TransactionRQA.RmbUserDefinedField3.value = "";	
		    document.TransactionRQA.RmbUserDefinedField4.value = "";	
		  }

		  function clearAdviseThroughBank() {
		    document.TransactionRQA.NabName.value = "";
		    document.TransactionRQA.NabAddressLine1.value = "";
		    document.TransactionRQA.NabAddressLine2.value = "";
		    document.TransactionRQA.NabCity.value = "";
		    document.TransactionRQA.NabStateProvince.value = "";
		    document.TransactionRQA.NabCountry.value = "";
		    document.TransactionRQA.NabPostalCode.value = "";
		    document.TransactionRQA.NabOTLCustomerId.value = "";
		    document.TransactionRQA.AdviseThroughBank.value = "";
		    document.TransactionRQA.NabUserDefinedField1.value = "";	
		    document.TransactionRQA.NabUserDefinedField2.value = "";	
		    document.TransactionRQA.NabUserDefinedField3.value = "";	
		    document.TransactionRQA.NabUserDefinedField4.value = "";
		  }
		  
		  function clearApplicantBank() {
		    document.TransactionRQA.ApbName.value = "";
		    document.TransactionRQA.ApbAddressLine1.value = "";
		    document.TransactionRQA.ApbAddressLine2.value = "";
		    document.TransactionRQA.ApbCity.value = "";
		    document.TransactionRQA.ApbStateProvince.value = "";
		    document.TransactionRQA.ApbCountry.value = "";
		    document.TransactionRQA.ApbPostalCode.value = "";
		    document.TransactionRQA.ApbOTLCustomerId.value = "";
		    document.TransactionRQA.ApplicantBank.value = "";
		    document.TransactionRQA.ApbUserDefinedField1.value = "";	
		    document.TransactionRQA.ApbUserDefinedField2.value = "";	
		    document.TransactionRQA.ApbUserDefinedField3.value = "";	
		    document.TransactionRQA.ApbUserDefinedField4.value = "";	
		  }

		  function clearApplicant() 
		  {
		    document.TransactionRQA.AppName.value = "";
		    document.TransactionRQA.AppAddressLine1.value = "";
		    document.TransactionRQA.AppAddressLine2.value = "";
		    document.TransactionRQA.AppCity.value = "";
		    document.TransactionRQA.AppStateProvince.value = "";
		    document.TransactionRQA.AppCountry.value = "";
		    document.TransactionRQA.AppPostalCode.value = "";
		    document.TransactionRQA.AppOTLCustomerId.value = "";
		    document.TransactionRQA.Applicant.value = "";
			document.TransactionRQA.ApplRefNum.value = "";
			document.TransactionRQA.AppUserDefinedField1.value = "";	
			document.TransactionRQA.AppUserDefinedField2.value = "";	
			document.TransactionRQA.AppUserDefinedField3.value = "";	
			document.TransactionRQA.AppUserDefinedField4.value = "";	
		  }
		  
		  function clearAmountTolerances()
		  {
		    document.forms[0].AmountTolerancePlus.value = "";
			document.forms[0].AmountToleranceMinus.value = "";
		  }
		  
		  function checkBeneficiaryName(originalName)
		  {
			  if(dijit.byId('BenName')){
    	  		if(dijit.byId('BenName').value != originalName){
    	  			if(null != document.getElementById('BenOTLCustomerId'))
    	  				document.getElementById('BenOTLCustomerId').value = "";
    	  		}
    	  	}
			<%-- 
		     if (document.forms[0].BenName.value != originalName)
		     {
		        document.forms[0].BenOTLCustomerId.value = "";
		     }
			  --%>
		  }
		  
		  function selectDraftsRequired()
		  {
		  	  if (document.forms[0].DrawnOnParty.value != "")
			  {
		  	  	document.forms[0].DraftsRequired[0].checked = false;
			  	document.forms[0].DraftsRequired[1].checked = true;
			  } else {
		  	  	document.forms[0].DraftsRequired[0].checked = true;
			  	document.forms[0].DraftsRequired[1].checked = false;
			  }
		  }
	function enablePMTType(){
	if(dijit.byId("PmtTermsNumDaysAfter").value > '' ||
		dijit.byId("PmtTermsDaysAfterType").value > ''){
		dijit.byId("PMT_DAYS_AFTER").set("checked", true);
	}
	}
		  function clearDrawnOnParty()
		  {
		  	document.forms[0].DrawnOnParty.value = "";
		  }

		</script>
