<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Request to Advise Issue Page - Bank Defined section

  Description:
    Contains HTML to create the Request to Advise Issue Bank Defined section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-RQA-ISS-BankDefined.jsp" %>    
*******************************************************************************
--%>
<%=widgetFactory.createSubLabel("common.RejectionReasonDetail")%>
<div class = "formItem">
	<%-- <%=widgetFactory.createSubLabel("common.RejectionReason")%>  --%>
	<%--  --%>
	<br/>
	
    <%out.print(rejectionReasonText);%>
	<br> 
</div>
