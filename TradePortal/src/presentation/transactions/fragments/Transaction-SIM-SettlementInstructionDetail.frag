<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Settlement Instructions Detail for IMC, DBA and DFP

  Description:
    Contains HTML to create Settlement instruction for SIM for IMC, DBA or DFP.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMC-SIM-DBA-SIM-DFP-SIM.jsp" %>
*******************************************************************************
--%>
<%
	presentationAmount.append(currencyCode);
	presentationAmount.append(" ");
	presentationAmount.append(displayAmount);
%>
<table width="100%">
	<tr class="formItem" width="100%">
		<td style="vertical-align: top;" width="17%">
			<%= widgetFactory.createTextField("displayAmount", "SettlementInstruction.amount",
				presentationAmount.toString(), "35", true, false, false, "", "", "") %>
		</td>
		<td style="vertical-align: top;" width="16%">
		<% 
        	if ( instrument != null) {
        		issueDate = instrument.getAttribute("issue_date");
        		if(InstrumentServices.isNotBlank(issueDate)){                                      
        			issueDate = TPDateTimeUtility.formatDate(issueDate, TPDateTimeUtility.SHORT, loginLocale);
        		}
        	}
		%>
			<%= widgetFactory.createTextField("issueDate", "MailMessage.IssueDate", issueDate, "35", true, false, false, "", "", "") %>
		</td>
		<% 
			if ( !InstrumentType.IMPORT_COL.equals(instrumentType) ) {
				if ( InstrumentType.LOAN_RQST.equals(instrumentType) ) {
					TermsWebBean termsForLoan        	  = null;
					TransactionWebBean originalTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");
					originalTransaction.getById(instrument.getAttribute("original_transaction_oid"));
					String bankReleasedTerms	= originalTransaction.getAttribute("c_BankReleasedTerms");         
					boolean hasBankReleasedTerms  = StringFunction.isNotBlank( bankReleasedTerms );
					if( hasBankReleasedTerms ) {
						termsForLoan = originalTransaction.registerBankReleasedTerms();
					} else {
						termsForLoan = originalTransaction.registerCustomerEnteredTerms(); 
					}
	
					if (termsForLoan != null) {
						maturityDate = termsForLoan.getAttribute("usance_maturity_date");
					}else{
						maturityDate = terms.getAttribute("loan_terms_fixed_maturity_dt");
					}
				}else{
					//Rel 9.4 UAT IR#T36000045536
					maturityDate = instrument.getAttribute("copy_of_expiry_date");
				}
				if ( StringFunction.isNotBlank(maturityDate) ) {
					maturityDate = TPDateTimeUtility.formatDate(maturityDate, TPDateTimeUtility.SHORT, loginLocale);
				}
		%>
			<td style="vertical-align: top;" width="15%">
	        	<%= widgetFactory.createTextField("maturityDate", "SettlementInstruction.maturityDate",
					maturityDate, "35", true, false, false, "", "", "") %>
	        </td>
		<%	} %>
		
		<td style="vertical-align: top;" width="30%">
			<%= widgetFactory.createTextField("otehrParty", "SettlementInstruction.otherParty",
				counterPartyName, "35", true, false, false, "", "", "") %>
		</td>
		<% 
			if ( InstrumentType.IMPORT_COL.equals(instrumentType) ) {
		%>
			<td style="vertical-align: top;" width="15%">&nbsp;</td>
		<%	} %>
		<td style="vertical-align: top;" width="22%">&nbsp;</td>
	</tr>
</table>
			
<div style="clear: both;"></div>
  <%@ include file="Transaction-SettlementInstruction-Common_detail.frag" %>	
<div style="clear: both;"></div>

<div class = "columnLeftWithIndent1">
<%=widgetFactory.createLabel("","SettlementInstruction.useFollwingInstr", false, true, false,"","")%>
  <%
    String payinFullFinRollInd = terms.getAttribute("pay_in_full_fin_roll_ind");
    String finCurrency = terms.getAttribute("fin_roll_curr");
    String CurrOptions = Dropdown.createSortedCurrencyCodeOptions(finCurrency,loginLocale); 
    String displayPartialAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fin_roll_partial_pay_amt"), finCurrency, loginLocale);
  %>
  <div class="formItem">
   <%= widgetFactory.createRadioButtonField("payInFullFinRollInd", "PayInFull", "SettlementInstruction.payFullAmount",
					TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_PAY_FULL, TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_PAY_FULL.equals(payinFullFinRollInd), 
					isReadOnly, "", "") %>
   </br>
 					
   <%= widgetFactory.createRadioButtonField("payInFullFinRollInd", "Finance", "SettlementInstruction.finInFinCurr",
					TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_FINANCE, TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_FINANCE.equals(payinFullFinRollInd), isReadOnly, "", "") %>
<%
	if(isReadOnly){
%>
	<%=widgetFactory.createBoldSubLabel(terms.getAttribute("fin_roll_curr"))%>
<%
	}else{
%>	
   <%=widgetFactory.createSelectField("finRollCurr","", " ", CurrOptions, isReadOnly, false,false, 
                   "style=\"width: 50px;\"", "", "none")%>
<%
	}
%>                   
   <div style="clear: both;"></div>
   <div class="formItem">
      <%@ include file="Transaction-SettlementInstruction-Roll-Finance_Detail.frag" %>
   </div>
 </div>
 </br>
</div>

<div class = "columnRightWithIndent1">
   <%@ include file="Transaction-SettlementInstruction-Debit_ACCT_Detail.frag" %>			
</div>
<div style="clear: both;"></div>
   <%@ include file="Transaction-SettlementInstruction-Other_Addl_detaill.frag" %>
