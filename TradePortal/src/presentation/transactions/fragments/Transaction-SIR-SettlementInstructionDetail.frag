<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                Settlement Instructions Detail for IMP_DLC, DBA and DFP

  Description:
    Contains HTML to create FX Detail for Settlement instruction for IMC, DBA and DFP SIR message.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMC-SIR-DBA-SIR-DFP-SIR.jsp" %>
*******************************************************************************
--%>
<%
	presentationAmount.append(currencyCode);
	presentationAmount.append(" ");
	presentationAmount.append(displayAmount);
%>

<table width="100%">
	<tr class="formItem" width="100%">
<% 
	if ( InstrumentType.IMPORT_DLC.equals(instrumentType) ) {
%>
		<td style="vertical-align: top;" width="19%">
			<%= widgetFactory.createTextField("presentationAmount", "MailMessage.PresentationAmount",
				presentationAmount.toString(), "35", true, false, false, "", "", "") %>
		</td>
		<td style="vertical-align: top;" width="19%">
			<%= widgetFactory.createTextField("PresentationNumber", "DiscrepancyResponse.PresentationNumber", terms.getAttribute("drawing_number"),
				"35", true, false, false, "", "", "") %>
		</td>
		<td style="vertical-align: top;" width="17%">
		<%
			String presentationDateSql = "select presentation_date from mail_message where  response_transaction_oid= ?";
			DocumentHandler results = DatabaseQueryBean.getXmlResultSet(presentationDateSql, false, new Object[]{transaction.getAttribute("transaction_oid")});

			if(results != null) {
				presentationDate = results.getAttribute("/ResultSetRecord(0)/PRESENTATION_DATE");
				//The retrieved presentationDate value would be in the format of "2007-10-25 00:00:00.0"
			}
			//As "PresentationDate" is a newly added attribute empty check is being done here to avoid exceptions
			// for existing transactions
			if(InstrumentServices.isNotBlank(presentationDate)) {
		%>
				<%= widgetFactory.createTextField("PresentationDate", "DiscrepancyResponse.PresentationDate",
					TPDateTimeUtility.formatDate(presentationDate, TPDateTimeUtility.SHORT, loginLocale),
					"35", true, false, false, "", "", "") %>
		<% 
			} else {
		%>
				<%= widgetFactory.createTextField("PresentationDate", "DiscrepancyResponse.PresentationDate", "", "35", true, false, false, "", "", "") %>
		<%	}%>
		</td>
<% 	} else {%>
		<td style="vertical-align: top;" width="15%">
			<%= widgetFactory.createTextField("presentationAmount", "SettlementInstruction.amount",
				presentationAmount.toString(), "35", true, false, false, "", "", "") %>
		</td>
		<td style="vertical-align: top;" width="15%">
		<% 
        	if ( instrument != null) {
        		issueDate = instrument.getAttribute("issue_date");
        		if(InstrumentServices.isNotBlank(issueDate)){                                      
        			issueDate = TPDateTimeUtility.formatDate(instrument.getAttribute("issue_date"), TPDateTimeUtility.SHORT, loginLocale);
        		}
        	}
		%>
			<%= widgetFactory.createTextField("issueDate", "MailMessage.IssueDate", issueDate,
                                                    "35", true, false, false, "", "", "") %>
		</td>
		<td style="vertical-align: top;" width="15%">
		<%
			String maturityDateSql = "select funding_date from mail_message where  response_transaction_oid= ?";
			DocumentHandler results = DatabaseQueryBean.getXmlResultSet(maturityDateSql, false, new Object[]{transaction.getAttribute("transaction_oid")});

			if(results != null) {
				maturityDate = results.getAttribute("/ResultSetRecord(0)/FUNDING_DATE");
				//The retrieved presentationDate value would be in the format of "2007-10-25 00:00:00.0"
			}
			//As "PresentationDate" is a newly added attribute empty check is being done here to avoid exceptions
			// for existing transactions
			if(InstrumentServices.isNotBlank(maturityDate)) {
	    %>
				<%= widgetFactory.createTextField("MaturityDate", "SettlementInstruction.maturityDate", TPDateTimeUtility.formatDate(maturityDate, TPDateTimeUtility.SHORT, 
						loginLocale), "35", true, false, false, "", "", "") %>
		<% 
			} else {
		%>
				<%= widgetFactory.createTextField("MaturityDate", "SettlementInstruction.maturityDate", "", "35", true, false, false, "", "", "") %>
		<%	}%>
		</td>
<% 	} %>
		<td style="vertical-align: top;" width="30%">
			<%= widgetFactory.createTextField("otehrParty", "SettlementInstruction.otherParty",
					counterPartyName, "35", true, false, false, "", "", "") %>
		</td>
		<td style="vertical-align: top;" width="18%">&nbsp;</td>
	</tr>
</table>
				
<div style="clear: both;"></div>
  <%@ include file="Transaction-SettlementInstruction-Common_detail.frag" %>
<div style="clear: both;"></div>

<div class = "columnLeftWithIndent1">
<%=widgetFactory.createLabel("","SettlementInstruction.useFollwingInstr", false, true, false,"","")%>
  <%
    String payinFullFinRollInd = terms.getAttribute("pay_in_full_fin_roll_ind");
    String finCurrency = terms.getAttribute("fin_roll_curr");
    String CurrOptions = Dropdown.createSortedCurrencyCodeOptions(finCurrency,loginLocale); 
    String displayPartialAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fin_roll_partial_pay_amt"), finCurrency, loginLocale);
  %>
  <div class="formItem">
   <%= widgetFactory.createRadioButtonField("payInFullFinRollInd", "PayInFull", "SettlementInstruction.payFullAmount",
					TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_PAY_FULL, TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_PAY_FULL.equals(payinFullFinRollInd), 
					isReadOnly, "", "") %>
   </br>
 					
   <%= widgetFactory.createRadioButtonField("payInFullFinRollInd", "Finance", "SettlementInstruction.finInFinCurr",
					TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_FINANCE, TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_FINANCE.equals(payinFullFinRollInd), isReadOnly, "", "") %>
<%
	if(isReadOnly){
%>
	<%=widgetFactory.createBoldSubLabel(terms.getAttribute("fin_roll_curr"))%>
<%
	}else{
%>	
  <%=widgetFactory.createSelectField("finRollCurr","", " ", CurrOptions, isReadOnly, false,false, 
                  "style=\"width: 50px;\"", "", "none")%>
<%
	}
%> 
                   
   <div style="clear: both;"></div>
   <div class="formItem">
      <%@ include file="Transaction-SettlementInstruction-Roll-Finance_Detail.frag" %>
   </div>
 </div>
 </br>
</div>

<div class = "columnRightWithIndent1">
  <%@ include file="Transaction-SettlementInstruction-Debit_ACCT_Detail.frag" %>				
</div>
<div style="clear: both;"></div>
 <%@ include file="Transaction-SettlementInstruction-Other_Addl_detaill.frag" %>
