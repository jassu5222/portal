<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Standby Issue Page - Link section

  Description:
    Contains HTML to display links to the other sections.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-SLC-ISS-Link.jsp" %>
*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#General">
            <%=resMgr.getText("SLCAmend.General", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#BankInstructions">
            <%=resMgr.getText("SLCAmend.InstructionsToBank", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>  
<%
	//Add Amendment Application Form PDF Link if not in admin template mode
	//and the transaction is not started and not deleted...
	if ((!userSession.getSecurityType().equals(TradePortalConstants.ADMIN)) &&
		(!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_STARTED)) &&
		(!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_DELETED)) && 
		(!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK)) ) {
%>
	  <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	       <%
	         linkArgs[0] = TradePortalConstants.PDF_SLC_AMEND_APPLICATION;
	         linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
	         linkArgs[2] = "en_US";
	         //loginLocale;
	         linkArgs[3] = userSession.getBrandingDirectory(); 
	       %>
	       <%= formMgr.getDocumentLinkAsHrefInFrame(resMgr.getText("SLCAmend.AmendApplicationForm",
	      							TradePortalConstants.TEXT_BUNDLE),
	      							TradePortalConstants.PDF_SLC_AMEND_APPLICATION,
	      							linkArgs,
	      							response) %>
	    </p>
	  </td>
<%
  }
%>  	  	  
      <td width="100%" height="30">&nbsp;</td>
    </tr>
  </table>

  <br>
  <br>
