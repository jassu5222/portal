<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Import DLC Issue Page - Bank Defined section

  Description:
    Contains HTML to create the Standby LC Issue Bank Defined section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-SLC-ISS-BankDefined.jsp" %>
*******************************************************************************
--%>

	<div class="columnLeft">
		<%= widgetFactory.createTextField("PurposeType", "SLCIssue.BankDefinedPurposeType", terms.getAttribute("purpose_type").toString(), "3", isReadOnly) %>
		<%
			options = Dropdown.createSortedRefDataOptions( TradePortalConstants.AVAILABLE_WITH_PARTY, terms.getAttribute("available_with_party"), loginLocale);
		%>
		<%= widgetFactory.createSelectField("AvailableWithParty", "SLCIssue.AvailableWithParty", " ", options, isReadOnly) %>
	</div>
	<div class="columnRight">
		<%= widgetFactory.createCheckboxField("DraftsRequired", "SLCIssue.DraftsRequired", terms.getAttribute("drafts_required").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false, "onClick='enableField();'","","") %>
		<%
			options = Dropdown.createSortedRefDataOptions(TradePortalConstants.DRAWN_ON_PARTY, terms.getAttribute("drawn_on_party"), loginLocale);
		%>
		<div class="formItemWithIndent4">
			<%= widgetFactory.createSelectField("DrawnOnParty", "SLCIssue.DrawnOnParty", " ", options, isReadOnly) %>
		</div>
		<%= widgetFactory.createCheckboxField("Irrevocable", "SLCIssue.Irrevocable",  terms.getAttribute("irrevocable").equals(TradePortalConstants.INDICATOR_YES), isReadOnly) %>
		<%= widgetFactory.createCheckboxField("Operative", "SLCIssue.Operative", terms.getAttribute("operative").equals(TradePortalConstants.INDICATOR_YES), isReadOnly) %>
	</div>
	<div style="clear: both"></div>