<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Standby LC Issue Page - Bank Instructions section

  Description:
    Contains HTML to create the Standby LC Issue Bank Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-ISS-BankInstructions.jsp" %>
*******************************************************************************
--%>

	<%
		options = Dropdown.createSortedRefDataOptions(TradePortalConstants.INSTRUMENT_LANGUAGE, instrument.getAttribute("language"), loginLocale);
	%>
	
	<%= widgetFactory.createSelectField("InstrumentLanguage", "SLCIssue.IssueInstrumentIn", " ", options, isReadOnly, true, false, "", "", "") %>
 
	<%
        if (isReadOnly || isFromExpress) {
          out.println("");
        } else {
	          
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
	          
	    defaultText = widgetFactory.createLabel("", "transaction.SelectPhrase", false, false, false, "");
	%>

	<%= widgetFactory.createSelectField("CommInvPhraseItem", "SLCIssue.AdditionalInstructions", defaultText, options, isReadOnly || isFromExpress,false, isExpressTemplate, 
 		"onChange=" + PhraseUtility.getPhraseChange("/Terms/special_bank_instructions", "SpclBankInstructions", textAreaMaxlen,"document.forms[0].SpclBankInstructions"), "", "") %>
  		
		
	<%
		}
	%>
     
	<%= widgetFactory.createTextArea("SpclBankInstructions", "", terms.getAttribute("special_bank_instructions"), isReadOnly || isFromExpress, false, false, "onFocus=defaultSpclBankInstructions(); rows='10' cols='128'", "placeHolder:'Enter any special instructions for your bank'", "", textAreaMaxlen) %>
	<%if(!isReadOnly) {%>
	<%= widgetFactory.createHoverHelp("SpclBankInstructions","InstrumentIssue.PlaceHolderSpecialBankInstr2")%>
	<%} %>
	<div class="columnLeft">
		<%= widgetFactory.createSubsectionHeader("SLCIssue.SettlementInstructions",false, false,false, "") %>
		
		<%= widgetFactory.createTextField("SettleOurAcct", "SLCIssue.DebitOAN", terms.getAttribute("settlement_our_account_num").toString(), "30", 
			isReadOnly, false, false, "class='char20'", "", "") %>
		
		<%= widgetFactory.createTextField("BranchCode", "SLCIssue.BranchCode", terms.getAttribute("branch_code").toString(), "30", 
			isReadOnly, false, false, "class='char20'", "", "") %>
			
		<%= widgetFactory.createTextField("SettleForeignAcct", "SLCIssue.DebitFCA", terms.getAttribute("settlement_foreign_acct_num").toString(), "30", 
			isReadOnly , false, false, "class='char20'", "", "") %>

		<%
			options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("settlement_foreign_acct_curr"), loginLocale);
		%>
			
		<%-- <%= widgetFactory.createCurrencySelectField("SettleForeignCcy", "SLCIssue.CcyofAccount", options, " ",  isReadOnly, false, false) %> --%>
		<%=widgetFactory.createSelectField("SettleForeignCcy","SLCIssue.CcyofAccount", " ", options, isReadOnly, false ,false, "style=\"width: 50px;\"", "", "inline")%>
		<div style="clear:both;"></div>
	</div>
	
	<div class="columnRight">
		<%= widgetFactory.createSubsectionHeader("SLCIssue.CommissionsCharges",false, false,false, "") %>
		
		<%= widgetFactory.createTextField("CandCOurAcct", "SLCIssue.DebitOurAccountNumber", terms.getAttribute("coms_chrgs_our_account_num").toString(), "30", 
			isReadOnly, false, false, "class='char20'", "", "") %>
			
		<%= widgetFactory.createTextField("CandCForeignAcct", "SLCIssue.DebitForeignCurrencyAccountNumber", terms.getAttribute("coms_chrgs_foreign_acct_num").toString(), "30", 
			isReadOnly, false, false, "class='char20'", "", "") %>
		
		<%
			options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("coms_chrgs_foreign_acct_curr"), loginLocale);
		%>		
		
		<%-- <%= widgetFactory.createCurrencySelectField("CandCForeignCcy", "SLCIssue.CcyofAccount", options," ",  isReadOnly, false, false) %> --%>
		<%=widgetFactory.createSelectField("CandCForeignCcy","SLCIssue.CcyofAccount", " ", options, isReadOnly, false,false, "style=\"width: 50px;\"", "", "inline")%>
	<div style="clear:both;"></div>
	</div>
	
	<div style="clear: both"></div><br/>
	
	<%
	    if (isReadOnly || isFromExpress) {
	      out.println("");
	    } else {
	    
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
	        defaultText = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);
	%>
		
	<%= widgetFactory.createSelectField("CommInvPhraseItem1", "SLCIssue.AdditionalInstructions", defaultText, options, isReadOnly || isFromExpress,false, false, 
 		"onChange=" + PhraseUtility.getPhraseChange("/Terms/coms_chrgs_other_text", "CandCOtherText", textAreaMaxlen,"document.forms[0].CandCOtherText"), "", "") %>

    
	<%
		}
	%>
 
 	<%= widgetFactory.createTextArea("CandCOtherText", "", terms.getAttribute("coms_chrgs_other_text"), isReadOnly, false, false, "onFocus=defaultCandCOtherText(); rows='10' cols='128'", "placeHolder:'Enter any additional Settlement or Commissions & Charges instructions'", "",textAreaMaxlen) %>
  	<%if(!isReadOnly) {%>
  	<%= widgetFactory.createHoverHelp("CandCOtherText","InstrumentIssue.PlaceHolderCommissionsChargesInstr")%>
  	<% } %>
  <br>
