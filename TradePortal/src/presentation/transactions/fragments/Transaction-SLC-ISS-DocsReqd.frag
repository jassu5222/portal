<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Standby LC Issue Page - Documents Required section

  Description:
    Contains HTML to create the Standby LC Issue Documents Required section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-SLC-ISS-DocsReqd.jsp" %>
*******************************************************************************
--%>
	
	<%
	if(!isReadOnly){	
	if (!isReadOnly || !isFromExpress) {
	   		options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
	    	defaultText = widgetFactory.createLabel("", "transaction.AddAPhrase", false, false, false, "");
	
	if(!isTemplate){%>
	<div class="formItem required">
	<%}else{%>
	<div class="formItem">
	<%}%>
	<tabel><tr><td>
	
  <%= widgetFactory.createSelectField("AddlDocListPhraseItem", "SLCIssue.Documents", defaultText, options, isReadOnly || isFromExpress,!isTemplate, isExpressTemplate, 
 		"onChange=" + PhraseUtility.getPhraseChange("/Terms/addl_doc_text","AddlDocsText", "6500","document.forms[0].AddlDocsText"), "", "none") %>
	</td><td>
	<%= widgetFactory.createNote("SLCIssue.DocsReqText") %></td>
	</tr></tabel></div>
	<%
		}}else{
	%>	    
	<div class="indent" ><%= widgetFactory.createNote("SLCIssue.DocsReqText") %></div>
	<%}
	if(!isReadOnly){ %>
	<%= widgetFactory.createTextArea("AddlDocsText", "", terms.getAttribute("addl_doc_text"), isReadOnly || isFromExpress, false, false, "rows='10' cols='128'", "", "","") %>
	<%}else{%>
	<%=widgetFactory.createAutoResizeTextArea("AddlDocsText", "", terms.getAttribute("addl_doc_text"), isReadOnly || isFromExpress, false, false, "style='width:600px;;min-height:140px;' cols='128'", "", "") %>
	<%}%>
