<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                Settlement Instructions for DCR 

  Description:
    Contains HTML to create settlement instruction for DCR transaction.  

  This is not a standalone JSP.  It MUST be included in DCR transaction.
 
*******************************************************************************
--%>

<%@ include file="Transaction-SettlementInstruction-Common_detail.frag" %>
<div style="clear: both;"></div>

<div class = "columnLeftWithIndent1">
<%=widgetFactory.createLabel("","SettlementInstruction.useFollwingInstr", false, true, false,"","")%>
  <%
    String payinFullFinRollInd = terms.getAttribute("pay_in_full_fin_roll_ind");
    String finCurrency = terms.getAttribute("fin_roll_curr");
    String CurrOptions = Dropdown.createSortedCurrencyCodeOptions(finCurrency,loginLocale); 
    String displayPartialAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fin_roll_partial_pay_amt"), finCurrency, loginLocale);
  %>
  <div class="formItem">
   <%= widgetFactory.createRadioButtonField("payInFullFinRollInd", "PayInFull", "SettlementInstruction.payFullAmount",
					TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_PAY_FULL, TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_PAY_FULL.equals(payinFullFinRollInd), 
					isReadOnly, "", "") %>
   </br>
 					
   <%= widgetFactory.createRadioButtonField("payInFullFinRollInd", "Finance", "SettlementInstruction.finInFinCurr",
					TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_FINANCE, TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_FINANCE.equals(payinFullFinRollInd), isReadOnly, "", "") %>
<%
	if(isReadOnly){
%>
	<%=widgetFactory.createBoldSubLabel(terms.getAttribute("fin_roll_curr"))%>
<%
	}else{
%>	
  <%=widgetFactory.createSelectField("finRollCurr","", " ", CurrOptions, isReadOnly, false,false, 
                  "style=\"width: 50px;\"", "", "none")%>
<%
	}
%>  
                   
   <div style="clear: both;"></div>
   <div class="formItem">
      <%@ include file="Transaction-SettlementInstruction-Roll-Finance_Detail.frag" %>
   </div>
 </div>
 </br>
</div>

<div class = "columnRightWithIndent1">
    <%@ include file="Transaction-SettlementInstruction-Debit_ACCT_Detail.frag" %>	
</div>
<div style="clear: both;"></div>
   <%@ include file="Transaction-SettlementInstruction-Other_Addl_detaill.frag" %>
