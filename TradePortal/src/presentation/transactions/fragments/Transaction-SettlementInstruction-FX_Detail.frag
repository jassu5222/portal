<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                Settlement Instructions FX Detail 

  Description:
    Contains HTML to create FX Detail for Settlement instruction for SIR, SIM and DSCR transaction.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-SIM.jsp" %>
  <%@ include file="Transaction-IMC-SIM-DBA-SIM-DFP-SIM.jsp" %>
  <%@ include file="Transaction-LRQ-SIR.jsp" %>
  <%@ include file="Transaction-IMC-SIR-DBA-SIR-DFP-SIR.jsp" %>
*******************************************************************************
--%>

  <% 
    String settlefxCurrency = terms.getAttribute("settle_fec_currency");
    String fxCurrOptions = Dropdown.createSortedCurrencyCodeOptions(settlefxCurrency,loginLocale);  
    
	  DocumentHandler settleFXPhraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
	  if (settleFXPhraseLists == null) settleFXPhraseLists = new DocumentHandler();
	
	  DocumentHandler settleFXPhraseList = settleFXPhraseLists.getFragment("/SettlementFXInstr");
	  if (settleFXPhraseList == null) {
	     settleFXPhraseList = PhraseUtility.createPhraseList( TradePortalConstants.PHRASE_CAT_SETTLEMENT, 
	                                    userSession, formMgr, resMgr);
	     settleFXPhraseLists.addComponent("/SettlementFXInstr", settleFXPhraseList);
	  }
	  
	  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, settleFXPhraseLists);
	 
  %>

<%=widgetFactory.createLabel("","SettlementInstruction.applyFollowingFX", false, false, false,"","")%>
<div class="formItem">

  <%= widgetFactory.createRadioButtonField("dailyRateFecOthInd", "DailyRate", "SettlementInstruction.useDailyExchangeRate",
					TradePortalConstants.SETTLEMENT_DAILY_RATE_FEC_TYPE_DAILY, TradePortalConstants.SETTLEMENT_DAILY_RATE_FEC_TYPE_DAILY.equals(terms.getAttribute("daily_rate_fec_oth_ind")), isReadOnly, "", "") %>
      <div style="clear: both;"></div>
   
  <%= widgetFactory.createRadioButtonField("dailyRateFecOthInd", "UseFXContract", "SettlementInstruction.useFXContract",
					TradePortalConstants.SETTLEMENT_DAILY_RATE_FEC_TYPE_FXCONTRACT, TradePortalConstants.SETTLEMENT_DAILY_RATE_FEC_TYPE_FXCONTRACT.equals(terms.getAttribute("daily_rate_fec_oth_ind")), isReadOnly, "", "") %>
      <div style="clear: both;"></div>
      <div class="formItem">
           <%=widgetFactory.createTextField("settleCoveredByFecNum", "SettlementInstruction.FXContractNum",
					terms.getAttribute("settle_covered_by_fec_num"), "14", isReadOnly, false, false,"class='char10'", "", "inline")%>
			<%
                String regExpRange = "[0-9]{0,5}([.][0-9]{0,8})?";
                regExpRange = "regExp:'" + regExpRange + "'"; 
			%>		
	       <%=widgetFactory.createSelectField("settleFecCurrency","SettlementInstruction.currency", " ", fxCurrOptions, isReadOnly, false,false, 
                   "style=\"width: 50px;\"", "", "inline")%>
	       
	       <%=widgetFactory.createTextField("settleFecRate", "SettlementInstruction.rate", terms.getAttribute("settle_fec_rate"), "14", 
                   isReadOnly, false, false, "class='char10'", regExpRange, "inline")%>
      </div>
   <div style="clear: both;"></div>
   
  <%= widgetFactory.createRadioButtonField("dailyRateFecOthInd", "OtherFXDetail", "SettlementInstruction.otherFXInstr",
					TradePortalConstants.SETTLEMENT_DAILY_RATE_FEC_TYPE_OTHER, TradePortalConstants.SETTLEMENT_DAILY_RATE_FEC_TYPE_OTHER.equals(terms.getAttribute("daily_rate_fec_oth_ind")), isReadOnly, "", "") %>
<%
	String FXInstrOptions = ListBox.createOptionList(settleFXPhraseList,
				"PHRASE_OID", "NAME", "", userSession.getSecretKey());
	String defaultFXSettleInstrText = resMgr.getText("transaction.SelectPhrase",
				TradePortalConstants.TEXT_BUNDLE);
%>
</br></br>
  <%=widgetFactory.createSelectField("settlementFXPhraseDropDown", "", defaultFXSettleInstrText, FXInstrOptions,isReadOnly,
						false,false,
						"onChange="+ PhraseUtility.getPhraseChange("/Terms/settle_other_fec_text","settleOtherFecText", "5000",
												"document.forms[0].settleOtherFecText"),"", "")%>
  <div style="clear: both;"></div>											
 <%=widgetFactory.createTextArea("settleOtherFecText", "",
					terms.getAttribute("settle_other_fec_text"),
					isReadOnly, false, false, "maxlength=210 rows='10' cols='250'", "", "")%>

</div>