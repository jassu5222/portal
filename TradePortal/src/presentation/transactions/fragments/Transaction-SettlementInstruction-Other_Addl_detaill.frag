<%=widgetFactory.createCheckboxField("otherAddlInstructionsInd", "SettlementInstruction.otherAdditionalBelow",
										TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("other_addl_instructions_ind")), isReadOnly, false,"","", "")%>
<%   
	  DocumentHandler settleDescrPhraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
	  if (settleDescrPhraseLists == null) settleDescrPhraseLists = new DocumentHandler();
	
	  DocumentHandler settleDescrPhraseList = settleDescrPhraseLists.getFragment("/SettlementDescrInstr");
	  if (settleDescrPhraseList == null) {
	     settleDescrPhraseList = PhraseUtility.createPhraseList( TradePortalConstants.PHRASE_CAT_SETTLEMENT, 
	                                    userSession, formMgr, resMgr);
	     settleDescrPhraseLists.addComponent("/SettlementDescrInstr", settleDescrPhraseList);
	  }
	  
	  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, settleDescrPhraseLists);
	
	String descrSettleOtherInstrOptions = ListBox.createOptionList(settleDescrPhraseList,
				"PHRASE_OID", "NAME", "", userSession.getSecretKey());
	String defaultdescrSettleOtherText = resMgr.getText("transaction.SelectPhrase",
				TradePortalConstants.TEXT_BUNDLE);
%>
  
  <%=widgetFactory.createSelectField("descrSettleOtherInstrDropDown", "SettlementInstruction.additionalInstr", defaultdescrSettleOtherText, 
                         descrSettleOtherInstrOptions,isReadOnly,false,false,
						"onChange="+ PhraseUtility.getPhraseChange("/Terms/other_addl_instructions","otherAddlInstructions", "5000",
												"document.forms[0].otherAddlInstructions"),"", "")%>
  <div style="clear: both;"></div>											
 <%=widgetFactory.createTextArea("otherAddlInstructions", "",
					terms.getAttribute("other_addl_instructions"),
					isReadOnly, false, false, "maxlength=1000 rows='10' cols='250'", "", "")%>