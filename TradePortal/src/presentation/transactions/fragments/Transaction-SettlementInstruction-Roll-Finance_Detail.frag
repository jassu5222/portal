<%

  String finRolloverRemainBalLabel;
  String finRolloverTermsLabel;
  String finRolloverForLabel;
  
  if ( InstrumentType.LOAN_RQST.equals(instrument.getAttribute("instrument_type_code")) ) {
     finRolloverRemainBalLabel  = resMgr.getText("SettlementInstruction.rolloverRemainingBal", TradePortalConstants.TEXT_BUNDLE);
     finRolloverTermsLabel      = resMgr.getText("SettlementInstruction.rolloverTerms", TradePortalConstants.TEXT_BUNDLE);
     finRolloverForLabel        = resMgr.getText("SettlementInstruction.rolloverFor", TradePortalConstants.TEXT_BUNDLE);
  } else {
     finRolloverRemainBalLabel = resMgr.getText("SettlementInstruction.finRemainingBal", TradePortalConstants.TEXT_BUNDLE);
     finRolloverTermsLabel     = resMgr.getText("SettlementInstruction.finTerms", TradePortalConstants.TEXT_BUNDLE);
     finRolloverForLabel       = resMgr.getText("SettlementInstruction.finFor", TradePortalConstants.TEXT_BUNDLE);
  }
  secureParms.put("transactionCurrency", transaction.getAttribute("copy_of_currency_code") );
%>      
      
<%= widgetFactory.createRadioButtonField("finRollFullPartialInd", "ForFull", "SettlementInstruction.forFullAmount",
					TradePortalConstants.SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_FULL, TradePortalConstants.SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_FULL.equals(terms.getAttribute("fin_roll_full_partial_ind")), isReadOnly, "", "") %>
 <div style="clear: both;"></div>
 <%= widgetFactory.createRadioButtonField("finRollFullPartialInd", "ForPartial", "SettlementInstruction.payPartialAmount",
					TradePortalConstants.SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_PARTIAL, 
					TradePortalConstants.SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_PARTIAL.equals(terms.getAttribute("fin_roll_full_partial_ind")), isReadOnly, "", "") %>
 </br></br>
<div class="formItemWithIndent2">
	 <%=widgetFactory.createSubLabel("SettlementInstruction.paymentAmount")%>
	 <%= widgetFactory.createTextField("instrumentCurr", "",transaction.getAttribute("copy_of_currency_code"),
	      "3", true, true, false, "", "", "none") %>
	 <%= widgetFactory.createAmountField("finRollPartialPayAmt", "", displayPartialAmount, finCurrency, isReadOnly, false, false,"style=\"width: 126px;\"", "", "none") %>
	 <div style="clear: both;"></div>
	 <%=widgetFactory.createLabel("",finRolloverRemainBalLabel,false,false,false,"","none")%>
 </div>
	   
 <div style="clear: both;"></div>
 <%=widgetFactory.createLabel("",finRolloverTermsLabel,false,false,false,"","")%>
 <div class="formItem">
	   <%= widgetFactory.createRadioButtonField("finForDaysOrDate", "FinForDays", finRolloverForLabel,
					TradePortalConstants.SETTLEMENT_ROLLOVER_TERMS_DAYS, TradePortalConstants.SETTLEMENT_ROLLOVER_TERMS_DAYS.equals(terms.getAttribute("rollover_terms_ind")), isReadOnly, "", "") %>

<%
	if(isReadOnly){
%>
	<%=widgetFactory.createBoldSubLabel(terms.getAttribute("fin_roll_number_of_days"))%>
<%
	}else{
%>
	   <%=widgetFactory.createNumberField("finRollNumberOfDays", "",terms.getAttribute("fin_roll_number_of_days"), "3",isReadOnly, false, false,"", "", "none")%>
<%
	}
%>
	   <%=widgetFactory.createLabel("","SettlementInstruction.days",false,false,false,"","none")%>
	   <div style="clear: both;"></div>
	   <%= widgetFactory.createRadioButtonField("finForDaysOrDate", "FinAtMaturityDate", "",
					TradePortalConstants.SETTLEMENT_ROLLOVER_TERMS_MATURITY_DATE, TradePortalConstants.SETTLEMENT_ROLLOVER_TERMS_MATURITY_DATE.equals(terms.getAttribute("rollover_terms_ind")), isReadOnly, "", "") %>
	   <%=widgetFactory.createSubLabel("SettlementInstruction.atFixedMaturityDate")%>
<%
	if(isReadOnly){
%>
	<%=widgetFactory.createBoldSubLabel(TPDateTimeUtility.formatDate(StringFunction.xssHtmlToChars(terms.getAttribute("fin_roll_maturity_date")), TPDateTimeUtility.SHORT, loginLocale))%>
<%
	}else{
%>
	  <%=widgetFactory.createDateField("finRollMaturityDate","",StringFunction.xssHtmlToChars(terms.getAttribute("fin_roll_maturity_date")), isReadOnly,false,false,"class='char10 formItemWithIndent3'",dateWidgetOptions, "none")%>
<%
	}
%>
</div>