<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
            Transaction Terms Detail Page - Terms Summary section

  Description:   
    This jsp is only designed to display the Terms Summary Section.  This
  jsp is called by possibly 2 different jsp's : The Transaction-TermsDetails.jsp
  and the Transaction-CurrentTerms.jsp. This is designed to be called by:
  	<%@ include file="Transaction-Terms-Summary.jsp" %>
  Because it is called this way, it is assumed that various variables are 
  defined.  Most noteably the isFromCurrentTerms and instrumentType are
  critical to bringing up this page with out error.  Also important are the 
  setup of the following webBeans: Instrument, Terms, TermsParty (1 and 2),
  and the transaction.
  Please Note that the Download LC Terms button is only meant to be displayed 
  if we're coming from the Current Terms.jsp and the instrumentType is 
  an Export LC.  The actual execution of the Download button is still to be 
  determined, right now it will just log the user off since it goes to a 
  place that's not defined in the NavMgr file.
*******************************************************************************
--%>


<%	
     /***************************************************************************************************
      * Start of Terms Summary Section 
      *********************************/           Debug.debug("****** Start of Terms Summary Section ******");

  TermsWebBean CustTerms = null;
  String amount;
  String tolerancePos;
  String toleranceNeg;
  String pmtTerms;
  String daysAfterType;
  String daysAfterNum;
  String fixedMatDate;
  String returnAction;
  String label = "";
  boolean maturityFlag = false;		//determines whether to display Expiry Date or Fixed Maturity Date.
  boolean doNotDisplayAmountSections = false;
  
  String instrumentTypeCode = instrument.getAttribute("instrument_type_code");
  String confirmationInd = instrument.getAttribute("copy_of_confirmation_ind");
  String referenceNumber = instrument.getAttribute("copy_of_ref_num");

  // IF the current Instrument Type is a Documentary_BA
  if( instrumentType.equals(InstrumentType.DOCUMENTARY_BA )) {

	maturityFlag = true;
  }
%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
      <td colspan=9>&nbsp;</td>
    </tr>
<%if(!isBILInstrument){ // 08/10/2015 -R94 -  CR 932%>
    <tr>
      <td nowrap>
      <%
      amount = transaction.getAttribute("instrument_amount");
	  currencyCode = transaction.getAttribute("copy_of_currency_code");
	  amount = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);

	  if (!InstrumentServices.isBlank(amount) ) { %>
      		<%= widgetFactory.createTextField("InstAmount", "TransactionTerms.InstAmount", currencyCode + "   " + amount, "25", true, false,false, "", "", "inline")%>
      <% } else { %>
      		<%= widgetFactory.createTextField("InstAmount", "TransactionTerms.InstAmount", "", "25", true, false,false, "", "", "inline")%>
      <% } %>				
      		
      </td>
      
	<%
	
	// MEer CR-1026 
	 if(userSession.isCustNotIntgTPS()){
    	  if(InstrumentServices.isValidInstrumentTypeForBTMU(instrumentOid,instrumentTypeCode))
    		 	doNotDisplayAmountSections = true;	 
     }
	
 	   
   if (!instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) &&
       !instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) &&
       !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) { %>
      <td nowrap>
      <%
      amount = transaction.getAttribute("available_amount");
      amount = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);
      if(!doNotDisplayAmountSections){
    	  if (!InstrumentServices.isBlank(amount) ) { %>
				<%= widgetFactory.createTextField("AvailableAmount", "TransactionTerms.AvailableAmount", currencyCode + "   " + amount, "25", true, false,false, "", "", "inline")%>
				<% } else { %>
				<%= widgetFactory.createTextField("AvailableAmount", "TransactionTerms.AvailableAmount", "", "25", true, false,false, "", "", "inline")%>
				<% }
      } %>
      
      </td>
<% } %>
	<% 
   if (!instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) &&
       !instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) &&
       !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) { %>
      <td nowrap>
      <%
      amount = transaction.getAttribute("equivalent_amount");
	  currencyCode = transaction.getAttribute("base_currency_code");
	  amount = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);
	  if(!doNotDisplayAmountSections){
	  	if (!InstrumentServices.isBlank(amount) ) { %>
			<%= widgetFactory.createTextField("EquivalentAmount", "TransactionTerms.EquivalentAmount", currencyCode + "   " + amount, "25", true, false,false, "", "", "inline")%>
     	 <% } else { %>
      		<%= widgetFactory.createTextField("EquivalentAmount", "TransactionTerms.EquivalentAmount", "", "25", true, false,false, "", "", "inline")%>
     	 <% } 
     	 }	 %>

      </td>
<% } %>
      <td nowrap>
           <%
            boolean displayAboutPercent;
 
	    try
             {
	      tolerancePos = terms.getAttribute("amt_tolerance_pos");
	      toleranceNeg = terms.getAttribute("amt_tolerance_neg");

	     }
            catch(Exception e)
             {
	       tolerancePos = "";
	       toleranceNeg = "";
	     }

	    if ((!InstrumentServices.isBlank(tolerancePos)) &&
	        (!InstrumentServices.isBlank(toleranceNeg)))
                 displayAboutPercent = true;
            else
                 displayAboutPercent = false;

              if(displayAboutPercent && !isFundsXfer && !instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) && !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
            	  String percentValue = resMgr.getText("transaction.Plus", 
                          TradePortalConstants.TEXT_BUNDLE)
				         + tolerancePos
				         + resMgr.getText("transaction.Percent", 
				                          TradePortalConstants.TEXT_BUNDLE)
				         + " " 
				         + resMgr.getText("transaction.Minus", 
				                          TradePortalConstants.TEXT_BUNDLE)
				         + toleranceNeg
				         + resMgr.getText("transaction.Percent", 
				                          TradePortalConstants.TEXT_BUNDLE);
				%>
              <%= widgetFactory.createTextField("AboutPercent", "TransactionTerms.About", percentValue, "25", true, false,false, "", "", "inline")%>
              <% }%>

      </td>
      
    </tr>
  <%} //08/10/2015 R 94 CR 932 end%>  
      
<% if (!isFundsXfer && !instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)) { // only print top two rows if not a Funds Transfer %>
    <tr>
      <td nowrap>
        <%
          if (isLoanRqst || isRFInstrument) {
             label = resMgr.getText("TransactionTerms.LoanStartDate", 
                                    TradePortalConstants.TEXT_BUNDLE);
		  }if (isBILInstrument) { // 08/10/2015   - R 94  -CR 932
  			 label = resMgr.getText("Billing.StartDate", 
                                    TradePortalConstants.TEXT_BUNDLE);
  		   } else {
             label = resMgr.getText("TransactionTerms.IssueDate", 
                                    TradePortalConstants.TEXT_BUNDLE);
          }
%>
          <%= widgetFactory.createDateField("Date1", label, instrument.getAttribute("issue_date"), true, false, false, "", "", "inline")%>
	
      </td>
      
      <td nowrap>
        
<%
          String dateLabel = "";
          if (isLoanRqst || isRFInstrument) {
             dateLabel = resMgr.getText("TransactionTerms.MaturityDate", 
                                        TradePortalConstants.TEXT_BUNDLE);
          }
          else if( maturityFlag ) {
             dateLabel = resMgr.getText("TransactionTerms.MaturityDate", 
                                        TradePortalConstants.TEXT_BUNDLE);
          } 
          else if (instrumentTypeCode.equals(InstrumentType.GUARANTEE)) {
             dateLabel = resMgr.getText("InstrumentSummary.ValidityDate", 
                                        TradePortalConstants.TEXT_BUNDLE);
          }else if (isBILInstrument) { // 08/10/2015 R94 - CR 932
  			 dateLabel = resMgr.getText("Billing.CloseDate", 
                                    TradePortalConstants.TEXT_BUNDLE);
  		  } else if (!instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
             dateLabel = resMgr.getText("TransactionTerms.ExpiryDate", 
                                        TradePortalConstants.TEXT_BUNDLE);
          }
          if (instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
    	      date = "";
    	  }else if(isLoanRqst){
    		  date =  terms.getAttribute("loan_terms_fixed_maturity_dt");
    	  }else {		
    		date = instrument.getAttribute("copy_of_expiry_date");
    	  }
%>
          <%= widgetFactory.createDateField("Date2", dateLabel, date, true, false, false, "", "", "inline")%>
	
      </td>
  
  
  	<% if (!isBILInstrument){ // 08/10/2015 R94  CR 932    %>
      <td nowrap>
       
	<%
          if (isLoanRqst || isRFInstrument || instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
             label = "&nbsp;";
          }
          else {
             label = resMgr.getText("TransactionTerms.LatestShipDate", 
                                    TradePortalConstants.TEXT_BUNDLE);
          }
		
		  int shipmentCount = -1;

			try{
			     if (isLoanRqst || isRFInstrument || instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
			         date = "";
			     }
			     else {
			         // Sometimes there are multiple shipments, each with their own latest shipment date
			         // In that situation, just display the word "Multiple"
			         shipmentCount = DatabaseQueryBean.getCount("shipment_oid", "shipment_terms", 
			                                                    "p_terms_oid = ? ", false, new Object[]{terms.getAttribute("terms_oid")});
			
			         if(shipmentCount <= 1)
			          {
			             terms.populateFirstShipmentFromDatabase();
			             date = terms.getFirstShipment().getAttribute("latest_shipment_date");
			          }
			     }
			}catch(Exception e){
			   date = "";
			 }
          if (!InstrumentServices.isBlank(date) && (shipmentCount <= 1)) { %>
	          <%= widgetFactory.createDateField("Date3", label, date, true, false, false, "", "", "inline")%>
		  <% } else if (shipmentCount > 1) { %> 
		       <%=resMgr.getText("TransactionTerms.MultipleShipments", TradePortalConstants.TEXT_BUNDLE)%>
		  <% } else if(InstrumentServices.isBlank(date)) {%>
				<%= widgetFactory.createDateField("Date3", label, "", true, false, false, "", "", "inline")%>
		  <% } %>		
      </td>
      <td nowrap><%
      if((instrumentTypeCode != null)
      	&& (confirmationInd != null)
      	&& (instrumentTypeCode.equals(InstrumentType.INCOMING_SLC) 
      		|| instrumentTypeCode.equals(InstrumentType.EXPORT_DLC)
      		|| instrumentTypeCode.equals(InstrumentType.INCOMING_GUA)))      
      {
    	  if(instrument.getAttribute("copy_of_confirmation_ind").equals(TradePortalConstants.INDICATOR_YES))
        	{%>
        		<%= widgetFactory.createTextField("ConfirmedText", "TransactionTerms.Confirmed", resMgr.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE), "30", true, false,false, "", "", "inline")%>
        	<%}
        	else
        	{%>
    			<%= widgetFactory.createTextField("ConfirmedText", "TransactionTerms.Confirmed", resMgr.getText("common.No", TradePortalConstants.TEXT_BUNDLE), "30", true, false,false, "", "", "inline")%>
    		<%}
        	
      	%>
      		
      			
      	</td><%
      }%>
   <%}// 08/10/2015 R 94 CR 932 END%>
    </tr>
    
<% } %>

 <% if (!isBILInstrument){ // 08/10/2015 R94  CR 932    %>
<tr>
	<td nowrap>
	    <%try{
	    //Because the Payment Terms may not be registered, it was put into this try/catch block
  	    //to prevent an exception from creating problems.  Also, since the Payment Terms Type
 	    //has types in it that require multiple pieces of data, an if block was the only way to 
	    //prep the data before sending out to display.

	    pmtTerms = terms.getAttribute("pmt_terms_type");

            if (isLoanRqst || isRFInstrument) {
	       if( pmtTerms.equals( TradePortalConstants.LOAN_DAYS_AFTER ) ) {
	   	  daysAfterNum  = terms.getAttribute("loan_terms_number_of_days");

		  //this will prevent an empty terms.pmt_terms_days_after_type from throwing
		  //an exception thus blanking out the built string.
		  try{
		    daysAfterType = ReferenceDataManager.getRefDataMgr().getDescr(
                                                      TradePortalConstants.LOAN_TERMS_TYPE,
						      pmtTerms);
		  }catch(Exception e){
		 	daysAfterType = "";
		  }
		  pmtTerms = daysAfterNum + "  "
                           + resMgr.getText("TransactionTerms.Days", TradePortalConstants.TEXT_BUNDLE)
                           + " " + daysAfterType;

	       }else if( pmtTerms.equals( TradePortalConstants.LOAN_FIXED_MATURITY ) ) {
		  pmtTerms = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.LOAN_TERMS_TYPE,
						                  	 pmtTerms);
		  date = terms.getAttribute("loan_terms_fixed_maturity_dt");
		  date = TPDateTimeUtility.formatDate( date, TPDateTimeUtility.LONG, loginLocale );
		  pmtTerms = pmtTerms + "  " + date;

	       }else{
		  pmtTerms = "";
	       }
            } else {
	       if( (pmtTerms.equals( TradePortalConstants.PMT_SIGHT ))        ||
	           (pmtTerms.equals( TradePortalConstants.PMT_CASH_AGAINST )) ||
	           (pmtTerms.equals( TradePortalConstants.PMT_OTHER )) ) {
	    	// IR T36000048415 Rel9.5 05/03/2016 - Passing locale parameter
   	 	     pmtTerms = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.PMT_TERMS_TYPE,
						                  	 pmtTerms,loginLocale);

	       }else if( pmtTerms.equals( TradePortalConstants.PMT_DAYS_AFTER ) ) {
	   	  daysAfterNum  = terms.getAttribute("pmt_terms_num_days_after");
	 	  daysAfterType = terms.getAttribute("pmt_terms_days_after_type");

		  //this will prevent an empty terms.pmt_terms_days_after_type from throwing
		  //an exception thus blanking out the built string.
		  try{
		    daysAfterType = ReferenceDataManager.getRefDataMgr().getDescr(
                                                      TradePortalConstants.PMT_TERMS_DAYS_AFTER,
						      daysAfterType);
		  }catch(Exception e){
		 	daysAfterType = "";
		  }
		  pmtTerms = resMgr.getText("TransactionTerms.DaysAfter", TradePortalConstants.TEXT_BUNDLE);
		  pmtTerms = daysAfterNum + "  " + pmtTerms + " " + daysAfterType;

	       }else if( pmtTerms.equals( TradePortalConstants.PMT_FIXED_MATURITY ) ) {
	    	// IR T36000048415 Rel9.5 05/03/2016 - Passing locale parameter
		  pmtTerms = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.PMT_TERMS_TYPE,
						                  	 pmtTerms,loginLocale);
		  date = terms.getAttribute("pmt_terms_fixed_maturity_date");
		  date = TPDateTimeUtility.formatDate( date, TPDateTimeUtility.LONG, loginLocale );
		  pmtTerms = pmtTerms + "  " + date;

	       }else{
		  pmtTerms = "";
	       }
            }

	    }catch(Exception e){
	        pmtTerms = "";
	    }

            if (isLoanRqst || isRFInstrument) {%>
            	<%
            	String loanTermsType = null;
    			if (termsForLoan != null) 
    			{
    			    loanTermsType = termsForLoan.getAttribute("loan_terms_type");
    			}
    			if (loanTermsType != null) 
    			{
    			  if (loanTermsType.equals(TradePortalConstants.LOAN_DAYS_AFTER)) {
    			%>
                    <%= widgetFactory.createTextField("PmtTerms", "TransactionTerms.LoanTerms", termsForLoan.getAttribute("loan_terms_number_of_days")+" " + resMgr.getText("LoanRequest.DaysFrom", TradePortalConstants.TEXT_BUNDLE), "60", true, false,false, "", "", "inline")%>
                  <%} else {
    				date = termsForLoan.getAttribute("loan_terms_fixed_maturity_dt"); //PUUH062751147
    				date = TPDateTimeUtility.formatDate( date, TPDateTimeUtility.LONG, loginLocale );
    				%>
                    <%=widgetFactory.createTextField("PmtTerms", "TransactionTerms.LoanTerms", resMgr.getText("LoanRequest.AtFixedMaturityDate",TradePortalConstants.TEXT_BUNDLE)+ " " +date, "60", true, false,false, "", "", "inline")%>
    			<%
    			  }
    			 }		
    			%>
            <%} else if (isFundsXfer || instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) || instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
            if(referenceNumber == null) referenceNumber = "";
			%>
				<%= widgetFactory.createTextField("PmtTerms", "TransactionTerms.YourReferenceNumber", referenceNumber, "60", true, false,false, "", "", "inline")%>
			<% } else if(!pmtTerms.equals("")) { %>
               <%= widgetFactory.createTextField("PmtTerms", "TransactionTerms.PaymentTerms", pmtTerms, "60", true, false,false, "", "", "inline")%>
            <% } else{%>
            	<%= widgetFactory.createTextField("PmtTerms", "TransactionTerms.PaymentTerms", "", "60", true, false,false, "", "", "inline")%>	
           <% }
            %>	
	</td>
	 <%-- Narayan CR 831 12-Jan-2014 Rel9.0 Begin --%>
    <td>
	<%	
	if(StringFunction.isNotBlank(transaction.getAttribute("c_BankReleasedTerms")) && StringFunction.isNotBlank(terms.getAttribute("final_expiry_date"))){
		date = TPDateTimeUtility.formatDate( terms.getAttribute("final_expiry_date"), TPDateTimeUtility.SHORT, loginLocale );	
	%>	
		<%= widgetFactory.createTextField("FinalExpiryDate", "TermsBeanAlias.final_expiry_date", date, "60", true, false,false, "", "", "inline")%>	
    <%} %>
	</td>
	 <%-- Narayan CR 831 12-Jan-2014 Rel9.0 End --%>
</tr>
<%} // 08/10/2015 R94 CR 932%>
<tr>
 <%
   
    if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) ||
        instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) ||
        instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
        //out.print("&nbsp;");
    } else {
          if(!(isLoanRqst || isFundsXfer || isRFInstrument)) {%>
          <td nowrap>
             <%= widgetFactory.createTextField("YourReferenceNumber", "InstrumentSummary.YourReferenceNumber", referenceNumber, "30", true, false, false, "", "", "inline")%>
          </td>   
         <%} 
    }      
   %>
     
</tr></table>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">	
 <tr>
  <td nowrap>
 <% System.out.println("XXX"+termsPartyApplicant.buildAddress(false, true, resMgr).trim()+"XXX");
 //Srinivasu_D IR#T36000019515 Rel8.4 01/20/2014 - Changed the text area to acoomodate more text to avoid scrollbars.
 //if(!("&nbsp;").equals(termsPartyApplicant.buildAddress(false, true, resMgr).trim())) {%>
  			<%if(instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY)) { %>
          <%--   	<%= widgetFactory.createTextArea("Applicant", "TransactionTerms.Buyer", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "style='width:auto;' rows='4' cols='35'", "", "inline")%>  --%>

				<%= widgetFactory.createAutoResizeTextArea("Applicant", "TransactionTerms.Buyer", termsPartyApplicant.buildAddress(false, false, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>

             <%} else if (instrumentTypeCode.equals(InstrumentType.FUNDS_XFER)) {%>
	       <%--      <%= widgetFactory.createTextArea("Applicant", "TransactionTerms.Applicant", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "style='width:auto;' rows='4' cols='35'", "", "inline")%>   --%>

				  <%= widgetFactory.createAutoResizeTextArea("Applicant", "TransactionTerms.Applicant", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>

	     	 <%} else if(instrumentTypeCode.equals(InstrumentType.LOAN_RQST)) {%>
	     <%--        <%= widgetFactory.createTextArea("Applicant", "TransactionTerms.Borrower", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "style='width:auto;' rows='4' cols='35'", "", "inline")%>  --%>

				  <%= widgetFactory.createAutoResizeTextArea("Applicant", "TransactionTerms.Borrower", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>

             <%} else if(instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)) { %>
	       <%--      <%= widgetFactory.createTextArea("Applicant", "TransactionTerms.DebitParty", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "style='width:auto;' rows='4' cols='35'", "", "inline")%>  --%>

				 <%= widgetFactory.createAutoResizeTextArea("Applicant", "TransactionTerms.DebitParty", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>

             <%}else if (isBILInstrument) {// 08/10/2015 R94  CR 932  %>
				 <%= widgetFactory.createAutoResizeTextArea("Applicant", "Billing.BillCustomerDetails", termsBillCustomer.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
             <%} else { %>
				 <%= widgetFactory.createAutoResizeTextArea("Applicant", "TransactionTerms.Applicant", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
             <%}
 //}	%>
 </td>
 <td nowrap> 
 		<%if (isLoanRqst || isRFInstrument) {%>
         	
      	<% } else {
          	if(instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY)) {
				%>
           <%--  <%= widgetFactory.createTextArea("Beneficiary", "TransactionTerms.Seller", termsPartyBeneficiary.buildAddress(false, false, resMgr), true, false,false, "style='width:auto;' rows='4' cols='35'", "", "inline")%> --%>
			  
			  <%= widgetFactory.createAutoResizeTextArea("Beneficiary", "TransactionTerms.Seller", termsPartyBeneficiary.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>

           <%}else if(instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)) {%>
		   <%--
             <%= widgetFactory.createTextArea("Beneficiary", "TransactionTerms.Payee", termsPartyBeneficiary.buildAddress(false, true, resMgr), true, false,false, "rows='4' cols='35'", "", "inline")%> --%>

			   <%= widgetFactory.createAutoResizeTextArea("Beneficiary", "TransactionTerms.Payee", termsPartyBeneficiary.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>

           <%} else if (!isBILInstrument) {// 08/10/2015 R94  CR 932 
			   %>
			   <%= widgetFactory.createAutoResizeTextArea("Beneficiary", "TransactionTerms.Beneficiary", termsPartyBeneficiary.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
           <%}
          }
         %>   
    
 </td>
 </tr> 
 </table> 

<%
     /***************************************************************************************************
      * END of Terms Summary Section 
      *********************************/

     Debug.debug("****** END of Terms Summary Section ******");
%>
