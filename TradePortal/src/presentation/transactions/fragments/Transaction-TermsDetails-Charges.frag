<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
       Transaction Terms Detail Page - Commissions and Charges Section

  Description:     
    This jsp's sole function is to display the Commissions and Charges section 
  which is essentially displaying data in a list view format.  This jsp is 
  called by:  	<%@ include file="Transaction-TermsDetails-Charges.jsp" %>
  There are 2 essential assumptions made for this page to work properly:
  There is a transaction webBean that can be reached to retrieve data from &
  that a resMgr is available.
  Please note to get data - a DatabaseQueryBean is used to get data into a
  DocumentHandler format.  Then by creating a vector from the Fragments of the 
  DocHandler we scroll through the fragments to display the data.  The hard
  part of this is building the sql statement, after that the rest is pretty
  much straight HTML.
*******************************************************************************
--%>

<%
     /***************************************************************************************************
      * Start of Commissions And Charges Section 
      *********************************/  Debug.debug("****** Start of Commissions And Charges Section ******");

  int    xMod;
%>



<table class="formDocumentsTable" >
<%--	Naveen 22-August-2012. IR-T36000004626. Added the table header tag and modified TD's as TH's. Also modified the property in second TH- START	--%>
	<thead>
		<th><%= widgetFactory.createSubLabel("TransactionTerms.ChargeType")%></th>
		<th><%= widgetFactory.createSubLabel("TransactionTerms.CCY")%></th>
		<th><%= widgetFactory.createSubLabel("TransactionTerms.Amount")%></th>
		<th><%= widgetFactory.createSubLabel("TransactionTerms.SettlementMethod")%></th>
		<th><%= widgetFactory.createSubLabel("TransactionTerms.AccountNo")%></th>
	</thead>
<%--	Naveen 22-August-2012. IR-T36000004626. Added the table header tag and modified td's as th's- END	--%>
    <%
     /****************************
      * Building list data (loop)
      ***************************/		Debug.debug("*** Building List data - loop ***");


  try{
	 query = new StringBuffer();

	 query.append("select charge_type, settlement_curr_code, settlement_curr_amount, ");
	 query.append("settlement_how_type, acct_number ");
	 query.append("from fee ");
	 query.append("where p_transaction_oid = ?");
	 //jgadela  R90 IR T36000026319 - SQL FIX
	 Object[] sqlParamsSett = new Object[1];
	 sqlParamsSett[0] =  transaction.getAttribute("transaction_oid");

   	 Debug.debug("Query is : " + query);
	 dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), true, sqlParamsSett);

	 if(dbQuerydoc != null)
        {
         Debug.debug("**** dbQuerydoc -- > " + dbQuerydoc.toString());

 	   listviewVector = dbQuerydoc.getFragments("/ResultSetRecord/");
        }
	 else
	  {
	   listviewVector = new Vector(1);
	  }

	//This loop is designed to scroll through the ResultSetRecords and display the data.
	Debug.debug("Walk the tree to display the text in the listview.");

 %>
 	<tbody>	
	<% for( int x=0; x < listviewVector.size(); x++)
	{

 	   Debug.debug("**** listview Vector : index "+ x +" : " + listviewVector.elementAt(x).toString() );
	   xMod = x % 2;
	   myDoc = ((DocumentHandler)listviewVector.elementAt(x));

 
%>
<%--	Naveen 22-August-2012. IR-T36000004626. Added the table body opening tag	--%>
     
	 <tr>
	     <td>

<%
	   	  attribute   = myDoc.getAttribute("/CHARGE_TYPE");
	   	  try {
 	   	  	displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CHARGE_TYPE,
						                  	      attribute,loginLocale);
          }
	   	  // W Zhu 3/10/08 NCUH121502535 begin 
	   	  // Refresh ReferenceDataManager if charge type is already in the db but not in the cache.
	   	  // This could happen if a new charge type is sent from OTL.  Usually the the unpackager would refresh the cache but
	   	  // that does not work if the agent is on a different weblogic server.
          catch (AmsException e) {
         	 //jgadela  R90 IR T36000026319 - SQL FIX
			  Object[] sqlParamsRefDataCnt = new Object[2];
			  sqlParamsRefDataCnt[0] =  TradePortalConstants.CHARGE_TYPE;
			  sqlParamsRefDataCnt[1] =  attribute;
       	      displayText = attribute; // Display charge_type code value instead of blank.
	          int refdataCount = DatabaseQueryBean.getCount("code", "refdata", "table_type = ? and locale_name is null and code = ?", true, sqlParamsRefDataCnt);
              if (refdataCount > 0 ) {
	              ReferenceDataManager.getRefDataMgr().refresh();
			   	  try {
 	   				  	displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CHARGE_TYPE,
						                  	      attribute,loginLocale);
	        	  }
	       		  catch (AmsException e2) {
	       		  displayText ="";
       			  }
       		  }
          }
	   	  // W Zhu 3/10/08 NCUH121502535 end
        	   


%>		  <%= displayText %>
		 </td>

<td>

<%
		  displayText  = myDoc.getAttribute("/SETTLEMENT_CURR_CODE");
		  currencyCode = displayText;
		  
%>		  <%= displayText %>
		
              </td>

<td>

<%
		  attribute   = myDoc.getAttribute("/SETTLEMENT_CURR_AMOUNT");
		  displayText = TPCurrencyUtility.getDisplayAmount(attribute, currencyCode, loginLocale);

%>		  <%= displayText %>
		</td>

<td nowrap>

<%
		  attribute   = myDoc.getAttribute("/SETTLEMENT_HOW_TYPE");
		  try{
			  // IR T36000048415 Rel9.5 05/03/2016 - Passing locale parameter
		  displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.SETTLEMENT_HOW_TYPE,
						                  	      attribute,loginLocale);
		}
	       		  catch (AmsException e2) {
	       		  displayText ="";
       			  }				                  	      
%>		  <%= displayText %>
		
      	      </td>

<td>

<%

		  displayText = myDoc.getAttribute("/ACCT_NUMBER");
		 
%>		  <%= displayText %>
		
              </td>
      	      
    	  </tr>
    	
<%--	Naveen 22-August-2012. IR-T36000004626. Added the table body closing tag	--%>    	
<%
 	} //End of for loop...

     }catch(Exception e)
     {
	Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Fee table *******");
     	e.printStackTrace();
     }

%>
 </tbody>
</table>