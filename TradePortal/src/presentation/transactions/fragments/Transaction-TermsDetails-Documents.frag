<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
     /***************************************************************************************************
      * Start of Documents Section 
      ************************************/  	       Debug.debug("****** Start of Documents Section ******");
 
  String urlParam;
%>

 
<%-- PHILC PUT IMAGES IN 3 COLUMN TABLE --%> 
<%
boolean displayedDocsGeneratedTable = false; //Vshah CR-452
try {
	query = new StringBuffer();
	//cquinton 8/25/2011 Rel 7.1.0 ppx240 - add hash
	query.append("select form_type, image_id, hash ");
	query.append("from document_image ");
	query.append("where p_transaction_oid = ?");
	query.append(" and form_type <> ?");
	query.append(" and form_type <> ?");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object[] sqlParamsTrmsDoc = new Object[3];
	sqlParamsTrmsDoc[0] =  transaction.getAttribute("transaction_oid");
	sqlParamsTrmsDoc[1] =  TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED;
	sqlParamsTrmsDoc[2] =  TradePortalConstants.DOC_IMG_FORM_TYPE_OTL_ATTACHED;
	Debug.debug("Query is : " + query);	 
	dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), true, sqlParamsTrmsDoc);

	if((dbQuerydoc != null)
      && (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0)) 
    {		
		Debug.debug("**** dbQuerydoc --> " + dbQuerydoc.toString());
		listviewVector = dbQuerydoc.getFragments("/ResultSetRecord/");
		displayedDocsGeneratedTable = true; //Vshah CR-452
%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="ListText">
	  <span class="ControlLabel">
            <%=resMgr.getText("TransactionTerms.DocsGenerated", 
                               TradePortalConstants.TEXT_BUNDLE)%>
	  </span>
	</p>
      </td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
<%
		//This loop is designed to scroll through the ResultSetRecords and display the Links.
		//Note that the links need parameters passed with them.
		Debug.debug("Walk the tree to display the text in the listview.");
		int liTotalDocs = listviewVector.size();
		int liDocCounter = liTotalDocs;
			
		for(int y=0; y < ((liTotalDocs+2)/3); y++) {
%>
<%	
			for (int x=0; x < 3 ; x++) {
				if (liDocCounter==0) break;
%>
<%// jgadela - Rel 8.3 T36000017389 - Moved tr tag inside loop to display the documents in a column %>
  <tr> 
    <td width="40" nowrap>&nbsp;</td>
    <td nowrap>
		<p class="ListText">
<%
					myDoc       = (DocumentHandler)listviewVector.elementAt((y*3)+x);
					attribute   = myDoc.getAttribute("/FORM_TYPE");
					displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.FORM_TYPE, attribute);
					attribute   = myDoc.getAttribute("/IMAGE_ID");
					encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( attribute, userSession.getSecretKey() );
					encryptVal3 = EncryptDecrypt.encryptStringUsingTripleDes( displayText, userSession.getSecretKey() );
                    // cquinton 7/21/2011 Rel 7.1.0 ppx240 - add hash parm
                    String imageHash = myDoc.getAttribute("/HASH");
                    String encryptedImageHash = "";
                    if ( imageHash != null && imageHash.length()>0 ) {
                        encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( imageHash, userSession.getSecretKey() );
                    } else {
                        //send something rather than nothing, which ensures we know we are not allowing
                        // attempts to view images when hash parm is just forgotten
                        encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( TradePortalConstants.NO_HASH, userSession.getSecretKey() );
                    }
					urlParam    = "&image_id=" + encryptVal2 + "&hash=" + encryptedImageHash + "&formType=" + encryptVal3;
%>
            <a href="<%=formMgr.getLinkAsUrl("goToDocumentView", urlParam, response) %>">
				<%= displayText %></a>
				
				 <% // [START] CR-529 - Tang - 3/1/2010
				 displayText = resMgr.getText("TransactionTerms.getImage", TradePortalConstants.TEXT_BUNDLE);
				 urlParam = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) +
				            "&hash=" + encryptedImageHash;
				 %>
				 <a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParam)%>' target='_blank'><%= displayText %></a>
				 <% // [END  ] CR-529 %>
		</p>
	</td>
	<%// jgadela - Rel 8.3 T36000017389 - Moved tr tag inside loop to display the documents in a column %>
  </tr>
		
<%
				liDocCounter--;
			}
			//if (liDocCounter==0) {
				//for (int z=liTotalDocs % 3; z > 0; z--) {
%>
    <%--td nowrap width="100">&nbsp;</td--%>
	<%--td></td--%>	
<%
				//}			
			//}
%>
	
<%
		}
%>
</table>  
<%
	}
} catch(Exception e) {
	Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Document Image table *******");
	e.printStackTrace();
}
//Vshah CR-452 Add Begin
if (showPDFLinks)
	{
	if (!displayedDocsGeneratedTable) {
%>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr> 
	      <td width="20" nowrap>&nbsp;</td>
    	  <td width="100%"> 
        	<p class="ListText">
			  <span class="ControlLabel">
        	    <%=resMgr.getText("TransactionTerms.DocsGenerated", 
            	                   TradePortalConstants.TEXT_BUNDLE)%>
			  </span>
			</p>
    	  </td>
	    </tr>
  	</table>
<% 	
	}
%>	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
    		<td width="40" nowrap>&nbsp;</td>
    		<td width="100%">
    			<p class="ListText">
<%
         				  linkArgs[0] = TradePortalConstants.PDF_EXPORT_COLLECTION;
				          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
					      linkArgs[2] = "en_US";
				          linkArgs[3] = userSession.getBrandingDirectory();
%>
					<%-- 	KMehta IR-T36000037543 Rel 9400 on 07-Aug-2015 Change - Begin	--%>
					<%  if((null != newRecentTransactionType) && newRecentTransactionType.equals(TransactionType.ISSUE)){ %>
  	  					  <%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("ExportCollectionIssue.ExportCollection", TradePortalConstants.TEXT_BUNDLE), 
					            									 TradePortalConstants.PDF_EXPORT_COLLECTION,
                                                     				 linkArgs,
                                                    				 response) %>
					<% } else { %>
							<%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("DirectSendCollection.AmendmentScheduleForDCO", TradePortalConstants.TEXT_BUNDLE), 
					            									 TradePortalConstants.PDF_EXPORT_COLLECTION,
                                                     				 linkArgs,
                                                    				 response) %>
					<% }  %> 
					<%-- 	KMehta IR-T36000037543 Rel 9400 on 07-Aug-2015 Change - End	--%>                                                 				 
			 	</p>
      		</td>
		</tr>
		<tr>
			<td width="40" nowrap>&nbsp;</td>
			<td width="100%">
				<p class="ListText">
<%
         				  linkArgs[0] = TradePortalConstants.PDF_BILL_OF_EXCHANGES;
				          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
					      linkArgs[2] = "en_US";
				          linkArgs[3] = userSession.getBrandingDirectory();
%>
  	  					  <%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("ExportCollectionIssue.BillOfExchanges", TradePortalConstants.TEXT_BUNDLE), 
					            									 TradePortalConstants.PDF_BILL_OF_EXCHANGES,
                                                     				 linkArgs,
                                                    				 response) %>
				</p>
      		</td>
		</tr>
		</table>  
<%
	}
%>		
<%-- Vshah CR-452 Add End --%> 

<%-- PHILC PUT IMAGES IN 3 COLUMN TABLE --%> 

<%-- PHILC PUT IMAGES IN 3 COLUMN TABLE --%> 
<%
try {
   query = new StringBuffer();

   //cquinton 8/25/2011 Rel 7.1.0 ppx240 - add image hash
   query.append("select doc_image_oid, image_id, doc_name, hash ");
   query.append("from document_image ");
   query.append("where p_transaction_oid = ?");
   query.append(" and form_type = ?");
   query.append(" and mail_message_oid is null");
   query.append(" order by image_id");

   Debug.debug("Query is : " + query);
   dbQuerydoc = null;
   //jgadela  R90 IR T36000026319 - SQL FIX
	Object[] sqlParamsTrmsDocIm = new Object[2];
	sqlParamsTrmsDocIm[0] =  transaction.getAttribute("transaction_oid");
	sqlParamsTrmsDocIm[1] =  TradePortalConstants.DOC_IMG_FORM_TYPE_OTL_ATTACHED;
	Debug.debug("Query is : " + query);	
   dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), false, sqlParamsTrmsDocIm);

   if((dbQuerydoc != null)
      && (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0)) 
   {
      Debug.debug("**** dbQuerydoc --> " + dbQuerydoc.toString());
      listviewVector = null;
      listviewVector = dbQuerydoc.getFragments("/ResultSetRecord/");
%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="ListText">
          <span class="ControlLabel">
            <%=resMgr.getText("TransactionTerms.DocsAttached", 
                               TradePortalConstants.TEXT_BUNDLE)%>
          </span>
        </p>
      </td>
    </tr>
  </table>
  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<%
	int liTotalDocs = listviewVector.size();
	int liDocCounter = liTotalDocs;
		
	for(int y=0; y < ((liTotalDocs+2)/3); y++) {
%>
  <tr> 
    <td width="40" nowrap>&nbsp;</td>
<%	
		for (int x=0; x < 3 ; x++) {
			if (liDocCounter==0) break;
%>
    <td nowrap width="250">
		<p class="ListText">
<%
            myDoc = (DocumentHandler)listviewVector.elementAt((y*3)+x);
            // cquinton 7/21/2011 Rel 7.1.0 ppx240 - add hash parm
            String imageHash = myDoc.getAttribute("/HASH");
            String encryptedImageHash = "";
            if ( imageHash != null && imageHash.length()>0 ) {
                encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( imageHash, userSession.getSecretKey() );
            } else {
                //send something rather than nothing, which ensures we know we are not allowing
                // attempts to view images when hash parm is just forgotten
                encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( TradePortalConstants.NO_HASH, userSession.getSecretKey() );
            }
            urlParam = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) +
                       "&hash=" + encryptedImageHash;
%>
            <a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParam)%>' target='_blank'>
            	<%= myDoc.getAttribute("/DOC_NAME") %>
            </a>
		</p>
	</td>	
	<td>&nbsp;</td>
<%
		liDocCounter--;
		}
	if (liDocCounter==0) {
		for (int z=liTotalDocs % 3; z > 0; z--) {
%>
    <td nowrap width="100">&nbsp;</td>
	<td>&nbsp;</td>	
<%
		}			
	}
%>
	<td width="100%">&nbsp;</td>
  </tr>
<%
    }
%>
</table>   
<%
   } // if((dbQuerydoc != null) && (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0)) 
}catch(Exception e){
   Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Document Image table *******");
   e.printStackTrace();
}
%>
<%-- PHILC PUT IMAGES IN 3 COLUMN TABLE --%> 

  <table width="100%" border="0" cellspacing="0" cellpadding="0">  
    <tr>
       <td>&nbsp;</td>
    </tr>
  </table>
