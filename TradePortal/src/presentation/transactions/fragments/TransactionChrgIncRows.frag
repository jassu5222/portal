<%-- RKAZI - 09/18/2013 - Rel82kyriba PPX-269 -Initial Version --%>
<%--this fragment is included in both the 
    Transaction-GUAR-ISS-General fragment for existing rows as well as the
    Add3moreChrgIncRows.jsp servlet for adding new rows via ajax.

  Passed variables:
    pmtTermsList - a list of margin rules
    pmtTermsIndex - an integer value, zero based of the 1st 
    isReadOnly
    isFromExpress
    loginLocale
--%>


	<%
			FeeWebBean feeBean = null;
			for (int iLoop = 0; iLoop < chrgIncList.size(); iLoop++) {
				feeBean = (FeeWebBean) chrgIncList.get(iLoop);
				currencies = Dropdown.createSortedCurrencyCodeOptions(feeBean.getAttribute("settlement_curr_code"),loginLocale);
				String chrgTypes = Dropdown.createSortedRefDataOptions("CHARGE_TYPE",feeBean.getAttribute("charge_type"),loginLocale);
				String chrgIndexEncode = StringFunction.xssCharsToHtml(Integer.toString(chrgIncIndex+iLoop));
		%>
		<tr id="chrgIncIndex<%=chrgIncIndex+iLoop%>">
			<td>    
				<div  class = "formCCItem">&nbsp;&nbsp;&nbsp;&nbsp;
    				<%= widgetFactory.createSelectField( "ChrgIncChargeType"+(chrgIncIndex+iLoop), "", " ", chrgTypes, isReadOnly || isFromExpress, false, false,  "style='width:14em;' onChange=resetFeeOid("+chrgIndexEncode+");", "", "none") %>
    	
			    </div>
			</td>
			<td>    
				<div  class = "formCCItem">&nbsp;&nbsp;&nbsp;&nbsp;
    				<%= widgetFactory.createSelectField( "ChrgIncCurrencyCode"+(chrgIncIndex+iLoop), "", " ", currencies, isReadOnly || isFromExpress, false, false,  "style='width:6em;' onChange=resetFeeOid("+(chrgIncIndex+iLoop)+");", "", "none") %>
    	
			    </div>
			</td>
			<td>
			<div  class = "formCCCItem">
				<%= widgetFactory.createAmountField( "ChrgIncAmount"+(chrgIncIndex+iLoop), "", feeBean.getAttribute("settlement_curr_amount"), feeBean.getAttribute("settlement_curr_code"), isReadOnly, !isTemplate, isExpressTemplate,"style=\"width: 146px;\" onChange=resetFeeOid("+(chrgIncIndex+iLoop)+");", "readOnly:" +isReadOnly, "inline") %>
			  </div>
			</td>
			<td align="left">
		
			<span class="deleteSelectedItem" style="margin:0.3em 1em" id='chrgincDelete<%=(chrgIncIndex+iLoop)%>'></span>
 		  </td>

			
		</tr>
	<%
			String feeOid = feeBean.getAttribute("fee_oid");
			if (StringFunction.isBlank(feeOid)){
				feeOid ="";
			}
			
			 
		
	%>
	<input data-dojo-type="dijit.form.TextBox" name="FeeOid<%=chrgIndexEncode%>" id="FeeOid<%=chrgIndexEncode%>"  type=hidden   maxLength="35" class="char35" value="<%=feeOid%>">

	
	<%		}
	%>