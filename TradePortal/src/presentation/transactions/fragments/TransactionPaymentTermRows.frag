<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 -Initial Version --%>
<%--this fragment is included in both the 
    Transaction-IMP_DLC-ISS-General fragment for existing rows as well as the
    Add2morePaymentTermRows.jsp servlet for adding new rows via ajax.

  Passed variables:
    pmtTermsList - a list of margin rules
    pmtTermsIndex - an integer value, zero based of the 1st 
    isReadOnly
    isFromExpress
    loginLocale
--%>


<%			
			String percentOrAmount = "";
			for ( iLoop = 0; iLoop < pmtTermsList.size(); iLoop++) {
				 pmtTerms = (PmtTermsTenorDtlWebBean) pmtTermsList.get(iLoop);
				 if(TradePortalConstants.PMT_IN_AMOUNT.equals(percentOrAmountInd)){
						percentOrAmount=pmtTerms.getAttribute("amount");
				}else {
						percentOrAmount=pmtTerms.getAttribute("percent");
				}
				// Leelavathi IR#T36000011665,IR#T36000011628,IR#T36000011882,IR#T36000011880 Rel-8.2 02/18/2013 Begin 
				String maturityDateReadOnly= "";
				String numDaysAfterReadOnly= "";
				if(InstrumentServices.isNotBlank(pmtTerms.getAttribute("maturity_date"))){
					// Leelavathi IR#T36000015311 Rel-8.2 04/04/2013 Begin
					numDaysAfterReadOnly = " READONLY ";
					//numDaysAfterReadOnly = " DISABLED ";
					// Leelavathi IR#T36000015311 Rel-8.2 04/04/2013 End
				}
				
				if(InstrumentServices.isNotBlank(pmtTerms.getAttribute("days_after_type")) 
						&& InstrumentServices.isNotBlank(pmtTerms.getAttribute("num_days_after"))){
					// Leelavathi IR#T36000015311 Rel-8.2 04/04/2013 Begin
					maturityDateReadOnly = " READONLY ";
					//maturityDateReadOnly = " DISABLED ";
					// Leelavathi IR#T36000015311 Rel-8.2 04/04/2013 End
				}
				// Leelavathi IR#T36000011665,IR#T36000011628,IR#T36000011882,IR#T36000011880 Rel-8.2 02/18/2013 End
				// Leelavathi IR#T36000011745 Rel-8.2 02/25/2013 Begin
				if("SGT".equals(pmtTerms.getAttribute("tenor_type"))){
					// Leelavathi IR#T36000015311 Rel-8.2 04/04/2013 Begin
					maturityDateReadOnly = " READONLY ";
					numDaysAfterReadOnly = " READONLY ";
					//maturityDateReadOnly = " DISABLED ";
					//numDaysAfterReadOnly = " DISABLED ";
					// Leelavathi IR#T36000015311 Rel-8.2 04/04/2013 End
				}
				// Leelavathi IR#T36000011745 Rel-8.2 02/25/2013 End
				
				String pmtTermIndexEncode = Integer.toString(pmtTermsIndex+iLoop);				
		%>
		<tr id="pmtTermsIndex<%=pmtTermsIndex+iLoop%>">
			<td id="PmtTermPercent"+ (pmtTermsIndex+iLoop)>
			
			<% if(TradePortalConstants.PMT_IN_AMOUNT.equals(percentOrAmountInd) && isReadOnly ){
				percentOrAmount = TPCurrencyUtility.getDisplayAmount(percentOrAmount, currency, loginLocale);
				%>
				<%=widgetFactory.createAmountField("PmtTermPercent"+(pmtTermsIndex+iLoop),"",percentOrAmount, "", isReadOnly, false, false,"class='char15'", "readOnly:"+isReadOnly, "none")%>
			<% }else{ %>
				<%= widgetFactory.createTextField( "PmtTermPercent"+ (pmtTermsIndex+iLoop), "", percentOrAmount,"", isReadOnly, true, false, "style='width:3em'"+"onClick=\"addConstraint();\""+accpDefpDisabled, "", "none" ) %>				
			<% } %> 
			
				</td>
			<td id ="PmtTermTenorType"+ (pmtTermsIndex+iLoop) >
				 <% 
				 options = Dropdown.createSortedRefDataOptions( TradePortalConstants.TENOR, pmtTerms.getAttribute("tenor_type"),loginLocale);
				 %>
				<%= widgetFactory.createSelectField( "PmtTermTenorType"+ (pmtTermsIndex+iLoop), "",  " ", options, isReadOnly || isFromExpress, false, true, "class='char10'"+"onChange=TenorDetailsAndMaturityDateReadOnly("+pmtTermIndexEncode+ ")"+accpDefpDisabled, "", "none" ) %>
			</td>
			<td>
				<div >
					<%= widgetFactory.createNumberField( "PmtTermNumDaysAfter"+ (pmtTermsIndex+iLoop), "", pmtTerms.getAttribute("num_days_after"), "3", isReadOnly || isFromExpress, false, true, " onChange=MaturityDateReadOnly("+pmtTermIndexEncode+ ")"+numDaysAfterReadOnly, "constraints:{min:-999,max:999,places:0}","none" ) %>
					<%if(!isReadOnly || StringFunction.isNotBlank(pmtTerms.getAttribute("num_days_after"))){%>
					<%-- Leelavathi IR#T36000016930 05/14/2013 CR-737 Rel-8.2 Begin --%>
						<%=resMgr.getText("RequestAdviseIssue.DaysAfter",TradePortalConstants.TEXT_BUNDLE)%>
						<%-- Leelavathi IR#T36000016930 05/14/2013 CR-737 Rel-8.2 End --%>
						<%}
					options = Dropdown.createSortedRefDataOptions( TradePortalConstants.PMT_TERMS_DAYS_AFTER, pmtTerms.getAttribute("days_after_type"),loginLocale);
					%>
					<%-- Leelavathi IR#T36000011665 Rel-8.2 02/18/2013 Begin --%>	
					<%= widgetFactory.createSelectField( "PmtTermDaysAfterType"+ (pmtTermsIndex+iLoop), "", " ", options, isReadOnly || isFromExpress, false, true, "onChange=MaturityDateReadOnly("+(pmtTermsIndex+iLoop)+ ")"+numDaysAfterReadOnly, "", "none" ) %>
				</div>
			</td>		
			<td>
				 <%= widgetFactory.createDateField( "PmtTermMaturityDate"+ (pmtTermsIndex+iLoop), "", StringFunction.xssHtmlToChars(pmtTerms.getAttribute("maturity_date")), isReadOnly, true, false,  "onChange=DaysAfterAndDaysAfterTypeReadOnly("+pmtTermIndexEncode+ ")"+maturityDateReadOnly, "", "none" ) %>
				 <%-- Leelavathi IR#T36000011665 Rel-8.2 02/18/2013 End --%>	
			</td>
		</tr>
			<%
	  			String pmtTermsTenorDtlOid = pmtTerms.getAttribute("pmt_terms_tenor_dtl_oid");
	  			out.print("<INPUT TYPE=HIDDEN NAME='PmtTermTenorDtlOid"+pmtTermIndexEncode+"' VALUE='" +pmtTermsTenorDtlOid+ "'>"); 
				}
			%>