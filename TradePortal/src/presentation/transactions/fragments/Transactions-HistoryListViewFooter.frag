<%--
*******************************************************************************
  Transactions Inquiries Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>

<%

  dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  //gridLayout is not used for now, its hardcoded below
  //String gridLayout = dgFactory.createGridLayout("instrumentSearchGridId", "InstrumentInquiriesDataGrid");
%>



<script type="text/javascript">
  var local = {
    searchType: "S" <%-- i.e. basic --%>
  };
	var custNotIntgWithTPS = false;
	<% if(userSession.isCustNotIntgTPS()){ %>
     		custNotIntgWithTPS = true;
     <% } %>
  <%--IR T36000015639 REL ER 8.1.0.4 BEGIN --%>
  var selectedOrg="";
  var instrType; <%-- used by copy --%>
  var userOrgOid = '<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>';
  <%--IR T36000015639 REL ER 8.1.0.4 END --%>
  
  var quickViewFormatter=function(columnValues, idx, level) {
    var gridLink="";

    if(level == 0){

      if ( columnValues.length >= 2 ) {
        gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
      }
      else if ( columnValues.length == 1 ) {
        <%-- no url parameters, just return the display --%>
        gridLink = columnValues[0];
      }
      return gridLink;

    }
    else if(level ==1){
      
      if(columnValues[3]=='PROCESSED_BY_BANK'){
        if(columnValues[2]){
          return "<a href='javascript:dialogQuickView("+columnValues[2]+");'>Quick View</a> "+columnValues[0]+" ";
        }
        else{
          return columnValues[0];
        }

      }
      else{
        return columnValues[0];
      }

    }
  }

  var formatGridLinkChild=function(columnValues, idx, level){
    var gridLink="";

    if(level == 0){
      return columnValues[0];

    }
    else if(level ==1){
      if ( columnValues.length >= 2 ) {
        gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
      }
      else if ( columnValues.length == 1 ) {
        <%-- no url parameters, just return the display --%>
        gridLink = columnValues[0];
      }
      return gridLink;
    }
  }

  <%-- cquinton 3/11/2013 problems with lazy tree grid resizing on search --%>
  <%--  as a temporary fix, recreate the grid on search --%>
  var gridLayout = [
    {name:"<%=resMgr.getText("InstrumentHistory.InstrumentId",TradePortalConstants.TEXT_BUNDLE) %>", field:"InstrumentId", fields:["InstrumentId","InstrumentId_linkUrl","transaction_oid","STATUSFORQVIEW","InstrumentType"], formatter:quickViewFormatter, width:"150px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.InstrumentType",TradePortalConstants.TEXT_BUNDLE) %>", field:"InstrumentType", fields:["InstrumentType","InstrumentType_linkUrl"], formatter:formatGridLinkChild, width:"150px"} ,
     <% if (userSession.isCustNotIntgTPS()) {%>
   	 	{name:"<%=resMgr.getText("InstrumentHistory.BankInstrumentID",TradePortalConstants.TEXT_BUNDLE) %>", field:"BankInstrumentID", width:"150px"} ,
     <%} %>
    {name:"<%=resMgr.getText("InstrumentHistory.CurrencyCode",TradePortalConstants.TEXT_BUNDLE) %>", field:"CurrencyCode", width:"35px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.Amount",TradePortalConstants.TEXT_BUNDLE) %>", field:"Amount", width:"80px", cellClasses:"gridColumnRight"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.STATUS",TradePortalConstants.TEXT_BUNDLE) %>", field:"Status", width:"70px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.Confirmed",TradePortalConstants.TEXT_BUNDLE) %>", field:"Confirmed", width:"70px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.RelatedInstr",TradePortalConstants.TEXT_BUNDLE) %>", field:"RelatedInstr", fields:["RelatedInstr","RelatedInstr_linkUrl"], formatter:formatGridLink, width:"130px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.Party",TradePortalConstants.TEXT_BUNDLE) %>", field:"Party", width:"120px"},
    {name:"<%=resMgr.getText("InstrumentHistory.ApplicantsRefNo",TradePortalConstants.TEXT_BUNDLE) %>", field:"ApplicantsRefNo", width:"160px"},
    {name:"<%=resMgr.getText("InstrumentHistory.OrigBanksRefNo",TradePortalConstants.TEXT_BUNDLE) %>", field:"OrigBanksRefNo", width:"150px"},
    {name:"<%=resMgr.getText("InstrumentHistory.VendorId",TradePortalConstants.TEXT_BUNDLE) %>", field:"VendorId", width:"80px"}
  ];

  var instrumentId = "";
  var refNum = "";
  var bankRefNum = "";
  var currency = "";
  var amountFrom = "";
  var amountTo = "";
  var dateFrom = "";
  var dateTo = "";
  var otherParty = "";
  var vendorId = "";
  var instrumentType = "";
  var init='loginLocale=<%=loginLocale%>';
  var savedSort = savedSearchQueryData["sort"];
   searchType = savedSearchQueryData["searchType"];
   instrStatusType = savedSearchQueryData["selectedStatus"];
   selectedOrg = savedSearchQueryData["selectedOrg"];
   <%--  DK CR-886 Rel8.4 10/15/2013 starts --%>
   var startDateFrom = "";
   var startDateTo = "";
   <%-- DK CR-886 Rel8.4 10/15/2013 ends --%>
   var bankInstrumentId = "";
   

  
   require(["dojo/dom","dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/ready","dijit/registry","dojo/domReady!"],
		      function(dom,query, on, t360grid, t360popup, ready, registry){
			  ready(function(){
	  if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg){
      <%-- PR ER-T36000015639 REL ER 8.1.0.4 - To send encrypted value of selectedOrg to the DataView --%>
	  selectedOrg = ( registry.byId('Organization') == null ?"":registry.byId('Organization').attr('value'));	
	  } 
	  <%-- IR 16481 start --%>
	  else{
		  if(registry.byId('Organization')){
			  registry.byId("Organization").set('value',selectedOrg);
	  }}
	    if("" == selectedOrg || null == selectedOrg){
	      selectedOrg = userOrgOid;
	    }
	    if("" == instrStatusType || null == instrStatusType || "undefined" == instrStatusType){
	    	if(dom.byId("InstrStatusTypeActive").checked && dom.byId("InstrStatusTypeInactive").checked){
	    	  <%-- jgadela - 29th Aug 2013 - Rel8.3 IR-T36000020337 [BEGIN] - Corrected the StatusType parameter - The query parameter should always be constant --%>
	    		<%-- instrStatusType="<%=resMgr.getText("common.StatusAll",TradePortalConstants.TEXT_BUNDLE)%>"; --%>
	    		instrStatusType="<%=TradePortalConstants.STATUS_ALL%>";
	          }
	          else if(dom.byId("InstrStatusTypeActive").checked){
	        	  <%-- instrStatusType="<%=resMgr.getText("common.StatusActive",TradePortalConstants.TEXT_BUNDLE)%>"; --%>
	        	  instrStatusType="<%=TradePortalConstants.STATUS_ACTIVE%>";
	          }
	          else if(dom.byId("InstrStatusTypeInactive").checked){
	        	 <%--  instrStatusType="<%=resMgr.getText("common.StatusInactive",TradePortalConstants.TEXT_BUNDLE)%>" --%>
	        	 instrStatusType="<%=TradePortalConstants.STATUS_INACTIVE%>"
	          }
	          else{
	        	  instrStatusType="<%=TradePortalConstants.STATUS_ACTIVE%>";
	          }	
	          <%-- jgadela - 29th Aug 2013 - Rel8.3 IR-T36000020337 [END] --%>
  		}
	    else{
	    	if(instrStatusType=="<%=resMgr.getText("common.StatusActive",TradePortalConstants.TEXT_BUNDLE)%>"){
	    	   registry.byId("InstrStatusTypeActive").set('checked',true);
	    	   registry.byId("InstrStatusTypeInactive").set('checked',false);
	    	}
	    		
	    	if(instrStatusType=="<%=resMgr.getText("common.StatusInactive",TradePortalConstants.TEXT_BUNDLE)%>"){
	    		 registry.byId("InstrStatusTypeInactive").set('checked',true);
	    		 registry.byId("InstrStatusTypeActive").set('checked',false);
	    	}
	    	if(instrStatusType=="<%=resMgr.getText("common.StatusAll",TradePortalConstants.TEXT_BUNDLE)%>"){
	    		  registry.byId("InstrStatusTypeInactive").set('checked',true);
	    	   registry.byId("InstrStatusTypeActive").set('checked',true);
	    	}
	    	
	    }	if("" == savedSort || null == savedSort || "undefined" == savedSort)
	    		  savedSort = '0';
	       if("" == searchType || null == searchType || "undefined" == searchType){
	        	searchType = registry.byId('SearchType');
	          }
	       else{
	    	   if('A'==searchType){
	    	   document.getElementById("advancedInstrumentFilter").style.display='block';
	           document.getElementById("basicInstrumentFilter").style.display='none';
	    	   }
	    	   else{
	    		   document.getElementById("advancedInstrumentFilter").style.display='none';
		           document.getElementById("basicInstrumentFilter").style.display='block';
	    	   }
	    	  
	       }
	             if('A'==searchType){
	        	 amountFrom = savedSearchQueryData["amountFrom"];
	        	 amountTo = savedSearchQueryData["amountTo"];
	        	 dateFrom = savedSearchQueryData["fromDate"];
	        	 dateTo = savedSearchQueryData["toDate"];
	        	 otherParty = savedSearchQueryData["otherParty"];
	        	 vendorId = savedSearchQueryData["vendorId"];
	        	 instrumentType = savedSearchQueryData["instrumentType"];
	        	 currency = savedSearchQueryData["currency"];
	        	 <%-- DK CR-886 Rel8.4 10/15/2013 starts     IR-45903 --%>
	        	 startDateFrom = savedSearchQueryData["startDateFrom"];
	        	 startDateTo = savedSearchQueryData["startDateTo"];
	        	 <%--DK CR-886 Rel8.4 10/15/2013 ends --%>
	        	
	        	if("" == currency || null == currency || "undefined" == currency)
	        		currency =checkString( dijit.byId("Currency") );
	        	else{
	        		registry.byId("Currency").set('value',currency);
		      	  }
	        	if("" == instrumentType || null == instrumentType || "undefined" == instrumentType)
	        		instrumentType = checkString( dijit.byId("advInstrumentType") );
	        	else{
	        		registry.byId("InstrumentTypeAdvance").set('value',instrumentType);
	      	  }
	        	if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
	        		 amountFrom = checkString( dijit.byId("AmountFrom") );
	        	else
	        		dom.byId("AmountFrom").value=amountFrom;
	        	if("" == amountTo || null == amountTo || "undefined" == amountTo)
	        		 amountTo = checkString( dijit.byId("AmountTo") );
	        	else
	        		dom.byId("AmountTo").value=amountTo;
	        	if("" == otherParty || null == otherParty || "undefined" == otherParty)
	        		 otherParty = checkString( dijit.byId("OtherParty") );
	        	else
	        		dom.byId("OtherParty").value=otherParty;
	        	if("" == vendorId || null == vendorId || "undefined" == vendorId)
	        		 vendorId = checkString( dijit.byId("VendorIdAdvance") );
	        	else
	        		dom.byId("VendorIdAdvance").value = vendorId;
	        	
	        	if("" == dateFrom || null == dateFrom || "undefined" == dateFrom){
	        		 dateFrom = dom.byId("DateFrom").value;
	        	}else{
	        		registry.byId("DateFrom").set('displayedValue',dateFrom);
	        	}
	        	if("" == dateTo || null == dateTo || "undefined" == dateTo){
	        		 dateTo = dom.byId("DateTo").value;
	        	}else{
	        		registry.byId("DateTo").set('displayedValue',dateTo);
	        	}
	        	
	        	<%--  DK CR-886 Rel8.4 10/15/2013 starts --%>
	        	if("" == startDateFrom || null == startDateFrom || "undefined" == startDateFrom)
	        		startDateFrom = dom.byId("StartDateFrom").value;
	        	else{
	        		registry.byId("StartDateFrom").set('displayedValue',startDateFrom);
	        	}
	        	if("" == startDateTo || null == startDateTo || "undefined" == startDateTo)
	        		startDateTo = dom.byId("StartDateTo").value;
	        	else{
	        		registry.byId("StartDateTo").set('displayedValue',startDateTo);
	        	}
	        	
	        	<%--  DK CR-886 Rel8.4 10/15/2013 ends --%>
	        	 <%-- Remove comma for amount fields --%>
	             var regex = new RegExp(',', 'g'); <%-- Created reg exp to find comma globally --%>
	        	 if(amountFrom!=null){
        		  amountFrom = amountFrom.replace(regex, '');		    	
       			 }	
       			 if(amountTo!=null){
         		 amountTo = amountTo.replace(regex, '');		    	
        			}
       			 var tempDatePattern = encodeURI('<%=datePattern%>');
       			<%--  DK CR-886 Rel8.4 10/15/2013 starts --%>
	        	init = init+"&currency="+currency+"&instrumentType="+instrumentType+"&amountFrom="+amountFrom+"&amountTo="+amountTo+"&otherParty="
	        	+otherParty+"&toDate="+dateTo+"&fromDate="+dateFrom+"&startToDate="+startDateTo+"&startFromDate="+startDateFrom+"&dPattern="+tempDatePattern;
	        	<%--  DK CR-886 Rel8.4 10/15/2013 ends --%>
	        }
	        else{
	 
	        	 refNum = savedSearchQueryData["refNum"];	
	        	 instrumentType = savedSearchQueryData["instrumentType"];
	        	 instrumentId = savedSearchQueryData["instrumentId"];
	        	 bankRefNum =savedSearchQueryData["BankRefNum"];
       			 vendorId = savedSearchQueryData["VendorIdBasic"];
       			  if(custNotIntgWithTPS){
       				 bankInstrumentId = savedSearchQueryData["bankInstrumentId"];
       			 }
       			 
	        	if("" == instrumentId || null == instrumentId || "undefined" == instrumentId)
	        		instrumentId = checkString( dijit.byId("InstrumentId") );
	        	else
	        		dom.byId("InstrumentId").value=instrumentId;
	        	if("" == instrumentType || null == instrumentType || "undefined" == instrumentType)
	        		instrumentType = checkString( dijit.byId("InstrumentType") );
	        	else{
	        		registry.byId("InstrumentTypeBasic").set('value',instrumentType);
		      		}
	        	 if("" == refNum || null == refNum || "undefined" == refNum)
	        		 refNum = checkString( dijit.byId("RefNum") );
	        	 else
	        		 dom.byId("RefNum").value=refNum;
	             if("" == bankRefNum || null == bankRefNum || "undefined" == bankRefNum)
	        		 bankRefNum = checkString( dijit.byId("BankRefNum") );
	             else
	            	 dom.byId("BankRefNum").value=bankRefNum;
	        	 if("" == vendorId || null == vendorId || "undefined" == vendorId)
	        		 vendorId = checkString( dijit.byId("VendorIdBasic") );
	        	 else
	        		 dom.byId("VendorIdBasic").value=vendorId;
	        	 if(custNotIntgWithTPS){	 
	        		 if("" == bankInstrumentId || null == bankInstrumentId || "undefined" == bankInstrumentId)
	        				bankInstrumentId = checkString( dijit.byId("BankInstrumentId") );
	        			else	 
	        				dom.byId("BankInstrumentId").value=bankInstrumentId;
	             		
	             	init = init+"&instrumentId="+instrumentId+"&instrumentType="+instrumentType+"&refNum="+refNum+"&bankInstrumentId="+bankInstrumentId;
	             }else{
	             	init = init+"&instrumentId="+instrumentId+"&instrumentType="+instrumentType+"&refNum="+refNum;
	             }
	             
	        	
	        	 
	        } 
	             <%--MEer Rel 8.4 IR-23915 removed filterAllCashMgmt from search Parameters--%>
	        var initSearchParms = init+"&selectedOrg="+selectedOrg+"&selectedStatus="+instrStatusType+
	        "&searchType="+searchType+"&searchCondition=<%=searchCondition%>";
	        <%-- IR 16481 end  --%>
   <%--  var initSearchParms = "selectedOrg="+selectedOrg+"&selectedStatus=<%=TradePortalConstants.STATUS_ACTIVE%>" +
      "&filterAllCashMgmtTrans=<%=filterAllCashMgmtTrans%>" +
      "&searchType="+local.searchType+"&searchCondition=<%=searchCondition%>" +
      "&otherParty=<%=otherParty%>&currency=<%=currency%>&amountFrom=<%=amountFrom%>" +
      "&amountTo=<%=amountTo%>";--%>
     
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InstrumentsInquiriesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
    local.theGrid =
      t360grid.createLazyTreeDataGrid("instrumentsSearchGridId", 
        viewName, gridLayout, initSearchParms, savedSort);
	  });
    local.searchInstruments = function(buttonId) {

      var mySearchType = "";
      if ( buttonId && "Basic" == buttonId ) {
        mySearchType = "S";
      }
      else if ( buttonId && "Advanced" == buttonId ) {
        mySearchType = "A";
      }
      else {
        mySearchType = local.searchType; <%-- use page variable --%>
      }

      var organization="";
      var gridId = registry.byId("instrumentsSearchGridId");
      if (gridId.selection.getSelectedCount() != 0){
        var selectedCount = dom.byId("instrumentsSearchGridId_selCount");
        selectedCount.innerHTML = 0;
      }
      if(registry.byId("Organization") != undefined){
        organization = registry.byId("Organization").attr('value');
      }
      else{
        <%-- T36000006523 by dillip --%>
        organization="<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>";
      }
      <%-- Ende Here --%>
      var status = "";
	 <%-- jgadela - 29th Aug 2013 - Rel8.3 IR-T36000020337 [BEGIN] - Corrected the StatusType parameter - The query parameter should always be constant --%>
      if(registry.byId("InstrStatusTypeActive").attr('checked') && registry.byId("InstrStatusTypeInactive").attr('checked')){
        <%-- status="<%=resMgr.getText("common.StatusAll",TradePortalConstants.TEXT_BUNDLE)%>"; --%>
        status="<%=TradePortalConstants.STATUS_ALL%>";
      }
      else if(registry.byId("InstrStatusTypeActive").attr('checked')){
        <%-- status="<%=resMgr.getText("common.StatusActive",TradePortalConstants.TEXT_BUNDLE)%>"; --%>
        status="<%=TradePortalConstants.STATUS_ACTIVE%>";
      }
      else if(registry.byId("InstrStatusTypeInactive").attr('checked')){
        <%-- status="<%=resMgr.getText("common.StatusInactive",TradePortalConstants.TEXT_BUNDLE)%>"; --%>
        status="<%=TradePortalConstants.STATUS_INACTIVE%>";
      }
      else{
        status="''";
      }		 
      <%-- jgadela - 29th Aug 2013 - Rel8.3 IR-T36000020337 [END]  --%>
      
    	 
      var searchParms = "selectedOrg="+organization+"&selectedStatus="+status+
        "&searchCondition=<%=searchCondition%>";
 		
      var tempDatePattern = encodeURI('<%=datePattern%>');
      searchParms=searchParms+"&dPattern="+tempDatePattern;
 		
      var instrumentId = "";
      var refNum = "";
      var bankRefNum = "";
      var vendorId = "";
      var instrumentType = "";

      var currency = "";
      var amountFrom = "";
      var amountTo = "";
      var dateFrom = "";
      var dateTo = "";
      var otherParty = "";
      var vendorId = "";
      var instrumentType = "";
      var externalBankDropDownId = ""; <%-- Kyriba CR 268 --%>
	    
      if("A" == mySearchType){       	
        currency = registry.byId("Currency").attr('value');
        amountFrom = dom.byId("AmountFrom").value;
        amountTo = dom.byId("AmountTo").value;
        dateFrom = dom.byId("DateFrom").value;
        dateTo = dom.byId("DateTo").value;
        otherParty = dom.byId("OtherParty").value;
        vendorId = dom.byId("VendorIdAdvance").value;
        instrumentType = registry.byId("InstrumentTypeAdvance").attr('value');
        externalBankDropDownId = registry.byId("ExternalBankDropDownId").attr('value'); <%-- Kyriba CR 268 --%>
        startDateFrom = dom.byId("StartDateFrom").value; <%--  DK IR T36000024022 Rel8.4 01/10/2014 --%>
        startDateTo = dom.byId("StartDateTo").value; <%--  DK IR T36000024022 Rel8.4 01/10/2014 --%>
         searchParms=searchParms+"&currency="+currency;
        <%-- Remove comma for amount fields --%>
        var regex = new RegExp(',', 'g'); <%-- Created reg exp to find comma globally --%>
        if(amountFrom!=null){
          amountFrom = amountFrom.replace(regex, '');		    	
        }	
        if(amountTo!=null){
          amountTo = amountTo.replace(regex, '');		    	
        }
        searchParms=searchParms+"&amountFrom="+amountFrom;
        searchParms=searchParms+"&amountTo="+amountTo;
        searchParms=searchParms+"&fromDate="+dateFrom;
        searchParms=searchParms+"&toDate="+dateTo;
        searchParms=searchParms+"&otherParty="+otherParty;
        searchParms=searchParms+"&vendorId="+vendorId;
        searchParms=searchParms+"&instrumentType="+instrumentType;
        searchParms=searchParms+"&externalBankOid="+externalBankDropDownId;  <%-- Kyriba CR 268 --%>
        searchParms=searchParms+"&searchType="+"A";
        <%-- DK CR-886 Rel8.4 10/15/2013 starts    IR-45903 --%>
        searchParms=searchParms+"&startDateFrom="+startDateFrom;
        searchParms=searchParms+"&startDateTo="+startDateTo;
        <%-- DK CR-886 Rel8.4 10/15/2013 ends --%>
      }
      else { <%-- basic --%>
        instrumentId = dom.byId("InstrumentId").value;
        refNum = dom.byId("RefNum").value;
        bankRefNum = dom.byId("BankRefNum").value;
        vendorId = dom.byId("VendorIdBasic").value;
        instrumentType = registry.byId("InstrumentTypeBasic").attr('value');
        
        searchParms=searchParms+"&instrumentId="+instrumentId;
        searchParms=searchParms+"&refNum="+refNum;
        
        if(custNotIntgWithTPS){
     		 bankInstrumentId = dom.byId("BankInstrumentId").value;
     		 searchParms=searchParms+"&bankInstrumentId="+bankInstrumentId;
    	}
       
        searchParms=searchParms+"&bankRefNum="+bankRefNum;
        searchParms=searchParms+"&vendorId="+vendorId;
        searchParms=searchParms+"&instrumentType="+instrumentType;
        searchParms=searchParms+"&searchType="+"S";
      
      }

      if (searchParms != "") {
        var strLen = searchParms.length;
        searchParms = searchParms.slice(0,strLen);
      }
 		
      var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InstrumentsInquiriesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
      <%-- cquinton 3/11/2013 problems with lazy tree grid not resizing on search -- --%>
      <%--  as a temporary fix, recreate the grid on search --%>
      local.theGrid =
        t360grid.searchLazyTreeDataGrid("instrumentsSearchGridId", 
          viewName, gridLayout, searchParms);
    };

    local.shuffleFilter = function(linkValue){
      var hd = dom.byId("secondaryNavHelp");
      var a = hd.children[0];
      if(linkValue=='Advanced'){
        a.setAttribute('href',"javascript:openHelp('/portal/help/en/customer/inst_hist.htm#advanced')");
        document.getElementById("advancedInstrumentFilter").style.display='block';
        document.getElementById("basicInstrumentFilter").style.display='none';
        local.searchType = "A";

        <%-- clearing Basic Filter --%>
        registry.byId("InstrumentTypeBasic").set("value","");
        registry.byId("InstrumentId").value='';
        registry.byId("RefNum").value='';
        registry.byId("BankRefNum").value='';
        registry.byId("VendorIdBasic").value=''; 
        if(custNotIntgWithTPS){
        	registry.byId("BankInstrumentId").value=''; 	
        }
    				
        <%-- Search call,  --%>
        local.searchInstruments("Advanced");
      }
      if(linkValue=='Basic'){
        a.setAttribute('href',"javascript:openHelp('/portal/help/en/customer/inst_hist.htm#basic')");
        document.getElementById("advancedInstrumentFilter").style.display='none';
        document.getElementById("basicInstrumentFilter").style.display='block';
        local.searchType = "S";
    				
        <%-- clearing Advance Filter --%>
        registry.byId("InstrumentTypeAdvance").set("value","");
        registry.byId("Currency").set("value","");
        registry.byId("AmountFrom").value='';
        registry.byId("AmountTo").value='';
        registry.byId("OtherParty").value='';
        registry.byId("VendorIdAdvance").value='';	
        registry.byId("DateFrom").set("value",0);
        registry.byId("DateTo").set("value",0);
        registry.byId("externalBankDropDownId").set("value",""); <%-- Kyriba CR 268 --%>
    				
        <%-- Search call --%>
        local.searchInstruments("Basic");
      }
    };

  });

  <%-- Method will be called when user press enter key on any search field	  --%>
  <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
  <%-- Modified the function in order for enter key to work in Firefox --%>  
  function filterOnEnter(evt, fieldID, buttonId){
  	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(evt) {
      if(evt && evt.keyCode==13){
      local.searchInstruments(buttonId);
    }
  });
 });  
 }<%-- end of filterOnEnter() --%>
  <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>
 
  function dialogQuickView(rowKey){
	 var dialogDiv = document.createElement("div");
	 dialogDiv.setAttribute('id',"quickviewdialogdivid");
	 document.forms[0].appendChild(dialogDiv);

	 var title="";

	require(["t360/dialog"], function(dialog ) {
		
       dialog.open('quickviewdialogdivid',title ,
                   'TransactionQuickViewDialog.jsp',
                   ['rowKey','DailogId'],[rowKey,'quickviewdialogdivid'], <%-- parameters --%>
                   'select', quickViewCallBack);
     });
  }


  <%-- function to set the title after ajax call of dialog value[2]- dialogId, value[0]-complete instrument id, value[1]-transaction type --%>
  function quickViewCallBack(key,value){
	dijit.byId(value[2]).set('title','Transaction Quick View - '+value[0]+' - '+value[1]);
	<%-- commented as this dialog gets minimize automatically  --%>
	<%-- dijit.byId(value[2]).set('style','width:600px'); --%>

  }
  function checkString(inquiryStr){
		if(null == inquiryStr)
			return "";
		else
			return inquiryStr.attr('value');
	}

</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="instrumentsSearchGridId" />
</jsp:include>
