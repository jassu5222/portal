
<%--
	*******************************************************************************
	Bank Transaction Status Update -  Amend Page
	
	*******************************************************************************
	--%>

<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
	com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
	com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
	com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,java.util.*,
	com.amsinc.ecsg.util.DateTimeUtility"%>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	TransactionWebBean transaction = null;
	InstrumentWebBean instrument = null;
	BankTransactionUpdateWebBean bankTransUpdate = null;
	TermsWebBean terms = null;
	CorporateOrganizationWebBean  corpOrg= null;
	TermsPartyWebBean termsPartyBen = null;

	String instrumentOid = null;
	String transactionOid = null;
	boolean isReadOnly = false;
	boolean populateFromDB = false;
	boolean isInstrumentLocked = false;

	WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
	ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

	// Get the document from the cache.  We'll may use it to repopulate the
	// screen if returning from another page, a save, validation, or any other
	// mediator called from this page.  Otherwise, we assume an instrument oid
	// and transaction oid was passed in.

	DocumentHandler docCache = formMgr.getFromDocCache();
	String maxError = null;
	HttpSession	theSession    = request.getSession(false);
	
	String fromPage =  null;
	userSession.setCurrentPrimaryNavigation("AdminNavigationBar.UpdateCentre");
	 
	String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
	
	 String loginLocale = userSession.getUserLocale();
%>

<%-- ********************* HTML for page begins here ********************* --%>
<%-- Body tag included as part of common header --%>
<%
	String pageTitleKey = "";
	String helpUrl;

	transaction = (TransactionWebBean) beanMgr.getBean("Transaction");
	instrument = (InstrumentWebBean) beanMgr.getBean("Instrument");
	terms          = (TermsWebBean) beanMgr.getBean("Terms");
    corpOrg        = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	termsPartyBen    = beanMgr.createBean(TermsPartyWebBean.class,"TermsParty");
	
	TermsPartyWebBean termsPartyAdv    = beanMgr.createBean(TermsPartyWebBean.class,"TermsParty");
    TermsPartyWebBean termsPartyApp    = beanMgr.createBean(TermsPartyWebBean.class,"TermsParty");


	if (transaction == null) {
		Debug.debug("No transaction bean found, creating");
		beanMgr.registerBean(
				"com.ams.tradeportal.busobj.webbean.TransactionWebBean",
				"Transaction");
		transaction = (TransactionWebBean) beanMgr
				.getBean("Transaction");
	}

	if (instrument == null) {
		Debug.debug("No instrument bean found, creating");
		beanMgr.registerBean(
				"com.ams.tradeportal.busobj.webbean.InstrumentWebBean",
				"Instrument");
		instrument = (InstrumentWebBean) beanMgr.getBean("Instrument");
	}
	
	
	String transactionType = null;
	String instrumentType = null;

	transactionType = transaction.getAttribute("transaction_type_code");
	instrumentType = instrument.getAttribute("instrument_type_code");

	if (bankTransUpdate == null) {
		Debug.debug("No Bank Transaction Update bean found, creating");
		beanMgr.registerBean(
				"com.ams.tradeportal.busobj.webbean.BankTransactionUpdateWebBean",
				"BankTransactionUpdate");
		bankTransUpdate = (BankTransactionUpdateWebBean) beanMgr
				.getBean("BankTransactionUpdate");
	}
	
	
	
//Check if docCache is available
	if (null != docCache) {
		Debug.debug("docCache : "+docCache);
		//Check for Errors
		if (null != docCache.getAttribute("/Error/maxerrorseverity")) {
			maxError = docCache.getAttribute("/Error/maxerrorseverity");
		}else{
			//If not errors, populate data from DB
			populateFromDB = true;
		}
	}else{
		//Doc Cache is null, get data from db
		populateFromDB = true;
	}
	Debug.debug("maxError : "+maxError);
	if (null != maxError){
		if( maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY) ) < 0){
			// We've returned from a save/applyupdates that was successful
			populateFromDB = true;
		}
	}
	
	
	 Vector errorList = null;
	 int errorListSize = 0;
	 if( null != docCache.getFragments("/Error/errorlist/error") ){
		 errorList = docCache.getFragments("/Error/errorlist/error");
		 errorListSize = errorList.size();
	 }
	
	
	for (int i = 0; i < errorListSize; i++)
	{
		DocumentHandler errorDoc = (DocumentHandler)errorList.elementAt(i);
		//Check for a specific errro code.
		if ( TradePortalConstants.DIFFERENT_APPROVER_REQUIRED.equalsIgnoreCase(errorDoc.getAttribute("/code")) )
		{
			populateFromDB = true;
			//break;
		}
		//check if the error code is for instrument lock
		if ( TradePortalConstants.INSTRUMENT_LOCKED.equalsIgnoreCase(errorDoc.getAttribute("/code")) ){
			isInstrumentLocked = true;
		}
	}
	//Check if the button pressed is attach-document.  Is so , get data from cache.
	if(null != docCache.getAttribute("/In/Update/ButtonPressed")){
		if(TradePortalConstants.BUTTON_ATTACH_DOCUMENTS.equals(docCache.getAttribute("/In/Update/ButtonPressed"))
				|| TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS.equals(docCache.getAttribute("/In/Update/ButtonPressed"))){
			populateFromDB = false;
		}
	}
	Debug.debug("populateFromDB : "+populateFromDB);
	

	String bankTransUpdateOID = null;
	//Check for bankTransOid in request
	if (null != request.getParameter("bankTransOid")) {		
		bankTransUpdateOID = request.getParameter("bankTransOid");
	}

	/* If bank_transaction_update_oid is not found in the request, 
	then it means returning from mediator.  Get it from docCache Cache. */
	if (null == bankTransUpdateOID) {
		if (null != docCache) {
			if (null != docCache
					.getAttribute("/In/BankTransactionUpdate/bank_transaction_update_oid")) {
				bankTransUpdateOID = docCache
						.getAttribute("/In/BankTransactionUpdate/bank_transaction_update_oid");
			}
		}
	}

	Debug.debug("bank_transaction_update_oid : "
			+ bankTransUpdateOID);

	String transactionOID = null;
	//Check for bank_transaction_update_oid in request
	if (null != request.getParameter("transOid")) {
		transactionOID = request.getParameter("transOid");
	}

	/* If transaction_oid is not found in the request, 
	then it means returning from mediator.  Get it from docCache Cache. */
	if (null == transactionOID) {
		if (null != docCache) {
			if (null != docCache
					.getAttribute("/In/Transaction/transaction_oid")) {
				transactionOID = docCache
						.getAttribute("/In/Transaction/transaction_oid");
			}
		}
	}

	Debug.debug("transaction_oid : " + transactionOID);

	String optLock = null;
	String bankTransactionStatus = null;
	String bankIntrumentID = null;
	String bankRejectionReason = null;
	String bankInternalInfo = null;
	String userID = null;
	String instrumentOID = null;
	String bankUpdateStatus = null;
	String completeInstrumentID = null;
	String corpOrgOid = instrument.getAttribute("corp_org_oid");
	corpOrg.getById(corpOrgOid);

	//populate data from DB
	if (populateFromDB) {
		//initialize transactioin / instrument bean
		transaction.setAttribute("transaction_oid", transactionOID);
		transaction.getDataFromAppServer();

		instrumentOID = transaction.getAttribute("instrument_oid");
		instrument.setAttribute("instrument_oid", instrumentOID);
		instrument.getDataFromAppServer();

		//initialize  BankTransactionUpdate bean
		bankTransUpdate.setAttribute("bank_transaction_update_oid",
				bankTransUpdateOID);
		bankTransUpdate.getDataFromAppServer();
		
		terms = transaction.registerCustomerEnteredTerms();

		String termsPartyOid;

		termsPartyOid = terms.getAttribute("c_FirstTermsParty");
		if (termsPartyOid != null && !termsPartyOid.equals("")) {
			termsPartyBen
					.setAttribute("terms_party_oid", termsPartyOid);
			termsPartyBen.getDataFromAppServer();
		}
		termsPartyOid = terms.getAttribute("c_SecondTermsParty");

		if (termsPartyOid != null && !termsPartyOid.equals("")) {
			termsPartyApp
					.setAttribute("terms_party_oid", termsPartyOid);
			termsPartyApp.getDataFromAppServer();
		}
		termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
		if (termsPartyOid != null && !termsPartyOid.equals("")) {
			termsPartyAdv
					.setAttribute("terms_party_oid", termsPartyOid);
			termsPartyAdv.getDataFromAppServer();
		}


		bankIntrumentID = instrument.getAttribute("bank_instrument_id");
		completeInstrumentID = instrument.getAttribute("complete_instrument_id");
		bankTransactionStatus = bankTransUpdate
				.getAttribute("bank_transaction_status");
		bankRejectionReason = bankTransUpdate
				.getAttribute("bank_rejection_reason_text");
		bankInternalInfo = bankTransUpdate
				.getAttribute("bank_internal_info");
		optLock = bankTransUpdate.getAttribute("opt_lock");
		userID = bankTransUpdate.getAttribute("bank_user_oid");
		bankUpdateStatus =  bankTransUpdate.getAttribute("bank_update_status");

	} else {
		//populate data from cache (docCache)
		if (null != docCache) {
			if (null != docCache
					.getAttribute("/In/Instrument/instrument_oid")) {
				instrumentOID = docCache
						.getAttribute("/In/Instrument/instrument_oid");
			}
			if (null != docCache
					.getAttribute("/In/Instrument/bank_instrument_id")) {
				bankIntrumentID = docCache
						.getAttribute("/In/Instrument/bank_instrument_id");
			}
			if (null != docCache
					.getAttribute("/In/Instrument/complete_instrument_id")) {
				completeInstrumentID = docCache
						.getAttribute("/In/Instrument/complete_instrument_id");
			}
			if (null != docCache
					.getAttribute("/In/BankTransactionUpdate/bank_transaction_update_oid")) {
				bankTransUpdateOID = docCache
						.getAttribute("/In/BankTransactionUpdate/bank_transaction_update_oid");
			}
			if (null != docCache
					.getAttribute("/In/BankTransactionUpdate/bank_rejection_reason_text")) {
				bankRejectionReason = docCache
						.getAttribute("/In/BankTransactionUpdate/bank_rejection_reason_text");
			}
			if (null != docCache
					.getAttribute("/In/BankTransactionUpdate/bank_internal_info")) {
				bankInternalInfo = docCache
						.getAttribute("/In/BankTransactionUpdate/bank_internal_info");
			}
			if (null != docCache
					.getAttribute("/In/BankTransactionUpdate/bank_transaction_status")) {
				bankTransactionStatus = docCache
						.getAttribute("/In/BankTransactionUpdate/bank_transaction_status");
			}
			if (null != docCache
					.getAttribute("/In/BankTransactionUpdate/opt_lock")) {
				optLock = docCache
						.getAttribute("/In/BankTransactionUpdate/opt_lock");
			}
			if (null != docCache
					.getAttribute("/In/Transaction/transaction_oid")) {
				transactionOID = docCache
						.getAttribute("/In/Transaction/transaction_oid");
			}
			if (null != docCache
					.getAttribute("/In/BankTransactionUpdate/bank_user_oid")) {
				userID = docCache
						.getAttribute("/In/BankTransactionUpdate/bank_user_oid");
			}
			if (null != docCache
					.getAttribute("/In/BankTransactionUpdate/bank_update_status")) {
				bankUpdateStatus = docCache
						.getAttribute("/In/BankTransactionUpdate/bank_update_status");
			}

		}

	}//end of else
		
		String bankTransUpdateMenu = null;
		boolean isSameUser = false;
		boolean isRepair = false;
		boolean isAmend = false;		
		/* Based upon bankUpdateStatus value, decide what menu to be displayed.
		Also descide if the page should be readOnly*/
		if( !StringFunction.isBlank(bankUpdateStatus) ){
			if( (TradePortalConstants.BANK_TRANS_UPDATE_STATUS_NOT_STARTED.equalsIgnoreCase(bankUpdateStatus))
					|| (TradePortalConstants.BANK_TRANS_UPDATE_STATUS_IN_PROGRESS.equalsIgnoreCase(bankUpdateStatus)) ){
				bankTransUpdateMenu = TradePortalConstants.BANK_IN_PROGRESS;
				isReadOnly = false;
			}else if( TradePortalConstants.BANK_TRANS_UPDATE_STATUS_APPLIED.equalsIgnoreCase(bankUpdateStatus) ){
				isReadOnly = true;
				bankTransUpdateMenu = TradePortalConstants.BANK_APPLIED;
				//Processor cant view approve/repair button
				if(StringFunction.isNotBlank(userID)){	
					if(StringFunction.isNotBlank(userSession.getUserOid())){	
						if(userID.equals(userSession.getUserOid())){
							isSameUser = true;
						}
					}
				}
			}else if( TradePortalConstants.BANK_TRANS_UPDATE_STATUS_APPROVED.equalsIgnoreCase(bankUpdateStatus) ){
				isReadOnly = true;
				bankTransUpdateMenu = TradePortalConstants.BANK_APPROVED;
			}else if( TradePortalConstants.BANK_TRANS_UPDATE_STATUS_REPAIR.equalsIgnoreCase(bankUpdateStatus) ){
				isReadOnly = true;
				isRepair = true;
				bankTransUpdateMenu = TradePortalConstants.BANK_REPAIR;
				
				if(StringFunction.isNotBlank(userID)){	
					if(StringFunction.isNotBlank(userSession.getUserOid())){	
						if(userID.equals(userSession.getUserOid())){
							isSameUser = true;
						}
					}
				}
			}
		}
		if ( TransactionType.AMEND.equals(transactionType) ){
			isAmend = true;
		}else{
			isAmend = isReadOnly;
		}
		//if instrument locked, show the page in readonly mode.
		if(isInstrumentLocked){
			isReadOnly = true;
			isAmend = true;
			isRepair = true;
			bankTransUpdateMenu = TradePortalConstants.BANK_INSTRUMENT_LOCKED;
		}
		
		Debug.debug("isInstrumentLocked : " + isInstrumentLocked);
		Debug.debug("bankTransUpdateMenu : " + bankTransUpdateMenu);
		Debug.debug("isReadOnly : " + isReadOnly);
		
		//get user oid from userSession if its blank
		if(StringFunction.isBlank(userID)){
			userID = userSession.getUserOid();
		}
		
		StringBuffer bankTitle = new StringBuffer();
		bankTitle.append(resMgr.getText("BankTransactionUpdate.Title.Instruments", TradePortalConstants.TEXT_BUNDLE));
		bankTitle.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
                instrument.getAttribute("instrument_type_code"), 
                userSession.getUserLocale()) );
 		bankTitle.append( " - " );
 		bankTitle.append(resMgr.getText("BankTransactionUpdate.Title", TradePortalConstants.TEXT_BUNDLE));
 		
 		// Retrieve the name of the counter party associated with the instrument
 	   String otherPartyOid = instrument.getAttribute("a_counter_party_oid");
 	   TermsPartyWebBean    counterParty                    = null;
 	   String otherPartyName = null;
 	  
 	   if (!InstrumentServices.isBlank(otherPartyOid))
 	   {
 	      counterParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

 	      counterParty.getById(otherPartyOid);

 	     otherPartyName = counterParty.getAttribute("name");
 	   }
 	   else
 	   {
 		  otherPartyName = "";
 	   }
 
%>

<jsp:include page="/common/Header.jsp">
	<jsp:param name="autoSaveFlag" value="<%=autoSaveFlag %>" />
</jsp:include>

<div class="pageMain">
	<%--PageMainDiv--%>
	<div class="pageContent">
		<%--PageContentDiv--%>

		<jsp:include page="/common/PageHeader.jsp">
			<jsp:param name="titleKey" value="<%=bankTitle %>" />
			<jsp:param name="item1Key" value="" />
			<jsp:param name="helpUrl" value="admin/updatecentre_trans_status.htm" />
		</jsp:include>
		<jsp:include page="/common/TransactionSubHeader.jsp" />

		<form id="BankTransStatusUpdateDetails-AMD-Form"
			name="BankTransStatusUpdateDetails-AMD-Form" method="post"
			data-dojo-type="dijit.form.Form"
			action="<%=formMgr.getSubmitAction(response)%>">
			<input type=hidden value="" name="buttonName">
			<%-- error section goes above form content --%>
			<div class="formArea">
				<%--FormArea--%>
				<jsp:include page="/common/ErrorSection.jsp" />
				<div class="formContent">
					<%--FormContent--%>

					<%
						// Store values such as the userid, security rights, and org in a
						// secure hashtable for the form.  Also store instrument and transaction
						// data that must be secured.
						Hashtable secureParms = new Hashtable();

						secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
						   secureParms.put("login_oid",                userSession.getUserOid());
						   secureParms.put("login_rights",             userSession.getSecurityRights());
						   secureParms.put("ownership_level",userSession.getOwnershipLevel());
						   secureParms.put("corp_org_oid", userSession.getOwnerOrgOid());
						   secureParms.put("complete_instrument_id", completeInstrumentID);
						   secureParms.put("transaction_type", transactionType);

						String extraPartTags = request.getParameter("extraPartTags");
					%>

					<%@ include file="fragment/Instruments-AttachDocuments.frag"%>

					<%=widgetFactory.createSectionHeader("1", "BankTransactionUpdate.Section1")%>
					<% if(transactionType.equals(TransactionType.AMEND)) { if
					(instrumentType.equals(InstrumentType.IMPORT_DLC)){ %>
					<%@ include file="fragment/BankTransStatusUpdateDetail-IMP_DLC_AMD-Terms.frag"%>
					<% }else if(instrumentType.equals(InstrumentType.STANDBY_LC)){ %>
					<%@ include file="fragment/BankTransStatusUpdateDetail-SLC_AMD-Terms.frag"%>
					<% }else if(instrumentType.equals(InstrumentType.GUARANTEE)){ %>
					<%@ include file="fragment/BankTransStatusUpdateDetail-GUA_AMD-Terms.frag"%>
					<% }} %>
					
				</div>
				<%=widgetFactory.createSectionHeader("2", "BankTransactionUpdate.Section2")%>
					<%@ include
						file="fragment/BankTransStatusUpdateDetails-StatusUpdate.frag"%>
				</div>
				<%=widgetFactory.createSectionHeader("3",
					"BankTransactionUpdate.Section3")%>
				<%@ include
					file="fragment/BankTransStatusUpdateDetails-InternalInfo.frag"%>
			</div>

			<input type=hidden name="bankTransUpdateOID"
				value="<%=bankTransUpdateOID%>"> <input type=hidden
				name="transactionOID" value="<%=transactionOID%>"> <input
				type=hidden name="bankUserOID" value="<%=userID%>"> <input
				type=hidden name="instrumentOID" value="<%=instrumentOID%>">
			<input dojoType="dijit.form.TextBox" type="hidden"
				name="bankUpdateStatus" id="bankUpdateStatus"
				value="<%=bankUpdateStatus %>" />
	</div>
	<%--FormContent--%>
</div>
<%--FormArea--%>
<div class="formSidebar" data-dojo-type="widget.FormSidebar"
	data-dojo-props="title: '', form: 'BankTransStatusUpdateDetails-AMD-Form'">
	<jsp:include page="/common/Sidebar.jsp">
		<jsp:param name="isReadOnly" value="<%=isReadOnly %>" />
		<jsp:param name="bankTransUpdateMenu"
			value="<%=bankTransUpdateMenu %>" />
		<jsp:param name="buttonPressed" value="" />
		<jsp:param name="error" value="" />
		<jsp:param name="formName" value="0" />
		<jsp:param name="showLinks" value="true" />
		<jsp:param name="isSameUser" value="<%=isSameUser%>" />
	</jsp:include>
</div>


<%=formMgr.getFormInstanceAsInputField(
					"BankTransStatusUpdateDetails-AMD-Form", secureParms)%>
</form>

</div>
<%--PageContentDiv--%>
</div>
<%--PageMainDiv--%>
<jsp:include page="/common/Footer.jsp">
	<jsp:param name="autoSaveFlag" value="<%=autoSaveFlag %>" />
	<jsp:param name="displayFooter"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/common/SidebarFooter.jsp"></jsp:include>

</body>
</html>

<script type="text/javascript">
	
<%// Finally, reset the cached document to eliminate carryover of
			// information to the next visit of this page.
			formMgr.storeInDocCache("default.doc", new DocumentHandler());%>
</script>
