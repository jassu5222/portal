<%--
**********************************************************************************
                              Bank Transaction Updates List page
							  
  Description:
     This page is used to allow admin users to view all authorized tranasctions, 
     which are not integreted with TPS
**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

  Debug.debug("***START***********Bank TRANSACTION Update LIST***************START***");

DocumentHandler   xmlDoc              = null;
DocumentHandler   corpCustomersDoc	= null;
String            ownershipLevel      = null;
String			instrTypeOptions	= null;
StringBuffer 		sqlQuery 			= null;
StringBuffer		customerOptions		= new StringBuffer();
Vector<String> 	instrumentTypes 	= new Vector<String>();
StringBuffer 	bankUpdateStatusOptions = new StringBuffer();
DocumentHandler bankUpdateStatusDoc = null;

  
WidgetFactory widgetFactory = new WidgetFactory(resMgr);
//DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, response);
ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
HttpSession	theSession    = request.getSession(false);

  // Set the global navigation tab indicator to the Update Centre tab
  userSession.setCurrentPrimaryNavigation("AdminNavigationBar.UpdateCentre");
//set the current navigation page in session 
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); //to keep it correct
  }
  else {
    userSession.addPage("goToBankTransactionUpdates");
  }
  //remove unwanted attributes from session
	theSession.removeAttribute("fromTPHomePage");
	theSession.removeAttribute("startHomePage");
	theSession.removeAttribute("newTransaction");
	theSession.removeAttribute("isFromTransDetailPage"); 
	theSession.removeAttribute("TradePortalHome");
	//Set tps indicator to yes. 
	userSession.setCustNotIntgTPS(true);
   
  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryData =null;
  // Determine whether any errors are being returned to this page. If there are, set the 
  // original search criteria selected/entered by the user for the listview.
  xmlDoc = formMgr.getFromDocCache();

  ownershipLevel = userSession.getOwnershipLevel();
  String loginLocale = userSession.getUserLocale();
		  
  //Building options for bank update status dropdown  
  Vector<String> codesToExclude = new Vector<String>();
  codesToExclude.add(TradePortalConstants.BANK_TRANS_UPDATE_STATUS_APPROVED); 
  //Add option All
  bankUpdateStatusOptions.append("<option value=\"").append(TradePortalConstants.STATUS_ALL).append("\" >");
  bankUpdateStatusOptions.append(resMgr.getText( "common.all", TradePortalConstants.TEXT_BUNDLE));
  bankUpdateStatusOptions.append("</option>");
  
  bankUpdateStatusOptions.append(Dropdown.createSortedRefDataOptionsNoDefault(TradePortalConstants.BANK_UPDATE_STATUS_TYPE, 
		  resMgr.getResourceLocale(), codesToExclude)); 
    
  List<Object> sqlParams = new ArrayList<Object>();
  sqlQuery = new StringBuffer();
  //Building options for CustomerName
   sqlQuery.append("select a.organization_oid, a.name");
   sqlQuery.append(" from corporate_org a, bank_organization_group b"); 
   sqlQuery.append(" where a.a_bank_org_group_oid = b.organization_oid ");
   
   //If client Bank
   if (TradePortalConstants.OWNER_BANK.equals(ownershipLevel)) {
       sqlQuery.append(" and b.p_client_bank_oid = ? ");
       sqlParams.add(userSession.getClientBankOid());
       //In Client Bank, if any restricted bank Org group (defined in bank grp restriction)
       if (StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid())) {
           sqlQuery.append(" and b.organization_oid not in ( ");
           sqlQuery.append("select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ");
           sqlParams.add(userSession.getBankGrpRestrictRuleOid());
           sqlQuery.append(" )");
       }
       
       //If Bank Organization Group
   } else if (TradePortalConstants.OWNER_BOG.equals(ownershipLevel)) {
       sqlQuery.append(" and a_bank_org_group_oid = ? ");
       sqlParams.add(userSession.getBogOid());
   }  
   else {
       sqlQuery.append(" and 1=0");
   }
   sqlQuery.append(" and a.activation_status = ? ");
   sqlParams.add(TradePortalConstants.ACTIVE);
   sqlQuery.append(" and a.cust_is_not_intg_tps = ? ");
   sqlParams.add(TradePortalConstants.INDICATOR_YES);
   
   sqlQuery.append(" order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));
   
   corpCustomersDoc  = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams);
   customerOptions.append(Dropdown.createSortedOptions(corpCustomersDoc, "ORGANIZATION_OID", 
           "NAME", "", resMgr.getResourceLocale()));
   
   //Building options for instrumentTypes
   instrumentTypes.addElement(InstrumentType.AIR_WAYBILL);
   instrumentTypes.addElement(InstrumentType.IMPORT_DLC);
   instrumentTypes.addElement(InstrumentType.LOAN_RQST);
   instrumentTypes.addElement(InstrumentType.GUARANTEE);
   instrumentTypes.addElement(InstrumentType.STANDBY_LC);
   instrumentTypes.addElement(InstrumentType.SHIP_GUAR);
   instrTypeOptions = Dropdown.getInstrumentList( "", loginLocale, instrumentTypes );
  
  String searchNav = "BankTransUpdate";  
  String currency = "";
  String options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);

%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>
<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="BankTransactionUpdateGrid.Title" />
      <jsp:param name="helpUrl"  value="admin/updatecentre_trans_listview.htm" />
    </jsp:include>

    <form name="BankTransactionUpdateForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">      
      <input type=hidden value="" name=buttonName>
      <jsp:include page="/common/ErrorSection.jsp" />
      <div class="formContentNoSidebar">

        <div class="searchHeader">
	        <%=widgetFactory.createSearchSelectField("bankUpdateStatus","BankTransactionUpdate.show","", bankUpdateStatusOptions.toString(), "onChange='filterBankTranUpdateGridView();'" )%>
			<%=widgetFactory.createCheckboxField("xmlDownloadInd", "BankTransactionUpdate.XMLDownload", false, false, false,"onClick='filterBankTranUpdateGridView();'","","none")%>
			<div class="searchHeaderActions">
				<jsp:include page="/common/dGridShowCount.jsp">
					<jsp:param name="gridId" value="bankTransactionUpdateGridId" />
				</jsp:include>
	
				<span id="bankTransactionUpdateRefresh" class="searchHeaderRefresh"></span>
				<%=widgetFactory.createHoverHelp("bankTransactionUpdateRefresh","RefreshImageLinkHoverText")%>
				<span id="bankTransactionUpdateGridEdit" class="searchHeaderEdit"></span>
				<%=widgetFactory.createHoverHelp("bankTransactionUpdateGridEdit","CustomizeListImageLinkHoverText")%>
				<div style="clear: both"></div>
			</div>
			<div class="searchDivider"></div>

			<div class="searchDetail">
				<span class="searchCriteria"> 
				<%=widgetFactory.createSearchTextField("InstrumentId","BankTransactionUpdate.InstrumentID","16", " class='char15' onKeydown='filterGridOnEnter(window.event, \"InstrumentId\");'")%>
				<%=widgetFactory.createSearchSelectField("CustomerName","BankTransactionUpdate.Customer"," ", customerOptions.toString(), "onChange='filterBankTranUpdateGridView();'" )%>
					<%=widgetFactory.createSearchTextField("BankInstrumentId","BankTransactionUpdate.BankInstrumentID","16", " class='char15' onKeydown='filterGridOnEnter(window.event, \"BankInstrumentId\");'")%>
				</span>
				<div style="clear: both;"></div>
				<%=widgetFactory.createSearchSelectField("InstrumentType","BankTransactionUpdate.InstrumentType"," ", instrTypeOptions, "onChange='filterTransTypesOptions(); filterBankTranUpdateGridView();'" )%>
				<%=widgetFactory.createSearchSelectField("TransactionType","BankTransactionUpdate.TransactionType"," ", "", "onChange='filterBankTranUpdateGridView();'" )%>
				<div style="clear: both;"></div>
				<%=widgetFactory.createSearchSelectField("Currency", "BankTransactionUpdate.Currency", " ", options,"onChange='filterBankTranUpdateGridView();'")%>
				<%=resMgr.getText("BankTransactionUpdate.Amount",TradePortalConstants.TEXT_BUNDLE)%>
				<%=widgetFactory.createSearchAmountField("AmountFrom", "BankTransactionUpdate.From","","style='width:8em' class='char5'", "")%>
				<%=widgetFactory.createSearchAmountField("AmountTo", "BankTransactionUpdate.To","","style='width:8em' class='char5'", "")%>
				<span class="searchActions">
            <button id="searchButton" data-dojo-type="dijit.form.Button" type="button" >
              <%=resMgr.getText("common.search",TradePortalConstants.TEXT_BUNDLE)%>
            </button>
            <%=widgetFactory.createHoverHelp("searchButton","SearchHoverText") %>
          </span>
				<div style="clear: both;"></div>
			</div>

        
          
<% 
if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	 savedSearchQueryData = (Map)searchQueryMap.get("BankTransactionUpdateDataView");
}

  String gridHtml = dgFactory.createDataGrid("bankTransactionUpdateGridId","BankTransactionUpdateDataGrid",null);
%>

<%= gridHtml %>
<% String gridLayout = dgFactory.createGridLayout("bankTransactionUpdateGridId","BankTransactionUpdateDataGrid"); %>        

      </div>
      </div>
    </form>

  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag"%>
<script>

var bankUpdateStatusType = "";
var bankXMLDownloadInd = "";
var customer = "";
var instrumentID = "";
var bankInstrumentID = "";
var transactionID = "";
var instrumentType = "";
var transactionType = "";
var currency = "";
var amountFrom = "";
var amountTo = "";
var initSearchParms = "";
var bankTransactionUpdateGridId ="";
var gridLayout = "";
var viewName = "";
var tempDatePattern = "";
var savedSort = savedSearchQueryData["sort"];

	function filterTransTypesOptions() {
		  require(["dojo/dom","dojo/store/Memory", "dijit/registry", "dojo/domReady!"],
		      function(dom,Memory,registry){
			    var instrumentType = registry.byId("InstrumentType").attr('value');
			    var transactionType = registry.byId("TransactionType");
			    transactionType.attr("value","",false); <%-- clear display value --%>
			    if("" == instrumentType || null == instrumentType || "undefined" == instrumentType)
			    {
			    		transactionType.store = new Memory({data: [
		    		                                           {id: "", name: ""},
		    		                                           {id: "<%=TransactionType.ISSUE%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionISS", TradePortalConstants.TEXT_BUNDLE)%>"},
		    		                                           {id: "<%=TransactionType.AMEND%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionAMD", TradePortalConstants.TEXT_BUNDLE)%>"},
		    		                                           {id: "<%=TransactionType.RELEASE%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionREL", TradePortalConstants.TEXT_BUNDLE)%>"}
			    		                                       ]});
			    }else if("<%=InstrumentType.AIR_WAYBILL%>" == instrumentType){
			    		transactionType.store = new Memory({data: [
		    		                                           {id: "", name: ""},
		    		                                           {id: "<%=TransactionType.RELEASE%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionREL", TradePortalConstants.TEXT_BUNDLE)%>"}
		    		                                       ]});
			    }else if("<%=InstrumentType.IMPORT_DLC%>" == instrumentType || "<%=InstrumentType.GUARANTEE%>" == instrumentType || "<%=InstrumentType.STANDBY_LC%>" == instrumentType){
			    		transactionType.store = new Memory({data: [
		    		                                           {id: "", name: ""},
		    		                                           {id: "<%=TransactionType.ISSUE%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionISS", TradePortalConstants.TEXT_BUNDLE)%>"},
		    		                                           {id: "<%=TransactionType.AMEND%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionAMD", TradePortalConstants.TEXT_BUNDLE)%>"}
		    		                                       ]});
			    }else if("<%=InstrumentType.LOAN_RQST%>" == instrumentType || "<%=InstrumentType.SHIP_GUAR%>" == instrumentType){
			    		transactionType.store = new Memory({data: [
		    		                                           {id: "", name: ""},
		    		                                           {id: "<%=TransactionType.ISSUE%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionISS", TradePortalConstants.TEXT_BUNDLE)%>"}
		    		                                       ]});
			    }
		            
		    });
	}
	
  require(["t360/OnDemandGrid", "dijit/registry", "dojo/on", "t360/common", "t360/datagrid", "t360/popup", 
           "dojo/dom","dojo/query","dojo/dom-construct", "dojo/ready"],
      function(onDemandGrid, registry, on, common, t360grid, t360popup, dom,query,domConstruct, ready) {
	  ready(function() {
		  
		gridLayout = <%= gridLayout %>;
		viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("BankTransactionUpdateDataView",userSession.getSecretKey())%>"; 
		filterTransTypesOptions();
	 	bankUpdateStatusType = savedSearchQueryData["bankUpdateStatusType"];
	 	bankXMLDownloadInd = savedSearchQueryData["bankXMLDownloadInd"];
	 	customer = savedSearchQueryData["customer"];
	 	instrumentID = savedSearchQueryData["instrumentID"];
	 	bankInstrumentID = savedSearchQueryData["bankInstrumentID"];
	 	instrumentType = savedSearchQueryData["instrumentType"];
	 	transactionType = savedSearchQueryData["transactionType"];
	 	currency = savedSearchQueryData["currency"];
	 	amountFrom = savedSearchQueryData["amountFrom"];
	 	amountTo = savedSearchQueryData["amountTo"];
	 	
	 	var savedSort = savedSearchQueryData["sort"];  
      	if("" == savedSort || null == savedSort || "undefined" == savedSort){
    		  savedSort = '0';
    	 }
	 	
	 	if("" == bankUpdateStatusType || null == bankUpdateStatusType || "undefined" == bankUpdateStatusType)
	 		bankUpdateStatusType = registry.byId('bankUpdateStatus').attr('value');	
    	else
    		  registry.byId("bankUpdateStatus").set('value',bankUpdateStatusType);
	 	
	 	if("" == bankXMLDownloadInd || null == bankXMLDownloadInd || "undefined" == bankXMLDownloadInd){
	 		bankXMLDownloadInd="";
	 	}
    	else{
    		  dom.byId("xmlDownloadInd").checked = bankXMLDownloadInd;
    	}
	 	
	 	if("" == customer || null == customer || "undefined" == customer)
	 		customer = registry.byId('CustomerName').attr('value');	
    	else
    		  registry.byId("CustomerName").set('value',customer);
	 	
	 	if("" == instrumentID || null == instrumentID || "undefined" == instrumentID)
    		instrumentID = checkString( dijit.byId("InstrumentId") );
   		else
   			dom.byId("InstrumentId").value = instrumentID;
	 	
	 	if("" == bankInstrumentID || null == bankInstrumentID || "undefined" == bankInstrumentID)
	 		bankInstrumentID = checkString( dijit.byId("BankInstrumentId") );
   		else
   			dom.byId("BankInstrumentId").value = bankInstrumentID;
	 	
	 	if("" == instrumentType || null == instrumentType || "undefined" == instrumentType)
    		instrumentType = registry.byId('InstrumentType').attr('value');	
    	else
    		registry.byId("InstrumentType").set('value',instrumentType,false);
	 	
	 	if("" == transactionType || null == transactionType || "undefined" == transactionType)
	 		transactionType = registry.byId('TransactionType').attr('value');	
    	else
    		registry.byId("TransactionType").set('value',transactionType,false);
	 	
	 	if("" == currency || null == currency || "undefined" == currency)
	 		currency = registry.byId('Currency').attr('value');	
    	else
    		registry.byId("Currency").set('value',currency);
	 	
	 	if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
	 		amountFrom = checkString( dijit.byId("AmountFrom") );
   		else
   			dom.byId("AmountFrom").value = amountFrom;
	 	
	 	if("" == amountTo || null == amountTo || "undefined" == amountTo)
	 		amountTo = checkString( dijit.byId("AmountTo") );
   		else
   			dom.byId("AmountTo").value = amountTo;

	 	initSearchParms = initSearchParms+"&bankUpdateStatusType="+bankUpdateStatusType+"&bankXMLDownloadInd="+bankXMLDownloadInd+
				        "&customer="+customer+"&instrumentID="+instrumentID+"&bankInstrumentID="+bankInstrumentID+
						"&transactionType="+transactionType+"&instrumentType="+instrumentType+
						"&currency="+currency+"&amountFrom="+amountFrom+"&amountTo="+amountTo;
		console.log("initSearchParms-"+initSearchParms);
		
		bankTransactionUpdateGridId =onDemandGrid.createOnDemandGrid("bankTransactionUpdateGridId", viewName, gridLayout, 
				initSearchParms,savedSort);
		    
		 query('#bankTransactionUpdateRefresh').on("click", function() {
			 filterBankTranUpdateGridView();
	     });
		 
		 query('#bankTransactionUpdateGridEdit').on("click", function() {
	          var columns = onDemandGrid.getColumnsForCustomization('bankTransactionUpdateGridId');
	          var parmNames = [ "gridId", "gridName", "columns" ];
	          var parmVals = [ "bankTransactionUpdateGridId", "BankTransactionUpdateDataGrid", columns ];
	          t360popup.open(
	            'bankTransactionUpdateGridEdit', 'dGridCustomizationPopup.jsp',
	            parmNames, parmVals,
	            null, null);  <%-- callbacks --%>
	     });
		 
		 registry.byId("searchButton").on("click", function() {
			 filterBankTranUpdateGridView();
		      });
		
	    });
    
  });
  
  function filterBankTranUpdateGridView() {
	  require(["dojo/dom","t360/datagrid","dijit/registry","t360/OnDemandGrid",],
	      function(dom,t360grid,registry,onDemandGridSearch){
		 
		    var searchParms = "";
		    
	        bankUpdateStatusType = registry.byId("bankUpdateStatus").attr('value');
	     	bankXMLDownloadInd = dom.byId("xmlDownloadInd").checked;
	     	if(false === bankXMLDownloadInd){
	     		bankXMLDownloadInd = "";
	     	}
	        customer = registry.byId("CustomerName").attr('value');
	        instrumentID = dom.byId("InstrumentId").value;
	        bankInstrumentID = dom.byId("BankInstrumentId").value;
	        <%-- transactionID = dom.byId("TransactionId").value; --%>
	        instrumentType = registry.byId("InstrumentType").attr('value');
	        transactionType = registry.byId("TransactionType").attr('value');
	        currency = registry.byId("Currency").attr('value');
	        amountFrom = dom.byId("AmountFrom").value;
	        amountTo = dom.byId("AmountTo").value;
	        
	        var regex = new RegExp(',', 'g'); <%-- Created reg exp to find comma globally --%>
       	 	if(amountFrom!=null){
   		  		amountFrom = amountFrom.replace(regex, '');		    	
  			}	
  			if(amountTo!=null){
    		 	amountTo = amountTo.replace(regex, '');		    	
   			}
	        
	        searchParms = searchParms+"&bankUpdateStatusType="+bankUpdateStatusType+"&bankXMLDownloadInd="+bankXMLDownloadInd+
	        "&customer="+customer+"&instrumentID="+instrumentID+"&bankInstrumentID="+bankInstrumentID+
			"&transactionType="+transactionType+"&instrumentType="+instrumentType+
			"&currency="+currency+"&amountFrom="+amountFrom+"&amountTo="+amountTo;
	        
	        <%-- searchParms = ""; --%>
	        
	               
	        onDemandGridSearch.searchDataGrid("bankTransactionUpdateGridId", "BankTransactionUpdateDataView",searchParms);
	            
	    });
  }
  
  function filterGridOnEnter(event, fieldID){
		require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldID), "keypress", function(event) {
		        if (event && event.keyCode == 13) {
		        	dojo.stopEvent(event);
		        	filterBankTranUpdateGridView();
		        }
		    });
		});
	  }

  function checkString(inquiryStr){
		if(null == inquiryStr)
			return "";
		else
			return inquiryStr.attr('value');
	}  
 

</script>

<jsp:include page="/common/dGridShowCountFooter.jsp">
		  <jsp:param name="gridId" value="bankTransactionUpdateGridId" />
		</jsp:include>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
