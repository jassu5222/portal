	<%--
	*
	*     Copyright  � 2001                         
	*     American Management Systems, Incorporated 
	*     All rights reserved
	--%>
	<%--
	*******************************************************************************
	Outgoing Guarante Amend Page - General section

		Description:
		Contains HTML to create the Outgoing Guarante Amend Page - General section.

		This is not a standalone JSP.  It MUST be included using the following tag:
		<%@ include file="BankTransStatusUpdateDetails-ISS.jsp" %>
*******************************************************************************
	--%>
	<%
	
	
 String incrDecrValue     = TradePortalConstants.NOAMOUNT;
 String transactionId     = transaction.getAttribute("display_transaction_oid");
 String issueDate         = TPDateTimeUtility.formatDate( transaction.getAttribute("first_authorize_status_date"), TPDateTimeUtility.SHORT, loginLocale );
 String expiryDate        = TPDateTimeUtility.formatDate( terms.getAttribute("expiry_date"), TPDateTimeUtility.SHORT, loginLocale );
 String currency          = terms.getAttribute("amount_currency_code");
 String amount            = transaction.getAttribute("copy_of_amount");
 String displayAmount;

 displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currency, loginLocale);
 

 String customer=corpOrg.getAttribute("name");
 
 if(transactionType.equals(TransactionType.ISSUE)){
	  
	  transactionType="Issue";
	  }
	  else if(transactionType.equals(TransactionType.AMEND)) 
	  {
	  transactionType="Amend";
	  }
	  else
	  {
	  transactionType="Release";
	  }

 
 String tolerancePos;
 String toleranceNeg;
 String percentValue;
 
    try
         {
	      tolerancePos = terms.getAttribute("amt_tolerance_pos");
	      toleranceNeg = terms.getAttribute("amt_tolerance_neg");
	      
	      if(StringFunction.isBlank(tolerancePos)){
	    	  tolerancePos = "";
	      }
	      if(StringFunction.isBlank(toleranceNeg)){
	    	  toleranceNeg = "";
	      }

	     }
         catch(Exception e)
         {
	       tolerancePos = "";
	       toleranceNeg = "";
	     }
 boolean displayAboutPercent ;
 
    if ((!InstrumentServices.isBlank(tolerancePos)) &&
	        (!InstrumentServices.isBlank(toleranceNeg)))
                 displayAboutPercent = true;
            else
                 displayAboutPercent = false;


            	   percentValue = resMgr.getText("transaction.Plus",TradePortalConstants.TEXT_BUNDLE)
            	                        + tolerancePos
				                        + resMgr.getText("transaction.Percent",TradePortalConstants.TEXT_BUNDLE)
				                        + " "
				                        + resMgr.getText("transaction.Minus",TradePortalConstants.TEXT_BUNDLE)
				                        + toleranceNeg
				                        + resMgr.getText("transaction.Percent",TradePortalConstants.TEXT_BUNDLE);
 
            	   BigDecimal tempAmount = new BigDecimal(0d);            	  
            	   try {            		   
            		   if(null != amount){
                		   tempAmount = new BigDecimal(amount);
                	   }            	        
            	     } catch (Exception e) {
            	        tempAmount = new BigDecimal(0d);
            	     }
            	     if (tempAmount.doubleValue() < 0){ incrDecrValue = TradePortalConstants.DECREASE;}
            		 else if (tempAmount.doubleValue() > 0){ incrDecrValue = TradePortalConstants.INCREASE;}
            	   //change the negative sign
            	     if(null != displayAmount){
            	    	 displayAmount = displayAmount.replaceFirst("-", "");
            	     }
	%>
	<table width="100%">
	  	<tr class="formItem" width="100%">
	  		<td style="vertical-align: top;" width="15%"><%= resMgr.getText("BankTransStatusUpdate.Customer",TradePortalConstants.TEXT_BUNDLE)%></td>
	  		<td style="font-weight: bold; word-wrap: break-word; vertical-align: top;" width="25%"><%=customer%></td>
	  		<td style="vertical-align: top;" width="15%"><%= resMgr.getText("BankTransStatusUpdate.TransactionType",TradePortalConstants.TEXT_BUNDLE)%></td>
	  		<td style="font-weight: bold; word-wrap: break-word; vertical-align: top;" width="15%"><%=transactionType %></td>
	  		<td style="vertical-align: top;" width="10%"><%= resMgr.getText("BankTransStatusUpdate.TransactionId",TradePortalConstants.TEXT_BUNDLE)%></td>
	  		<td style="font-weight: bold; word-wrap: break-word; vertical-align: top;" width="20%"><%=transactionId %></td>
	  	</tr>
	  	<tr class="formItem" width="100%"><td>&nbsp;</td></tr>
	  	<tr class="formItem" width="100%">
	      	<td style="vertical-align: top;" width="15%"><%= resMgr.getText("BankTransStatusUpdate.EntryDate",TradePortalConstants.TEXT_BUNDLE)%></td>
	  		<td style="font-weight: bold; word-wrap: break-word; vertical-align: top;" width="25"><%=issueDate%></td>
	  		<td style="vertical-align: top;" width="15%"><%= resMgr.getText("BankTransStatusUpdate.NewExpiryDate",TradePortalConstants.TEXT_BUNDLE)%></td>
	  		<td style="font-weight: bold; word-wrap: break-word; vertical-align: top;" width="15%"><%=expiryDate %></td>
	  		<td style="vertical-align: top;" width="10%"><%= resMgr.getText("BankTransStatusUpdate.OtherParty",TradePortalConstants.TEXT_BUNDLE)%></td>
	  		<td style="font-weight: bold; word-wrap: break-word; vertical-align: top;" width="20%"><%=otherPartyName %></td>
	  	</tr>
	  	<tr class="formItem" width="100%"><td>&nbsp;</td></tr>
	  	<tr class="formItem" width="100%">
	  		<td style="vertical-align: top;" width="15%"><%= resMgr.getText("BankTransStatusUpdate.AmendAmount",TradePortalConstants.TEXT_BUNDLE)%></td>
	  		<td style="font-weight: bold; word-wrap: break-word; vertical-align: top;" width="25%"><%= currency + "  " + displayAmount%></td>
	  		<td style="vertical-align: top;" width="35%" colspan="2">
	  			<%=widgetFactory.createRadioButtonField("IncreaseDecreaseFlag", "Increase", "BankTransStatusUpdate.IncreaseAmount", TradePortalConstants.INCREASE, incrDecrValue.equals(TradePortalConstants.INCREASE), true, "", "")%>
	  			<br>
	  			<%=widgetFactory.createRadioButtonField("IncreaseDecreaseFlag", "Decrease", "BankTransStatusUpdate.DecreseAmount", TradePortalConstants.DECREASE, incrDecrValue.equals(TradePortalConstants.DECREASE), true,"", "")%>
	  		</td>
	  		<td style="vertical-align: top;" width="10%"><%= resMgr.getText("BankTransStatusUpdate.NewAmountTolerance",TradePortalConstants.TEXT_BUNDLE)%></td>
	  		<td style="font-weight: bold; word-wrap: break-word; vertical-align: top;" width="20%"><%=percentValue %></td>
	    </tr>
    </table>