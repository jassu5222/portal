<%--
 
--%>

<%
	String bankTransactionStatusOptions = null;
	bankTransactionStatusOptions = Dropdown.createSortedRefDataOptions(
			TradePortalConstants.BANK_TRANSACTION_STATUS,
			bankTransactionStatus, resMgr.getResourceLocale(), null);
%>

<%=widgetFactory.createSelectField("bankTransactionStatus",
					"BankTransactionUpdate.Label.Status", " ",
					bankTransactionStatusOptions, isReadOnly, true, false,
					" class='char25'", "", "inline")%>

<%=widgetFactory.createTextField("bankInstrumentOID",
					"BankTransactionUpdate.Label.BankInstrumentID",
					bankIntrumentID, "16", isAmend, false, false, "", "",
					"inline")%>
<div style="clear: both"></div>
<%
if(isReadOnly){
	%>
		<input type=hidden name="bankTransactionStatus" value="<%=bankTransactionStatus%>">
	<%
}
if(isAmend){
	%>
		<input type=hidden name="bankInstrumentOID" value="<%=bankIntrumentID%>">
	<%
}
%>
<div class="formItem">
	<%=widgetFactory
					.createNote("BankTransactionUpdate.Note.RejectionReason")%>
</div>
<%
	if (!isReadOnly) {
%>
<%=widgetFactory.createTextArea("bankRejectionReason",
						"", bankRejectionReason, isReadOnly, false, false,
						"maxlength=4000 rows='5' cols='128'", "", "")%>

<%=widgetFactory.createHoverHelp("bankRejectionReason",
						"")%>
<%
	} else {
%>

<%=widgetFactory.createTextArea(
						"bankRejectionReason", "", bankRejectionReason,
						isReadOnly, false, false,
						"maxlength=4000 rows='5' cols='128'", "", "")%>
<%
	}
%>



<div style="clear: both"></div>
