<div id="AttachedDocumentsSection" class ="DocumentsSection" >
<%
     /***********************************
      * Start of Documents Section       *
      ***********************************/


try {
   StringBuffer query = new StringBuffer();
   boolean isEditable = InstrumentServices.isEditableTransStatus(transaction.getAttribute("transaction_status"));

   //cquinton 8/24/2011 Rel 7.1.0 ppx240 add hash
   query.append("select doc_image_oid, image_id, doc_name, hash ");
   query.append("from document_image ");
   query.append("where p_transaction_oid = ?");
   // [BEGIN] IR-YYUH032255414 - jkok
   query.append(" and form_type = ?");
   // [END] IR-YYUH032255414 - jkok
   // [BEGIN] IR-YYUH032637780 - jkok - this assumes that the image_id is related to the
   //                                   order that the documents were attached
   query.append(" and mail_message_oid is null");
   query.append(" order by image_id");
   // [END] IR-YYUH032637780 - jkok

	//jgadela  R90 IR T36000026319 - SQL FIX
   Object[] sqlParamsAttDoc = new Object[2];
   sqlParamsAttDoc[0] =  bankTransUpdateOID;	
   sqlParamsAttDoc[1] =  TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED;	
   Debug.debug("Query is : " + query);
   DocumentHandler dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), false, sqlParamsAttDoc);
  
   Vector listviewVector = null;
   if((dbQuerydoc != null)
      && (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0)) {
      Debug.debug("**** dbQuerydoc --> " + dbQuerydoc.toString());
      listviewVector = dbQuerydoc.getFragments("/ResultSetRecord/");
%>
	
	<div class="formItem inline">
	<div class="attachIcon"><a></a>&nbsp;<%=resMgr.getText("common.InstrumentAttachments", TradePortalConstants.TEXT_BUNDLE)%></div>
      	
   	</div>
   	
		<%--= widgetFactory.createLabel("", "common.Attachments", false, false, false, "inline")--%>
		
<%
	int liTotalDocs = listviewVector.size();
	int liDocCounter = liTotalDocs;
		
	for(int y=0; y < ((liTotalDocs+2)/3); y++) {
		for (int x=0; x < 3 ; x++) {
			if (liDocCounter==0) break;

            DocumentHandler myDoc = (DocumentHandler)listviewVector.elementAt((y*3)+x);
            // cquinton 7/21/2011 Rel 7.1.0 ppx240 - add hash parm
            String imageHash = myDoc.getAttribute("/HASH");
            String encryptedImageHash = "";
            if ( imageHash != null && imageHash.length()>0 ) {
                encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( imageHash, userSession.getSecretKey() );
            } else {
                //send something rather than nothing, which ensures we know we are not allowing
                // attempts to view images when hash parm is just forgotten
                encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( TradePortalConstants.NO_HASH, userSession.getSecretKey() );
            }
    		String urlParam = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) +
    		                  "&hash=" + encryptedImageHash;
    		String encodedURL = response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParam);
    		String userRights = userSession.getSecurityRights();
    		
			
			if ( (SecurityAccess.hasRights(userRights, SecurityAccess.BANK_TRANS_DELETE_DOC)) ) {
%>			
				<%= widgetFactory.createCheckboxField("AttachedDocument","AttachedDocument"+liDocCounter, "",
						EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/DOC_IMAGE_OID"), userSession.getSecretKey()), false, false, false, "", "", "inline") %>
			
<% //Changes done for PortalRefresh IR - T36000004278 - Pavani Mitnala
//Changes are required as the existing logic does not work for the dojo checkboxes.

            }
%>	
       <%= widgetFactory.createLabel("", "<a href='"+encodedURL+"' target='_blank'>"+StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(myDoc.getAttribute("/DOC_NAME")))+"</a>", 
    		   false, false, false, "inline attachedDocLabel")%>
	
	
<% 
		liDocCounter--;
		}
	if (liDocCounter==0) {
		for (int z=liTotalDocs % 3; z > 0; z--) {

		}			
	}
    }
} 
} catch(Exception e) {
   System.err.println("[Transaction-Documents.frag] Caught an exception while trying to get list of documents associated with transaction "
   	+transaction.getAttribute("transaction_oid"));
   e.printStackTrace();
}
%>

</div>
<div style="clear: both;"></div>


<script type="text/javascript">

function setValues(fieldID){
	require(["dijit/registry","dojo/domReady!"],
	        function(registry , dom) {
		if(registry.byId(fieldID).checked){
			var value = registry.byId(fieldID).value;
			alert(value);
			var input = document.createElement("input");
			input.setAttribute("type", "hidden");
			input.setAttribute("name", fieldID);
			input.setAttribute("value", value);
			document.forms(0).appendChild(input);
			
		}
	});
	
}
</script>