
<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridLayout = dgFactory.createGridLayout("invoiceFileUploadGrid", "InvoiceFileUploadDataGrid");
	
	//out.println("selectedWorkflow ="+EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey()));
	//out.println("selectedWorkflow ="+EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey()));
	// DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Starts
	String allow_auto_loan_req = corpOrg.getAttribute("allow_auto_loan_req_inv_upld");
	String allow_auto_atp = corpOrg.getAttribute("allow_auto_atp_inv_only_upld");
	String process_invoice_file_upload = corpOrg.getAttribute("process_invoice_file_upload");
	String atp_invoice_indicator = corpOrg.getAttribute("atp_invoice_indicator");
	// DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Ends
	
	String pay_invoice_utilised_ind = corpOrg.getAttribute("pay_invoice_utilised_ind");
	String pay_invoice_grouping_ind = corpOrg.getAttribute("pay_invoice_grouping_ind");
	String rec_invoice_utilised_ind = corpOrg.getAttribute("rec_invoice_utilised_ind");
%>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;

  <%--set the initial search parms--%>
<% 
  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
  //search - note that the value in the dropdown is encrypted
  //String encryptedOrgOid = EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
%>
 
  var initSearchParms = "userOrgOid=<%=userOrgOid%>&userOid=<%=userOid%>&selectedWorkflow=<%=selectedWorkflow%>";
 console.log ('initParams='+initSearchParms);
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoiceFileUploadDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var InvoiceFileUploadId = 
    createDataGrid("invoiceFileUploadGrid", viewName,
                    gridLayout, initSearchParms,'-2');
  
  require(["dijit/registry", "dojo/on", "dojo/ready"],
      function(registry, on, ready) {
    ready( function(dom){
    	setManagementCheck();  
	});
  });                    
  function searchInvoiceUploads(){
	 
	  require(["dojo/dom"],
		      function(dom){
		 var searchParms = "";
		 var selectField = dijit.byId('Workflow');
		 var val = selectField.attr('value');
		  if (val == '') {
		 	val = selectField.attr('displayedValue');
		 	displayedValue = 'false';
		 	
		 } else {
		 searchParms=searchParms+"&displayedValue=true";
		 }
		 
		  searchParms=searchParms+"&selectedWorkflow="+val;
		
		        searchDataGrid("invoiceFileUploadGrid", "InvoiceFileUploadDataView",
		                       searchParms);
		      });

  }
  
  function deleteSelectedInvoiceFiles(){
		console.log("inside deleteSelectedInvoiceFiles()");
		
		var formName = 'InvoiceUploadsForm',
			buttonName = '<%=TradePortalConstants.BUTTON_DELETE_INVOICE_UPLOAD%>',
			rowKeys = getSelectedGridRowKeys("invoiceFileUploadGrid");
		
			var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getText("CashMgmtFutureValue.PopupMessage", 
            TradePortalConstants.TEXT_BUNDLE)) %>";


		if(rowKeys == ""){
			alert("Please pick record(s) and click 'Delete'");
			return;
		}
		if (!confirm(confirmMessage)) 
		{
			 return;
		}
		console.log("buttonName="+buttonName);
		console.log("rowKeys="+rowKeys);

		submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
	}
	function setInvGroupOption(){
	      require(["dijit/registry", "dojo/dom", "dojo/dom-style"],
    		    function(registry, dom, domStyle) {
    		    	var invItemsGroupOptions = registry.getEnclosingWidget(document.getElementById('InvItemsGroupingOption'));
    		    	if (invItemsGroupOptions){
	    		    	if (invItemsGroupOptions.checked){
	    		    		registry.getEnclosingWidget(document.getElementById('InvItemsGroupingOption')).set('value', 'I');	
	    		    	}
    		    	}
    		    	
				});
				
	}
	var errorFound = <%=errorFound%>;
	var invPayUploadDataOption = "<%=invPayUploadDataOption%>";	
	var invUploadInstrOption = "<%=invUploadInstrOption%>";	
	var invItemsGroupingOption = "<%=invItemsGroupingOption%>";
	var pay_invoice_utilised_ind = "<%=pay_invoice_utilised_ind%>";	
	var pay_invoice_grouping_ind = "<%=pay_invoice_grouping_ind%>";
	var rec_invoice_utilised_ind = "<%=rec_invoice_utilised_ind%>";		
  <%--  DK IR T36000011738 Rel8.2 02/19/2013 Starts --%>
  function setManagementCheck() {
      require(["dijit/registry", "dojo/dom", "dojo/dom-style"],
    		    function(registry, dom, domStyle) {
       		    	    var selectedValue = registry.byId("invoiceDefinitionName").get('displayedValue');       		    		
    		         	if(selectedValue!= null){
    		         	var invType = selectedValue.substring(selectedValue.lastIndexOf("-")+2);
    		         	var payMgt = "<%=StringFunction.asciiToUnicode(resMgr.getText("InvoiceDefinition.PayableMgmt",TradePortalConstants.TEXT_BUNDLE))%>"; 
    		         	var recMgt = "<%=StringFunction.asciiToUnicode(resMgr.getText("InvoiceDefinition.ReceivableMgmt",TradePortalConstants.TEXT_BUNDLE))%>"; 
    		         <%--  DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Starts --%>
    		         	var allow_auto_loan_req = "<%=allow_auto_loan_req%>";
           				var allow_auto_atp = "<%=allow_auto_atp%>";
           				<%--  DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Starts --%>
           				var canProcessInvoiceForATP = "<%=canProcessInvoiceForATP%>";
           				var canProcessLRQInvoice = "<%=canProcessLRQInvoice%>";
           				var atp_invoice_indicator = "<%=atp_invoice_indicator%>";
           				var process_invoice_file_upload = "<%=process_invoice_file_upload%>";  
           				var index = 0;
           				var tempWidgetName = "";
           						  
    		         		if(invType == payMgt){
    		         		 	domStyle.set(dom.byId("manual"), 'display', 'block');
								<%-- Srinivasu_D IR#T36000017499 Rel8.2 05/27/2013 - commented below cond & added new condition --%>
    		         		 <%-- 	if((allow_auto_loan_req == 'Y' || allow_auto_atp == 'Y') && 
    		         		 			(atp_invoice_indicator == 'Y' || process_invoice_file_upload == 'Y') && 
    		         		 			(canProcessInvoiceForATP == 'true' || canProcessLRQInvoice == 'true')){
							  --%>
							 <%--CR 913 start--%>
							 <%-- T36000029824 --%>
							 	if(pay_invoice_grouping_ind == 'Y'){
							 		index++;
							 		tempWidgetName = "TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS";
							 	}
								if(pay_invoice_utilised_ind == 'Y'){
									 domStyle.set(dom.byId("payProg"), 'display', 'block');
									 index++;
									 tempWidgetName = "TradePortalConstants.FOR_PAY_PROG";
									 if(!(canProcessInvoiceForATP == 'true' || canProcessLRQInvoice == 'true')){
										 domStyle.set(dom.byId("manualOnly"), 'display', 'none');
										 index--;
										 tempWidgetName = "TradePortalConstants.FOR_PAY_PROG";
									 }else{
										 index++;
									 }
							 	}								
								<%-- T36000029824 --%>
								if(index==1){
									registry.getEnclosingWidget(document.getElementById(tempWidgetName)).set('checked', true);
								}
								
							 <%--CR 913 end--%>
								 
								if(allow_auto_atp == 'Y' && atp_invoice_indicator == 'Y' && canProcessInvoiceForATP == 'true'){
    		         		 			domStyle.set(dom.byId("group"), 'display', 'block');
    		         		 			registry.getEnclosingWidget(document.getElementById('TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS')).set('checked', false);
										domStyle.set(dom.byId("atp"), 'display', 'block');
										domStyle.set(dom.byId("initems"), 'display', 'block');
    		         		 	}
    		         		 	else{ 
    		         		 		domStyle.set(dom.byId("group"), 'display', 'none');
									domStyle.set(dom.byId("atp"), 'display', 'none'); 	
    		         		 		<%-- registry.getEnclosingWidget(document.getElementById('TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS')).set('checked', true); --%>
    		         		 	}
								if(allow_auto_loan_req == 'Y' && process_invoice_file_upload == 'Y' && canProcessLRQInvoice == 'true'){
    		         		 			domStyle.set(dom.byId("group"), 'display', 'block');
    		         		 			registry.getEnclosingWidget(document.getElementById('TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS')).set('checked', false);
										domStyle.set(dom.byId("lrq"), 'display', 'block'); 
										domStyle.set(dom.byId("initems"), 'display', 'block');
    		         		 	}
    		         		 	else{ 
    		         		 		<%-- domStyle.set(dom.byId("group"), 'display', 'none'); --%>
									domStyle.set(dom.byId("lrq"), 'display', 'none'); 	
    		         		 		<%-- registry.getEnclosingWidget(document.getElementById('TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS')).set('checked', true); --%>
    		         		 	}
								<%-- T36000029824 --%>
								if(index!=1){
    		         		 		registry.getEnclosingWidget(document.getElementById('TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS')).set('checked', false);
								}
    		         		 	registry.getEnclosingWidget(document.getElementById('TradePortalConstants.GROUP_PO_LINE_ITEMS')).set('checked', false);       					
    		         		 	domStyle.set(dom.byId("recProg"), 'display', 'none');
    		         		 	<%-- domStyle.set(dom.byId("atp"), 'display', 'block'); --%>
      		         		  	<%-- domStyle.set(dom.byId("lrq"), 'display', 'block');  --%>
        					<%-- domStyle.set(dom.byId("initems"), 'display', 'block'); --%>
    		         		} else if(invType == recMgt){
    		         			if(rec_invoice_utilised_ind == 'Y'){
    		         				domStyle.set(dom.byId("recProg"), 'display', 'block');
    		         			}
    		         		 	domStyle.set(dom.byId("manual"), 'display', 'block');
    		         		 	domStyle.set(dom.byId("payProg"), 'display', 'none');
    		         		 	if(allow_auto_loan_req == 'Y' && canProcessLRQInvoice == 'true' && process_invoice_file_upload == 'Y'){
    		         		 			domStyle.set(dom.byId("group"), 'display', 'block');
    		         		 	}
    		         		 	else{
    		         		 		domStyle.set(dom.byId("group"), 'display', 'none');
									domStyle.set(dom.byId("lrq"), 'display', 'none'); 									
									domStyle.set(dom.byId("initems"), 'display', 'none');
    		         		 	} 
    		         		 	registry.getEnclosingWidget(document.getElementById('TradePortalConstants.FOR_REC_PROG')).set('checked', false);
    		         		 	<%--  DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Ends --%>
    	    		         	registry.getEnclosingWidget(document.getElementById('TradePortalConstants.GROUP_PO_LINE_ITEMS')).set('checked', false);
    	               			registry.getEnclosingWidget(document.getElementById('TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS')).set('checked', false);
               					domStyle.set(dom.byId("atp"), 'display', 'none');
      		         		  	<%--  jgadela 09/20/2013 Rel 8.3 T36000016416 Issue 11 [START] - once the invoice definition has been selected, then the whole section below needs to be displayed  --%>
      		         		  	domStyle.set(dom.byId("lrq"), 'display', 'block');
        						domStyle.set(dom.byId("initems"), 'display', 'block');
        						<%--  jgadela 09/20/2013 Rel 8.3 T36000016416 Issue 11 [END] --%>

    		         		}
    		         		else{
    		         			domStyle.set(dom.byId("manual"), 'display', 'none');
    		         			domStyle.set(dom.byId("group"), 'display', 'none');
              					domStyle.set(dom.byId("atp"), 'display', 'none');
      		         		  	domStyle.set(dom.byId("lrq"), 'display', 'none');
        					domStyle.set(dom.byId("initems"), 'display', 'none');
        						errorFound = false;
    		         		} 
    		         		if (invItemsGroupingOption == 'I' && errorFound){
    		         			registry.getEnclosingWidget(document.getElementById('InvItemsGroupingOption')).set('checked', true);
    		         			<%-- registry.getEnclosingWidget(document.getElementById('InvItemsGroupingOption')).set('value', 'I'); --%>
    		         		}
    		         		if (invPayUploadDataOption == 'G' && errorFound){
    		         			registry.getEnclosingWidget(document.getElementById('TradePortalConstants.GROUP_PO_LINE_ITEMS')).set('checked', true);
    		         			errorFound = false;
    		         		} else if (invPayUploadDataOption == 'M' && errorFound){
    		         			registry.getEnclosingWidget(document.getElementById('TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS')).set('checked', true);
    		         			errorFound = false;
    		         		}
    		         	<%--  DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Ends --%>
    		         	}
    		  });
  }

  <%-- Sandeep - Rel-8.3 IR# T36000019954 09/19/2013 - Begin --%>
	function checkGroup(){
		require(["dijit/registry", "dojo/dom", "dojo/dom-style"], function(registry, dom, domStyle) {
			
			var radioChecked1 = registry.getEnclosingWidget(document.getElementById('InstrumentType.APPROVAL_TO_PAY')).checked;
			var radioChecked2 = registry.getEnclosingWidget(document.getElementById('InstrumentType.LOAN_RQST')).checked;
			
			if(radioChecked1 == true || radioChecked2 == true){
				registry.getEnclosingWidget(document.getElementById('TradePortalConstants.GROUP_PO_LINE_ITEMS')).set('checked', true);
			}
		});
	}  
	<%-- Sandeep - Rel-8.3 IR# T36000019954 09/19/2013 - End --%>

  function groupCheck() {
      require(["dijit/registry", "dojo/dom", "dojo/dom-style"],
    		  function(registry, dom, domStyle) {
				    
    	  			var loanTypeStore = null; 
				    var recPrg = registry.byId("TradePortalConstants.FOR_REC_PROG"); 
    	  			var group = registry.byId("TradePortalConstants.GROUP_PO_LINE_ITEMS");
    	  			var manual = registry.byId("TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS");    	  			
    	  			var selectedValue = registry.byId("invoiceDefinitionName").get('displayedValue');
       				if(selectedValue!= null){
       				var invType = selectedValue.substring(selectedValue.lastIndexOf("-")+2);
       				var payMgt = "<%=StringFunction.asciiToUnicode(resMgr.getText("InvoiceDefinition.PayableMgmt",TradePortalConstants.TEXT_BUNDLE))%>"; 
       				var recMgt = "<%=StringFunction.asciiToUnicode(resMgr.getText("InvoiceDefinition.ReceivableMgmt",TradePortalConstants.TEXT_BUNDLE))%>";   
       				<%--  DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Starts --%>
       				var atp_invoice_indicator = "<%=atp_invoice_indicator%>";
       				var process_invoice_file_upload = "<%=process_invoice_file_upload%>";
       				var allow_auto_loan_req = "<%=allow_auto_loan_req%>";
       				var allow_auto_atp = "<%=allow_auto_atp%>";
       				var canProcessInvoiceForATP = "<%=canProcessInvoiceForATP%>";
       				var canProcessLRQInvoice = "<%=canProcessLRQInvoice%>";
       					
       				if(group.checked){

       					if(invType == payMgt){	   
	       					loanTypeStore = new dojo.store.Memory({
	       		              <%=payInvLoanType%>
	       		             });
	       					
	       					if ((allow_auto_loan_req == 'Y' && allow_auto_atp == 'Y') 
	       							&& (canProcessInvoiceForATP == 'true' && canProcessLRQInvoice == 'true')
	       							&& (atp_invoice_indicator == 'Y' && process_invoice_file_upload == 'Y')){
	       						domStyle.set(dom.byId("atp"), 'display', 'block');
	    		         		domStyle.set(dom.byId("lrq"), 'display', 'block'); 
	    					domStyle.set(dom.byId("initems"), 'display', 'block');
	       					}
	       					else if(allow_auto_loan_req == 'Y' && canProcessLRQInvoice == 'true' && process_invoice_file_upload == 'Y'){
	       						domStyle.set(dom.byId("lrq"), 'display', 'block');
	    						domStyle.set(dom.byId("initems"), 'display', 'block');
	       					}  
	       					else if(allow_auto_atp == 'Y' && canProcessInvoiceForATP == 'true' && atp_invoice_indicator == 'Y'){
	       						domStyle.set(dom.byId("atp"), 'display', 'block');
	       						domStyle.set(dom.byId("initems"), 'display', 'block');
	       					}	         		    
	         			}else if(invType == recMgt){
       				
	       					loanTypeStore = new dojo.store.Memory({
	         		              <%=recInvLoanType%>
	         		             });
	       					
	       					domStyle.set(dom.byId("atp"), 'display', 'none');
	       					if(allow_auto_loan_req == 'Y' && canProcessLRQInvoice == 'true' && process_invoice_file_upload == 'Y'){
	       						domStyle.set(dom.byId("lrq"), 'display', 'block');
	    						domStyle.set(dom.byId("initems"), 'display', 'block');
	       					}       
       				}
       				
	       				var ltChooser = dijit.byId("loanType");
	       				ltChooser.attr('value','');
	       				ltChooser.store = loanTypeStore;	       				
						
       					<%-- registry.getEnclosingWidget(document.getElementById('TradePortalConstants.APPROVAL_TO_PAY')).set('checked', false); --%>
       					<%-- registry.getEnclosingWidget(document.getElementById('TradePortalConstants.LOAN_RQST')).set('checked', false); --%>
       					if (invItemsGroupingOption != 'I'){
       						registry.getEnclosingWidget(document.getElementById('InvItemsGroupingOption')).set('checked', false);
       					}
       				
       				}else if (manual.checked){
       					
						<%-- jgadela 09/20/2013 Rel 8.3 T36000016416 Issue 11 [START] - once the invoice definition has been selected, then the whole section below needs to be displayed --%>
	       					<%-- domStyle.set(dom.byId("atp"), 'display', 'none'); --%>
			         		<%-- domStyle.set(dom.byId("lrq"), 'display', 'none');   --%>
			         		<%-- domStyle.set(dom.byId("loantype"), 'display', 'none'); --%>
							<%-- domStyle.set(dom.byId("initems"), 'display', 'none'); --%>
						<%--  jgadela 09/20/2013 Rel 8.3 T36000016416 Issue 11 [END]  --%>
       				}
					if(recPrg.checked){
					
						<%--  jgadela 09/20/2013 Rel 8.3 T36000016416 Issue 11 [START] - - once the invoice definition has been selected, then the whole section below needs to be displayed --%>
							<%-- domStyle.set(dom.byId("atp"), 'display', 'none'); --%>
			         		<%-- domStyle.set(dom.byId("lrq"), 'display', 'none');  		         	 --%>
							<%-- domStyle.set(dom.byId("initems"), 'display', 'none'); --%>
						<%--  jgadela 09/20/2013 Rel 8.3 T36000016416 Issue 11 [END]  --%>
						
					}
       			<%--  DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Ends --%>
    	  			}       	
       				
       				<%-- Sandeep - Rel-8.3 IR# T36000019954 09/19/2013 - Begin --%>
       				var radioChecked = registry.getEnclosingWidget(document.getElementById('TradePortalConstants.GROUP_PO_LINE_ITEMS')).checked;
       				if(radioChecked == false){
       					registry.getEnclosingWidget(document.getElementById('InstrumentType.APPROVAL_TO_PAY')).set('checked', false);
       					registry.getEnclosingWidget(document.getElementById('InstrumentType.LOAN_RQST')).set('checked', false);
       					registry.getEnclosingWidget(document.getElementById('loanType')).setValue("");
       				}
       				<%-- Sandeep - Rel-8.3 IR# T36000019954 09/19/2013 - End --%>
    		  });
  }
<%-- DK IR T36000011738 Rel8.2 02/19/2013 Ends --%>

<%-- DK IR T36000015919 Rel8.2 04/15/2013 Starts --%>
  function setLRQ(){
	  require(["dijit/registry", "dojo/dom", "dojo/dom-style"],
  		    function(registry, dom, domStyle) {
     		    	    var loanType = registry.byId("loanType");
     		    	    if(loanType!=''){
     		    	    	registry.getEnclosingWidget(document.getElementById('InstrumentType.LOAN_RQST')).set('checked', true);
     		    	    }
     		    	    else{
     		    	    	registry.getEnclosingWidget(document.getElementById('InstrumentType.LOAN_RQST')).set('checked', false);
     		    	    }
	  });
  }
<%-- DK IR T36000015919 Rel8.2 04/15/2013 Ends  --%>

  require(["dojo/query", "dojo/on", "dojo/domReady!"],
      function(query, on){
    query('#invUploadRefresh').on("click", function() {
      searchInvoiceUploads();
    });
  });  

</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="invoiceFileUploadGrid" />
</jsp:include>
