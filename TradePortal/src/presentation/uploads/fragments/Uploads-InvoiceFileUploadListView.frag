<%--
*******************************************************************************
  Payment File Upload

  Description: List uploaded payments and provide ability to upload here.



*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>

<%
	String invoiceDefnSql = "select INV_UPLOAD_DEFINITION_OID, ind.NAME as NAME from INVOICE_DEFINITIONS ind order by NAME";
	DocumentHandler invoiceDefinitionList = DatabaseQueryBean
			.getXmlResultSet(invoiceDefnSql.toString(), false, new ArrayList());
	String newPath = null;
	
		
	String allow_auto_loan_req = corpOrg.getAttribute("allow_auto_loan_req_inv_upld");
	String allow_auto_atp = corpOrg.getAttribute("allow_auto_atp_inv_only_upld");
	String process_invoice_file_upload = corpOrg.getAttribute("process_invoice_file_upload");
	String atp_invoice_indicator = corpOrg.getAttribute("atp_invoice_indicator");
	String allow_pay_upload_indicator = corpOrg.getAttribute("invoice_file_upload_pay_ind");
	String allow_rec_upload_indicator = corpOrg.getAttribute("invoice_file_upload_rec_ind");
%>
<SCRIPT LANGUAGE="JavaScript">


	var blink_speed=200;
	var i=0;

 function section3Check() {
	 var retValue = false;

      require(["dijit/registry", "dojo/dom", "dojo/dom-style"],
    		    
    		  function(registry, dom, domStyle) {
				    
    	  			var loanTypeStoreValue = null; 
				    var recPrg = registry.byId("TradePortalConstants.FOR_REC_PROG"); 
    	  			var group = registry.byId("TradePortalConstants.GROUP_PO_LINE_ITEMS");
    	  			var manual = registry.byId("TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS");    	  			
    	  			var selectedValue = registry.byId("invoiceDefinitionName").get('displayedValue');
       				
				if(selectedValue!= null){
       				var invTypeValue = selectedValue.substring(selectedValue.lastIndexOf("-")+2);
       				var payMgtValue = "<%=resMgr.getText("InvoiceDefinition.PayableMgmt",TradePortalConstants.TEXT_BUNDLE)%>"; 
       				var recMgtValue = "<%=resMgr.getText("InvoiceDefinition.ReceivableMgmt",TradePortalConstants.TEXT_BUNDLE)%>";   
       			
       				var atp_invoice_indicator = "<%=atp_invoice_indicator%>";
       				var process_invoice_file_upload = "<%=process_invoice_file_upload%>";
       				var allow_auto_loan_req = "<%=allow_auto_loan_req%>";
       				var allow_auto_atp = "<%=allow_auto_atp%>";
       				var canProcessInvoiceForATP = "<%=canProcessInvoiceForATP%>";
       				var canProcessLRQInvoice = "<%=canProcessLRQInvoice%>";
					var atpInst = registry.byId("InstrumentType.APPROVAL_TO_PAY");	
					var lrqInst = registry.byId("InstrumentType.LOAN_RQST");	
					var lineItem = registry.byId("loanType");
					
       				if(!recPrg.checked && !group.checked && !manual.checked){
						alert("The 'Use for the Uploaded Invoices' has not been specified. Please select a value under Step 3 below.");
						retValue = false;
						return false;
					}
       				if(group.checked){
						
						if(!atpInst.checked && !lrqInst.checked){
							alert("Please select an instrument type for data you want to include for uploading the Invoice file.");
							retValue = false;
							return false;
						}
						<%-- 
						if(invTypeValue == payMgtValue){	   
	       					loanTypeStore = new dojo.store.Memory({
	       		              <%=payInvLoanType%>
	       		             });
						}
						else if(invTypeValue == recMgtValue){       				
	       					loanTypeStore = new dojo.store.Memory({
	         		              <%=recInvLoanType%>
	         		             });
						}
						var ltChooser = dijit.byId("loanType");
	       				var lnType = ltChooser.attr("value");
						if(lrqInst.checked){
						
							if(lnType == ""){
								alert("Please select Loan Type..");
								retValue = false;
								return false;
							}
						} 
						 --%>
       				} 			

					retValue = true;	
				}
			
			
				
    		  });

			  return retValue;
  }

function restrictAttach(form,path) {

		var allowUpload = true;

		var file = form.PaymentDataFilePath.value;
		if (!file) {
			<%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
			alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS(
				"InvoiceUploadLogDetail.NoInvoiceFileError",
				TradePortalConstants.TEXT_BUNDLE))%>');
			return false;
		}

		<%-- NAR IR T36000011413  - Alert Message if definition not selected--%>
		var filedef = form.invoiceDefinitionName[0].value;
        var defaultDesc = '<%=resMgr.getText("LocateUploadINV.SelectUploadINVDefinition", TradePortalConstants.TEXT_BUNDLE)%>';
		if (!filedef || filedef == defaultDesc) {
			alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
							"StructuredPOUpload.NoPOFileDefError",
							TradePortalConstants.TEXT_BUNDLE))%>');
			return false;
		}

		if (path){
		var delimiter = "\\";
			delimiter = (file.lastIndexOf("//") != -1)?"//":"\\";
			delimiter = (file.lastIndexOf("/") != -1)?"/":"\\";

		if (file.lastIndexOf(delimiter) != -1){
		 var slashIndex = file.lastIndexOf(delimiter) ;
		 file = file.substring(0,slashIndex);
		 newPath = file;

		<%-- if path is not same as restricted path the show up the error --%>
		if(newPath != path){
			<%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
			alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS(
					"InvoiceMgmtUploads.RestrictFilePathErrorText",
					TradePortalConstants.TEXT_BUNDLE))%>');
		return false;
		}
      }}
		if (allowUpload) {
			   form.action ="<%=response.encodeURL(request.getContextPath()
							+ "/transactions/InvoiceFileUploadServlet.jsp")%>";
			   form.enctype="multipart/form-data";
			   form.encoding="multipart/form-data";
			   document.getElementById('UploadButtonDiv').style.visibility = 'hidden';
			   document.getElementById('UploadButtonDiv').style.display = 'none';
			   return true;
		   }
		   else {
		       <%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
		       alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS(
							"InvoiceUploadLogDetail.ValidFileExtionsionText",
							TradePortalConstants.TEXT_BUNDLE))%>');
		       return false;
		   }
}



</script>


 	      <%
		   //ShilpaR IR MAUM021040190 start
		   // This is the query used for populating the Invoice Upload Definition dropdown list;
		   // it retrieves all Invoice Upload Definitions that belong to the corporate customer
		   // user's org that are used for uploading
		   List<Object> sqlParams = new ArrayList<Object>();
		   StringBuilder sqlQuery1 = new StringBuilder();
		   sqlQuery1.append("select INV_UPLOAD_DEFINITION_OID, NAME,INVOICE_TYPE_INDICATOR");
		   sqlQuery1.append(" from invoice_definitions");
		   sqlQuery1.append(" where a_owner_org_oid in (?");
			sqlParams.add(userSession.getOwnerOrgOid());
		   // Also include invoice definitions from the user's actual organization if using subsidiary access
		   if(userSession.showOrgDataUnderSubAccess())
		    {
		      sqlQuery1.append(",?");
		      sqlParams.add(userSession.getSavedUserSession().getOwnerOrgOid());
		    }

		   sqlQuery1.append(") ");
		   //CR 913 start - Check both indicators
		   boolean bothSelected = false;
		   sqlQuery1.append(" and  INVOICE_TYPE_INDICATOR in (");
		   if("Y".equals(allow_pay_upload_indicator)){
			   sqlQuery1.append("?");
			   bothSelected = true;
			   sqlParams.add("P");
		   }
		   if("Y".equals(allow_rec_upload_indicator)){
			   if(bothSelected){
			   		sqlQuery1.append(",?");
			   	}else{
			   		sqlQuery1.append("?");
			   		}
  		       	sqlParams.add("R");
  		       	
		   }
		   sqlQuery1.append(") ");
		   //CR 913 end
		   sqlQuery1.append(" order by ");
		   sqlQuery1.append(resMgr.localizeOrderBy("name"));

		   DocumentHandler invUploadDefinitionDoc  = DatabaseQueryBean.getXmlResultSet(sqlQuery1.toString(), false, sqlParams);

		   if(invUploadDefinitionDoc == null)
			   invUploadDefinitionDoc = new DocumentHandler();
		//   String invDefinitionOid = invUploadDefinitionDoc.getAttribute("/ResultSetRecord(0)/INV_UPLOAD_DEFINITION_OID");
		   StringBuffer invDefOptions = new StringBuffer();
		   String defaultInv = resMgr.getText("LocateUploadINV.SelectUploadDefinition", TradePortalConstants.TEXT_BUNDLE);
		   if (!errorFound)	
		   	invDefOptions.append("<option selected>"+defaultInv+"</option>");
		   else
		   	invDefOptions.append("<option>"+defaultInv+"</option>");
		  // invDefOptions.append(Dropdown.createSortedOptions(invUploadDefinitionDoc, "INV_UPLOAD_DEFINITION_OID",
              //                                                "NAME", errorFound ? invoiceDefinitionName : " " , userSession.getSecretKey(), resMgr.getResourceLocale()));
		   invDefOptions.append(Dropdown.getSuffixDropdownOptions(invUploadDefinitionDoc, "INV_UPLOAD_DEFINITION_OID",
                   "NAME","INVOICE_TYPE_INDICATOR","INVOICE_FILE_TYPE", errorFound ? invoiceDefinitionName : "" , userSession.getSecretKey(),resMgr.getResourceLocale(),resMgr));


		   String  selectedStatus   = errorFound ? loanType : "";
		   String  loginLocale      = null;
		  // KMehta IR-T36000041657 Rel 9500 on 14-Dec-2015 Add  - Begin
		   String  loginRights    = userSession.getSecurityRights();
		   String  defaultWIPView = userSession.getDefaultWipView();
		   String  currentSecurityType = userSession.getSecurityType();
		   String selectedTranWork = "";
		// KMehta IR-T36000041657 Rel 9500 on 14-Dec-2015 Add  - Begin
 		   Vector recExclusion = new Vector();
		   Vector payExclusion = new Vector();
		   Vector loanTypesNotAllowed = InvoiceUtility.getLoanTypesNotAllowed(corpOrg.getAttribute("organization_oid"));
			recExclusion.addElement(TradePortalConstants.INV_FIN_BR);
			recExclusion.addElement(TradePortalConstants.INV_FIN_BB);
			recExclusion.addAll(loanTypesNotAllowed);
			payExclusion.addElement(TradePortalConstants.EXPORT_FIN);
			payExclusion.addElement(TradePortalConstants.INV_FIN_REC);
		   payExclusion.addAll(loanTypesNotAllowed);

		   loginLocale=userSession.getUserLocale();
		   ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
		   String x = Dropdown.createSortedRefDataOptions("LOAN_TYPE",
                   selectedStatus, loginLocale, new Vector());

			  StringBuffer recInvLoanTypeBuffer = new StringBuffer(Dropdown.createSortedRefDataOptions("LOAN_TYPE",
		                                             selectedStatus, loginLocale, recExclusion));

			  StringBuffer payInvLoanTypeBuffer = new StringBuffer(Dropdown.createSortedRefDataOptions("LOAN_TYPE",
		                                             selectedStatus, loginLocale, payExclusion));

//			  recInvLoanType = "<option value = \"\"> </option>" + recInvLoanTypeBuffer.toString();
//			  payInvLoanType =  "<option value = \"\"> </option>" + payInvLoanTypeBuffer.toString();

			  recInvLoanType = recInvLoanTypeBuffer.toString();
        	  payInvLoanType = payInvLoanTypeBuffer.toString();

			  StringBuilder acs = new StringBuilder();
	          String[] arr = null;

	          if (recInvLoanType.length() > 0) {

 	            	  int count = 0;
	                  arr = recInvLoanType.split("</option>");
	                  acs = new StringBuilder("data: [{name:\"\",id:\"\"},");
	                  for (count = 0; count < arr.length ; count++) {
	                        String str = arr[count];
	                        int start = str.indexOf("=");
	                        int end = str.indexOf(">");
	                        String key = str.substring(start + 2, end - 1);
	                        String val = str.substring(end + 1, str.length());

	                        acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
	                        acs.append(",");
	                  }

	                  acs.deleteCharAt(acs.length() - 1);
	                  acs.append("]");
	                  recInvLoanType = acs.toString();
 	            }

 	            if (payInvLoanType.length() > 0) {
	                  int count = 0;
	                  arr = payInvLoanType.split("</option>");
	                  acs = new StringBuilder("data: [{name:\"\",id:\"\"},");
	                  for (count = 0; count < arr.length; count++) {
	                        String str = arr[count];
	                        int start = str.indexOf("=");
	                        int end = str.indexOf(">");
	                        String key = str.substring(start + 2, end - 1);
	                        String val = str.substring(end + 1, str.length());

	                        acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
	                        acs.append(",");
	                  }

	                  acs.deleteCharAt(acs.length() - 1);
	                  acs.append("]");
	                  payInvLoanType = acs.toString();
 	            }
 		%>


<div>

	<%--Existing Code Comment Ended by dillip--%>
	<%-- Begin by dillip IR #T36000015528--%>
 <%=widgetFactory.createWideSubsectionHeader("LocateUploadInvoiceFile.Step1") %>
 <br>
     	<div class="formItem">
  		   <%= widgetFactory.createFileField("PaymentDataFilePath", "","class='char40'","", false) %>
		</div>

          <%= widgetFactory.createWideSubsectionHeader("LocateUploadInvoiceFile.Step2") %>
          <br>
     <div class="formItem">
 	      <%= widgetFactory.createSelectFieldInline("invoiceDefinitionName", "","", invDefOptions.toString(), false,false,false,"onChange='setManagementCheck();' class='char35em'","","")%>
 	   	  </div>
 	      <%= widgetFactory.createWideSubsectionHeader("LocateUploadInvoiceFile.Step3") %>

    <div class="formItem">
    <div id="manual" style="display:none;">
 	  <%-- DK IR T36000016416 Rel8.2 05/14/2013 starts --%>
 	<%-- %=widgetFactory.createLabel("",
					"LocateUploadInvoiceFile.InvData", false, true, false,
					"","")%> --%>
	<%-- DK IR T36000016416 Rel8.2 05/14/2013 ends --%>
	<div id="recProg" style="display:none;">
  	             <%=widgetFactory.createRadioButtonField("InvPayUploadDataOption","TradePortalConstants.FOR_REC_PROG","",TradePortalConstants.FOR_REC_PROG,false, false,"onChange='groupCheck();'","") %>
    		 		<%=resMgr.getText("LocateUploadInvoiceFile.RecProg",TradePortalConstants.TEXT_BUNDLE)%>
     </div>
     <%--CR 913 start--%>
     <div id="payProg" style="display:none;">
  	             <%=widgetFactory.createRadioButtonField("InvPayUploadDataOption","TradePortalConstants.FOR_PAY_PROG","",TradePortalConstants.FOR_PAY_PROG,false, false,"onChange='groupCheck();'","") %>
    		 		<%=resMgr.getText("LocateUploadInvoiceFile.PayProg",TradePortalConstants.TEXT_BUNDLE)%>
     </div>
     <%--CR 913 end--%>
     <div id="manualOnly" style="display:block;">
         <%=widgetFactory.createRadioButtonField("InvPayUploadDataOption","TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS","",TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS,true, false,"onChange='groupCheck();'","") %>
  	 	 <%=resMgr.getText("LocateUploadInvoiceFile.MakeInvoiceAvailable",TradePortalConstants.TEXT_BUNDLE)%>
  	 </div>
  	  </div>
  	 <div id="group" style="display:none;">
  	            <%=widgetFactory.createRadioButtonField("InvPayUploadDataOption","TradePortalConstants.GROUP_PO_LINE_ITEMS","",TradePortalConstants.GROUP_PO_LINE_ITEMS,false, false,"onChange='groupCheck();'","") %>
      <%=resMgr.getText("LocateUploadInvoiceFile.GroupUploadedPOInvoice",TradePortalConstants.TEXT_BUNDLE)%>
      </div>
      </div>
 <div class=formItemWithIndent4>
 <table><tr><td valign=top> 
 		<span id="atp" style="display:none;" >
         <%=widgetFactory.createRadioButtonField("InvUploadInstrOption","InstrumentType.APPROVAL_TO_PAY","LocateUploadInvoiceFile.ApprovalToPay",InstrumentType.APPROVAL_TO_PAY,false, false,"onChange='checkGroup();'","") %><%--Sandeep - Rel-8.3 IR# T36000019954 09/19/2013--%>
 	</span></td><td>
 		<span id="lrq" style="display:none;">
 		<%=widgetFactory.createRadioButtonField("InvUploadInstrOption","InstrumentType.LOAN_RQST","LocateUploadInvoiceFile.LoanRequest",InstrumentType.LOAN_RQST,false, false,"onChange='checkGroup();'","") %><%--Sandeep - Rel-8.3 IR# T36000019954 09/19/2013--%>
 		<%-- DK IR T36000015919 Rel8.2 04/15/2013 Starts --%>
 		<div class=formItemWithIndent6>
 		 <%= widgetFactory.createSelectFieldInline("loanType", "LocateUploadInvoiceFile.LoanType","", "", false,false,false,"onChange='setLRQ(); checkGroup();'","","")%><%--Sandeep - Rel-8.3 IR# T36000019954 09/19/2013--%>
 		</div>
 		<%-- DK IR T36000015919 Rel8.2 04/15/2013 Ends --%>
 	</span>
 	</td></tr></table>
 	<span id="initems" style="display:none;">
 		 	<%=widgetFactory.createCheckboxField("InvItemsGroupingOption", "", errorFound ? invItemsGroupingOption : TradePortalConstants.USE_ONLY_FILE_PO_LINE_ITEMS, false, false, false, "onChange='setInvGroupOption();'", "", "none" ) %>
 	<%=resMgr.getText("LocateUploadInvoiceFile.IncludeInvoiceInTheFile",TradePortalConstants.TEXT_BUNDLE)%>
 	</span>
</div>

     <div style="clear:both;"></div>

</div>


  <%-- cquinton 10/15/2012 adding Show Counts - use standard search headers as well --%>
  <div class="searchDivider"></div>

  <div class="searchHeader">
    <span class="searchHeaderCriteria">

<%
        StringBuffer prependText = new StringBuffer();
        prependText.append(resMgr.getText("PaymentFileUpload.WorkFor",
                                           TradePortalConstants.TEXT_BUNDLE));
        prependText.append(" ");
	// KMehta IR-T36000041657 Rel 9500 on 14-Dec-2015 Add  - Begin	
		String defaultValue = "";
	    String defaultText  = "";
	    if (selectedWorkflow == null){
		     selectedWorkflow = (String) session.getAttribute("workflow");	
		          }
		 
		    if (selectedWorkflow != null)
		      {
		    
			            if (defaultWIPView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
			            {
			               selectedWorkflow = userOrgOid;
			               
			            }
			            else if (defaultWIPView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
			            {
			                  if(totalOrganizations > 1){
			            	      selectedWorkflow = ALL_WORK;
			                     }else{
			                	selectedWorkflow = userOrgOid;
			            	     }
			            }
			            else
			            {
			                 selectedWorkflow = MY_WORK;
			             }
			        selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
			          
			      }
	      session.setAttribute("workflow", selectedWorkflow);

	      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
	      
	        
	        if(!userSession.hasSavedUserSession())
	         {
	        	           
	           defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
	           defaultText = MY_WORK;
	        
	           dropdownOptions.append("<option value=\"");
		       dropdownOptions.append(defaultValue);
		       dropdownOptions.append("\"");

		       if (selectedWorkflow.equals(defaultText))
		       {
		          dropdownOptions.append(" selected");
		       }

		       dropdownOptions.append(">");
		       dropdownOptions.append(resMgr.getText( defaultText, TradePortalConstants.TEXT_BUNDLE));
		       dropdownOptions.append("</option>");			   
		       	selectedTranWork = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
	         }

	     // KMehta IR-T36000041657 Rel 9500 on 14-Dec-2015 Add  - End
	     
        // If the user has rights to view child organization work, retrieve the
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(loginRights,
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
           dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc,
                                                               "ORGANIZATION_OID", "NAME",
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));
           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
              dropdownOptions.append("<option value=\"");
              dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              dropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 dropdownOptions.append(" selected");
              }

              dropdownOptions.append(">");
              dropdownOptions.append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
              dropdownOptions.append("</option>");
              selectedTranWork = EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey());
           }
        }
        else
        {  dropdownOptions.append("<option value=\"");
        dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
        dropdownOptions.append("\"");

        if (selectedWorkflow.equals(userOrgOid))
        {
           dropdownOptions.append(" selected");
        }

        dropdownOptions.append(">");
        dropdownOptions.append(prependText.toString());
        dropdownOptions.append(userSession.getOrganizationName());
        dropdownOptions.append("</option>");
        selectedTranWork = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
     }
       
        extraTags.append("onchange=\"location='");
        extraTags.append(formMgr.getLinkAsUrl("goToUploadsHome", response));
        extraTags.append("&amp;current2ndNav=");
        extraTags.append(TradePortalConstants.NAV__INVOICE_FILE_UPLOAD);
        extraTags.append("&amp;workflow='+this.options[this.selectedIndex].value\"");
// KMehta IR-T36000041657 Rel 9500 on 14-Dec-2015 Moved code above 
        		  // Kiran  03/14/2013  Rel8.2 IR-T36000014741  End

     out.print(widgetFactory.createSearchSelectField("Workflow","PaymentFileUpload.Show","",dropdownOptions.toString(),
    		 "onChange='searchInvoiceUploads();'"));

%>
    </span>
    <span class="searchHeaderActions">
      <span id="invUploadRefresh" class="searchHeaderRefresh"></span>
      <%=widgetFactory.createHoverHelp("invUploadRefresh", "RefreshImageLinkHoverText") %>
    </span>
    <div style="clear:both;"></div>
  </div>

 <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = dgFactory.createDataGrid("invoiceFileUploadGrid","InvoiceFileUploadDataGrid",null);
%>
<%=gridHtml%>

