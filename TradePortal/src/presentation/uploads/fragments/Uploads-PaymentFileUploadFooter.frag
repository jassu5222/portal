
<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridLayout = dgFactory.createGridLayout("paymentFileUploadGrid", "PaymentFileUploadDataGrid");
	
	//out.println("selectedWorkflow ="+EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey()));
	//out.println("selectedWorkflow ="+EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey()));
%>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;

  <%--set the initial search parms--%>
<% 
  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
  //search - note that the value in the dropdown is encrypted
  //String encryptedOrgOid = EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
%>
 
  var initSearchParms = "userOrgOid=<%=userOrgOid%>&userOid=<%=userOid%>&selectedWorkflow=<%=selectedWorkflow%>";
 console.log ('initParams='+initSearchParms);
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentFileUploadDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var PaymentFileUploadId = 
    createDataGrid("paymentFileUploadGrid", viewName,
                    gridLayout, initSearchParms,'-2');
  function searchPaymentUploads(){
	 
	  require(["dojo/dom"],
		      function(dom){
		 var searchParms = "userOrgOid=<%=userOrgOid%>&userOid=<%=userOid%>";
		 var selectField = dijit.byId('Workflow');
		 var val = selectField.attr('value');
		  if (val == '') {
		 	val = selectField.attr('displayedValue');
		 	displayedValue = 'false';
		 	
		 } else {
		 searchParms=searchParms+"&displayedValue=true";
		 }
		 
		  searchParms=searchParms+"&selectedWorkflow="+val;
		
		        searchDataGrid("paymentFileUploadGrid", "PaymentFileUploadDataView",
		                       searchParms);
		      });

  }
  
  require(["dojo/query", "dojo/on", "dojo/domReady!"],
      function(query, on){
    query('#pmtUploadRefresh').on("click", function() {
      searchPaymentUploads();
    });
  });
  
  
  function deletePaymentTransactions() {
  
	    var formName = 'PaymentFileUploadForm';    
	    var buttonName = '<%=TradePortalConstants.BUTTON_DELETE_REJECTED__ITEMS%>';

	    
     <%--- SureshL  5/23/2014 Rel8.4  IR-T36000027756 -start  ---%>   
		    var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getText("FutureValueTransactions.PopupMessage",
		    TradePortalConstants.TEXT_BUNDLE)) %>";
		     
		   
		 <%--get array of rowkeys from the grid--%>
	    var rowKeys = getSelectedGridRowKeys("paymentFileUploadGrid");
		    if(rowKeys == '' || rowKeys == "")
		    	return false;
		    	
		    if (!confirm(confirmMessage)) 
			{
				return false;
			}else{
				<%--submit the form with rowkeys
		        becuase rowKeys is an array, the parameter name for
		        each will be checkbox + an iteration number -
		        i.e. checkbox0, checkbox1, checkbox2, etc --%>
		    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
			}
	  <%--- SureshL  5/23/2014 Rel8.4  IR-T36000027756 -end  ---%> 

  }
  
  
  
</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="paymentFileUploadGrid" />
</jsp:include>
