<%--
*******************************************************************************
  Purchase Orders Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%
	DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	String gridLayout = dgFactory.createGridLayout("structurePurchaseOrdersDatagridId", "StructurePurchaseOrdersDatagrid");
%>

<script type="text/javascript">
  	var gridLayout = <%= gridLayout %>,
  		initSearchParms = "userOrgOid=<%=userOrgOid%>";
  		 var savedSort = savedSearchQueryData["sort"];  
  		require(["dijit/registry", "dojo/dom", 
  	           "dojo/ready"],
  	      function(registry, dom, ready ) {
  	    ready(function() {
   			var poNumber = savedSearchQueryData["poNumber"];   
   			beneName = savedSearchQueryData["beneName"];  
     		 if("" == poNumber || null == poNumber || "undefined" == poNumber)
     			 poNumber=(dom.byId("PONumber").value).toUpperCase();
     		 else
     			registry.byId("PONumber").set('value',poNumber);
     	 	
     	 	 if("" == beneName || null == beneName || "undefined" == beneName)
     	 		beneName=(dom.byId("BeneName").value).toUpperCase();
     		 else
     			registry.byId("BeneName").set('value',beneName);
     	 	initSearchParms = initSearchParms+"&poNumber="+poNumber+"&beneName="+beneName;
     	 	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("StructurePurchaseOrdersDataview",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
     	 	createDataGrid("structurePurchaseOrdersDatagridId", viewName,gridLayout, initSearchParms,savedSort);
   		});
  		});
		
  	
	
  	function filterStructPOsOnEnter(fieldId){
		<%-- 
	  	* The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
	  	 --%>
	  	require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldId), "keypress", function(event) {
		        if (event && event.keyCode == 13) {
		        	dojo.stopEvent(event);
		        	filterPOs();
		        }
		    });
		});
	}
  	
  	function filterPOs() {
	    require(["dojo/dom","dijit/registry"],
	    function(dom,registry){
	        var poNumber = registry.byId("PONumber").get('value'),
	        	beneName = registry.byId("BeneName").get('value'),
	        	searchParms = '';

	        searchParms = "userOrgOid=<%=userOrgOid%>&poNumber="+poNumber+"&beneName="+beneName;
			searchDataGrid("structurePurchaseOrdersDatagridId", "StructurePurchaseOrdersDataview", searchParms);
		});
  	}
  	
  	function deleteSelectedPurchaseOrder(){
		var formName = 'PurchaseOrderStructuredListForm',
			buttonName = '<%=TradePortalConstants.BUTTON_DELETE_PO_STRUCTURED%>',
			rowKeys = getSelectedGridRowKeys("structurePurchaseOrdersDatagridId");
		
		if(rowKeys == ""){
			alert("Please pick record(s) and click 'Delete'");
			return;
		}
		
		var confirmDelete = confirm("<%=resMgr.getText("PendingTransactions.PopupMessage",TradePortalConstants.TEXT_BUNDLE)%>");
		
		if(confirmDelete == true){
			submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
		}
  	}
  	
  	function createATPsUsingRuleText(){
  		var formName = 'PurchaseOrderStructuredListForm',
		buttonName = '<%=TradePortalConstants.BUTTON_CREATE_ATP_FROM_PO_STRUCTURED%>',
		rowKeys = getSelectedGridRowKeys("structurePurchaseOrdersDatagridId");
	
		submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
  	}
  	
  	function createLCsUsingRuleText(){
  		var formName = 'PurchaseOrderStructuredListForm',
		buttonName = '<%=TradePortalConstants.BUTTON_CREATE_LC_FROM_PO_STRUCTURED%>',
		rowKeys = getSelectedGridRowKeys("structurePurchaseOrdersDatagridId");
	
		submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
  	}

  	function openURL(URL){
    if (isActive =='Y') {
    	if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
	    }
    }
 		document.location.href  = URL;
 		return false;
	}
	require(["dojo/query", "dojo/on", "dojo/domReady!"],
  	      function(query, on){
  	    query('#RefreshPOPageButton').on("click", function() {
  	    	filterPOs();
  	    });
  	}); 
</script>

<jsp:include page="/common/gridShowCountFooter.jsp">
<jsp:param name="gridId" value="structurePurchaseOrdersDatagridId" />
</jsp:include>