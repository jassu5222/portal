This document provides a high level breakdown of the file shipped with viewONE.
For additional information on HTML and Javascript options please see html.pdf and annot.pdf

(files marked * are essential, the others are optional depending on required features)


Versioning and licensing files
------------------------------

build.v1 * 
ver.v1 * 
lic.v1 * (may be replaced with alternate license supplied by Daeja or Daeja agent)


Core viewONE Standard applet classes
------------------------------------

ji.cab * (for Microsoft Java support)
ji.jar * (for Sun JRE, Netscape 4 and Unix support)
jis.jar * (for Apple Macintosh support)


Core viewONE Pro applet classes
-------------------------------

viewone.cab * (for Microsoft Java support)
viewone.jar * (for Sun JRE, Netscape 4 and Unix support)
viewonedsa.jar (for Apple Macintosh support)

ji.cab * (for Microsoft Java support)
ji.jar * (for Sun JRE, Netscape 4 and Unix support)
jis.jar * (for Apple Macintosh support)

ji.v1 * (for all JRE/JVM's except Apple Macintosh) - since build 526 
jis.v1 *  (for Apple Macintosh) - since build 526 


Optional PNG file format support (Standard and Pro)
---------------------------------------------------

daeja1.cab
daeja1.jar
daeja1s.jar


Clipboard support
-----------------

jiex1.v1 *
jiex1c.v1 *


Font resource files (Annotations and text file viewing)
-------------------------------------------------------

ftcrrb12.v1 *
ftcrrb24.v1 *
ftcrrb48.v1 *
ftcrrb84.v1 *
ftlist.v1 *
ftrlb12.v1 *
ftrlb24.v1 *
ftrlb48.v1 *
ftrlb84.v1 *
ftsnssrfp84.v1 *
ftsrfp84.v1 *
ftrlp84.v1 *
ftcrrp84.v1 *

Generic Language files 
-----------------------

lang.v1 *
locals.txt *


Specific Language files
-----------------------

ar.v1 * Arabic
bg.v1 * Bulgarian
ca.v1 * Catalan
cs.v1 * Czech
da.v1 * Danish
de.v1 * German
en.v1 * English
es.v1 * Spanish
fi.v1 * Finnish
fr.v1 * French
hr.v1 * Croatian
hu.v1 * Hungarian
it.v1 * Italian
iw.v1 * Hebrew
ja.v1 * Japanese
ko.v1 * Korean
nl.v1 * Dutch
no.v1 * Norwegian
pl.v1 * Polish
pt.v1 * Portuguese
pt_br.v1 * Brazilian Portuguese
ro.v1 * Romanian
ru.v1 * Russian
sk.v1 * Slovak
sv.v1 * Swedish
th.v1 * Thai
tr.v1 * Turkish
zh_cn.v1 * Chinese (Simplified)
zt_tw.v1 * Chinese (Traditional)


General user-interface`graphics 
-------------------------------

res1.v1 *
res2.v1 *
res3.v1 *
res4.v1 *
trial.v1 *
vl1.v1 *
vlpro.v1 *
fn*.v1
vv*.v1 *
vn*.v1 *


General File-Open error messages (may be changed to suit your environment)
--------------------------------------------------------------------------

notfound.txt *
openerror.txt *
notfound.tif *


Pointer to external Java classes when extending viewONE's image format capability
---------------------------------------------------------------------------------

external.txt * 


General HTML files used to report update/installation issues  (viewONE Pro)
---------------------------------------------------------------------------

acm/<various>.htm *


Optional Print-Accelerator
--------------------------

jip86.v1
jip86l.v1


Optional LizardTech DjVu file format support (viewONE Pro)
----------------------------------------------------------

jiFDJV.v1
jiFDJVl.v1
JNIDjVu.v1


Optional PDF file format support (viewONE version Pro)
----------------------------------------------------

daeja2.cab
daeja2.jar
jpd.v1
jpd-macosx-ppc.v1


Optional JPEG2000 file format support (viewONE version Pro)
----------------------------------------------------

daeja3.cab
daeja3.jar


Optional Universal Viewing module (viewONE version Pro)
-------------------------------------------------------

jiuv.v1
stel.v1
stelfi.v1

Sample parameter files (see html.pdf for explanation) 
-----------------------------------------------------

params1.txt
params2.txt


Demo files (for use with standard demo package) - can be removed
----------------------------------------------------------------

popup.htm
v1.bmp
v1.gif
v1t.bmp
web.htm


Demo files (for custom buttons that you may choose to create, see html.pdf) - can be removed
--------------------------------------------------------------------------------------------

button.jpg 
button1.jpg
button2.jpg


Quickstart files (to permit QuickStart utility to be used)
----------------------------------------------------------

quickstart.htm
jiqs.v1
jiqsc.v1