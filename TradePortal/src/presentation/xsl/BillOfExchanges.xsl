<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:import href="./templates/DefaultFonts.xsl"/><!-- BSL Cocoon Upgrade 10/12/11 Rel 7.0 ADD -->

  <xsl:template match="BillOfExchanges">
    <!--<xsl:processing-instruction name="cocoon-format">type="text/xslfo"</xsl:processing-instruction>--><!-- BSL Cocoon Upgrade 10/12/11 Rel 7.0 DELETE -->
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
      <fo:layout-master-set>
       <fo:simple-page-master master-name="normal">
         <fo:region-body margin-bottom="50pt" margin-top="50pt" margin-left="50pt" margin-right="50pt"/>
       </fo:simple-page-master>
      </fo:layout-master-set>

      <fo:page-sequence master-reference="normal"><!-- BSL Cocoon Upgrade 10/12/11 Rel 7.0 - change master-name to master-reference and use wrapper to specify Unicode font -->
        <fo:flow flow-name="xsl-region-body">
          <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
            <xsl:apply-templates/>
          </xsl:element>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>


  <xsl:template match="BillOfExchanges/BillOfExchange">
    <xsl:if test="not(position() = last())">
    <fo:block break-after="page">
       <xsl:apply-templates/>
    </fo:block>
    </xsl:if>
    <xsl:if test="position() = last()">
       <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionA">
    <fo:table>
     <fo:table-column column-width="250pt"/>
     <fo:table-column column-width="250pt"/>   
      <fo:table-body>
       <fo:table-row>
        <fo:table-cell>
         <fo:block font-size="10pt" font-weight="bold">Amount In Figures</fo:block>
        </fo:table-cell>
        <fo:table-cell>
         <fo:block font-size="10pt" font-weight="bold">Date</fo:block>
        </fo:table-cell>
       </fo:table-row>
       <fo:table-row>
        <xsl:apply-templates/>
       </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionA/Amount">
    <fo:table-cell>
     <fo:block font-size="10pt"><xsl:apply-templates/></fo:block>
    </fo:table-cell>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionA/Date">
    <fo:table-cell>
     <fo:block font-size="10pt"><xsl:apply-templates/></fo:block>
    </fo:table-cell>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionB">

    <xsl:variable name="showAt">
        <xsl:value-of select="showAt" />
    </xsl:variable>

    <xsl:if test="$showAt = 'false'">
       <fo:block font-size="10pt" font-weight="bold" space-before.optimum="15pt"><xsl:value-of select="text" /> of this sole bill of exchange</fo:block>
    </xsl:if>

    <xsl:if test="$showAt != 'false'">
       <fo:block font-size="10pt" font-weight="bold" space-before.optimum="15pt">At <xsl:value-of select="text" /> of this sole bill of exchange</fo:block>
    </xsl:if>

     <fo:block font-size="10pt" font-weight="bold">Pay to the order of</fo:block>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionC">
   <fo:block font-size="12pt" font-weight="bold"><xsl:apply-templates/></fo:block>   
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionD">
    <fo:block space-before.optimum="10pt"></fo:block>
    <fo:table>
     <fo:table-column column-width="250pt"/>
     <fo:table-column column-width="250pt"/>   
      <fo:table-body>
       <!-- BSL Cocoon Upgrade 10/13/11 Rel 7.0 DELETE - duplicate table row with invalid extra table-cell -->
       <!--<fo:table-row>
       <fo:table-cell></fo:table-cell>
        <xsl:apply-templates/>
       </fo:table-row>-->
         <fo:table-row>
           <xsl:apply-templates/>
       </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionD/Empty">
     <fo:table-cell>
      <fo:block font-size="10pt"></fo:block>
     </fo:table-cell>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionD/DraftNumber">
     <fo:table-cell>
      <fo:block font-size="10pt" font-weight="bold">Draft Number</fo:block>
      <fo:block font-size="10pt"><xsl:apply-templates/></fo:block>
     </fo:table-cell>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionD/CollectionReference">
     <fo:table-cell>
      <fo:block font-size="12pt" font-weight="bold">COLLECTION REFERENCE</fo:block>
      <fo:block font-size="12pt"><xsl:apply-templates/></fo:block>
     </fo:table-cell>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionE">
     <fo:block font-size="10pt" font-weight="bold" space-before.optimum="10pt">The sum of <xsl:apply-templates/></fo:block>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionF">
    <fo:block space-before.optimum="10pt"></fo:block>
    <fo:table>
     <fo:table-column column-width="20pt"/>
     <fo:table-column column-width="480pt"/>   
      <fo:table-body>
       <xsl:apply-templates/>
      </fo:table-body>
    </fo:table>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionF/FirstAddressLine">
    <fo:table-row>
     <fo:table-cell>
      <fo:block font-size="10pt" font-weight="bold">To</fo:block>
     </fo:table-cell>     
     <fo:table-cell>
      <fo:block font-size="10pt"><xsl:apply-templates/></fo:block>
     </fo:table-cell>
    </fo:table-row>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionF/AddressLine">
    <fo:table-row>
     <fo:table-cell>
      <fo:block></fo:block>
     </fo:table-cell>     
     <fo:table-cell>
      <fo:block font-size="10pt"><xsl:apply-templates/></fo:block>
     </fo:table-cell>
    </fo:table-row>
  </xsl:template>

  <xsl:template match="BillOfExchanges/BillOfExchange/SectionG">
    <fo:table>
     <fo:table-column column-width="250pt"/>
     <fo:table-column column-width="250pt"/>   
      <fo:table-body>
       <fo:table-row>
        <fo:table-cell>
        </fo:table-cell>
        <fo:table-cell>
         <fo:block font-size="10pt"><xsl:apply-templates/></fo:block>
         <fo:block font-size="10pt" space-before.optimum="10pt">............................................</fo:block>
        </fo:table-cell>
       </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>


<!--POUK062934504 08/24/10 Begin-->
 <!-- VSHAH Vasavi Rework Begin-->
 
  <xsl:template match="BillOfExchanges/BillOfExchange/SectionH">
	    <fo:block font-size="10pt"  space-before.optimum="10pt"></fo:block>
		<fo:block font-size="10pt"  text-align="center" space-before.optimum="10pt">Pay to the order of</fo:block>
   	    <fo:block font-size="10pt"  text-align="center" space-before.optimum="10pt">BANK or BANKER</fo:block>
   	    <fo:block font-size="10pt"  text-align="center" space-before.optimum="10pt">Value For Collection</fo:block>
   	    <fo:block font-size="10pt"  space-before.optimum="10pt"></fo:block>
   	    <fo:block font-size="10pt"  text-align="center" space-before.optimum="10pt">Without Recourse</fo:block>
   	    <fo:block font-size="12pt"  text-align="center" font-weight="bold"><xsl:apply-templates/></fo:block>  
  </xsl:template>
 <!-- VSHAH Vasavi Rework End -->
  <!--POUK062934504 08/24/10 End-->


</xsl:stylesheet>