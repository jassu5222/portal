<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:import href="./templates/DefaultFonts.xsl"/>
   <xsl:import href="./templates/FOCommon.xsl"/>
   <xsl:import href="./templates/invoicemanagement/SectionA-CreditNoteDetails.xsl"/>
   <xsl:import href="./templates/invoicemanagement/SectionB1-CreditNoteDetails.xsl"/>
   <xsl:import href="./templates/invoicemanagement/SectionB2-CreditNoteDetails.xsl"/>
   <xsl:import href="./templates/invoicemanagement/SectionC-CreditNoteDetails.xsl"/>
   <xsl:import href="./templates/invoicemanagement/SectionD-CreditNoteDetails.xsl"/>
   <xsl:import href="./templates/invoicemanagement/SectionE-CreditNoteDetails.xsl"/>
   <xsl:template match="Document">
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
            <fo:simple-page-master master-name="normal" page-width="297mm" page-height="210mm">
               <fo:region-body margin-bottom="75pt" margin-top="75pt" margin-left="35pt" margin-right="35pt"/>
               <fo:region-before extent="75pt" padding="35pt"/>
               <fo:region-after extent="75pt"/>
            </fo:simple-page-master>
         </fo:layout-master-set>
         
         <fo:page-sequence master-reference="normal">
            <fo:static-content flow-name="xsl-region-before">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <xsl:apply-templates select="./SectionA"/>
               </xsl:element>
            </fo:static-content>
            <fo:static-content flow-name="xsl-region-after">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <fo:block text-align="center">Page: <fo:page-number/></fo:block>
               </xsl:element>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <xsl:apply-templates select="./SectionB1"/>
                  <xsl:apply-templates select="./SectionB2"/>
                  <xsl:apply-templates select="./SectionC"/>
                  <xsl:apply-templates select="./SectionD"/>  
                  <xsl:apply-templates select="./SectionE"/>
               </xsl:element>
            </fo:flow>
         </fo:page-sequence>
         
      </fo:root>
   </xsl:template>
   
   <xsl:template match="section-header" >
      <xsl:param name="start-indent"></xsl:param>
      <fo:block  background-color="#E7E7E7" height="4mm" text-align="left">
         <fo:inline font-weight="bold"><xsl:value-of select="name"/></fo:inline>
         <fo:leader leader-length="20mm" leader-pattern="space"/>
         <fo:inline><xsl:value-of select="value"/></fo:inline>
      </fo:block>   
      <fo:block>
         <fo:leader leader-length="272mm" leader-pattern="rule" rule-thickness="1pt" color="black"/>
      </fo:block>
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
   </xsl:template>  
   
   <xsl:template match="name" mode="block">
      <fo:block>
         <fo:inline font-weight="bold"><xsl:value-of select="."/> </fo:inline>
      </fo:block>   
   </xsl:template>  
   
   <xsl:template match="value" mode="block">
      <fo:block>
         <fo:inline><xsl:value-of select="."/></fo:inline>
      </fo:block>   
   </xsl:template>  
   
   <xsl:template match="name" mode="inline">
      <fo:inline font-weight="bold"><xsl:value-of select="."/> </fo:inline>
   </xsl:template>  
   
   <xsl:template match="value" mode="inline">
      <fo:inline><xsl:value-of select="."/></fo:inline>
   </xsl:template>  

   <xsl:template match="value" mode="split">
    <!-- BSL IR RNUM050251590 06/07/2012 Rel 8.0 BEGIN -->
         <xsl:choose>
            <xsl:when test="string-length(.) &gt; 40">
               <xsl:call-template name="splitString">
                  <xsl:with-param name="value" select="."/>
                  <xsl:with-param name="startPosition" select="1"/>
                  <xsl:with-param name="endPosition" select="40"/>
               </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
               <fo:block>
               <fo:inline><xsl:value-of select="." /> </fo:inline>
                  </fo:block>
            </xsl:otherwise>
         </xsl:choose>
<!--      <xsl:param name="maxWordLength" select="25"/>
      <fo:block>
         <xsl:call-template name="splitString">
            <xsl:with-param name="value" select="."/>
            <xsl:with-param name="maxWordLength" select="$maxWordLength"/>
         </xsl:call-template>
      </fo:block>-->
   <!-- BSL IR RNUM050251590 06/07/2012 Rel 8.0 END -->
   </xsl:template>  
   
   <!-- NAR IR T36000015701 04/10/2013 Rel 8.2 END -->
   <xsl:template match="value" mode="invSummSplit">
         <xsl:choose>
            <xsl:when test="string-length(.) &gt; 17">
               <xsl:call-template name="splitString">
                  <xsl:with-param name="value" select="."/>
                  <xsl:with-param name="startPosition" select="1"/>
                  <xsl:with-param name="endPosition" select="17"/>
               </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
               <fo:block>
                 <fo:inline><xsl:value-of select="." /> </fo:inline>
               </fo:block>
            </xsl:otherwise>
         </xsl:choose>
   </xsl:template> 
   
<!-- BSL IR RNUM050251590 06/07/2012 Rel 8.0 - rewrote and moved splitString to FOCommon.xsl -->
   <xsl:template name="splitString">
      <xsl:param name="value"/>
      <xsl:param name="startPosition"/>
      <xsl:param name="endPosition"/>
      <xsl:choose>
         <xsl:when test="string-length($value) &gt; $endPosition">
            <fo:block>
               <fo:inline><xsl:value-of select="substring($value,$startPosition,$endPosition)" /> </fo:inline>
               <fo:leader leader-pattern="space"/>
            </fo:block>
            
            <xsl:call-template name="splitString">
               <xsl:with-param name="value" select="substring($value,number($endPosition)+1,string-length($value))"/>
               <xsl:with-param name="startPosition" select="$startPosition"/>
               <xsl:with-param name="endPosition" select="$endPosition"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <fo:block>
               <fo:inline><xsl:value-of select="$value" /> </fo:inline>
            </fo:block>   
         </xsl:otherwise>
      </xsl:choose>
      
   </xsl:template>
   
</xsl:stylesheet>
