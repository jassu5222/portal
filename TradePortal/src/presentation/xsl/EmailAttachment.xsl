<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">	
	<xsl:import href="./templates/DefaultFonts.xsl"/><!-- BSL Cocoon Upgrade 10/12/11 Rel 7.0 ADD -->
	<xsl:import href="./templates/po/GroupedInvoiceDetails.xsl"/>
	<xsl:output method="xml" version="1.0" indent="yes" encoding="UTF-8"/>
	<xsl:template match="EmailAttachment">
		<!-- BSL Cocoon Upgrade 10/06/11 Rel 7.0 DELETE -->
		<!--<xsl:processing-instruction name="cocoon-format">
			type="text/xslfo"
			</xsl:processing-instruction>-->
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="normal">
					<fo:region-body margin-bottom="50pt" margin-top="50pt"
						margin-left="50pt" margin-right="50pt" />
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="normal"><!-- BSL Cocoon Upgrade 10/12/11 Rel 7.0 - change master-name to master-reference and use wrapper to specify Unicode font -->
				<fo:flow flow-name="xsl-region-body">
					<xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
						<xsl:apply-templates/>
					</xsl:element>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	<xsl:template match="EmailAttachment/SectionA">
		<fo:block start-indent="0.1in">
			<xsl:apply-templates select="Logo" />
		</fo:block>
		<!-- BSL IR POUL032966481 BEGIN -->
		<!-- <fo:block font-size="12pt" start-indent="4in"> -->
		<fo:block font-size="12pt" start-indent="2.2in">
			<!-- BSL IR POUL032966481 END -->
			<xsl:apply-templates select="EmailMessage" />
		</fo:block>
		<fo:block font-size="8pt" space-before="0.5in" space-after="0.25in"
			start-indent="0.1in" end-indent="1.5in">
			<xsl:apply-templates select="EmailText" />
		</fo:block>
		<!-- BSL Cocoon Upgrade 10/11/11 Rel 7.0 - Adjust space-after due to changes in SectionB -->
		<fo:block font-size="8pt" space-before="0.05in" space-after="0.1in"
			start-indent="0.075in" linefeed-treatment="preserve"
			white-space-collapse="false" wrap-option="wrap"
			white-space-treatment="preserve">
			<fo:inline>
				<xsl:apply-templates select="BankGroupLabel" />
			</fo:inline>
			<fo:inline>
				<xsl:text>&#9;</xsl:text>
			</fo:inline>
			<fo:inline>
				<xsl:apply-templates select="BankGroupValue" />
			</fo:inline>
		</fo:block>
	</xsl:template>
	<xsl:template match="EmailAttachment/SectionA/Logo">
		<!-- BSL Cocoon Upgrade 11/17/11 Rel 7.0 - In external-graphic, replace height with content-height and remove width to preserve aspect ratio -->
		<!--<fo:external-graphic height = "0.7in" width = "1.4in">-->
		<fo:external-graphic content-height="0.7in">
			<xsl:attribute name="src"><xsl:value-of select="." />logo-new.gif</xsl:attribute>
		</fo:external-graphic>
	</xsl:template>
	<xsl:template match="EmailAttachment/SectionB">
		<!-- CJR IR HNUL070449897 BEGIN - Replaced top of Section B to be a table to allow values to align and wrap properly. -->
		<!--<fo:table-and-caption>--><!-- BSL Cocoon Upgrade 10/07/11 Rel 7.0 DELETE - table-and-caption not supported in FOP 1.0 -->
		
		<fo:table font-size="8pt">
			
			<fo:table-column column-width="1.5in"/>
			<fo:table-column column-width="4in"/>		
			
			<!-- BSL Cocoon Upgrade 10/11/11 Rel 7.0 - Replace padding-* attributes with height on table-rows -->
			<fo:table-body>
				<fo:table-row height="16pt">
					<fo:table-cell padding-left=".10in">
						<fo:block><xsl:apply-templates select="SubjectLabel" /></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="SubjectValue" /></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row height="16pt">
					<fo:table-cell padding-left=".10in">
						<fo:block><xsl:apply-templates select="PartyNameLabel" /></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="PartyNameValue" /></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row height="16pt">
					<fo:table-cell padding-left=".10in">
						<fo:block><xsl:apply-templates select="WeHavePaidLabel" /></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="WeHavePaidValue" /></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row height="16pt">
					<fo:table-cell padding-left=".10in">
						<fo:block><xsl:apply-templates select="PaymentMethodLabel" /></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="PaymentMethodValue" /></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row height="16pt">
					<fo:table-cell padding-left=".10in">
						<fo:block><xsl:apply-templates select="OnAccountLabel" /></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="OnAccountValue" /></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left=".10in">
						<fo:block><xsl:apply-templates select="AtLabel" /></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="BankCodeValue" /></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row height="20pt">
					<fo:table-cell padding-left=".10in">
						<fo:block> </fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="BankNameValue" /></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
			
			
		</fo:table>
		
		
		<fo:table font-size="8pt">
			
			<fo:table-column column-width="1.5in"/>
			<fo:table-column column-width="2in"/>
			<fo:table-column column-width="1.5in"/>
			<fo:table-column column-width="2in"/>
			
			<fo:table-body>
				<fo:table-row height="16pt">
					<fo:table-cell padding-left=".10in">
						<fo:block><xsl:apply-templates select="ValueDateLabel"/></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="ValueDateValue"/></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="AmountLabel"/></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="AmountValue"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row height="12pt">
					<fo:table-cell padding-left=".10in">
						<fo:block><xsl:apply-templates select="OurRefLabel"/></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="OurRefValue"/></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="PaymentCurrencyLabel"/></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block><xsl:apply-templates select="PaymentCurrencyValue"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- BSL Cocoon Upgrade 10/10/11 Rel 7.0 END -->
	
	<xsl:template match="BankCodeValue">
		<xsl:apply-templates />
		<xsl:if test="BranchCodeValue!=''">
			<fo:inline font-size="8pt">/</fo:inline>
		</xsl:if>
		<fo:inline font-size="8pt">
			<xsl:apply-templates select="BranchCodeValue" />
		</fo:inline>
	</xsl:template>
	
	<xsl:template match="EmailAttachment/SectionC">
		<fo:block font-size="8pt" space-before="0.1in" space-after="0.01in"
			start-indent="0.1in" end-indent="1.5in" xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<xsl:apply-templates select="PaymentDetailsLabel" />
		</fo:block>
		<!-- BSL IR POUL032966481 BEGIN -->
		<!-- <fo:block font-size="8pt" space-before="0.01in" space-after="0.1in"
			start-indent="0.1in" end-indent="1.5in"> -->
		<fo:block font-size="8pt" space-before="0.01in" space-after="0.1in"
			start-indent="0.1in" end-indent="1.5in"
			linefeed-treatment="preserve" white-space-collapse="false"
			wrap-option="wrap" white-space-treatment="preserve">
			<!-- BSL IR POUL032966481 END -->
			<xsl:apply-templates select="PaymentDetailsValue" />
		</fo:block>
		<fo:block font-size="8pt" space-before="0.1in" space-after="0.01in"
			start-indent="0.1in" end-indent="1.5in">
			<xsl:apply-templates select="InvoiceDetailsLabel" />
		</fo:block>
		<!-- BSL IR POUL032966481 BEGIN -->
		<!-- <fo:block font-size="8pt" space-before="0.01in" space-after="0.1in"
			start-indent="0.1in" end-indent="1.5in"> -->
		<fo:block font-size="8pt" space-before="0.01in" space-after="0.1in"
			start-indent="0.1in" end-indent="1.5in"
			linefeed-treatment="preserve" white-space-collapse="false"
			wrap-option="wrap" white-space-treatment="preserve">
			<!-- BSL IR POUL032966481 END -->
					<xsl:call-template name="GroupedInvoiceDetails"/>
		</fo:block>
	</xsl:template>
	<xsl:template match="EmailAttachment/SectionD">
		<fo:block font-size="7pt" space-before="0.25in" space-after="0.01in"
			start-indent="0.1in" end-indent="0.5in">
			<xsl:apply-templates select="Disclaimer" />
		</fo:block>
	</xsl:template>
</xsl:stylesheet>