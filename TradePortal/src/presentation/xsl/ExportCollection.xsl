<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:import href="./templates/DefaultFonts.xsl"/><!-- BSL Cocoon Upgrade 10/12/11 Rel 7.0 ADD -->
   <xsl:template match="ExportCollection">
      <!--<xsl:processing-instruction name="cocoon-format">type="text/xslfo"</xsl:processing-instruction>--><!-- BSL Cocoon Upgrade 10/12/11 Rel 7.0 DELETE -->
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
            <fo:simple-page-master master-name="normal">
               <fo:region-body margin-bottom="50pt" margin-top="50pt" margin-left="50pt" margin-right="50pt"/>
            </fo:simple-page-master>
         </fo:layout-master-set>
         <fo:page-sequence master-reference="normal"><!-- BSL Cocoon Upgrade 10/12/11 Rel 7.0 - change master-name to master-reference and use wrapper to specify Unicode font -->
            <fo:flow flow-name="xsl-region-body">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <xsl:apply-templates/>
               </xsl:element>
            </fo:flow>
         </fo:page-sequence>
      </fo:root>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionA">
      <xsl:apply-templates select="./Title"/>
      <fo:table>
         <fo:table-column column-width="300pt"/>
         <fo:table-column column-width="300pt"/>
         <fo:table-body>
            <fo:table-row>
               <xsl:apply-templates select="./Logo"/>
               <xsl:apply-templates select="./CollectionReference"/>         
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionA/Title">
      <xsl:apply-templates/>
      <fo:block><fo:leader leader-pattern="space"/></fo:block>
   </xsl:template>       
   <xsl:template match="ExportCollection/SectionA/Title/Line1">
      <fo:block font-weight="bold" text-align="center"><xsl:apply-templates/></fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionA/Logo">
      <fo:table-cell>
         <fo:block><!-- BSL Cocoon Upgrade 10/05/11 Rel 7.0 - external-graphic should be wrapped in a block -->
         <!-- DK IR STUL081684599 06/29/2012 Rel 7.1cma Begin -->
            <fo:external-graphic content-height="70%" content-width="250%">
		 <!-- DK IR STUL081684599 06/29/2012 Rel 7.1cma End -->            
               <xsl:attribute name="src"><xsl:value-of select="."/>pdf_logo.gif</xsl:attribute>
            </fo:external-graphic>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionA/CollectionReference">
      <fo:table-cell>
         <fo:block font-weight="bold">COLLECTION REFERENCE:</fo:block>
         <fo:block>
            <xsl:apply-templates/>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB">
      <xsl:apply-templates/>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress">
      <fo:block font-size="10pt">
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/ComplexAddressLine">
      <fo:table>
         <fo:table-column column-width="300pt"/>
         <fo:table-column column-width="85pt"/>
         <fo:table-column column-width="100pt"/>
         <fo:table-body>
            <fo:table-row>
               <xsl:apply-templates/>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/ComplexAddressLine/BankOrgName">
      <fo:table-cell>
         <fo:block>
            <xsl:apply-templates/>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/ComplexAddressLine/CollectionDateTag">
      <fo:table-cell>
         <fo:block font-weight="bold">Collection Date</fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/ComplexAddressLine/AmendmentDateTag">
      <fo:table-cell>
         <fo:block font-weight="bold">Amendment Date</fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/ComplexAddressLine/TracerDateTag">
      <fo:table-cell>
         <fo:block font-weight="bold">Tracer Date</fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/ComplexAddressLine/DateValue">
      <fo:table-cell>
         <fo:block>
            <xsl:apply-templates/>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/BankAddressLine">
      <fo:block>
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/TelephoneNumber">
      <fo:inline font-weight="bold" white-space-collapse="false">Tel:      </fo:inline>
      <fo:inline>
         <xsl:apply-templates/>
      </fo:inline>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/FaxNumber">
      <fo:inline font-weight="bold" white-space-collapse="false">      Fax:</fo:inline>
      <fo:inline>
         <xsl:apply-templates/>
      </fo:inline>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/BankSwift">
      <fo:block/>
      <fo:inline font-weight="bold" white-space-collapse="false">Swift:   </fo:inline>
      <fo:inline>
         <xsl:apply-templates/>
      </fo:inline>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/BankAddress/BankTelex">
      <fo:block/>
      <fo:inline font-weight="bold" white-space-collapse="false">Telex:  </fo:inline>
      <fo:inline>
         <xsl:apply-templates/>
      </fo:inline>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/CustomerAddress">
      <fo:block font-size="10pt" space-before.optimum="25pt">
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionB/CustomerAddress/CustomerAddressLine">
      <fo:block>
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionC">
      <fo:block font-size="10pt" space-before.optimum="15pt">
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionC/IssueStatement">
      <fo:block font-weight="bold" white-space-collapse="false">We enclose the following documents for Collection. Please be guided by the instructions indicated below.</fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionC/AmendStatement">
      <fo:block font-weight="bold" white-space-collapse="false">We hereby amend the terms and or conditions of our collection referenced above.  Please be guided by the instructions indicated below.</fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionC/TracerStatementDate">
      <fo:block font-weight="bold">
         According to our records the above mentioned, collection forwarded to you on 
         <xsl:apply-templates/>
         , remains outstanding.
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionC/TracerStatementBankOrg">
      <fo:block font-weight="bold">
         Please advise by return SWIFT/Airmail the current status of this collection to 
         <xsl:apply-templates/>
          at the head of the schedule.
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionD">
      <fo:table space-before.optimum="15pt">
         <fo:table-column column-width="235pt"/>
         <fo:table-column column-width="235pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block font-size="10pt" font-weight="bold">Drawer:</fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block font-size="10pt" font-weight="bold">Drawee:</fo:block>
               </fo:table-cell>
            </fo:table-row>
            <xsl:apply-templates/>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionD/DrawerDraweeRow">
      <fo:table-row>
         <xsl:apply-templates/>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionD/DrawerDraweeRow/DrawerDraweeCell">
      <fo:table-cell>
         <fo:block font-size="10pt">
            <xsl:apply-templates/>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionE">
      <fo:block font-size="10pt" space-before.optimum="15pt">
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionE/CollectionTable">
      <fo:table>
         <fo:table-column column-width="235pt"/>
         <fo:table-column column-width="235pt"/>
         <fo:table-body>
            <xsl:apply-templates/>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionE/CollectionTable/CollectionRow">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-weight="bold">Collection Amount:</fo:block>
            <fo:block>
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-weight="bold"/>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionE/CollectionTable/AmendTracerRow">
      <fo:table-row>
         <xsl:apply-templates/>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionE/CollectionTable/AmendTracerRow/CollectionAmount">
      <fo:table-cell>
         <fo:block font-weight="bold">Original Collection Amount:</fo:block>
         <fo:block>
            <xsl:apply-templates/>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionE/CollectionTable/AmendTracerRow/CollectionDate">
      <fo:table-cell>
         <fo:block font-weight="bold">Original Collection Date:</fo:block>
         <fo:block>
            <xsl:apply-templates/>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionE/CollectionAmount">
      <fo:block>
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionE/NewCollection">
      <fo:block font-weight="bold">New Collection Amount:</fo:block>
      <fo:block>
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionE/NewCollection/CollectionAmount">
      <fo:block>
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionE/NewCollection/CollectionAmountWords">
      <fo:block>
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF">
      <fo:block font-size="10pt">
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/Issue">
      <fo:block font-size="10pt" font-weight="bold" space-before.optimum="15pt">Tenor Detail(s):</fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/Amendment">
      <fo:block font-size="10pt" font-weight="bold" space-before.optimum="15pt">Amended Tenor Detail(s):</fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/Sight">
      <fo:block>Sight</fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/CashAgnDocs">
      <fo:block>Cash Against Documents</fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/NumberOfDays">
      <fo:block>
         <xsl:apply-templates select="TenorDays" />
          <fo:inline white-space-collapse="false"> days after </fo:inline>
         <xsl:apply-templates select="TenorType" />
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/NumberOfDays/TenorDays">
      <fo:inline>
         <xsl:apply-templates/>
      </fo:inline>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/NumberOfDays/TenorType">
      <fo:inline>
         <xsl:apply-templates/>
      </fo:inline>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/AtFixedMature">
      <fo:block>
         Fixed Maturity Date - 
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/MultipleTenors">
      <fo:table>
         <fo:table-column column-width="25pt"/>
         <fo:table-column column-width="205pt"/>
         <fo:table-column column-width="150pt"/>
         <fo:table-column column-width="150pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block font-size="10pt" font-weight="bold"/>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block font-size="10pt" font-weight="bold">Tenor</fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block font-size="10pt" font-weight="bold">Amount</fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block font-size="10pt" font-weight="bold">Draft Number</fo:block>
               </fo:table-cell>
            </fo:table-row>
            <xsl:apply-templates/>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/MultipleTenors/TenorLine">
      <fo:table-row>
         <xsl:apply-templates/>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/MultipleTenors/TenorLine/RowNumber">
      <fo:table-cell>
         <fo:block font-size="10pt">
            <xsl:apply-templates/>
            .
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/MultipleTenors/TenorLine/Tenor">
      <fo:table-cell>
         <fo:block font-size="10pt">
            <xsl:apply-templates/>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/MultipleTenors/TenorLine/Amount">
      <fo:table-cell>
         <fo:block font-size="10pt">
            <xsl:apply-templates/>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionF/MultipleTenors/TenorLine/DraftNumber">
      <fo:table-cell>
         <fo:block font-size="10pt">
            <xsl:apply-templates/>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG">
      <fo:block font-size="10pt" font-weight="bold" space-before.optimum="15pt">We enclose the following documents:</fo:block>
      <fo:table>
         <fo:table-column column-width="25pt"/>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <xsl:apply-templates/>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG/DocumentRow">
      <fo:table-row>
         <xsl:apply-templates/>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG/BillOfEx">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Bill(s) of Exchange / Drafts</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG/CommInvoice">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Commercial Invoice</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG/BillOfLading">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Bill of Lading</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG/NonNegBillOfLading">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Non-Negotiable Bill of Lading</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG/AirWaybill">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Air Waybill</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG/InsurancePolicy">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Insurance Policy / Certificate</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG/CertificateOfOrigin">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Certificate of Origin</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG/PackingList">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Packing List</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionG/DocumentText">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt"/>
         </fo:table-cell>
         <fo:table-cell>
            <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
            <!--<fo:block font-size="10pt" white-space-collapse="false">-->
            <fo:block font-size="10pt" white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
            <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH">
      <fo:block font-size="10pt" font-weight="bold" space-before.optimum="15pt">Instructions:</fo:block>
      <fo:table>
         <fo:table-column column-width="25pt"/>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <xsl:apply-templates/>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/ReleaseDocOnPay">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Release documents on payment</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/ReleaseDocOnAccept">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Release documents on acceptance</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/AdvisePayByTelecom">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Advise payment by telecommunication</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/AdviseAcceptByTelecom">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Advise acceptance by telecommunication</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/AdviseNonPayByTelecom">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Advise non-payment by telecommunication</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/AdviseNonAcceptByTelecom">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Advise non-acceptance by telecommunication</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/NoteForNonPay">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Note for non-payment</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/ProtestForNonPay">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Protest for non-payment</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/NoteForNonAccept">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Note for non-acceptance</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/ProtestForNonAccept">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Protest for non-acceptance</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/IncludeOurCharges">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">All bank charges, including overseas bank charges, are for the account of the Drawee,</fo:block>
            <fo:block font-size="10pt">including our charges of:</fo:block>
            <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
            <!--<fo:block font-size="10pt" white-space-collapse="false">-->
            <fo:block font-size="10pt" white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
            <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/OverseasCharges">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Overseas / other bank charges are for the account of the Drawee</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/AllCharges">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">All charges for the account of the Drawer</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/DontWaiveCharges">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Do not waive charges if refused</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/DontWaiveInterest">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Do not waive interest if refused</fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/CollectInterestAt">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">Collect interest at:</fo:block>
            <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
            <!--<fo:block font-size="10pt" white-space-collapse="false">-->
            <fo:block font-size="10pt" white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
            <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/InCaseOfNeed">
      <fo:table-row>
         <fo:table-cell>
            <fo:block font-size="10pt">-</fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block font-size="10pt">In case of need refer to:</fo:block>
            <fo:block font-size="10pt">
               <xsl:apply-templates/>
            </fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/InCaseOfNeed/Name">
      <fo:block>
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/InCaseOfNeed/AddressLine1">
      <fo:block>
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/InCaseOfNeed/AddressLine2">
      <fo:block>
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/InCaseOfNeed/AddressLine3">
      <fo:block>
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/InCaseOfNeed/HasAuthority">
      <fo:block>Who has authority to amend the terms of this collection.</fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/InCaseOfNeed/HasNoAuthority">
      <fo:block>Who does not have authority to amend the terms of this collection.</fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/InCaseOfNeed/AdditionalInstructions">
      <fo:block>Additional Case of Need Instructions:</fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionH/InCaseOfNeed/Text">
      <!-- BSL IR RSUL121604111 12/20/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false">-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
      <!-- BSL IR RSUL121604111 12/20/11 Rel 7.1 END -->
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionI">
      <fo:block font-size="10pt" font-weight="bold" space-before.optimum="15pt">Additional Instructions:</fo:block>
      <!-- BSL IR RSUL121604111 12/20/11 Rel 7.1 BEGIN -->
      <!--<fo:block font-size="10pt" white-space-collapse="false">-->
      <fo:block font-size="10pt" white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
      <!-- BSL IR RSUL121604111 12/20/11 Rel 7.1 END -->
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionJ">
      <fo:block font-size="10pt" font-weight="bold" space-before.optimum="15pt">Settlement Instructions:</fo:block>
      <fo:block font-size="10pt">
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionK">
      <fo:block font-size="10pt" font-weight="bold" space-before.optimum="15pt">
         <xsl:apply-templates/>
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionK/Line1">
      <fo:block>
         This collection is subject to the Uniform Rules for Collection, 
         <xsl:apply-templates/>
          Revision,
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionK/Line2">
      <fo:block>
         International Chamber of Commerce Publication number<xsl:apply-templates/>.
      </fo:block>
   </xsl:template>
   <xsl:template match="ExportCollection/SectionL">
      <fo:block font-size="10pt" font-weight="bold" space-before.optimum="15pt">This is a computer-generated advice and no signature is required.</fo:block>
   </xsl:template>
</xsl:stylesheet>
