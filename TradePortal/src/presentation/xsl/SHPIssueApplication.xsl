<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">   
   <xsl:import href="./templates/DefaultFonts.xsl"/>
   <xsl:import href="./templates/FOCommon.xsl"/>
   <xsl:import href="./templates/SectionA-ExtendedIssue.xsl"/>
   <xsl:import href="./templates/SectionB.xsl"/>
   <xsl:import href="./templates/SectionC-SHPIssue.xsl"/>
   <xsl:import href="./templates/SectionD-ExtendedIssue.xsl"/>
   <xsl:import href="./templates/SectionD1.xsl"/>
   <xsl:import href="./templates/SectionE.xsl"/>
   <xsl:import href="./templates/SectionF-SHPIssue.xsl"/>
   <xsl:import href="./templates/SectionG-SHPIssue.xsl"/>
   <xsl:import href="./templates/SectionT-SHPIssue.xsl"/>
   <xsl:import href="./templates/SectionM-SHPIssue.xsl"/>
      
   <xsl:template match="Document">
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
            <xsl:call-template name="SimplePageMaster-HeaderBodyFooter"/>
         </fo:layout-master-set>

         <fo:page-sequence master-reference="HeaderBodyFooter">
            <fo:static-content flow-name="xsl-region-before">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <xsl:apply-templates select="./SectionA"/>
               </xsl:element>
            </fo:static-content>
            <fo:static-content flow-name="xsl-region-after">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <fo:block text-align="center">Page: <fo:page-number/></fo:block>
               </xsl:element>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <xsl:apply-templates select="./SectionB"/>
                  <xsl:apply-templates select="./SectionC"/>
                  <xsl:apply-templates select="./SectionD"/>
                  <xsl:apply-templates select="./SectionD1"/>
                  <xsl:apply-templates select="./SectionE"/>
                  <xsl:apply-templates select="./SectionF"/>
                  <xsl:apply-templates select="./SectionG"/>
                  <xsl:apply-templates select="./SectionT"/>
                  <xsl:apply-templates select="./SectionM"/>
               </xsl:element>
            </fo:flow>
         </fo:page-sequence>
         
      </fo:root>
   </xsl:template>
   
</xsl:stylesheet>
