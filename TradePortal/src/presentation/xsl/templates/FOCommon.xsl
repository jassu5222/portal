<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template name="SimplePageMaster-HeaderBodyFooter">
		<fo:simple-page-master master-name="HeaderBodyFooter">
			<fo:region-body margin-bottom="75pt" margin-top="195pt" margin-left="35pt" margin-right="35pt"/>
			<fo:region-before extent="170pt" padding-top="35pt" padding-left="35pt"/>
			<fo:region-after extent="50pt"/>
		</fo:simple-page-master>
	</xsl:template>
<!-- BSL IR RNUM050251590 06/07/2012 Rel 8.0 - rewrote splitString and put in common XSL -->
	<xsl:template name="splitString">
		<xsl:param name="value"/>
		<xsl:param name="currentWordLength" select="1"/>
		<xsl:param name="maxWordLength" select="15"/>

		<xsl:if test="string-length($value) &gt; 0">
			<xsl:variable name="c1" select="substring($value, 1, 1)"/>
			<xsl:variable name="c2" select="substring($value, 2, 1)"/>
			<xsl:variable name="spaceChars">
&#x9;&#xA;
&#x2000;&#x2001;&#x2002;&#x2003;&#x2004;&#x2005;
&#x2006;&#x2007;&#x2008;&#x2009;&#x200A;&#x200B;
			</xsl:variable>
			<xsl:variable name="foundSpace" select="contains($spaceChars, $c1) or contains($spaceChars, $c2)"/>

			<xsl:value-of select="$c1"/>
			<xsl:choose>
				<xsl:when test="$currentWordLength + 1 &gt; $maxWordLength">
					<xsl:if test="$c2 != '' and not($foundSpace)">
						<xsl:text>&#x200B;</xsl:text>
					</xsl:if>
					<xsl:call-template name="splitString">
						<xsl:with-param name="value" select="substring($value, 2)"/>
						<xsl:with-param name="maxWordLength" select="$maxWordLength"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="$foundSpace">
							<xsl:call-template name="splitString">
								<xsl:with-param name="value" select="substring($value, 2)"/>
								<xsl:with-param name="maxWordLength" select="$maxWordLength"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="splitString">
								<xsl:with-param name="value" select="substring($value, 2)"/>
								<xsl:with-param name="currentWordLength" select="$currentWordLength + 1"/>
								<xsl:with-param name="maxWordLength" select="$maxWordLength"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>