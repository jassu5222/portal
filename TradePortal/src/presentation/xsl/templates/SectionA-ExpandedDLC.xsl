<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:template match="Document/SectionA">
      <xsl:apply-templates select="./Title"/>
      <fo:block><fo:leader leader-pattern="space"/></fo:block>
      <fo:table>
         <fo:table-column column-width="200pt"/>
         <fo:table-column column-width="20pt"/>
         <fo:table-column column-width="280pt"/>         
         <fo:table-body>
            <fo:table-row>
               <xsl:apply-templates select="./Logo"/>
               <fo:table-cell/>
               <xsl:apply-templates select="./TransInfo"/>         
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:block><fo:leader leader-length="500pt" leader-pattern="rule" rule-thickness="1pt" color="black"/></fo:block>
   </xsl:template>
     
   <xsl:template match="Document/SectionA/Title">
      <xsl:apply-templates/>
   </xsl:template>    
     
   <xsl:template match="Document/SectionA/Title/Line1">
      <fo:block font-weight="bold" text-align="center"><xsl:apply-templates/></fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionA/Title/Line2">
      <fo:block font-weight="bold" text-align="center"><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionA/Title/Line3">
      <fo:block font-weight="bold" text-align="center"><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionA/Logo">
      <fo:table-cell>
         <fo:block><!-- BSL Cocoon Upgrade 10/05/11 Rel 7.0 - external-graphic should be wrapped in a block -->
            <fo:external-graphic><!-- DK IR  T36000026736 04/07/2014 Rel 8.4 -->
               <xsl:attribute name="src"><xsl:value-of select="."/>pdf_logo.gif</xsl:attribute>
            </fo:external-graphic>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
   
   <xsl:template match="Document/SectionA/TransInfo">
      <fo:table-cell>
         <xsl:apply-templates/>
      </fo:table-cell>
   </xsl:template>
   <xsl:template match="Document/SectionA/TransInfo/BankInstrumentID">
      <fo:block>
         <fo:inline font-weight="bold">Bank Instrument ID: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>        
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionA/TransInfo/InstrumentID">
      <fo:block>
         <fo:inline font-weight="bold">Instrument ID: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>        
      </fo:block>  
   </xsl:template>
  

   <xsl:template match="Document/SectionA/TransInfo/TransactionID">
      <fo:block>
         <fo:inline font-weight="bold">Transaction ID (Bank Use Only):  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>        
      </fo:block>
   </xsl:template>    
   
   <xsl:template match="Document/SectionA/TransInfo/ReferenceNumber">
      <fo:block>
         <fo:inline font-weight="bold">Our Reference: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
     
   <xsl:template match="Document/SectionA/TransInfo/ApplicationDate">
      <fo:block>
         <fo:inline font-weight="bold">Application Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionA/TransInfo/AmendmentDate">
      <fo:block>
         <fo:inline font-weight="bold">Amendment Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
</xsl:stylesheet>