<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:template match="Document/SectionC">
      <fo:table text-align="justify">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row>
            	<fo:table-cell>
            	   <fo:block font-weight="bold">
            	      <fo:inline>We ("Applicant") request you, </fo:inline>
            	      <fo:inline><xsl:apply-templates/></fo:inline>
            	      <fo:inline> ("Bank") to issue an Irrevocable Commercial Letter of Credit 
            	      ("Credit") with the following terms and conditions for delivery to the 
            	      beneficiary named below ("Beneficiary") by: SWIFT</fo:inline>
            	   </fo:block>
            	</fo:table-cell>
            </fo:table-row>
	 </fo:table-body>
      </fo:table>     
   </xsl:template> 
   
</xsl:stylesheet>