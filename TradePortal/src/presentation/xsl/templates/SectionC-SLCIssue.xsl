<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:template match="Document/SectionC">
      <fo:table text-align="justify">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row>
            	<fo:table-cell>
            	   <fo:block font-weight="bold">
            	   <!-- //jgadela 06-18-2014 Rel-9.0 IR# T36000026067. Made the text dynamic  -->
            	   	<!-- //Rel-8.4.0.1 IR# T36000028107. Introduce back the code changed by  T36000021894- Begin  -->
            	     <!-- <fo:inline>We ("Applicant") request you, </fo:inline>
            	      <fo:inline><xsl:apply-templates/></fo:inline>
            	      <fo:inline> ("Bank") to issue an irrevocable standby letter of credit 
            	      ("Credit") with the following terms and conditions for delivery to the 
            	      beneficiary named below ("Beneficiary") by: SWIFT</fo:inline> -->
            	      <xsl:apply-templates/>
         
            	   <!-- Rel-8.4 IR# T36000021894 on 12/04/2013 - Begin -->
            	      	   <!-- //Rel-8.4.0.1 IR# T36000028107. comment out new code changed done T36000021894
	            	      	<fo:inline>We ("Applicant") request you, </fo:inline>
	            	      	<fo:inline><xsl:apply-templates select="Name"/></fo:inline>
	            	      	<fo:inline> ("Bank") to issue an irrevocable standby letter of credit 
	            	      		("Credit") with the following terms and conditions for delivery to the party named below ("
	            	      	</fo:inline>
	   	  					<fo:inline><xsl:apply-templates select="DeliverTo"/></fo:inline>	
	            	      	<fo:inline> ") by: </fo:inline>
	            	      	<fo:inline><xsl:apply-templates select="DeliverBy"/></fo:inline>
            	      	-->
            	   <!-- Rel-8.4 IR# T36000021894 on 12/04/2013 - End --> 
            	   <!-- //Rel-8.4.0.1 IR# T36000028107. Backing out changes done under T36000021894 - End  -->
            	              	   	
            	   </fo:block>
            	</fo:table-cell>
            </fo:table-row>
	 </fo:table-body>
      </fo:table>     
   </xsl:template> 
   
</xsl:stylesheet>