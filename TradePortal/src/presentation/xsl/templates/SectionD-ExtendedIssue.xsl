<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:template match="Document/SectionD">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table>
         <fo:table-column column-width="300pt"/>
         <fo:table-column column-width="225pt"/>
         <fo:table-body>      
             <xsl:if test="count(Party) = 1">
               <fo:table-row>
                  <fo:table-cell>
                     <xsl:apply-templates select="./Party[1]"/>
                  </fo:table-cell>
                  <fo:table-cell/>
               </fo:table-row>
            </xsl:if>
            
            <xsl:if test="count(Party) = 2">
               <fo:table-row>
                  <fo:table-cell>
                     <xsl:apply-templates select="./Party[1]"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./Party[2]"/>
                  </fo:table-cell>
               </fo:table-row>
            </xsl:if>         
            
         </fo:table-body>
      </fo:table>
   </xsl:template>
                   
   <xsl:template match="Document/SectionD/Party/Label">
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline"><xsl:apply-templates/>:</fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionD/Party/Name">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionD/Party/AddressLine1">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionD/Party/AddressLine2">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionD/Party/AddressLine3">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionD/Party/AddressStateProvince">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>
                
   <xsl:template match="Document/SectionD/Party/AddressCountryPostCode">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionD/Party/PhoneNumber">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionD/Party/RefNum">
      <fo:block>
         <fo:inline font-weight="bold">Applicant Reference </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
</xsl:stylesheet>