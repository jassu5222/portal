<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="Document/SectionD/PayDtls/SubTitle">
		<fo:block><fo:leader leader-length="500pt" leader-pattern="space"/></fo:block>
       <fo:block>
          <fo:inline color="red" text-decoration="underline" font-style="italic"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/PayFull/SubTitle">
       <fo:block>
          <fo:inline font-weight="bold"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/PayFull/Account/SubTitle">
       <fo:block>
          <fo:inline font-weight="bold">Payment Details: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/PayFull/Account/DebitAccForPrinciple">
       <fo:block>
		<!-- <fo:inline><fo:leader leader-length="20pt" leader-pattern="space"/></fo:inline> -->
          <fo:inline font-weight="bold">Debit Account for Principal: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/PayFull/Account/DebitAccForInterest">
       <fo:block>
          <!-- <fo:inline><fo:leader leader-length="20pt" leader-pattern="space"/></fo:inline> -->
          <fo:inline font-weight="bold">Debit Account for Interest: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/PayFull/Account/DebitAccForCharges">
       <fo:block>
          <!-- <fo:inline><fo:leader leader-length="20pt" leader-pattern="space"/></fo:inline> -->
          <fo:inline font-weight="bold">Debit Account for Charges: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/PayFull/Account/Remitted">
       <fo:block>
          <fo:inline font-weight="bold">Payment Details: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/Rollover/FullAmt/SubTitle">
       <fo:block>
          <fo:inline font-weight="bold"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/Rollover/FullAmt/Terms">
       <fo:block>
          <fo:inline font-weight="bold"> <xsl:apply-templates select="./SubTitle"/></fo:inline>
          <fo:inline><xsl:apply-templates select="./Details"/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/Rollover/PartialAmt/SubTitle">
       <fo:block>
          <fo:inline font-weight="bold"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/Rollover/PartialAmt/Account/DebitAccForPrinciple">
       <fo:block>
          <!-- <fo:inline><fo:leader leader-length="20pt" leader-pattern="space"/></fo:inline> -->
          <fo:inline font-weight="bold">Debit Account for Principal: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/Rollover/PartialAmt/Account/DebitAccForInterest">
       <fo:block>
          <!-- <fo:inline><fo:leader leader-length="20pt" leader-pattern="space"/></fo:inline> -->
          <fo:inline font-weight="bold">Debit Account for Interest: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/Rollover/PartialAmt/Account/DebitAccForCharges">
       <fo:block>
          <!-- <fo:inline><fo:leader leader-length="20pt" leader-pattern="space"/></fo:inline> -->
          <fo:inline font-weight="bold">Debit Account for Charges: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
    <xsl:template match="Document/SectionD/PayDtls/Rollover/PartialAmt/Remitted">
       <fo:block>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/PayDtls/Rollover/PartialAmt/Terms">
       <fo:block><fo:leader leader-length="500pt" leader-pattern="space"/></fo:block>
       <fo:block>
          <fo:inline font-weight="bold"> <xsl:apply-templates select="./SubTitle"/></fo:inline>
          <fo:inline><xsl:apply-templates select="./Details"/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionD/Other">
   	   <fo:block><fo:leader leader-length="500pt" leader-pattern="space"/></fo:block>
       <fo:block>
          <fo:inline font-weight="bold">Other: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
</xsl:stylesheet>