<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:template match="Document/SectionD1">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>     
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <xsl:apply-templates select="./Party[1]"/>
               </fo:table-cell>
            </fo:table-row>
	 </fo:table-body>
      </fo:table>     
   </xsl:template>  
  
   <xsl:template match="Document/SectionD1/Party/Label">
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline"><xsl:apply-templates/>:</fo:inline>
      </fo:block>
   </xsl:template>
  
   <xsl:template match="Document/SectionD1/Party/Name">
      <fo:block><xsl:apply-templates/></fo:block>   
   </xsl:template>  
   
   <xsl:template match="Document/SectionD1/Party/AddressLine1">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionD1/Party/AddressLine2"> 
      <fo:block><xsl:apply-templates/></fo:block>   
   </xsl:template>     
   
   <xsl:template match="Document/SectionD1/Party/AddressStateProvince">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>        
  
   <xsl:template match="Document/SectionD1/Party/AddressCountryPostCode">
      <fo:block><xsl:apply-templates/></fo:block> 
   </xsl:template>    
   
   <xsl:template match="Document/SectionD1/Party/PhoneNumber">
      <fo:block>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>   
   </xsl:template>     
      
   <xsl:template match="Document/SectionD1/Party/CarrierFax">
      <fo:block>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionD1/Party/CarrierAttentionOf">
      <fo:block>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
</xsl:stylesheet>