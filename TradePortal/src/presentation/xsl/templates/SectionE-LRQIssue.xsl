<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionE">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>    
      <fo:table text-align="justify">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>     
                  <xsl:apply-templates/>      
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
   
   <xsl:template match="Document/SectionE/LoanRequestDetails"> 
      <fo:block>
         <fo:inline font-weight="bold" text-decoration="underline">Loan Request Details: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionE/LoanRequestDetails/StartDate">
      <fo:block>
         <fo:inline font-weight="bold">Start Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionE/LoanRequestDetails/LoanTerms">
      <fo:block>
         <fo:inline font-weight="bold">Loan Terms:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
    
    
   <xsl:template match="Document/SectionE/LoanRequestDetails/AmountDetail">
      <fo:block>
         <fo:inline font-weight="bold">Amount Details: </fo:inline>
         <xsl:apply-templates/>
      </fo:block>      
   </xsl:template>
   
   <xsl:template match="Document/SectionE/LoanRequestDetails/AmountDetail/CurrencyValueAmountInNumbersAndWords">
         <fo:inline><xsl:apply-templates/> </fo:inline>
   </xsl:template>   
 
   <xsl:template match="Document/SectionE/LoanRequestDetails/InterestToBePaid">
      <fo:block>
         <fo:inline font-weight="bold">Interest to be Paid: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
</xsl:stylesheet>