<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="Document/SectionE/FXRates/Title">
		<fo:block><fo:leader leader-length="500pt" leader-pattern="space"/></fo:block>
       <fo:block>
          <fo:inline font-weight="bold" text-decoration="underline"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionE/FXRates/SubTitle">
       <fo:block>
          <fo:inline color="red" text-decoration="underline" font-style="italic"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionE/FXRates/ExRate">
       <fo:block>
          <fo:inline font-weight="bold"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionE/FXRates/FXContract/SubTitle">
       <fo:block>
          <fo:inline font-weight="bold"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionE/FXRates/FXContract/Num">
       <fo:block>
          <fo:inline>FX Contract Number: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionE/FXRates/FXContract/Curr">
       <fo:block>
          <fo:inline>Currency: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionE/FXRates/FXContract/Rate">
       <fo:block>
          <fo:inline>Rate: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionE/FXRates/Other">
       <fo:block>
          <fo:inline font-weight="bold">Other FX Instructions: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
</xsl:stylesheet>