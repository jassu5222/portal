<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionE0">
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="Document/SectionE0/IssuingBank">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
                  <fo:block font-weight="bold">
                     <fo:inline text-decoration="underline">Issuing Instructions: </fo:inline>
                  </fo:block>  
                  <xsl:apply-templates/> 
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
   
   <xsl:template match="Document/SectionE0/IssuingBank/IssuedBy">
      <fo:block>
         <fo:inline>The instrument is to be issued by </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionE0/IssuingBank/Date">
      <fo:block>
         <fo:inline>Validity Date at Overseas Bank: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionE0/IssuingBank/OverseasBank">
      <fo:block>Overseas Bank:</fo:block>
      <fo:block><xsl:apply-templates select="./Name"/></fo:block>
   </xsl:template>         
   
   
</xsl:stylesheet>