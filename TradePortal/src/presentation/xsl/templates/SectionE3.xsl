<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionE3">
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="Document/SectionE3/DeliveryInstructions">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
                  <fo:block font-weight="bold">
                     <fo:inline text-decoration="underline">Deliver By: </fo:inline>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>     
            <fo:table-row>
  
               <fo:table-cell>
                  <xsl:apply-templates select="./DeliverBy"/>
               </fo:table-cell>        
            </fo:table-row>
         </fo:table-body>
      </fo:table>      
   </xsl:template>          
   
   <xsl:template match="Document/SectionE3/DeliveryInstructions/DeliverBy">
      <fo:block>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
</xsl:stylesheet>