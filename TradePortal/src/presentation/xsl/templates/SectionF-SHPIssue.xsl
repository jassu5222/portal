<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionF">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table>
         <fo:table-column column-width="300pt"/>
         <fo:table-column column-width="225pt"/>
         <fo:table-body>      
            <fo:table-row>     
           	<fo:table-cell>
            	   <xsl:apply-templates select="./AmountDetail"/>
            	</fo:table-cell>    
            	<fo:table-cell>
            	   <xsl:apply-templates select="./RelatedInstrumentId"/>
            	</fo:table-cell>        
           </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>

   <xsl:template match="Document/SectionF/AmountDetail">
      <xsl:apply-templates/>
   </xsl:template>
            
   <xsl:template match="Document/SectionF/AmountDetail/IssueDate">
       <fo:block>
          <fo:inline font-weight="bold">Issue Date: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionF/AmountDetail/CurrencyCodeAmount">
      <fo:block>
         <fo:inline font-weight="bold">Amount Details:</fo:inline>
      </fo:block>
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionF/AmountDetail/CurrencyValueAmountInWords">
       <fo:block>
          <fo:inline>[</fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
          <fo:inline>]</fo:inline>
       </fo:block>
   </xsl:template>   

   <xsl:template match="Document/SectionF/RelatedInstrumentId">
      <xsl:apply-templates/>
   </xsl:template>   
   
   <xsl:template match="Document/SectionF/RelatedInstrumentId/InstrumentID">
      <fo:block>
         <fo:inline font-weight="bold">Related Instrument Id</fo:inline>
      </fo:block>
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>
   
</xsl:stylesheet>