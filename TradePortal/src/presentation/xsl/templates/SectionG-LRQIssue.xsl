<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionG">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>    
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>     
		  <xsl:apply-templates/>      
	       </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
    
   <xsl:template match="Document/SectionG/ShipmentDetails"> 
      <fo:block>
         <fo:inline font-weight="bold" text-decoration="underline">Shipment and Goods Details:</fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionG/ShipmentDetails/DispatchFrom">
      <fo:block>
         <fo:inline font-weight="bold">From Place of Taking Charge/Dispatch From…/Receipt: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionG/ShipmentDetails/AirportOfDeparture">
      <fo:block>
         <fo:inline font-weight="bold">From Port of Loading/Airport of Departure: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionG/ShipmentDetails/AirportOfDestination">
      <fo:block>
         <fo:inline font-weight="bold">To Port of Discharge/Airport of Destination: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionG/ShipmentDetails/FinalDestination">
      <fo:block>
         <fo:inline font-weight="bold">To Place of Final Destination/Delivery/For Transport To: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionG/ShipmentDetails/VesselName">
      <fo:block>
         <fo:inline font-weight="bold">Vessel Name/Carrier: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionG/ShipmentDetails/AirWayBill">
      <fo:block>
         <fo:inline font-weight="bold">Bill of Lading/Air Waybill: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionG/ShipmentDetails/TextBox">
      <fo:block>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>    
   
</xsl:stylesheet>