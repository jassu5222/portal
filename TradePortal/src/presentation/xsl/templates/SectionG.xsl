<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionG">
      <fo:table text-align="justify">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>     
		  <xsl:apply-templates/>      
	       </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
    
   <xsl:template match="Document/SectionG/PresentationDays"> 
      <fo:block>
         <fo:inline font-weight="bold">Documents to be presented within </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
         <fo:inline font-weight="bold"> Days of shipment, but within the validity of the credit.</fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionG/Credit"> 
      <fo:block>
         <fo:inline font-weight="bold">This Credit is </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
</xsl:stylesheet>