<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionH">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table>
         <fo:table-column column-width="300pt"/>
         <fo:table-column column-width="225pt"/>
         <fo:table-body>      
            <fo:table-row>      
		<xsl:apply-templates/>      
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>
    
   <xsl:template match="Document/SectionH/PaymentTerms">
      <fo:table-cell>
         <fo:block font-weight="bold">
            <fo:inline text-decoration="underline">Payment Terms:</fo:inline>
         </fo:block>
         <xsl:apply-templates/>
      </fo:table-cell>
   </xsl:template>

   <xsl:template match="Document/SectionH/PaymentTerms/Sight">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionH/PaymentTerms/DaysAfter">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionH/PaymentTerms/Maturity">
      <fo:block><xsl:apply-templates/></fo:block> 
   </xsl:template>
   
   <xsl:template match="Document/SectionH/PaymentTerms/For">
      <fo:block>For <xsl:apply-templates/>% of the invoice value</fo:block> 
   </xsl:template>

   <xsl:template match="Document/SectionH/PaymentTerms/PmtTenorDetails">
      <fo:block linefeed-treatment="preserve" white-space-collapse="false" white-space-treatment="preserve"><xsl:apply-templates/></fo:block> 
   </xsl:template>
   
   <xsl:template match="Document/SectionH/PaymentTerms/DrawnOnParty">
      <fo:block>
         <fo:inline font-weight="bold">Drafts drawn on </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>   
   </xsl:template>

   <xsl:template match="Document/SectionH/BankCharges">
      <fo:table-cell>
         <fo:block font-weight="bold">
            <fo:inline text-decoration="underline">Bank Charges:</fo:inline>
         </fo:block>
         <xsl:apply-templates/>
      </fo:table-cell>
   </xsl:template>
   
   <xsl:template match="Document/SectionH/BankCharges/AllOurAccount">
      <fo:block>All for Applicant’s Account<xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionH/BankCharges/AllBankCharges">
   	<xsl:choose>
   	  <xsl:when test="/Document/SectionA/Title/Line2 ='Commercial Approval to Pay'">
   	  	<fo:block>All for Seller's account<xsl:apply-templates/></fo:block>
   	  </xsl:when>
   	  <xsl:otherwise>
      	<fo:block>All bank charges other than the Issuing Bank’s charges are for the account of the beneficiary<xsl:apply-templates/></fo:block>
      </xsl:otherwise>
    </xsl:choose>
   </xsl:template>

   <xsl:template match="Document/SectionH/BankCharges/Other">
      <fo:block>See Additional Conditions<xsl:apply-templates/></fo:block>
   </xsl:template>
   
</xsl:stylesheet>