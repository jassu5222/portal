<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionH1">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table>
         <fo:table-column column-width="300pt"/>
         
         <fo:table-body>      
            <fo:table-row>      
               <xsl:apply-templates/>      
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   
   
   
   <xsl:template match="Document/SectionH1/BankCharges">
      <fo:table-cell>
         <fo:block font-weight="bold">
            <fo:inline text-decoration="underline">Bank Charges:</fo:inline>
         </fo:block>
         <xsl:apply-templates/>
      </fo:table-cell>
   </xsl:template>
   
   <xsl:template match="Document/SectionH1/BankCharges/AllOurAccount">
      <fo:block>All for Applicant’s Account<xsl:apply-templates/></fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionH1/BankCharges/AllBankCharges">
      <xsl:choose>
         <xsl:when test="/Document/SectionA/Title/Line2 ='Commercial Approval to Pay'">
            <fo:block>All for Seller's account<xsl:apply-templates/></fo:block>
         </xsl:when>
         <xsl:otherwise>
            <fo:block>All bank charges other than the Issuing Bank’s charges are for the account of the beneficiary<xsl:apply-templates/></fo:block>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   
   <xsl:template match="Document/SectionH1/BankCharges/Other">
      <fo:block>See Additional Conditions<xsl:apply-templates/></fo:block>
   </xsl:template>
   
</xsl:stylesheet>