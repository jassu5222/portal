<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionH3">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>    
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>     
                  <xsl:apply-templates/>      
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
   
   <xsl:template match="Document/SectionH3/LoanInstructions"> 
      <fo:block>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionH3/InvoiceDetails">
      <fo:block>
         <fo:inline font-weight="bold">Invoice Details:</fo:inline>
      </fo:block>
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionH3/FinancedInvoices">
      <fo:block>
         <fo:inline font-weight="bold">Details of invoices being financed: </fo:inline>
      </fo:block>
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>   
 
   <xsl:template match="Document/SectionH3/LoanMaturity">
      <fo:block>
         <fo:inline font-weight="bold">Loan Maturity: </fo:inline>
      </fo:block>
       <fo:block>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
</xsl:stylesheet>