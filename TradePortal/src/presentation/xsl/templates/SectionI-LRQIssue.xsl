<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionI">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>    
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>     
                  <xsl:apply-templates/>      
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
   
   <xsl:template match="Document/SectionI/ChargesAndInterest"> 
      <fo:block>
         <fo:inline font-weight="bold" text-decoration="underline">Charges And Interest: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionI/FxConversionDetails"> 
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>         
      <fo:block>
         <fo:inline font-weight="bold" text-decoration="underline">Foreign Exchange Conversion Details: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
     
   <!-- Loan Proceeds start -->
   
   <xsl:template match="Document/SectionI/FxConversionDetails/loanProceeds">
      <fo:block>
         <fo:inline font-weight="bold">Loan Proceeds:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
    
   <xsl:template match="Document/SectionI/FxConversionDetails/loanProceeds/bankToBook">
	   <fo:block>
	         <fo:inline><xsl:apply-templates/></fo:inline>
	   </fo:block>
   </xsl:template>    
    <xsl:template match="Document/SectionI/FxConversionDetails/loanProceeds/fecNum">
      <fo:block>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
    <xsl:template match="Document/SectionI/FxConversionDetails/loanProceeds/other">
	    <fo:block>
	         <fo:inline><xsl:apply-templates/></fo:inline>
	    </fo:block>
   </xsl:template>    
    
   <xsl:template match="Document/SectionI/FxConversionDetails/loanProceeds/fecNum/FecNumber">
      <fo:block>
         <fo:inline font-weight="bold">Covered by FEC Number:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionI/FxConversionDetails/loanProceeds/fecNum/Rate">
      <fo:block>
         <fo:inline font-weight="bold">Rate: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionI/FxConversionDetails/loanProceeds/fecNum/Amount">
      <fo:block>
         <fo:inline font-weight="bold">Amount: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionI/FxConversionDetails/loanProceeds/fecNum/MaturityDate">
      <fo:block>
         <fo:inline font-weight="bold">Maturity Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>    
   
    <!-- Loan Proceeds end -->  
    
   <!-- Loan Maturity start -->
     
    <xsl:template match="Document/SectionI/FxConversionDetails/loanMaturity">
       <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>   
      <fo:block>
         <fo:inline font-weight="bold">Loan Maturity: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionI/FxConversionDetails/loanMaturity/bankToBook">
	   <fo:block>
	         <fo:inline><xsl:apply-templates/></fo:inline>
	   </fo:block>
   </xsl:template>    
    <xsl:template match="Document/SectionI/FxConversionDetails/loanMaturity/fecNum">
      <fo:block>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
    <xsl:template match="Document/SectionI/FxConversionDetails/loanMaturity/other">
	    <fo:block>
	         <fo:inline><xsl:apply-templates/></fo:inline>
	    </fo:block>
   </xsl:template> 
    
   <xsl:template match="Document/SectionI/FxConversionDetails/loanMaturity/fecNum/FecNumber">
      <fo:block>
         <fo:inline font-weight="bold">Covered by FEC Number:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionI/FxConversionDetails/loanMaturity/fecNum/Rate">
      <fo:block>
         <fo:inline font-weight="bold">Rate: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionI/FxConversionDetails/loanMaturity/fecNum/Amount">
      <fo:block>
         <fo:inline font-weight="bold">Amount: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionI/FxConversionDetails/loanMaturity/fecNum/MaturityDate">
      <fo:block>
         <fo:inline font-weight="bold">Maturity Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
  
   
   <!-- Loan Maturity - end -->
 
   
</xsl:stylesheet>