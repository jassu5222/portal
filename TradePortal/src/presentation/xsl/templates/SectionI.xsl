<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionI">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Documents Required:</fo:inline>
      </fo:block>
      <xsl:apply-templates/>      
   </xsl:template>
   
   <xsl:template match="Document/SectionI/comm_invoice">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false"><xsl:apply-templates/></fo:block>-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
   </xsl:template>
   <xsl:template match="Document/SectionI/comm_invoice/Originals">
      <fo:inline><xsl:apply-templates/> Originals </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/comm_invoice/Original">
      <fo:inline><xsl:apply-templates/> Original </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/comm_invoice/Copies">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copies </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/comm_invoice/Copy">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copy </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/comm_invoice/Name">
      <fo:inline>Commercial Invoice</fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/comm_invoice/Text">
      <fo:inline> - <xsl:apply-templates/></fo:inline>
   </xsl:template>

   <xsl:template match="Document/SectionI/packing_list">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false"><xsl:apply-templates/></fo:block>-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
   </xsl:template>
   <xsl:template match="Document/SectionI/packing_list/Originals">
      <fo:inline><xsl:apply-templates/> Originals  </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/packing_list/Original">
      <fo:inline><xsl:apply-templates/> Original </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/packing_list/Copies">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copies </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/packing_list/Copy">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copy </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/packing_list/Name">
      <fo:inline>Packing List</fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/packing_list/Text">
      <fo:inline> - <xsl:apply-templates/></fo:inline>
   </xsl:template>

   <xsl:template match="Document/SectionI/cert_origin">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false"><xsl:apply-templates/></fo:block>-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
   </xsl:template>
   <xsl:template match="Document/SectionI/cert_origin/Originals">
      <fo:inline><xsl:apply-templates/> Originals </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/cert_origin/Original">
      <fo:inline><xsl:apply-templates/> Original </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/cert_origin/Copies">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copies </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/cert_origin/Copy">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copy </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/cert_origin/Name">
      <fo:inline>Certificate of Origin</fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/cert_origin/Text">
      <fo:inline> - <xsl:apply-templates/></fo:inline>
   </xsl:template>

   <xsl:template match="Document/SectionI/ins_policy">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false"><xsl:apply-templates/></fo:block>-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
   </xsl:template>
   <xsl:template match="Document/SectionI/ins_policy/Originals">
      <fo:inline><xsl:apply-templates/> Originals </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/ins_policy/Original">
      <fo:inline><xsl:apply-templates/> Original </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/ins_policy/Copies">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copies </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/ins_policy/Copy">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copy </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/ins_policy/Plus">
      <fo:inline> Insurance Policy / Certificate endorsed in blank for the invoice plus <xsl:apply-templates/> % </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/ins_policy/Covering">
      <fo:inline>covering <xsl:apply-templates/> </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/ins_policy/Text">
      <fo:inline> - <xsl:apply-templates/></fo:inline>
   </xsl:template>

   <xsl:template match="Document/SectionI/other_req_doc_1">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false"><xsl:apply-templates/></fo:block>-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_1/Originals">
      <fo:inline><xsl:apply-templates/> Originals </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_1/Original">
      <fo:inline><xsl:apply-templates/> Original </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_1/Copies">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copies </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_1/Copy">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copy </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_1/Name">
      <fo:inline><xsl:apply-templates/></fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_1/Text">
      <fo:inline> - <xsl:apply-templates/></fo:inline>
   </xsl:template>   

   <xsl:template match="Document/SectionI/other_req_doc_2">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false"><xsl:apply-templates/></fo:block>-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_2/Originals">
      <fo:inline><xsl:apply-templates/> Originals </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_2/Original">
      <fo:inline><xsl:apply-templates/> Original </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_2/Copies">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copies </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_2/Copy">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copy </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_2/Name">
      <fo:inline><xsl:apply-templates/></fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_2/Text">
      <fo:inline> - <xsl:apply-templates/></fo:inline>
   </xsl:template>   

   <xsl:template match="Document/SectionI/other_req_doc_3">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false"><xsl:apply-templates/></fo:block>-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_3/Originals">
      <fo:inline><xsl:apply-templates/> Originals </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_3/Original">
      <fo:inline><xsl:apply-templates/> Original </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_3/Copies">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copies </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_3/Copy">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copy </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_3/Name">
      <fo:inline><xsl:apply-templates/></fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_3/Text">
      <fo:inline> - <xsl:apply-templates/></fo:inline>
   </xsl:template>   

   <xsl:template match="Document/SectionI/other_req_doc_4">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false"><xsl:apply-templates/></fo:block>-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_4/Originals">
      <fo:inline><xsl:apply-templates/> Originals </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_4/Original">
      <fo:inline><xsl:apply-templates/> Original </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_4/Copies">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copies </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_4/Copy">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copy </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_4/Name">
      <fo:inline><xsl:apply-templates/></fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/other_req_doc_4/Text">
      <fo:inline> - <xsl:apply-templates/></fo:inline>
   </xsl:template>   
   <xsl:template match="Document/SectionI/addl_req_doc/*">
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionI/addl_req_doc/*/Originals">
      <fo:inline><xsl:apply-templates/> Originals </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/addl_req_doc/*/Original">
      <fo:inline><xsl:apply-templates/> Original </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/addl_req_doc/*/Copies">
   	  <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
      <fo:inline><xsl:apply-templates/> Copies </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/addl_req_doc/*/Copy">
      <xsl:if test="../Originals or ../Original">
   	  	<fo:inline>and </fo:inline>
   	  </xsl:if>
   
      <fo:inline><xsl:apply-templates/> Copy </fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/addl_req_doc/*/Name">
      <fo:inline><xsl:apply-templates/></fo:inline>
   </xsl:template>
   <xsl:template match="Document/SectionI/addl_req_doc/*/Text">
      <fo:inline> - <xsl:apply-templates/></fo:inline>
   </xsl:template>   
  
 </xsl:stylesheet>
