<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:import href="./po/POList.xsl"/><!-- BSL IR RNUM050251590 06/08/11 Rel 8.0 ADD -->
   <xsl:template match="Document/SectionK">
      <xsl:for-each select="./TransportDocument">
         <xsl:apply-templates/>
      </xsl:for-each>
   </xsl:template>   
   
   <xsl:template match="Document/SectionK/TransportDocument/TransDoc">
      <fo:block>
         <fo:leader leader-pattern="space"/>
       </fo:block>
              
       <fo:block font-weight="bold">
          <fo:inline text-decoration="underline">Transport Document(s) and Shipment Information: <xsl:value-of select="@ID"/></fo:inline>
       </fo:block>  
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">LC Shipment Terms: </fo:inline>
      </fo:block>
       <xsl:apply-templates/> 
   </xsl:template>   

   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/PartialShipment">
      <fo:block>
         <fo:inline font-weight="bold">Partial shipment:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/PresentationDays"> 
      <fo:block>
         <fo:inline>Documents to be presented within </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
         <fo:inline> Days of shipment, but within the validity of the credit.</fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/TransDocHeader">
      <fo:block>
         <fo:inline text-decoration="underline" font-weight="bold">Transport Document: </fo:inline>
      </fo:block>
   </xsl:template>
   

   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/TransDocLabel">
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
         <fo:inline font-weight="bold">Transport Document/Shipment Description: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>    
    <xsl:template match="Document/SectionK/TransportDocument/TransDoc/TransDocType">

   </xsl:template>  
    
   
   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/TransDocumentType">
      <fo:block>
          <fo:inline font-weight="bold">Document Type: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/Originals">
      <fo:block>
         <fo:inline font-weight="bold">Originals: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/Copies">
      <fo:block>
         <fo:inline font-weight="bold">Copies: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/TransShipDeclaration">
      <fo:block>
         <fo:inline font-weight="bold">Description: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/ConsignedOrder">
      <fo:block>
         <fo:inline font-weight="bold">Consigned to the order of: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/ConsignedTo">
      <fo:block>
         <fo:inline font-weight="bold">Consigned to: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/MarkedFreight">
      <fo:block>
         <fo:inline font-weight="bold">Marked Freight: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
      
   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/NotifyParty">
      <fo:block font-weight="bold">Notify Party:</fo:block>
      <fo:block><xsl:apply-templates select="./Name"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressLine1"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressLine2"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressLine3"/></fo:block>
   </xsl:template>   
      
   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/OtherConsignee">
      <fo:block font-weight="bold">Other Consignee Party:</fo:block>
      <fo:block><xsl:apply-templates select="./Name"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressLine1"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressLine2"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressLine3"/></fo:block>
   </xsl:template>
   
    
   
   <xsl:template match="Document/SectionK/TransportDocument/TransDoc/AddlTransDocs">
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
         <fo:inline font-weight="bold">Additional Transport Document: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionK/TransportDocument/ShipmentTerms">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Shipment Details:</fo:inline>
      </fo:block>  
      <xsl:apply-templates/> 
   </xsl:template>
   
   <xsl:template match="Document/SectionK/TransportDocument/ShipmentTerms/TransShipment">
      <fo:block>
         <fo:inline font-weight="bold">Transhipment: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
      
   <xsl:template match="Document/SectionK/TransportDocument/ShipmentTerms/ShipmentDate">
      <fo:block>
         <fo:inline font-weight="bold">Latest Shipment Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
       
   <xsl:template match="Document/SectionK/TransportDocument/ShipmentTerms/Incoterm">
      <fo:block>
         <fo:inline font-weight="bold">Incoterm: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>      

   <xsl:template match="Document/SectionK/TransportDocument/ShipmentTerms/From">
      <fo:block>
         <fo:inline font-weight="bold">From Place of Taking Charge/Dispatch From…/Receipt:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionK/TransportDocument/ShipmentTerms/FromLoad">
      <fo:block>
         <fo:inline font-weight="bold">From Port of Loading/Airport of Departure:   </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionK/TransportDocument/ShipmentTerms/ToDischarge">
      <fo:block>
         <fo:inline font-weight="bold">To Port of Discharge/Airport of Destination:   </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionK/TransportDocument/ShipmentTerms/To">
      <fo:block>
         <fo:inline font-weight="bold">To Place of Final Destination/Delivery/For Transport To: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionK/TransportDocument/GoodsDescription">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Goods Description:</fo:inline>
      </fo:block>    
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
   </xsl:template>
   
    <xsl:template match="Document/SectionK/TransportDocument/POLineItems">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">PO Line Items:</fo:inline>
      </fo:block>    
      <fo:block font-family="Courier" font-size="10pt" font-weight="bold" white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
    <xsl:apply-templates/>
      </fo:block>    
   </xsl:template>
   
   <xsl:template match="Document/SectionK/TransportDocument/PurchaseOrderLists">
    <xsl:call-template name="POList"/>    
   </xsl:template>
   
</xsl:stylesheet>