<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:import href="./po/InvList.xsl"/>
   <xsl:template match="Document/SectionL">
      <fo:block>
        <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
        <fo:inline text-decoration="underline">Other Conditions:</fo:inline>
      </fo:block>    
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="Document/SectionL/Confirmation">
      <fo:block>
         <fo:inline font-weight="bold">Confirmation: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionL/Transferable">
      <fo:block>
         <fo:inline font-weight="bold">Transferable: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>      
   
   <xsl:template match="Document/SectionL/Revolve">
      <fo:block>
         <fo:inline font-weight="bold">Revolve: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <!-- Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - Begin -->
   <xsl:template match="Document/SectionL/ICCPublications">
      <fo:block>
        <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
        <fo:inline text-decoration="underline">ICC Publications:</fo:inline>
      </fo:block>    
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="Document/SectionL/ICCApplicableRules">
      <fo:block>
         <fo:inline font-weight="bold">ICC Applicable Rules: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionL/ICCPVersion">
      <fo:block>
         <fo:inline font-weight="bold">Version: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionL/ICCPDetails">
      <fo:block>
         <fo:inline font-weight="bold">Details: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   <!-- Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - End -->
   
   <xsl:template match="Document/SectionL/AutoExtension/ExtAllowed">
      <fo:block>
        <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
        <fo:inline text-decoration="underline">Auto Extension Terms:</fo:inline>
      </fo:block>    
      <fo:block>
         <fo:inline font-weight="bold">Auto Extension: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   <xsl:template match="Document/SectionL/AutoExtension/MaxExtAllowed">
      <fo:block>
         <fo:inline font-weight="bold">Maximum Number: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 

   <xsl:template match="Document/SectionL/AutoExtension/AutoExtPeriod">
      <fo:block>
         <fo:inline font-weight="bold">Extension Period: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   <xsl:template match="Document/SectionL/AutoExtension/AutoExtDays">
      <fo:block>
         <fo:inline font-weight="bold">Number of Days: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 

   <xsl:template match="Document/SectionL/AutoExtend">
      <fo:block>
         <fo:inline font-weight="bold">Auto Extend: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   <!-- Narayan CR831 Rel9.0 10-Jan-2014 BEGIN -->
   <xsl:template match="Document/SectionL/AutoExtension/FinalExpiryDate">
      <fo:block>
         <fo:inline font-weight="bold">Final Expiry Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 

   <xsl:template match="Document/SectionL/AutoExtension/NotifyBeneDays">
      <fo:block>
         <fo:inline font-weight="bold">Notify Beneficiary Days: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   <!-- Narayan CR831 Rel9.0 10-Jan-2014 END -->

   <xsl:template match="Document/SectionL/InvoiceOnly">
      <fo:block>
         <fo:inline font-weight="bold">Invoices: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>     

   <xsl:template match="Document/SectionL/InvoiceDueDate">
      <fo:block>
         <fo:inline font-weight="bold">Invoice Due Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   <xsl:template match="Document/SectionL/InvoiceDetails">
       <xsl:call-template name="InvList"/>
   </xsl:template>
   <xsl:template match="Document/SectionL/TTReimbursement">
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
         <fo:inline font-weight="bold">TT Reimbursement:</fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   <xsl:template match="Document/SectionL/AdditionalConditions">
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
         <fo:inline font-weight="bold">Additional Conditions: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
</xsl:stylesheet>