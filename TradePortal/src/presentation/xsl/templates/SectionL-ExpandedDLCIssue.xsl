<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:import href="./po/InvList.xsl"/>
   <xsl:template match="Document/SectionL">
      <fo:block>
        <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
        <fo:inline text-decoration="underline">Other Conditions: </fo:inline>
      </fo:block>    
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="Document/SectionL/Confirmation">
      <fo:block>
         <fo:inline font-weight="bold">Confirmation: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionL/Transferable">
      <fo:block>
         <fo:inline font-weight="bold">Transferable: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>      
   
   <xsl:template match="Document/SectionL/Revolve">
      <fo:block>
         <fo:inline font-weight="bold">Revolve: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionL/TTReimbursement">
      <fo:block>
         <fo:inline font-weight="bold">TT Reimbursement: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
    <xsl:template match="Document/SectionL/ICCApplicableRules">
        <fo:block>
            <fo:inline font-weight="bold">ICC Applicable Rules: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template> 
    
    <xsl:template match="Document/SectionL/ICCPDetails">
        <fo:block>
            <fo:inline font-weight="bold">Details: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template>    

   <xsl:template match="Document/SectionL/AdditionalConditions">
      <fo:block>
         <fo:inline font-weight="bold">Additional Conditions: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>


   
</xsl:stylesheet>