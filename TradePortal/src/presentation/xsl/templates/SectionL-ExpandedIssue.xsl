<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="Document/SectionL">
        <fo:block>
            <fo:leader leader-pattern="space"/>
        </fo:block>
        <fo:block font-weight="bold">
            <fo:inline text-decoration="underline">Other Conditions:</fo:inline>
        </fo:block>    
        <xsl:apply-templates/>
    </xsl:template>
    
    
    <xsl:template match="Document/SectionL/AutoExtension">
        <fo:table>
            <fo:table-column column-width="400pt"/>
          <!--  <fo:table-column column-width="225pt"/>-->
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell>                   
                        <fo:block>
                            <fo:inline font-weight="bold">Auto Extension Terms: </fo:inline>
                        </fo:block>                     
                    </fo:table-cell>
                </fo:table-row>  
                <fo:table-row>   
                    <fo:table-cell>    
                        <xsl:apply-templates select="./ExtAllowed"/>    
                    </fo:table-cell>   
                </fo:table-row>
                <fo:table-row>   
                    <fo:table-cell>    
                        <xsl:apply-templates select="./FinalExpiryDate"/>    
                    </fo:table-cell>  
                </fo:table-row>   
                <fo:table-row>       
                    <fo:table-cell>
                        <xsl:apply-templates select="./MaxExtAllowed"/> 
                    </fo:table-cell>
                </fo:table-row>   
                
                <fo:table-row>   
                    <fo:table-cell>    
                        <xsl:apply-templates select="./AutoExtPeriod"/>    
                    </fo:table-cell> 
                </fo:table-row>   
                <fo:table-row>         	
                    <fo:table-cell>
                        <xsl:apply-templates select="./AutoExtDays"/> 
                    </fo:table-cell>
                </fo:table-row>                
                <fo:table-row>   
                    <fo:table-cell>    
                        <xsl:apply-templates select="./NotifyBeneDays"/>    
                    </fo:table-cell> 
                </fo:table-row> 
                
            </fo:table-body>
        </fo:table>
    </xsl:template> 
    
    <xsl:template match="Document/SectionL/AutoExtension/ExtAllowed">
        <fo:block>
            <fo:inline font-weight="bold">Auto Extend: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template> 
    <xsl:template match="Document/SectionL/AutoExtension/FinalExpiryDate">
        <fo:block>
            <fo:inline font-weight="bold">Final Expiry Date: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template> 
    <xsl:template match="Document/SectionL/AutoExtension/MaxExtAllowed">
        <fo:block>
            <fo:inline font-weight="bold">Maximum Number of Extends: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template> 
    
    
    
    <xsl:template match="Document/SectionL/AutoExtension/AutoExtPeriod">
        <fo:block>
            <fo:inline font-weight="bold">Extension Period: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template> 
    <xsl:template match="Document/SectionL/AutoExtension/AutoExtDays">
        <fo:block>
            <fo:inline font-weight="bold">Number of Days: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template> 
    
    
    <xsl:template match="Document/SectionL/AutoExtension/NotifyBeneDays">
        <fo:block>
            <fo:inline font-weight="bold">Notify Beneficiary Days: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template>  
    
    <xsl:template match="Document/SectionL/ICCPVersion">
        <fo:block>
            <fo:inline font-weight="bold">ICC Applicable Rules: </fo:inline>
            <fo:inline>Version: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template> 
    
    <xsl:template match="Document/SectionL/ICCPDetails">
        <fo:block>
            <fo:inline font-weight="bold">Details: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template> 
    
    
    <xsl:template match="Document/SectionL/AdditionalConditions">       
        <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"> 
         	<fo:block>  <fo:leader leader-pattern="space"/> </fo:block>          
            <fo:inline font-weight="bold">Additional Information: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template> 
    
    
    
</xsl:stylesheet>