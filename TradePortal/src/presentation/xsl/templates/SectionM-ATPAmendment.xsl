<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <xsl:template match="Document/SectionM">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table text-align="center">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>
                  <fo:block text-align="center">
                     <fo:leader leader-length="500pt" leader-pattern="rule" rule-thickness="1pt" color="black"/>
                  </fo:block>
                  <fo:block text-align="center" font-weight="bold">Important Notice</fo:block>		      
                  <fo:block><fo:leader leader-pattern="space"/></fo:block>
		  <fo:block text-align="justify" font-weight="bold">Buyer understands that this amendment is subject to acceptance by the Seller and Bank.  All other terms and conditions of ATP shall remain unchanged, and all of our obligations and liabilities to you with respect to ATP shall apply to ATP as so amended.</fo:block>
		  <fo:block><fo:leader leader-pattern="space"/></fo:block>
		  <fo:block text-align="justify" font-weight="bold">Buyer understands that the final form of the amendment to ATP may be subject to such revision and changes as are deemed necessary or appropriate by Bank and Buyer hereby consents to such revisions and changes.</fo:block>
                  <fo:block text-align="center">
                     <fo:leader leader-length="500pt" leader-pattern="rule" rule-thickness="1pt" color="black"/>
                  </fo:block>     
	       </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   
</xsl:stylesheet>