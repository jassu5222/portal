<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<!-- //jgadela 06-18-2014 Rel-9.0 IR# T36000026067. Implemented the pdf app for GUA and Made the Important Notice text dynamic  -->  
   <xsl:template match="Document/SectionM">     
      <fo:table>
      <fo:table-column column-width="500pt"/>
      <fo:table-body>
         <fo:table-row>
            <fo:table-cell>
               <fo:block>
                  <fo:leader leader-pattern="space"/>
               </fo:block>
               <fo:block text-align="center">
                  <fo:leader leader-length="500pt" leader-pattern="rule" rule-thickness="1pt" color="black"/>
               </fo:block>
               <fo:block text-align="center" font-weight="bold">Important Notice</fo:block>		      
               <fo:block><fo:leader leader-pattern="space"/></fo:block>
            </fo:table-cell>
         </fo:table-row>
      </fo:table-body>
      </fo:table>   
      
      <fo:block text-align="justify" font-weight="bold">
      	<xsl:value-of select="NoticeLine1"/>
      </fo:block>
      <fo:block text-align="justify" font-weight="bold">
      	<fo:block><fo:leader leader-pattern="space"/></fo:block>
      	<xsl:value-of select="concat(NoticeLine2, NoticeLine3)"/>
      </fo:block>
       <fo:block text-align="center">
                  <fo:leader leader-length="500pt" leader-pattern="rule" rule-thickness="1pt" color="black"/>
               </fo:block>
   </xsl:template>
  
       
   
</xsl:stylesheet>