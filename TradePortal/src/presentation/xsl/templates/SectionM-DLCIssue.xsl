<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <xsl:template match="Document/SectionM">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table text-align="center">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>
                  <fo:block text-align="center">
                     <fo:leader leader-length="500pt" leader-pattern="rule" rule-thickness="1pt" color="black"/>
                  </fo:block>
                  <fo:block text-align="center" font-weight="bold">Important Notice</fo:block>		      
                  <fo:block><fo:leader leader-pattern="space"/></fo:block>
		          <fo:block text-align="justify" font-weight="bold"></fo:block>
			      <fo:block text-align="justify" font-weight="bold">
			      	<xsl:value-of select="concat('Applicant understands that the final form of Credit may be subject to such revisions and changes as are deemed necessary or appropriate by Bank and Applicant hereby consents to such revisions and changes.', NoticeLine1)"/>
			      </fo:block>
                  <xsl:apply-templates/>
                  <fo:block text-align="center">
                     <fo:leader leader-length="500pt" leader-pattern="rule" rule-thickness="1pt" color="black"/>
                  </fo:block>     
	       </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   
   
</xsl:stylesheet>