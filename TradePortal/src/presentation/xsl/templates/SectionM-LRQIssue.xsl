<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<!-- //jgadela 06-18-2014 Rel-9.0 IR# T36000026067. Implemented the pdf app for GUA and Made the Important Notice text dynamic  -->
    <xsl:template match="Document/SectionM/NoticeLine1">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      
      <fo:table text-align="center">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>
                  <fo:block text-align="center">
                     <fo:leader leader-length="500pt" leader-pattern="rule" rule-thickness="1pt" color="black"/>
                  </fo:block>
                  <fo:block text-align="center" font-weight="bold">Important Notice</fo:block>		      
                  <fo:block><fo:leader leader-pattern="space"/></fo:block>
					  <fo:block text-align="justify" font-weight="bold"> <xsl:apply-templates/> </fo:block>
	       		</fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      </xsl:template>
      <xsl:template match="Document/SectionM/NoticeLine2">
      <fo:table text-align="center">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>
                  <fo:block><fo:leader leader-pattern="space"/></fo:block>
                  <fo:block text-align="justify" font-weight="bold"><xsl:apply-templates/></fo:block>
	       </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table>
          <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                 <fo:block text-align="center">
                     <fo:leader leader-length="500pt" leader-pattern="rule" rule-thickness="1pt" color="black"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
      </fo:table>
      </xsl:template>
   
</xsl:stylesheet>