<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:template match="Document/SectionN">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>
		  <xsl:apply-templates/>
	       </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   
   <xsl:template match="Document/SectionN/IssueDate">
      <fo:block>
         <fo:inline font-weight="bold">Original Date of Issuance: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionN/LCCurrencyAmount">
       <fo:block>
         <fo:inline font-weight="bold">Original LC Amount: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
    <xsl:template match="Document/SectionN/GuaranteeCurrencyAmount">
       <fo:block>
         <fo:inline font-weight="bold">Original Guarantee Amount: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionN/IncreaseBy">
      <fo:block>
         <fo:inline font-weight="bold">Increase By: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionN/DecreaseBy">
      <fo:block>
         <fo:inline font-weight="bold">Decrease By: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionN/NewCurrencyAmount">
      <fo:block>
         <fo:inline font-weight="bold">New LC Amount: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionN/NewGuaranteeCurrencyAmount">
      <fo:block>
         <fo:inline font-weight="bold">New Guarantee Amount: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionN/Tolerance">
       <fo:block>
          <fo:inline font-weight="bold">New Amount Tolerance: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionN/ExpiryDate">
      <fo:block>
         <fo:inline font-weight="bold">New Expiry Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionN/MaximumCreditAmount">
       <fo:block>
          <fo:inline font-weight="bold">New Maximum Credit Amount: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionN/AdditionalAmountsCovered">
       <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
       <!--<fo:block white-space-collapse="false">-->
       <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
       <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
          <fo:inline font-weight="bold">New Additional Amounts Covered: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
</xsl:stylesheet>