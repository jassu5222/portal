<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:import href="./po/POList.xsl"/>
   <xsl:template match="Document/SectionPExpanded">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>
                  <fo:block font-weight="bold" >
                     <fo:inline text-decoration="underline">Shipment</fo:inline>               
                  </fo:block>    
		
	       </fo:table-cell>
            </fo:table-row>
            <fo:table-row> 
               <fo:table-cell>
                  <fo:block font-weight="bold" >
                       <fo:inline font-weight="bold">Shipment Details:</fo:inline>
                  </fo:block>    
		  	<xsl:apply-templates/>
	       </fo:table-cell>
            </fo:table-row>
           
         </fo:table-body>
      </fo:table>
   </xsl:template>
   
   <xsl:template match="Document/SectionPExpanded/ShipmentDate">
      <fo:block>
         <fo:inline font-weight="bold">New Latest Shipment Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionPExpanded/Incoterm">
       <fo:block>
         <fo:inline font-weight="bold">New Incoterm: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionPExpanded/From">
      <fo:block>
         <fo:inline font-weight="bold">From New Place of Taking Charge/Dispatch From.../Receipt: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
    <xsl:template match="Document/SectionPExpanded/FromLoad">
      <fo:block>
         <fo:inline font-weight="bold">From New Port of Loading/Airport of Departure:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
    <xsl:template match="Document/SectionPExpanded/ToDischarge">
      <fo:block>
         <fo:inline font-weight="bold">To New Port of Discharge/Airport of Destination: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionPExpanded/To">
      <fo:block>
         <fo:inline font-weight="bold">To New Place of Final Destination/Delivery/For Transport To: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
 
   
   <xsl:template match="Document/SectionPExpanded/GoodsDescription"> 
       <fo:block>
         <fo:inline font-weight="bold">Changes to Goods Description/Shipment Details: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>

 
   <xsl:template match="Document/SectionPExpanded/POLineItems">
       <fo:block>
          <fo:leader leader-pattern="space"/>
       </fo:block>
       <fo:block font-weight="bold">
          <fo:inline text-decoration="underline">PO Line Items:</fo:inline>
       </fo:block>  
      
       <fo:block font-family="Courier" font-size="10pt" font-weight="bold" white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"> 
          <xsl:apply-templates/>
       </fo:block>
   </xsl:template>	
   
    <xsl:template match="Document/SectionPExpanded/PurchaseOrderLists">
     <xsl:call-template name="POList"/>
   </xsl:template>
   
   
   
</xsl:stylesheet>