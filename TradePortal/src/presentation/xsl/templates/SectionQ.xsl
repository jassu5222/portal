<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:import href="./po/POList.xsl"/><!-- BSL IR RNUM050251590 06/08/11 Rel 8.0 ADD -->
   <xsl:template match="Document/SectionQ">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold" >
         <fo:inline text-decoration="underline">Changes to Goods Description/Shipment Details:</fo:inline>
      </fo:block>    
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="Document/SectionQ/GoodsDescription">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false"><xsl:apply-templates/></fo:block>-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
   </xsl:template>

   <xsl:template match="Document/SectionQ/POLineItems">
       <fo:block>
          <fo:leader leader-pattern="space"/>
       </fo:block>
       <fo:block font-weight="bold">
          <fo:inline text-decoration="underline">PO Line Items:</fo:inline>
       </fo:block>
       <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
       <!--<fo:block font-family="Courier" font-size="10pt" font-weight="bold" white-space-collapse="false">-->
       <fo:block font-family="Courier" font-size="10pt" font-weight="bold" white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
       <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
          <xsl:apply-templates/>
       </fo:block>
   </xsl:template>	
   
   <xsl:template match="Document/SectionQ/PurchaseOrderLists">
      <!-- BSL IR RNUM050251590 06/08/2012 Rel 8.0 BEGIN -->
      <!--<fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Purchase Orders List:</fo:inline>
      </fo:block>
      <fo:block font-family="Courier" font-size="10pt" font-weight="bold" white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
         <fo:table font-size="9pt">
            <xsl:for-each select="./ResultSetRecord[1]/*">
               <fo:table-column>
                  <xsl:attribute name="column-width">
                     <xsl:text>29mm</xsl:text>
                  </xsl:attribute>
                  <xsl:attribute name="column-number">
                     <xsl:value-of select="position()"/>
                  </xsl:attribute>
               </fo:table-column>
               
            </xsl:for-each>
            <fo:table-header>
               <fo:table-row>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block >
                        <xsl:text>PO Number</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block >
                        <xsl:text>Issue Date</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block >
                        <xsl:text>Latest Shipment Date</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block >
                        <xsl:text>CCY</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block >
                        <xsl:text>Amount</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block >
                        <xsl:text>Seller</xsl:text>
                     </fo:block>
                  </fo:table-cell>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block>
                        <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black">
                           <xsl:attribute name="leader-length">
                              <xsl:value-of select="174"/>mm </xsl:attribute>
                        </fo:leader>
                     </fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-header>
            <fo:table-body>
               <xsl:for-each select="./ResultSetRecord">
                  <fo:table-row>
                     <xsl:if test="(position() mod 2) =0">
                        <xsl:attribute name="background-color">#E7E7E7</xsl:attribute>
                     </xsl:if>
                     <xsl:for-each select="./*">
                        <fo:table-cell>
                           <fo:block>
                              <xsl:value-of select="."/>
                           </fo:block>
                        </fo:table-cell>
                     </xsl:for-each>
                  </fo:table-row>
                  <fo:table-row>
                     <fo:table-cell>
                        <fo:block>
                           <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black">
                              <xsl:attribute name="leader-length">
                                 <xsl:value-of select="174"/>mm </xsl:attribute>
                           </fo:leader>
                        </fo:block>
                     </fo:table-cell>
                  </fo:table-row>
               </xsl:for-each>
               
            </fo:table-body>
         </fo:table>
      </fo:block>-->
      <xsl:call-template name="POList"/>
      <!-- BSL IR RNUM050251590 06/08/2012 Rel 8.0 END -->
   </xsl:template>
   
   
</xsl:stylesheet>