<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:import href="./po/InvList.xsl"/>
   <xsl:template match="Document/SectionR">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold" >
         <fo:inline text-decoration="underline">Additional Conditions:</fo:inline>
      </fo:block>    
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="Document/SectionR/InvoiceOnly">
      <fo:block>
         <fo:inline font-weight="bold">Invoices: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>     

   <xsl:template match="Document/SectionR/InvoiceDueDate">
      <fo:block>
         <fo:inline font-weight="bold">Invoice Due Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   <!-- SHR CR708 Rel8.1.1 BEGIN -->
      <xsl:template match="Document/SectionR/InvoiceDetails">
          <xsl:call-template name="InvList"/>
      </xsl:template>   
   <!-- SHR CR708 Rel8.1.1 END -->
   
   <xsl:template match="Document/SectionR/AdditionalConditions">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false"><xsl:apply-templates/></fo:block>-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
   </xsl:template>
   
</xsl:stylesheet>