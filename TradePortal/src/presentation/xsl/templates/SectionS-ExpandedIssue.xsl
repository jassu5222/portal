<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    
    <xsl:template match="Document/SectionSExpanded">  
        <fo:block>  
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="Document/SectionSExpanded/Validity">
        <fo:table>
            <fo:table-column column-width="300pt"/>
            <fo:table-column column-width="225pt"/>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell>
                        <fo:block>
                            <fo:leader leader-pattern="space"/>
                        </fo:block>
                        <fo:block font-weight="bold">
                            <fo:inline text-decoration="underline"> Validity : </fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell/>
                </fo:table-row>     
                <fo:table-row>   
                    <fo:table-cell>    
                        <xsl:apply-templates select="./ValidFrom"/>    
                    </fo:table-cell>       	
                </fo:table-row> 
                <fo:table-row> 
                    <fo:table-cell>
                        <xsl:apply-templates select="./ValidTo"/> 
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>  
    </xsl:template>   
    <xsl:template match="Document/SectionSExpanded/Validity/ValidFrom">
        <fo:block>
            <fo:inline font-weight="bold">Valid From: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template>   
    
    <xsl:template match="Document/SectionSExpanded/Validity/ValidTo">
        <fo:block>
            <fo:inline font-weight="bold">Valid To: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="Document/SectionSExpanded/DeliveryInstructions">
        <fo:table>
            <fo:table-column column-width="300pt"/>
            <fo:table-column column-width="225pt"/>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell>
                        <fo:block>
                            <fo:leader leader-pattern="space"/>
                        </fo:block>
                        <fo:block font-weight="bold">
                            <fo:inline text-decoration="underline">Delivery Instructions: </fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell/>
                </fo:table-row>     
                <fo:table-row>
                    <fo:table-cell>
                        <xsl:apply-templates select="./DeliverTo"/>
                    </fo:table-cell>  
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell>
                        <xsl:apply-templates select="./DeliverBy"/>
                    </fo:table-cell>        
                </fo:table-row>
                
                
            </fo:table-body>
        </fo:table>      
    </xsl:template>   
    
    <xsl:template match="Document/SectionSExpanded/DeliveryInstructions/DeliverTo">
     <fo:block>  <fo:leader leader-pattern="space"/> </fo:block>
     <fo:block font-weight="bold">      
            <fo:inline text-decoration="underline">Deliver To: </fo:inline>
          </fo:block>
           <fo:block><xsl:apply-templates/></fo:block>
     
      
    </xsl:template>  
    
    
    <xsl:template match="Document/SectionSExpanded/DeliveryInstructions/DeliverTo/Agent">
        <fo:block><xsl:apply-templates select="./Name"/></fo:block>
        <fo:block><xsl:apply-templates select="./ContactName"/></fo:block>
        <fo:block><xsl:apply-templates select="./AddressLine1"/></fo:block>
        <fo:block><xsl:apply-templates select="./AddressLine2"/></fo:block>
        <fo:block><xsl:apply-templates select="./AddressStateProvince"/></fo:block>
        <fo:block><xsl:apply-templates select="./AddressCountryPostCode"/></fo:block>
        
        <fo:block><xsl:apply-templates select="./PhoneNumber"/></fo:block>
    </xsl:template>  
    
    <xsl:template match="Document/SectionSExpanded/DeliveryInstructions/DeliverTo/Instructions">
    <fo:block>   <fo:leader leader-pattern="space"/>  </fo:block>
        <fo:block>    
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template>   
    
    <xsl:template match="Document/SectionSExpanded/DeliveryInstructions/DeliverTo/ContractDetails">
        <fo:block>    
            <fo:inline font-weight="bold">Details of Tender / Order / Contract: </fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template>  
    
    <xsl:template match="Document/SectionSExpanded/DeliveryInstructions/DeliverBy">
 	   <fo:block>   <fo:leader leader-pattern="space"/>   </fo:block>
        <fo:block font-weight="bold">      
            <fo:inline text-decoration="underline">Deliver By: </fo:inline>
               </fo:block>
             <fo:block><xsl:apply-templates/></fo:block>
           
     
    </xsl:template>   
    
    <xsl:template match="Document/SectionSExpanded/DetailedInformation">
        <fo:table>
            <fo:table-column column-width="300pt"/>
            <fo:table-column column-width="225pt"/>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell>
                        <fo:block>
                            <fo:leader leader-pattern="space"/>
                        </fo:block>
                        <fo:block font-weight="bold">
                            <fo:inline text-decoration="underline">Detailed Information: </fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell/>
                </fo:table-row>     
                <fo:table-row>
                    <fo:table-cell>
                        <xsl:apply-templates select="./TypeOfGuarantee"/>
                    </fo:table-cell>  
                </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell>
                        <xsl:apply-templates select="./TypeOfStandbyLC"/>
                    </fo:table-cell>  
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell>                   
                        <fo:block>
                            <xsl:apply-templates select="./CurrencyCodeAmount"/>
                            <xsl:apply-templates select="./CurrencyValueAmountInWords"/>
                        </fo:block>
                    </fo:table-cell> 
                    
                    <fo:table-cell>
                        <fo:block>
                            <xsl:apply-templates select="./ExpiryPlace"/>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <fo:table-row>
                    <fo:table-cell>
                        <xsl:apply-templates select="./IssuingBank"/>
                    </fo:table-cell>        
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell>
                        <xsl:apply-templates select="./IssuingBank/Date"/>
                    </fo:table-cell>  
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell>
                        <xsl:apply-templates select="./IssuingBank/OverseasBank"/>
                    </fo:table-cell>        
                </fo:table-row>
            </fo:table-body>
        </fo:table>      
    </xsl:template> 
    
    
    <xsl:template match="Document/SectionSExpanded/DetailedInformation/TypeOfGuarantee">
        <fo:block>    
            <fo:inline font-weight="bold">Type of Guarantee: </fo:inline>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>  
    
    <xsl:template match="Document/SectionSExpanded/DetailedInformation/TypeOfStandbyLC">
        <fo:block>    
            <fo:inline font-weight="bold">Type of LC: </fo:inline>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template> 
    
    <xsl:template match="Document/SectionSExpanded/DetailedInformation/CurrencyCodeAmount">
        <fo:block>
            <fo:inline font-weight="bold">Amount:</fo:inline>
        </fo:block>
        <fo:block><xsl:apply-templates/></fo:block>
    </xsl:template>
    
    <xsl:template match="Document/SectionSExpanded/DetailedInformation/CurrencyValueAmountInWords">
        <fo:block>
            <fo:inline>[</fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
            <fo:inline>]</fo:inline>
        </fo:block>
    </xsl:template>  
    <xsl:template match="Document/SectionSExpanded/DetailedInformation/ExpiryPlace">
        <fo:block>
            <fo:inline font-weight="bold">Expiry Place: </fo:inline>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>   
    
    <xsl:template match="Document/SectionSExpanded/DetailedInformation/IssuingBank">
     <fo:block>   <fo:leader leader-pattern="space"/>   </fo:block>
     <fo:block>
            <fo:inline font-weight="bold">Issuing Instructions: </fo:inline>
        </fo:block>
        <fo:block>  
            <fo:inline font-weight="bold">The instrument is to be issued by: </fo:inline>
            <xsl:apply-templates select="./IssuedBy"/>
        </fo:block>
        
    </xsl:template>  
    
    <xsl:template match="Document/SectionSExpanded/DetailedInformation/IssuingBank/Date">
        <fo:block>  
            <fo:inline font-weight="bold"> Validity Date at Overseas Bank </fo:inline>
            <xsl:apply-templates/>
        </fo:block>
        
    </xsl:template>  
    
    <xsl:template match="Document/SectionSExpanded/DetailedInformation/IssuingBank/OverseasBank">      
        <fo:block><xsl:apply-templates select="./Name"/></fo:block>
        <fo:block><xsl:apply-templates select="./AddressLine1"/></fo:block>
        <fo:block><xsl:apply-templates select="./AddressLine2"/></fo:block>
        <fo:block><xsl:apply-templates select="./AddressStateProvince"/></fo:block>
        <fo:block><xsl:apply-templates select="./AddressCountryPostCode"/></fo:block> 
        <fo:block><xsl:apply-templates select="./PhoneNumber"/></fo:block>
    </xsl:template>  
    
    
</xsl:stylesheet>
