<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionT">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <xsl:apply-templates/>

   </xsl:template>

   <xsl:template match="Document/SectionT/InstructionsToBank">
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Instructions To Bank:</fo:inline>
      </fo:block>
      <fo:block >
     	 <fo:inline>Issue instrument in: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionT/CommissionsAndCharges"> 
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Commissions and Charges: </fo:inline>
      </fo:block>
      <fo:block >
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   

   <xsl:template match="Document/SectionT/CommissionsAndCharges/DebitOurs">
      <fo:block>
         <fo:inline font-weight="bold">Debit: Our Account Number </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionT/CommissionsAndCharges/DebitForeign">
      <fo:block>
         <fo:inline font-weight="bold">Debit: Foreign Currency Account Number </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionT/CommissionsAndCharges/Currency">
      <fo:block>
         <fo:inline font-weight="bold">Currency of Account: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionT/CommissionsAndCharges/AdditionalInstructions">
      <fo:block>
         <fo:inline font-weight="bold">Additional Instructions: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   
</xsl:stylesheet>