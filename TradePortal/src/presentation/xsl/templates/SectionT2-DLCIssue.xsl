<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionT2">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="Document/SectionT2/SettlementInstructions">
      <!-- <fo:block font-weight="bold">
      		 <fo:inline text-decoration="underline">Settlement Instructions:</fo:inline>
     	 </fo:block>   -->
      <fo:block>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
    <xsl:template match="Document/SectionT2/SettlementInstructions/Heading">
      <fo:block  font-weight="bold">
         <fo:inline text-decoration="underline"><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   <xsl:template match="Document/SectionT2/SettlementInstructions/DebitOurAccount">
      <fo:block>
         <fo:inline font-weight="bold">Debit: Our Account Number </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   <xsl:template match="Document/SectionT2/SettlementInstructions/BranchCode">
      <fo:block>
         <fo:inline font-weight="bold">Branch Code:   </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   <xsl:template match="Document/SectionT2/SettlementInstructions/DebitForeignAccount">
      <fo:block>
         <fo:inline font-weight="bold">Debit: Foreign Currency Account Number </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   <xsl:template match="Document/SectionT2/SettlementInstructions/CurrencyOfAccount">
      <fo:block>
         <fo:inline font-weight="bold">Currency of Account:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
</xsl:stylesheet>