<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionU1">   
      <fo:table text-align="justify">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>     
                  <xsl:apply-templates/>      
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
   
   <xsl:template match="Document/SectionU1/CommisionAndCharges"> 
      <fo:block>
         <fo:inline font-weight="bold" text-decoration="underline">Commissions and Charges: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   <xsl:template match="Document/SectionU1/CommisionAndCharges/DebitOurAccount">
      <fo:block>
         <fo:inline font-weight="bold">Debit: Our Account Number </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   <xsl:template match="Document/SectionU1/CommisionAndCharges/DebitForeignAccount">
      <fo:block>
         <fo:inline font-weight="bold">Debit: Foreign Currency Account Number </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>    
   <xsl:template match="Document/SectionU1/CommisionAndCharges/CurrencyOfAccount">
      <fo:block>
         <fo:inline font-weight="bold">Currency of Account: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
      
   
</xsl:stylesheet>