<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
 <xsl:template match="Document/SettleInstrFooter/footerItems">
   		<fo:block margin-left="35pt" margin-right="35pt">
         	<fo:inline color="grey"><xsl:apply-templates select="./FooterItem"/></fo:inline>
         	<fo:inline><fo:leader leader-length="150pt" leader-pattern="space"/></fo:inline>
        	<fo:inline color="grey">Page: <fo:page-number/></fo:inline>
      	</fo:block>
   </xsl:template> 
</xsl:stylesheet>