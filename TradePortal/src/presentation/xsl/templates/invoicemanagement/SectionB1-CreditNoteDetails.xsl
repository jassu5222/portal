<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:template match="Document/SectionB1">
      
      <xsl:apply-templates select="./InvoiceDetails[1]/section-header" />
      
      <fo:table >    
        
          <xsl:for-each select="./InvoiceDetails[1]/*">
               <fo:table-column>
                  <xsl:attribute name="column-width">
                     <xsl:text>50mm</xsl:text>
                  </xsl:attribute>
                  <xsl:attribute name="column-number">
                     <xsl:value-of select="position()"/>
                  </xsl:attribute>
               </fo:table-column>
            </xsl:for-each>         
         <fo:table-header>
            <fo:table-row>
               <xsl:for-each select="./InvoiceDetails[1]/field">
                  <fo:table-cell>
                   <xsl:apply-templates select="./name" mode="block"></xsl:apply-templates>                   
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
         </fo:table-header>
         
         <fo:table-body >
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="rule" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row >
               <xsl:for-each select="./InvoiceDetails[1]/field">
                  <fo:table-cell>
                     <fo:block wrap-option="wrap" font-size="10pt">
                     <fo:inline><xsl:value-of select="./value" /> </fo:inline>
                    </fo:block>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
         </fo:table-body>
      </fo:table>     
   </xsl:template>  
   
</xsl:stylesheet>