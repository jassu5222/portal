<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:template match="Document/SectionE">
      <fo:block height="4mm" width="180mm">
         <fo:leader leader-pattern="space"/>
      </fo:block>

      <xsl:apply-templates select="./InvoiceGoodsDetails[1]/section-header" />
         
      <xsl:for-each select="./InvoiceGoodsDetails">
         <fo:table>
            <fo:table-column column-width="131mm" column-number="1"/>
            <fo:table-column column-width="131mm" column-number="2"/>
            <fo:table-body>
               <fo:table-row>
                  <fo:table-cell>
                      <fo:block>
                        <fo:inline font-weight="bold"><xsl:apply-templates select="./POIDLabel"/> </fo:inline>
                      </fo:block>   
                  </fo:table-cell>
                  <fo:table-cell>
                      <fo:block>
                        <fo:inline font-weight="bold"><xsl:apply-templates select="./POIssueDateLabel"/> </fo:inline>
                      </fo:block>   
                  </fo:table-cell>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                      <fo:block>
                        <fo:inline><xsl:apply-templates select="./POID"/> </fo:inline>
                      </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                      <fo:block>
                        <fo:inline><xsl:apply-templates select="./POIssueDate"/> </fo:inline>
                      </fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-body>
         </fo:table>         
      <fo:block height="4mm" width="180mm">
         <fo:leader leader-pattern="space"/>
      </fo:block>
         <fo:table>
            <fo:table-column column-width="131mm" column-number="1"/>
            <fo:table-column column-width="131mm" column-number="2"/>
            <fo:table-body>
               <fo:table-row>
                  <fo:table-cell>
                      <fo:block>
                        <fo:inline font-weight="bold"><xsl:apply-templates select="./GoodsDescriptionLabel"/> </fo:inline>
                      </fo:block>   
                  </fo:table-cell>
                  <fo:table-cell>
                  </fo:table-cell>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                      <fo:block>
                        <fo:inline><xsl:apply-templates select="./GoodsDescription"/> </fo:inline>
                      </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-body>
         </fo:table>  
         
      <fo:block height="4mm" width="180mm">
         <fo:leader leader-pattern="space"/>
      </fo:block>
         <fo:table>
            <fo:table-column column-width="131mm" column-number="1"/>
            <fo:table-column column-width="131mm" column-number="2"/>
            <fo:table-body>
               <fo:table-row>
                  <fo:table-cell>
                      <fo:block>
                        <fo:inline font-weight="bold"><xsl:apply-templates select="./SellerDefinedFieldsLabel"/> </fo:inline>
                      </fo:block>   
                  </fo:table-cell>
                  <fo:table-cell>
                  </fo:table-cell>
               </fo:table-row>
               <xsl:for-each select="./SellerDefinedFields/SellerInfo">
                   <fo:table-row>
                      <fo:table-cell>
                      
                      <fo:table>
            			<fo:table-column column-number="1"/>
            			<fo:table-column column-number="2"/>
            			<fo:table-body>
               			<fo:table-row>
                  		<fo:table-cell>
                  		
                         <fo:block>
                            <xsl:apply-templates select="./Label" mode="inline"/>
                         </fo:block>
                      </fo:table-cell>
                      <fo:table-cell>
                         <fo:block>
                            <xsl:apply-templates select="./Value" mode="split"/>
                         </fo:block>
                      </fo:table-cell>  
                       </fo:table-row> 
                         </fo:table-body>
        			 </fo:table>
        			 
                      </fo:table-cell>
                   </fo:table-row>
                </xsl:for-each>
            </fo:table-body>
         </fo:table>
                
      </xsl:for-each>
   </xsl:template>
   
   
</xsl:stylesheet>