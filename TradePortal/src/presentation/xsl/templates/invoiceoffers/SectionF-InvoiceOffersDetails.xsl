<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionF">

      <fo:block height="4mm" width="180mm">
         <fo:leader leader-pattern="space"/>
      </fo:block>
             
      <xsl:apply-templates select="./InvoiceHistory/section-header" />
      
      <fo:table >
         <fo:table-column column-width="50mm" column-number="1"/>
         <fo:table-column column-width="50mm" column-number="2"/>
         <fo:table-column column-width="70mm" column-number="3"/>
         <fo:table-column column-width="50mm" column-number="4"/>
         
         <fo:table-header>
            <fo:table-row>
               <xsl:for-each select="./InvoiceHistory/name">
                  <fo:table-cell>
                     <xsl:apply-templates select="." mode="block"/>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
         </fo:table-header>         
         
         <fo:table-body>
            <xsl:for-each select="./InvoiceHistory/Log">
                <fo:table-row>
                   <xsl:for-each select="./value">
                        <fo:table-cell>
                            <xsl:apply-templates select="." mode="block"/>
                        </fo:table-cell>
                    </xsl:for-each>
                </fo:table-row>
            </xsl:for-each>
         </fo:table-body>
      </fo:table>     
   </xsl:template>  
   
</xsl:stylesheet>