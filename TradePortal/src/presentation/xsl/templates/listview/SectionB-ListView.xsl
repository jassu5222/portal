<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <!--  RKazi CR-710 02/02/2012 Rel 8.0 Add -->
   <xsl:template match="Document/SectionB">
      <fo:table font-size="9pt">
         <xsl:for-each select="./ResultSetRecord[1]/field">
            <xsl:variable name="po">
               <xsl:value-of select="position()"/>
            </xsl:variable>
            <xsl:variable name="colNWidth">
               <xsl:for-each select="/Document/SectionB/ResultSetRecord/field[number($po)]">
                  <xsl:sort select="nameSize" data-type="number" order="descending"/>
                  <xsl:if test="position() = 1">
                     <xsl:value-of select="nameSize"/>
                  </xsl:if>
               </xsl:for-each>
            </xsl:variable>
            <xsl:variable name="colVWidth">
               <xsl:for-each select="/Document/SectionB/ResultSetRecord/field[number($po)]">
                  <xsl:sort select="valueSize" data-type="number" order="descending"/>
                  <xsl:if test="position() = 1">
                     <xsl:value-of select="valueSize"/>
                  </xsl:if>
               </xsl:for-each>
            </xsl:variable>
            
            <fo:table-column>
               <xsl:attribute name="column-width">
                  <xsl:if test="$colNWidth &gt; $colVWidth">
                     <xsl:value-of select="number($colNWidth) + 15"/>mm </xsl:if>
                  <xsl:if test="$colNWidth &lt; $colVWidth">
                     <xsl:value-of select="number($colVWidth) + 15"/>mm </xsl:if>
                  <xsl:if test="$colNWidth = $colVWidth">
                     <xsl:value-of select="number($colVWidth) + 15"/>mm </xsl:if>
               </xsl:attribute>
               <xsl:attribute name="column-number">
                  <xsl:value-of select="position()"/>
               </xsl:attribute>
            </fo:table-column>
         </xsl:for-each>
         
         <fo:table-header>
            <fo:table-row>
               <xsl:for-each select="./ResultSetRecord[1]/field">
                  <fo:table-cell background-color="#EEEECC">
                     <xsl:apply-templates select="./name" mode="block"/>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black">
                        <xsl:attribute name="leader-length">
                           <xsl:value-of select="$lineWidth"/>mm </xsl:attribute>
                     </fo:leader>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-header>
         
         <fo:table-body>
            <xsl:for-each select="./ResultSetRecord">
               <fo:table-row>
                  <xsl:if test="(position() mod 2) =0">
                     <xsl:attribute name="background-color">#E7E7E7</xsl:attribute>
                  </xsl:if>
                  <xsl:for-each select="./field">
                     <fo:table-cell>
                        <xsl:apply-templates select="./value" mode="split"/><!--AAlubala IR#DAUM050538861 Wrap the values of the column - changed from block to split mode-->
                     </fo:table-cell>
                  </xsl:for-each>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block>
                        <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black">
                           <xsl:attribute name="leader-length">
                              <xsl:value-of select="$lineWidth"/>mm </xsl:attribute>
                        </fo:leader>
                     </fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </xsl:for-each>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   
</xsl:stylesheet>