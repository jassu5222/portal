<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:import href="../FOCommon.xsl"/>
  <xsl:template name="PayInvList" match="/EmailAttachment">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block >
         <fo:inline>Payment Covering:</fo:inline>
      </fo:block>
	  <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
         <fo:table font-size="8pt">
         	<fo:table-column column-width="2in" />
			<fo:table-column column-width="1.8in"/>
			<fo:table-column column-width="2in"/>
			<fo:table-column column-width="2.7in" />
           <fo:table-header>
               <fo:table-row height="15pt">
                  <fo:table-cell>
                     <fo:block>
                        <xsl:text>Invoice Id</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                     <fo:block>
                        <xsl:text>Currency</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                     <fo:block text-align="right">
                        <xsl:text>Amount</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                     <fo:block>
                        <xsl:text>Due/Payment Date</xsl:text>
                     </fo:block>
                  </fo:table-cell>
               </fo:table-row>
               
            </fo:table-header>
            <fo:table-body>
			<xsl:for-each select="/EmailAttachment/SectionC/InvoiceDetailsList">
               <xsl:call-template  name="POListRow"/>
			   </xsl:for-each>
            </fo:table-body>
         </fo:table>
     
   </xsl:template>
   <xsl:template  name="POListRow">
      <fo:table-row>
          <xsl:call-template name="parseString">
  			<xsl:with-param name="list" select="."/>
		  </xsl:call-template>
      </fo:table-row>
      
   </xsl:template>

<xsl:template name="parseString">
	<xsl:param name="list" />

	<xsl:choose>
		<xsl:when test="contains($list, ', ')">
			<fo:table-cell>
				<fo:block>
					<xsl:value-of select="substring-before($list, ', ')" />
				</fo:block>
			</fo:table-cell>
			<xsl:call-template name="parseString">
				<xsl:with-param name="list" select="substring-after($list, ', ')" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<fo:table-cell>
				<fo:block>
					<xsl:value-of select="$list" />
				</fo:block>
			</fo:table-cell>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>
</xsl:stylesheet>