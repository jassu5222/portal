<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
    <!--  RKazi CR-710 02/02/2012 Rel 8.0 Add -->
   <xsl:template match="Document/SectionB">
      
      <xsl:apply-templates select="./PODetails[1]/section-header" />
      
      <fo:table >
         <fo:table-column column-width="40mm" column-number="1"/>
         <fo:table-column column-width="45mm" column-number="2"/>
         <fo:table-column column-width="45mm" column-number="3"/>
         <fo:table-column column-width="45mm" column-number="4"/>
         <fo:table-column column-width="45mm" column-number="5"/>
         <fo:table-column column-width="45mm" column-number="6"/>

         <!--<fo:table-header>-->
            <!--<fo:table-row>-->
               <!--<xsl:for-each select="./PODetails[1]/field">-->
               <!--</xsl:for-each>-->
            <!--</fo:table-row>-->
         <!--</fo:table-header>-->
         
         <fo:table-body >
 
            <fo:table-row>
               <xsl:for-each select="./PODetails[1]/field[position() &lt; 4]">
                         <fo:table-cell>
                             <xsl:apply-templates select="./name" mode="block"/>
                         </fo:table-cell>
                         <fo:table-cell>
                           <xsl:apply-templates select="./value" mode="split"/>
                        </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            
            <fo:table-row>
               <xsl:for-each select="./PODetails[1]/field[position() &gt; 3 and position() &lt; 7]">
                  <fo:table-cell>
                     <xsl:apply-templates select="./name" mode="block"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./value" mode="split"/>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row>
               <xsl:for-each select="./PODetails[1]/field[position() &gt; 6 and position() &lt; 10]">
                  <fo:table-cell>
                     <xsl:apply-templates select="./name" mode="block"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./value" mode="split"/>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="rule" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            
         </fo:table-body>
      </fo:table>     
   </xsl:template>  
   
</xsl:stylesheet>