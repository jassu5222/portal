/*
 * The Working-Dogs.com License, Version 1.1
 *
 * Copyright (c) 1999 Working-Dogs.com.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:  
 *       "This product includes software developed by the 
 *        Working-Dogs.com <http://www.Working-Dogs.com/>."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "Working-Dogs.com" and "Village" must not be used to 
 *    endorse or promote products derived from this software without 
 *    prior written permission. For written permission, please contact 
 *    jon@working-dogs.com.
 *
 * 5. Products derived from this software may not be called 
 *    "Working-Dogs.com" nor may "Village" appear in their names 
 *    without prior written permission of Working-Dogs.com.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Working-Dogs.com.  For more
 * information on the Working-Dogs.com, please see
 * <http://www.Working-Dogs.com/>.
 */
/*
 *     Modifications are Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

package com.workingdogs.village;

import java.sql.*;
import java.util.*;

/**
This class is used for doing select/insert/delete/update on the database.
A TableDataSet cannot be used to join multiple tables for an update, if you
need join functionality on a select, you should use a 
<a href="QueryDataSet.html">QueryDataSet</a>.
<P>
Here is an example usage for this code that gets the first 10 records where
column "a" = 1:

  <PRE>
KeyDef kd = new KeyDef().setAttrib("column");
TableDataSet tds = new TableDataSet(connection, "table_name", kd );
tds.where ("a=1" ); // WHERE a = 1
tds.fetchRecords(10); // fetch first 10 records where column a=1
for ( int i=0;i< tds.size(); i++ )
{
  Record rec = tds.getRecord(0); // zero based
  String columnA = rec.getValue("a");
  if ( columnA.equals ("1") )
    System.out.print ("We got a column!");
}
tds.close();
  </PRE>

<P>
It is important to remember to always close() the TableDataSet when you are finished
with it.
<P>
As you can see, using a TableDataSet makes doing selects from the database trivial.
You do not need to write any SQL and it makes it easy to cache a TableDataSet for future
use within your application.

@author Jon S. Stevens <A HREF="mailto:jon@working-dogs.com">jon@working-dogs.com</A>
@version $Revision:   1.0  $
*/
public class TableDataSet extends DataSet
{
    /** the optimistic locking column value */
    private String optimisticLockingCol;
    /** the value for the sql where clause */
    private String where = null;
    /** the value for the sql order by clause */
    private String order = null;
    /** the value for the sql other clause */
    private String other = null;
    // by default this is false;
    private boolean refreshOnSave = false;

    /**
     * Default constructor.
     *
     * @exception   SQLException
     * @exception   DataSetException
     */
    public TableDataSet() throws SQLException, DataSetException
    {
        super();
    }
    public TableDataSet (Connection conn,
            String tableName) throws SQLException, DataSetException
    {
        super (conn, tableName);
    }
    public TableDataSet (Connection conn, Schema schema,
            KeyDef keydef) throws SQLException, DataSetException
    {
        super (conn, schema, keydef);
    }
    public TableDataSet (Connection conn, String tableName,
            KeyDef keydef) throws SQLException, DataSetException
    {
        super (conn, tableName, keydef);
    }
    public TableDataSet (Connection conn, String tableName,
            String columns) throws SQLException, DataSetException
    {
        super (conn, tableName, columns);
    }
    public TableDataSet (Connection conn, String tableName,
            String columns, KeyDef keydef) throws SQLException,
    DataSetException
    {
        super (conn, tableName, columns, keydef);
    }

    /**
      * Use the TDS fetchRecords instead of the DataSet.fetchRecords
      *
      * @return     an instance of myself
      * @exception   SQLException
      * @exception   DataSetException
      */
    public DataSet fetchRecords() throws SQLException, DataSetException
    {
        return fetchRecords (-1);
    }

    /**
      * Use the TDS fetchRecords instead of the DataSet.fetchRecords
      *
      * @param   max
      * @return     an instance of myself
      * @exception   SQLException
      * @exception   DataSetException
      */
    public DataSet fetchRecords(int max) throws SQLException,
    DataSetException
    {
        return fetchRecords (0, max);
    }

    /**
      * Fetch start to max records. start is at Record 0
      *
      * @param   start
      * @param   max
      * @return     an instance of myself
      * @exception   SQLException
      * @exception   DataSetException
      */
    public DataSet fetchRecords(int start,
            int max) throws SQLException, DataSetException
    {
        buildSelectString();
        return super.fetchRecords (start, max);
    }

    /**
      * this is a string that contains the columns for the table
      * that this TableDataSet represents.
      *
      * @return     columns separated by ","
      */
    public String attributes()
    {
        return super.getColumns();
    }

    /**
      * Returns the KeyDef for the DataSet
      *
      * @return     a keydef
      */
    public KeyDef keydef()
    {
        return super.keydef();
    }

    /**
      * Returns the ResultSet for the DataSet
      *
      * @return     a ResultSet
      */
    public ResultSet resultSet() throws SQLException, DataSetException
    {
        return super.resultSet();
    }

    /**
      * Returns the Schema for the DataSet
      *
      * @return     a Schema
      */
    public Schema schema()
    {
        return super.schema();
    }

    /**
       Saves all the records in the DataSet.
       @return total number of records updated/inserted/deleted
     */
    public int save() throws SQLException, DataSetException
    {
        return save (connection(), false);
    }

    /**
       Saves all the records in the DataSet with the intransaction boolean value.
       @return total number of records updated/inserted/deleted
     */
    public int save(boolean intransaction) throws SQLException,
    DataSetException
    {
        return save (connection(), intransaction);
    }

    /**
       Saves all the records in the DataSet with the given connection and intransaction boolean value.
       @return total number of records updated/inserted/deleted
     */
    public int save(Connection conn,
            boolean intransaction) throws SQLException, DataSetException
    {
        int j = 0;
        for ( Enumeration e = records.elements() ; e.hasMoreElements(); )
        {
            Record rec = (Record) e.nextElement();
            j += rec.save(conn);
            //j++;
        }
        // now go through and remove any records
        // that were previously marked as a zombie by the 
        // delete process
        removeDeletedRecords();
        return j;
    }

    /**
       Not yet implemented
     */
    public int saveWithoutStatusUpdate(Connection conn)
            throws SQLException, DataSetException
    {
        throw new DataSetException ("Not yet implemented!");
    }

    /**
       Hell if I know what this does.
     */
    public String debugInfo()
    {
        return "Not yet implemented!";
    }

    /**
       Removes any records that are marked as a zombie.
     */
    public void removeDeletedRecords() throws DataSetException
    {
        for ( Enumeration e = records.elements() ; e.hasMoreElements(); )
        {
            Record rec = (Record) e.nextElement();
            if (rec.isAZombie())
                removeRecord (rec);
        }
    }

    /**
       Sets the table column used for optomistic locking.
     */
    public void setOptimisticLockingColumn(String olc)
    {
        this.optimisticLockingCol = olc;
    }

    /**
       Gets the table column used for optomistic locking.

       @returns string
     */
    public String optimisticLockingCol()
    {
        return this.optimisticLockingCol;
    }

    /**
       Sets the value for the SQL portion of the WHERE statement

       @returns instance of self
     */
    public TableDataSet where(String where) throws DataSetException
    {
        if (where == null)
            throw new DataSetException ("null not allowed for where clause");

        this.where = where;
        return this;
    }

    /**
       Gets the value of the SQL portion of WHERE.

       @returns string
     */
    String getWhere()
    {
        return this.where;
    }

    /**
       Sets the value for the SQL portion of the ORDER statement

       @returns instance of self
     */
    public TableDataSet order(String order) throws DataSetException
    {
        if (order == null)
            throw new DataSetException ("null not allowed for order clause");

        this.order = order;
        return this;
    }

    /**
       Gets the value of the SQL portion of ORDER.

       @returns string
     */
    String getOrder()
    {
        return this.order;
    }

    /**
       Sets the value for the SQL portion of the OTHER statement

       @returns instance of self
     */
    public TableDataSet other (String other) throws DataSetException
    {
        if (other == null)
            throw new DataSetException ("null not allowed for other clause");

        this.other = other;
        return this;
    }

    /**
       Gets the value of the SQL portion of OTHER.

       @returns string
     */
    String getOther()
    {
        return this.other;
    }

    /**
       This method refreshes all of the Records stored in this TableDataSet.
     */
    public void refresh(Connection conn) throws SQLException,
    DataSetException
    {
        for ( Enumeration e = records.elements() ; e.hasMoreElements(); )
        {
            Record rec = (Record) e.nextElement();
            rec.refresh(conn);
        }
    }

    /**
       Setting this causes each Record to refresh itself when a save() is performed on it.
       <P>
       Default value is false.

       @returns true if it is on; false otherwise
     */
    public void setRefreshOnSave(boolean val)
    {
        this.refreshOnSave = val;
    }

    /**
       Setting this causes each Record to refresh itself when a save() is performed on it.
       <P>
       Default value is false.

       @returns true if it is on; false otherwise
     */
    public boolean refreshOnSave()
    {
        return this.refreshOnSave;
    }

    /**
       This sets additional SQL for the table name.
       The string appears after the table name.
       Sybase users would set this to "HOLDLOCK" to get repeatable reads.
       <P>
       FIXME: Is this right? I don't use Sybase.

       @returns an instance of self
     */
    public TableDataSet tableQualifier(String tq)
    {
        // go directly to schema() cause it is where tableName is stored
        schema().appendTableName (tq);
        return this;
    }

    /**
       The name of the table for which this TableDataSet was created.

       @returns string
     */
    public String tableName() throws DataSetException
    {
        return super.tableName();
    }


    /**
      * Not yet implemented
      *
      * @exception   SQLException
      * @exception   DataSetException
      */
    public void updateStatus() throws SQLException, DataSetException
    {
        throw new DataSetException ("Not yet implemented!");
    }

    /**
       Builds the select string that was used to populate this TableDataSet.

       @returns SQL select string
     */
    public String getSelectString () throws DataSetException
    {
        buildSelectString();
        return this.selectString.toString();
    }

    /**
       Used by getSelectString to build the select string that was used to
       populate this TableDataSet.
     */
    private void buildSelectString () throws DataSetException
    {
        if (selectString == null)
            selectString = new StringBuffer (256);
        else
            selectString.setLength (0);

        selectString.append ("SELECT ");
        selectString.append (schema().attributes());
        selectString.append (" FROM ");
        selectString.append (schema().tableName());
        if (this.where != null && this.where.length() > 0)
            selectString.append (" WHERE " + this.where);
        if (this.order != null && this.order.length() > 0)
            selectString.append (" ORDER BY " + this.order);
        if (this.other != null && this.other.length() > 0)
            selectString.append (this.other);
    }
}
